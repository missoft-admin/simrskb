<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbuku_besar_piutang_setting extends CI_Controller {

	/**
	 * Setting Saldo Awal Piutang controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbuku_besar_piutang_setting_model','model');
		$this->load->model('tvalidasi_pendapatan_rajal_model');
  }

	function index(){
		
		$data = array();
		$data['list_KP'] 			= $this->model->list_KP();
		$data['list_asuransi'] 			= $this->model->list_asuransi();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Saldo Awal Piutang';
		$data['content'] 		= 'Tbuku_besar_piutang_setting/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Saldo Awal Piutang",'#'),
									    			array("List",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex()
	{
		// GetTipeDistributor
		$where='';
		$idrekanan=$this->input->post('idrekanan');
		$idkelompok=$this->input->post('idkelompok');
		if ($idrekanan){
			$idrekanan=implode(',',$idrekanan);			
		}
		if ($idkelompok){
			$idkelompok=implode(',',$idkelompok);			
		}
		if ($idrekanan){
			$where .=" AND CONCAT(T.idkelompok,'-',T.idrekanan) IN (".$idrekanan.")";
		}
		if ($idkelompok){
			$where .=" AND T.idkelompok IN (".$idkelompok.")";
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		$from="
			(
				SELECT T.*,IFNULL(SUM(H.debet),0) as debet,IFNULL(SUM(H.kredit),0) as kredit,K.nama as nama_kelompok,0 as st_verifikasi FROM (
					SELECT '1' as urutan,H.id as idkelompok,H.id as idrekanan,H.nama,CONCAT(H.id,'-',0) as idkel FROM mpasien_kelompok H
					WHERE H.`status`='1' AND H.id NOT IN (1,5)

					UNION ALL

					SELECT '2' as urutan,1 as idkelompok,H.id as idrekanan,H.nama,CONCAT('1','-',H.id) as idkel FROM mrekanan H
					WHERE H.`status`='1' 
					) T
					LEFT JOIN mpasien_kelompok K ON K.id=T.idkelompok
					LEFT JOIN tbuku_besar_piutang H ON CONCAT(H.idkelompokpasien,'-',H.idrekanan)=T.idkel
					WHERE T.urutan IS NOT NULL ".$where."
					
					GROUP BY T.idkel
					ORDER BY T.urutan,T.idkelompok,T.nama
				
			)tbl
		";
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array();
		$this->group  = array();

		$this->column_search   = array('nama');
		$this->column_order    = array();

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
		  $no++;
		  $row = array();

		  $row[] = $no;
		  $row[] = ($r->nama_kelompok);
		  $row[] = $r->nama.($r->st_verifikasi?'<br>'.text_danger('TELAH DIVERIFIKASI'):'');
		  $row[] = number_format($r->debet,2);
		  $row[] = number_format($r->kredit,2);
		  $aksi = '<div class="btn-group">';          
				$aksi .= '<a href="'.site_url().'tbuku_besar_piutang_setting/update/'.$r->idkelompok.'/'.$r->idrekanan.'" data-toggle="tooltip" title="Atur Saldo" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>';          
				$aksi .= '<a href="'.site_url().'tbuku_besar_piutang_detail/index/'.$r->idkelompok.'/'.$r->idrekanan.'" target="_blank" data-toggle="tooltip" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>';          
				// if ($r->st_verifikasi=='0'){
					// $aksi .= '<button data-toggle="tooltip" title="Verifikasi Saldo" class="btn btn-success btn-sm"><i class="fa fa-check" onclick="verifikasi('.$r->idkelompok.','.$r->idrekanan.')"> Verifikasi</i></button>';          
					
				// }
				// $aksi .= '<a href="'.site_url().'tbuku_besar_piutang_setting/update/'.$r->idkelompok.'/'.$r->id.'" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';          
		  $aksi .= '</div>';
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}
	

	function update($idkelompok,$idrekanan){
		$data=$this->model->getSpecified($idkelompok,$idrekanan);
		// print_r($data);exit();
		if($data){
			// if ($data['st_verifikasi']=='1'){
				// $data['disabel']='disabled';
			// }else{
				
				// $data['disabel']='';
			// }
			$data['list_akun'] = $this->tvalidasi_pendapatan_rajal_model->list_akun();
			$data['cari_idkelompok'] 			= '1';
			$data['tanggal_trx1'] 			= '';
			$data['tanggal_trx2'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Setting Saldo Awal Piutang';
			$data['content']	 	= 'Tbuku_besar_piutang_setting/manage';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Setting Saldo Awal Piutang",'#'),
														array("Ubah",'mdistributor')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdistributor');
		}
	}
	function generate(){
			$q="SELECT *FROM tbuku_besar_piutang_info";
			$row=$this->db->query($q)->row();
			if ($row){
				$data['tanggal_tagihan'] 			= HumanDateShort($row->tanggal_awal);
				$data['tanggal_tagihan2'] 			= HumanDateShort($row->tanggal_akhir);
				$data['idakun'] 			= ($row->idakun);
				$data['st_verifikasi'] 			= ($row->st_verifikasi);
				$data['saldo'] 			= ($row->saldo);
				$data['debet'] 			= ($row->debet);
				$data['kredit'] 			= ($row->kredit);
			}else{
				$data['tanggal_tagihan'] 			= '01-10-2020';
				$data['tanggal_tagihan2'] 			= '30-09-2021';
				$data['idakun'] 			= '#';
				$data['st_verifikasi'] 		= 0;
				$data['saldo'] 			= 0;
				$data['debet'] 			= 0;
				$data['kredit'] 		= 0;
			}
		
			// $data['list_akun'] = $this->tvalidasi_pendapatan_rajal_model->list_akun();
			$data['cari_idkelompok'] 			= '1';
			if ($data['st_verifikasi']=='1'){
				$data['disabel'] ='disabled';
			}else{
				$data['disabel'] ='';
			}
			$data['error'] 			= '';
			$data['title'] 			= 'Generate Saldo Awal Piutang';
			$data['content']	 	= 'Tbuku_besar_piutang_setting/manage_generate';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Setting Saldo Awal Piutang",'#'),
														array("Ubah",'mdistributor')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	
	public function list_pasien()
    {
        $cari 	= $this->input->post('search');
        $data_obat = $this->model->list_pasien($cari);
        $this->output->set_output(json_encode($data_obat));
    }
	public function simpan_create()
    {
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$tipe=$this->input->post('tipe');
		$idrekanan=$this->input->post('idrekanan');
		$st_multiple=$this->input->post('st_multiple');
		$tanggal_tagihan=YMDFormat($this->input->post('tanggal_tagihan'));
		$tanggal_kirim=YMDFormat($this->input->post('tanggal_kirim'));
		$jatuh_tempo_bayar=YMDFormat($this->input->post('jatuh_tempo_bayar'));
		if ($st_multiple=='1'){
			$idpasien=null;			
		}else{
			$idpasien=$this->input->post('idpasien');			
		}
		$data_info=array(
			'tipe' =>$tipe,
			'idkelompokpasien' =>$idkelompokpasien,
			'idrekanan' =>$idrekanan,
			'st_multiple' =>$st_multiple,
			'idpasien' =>$idpasien,
			'tanggal_tagihan' =>$tanggal_tagihan,
			'jatuh_tempo_bayar' =>$jatuh_tempo_bayar,
			'status_other_loss' =>0,
			'status_lunas' =>0,
			'tidak_terbayar' =>0,
			'total_bayar' =>0,
			'total_tagihan' =>0,
			'jml_faktur' =>0,
			'status_kirim' =>1,
			'st_saldo_awal' =>1,
			'tanggal_kirim' =>$tanggal_kirim,
			'kirim_date' =>$tanggal_kirim,
			'batas_kirim' =>$tanggal_kirim,
			'created_date' =>date('Y-m-d H:i:s'),
			'kirim_user_id' =>$this->session->userdata('user_id'),
			'kirim_user' =>$this->session->userdata('user_name'),
			'created_user_id' =>$this->session->userdata('user_id'),
			'created_user' =>$this->session->userdata('user_name'),
			'status' =>'3',
		);
		// print_r($data_info);exit();
		$result=$this->db->insert('tklaim', $data_info);
		
        $this->output->set_output(json_encode($result));
    }
	public function get_tklaim($id)
    {
		$q="SELECT H.id as klaim_id,H.tipe,H.idpasien,H.st_multiple,CONCAT(M.no_medrec,' - ',M.nama) as namapasien FROM tklaim H
LEFT JOIN mfpasien M ON M.id=H.idpasien
WHERE H.id='$id'";
		// print_r($data_info);exit();
		$result=$this->db->query($q)->row_array();
		
        $this->output->set_output(json_encode($result));
    }
	function list_tagihan(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$no_klaim=$this->input->post('no_klaim');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$status_pengiriman=$this->input->post('status_pengiriman');
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where1='';
		$where2='';
		$where='';
		
		if ($no_klaim !=''){
			$where1 .=" AND TK.no_klaim LIKE '%".$no_klaim."%' ";
		}
		
		if ($idkelompokpasien !='#'){
			$where1 .=" AND TK.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($idkelompokpasien !='1'){
			if ($idkelompokpasien!='1'){
				$idrekanan=0;
			}
		}
			$where1 .=" AND TK.idrekanan='$idrekanan' ";
		if ($tipe !='#'){
			$where1 .=" AND TK.tipe='$tipe' ";
		}
		// if ($status !='#'){
			$where1 .=" AND TK.st_saldo_awal='1' ";
		// }
		if ($status_pengiriman !='#'){
			if ($status_pengiriman=='1'){
				$where .=" WHERE selisih='0' ";
			}elseif ($status_pengiriman=='2'){
				$where .=" WHERE selisih > 1 ";
			}elseif ($status_pengiriman=='3'){
				$where .=" WHERE selisih < 0 ";
			}
		}
		
		
		if ('' != $tanggal_tagihan) {
            $where1 .= " AND DATE(TK.tanggal_tagihan) >='".YMDFormat($tanggal_tagihan)."' AND DATE(TK.tanggal_tagihan) <='".YMDFormat($tanggal_tagihan2)."'";
        }
		
        $from = "(
					SELECT CASE WHEN RS.id IS NULL THEN 2 ELSE 1 END as st_cari,TK.id,TK.no_klaim,TK.tipe,CASE WHEN TK.idkelompokpasien !='1' THEN kel.nama ELSE R.nama END as rekanan_nama,TK.idkelompokpasien
					,TK.idrekanan,TK.jml_faktur,TK.total_tagihan,TK.tanggal_tagihan
					,DATEDIFF(TK.tanggal_kirim, TK.batas_kirim) as selisih,TK.tanggal_kirim,TK.jatuh_tempo_bayar,TK.kirim_date,TK.noresi
					,TK.`status`,TK.status_kirim
					,TK.st_multiple,CONCAT(MP.no_medrec,' - ',MP.nama) nama_pasien,TK.idpasien
					FROM tklaim TK
					LEFT JOIN mpasien_kelompok kel ON kel.id=TK.idkelompokpasien
					LEFT JOIN mrekanan R ON R.id=TK.idrekanan
					LEFT JOIN mrekanan_setting RS ON RS.idrekanan=TK.idrekanan AND  TK.idkelompokpasien='1' AND RS.`status`='1'
					LEFT JOIN mfpasien MP ON MP.id=TK.idpasien AND TK.st_multiple='2'
					WHERE TK.`status` > 0 ".$where1."
					GROUP BY TK.id
				) as tbl ".$where." ORDER BY tanggal_tagihan DESC";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->tipe;
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->no_klaim.'<br>'.'<button title="Tambah Tagihan"  class="btn btn-xs btn-primary" onclick="add_detail('.$r->id.')"><i class="fa fa-plus"></i> Add Detail</button>';
            $row[] = $r->rekanan_nama.($r->st_multiple=='2'?'<br>'.$r->nama_pasien:'');
            $row[] = ($r->tipe=='1' ? '<span class="label label-primary">RJ</span>':'<span class="label label-success">RI/ODS</span>');
            $row[] = ($r->jml_faktur>0?$r->jml_faktur:text_danger('Belum Ada Detail'));
            $row[] = ($r->total_tagihan>0?number_format($r->total_tagihan,2):text_danger('Belum Ada Detail'));
            $row[] = HumanDateShort($r->tanggal_tagihan);
			if ($r->status=='1'){
				$row[] = ($r->status_kirim=='1' ? '<span class="label label-success">TELAH DIKIRIM</span>':'<span class="label label-danger">BELUM DIKIRIM</span>');
			}elseif ($r->status=='2'){
				$row[] = ($r->status_kirim=='1' ? '<span class="label label-success">TELAH DIKIRIM</span>':'<span class="label label-danger">BELUM DIKIRIM</span> <span class="label label-warning">STOP</span>');
			}elseif ($r->status=='3'){
				$row[] = ($r->status_kirim=='1' ? '<span class="label label-success">TELAH DIKIRIM</span>':'<span class="label label-danger">BELUM DIKIRIM</span> <span class="label label-warning">STOP</span>');
				
			}
            $row[] = ($r->selisih==''?'':status_selisih($r->selisih));
            $row[] = HumanDateShort($r->jatuh_tempo_bayar);
            $row[] = ($r->status_kirim=='1' ?HumanDateShort($r->tanggal_kirim):'<span class="label label-danger">BELUM DIKIRIM</span>');;
            $row[] = $r->noresi;

			
			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'/'.$r->status_kirim.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
			if ($r->status=='1'){
				$aksi .= '<button title="Stop Tagihan"  class="btn btn-xs btn-warning stop"><i class="fa fa-stop-circle"></i></button>';
			}elseif ($r->status=='2'){
				$aksi .= '<button title="Buka Tagihan"  class="btn btn-xs btn-success buka"><i class="fa fa-play-circle"></i></button>';
			}
			if ($r->status_kirim=='0'){
			$aksi .= '<button title="Kirim"  class="btn btn-xs btn-primary kirim"><i class="fa fa-send"></i></button>';
			$aksi .= '<button title="Memajukan Tagihan"  class="btn btn-xs btn-danger ganti"><i class="si si-arrow-right"></i></button>';
			}
			// if ($r->status_kirim=='1'){
			// $aksi .= '<button title="No Resi"  class="btn btn-xs btn-primary kirim"><i class="si si-docs"></i></button>';
			// }
			 $aksi .= '<div class="btn-group">
				  <div class="btn-group dropright">
					<button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
					  <span class="fa fa-print"></span>
					</button>
				  <ul class="dropdown-menu">
					<li>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_kwitansi/'.$r->id.'">Kwitansi</a>';
			if ($r->tipe=='1'){
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_rekap/'.$r->id.'">Rekapitulasi Tagihan</a>';
			}
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_surat/'.$r->id.'">Surat Tagihan</a>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_history_tagihan/'.$r->id.'">Tagihan Sebelumnya</a>';
			$aksi .= '</li>
				  </ul>
				  </div>
				</div>';
			$aksi.='</div>';
            $row[] = $aksi;           
            $row[] = $r->idkelompokpasien;  //15         
            $row[] = $r->idrekanan;           //16
            $row[] = $r->st_cari;           //17
            $row[] = $r->st_multiple;    //18       
            $row[] = $r->idpasien;           //19
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function info_total(){
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		$query=$this->query_saldo($tanggal_tagihan,$tanggal_tagihan2);
		$q="SELECT SUM(T.debet) as debet,SUM(T.kredit) as kredit,SUM(T.debet - T.kredit) as saldo FROM (
			".$query."
		) T";
		$arr=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function save_generate(){
		$idakun=$this->input->post('idakun');
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		$debet=$this->input->post('debet');
		$kredit=$this->input->post('kredit');
		$saldo=$this->input->post('saldo');
		$query=$this->query_saldo($tanggal_tagihan,$tanggal_tagihan2);
		$q="SELECT T.*,H.idpasien,D.pembayaran_id,D.pendaftaran_id,D.kasir_id,H.st_multiple FROM (
						".$query."
			) T
			LEFT JOIN tklaim H ON H.id=T.klaim_id
			LEFT JOIN tklaim_detail D ON D.id=T.klaim_detail_id";
		$this->db->delete('tbuku_besar_piutang',array('st_saldo_awal'=>1));
		$rows=$this->db->query($q)->result();
		foreach($rows as $row){
			$data=array(
				'klaim_id' => $row->klaim_id,
				'klaim_detail_id' => $row->klaim_detail_id,
				'klaim_pembayaran_id' => $row->klaim_pembayaran_id,
				'jenis_transaksi' => $row->jenis_transaksi,
				'idkelompokpasien' => $row->idkelompokpasien,
				'idrekanan' => $row->idrekanan,
				'tanggal_transaksi' => $row->tanggal,
				'asuransi' => $row->asuransi,
				'tipe' => $row->tipe,
				'pendaftaran_id' => $row->pendaftaran_id,
				'kasir_id' => $row->kasir_id,
				'pembayaran_id' => $row->pembayaran_id,
				'idpasien' => $row->idpasien,
				'st_multiple' => $row->st_multiple,
				'debet' => $row->debet,
				'kredit' => $row->kredit,
				'posisi' => ($row->debet?'D':'K'),
				'idakun' => $idakun,
				'st_verifikasi' => 1,
				'st_saldo_awal' => 1,
			);
			$this->db->insert('tbuku_besar_piutang',$data);			
		}
		$this->db->delete('tbuku_besar_piutang_info',array('st_verifikasi'=>1));
		$data=array(
			'tanggal_awal' => YMDFormat($tanggal_tagihan),
			'tanggal_akhir' => YMDFormat($tanggal_tagihan2),
			'debet' => RemoveComma($debet),
			'kredit' => RemoveComma($kredit),
			'saldo' => RemoveComma($saldo),
			'idakun' => ($idakun),
			'st_verifikasi' => 1,
		);
		$arr=$this->db->insert('tbuku_besar_piutang_info',$data);
		$this->output->set_output(json_encode($arr));
	}
	function query_saldo($tanggal_tagihan,$tanggal_tagihan2){
		$q="
			SELECT H.id as klaim_id,D.id as klaim_detail_id,'' klaim_pembayaran_id,H.tipe,'1' as jenis_transaksi,H.tanggal_kirim as tanggal,H.idkelompokpasien,CASE WHEN H.idkelompokpasien='1' THEN H.idrekanan ELSE 0 END as idrekanan
			,CONCAT(H.idkelompokpasien,'-',CASE WHEN H.idkelompokpasien='1' THEN H.idrekanan ELSE 0 END ) as idkelompok
			,COALESCE(MR.nama,MP.nama) as asuransi
			,D.nominal as debet,0 as kredit 
			FROM tklaim H
			LEFT JOIN tklaim_detail D ON D.klaim_id=H.id
			LEFT JOIN mpasien_kelompok MP ON MP.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
			WHERE H.status_kirim='1' AND H.tanggal_kirim >='".YMDFormat($tanggal_tagihan)."' AND H.tanggal_kirim <= '".YMDFormat($tanggal_tagihan2)."'

			UNION ALL

			SELECT D.klaim_id,D.klaim_detail_id,D.id as klaim_pembayaran_id,H.tipe,'2' as jenis_transaksi,D.tanggal_pembayaran as tanggal,H.idkelompokpasien,CASE WHEN H.idkelompokpasien='1' THEN H.idrekanan ELSE 0 END as idrekanan 
			,CONCAT(H.idkelompokpasien,'-',CASE WHEN H.idkelompokpasien='1' THEN H.idrekanan ELSE 0 END ) as idkelompok
			,COALESCE(MR.nama,MP.nama) as asuransi,'0' as debet,D.nominal_bayar as kredit
			FROM tklaim_pembayaran D
			LEFT JOIN tklaim H ON H.id=D.klaim_id
			LEFT JOIN mpasien_kelompok MP ON MP.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
			WHERE D.`status`='1' AND D.tanggal_pembayaran >='".YMDFormat($tanggal_tagihan)."' AND D.tanggal_pembayaran <= '".YMDFormat($tanggal_tagihan2)."'

			UNION ALL


			SELECT H.id as klaim_id,D.id as klaim_detail_id,'' klaim_pembayaran_id,H.tipe,'3' as jenis_transaksi,H.tanggal_kirim as tanggal,H.idkelompokpasien,CASE WHEN H.idkelompokpasien='1' THEN H.idrekanan ELSE 0 END as idrekanan
			,CONCAT(H.idkelompokpasien,'-',CASE WHEN H.idkelompokpasien='1' THEN H.idrekanan ELSE 0 END ) as idkelompok
			,COALESCE(MR.nama,MP.nama) as asuransi
			,0 as debet,D.tidak_terbayar  as kredit 
			FROM tklaim H
			LEFT JOIN tklaim_detail D ON D.klaim_id=H.id
			LEFT JOIN mpasien_kelompok MP ON MP.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
			WHERE H.status_kirim='1' AND H.status_lunas='1' AND H.status_other_loss='1' AND H.tanggal_kirim >='".YMDFormat($tanggal_tagihan)."' AND H.tanggal_kirim <= '".YMDFormat($tanggal_tagihan2)."'
			";
		return $q;
	}
	function rekap_piutang(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where1='';
	
		$query=$this->query_saldo($tanggal_tagihan,$tanggal_tagihan2);
		
        $from = "(
					SELECT T.idkelompokpasien,T.idrekanan,T.idkelompok,T.asuransi,SUM(T.debet) as debet,SUM(T.kredit) as kredit,SUM(T.debet - T.kredit) as saldo FROM (
						".$query."	
					) T
					GROUP BY T.idkelompok
					ORDER BY T.idkelompok DESC

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array('asuransi');
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->idkelompok;
            $row[] = $no;
            $row[] = $r->asuransi;
            $row[] = number_format($r->debet,2);
            $row[] = number_format($r->kredit,2);
            $row[] = number_format($r->saldo,2);
           
			
			$aksi .= '<button title="Lihat Detail" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></button>';
			
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function rekap_saldo(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where1='';
	
		$query=$this->query_saldo($tanggal_tagihan,$tanggal_tagihan2);
		
        $from = "(
					SELECT H.idkelompokpasien,H.idrekanan,CONCAT(H.idkelompokpasien,'-',H.idrekanan) as idkelompok 
					,H.asuransi,SUM(H.debet) as debet,SUM(H.kredit) as kredit,SUM(H.debet - H.kredit) as saldo
					FROM `tbuku_besar_piutang` H
					WHERE H.st_saldo_awal='1'
					GROUP BY CONCAT(H.idkelompokpasien,'-',H.idrekanan)
					ORDER BY CONCAT(H.idkelompokpasien,'-',H.idrekanan) DESC

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array('asuransi');
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->idkelompok;
            $row[] = $no;
            $row[] = $r->asuransi;
            $row[] = number_format($r->debet,2);
            $row[] = number_format($r->kredit,2);
            $row[] = number_format($r->saldo,2);
           
			
			$aksi .= '<button title="Lihat Detail" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></button>';
			
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_transaksi(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');


		$no_reg=$this->input->post('no_reg');
		$idpasien=$this->input->post('idpasien');
		$st_multiple=$this->input->post('st_multiple');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$tgl_trx1=$this->input->post('tgl_trx1');
		$tgl_trx2=$this->input->post('tgl_trx2');
		$tanggaldaftar1=$this->input->post('tanggaldaftar1');
		$tanggaldaftar2=$this->input->post('tanggaldaftar2');

		$where1='';
		$where2='';
		$where='';
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
			$where2 .=" AND RI.nopendaftaran='$no_reg' ";
		}
		
		if ($idkelompokpasien !='#'){
			$where1 .=" AND TK.tipekontraktor='$idkelompokpasien' ";
			$where2 .=" AND PD.tipekontraktor='$idkelompokpasien' ";
		}
		if ($idkelompokpasien =='1'){
			$where1 .=" AND TK.idkontraktor='$idrekanan' ";
			$where2 .=" AND PD.idkontraktor='$idrekanan' ";
		}
		if ($idpasien){
			$where1 .=" AND TP.idpasien='$idpasien' ";
			$where2 .=" AND RI.idpasien='$idpasien' ";
		}
		if ($tipe !='#'){
			$where .="  tipe='$tipe' ";
		}
		if ($status !='#'){
			$where1 .=" AND TK.st_verifikasi_piutang='0' ";
			$where2 .=" AND PD.st_verifikasi_piutang='0' ";
		}


		if ('' != $tgl_trx1) {
            $where1 .= " AND DATE(K.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(K.tanggal) <='".YMDFormat($tgl_trx2)."'";
            $where2 .= " AND DATE(PDH.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(PDH.tanggal) <='".YMDFormat($tgl_trx2)."'";
        }
		if ('' != $tanggaldaftar1) {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
            $where2 .= " AND DATE(RI.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(RI.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
        }
        $from = "(
					SELECT CASE WHEN S.id IS NOT NULL THEN 1 ELSE 2 END as st_setting,TK.id as idpembayaran

					, '1' as tipe,'RJ' as tipe_nama,TP.id as idpendaftaran
					,TP.tanggaldaftar,K.tanggal as tanggal_kasir,TP.nopendaftaran, TP.idpasien,TP.no_medrec,TP.namapasien,TP.idkelompokpasien,Kel.nama as kel_pasien
					,TK.tipekontraktor,R.nama as asuransi_nama,TK.idkontraktor,TK.nominal,TK.`status`,TK.st_verifikasi_piutang
					,TK.tanggal_jatuh_tempo
					,cek_jatuh_tempo_bayar ( TK.tipekontraktor, TK.idkontraktor, 1, S.id,Kel.st_multiple,TP.idpasien) AS next_date
					,K.id as idkasir,K.total as tot_trx,tklaim.`status` as status_klaim,KD.id as klaim_detail_id,tklaim.id as klaim_id,tklaim.no_klaim
					,Kel.st_multiple
					from tkasir K
					INNER JOIN tkasir_pembayaran TK  ON TK.idkasir=K.id
					LEFT JOIN tpoliklinik_tindakan TD ON TD.id=K.idtindakan
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.idpendaftaran
					LEFT JOIN mpasien_kelompok Kel ON Kel.id=TK.tipekontraktor
					LEFT JOIN mrekanan R ON R.id=TK.idkontraktor AND TK.tipekontraktor='1'
					LEFT JOIN mrekanan_setting S ON S.idrekanan=TK.idkontraktor AND S.`status`='1' AND TK.tipekontraktor='1' AND S.tipe='1'
					LEFT JOIN tklaim_detail KD ON KD.pembayaran_id=TK.id AND KD.tipe='1'
					LEFT JOIN tklaim ON tklaim.id=KD.klaim_id
					WHERE TK.idmetode='7' AND K.`status` !='0' AND K.idtipe IN (1,2) ".$where1."
					GROUP BY TK.id
					UNION ALL

					SELECT  CASE WHEN S.id IS NOT NULL THEN 1 ELSE 2 END as st_setting,PD.id as idpembayaran
					, '2' as tipe,'RI/ODS' as tipe_nama,RI.id as idpendaftaran
					,RI.tanggaldaftar,PDH.tanggal as tanggal_kasir,RI.nopendaftaran,RI.idpasien,RI.no_medrec,RI.namapasien,RI.idkelompokpasien,Kel.nama as kel_pasien,PD.tipekontraktor
					,R.nama as asuransi_nama,PD.idkontraktor,PD.nominal,RI.`status`,PD.st_verifikasi_piutang
					,PD.tanggal_jatuh_tempo
					,cek_jatuh_tempo_bayar ( PD.tipekontraktor, PD.idkontraktor, 2, S.id,Kel.st_multiple,RI.idpasien ) AS next_date
					,PDH.id as idkasir,PDH.totalharusdibayar as tot_trx,tklaim.`status` as status_klaim,KD.id as klaim_detail_id,tklaim.id as klaim_id,tklaim.no_klaim
					,Kel.st_multiple
					FROM trawatinap_tindakan_pembayaran PDH
					LEFT JOIN trawatinap_tindakan_pembayaran_detail PD ON PDH.id=PD.idtindakan
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=PDH.idtindakan
					LEFT JOIN mpasien_kelompok Kel ON Kel.id=PD.tipekontraktor
					LEFT JOIN mrekanan R ON R.id=PD.idkontraktor AND PD.tipekontraktor='1'
					LEFT JOIN mrekanan_setting S ON S.idrekanan=PD.idkontraktor AND S.`status`='1' AND PD.tipekontraktor='1' AND S.tipe='2'
					LEFT JOIN tklaim_detail KD ON KD.pembayaran_id=PD.id AND KD.tipe='2'
					LEFT JOIN tklaim ON tklaim.id=KD.klaim_id
					WHERE PD.idmetode='8' AND PDH.statusbatal='0' ".$where2."
					GROUP BY PD.id
				) as tbl WHERE  ".$where." ORDER BY tanggaldaftar ASC";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_kasir        = site_url('tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $url_klaim       = site_url('tklaim_rincian/');
            $url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
			if ($r->no_klaim){
				$no_klaim='<br><br><span class="label label-warning">'.$r->no_klaim.'</span>';			
			}else{
				$no_klaim='';				
			}
            $row[] = $r->tipe;
            $row[] = $r->idpembayaran;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = HumanDateShort($r->tanggal_kasir);
            $row[] = ($r->tipe=='1' ? '<span class="label label-primary">'.$r->tipe_nama.'</span>':'<span class="label label-success">'.$r->tipe_nama.'</span>');
			if ($r->tipe=='1'){
				$row[] = '<a href="'.$url_kasir.'transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i> '.$r->nopendaftaran.'</a>';				
			}else{
				$row[] = '<a href="'.$url_ranap.'verifikasi/'.$r->idpendaftaran.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i> '.$r->nopendaftaran.'</a>';				
			}
            $row[] = $r->no_medrec.'<br>'.$r->namapasien;
            $row[] = $r->nopendaftaran;
            $row[] = $r->kel_pasien;
            $row[] = ($r->idkelompokpasien!='1'?$r->kel_pasien:$r->asuransi_nama);
            $row[] = status_piutang($r->st_verifikasi_piutang).$no_klaim;
            $row[] = number_format($r->tot_trx,0);
            $row[] = number_format($r->nominal,0);
            $row[] = number_format($r->tot_trx-$r->nominal,0);
			if ($r->st_verifikasi_piutang=='0'){
				// if ($r->tanggal_jatuh_tempo==null || ($r->tanggal_jatuh_tempo < date('Y-m-d'))){
					if ($r->next_date){
						$row[] = HumanDateShort($r->next_date);
					}else{
						$row[] = '<span class="label label-danger">BELUM DISETTING</span>';
						$date_setting='0';
					}

				// }else{
					// $row[] = HumanDateShort($r->tanggal_jatuh_tempo);
				// }
			}else{
				$row[] = HumanDateShort($r->tanggal_jatuh_tempo);//15
			}

			if ($r->status_klaim){
			$aksi .= '<a class="view btn btn-xs btn-warning" href="'.$url_klaim.'rincian_tagihan/'.$r->klaim_id.'/'.$r->tipe.'/1" target="_blank"  type="button"  title="Rincian Tagihan"><i class="fa fa-external-link"></i></a>';
			}
			if($r->st_verifikasi_piutang=='0'){
				$aksi .= '<button title="Verifikasi" '.($date_setting=='0'?"disabled":"").' class="btn btn-xs btn-danger verifikasi"><i class="fa fa-check"></i> Pilih</button>';
			}
			
			$aksi.='</div>';
            $row[] = $aksi;//16
            $row[] = $r->klaim_detail_id;//17
            $row[] = '';//18
            $row[] = '';//19
            $row[] = $r->tipekontraktor;//20
            $row[] = $r->idkontraktor;//21
            $row[] = $r->st_multiple;//22
            $row[] = $r->idpasien;//23
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	public function verifikasi()
    {
		// $klaim_id=
		
		$klaim_id = $this->input->post('klaim_id');
		$id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $tanggal_jt =$this->db->query("SELECT H.jatuh_tempo_bayar FROM tklaim H WHERE H.id='$klaim_id'")->row('jatuh_tempo_bayar');;
		if ($this->cek_duplicate($tipe,$id)==false){
			$data_det=array(
					'klaim_id' =>$klaim_id,
					'tipe' =>$tipe,
					'pembayaran_id' =>$id,
					'user_verifikasi'=>$this->session->userdata('user_name'),
					'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					'tanggal_tagihan' =>$tanggal_jt,
				);
			$result=$this->db->insert('tklaim_detail', $data_det);

		
			$data =array(
				'st_verifikasi_piutang'=>'1',
				'user_verifikasi'=>$this->session->userdata('user_name'),
				'tanggal_verifikasi'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			if ($tipe=='1'){
				$result=$this->db->update('tkasir_pembayaran', $data);
			}else{
				$result=$this->db->update('trawatinap_tindakan_pembayaran_detail', $data);
			}
			if ($result) {
				$this->output->set_output(json_encode($result));
			} else {
				$this->output->set_output(json_encode($result));
			}
		}else{
			
            $this->output->set_output(json_encode(false));
		}
    }
	function cek_duplicate($tipe,$id){
		$q="SELECT D.id FROM `tklaim_detail` D WHERE D.pembayaran_id='$id' AND D.tipe='$tipe'";
		$result=$this->db->query($q)->row('id');
		if ($result){
			return true;
		}else{
			return false;
		}
	}

}
