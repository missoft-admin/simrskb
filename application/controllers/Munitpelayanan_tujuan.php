<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Munitpelayanan_tujuan extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Munitpelayanan_tujuan_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('2039'))){
			
			$data['tab'] 			= $tab;
			$data['idtipe'] 			= '#';
			$data['idunit'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Unti Pelayanan Asal / Tujuan';
			$data['content'] 		= 'Munitpelayanan_tujuan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Unit Pelayanan Asal / Tujuan",'munitpelayanan_tujuan')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_unit(){
		$idunit=$this->input->post('idunit');
		$data=array(
			'idunit'=>$this->input->post('idunit'),
		);
		$data['created_by']=$this->session->userdata('user_id');
		$data['created_date']=date('Y-m-d H:i:s');
		$hasil=$this->db->insert('munitpelayanan_tujuan',$data);

		json_encode($hasil);
	}
	
	
	function load_unit()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama FROM `munitpelayanan_tujuan` H
						INNER JOIN munitpelayanan M ON M.id=H.idunit
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('idunit','nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		
          $result[] = $no;
			 $aksi='';
          $result[] = $r->nama;
          $aksi = '<div class="btn-group">';
		  
		   if (UserAccesForm($user_acces_form,array('2041'))){
		  $aksi .= '<button onclick="hapus_unit('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_unit(){
	  $id=$this->input->post('id');
	 
		$this->db->where('idunit',$id);
		$hasil=$this->db->delete('munitpelayanan_tujuan');
	  
	  json_encode($hasil);
	  
  }


  function find_unit(){
	  $q="SELECT H.id,H.nama FROM munitpelayanan H
			WHERE H.`status`='1' AND H.id NOT IN (SELECT idunit FROM munitpelayanan_tujuan)";
	  $opsi='<option value="#" selected>-PILIH-</option>';
	  
	  $hasil=$this->db->query($q)->result();
	  foreach ($hasil as $r){
		$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
		  
	  }
	  
	  $this->output->set_output(json_encode($opsi));
  }
  
}
