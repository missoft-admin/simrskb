<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrole extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    PermissionUserLoggedIn($this->session);
    $this->load->model('mrole_model', 'model');
    $this->load->model('Mresources_model', 'r_model');
    //Codeigniter : Write Less Do More
  }

  function index(){
    
    $data = array();
    $data['error'] 			= '';
    $data['title'] 			= 'Role';
    $data['content'] 		= 'Mrole/index';
    $data['breadcrum'] 		= array(array("RSKB Halmahera",'#'), array("Role",'#'));

    $data['list_parent'] = $this->model->getAllParent();

    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }
  function manage($id){
    // $data = array(
		// 'id' => '#',
		// 'id_menu_sub' => '#',
		// 'action' => '',
		// 'status' => '#',            
	// );
    $data =$this->model->get_data($id);
	// print_r($data);exit();
    $this->model->insert_init($id);
	$data['menu_id']='#';
	$data['sub_id']='#';
	$data['list_role'] 			= $this->model->getAllRoles2($id);
	$data['list_menu'] 			= $this->r_model->get_utama();
    $data['list_sub'] 			= array();
    $data['error'] 			= '';
    $data['title'] 			= 'Role';
    $data['content'] 		= 'Mrole/manage';
    $data['breadcrum'] 		= array(array("RSKB Halmahera",'#'), array("Role",'#'));

    // $data['list_parent'] = $this->model->getAllParent();

    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  // @
  function get_child_level($headerpath) {
    $arr = $this->model->getPathLevel($headerpath);
    $this->output->set_output(json_encode($arr));
  }

  // @
  function save() {
    $data = [
      'nama'       => $this->input->post('nama-roles'),
      'idkelompok' => $this->input->post('jenis-roles'),
      'headerpath' => $this->input->post('parent-id'),
      'path'       => $this->input->post('path'),
      'level'      => $this->input->post('level'),
      'status'     => 1,
    ];

    $result = $this->model->saveData($data);

    if ($result) {
      $this->session->set_flashdata('confirm', true);
      $this->session->set_flashdata('message_flash', 'data telah disimpan.');
      redirect('mrole/index', 'location');
    }
  }

  // @
  function saveDetail() {
    $id     = $this->input->post('id');
    $detail = $this->input->post('detail');

    $result = $this->model->saveDetailData($id, $detail);

    if ($result) {
      return true;
    }
  }
	function savePilih() {
    $role_id     = $this->input->post('role_id');
    $id3     = $this->input->post('id3');
    $pilih     = $this->input->post('pilih');
	$this->db->where('id3',$id3);
	$this->db->where('role_id',$role_id);
    $result = $this->db->update('mroles_akses', array('pilih' => $pilih));

    if ($result) {
      return true;
    }
  }
  function utama_c_all() {
    $role_id     = $this->input->post('role_id');
    $menu_id     = $this->input->post('menu_id');
    $pilih     = $this->input->post('pilih');
   
    $q="UPDATE mroles_akses INNER JOIN (
		SELECT L3.id3 from mmenu L1 
		INNER JOIN mmenu_sub L2 ON L1.id=L2.id_menu
		INNER JOIN mmenu_action L3 ON L3.id_menu_sub=L2.id2 AND L3.`status`='1'
		WHERE L1.id='$menu_id' ) T ON T.id3=mroles_akses.id3 
		SET pilih='$pilih' WHERE mroles_akses.role_id='$role_id'";
    $result = $this->db->query($q);

    if ($result) {
      return true;
    }
  }
  function sub_c_all() {
    $role_id     = $this->input->post('role_id');
    $sub_id     = $this->input->post('sub_id');
    $pilih     = $this->input->post('pilih');
   
    $q="UPDATE mroles_akses INNER JOIN (
		SELECT L3.id3 from mmenu L1 
		INNER JOIN mmenu_sub L2 ON L1.id=L2.id_menu
		INNER JOIN mmenu_action L3 ON L3.id_menu_sub=L2.id2 AND L3.`status`='1'
		WHERE L2.id2='$sub_id' ) T ON T.id3=mroles_akses.id3 
		SET pilih='$pilih' WHERE mroles_akses.role_id='$role_id'";
    $result = $this->db->query($q);

    if ($result) {
      return true;
    }
  }
  function copy_to() {
    $role_id     = $this->input->post('role_id');//Tujuan Copy
    $id     = $this->input->post('id');//ID yg dicopy
   
    $q="UPDATE mroles_akses INNER JOIN (
		SELECT M.id3,M.pilih from mroles_akses M WHERE M.role_id='$role_id ') T ON T.id3=mroles_akses.id3
		SET mroles_akses.pilih=T.pilih
		WHERE mroles_akses.role_id='$id'
			";
    $result = $this->db->query($q);

    if ($result) {
      return true;
    }
  }

  public function showRoles() {
	  
$data_user=get_acces();
$user_acces_form=$data_user['user_acces_form'];
    $result = $this->model->getAllRoles();

    $data = array();

    $no = 1;
    foreach ($result as $row) {
      $rows = array();

	  $url        = site_url('mrole/');
      $btn_rules = '';
      if (UserAccesForm($user_acces_form,array('245'))){
        $btn_rules  = '<a href="'.$url.'manage/'.$row->id.'" type="button" title="Edit" class="btn btn-xs btn-success" style="float:right;"><i class="fa fa-plus"></i> Rules</a>';
      }
      $btn_delete = '';
      if (UserAccesForm($user_acces_form,array('246'))){
        $btn_delete = '<button class="btn btn-xs btn-danger delete-rules" data-value="'.$row->id.'"><i class="fa fa-trash"></i>&nbsp;Hapus</button>';
      }
      $rows[] = $no++;
      $rows[] = TreeView($row->level, $row->nama).$btn_rules;
      $rows[] = $btn_delete;

      $data[] = $rows;
    }

    $output = array(
      "recordsTotal"    => count($result),
      "recordsFiltered" => count($result),
      "data"            => $data
    );

    echo json_encode($output);
  }
public function showAkses($id) {
    $result = $this->model->getAllAkses($id);

    $data = array();

    $no = 1;
    foreach ($result as $row) {
      $rows = array();


      $btn_rules = '';
      if(button_roles('mrole/saveDetail')) {
        $btn_rules  = '<button class="btn btn-xs btn-success add-resource" data-value="'.$row->id3.'" data-toggle="modal" data-target="#add-rules" style="float:right;">Rules</button>';
      }
      $btn_delete = '';
      if(button_roles('mrole/deleteRole')) {
        $btn_delete = '<button class="btn btn-xs btn-danger delete-rules" data-value="'.$row->id3.'"><i class="fa fa-trash"></i>&nbsp;Hapus</button>';
      }
      $rows[] = $no++;
      // $rows[] = TreeView($row->level, $row->nama).$btn_rules;
      $rows[] = $row->menu3;
      $rows[] = $btn_delete;

      $data[] = $rows;
    }

    $output = array(
      "recordsTotal"    => count($result),
      "recordsFiltered" => count($result),
      "data"            => $data
    );

    echo json_encode($output);
  }
  function getAkses()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
    		$this->join 	= array();
    		$this->where  = array();

        if(isset($_POST['role_id']) && $_POST['role_id'] != '' && $_POST['role_id'] != null){
          $this->where = array_merge($this->where, array('view_akses.role_id' => $_POST['role_id']));
        }
		if(isset($_POST['menu_id']) && $_POST['menu_id'] != '#'){
          $this->where = array_merge($this->where, array('view_akses.id' => $_POST['menu_id']));
        }
		if(isset($_POST['sub_id']) && $_POST['sub_id'] != '#'){
          $this->where = array_merge($this->where, array('view_akses.id2' => $_POST['sub_id']));
        }

		// $def="A";
		// if (isset($_POST['snomedrec']) && isset($_POST['snamapasien'])){
			// $this->where = array_merge($this->where, array('mfpasien.nama LIKE' => '%'.$def.'%'));
		// }
    		$this->order  = array(
    			'index' => 'ASC'
    		);
    		$this->group  = array();
    		$this->from   = 'view_akses';

        $this->column_search   = array('menu','menu2','action','id3');
        $this->column_order    = array('menu','menu2','action');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $no;
            $row[] = $r->id3;
            $row[] = $r->menu;
            $row[] = $r->menu2;
            $row[] = $r->action;
			$chk="";
			$chk_sub="";
			// if ($r->index3=='1'){
				// $chk_sub='<label class="css-input switch switch-sm switch-danger">
                         // <input type="checkbox" '.$chk.' onclick="handleClick2(this,'.$r->id2.');"><span></span> Satu Form
					  // </label>';
			// }
			
			if ($r->pilih=='1'){
				$chk="checked";
			}
            $row[] = '<label class="css-input switch switch-sm switch-success">
                         <input type="checkbox" '.$chk.' class="pil" onclick="handleClick(this,'.$r->id3.');"><span></span> 
					  </label> ';
            // $row[] = ($r->status_kawin == 1 ? 'Belum Menikah' : 'Sudah Menikah');

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }


  function deleteRole() {
    
    $id = $this->input->post('id');

    $result = $this->model->deleteRole($id);
    if ($result) {
      return true;
    }
  }

  function showRules($id) {
    $result = $this->model->getAllRules($id);

    if (count($result) == 0) {
      $result2 = $this->model->getAllRules();

      $data = array();

      $no = 1;
      foreach ($result2 as $row) {
        $rows = array();

        $name = "permission".$row->id;

        $rows[] = $no++;
        $rows[] = TreeView($row->level, $row->nama);

        $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="2">';
        $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="1">';
        $rows[] = '<label class="radio-inline"><input class="permission" type="radio" name="'.$name.'" value="0" checked>&nbsp;<i>Deny</i></label>';

        $rows[] = "0";
        $rows[] = $row->id;

        $data[] = $rows;
      }
    } else {
      $data = array();

      $no = 1;
      foreach ($result as $row) {
        $rows = array();

        $name = "permission".$row->id;

        $rows[] = $no++;
        $rows[] = TreeView($row->level, $row->nama);

        if ($row->status == 2) {
          $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="2" checked>';
          $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="1">';
          $rows[] = '<label class="radio-inline"><input class="permission" type="radio" name="'.$name.'" value="0">&nbsp;<i>Deny</i></labe>';
        } elseif ($row->status == 1) {
          $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="2">';
          $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="1" checked>';
          $rows[] = '<label class="radio-inline"><input class="permission" type="radio" name="'.$name.'" value="0">&nbsp;<i>Deny</i></labe>';
        } else {
          $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="2">';
          $rows[] = '<input class="permission" type="radio" name="'.$name.'" value="1">';
          $rows[] = '<label class="radio-inline"><input class="permission" type="radio" name="'.$name.'" value="0" checked>&nbsp;<i>Deny</i></label>';
        }

        $rows[] = $row->status;
        $rows[] = $row->id;

        $data[] = $rows;
      }
    }

    $output = array(
      "recordsTotal"    => count($result),
      "recordsFiltered" => count($result),
      "data"            => $data
    );

    echo json_encode($output);
  }

}
