<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tews extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->model('Tews_model');
		$this->load->helper('path');
		
  }

	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
	
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_satuan_ttv=array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		// print_r($data);exit;
		$data_login_ppa=get_ppa_login();
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tpendaftaran_poli_ttv_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['st_ranap'] 			= $st_ranap;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['assesmen_id'] 			= '';
		$data['status_assesmen'] 			= '1';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Assesmen EWS';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan  ",'#'),
											  array("Assesmen EWS",'tpendaftaran_poli_ttv')
											);
		
		
		
		if ($menu_kiri=='input_ews' || $menu_kiri=='his_ews'){
			$data['template_assesmen_id']='';
			if ($trx_id!='#'){
				if ($menu_kiri=='his_ews'){
					$data_assemen=$this->Tews_model->get_data_ews_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tews_model->get_data_ews_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tews_model->get_data_ews_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tews_model->get_data_ews($pendaftaran_id,$st_ranap);
			// print_r($data_assemen);exit;
				
			}
			if ($data_assemen){
				$data_assemen['tab_utama']='1';
			}else{
				$data_assemen['tab_utama']='2';
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['total_skor_ews']='0';
				$data_assemen['tanggal_pengkajian']=date('d-m-Y');
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_ews'){
				
			}else{
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$list_template_assement=$this->Tews_model->list_template_ews();
			$data_assemen['list_template_assement']=$list_template_assement;
			
			$setting_ews=$this->Tpendaftaran_poli_ttv_model->setting_ews();
			
			$data = array_merge($data,$data_assemen,$setting_ews);
		}
		if ($menu_kiri=='input_pews' || $menu_kiri=='his_pews'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_pews'){
					$data_assemen=$this->Tews_model->get_data_pews_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tews_model->get_data_pews_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tews_model->get_data_pews_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tews_model->get_data_pews($pendaftaran_id,$st_ranap);
				
				
				
			}
			if ($data_assemen){
				// $data_assemen['']=$arr=explode(',',$str);
				$data_assemen['tab_utama']='1';
			}else{
				$data_assemen['tab_utama']='2';
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['total_skor_pews']='0';
				$data_assemen['mpews_id']='0';
				$data_assemen['tanggal_pengkajian']=date('d-m-Y');
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			$list_template_assement=$this->Tews_model->list_template_pews();
			
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['template_id']='';
			$setting_assesmen=$this->Tews_model->setting_pews();
			$data = array_merge($data,$setting_assesmen,$data_assemen);
		}
		
		$data['trx_id']=$trx_id;
		// print_r($data);exit;
		$data = array_merge($data, backend_info());
			// print_r($list_template_assement);exit;
		$this->parser->parse('module_template', $data);
		
	}
	//SBAR
	function batal_template_ews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_ews',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function create_ews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
		
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'st_ranap' => $st_ranap,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		'tipe_input' => 3,
	  );
	  
	  $hasil=$this->db->insert('tpoliklinik_ews',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_ews(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			'tingkat_kesadaran' => $this->input->post('tingkat_kesadaran'),
			'nadi' => $this->input->post('nadi'),
			'nafas' => $this->input->post('nafas'),
			'spo2' => $this->input->post('spo2'),
			'td_sistole' => $this->input->post('td_sistole'),
			'td_diastole' => $this->input->post('td_diastole'),
			'suhu' => $this->input->post('suhu'),
			'tinggi_badan' => $this->input->post('tinggi_badan'),
			'berat_badan' => $this->input->post('berat_badan'),
			'suplemen_oksigen' => $this->input->post('suplemen_oksigen'),
			'total_skor_ews' => $this->input->post('total_skor_ews'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ews',$data);
		// if ($status_assemen=='2'){
			update_logic_EWS($assesmen_id);
		// }
		// $ttv_id=$this->db->insert_id();
		 $q="
		SELECT *FROM (
			SELECT H.*
			,TK.ref as nama_tk
			,MSTK.skor as skor_tk,MSTK.warna as warna_tk
			,MSnadi.skor as skor_nadi,MSnadi.warna as warna_nadi
			,MSnafas.skor as skor_nafas,MSnafas.warna as warna_nafas
			,MSspo2.skor as skor_spo2,MSspo2.warna as warna_spo2
			,MStd_sistole.skor as skor_td_sistole,MStd_sistole.warna as warna_td_sistole
			,MStd_diastole.skor as skor_td_diastole,MStd_diastole.warna as warna_td_diastole
			,MSsuhu.skor as skor_suhu,MSsuhu.warna as warna_suhu
			,MStinggi_badan.skor as skor_tinggi_badan,MStinggi_badan.warna as warna_tinggi_badan
			,MSberat_badan.skor as skor_berat_badan,MSberat_badan.warna as warna_berat_badan
			,MSSO.skor as skor_so,MSSO.warna as warna_so,SO.ref as nama_so

			FROM (
				SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.assesmen_id,H.tanggal_input,H.pendaftaran_id
						,null as pendaftaran_id_ranap,H.tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.tinggi_badan,H.berat_badan,H.suplemen_oksigen
						,1 as st_ranap
						,jml_edit,DR.hari,mhari.nama_hari,MP.nama as nama_created,H.status_assemen,H.hasil_ews,H.warna_ews 
						from tpoliklinik_ews H
						INNER JOIN mppa MP ON MP.id=H.created_ppa
						LEFT JOIN date_row DR ON DR.tanggal=H.tanggal_input
						LEFT JOIN mhari ON mhari.id=DR.hari
						WHERE assesmen_id='$assesmen_id'
			) H
			LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
			LEFT JOIN ews_master_skor MSTK ON MSTK.nilai_1=H.tingkat_kesadaran AND MSTK.staktif='1' AND MSTK.ews_id='1'
			LEFT JOIN ews_master_skor MSnadi ON H.nadi BETWEEN MSnadi.nilai_1 AND MSnadi.nilai_2 AND  MSnadi.staktif='1' AND MSnadi.ews_id='2'
			LEFT JOIN ews_master_skor MSnafas ON H.nafas BETWEEN MSnafas.nilai_1 AND MSnafas.nilai_2 AND  MSnafas.staktif='1' AND MSnafas.ews_id='3'
			LEFT JOIN ews_master_skor MSspo2 ON H.spo2 BETWEEN MSspo2.nilai_1 AND MSspo2.nilai_2 AND  MSspo2.staktif='1' AND MSspo2.ews_id='4'
			LEFT JOIN ews_master_skor MStd_sistole ON H.td_sistole BETWEEN MStd_sistole.nilai_1 AND MStd_sistole.nilai_2 AND  MStd_sistole.staktif='1' AND MStd_sistole.ews_id='5'
			LEFT JOIN ews_master_skor MStd_diastole ON H.td_diastole BETWEEN MStd_diastole.nilai_1 AND MStd_diastole.nilai_2 AND  MStd_diastole.staktif='1' AND MStd_diastole.ews_id='6'
			LEFT JOIN ews_master_skor MSsuhu ON H.suhu BETWEEN MSsuhu.nilai_1 AND MSsuhu.nilai_2 AND  MSsuhu.staktif='1' AND MSsuhu.ews_id='7'
			LEFT JOIN ews_master_skor MStinggi_badan ON H.tinggi_badan BETWEEN MStinggi_badan.nilai_1 AND MStinggi_badan.nilai_2 AND  MStinggi_badan.staktif='1' AND MStinggi_badan.ews_id='8'
			LEFT JOIN ews_master_skor MSberat_badan ON H.berat_badan BETWEEN MSberat_badan.nilai_1 AND MSberat_badan.nilai_2 AND  MSberat_badan.staktif='1' AND MSberat_badan.ews_id='9'
			LEFT JOIN ews_master_skor MSSO ON MSSO.nilai_1=H.suplemen_oksigen AND MSSO.staktif='1' AND MSSO.ews_id='10'
			LEFT JOIN merm_referensi SO ON SO.nilai=H.suplemen_oksigen AND SO.ref_head_id='421'
			
			) T
	  ";
	  $data=$this->db->query($q)->row_array();
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function list_history_pengkajian_ews()
    {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_ews($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idrawat_ranap=$this->input->post('idrawat_ranap');
		$idtipe=$this->input->post('idtipe');
		$idruang=$this->input->post('idruang');
		$idbed=$this->input->post('idbed');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$st_ranap=$this->input->post('st_ranap');
		$where='';
		
		if ($idrawat_ranap != '#'){
			if ($st_ranap=='1'){
			$where .=" AND H.pendaftaran_id_ranap = '".$idrawat_ranap."'";
				
			}else{
			$where .=" AND H.pendaftaran_id = '".$idrawat_ranap."'";
				
			}
		}
		if ($idruang != '#'){
			$where .=" AND MP.idruangan = '".$idruang."'";
		}
		if ($idkelas != '#'){
			$where .=" AND MP.idkelas = '".$idkelas."'";
		}
		if ($idbed != '#'){
			$where .=" AND MP.idbed = '".$idbed."'";
		}
		if ($notransaksi != ''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter != '#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		
		if ($mppa_id != '#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1  != ''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1  != ''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_ews")->row_array();
		// print_r($akses_general);exit;
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited,H.assesmen_id
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE RJ.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE RJ.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MP.idtipe ELSE RJ.idtipe END as idtipe
					,CASE WHEN MP.idtipe = 1 THEN 'Rawat Inap' ELSE 'ODS' END as idtipe_nama
					
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.pendaftaran_id,H.pendaftaran_id_ranap,H.tanggal_input as tanggal_pengkajian
					,MPOL.nama as nama_poli,H.st_ranap
					FROM `tpoliklinik_ews` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran RJ ON RJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPOL ON RJ.idpoliklinik=MPOL.id 
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.tipe_input='3' AND H.status_assemen='2' ".$where."
					
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_edit='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/input_ews"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></a>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen_all('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ews/input_ews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
		$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id.'/0/erm_ews/input_ews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id != $r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id != $r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_ews']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_ews']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			// $aksi_edit='<a href="'.base_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_ews/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->nopendaftaran);
		if ($r->st_ranap=='1'){
			$result[] = ('<strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:''));
		}else{
			$result[] = ($idtipe=='1'?'Poliklinik':'IGD').' - '.'<strong>'.$r->nama_poli.'</strong>';
		}
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa);
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
		}
		
		function save_edit_ews(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_edit_id=$this->input->post('alasan_id');
			$keterangan_edit=$this->input->post('keterangan_edit');
			$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_ews WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			
				
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_ews',$data);
			$data=$this->db->query("SELECT * FROM tpoliklinik_ews WHERE assesmen_id='$assesmen_id'")->row_array();
		
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		
			$this->output->set_output(json_encode($hasil));
		}
		// function save_edit_ews(){
			// $assesmen_id=$this->input->post('assesmen_id');
			// $alasan_edit_id=$this->input->post('alasan_id');
			// $keterangan_edit=$this->input->post('keterangan_edit');
			// $jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_ews WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// // $jml_edit=$this->input->post('jml_edit');
			// $res=$this->simpan_history_ews($assesmen_id,$jml_edit);
			// // print_r($res);exit;
			// if ($res){
				
				// $data=array(
					// 'status_assemen' => 1,
					// 'st_edited' => 1,
					// 'alasan_edit_id' =>$alasan_edit_id,
					// 'keterangan_edit' =>$keterangan_edit,

				// );
				// $login_ppa_id=$this->session->userdata('login_ppa_id');
				// $data['edited_ppa']=$login_ppa_id;
				// $data['edited_date']=date('Y-m-d H:i:s');
				// $this->db->where('assesmen_id',$assesmen_id);
				// $result=$this->db->update('tpoliklinik_ews',$data);
				// $data=$this->db->query("SELECT * FROM tpoliklinik_ews WHERE assesmen_id='$assesmen_id'")->row_array();
			
				// if ($result){
					// $hasil=$data;
				// }else{
					// $hasil=false;
				// }
			// }else{
				// $hasil=$res;
			// }
			// $this->output->set_output(json_encode($hasil));
		// }
		function simpan_history_ews($assesmen_id,$jml_edit){
			$hasil=false;
			// $jml_edit=$jml_edit+1;
			$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_ews_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
			// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
			if ($baris==0){
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="INSERT IGNORE INTO tpoliklinik_ews_his 
							(versi_edit,
							assesmen_id,template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date)
					SELECT '$jml_edit',
					'$assesmen_id',template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date 
					FROM tpoliklinik_ews 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
			  
			}
		  return $hasil;
		}
		
		function batal_ews(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $st_edited=$this->input->post('st_edited');
		  $jml_edit=$this->input->post('jml_edit');
		  // $data=array(
			// 'deleted_ppa' => $login_ppa_id,
			// 'deleted_date' => date('Y-m-d H:i:s'),
			
		  // );
		   if ($jml_edit=='0'){
			  $data['edited_ppa']=null;
			  $data['edited_date']=null;
			  $data['alasan_edit_id']='';
			  $data['keterangan_edit']='';
			  
		  }
		  if ($st_edited=='1'){
			  $data['status_assemen']='2';
		  }else{
			  $data['status_assemen']='0';
			  
		  }
		  $this->db->where('assesmen_id',$assesmen_id);
		  $hasil=$this->db->update('tpoliklinik_ews',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
	  // function batal_ews(){
		  // $login_ppa_id=$this->session->userdata('login_ppa_id');
		  // $pendaftaran_id=$this->input->post('pendaftaran_id');
		  // $assesmen_id=$this->input->post('assesmen_id');
		  // $st_edited=$this->input->post('st_edited');
		  // $jml_edit=$this->input->post('jml_edit');
		  // $data=array(
			// 'deleted_ppa' => $login_ppa_id,
			// 'deleted_date' => date('Y-m-d H:i:s'),
			
		  // );
		   // if ($jml_edit=='0'){
			  // $data['edited_ppa']=null;
			  // $data['edited_date']=null;
			  // $data['alasan_edit_id']='';
			  // $data['keterangan_edit']='';
			  
		  // }
		  // if ($st_edited=='1'){
				// $q="SELECT * FROM tpoliklinik_ews_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				// $data_his_edit=$this->db->query($q)->row();
				// if ($data_his_edit){
					// $data['edited_ppa']=$data_his_edit->edited_ppa;
					// $data['edited_date']=$data_his_edit->edited_date;
					// $data['alasan_edit_id']=$data_his_edit->alasan_edit_id;
					// $data['keterangan_edit']=$data_his_edit->keterangan_edit;
					// $data['tanggal_input']=$data_his_edit->tanggal_input;
					// $data['subjectif']=$data_his_edit->subjectif;
					// $data['objectif']=$data_his_edit->objectif;
					// $data['assemen']=$data_his_edit->assemen;
					// $data['planing']=$data_his_edit->planing;
					// $data['intruksi']=$data_his_edit->intruksi;
					// $data['intervensi']=$data_his_edit->intervensi;
					// $data['evaluasi']=$data_his_edit->evaluasi;
					// $data['reassesmen']=$data_his_edit->reassesmen;

				// }
				
				
				// $q="DELETE FROM tpoliklinik_ews_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				// $this->db->query($q);
				
				
			  // $data['status_assemen']='2';
		  // }else{
			  // $data['status_assemen']='0';
			  
		  // }
		  // $this->db->where('assesmen_id',$assesmen_id);
		  // $hasil=$this->db->update('tpoliklinik_ews',$data);
		  // $this->output->set_output(json_encode($hasil));
	  // }
  function create_with_template_ews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_ews (
			template_assesmen_id,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idpasien,created_date,created_ppa,status_assemen,st_edited,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,suplemen_oksigen,total_skor_ews,hasil_ews,warna_ews,tipe_input
			)
			SELECT 
			template_assesmen_id,NOW(),pendaftaran_id,'$pendaftaran_id_ranap','$st_ranap','$idpasien',NOW(),'$login_ppa_id',1,0,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,suplemen_oksigen,total_skor_ews,hasil_ews,warna_ews,tipe_input
			
			FROM `tpoliklinik_ews`

			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	 
	  $q="UPDATE tpoliklinik_ews 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
   }
   function hapus_record_ews(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ews',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function list_index_template_ews()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,M.nama as nama_mppa
						 FROM tpoliklinik_ews H 
						 LEFT JOIN mppa M ON M.id=H.created_ppa
						 WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.assesmen_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		// $result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->assesmen_id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		
		$result[] =$no;
		$result[] =$aksi;
		$result[] =$r->nama_template;
		$result[] =$r->jumlah_template;
			$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }	
  function create_template_ews(){
	   $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $tipe_input=3;
	  $st_ranap=$this->input->post('st_ranap');
		
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'tipe_input' => $tipe_input,
		'st_ranap' => $st_ranap,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  if ($tipe_input=='1'){
		  $data['ppa_menerima']=$login_ppa_id;
		  $data['ppa_pemberi']=$login_ppa_id;
	  }
	  $hasil=$this->db->insert('tpoliklinik_ews',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	function edit_template_ews(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ews',$data);
		
		$this->output->set_output(json_encode($result));
  }
  function load_ews(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $list_tk=list_variable_ref(23);
	  $list_so=list_variable_ref(421);
	  $q="
		SELECT *FROM (
			SELECT H.*
			,TK.ref as nama_tk
			,MSTK.skor as skor_tk,MSTK.warna as warna_tk
			,MSnadi.skor as skor_nadi,MSnadi.warna as warna_nadi
			,MSnafas.skor as skor_nafas,MSnafas.warna as warna_nafas
			,MSspo2.skor as skor_spo2,MSspo2.warna as warna_spo2
			,MStd_sistole.skor as skor_td_sistole,MStd_sistole.warna as warna_td_sistole
			,MStd_diastole.skor as skor_td_diastole,MStd_diastole.warna as warna_td_diastole
			,MSsuhu.skor as skor_suhu,MSsuhu.warna as warna_suhu
			,MStinggi_badan.skor as skor_tinggi_badan,MStinggi_badan.warna as warna_tinggi_badan
			,MSberat_badan.skor as skor_berat_badan,MSberat_badan.warna as warna_berat_badan
			,MSSO.skor as skor_so,MSSO.warna as warna_so,SO.ref as nama_so

			FROM (
				SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.assesmen_id,H.tanggal_input,H.pendaftaran_id
						,null as pendaftaran_id_ranap,H.tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.tinggi_badan,H.berat_badan,H.suplemen_oksigen
						,H.st_ranap
						,jml_edit,DR.hari,mhari.nama_hari,MP.nama as nama_created,H.hasil_ews,H.warna_ews
 
						from tpoliklinik_ews H
						INNER JOIN mppa MP ON MP.id=H.created_ppa
						LEFT JOIN date_row DR ON DR.tanggal=H.tanggal_input
						LEFT JOIN mhari ON mhari.id=DR.hari
						WHERE assesmen_id='$assesmen_id'
			) H
			LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
			LEFT JOIN ews_master_skor MSTK ON MSTK.nilai_1=H.tingkat_kesadaran AND MSTK.staktif='1' AND MSTK.ews_id='1'
			LEFT JOIN ews_master_skor MSnadi ON H.nadi BETWEEN MSnadi.nilai_1 AND MSnadi.nilai_2 AND  MSnadi.staktif='1' AND MSnadi.ews_id='2'
			LEFT JOIN ews_master_skor MSnafas ON H.nafas BETWEEN MSnafas.nilai_1 AND MSnafas.nilai_2 AND  MSnafas.staktif='1' AND MSnafas.ews_id='3'
			LEFT JOIN ews_master_skor MSspo2 ON H.spo2 BETWEEN MSspo2.nilai_1 AND MSspo2.nilai_2 AND  MSspo2.staktif='1' AND MSspo2.ews_id='4'
			LEFT JOIN ews_master_skor MStd_sistole ON H.td_sistole BETWEEN MStd_sistole.nilai_1 AND MStd_sistole.nilai_2 AND  MStd_sistole.staktif='1' AND MStd_sistole.ews_id='5'
			LEFT JOIN ews_master_skor MStd_diastole ON H.td_diastole BETWEEN MStd_diastole.nilai_1 AND MStd_diastole.nilai_2 AND  MStd_diastole.staktif='1' AND MStd_diastole.ews_id='6'
			LEFT JOIN ews_master_skor MSsuhu ON H.suhu BETWEEN MSsuhu.nilai_1 AND MSsuhu.nilai_2 AND  MSsuhu.staktif='1' AND MSsuhu.ews_id='7'
			LEFT JOIN ews_master_skor MStinggi_badan ON H.tinggi_badan BETWEEN MStinggi_badan.nilai_1 AND MStinggi_badan.nilai_2 AND  MStinggi_badan.staktif='1' AND MStinggi_badan.ews_id='8'
			LEFT JOIN ews_master_skor MSberat_badan ON H.berat_badan BETWEEN MSberat_badan.nilai_1 AND MSberat_badan.nilai_2 AND  MSberat_badan.staktif='1' AND MSberat_badan.ews_id='9'
			LEFT JOIN ews_master_skor MSSO ON MSSO.nilai_1=H.suplemen_oksigen AND MSSO.staktif='1' AND MSSO.ews_id='10'
			LEFT JOIN merm_referensi SO ON SO.nilai=H.suplemen_oksigen AND SO.ref_head_id='421'
			
			) T
	  ";
	  $row=$this->db->query($q)->row();
	   $q="SELECT *FROM ews_master WHERE staktif='1' ORDER BY nourut ASC";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  $total=0;
	  $total_ews=0;
	  $warna_ews='';
	  $hasil_ews='';
	  foreach($hasil as $r){
		  $no=$no+1;
		  
		  $hasil=$this->jawaban_ews_detail($r,$row,$list_tk,$list_so);
		  $total_ews=$total_ews+$hasil['hasil_skor'];
		 $opsi.='<tr>
					<td>'.$no.'</td>
					<td>'.$r->nama_ews.'<input class="ews_id" type="hidden"  value="'.$r->id.'" ></td>
					<td>'.$hasil['hasil_text'].'</td>
					<td class="text-center"><input class="form-control number param_skor param_id_'.$r->id.'" readonly style="height: 28px;" type="text" value="'.$hasil['hasil_skor'].'"></td>
					</td>
					
				</tr>';
				// $total=$total + $r->nilai;
	  }
	  
	  $opsi.='<tr><td colspan="3" class="text-right text-primary"><strong>TOTAL SKOR EWS</strong></td><td><input class="form-control number param_total" readonly style="height: 28px;" type="text" value="'.$total_ews.'"></td></tr>';
	  $opsi.='<tr><td colspan="3" class="text-right text-primary"><strong>TINDAK LANJUT</strong></td><td><label style="color:'.$row->warna_ews.'" class="param_tindak_lanjut label-nama">'.$row->hasil_ews.'</label></td></tr>';
	  // $opsi.='<td class="text-center"><strong><span class="label_total_skor">'.$total.'</span></strong></td></tr>';
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function jawaban_ews_detail($r,$row,$list_tk,$list_so){
	  $jenis_param=$r->id;
	   $data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,mpupil_satuan,mspo2_satuan")->row_array();
	  $hasil_text='';
	  $hasil_skor='0';
	  $hasil_warna='';
	  if ($jenis_param=='1'){//
			$selected_id=$row->tingkat_kesadaran;
			$hasil_text .='<select style="background-color:#d0f3df;" class="js-select2 form-control value_tingkat_kesadaran opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
			$hasil_text .='<option value="" '.($selected_id==''?'selected':'').'>-Belum dipilih-</option>';
			foreach($list_tk as $tk){
				$hasil_text .='<option value="'.$tk->id.'" '.($tk->id==$selected_id?'selected':'').'>'.$tk->nama.'</option>';
			}
				
			$hasil_text .='</select>';
			$hasil_skor=$row->skor_tk;
			$hasil_warna=$row->warna_tk;
	  }
	  if ($jenis_param=='10'){//
			$selected_id=$row->suplemen_oksigen;
			$hasil_text .='<select style="background-color:#d0f3df;" class="js-select2 form-control value_suplemen_oksigen opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
			$hasil_text .='<option value="" '.($selected_id==''?'selected':'').'>-Belum dipilih-</option>';
			foreach($list_so as $tk){
				$hasil_text .='<option value="'.$tk->id.'" '.($tk->id==$selected_id?'selected':'').'>'.$tk->nama.'</option>';
			}
				
			$hasil_text .='</select>';
			$hasil_skor=$row->skor_so;
			$hasil_warna=$row->warna_so;
	  }
	  if ($jenis_param=='2'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_nadi" style="height: 28px;" type="text" value="'.$row->nadi.'">
					<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_nadi'].'</span>
				</div>';
		$hasil_skor=$row->skor_nadi;
		$hasil_warna=$row->warna_nadi;
	 }
	 if ($jenis_param=='3'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_nafas" style="height: 28px;" type="text" value="'.$row->nafas.'">
					<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_nafas'].'</span>
				</div>';
		$hasil_skor=$row->skor_nafas;
		$hasil_warna=$row->warna_nafas;
	 }
	 if ($jenis_param=='4'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_spo2" style="height: 28px;" type="text" value="'.$row->spo2.'">
					<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_spo2'].'</span>
				</div>';
		$hasil_skor=$row->skor_spo2;
		$hasil_warna=$row->warna_spo2;
	 }
	 if ($jenis_param=='5'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_td_sistole" style="height: 28px;" type="text" value="'.$row->td_sistole.'">
					<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_td'].'</span>
				</div>';
		$hasil_skor=$row->skor_td_sistole;
		$hasil_warna=$row->warna_td_sistole;
	 }
	 if ($jenis_param=='6'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_td_diastole" style="height: 28px;" type="text" value="'.$row->td_diastole.'">
					<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_td'].'</span>
				</div>';
		$hasil_skor=$row->skor_td_diastole;
		$hasil_warna=$row->warna_td_diastole;
	 }
	 if ($jenis_param=='7'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_suhu" style="height: 28px;" type="text" value="'.$row->suhu.'">
					<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_suhu'].'</span>
				</div>';
		$hasil_skor=$row->skor_suhu;
		$hasil_warna=$row->warna_suhu;
	 }
	 if ($jenis_param=='8'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_tinggi_badan" style="height: 28px;" type="text" value="'.$row->tinggi_badan.'">
					<span class="input-group-addon" style="height: 22px;">CM</span>
				</div>';
		$hasil_skor=$row->skor_tinggi_badan;
		$hasil_warna=$row->warna_tinggi_badan;
	 }
	  if ($jenis_param=='9'){//
		$hasil_text .='<div class="input-group">
					<input class="form-control auto_blur decimal auto_berat_badan" style="height: 28px;" type="text" value="'.$row->berat_badan.'">
					<span class="input-group-addon" style="height: 22px;">CM</span>
				</div>';
		$hasil_skor=$row->skor_berat_badan;
		$hasil_warna=$row->warna_berat_badan;
	 }
	  $hasil['hasil_text']=$hasil_text;
	  $hasil['hasil_skor']=$hasil_skor;
	  $hasil['hasil_warna']=$hasil_warna;
	  return $hasil;
  }
   function update_nilai_ews(){
	  $id=$this->input->post('risiko_nilai_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $template_id=$this->input->post('template_id');
	  $nilai_id=$this->input->post('nilai_id');
	  $data=array(
		'param_nilai_id' =>$nilai_id
	  );
	  $this->db->where('id',$id);
	  $result=$this->db->update('tranap_ews_detail',$data);
	  
	  $skor=$this->db->query("SELECT SUM(H.nilai) as skor 
		FROM `tranap_ews_detail` H WHERE H.assesmen_id='$assesmen_id'")->row('skor');
	  $q="SELECT H.ref_nilai,H.ref_nilai_id,H.st_tindakan,H.nama_tindakan 
			FROM mews_ranap_setting_nilai H
			WHERE H.mpews_id='$template_id' AND ('$skor' BETWEEN H.skor_1 AND H.skor_2) LIMIT 1";
			// print_r($q);exit;
	  $hasil=$this->db->query($q)->row();
	  
	  $data['skor']=$skor;
	  if ($hasil){
		  $data['ref_nilai_id']=$hasil->ref_nilai_id;
		  $data['ref_nilai']=$hasil->ref_nilai;
		  $data['st_tindakan']=$hasil->st_tindakan;
		  $data['nama_tindakan']=$hasil->nama_tindakan;
	  }else{
		  $data['ref_nilai_id']=0;
		  $data['ref_nilai']='';
		  $data['st_tindakan']=0;
		  $data['nama_tindakan']='';
	  }
	 
	  
	  $data_update=array(
		'total_skor_ews' =>$skor,
		'ref_nilai_id' =>$data['ref_nilai_id'],
		'nama_hasil_pengkajian' =>$data['ref_nilai'],
		'nama_tindakan' =>$data['nama_tindakan'],
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $result=$this->db->update('tranap_ews',$data_update);
	  // $data['']
	  $this->output->set_output(json_encode($data));
  }
  function load_ews_his(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
	$login_profesi_id=$this->session->userdata('login_profesi_id');

	  $logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_ews($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
	  $akses_general=$this->db->query("SELECT *FROM setting_ews")->row_array();
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $tgl_filter_1=$this->input->post('tgl_filter_1');
	  $tgl_filter_2=$this->input->post('tgl_filter_2');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $q="SELECT *FROM ews_master WHERE staktif='1' ORDER BY nourut ASC";
	  $record=$this->db->query($q)->result();
	 
	 $where='';
	 if ($tgl_filter_1){
			$where=" WHERE DATE(H.tanggal_input) >= '".YMDFormat($tgl_filter_1)."' AND DATE(H.tanggal_input) <= '".YMDFormat($tgl_filter_2)."'";
	 }
	 
	 $q="
		SELECT *FROM (
			SELECT H.*
			,TK.ref as nama_tk
			,MSTK.skor as skor_tk,MSTK.warna as warna_tk
			,MSnadi.skor as skor_nadi,MSnadi.warna as warna_nadi
			,MSnafas.skor as skor_nafas,MSnafas.warna as warna_nafas
			,MSspo2.skor as skor_spo2,MSspo2.warna as warna_spo2
			,MStd_sistole.skor as skor_td_sistole,MStd_sistole.warna as warna_td_sistole
			,MStd_diastole.skor as skor_td_diastole,MStd_diastole.warna as warna_td_diastole
			,MSsuhu.skor as skor_suhu,MSsuhu.warna as warna_suhu
			,MStinggi_badan.skor as skor_tinggi_badan,MStinggi_badan.warna as warna_tinggi_badan
			,MSberat_badan.skor as skor_berat_badan,MSberat_badan.warna as warna_berat_badan
			,MSSO.skor as skor_so,MSSO.warna as warna_so,SO.ref as nama_so

			FROM (
				
						SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.assesmen_id,H.tanggal_input,H.pendaftaran_id
						,H.pendaftaran_id_ranap,H.tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.tinggi_badan,H.berat_badan,H.suplemen_oksigen
						,H.st_ranap,H.tipe_input
						,jml_edit,DR.hari,mhari.nama_hari,MP.nama as nama_created
						,H.hasil_ews,H.warna_ews,H.total_skor_ews

						from tpoliklinik_ews H
						INNER JOIN mppa MP ON MP.id=H.created_ppa
						LEFT JOIN date_row DR ON DR.tanggal=H.tanggal_input
						LEFT JOIN mhari ON mhari.id=DR.hari
						WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.st_ranap='1'
						
						UNION ALL
						
						SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.assesmen_id,H.tanggal_input,H.pendaftaran_id
						,H.pendaftaran_id_ranap,H.tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.tinggi_badan,H.berat_badan,H.suplemen_oksigen
						,H.st_ranap,H.tipe_input
						,jml_edit,DR.hari,mhari.nama_hari,MP.nama as nama_created 
						,H.hasil_ews,H.warna_ews,H.total_skor_ews
						from tpoliklinik_ews H
						INNER JOIN mppa MP ON MP.id=H.created_ppa
						LEFT JOIN date_row DR ON DR.tanggal=H.tanggal_input
						LEFT JOIN mhari ON mhari.id=DR.hari
						WHERE pendaftaran_id='$pendaftaran_id' AND H.st_ranap='0'
						
			) H
			LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
			LEFT JOIN ews_master_skor MSTK ON MSTK.nilai_1=H.tingkat_kesadaran AND MSTK.staktif='1' AND MSTK.ews_id='1'
			LEFT JOIN ews_master_skor MSnadi ON H.nadi BETWEEN MSnadi.nilai_1 AND MSnadi.nilai_2 AND  MSnadi.staktif='1' AND MSnadi.ews_id='2'
			LEFT JOIN ews_master_skor MSnafas ON H.nafas BETWEEN MSnafas.nilai_1 AND MSnafas.nilai_2 AND  MSnafas.staktif='1' AND MSnafas.ews_id='3'
			LEFT JOIN ews_master_skor MSspo2 ON H.spo2 BETWEEN MSspo2.nilai_1 AND MSspo2.nilai_2 AND  MSspo2.staktif='1' AND MSspo2.ews_id='4'
			LEFT JOIN ews_master_skor MStd_sistole ON H.td_sistole BETWEEN MStd_sistole.nilai_1 AND MStd_sistole.nilai_2 AND  MStd_sistole.staktif='1' AND MStd_sistole.ews_id='5'
			LEFT JOIN ews_master_skor MStd_diastole ON H.td_diastole BETWEEN MStd_diastole.nilai_1 AND MStd_diastole.nilai_2 AND  MStd_diastole.staktif='1' AND MStd_diastole.ews_id='6'
			LEFT JOIN ews_master_skor MSsuhu ON H.suhu BETWEEN MSsuhu.nilai_1 AND MSsuhu.nilai_2 AND  MSsuhu.staktif='1' AND MSsuhu.ews_id='7'
			LEFT JOIN ews_master_skor MStinggi_badan ON H.tinggi_badan BETWEEN MStinggi_badan.nilai_1 AND MStinggi_badan.nilai_2 AND  MStinggi_badan.staktif='1' AND MStinggi_badan.ews_id='8'
			LEFT JOIN ews_master_skor MSberat_badan ON H.berat_badan BETWEEN MSberat_badan.nilai_1 AND MSberat_badan.nilai_2 AND  MSberat_badan.staktif='1' AND MSberat_badan.ews_id='9'
			LEFT JOIN ews_master_skor MSSO ON MSSO.nilai_1=H.suplemen_oksigen AND MSSO.staktif='1' AND MSSO.ews_id='10'
			LEFT JOIN merm_referensi SO ON SO.nilai=H.suplemen_oksigen AND SO.ref_head_id='421'
			".$where."
		) T 
		
		ORDER BY T.tanggal_input ASC
	 ";
	  // print_r($q);exit;
	 $list_param=$this->db->query($q)->result();
	 
	 $record_detail=$this->db->query($q)->result_array();
	 $data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,mpupil_satuan,mspo2_satuan")->row_array();
	 if ($list_param){
		 $tabel='<table class="table table table-bordered table-condensed" id="index_his">';
		  $tabel .='<thead>';
		  $header='<tr>';
		  $header .='<th class="text-center" style="width:5%;vertical-align: middle;"><strong>NO</strong></th>';
		  $header .='<th class="text-center" style="width:250px;vertical-align: middle;"><strong>PARAMETER FISIOLOGIS</strong></th>';
		  $jml_kolom=0;
		  foreach($list_param as $r){
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
				$result = array();
				if ($r->tipe_input=='3'){
					
					$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
					$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
					$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
					if ($r->st_ranap=='1'){
						$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$pendaftaran_id_ranap.'/1/erm_ews/input_ews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
						
					}else{
						$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$pendaftaran_id.'/0/erm_ews/input_ews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
						
					}
				}else{
					$btn_edit='';
					$btn_duplikasi='';
					$btn_hapus='';
					if ($r->st_ranap=='1'){
					$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/input_ttv_ri"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
						
					}else{
					$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_ttv"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
						
					}
				}
		
				 if ($akses_general['st_edit_catatan']=='0'){
					  $btn_edit='';
				  }else{
					  if ($akses_general['lama_edit']>0){
						  if ($r->selisih>$akses_general['lama_edit']){
							  $btn_edit='';
						  }else{
							  
						  }
						  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
							  if ($login_ppa_id != $r->created_ppa){
									$btn_edit='';
							  }
						  }
					  }
				  }
				  if ($akses_general['st_duplikasi_catatan']=='0'){
					  $btn_duplikasi='';
				  }else{
					  if ($akses_general['lama_duplikasi']>0){
						  if ($r->selisih>$akses_general['lama_duplikasi']){
							  $btn_duplikasi='';
						  }else{
							 
						  }
					  }
					   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id != $r->created_ppa){
								$btn_duplikasi='';
						  }
					   }
				  }
				  if ($akses_general['st_hapus_catatan']=='0'){
					  $btn_hapus='';
				  }else{
					  if ($akses_general['lama_hapus']>0){
						  if ($r->selisih>$akses_general['lama_hapus']){
							  $btn_hapus='';
						  }else{
							 
						  }
						   
					  }
					  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id <> $r->created_ppa){
								$btn_hapus='';
						  }else{
							  
						  }
								// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
					  }
				  }
				if ($logic_akses_assesmen['st_edit_ews']=='0'){
				  $btn_edit='';
				}
				if ($logic_akses_assesmen['st_hapus_ews']=='0'){
				  $btn_hapus='';
				}
				$aksi_edit='';
				if ($r->jml_edit>0){
					if ($r->tipe_input=='3'){
					$aksi_edit='<a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_ews/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit "><i class="fa fa-info"></a>';
					}else{
						
					$aksi_edit='';
					}
				}
				$aksi='';
				
				
				$aksi = '<div class="btn-group">';
				$aksi .= $btn_edit;	
				$aksi .= $btn_duplikasi;	
				$aksi .= $btn_hapus;	
				$aksi .= $btn_lihat;	
				$aksi .= $aksi_edit;	
				$aksi .= '</div>';
		
				$header .='<th class="text-center" style="width:150px;vertical-align: middle;"><strong>';
				$header .='<span class="text-center">'.$r->nama_hari.' '.HumanDateShort($r->tanggal_input).'</span></strong> '.$this->tipe_pengkajian($r->tipe_input);
				$header .='<br><span class="text-center text-primary text-nama">'.$r->nama_created.'<br>'.HumanDateLong($r->tanggal_input).'</span>';
				$header .='<br>'.$aksi.'</span>';
				$header .='</th>';
				$jml_kolom =$jml_kolom+1;
				
		  }
		  $tabel .=$header;
		  $tabel .='</tr>';
		  $tabel .='</thead>';
		  $tabel .='<tbody>';
		  $no=1;
		  foreach($record as $r){
				// $param_id=$r->param_id;
				$tabel .='<tr>';
				$tabel .='<td>'.$no.'</td>';
				$tabel .='<td>'.$r->nama_ews.'</td>';
				foreach($list_param as $row){
					$tabel .='
						<td class="text-center">
							'.$this->jawaban_ews_detail_read_only($r,$row,$record_detail,$data_satuan_ttv).'
						</td>
					';
				}
				
				
				$no++;
		  }
			$tabel .='</tr>';
			$tabel .='<tr>';
			$tabel .='<td></td>';
			$tabel .='<td class="text-center" ><strong>TOTAL SKOR</strong></td>';
			foreach($list_param as $row){
			$tabel .='<td class="text-center"><strong><label style="color:'.$row->warna_ews.';"  class="label-total">'.$row->total_skor_ews.'</label></strong></td>';
			}
			$tabel .='</tr>';
			
			$tabel .='<tr>';
			$tabel .='<td></td>';
			$tabel .='<td class="text-center" ><strong>HASIL EWS</strong></td>';
			foreach($list_param as $row){
			$tabel .='<td class="text-center"><strong><label style="color:'.$row->warna_ews.';"  class="label-nama">'.$row->hasil_ews.'</label></strong></td>';
			}
			$tabel .='</tr>';
			
		  $tabel.='</tbody>';
		  $tabel.='</table>';
	 }else{
		 $tabel='
			<div class="form-group">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p>Info <a class="alert-link" href="javascript:void(0)"> Belum ada Penilaian, Klik New Untuk Memulai Penilaian</a>!</p>
						</div>
					</div>
					
				</div>
			</div>
		 ';
		 
	 }
	 
	$arr['tabel']=$tabel;
	$this->output->set_output(json_encode($arr));
	
  }
  function tipe_pengkajian($id){
	  $hasil='';
	  if ($id=='1'){
		  $hasil=' '.text_primary('TTV Rawat Jalan');
	  }
	  if ($id=='2'){
		  $hasil=' '.text_success('TTV Rawat Inap');
	  }
	  if ($id=='3'){
		  $hasil=''.text_default('EWS');
	  }
	  return '<br>'. $hasil;
  }
  function jawaban_ews_detail_read_only($row,$row_header,$record_detail,$data_satuan_ttv){
	  $jenis_param=$row->id;
	  $hasil='';
	  $assesmen_id=$row_header->assesmen_id;
	  $st_ranap=$row_header->st_ranap;
	  $data_pilih = array_filter($record_detail, function($var) use ($assesmen_id,$st_ranap) { 
		return ($var['assesmen_id'] == $assesmen_id && $var['st_ranap'] == $st_ranap);
	  });
	$data_pilih=reset($data_pilih);
	$warna='';$skor='0';
	if ($data_pilih){
		 if ($jenis_param=='1'){//
			$hasil .='<label class="label-nama" style="color:'.$data_pilih['warna_tk'].';">'.$data_pilih['nama_tk'].'</label>';
			 $warna=$data_pilih['warna_tk'];
			 $skor=$data_pilih['skor_tk'];
		 }
		 if ($jenis_param=='2'){//
			$hasil .='<div class="input-group">
						<input class="form-control  decimal" readonly style="color:'.$data_pilih['warna_nadi'].';height: 28px;" type="text" value="'.$data_pilih['nadi'].'">
						<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_nadi'].'</span>
					</div>';
			$warna=$data_pilih['warna_nadi'];
			 $skor=$data_pilih['skor_nadi'];
		 }
		if ($jenis_param=='3'){//
			$hasil .='<div class="input-group">
						<input class="form-control  decimal" readonly style="color:'.$data_pilih['warna_nafas'].';height: 28px;" type="text" value="'.$data_pilih['nafas'].'">
						<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_nafas'].'</span>
					</div>';
			$warna=$data_pilih['warna_nafas'];
			 $skor=$data_pilih['skor_nafas'];
		 }
		 if ($jenis_param=='4'){//
			$hasil .='<div class="input-group">
						<input class="form-control readonly decimal" readonly style="color:'.$data_pilih['warna_spo2'].';height: 28px;" type="text" value="'.$data_pilih['spo2'].'">
						<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_spo2'].'</span>
					</div>';
			$warna=$data_pilih['warna_spo2'];
			 $skor=$data_pilih['skor_spo2'];
		 }
		 if ($jenis_param=='5'){//
			$hasil .='<div class="input-group">
						<input class="form-control readonly decimal" readonly style="color:'.$data_pilih['warna_td_sistole'].';height: 28px;" type="text" value="'.$data_pilih['td_sistole'].'">
						<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_td'].'</span>
					</div>';
			$warna=$data_pilih['warna_td_sistole'];
			 $skor=$data_pilih['skor_td_sistole'];
		 }
		 if ($jenis_param=='6'){//
			$hasil .='<div class="input-group">
						<input class="form-control readonly decimal" readonly style="color:'.$data_pilih['warna_td_diastole'].';height: 28px;" type="text" value="'.$data_pilih['td_diastole'].'">
						<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_td'].'</span>
					</div>';
			$warna=$data_pilih['warna_td_diastole'];
			 $skor=$data_pilih['skor_td_diastole'];
		 }
		 if ($jenis_param=='7'){//
			$hasil .='<div class="input-group">
						<input class="form-control readonly decimal" readonly style="color:'.$data_pilih['warna_suhu'].';height: 28px;" type="text" value="'.$data_pilih['suhu'].'">
						<span class="input-group-addon" style="height: 22px;">'.$data_satuan_ttv['satuan_suhu'].'</span>
					</div>';
			$warna=$data_pilih['warna_suhu'];
			 $skor=$data_pilih['skor_suhu'];
		 }
		 if ($jenis_param=='8'){//
			$hasil .='<div class="input-group">
						<input class="form-control readonly decimal" readonly style="color:'.$data_pilih['warna_tinggi_badan'].';height: 28px;" type="text" value="'.$data_pilih['tinggi_badan'].'">
						<span class="input-group-addon" style="height: 22px;">CM</span>
					</div>';
			$warna=$data_pilih['warna_tinggi_badan'];
			 $skor=$data_pilih['skor_tinggi_badan'];
		 }
		 if ($jenis_param=='9'){//
			$hasil .='<div class="input-group">
						<input class="form-control readonly decimal" readonly style="color:'.$data_pilih['warna_berat_badan'].';height: 28px;" type="text" value="'.$data_pilih['berat_badan'].'">
						<span class="input-group-addon" style="height: 22px;">CM</span>
					</div>';
			$warna=$data_pilih['warna_berat_badan'];
			 $skor=$data_pilih['skor_berat_badan'];
		 }
		 if ($jenis_param=='10'){//
			$hasil .='<label class="label-nama" style="color:'.$data_pilih['warna_so'].';">'.$data_pilih['nama_so'].'</label>';
			$warna=$data_pilih['warna_so'];
			$skor=$data_pilih['skor_so'];
		 }
		 // if ($jenis_param!='1' || $jenis_param!='10'){
		 $hasil .='<div class="clear" style="margin-top:5px;"><input class="form-control readonly number" readonly style="color:'.$warna.';height: 28px;width:100%" type="text" value="'.$skor.'"></div>';
			 
		 // }
	}else{
		$hasil='';
	}
	  
	 
	  
	  return $hasil;
  }
  function create_pews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $mpews_id=$this->input->post('mpews_id');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
	 
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'st_ranap' => $st_ranap,
		'mpews_id' => $mpews_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'tanggal_pengkajian' => date('Y-m-d'),
		'status_assemen' => 1,
		
	  );
	  
	  $hasil=$this->db->insert('tranap_pews',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="
		INSERT INTO tranap_pews_detail (assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT '$assesmen_id',H.id as param_id, H.parameter_nama,GROUP_CONCAT(M.id) as group_nilai,null as param_nilai_id,0 as nilai 
			FROM `mpews_param` H
			LEFT JOIN mpews_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
			WHERE H.mpews_id='$mpews_id' AND H.staktif='1'
			GROUP BY H.id
	  ";
	  $hasil=$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
  function load_pews_ri(){
		$mpews_id=$this->input->post('mpews_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $q="SELECT warna_hasil FROM `tranap_pews` H WHERE H.assesmen_id='$assesmen_id'";
	  $warna_hasil=$this->db->query($q)->row('warna_hasil');
	  $q="SELECT * FROM `tranap_pews_detail` H WHERE H.assesmen_id='$assesmen_id'";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  $total=0;
	  foreach($hasil as $r){
		  $no=$no+1;
		 $opsi.='<tr>
					<td>'.$r->parameter_nama.'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td><select tabindex="8" class="form-control nilai " style="width: 100%;" data-placeholder="Pilih Opsi" required>
						'.$this->opsi_nilai_rj($r->group_nilai,$r->param_nilai_id).'
					</select></td>
					<td class="text-center">
					
					<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button">'.$r->nilai.'</button>
					</td>
					</td>
					
				</tr>';
				$total=$total + $r->nilai;
	  }
	  $btn_total='<button class="btn btn-block btn-sm text-white" style="background-color: '.$warna_hasil.'" type="button">'.$total.'</button>';
	  $opsi.='<tr><td colspan="2" class="text-right"><strong>TOTAL</strong></td>';
	  $opsi.='<td class="text-center"><strong><span class="label_total_skor">'.$btn_total.'</span></strong></td></tr>';
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function opsi_nilai_rj($param,$param_nilai_id){
	  $q="SELECT * FROM `mpews_param_skor` H WHERE H.id IN ($param)";
	  $hasil='';
	  $row=$this->db->query($q)->result();
		  $hasil .='<option data-nilai="0" value="" '.($param_nilai_id==''?'selected':'').'>-Opsi-</option>';
	  foreach($row as $r){
		  $hasil .='<option data-nilai="'.$r->skor.'" data-warna="'.$r->warna.'" value="'.$r->id.'" '.($param_nilai_id==$r->id?'selected':'').'>'.$r->deskripsi_nama.'</option>';
	  }
	  return $hasil;
  }
   function update_nilai_rj(){
	  $id=$this->input->post('risiko_nilai_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $template_id=$this->input->post('template_id');
	  $nilai_id=$this->input->post('nilai_id');
	  $data=array(
		'param_nilai_id' =>$nilai_id
	  );
	  $this->db->where('id',$id);
	  $result=$this->db->update('tranap_pews_detail',$data);
	  
	  $skor=$this->db->query("SELECT SUM(H.nilai) as skor 
		FROM `tranap_pews_detail` H WHERE H.assesmen_id='$assesmen_id'")->row('skor');
	  $q="SELECT H.ref_nilai,H.ref_nilai_id,H.st_tindakan,H.nama_tindakan,H.warna 
			FROM mpews_setting_nilai H
			WHERE H.mpews_id='$template_id' AND ('$skor' BETWEEN H.skor_1 AND H.skor_2) LIMIT 1";
			// print_r($q);exit;
	  $hasil=$this->db->query($q)->row();
	  
	  $data['skor']=$skor;
	  if ($hasil){
		  $data['ref_nilai_id']=$hasil->ref_nilai_id;
		  $data['ref_nilai']=$hasil->ref_nilai;
		  $data['st_tindakan']=$hasil->st_tindakan;
		  $data['nama_tindakan']=$hasil->nama_tindakan;
		  $data['warna_hasil']=$hasil->warna;
	  }else{
		  $data['ref_nilai_id']=0;
		  $data['ref_nilai']='';
		  $data['st_tindakan']=0;
		  $data['nama_tindakan']='';
		  $data['warna_hasil']='#000000';
	  }
	 
	  
	  $data_update=array(
		'total_skor_pews' =>$skor,
		'ref_nilai_id' =>$data['ref_nilai_id'],
		'nama_hasil_pengkajian' =>$data['ref_nilai'],
		'nama_tindakan' =>$data['nama_tindakan'],
		'warna_hasil' =>$data['warna_hasil'],
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $result=$this->db->update('tranap_pews',$data_update);
	  // $data['']
	  $this->output->set_output(json_encode($data));
  }
  function save_pews(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$riwayat_penyakit=$this->input->post('riwayat_penyakit');
		
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'status_assemen' => $this->input->post('status_assemen'),
			'nama_template' => $this->input->post('nama_template'),
			'total_skor_pews' => $this->input->post('total_skor_pews'),
			'tanggal_pengkajian' => YMDFormat($this->input->post('tanggal_pengkajian')),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tranap_pews_x_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_pews',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_history_pengkajian_pews()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_pews($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idrawat_ranap=$this->input->post('idrawat_ranap');
		$idtipe=$this->input->post('idtipe');
		$idruang=$this->input->post('idruang');
		$idbed=$this->input->post('idbed');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$st_ranap=$this->input->post('st_ranap');
		$where='';
		
		if ($idrawat_ranap != '#'){
			if ($st_ranap=='1'){
				$where .=" AND H.pendaftaran_id_ranap = '".$idrawat_ranap."'";
			}else{
				$where .=" AND H.pendaftaran_id = '".$idrawat_ranap."'";
			}
		}
		if ($idruang != '#'){
			$where .=" AND MP.idruangan = '".$idruang."'";
		}
		if ($idkelas != '#'){
			$where .=" AND MP.idkelas = '".$idkelas."'";
		}
		if ($idbed != '#'){
			$where .=" AND MP.idbed = '".$idbed."'";
		}
		if ($notransaksi != ''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter != '#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		
		if ($mppa_id != '#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1  != ''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1  != ''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_pews")->row_array();
		// print_r($akses_general);exit;
		if ($st_ranap=='1'){
			$group_by=' GROUP BY H.pendaftaran_id_ranap,H.st_ranap';
		}else{
			$group_by=' GROUP BY H.pendaftaran_id,H.st_ranap';
			
		}
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited,H.assesmen_id
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE RJ.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE RJ.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MP.idtipe ELSE RJ.idtipe END as idtipe
					,CASE WHEN MP.idtipe = 1 THEN 'Rawat Inap' ELSE 'ODS' END as idtipe_nama
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.pendaftaran_id,H.pendaftaran_id_ranap,H.tanggal_input as tanggal_pengkajian
					,COUNT(H.assesmen_id) as jumlah_pengkajian,H.st_ranap
					,MPOL.nama as nama_poli
					FROM `tranap_pews` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran RJ ON RJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPOL ON RJ.idpoliklinik=MPOL.id 
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					".$group_by."
					ORDER BY H.created_date DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_edit='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ri/input_pews"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></a>';
		}else{
		$btn_edit='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id.'/0/erm_ri/input_pews"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></a>';
			
		}
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen_all('.$r->pendaftaran_id_ranap.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen_all('.$r->pendaftaran_id_ranap.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ews/input_pews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
		$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id.'/0/erm_ews/input_pews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id != $r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id != $r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_pews']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_pews']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			// $aksi_edit='<a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_pews/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		// $aksi .= $btn_duplikasi;	
		// $aksi .= $btn_hapus;	
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->nopendaftaran).' '.($r->jumlah_pengkajian>0?'<br>'.text_danger($r->jumlah_pengkajian.' Pengkajian'):'');
		if ($r->st_ranap=='1'){
			$result[] = ('<strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:''));
		}else{
			$result[] = ($idtipe=='1'?'Poliklinik':'IGD').' - '.'<strong>'.$r->nama_poli.'</strong>';
		}
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa);
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_index_template_pews()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tranap_pews H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function load_pews_his(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
	$login_profesi_id=$this->session->userdata('login_profesi_id');

	  $logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_pews($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
	  $akses_general=$this->db->query("SELECT *FROM setting_pews")->row_array();
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $st_ranap=$this->input->post('st_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $mpews_id=$this->input->post('mpews_id');
	  $where='';
	  if ($st_ranap=='1'){
		  $where =" AND (H.pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.st_ranap='1') ";
	  }else{
		  
		  $where =" AND (H.pendaftaran_id='$pendaftaran_id' AND H.st_ranap='0') ";
	  }
	  $q="SELECT H.assesmen_id,H.tanggal_input,H.tanggal_pengkajian,H.created_ppa,DR.hari,mhari.nama_hari 
			,MP.nama as nama_created,DATEDIFF(NOW(), H.tanggal_input) as selisih,H.assesmen_id,H.jml_edit,H.pendaftaran_id_ranap,H.pendaftaran_id,H.st_ranap
			,H.keterangan_edit,H.total_skor_pews,H.nama_hasil_pengkajian,H.warna_hasil
			FROM `tranap_pews` H
			LEFT JOIN date_row DR ON DR.tanggal=H.tanggal_pengkajian
			LEFT JOIN mhari ON mhari.id=DR.hari
			INNER JOIN mppa MP ON MP.id=H.created_ppa
			WHERE  H.mpews_id='$mpews_id' AND H.status_assemen='2' ".$where."
			ORDER BY H.tanggal_pengkajian,H.assesmen_id
			";
	 $record=$this->db->query($q)->result();
	 
	 $q="SELECT D.* FROM `tranap_pews` H
			INNER JOIN tranap_pews_detail D ON D.assesmen_id=H.assesmen_id
			WHERE H.pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.mpews_id='$mpews_id'
			";
	 $record_detail=$this->db->query($q)->result_array();
	 
	 $q="SELECT D.param_id,D.parameter_nama,D.group_nilai,GROUP_CONCAT(H.assesmen_id) as assesmen_id_arr
		FROM `tranap_pews` H
		INNER JOIN tranap_pews_detail D ON D.assesmen_id=H.assesmen_id
		WHERE H.mpews_id='$mpews_id' ".$where."

		GROUP BY D.param_id";
	  
	 $list_param=$this->db->query($q)->result();
	 
	 if ($record){
		 $tabel='<table class="table table table-bordered table-condensed" id="index_his">';
		  $tabel .='<thead>';
		  $header='<tr>';
		  $header .='<th class="text-center" style="width:5%;vertical-align: middle;"><strong>NO</strong></th>';
		  $header .='<th class="text-center" style="width:250px;vertical-align: middle;"><strong>PARAMETER</strong></th>';
		  $jml_kolom=0;
		  foreach($record as $r){
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
				$result = array();
				$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
				$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
				$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
				if ($r->st_ranap=='1'){
				$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ews/input_pews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					
				}else{
					
				$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id.'/0/erm_ews/input_pews/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				}
		
				 if ($akses_general['st_edit_catatan']=='0'){
					  $btn_edit='';
				  }else{
					  if ($akses_general['lama_edit']>0){
						  if ($r->selisih>$akses_general['lama_edit']){
							  $btn_edit='';
						  }else{
							  
						  }
						  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
							  if ($login_ppa_id != $r->created_ppa){
									$btn_edit='';
							  }
						  }
					  }
				  }
				  if ($akses_general['st_duplikasi_catatan']=='0'){
					  $btn_duplikasi='';
				  }else{
					  if ($akses_general['lama_duplikasi']>0){
						  if ($r->selisih>$akses_general['lama_duplikasi']){
							  $btn_duplikasi='';
						  }else{
							 
						  }
					  }
					   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id != $r->created_ppa){
								$btn_duplikasi='';
						  }
					   }
				  }
				  if ($akses_general['st_hapus_catatan']=='0'){
					  $btn_hapus='';
				  }else{
					  if ($akses_general['lama_hapus']>0){
						  if ($r->selisih>$akses_general['lama_hapus']){
							  $btn_hapus='';
						  }else{
							 
						  }
						   
					  }
					  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id <> $r->created_ppa){
								$btn_hapus='';
						  }else{
							  
						  }
								// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
					  }
				  }
				if ($logic_akses_assesmen['st_edit_pews']=='0'){
				  $btn_edit='';
				}
				if ($logic_akses_assesmen['st_hapus_pews']=='0'){
				  $btn_hapus='';
				}
				$aksi_edit='';
				if ($r->jml_edit>0){
					if ($r->st_ranap=='1'){
					$aksi_edit='<a href="'.base_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ews/his_pews/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit "><i class="fa fa-info"></a>';
					}else{
					$aksi_edit='<a href="'.base_url().'tews/tindakan/'.$r->pendaftaran_id.'/0/erm_ews/his_pews/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit "><i class="fa fa-info"></a>';
						
					}
				}
				$aksi='';
				
				
				$aksi = '<div class="btn-group">';
				$aksi .= $btn_edit;	
				$aksi .= $btn_duplikasi;	
				$aksi .= $btn_hapus;	
				$aksi .= $btn_lihat;	
				$aksi .= $aksi_edit;	
				$aksi .= '</div>';
		
				$header .='<th class="text-center" style="width:150px;vertical-align: middle;"><strong>';
				$header .='<span class="text-center">'.$r->nama_hari.' '.HumanDateShort($r->tanggal_pengkajian).'</span>';
				$header .='<br><span class="text-center text-primary">'.$r->nama_created.' '.HumanDateLong($r->tanggal_input).'</span>';
				$header .='<br>'.$aksi.'</span>';
				$header .='</strong></th>';
				$jml_kolom =$jml_kolom+1;
				
		  }
		  $tabel .=$header;
		  $tabel .='</tr>';
		  $tabel .='</thead>';
		  $tabel .='<tbody>';
		  $no=1;
		  foreach($list_param as $r){
				$param_id=$r->param_id;
				$tabel .='<tr>';
				$tabel .='<td>'.$no.'</td>';
				$tabel .='<td>'.$r->parameter_nama.'</td>';
				foreach($record as $row){
					$assesmen_id=$row->assesmen_id;
					$data_pilih = array_filter($record_detail, function($var) use ($assesmen_id,$param_id) { 
						return ($var['assesmen_id'] == $assesmen_id && $var['param_id'] == $param_id);
					  });
					$data_pilih=reset($data_pilih);
					if ($data_pilih){
						$nilai_id=$data_pilih['param_nilai_id'];
						$nilai=$data_pilih['nilai'];
						$warna=$data_pilih['warna'];
						$param_nama=$data_pilih['param_nama'];
					}else{
						$nilai_id='';
						$nilai_id='';
						$nilai=0;
						$warna="#00000";
						$param_nama="";
					}
					$tabel .='
						<td class="text-center">
						'.$param_nama.'
						
						<div style="margin-top:5px"><button class="btn btn-xs text-white" style="background-color: '.$warna.'" type="button">'.$nilai.'</button></div>
						<input type="hidden" class="nilai_angka" value="'.$nilai.'">
						</td>
					';
				}
				
				$tabel .='</tr>';
				
				
				$no++;
		  }
		  $tabel .='<tr>';
		  $tabel .='<td></td>';
		  $tabel .='<td><strong>TOTAL</strong></td>';
		  foreach($record as $r){
				$btn='<div class="margin-top:5px"><button class="btn btn-block btn-xs text-white" style="background-color: '.$r->warna_hasil.'" type="button">'.$r->total_skor_pews.'</button></div>';
				$tabel .='<td>'.$r->nama_hasil_pengkajian.$btn.'</td>';
				
		  }
		  $tabel .='</tr>';
		  $tabel.='</tbody>';
		  $tabel.='</table>';
	 }else{
		 $tabel='
			<div class="form-group">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p>Info <a class="alert-link" href="javascript:void(0)"> Belum ada Penilaian, Klik New Untuk Memulai Penilaian</a>!</p>
						</div>
					</div>
					
				</div>
			</div>
		 ';
		 
	 }
	 
	$arr['tabel']=$tabel;
	$this->output->set_output(json_encode($arr));
	
  }
  function opsi_nilai_rj_detail($param,$param_nilai_id){
	  $q="SELECT * FROM `mpews_param_skor` H WHERE H.id IN ($param)";
	  $hasil='';
	  $row=$this->db->query($q)->result();
		  $hasil .='<option data-nilai="0" value="" '.($param_nilai_id==''?'selected':'').'>TIDAK MENGISI</option>';
	  foreach($row as $r){
		  $hasil .='<option data-nilai="'.$r->skor.'" value="'.$r->id.'" '.($param_nilai_id==$r->id?'selected':'').'>'.$r->deskripsi_nama.'</option>';
	  }
	  return $hasil;
  }
  function batal_pews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
		  
	  }
	  if ($st_edited=='1'){
		  $q="DELETE FROM tranap_pews WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT INTO tranap_pews (
				assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_edit_id,alasan_id,keterangan_edit,jml_edit,edited_ppa,edited_date,keterangan_hapus,mpews_id,tanggal_pengkajian,ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan,st_ranap,warna_hasil
				)
			SELECT 
			assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_edit_id,alasan_id,keterangan_edit,jml_edit,edited_ppa,edited_date,keterangan_hapus,mpews_id,tanggal_pengkajian,ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan,st_ranap,warna_hasil
			FROM tranap_pews_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tranap_pews_detail WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			$q="INSERT INTO tranap_pews_detail (
				id,assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai,warna,param_nama
				)
			SELECT 
			id,assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai,warna,param_nama
			FROM tranap_pews_x_his_detail WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_pews',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_record_pews(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_pews',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_record_pews_all(){
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('pendaftaran_id_ranap',$pendaftaran_id_ranap);
		$result=$this->db->update('tranap_pews',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function create_with_template_pews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $q="INSERT INTO tranap_pews (
			template_assesmen_id,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,created_date,created_ppa,template_id,status_assemen,idpasien
			,mpews_id,tanggal_pengkajian,ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan,st_ranap,warna_hasil

			)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id','$pendaftaran_id_ranap',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen,'$idpasien'
			,mpews_id,CURRENT_DATE(),ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan,'$st_ranap',warna_hasil
			FROM `tranap_pews`

			WHERE assesmen_id='$template_assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
		$q="INSERT INTO tranap_pews_detail (
			assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai,warna,param_nama

		)
		SELECT
		'$assesmen_id',param_id,parameter_nama,group_nilai,param_nilai_id,nilai,warna,param_nama
		FROM `tranap_pews_detail`

		WHERE assesmen_id='$template_assesmen_id'";
	  $hasil=$this->db->query($q);
	  
	   //Update jumlah_template
	  $q="UPDATE tranap_pews 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	function create_with_template_pews_all(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="SELECT *FROM tranap_pews H WHERE H.pendaftaran_id_ranap='$template_assesmen_id' AND H.status_assemen='2'";
	  $list_duplikasi=$this->db->query($q)->result();
	  foreach ($list_duplikasi as $row){
		  $template_assesmen_id=$row->assesmen_id;
		  $q="INSERT INTO tranap_pews (
				template_assesmen_id,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,created_date,created_ppa,template_id,status_assemen,idpasien
				,mpews_id,tanggal_pengkajian,ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan

				)
				SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id','$pendaftaran_id_ranap',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen,'$idpasien'
				,mpews_id,CURRENT_DATE(),ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan
				FROM `tranap_pews`

				WHERE assesmen_id='$template_assesmen_id'";
		  // print_r($q);exit;
		  $hasil=$this->db->query($q);
		  $assesmen_id=$this->db->insert_id();
			$q="INSERT INTO tranap_pews_detail (
				assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai

			)
			SELECT
			'$assesmen_id',param_id,parameter_nama,group_nilai,param_nilai_id,nilai
			FROM `tranap_pews_detail`

			WHERE assesmen_id='$template_assesmen_id'";
		  $hasil=$this->db->query($q);
		  
		   //Update jumlah_template
		  $q="UPDATE tranap_pews 
				SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
				$this->db->query($q);
		}
	  $this->output->set_output(json_encode($hasil));
    }
	function edit_template_pews(){
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'pendaftaran_id_ranap' => $this->input->post('pendaftaran_id_ranap'),
			'idpasien' => $this->input->post('idpasien'),
		);

		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_pews',$data);

		$this->output->set_output(json_encode($result));
	}
	
	function batal_template_pews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_pews',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function save_edit_pews(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tranap_pews WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_pews($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tranap_pews',$data);
			// $ttv_id=$this->db->insert_id();
			$q="SELECT * from tranap_pews WHERE assesmen_id='$assesmen_id'";
			$data=$this->db->query($q)->row_array();
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_pews($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tranap_pews_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tranap_pews_x_his (versi_edit,
				assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_edit_id,alasan_id,keterangan_edit,jml_edit,edited_ppa,edited_date,keterangan_hapus,mpews_id,tanggal_pengkajian,ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan,st_ranap,warna_hasil
				)
				SELECT '$jml_edit',
				assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_edit_id,alasan_id,keterangan_edit,jml_edit,edited_ppa,edited_date,keterangan_hapus,mpews_id,tanggal_pengkajian,ref_nilai_id,total_skor_pews,nama_hasil_pengkajian,nama_tindakan,st_ranap,warna_hasil
				FROM tranap_pews
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
			//DETAIL
			$q="INSERT IGNORE INTO tranap_pews_x_his_detail 
				(versi_edit,
				id,assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai,warna,param_nama
				)
				SELECT '$jml_edit',
				id,assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai,warna,param_nama
				FROM tranap_pews_detail WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
		}
	  return $hasil;
	}
	function create_template_pews(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $mpews_id=$this->input->post('mpews_id');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
	 
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'mpews_id' => $mpews_id,
		'st_ranap' => $st_ranap,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'tanggal_pengkajian' => date('Y-m-d'),
		'status_assemen' => 3,
		
	  );
	  
	  $hasil=$this->db->insert('tranap_pews',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="
		INSERT INTO tranap_pews_detail (assesmen_id,param_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT '$assesmen_id',H.id as param_id, H.parameter_nama,GROUP_CONCAT(M.id) as group_nilai,null as param_nilai_id,0 as nilai 
			FROM `mpews_param` H
			LEFT JOIN mpews_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
			WHERE H.mpews_id='$mpews_id' AND H.staktif='1'
			GROUP BY H.id
	  ";
	  $hasil=$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
}	
