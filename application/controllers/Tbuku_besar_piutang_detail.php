<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tbuku_besar_piutang_detail extends CI_Controller {

	/**
	 * Detail Buku Besar Pembantu Piutang controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbuku_besar_piutang_setting_model');
		$this->load->model('Tbuku_besar_piutang_detail_model','model');
  }

	function index($idkelompok='',$idrekanan=''){
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-130 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");

		$data = array();
		$data['list_KP'] 			= $this->Tbuku_besar_piutang_setting_model->list_KP();
		$data['list_asuransi'] 			= $this->Tbuku_besar_piutang_setting_model->list_rekanan();
		// $data['list_distributor'] 			= $this->Tbuku_besar_PIUTANG_setting_model->list_distributor();
		// print_r($data['list_asuransi']);exit();
		$data['tanggal_trx1'] 			= HumanDateShort($date1);
		$data['tanggal_trx2'] 			= HumanDateShort($date2);
		// $data['tipe'] 			= $tipe;
		$data['idkelompok'] 	= $idkelompok;
		$data['idrekanan'] 	= $idrekanan;
		// $data['iddistributor_gabungan'] 	= '';
		// if ($iddistributor !=''){
			// $data['iddistributor_gabungan'] 	= $tipe.'-'.$iddistributor;
		// }
		$data['error'] 			= '';
		$data['title'] 			= 'Detail Buku Besar Pembantu Piutang';
		$data['content'] 		= 'Tbuku_besar_piutang_detail/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Detail Buku Besar Pembantu Piutang",'#'),
									    			array("List",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex()
	{
		// GetTipeDistributor
		$where='';
		$tipe_transaksi=$this->input->post('tipe_transaksi');
		$idkelompok=$this->input->post('idkelompok');
		$idrekanan=$this->input->post('idrekanan');
		
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		if ($idkelompok){
			$idkelompok=implode(',',$idkelompok);			
		}
		if ($idrekanan){
			$idrekanan=implode(',',$idrekanan);			
		}
		if ($tipe_transaksi){
			$tipe_transaksi=implode(',',$tipe_transaksi);			
		}
		
	
		if ($tanggal_trx1 !=''){
			$where .=" AND tanggal_transaksi >='".YMDFormat($tanggal_trx1)."' AND tanggal_transaksi <='".YMDFormat($tanggal_trx2)."'";
		}
		if ($idkelompok){
			$where .=" AND idkelompokpasien IN (".$idkelompok.")";
			
		}
		if ($idrekanan){
			$where .=" AND idrekanan IN (".$idrekanan.")";
			
		}
		if ($tipe_transaksi){
		$where .=" AND jenis_transaksi IN (".$tipe_transaksi.")";
			
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		$from="
			(	SELECT *FROM (
				SELECT H.id,H.klaim_id,H.klaim_detail_id,H.tipe, H.idkelompokpasien,H.idrekanan,CONCAT(H.idkelompokpasien,'-',H.idrekanan) as idkelompok,H.asuransi 
				,T.no_klaim,H.jenis_transaksi,H.debet,H.kredit,H.tanggal_transaksi
				FROM tbuku_besar_piutang H
				LEFT JOIN tklaim T ON T.id=H.klaim_id				
				
				) T WHERE T.idkelompokpasien IS NOT NULL ".$where."
				ORDER BY idkelompok  ASC,tanggal_transaksi ASC
			)tbl
		";
		// print_r('');exit();
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array();
		$this->group  = array();
		$saldo_awal=0;
		$this->column_search   = array();
		$this->column_order    = array();

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$str_asuransi='';
		foreach ($list as $r) {
		  $no++;
		  $row = array();
		  $debet=0;$kredit=0;
		  $debet=$r->debet;
		  $kredit=$r->kredit;
		  
		  if ($str_asuransi != $r->idkelompok){
			  if ($tanggal_trx1==''){
				  $tanggal_trx1=$r->tanggal_transaksi;
			  }
			$saldo_awal=get_saldo_awal_piutang($r->idkelompok,YMDFormat($tanggal_trx1));
			$str_asuransi = $r->idkelompok;
		  }
		   $url        = site_url('tklaim_rincian/');
          
		  $row[] = HumanDateShort($r->tanggal_transaksi);
		  $row[] = $r->asuransi.'<br>'.$r->idkelompok;
		  $row[] = $r->asuransi;
		  $row[] = $this->jenis_transaksi($r->jenis_transaksi);
		  // $row[] = $saldo_awal;
		  $row[] = $r->no_klaim;
		  $row[] = number_format($saldo_awal,2);
		  $row[] = number_format($debet,2);
		  $row[] = number_format($kredit,2);
		  $saldo_awal=$saldo_awal + $debet - $kredit;
		  $row[] = number_format($saldo_awal,2);
			   $aksi = '<div class="btn-group">';     
				$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->klaim_id.'/'.$r->tipe.'/1" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>';
			  
			  $aksi .= '</div>';
		
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}

	
	function jenis_transaksi($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-primary">PIUTANG</span>';
		}
		if ($tipe=='2'){
			$tipe='<span class="label label-success">PEMBAYARAN</span>';
			
		}
		if ($tipe=='3'){
			$tipe='<span class="label label-danger">OTHER LOSS</span>';
		}
		
		return $tipe;
	}
	public function export()
    {
		// print_r($this->input->post());exit();
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
        $where='';
		$idkelompokpasien=$this->input->post('idkelompok');
		$idrekanan=$this->input->post('idrekanan');
		// print_r($idkelompokpasien);exit();
		$tipe_transaksi=$this->input->post('tipe_transaksi');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$data=array(
			'judul'=>'BUKU BESAR PEMBANTU PIUTANG',
			'distributor'=>'-All-',
			'rekanan'=>'-All-',
			'tanggal'=>'',
			'tanggal_trx1'=>$tanggal_trx1,
		);
		
		if ($tanggal_trx1 !=''){
			$where .=" AND H.tanggal_transaksi >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_transaksi <='".YMDFormat($tanggal_trx2)."'";
			$data['tanggal']=HumanDateShort(YMDFormat($tanggal_trx1)).' s/d '.HumanDateShort(YMDFormat($tanggal_trx2));
		}
		if ($idkelompokpasien){
			$idkelompokpasien=implode(',',$idkelompokpasien);		
			$where .=" AND H.idkelompokpasien IN (".$idkelompokpasien.")";
			$data['distributor']=$this->model->getKP($idkelompokpasien);
			// print_r($data['distributor']);exit();
		}
		if ($idrekanan){
			$idrekanan=implode(',',$idrekanan);		
			$where .=" AND H.idrekanan IN (".$idrekanan.")";
			$data['rekanan']=$this->model->getRekanan($idrekanan);
			// print_r($data['distributor']);exit();
		}
		if ($tipe_transaksi){
			$tipe_transaksi=implode(',',$tipe_transaksi);	
			$where .=" AND (H.tipe_transaksi) IN (".$tipe_transaksi.")";
		}
		
		$from="
				SELECT *FROM (
				SELECT H.id,H.klaim_id,H.klaim_detail_id,H.tipe, H.idkelompokpasien,H.idrekanan,CONCAT(H.idkelompokpasien,'-',H.idrekanan) as idkelompok,H.asuransi 
				,T.no_klaim,H.jenis_transaksi,H.debet,H.kredit,H.tanggal_transaksi
				,MP.nama as kelompok_nama
				FROM tbuku_besar_piutang H
				LEFT JOIN tklaim T ON T.id=H.klaim_id				
				LEFT JOIN mpasien_kelompok MP ON MP.id=H.idkelompokpasien
				) H WHERE H.idkelompokpasien IS NOT NULL ".$where."
				ORDER BY idkelompok  ASC,tanggal_transaksi ASC
			";
				// print_r($from);exit();
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($from)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail,$data);
		}
		if ($btn=='2'){
			$this->pdf($row_detail,$data);
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$data);
		}
		
		
		
    }
	public function pdf($row_detail,$header){
		// print_r($header);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$header);
        
        $data['detail'] = $row_detail;

		$data = array_merge($data, backend_info());

        $html = $this->load->view('Tbuku_besar_piutang_detail/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Jurnal Kas.pdf', array("Attachment"=>0));
	}
	
	function excel($row_detail,$data){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle($data['judul']);

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		$activeSheet->setCellValue('B5', "KELOMPOK PASIEN");
		$activeSheet->setCellValue('C5', $data['distributor']);
		$activeSheet->setCellValue('B6', "ASURANSI");
		$activeSheet->setCellValue('C6', $data['rekanan']);
		$activeSheet->setCellValue('B7', "TANGGAL");
		$activeSheet->setCellValue('C7', $data['tanggal']);
		// $activeSheet->setCellValue('B7', "TANGGAL");
		// $activeSheet->setCellValue('C7', $data['tanggal1'].' s/d'. $data['tanggal2']);
		
		// Set Title
		$activeSheet->setCellValue('B9', $data['judul']);
		$activeSheet->mergeCells('B9:H9');
		$activeSheet->getStyle('B9')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$x = 11;
		$activeSheet->setCellValue("B$x", "TANGGAL");
		$activeSheet->setCellValue("C$x", "ASURNASI");
		$activeSheet->setCellValue("D$x", "ASURANSI");
		$activeSheet->setCellValue("E$x", "KETERANGAN");
		$activeSheet->setCellValue("F$x", "SALDO AWAL");
		$activeSheet->setCellValue("G$x", "DEBET");
		$activeSheet->setCellValue("H$x", "KREDIT");
		$activeSheet->setCellValue("I$x", "SALDO AKHIR");
		$activeSheet->getStyle("B$x:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$x:I$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		
		$debet = 0;
		$kredit= 0;
		$saldo_awal=0;
		$saldo_akhir=0;
		$str_asuransi='';
		$tanggal_trx1=$data['tanggal_trx1'];
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				if ($str_asuransi != $row->idkelompok){
					if ($tanggal_trx1==''){
						  $data['tanggal_trx1']=$row->tanggal_transaksi;
					}
					$saldo_awal=get_saldo_awal_piutang($row->idkelompok,YMDFormat($tanggal_trx1));
					$str_asuransi = $row->idkelompok;
				}
				
	            $activeSheet->setCellValue("B$x", HumanDateShort($row->tanggal_transaksi));
	            $activeSheet->setCellValue("C$x", $row->asuransi);
	            $activeSheet->setCellValue("D$x", $this->model->tipe_pemesanan_biasa($row->jenis_transaksi));
	            $activeSheet->setCellValue("E$x", $row->no_klaim);
	            $activeSheet->setCellValue("F$x", $saldo_awal);
	            $activeSheet->setCellValue("G$x", $row->debet);
	            $activeSheet->setCellValue("H$x", $row->kredit);
				$saldo_awal=(int)$saldo_awal-(int)$row->kredit+(int)$row->debet;
				// $kredit=(int)$kredit+(int)$row->kredit;
				// $saldo_awal=$saldo_awal + $row->kredit - $row->debet;
	            $activeSheet->setCellValue("I$x", $saldo_awal);
				
	           
				
				
			}
		}
		// $x = $x+1;
		
		$activeSheet->getStyle("B11:D$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("H11:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("E11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("F11:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("F11:I$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B11:I$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("i")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("H")->setWidth(45);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$data['judul'].' '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
	
}
