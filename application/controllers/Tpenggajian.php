<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpenggajian extends CI_Controller {

	/**
	 * Transaksi Gaji controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpenggajian_model');
  }

	function index(){
		$data = array(
			'tahun' => date('Y'),
		);
		$data['list_jenis'] 			= $this->Tpenggajian_model->list_jenis();
		$data['error'] 			= '';
		$data['title'] 			= 'Transaksi Gaji';
		$data['content'] 		= 'Tpenggajian/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Transaksi Gaji",'tpenggajian/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter(){
		$data = array(
			'tahun' => $this->input->post('tahun')
		);

    $this->session->set_userdata($data);

		$data['error'] 			= '';
		$data['title'] 			= 'Transaksi Gaji';
		$data['content'] 		= 'Tpenggajian/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Transaksi Gaji",'tpenggajian/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 			=> '',
			'bulan' 	=> date("m"),
			'periode' 	=> date("d-m-Y"),
			'tahun' 	=> date("Y"),
			'subyek' 	=> '',
			'status' 	=> '',
			'idjenis' 	=> '#',
		);

		$data['error'] 			= '';
		$data['list_jenis'] 			= $this->Tpenggajian_model->list_jenis();
		$data['title'] 			= 'Tambah Transaksi Gaji';
		$data['content'] 		= 'Tpenggajian/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Transaksi Gaji",'#'),
									    			array("Tambah",'tgaji')
													);

		$data['list_detail'] = array();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Tpenggajian_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 			=> $row->id,
					'bulan' 	=> $row->bulan,
					'periode' 	=> HumanDateShort($row->periode),
					'idjenis' 	=> $row->idjenis,
					'tahun' 	=> $row->tahun,
					'subyek' 	=> $row->subyek,
					'status' 	=> $row->status,
				);
				$data['list_jenis'] 			= $this->Tpenggajian_model->list_jenis();
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Transaksi Gaji';
				$data['content']	 	= 'Tpenggajian/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Transaksi Gaji",'#'),
											    			array("Ubah",'tgaji')
															);

				$data['list_detail'] 	= $this->Tpenggajian_model->getDetailData($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('tgaji','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tgaji','location');
		}
	}

	function review($id,$disabel=''){
		if($id != ''){
			$row = $this->Tpenggajian_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 			=> $row->id,
					'bulan' 	=> $row->bulan,
					'tahun' 	=> $row->tahun,
					'subyek' 	=> $row->subyek,
					'status' 	=> $row->status,
				);

				$data['disabel'] 			= $disabel;
				$data['error'] 			= '';
				$data['title'] 			= 'Input Transaksi Gaji ['.$row->nama_jenis.']';
				$data['content']	 	= 'Tpenggajian/review';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Transaksi Gaji",'#'),
											    			array("Input",'tgaji')
															);

				$data['list_detail'] 	= $this->Tpenggajian_model->getReviewData($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('tgaji','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tgaji','location');
		}
	}

	function delete($id){
		$this->Tpenggajian_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('tgaji','location');
	}
	function hapus_gaji($id){
		$result=$this->Tpenggajian_model->softDelete($id);
		$this->output->set_output(json_encode($result));
	}

	function save(){
		if($this->input->post('id') == '' ) {
			if($this->Tpenggajian_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tpenggajian/index/'.$this->input->post('idtipe'),'location');
			}
		} else {
			if($this->Tpenggajian_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tpenggajian/index/'.$this->input->post('idtipe'),'location');
			}
		}
	}

	function updateReview(){
		// print_r($this->input->post());exit();
		if($this->Tpenggajian_model->updateReview()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tpenggajian/index/'.$this->input->post('idtipe'),'location');
		}
	}

	function getVariableRekapan($idtipevariable)
	{
		$html = '';
		$result = $this->Tpenggajian_model->getVariableRekapan($idtipevariable);

		foreach ($result as $row) {
			$html .= '<tr>';
			$html .= '  <td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" class="checkboxVariable"><span></span></label></td>';
			$html .= '  <td style="display:none">'.$row->id.'</td>';
			$html .= '  <td>'.$row->nama.'</td>';
			$html .= '  <td>'.($row->idtipe == 1 ? "Pengeluaran" : "Pengeluaran + Pendapatan").'</td>';
			$html .= '</tr>';
		}

		echo $html;
	}

	function getIndex($uri = '')
	{
		$where='';
		$where_status='';
		$notransaksi=$this->input->post('notransaksi');
		$periode=$this->input->post('periode');
		$idjenis=$this->input->post('idjenis');
		$status=$this->input->post('status');
		if ($notransaksi!=''){
			$where .=" AND H.notransaksi='$notransaksi'";
		}
		if ($periode!=''){
			$where .=" AND H.periode='".YMDFormat($periode)."'";
		}
		if ($idjenis!='#'){
			$where .=" AND H.idjenis='$idjenis'";
		}
		if ($status!='#'){
			if ($status=='3'){
				$where .=" AND H.st_verifikasi='1'";
			}
			if ($status=='2'){
				$where_status .=" WHERE st_verifikasi='0' AND nominal > 0";
			}
			if ($status=='1'){
				$where_status .=" WHERE st_verifikasi='0' AND nominal = 0";
			}
			
		}
			$this->select = array();
			$this->from   = "
							(
							SELECT H.id,H.periode,H.st_verifikasi,H.subyek,H.idjenis,M.nama as jenis
							,H.bulan,H.tahun,SUM(D.nominal) as nominal,H.`status`,H.notransaksi
							FROM tgaji H
							LEFT JOIN mjenis_gaji M ON M.id=H.idjenis
							LEFT JOIN tgaji_detail D ON D.idgaji=H.id AND D.idsub !='1'
							WHERE H.`status`='1' ".$where."
							GROUP BY H.id
							) as tbl 
			".$where_status;
			$this->join 	= array();
			$this->where  = array();


			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('notransaksi', 'tahun', 'bulan', 'subyek');
			$this->column_order    = array('notransaksi', 'tahun', 'bulan', 'subyek');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();
					$status='';
					$btn_disable='';
					if ($r->nominal==0){
						$btn_disable="disabled";
					}
					if ($r->st_verifikasi=='0'){
						if ($r->nominal > 0){
							$status='<span class="label label-success">TELAH DIPROSES</span>';
						}else{
							$status='<span class="label label-danger">BELUM DIPROSES</span>';							
						}
					}else{
						$status='<span class="label label-primary">DIVERIFIKASI</span>';		
					}
					$row[] = $r->id;
					$row[] = $no;
					$row[] = $r->notransaksi;
					$row[] = $r->jenis;
					$row[] = $r->subyek;
					$row[] = HumanDateShort($r->periode);
					$row[] = MONTHFormat($r->bulan).' '.$r->tahun;
					$row[] = $status;
					$aksi = '<div class="btn-group">';
					
                                                
					$aksi .= '<a href="'.site_url().'tpenggajian/review/'.$r->id.'/disabled" data-toggle="tooltip" title="Input Gaji" class="btn btn-default btn-xs"><i class="fa fa-eye"></i> </a>';
					if ($r->st_verifikasi != 1) {
					$aksi .= '<a href="'.site_url().'tpenggajian/review/'.$r->id.'" data-toggle="tooltip" title="Input Gaji" class="btn btn-success btn-xs"><i class="fa fa-list"></i> Input Gaji</a>';
					$aksi .= '<button title="Ubah" class="btn btn-danger btn-xs verifikasi" '.$btn_disable.'><i class="fa fa-check"></i> Verifikasi</button>';
							$aksi .= '<a href="'.site_url().'tpenggajian/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
							
							$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs" onclick="hapus_gaji('.$r->id.')"><i class="fa fa-trash-o"></i></button>';
					}
					$aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function verifikasi()
	{
		$idgaji=$this->input->post('idgaji');
		$q="SELECT H.id,H.periode,H.idjenis,SUM(CASE WHEN D.idtipe='1' THEN D.nominal ELSE 0 END) as total from tgaji H
			LEFT JOIN tgaji_detail D ON D.idgaji=H.id AND D.idsub !='1'
			WHERE H.id='$idgaji'
			GROUP BY H.id";
		// print_r($q);exit();
		$row=$this->db->query($q)->row_array();
		$periode=$row['periode'];
		$idjenis=$row['idjenis'];
		$total=$row['total'];
		$qcari="SELECT R.id,R.`status` FROM trekap R WHERE R.periode='".YMDFormat($periode)."' AND R.status!='0'";
		$cari=$this->db->query($qcari)->row_array();
		if ($cari){
			
			// print_r($cari);exit();
			if ($cari['status']=='1'){
				$id_rekap=$cari['id'];
				$data_det=array(
					'idrekap' =>$id_rekap,
					'idgaji' =>$idgaji,
					'periode' =>$periode,
					'nominal_trx' =>RemoveComma($total),
					'created_by' =>$this->session->userdata('user_id'),
					'created_date' => date('Y-m-d H:i:s'),					
				);
				$result=$this->db->insert('trekap_detail',$data_det);
			}else{
				$result=false;
			}
		}else{
			$data_det=array(
				'periode' =>$periode,
				'jml_gaji' =>1,
				'created_by' =>$this->session->userdata('user_id'),
				'created_date' => date('Y-m-d H:i:s'),					
			);
			$this->db->insert('trekap',$data_det);
			$id_rekap=$this->db->insert_id();
			$data_det=array(
				'idrekap' =>$id_rekap,
				'idgaji' =>$idgaji,
				'periode' =>$periode,
				'nominal_trx' =>RemoveComma($total),
				'created_by' =>$this->session->userdata('user_id'),
				'created_date' => date('Y-m-d H:i:s'),					
			);
			$result=$this->db->insert('trekap_detail',$data_det);;
		}
	
		$this->output->set_output(json_encode($result));
	}
}
