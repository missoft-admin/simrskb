<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_hutang extends CI_Controller {

	/**
	 * Setting Jurnal Hutang controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_hutang_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_hutang_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_hutang_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Hutang';
		$data['content'] 		= 'Msetting_jurnal_hutang/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Hutang",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_kategori($iddistributor){
		// $iddistributor=$this->input->post('iddistributor');
		$q="SELECT * FROM mdata_kategori M
			WHERE M.`status`='1' AND M.iddistributor='$iddistributor'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_distributor($tipe_distributor){
		if ($tipe_distributor=='1'){
			$q="SELECT * FROM mdistributor M WHERE M.`status`='1' ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mvendor M WHERE M.`status`='1' ORDER BY M.nama ASC";
			
		}
		
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	public function list_barang()
    {
        $cari 	= $this->input->post('search');
        $iddistributor 	= $this->input->post('iddistributor');
        $idkategori 	= $this->input->post('idkategori');
		$where='';
		if ($idkategori!='#' && $idkategori!='0'){
			$where .=" AND M.idkategori='$idkategori' ";
		}
		if ($cari){
			$where .=" AND (M.nama LIKE '%".$cari."%' OR M.kode LIKE '%".$cari."%')";
		}
		$q="SELECT *FROM view_barang M WHERE M.iddistributor='$iddistributor' ".$where." LIMIT 100";
		// print_r($q);exit();
        $data_obat = $this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			// 'idakun_kredit'=>$this->input->post('idakun_kredit'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_hutang',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Hutang
	function load_hutang()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.tipe_distributor,H.iddistributor
				,CASE WHEN H.tipe_distributor='1' THEN M.nama ELSE V.nama END as nama_distributor,H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM `msetting_jurnal_hutang_hutang` H
				LEFT JOIN mdistributor M ON M.id=H.iddistributor AND H.tipe_distributor='1'
				LEFT JOIN mvendor V ON V.id=H.iddistributor AND H.tipe_distributor='3'
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.tipe_distributor,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','nama_distributor');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->tipe_distributor=='1'?text_success('Pemesanan Gudang'):text_primary('Pengajuan'));
            $row[] = ($r->iddistributor=='0'?text_default('All Distributor'):$r->nama_distributor);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_hutang('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_hutang(){
		$id_edit=$this->input->post('id_edit');
		$iddistributor=($this->input->post('iddistributor')=='#'?0:$this->input->post('iddistributor'));
		$data=array(
			'setting_id'=>1,
			'iddistributor'=>$iddistributor,
			'tipe_distributor'=>$this->input->post('tipe_distributor'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_hutang($data['tipe_distributor'],$iddistributor)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_hutang_hutang',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_hutang($tipe_distributor,$iddistributor){
		
		$gabung=$tipe_distributor.'-'.$iddistributor;
		$q="SELECT *FROM msetting_jurnal_hutang_hutang S
			WHERE CONCAT(S.tipe_distributor,'-',S.iddistributor)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_hutang($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_hutang_hutang');
		echo json_encode($result);
	}
	
	//PPN
	function load_cheq()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				
				SELECT H.id,H.tipe_distributor,H.iddistributor
				,CASE WHEN H.tipe_distributor='1' THEN M.nama ELSE V.nama END as nama_distributor,H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM `msetting_jurnal_hutang_cheq` H
				LEFT JOIN mdistributor M ON M.id=H.iddistributor AND H.tipe_distributor='1'
				LEFT JOIN mvendor V ON V.id=H.iddistributor AND H.tipe_distributor='3'
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.tipe_distributor,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','nama_distributor');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;     
			$row[] = ($r->tipe_distributor=='1'?text_success('Pemesanan Gudang'):text_primary('Pengajuan'));
            $row[] = ($r->iddistributor=='0'?text_default('All Distributor'):$r->nama_distributor);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_cheq('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_cheq(){
		$id_edit=$this->input->post('id_edit');
		$iddistributor=($this->input->post('iddistributor')=='#'?0:$this->input->post('iddistributor'));
		$data=array(
			'setting_id'=>1,
			'iddistributor'=>$iddistributor,
			'tipe_distributor'=>$this->input->post('tipe_distributor'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_cheq($data['tipe_distributor'],$iddistributor)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_hutang_cheq',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_cheq($tipe_distributor,$iddistributor){
		$gabung=$tipe_distributor.'-'.$iddistributor;
		$q="SELECT *FROM msetting_jurnal_hutang_cheq S
			WHERE CONCAT(S.tipe_distributor,'-',S.iddistributor)='$gabung'";
		// print_r($q);
		return $this->db->query($q)->row('id');
	}	
	function hapus_cheq($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_hutang_cheq');
		echo json_encode($result);
	}
	
	//DISKON
	
	function load_materai()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.tipe_distributor,H.iddistributor
				,CASE WHEN H.tipe_distributor='1' THEN M.nama ELSE V.nama END as nama_distributor,H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM `msetting_jurnal_hutang_materai` H
				LEFT JOIN mdistributor M ON M.id=H.iddistributor AND H.tipe_distributor='1'
				LEFT JOIN mvendor V ON V.id=H.iddistributor AND H.tipe_distributor='3'
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.tipe_distributor,H.id

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','nama_distributor');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;  
			$row[] = ($r->tipe_distributor=='1'?text_success('Pemesanan Gudang'):text_primary('Pengajuan'));
            $row[] = ($r->iddistributor=='0'?text_default('All Distributor'):$r->nama_distributor);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_materai('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_materai(){
		$id_edit=$this->input->post('id_edit');
		$iddistributor=($this->input->post('iddistributor')=='#'?0:$this->input->post('iddistributor'));
		$data=array(
			'setting_id'=>1,
			'iddistributor'=>$iddistributor,
			'tipe_distributor'=>$this->input->post('tipe_distributor'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_materai($data['tipe_distributor'],$iddistributor)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_hutang_materai',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_materai($tipe_distributor,$iddistributor){
		$gabung=$tipe_distributor.'-'.$iddistributor;
		$q="SELECT *FROM msetting_jurnal_hutang_materai S
				WHERE CONCAT(S.tipe_distributor,'-',S.iddistributor)='$gabung'";
		
		return $this->db->query($q)->row('id');
	}	
	function hapus_materai($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_hutang_materai');
		echo json_encode($result);
	}
	
	//KAS
	function load_kas()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.sumber_kas_id,M.nama as nama_kas,H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM `msetting_jurnal_hutang_kas` H
				LEFT JOIN msumber_kas M ON M.id=H.sumber_kas_id
				LEFT JOIN makun_nomor A ON A.id=H.idakun

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','nama_kas');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->sumber_kas_id=='0'?text_default('All Sumber Kas'):$r->nama_kas);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_kas('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_kas(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'sumber_kas_id'=>$this->input->post('sumber_kas_id'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_kas($data['sumber_kas_id'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_hutang_kas',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_kas($sumber_kas_id){
		$q="SELECT *FROM msetting_jurnal_hutang_kas S
			WHERE S.sumber_kas_id='$sumber_kas_id'";
		
		return $this->db->query($q)->row('id');
	}	
	function hapus_kas($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_hutang_kas');
		echo json_encode($result);
	}
}
