<?php

defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Trm_analisa extends CI_Controller
{
    /**
     * Analisa Rekam Medis controller
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_analisa_model', 'model');
        $this->load->model('Trm_berkas_model');
    }

    public function proses($idpasien, $readonly='')
    {
        $data = array();
        $data['error'] = '';
        $data['content'] = 'Trm_analisa/index';
        $data['breadcrum'] = [
            ["RSKB Halmahera",'#'],
            ["Berkas Rekam Medis",'Trm_analisa/proses'],
            ["Analisa",'#']
        ];

        $data['pasien'] = $this->Trm_berkas_model->get_data_pribadi($idpasien);
        $data['list_dokter'] = $this->Trm_berkas_model->list_dokter2();

        $data['title'] = $data['pasien']['title'].' '.$data['pasien']['nama'];

        if (isset($_GET['tipe']) && isset($_GET['idberkas'])) {
            $data['tipe'] = $_GET['tipe'];
            $data['idberkas'] = $_GET['idberkas'];
            $data['idpendaftaran'] = $_GET['idpendaftaran'];
            $data['asalpendaftaran'] = $_GET['asalpendaftaran'];
        } else {
            $data['tipe'] = null;
            $data['idberkas'] = null;
            $data['idpendaftaran'] = null;
            $data['asalpendaftaran'] = null;
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function save()
    {
        $idberkas = $this->input->post('idberkas');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $asalpendaftaran = $this->input->post('asalpendaftaran');

        $data = array(
          'st_analisa' => '1',
          'tgl_analisa' => date('Y-m-d H:i:s'),
          'user_nama_analisa' => $this->session->userdata('user_name'),
          'user_id_analisa' => $this->session->userdata('user_id'),
        );

        $this->db->where('id', $idberkas);
        if ($this->db->update('trm_layanan_berkas', $data)) {
          $this->save_berkas_analisa($idberkas, $idpendaftaran, $asalpendaftaran);
        }

        return true;
    }

    public function save_verifikasi()
    {
        $idberkas = $this->input->post('idberkas');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $asalpendaftaran = $this->input->post('asalpendaftaran');

        $data = array(
          'st_verifikasi' => '1',
          'tgl_verifikasi' => date('Y-m-d H:i:s'),
          'user_nama_verifikasi' => $this->session->userdata('user_name'),
          'user_id_verifikasi' => $this->session->userdata('user_id'),
        );

        $this->db->where('id', $idberkas);
        if ($this->db->update('trm_layanan_berkas', $data)) {
          $this->save_berkas_analisa($idberkas, $idpendaftaran, $asalpendaftaran);
        }

        return true;
    }

    public function save_berkas_analisa($idberkas, $idpendaftaran, $asalpendaftaran) {
      // delete old data before insert new data
      $this->db->where('idberkas', $idberkas);
      $this->db->delete('trm_berkas_analisa');

      $this->db->where('idberkas', $idberkas);
      $this->db->delete('trm_berkas_analisa_dokter');

      // insert new data analisa
      $analisa = json_decode($this->input->post('analisa'));
      foreach ($analisa as $row) {
        $dataAnalisa = array(
          'idberkas' => $row->idberkas,
          'idanalisa' => $row->idanalisa,
          'idlembaran' => $row->idlembaran,
          'idpendaftaran' => $idpendaftaran,
          'asalpendaftaran' => $asalpendaftaran,
          'kriteria_nilai' => $row->kriteria[0]->nilai,
          'kriteria_keterangan' => $row->kriteria[0]->keterangan,
          'detail' => json_encode($row->detail),
          'dokter' => json_encode($row->dokter),
          'catatan' => $row->catatan,
          'hold' => $row->hold,
          'created_at' => date("Y-m-d H:i:s"),
          'created_by' => $this->session->userdata('user_id'),
        );

        $this->db->insert('trm_berkas_analisa', $dataAnalisa);


        // insert new data dokter
        $listDokter = $row->dokter;
        foreach ($listDokter as $dokter) {
          $dataAnalisaDokter = array(
            'idberkas' => $row->idberkas,
            'idanalisa' => $row->idanalisa,
            'idlembaran' => $row->idlembaran,
            'idpendaftaran' => $idpendaftaran,
            'asalpendaftaran' => $asalpendaftaran,
            'tipe' => $dokter->tipe,
            'iddokter' => $dokter->id,
            'namadokter' => $dokter->value,
          );

          $this->db->insert('trm_berkas_analisa_dokter', $dataAnalisaDokter);
        }
      }
    }

    public function get_berkas_pasien()
    {
        $idpasien = $this->input->post('idpasien');
        $filter_idjenis = $this->input->post('filter_idjenis');

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $where = '';
        if ($filter_idjenis != '#') {
            $where .=" AND REF.tujuan='$filter_idjenis'";
        }

        $from = "(SELECT REF.*,
					CASE
					WHEN REF.tujuan='3'THEN
						CONCAT(K.nama,'/',B.nama,' - ',D.nama)
					WHEN REF.tujuan='5'THEN
						REF.catatan
					ELSE
						CONCAT(P.nama,' - ',D.nama)
					END as detail,D.nama as namadokter,
          REF.id_trx AS idpendaftaran,
          (CASE
            WHEN REF.tujuan IN (1, 2) THEN 'rawatjalan'
            WHEN REF.tujuan IN (3, 4) THEN 'rawatinap'
          END) AS asalpendaftaran
				FROM trm_layanan_berkas as REF
				LEFT JOIN mdokter D ON D.id=REF.iddokter
				LEFT JOIN mpoliklinik P ON P.id=REF.idpoliklinik
				LEFT JOIN mbed B ON B.id=REF.bed
				LEFT JOIN musers U ON U.id=REF.user_trx
				LEFT JOIN mkelas K ON K.id=REF.kelas
				WHERE REF.idpasien='$idpasien' AND REF.status <>'0'  AND REF.status_kirim <>'0'  AND REF.tujuan <>'5' ".$where." ORDER BY REF.tanggal_trx DESC) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];

        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        foreach ($list as $r) {
            $no++;
            $row = array();

            $info_berkas = GetTujuanBerkas($r->tujuan).'<br>'.HumanDateLong($r->tanggal_trx).'<br>'.$r->namadokter;

            $status = '';
            $aksi = '<div class="btn-group">';
            if ($r->status_kirim=='1') {
                if (UserAccesForm($user_acces_form, array('1222'))) {
                    $aksi .= '<button class="btn btn-xs btn-danger kembali" title="Kembalikan Berkas"><i class="si si-arrow-left"></i></button>';
                }
                $status = text_danger('BELUM KEMBALI');
            } elseif ($r->st_analisa=='0') {
                if (UserAccesForm($user_acces_form, array('1186'))) {
                    $aksi .= '<a href="?tipe=analisa&idberkas=' . $r->id . '&idpendaftaran=' . $r->idpendaftaran . '&asalpendaftaran=' . $r->asalpendaftaran . '" class="btn btn-xs btn-primary analisa" title="Analisa"><i class="si si-arrow-right"></i></a>';
                }
                $status = text_default('BELUM DIANALISA');
            } elseif ($r->st_analisa=='1' && $r->st_verifikasi=='0') {
                if (UserAccesForm($user_acces_form, array('1188'))) {
                    $aksi .= '<a href="?tipe=verifikasi&idberkas=' . $r->id . '&idpendaftaran=' . $r->idpendaftaran . '&asalpendaftaran=' . $r->asalpendaftaran . '" class="btn btn-xs btn-success verifikasi" title="Verifikasi"><i class="si si-check"></i></a>';
                }
                $status = text_success('TELAH DIANALISA');
            } elseif ($r->st_verifikasi=='1') {
                if (UserAccesForm($user_acces_form, array('1188'))) {
                    $aksi .= '<a href="?tipe=verifikasi&idberkas=' . $r->id . '&idpendaftaran=' . $r->idpendaftaran . '&asalpendaftaran=' . $r->asalpendaftaran . '" class="btn btn-xs btn-success verifikasi" title="Verifikasi"><i class="si si-check"></i></a>';
                }
                if (UserAccesForm($user_acces_form, array('1190'))) {
                    $aksi .= '<a href="?tipe=lihat&idberkas=' . $r->id . '&idpendaftaran=' . $r->idpendaftaran . '&asalpendaftaran=' . $r->asalpendaftaran . '" class="btn btn-xs btn-default lihat" title="Lihat Hasil Analisa"><i class="si si-eye"></i></a>';
                }
                $status = text_primary('TELAH DIVERIFIKASI');
            }
            $aksi.='</div>';

            $row[] = $info_berkas;
            $row[] = $status;
            $row[] = $aksi;
            $row[] = $r->id;
            $row[] = $r->tujuan;

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function testLogic() {
      $setting = get_all('msetting_analisa', array('status' => '1'));
      $this->model->runLogic($setting[1], '9962');
    }

    public function print_analisa($idberkas, $idanalisa) {
      $dompdf = new Dompdf();
      $infoBerkas = $this->model->GetInfoBerkas($idberkas);
      $infoSetting = $this->model->GetInfoSetting($idanalisa);

      $data = [
        'idberkas' => $idberkas,
        'idanalisa' => $idanalisa,
        'notransaksi' => $infoBerkas->notransaksi,
        'nomedrec' => $infoBerkas->nomedrec,
        'namapasien' => $infoBerkas->namapasien,
        'petugas_analisa' => $infoBerkas->petugas_analisa,
        'tanggal_lahir' => $infoBerkas->tanggal_lahir,
        'dokter_pj' => $infoBerkas->dokter_pj,
        'ruangan_perawatan' => $infoBerkas->ruangan_perawatan,
        'nama_analisa' => $infoSetting->nama,
        'lembaran' => $infoSetting->lembaran,
      ];

      // $this->load->view('Trm_analisa/print_analisa', $data);
      $html = $this->load->view('Trm_analisa/print_analisa', $data, true);
      $dompdf->loadHtml($html);
      $dompdf->setPaper('A4', 'portrait');
      $dompdf->render();
      $dompdf->stream('Berkas Analisa.pdf', ['Attachment' => 0]);
    }
}
