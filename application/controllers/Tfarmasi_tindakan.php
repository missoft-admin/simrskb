<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
use Hashids\Hashids;
class Tfarmasi_tindakan extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Tfarmasi_tindakan_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tpoliklinik_resep_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
			$log['path_tindakan']='tfarmasi_tindakan';
			$this->session->set_userdata($log);
			$user_id=$this->session->userdata('user_id');
			$user_nama_login=$this->session->userdata('user_name');
		
			// print_r($this->session->userdata());exit;
		    $this->db->query("UPDATE tpoliklinik_pendaftaran INNER JOIN (
			SELECT H.id,H.tanggal,H.kode_antrian,K.kodeantrian_poli
			,CONCAT(COALESCE(K.kodeantrian_poli,''),COALESCE(KD.kodeantrian_dokter,''),LPAD(H.noantrian,3,'0')) as kode_baru
			FROM tpoliklinik_pendaftaran H
			LEFT JOIN antrian_poli_kode K ON K.idpoli=H.idpoliklinik
			LEFT JOIN antrian_poli_kode_dokter KD ON KD.idpoli=H.idpoliklinik AND KD.iddokter=H.iddokter
			WHERE H.kode_antrian IS NULL AND H.reservasi_tipe_id > 0
			) T ON T.id=tpoliklinik_pendaftaran.id
			SET tpoliklinik_pendaftaran.kode_antrian=T.kode_baru");

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// if ($tab=='0'){			
			// if (UserAccesForm($user_acces_form,array('1566'))){
				// $tab='1';
			// }elseif (UserAccesForm($user_acces_form,array('1567'))){
				// $tab='2';
			// }elseif (UserAccesForm($user_acces_form,array('1568'))){
				// $tab='3';
			// }elseif (UserAccesForm($user_acces_form,array('1569'))){
				// $tab='4';
			// }
		// }
		if (UserAccesForm($user_acces_form,array('1835'))){
			$data = array(
				
			);
			// $DL =$this->Tfarmasi_tindakan_model->get_data_login();
			// if ($DL){
				// $data=$DL;
			// }else{
				// $data['st_login']='0';
				// $data['tanggal_login']='';
				// $data['iddokter']='#';
				// $data['idpoli']='#';
				// $data['ruangan_id']='#';
			// }
			$data['list_ppa_all'] = $this->Tpoliklinik_resep_model->list_ppa_all();
			$data['list_tujuan'] 			= $this->Tfarmasi_tindakan_model->list_tujuan();
			$data['str_array'] 			= implode(",",array_column($data['list_tujuan'],'id'));
			$data['array_tujuan_antrian'] 			= implode(",",array_unique(array_column($data['list_tujuan'],'mtujuan_antrian_id')));
			// print_r(($data['array_tujuan_antrian']));exit;
			// print_r(count($data['list_tujuan']));exit;
			$data['list_poli'] 			= $this->Tfarmasi_tindakan_model->list_poli();
			$data['list_dokter'] 			= $this->Tfarmasi_tindakan_model->list_dokter();
			$data['jumlah_tujuan'] 			= count($data['list_tujuan']);
			
			$data['tujuan_farmasi_id'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['iddokter'] 			= '#';
			$data['no_medrec'] 			= '';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			$data['tab_utama'] 			= 11;
			$date1=date_create(date('Y-m-d'));
			$date2=date_create(date('Y-m-d'));
			date_add($date1,date_interval_create_from_date_string("0 days"));
			date_add($date2,date_interval_create_from_date_string("0 days"));
			$date1= date_format($date1,"Y-m-d");
			$date2= date_format($date2,"Y-m-d");


			$data['tanggal'] 			= DMYFormat($date1);
			$data['tanggal_2'] 			= DMYFormat($date2);
			$data['error'] 			= '';
			$data['title'] 			= 'Transaksi E-Resep Rawat Jalan';
			$data['content'] 		= 'Tfarmasi_tindakan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Transaksi E-Resep",'#'),
												  array("Rawat Jalan",'tfarmasi_tindakan')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function login(){
		$ruangan_id=$this->input->post('ruangan_id');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['iddokter']=$iddokter;
		$data['idpoli']=$idpoli;
		$data['user_login']=$user_id;
		$data['user_nama_login']=$user_nama_login;
		$data['tanggal_login']=date('Y-m-d H:i:s');
		$data['st_login']=1;
		$this->db->where('id',$ruangan_id);
		$hasil=$this->db->update('mtujuan',$data);
		// print_r($ruangan_id);
		$this->output->set_output(json_encode($hasil));
		
	}
		function logout(){
		$ruangan_id=$this->input->post('ruangan_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['user_login']=null;
		$data['user_nama_login']=null;
		$data['tanggal_login']='';
		$data['st_login']=0;
		// print_r($ruangan_id);
		$this->db->where('id',$ruangan_id);
		$hasil=$this->db->update('mtujuan',$data);
		$this->output->set_output(json_encode($hasil));
		
	}
	function getIndex_all()
	{
			insert_direct_poli();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$tab_utama =$this->input->post('tab_utama');
			$no_medrec =$this->input->post('no_medrec');
			$st_login =$this->input->post('st_login');
			$namapasien =$this->input->post('namapasien');
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tujuan_farmasi_id =$this->input->post('tujuan_farmasi_id');
			$str_array =$this->input->post('str_array');
			$tanggal =YMDFormat($this->input->post('tanggal'));
			$tanggal_2 =YMDFormat($this->input->post('tanggal_2'));
			$tab =$this->input->post('tab');
			if ($tanggal!=''){
				$where .=" AND DATE(H.tanggal_permintaan) >= '".YMDFormat($tanggal)."' AND DATE(H.tanggal_permintaan) <= '".YMDFormat($tanggal_2)."'";
			}
			if ($tujuan_farmasi_id!='#'){
				$where .=" AND (H.tujuan_farmasi_id) = '$tujuan_farmasi_id'";
			}else{
				$where .=" AND (H.tujuan_farmasi_id) IN (".$str_array.")";
				
			}
			if ($idpoli!='#'){
				$where .=" AND (MP.idtipe) = '$idpoli'";
			}
			if ($iddokter!='#'){
				$where .=" AND (H.iddokter_resep) = '$iddokter'";
			}
			if ($no_medrec!=''){
				$where .=" AND (MP.no_medrec) = '$no_medrec'";
			}
			if ($namapasien!=''){
				$where .=" AND (H.namapasien) LIKE '%".$namapasien."%'";
			}
			if ($tab=='2'){
				$where .=" AND (H.st_panggil) = '0'";
			}
			if ($tab=='3'){
				$where .=" AND (H.st_panggil) = '1'";
			}
			if ($tab=='4'){
				$where .=" AND (H.st_panggil) = '2'";
			}
			if ($tab_utama=='11'){
				$where .=" AND (H.status_tindakan) = '0'";
			}
			if ($tab_utama=='22'){
				$where .=" AND (H.status_tindakan) = '1'";
			}
			if ($tab_utama=='33'){
				$where .=" AND ((H.status_tindakan) = '2' OR (H.status_tindakan) = '3')";
			}
			if ($tab_utama=='44'){
				$where .=" AND (H.status_tindakan) = '4'";
			}
			if ($tab_utama=='55'){
				$where .=" AND (H.status_tindakan) = '5'";
			}
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT 
							UT.nama as nama_user_telaah,UP.nama as nama_user_proses,UE.nama as nama_user_etiket,UA.nama as nama_user_pengmabil,US.nama as nama_user_penyerah
							,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header,MPD.nama as nma_dokter_resep 
							,MP.no_medrec,MP.title,MP.jenis_kelamin,JK.ref as jk 
							,MD.nama as nama_dokter,P.nama as nama_poli
							
							,MP.umurhari,MP.umurbulan,MP.umurtahun
							,H.*,MP.tanggal_lahir,MP.tanggaldaftar,MP.idtipe
							,MR.nama as nama_rekanan,'0' as status_input_perawat,MP.tanggal
							,MK.nama as nama_kelompok,MP.idkelompokpasien
							,TIME_TO_SEC(TIMEDIFF(H.tanggal_penyerah, H.tanggal_proses_resep )) / 60 as selisih_layanan
							FROM `tpoliklinik_e_resep` H
							INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
							INNER JOIN merm_referensi JK ON JK.nilai=MP.jenis_kelamin AND JK.ref_head_id='1'
							INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
							LEFT JOIN mdokter MD ON MD.id=MP.iddokter
							LEFT JOIN mppa MPD ON MPD.id=H.iddokter_resep
							LEFT JOIN mpasien_kelompok MK ON MK.id=MP.idkelompokpasien
							LEFT JOIN mrekanan MR ON MR.id=MP.idrekanan
							LEFT JOIN mppa UT ON UT.id=H.user_telaah_resep
							LEFT JOIN mppa UP ON UP.id=H.user_proses
							LEFT JOIN mppa UE ON UE.id=H.user_etiket
							LEFT JOIN mppa UA ON UA.id=H.user_ambil
							LEFT JOIN mppa US ON US.id=H.user_penyerah
							LEFT JOIN (
										".get_alergi_sql()."
									) A ON A.idpasien=H.idpasien
							WHERE H.status_assemen='2' AND H.asalrujukan <> 3 ".$where."
							GROUP BY H.assesmen_id
							ORDER BY H.tanggal_kirim ASC
					) as tbl WHERE tanggal_kirim IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
	  // print_r($list);exit;
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_cetak_resep='';
		  $btn_print_lama='';
		  if ($r->idpenjualan){			  
			  if (UserAccesForm($user_acces_form,array('1209','1210','1211'))){
				if (UserAccesForm($user_acces_form,array('1209'))){
					$btn_print_lama .= '<li><a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->idpenjualan.'">Kwitansi</a></li>';
				}
			   if (UserAccesForm($user_acces_form,array('1210'))){
					$btn_print_lama .= '<li><a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->idpenjualan.'">Kwitansi Farmasi</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1211'))){
					$btn_print_lama .= '<li><a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->idpenjualan.'">E-Ticket</a></li>';
				}
				
			  }
		  }
		  if ($r->status_tindakan=='2' || $r->status_tindakan=='3'){
			   $btn_cetak_resep='
			   <li>
					<a  tabindex="-1" href="javascript:void(0)" onclick="set_petugas_pengambil('.$r->assesmen_id.',1)"> Petugas Ambil Obat</a>
				</li>
				<li>
					<a tabindex="-1" href="javascript:void(0)" onclick="set_petugas_etiket('.$r->assesmen_id.',2)"> Petugas Cetak E-Tiket</a>
				</li>
				

			  ';
		  }
		  $btn_panel_kendali='';
		  $btn_print_original='';
		  if ($r->status_tindakan=='2' || $r->status_tindakan=='3'){
			   $btn_print_original .='
					<li>
						<a href="'.base_url().'tfarmasi_tindakan/cetak_show/'.$r->assesmen_id.'/0/A4" target="_blank"> Cetak E-Resep</a>
					</li>
			  ';
		  }
		  $btn_print_original .='
				<li>
					<a href="'.base_url().'tfarmasi_tindakan/cetak_show/'.$r->assesmen_id.'/1/A4" target="_blank"> Cetak E-Resep Original</a>
				</li>
				<li>
					<a href="'.base_url().'tfarmasi_tindakan/cetak_show_copy/'.$r->assesmen_id.'/1/A4" target="_blank"> Copy E-Resep</a>
				</li>
		  ';

		  $btn_print_original_hvs='';
		  if ($r->status_tindakan=='2' || $r->status_tindakan=='3'){
			   $btn_print_original_hvs .='
					<li>
						<a href="'.base_url().'tfarmasi_tindakan/cetak_show/'.$r->assesmen_id.'/0/F4" target="_blank"> Cetak E-Resep HVS</a>
					</li>
			  ';
		  }
		  $btn_print_original_hvs .='
				<li>
					<a href="'.base_url().'tfarmasi_tindakan/cetak_show/'.$r->assesmen_id.'/1/F4" target="_blank"> Cetak E-Resep Original HVS</a>
				</li>
				<li>
					<a href="'.base_url().'tfarmasi_tindakan/cetak_show_copy/'.$r->assesmen_id.'/1/F4" target="_blank"> Copy E-Resep HVS</a>
				</li>
		  ';
		  // if ($r->status_tindakan=='2' || $r->status_tindakan=='3'){
		  $btn_panel_kendali='
			<div class="btn-group push-5-t">
				<button class="btn btn-warning btn-sm" type="button">PANEL KENDALI</button>
				<div class="btn-group">
					<button class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						
						'.$btn_cetak_resep.'
						'.$btn_print_lama.'
						'.$btn_print_original.'
						'.$btn_print_original_hvs.'
					</ul>
				</div>
			</div>
		  ';
		  // }
		  // tpendaftaran_poli_perawat/tindakan/74329/erm_rj/input_ttv
		  
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t push-b">
										<a href="'.base_url().'tfarmasi_tindakan/tindakan/'.$r->pendaftaran_id.'/erm_e_resep/input_telaah/'.$r->assesmen_id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block '.($r->status_tindakan < 4?'btn-primary':'btn-default').' btn-xs menu_click"><i class="fa '.($r->status_tindakan < 4?'fa-user-md':'fa-eye').'"></i> '.($r->status_tindakan < 4?'LAYANI':'LIHAT').'</a> 
									</div>
										'.$btn_panel_kendali.'
									';
									if ($r->status_tindakan=='2' || $r->status_tindakan=='3'){
									$btn_1 .='<div class="push-5-t"><button onclick="batalkan_transaksi('.$r->assesmen_id.','.$r->status_tindakan.','.$r->idpenjualan.')" type="button" data-toggle="tooltip" title="Call"  class="btn btn-block btn-danger btn-xs"><i class="fa fa-times"></i> '.($r->status_tindakan=='2'?'BATALKAN TRANSAKSI':'BATALKAN VALIDASI').'</button> </div>';
										
									}
									
								$btn_1 .='</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
										<button class="btn btn-success btn-sm" onclick="call_antrian_manual('.$r->assesmen_id.')" type="button" title="Recall"><i class="glyphicon glyphicon-volume-up"></i></button>
									
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button" '.($r->riwayat_alergi_header?'':'disabled').' onclick="lihat_his_alergi_pasien('.$r->idpasien.')">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" '.($r->riwayat_alergi_header?'':'disabled').' onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									'.GetPrioritasCito($r->prioritas_cito).' '.($r->st_direct=='1'?text_warning('TIDAK ADA E-RESEP'):'').'
									</div>
									<div class="text-danger push-5-t"><strong>'.$r->noresep.' | '.HumanDateShort($r->tanggal_permintaan).' | '.$r->nma_dokter_resep.'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $td_selisih='';
		  
		  if ($tab_utama=='44'){
			  $td_selisih='
				<tr>
					<td class="bg-white" style="vertical-align: top!important;">
						<div class="h5 text-danger">Diserahkan Oleh:</div>
						<div class=" text-muted "> '.($r->nama_user_penyerah).' <br>'.HumanDateLong($r->tanggal_penyerah).'</div>
					</td>
					<td class="bg-white" style="vertical-align: top!important;">
						<div class="h5 text-danger">Waktu Tunggu:</div>
						<div class=" text-muted "> '.number_format($r->selisih_layanan,1).' Menit</div>
					</td>
					
				</tr>
			  
			  ';
		  }
		  $btn_5='<table class="block-table text-left">
						<tbody>
							
							<tr>
								<td class="bg-white">
									<div class="h5 text-primary">Telaah Resep:</div>
									<div class=" text-muted "> '.($r->nama_user_telaah?$r->nama_user_telaah:text_danger('BELUM DITELAAH')).'</div>
									
									<div class="h5 push-10-t text-primary">Ambil:</div>
									<div class=" text-muted "> '.($r->nama_user_pengmabil?$r->nama_user_pengmabil:text_danger('BELUM DIINPUT')).'</div>
								</td>
								<td class="bg-white">
									<div class="h5 text-primary"> Input Harga:</div>
									<div class=" text-muted"> '.($r->nama_user_proses?$r->nama_user_proses:text_danger('BELUM DIPROSES')).'</div>
									<div class="h5  push-10-t text-primary"> E-Tiket:</div>
									<div class=" text-muted"> '.($r->nama_user_etiket?$r->nama_user_etiket:text_danger('BELUM DIISI')).'</div>
								</td>
								'.$td_selisih.'
							</tr>
						</tbody>
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->idkelompokpasien=='1'?'ASURANSI : '.$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.status_tindakan_farmasi($r->status_tindakan).'</div><br>
									<div class="h5 text-primary"> '.StatusPanggilFarmasi($r->st_panggil).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function batalkan_transaksi(){
	  $user_id=$this->session->userdata('user_id');
	  $user_name=$this->session->userdata('user_name');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $status_tindakan=$this->input->post('status_tindakan');
	  $idpenjualan=$this->input->post('idpenjualan');
	  if ($status_tindakan=='3'){
		  $data['status_tindakan']='2';
		  $data['st_validasi']='0';
	  }else{
		  $data['status_tindakan']='1';
		  $data['st_validasi']='0';
		  if ($idpenjualan){
			$data['idpenjualan']=null;
			$q="UPDATE tpasien_penjualan SET status='0',deleted_by='$user_id',deleted_nama='$user_name' WHERE id='$idpenjualan'";
			$this->db->query($q);
		  }
	  }
	   $this->db->where('assesmen_id',$assesmen_id);
	   $hasil= $this->db->update('tpoliklinik_e_resep',$data);
	   $this->output->set_output(json_encode($hasil));
  }
  function simpan_set(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $tipe=$this->input->post('tipe');
	  $mppa_id=$this->input->post('mppa_id');
	  if ($tipe=='2'){
		  $data['user_etiket']=$mppa_id;
		  $data['tanggal_etiket']=date('Y-m-d H:i:s');
	  }else{
		  $data['user_ambil']=$mppa_id;
		  $data['tanggal_ambil']=date('Y-m-d H:i:s');
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	 $hasil= $this->db->update('tpoliklinik_e_resep',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  public function get_dokter_poli()
	{
		$idpoli=$this->input->post('idpoli');
		$q="SELECT MD.id,MD.nama,CASE WHEN MT.id IS NOT NULL THEN 'disabled' ELSE '' END as disabel,MT.user_nama_login,MT.nama_tujuan FROM mpoliklinik_dokter H
			INNER JOIN mdokter MD ON MD.id=H.iddokter AND MD.`status`='1'
			LEFT JOIN mtujuan MT ON MT.st_login='1' AND MT.iddokter=H.iddokter AND MT.idpoli=H.idpoliklinik
			WHERE H.`status`='1' AND H.idpoliklinik='$idpoli'
			";
		$hasil = $this->db->query($q)->result();
		$opsi='<option value="#" selected>-Pilih Dokter-</option>';
		foreach($hasil as $r){
			$opsi .='<option value="'.$r->id.'" '.$r->disabel.'>'.$r->nama.($r->disabel?' ('.$r->nama_tujuan.' - '.$r->user_nama_login.')':'').'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	function get_tujuan_spesifik(){
		$tujuan_farmasi_id=$this->input->post('tujuan_farmasi_id');
		$q="SELECT H.mtujuan_antrian_id as tujuan_id FROM mtujuan_farmasi H WHERE H.id='$tujuan_farmasi_id'";
		$tujuan_id=$this->db->query($q)->row('tujuan_id');
		$this->output->set_output(json_encode($tujuan_id));
	}
	function refres_div_counter(){
		$tujuan_id =$this->input->post('tujuan_id');
		$tujuan_farmasi_id =$this->input->post('tujuan_farmasi_id');
		// $idpoli =$this->input->post('idpoli');
		// $iddokter =$this->input->post('iddokter');
		// $ruangan_id =$this->input->post('ruangan_id');
		// $st_login =$this->input->post('st_login');
		$tanggal =($this->input->post('tanggal'));
		$tanggal_2 =($this->input->post('tanggal_2'));
		
		
		// $tanggal =date('Y-m-d');
		$id_next='0';
		$id_layani='0';
		$div_tabel='';
		$where_tanggal=" AND DATE(H.tanggal_permintaan) >= '".YMDFormat($tanggal)."' AND DATE(H.tanggal_permintaan) <= '".YMDFormat($tanggal_2)."'";
		
		$q="SELECT H.assesmen_id as id,MP.namapasien,MP.no_medrec,MP.kode_antrian,MP.noantrian 
FROM `mtujuan` M
			INNER JOIN tpoliklinik_e_resep H ON H.assesmen_id=M.pendaftaran_dilayani
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			WHERE H.status_assemen='2' AND H.tujuan_id IN (".$tujuan_id.") AND H.asalrujukan <> '3' AND H.tujuan_farmasi_id IN (".$tujuan_farmasi_id.") ".$where_tanggal;
		$data_next=$this->db->query($q)->row_array();
		if ($data_next){
			$data_next['button_disabel']='';
			$id_layani=$data_next['id'];
		}else{
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		
		$div_tabel.='
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-danger" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >ANTRIAN DILAYANI</h3>
					</div>
					<div class="content-mini content-mini-full bg-danger">
						<div>
							<div class="h2 text-center text-white">'.$data_next['kode_antrian'].'</div>
							<div class="h5 text-center text-white">'.$data_next['no_medrec'].'</div>
							<div class="h5 text-center text-white push-10">'.$data_next['namapasien'].'</div>
							<div class="text-center ">
							</div>
						</div>
						<div class="text-center ">
								<button class="btn btn-block btn-success" '.$data_next['button_disabel'].' type="button" onclick="recall_manual('.$data_next['id'].')" title="Panggil Ulang"><i class="si si-refresh pull-left"></i> PANGGIL ULANG</button>
						</div>
					</div>
				</div>
			</div>
		';
		$q="SELECT H.assesmen_id as id,MP.namapasien,MP.no_medrec,MP.kode_antrian,MP.noantrian 
			FROM tpoliklinik_e_resep H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			WHERE H.status_assemen='2' AND H.tujuan_farmasi_id IN (".$tujuan_farmasi_id.") AND H.asalrujukan <> '3' AND H.st_panggil='0' ".$where_tanggal."

			ORDER BY H.tanggal_permintaan ASC
			LIMIT 1
			";
			// print_r($q);exit;
		$data_next=$this->db->query($q)->row_array();
		if ($data_next){
			$data_next['button_disabel']='';
			$id_next=$data_next['id'];
		}else{
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		// if ($st_login=='0'){
			// $data_next=array(
				// 'noantrian'=>'',
				// 'id'=>'',
				// 'kode_antrian'=>'-',
				// 'no_medrec'=>'-',
				// 'namapasien'=>'-',
				// 'button_disabel'=>'disabled',
			// );
		// }
		$div_tabel.='<div class="col-sm-3">
							<div class="block block-themed">
								<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
									<h3 class="block-title text-center" >ANTRIAN SELANJUTNYA</h3>
								</div>
								<div class="content-mini content-mini-full bg-success">
									<div>
										<div class="h2 text-center text-white">'.$data_next['kode_antrian'].'</div>
										<div class="h5 text-center text-white">'.$data_next['no_medrec'].'</div>
										<div class="h5 text-center text-white push-10">'.$data_next['namapasien'].'</div>
									</div>
									<div class="text-center ">
										<button class="btn btn-block btn-primary " onclick="call_antrian_manual('.$data_next['id'].')" '.$data_next['button_disabel'].' title="Lanjut Panggil" type="button"><i class="glyphicon glyphicon-volume-up pull-left"></i> '.($id_layani=='0'?'PANGGIL':'SELANJUTNYA').'</button>
									</div>
								</div>
								
							</div>
							
						</div>';
		$q="SELECT SUM(1) as total_antrian,SUM(CASE WHEN H.st_panggil='0' THEN 1 ELSE 0 END) as sisa_antrian
			FROM tpoliklinik_e_resep H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			WHERE  H.status_assemen='2' AND H.tujuan_farmasi_id IN (".$tujuan_farmasi_id.") AND H.asalrujukan <> '3' ".$where_tanggal."
			
			
			ORDER BY H.tanggal_permintaan ASC
			
			

			";
			// print_r($q);exit;
		$data_next=$this->db->query($q)->row_array();
		if ($id_layani=='0'){
			$button_disabel='disabled';
		}else{
			$button_disabel='';
			
		}
		if ($data_next['sisa_antrian']=='0'){
			$button_disabel='disabled';
			
		}
		// if ($st_login=='0'){
			// $button_disabel='disabled';
		// }
		$div_tabel.='
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >SISA ANTRIAN</h3>
					</div>
					<div class="content-mini content-mini-full bg-success">
						<div>
							<div class="h2 text-center text-white">'.$data_next['sisa_antrian'].'</div>
							<div class="h5 text-center text-white">&nbsp;</div>
							<div class="h5 text-center text-white push-10">&nbsp;</div>
						</div>
						<div class="text-center ">
							<button class="btn btn-block btn-warning" type="button" '.$button_disabel.' onclick="call_lewati('.$id_layani.','.$id_next.')"  title="LEWATI"><i class="fa fa-mail-forward pull-left"></i> LEWATI</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >TOTAL ANTRIAN</h3>
					</div>
					<div class="content-mini content-mini-full bg-success">
						<div>
							<div class="h1 font-w700 text-center text-white">'.$data_next['total_antrian'].'</div>
						</div>
					</div>
				</div>
			</div>
		';
		$this->output->set_output(json_encode($div_tabel));
	}
	function call_antrian(){
		$assesmen_id=$this->input->post('assesmen_id');
		
		$data=$this->db->query("
		SELECT H.assesmen_id,MP.namapasien,MP.no_medrec,MP.kode_antrian,MP.noantrian,H.tujuan_id 
			,DATE(H.tanggal_kirim) as tanggal,MP.idpoliklinik as idpoli,MP.iddokter 
			FROM tpoliklinik_e_resep H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			WHERE H.assesmen_id='$assesmen_id'
		")->row();
		$idpoli=$data->idpoli;
		$iddokter=$data->iddokter;
		$tujuan_id=$data->tujuan_id;
		$noantrian=$data->noantrian;
		$tanggal=$data->tanggal;
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM mtujuan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.tujuan_id='$tujuan_id' AND H.`status`='1'
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		
		if ($data){
			$kode_number=str_pad($noantrian,3,"0",STR_PAD_LEFT);
			// $this->db->where('id',$data->id);
			// $this->db->update('antrian_harian',array('st_panggil'=>1));
			$sound_play='';
			// $pelayanan_id=$data->antrian_id;
			
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_dokter H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_dokter_id
			WHERE H.idpoli='$idpoli' AND H.iddokter='$iddokter'";
			$file_dokter=$this->db->query($q1)->row('file');
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.idpoli='$idpoli' AND H.`status`='1'
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			if ($file_dokter){
				$file_1=$file_1.':'.$file_dokter;
			}
			
			$file_2=terbilang_sound($kode_number);
			$sound_play=($file_1?$file_1.':':'').($file_2?$file_2.':':'').$file_3;
			$data_insert=array(
				'transaksi_id' => $data->assesmen_id,
				'tipe' => 1,
				'tujuan_antrian_id' => $tujuan_id,
				'kode_antrian' => $data->kode_antrian,
				'noantrian' => $data->noantrian,
				'sound_play' => $sound_play,
				'tanggal' => date('Y-m-d'),
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			$this->db->insert('antrian_harian_khusus_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function call_lewati(){
		$antrian_id=$this->input->post('assesmen_id_next');
		$antrian_id_skip=$this->input->post('assesmen_id');
		$data_update=array(
			'st_panggil'=>2,
			'waktu_dipanggil'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('assesmen_id',$antrian_id_skip);
		$this->db->update('tpoliklinik_e_resep',$data_update);
		$data=$this->db->query("
		SELECT H.assesmen_id,MP.namapasien,MP.no_medrec,MP.kode_antrian,MP.noantrian,H.tujuan_id 
			,DATE(H.tanggal_kirim) as tanggal,MP.idpoliklinik as idpoli,MP.iddokter 
			FROM tpoliklinik_e_resep H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			WHERE H.assesmen_id='$antrian_id'
		")->row();
		$idpoli=$data->idpoliklinik;
		$iddokter=$data->iddokter;
		$noantrian=$data->noantrian;
		$tanggal=$data->tanggal;
		$tujuan_id=$this->input->post('tujuan_id');
		// $noantrian=$this->input->post('noantrian');
		// $tanggal='$tanggal';
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM mtujuan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.tujuan_id='$tujuan_id' AND H.`status`='1'
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		
		if ($data){
			$kode_number=str_pad($noantrian,3,"0",STR_PAD_LEFT);
			// $this->db->where('id',$data->id);
			// $this->db->update('antrian_harian',array('st_panggil'=>1));
			$sound_play='';
			// $pelayanan_id=$data->antrian_id;
			
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_dokter H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_dokter_id
			WHERE H.idpoli='$idpoli' AND H.iddokter='$iddokter'";
			$file_dokter=$this->db->query($q1)->row('file');
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.idpoli='$idpoli' AND H.`status`='1'
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			if ($file_dokter){
				$file_1=$file_1.':'.$file_dokter;
			}
			
			$file_2=terbilang_sound($kode_number);
			$sound_play=($file_1?$file_1.':':'').($file_2?$file_2.':':'').$file_3;
			$data_insert=array(
				'transaksi_id' => $data->assesmen_id,
				'tipe' => 1,
				'tujuan_antrian_id' => $tujuan_id,
				'kode_antrian' => $data->kode_antrian,
				'noantrian' => $data->noantrian,
				'sound_play' => $sound_play,
				'tanggal' => date('Y-m-d'),
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			$this->db->insert('antrian_harian_khusus_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function get_notif(){
		$tujuan_array=$this->input->post('tujuan_array');
		$q="SELECT GROUP_CONCAT(H.assesmen_id) assesmen_id ,GROUP_CONCAT( H.sound_play SEPARATOR ':') as sound_play FROM `tpoliklinik_e_resep` H WHERE H.status_assemen='2' AND H.st_notif_sound='0' AND H.sound_play!='' AND H.tujuan_farmasi_id IN (".$tujuan_array.")";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function update_notif(){
		$assesmen_id=$this->input->post('assesmen_id');
		$q="UPDATE tpoliklinik_e_resep SET st_notif_sound=1 WHERE assesmen_id IN (".$assesmen_id.")";
		$hasil=$this->db->query($q);
		$this->output->set_output(json_encode($hasil));
	}
	//HAPUS
	public function getIndex_penjualan()
    {
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$tanggaldari=$this->input->post('tanggaldari');
		$nopenjualan=$this->input->post('nopenjualan');
		$nomedrec=$this->input->post('no_medrec');
		$namapasien=$this->input->post('namapasien');
		$idasalpasien=$this->input->post('idasalpasien');
		$tanggaldari=$this->input->post('tanggaldari');
		$tanggalsampai=$this->input->post('tanggalsampai');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$status=$this->input->post('status');
		
        $this->select = array('tpasien_penjualan.*','trawatinap_pendaftaran.idtipe');

        $this->from   = 'tpasien_penjualan';

        $this->join 	=array(
          array('trawatinap_pendaftaran', 'tpasien_penjualan.idtindakan = trawatinap_pendaftaran.id AND tpasien_penjualan.asalrujukan = 3', 'LEFT')
        );

        // FILTER
        $this->where  = array('idtindakan <> '=>null);
        // if ($uri == 'filter') {
            if ($namapasien!= '') {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nama LIKE' => '%'.$this->session->userdata('namapasien').'%'));
            }
            if ($tanggaldari != '') {
                $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) >= ' => YMDFormat($tanggaldari)));
            }
            if ($tanggalsampai != '') {
                $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) <= ' => YMDFormat($tanggalsampai)));
            }
            if ($idasalpasien != "0") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.asalrujukan' => $idasalpasien));
            }
            if ($idkelompokpasien != "#") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.idkelompokpasien' => $idkelompokpasien));
            }
            if ($status != "0") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.status' => $status));
            }
			if ($nopenjualan != "") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nopenjualan' => $nopenjualan));
            }
			if ($nomedrec != "") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nomedrec' => $nomedrec));
            }
        // } else {
			// $tgl_sampai=date('Y-m-d', strtotime("-1 week"));
			// $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) >=' => YMDFormat($tgl_sampai)));
            // $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) <=' => date("Y-m-d")));
        // }

        $this->order  = array(
            'tpasien_penjualan.id' => 'DESC'
          );

        $this->group  = array();

        $this->column_search   = array('tpasien_penjualan.tanggal','tpasien_penjualan.nopenjualan','tpasien_penjualan.idpasien','tpasien_penjualan.nama','tpasien_penjualan.alamat','tpasien_penjualan.notelp');
        $this->column_order   = array('tpasien_penjualan.tanggal','tpasien_penjualan.nopenjualan','tpasien_penjualan.idpasien','tpasien_penjualan.nama','tpasien_penjualan.alamat','tpasien_penjualan.notelp');
        $list = $this->datatable->get_datatables();

        // print_r($this->db->last_query());exit();

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $closed = '';
            if ($r->status == 0) {
                $action = '<label>-</label>';
            } elseif ($r->status == 1) {
                $action .= '<div class="btn-group">';
                if (UserAccesForm($user_acces_form,array('1046'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/create/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Tindakan"><i class="fa fa-pencil"></i></a>';
                }
                if (UserAccesForm($user_acces_form,array('1208'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/batalkan/'.$r->id.'" data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm" onclick="remove_row(this); return true;"><i class="fa fa-trash"></i></a>';
                }
                $action .= '</div>';
            } elseif ($r->status == 2) {
                $action .= '<div class="btn-group">';
                if (UserAccesForm($user_acces_form,array('1207'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/serahkan/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Serahkan" onclick="serahkan_row(this); return true;"><i class="fa fa-check"></i></a>';
                }
				if (UserAccesForm($user_acces_form,array('1209','1210','1211'))){
                $action .= '<div class="btn-group">';
                $action .= '<div class="btn-group dropup">';
                $action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"><span class="fa fa-print"></span></button>';
                $action .= '<ul class="dropdown-menu"><li>';
                if (UserAccesForm($user_acces_form,array('1209'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->id.'">Kwitansi</a>';
                }
               if (UserAccesForm($user_acces_form,array('1210'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->id.'">Kwitansi Farmasi</a>';
                }
                if (UserAccesForm($user_acces_form,array('1211'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->id.'">E-Ticket</a>';
                }
                $action .= '</li></ul>';
                $action .= '</div>';
                $action .= '</div>';
				}
                $action .= '&nbsp;';
                $action .= '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('1206'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
                }
				if (UserAccesForm($user_acces_form,array('1208'))){
					if ($r->closed_by_kasir =='0'){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/batalkan/'.$r->id.'" data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm" onclick="remove_row(this); return true;"><i class="fa fa-trash"></i></a>';
					}
			   }
                $action .= '</div>';
            } else {
                if ($r->closed_by_kasir==0) {
                    $closed.='<div class="btn-group">';
                    if (UserAccesForm($user_acces_form,array('1206'))){
                        $closed.='<a href="'.site_url().'tpasien_penjualan/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
                    }
                    $closed.='</div>';
                } else {
                    $closed='';
                }
				if (UserAccesForm($user_acces_form,array('1209','1210','1211'))){
                $action .= '<div class="btn-group">';
                $action .= '<div class="btn-group dropup">';
                $action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="fa fa-print"></span> </button>';
                $action .= '<ul class="dropdown-menu"> <li>';
                if (UserAccesForm($user_acces_form,array('1209'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->id.'">Kwitansi</a>';
                }
                if (UserAccesForm($user_acces_form,array('1210'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->id.'">Kwitansi Farmasi</a>';
                }
                if (UserAccesForm($user_acces_form,array('1211'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->id.'">E-Ticket</a>';
                }
                $action .= '</li></ul>';
                $action .= '</div>';
                $action .= '</div>';
				}
                $action .= $closed;
            }

            $row[] = $no;
            $row[] = $r->tanggal;
            $row[] = $r->nopenjualan;
            $row[] = $r->nomedrec;
            $row[] = $r->nama;

            if ($r->idtipe=='2') {
                $row[] ='One Day Surgery (ODS)';
            } else {
                $row[] = GetAsalRujukan($r->asalrujukan);
            }

            $row[] = StatusPembelianFarmasi($r->status);

            $row[] = $action;

            $data[] = $row;
        }
        $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->datatable->count_all(),
              "recordsFiltered" => $this->datatable->count_all(),
              "data" => $data
            );
        echo json_encode($output);
    }
	function tindakan($pendaftaran_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		// print_r($this->session->userdata());exit;
		$data_login_ppa=get_ppa_login();
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id);
		// print_r($data);exit;
		$data['tittle']='E-RESEP';
		$def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		
		
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Tindakan';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
			  array("RSKB Halmahera",'#'),
			  array("Poliklinik Tindakan ",'#'),
			  array("Input E-RESEP",'tpoliklinik_trx')
			);
		$data['tanggal_transaksi']=date('d-m-Y H:i:s');
		$data['pendaftaran_id']=$pendaftaran_id;
		
		if ($menu_kiri=='input_eresep' || $menu_kiri=='his_eresep'){
			$data['breadcrum'] 	= array(
			  array("RSKB Halmahera",'#'),
			  array("Poliklinik Tindakan ",'#'),
			  array("E-RESEP",'tpoliklinik_trx')
			);

			$data_label=$this->Tpoliklinik_resep_model->setting_label();
			$data = array_merge($data,$data_label);
			
			$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='1' AND H.tipepegawai='2' AND pegawai_id='".$data['iddokter']."'";
			$data_ppa=$this->db->query($q)->row();
			// print_r($data);exit;
			if ($data_ppa){
				$iddokter_resep=$data_ppa->id;
			}else{
				$iddokter_resep=0;
				
			}
			
			if ($trx_id!='#'){
				
					$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen_trx($trx_id);
					$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
					$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep_all();
					
			}else{
				$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen($pendaftaran_id);
				$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
				$data['list_ppa_all'] = $this->Tpoliklinik_resep_model->list_ppa_all();
				$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep($data['idtipe_poli'],$data['idpoliklinik'],$iddokter_resep);
			}
			if ($data_assemen){
				$data['racikan_id']='';
				$q="SELECT racikan_id,nama_racikan,jumlah_racikan,jenis_racikan,interval_racikan,rute_racikan,aturan_tambahan_racikan,iter_racikan,status_racikan
					FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='".$data_assemen['assesmen_id']."' AND H.status_racikan='1'";	
				$data_racikan=$this->db->query($q)->row_array();
				if ($data_racikan){}else{
					$data_racikan['nama_racikan']='';
					$data_racikan['jumlah_racikan']='';
					$data_racikan['jenis_racikan']='';
					$data_racikan['interval_racikan']='';
					$data_racikan['rute_racikan']='';
					$data_racikan['aturan_tambahan_racikan']='';
					$data_racikan['iter_racikan']='';
					$data_racikan['status_racikan']='';
				}
					$data = array_merge($data,$data_racikan);
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['racikan_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				
			}
			$data_assemen['trx_id']=$trx_id;
			
			$setting_assesmen=$this->Tpoliklinik_resep_model->setting_eresep();
			$data = array_merge($data,$data_assemen,$setting_assesmen);
		}
		if ($menu_kiri=='input_telaah'){
			$data['breadcrum'] 	= array(
			  array("RSKB Halmahera",'#'),
			  array("Poliklinik Tindakan ",'#'),
			  array("VERIFIKASI RESEP",'tpoliklinik_trx')
			);

			$data_label=$this->Tpoliklinik_resep_model->setting_label();
			$data = array_merge($data,$data_label);
			
		
			
			if ($trx_id!='#'){
				$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen_trx($trx_id);
				$this->update_stok($trx_id);
				$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
				$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep_all();
				$data['list_telaah']=$this->Tpoliklinik_resep_model->get_telaah($data_assemen['assesmen_id']);
				$data['list_telaah_obat']=$this->Tpoliklinik_resep_model->get_telaah_obat($data_assemen['assesmen_id']);
				
				$q="SELECT racikan_id,nama_racikan,jumlah_racikan,jenis_racikan,interval_racikan,rute_racikan,aturan_tambahan_racikan,iter_racikan,status_racikan
					FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='".$data_assemen['assesmen_id']."' AND H.status_racikan='1'";	
				$data_racikan=$this->db->query($q)->row_array();
				if ($data_racikan){}else{
					$data_racikan['racikan_id']='';
					$data_racikan['nama_racikan']='';
					$data_racikan['jumlah_racikan']='';
					$data_racikan['jenis_racikan']='';
					$data_racikan['interval_racikan']='';
					$data_racikan['rute_racikan']='';
					$data_racikan['aturan_tambahan_racikan']='';
					$data_racikan['iter_racikan']='';
					$data_racikan['status_racikan']='';
				}
				$data = array_merge($data,$data_racikan);
					
			}
			// print_r($data_assemen);exit;
			$data_assemen['trx_id']=$trx_id;
			$data['list_ppa_all'] = $this->Tpoliklinik_resep_model->list_ppa_all();
			$setting_assesmen=$this->Tpoliklinik_resep_model->setting_eresep();
			$data = array_merge($data,$data_assemen,$setting_assesmen);
		}
		
		$pegawai_id=$this->session->userdata('login_pegawai_id');
		$data['list_template_assement']=array();
		$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
		$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
		$data = array_merge($data, backend_info());
		$data['trx_id']='#';
		$this->parser->parse('module_template', $data);
	}
	function update_stok($assesmen_id){
		$q="
			UPDATE tpoliklinik_e_resep_obat M INNER JOIN (
			SELECT G.stok,H.* 
			FROM tpoliklinik_e_resep_obat H
			LEFT JOIN mgudang_stok G ON G.idunitpelayanan=H.idunit AND G.idtipe=H.idtipe AND G.idbarang=H.idbarang 
			WHERE H.assesmen_id='$assesmen_id' AND H.stok_asal IS NULL
			) T ON T.id=M.id
			SET M.stok_asal=T.stok
		";
		$this->db->query($q);
	}
	public function cetak_show($assesmen_id,$st_original='0',$ukuran='A4',$st_cetak='0'){
		// $st_cetak='0';
		// print_r($this->session->userdata());exit;
		$data['assesmen_id']=$assesmen_id;
		$data['st_original']=$st_original;
		$data['st_cetak']=$st_cetak;
		$data['ukuran']=$ukuran;
		$data['base_url'] = $this->config->base_url();
		if ($st_cetak == '1') {
			$data_cetak=$this->Tfarmasi_tindakan_model->update_cetak($assesmen_id, $st_original);
		}
		$data = array_merge($data, backend_info());
		$this->load->view('Tfarmasi_tindakan/print_show', $data);
	}
	public function cetak_resep($assesmen_id,$st_original='0',$ukuran='A4'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *

			FROM setting_eresep_print H ";
			// print_r($q);exit;
		$data=$this->db->query($q)->row_array();
		$data['st_original']=$st_original;
		$data['st_alergi_setting']=$data['st_alergi'];
		$data['st_puasa_setting']=$data['st_puasa'];
		$q="SELECT 
			H.assesmen_id,H.noresep,H.created_date,MP.namapasien,MP.no_medrec,H.iddokter_resep,MP.idkelompokpasien,H.nama_penerima,H.assesmen_id_resep
			,JK.ref as jeniskelamin,MP.nopendaftaran,MP.tanggal,MP.tanggal_lahir,CONCAT(MP.umurtahun,' Tahun ',MP.umurbulan,' Bulan ',MP.umurhari,' Hari ') as umur
			,MK.nama as nama_kelompok,MR.nama as nama_rekanan
			,H.alergi,H.diagnosa,H.berat_badan,H.tinggi_badan,H.luas_permukaan,MY.ref as menyusui
			,PS.ref as puasa,RP.ref as resep_pulang,PR.ref prioritas,GJ.ref as ginjal,H.catatan_resep,CONCAT(MD.nama,' - ',MD.nip) as dokter,(MD.nama) as nama_pemberi_rsep,MD.id as ppa_resep,MD.nip as nik_dokter
			,H.penerima_ttd,H.user_validasi,H.user_proses,H.user_penyerah,H.user_ambil,H.user_etiket,H.user_penyerah
			,H.user_telaah_resep,H.tanggal_telaah_resep,H.tanggal_validasi,H.status_tindakan
			FROM tpoliklinik_e_resep H 
			
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN merm_referensi JK ON JK.nilai=MP.jenis_kelamin AND JK.ref_head_id='1'
			INNER JOIN mpasien_kelompok MK ON MK.id=MP.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=MP.idrekanan
			LEFT JOIN merm_referensi MY ON MY.nilai=H.menyusui AND MY.ref_head_id='107'
			LEFT JOIN merm_referensi PS ON PS.nilai=H.st_puasa AND PS.ref_head_id='102'
			LEFT JOIN merm_referensi RP ON RP.nilai=H.resep_pulang AND RP.ref_head_id='104'
			LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
			LEFT JOIN merm_referensi GJ ON GJ.nilai=H.gangguan_ginjal AND GJ.ref_head_id='101'
			LEFT JOIN mppa MD ON MD.id=H.iddokter_resep
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_assemen=$this->db->query($q)->row_array();
		// print_r($data_assemen);exit;
		$q_obat="
			SELECT H.id
				,B.nama as nama_barang,B.hargadasar
				,H.idtipe,H.idbarang,H.nama,H.dosis,H.jumlah,H.`interval`,MI.nama as nama_interval,MI.jumlah_setiap_pemberian,SL.nama as satuan_label,SD.nama as satuan_dosis
				,MR.ref as nama_rute,H.aturan_tambahan
				,CASE WHEN H.kuantitas IS NOT NULL THEN H.kuantitas ELSE H.jumlah END as kuantitas
				,H.waktu_minum,BS.ref sediaan_nama
				,H.diskon,H.diskon_rp,H.harga_jual,H.harga_dasar,H.tuslah,H.expire_date,H.totalharga,H.cover
				,H.sisa_obat,H.kegunaan,H.iter,HH.st_ambil	
				FROM tpoliklinik_e_resep_obat H
				LEFT JOIN `tpoliklinik_e_resep` HH ON HH.assesmen_id=H.assesmen_id
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN minterval MI ON MI.id=H.`interval`
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				LEFT JOIN msatuan SL ON SL.id=B.idsatuanlabel
				LEFT JOIN msatuan SD ON SD.id=B.idsatuandosis
				WHERE H.assesmen_id='$assesmen_id' 
				ORDER BY H.id ASC
		";
		$data['list_obat']=$this->db->query($q_obat)->result();
		$q_obat="
			SELECT H.racikan_id,H.nama_racikan,H.jumlah_racikan,JS.ref as jenis_racik_nama,MD.deskripsi as racikan_des
				,H.interval_racikan `interval`,MI.nama as nama_interval,MI.jumlah_setiap_pemberian
				,MR.ref as nama_rute,H.aturan_tambahan_racikan as aturan_tambahan
				,H.waktu_minum,H.iter_racikan as iter
				FROM tpoliklinik_e_resep_obat_racikan H
			
				LEFT JOIN minterval MI ON MI.id=H.`interval_racikan`				
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute_racikan AND MR.ref_head_id='74'
				LEFT JOIN merm_referensi JS ON JS.nilai=H.jenis_racikan AND JS.ref_head_id='105'
				LEFT JOIN mracikan_des MD ON MD.jenis_racik_id=H.jenis_racikan
				
				WHERE H.assesmen_id='$assesmen_id' 
				ORDER BY H.racikan_id ASC
		";
		$data['list_obat_racikan']=$this->db->query($q_obat)->result();
		$q="SELECT count(id) as printed_number FROM tpoliklinik_e_resep_print_his WHERE assesmen_id='$assesmen_id' AND st_original='$st_original'";
		$data['printed_number']=$this->db->query($q)->row('printed_number');
		$data['login_nama_ppa']=$this->session->userdata('login_nama_ppa');
		$data['list_telaah']=$this->Tpoliklinik_resep_model->get_telaah($assesmen_id);
		$data['list_telaah_obat']=$this->Tpoliklinik_resep_model->get_telaah_obat($assesmen_id);
		$data['list_obat_perubahan']=$this->Tfarmasi_tindakan_model->list_obat_perubahan($assesmen_id);
		// print_r($ukuran);exit;
		if ($ukuran=='A4'){
			$size="210mm 297mm";
		}else{
			$size="150mm 210mm";
		}
		$data['ukuran']=$size;
		// print_r($data['ukuran']);exit;
		$data=array_merge($data,$data_assemen);
        $data['title']=$data['judul_header_ina'];
		$data = array_merge($data, backend_info());
		
        $html = $this->load->view('Tfarmasi_tindakan/pdf_resep', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
		
			
			$dompdf->setPaper('A4', 'portrait');

		
        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
	}
	public function cetak_show_copy($assesmen_id,$st_original='0',$ukuran='A4',$st_cetak='0'){
		// $st_cetak='0';
		// print_r($this->session->userdata());exit;
		$data['assesmen_id']=$assesmen_id;
		$data['st_original']=$st_original;
		$data['st_cetak']=$st_cetak;
		$data['ukuran']=$ukuran;
		$data['base_url'] = $this->config->base_url();
		if ($st_cetak == '1') {
			$data_cetak=$this->Tfarmasi_tindakan_model->update_cetak_copy($assesmen_id, $st_original);
		}
		$data = array_merge($data, backend_info());
		$this->load->view('Tfarmasi_tindakan/print_show_copy', $data);
	}
	public function cetak_resep_copy($assesmen_id,$st_original='0',$ukuran='A4'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *

			FROM setting_eresep_copy H ";
		$data=$this->db->query($q)->row_array();
		$data['st_original']=$st_original;
		$data['st_alergi_setting']=$data['st_alergi'];
		$data['st_puasa_setting']=$data['st_puasa'];
		$q="SELECT 
			H.assesmen_id,H.noresep,H.created_date,MP.namapasien,MP.no_medrec,H.iddokter_resep,MP.idkelompokpasien,H.nama_penerima,H.assesmen_id_resep
			,JK.ref as jeniskelamin,MP.nopendaftaran,MP.tanggal,MP.tanggal_lahir,CONCAT(MP.umurtahun,' Tahun ',MP.umurbulan,' Bulan ',MP.umurhari,' Hari ') as umur
			,MK.nama as nama_kelompok,MR.nama as nama_rekanan
			,H.alergi,H.diagnosa,H.berat_badan,H.tinggi_badan,H.luas_permukaan,MY.ref as menyusui
			,PS.ref as puasa,RP.ref as resep_pulang,PR.ref prioritas,GJ.ref as ginjal,H.catatan_resep,CONCAT(MD.nama,' - ',MD.nip) as dokter,(MD.nama) as nama_pemberi_rsep,MD.id as ppa_resep,MD.nip as nik_dokter
			,H.penerima_ttd,H.user_validasi,H.user_proses,H.user_penyerah,H.user_ambil,H.user_etiket,H.user_penyerah
			,H.user_telaah_resep,H.tanggal_telaah_resep,H.tanggal_validasi,H.status_tindakan
			FROM tpoliklinik_e_resep H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN merm_referensi JK ON JK.nilai=MP.jenis_kelamin AND JK.ref_head_id='1'
			INNER JOIN mpasien_kelompok MK ON MK.id=MP.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=MP.idrekanan
			LEFT JOIN merm_referensi MY ON MY.nilai=H.menyusui AND MY.ref_head_id='107'
			LEFT JOIN merm_referensi PS ON PS.nilai=H.st_puasa AND PS.ref_head_id='102'
			LEFT JOIN merm_referensi RP ON RP.nilai=H.resep_pulang AND RP.ref_head_id='104'
			LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
			LEFT JOIN merm_referensi GJ ON GJ.nilai=H.gangguan_ginjal AND GJ.ref_head_id='101'
			INNER JOIN mppa MD ON MD.id=H.iddokter_resep
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_assemen=$this->db->query($q)->row_array();
		// print_r($data);exit;
		$q_obat="
			SELECT 
			H.id
			,B.nama as nama_barang,B.hargadasar
			,H.idtipe,H.idbarang,H.nama,H.dosis,H.jumlah,H.`interval`,MI.nama as nama_interval,MI.jumlah_setiap_pemberian,SL.nama as satuan_label,SD.nama as satuan_dosis
			,MR.ref as nama_rute,H.aturan_tambahan
			,CASE WHEN H.kuantitas IS NOT NULL THEN H.kuantitas ELSE H.jumlah END as kuantitas
			,H.waktu_minum,BS.ref sediaan_nama
			,H.diskon,H.diskon_rp,H.harga_jual,H.harga_dasar,H.tuslah,H.expire_date,H.totalharga,H.cover
			,H.sisa_obat,H.kegunaan,H.iter,HH.st_ambil

			FROM `tpoliklinik_e_resep_obat` H
			LEFT JOIN `tpoliklinik_e_resep` HH ON HH.assesmen_id=H.assesmen_id
			LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
			LEFT JOIN minterval MI ON MI.id=H.`interval`
			LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
			LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
			LEFT JOIN msatuan SL ON SL.id=B.idsatuanlabel
			LEFT JOIN msatuan SD ON SD.id=B.idsatuandosis
			WHERE 
			H.assesmen_id='$assesmen_id'
			AND ((((H.iter > 0 AND H.ambil_iter<H.iter) OR (H.sisa_obat > 0)) AND HH.st_ambil='1') OR (HH.st_ambil='2'))
			
			ORDER BY H.id ASC
		";
		$data['list_obat']=$this->db->query($q_obat)->result();
		$q_obat="
			SELECT H.racikan_id,H.nama_racikan,H.jumlah_racikan,JS.ref as jenis_racik_nama,MD.deskripsi as racikan_des
				,H.interval_racikan `interval`,MI.nama as nama_interval,MI.jumlah_setiap_pemberian
				,MR.ref as nama_rute,H.aturan_tambahan_racikan as aturan_tambahan
				,H.waktu_minum,H.iter_racikan as iter
				FROM tpoliklinik_e_resep_obat_racikan H
			
				LEFT JOIN minterval MI ON MI.id=H.`interval_racikan`				
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute_racikan AND MR.ref_head_id='74'
				LEFT JOIN merm_referensi JS ON JS.nilai=H.jenis_racikan AND JS.ref_head_id='105'
				LEFT JOIN mracikan_des MD ON MD.jenis_racik_id=H.jenis_racikan
				
				WHERE H.assesmen_id='$assesmen_id' AND H.iter_racikan > 0 
				ORDER BY H.racikan_id ASC
		";
		$data['list_obat_racikan']=$this->db->query($q_obat)->result();
		$q="SELECT count(id) as printed_number FROM tpoliklinik_e_resep_print_copy_his WHERE assesmen_id='$assesmen_id' AND st_original='$st_original'";
		$data['printed_number']=$this->db->query($q)->row('printed_number');
		$data['login_nama_ppa']=$this->session->userdata('login_nama_ppa');
		$data['login_nip_ppa']=$this->session->userdata('login_nip_ppa');
		$data['login_ppa_id']=$this->session->userdata('login_ppa_id');
		$data['list_telaah']=$this->Tpoliklinik_resep_model->get_telaah($data_assemen['assesmen_id']);
		$data['list_telaah_obat']=$this->Tpoliklinik_resep_model->get_telaah_obat($data_assemen['assesmen_id']);
		$data['list_obat_perubahan']=$this->Tfarmasi_tindakan_model->list_obat_perubahan($data_assemen['assesmen_id']);
		if ($ukuran=='A4'){
			$size="210mm 297mm";
		}else{
			$size="150mm 210mm";
		}
		$data['ukuran']=$size;
		// print_r($data['list_obat_racikan']);
		$data=array_merge($data,$data_assemen);
        $data['title']=$data['judul_header_ina'];
		$data = array_merge($data, backend_info());
		
        $html = $this->load->view('Tfarmasi_tindakan/pdf_resep_copy', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
	}
}
