<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;
use Dompdf\Options;

class Term_laboratorium_umum extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');

        $this->load->model('Term_laboratorium_umum_model', 'model');
        $this->load->model('Term_laboratorium_model', 'global_model');
        $this->load->model('Term_laboratorium_umum_permintaan_model', 'permintaan_model');
        $this->load->model('Trujukan_laboratorium_model', 'rujukan_model');
        $this->load->model('Tpendaftaran_poli_ttv_model', 'poli_ttv_model');
    }

    public function tindakan($asal_rujukan = '', $pendaftaran_id = '', $menu_atas = '', $menu_kiri = '', $transaksi_id = '#', $status_form = ''): void
    {
        $ref = isset($_GET['ref']) ? $_GET['ref'] : '';
        $ref_pendaftaran_id = isset($_GET['ref_pendaftaran_id']) ? $_GET['ref_pendaftaran_id'] : '';
        $ref_asal_rujukan = isset($_GET['ref_asal_rujukan']) ? $_GET['ref_asal_rujukan'] : '';
        $pendaftaran_id = $ref_pendaftaran_id ? $ref_pendaftaran_id : $pendaftaran_id;
        $asal_rujukan = $ref_asal_rujukan ? ($ref_asal_rujukan == '1' || $ref_asal_rujukan == '2' ? 'rawat_jalan' : 'rawat_inap')  : $asal_rujukan;
        $data = $this->_getRequired($asal_rujukan, $pendaftaran_id, $menu_atas, $menu_kiri);

        $data['ref_asal_rujukan'] = $ref_asal_rujukan;
        $data['asal_rujukan_status'] = $asal_rujukan;
        if ($asal_rujukan == 'rawat_jalan') {
            $data['pendaftaran_id'] = $data['pendaftaran_id'];
            $data['pasien_id'] = get_by_field('id', $pendaftaran_id, 'tpoliklinik_pendaftaran')->idpasien;
        } else {
            // $data['pendaftaran_id'] = $data['pendaftaran_id'];
            $data['pendaftaran_id'] = $data['pendaftaran_id_ranap'];
            $data['pasien_id'] = get_by_field('id', $pendaftaran_id, 'trawatinap_pendaftaran')->idpasien;
        }
        
        $data['error'] = '';
        $data['title'] = 'My Pasien';
        $data['content'] = 'Tpendaftaran_poli_ttv/menu/_app_header';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Tindakan Poliklinik', '#'],
            ['My Pasien', 'tpendaftaran_poli_ttv'],
        ];

        if ('#' != $transaksi_id) {
            $data['status_form'] = $status_form;
            if ('input_permintaan' == $status_form) {
                $data['label_form'] = 'Input Permintaan';
            } elseif ('edit_permintaan' == $status_form) {
                $data['label_form'] = 'Ubah Permintaan';
            } elseif ('lihat_permintaan' == $status_form) {
                $data['label_form'] = 'Lihat Permintaan';
            }

            $data_permintaan = $this->model->get_data_transaksi($transaksi_id);

            $data = array_merge($data, [
                'transaksi_id' => $data_permintaan->id,
                'jenis_pemeriksaan' => $ref == 'planorder' ? 1 : $data_permintaan->jenis_pemeriksaan,
                'rencana_pemeriksaan' => $ref == 'planorder' ? date("d/m/Y") : DMYFormat($data_permintaan->rencana_pemeriksaan),
                'tanggal_waktu_pembuatan' => $data_permintaan->created_at,
                'informasi_ppa' => $data_permintaan->nip_ppa.' - '.$data_permintaan->created_ppa,
                'tujuan_laboratorium' => $data_permintaan->tujuan_laboratorium,
                'dokter_peminta_id' => $data_permintaan->dokter_peminta_id,
                'diagnosa' => $data_permintaan->diagnosa,
                'catatan_permintaan' => $data_permintaan->catatan_permintaan,
                'tanggal_permintaan' => DMYFormat($data_permintaan->waktu_permintaan),
                'waktu_permintaan' => HISTimeFormat($data_permintaan->waktu_permintaan),
                'prioritas' => $data_permintaan->prioritas,
                'pasien_puasa' => $data_permintaan->pasien_puasa,
                'pengiriman_hasil' => $data_permintaan->pengiriman_hasil,
                'tanggal_pengambilan_sample' => $data_permintaan->waktu_pengambilan_sample ? DMYFormat($data_permintaan->waktu_pengambilan_sample) : date('d/m/Y'),
                'waktu_pengambilan_sample' => $data_permintaan->waktu_pengambilan_sample ? HISTimeFormat($data_permintaan->waktu_pengambilan_sample) : date('H:i:s'),
                'petugas_pengambilan_sample' => $data_permintaan->petugas_pengambilan_sample,
                'nomor_laboratorium' => $data_permintaan->nomor_laboratorium,
                'dokter_laboratorium' => $data_permintaan->dokter_laboratorium,
                'dokter_penanggung_jawab' => $data_permintaan->dokter_penanggung_jawab,
                'selectedPemeriksaan' => $this->model->getSelectedPemeriksaan($transaksi_id),
            ]);
        } else {
            $data_draft = $this->model->get_data_transaksi($pendaftaran_id, $asal_rujukan, 'draft');
            
            $data = array_merge($data, [
                'status_form' => $data_draft ? 'draft' : '',
                'label_form' => $data_draft ? '' : '',
                'transaksi_id' => $data_draft ? $data_draft->id : '#',
                'jenis_pemeriksaan' => $data_draft ? $data_draft->jenis_pemeriksaan : '',
                'rencana_pemeriksaan' => $data_draft ? DMYFormat($data_draft->rencana_pemeriksaan) : date('d/m/Y'),
                'tanggal_waktu_pembuatan' => $data_draft ? $data_draft->created_at : date('d/m/Y H:i:s'),
                'informasi_ppa' => $data_draft ? $data_draft->nip_ppa.' - '.$data_draft->created_ppa : $data['login_nip_ppa'].' - '.$data['login_nama_ppa'],
                'tujuan_laboratorium' => $data_draft ? $data_draft->tujuan_laboratorium : '',
                'dokter_peminta_id' => $data_draft ? $data_draft->dokter_peminta_id : '',
                'diagnosa' => $data_draft ? $data_draft->diagnosa : '',
                'catatan_permintaan' => $data_draft ? $data_draft->catatan_permintaan : '',
                'tanggal_permintaan' => $data_draft ? ($data_draft->waktu_permintaan ? DMYFormat($data_draft->waktu_permintaan) : date('d/m/Y')) : date('d/m/Y'),
                'waktu_permintaan' => $data_draft ? ($data_draft->waktu_permintaan ? HISTimeFormat($data_draft->waktu_permintaan) : date('H:i:s')) : date('H:i:s'),
                'prioritas' => $data_draft ? $data_draft->prioritas : '',
                'pasien_puasa' => $data_draft ? $data_draft->pasien_puasa : '',
                'pengiriman_hasil' => $data_draft ? $data_draft->pengiriman_hasil : '',
                'tanggal_pengambilan_sample' => $data_draft ? ($data_draft->waktu_pengambilan_sample ? DMYFormat($data_draft->waktu_pengambilan_sample) : date('d/m/Y')) : date('d/m/Y'),
                'waktu_pengambilan_sample' => $data_draft ? ($data_draft->waktu_pengambilan_sample ? HISTimeFormat($data_draft->waktu_pengambilan_sample) : date('H:i:s')) : date('H:i:s'),
                'petugas_pengambilan_sample' => $data_draft ? $data_draft->petugas_pengambilan_sample : '',
                'nomor_laboratorium' => $data_draft ? $data_draft->nomor_laboratorium : '',
                'dokter_laboratorium' => $data_draft ? $data_draft->dokter_laboratorium : '',
                'dokter_penanggung_jawab' => $data_draft ? $data_draft->dokter_penanggung_jawab : '',
                'selectedPemeriksaan' => $data_draft ? $this->model->getSelectedPemeriksaan($data_draft->id) : [],
            ]);
        }

        $tipe_layanan = '1'; // Laboratorium
        $data['head_parent'] = $this->rujukan_model->getHeadParentTarifLaboratorium(1, $data['idkelompokpasien'], $data['idrekanan']);
        $data['list_tujuan_laboratorium'] = $this->model->logicTujuanLaboratorium($tipe_layanan, $data['idtipe'], $data['idpoliklinik'], $data['dpjp']);

        $data['pengaturan_form'] = (array) get_row('merm_pengaturan_form_order_laboratorium_umum');

        $data = array_merge($data, backend_info());
        $data['trx_id'] = $transaksi_id;
        
        $this->parser->parse('module_template', $data);
    }

    public function buat_draft_permintaan($status_pemeriksaan = 1, $tujuan_laboratorium = 1): void
    {
        $pendaftaran_id = $this->input->post('pendaftaran_id');
        $asal_rujukan = $this->input->post('asal_rujukan');
        $diagnosa_utama = $this->global_model->get_dianosa_utama($pendaftaran_id, $asal_rujukan);

        $data = [
            'pendaftaran_id' => $this->input->post('pendaftaran_id'),
            'pasien_id' => $this->input->post('pasien_id'),
            'asal_rujukan' => $this->input->post('asal_rujukan'),
            'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
            'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
            'status_pemeriksaan' => $status_pemeriksaan,
            'tujuan_laboratorium' => $tujuan_laboratorium,
            'diagnosa' => $diagnosa_utama,
            'waktu_permintaan' => date('Y-m-d H:i:s'),
            'created_ppa' => $this->session->userdata('login_ppa_id'),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $this->db->insert('term_laboratorium_umum', $data);

        echo json_encode([
            'success' => true,
            'data' => $this->db->insert_id(),
        ]);
    }

    public function batal_draft_permintaan($id): void
    {
        $this->db->set('status_pemeriksaan', 0);
        $this->db->where('id', $id);
        if ($this->db->update('term_laboratorium_umum')) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function update_draft_permintaan(): void
    {
        $data = [
            'pendaftaran_id' => $this->input->post('pendaftaran_id'),
            'pasien_id' => $this->input->post('pasien_id'),
            'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
            'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
            'tujuan_laboratorium' => $this->input->post('tujuan_laboratorium'),
            'dokter_perujuk_id' => $this->input->post('dokter_perujuk_id'),
            'dokter_peminta_id' => $this->input->post('dokter_peminta_id'),
            'diagnosa' => $this->input->post('diagnosa'),
            'catatan_permintaan' => $this->input->post('catatan_permintaan'),
            'waktu_permintaan' => YMDTimeFormat($this->input->post('tanggal_permintaan').' '.$this->input->post('waktu_permintaan')),
            'prioritas' => $this->input->post('prioritas'),
            'pasien_puasa' => $this->input->post('pasien_puasa'),
            'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
        ];

        $this->db->where('id', $this->input->post('transaksi_id'));
        if ($this->db->update('term_laboratorium_umum', $data)) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function simpan_permintaan(): void
    {
        if ($this->model->insertPemeriksaan()) {
            $asalRujukan = $this->input->post('asal_rujukan_status');
            $pendaftaranId = $this->input->post('pendaftaran_id');
            $transaksiId = $this->input->post('transaksi_id');

            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data Permintaan berhasil disimpan.');

            $formSubmit = $this->input->post('form_submit');

            switch ($formSubmit) {
                case 'form-submit-only':
                    $redirectUrl = 'term_laboratorium_umum_permintaan';

                    break;

                case 'form-submit-and-process':
                    $redirectUrl = "term_laboratorium_umum/tindakan/{$asalRujukan}/{$pendaftaranId}/erm_lab/laboratorium_pemeriksaan/{$transaksiId}/input_pemeriksaan";

                    break;

                default:
                    $redirectUrl = "term_laboratorium_umum/tindakan/{$asalRujukan}/{$pendaftaranId}/erm_lab/laboratorium_permintaan";
            }

            redirect($redirectUrl, 'location');
        }
    }

    public function simpan_pemeriksaan(): void
    {
        if ($this->model->updatePemeriksaan()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data telah disimpan.');

            redirect('term_laboratorium_umum_permintaan', 'location');
        }
    }

    public function simpan_split_pemeriksaan() : void
    {
        $transaksiId = $this->input->post('transaksi_id');
        $selectedOrderIds = json_decode($_POST['selected_order_ids']);

        if ($newTansaksiId = $this->model->splitOrder($transaksiId)) {
            $this->model->updatePemeriksaanWithSplit($transaksiId, $newTansaksiId, $selectedOrderIds);

            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data telah disimpan.');
            redirect("term_laboratorium_umum_permintaan", 'location');
        }
    }

    public function ubah_tujuan_laboratorium($id, $tujuan_laboratorium): void
    {
        $this->db->set('tujuan_laboratorium', $tujuan_laboratorium);
        $this->db->where('id', $id);
        if ($this->db->update('term_laboratorium_umum')) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function update_data_laboratorium(): void
    {
        $transaksi_id = $this->input->post('transaksi_id');

        $dataToUpdate = [
            'nama_dokter_luar_rs' => $this->input->post('nama_dokter_luar_rs'),
            'notelp_dokter_pengirim_luar_rs' => $this->input->post('notelp_dokter_pengirim_luar_rs'),
            'nama_fasilitas_pelayanana_kesehatan_luar_rs' => $this->input->post('nama_fasilitas_pelayanana_kesehatan_luar_rs'),
            'prioritas' => $this->input->post('prioritas'),
            'pasien_puasa' => $this->input->post('pasien_puasa'),
            'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
            'waktu_pengambilan_sample' => YMDTimeFormat($this->input->post('tanggal_pengambilan_sample').' '.$this->input->post('waktu_pengambilan_sample')),
            'petugas_pengambilan_sample' => $this->input->post('petugas_pengambilan_sample'),
            'waktu_pengujian_sample' => YMDTimeFormat($this->input->post('tanggal_pengujian_sample').' '.$this->input->post('waktu_pengujian_sample')),
            'petugas_pengujian_sample' => $this->input->post('petugas_pengujian_sample'),
            'waktu_penginputan_hasil' => YMDTimeFormat($this->input->post('tanggal_penginputan_hasil').' '.$this->input->post('waktu_penginputan_hasil')),
            'petugas_penginputan_hasil' => $this->input->post('petugas_penginputan_hasil'),
            'waktu_validasi_hasil' => YMDTimeFormat($this->input->post('tanggal_validasi_hasil').' '.$this->input->post('waktu_validasi_hasil')),
            'petugas_validasi_hasil' => $this->input->post('petugas_validasi_hasil'),
            'waktu_pengiriman_hasil' => YMDTimeFormat($this->input->post('tanggal_pengiriman_hasil').' '.$this->input->post('waktu_pengiriman_hasil')),
            'petugas_pengiriman_hasil' => $this->input->post('petugas_pengiriman_hasil'),
            'waktu_penerimaan_hasil' => YMDTimeFormat($this->input->post('tanggal_penerimaan_hasil').' '.$this->input->post('waktu_penerimaan_hasil')),
            'petugas_penerimaan_hasil' => $this->input->post('petugas_penerimaan_hasil'),
        ];

        $this->db->where('id', $transaksi_id);
        $this->db->update('term_laboratorium_umum', $dataToUpdate);

        $response = ['status' => 'success', 'message' => 'Data Laboratorium Updated'];
        echo json_encode($response);
    }

    public function daftar_pemeriksaan($orderId): void
    {
        $data['status'] = 'success';
        $data['data'] = $this->model->getPemeriksaanData($orderId);
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function clone_permintaan($id): void
    {
        $login_ppa_id = $this->session->userdata('login_ppa_id');

        $query = "INSERT INTO term_laboratorium_umum (
                pendaftaran_id, pasien_id, asal_rujukan, jenis_pemeriksaan, rencana_pemeriksaan,
                tujuan_laboratorium, dokter_perujuk_id, dokter_peminta_id, diagnosa, catatan_permintaan,
                waktu_permintaan, prioritas, pasien_puasa, pengiriman_hasil, status_pemeriksaan,
                created_ppa, created_at, edited_ppa, edited_at, deleted_ppa, deleted_at
        )
        SELECT
                pendaftaran_id, pasien_id, asal_rujukan, jenis_pemeriksaan, rencana_pemeriksaan,
                tujuan_laboratorium, dokter_perujuk_id, dokter_peminta_id, diagnosa, catatan_permintaan,
                waktu_permintaan, prioritas, pasien_puasa, pengiriman_hasil, '1' as status_pemeriksaan,
                '{$login_ppa_id}', NOW(), '{$login_ppa_id}', NOW(), null, null
        FROM term_laboratorium_umum
        WHERE id = '{$id}'";

        $this->db->query($query);
        $transaksi_id = $this->db->insert_id();

        // Clone pemeriksaan
        $queryPemeriksaan = "INSERT INTO term_laboratorium_umum_pemeriksaan (
						transaksi_id, iddokter, idlaboratorium, `index`, namatarif, jasasarana, jasasarana_disc,
						jasapelayanan, jasapelayanan_disc, bhp, bhp_disc, biayaperawatan, biayaperawatan_disc,
						total, kuantitas, diskon, totalkeseluruhan, hasil, satuan, nilainormal, keterangan,
						statusrincianpaket, status, statusverifikasi, st_paket, pajak_dokter, potongan_rs,
						periode_pembayaran, periode_jatuhtempo, status_jasamedis
				)
				SELECT
						'{$transaksi_id}', iddokter, idlaboratorium, `index`, namatarif, jasasarana, jasasarana_disc,
						jasapelayanan, jasapelayanan_disc, bhp, bhp_disc, biayaperawatan, biayaperawatan_disc,
						total, kuantitas, diskon, totalkeseluruhan, hasil, satuan, nilainormal, keterangan,
						statusrincianpaket, status, statusverifikasi, st_paket, pajak_dokter, potongan_rs,
						periode_pembayaran, periode_jatuhtempo, status_jasamedis
				FROM term_laboratorium_umum_pemeriksaan
				WHERE transaksi_id = '{$id}'";

        $this->db->query($queryPemeriksaan);

        $this->output->set_output(json_encode([
            'status' => 'success',
            'data' => [
                'transaksi_id' => $transaksi_id,
            ],
        ]));
    }

    public function merge_permintaan($transaksiId, $newTransaksiId) : void
    {
        // Step 1: Copy records from term_laboratorium_umum_pemeriksaan with ON DUPLICATE KEY UPDATE
        $this->db->select('*');
        $this->db->from('term_laboratorium_umum_pemeriksaan');
        $this->db->where('transaksi_id', $transaksiId);

        $query = $this->db->get();

        foreach ($query->result() as $row) {
            $row->transaksi_id = $newTransaksiId;
            unset($row->id);
            $this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $row);
        }

        // Step 2: Update term_laboratorium_umum
        $this->db->set('status_pemeriksaan', 0);
        $this->db->where('id', $transaksiId);
        $this->db->update('term_laboratorium_umum');

        // Return JSON response
        $this->output->set_output(json_encode([
            'status' => 'success',
            'data' => [
                'transaksiId' => $transaksiId,
                'newTransaksiId' => $newTransaksiId,
            ]
        ]));
    }

    public function cetak_bukti_permintaan($id): void
    {
        if ('' != $id) {
            $row = $this->model->get_data_transaksi($id);
            $data = [
                'id' => $id,
                'nomor_registrasi' => $row->nomor_registrasi,
                'waktu_permintaan' => $row->waktu_permintaan,
                'nomedrec' => $row->nomedrec,
                'nama_pasien' => $row->nama_pasien,
                'tanggal_lahir' => $row->tanggal_lahir,
                'diagnosa' => $row->diagnosa,
                'prioritas' => $row->prioritas_label,
                'nomor_permintaan' => $row->nomor_permintaan,
                'rencana_pemeriksaan' => $row->rencana_pemeriksaan,
                'dokter_peminta_id' => $row->dokter_peminta_id,
                'dokter_peminta' => $row->dokter_peminta,
                'catatan_permintaan' => $row->catatan_permintaan,
            ];

            $data['error'] = '';
            $data['title'] = 'Bukti Permintaan Laboratorium';

            $data['list_pemeriksaan'] = $this->model->getListPemeriksaan($id);

            $data['pengaturan_printout'] = (array) get_row('merm_pengaturan_printout_laboratorium');

            $data = array_merge($data, backend_info());
            $this->parser->parse('Tpendaftaran_poli_ttv/erm_lab/print/bukti_permintaan', $data);
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('term_laboratorium_umum/tindakan');
        }
    }

    public function cetak_bukti_pengambilan_pemeriksaan($id): void
    {
        if ('' != $id) {
            $row = $this->model->get_data_transaksi($id);
            $data = [
                'id' => $id,
                'nomor_permintaan' => $row->nomor_permintaan,
                'nomor_medrec' => $row->nomedrec,
                'nama_pasien' => $row->nama_pasien,
                'tanggal_lahir' => $row->tanggal_lahir,
                'umur_tahun' => $row->umur_tahun,
                'umur_bulan' => $row->umur_bulan,
                'umur_hari' => $row->umur_hari,
                'asal_rujukan' => GetAsalRujukan($row->asal_rujukan),
                'dokter_perujuk' => $row->dokter_perujuk,
                'kelompok_pasien' => $row->kelompok_pasien,
                'dokter_peminta_id' => $row->dokter_peminta_id,
                'dokter_peminta' => $row->dokter_peminta,
                'waktu_pemeriksaan' => $row->waktu_proses_transaksi,
                'waktu_pengambilan' => $row->waktu_pengambilan_sample,
            ];

            $data['error'] = '';
            $data['title'] = 'Bukti Pengambilan Laboratorium';

            $data['list_pemeriksaan'] = $this->model->getListPemeriksaan($id);

            $data['pengaturan_printout'] = (array) get_row('merm_pengaturan_printout_laboratorium');

            $data = array_merge($data, backend_info());
            $this->parser->parse('Tpendaftaran_poli_ttv/erm_lab/print/bukti_pengambilan', $data);
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('term_laboratorium_umum/tindakan');
        }
    }

    public function cetak_bukti_pengambilan_pemeriksaan_small($id): void
    {
        if ('' != $id) {
            $options = new Options();
            $options->set('isRemoteEnabled', true);
            $dompdf = new Dompdf($options);

            $row = $this->model->get_data_transaksi($id);

            $data = [
                'nomor_permintaan' => $row->nomor_permintaan,
                'nomor_medrec' => $row->nomedrec,
                'nama_pasien' => $row->nama_pasien,
                'tanggal_lahir' => $row->tanggal_lahir,
                'umur_tahun' => $row->umur_tahun,
                'umur_bulan' => $row->umur_bulan,
                'umur_hari' => $row->umur_hari,
                'asal_rujukan' => GetAsalRujukan($row->asal_rujukan),
                'dokter_perujuk' => $row->dokter_perujuk,
                'kelompok_pasien' => $row->kelompok_pasien,
                'dokter_peminta_id' => $row->dokter_peminta_id,
                'dokter_peminta' => $row->dokter_peminta,
                'waktu_pemeriksaan' => $row->waktu_proses_transaksi,
                'waktu_pengambilan' => $row->waktu_pengambilan_sample,
            ];

            $data['list_pemeriksaan'] = $this->model->getListPemeriksaan($id);
            
            $data['pengaturan_printout'] = (array) get_row('merm_pengaturan_printout_laboratorium');

            $html = $this->parser->parse('Tpendaftaran_poli_ttv/erm_lab/print/bukti_pengambilan_small', array_merge($data, backend_info()), true);
            $html = $this->parser->parse_string($html, $data);

            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation
            $customPaperThermal = [0, 0, 226, 4000];
            $dompdf->set_paper($customPaperThermal);

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream('BUKTI PENGAMBILAN RADIOLOGI.pdf', ['Attachment' => 0]);
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('term_laboratorium_umum/tindakan');
        }
    }

    public function get_daftar_pemeriksaan_laboratorium(): void
    {
        $idkelas = $this->input->post('idkelas');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $idtipe = $this->input->post('idtipe');
        $idpaket = $this->input->post('idpaket');
        $idtindakan = $this->input->post('idtindakan');
        $idsubheader = $this->input->post('idsubheader');

        if (null != $idsubheader) {
            $idsubheader = json_decode($idsubheader, true);
        } else {
            $idsubheader = [];
        }

        if (1 == $idkelompokpasien) {
            $this->db->select('mtarif_laboratorium.path');
            $this->db->join('mrekanan', 'mrekanan.tlaboratorium_umum = mtarif_laboratorium.id');
            $this->db->where('mrekanan.id', $idrekanan);
            $query = $this->db->get('mtarif_laboratorium');

            if ($query->num_rows() > 0) {
                $row = $query->row();
            } else {
                $this->db->select('mtarif_laboratorium.path');
                $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
                $this->db->where('mpasien_kelompok.id', $idkelompokpasien);
                $query = $this->db->get('mtarif_laboratorium');
                $row = $query->row();
            }
        } else {
            $this->db->select('mtarif_laboratorium.path');
            $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
            $this->db->where('mpasien_kelompok.id', $idkelompokpasien);
            $query = $this->db->get('mtarif_laboratorium');
            $row = $query->row();
        }

        if ($query->num_rows() > 0) {
            $this->select = [
                'mtarif_laboratorium.id',
                'mtarif_laboratorium.idtipe',
                'mtarif_laboratorium.nama',
                'mtarif_laboratorium.idkelompok',
                'mtarif_laboratorium.idpaket',
                'mtarif_laboratorium.headerpath',
                'mtarif_laboratorium.path',
                'mtarif_laboratorium.level',
                'mtarif_laboratorium_detail.*',
            ];

            $this->from = 'mtarif_laboratorium';

            $this->join = [
                ['mtarif_laboratorium_detail', 'mtarif_laboratorium_detail.idtarif = mtarif_laboratorium.id', 'LEFT'],
                ['mtarif_laboratorium mtarif_paket', 'mtarif_paket.idpaket = 1  AND mtarif_laboratorium_detail.idtarif = mtarif_paket.id AND LEFT ( mtarif_laboratorium.path, LENGTH( mtarif_paket.path ) ) = mtarif_paket.path AND mtarif_laboratorium.path <> mtarif_paket.path', 'LEFT'],
            ];

            if (0 != $idtindakan) {
                $this->where = [
                    'mtarif_laboratorium_detail.kelas' => $idkelas,
                    'mtarif_laboratorium_detail.status' => '1',
                    'mtarif_laboratorium.idtipe' => $idtipe,
                    'mtarif_laboratorium.path LIKE' => $idtindakan.'%',
                ];
            } else {
                $this->where = [
                    'mtarif_laboratorium_detail.kelas' => $idkelas,
                    'mtarif_laboratorium_detail.status' => '1',
                    'mtarif_laboratorium.idtipe' => $idtipe,
                    'mtarif_laboratorium.path LIKE' => $row->path.'.%',
                ];
            }

            if (2 != $idpaket) {
                $this->where = array_merge($this->where, ['mtarif_laboratorium.idpaket' => $idpaket]);
            }

            $this->or_where = [];

            if (is_array($idsubheader)) {
                foreach ($idsubheader as $key => $value) {
                    $this->or_where[$key] = ['mtarif_laboratorium.path LIKE' => $value.'%'];
                }
            }

            $this->session->set_userdata('sessionOrWhere', $this->or_where);

            $this->order = [
                "SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',1) + 0,
					SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',2),'.',-1) + 0,
					SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',3),'.',-1) + 0,
					SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',4),'.',-1) + 0" => '',
            ];

            $this->group = ['mtarif_laboratorium.id'];

            $this->column_search = ['mtarif_laboratorium.nama'];
            $this->column_order = ['mtarif_laboratorium.nama'];

            $list = $this->datatable->get_datatables();

            $data = [];
            $no = $_POST['start'];
            foreach ($list as $r) {
                ++$no;
                $row = [];

                $treeView = (1 == $r->idpaket) ? '<span class="label label-primary" data-toggle="tooltip" title="PAKET">'.TreeView($r->level, $r->nama).'</span>' : TreeView($r->level, $r->nama);

                $row[] = '<div class="checkbox">
                    <label>
                        <input type="checkbox" id="pemeriksaan-'.$r->id.'" class="'.(1 == $r->idkelompok && 0 == $r->idpaket ? 'parent-checkbox' : 'child-checkbox').'"
                            data-kelas="'.$r->kelas.'"
                            data-nama-pemeriksaan="'.TreeView($r->level, $r->nama).'"
                            data-path="'.$r->path.'"
                            data-jasa-sarana="'.$r->jasasarana.'"
                            data-jasa-pelayanan="'.$r->jasapelayanan.'"
                            data-bhp="'.$r->bhp.'"
                            data-biaya-perawatan="'.$r->biayaperawatan.'"
                            data-total="'.$r->total.'"
                            data-kuantitas="1"
                            data-diskon="0"
                            data-total-keseluruhan="'.$r->total.'"
                            data-status-verifikasi="0"
                            value="'.$r->id.'">
                    </label>
                </div>';

                $row[] = $no;
                $row[] = $treeView;
                $row[] = number_format((int) $r->total);
                $row[] = $r->idkelompok;
                $row[] = $r->idpaket;

                $data[] = $row;
            }
            $output = [
                'draw' => $_POST['draw'],
                'recordsTotal' => $this->datatable->count_all(),
                'recordsFiltered' => $this->datatable->count_all(),
                'data' => $data,
            ];
            echo json_encode($output);
        } else {
            $output = [
                'draw' => '',
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => '',
            ];
            echo json_encode($output);
        }
    }

    public function get_daftar_order_laboratorium($pasien_id, $ppa_id = ''): void
    {
        $tanggal_permintaan_dari = $this->input->post('tanggal_permintaan_dari');
        $tanggal_permintaan_sampai = $this->input->post('tanggal_permintaan_sampai');
        $tanggal_pendaftaran_dari = $this->input->post('tanggal_pendaftaran_dari');
        $tanggal_pendaftaran_sampai = $this->input->post('tanggal_pendaftaran_sampai');
        $nomor_registrasi = $this->input->post('nomor_registrasi');
        $tujuan_klinik = $this->input->post('tujuan_klinik');
        $tujuan_dokter = $this->input->post('tujuan_dokter');
        $tujuan_laboratorium = $this->input->post('tujuan_laboratorium');

        $this->select = [
            'term_laboratorium_umum.*,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan = 1 THEN "Poliklinik"
                WHEN term_laboratorium_umum.asal_rujukan = 2 THEN "Instalasi Gawat Darurat (IGD)"
                WHEN term_laboratorium_umum.asal_rujukan = 3 THEN "Rawat Inap"
                WHEN term_laboratorium_umum.asal_rujukan = 4 THEN "One Day Surgery (ODS)"
            END) AS tujuan_pendaftaran,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
                    trawatinap_pendaftaran.nopendaftaran
            END) AS nomor_registrasi,
            mdokter_perujuk.nama AS dokter_perujuk,
            mpoliklinik.nama AS poliklinik,
            mdokter_peminta.nama AS dokter_peminta,
            merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium,
            referensi_prioritas.ref AS prioritas_label,
            mppa.nama AS created_ppa',
        ];

        $this->from = 'term_laboratorium_umum';

        $this->join = [
            ['trawatinap_pendaftaran', 'term_laboratorium_umum.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_umum.asal_rujukan IN (3, 4)', 'LEFT'],
            ['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_umum.pendaftaran_id AND term_laboratorium_umum.asal_rujukan IN (1, 2)', 'LEFT'],
            ['trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_umum.pendaftaran_id', 'LEFT'],
            ['mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT'],
            ['mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_umum.dokter_perujuk_id', 'LEFT'],
            ['mdokter mdokter_peminta', 'mdokter_peminta.id = term_laboratorium_umum.dokter_peminta_id', 'LEFT'],
            ['merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_umum.tujuan_laboratorium', 'LEFT'],
            ['merm_referensi referensi_prioritas', 'referensi_prioritas.ref_head_id = 85 AND referensi_prioritas.nilai = term_laboratorium_umum.prioritas', 'LEFT'],
            ['mppa', 'mppa.id = term_laboratorium_umum.created_ppa', 'LEFT'],
        ];

        $this->where = [];

        if (!empty($tanggal_permintaan_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) >=' => YMDFormat($tanggal_permintaan_dari),
            ]);
        }

        if (!empty($tanggal_permintaan_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) <=' => YMDFormat($tanggal_permintaan_sampai),
            ]);
        }

        if (!empty($tanggal_pendaftaran_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) >=' => YMDFormat($tanggal_pendaftaran_dari),
            ]);
        }

        if (!empty($tanggal_pendaftaran_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) <=' => YMDFormat($tanggal_pendaftaran_sampai),
            ]);
        }

        if (!empty($nomor_registrasi)) {
            $this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.nopendaftaran LIKE' => '%'.$nomor_registrasi.'%']);
        }

        if (!empty($tujuan_klinik) || '0' != $tujuan_klinik) {
            $this->where = array_merge($this->where, ['mpoliklinik.id' => $tujuan_klinik]);
        }

        if (!empty($tujuan_dokter) || '0' != $tujuan_dokter) {
            $this->where = array_merge($this->where, ['mdokter_perujuk.id' => $tujuan_dokter]);
        }

        if (!empty($tujuan_laboratorium) || '0' != $tujuan_laboratorium) {
            $this->where = array_merge($this->where, ['term_laboratorium_umum.tujuan_laboratorium' => $tujuan_laboratorium]);
        }

        if (!empty($pasien_id) || '' != $pasien_id) {
            $this->where = array_merge($this->where, ['term_laboratorium_umum.pasien_id' => $pasien_id]);
        }
        
        if (!empty($ppa_id) || '' != $ppa_id) {
            $this->where = array_merge($this->where, ['term_laboratorium_umum.created_ppa' => $ppa_id]);
        }

        $this->where = array_merge($this->where, ['term_laboratorium_umum.status_pemeriksaan !=' => '1']);

        $this->order = [
            'term_laboratorium_umum.id' => 'DESC',
        ];

        $this->group = ['term_laboratorium_umum.id'];

        $this->column_search = ['term_laboratorium_umum.nomor_permintaan'];
        $this->column_order = ['term_laboratorium_umum.nomor_permintaan'];

        $list = $this->datatable->get_datatables();

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];
            
            $hasil_pemeriksaan_id = $this->model->getHasilPemeriksaanTerakhir($r->id);
            $asal_rujukan = (in_array($r->asal_rujukan, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');

            $row[] = '<div class="btn-group">
						<a href="#" data-toggle="tooltip" title="Lihat Permintaan" class="btn btn-default btn-xs view-order" onclick="lihatPermintaan(\''.$asal_rujukan.'\', '.$r->pendaftaran_id.', '.$r->id.')"><i class="fa fa-eye"></i></a>
						<a href="#" data-toggle="tooltip" title="Clone Permintaan" class="btn btn-warning btn-xs clone-order" onclick="clonePermintaan(\''.$asal_rujukan.'\', '.$r->id.')"><i class="fa fa-clone"></i></a>
						'.(2 == $r->status_pemeriksaan ? '<a href="#" data-toggle="tooltip" title="Edit Permintaan edit-order" class="btn btn-primary btn-xs" onclick="editPermintaan(\''.$asal_rujukan.'\', '.$r->pendaftaran_id.', '.$r->id.', '.$r->jumlah_edit.')"><i class="fa fa-pencil"></i></a>' : '').'
						'.(2 == $r->status_pemeriksaan ? '<a href="#" data-toggle="tooltip" title="Batal Permintaan delete-order" class="btn btn-danger btn-xs" onclick="batalPermintaan('.$r->id.')"><i class="fa fa-times-circle"></i></a>' : '').'
						'.($r->status_pemeriksaan >= 6 ? '<a href="#" data-toggle="tooltip" title="Lihat Pemeriksaan" class="btn btn-success btn-xs" onclick="lihatHasilPemeriksaan('.$r->id.')"><i class="fa fa-list"></i></a>' : '').'
                        <div class="btn-group dropup print-order">
                            <button class="btn btn-primary btn-xs btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-print"></i>
                            </button>
                            <ul class="dropdown-menu pull-left">
                                <li class=""><a href="#" onclick="cetakPermintaan('.$r->id.')">Bukti Permintaan</a></li>
                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPengambilanPemeriksaan(' . $r->id . ')">Bukti Pengambilan Pemeriksaan</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPengambilanPemeriksaanSmall(' . $r->id . ')">Bukti Pengambilan Pemeriksaan (Small)</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $hasil_pemeriksaan_id . ', \'FORMAT_1\')">Format 1</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $hasil_pemeriksaan_id . ', \'FORMAT_2\')">Format 2</a></li>' : '') .'
                            </ul>
                        </div>
				</div>';

            $row[] = $r->nomor_registrasi;
            $row[] = $r->waktu_permintaan;
            $row[] = $r->nomor_permintaan;
            $row[] = $r->tujuan_pendaftaran.' - '.$r->poliklinik.' - '.$r->dokter_perujuk;
            $row[] = $r->dokter_peminta;
            $row[] = $r->diagnosa;
            $row[] = $r->tujuan_laboratorium;
            $row[] = '<span class="label label-md label-danger" style="font-size: 11px;">'.$r->prioritas_label.'</span>';
            $row[] = StatusERMTindakan($r->status_pemeriksaan);
            $row[] = $r->created_ppa.' - '.HumanDateLong($r->created_at);

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function petugas_pengambilan_sample($tujuan_laboratorium): void
    {
        $data['petugas'] = $this->model->getPetugasPengambilSampleByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    public function petugas_pengujian_sample($tujuan_laboratorium): void
    {
        $data['petugas'] = $this->model->getPetugasPengujianByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    public function petugas_penginputan_hasil($tujuan_laboratorium): void
    {
        $data['petugas'] = $this->model->getPetugasPenginputanHasilByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    public function petugas_validasi_hasil($tujuan_laboratorium): void
    {
        $data['petugas'] = $this->model->getPetugasValidasiHasilByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    public function petugas_pengiriman_hasil($tujuan_laboratorium): void
    {
        $data['petugas'] = $this->model->getPetugasPengirimanHasilByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    public function petugas_penerimaan_hasil($tujuan_laboratorium): void
    {
        $data['petugas'] = $this->model->getPetugasPenerimaanHasilByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    public function dokter_laboratorium($tujuan_laboratorium): void
    {
        $data['dokter'] = $this->model->getDokterByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    public function dokter_penanggung_jawab($tujuan_laboratorium): void
    {
        $data['dokter'] = $this->model->getDokterPenanggungJawabByTujuanLaboratorium($tujuan_laboratorium);
        echo json_encode($data);
    }

    // LABORATORIUM PERMINTAAN
    public function plan_order($pendaftaran_id = '', $pasien_id = '', $asal_rujukan = ''): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $pasien = $this->model->getDataPasien($pasien_id);
            $data = [
                'nomor_medrec' => $pasien->no_medrec,
                'nama_pasien' => $pasien->nama,
                'nomor_ktp' => $pasien->ktp,
                'jenis_kelamin' => $pasien->jenis_kelamin,
                'email' => $pasien->email,
                'alamat_pasien' => $pasien->alamat_jalan,
                'tanggal_lahir' => DMYFormat($pasien->tanggal_lahir),
                'umur_hari' => $pasien->umur_hari,
                'umur_bulan' => $pasien->umur_bulan,
                'umur_tahun' => $pasien->umur_tahun,
            ];

            $data['error'] = '';
            $data['title'] = 'Plan Order';
            $data['content'] = 'Term_laboratorium_umum/plan_order';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Laboratorium', '#'],
                ['Plan Order', 'term_laboratorium_umum'],
            ];

            $data['tanggal'] = '';
            $data['pendaftaran_id'] = $pendaftaran_id;
            $data['pasien_id'] = $pasien_id;
            $data['asal_rujukan'] = $asal_rujukan;

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function switch_order($pendaftaran_id = '', $pasien_id = '', $transaksi_id = ''): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $pasien = $this->model->getDataPasien($pasien_id);
            $data = [
                'nomor_medrec' => $pasien->no_medrec,
                'nama_pasien' => $pasien->nama,
                'nomor_ktp' => $pasien->ktp,
                'jenis_kelamin' => $pasien->jenis_kelamin,
                'email' => $pasien->email,
                'alamat_pasien' => $pasien->alamat_jalan,
                'tanggal_lahir' => DMYFormat($pasien->tanggal_lahir),
                'umur_hari' => $pasien->umur_hari,
                'umur_bulan' => $pasien->umur_bulan,
                'umur_tahun' => $pasien->umur_tahun,
            ];

            $data['error'] = '';
            $data['title'] = 'Switch Order';
            $data['content'] = 'Term_laboratorium_umum/switch_order';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Laboratorium', '#'],
                ['Switch Order', 'term_laboratorium_umum'],
            ];
            
            $data['tanggal'] = date('d/m/Y');
            $data['transaksi_id'] = $transaksi_id;
            $data['pendaftaran_id'] = $pendaftaran_id;
            $data['pasien_id'] = $pasien_id;
            $data['asal_rujukan'] = $asal_rujukan;

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function proses_switch_order($transaksi_id, $pendaftaran_id): void
    {
        $this->db->set('pendaftaran_id', $pendaftaran_id);
        $this->db->set('rencana_pemeriksaan', date("Y-m-d"));
        $this->db->set('jenis_pemeriksaan', 1);
        $this->db->where('id', $transaksi_id);
        $this->db->update('term_laboratorium_umum');

        $this->output->set_output(json_encode([
            'status' => 'success',
            'data' => [
                'pendaftaran_id' => $pendaftaran_id,
            ],
        ]));
    }

    public function gabung_permintaan($pendaftaran_id = '', $transaksi_id = '', $pasien_id = ''): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $pasien = $this->model->getDataPasien($pasien_id);
            $data = [
                'nomor_medrec' => $pasien->no_medrec,
                'nama_pasien' => $pasien->nama,
                'nomor_ktp' => $pasien->ktp,
                'jenis_kelamin' => $pasien->jenis_kelamin,
                'email' => $pasien->email,
                'alamat_pasien' => $pasien->alamat_jalan,
                'tanggal_lahir' => DMYFormat($pasien->tanggal_lahir),
                'umur_hari' => $pasien->umur_hari,
                'umur_bulan' => $pasien->umur_bulan,
                'umur_tahun' => $pasien->umur_tahun,
            ];

            $data['error'] = '';
            $data['title'] = 'Gabung Permintaan';
            $data['content'] = 'Term_laboratorium_umum/gabung_permintaan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Laboratorium', '#'],
                ['Gabung Permintaan', 'term_laboratorium_umum'],
            ];

            $data['tanggal'] = date('d/m/Y');
            $data['transaksi_id'] = $transaksi_id;
            $data['pendaftaran_id'] = $pendaftaran_id;
            $data['pasien_id'] = $pasien_id;
            $data['asal_rujukan'] = $asal_rujukan;

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function riwayat_laboratorium($pendaftaran_id = '', $pasien_id = ''): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $data = [
                'tanggal' => date('d/m/Y'),
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
            ];

            $data['error'] = '';
            $data['title'] = 'Riwayat Laboratorium';
            $data['content'] = 'Term_laboratorium_umum/riwayat_laboratorium';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Laboratorium', '#'],
                ['Riwayat Laboratorium', 'term_laboratorium_umum'],
            ];

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function data_laboratorium($asal_rujukan = 'rawat_jalan', $pendaftaran_id = '', $transaksi_id = ''): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            if ($asal_rujukan == 'rawat_jalan') {
                $pendaftaran = $this->permintaan_model->getDataPendaftaranRawatJalan($pendaftaran_id);
            } else {
                $pendaftaran = $this->permintaan_model->getDataPendaftaranRawatInap($pendaftaran_id);
            }
    
            $laboratorium = $this->permintaan_model->getTransaksiLaboratorium($transaksi_id);

            $data = [
                'nomor_medrec' => $pendaftaran->nomor_medrec,
                'nama_pasien' => $pendaftaran->nama_pasien,
                'nomor_ktp' => $pendaftaran->nomor_ktp,
                'alamat_pasien' => $pendaftaran->alamatpasien,
                'jenis_kelamin' => $pendaftaran->jenis_kelamin,
                'tanggal_lahir' => $pendaftaran->tanggal_lahir,
                'umur_hari' => $pendaftaran->umur_hari,
                'umur_bulan' => $pendaftaran->umur_bulan,
                'umur_tahun' => $pendaftaran->umur_tahun,
                'kelompok_pasien' => $pendaftaran->kelompok_pasien,
                'nama_asuransi' => $pendaftaran->nama_asuransi,
                'tipe_kunjungan' => $pendaftaran->tipe_kunjungan,
                'nama_poliklinik' => $pendaftaran->nama_poliklinik,
                'dpjp' => $pendaftaran->dpjp,
                'nomor_pendaftaran' => $pendaftaran->nomor_pendaftaran,
                'tujuan_laboratorium' => $laboratorium->tujuan_laboratorium,
                'nomor_permintaan' => $laboratorium->nomor_permintaan,
                'nomor_transaksi_laboratorium' => $laboratorium->nomor_transaksi_laboratorium,
                'waktu_pembuatan' => $laboratorium->created_at,
                'waktu_permintaan' => $laboratorium->waktu_permintaan,
                'rencana_pemeriksaan' => DMYFormat($laboratorium->rencana_pemeriksaan),
                'diagnosa' => $laboratorium->diagnosa,
                'catatan_permintaan' => $laboratorium->catatan_permintaan,
                'nomor_laboratorium' => $laboratorium->nomor_laboratorium,
                'dokter_peminta_id' => $laboratorium->dokter_peminta_id,
                'nama_dokter_luar_rs' => $laboratorium->nama_dokter_luar_rs,
                'notelp_dokter_pengirim_luar_rs' => $laboratorium->notelp_dokter_pengirim_luar_rs,
                'nama_fasilitas_pelayanana_kesehatan_luar_rs' => $laboratorium->nama_fasilitas_pelayanana_kesehatan_luar_rs,
                'prioritas' => $laboratorium->prioritas,
                'pasien_puasa' => $laboratorium->pasien_puasa,
                'pengiriman_hasil' => $laboratorium->pengiriman_hasil,
                'tanggal_pengambilan_sample' => DMYFormat($laboratorium->waktu_pengambilan_sample),
                'waktu_pengambilan_sample' => HISTimeFormat($laboratorium->waktu_pengambilan_sample),
                'petugas_pengambilan_sample' => $laboratorium->petugas_pengambilan_sample,
                'tanggal_pengujian_sample' => DMYFormat($laboratorium->waktu_pengujian_sample),
                'waktu_pengujian_sample' => HISTimeFormat($laboratorium->waktu_pengujian_sample),
                'petugas_pengujian_sample' => $laboratorium->petugas_pengujian_sample,
                'tanggal_penginputan_hasil' => DMYFormat($laboratorium->waktu_penginputan_hasil),
                'waktu_penginputan_hasil' => HISTimeFormat($laboratorium->waktu_penginputan_hasil),
                'petugas_penginputan_hasil' => $laboratorium->petugas_penginputan_hasil,
                'tanggal_validasi_hasil' => DMYFormat($laboratorium->waktu_validasi_hasil),
                'waktu_validasi_hasil' => HISTimeFormat($laboratorium->waktu_validasi_hasil),
                'petugas_validasi_hasil' => $laboratorium->petugas_validasi_hasil,
                'tanggal_pengiriman_hasil' => DMYFormat($laboratorium->waktu_pengiriman_hasil),
                'waktu_pengiriman_hasil' => HISTimeFormat($laboratorium->waktu_pengiriman_hasil),
                'petugas_pengiriman_hasil' => $laboratorium->petugas_pengiriman_hasil,
                'tanggal_penerimaan_hasil' => DMYFormat($laboratorium->waktu_penerimaan_hasil),
                'waktu_penerimaan_hasil' => HISTimeFormat($laboratorium->waktu_penerimaan_hasil),
                'petugas_penerimaan_hasil' => $laboratorium->petugas_penerimaan_hasil,
            ];

            $data['error'] = '';
            $data['title'] = 'Data Laboratorium';
            $data['content'] = 'Term_laboratorium_umum/data_laboratorium';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Laboratorium', '#'],
                ['Data Laboratorium', 'term_laboratorium_umum'],
            ];

            $data['transaksi_id'] = $transaksi_id;
            $data['pendaftaran_id'] = $pendaftaran_id;

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function stiker_identitas($transaksi_id): void
    {
        $data = [];
        $data['tab'] = 0;
        $data['jml'] = 10;
        $data['start_awal'] = 1;
        $data['transaksi_id'] = $transaksi_id;
        if ($this->session->userdata('session_print_stiker')) {
            $data['margin'] = $this->session->userdata('session_print_stiker');
        } else {
            $data['margin'] = 'tengah';
        }

        $data['error'] = '';
        $data['title'] = 'Stiker Identitas';
        $data['content'] = 'Term_laboratorium_umum/stiker_identitas';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Laboratorium', 'term_laboratorium_umum'],
            ['Stiker Identitas', 'term_laboratorium_umum/stiker_identitas'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function cetak_stiker_identitas(): void
    {
        $data = [];
        
        $transaksi_id = $this->input->post('transaksi_id');

        $row = $this->model->get_data_transaksi($transaksi_id);

        $data = [
            'nomedrec' => $row->nomedrec,
            'namapasien' => $row->nama_pasien,
            'title' => $row->title,
            'jeniskelamin' => (1 == $row->jenis_kelamin ? 'L' : 'P'),
            'alamat' => $row->alamat_jalan,
            'tanggallahir' => $row->tanggal_lahir,
            'umurtahun' => $row->umur_tahun,
            'umurbulan' => $row->umur_bulan,
            'umurhari' => $row->umur_hari,
            'notelepon' => $row->telepon,
            'pekerjaan' => $row->pekerjaan
        ];

        $data_margin = ['session_print_stiker' => $this->input->post('margin')];
        $this->session->set_userdata($data_margin);

        $data['margin'] = $this->input->post('margin');
        $data['start_awal'] = $this->input->post('start_awal');
        $data['jml'] = $this->input->post('jml');
        $data['start_akhir'] = $this->input->post('jml') + $this->input->post('start_awal') - 1;

        $data_array = [];

        $data_akhir = cek_akhir_row($data['start_akhir']);

        for ($i = 1; $i <= $data_akhir; ++$i) {
            if ($i < $data['start_awal']) {
                $data_array[$i] = '0';
            } else {
                if ($i > $data['start_akhir']) {
                    $data_array[$i] = '0';
                } else {
                    $data_array[$i] = '1';
                }
            }
        }
        $data['data_array'] = $data_array;
        $data['data_akhir'] = $data_akhir;

        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);

        $html = $this->parser->parse('Term_laboratorium_umum/print/stiker_identitas', array_merge($data, backend_info()), true);
        $html = $this->parser->parse_string($html, $data);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();

        $dompdf->stream('Stiker Identitas.pdf', ['Attachment' => 0]);
    }

    public function label_tabung_darah($transaksi_id): void
    {
        $data = [];
        $data['tab'] = 0;
        $data['jml'] = 10;
        $data['start_awal'] = 1;
        $data['transaksi_id'] = $transaksi_id;
        if ($this->session->userdata('session_print_stiker')) {
            $data['margin'] = $this->session->userdata('session_print_stiker');
        } else {
            $data['margin'] = 'tengah';
        }

        $data['error'] = '';
        $data['title'] = 'Label Tabung Darah';
        $data['content'] = 'Term_laboratorium_umum/label_tabung_darah';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Laboratorium', 'term_laboratorium_umum'],
            ['Label Tabung Darah', 'term_laboratorium_umum/label_tabung_darah'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function cetak_label_tabung_darah(): void
    {
        $data = [];

        $transaksi_id = $this->input->post('transaksi_id');

        $row = $this->model->get_data_transaksi($transaksi_id);

        $data = [
            'nomedrec' => $row->nomedrec,
            'namapasien' => $row->nama_pasien,
            'title' => $row->title,
            'jeniskelamin' => (1 == $row->jenis_kelamin ? 'L' : 'P'),
            'alamat' => $row->alamat_jalan,
            'tanggallahir' => $row->tanggal_lahir,
            'umurtahun' => $row->umur_tahun,
            'umurbulan' => $row->umur_bulan,
            'umurhari' => $row->umur_hari,
            'notelepon' => $row->telepon,
            'pekerjaan' => $row->pekerjaan,
            'namapoliklinik' => $row->poliklinik,
            'tanggaldaftar' => $row->tanggal_daftar,
            'tujuan_pendaftaran' => $row->tujuan_pendaftaran,
            'nomor_laboratorium' => $row->nomor_laboratorium,
            'rencana_pemeriksaan' => $row->rencana_pemeriksaan,
            'dokter_peminta_id' => $row->dokter_peminta_id,
            'dokter_peminta' => $row->dokter_peminta,
        ];

        $data_margin = ['session_print_stiker' => $this->input->post('margin')];
        $this->session->set_userdata($data_margin);

        $data['margin'] = $this->input->post('margin');
        $data['start_awal'] = $this->input->post('start_awal');
        $data['jml'] = $this->input->post('jml');
        $data['start_akhir'] = $this->input->post('jml') + $this->input->post('start_awal') - 1;

        $data_array = [];

        $data_akhir = cek_akhir_row($data['start_akhir']);

        for ($i = 1; $i <= $data_akhir; ++$i) {
            if ($i < $data['start_awal']) {
                $data_array[$i] = '0';
            } else {
                if ($i > $data['start_akhir']) {
                    $data_array[$i] = '0';
                } else {
                    $data_array[$i] = '1';
                }
            }
        }
        $data['data_array'] = $data_array;
        $data['data_akhir'] = $data_akhir;

        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);

        $html = $this->parser->parse('Term_laboratorium_umum/print/label_tabung_darah', array_merge($data, backend_info()), true);
        $html = $this->parser->parse_string($html, $data);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();

        $dompdf->stream('Label Tabung Darah.pdf', ['Attachment' => 0]);
    }

    public function get_tarif_kelas_pemeriksaan($tansaksi_id, $kelas_id) {
        $tarif_data = $this->model->getTarifByKelas($tansaksi_id, $kelas_id);

        $response_data = array(
            'total' => $tarif_data->total,
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($response_data));
    }
    
    public function get_tarif_pemeriksaan_by_kelas($tarif_id, $kelas_id) {
        $response_data = $this->db
                    ->where('idtarif', $tarif_id)
                    ->where('kelas', $kelas_id)
                    ->get('mtarif_laboratorium_detail')
                    ->row();

        $this->output->set_content_type('application/json')->set_output(json_encode($response_data));
    }

    public function ubah_kelas_tarif($transaksi_id, $kelas_id) {
        $pemeriksaan = $this->db
            ->select('id, idlaboratorium, kuantitas, diskon')
            ->from('term_laboratorium_umum_pemeriksaan')
            ->where('transaksi_id', $transaksi_id)
            ->get()
            ->result();

        if ($pemeriksaan) {
            foreach ($pemeriksaan as $row) {
                // Ambil data tarif berdasarkan kelas dari model
                $tarif_data = $this->db
                    ->where('idtarif', $row->idlaboratorium)
                    ->where('kelas', $kelas_id)
                    ->get('mtarif_laboratorium_detail')
                    ->row();

                if ($tarif_data) {
                    // Data untuk diupdate pada term_laboratorium_umum_pemeriksaan
                    $update_data = array(
                        'kelas' => $tarif_data->kelas,
                        'jasasarana' => $tarif_data->jasasarana,
                        'jasapelayanan' => $tarif_data->jasapelayanan,
                        'bhp' => $tarif_data->bhp,
                        'biayaperawatan' => $tarif_data->biayaperawatan,
                        'total' => $tarif_data->total,
                        'totalkeseluruhan' => ($tarif_data->total * $row->kuantitas) - $row->diskon,
                    );

                    // Update data pada term_laboratorium_umum_pemeriksaan
                    $this->db->where('id', $row->id);
                    $this->db->update('term_laboratorium_umum_pemeriksaan', $update_data);

                    // Berikan respon sukses jika proses update berhasil
                    $response = array(
                        'status' => 'success',
                        'message' => 'Kelas tarif berhasil diubah.'
                    );
                } else {
                    // Berikan respon error jika data tarif tidak ditemukan
                    $response = array(
                        'status' => 'error',
                        'message' => 'Data tarif tidak ditemukan untuk kelas yang dipilih.'
                    );
                }
            }
        } else {
            // Berikan respon error jika idlaboratorium tidak ditemukan
            $response = array(
                'status' => 'error',
                'message' => 'Idlaboratorium tidak ditemukan untuk transaksi yang dipilih.'
            );
        }

        // Mengirim response dalam format JSON
        $this->output->set_content_type('application/json')->set_output(json_encode($response));

    }

    public function getIndexPlanOrder($pasien_id, $ref_pendaftaran_id, $ref_asal_rujukan): void
    {
        $tanggal_permintaan_dari = $this->input->post('tanggal_permintaan_dari');
        $tanggal_permintaan_sampai = $this->input->post('tanggal_permintaan_sampai');
        $tanggal_pendaftaran_dari = $this->input->post('tanggal_pendaftaran_dari');
        $tanggal_pendaftaran_sampai = $this->input->post('tanggal_pendaftaran_sampai');
        $nomor_registrasi = $this->input->post('nomor_registrasi');
        $tujuan_klinik = $this->input->post('tujuan_klinik');
        $tujuan_dokter = $this->input->post('tujuan_dokter');
        $dokter_peminta = $this->input->post('dokter_peminta');

        $this->select = [
            'term_laboratorium_umum.*,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan = 1 THEN "Poliklinik"
                WHEN term_laboratorium_umum.asal_rujukan = 2 THEN "Instalasi Gawat Darurat (IGD)"
                WHEN term_laboratorium_umum.asal_rujukan = 3 THEN "Rawat Inap"
                WHEN term_laboratorium_umum.asal_rujukan = 4 THEN "One Day Surgery (ODS)"
            END) AS tujuan_pendaftaran,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
                    trawatinap_pendaftaran.nopendaftaran
            END) AS nomor_registrasi,
            mdokter_perujuk.nama AS dokter_perujuk,
            mpoliklinik.nama AS poliklinik,
            mdokter_peminta.nama AS dokter_peminta,
            merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium,
            referensi_prioritas.ref AS prioritas_label,
            referensi_pemeriksaan.ref AS status_pemeriksaan_label,
            mppa.nama AS created_ppa',
        ];

        $this->from = 'term_laboratorium_umum';

        $this->join = [
            ['trawatinap_pendaftaran', 'term_laboratorium_umum.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_umum.asal_rujukan IN (3, 4)', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_umum.pendaftaran_id AND term_laboratorium_umum.asal_rujukan IN (1, 2)', 'LEFT'],
            ['trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_umum.pendaftaran_id', 'LEFT'],
            ['mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT'],
            ['mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_umum.dokter_perujuk_id', 'LEFT'],
            ['mdokter mdokter_peminta', 'mdokter_peminta.id = term_laboratorium_umum.dokter_peminta_id', 'LEFT'],
            ['merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_umum.tujuan_laboratorium', 'LEFT'],
            ['merm_referensi referensi_prioritas', 'referensi_prioritas.ref_head_id = 85 AND referensi_prioritas.nilai = term_laboratorium_umum.prioritas', 'LEFT'],
            ['merm_referensi referensi_pemeriksaan', 'referensi_pemeriksaan.ref_head_id = 89 AND referensi_pemeriksaan.nilai = term_laboratorium_umum.status_pemeriksaan', 'LEFT'],
            ['mppa', 'mppa.id = term_laboratorium_umum.created_ppa', 'LEFT'],
        ];

        $this->where = [];

        if (!empty($tanggal_permintaan_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) >=' => YMDFormat($tanggal_permintaan_dari),
            ]);
        }

        if (!empty($tanggal_permintaan_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) <=' => YMDFormat($tanggal_permintaan_sampai),
            ]);
        }

        if (!empty($tanggal_pendaftaran_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) >=' => YMDFormat($tanggal_pendaftaran_dari),
            ]);
        }

        if (!empty($tanggal_pendaftaran_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) <=' => YMDFormat($tanggal_pendaftaran_sampai),
            ]);
        }

        if (!empty($nomor_registrasi)) {
            $this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.nopendaftaran LIKE' => '%'.$nomor_registrasi.'%']);
        }

        if (!empty($tujuan_klinik) || '0' != $tujuan_klinik) {
            $this->where = array_merge($this->where, ['mpoliklinik.id' => $tujuan_klinik]);
        }

        if (!empty($tujuan_dokter) || '0' != $tujuan_dokter) {
            $this->where = array_merge($this->where, ['mdokter_perujuk.id' => $tujuan_dokter]);
        }

        if (!empty($dokter_peminta) || '0' != $dokter_peminta) {
            $this->where = array_merge($this->where, ['term_laboratorium_umum.dokter_peminta_id' => $dokter_peminta]);
        }

        if (!empty($pasien_id) || '' != $pasien_id) {
            $this->where = array_merge($this->where, ['term_laboratorium_umum.pasien_id' => $pasien_id]);
        }

        $this->where = array_merge($this->where, ['term_laboratorium_umum.status_pemeriksaan' => '2']);

        $this->order = [
            'term_laboratorium_umum.id' => 'DESC',
        ];

        $this->group = ['term_laboratorium_umum.id'];

        $this->column_search = ['term_laboratorium_umum.nomor_permintaan'];
        $this->column_order = ['term_laboratorium_umum.nomor_permintaan'];

        $list = $this->datatable->get_datatables();

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $asal_rujukan = (in_array($r->asal_rujukan, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');

            $buttonsHTML = '<div class="btn-group btn-block">
                    <div class="btn-group">
                        <a href="#" class="btn btn-xs btn-primary" onclick="prosesPermintaan(\''.$asal_rujukan.'\', '.$r->pendaftaran_id.', '.$r->id.', '.$ref_pendaftaran_id.', '.$ref_asal_rujukan.')"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-warning" onclick="splitPermintaan(\''.$asal_rujukan.'\', '.$r->pendaftaran_id.', '.$r->id.')"><i class="fa fa-scissors"></i></a>
                        <a href="#" class="btn btn-xs btn-success" onclick="cetakPermintaan('.$r->id.')"><i class="fa fa-print"></i></a>
                    </div>
            </div>';

            $row[] = $buttonsHTML;

            $row[] = $r->waktu_permintaan;
            $row[] = $r->nomor_permintaan;
            $row[] = $r->nomor_registrasi;
            $row[] = $r->tujuan_pendaftaran.' - '.$r->poliklinik.' - '.$r->dokter_perujuk;
            $row[] = $r->dokter_peminta;
            $row[] = $r->diagnosa;
            $row[] = $r->tujuan_laboratorium;
            $row[] = '<span class="label label-md label-danger" style="font-size: 11px;">'.$r->prioritas_label.'</span>';
            $row[] = $r->created_ppa.' - '.HumanDateLong($r->created_at);

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexGabungPermintaan($pasien_id, $transaksi_id): void
    {
        $tanggal_permintaan_dari = $this->input->post('tanggal_permintaan_dari');
        $tanggal_permintaan_sampai = $this->input->post('tanggal_permintaan_sampai');
        $tanggal_pendaftaran_dari = $this->input->post('tanggal_pendaftaran_dari');
        $tanggal_pendaftaran_sampai = $this->input->post('tanggal_pendaftaran_sampai');
        $nomor_registrasi = $this->input->post('nomor_registrasi');
        $tujuan_klinik = $this->input->post('tujuan_klinik');
        $tujuan_dokter = $this->input->post('tujuan_dokter');
        $dokter_peminta = $this->input->post('dokter_peminta');

        $this->select = [
            'term_laboratorium_umum.*,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan = 1 THEN "Poliklinik"
                WHEN term_laboratorium_umum.asal_rujukan = 2 THEN "Instalasi Gawat Darurat (IGD)"
                WHEN term_laboratorium_umum.asal_rujukan = 3 THEN "Rawat Inap"
                WHEN term_laboratorium_umum.asal_rujukan = 4 THEN "One Day Surgery (ODS)"
            END) AS tujuan_pendaftaran,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
                    trawatinap_pendaftaran.nopendaftaran
            END) AS nomor_registrasi,
            mdokter_perujuk.nama AS dokter_perujuk,
            mpoliklinik.nama AS poliklinik,
            mdokter_peminta.nama AS dokter_peminta,
            merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium,
            referensi_prioritas.ref AS prioritas_label,
            referensi_pemeriksaan.ref AS status_pemeriksaan_label,
            mppa.nama AS created_ppa',
        ];

        $this->from = 'term_laboratorium_umum';

        $this->join = [
            ['trawatinap_pendaftaran', 'term_laboratorium_umum.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_umum.asal_rujukan IN (3, 4)', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_umum.pendaftaran_id AND term_laboratorium_umum.asal_rujukan IN (1, 2)', 'LEFT'],
            ['trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_umum.pendaftaran_id', 'LEFT'],
            ['mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT'],
            ['mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_umum.dokter_perujuk_id', 'LEFT'],
            ['mdokter mdokter_peminta', 'mdokter_peminta.id = term_laboratorium_umum.dokter_peminta_id', 'LEFT'],
            ['merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_umum.tujuan_laboratorium', 'LEFT'],
            ['merm_referensi referensi_prioritas', 'referensi_prioritas.ref_head_id = 85 AND referensi_prioritas.nilai = term_laboratorium_umum.prioritas', 'LEFT'],
            ['merm_referensi referensi_pemeriksaan', 'referensi_pemeriksaan.ref_head_id = 89 AND referensi_pemeriksaan.nilai = term_laboratorium_umum.status_pemeriksaan', 'LEFT'],
            ['mppa', 'mppa.id = term_laboratorium_umum.created_ppa', 'LEFT'],
        ];

        $this->where = [
            'term_laboratorium_umum.pasien_id' => $pasien_id,
            'term_laboratorium_umum.id !=' => $transaksi_id,
            'term_laboratorium_umum.status_pemeriksaan !=' => '0',
            'term_laboratorium_umum.status_pemeriksaan !=' => '1',
            'term_laboratorium_umum.status_pemeriksaan !=' => '6',
            'term_laboratorium_umum.status_pemeriksaan !=' => '7',
        ];

        if (!empty($tanggal_permintaan_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) >=' => YMDFormat($tanggal_permintaan_dari),
            ]);
        }

        if (!empty($tanggal_permintaan_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) <=' => YMDFormat($tanggal_permintaan_sampai),
            ]);
        }

        if (!empty($tanggal_pendaftaran_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) >=' => YMDFormat($tanggal_pendaftaran_dari),
            ]);
        }

        if (!empty($tanggal_pendaftaran_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) <=' => YMDFormat($tanggal_pendaftaran_sampai),
            ]);
        }

        if (!empty($nomor_registrasi)) {
            $this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.nopendaftaran LIKE' => '%'.$nomor_registrasi.'%']);
        }

        if (!empty($tujuan_klinik) || '0' != $tujuan_klinik) {
            $this->where = array_merge($this->where, ['mpoliklinik.id' => $tujuan_klinik]);
        }

        if (!empty($tujuan_dokter) || '0' != $tujuan_dokter) {
            $this->where = array_merge($this->where, ['mdokter_perujuk.id' => $tujuan_dokter]);
        }

        if (!empty($dokter_peminta) || '0' != $dokter_peminta) {
            $this->where = array_merge($this->where, ['term_laboratorium_umum.dokter_peminta_id' => $dokter_peminta]);
        }

        $this->where = array_merge($this->where, ['term_laboratorium_umum.status_pemeriksaan !=' => '1']);

        $this->order = [
            'term_laboratorium_umum.id' => 'DESC',
        ];

        $this->group = ['term_laboratorium_umum.id'];

        $this->column_search = ['term_laboratorium_umum.nomor_permintaan'];
        $this->column_order = ['term_laboratorium_umum.nomor_permintaan'];

        $list = $this->datatable->get_datatables();

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $asal_rujukan = (in_array($r->asal_rujukan, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');

            $buttonsHTML = '<div class="btn-group btn-block">
                    <div class="btn-group">
                        <a href="#" class="btn btn-xs btn-primary merge-order" onclick="gabungPermintaan(\''.$asal_rujukan.'\', '.$r->pendaftaran_id.', '.$r->id.')"><i class="fa fa-compress"></i></a>
                        <a href="#" class="btn btn-xs btn-success print-order" onclick="cetakPermintaan('.$r->id.')"><i class="fa fa-print"></i></a>
                    </div>
            </div>';

            $row[] = $buttonsHTML;

            $row[] = $r->waktu_permintaan;
            $row[] = $r->nomor_permintaan;
            $row[] = $r->nomor_registrasi;
            $row[] = $r->tujuan_pendaftaran.' - '.$r->poliklinik.' - '.$r->dokter_perujuk;
            $row[] = $r->dokter_peminta;
            $row[] = $r->diagnosa;
            $row[] = $r->tujuan_laboratorium;
            $row[] = '<span class="label label-md label-danger" style="font-size: 11px;">'.$r->prioritas_label.'</span>';
            $row[] = $r->created_ppa.' - '.HumanDateLong($r->created_at);

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexSwitchOrder($pasien_id): void
    {
        $tanggal_pendaftaran_dari = $this->input->post('tanggal_pendaftaran_dari');
        $tanggal_pendaftaran_sampai = $this->input->post('tanggal_pendaftaran_sampai');
        $nomor_registrasi = $this->input->post('nomor_registrasi');
        $tujuan_klinik = $this->input->post('tujuan_klinik');
        $tujuan_dokter = $this->input->post('tujuan_dokter');

        $where = '';
        if ('' != $tanggal_pendaftaran_dari) {
            $where .= " AND DATE(tbl.tanggal_pendaftaran) >= '".YMDFormat($tanggal_pendaftaran_dari)."'";
        }
        if ('' != $tanggal_pendaftaran_sampai) {
            $where .= " AND DATE(tbl.tanggal_pendaftaran) <= '".YMDFormat($tanggal_pendaftaran_sampai)."'";
        }
        if ('' != $nomor_registrasi) {
            $where .= " AND (tbl.nomor_pendaftaran) = '{$nomor_registrasi}'";
        }
        if ('0' != $tujuan_klinik) {
            $where .= " AND (tbl.poliklinik_id) = '{$tujuan_klinik}'";
        }
        if ('0' != $tujuan_dokter) {
            $where .= " AND (tbl.dokter_id) = '{$tujuan_dokter}'";
        }

        $this->select = [];
        $from = "
            (
            SELECT
                tpoliklinik_pendaftaran.id AS pendaftaran_id,
                tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_pendaftaran,
                (CASE
                    WHEN tpoliklinik_pendaftaran.idtipe = '1' THEN '1'
                    WHEN tpoliklinik_pendaftaran.idtipe = '2' THEN '2'
                END) AS asal_pasien_id,
                tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
                mpasien_kelompok.nama AS kelompok_pasien,
                CONCAT( mpoliklinik.nama, ' - ', mdokter.nama ) AS detail,
                mpoliklinik.id AS poliklinik_id,
                mdokter.id AS dokter_id,
                tpoliklinik_pendaftaran.idpasien AS pasien_id
            FROM
                tpoliklinik_pendaftaran
                LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
                LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
                LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
                LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
                LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id
                AND ( tkasir.STATUS IS NULL OR tkasir.STATUS = 1 )
                AND tkasir.idtipe IN ( 1, 2 )

            UNION ALL

            SELECT
                trawatinap_pendaftaran.id AS pendaftaran_id,
                trawatinap_pendaftaran.tanggaldaftar AS tanggal_pendaftaran,
                (CASE
                    WHEN trawatinap_pendaftaran.idtipe = '1' THEN '3'
                    WHEN trawatinap_pendaftaran.idtipe = '2' THEN '4'
                END) AS asal_pasien_id,
                trawatinap_pendaftaran.nopendaftaran AS nomor_pendaftaran,
                mpasien_kelompok.nama AS kelompok_pasien,
                CONCAT( mpoliklinik.nama, ' - ', mdokter.nama ) AS detail,
                mpoliklinik.id AS poliklinik_id,
                mdokter.id AS dokter_id,
                tpoliklinik_pendaftaran.idpasien AS pasien_id
            FROM
                trawatinap_pendaftaran
                LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
                LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
                LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
                LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
                LEFT JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
                LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id
                AND ( tkasir.STATUS IS NULL OR tkasir.STATUS = 1 )
                AND tkasir.idtipe IN ( 1, 2 )
            ) AS tbl
            WHERE tbl.pendaftaran_id IS NOT NULL AND tbl.pasien_id = ".$pasien_id.'  '.$where.'
        ';

        $this->from = $from;
        $this->join = [];

        $this->order = [
            'tbl.tanggal_pendaftaran' => 'DESC',
        ];
        $this->group = ['tbl.pendaftaran_id', 'tbl.asal_pasien_id'];
        $this->column_search = ['nomor_permintaan'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables();

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $asal_rujukan = (in_array($r->asal_pasien_id, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');

            $buttonsHTML = '<a href="#" class="btn btn-xs btn-success" onclick="switchOrder(\''.$asal_rujukan.'\', '.$r->pendaftaran_id.')"><i class="fa fa-plus"></i> Pilih</a>';

            $row[] = $buttonsHTML;

            $row[] = HumanDateLong($r->tanggal_pendaftaran);
            $row[] = GetAsalPasien($r->asal_pasien_id);
            $row[] = $r->nomor_pendaftaran;
            $row[] = $r->kelompok_pasien;
            $row[] = $r->detail;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexRiwayatLaboratorium($pasien_id): void
    {
        $tanggal_permintaan_dari = $this->input->post('tanggal_permintaan_dari');
        $tanggal_permintaan_sampai = $this->input->post('tanggal_permintaan_sampai');
        $tanggal_pendaftaran_dari = $this->input->post('tanggal_pendaftaran_dari');
        $tanggal_pendaftaran_sampai = $this->input->post('tanggal_pendaftaran_sampai');
        $nomor_registrasi = $this->input->post('nomor_registrasi');
        $tujuan_klinik = $this->input->post('tujuan_klinik');
        $tujuan_dokter = $this->input->post('tujuan_dokter');
        $tujuan_laboratorium = $this->input->post('tujuan_laboratorium');

        $this->select = [
            'term_laboratorium_umum.*,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan = 1 THEN "Poliklinik"
                WHEN term_laboratorium_umum.asal_rujukan = 2 THEN "Instalasi Gawat Darurat (IGD)"
                WHEN term_laboratorium_umum.asal_rujukan = 3 THEN "Rawat Inap"
                WHEN term_laboratorium_umum.asal_rujukan = 4 THEN "One Day Surgery (ODS)"
            END) AS tujuan_pendaftaran,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
                    trawatinap_pendaftaran.nopendaftaran
            END) AS nomor_registrasi,
            mdokter_perujuk.nama AS dokter_perujuk,
            mpoliklinik.nama AS poliklinik,
            mdokter_peminta.nama AS dokter_peminta,
            merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium,
            referensi_prioritas.ref AS prioritas_label,
            referensi_pemeriksaan.ref AS status_pemeriksaan_label,
            mppa.nama AS created_ppa,
            1 AS tipe_laboratorium',
        ];

        $this->from = 'term_laboratorium_umum';

        $this->join = [
            ['trawatinap_pendaftaran', 'term_laboratorium_umum.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_umum.asal_rujukan IN (3, 4)', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_umum.pendaftaran_id AND term_laboratorium_umum.asal_rujukan IN (1, 2)', 'LEFT'],
            ['trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_umum.pendaftaran_id', 'LEFT'],
            ['mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT'],
            ['mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_umum.dokter_perujuk_id', 'LEFT'],
            ['mdokter mdokter_peminta', 'mdokter_peminta.id = term_laboratorium_umum.dokter_peminta_id', 'LEFT'],
            ['merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_umum.tujuan_laboratorium', 'LEFT'],
            ['merm_referensi referensi_prioritas', 'referensi_prioritas.ref_head_id = 85 AND referensi_prioritas.nilai = term_laboratorium_umum.prioritas', 'LEFT'],
            ['merm_referensi referensi_pemeriksaan', 'referensi_pemeriksaan.ref_head_id = 89 AND referensi_pemeriksaan.nilai = term_laboratorium_umum.status_pemeriksaan', 'LEFT'],
            ['mppa', 'mppa.id = term_laboratorium_umum.created_ppa', 'LEFT'],
        ];

        $this->where = [
            'term_laboratorium_umum.pasien_id' => $pasien_id,
        ];

        if (!empty($tanggal_permintaan_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) >=' => YMDFormat($tanggal_permintaan_dari),
            ]);
        }

        if (!empty($tanggal_permintaan_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(term_laboratorium_umum.waktu_permintaan) <=' => YMDFormat($tanggal_permintaan_sampai),
            ]);
        }

        if (!empty($tanggal_pendaftaran_dari)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) >=' => YMDFormat($tanggal_pendaftaran_dari),
            ]);
        }

        if (!empty($tanggal_pendaftaran_sampai)) {
            $this->where = array_merge($this->where, [
                'DATE(tpoliklinik_pendaftaran.tanggaldaftar) <=' => YMDFormat($tanggal_pendaftaran_sampai),
            ]);
        }

        if (!empty($nomor_registrasi)) {
            $this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.nopendaftaran LIKE' => '%'.$nomor_registrasi.'%']);
        }

        if (!empty($tujuan_klinik) || '0' != $tujuan_klinik) {
            $this->where = array_merge($this->where, ['mpoliklinik.id' => $tujuan_klinik]);
        }

        if (!empty($tujuan_dokter) || '0' != $tujuan_dokter) {
            $this->where = array_merge($this->where, ['mdokter_perujuk.id' => $tujuan_dokter]);
        }

        if (!empty($tujuan_laboratorium) || '0' != $tujuan_laboratorium) {
            $this->where = array_merge($this->where, ['term_laboratorium_umum.tujuan_laboratorium' => $tujuan_laboratorium]);
        }

        $this->where = array_merge($this->where, ['term_laboratorium_umum.status_pemeriksaan !=' => '1']);

        $this->order = [
            'term_laboratorium_umum.id' => 'DESC',
        ];

        $this->group = ['term_laboratorium_umum.id'];

        $this->column_search = ['term_laboratorium_umum.nomor_permintaan'];
        $this->column_order = ['term_laboratorium_umum.nomor_permintaan'];

        $list = $this->datatable->get_datatables();

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $asal_rujukan = (in_array($r->asal_rujukan, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');

            $buttonsHTML = '<div class="btn-group btn-block">
                    <div class="btn-group">
                        <a href="#" class="btn btn-xs btn-warning" onclick="clonePermintaan(\''.$asal_rujukan.'\', '.$r->id.')"><i class="fa fa-clone"></i></a>
                        <a href="#" class="btn btn-xs btn-success" onclick="cetakPermintaan('.$r->id.')"><i class="fa fa-print"></i></a>
                        ' . (5 == $r->status_pemeriksaan ? '<a href="#" class="btn btn-xs btn-success" onclick="lihatPemeriksaan('.$r->id.')"><i class="fa fa-list"></i></a>' : '') . '
                    </div>
            </div>';

            $row[] = $buttonsHTML;

            $row[] = GetTipeLaboratorium($r->tipe_laboratorium);
            $row[] = $r->nomor_registrasi;
            $row[] = $r->waktu_permintaan;
            $row[] = $r->nomor_permintaan;
            $row[] = $r->tujuan_pendaftaran.' - '.$r->poliklinik.' - '.$r->dokter_perujuk;
            $row[] = $r->dokter_peminta;
            $row[] = $r->diagnosa;
            $row[] = $r->tujuan_laboratorium;
            $row[] = '<span class="label label-md label-danger" style="font-size: 11px;">'.$r->prioritas_label.'</span>';
            $row[] = StatusERMTindakan($r->status_pemeriksaan);
            $row[] = $r->created_ppa.' - '.HumanDateLong($r->created_at);

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }
    // EOF LABORATORIUM PERMINTAAN

    public function _getRequired($asal_rujukan, $pendaftaran_id, $menu_atas, $menu_kiri)
    {
        if ($asal_rujukan == 'rawat_jalan') {
            $logicForm = $this->poli_ttv_model->logic_akses_form_erm($pendaftaran_id);
        } else {
            $logicForm = $this->poli_ttv_model->logic_akses_form_erm($pendaftaran_id, 1);
        }

        $defImage = '';
        if (isset($logicForm['jenis_kelamin'])) {
            $defImage = ('1' == $logicForm['jenis_kelamin']) ? 'def_1.png' : 'def_2.png';
        }

        $data = array_merge(
            $logicForm,
            [
                'url_back' => site_url().'tpendaftaran_poli_ttv',
                'url_utama' => site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id,
                'menu_atas' => $menu_atas,
                'menu_kiri' => $menu_kiri,
                'def_image' => $defImage,
            ]
        );

        return array_merge($data, get_ppa_login());
    }
}
