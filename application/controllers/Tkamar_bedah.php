<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Tkamar_bedah extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkamar_bedah_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpoliklinik_ranap_model');
		$this->load->model('Tpoliklinik_trx_model');
		$this->load->model('Tko_model', 'model');
		$this->load->helper('path');
		
		
  }
	function index(){
		$log['path_tindakan']='tkamar_bedah';
		$this->session->set_userdata($log);
		 get_ppa_login();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('2287'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-30 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_dokter'] 			= $this->Tkamar_bedah_model->list_dokter();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tanggal_div_22_1'] 			= $date2;
			$data['tanggal_div_22_2'] 			= date('d/m/Y');
			$data['tanggal_div_33_1'] 			= $date2;
			$data['tanggal_div_33_2'] 			= date('d/m/Y');
			$data['tab_utama'] 			= 3;
			$data['tab_utama_2'] 			= 1;
			$data['tab_atas'] 			= '22';
			$data['tab_detail_2'] 			= '1';
			$data['idalasan'] 			= '#';
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['tab_detail'] 			=1;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE()";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Tkamar_bedah/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Kamar Bedah",'#'),
												  array("Penjadwalan",'tkamar_bedah')
												);
			//Kamar Bedah
			 $data['dokterumum'] = $this->model->getdokterumum();
			$data['kelas_list'] = $this->model->kelas_list();
			$data['mjenis_operasi'] = $this->model->mjenis_operasi();
			$data['mruangan'] = $this->model->mruangan();
			$data['mkelas'] = $this->model->mkelas();
			$data['mkelompok_operasi'] = $this->model->mkelompok_operasi();
			$data['mjenis_anaesthesi'] = $this->model->mjenis_anaesthesi();
			$q="SELECT *from setting_tko";
			$data_setting=$this->db->query($q)->row_array();
			$data = array_merge($data,$data_setting, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	 public function find_pasien($id)
    {
        $arr = $this->Tkamar_bedah_model->find_pasien($id);
		$q="SELECT *from setting_tko";
		$data_setting=$this->db->query($q)->row_array();
		$arr['nopermintaan']=$this->get_no_bedah();
		$arr = array_merge($arr,$data_setting);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$arr['label_pembuat']='Dibuat Oleh : '.get_nama_ppa($login_ppa_id).' : '.HumanDateLong(date('Y-m-d H:i:s'));
        $this->output->set_output(json_encode($arr));
    }
	function get_no_bedah(){
		$vkodeawal = 'RB-'.date('ym');
		$q="SELECT MAX(SUBSTR(nopermintaan,-4)) as vnourut FROM tpoliklinik_ranap_perencanaan_bedah WHERE SUBSTR(nopermintaan,1,7)='$vkodeawal'";	
		$vnourut=$this->db->query($q)->row('vnourut');
		$vnourut=$vnourut+1;
		
		$vkode =$vkodeawal.sprintf("%04s", $vnourut);;
		return $vkode;
	}
	function get_tarif(){
					
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$kelompok_opr=$this->input->post('kelompok_operasi');
		$jenis_opr=$this->input->post('jenis_operasi');
		$jenis_bedah=$this->input->post('jenis_bedah');
		$jenis_anestesi=$this->input->post('jenis_anestesi');
		$q="
			SELECT H.tarif_jenis_opr,H.taif_full_care,H.tarif_sewa_alat  FROM setting_tko_tarif_bedah H INNER JOIN (
			SELECT 
			compare_value_11(
				MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='0' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='0' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF((H.kelompok_opr = '$kelompok_opr' OR H.kelompok_opr = '0') AND (H.jenis_opr='$jenis_opr' OR H.jenis_opr='0') AND (H.jenis_bedah='$jenis_bedah' OR H.jenis_bedah='0') AND (H.jenis_anestesi='$jenis_anestesi' OR H.jenis_anestesi='0'),H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
			) id
			FROM setting_tko_tarif_bedah H

			WHERE H.idkelompokpasien='$idkelompokpasien' AND (H.idrekanan='$idrekanan' OR H.idrekanan='0')
			) S ON S.id=H.id
			LIMIT 1
		   ";
		   // print_r($q);exit;
		
		$data=$this->db->query($q)->row_array();
		
		$this->output->set_output(json_encode($data));
	}
	public function simpan_add()
    {
		$trxid=$this->input->post('trxid');
		if ($trxid){
			$q="SELECT *FROM tkamaroperasi_pendaftaran WHERE id='$trxid'";
			$data_awal=$this->db->query($q)->row();
		}
		// print_r($data_awal);exit;
        $data=array(
            'idasalpendaftaran'=>$this->input->post('idasalpendaftaran'),
            'idpendaftaran'=>$this->input->post('idpendaftaran'),
            'tipe'=>$this->input->post('tipe'),
            'idpasien'=>$this->input->post('idpasien'),
            'namapasien'=>$this->input->post('namapasien'),
            'tanggaloperasi'=>YMDFormat($this->input->post('tanggaloperasi')),
            'waktumulaioperasi'=>$this->input->post('waktumulaioperasi'),
            'waktuselesaioperasi'=>$this->input->post('waktuselesaioperasi'),
            'diagnosa'=>$this->input->post('diagnosa'),
            'operasi'=>$this->input->post('operasi'),
            'jenis_operasi_id'=>$this->input->post('jenis_operasi_id'),
            'jenis_anestesi_id'=>$this->input->post('jenis_anestesi_id'),
            'kelompok_operasi_id'=>$this->input->post('kelompok_operasi_id'),
            // 'kelompok_tindakan_id'=>$this->input->post('kelompok_tindakan_id'),
            'kelas_tarif_id'=>$this->input->post('kelas_tarif_id'),
            'ruang_id'=>$this->input->post('ruang_id'),
            // 'kelompk_diagnosa_id'=>$this->input->post('kelompk_diagnosa_id'),
            'namapetugas'=>$this->session->userdata('user_name'),
            'idapprove'=>$this->session->userdata('user_id'),
            'catatan'=>$this->input->post('catatan'),
            'dpjp_id'=>$this->input->post('dpjp_id'),
            'jenis_bedah'=>$this->input->post('jenis_bedah'),
            'tarif_jenis_opr'=>$this->input->post('tarif_jenis_opr'),
            'statussetuju'=>$this->input->post('statussetuju'),
            'statusdatang'=>$this->input->post('statusdatang'),
            'st_ranap'=>$this->input->post('st_ranap'),
            'status'=>'1',
            // 'tanggal_disetujui'=>date('Y-m-d H:i:s'),
        );

        $kelompok_tindakan_id=$this->input->post('kelompok_tindakan_id');
        $kelompk_diagnosa_id=$this->input->post('kelompk_diagnosa_id');

        // print_r($data);
        if ($trxid){
			if ($data_awal->statussetuju=='0'){
				if ($this->input->post('statussetuju')=='1'){
					$data['user_menyetujui']=$this->session->userdata('user_id');
					$data['tanggal_disetujui']=date('Y-m-d H:i:s');
				}
			}
			$this->db->where('id',$trxid);
			$result = $this->db->update('tkamaroperasi_pendaftaran', $data);
			$id_insert=$trxid;
		}else{
			if ($this->input->post('statussetuju')=='1'){
				$data['user_menyetujui']=$this->session->userdata('user_id');
				$data['tanggal_disetujui']=date('Y-m-d H:i:s');
			}
			$data['user_pengaju']=$this->session->userdata('user_id');
			$data['tanggal_diajukan']=date('Y-m-d H:i:s');
			$result = $this->db->insert('tkamaroperasi_pendaftaran', $data);
			$id_insert=$this->db->insert_id();
		}
        
        if ($result) {
			if ($trxid){
				$this->db->where('idpendaftaranoperasi',$trxid);
				$this->db->delete('tkamaroperasi_tindakan');
				
				$this->db->where('idpendaftaranoperasi',$trxid);
				$this->db->delete('tkamaroperasi_diagnosa');
			}
            if ($kelompok_tindakan_id) {
				
                $arr=$kelompok_tindakan_id;
                foreach ($arr as $value) {
                    // $print_r($value);exit();
                    $data_tindakan=array(
                        'idpendaftaranoperasi'=>$id_insert,
                        'kelompok_tindakan_id'=>$value,
                    );
                    $this->db->insert('tkamaroperasi_tindakan', $data_tindakan);
                }
            }
            if ($kelompk_diagnosa_id) {
                $arr=$kelompk_diagnosa_id;
                foreach ($arr as $value) {
                    // $print_r($value);exit();
                    $data_tindakan=array(
                        'idpendaftaranoperasi'=>$id_insert,
                        'kelompok_diagnosa_id'=>$value,
                    );
                    $this->db->insert('tkamaroperasi_diagnosa', $data_tindakan);
                }
            }
			if ($trxid==''){
				$login_tipepegawai=$this->session->userdata('login_tipepegawai');
				$login_pegawai_id=$this->session->userdata('login_pegawai_id');
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$pendaftaran_id_ranap=$this->input->post('idpendaftaran');
				$iddokter=$this->input->post('dpjp_id');
				if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
					$iddokter_peminta=$login_ppa_id;
					$dpjp=$login_pegawai_id;
				}else{
					$q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
					$iddokter_peminta=$this->db->query($q)->row('id');
					$dpjp=$iddokter;
				}

				$q="SELECT *FROM tpoliklinik_ranap_perencanaan_bedah WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND pendaftaran_bedah_id IS NULL AND status_assemen='2'";
				$row_data=$this->db->query($q)->row('assesmen_id');
				if ($row_data){
					$data_rencana=array(
						'jumlah_kunjungan' => '1',
						'st_kunjungan' => '1',
						'pendaftaran_bedah_id' => $id_insert,
					);
					$this->db->where('assesmen_id',$row_data);
					$hasil=$this->db->update('tpoliklinik_ranap_perencanaan_bedah',$data_rencana);
				}else{
					//Insert Ke Rencana Bedah
					$tanggal_bedah= YMDFormat($this->input->post('tanggaloperasi')). ' ' .$this->input->post('waktumulaioperasi');
					$data_rencana=array(
						'idtipe' => '3',
						'idpoliklinik' => '',
						'tanggal_input' => date('Y-m-d H:i:s'),
						'pendaftaran_id' => '',
						'pendaftaran_id_ranap' => $this->input->post('idpendaftaran'),
						'st_ranap' => '1',
						'idpasien' => $this->input->post('idpasien'),
						'created_date' => date('Y-m-d H:i:s'),
						'created_ppa' => $login_ppa_id,
						'status_assemen' => '2',
						'tanggal_permintaan' => date('Y-m-d H:i:s'),
						'iddokter_peminta' => $iddokter_peminta,
						'dengan_diagnosa' => $this->input->post('diagnosa'),
						'catatan' => $this->input->post('catatan'),
						'tipe' => '3',
						'dpjp' => $dpjp,
						'tanggal_bedah' => $tanggal_bedah,
						'rencana_tindakan' => $this->input->post('operasi'),
						'jumlah_kunjungan' => '1',
						'st_kunjungan' => '1',
						'pendaftaran_bedah_id' => $id_insert,
					);
					$hasil=$this->db->insert('tpoliklinik_ranap_perencanaan_bedah',$data_rencana);
				}
			}
			
            return $hasil;
        } else {
            return false;
        }
    }
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$iddokter =$this->input->post('iddokter');
			$idtipe =$this->input->post('idtipe');
			$idtipe_asal =$this->input->post('idtipe_asal');
			$cari_pasien =$this->input->post('cari_pasien');
			$nopendaftaran=$this->input->post('nopendaftaran');
			$idruangan=$this->input->post('idruangan');
			$idkelas=$this->input->post('idkelas');
			$idbed=$this->input->post('idbed');
			$norujukan=$this->input->post('norujukan');
			$iddokter_perujuk=$this->input->post('iddokter_perujuk');
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$idrekanan=$this->input->post('idrekanan');
			$status_tindakan_index=$this->input->post('status_tindakan_index');
			
			$idpoliklinik_array=$this->input->post('idpoliklinik_array');
			if ($idpoliklinik_array){
				$idpoliklinik_array=implode(", ", $idpoliklinik_array);
				$where .=" AND (H.idpoliklinik_asal IN (".$idpoliklinik_array.")) ";
			}
			$iddokter_perujuk_array=$this->input->post('iddokter_perujuk_array');
			if ($iddokter_perujuk_array){
				$iddokter_perujuk_array=implode(", ", $iddokter_perujuk_array);
				$where .=" AND (H.iddokter_perujuk IN (".$iddokter_perujuk_array.")) ";
			}
			$iddokter_dpjp=$this->input->post('iddokter_dpjp');
			if ($iddokter_dpjp){
				$iddokter_dpjp=implode(", ", $iddokter_dpjp);
				$where .=" AND (H.iddokterpenanggungjawab IN (".$iddokter_dpjp.")) ";
			}
			$dpjp_pendukung_array=$this->input->post('dpjp_pendukung_array');
			if ($dpjp_pendukung_array){
				$dpjp_pendukung_array=implode(", ", $dpjp_pendukung_array);
				$where .=" AND (HP.iddokter IN (".$dpjp_pendukung_array.")) ";
			}
			$idkelompokpasien_array=$this->input->post('idkelompokpasien_array');
			if ($idkelompokpasien_array){
				$idkelompokpasien_array=implode(", ", $idkelompokpasien_array);
				$where .=" AND (H.idkelompokpasien IN (".$idkelompokpasien_array.")) ";
			}
			$idrekanan_array=$this->input->post('idrekanan_array');
			if ($idrekanan_array){
				$idrekanan_array=implode(", ", $idrekanan_array);
				$where .=" AND (H.idrekanan IN (".$idrekanan_array.")) ";
			}
			$idruangan_array=$this->input->post('idruangan_array');
			if ($idruangan_array){
				$idruangan_array=implode(", ", $idruangan_array);
				$where .=" AND (H.idruangan IN (".$idruangan_array.")) ";
			}
			$idkelas_array=$this->input->post('idkelas_array');
			if ($idkelas_array){
				$idkelas_array=implode(", ", $idkelas_array);
				$where .=" AND (H.idkelas IN (".$idkelas_array.")) ";
			}
			$idbed_array=$this->input->post('idbed_array');
			if ($idbed_array){
				$idbed_array=implode(", ", $idbed_array);
				$where .=" AND (H.idkelas IN (".$idbed_array.")) ";
			}
			$status_tindakan_array=$this->input->post('status_tindakan_array');
			if ($status_tindakan_array){
				$where .=" AND (";
				$where_tindakan='';
				
				if (in_array('2', $status_tindakan_array)){
					// if ($statuskasir=='1' && $statuspembayaran=='0'){
					$where_tindakan .=($where_tindakan==''?'':' OR ')."(H.statuskasir='1' AND H.statuspembayaran='0') ";
				}
				if (in_array('3', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.st_terima_ranap='1' AND H.status='1') ";
				}
				if (in_array('4', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuspembayaran='1') ";
				}
				if (in_array('5', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuscheckout='1') ";
				}
				$where .=$where_tindakan.")";
			}
			$no_medrec=$this->input->post('no_medrec');
			if ($no_medrec !=''){
				$where .=" AND (H.no_medrec LIKE '%".$no_medrec."%') ";
			}
			$namapasien=$this->input->post('namapasien');
			if ($namapasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$namapasien."%') ";
			}
			
			
			if ($iddokter!='#'){
				$where .=" AND (H.iddokterpenanggungjawab='$iddokter') ";
			}
			if ($idrekanan!='#'){
				$where .=" AND (H.idrekanan='$idrekanan') ";
			}
			if ($idkelompokpasien!='#'){
				$where .=" AND (H.idkelompokpasien='$idkelompokpasien') ";
			}
			if ($iddokter_perujuk!='#'){
				$where .=" AND (H.iddokter_perujuk='$iddokter_perujuk') ";
			}
			if ($idbed!='#'){
				$where .=" AND (H.idbed='$idbed') ";
			}
			if ($idruangan!='#'){
				$where .=" AND (H.idruangan='$idruangan') ";
			}
			if ($idkelas!='#'){
				$where .=" AND (H.idkelas='$idkelas') ";
			}
			if ($idtipe!='#'){
				$where .=" AND (H.idtipe='$idtipe') ";
			}
			if ($idtipe_asal!='#'){
				$where .=" AND (H.idtipe_asal='$idtipe_asal') ";
			}
			if ($norujukan !=''){
				$where .=" AND (H.nopermintaan LIKE '%".$norujukan."%') ";
			}
			if ($nopendaftaran !=''){
				$where .=" AND (H.nopendaftaran LIKE '%".$nopendaftaran."%' OR H.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			}
			
			if ($cari_pasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$cari_pasien."%' OR H.no_medrec LIKE '%".$cari_pasien."%') ";
			}
			// print_r($where);exit;
			$tab =$this->input->post('tab');
			if ($tab=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($tab=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1' AND H.statuscheckout='0') ";
			}
			if ($tab=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($tab=='5'){
				$where .=" AND (H.statuscheckout='1') ";
			}
			if ($status_tindakan_index=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($status_tindakan_index=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			}
			if ($status_tindakan_index=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($status_tindakan_index=='5'){
				$where .=" AND (H.statuscheckout='1') ";
				if ($tanggal_1 !=''){
					$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
				}else{
					$where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
				}
			}
			
			
			$tanggal_1_trx =$this->input->post('tanggal_1_trx');
			if ($tanggal_1_trx !=''){
				$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2_trx)."'";
			}
			$tanggal_1_pulang =$this->input->post('tanggal_1_pulang');
			if ($tanggal_1_pulang !=''){
				$where .=" AND DATE(H.tanggalcheckout) >='".YMDFormat($tanggal_1_pulang)."' AND DATE(H.tanggalcheckout) <='".YMDFormat($tanggal_2_pulang)."'";
			}
			$this->select = array();
			$get_alergi_sql=get_alergi_sql();
			$from="
					(
						SELECT 
						PB.status_verifikasi_bayar,PB.st_verifikasi_piutang
						,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
						,H.*,MKL.nama as nama_kelas 
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
						,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping,KO.id as pendaftaran_bedah_id
						FROM trawatinap_pendaftaran H
						LEFT JOIN tkamaroperasi_pendaftaran KO ON KO.idpendaftaran=H.id AND KO.st_ranap='1' AND KO.statussetuju='1' AND KO.status!='0'
						LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
						LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
						LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
						LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
						LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mbed MB on MB.id=H.idbed						
						LEFT JOIN musers UH on UH.id=H.deleted_by	
						LEFT JOIN (
							".$get_alergi_sql."
						) A ON A.idpasien=H.idpasien 
						LEFT JOIN (
							SELECT H.idtindakan as pendaftaran_id,H.status_verifikasi as status_verifikasi_bayar,D.st_verifikasi_piutang FROM trawatinap_tindakan_pembayaran H
							LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id AND D.st_verifikasi_piutang=1 AND D.idmetode IN (6,8)
							WHERE H.statusbatal='0'
							GROUP BY H.id

						) PB ON PB.pendaftaran_id=H.id
						
						WHERE H.idtipe IS NOT NULL ".$where."
						GROUP BY H.id
						
						ORDER BY H.tanggaldaftar DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  if ($r->idtipe=='1'){
			  $info_bed='<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>';
		  }else{
			  $info_bed='';
		  }
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_checkout='';
		  $btn_terima='';
		  $btn_layani='';
		  $btn_panel='';
		  $btn_add_jawal='';
		  $btn_verifikasi='';
		  $btn_verifikasi=($r->status_verifikasi_bayar?text_success('VERIFIED'):'');
		  $btn_verifikasi_piutang='';
		  $btn_verifikasi_piutang='&nbsp;&nbsp;'.($r->st_verifikasi_piutang?text_danger('VERIFIED PIUTANG'):'');
		  if (UserAccesForm($user_acces_form,array('2288'))){
		  $btn_add_jawal='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Tambah Jadwal" onclick="add_jawal('.$r->id.')" class="btn btn-danger btn-xs"><i class="fa fa-plus"></i> &nbsp;&nbsp;JADWAL&nbsp;&nbsp;</a> </div>';
		  }
		  // $label_total='Total Keseluruhan : Rp. 1.780.000';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
			  if ($r->status!='0'){
				  if (UserAccesForm($user_acces_form,array('2291'))){
					  if ($r->pendaftaran_bedah_id){
						  $str_kosong="";
						$btn_layani='<div class="push-5-t"><button onclick="pilih_ko('.$r->id.','.$str_kosong.')" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs"><i class="fa fa-user-md"></i> LAYANI</button> </div>';
					  }
				  }
				  if (UserAccesForm($user_acces_form,array('2290'))){
					  $btn_panel=div_panel_kendali_ri($user_acces_form,$r->id);
				  }
			  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_layani.'
									'.$btn_panel.'
									'.$btn_add_jawal.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="text-danger font-s13 push-5-t text-bold">'.$btn_verifikasi.$btn_verifikasi_piutang.'</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='';
		 
          $result[] = $btn_5;
		  
		  
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i>'.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_ranap($r->status,$r->st_terima_ranap,$r->statuscheckout,$r->statuspembayaran,$r->statuskasir).'</div>
									'.$info_bed.'
									<div class="h5 push-10-t text-primary"><strong>'.GetTipeRujukanRawatInap($r->idtipe).'</strong></div>
									<div class="h5 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggaldaftar).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_ranap($status,$st_terima_ranap,$statuscheckout,$statuspembayaran,$statuskasir){
	  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-facebook pull-left "></i> Login with Facebook</button>';
	  if ($status=='0'){
		  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button"><i class="fa fa-times pull-left "></i>DIBATALKAN</button>';
	  }else{
		  if ($st_terima_ranap=='0'){
			  $label='<button class="btn btn-xs btn-block btn-info push-10" type="button"><i class="fa fa-wrench pull-left "></i>MENUNGGU DIRAWAT</button>';
		  }
		  if ($st_terima_ranap=='1' && $statuscheckout=='0'){
			  $label='<button class="btn btn-xs btn-block btn-warning push-10" type="button"><i class="fa fa-bed pull-left"></i>DIRAWAT</button>';
		  }
		 
		  if ($statuskasir=='1' && $statuspembayaran=='0'){
			  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button">MENUNGGU TRANSAKSI</button>';
		  }elseif ($statuskasir=='1' && $statuspembayaran=='1'){
			  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button">SELESAI</button>';
		  }
		   
		  if ($statuscheckout=='1'){
			  $label='<button class="btn btn-xs btn-block btn-success push-10" type="button">PULANG</button>';
		  }
	  }
	  
	  return $label;
  }
	public function get_edit($id)
	{
		$arr = $this->Tkamar_bedah_model->get_edit($id);
		$arr['diagnosa']=str_replace("&nbsp;","",strip_tags($arr['diagnosa']));
		$q="SELECT *from setting_tko";
		$data_setting=$this->db->query($q)->row_array();
		// $arr['nopermintaan']=$this->get_no_bedah();
		// print_r($arr->idpendaftaran);exit;
		$arr = array_merge($arr,$data_setting);
		$arr['label_pembuat']='Dibuat Oleh  '.($arr['namapetugas']).'  '.HumanDateLong($arr['tanggal_trx']);
		$this->output->set_output(json_encode($arr));
	}
	public function simpan_gabung()
    {
        $id=$this->input->post('idjadwal');
        $data=array(
            'tipe'=>$this->input->post('idtipe'),
            'idasalpendaftaran'=>$this->input->post('idasalpendaftaran'),
            'idpendaftaran'=>$this->input->post('idpendaftaran'),
            'idpasien'=>$this->input->post('idpasien'),
            'st_ranap'=>1,
        );

        // print_r($data);
        $this->db->where('id', $id);
        $result = $this->db->update('tkamaroperasi_pendaftaran', $data);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
  function getIndex_all_2()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$where_22='';
		$where_33='';
		$tab_utama=$this->input->post('tab_utama');
		$tab_atas=$this->input->post('tab_atas');
		
		
		
		if ($tab_atas=='22'){
			$idtipe_22=$this->input->post('idtipe_22');
			$asal_22=$this->input->post('asal_22');
			$iddokter_bedah_22=$this->input->post('iddokter_bedah_22');
			$iddokter_peminta_22=$this->input->post('iddokter_peminta_22');
			$tanggal_22_1=$this->input->post('tanggal_22_1');
			$tanggal_22_2=$this->input->post('tanggal_22_2');
			$nopermintaan_22=$this->input->post('nopermintaan_22');
			$cari_pasien_22=$this->input->post('cari_pasien_22');
			
			// if ($tab_utama=='2'){
				// $where .=" AND H.statussetuju='0' AND H.status='1'";
			// }
			// if ($tab_utama=='3'){
				// $where .=" AND H.statussetuju='1'  AND H.status='1'";
			// }
			// if ($tab_utama=='4'){
				// $where .=" AND H.statusdatang='1'  AND H.status='1'";
			// }
			// if ($tab_utama=='5'){
				// $where .=" AND H.statusdatang='0'  AND H.status='1'";
			// }
			// if ($tab_utama=='5'){
				// $where .=" AND H.statusdatang='0'  AND H.status='1'";
			// }
			if ($idtipe_22!='#'){
				$where .=" AND RB.idtipe='$idtipe_22'";
			}
			if ($asal_22!='#'){
				$where .=" AND RB.st_ranap='$asal_22'";
			}
			
			if ($iddokter_bedah_22!='#'){
				$where .=" AND RB.dpjp='$iddokter_22'";
			}
			if ($iddokter_peminta_22!='#'){
				$where .=" AND RB.iddokter_peminta='$iddokter_peminta_22'";
			}
			if ($nopermintaan_22!=''){
				$where .=" AND (RB.nopermintaan LIKE '%".$nopermintaan_22."%') ";
			}
			if ($cari_pasien_22!=''){
				$where .=" AND (RB.nama_pasien LIKE '%".$cari_pasien_22."%' OR RB.nomedrec_pasien LIKE '%".$cari_pasien_22."%') ";
			}
			if ($tanggal_22_1!=''){
				$where .=" AND DATE(RB.tanggal_input) >='".YMDFormat($tanggal_22_1)."' AND DATE(RB.tanggal_input) <='".YMDFormat($tanggal_22_2)."'";
			}
			
			
		}
		if ($tab_atas=='33'){
			$idtipe_33=$this->input->post('idtipe_33');
			$idruangan_33=$this->input->post('idruangan_33');
			$idkelas_33=$this->input->post('idkelas_33');
			$idbed_33=$this->input->post('idbed_33');
			$iddokter_33=$this->input->post('iddokter_33');
			$tanggal_33_1=$this->input->post('tanggal_33_1');
			$tanggal_33_2=$this->input->post('tanggal_33_2');
			$cari_pasien_33=$this->input->post('cari_pasien_33');
			
			if ($tab_utama=='2'){
				$where .=" AND H.statussetuju='0' AND H.status='1'";
			}
			if ($tab_utama=='3'){
				$where .=" AND H.statussetuju='1'  AND H.status='1'";
			}
			if ($tab_utama=='4'){
				$where .=" AND H.statusdatang='1'  AND H.status='1'";
			}
			if ($tab_utama=='5'){
				$where .=" AND H.statusdatang='0'  AND H.status='1'";
			}
			if ($tab_utama=='5'){
				$where .=" AND H.statusdatang='0'  AND H.status='1'";
			}
			if ($idtipe_33!='#'){
				$where .=" AND H.tipe='$idtipe_33'";
			}
			if ($idruangan_33!='#'){
				$where .=" AND H.ruang_id='$idruangan_33'";
			}
			if ($idkelas_33!='#'){
				$where .=" AND H.kelas_tarif_id='$idkelas_33'";
			}
			if ($idkelas_33!='#'){
				$where .=" AND H.kelas_tarif_id='$idkelas_33'";
			}
			if ($idbed_33!='#'){
				$where .=" AND RI.idbed='$idbed_33'";
			}
			if ($iddokter_33!='#'){
				$where .=" AND RI.dpjp_id='$iddokter_33'";
			}
			if ($cari_pasien_33!=''){
				$where .=" AND (H.namapasien LIKE '%".$cari_pasien_33."%' OR RI.no_medrec LIKE '%".$cari_pasien_33."%' OR TP.no_medrec LIKE '%".$cari_pasien_33."%') ";
			}
			if ($tanggal_33_1!=''){
				$where .=" AND DATE(H.tanggaloperasi) >='".YMDFormat($tanggal_33_1)."' AND DATE(H.tanggaloperasi) <='".YMDFormat($tanggal_33_2)."'";
			}
			
			
		}
		$this->select = array();
		$get_alergi_sql=get_alergi_sql();
		if ($tab_atas=='22'){
			$from="
				(
					SELECT *FROM (
						SELECT H.id,RB.assesmen_id,
						H.waktumulaioperasi,H.waktuselesaioperasi,H.tanggaloperasi,ruang.nama as ruang_operasi,
						CASE WHEN H.tipe='1' THEN 'RAWAT INAP' WHEN H.tipe='2' THEN 'ODS' END as tipe,
						H.tipe as idtipe,MRU.nama as nama_ruangan,
						CASE WHEN RB.st_ranap='0' THEN TP.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran,
						CASE WHEN RB.st_ranap='0' THEN TP.tanggaldaftar ELSE RI.tanggaldaftar END as tanggaldaftar,
						CASE WHEN RB.st_ranap='0' THEN TP.namapasien ELSE RI.namapasien END as namapasien,
						CASE WHEN RB.st_ranap='0' THEN TP.no_medrec ELSE RI.no_medrec END as no_medrec,
						CASE WHEN RB.st_ranap='0' THEN TP.tanggal_lahir ELSE RI.tanggal_lahir END as tanggal_lahir,
						CASE WHEN RB.st_ranap='0' THEN TP.title ELSE RI.title END as title,
						CASE WHEN RB.st_ranap='0' THEN TP.jenis_kelamin ELSE RI.jenis_kelamin END as jenis_kelamin,
						CASE WHEN RB.st_ranap='0' THEN DRP.nama ELSE DRI.nama END as nama_dokter,
						CASE WHEN RB.st_ranap='0' THEN JK2.ref ELSE JK1.ref END as jk,
						CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header,
						K.nama as nama_kelas,B.nama as nama_bed,
						MP.nama as nama_poli,
						H.diagnosa,H.operasi,H.statussetuju,H.statusdatang,H.`status`,RI.statuskasir,RI.statuspembayaran
						,RB.pendaftaran_bedah_id,RB.nopermintaan,RB.tanggal_input,PC.nama as pembuat,RB.created_date
						,CASE WHEN RB.tipe='3' THEN 'Rawat Inap' ELSE 'ODS' END as rencana,RB.dengan_diagnosa,RB.rencana_tindakan
						,RB.tanggal_bedah,RB.catatan as catatan_rencana,RB.dpjp,DRB.nama as nama_dokter_bedah
						,H.umurtahun,H.umurbulan,H.umurhari,H.idpasien

						from tpoliklinik_ranap_perencanaan_bedah RB
						LEFT JOIN tkamaroperasi_pendaftaran  H  ON RB.pendaftaran_bedah_id=H.id
						LEFT JOIN trawatinap_pendaftaran RI ON RI.id=RB.pendaftaran_id_ranap 
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=RB.pendaftaran_id
						LEFT JOIN mruangan ruang ON ruang.id=H.ruang_id
						LEFT JOIN merm_referensi JK1 ON JK1.ref_head_id='1' AND JK1.nilai=RI.jenis_kelamin
						LEFT JOIN merm_referensi JK2 ON JK2.ref_head_id='1' AND JK2.nilai=TP.jenis_kelamin
						LEFT JOIN mpoliklinik MP ON MP.id=TP.idpoliklinik
						LEFT JOIN mkelas K ON K.id=RI.idkelas
						LEFT JOIN mbed B ON B.id=RI.idbed
						LEFT JOIN mruangan MRU ON MRU.id=RI.idruangan
						LEFT JOIN mppa PC ON PC.id=RB.created_ppa
						LEFT JOIN mdokter DRB ON DRB.id=RB.dpjp
						LEFT JOIN mdokter DRI ON DRI.id=RI.iddokterpenanggungjawab
						LEFT JOIN mdokter DRP ON DRP.id=TP.iddokter
						LEFT JOIN (
							".$get_alergi_sql."
						) A ON A.idpasien=H.idpasien 
						WHERE RB.status_assemen='2' ".$where."
						GROUP BY RB.assesmen_id
					) KO
				) as tbl
			";
		}else{
			$from="
				(
					SELECT *FROM (
						SELECT H.id,
						H.waktumulaioperasi,H.waktuselesaioperasi,H.tanggaloperasi,ruang.nama as ruang_operasi,
						CASE WHEN H.tipe='1' THEN 'RAWAT INAP' WHEN H.tipe='2' THEN 'ODS' END as tipe,
						H.tipe as idtipe,MRU.nama as nama_ruangan,
						CASE WHEN H.st_ranap='0' THEN TP.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran,
						CASE WHEN H.st_ranap='0' THEN TP.tanggaldaftar ELSE RI.tanggaldaftar END as tanggaldaftar,
						CASE WHEN H.st_ranap='0' THEN TP.namapasien ELSE RI.namapasien END as namapasien,
						CASE WHEN H.st_ranap='0' THEN TP.no_medrec ELSE RI.no_medrec END as no_medrec,
						CASE WHEN H.st_ranap='0' THEN TP.tanggal_lahir ELSE RI.tanggal_lahir END as tanggal_lahir,
						CASE WHEN H.st_ranap='0' THEN TP.title ELSE RI.title END as title,
						CASE WHEN H.st_ranap='0' THEN TP.jenis_kelamin ELSE RI.jenis_kelamin END as jenis_kelamin,
						CASE WHEN H.st_ranap='0' THEN DRP.nama ELSE DRI.nama END as nama_dokter,
						CASE WHEN H.st_ranap='0' THEN JK2.ref ELSE JK1.ref END as jk,
						CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header,
						K.nama as nama_kelas,B.nama as nama_bed,
						MP.nama as nama_poli,
						H.diagnosa,H.operasi,H.statussetuju,H.statusdatang,H.`status`,RI.statuskasir,RI.statuspembayaran
						,RB.pendaftaran_bedah_id,RB.nopermintaan,RB.tanggal_input,PC.nama as pembuat,RB.created_date
						,CASE WHEN RB.tipe='3' THEN 'Rawat Inap' ELSE 'ODS' END as rencana,RB.dengan_diagnosa,RB.rencana_tindakan
						,RB.tanggal_bedah,RB.catatan as catatan_rencana,RB.dpjp,DRB.nama as nama_dokter_bedah
						,H.umurtahun,H.umurbulan,H.umurhari,H.idpasien

						from tkamaroperasi_pendaftaran H
						LEFT JOIN tpoliklinik_ranap_perencanaan_bedah RB ON RB.pendaftaran_bedah_id=H.id
						LEFT JOIN mruangan ruang ON ruang.id=H.ruang_id
						LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idpendaftaran
						LEFT JOIN merm_referensi JK1 ON JK1.ref_head_id='1' AND JK1.nilai=RI.jenis_kelamin
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpendaftaran
						LEFT JOIN merm_referensi JK2 ON JK2.ref_head_id='1' AND JK2.nilai=TP.jenis_kelamin
						LEFT JOIN mpoliklinik MP ON MP.id=TP.idpoliklinik
						LEFT JOIN mkelas K ON K.id=RI.idkelas
						LEFT JOIN mbed B ON B.id=RI.idbed
						LEFT JOIN mruangan MRU ON MRU.id=RI.idruangan
						LEFT JOIN mppa PC ON PC.id=RB.created_ppa
						LEFT JOIN mdokter DRB ON DRB.id=RB.dpjp
						LEFT JOIN mdokter DRI ON DRI.id=RI.iddokterpenanggungjawab
						LEFT JOIN mdokter DRP ON DRP.id=TP.iddokter
						LEFT JOIN (
							".$get_alergi_sql."
						) A ON A.idpasien=H.idpasien 
						WHERE H.id >= 2406 ".$where."
						GROUP BY H.id
					) KO WHERE id <> ''
				) as tbl
			";
		}
		
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  if ($r->idtipe=='1'){
			  $info_bed='<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>';
		  }else{
			  $info_bed='';
		  }
		  
		  $btn_checkout='';
		  $btn_terima='';
		  $btn_layani='';
		  $btn_panel='';
		  $btn_add_jawal='';
		  $btn_add_jawal='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Tambah Jadwal" onclick="add_jawal('.$r->id.')" class="btn btn-danger btn-xs"><i class="fa fa-plus"></i> &nbsp;&nbsp;JADWAL&nbsp;&nbsp;</a> </div>';
		  
		  // $label_total='Total Keseluruhan : Rp. 1.780.000';
		 
			  if ($r->status!='0'){
					
				  if ($tab_atas=='22'){
					  if ($r->id){
						  if (UserAccesForm($user_acces_form,array('2293'))){
							$btn_layani='<div class="push-5-t"><button ype="button" data-toggle="tooltip" title="Proses Jadwal" onclick="show_proses('.$r->id.')" class="btn btn-block btn-primary btn-xs"><i class="fa fa-check"></i> PROSES JADWAL</a> </div>';
						}
					  }
					  $btn_panel=div_panel_kendali_rencana_bedah($user_acces_form,$r);
					  
				  }
				  if ($tab_atas=='33'){
					  $btn_panel=div_panel_kendali_jadwal_bedah($user_acces_form,$r);
					  
				  }
			  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_layani.'
									'.$btn_panel.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_tgl_bedah='';
		  if ($tab_atas=='33'){
			  $btn_tgl_bedah='
				<div class="text-center text-white bg-warning push-5-t">
					<strong>'.HumanDateShort($r->tanggaloperasi).'</strong>
				</div>
				<div class="text-center text-white bg-success push-5-t">
					<strong>'.HumanTimeShort($r->waktumulaioperasi).' - '.HumanTimeShort($r->waktuselesaioperasi).'</strong>
				</div>
				
			  ';
		  }
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 20%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
										'.$btn_tgl_bedah.'
									</div>
								</td>
								<td class="bg-white" style="width: 80%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" '.($r->riwayat_alergi_header=='0'?'disabled':'').' onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									'.($r->nopermintaan?'<div class="push-5-t text-danger">'.$r->nopermintaan.' | '.HumanDateLong($r->tanggal_input).' | '.$r->pembuat.'</div>':'').'
									
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='';
		  if ($tab_atas=='22'){
			  $btn_5 .='<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-white" style="width: 33%;">
									<div class=" text-primary text-center">RENCANA</div>
									<div class=" text-muted  text-center push-5-t">'.($r->pendaftaran_bedah_id?$r->rencana:'&nbsp;').'<div>
								</td>
								<td class="bg-white" style="width: 33%;">
									<div class=" text-primary text-center">DIAGNOSA</div>
									<div class=" text-muted  text-center push-5-t">'.($r->dengan_diagnosa?$r->dengan_diagnosa:'&nbsp;').'</div>
								</td>
								<td class="bg-white" style="width: 33%;">
									<div class=" text-primary text-center">TINDAKAN PEMBEDAHAN</div>
									<div class=" text-muted  text-center push-5-t">'.($r->rencana_tindakan).'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white" style="width: 33%;">
									<div class=" text-primary text-center">WAKTU PEMBEDAHAN</div>
									<div class=" text-muted  text-center push-5-t">'.($r->tanggal_bedah?HumanDateLong($r->tanggal_bedah):'&nbsp;').'<div>
								</td>
								<td class="bg-white" style="width: 33%;">
									<div class=" text-primary text-center">CATATAN</div>
									<div class=" text-muted  text-center push-5-t">'.($r->catatan_rencana?$r->catatan_rencana:'&nbsp;').'</div>
								</td>
								<td class="bg-white" style="width: 33%;">
									<div class=" text-primary text-center">DOKTER BEDAH</div>
									<div class=" text-muted  text-center push-5-t">'.($r->nama_dokter_bedah).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
		  }else{
			  $btn_5 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 50%;">
									<div class=" text-primary text-center">DIAGNOSA</div>
									<div class=" text-muted  text-center push-5-t">'.($r->diagnosa?$r->diagnosa:'&nbsp;').'</div>
								</td>
								<td class="bg-white" style="width: 50%;">
									<div class=" text-primary text-center">TINDAKAN PEMBEDAHAN</div>
									<div class=" text-muted  text-center push-5-t">'.($r->operasi).'</div>
								</td>
							</tr>
							
						</tbody>
					</table>';
		  }
		   
          $result[] = $btn_5;
		  $btn_3='';
		  if ($r->statusdatang=='1'){
			  
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan?$r->nama_ruangan.' - '.$r->nama_kelas.' - '.$r->nama_bed:$r->nama_poli).'</div>
									<div class=""><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
		  }
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div>'.($r->status=='0'?'<span class="h5">'.text_danger('DIBATALKAN').'</div>': GetStatusBedah($r->statussetuju,$r->statusdatang)).'</div>
									
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  public function batalkan()
	{
		$id     = $this->input->post('id');

		// $idtolak=$this->session->userdata('user_id');
		// $tanggal_tolak=date('Y-m-d H:s:i');
		$q="UPDATE tkamaroperasi_pendaftaran SET status='0' WHERE id='$id'";



		$result = $this->db->query($q);

		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	function insert_jadwal(){
		$assesmen_id     = $this->input->post('id');
		$q="SELECT *FROM tpoliklinik_ranap_perencanaan_bedah H WHERE H.assesmen_id='$assesmen_id'";
			
			$data_ass=$this->db->query($q)->row();
			$data_ko=array(
				'idasalpendaftaran'=>($data_ass->idtipe=='1'?'1':'2'),
				'idpendaftaran'=>($data_ass->idtipe=='1'?$data_ass->pendaftaran_id:$data_ass->pendaftaran_id_ranap),
				'tipe'=>($data_ass->tipe=='3'?'1':'2'),
				'idpasien'=>$data_ass->idpasien,
				'namapasien'=>$data_ass->nama_pasien,
				'tanggaloperasi'=> YMDFormat($data_ass->tanggal_bedah),
				'waktumulaioperasi'=>HumanTime($this->input->post('waktumulaioperasi')),
				'waktuselesaioperasi'=>null,
				'diagnosa'=>$data_ass->dengan_diagnosa,
				'operasi'=>$data_ass->rencana_tindakan,
				'namapetugas'=>$this->session->userdata('user_name'),
				'idapprove'=>$this->session->userdata('user_id'),
				'catatan'=>$data_ass->catatan,
				'dpjp_id'=>$data_ass->dpjp,
				'st_ranap'=>$data_ass->st_ranap,
				'statussetuju'=>0,
				'statusdatang'=>'0',
				'status'=>'1',
				'tanggal_diajukan'=>date('Y-m-d H:i:s'),
				'user_pengaju'=>$this->session->userdata('user_id'),
				// 'tanggal_disetujui'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->insert('tkamaroperasi_pendaftaran',$data_ko);
			$pendaftaran_bedah_id= $this->db->insert_id();
			// print_r($pendaftaran_bedah_id);exit;
			// pendaftaran_bedah_id
			if ($result){
				$q="UPDATE tpoliklinik_ranap_perencanaan_bedah set pendaftaran_bedah_id='$pendaftaran_bedah_id' WHERE assesmen_id='$assesmen_id'";
				
				$result=$this->db->query($q);
			}
			$this->output->set_output(json_encode($result));
	}
	
	function simpan_tranaksi_bedah(){
		$pendaftaran_bedah_id     = $this->input->post('pendaftaran_bedah_id');
		$data=array(
			'dpjp_id' => $this->input->post('tdpjp'),
			'tanggaloperasi' =>YMDFormat($this->input->post('tanggaloperasi')),
			'waktumulaioperasi' => $this->input->post('waktumulaioperasi'),
			'waktuselesaioperasi' => $this->input->post('waktuselesaioperasi'),
			'diagnosa' => $this->input->post('diagnosa'),
			'operasi' => $this->input->post('operasi'),
			'catatan' => $this->input->post('catatan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'jenis_anestesi_id' => $this->input->post('jenis_anestesi_id'),
			'kelompok_operasi_id' => $this->input->post('kelompok_operasi_id'),
			'kelompok_tindakan_id' => $this->input->post('kelompok_tindakan_id'),
			'kelas_tarif_id' => $this->input->post('kelas_tarif_id'),
			'jenis_bedah' => $this->input->post('jenis_bedah'),
			'tarif_jenis_opr' => $this->input->post('tarif_jenis_opr'),
		);
		// print_r($data);exit;
		$this->db->where('id',$pendaftaran_bedah_id);
		$result=$this->db->update('tkamaroperasi_pendaftaran',$data);
		$kelompok_tindakan= $this->input->post('kelompok_tindakan');
		$kelompok_diagnosa= $this->input->post('kelompok_diagnosa');
		if ($pendaftaran_bedah_id){
			$this->db->where('idpendaftaranoperasi',$pendaftaran_bedah_id);
			$this->db->delete('tkamaroperasi_tindakan');
			
			$this->db->where('idpendaftaranoperasi',$pendaftaran_bedah_id);
			$this->db->delete('tkamaroperasi_diagnosa');
			
			
			
		}
		$id_insert=$pendaftaran_bedah_id;
		if ($kelompok_tindakan) {
			
			$arr=$kelompok_tindakan;
			foreach ($arr as $value) {
				// $print_r($value);exit();
				$data_tindakan=array(
					'idpendaftaranoperasi'=>$id_insert,
					'kelompok_tindakan_id'=>$value,
				);
				$this->db->insert('tkamaroperasi_tindakan', $data_tindakan);
			}
		}
		if ($kelompok_diagnosa) {
			$arr=$kelompok_diagnosa;
			foreach ($arr as $value) {
				// $print_r($value);exit();
				$data_tindakan=array(
					'idpendaftaranoperasi'=>$id_insert,
					'kelompok_diagnosa_id'=>$value,
				);
				$this->db->insert('tkamaroperasi_diagnosa', $data_tindakan);
			}
		}
		$this->db->where('idpendaftaranoperasi',$pendaftaran_bedah_id);
		$this->db->delete('tkamaroperasi_konsulen');
		
		$konsulen_id= $this->input->post('konsulen_id');
		if ($konsulen_id) {
			$arr=$konsulen_id;
			foreach ($arr as $value) {
				// $print_r($value);exit();
				$data_tindakan=array(
					'idpendaftaranoperasi'=>$id_insert,
					'iddokter'=>$value,
				);
				$this->db->insert('tkamaroperasi_konsulen', $data_tindakan);
			}
		}
		$this->db->where('idpendaftaranoperasi',$pendaftaran_bedah_id);
		$this->db->delete('tkamaroperasi_instrumen');
		
		$instrumen_id= $this->input->post('instrumen_id');
		if ($instrumen_id) {
			$arr=$instrumen_id;
			foreach ($arr as $value) {
				// $print_r($value);exit();
				$data_tindakan=array(
					'idpendaftaranoperasi'=>$id_insert,
					'idpegawai'=>$value,
				);
				$this->db->insert('tkamaroperasi_instrumen', $data_tindakan);
			}
		}
		$this->db->where('idpendaftaranoperasi',$pendaftaran_bedah_id);
		$this->db->delete('tkamaroperasi_sirkuler');
		
		$sirkuler_id= $this->input->post('sirkuler_id');
		if ($sirkuler_id) {
			$arr=$sirkuler_id;
			foreach ($arr as $value) {
				// $print_r($value);exit();
				$data_tindakan=array(
					'idpendaftaranoperasi'=>$id_insert,
					'idpegawai'=>$value,
				);
				$this->db->insert('tkamaroperasi_sirkuler', $data_tindakan);
			}
		}
		
		$this->output->set_output(json_encode($result));
	}
	function tindakan($pendaftaran_bedah_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_bedah=$this->Tkamar_bedah_model->get_data_bedah($pendaftaran_bedah_id);
		$pendaftaran_id=$data_bedah['idpendaftaran'];
		$st_ranap=$data_bedah['st_ranap'];
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		// print_r($data);exit;
		
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$q="SELECT *from setting_tko";
		$data_setting=$this->db->query($q)->row_array();
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpoliklinik_ranap/tindakan/'.$pendaftaran_bedah_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['error'] 			= '';
		$data['status_assemen'] 			= '0';
		$data['assesmen_id'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Konsultasi';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Konsultasi ",'#'),
							  array("Dokter",'tpoliklinik_ranap')
							);
		$data_assemen=array('idpoli'=>'','iddokter_peminta'=>'');
		// print_r($data_assemen);exit;
		//CAtatan Evaluasi MPP
		if ($menu_kiri=='input_bedah_trx'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN';
		}
		if ($menu_kiri=='input_bedah_sewa_ruangan'){
			$data['judul_form']='TRANSAKSI SEWA RUANGAN';
		}
		if ($menu_kiri=='input_bedah_full_care'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN FULL CARE';
		}
		if ($menu_kiri=='input_bedah_do'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN DOKTER OPERATOR';
		}
		if ($menu_kiri=='input_bedah_da'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN DOKTER ANESTESI';
			$data['tind_dokter_anatesi'] = $this->model->tind_dokter_anatesi($pendaftaran_bedah_id);
		}
		if ($menu_kiri=='input_bedah_daa'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN ASSISTEN ANESTESI';
		}
		if ($menu_kiri=='input_bedah_dao'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN ASSISTEN OPERATOR';
		}
		if ($menu_kiri=='input_bedah_narcose'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN NARCOSE';
			$data['def_idtipe']=$data_setting['peggunaan_narcose'];
			$data['status_paket']='0';
			$data['paket_id']='';
			$data['nama_tabel_diskon']='merm_pengaturan_diskon_ranap_narcose_bedah';
			$data['nama_tabel_trx']='tkamaroperasi_narcose';
			$data_paket=$this->Tkamar_bedah_model->get_data_paket($menu_kiri);
			if ($data_paket){
				// print_r($data_paket);exit;
				$data['status_paket']='1';
				$data = array_merge($data,$data_paket);
			}
			
		}
		if ($menu_kiri=='input_bedah_obat'){
			$data['def_idtipe']=$data_setting['peggunaan_obat'];
			$data['judul_form']='TRANSAKSI PEMBEDAHAN OBAT';
			$data['status_paket']='0';
			$data['paket_id']='';
			$data['nama_tabel_diskon']='merm_pengaturan_diskon_ranap_obat_bedah';
			$data['nama_tabel_trx']='tkamaroperasi_obat';
		}
		if ($menu_kiri=='input_bedah_alkes'){
			$data['def_idtipe']=$data_setting['peggunaan_alkes'];
			$data['judul_form']='TRANSAKSI PEMBEDAHAN ALKES';
			$data['status_paket']='0';
			$data['paket_id']='';
			$data['nama_tabel_diskon']='merm_pengaturan_diskon_ranap_alkes_bedah';
			$data['nama_tabel_trx']='tkamaroperasi_alkes';
		}
		if ($menu_kiri=='input_bedah_implan'){
			$data['def_idtipe']=$data_setting['peggunaan_implan'];
			$data['judul_form']='TRANSAKSI PEMBEDAHAN IMPLANT';
			$data['status_paket']='0';
			$data['paket_id']='';
			$data['nama_tabel_diskon']='merm_pengaturan_diskon_ranap_implan_bedah';
			$data['nama_tabel_trx']='tkamaroperasi_implan';
		}
		if ($menu_kiri=='input_bedah_sewa_alat'){
			$data['judul_form']='TRANSAKSI PEMBEDAHAN SEWA ALAT';
		}
		$data['tanggal_transaksi']=date('Y-m-d H:i:s');
		
		$data['list_unitpelayanan'] = $this->Tkamar_bedah_model->getListUnitPelayanan();
		$data['idunit_default'] = $this->Tkamar_bedah_model->getDefaultUnitPelayananUser();
		$data['list_tipe'] = $this->Tkamar_bedah_model->getListTipe();
		
		$data_bedah['kelompok_tindakan_list']= $this->Tkamar_bedah_model->kelompok_tindakan_list($pendaftaran_bedah_id);
		$data_bedah['kelompok_diagnosa_list']= $this->Tkamar_bedah_model->kelompok_diagnosa_list($pendaftaran_bedah_id);
		$data_bedah['tind_konsulen'] = $this->Tkamar_bedah_model->tind_konsulen($pendaftaran_bedah_id);
		$data_bedah['tind_instrumen'] = $this->Tkamar_bedah_model->tind_instrumen($pendaftaran_bedah_id);
		$data_bedah['tind_sirkuler'] = $this->Tkamar_bedah_model->tind_sirkuler($pendaftaran_bedah_id);
		$data = array_merge($data,$data_bedah);
			// $data['pendaftaran_id']=$pendaftaran_id;
		$data['dokterumum'] = $this->model->getdokterumum();
		$data['list_instrumen'] = $this->Tkamar_bedah_model->list_instrumen();
		$data['kelas_list'] = $this->model->kelas_list();
		$data['mjenis_operasi'] = $this->model->mjenis_operasi();
		$data['mruangan'] = $this->model->mruangan();
		$data['mkelas'] = $this->model->mkelas();
		$data['mkelompok_operasi'] = $this->model->mkelompok_operasi();
		$data['mjenis_anaesthesi'] = $this->model->mjenis_anaesthesi();
		$data['list_dokter_anestesi'] = $this->model->list_dokter_anestesi();
		$data['list_alat'] = $this->model->list_alat();
		$data['trx_id']=$trx_id;
		$data['pendaftaran_bedah_id']=$pendaftaran_bedah_id;
		$data['versi_edit']=$versi_edit;
		// print_r($data['list_dokter_all']);exit;
		// print_r($data);exit;
		if ($data_setting['auto_input_sr']=='1'){
			$this->Tkamar_bedah_model->auto_insert_sewa_ruangan($pendaftaran_bedah_id);
		}
		if ($data_setting['auto_input_fc']=='1'){
			$this->Tkamar_bedah_model->auto_insert_sewa_ruangan_fc($pendaftaran_bedah_id,$data);
		}
		$data['mjenis_operasi_tindakan'] = $this->model->mjenis_operasi_tindakan($data['idkelompokpasien'], $data['idrekanan']);
		$data = array_merge($data,$data_setting, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	
	
	public function load_tarif_ruangan()
    {
        $jenis_operasi_id     = $this->input->post('jenis_operasi_id');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
        $arr = $this->Tkamar_bedah_model->load_tarif_ruangan($jenis_operasi_id, $kelas_tarif_id);
        $this->output->set_output(json_encode($arr));
    }
	
	
	public function load_tarif_ruangan_fc()
    {
        $id_tarif     = $this->input->post('id_tarif');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
		$q="SELECT * FROM `mtarif_rawatinap_detail` H
			WHERE H.idtarif='$id_tarif' AND H.kelas='$kelas_tarif_id'";
        $arr = $this->db->query($q)->row_array();
        $this->output->set_output(json_encode($arr));
    }
	function list_index_sewa_ruangan()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		$auto_input_sr=$this->input->post('auto_input_sr');
		$q="
			SELECT M.nama as nama_kelas,R.ref as nama_jenis, H.* 
			
			FROM tkamaroperasi_ruangan H 
			LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			
			LEFT JOIN merm_referensi R ON R.nilai=H.jenis_operasi_id AND R.ref_head_id='124'
			WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			ORDER BY H.id ASC
			";
		// $q="
			// SELECT M.nama as nama_kelas,R.nama as nama_jenis, H.* FROM tkamaroperasi_ruangan H 
			// LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			// LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif
			// LEFT JOIN mjenis_operasi R ON R.id=TR.idjenis
			// WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			// ORDER BY H.id ASC
			// ";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		$totalkeseluruhan=0;
      foreach ($list as $r) {
          $result = array();
		  $no++;
		  $aksi='';
		  $tabel .='<tr>';
          $tabel .='<td class="text-center">'.($no).'</td>';
          $tabel .='<td class="text-center">'.$r->nama_jenis.'</td>';
          $tabel .='<td class="text-center">'.$r->nama_kelas.'</td>';
          $tabel .='<td class="text-left">'.$r->nama_tarif.'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasasarana).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasapelayanan).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->bhp).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->biayaperawatan).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->total).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan).'</td>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2315'))){ 
		  $aksi .= '<button onclick="edit('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		  if (UserAccesForm($user_acces_form,array('2316'))){ 
		  $aksi .= '<button onclick="hapus('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
		  if ($auto_input_sr=='0'){
          $tabel .='<td class="text-center">'.($aksi).'</td>';
			  
		  }
		  $tabel .= '</tr>';
		  $totalkeseluruhan=$totalkeseluruhan + $r->totalkeseluruhan;

      }
	  $q="UPDATE tkamaroperasi_pendaftaran set total_sewa_ruangan='$totalkeseluruhan' WHERE id='$pendaftaran_bedah_id'";
	  $this->db->query($q);
	  
	  $data['tabel']=$tabel;
	  $data['totalkeseluruhan']=$totalkeseluruhan;
      $this->output->set_output(json_encode($data));
  }
	function simpan_sewa_ruangan(){
	  $trx_id_tabel=$this->input->post('trx_id_tabel');
		$data=array(
			'idpendaftaranoperasi' => $this->input->post('idpendaftaranoperasi'),
			'id_tarif' => $this->input->post('id_tarif'),
			'kelas_tarif' => $this->input->post('kelas_tarif'),
			'nama_tarif' => $this->input->post('nama_tarif'),
			'statusverif' => 0,
			'status' => 1,
			'jasasarana' => $this->input->post('jasasarana'),
			'jasasarana_disc' => $this->input->post('jasasarana_disc'),
			'jasapelayanan' => $this->input->post('jasapelayanan'),
			'jasapelayanan_disc' => $this->input->post('jasapelayanan_disc'),
			'bhp' => $this->input->post('bhp'),
			'bhp_disc' => $this->input->post('bhp_disc'),
			'biayaperawatan' => $this->input->post('biayaperawatan'),
			'biayaperawatan_disc' => $this->input->post('biayaperawatan_disc'),
			'total' => $this->input->post('total'),
			'diskon' => $this->input->post('diskon'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
		);
		$user_id=$this->session->userdata('user_id');
		if ($trx_id_tabel==''){
			$data['tanggal_transaksi'] = date('Y-m-d H:i:s');
			$data['iduser_transaksi']=$user_id;
			$result=$this->db->insert('tkamaroperasi_ruangan',$data);
			
		}else{
			$data['iduser_ubah']=$user_id;
			$data['tanggal_ubah'] = date('Y-m-d H:i:s');
			$this->db->where('id',$trx_id_tabel);
			$result=$this->db->update('tkamaroperasi_ruangan',$data);
		}
		$this->output->set_output(json_encode($result));
	}
	
	
	
	
	function hapus_sewa_ruangan($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('tkamaroperasi_ruangan');
		$this->output->set_output(json_encode($result));
	}
	
	function get_edit_sewa_ruangan($id){
		$q="SELECT TR.idjenis as jenis_tarif_id ,H.* 
FROM tkamaroperasi_ruangan H
			LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif AND TR.idtipe='1'
			
		WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
	  
		$this->output->set_output(json_encode($hasil));
	}
	
	
	//FULLCARE
	function simpan_sewa_ruangan_fc(){
	  $trx_id_tabel=$this->input->post('trx_id_tabel');
		$data=array(
			'idpendaftaran' => $this->input->post('idpendaftaranoperasi'),
			'idtarif' => $this->input->post('id_tarif'),
			'kelas_tarif' => $this->input->post('kelas_tarif'),
			'namatarif' => $this->input->post('nama_tarif'),
			'statusverifikasi' => 0,
			'status' => 1,
			'jasasarana' => $this->input->post('jasasarana'),
			'jasasarana_disc' => $this->input->post('jasasarana_disc'),
			'jasapelayanan' => $this->input->post('jasapelayanan'),
			'jasapelayanan_disc' => $this->input->post('jasapelayanan_disc'),
			'bhp' => $this->input->post('bhp'),
			'bhp_disc' => $this->input->post('bhp_disc'),
			'biayaperawatan' => $this->input->post('biayaperawatan'),
			'biayaperawatan_disc' => $this->input->post('biayaperawatan_disc'),
			'total' => $this->input->post('total'),
			'diskon' => $this->input->post('diskon'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
		);
		$user_id=$this->session->userdata('user_id');
		if ($trx_id_tabel==''){
			// $data['tanggal_transaksi'] = date('Y-m-d H:i:s');
			// $data['iduser_transaksi']=$user_id;
			$result=$this->db->insert('tkamaroperasi_fullcare',$data);
			
		}else{
			// $data['iduser_ubah']=$user_id;
			// $data['tanggal_ubah'] = date('Y-m-d H:i:s');
			$this->db->where('id',$trx_id_tabel);
			$result=$this->db->update('tkamaroperasi_fullcare',$data);
		}
		$this->output->set_output(json_encode($result));
	}
	function list_index_sewa_ruangan_fc()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		$auto_input_fc=$this->input->post('auto_input_fc');
		$q="
			SELECT M.nama as nama_kelas,R.ref as nama_jenis, H.* 
			
			FROM tkamaroperasi_fullcare H 
			LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			LEFT JOIN merm_referensi R ON R.nilai=H.jenis_operasi_id AND R.ref_head_id='124'
			WHERE H.idpendaftaran='$pendaftaran_bedah_id' AND H.status='1'
			ORDER BY H.id ASC
			";
		// $q="
			// SELECT M.nama as nama_kelas,R.nama as nama_jenis, H.* FROM tkamaroperasi_ruangan H 
			// LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			// LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif
			// LEFT JOIN mjenis_operasi R ON R.id=TR.idjenis
			// WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			// ORDER BY H.id ASC
			// ";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		$totalkeseluruhan=0;
      foreach ($list as $r) {
          $result = array();
		  $no++;
		  $aksi='';
		  $tabel .='<tr>';
          $tabel .='<td class="text-center">'.($no).'</td>';
          $tabel .='<td class="text-center">'.$r->nama_jenis.'</td>';
          $tabel .='<td class="text-center">'.$r->nama_kelas.'</td>';
          $tabel .='<td class="text-left">'.$r->namatarif.'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasasarana).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasapelayanan).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->bhp).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->biayaperawatan).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->total).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan).'</td>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2312'))){ 
		  $aksi .= '<button onclick="edit('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		  if (UserAccesForm($user_acces_form,array('2313'))){ 
		  $aksi .= '<button onclick="hapus('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
		  if ($auto_input_fc=='0'){
          $tabel .='<td class="text-center">'.($aksi).'</td>';
			  
		  }
		  $tabel .= '</tr>';
		  $totalkeseluruhan=$totalkeseluruhan + $r->totalkeseluruhan;

      }
	  $q="UPDATE tkamaroperasi_pendaftaran set total_fullcare='$totalkeseluruhan' WHERE id='$pendaftaran_bedah_id'";
	  $this->db->query($q);
	  
	  $data['tabel']=$tabel;
	  $data['totalkeseluruhan']=$totalkeseluruhan;
      $this->output->set_output(json_encode($data));
  }
  function hapus_sewa_ruangan_fc($id){
		$this->db->where('id',$id);
		$result=$this->db->update('tkamaroperasi_fullcare',array('status'=>0));
		$this->output->set_output(json_encode($result));
	}
	function get_edit_sewa_ruangan_fc($id){
		$q="SELECT H.* 
			FROM tkamaroperasi_fullcare H
			
		WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
	  
		$this->output->set_output(json_encode($hasil));
	}
	
	//DOKTER OPERATOR
	public function load_tarif_do()
    {
        $jenis_operasi_id     = $this->input->post('jenis_operasi_id');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
        $arr = $this->Tkamar_bedah_model->load_tarif_do($jenis_operasi_id, $kelas_tarif_id);
        $this->output->set_output(json_encode($arr));
    }
	public function load_tarif_do_single()
    {
        $id_tarif     = $this->input->post('id_tarif');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
        $q="SELECT D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total
		from mtarif_operasi_detail D
			WHERE D.idtarif='$id_tarif' AND D.kelas='$kelas_tarif_id'";
		$query=$this->db->query($q);
		$result= $query->row();
		if ($result){
			$arr= $result;

		}else{
			$arr= 'error';
		}
		
        $this->output->set_output(json_encode($arr));
    }
	
	function simpan_do(){
	  $trx_id_tabel=$this->input->post('trx_id_tabel');
		$data=array(
			'idpendaftaranoperasi' => $this->input->post('idpendaftaranoperasi'),
			'iddokteroperator' => $this->input->post('iddokteroperator'),
			'namadokteroperator' => $this->input->post('namadokteroperator'),
			'id_tarif' => $this->input->post('id_tarif'),
			'kelas_tarif' => $this->input->post('kelas_tarif'),
			'nama_tarif' => $this->input->post('nama_tarif'),
			'statusverif' => 0,
			'status' => 1,
			'jasasarana' => $this->input->post('jasasarana'),
			'jasasarana_disc' => $this->input->post('jasasarana_disc'),
			'jasapelayanan' => $this->input->post('jasapelayanan'),
			'jasapelayanan_disc' => $this->input->post('jasapelayanan_disc'),
			'bhp' => $this->input->post('bhp'),
			'bhp_disc' => $this->input->post('bhp_disc'),
			'biayaperawatan' => $this->input->post('biayaperawatan'),
			'biayaperawatan_disc' => $this->input->post('biayaperawatan_disc'),
			'total' => $this->input->post('total'),
			'diskon' => $this->input->post('diskon'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'opr_ke' => $this->input->post('opr_ke'),
		);
		$user_id=$this->session->userdata('user_id');
		if ($trx_id_tabel==''){
			$data['tanggal_transaksi'] = date('Y-m-d H:i:s');
			$data['iduser_transaksi']=$user_id;
			$result=$this->db->insert('tkamaroperasi_jasado',$data);
			
		}else{
			$data['iduser_ubah']=$user_id;
			$data['tanggal_ubah'] = date('Y-m-d H:i:s');
			$this->db->where('id',$trx_id_tabel);
			$result=$this->db->update('tkamaroperasi_jasado',$data);
		}
		$this->output->set_output(json_encode($result));
	}
	
	function list_index_do()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		$auto_input_sr=$this->input->post('auto_input_sr');
		$q="
			SELECT M.nama as nama_kelas,R.ref as nama_jenis, H.* 
			
			FROM tkamaroperasi_jasado H 
			LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			
			LEFT JOIN merm_referensi R ON R.nilai=H.jenis_operasi_id AND R.ref_head_id='124'
			WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			ORDER BY H.opr_ke ASC
			";
		// $q="
			// SELECT M.nama as nama_kelas,R.nama as nama_jenis, H.* FROM tkamaroperasi_ruangan H 
			// LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			// LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif
			// LEFT JOIN mjenis_operasi R ON R.id=TR.idjenis
			// WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			// ORDER BY H.id ASC
			// ";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		$totalkeseluruhan=0;
      foreach ($list as $r) {
          $result = array();
		  $no++;
		  $aksi='';
		  $diskon='';
		  if ($r->jasapelayanan_disc>0){
			  $diskon='<br><span class="text-danger">(-'.number_format($r->jasapelayanan_disc).')</span>';
		  }
		  $tabel .='<tr>';
          $tabel .='<td class="text-center">'.($no).'</td>';
          $tabel .='<td class="text-left">'.$r->namadokteroperator.'</td>';
          $tabel .='<td class="text-center">'.$r->opr_ke.'</td>';
          $tabel .='<td class="text-center">'.$r->nama_jenis.'</td>';
          $tabel .='<td class="text-center">'.$r->nama_kelas.'</td>';
          $tabel .='<td class="text-left">'.$r->nama_tarif.'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasasarana).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasapelayanan).$diskon.'</td>';
          $tabel .='<td class="text-right">'.number_format($r->bhp).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->biayaperawatan).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->total).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan).'</td>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2318'))){ 
		  $aksi .= '<button onclick="edit('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		  if (UserAccesForm($user_acces_form,array('2319'))){ 
		  $aksi .= '<button onclick="hapus('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
		  if ($auto_input_sr=='0'){
          $tabel .='<td class="text-center">'.($aksi).'</td>';
			  
		  }
		  $tabel .= '</tr>';
		  $totalkeseluruhan=$totalkeseluruhan + $r->totalkeseluruhan;

      }
	  $q="UPDATE tkamaroperasi_pendaftaran set total_do='$totalkeseluruhan' WHERE id='$pendaftaran_bedah_id'";
	  $this->db->query($q);
	  // $q="SELECT * FROM tkamaroperasi_pendaftaran WHERE id='$pendaftaran_bedah_id'";
	  // $data_update=$this->db->query($q)->row();
	  // if ($data_update->persen_da){
		  // $this->Tkamar_bedah_model->update_persen_da($pendaftaran_bedah_id,$data_update->total_do,$data_update->persen_da);
	  // }
	  $data['tabel']=$tabel;
	  $data['totalkeseluruhan']=$totalkeseluruhan;
      $this->output->set_output(json_encode($data));
  }
  
  function hapus_do($id,$pendaftaran_bedah_id){
		$this->db->where('id',$id);
		$result=$this->db->update('tkamaroperasi_jasado',array('status'=>0));
		
		$q="SELECT * FROM tkamaroperasi_pendaftaran WHERE id='$pendaftaran_bedah_id'";
		  $data_update=$this->db->query($q)->row();
		  if ($data_update->persen_da){
			  $this->Tkamar_bedah_model->update_persen_da($pendaftaran_bedah_id,$data_update->total_do,$data_update->persen_da);
		  }
		  
		$this->output->set_output(json_encode($result));
	}
	function get_edit_do($id){
		$q="SELECT H.* 
			FROM tkamaroperasi_jasado H
			LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif AND TR.idtipe='1'
			
		WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
	  
		$this->output->set_output(json_encode($hasil));
	}
	
	
	//DA
	function get_edit_da($id){
		$q="SELECT H.* 
			FROM tkamaroperasi_jasada H
		WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
	  
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_da(){
	  $trx_id_tabel=$this->input->post('trx_id_tabel');
	  $total_before_diskon=$this->input->post('total_acuan')*($this->input->post('persen')/100);
		$data=array(
			'idpendaftaranoperasi' => $this->input->post('idpendaftaranoperasi'),
			'iddokteranastesi' => $this->input->post('iddokteroperator'),
			'namadokteranastesi' => $this->input->post('namadokteroperator'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'total_acuan' => $this->input->post('total_acuan'),
			'persen' => $this->input->post('persen'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'diskon' => $this->input->post('diskon'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'total_before_diskon' => $total_before_diskon,
			'status' => 1,
			'statusverif' => 0,
		);
		$user_id=$this->session->userdata('user_id');
		if ($trx_id_tabel==''){
			$data['tanggal_transaksi'] = date('Y-m-d H:i:s');
			$data['iduser_transaksi']=$user_id;
			$result=$this->db->insert('tkamaroperasi_jasada',$data);
			
		}else{
			$data['iduser_ubah']=$user_id;
			$data['tanggal_ubah'] = date('Y-m-d H:i:s');
			$this->db->where('id',$trx_id_tabel);
			$result=$this->db->update('tkamaroperasi_jasada',$data);
		}
		
		$this->output->set_output(json_encode($result));
	}
	
	function update_persen_da(){
		$idpendaftaranoperasi=$this->input->post('idpendaftaranoperasi');
		$persen_da=$this->input->post('persen_da');
		$total_do=$this->input->post('total_o');
		$result=$this->Tkamar_bedah_model->update_persen_da($idpendaftaranoperasi,$total_do,$persen_da);
		$this->output->set_output(json_encode($result));
	}
	
	function list_index_da()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		$persen_da=$this->input->post('persen_da');
		$total_acuan_da=$this->input->post('total_da');
		$q="
			SELECT R.ref as nama_jenis, H.* 
			
			FROM tkamaroperasi_jasada H 
			LEFT JOIN merm_referensi R ON R.nilai=H.jenis_operasi_id AND R.ref_head_id='124'
			WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			ORDER BY H.id ASC
			";
		// $q="
			// SELECT M.nama as nama_kelas,R.nama as nama_jenis, H.* FROM tkamaroperasi_ruangan H 
			// LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			// LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif
			// LEFT JOIN mjenis_operasi R ON R.id=TR.idjenis
			// WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			// ORDER BY H.id ASC
			// ";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		$totalkeseluruhan=0;
		$totalkeseluruhan_persen=0;
      foreach ($list as $r) {
          $result = array();
		  $no++;
		  $aksi='';
		  $diskon='';
		  $diskon_persen='0';
		  $jasa_medis=$r->total_before_diskon;
		  
		  
		  if ($r->diskon>0){
			  $diskon_persen=$r->diskon/$jasa_medis*100;
			  $diskon=number_format($diskon_persen).' % <br><span class="text-danger">Rp.'.number_format($r->diskon).'</span>';
		  }else{
			  $diskon='<span class="">Rp.'.number_format($r->diskon).'</span>';
			  
		  }
		  $tabel .='<tr>';
          $tabel .='<td class="text-left">'.$r->namadokteranastesi.'</td>';
          $tabel .='<td class="text-center">'.$r->persen.' %</td>';
          $tabel .='<td class="text-right">'.number_format($jasa_medis).'</td>';
          $tabel .='<td class="text-right">'.($diskon).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan).'</td>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2321'))){ 
		  $aksi .= '<button onclick="edit('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';
		  }
		  if (UserAccesForm($user_acces_form,array('2322'))){ 		  
		  $aksi .= '<button onclick="hapus('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $tabel .='<td class="text-center">'.($aksi).'</td>';
		  $tabel .= '</tr>';
		  $totalkeseluruhan=$totalkeseluruhan + $r->totalkeseluruhan;
		  $totalkeseluruhan_persen=$totalkeseluruhan_persen + $r->persen;

      }
	  $q="UPDATE tkamaroperasi_pendaftaran set persen_da='$persen_da',total_acuan_da='$total_acuan_da',total_da='$totalkeseluruhan' WHERE id='$pendaftaran_bedah_id'";
	  $this->db->query($q);
	  
	  $data['tabel']=$tabel;
	  $data['totalkeseluruhan']=$totalkeseluruhan;
	  $data['lbl_total_persen_da']=$totalkeseluruhan_persen;
      $this->output->set_output(json_encode($data));
  }
  function hapus_da($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('tkamaroperasi_jasada');
		$this->output->set_output(json_encode($result));
	}
	
	
	//ASISTEN ANESTESI
	function hapus_daa($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('tkamaroperasi_jasadaa');
		$this->output->set_output(json_encode($result));
	}
	
	function list_index_daa()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		$persen_daa=$this->input->post('persen_daa');
		$total_acuan_da=$this->input->post('total_da');
		$q="
			SELECT R.ref as nama_jenis, H.* 
			
			FROM tkamaroperasi_jasadaa H 
			LEFT JOIN merm_referensi R ON R.nilai=H.jenis_operasi_id AND R.ref_head_id='124'
			WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			ORDER BY H.id ASC
			";
		// $q="
			// SELECT M.nama as nama_kelas,R.nama as nama_jenis, H.* FROM tkamaroperasi_ruangan H 
			// LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			// LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif
			// LEFT JOIN mjenis_operasi R ON R.id=TR.idjenis
			// WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			// ORDER BY H.id ASC
			// ";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		$totalkeseluruhan=0;
		$totalkeseluruhan_persen=0;
      foreach ($list as $r) {
          $result = array();
		  $no++;
		  $aksi='';
		  $diskon='';
		  $diskon_persen='0';
		  $jasa_medis=$r->total_before_diskon;
		  
		  
		  if ($r->diskon>0){
			  $diskon_persen=$r->diskon/$jasa_medis*100;
			  $diskon=number_format($diskon_persen).' % <br><span class="text-danger">Rp.'.number_format($r->diskon).'</span>';
		  }else{
			  $diskon='<span class="">Rp.'.number_format($r->diskon).'</span>';
			  
		  }
		  $tabel .='<tr>';
          $tabel .='<td class="text-left">'.$r->namaasisten_da.'</td>';
          $tabel .='<td class="text-center">'.$r->persen.' %</td>';
          $tabel .='<td class="text-right">'.number_format($jasa_medis).'</td>';
          $tabel .='<td class="text-right">'.($diskon).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan).'</td>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2324'))){ 
		  $aksi .= '<button onclick="edit('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		  if (UserAccesForm($user_acces_form,array('2325'))){ 
		  $aksi .= '<button onclick="hapus('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $tabel .='<td class="text-center">'.($aksi).'</td>';
		  $tabel .= '</tr>';
		  $totalkeseluruhan=$totalkeseluruhan + $r->totalkeseluruhan;
		  $totalkeseluruhan_persen=$totalkeseluruhan_persen + $r->persen;

      }
	  $q="UPDATE tkamaroperasi_pendaftaran set persen_daa='$persen_daa',total_acuan_da='$total_acuan_da',total_daa='$totalkeseluruhan' WHERE id='$pendaftaran_bedah_id'";
	  $this->db->query($q);
	  
	  $data['tabel']=$tabel;
	  $data['totalkeseluruhan']=$totalkeseluruhan;
	  $data['lbl_total_persen_daa']=$totalkeseluruhan_persen;
      $this->output->set_output(json_encode($data));
  }
	function update_persen_daa(){
	  $idpendaftaranoperasi=$this->input->post('idpendaftaranoperasi');
	  $persen_daa=$this->input->post('persen_daa');
	  $total_daa=$this->input->post('total_aa');
	  $result=$this->Tkamar_bedah_model->update_persen_daa($idpendaftaranoperasi,$total_daa,$persen_daa);
	  $this->output->set_output(json_encode($result));
	}
	function simpan_daa(){
	  $trx_id_tabel=$this->input->post('trx_id_tabel');
	  $total_before_diskon=$this->input->post('total_acuan')*($this->input->post('persen')/100);
		$data=array(
			'idpendaftaranoperasi' => $this->input->post('idpendaftaranoperasi'),
			'id_asisten_da' => $this->input->post('iddokteroperator'),
			'namaasisten_da' => $this->input->post('namadokteroperator'),
			'jenis_asisten_da' => $this->input->post('asisten_anestesi_tipe'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'total_acuan' => $this->input->post('total_acuan'),
			'persen' => $this->input->post('persen'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'diskon' => $this->input->post('diskon'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'total_before_diskon' => $total_before_diskon,
			'status' => 1,
			'statusverif' => 0,
		);
		$user_id=$this->session->userdata('user_id');
		if ($trx_id_tabel==''){
			$data['tanggal_transaksi'] = date('Y-m-d H:i:s');
			$data['iduser_transaksi']=$user_id;
			$result=$this->db->insert('tkamaroperasi_jasadaa',$data);
			
		}else{
			$data['iduser_ubah']=$user_id;
			$data['tanggal_ubah'] = date('Y-m-d H:i:s');
			$this->db->where('id',$trx_id_tabel);
			$result=$this->db->update('tkamaroperasi_jasadaa',$data);
		}
		
		$this->output->set_output(json_encode($result));
	}
	function get_edit_daa($id){
		$q="SELECT H.* 
			FROM tkamaroperasi_jasadaa H
		WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
	  
		$this->output->set_output(json_encode($hasil));
	}
	
	//DAO
	
	function list_index_dao()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		$persen_da=$this->input->post('persen_dao');
		$total_acuan_da=$this->input->post('total_acuan_dao');
		$q="
			SELECT R.ref as nama_jenis, H.* 
			
			FROM tkamaroperasi_jasaao H 
			LEFT JOIN merm_referensi R ON R.nilai=H.jenis_operasi_id AND R.ref_head_id='124'
			WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			ORDER BY H.id ASC
			";
		// $q="
			// SELECT M.nama as nama_kelas,R.nama as nama_jenis, H.* FROM tkamaroperasi_ruangan H 
			// LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			// LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif
			// LEFT JOIN mjenis_operasi R ON R.id=TR.idjenis
			// WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			// ORDER BY H.id ASC
			// ";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		$totalkeseluruhan=0;
		$totalkeseluruhan_persen=0;
      foreach ($list as $r) {
          $result = array();
		  $no++;
		  $aksi='';
		  $diskon='';
		  $diskon_persen='0';
		  $jasa_medis=$r->total_before_diskon;
		  
		  
		  if ($r->diskon>0){
			  $diskon_persen=$r->diskon/$jasa_medis*100;
			  $diskon=number_format($diskon_persen).' % <br><span class="text-danger">Rp.'.number_format($r->diskon).'</span>';
		  }else{
			  $diskon='<span class="">Rp.'.number_format($r->diskon).'</span>';
			  
		  }
		  $tabel .='<tr>';
          $tabel .='<td class="text-left">'.$r->namaasisten_dao.'</td>';
          $tabel .='<td class="text-center">'.$r->persen.' %</td>';
          $tabel .='<td class="text-right">'.number_format($jasa_medis).'</td>';
          $tabel .='<td class="text-right">'.($diskon).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan).'</td>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2327'))){ 
		  $aksi .= '<button onclick="edit('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		  if (UserAccesForm($user_acces_form,array('2328'))){ 
		  $aksi .= '<button onclick="hapus('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $tabel .='<td class="text-center">'.($aksi).'</td>';
		  $tabel .= '</tr>';
		  $totalkeseluruhan=$totalkeseluruhan + $r->totalkeseluruhan;
		  $totalkeseluruhan_persen=$totalkeseluruhan_persen + $r->persen;

      }
	  $q="UPDATE tkamaroperasi_pendaftaran set persen_dao='$persen_da',total_acuan_dao='$total_acuan_da',total_dao='$totalkeseluruhan' WHERE id='$pendaftaran_bedah_id'";
	  $this->db->query($q);
	  
	  $data['tabel']=$tabel;
	  $data['totalkeseluruhan']=$totalkeseluruhan;
	  $data['lbl_total_persen_dao']=$totalkeseluruhan_persen;
      $this->output->set_output(json_encode($data));
  }
  function simpan_dao(){
	  $trx_id_tabel=$this->input->post('trx_id_tabel');
	  $total_before_diskon=$this->input->post('total_acuan')*($this->input->post('persen')/100);
		$data=array(
			'idpendaftaranoperasi' => $this->input->post('idpendaftaranoperasi'),
			'id_asisten_dao' => $this->input->post('iddokteroperator'),
			'namaasisten_dao' => $this->input->post('namadokteroperator'),
			'jenis_asisten_dao' => $this->input->post('jenis_asisten_dao'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'total_acuan' => $this->input->post('total_acuan'),
			'persen' => $this->input->post('persen'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'diskon' => $this->input->post('diskon'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'total_before_diskon' => $total_before_diskon,
			'status' => 1,
			'statusverif' => 0,
		);
		$user_id=$this->session->userdata('user_id');
		if ($trx_id_tabel==''){
			$data['tanggal_transaksi'] = date('Y-m-d H:i:s');
			$data['iduser_transaksi']=$user_id;
			$result=$this->db->insert('tkamaroperasi_jasaao',$data);
			
		}else{
			$data['iduser_ubah']=$user_id;
			$data['tanggal_ubah'] = date('Y-m-d H:i:s');
			$this->db->where('id',$trx_id_tabel);
			$result=$this->db->update('tkamaroperasi_jasaao',$data);
		}
		
		$this->output->set_output(json_encode($result));
	}
	function get_edit_dao($id){
		$q="SELECT H.* 
			FROM tkamaroperasi_jasaao H
		WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
	  
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_dao($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('tkamaroperasi_jasaao');
		$this->output->set_output(json_encode($result));
	}
	function update_persen_dao(){
		$idpendaftaranoperasi=$this->input->post('idpendaftaranoperasi');
		$persen_da=$this->input->post('persen_dao');
		$total_do=$this->input->post('total_o');
		$result=$this->Tkamar_bedah_model->update_persen_dao($idpendaftaranoperasi,$total_do,$persen_da);
		$this->output->set_output(json_encode($result));
	}
	//SEWA ALAT
	public function load_tarif_alat_list()
    {
        $id_tarif_header     = $this->input->post('id_tarif_header');
		$q="SELECT D.* FROM `mtarif_operasi_sewaalat` H
			INNER JOIN mtarif_operasi_sewaalat D ON D.headerpath=H.headerpath AND D.idkelompok='0' AND D.`status`='1'
			WHERE H.id='$id_tarif_header' ORDER BY H.path ASC ";
        $list_tarif     = $this->db->query($q)->result();
		$opsi='<option value="0" selected>Pilih</option>';
		foreach($list_tarif as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
		}
        $arr['opsi'] = $opsi;
        $this->output->set_output(json_encode($arr));
    }
	public function load_tarif_alat()
    {
        $jenis_operasi_id     = $this->input->post('jenis_operasi_id');
        $id_tarif     = $this->input->post('id_tarif');
        $kelas     = $this->input->post('kelas');
		$q="SELECT *FROM mtarif_operasi_sewaalat_detail H 
		WHERE H.idjenis='$jenis_operasi_id' AND H.idtarif='$id_tarif' AND H.kelas='$kelas'";
        $arr = $this->db->query($q)->row_array();
        $this->output->set_output(json_encode($arr));
    }
	function simpan_sewa_alat(){
	  $trx_id_tabel=$this->input->post('trx_id_tabel');
		$data=array(
			'idpendaftaranoperasi' => $this->input->post('idpendaftaranoperasi'),
			'idalat' => $this->input->post('id_tarif'),
			// 'kelas_tarif' => $this->input->post('kelas_tarif'),
			// 'nama_tarif' => $this->input->post('nama_tarif'),
			'statusverifikasi' => 0,
			// 'status' => 1,
			'jasasarana' => $this->input->post('jasasarana'),
			'jasasarana_disc' => $this->input->post('jasasarana_disc'),
			'jasapelayanan' => $this->input->post('jasapelayanan'),
			'jasapelayanan_disc' => $this->input->post('jasapelayanan_disc'),
			'bhp' => $this->input->post('bhp'),
			'bhp_disc' => $this->input->post('bhp_disc'),
			'biayaperawatan' => $this->input->post('biayaperawatan'),
			'biayaperawatan_disc' => $this->input->post('biayaperawatan_disc'),
			'total' => $this->input->post('total'),
			'diskon' => $this->input->post('diskon'),
			'kuantitas' => $this->input->post('kuantitas'),
			'totalkeseluruhan' => $this->input->post('totalkeseluruhan'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'kelas_tarif' => $this->input->post('kelas_tarif'),
		);
		$user_id=$this->session->userdata('user_id');
		if ($trx_id_tabel==''){
			$data['tanggal_transaksi'] = date('Y-m-d H:i:s');
			$data['iduser_transaksi']=$user_id;
			$result=$this->db->insert('tkamaroperasi_sewaalat',$data);
			
		}else{
			$data['iduser_transaksi']=$user_id;
			$data['tanggal_ubah'] = date('Y-m-d H:i:s');
			$this->db->where('id',$trx_id_tabel);
			$result=$this->db->update('tkamaroperasi_sewaalat',$data);
		}
		$this->output->set_output(json_encode($result));
	}
	function list_index_sewa_alat()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		$auto_input_sr=$this->input->post('auto_input_sr');
		$q="
			SELECT M.nama as nama_kelas,R.ref as nama_jenis,SA.nama as nama_alat, H.* 
			
			FROM tkamaroperasi_sewaalat H 
			LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			LEFT JOIN mtarif_operasi_sewaalat SA ON SA.id=H.idalat
			LEFT JOIN merm_referensi R ON R.nilai=H.jenis_operasi_id AND R.ref_head_id='124'
			WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id'
			ORDER BY H.id ASC
			";
		// $q="
			// SELECT M.nama as nama_kelas,R.nama as nama_jenis, H.* FROM tkamaroperasi_ruangan H 
			// LEFT JOIN mkelas M ON M.id=H.kelas_tarif
			// LEFT JOIN mtarif_operasi TR ON TR.id=H.id_tarif
			// LEFT JOIN mjenis_operasi R ON R.id=TR.idjenis
			// WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.status='1'
			// ORDER BY H.id ASC
			// ";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		$totalkeseluruhan=0;
      foreach ($list as $r) {
          $result = array();
		  $no++;
		  $aksi='';
		  $tabel .='<tr>';
          $tabel .='<td class="text-center">'.($no).'</td>';
          $tabel .='<td class="text-center">'.$r->nama_jenis.'</td>';
          $tabel .='<td class="text-left">'.$r->nama_alat.'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasasarana).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->jasapelayanan).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->bhp).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->biayaperawatan).'</td>';
          $tabel .='<td class="text-right">'.number_format($r->total).'</td>';
          $tabel .='<td class="text-center">'.$r->kuantitas.'</td>';
          $tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan).'</td>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  if ($auto_input_sr=='0'){
          $tabel .='<td class="text-center">'.($aksi).'</td>';
			  
		  }
		  $tabel .= '</tr>';
		  $totalkeseluruhan=$totalkeseluruhan + $r->totalkeseluruhan;

      }
	  $q="UPDATE tkamaroperasi_pendaftaran set total_sewa='$totalkeseluruhan' WHERE id='$pendaftaran_bedah_id'";
	  $this->db->query($q);
	  
	  $data['tabel']=$tabel;
	  $data['totalkeseluruhan']=$totalkeseluruhan;
      $this->output->set_output(json_encode($data));
  }
  function hapus_sewa_alat($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('tkamaroperasi_sewaalat');
		$this->output->set_output(json_encode($result));
	}
	function get_edit_sewa_alat($id){
		$q="SELECT H.* 
			FROM tkamaroperasi_sewaalat H
			
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
	  
		$this->output->set_output(json_encode($hasil));
	}
	//NARCOSE
	public function get_obat()
    {
        $cari 	= $this->input->post('search');
        $idtipe 	= $this->input->post('idtipe');
        $idunit 	= $this->input->post('idunit');
        $q="SELECT H.*,S.stok FROM (
				SELECT '1' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_alkes  WHERE status='1'
				UNION ALL
				SELECT '2' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_implan  WHERE status='1'
				UNION ALL
				SELECT '3' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_obat WHERE status='1'
				) H 
				INNER JOIN mgudang_stok S ON S.idbarang=H.idbarang AND S.idtipe=H.idtipe AND S.idunitpelayanan='$idunit'
				
				WHERE H.idtipe='$idtipe' AND H.nama LIKE '%".$cari."%'
				ORDER BY nama
				";
		$data_obat=$this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	
	public function get_obat_detail()
    {
		$id=$this->input->post('idbarang');
		$nama_tabel_diskon=$this->input->post('nama_tabel_diskon');
		$idtipe=$this->input->post('idtipe');
		// print_r($idtipe);exit;
		$kelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		$tujuan_pasien=$this->input->post('tujuan_pasien');
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$jenis_opr=$this->input->post('jenis_opr');
		// $idtipe=$this->input->post('idtipe');
        $data_obat = $this->Tpoliklinik_trx_model->get_obat_detail($id, $kelompokpasien, $idtipe,$idunit);
		// print_r($data_obat);exit;
		$harga_before_diskon=ceiling(($data_obat->margin * $data_obat->hargadasar / 100) + ($data_obat->hargadasar),100);
		// $harga_before_diskon->hargajual=$hargajual;
		$data_obat->harga_before_diskon=$harga_before_diskon;
		$idbarang=$id;
		$q_diskon="
			SELECT M.* FROM (
					SELECT compare_value_11(
						MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.tujuan_pasien='0' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='2' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='0' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='0' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='0' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND (H.tujuan_pasien='0' OR H.tujuan_pasien='$tujuan_pasien') 
							AND (H.idruangan='0' OR H.idruangan='$idruangan') AND (H.idkelas='0' OR H.idkelas='$idkelas') AND (H.jenis_opr='$jenis_opr' OR H.jenis_opr='0') 
							AND (H.idrekanan='0' OR H.idrekanan='$idrekanan') AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					) as id FROM ".$nama_tabel_diskon." H
				
				) T INNER JOIN ".$nama_tabel_diskon." M
				WHERE M.id=T.id

		";
		// print_r($q_diskon);exit;
		$data_diskon=$this->db->query($q_diskon)->row();
		$diskon_rp=0;
		$diskon_persen=0;
		if ($data_diskon){
			
			if ($data_diskon->operand!=0){
				if ($data_diskon->operand=1){
					$diskon_persen=$data_diskon->diskon;
					$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
				}else{
					$diskon_persen=0;
					$diskon_rp=ceiling($data_diskon->diskon,100);
				}
			}
		}
		$hargajual_before=$harga_before_diskon-$diskon_rp;
		$hargajual=ceiling($hargajual_before,100);
		$pembulatan=$hargajual-$hargajual_before;
		$data_obat->hargajual=$hargajual;
		$data_obat->diskon_rp=$diskon_rp;
		$data_obat->diskon_persen=$diskon_persen;
		$data_obat->pembulatan=$pembulatan;
        $this->output->set_output(json_encode($data_obat));
    }
	
	public function get_obat_detail_paket()
    {
		$id=$this->input->post('idbarang');
		$idtipe=$this->input->post('idtipe');
		// print_r($idtipe);exit;
		$kelompokpasien=5;
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		// print_r($idunit);exit;
        $data_obat = $this->Tpoliklinik_trx_model->get_obat_detail($id, $kelompokpasien, $idtipe,$idunit);
		$harga_before_diskon=ceiling(($data_obat->margin * $data_obat->hargadasar / 100) + ($data_obat->hargadasar),100);
		$idbarang=$id;
		
		$hargajual=ceiling($harga_before_diskon,100);
		$data_obat->hargajual=$hargajual;
        $this->output->set_output(json_encode($data_obat));
    }
	
	function simpan_narcose(){
		$user_id=$this->session->userdata('user_id');
		$nama_user_transaksi=$this->session->userdata('user_name');
		$tanggal_transaksi= YMDFormat($this->input->post('tgltransaksi')). ' ' .$this->input->post('waktutransaksi');
		$transaksi_id=$this->input->post('transaksi_id');
		$idpendaftaranoperasi=$this->input->post('idpendaftaranoperasi');
		$nama_tabel_trx=$this->input->post('nama_tabel_trx');
		$idtipe=$this->input->post('idtipe');
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			$this->db->delete($nama_tabel_trx);
		}
		
		$data=array(
			'idpendaftaranoperasi' => $idpendaftaranoperasi,
			'tanggal_transaksi'=> $tanggal_transaksi,
			'iduser_transaksi' => $user_id,
			'nama_user_transaksi' => $nama_user_transaksi,
			'idobat' => $this->input->post('idobat'),
			'idunit' => $this->input->post('idunit'),
			'idtipe' => $this->input->post('idtipe'),
			'hargadasar' => RemoveComma($this->input->post('hargadasar')),
			'margin' => RemoveComma($this->input->post('margin')),
			'hargajual' => RemoveComma($this->input->post('hargajual')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'statusverifikasi' => 0,
			'harga_before_diskon' => RemoveComma($this->input->post('harga_before_diskon')),
			'diskon_persen' => RemoveComma($this->input->post('diskon_persen')),
			'pembulatan' => RemoveComma($this->input->post('pembulatan')),


		);
		$result=$this->db->insert($nama_tabel_trx,$data);
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_narcose_list(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$statuskasir=$this->input->post('statuskasir');
		$nama_tabel_trx=$this->input->post('nama_tabel_trx');
		
		$q="SELECT MU.nama as nama_unit,B.nama,B.idsatuan,B.namatipe,MS.nama as nama_satuan 
			,H.*,U.`name` as nama_user
			FROM ".$nama_tabel_trx." H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			INNER JOIN munitpelayanan MU ON MU.id=H.idunit
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			INNER JOIN musers U ON U.id=H.iduser_transaksi 
			WHERE H.idpendaftaranoperasi='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='';
			$btn_hapus='';
			if ($nama_tabel_trx=='tkamaroperasi_narcose' && UserAccesForm($user_acces_form,array('2333'))){
				$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			}
			if ($nama_tabel_trx=='tkamaroperasi_obat' && UserAccesForm($user_acces_form,array('2336'))){
				$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			}
			if ($nama_tabel_trx=='tkamaroperasi_alkes' && UserAccesForm($user_acces_form,array('2339'))){
				$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			}
			if ($nama_tabel_trx=='tkamaroperasi_implan' && UserAccesForm($user_acces_form,array('2342'))){
				$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			}
			if ($nama_tabel_trx=='tkamaroperasi_narcose' && UserAccesForm($user_acces_form,array('2334'))){
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			}
			if ($nama_tabel_trx=='tkamaroperasi_obat' && UserAccesForm($user_acces_form,array('2337'))){
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			}
			if ($nama_tabel_trx=='tkamaroperasi_alkes' && UserAccesForm($user_acces_form,array('2340'))){
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			}
			if ($nama_tabel_trx=='tkamaroperasi_implan' && UserAccesForm($user_acces_form,array('2343'))){
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_narcose('.$r->id.','.$r->idtipe.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			}
			// $result[] = $no;
			$aksi='';
			$aksi = '<div class="btn-group">';
			if ($r->statusverifikasi=='0'){
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			}
			$col_tipe='<div class="text-primary">['.strtoupper($r->namatipe).']</div><div class="">'.$r->nama.'</div>';
			$col_harga='<div class="h5 text-primary">Harga : Rp. '.number_format($r->hargajual,0).'</div>';
			$col_harga .='<div class="text-muted">Harga : Rp. '.number_format($r->harga_before_diskon,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : Rp. ('.number_format($r->diskon,0).')</div>';
			$aksi .= '</div>';
			if ($statuskasir!='0'){
				$aksi=text_danger('TELAH DIVERIFIKASI');
			}
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$r->nama_unit.'</td>';
			$tabel .='<td class="text-center">'.HumanDateLong($r->tanggal_transaksi).'</td>';
			$tabel .='<td class="text-left">'.$col_tipe.'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.HumanDateLong($r->tanggal_transaksi).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'"><input type="hidden" class="cls_idobat" value="'.$r->idobat.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
		  if ($nama_tabel_trx=='tkamaroperasi_narcose'){
			  $q="UPDATE tkamaroperasi_pendaftaran set total_narcose='$total_penjualan' WHERE id='$pendaftaran_id'";
		  }
		  if ($nama_tabel_trx=='tkamaroperasi_obat'){
			  $q="UPDATE tkamaroperasi_pendaftaran set total_obat='$total_penjualan' WHERE id='$pendaftaran_id'";
		  }
		  if ($nama_tabel_trx=='tkamaroperasi_alkes'){
			  $q="UPDATE tkamaroperasi_pendaftaran set total_alkes='$total_penjualan' WHERE id='$pendaftaran_id'";
		  }
		  if ($nama_tabel_trx=='tkamaroperasi_implan'){
			  $q="UPDATE tkamaroperasi_pendaftaran set total_implan='$total_penjualan' WHERE id='$pendaftaran_id'";
		  }
	     $this->db->query($q);
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	
	
	
	function hapus_narcose(){
		$id=$this->input->post('id');
		$idtipe=$this->input->post('idtipe');
		$nama_tabel_trx=$this->input->post('nama_tabel_trx');
		
		$this->db->where('id',$id);
		$hasil=$this->db->delete($nama_tabel_trx);
		
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_paket_detail_narcose(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('mpaket_bedah_detail');
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_narcose_paket(){
		$id=$this->input->post('paket_id');
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'deleted_date'=>date('Y-m-d H:i:s'),
			'deleted_by'=>$user_id,
			'staktif'=>0,
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('mpaket_bedah',$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	function get_edit_narcose(){
		$id=$this->input->post('id');
		$idtipe=$this->input->post('idtipe');
		$nama_tabel_trx=$this->input->post('nama_tabel_trx');
		
		$q="SELECT H.*,B.nama as nama_barang,MS.singkatan FROM `".$nama_tabel_trx."` H
		INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
		LEFT JOIN msatuan MS ON MS.id=B.idsatuan
		WHERE H.id='$id'";
		
		
		$hasil=$this->db->query($q)->row_array();
		$hasil['tgltransaksi']=HumanDateShort($hasil['tanggal_transaksi']);
		$hasil['waktutransaksi']=HumanTime($hasil['tanggal_transaksi']);
		$this->output->set_output(json_encode($hasil));
	}
	
	 public function get_obat_filter()
    {
        $nama_barang     = $this->input->post('nama_barang');
        $idunit     = $this->input->post('idunit');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $iduser=$this->session->userdata('user_id');

        $where='';
		if ($nama_barang!=''){
			 $where .=" AND B.nama LIKE '%".$nama_barang."%'";
		}
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
            $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='$idunit'
					)";
        }
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        $from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		INNER JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='$idunit' ".$where." ORDER BY B.idtipe,B.nama) as tbl";


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
        $this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        // print_r($list);exit();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = "<a href='#' class='selectObat' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->kode."</span></a>";
            $row[] = "<a href='#' class='selectObat' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
            $row[] = $r->satuan;

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
	public function get_obat_paket_filter()
    {
        $nama_barang     = $this->input->post('nama_barang');
        $idunit     = $this->input->post('idunit');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $iduser=$this->session->userdata('user_id');

        $where='';
		if ($nama_barang!=''){
			 $where .=" AND B.nama LIKE '%".$nama_barang."%'";
		}
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
            $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='$idunit'
					)";
        }
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        $from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		INNER JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='$idunit' ".$where." ORDER BY B.idtipe,B.nama) as tbl";


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
        $this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        // print_r($list);exit();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = "<a href='#' class='selectObatPaket' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->kode."</span></a>";
            $row[] = "<a href='#' class='selectObatPaket' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
            $row[] = $r->satuan;

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
	
	
  function lihat_data_narcose(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT MU.nama as nama_unit,B.nama,B.idsatuan,B.namatipe,MS.nama as nama_satuan 
			,H.*,U.`name` as nama_user
			FROM tpoliklinik_obat H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			INNER JOIN munitpelayanan MU ON MU.id=H.idunit
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			INNER JOIN musers U ON U.id=H.created_by 
			WHERE H.pendaftaran_id='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-warning  btn-xs" onclick="copy_narcose('.$r->id.')" type="button" title="Copy Data" type="button"><i class="fa fa-copy"></i></button>';
			// $result[] = $no;
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= '</div>';
			$col_tipe='<div class="text-primary">['.strtoupper($r->namatipe).']</div><div class="">'.$r->nama.'</div>';
			$col_harga='<div class="h5 text-primary">Harga : Rp. '.number_format($r->hargajual,0).'</div>';
			$col_harga .='<div class="text-muted">Harga : Rp. '.number_format($r->harga_before_diskon,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : Rp. ('.number_format($r->diskon,0).')</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$r->nama_unit.'</td>';
			$tabel .='<td class="text-center">'.HumanDateLong($r->tanggal_transaksi).'</td>';
			$tabel .='<td class="text-left">'.$col_tipe.'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.HumanDateLong($r->created_date).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	
	function copy_narcose(){
		// $user_id=$this->session->userdata('user_id');
		// $id=$this->input->post('id');
		// $pendaftaran_id=$this->input->post('pendaftaran_id');
		// $idpasien=$this->input->post('idpasien');
		// $idpoliklinik=$this->input->post('idpoliklinik');
		
		
		// $q="
			// SELECT 
			// idunit,idtipe,idobat,hargadasar,margin,hargajual,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,statusstok,NOW(),iduser_transaksi,tanggal_verifikasi,iduser_verifikasi,'$pendaftaran_id','$idpasien','$idpoliklinik',harga_before_diskon,diskon_persen,pembulatan,NOW(),'$user_id' 
			// FROM `tpoliklinik_obat` 
			// WHERE id='$id'
		// ";
		// $hasil=$this->db->query($q)->row();
		$user_id=$this->session->userdata('user_id');
		$idunit=$this->input->post('idunit');
		$kelompok_pasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$idbarang=$this->input->post('id');
		$pertemuan_id=$this->input->post('pertemuan_id');
		// print_r($idbarang);exit;
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		$idpoli=$this->input->post('idpoliklinik');
		$idtipe_poli=$this->input->post('idtipe_poli');
		if ($kelompok_pasien!='1'){
			$idrekanan=0;
		}
		  $q="
			SELECT S.stok as stok_asli,MS.nama as nama_satuan 
					,B.*,H.kuantitas,H.idunit
					,(CASE
					WHEN $kelompok_pasien = 5 THEN
						marginumum
					WHEN $kelompok_pasien = 0 THEN
						marginumum
					WHEN $kelompok_pasien = 1 THEN
						marginasuransi
					WHEN $kelompok_pasien = 2 THEN
						marginjasaraharja
					WHEN $kelompok_pasien = 3 THEN
						marginbpjskesehatan
					WHEN $kelompok_pasien = 4 THEN
						marginbpjstenagakerja
				END) AS margin
					FROM `tpoliklinik_obat` H
					LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan=H.idunit
					INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
					LEFT JOIN msatuan MS ON MS.id=B.idsatuan
					WHERE H.id IN (".$idbarang.")
		  ";
		  // print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$jml_insert=0;
		foreach($list_barang as $r){
			$idunit=$r->idunit;
			if ($r->kuantitas<=$r->stok_asli){
				$jml_insert=$jml_insert+1;
				$kelompokpasien=$kelompok_pasien;
				$idbarang=$r->id;
				$idtipe=$r->idtipe;
				$kuantitas=$r->kuantitas;
				$hargadasar=$r->hargadasar;
				$harga_before_diskon=(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100));
				$q_diskon="
					SELECT M.* FROM (
					SELECT compare_value_10(
					MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					) as id FROM merm_pengaturan_diskon_rajal_narcose H

					) T INNER JOIN merm_pengaturan_diskon_rajal_narcose M
					WHERE M.id=T.id

					";
					$data_diskon=$this->db->query($q_diskon)->row();
					$diskon_rp=0;
					$diskon_persen=0;
					if ($data_diskon){
						if ($data_diskon->operand!=0){
							if ($data_diskon->operand=1){
								$diskon_persen=$data_diskon->diskon;
								$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
							}else{
								$diskon_persen=0;
								$diskon_rp=ceiling($data_diskon->diskon,100);
							}
						}
					}else{
						
					}
					// print_r($harga_before_diskon);exit;
				$hargajual_before=$harga_before_diskon-$diskon_rp;
				$hargajual=ceiling($hargajual_before,100);	
				$data=array(
					'tanggal_transaksi'=> date('Y-m-d H:i:s'),
					'created_date' => date('Y-m-d H:i:s'),
					'created_by' => $user_id,
					'idunit' => $idunit,
					'idtipe' => $r->idtipe,
					'idobat' => $r->id,
					'hargadasar' => RemoveComma($hargadasar),
					'margin' => ($r->margin),
					'hargajual' => $hargajual,
					'kuantitas' => ($kuantitas),
					'diskon' => $diskon_rp,
					'totalkeseluruhan' => $hargajual * $kuantitas,
					'status' => 1,
					'statusstok' => $r->stok_asli,
					'iduser_transaksi' => $user_id,
					'pendaftaran_id' => $this->input->post('pendaftaran_id'),
					'idpasien' => $this->input->post('idpasien'),
					'idpoliklinik' => $this->input->post('idpoliklinik'),
					'harga_before_diskon' => ($harga_before_diskon),
					'diskon_persen' => $diskon_persen,
					'pembulatan' => 0,
				);
				
				$result=$this->db->insert('tpoliklinik_obat',$data);
			}
		}
		$this->output->set_output(json_encode($jml_insert));
		// $this->output->set_output(json_encode($hasil));
	}
	
	function create_paket_narcose(){
		
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'tipe_form' =>$this->input->post('tipe_form'),
			'created_date' =>date('Y-m-d H:i:s'),
			'created_by' =>$user_id,
			'nama_paket' =>'',
			'status_paket' =>'1',
			'total' =>'0',
			'st_edited' =>'0',

		);
		$this->db->insert('mpaket_bedah',$data);
		$this->output->set_output(json_encode($data));
	}
	
	function simpan_paket_narcose(){
		$user_id=$this->session->userdata('user_id');
		$paket_detail_id=$this->input->post('paket_detail_id');
		
		$data=array(
			'paket_id' => $this->input->post('paket_id'),
			'idtipe' => $this->input->post('idtipe'),
			'idobat' => $this->input->post('idobat'),
			'nama_barang' => $this->input->post('nama_barang'),
			'nama_satuan' => $this->input->post('nama_satuan'),
			'idsatuan' => $this->input->post('idsatuan'),
			'harga' => RemoveComma($this->input->post('harga')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'total_harga' => RemoveComma($this->input->post('total_harga')),
			'created_by' => $user_id,
			'created_date' => date('Y-m-d H:i:s'),

		);
		if ($paket_detail_id){
			$this->db->where('id',$paket_detail_id);
			$result=$this->db->update('mpaket_bedah_detail',$data);
		}else{
			$result=$this->db->insert('mpaket_bedah_detail',$data);
			
		}
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function load_narcose_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT M.nama_tipe,H.* FROM mpaket_bedah_detail H
			INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_narcose('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_narcose('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'"><input type="hidden" class="cls_idobat" value="'.$r->idobat.'"> '.$r->nama_tipe.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_barang.'</td>';
			$tabel .='<td class="text-center">'.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total_harga,0).'</td>';
			
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->total_harga;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function lihat_data_narcose_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT M.nama_tipe,H.* FROM mpaket_bedah_detail H
			INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			// $result[] = $no;
			$aksi='';
			$btn_copy='<button class="btn btn-warning  btn-xs" onclick="copy_narcose_paket_detail('.$r->id.')" type="button" title="Terapkan Paket" type="button"><i class="fa fa-copy"></i> Apply</button>';
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_copy;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'"><input type="hidden" class="cls_idobat" value="'.$r->idobat.'"> '.$r->nama_tipe.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_barang.'</td>';
			$tabel .='<td class="text-center">'.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total_harga,0).'</td>';
			
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->total_harga;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function get_edit_paket_narcose(){
		$id=$this->input->post('id');
		$q="SELECT H.* FROM `mpaket_bedah_detail` H
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_paket(){
		$paket_id=$this->input->post('paket_id');
		$nama_paket=$this->input->post('nama_paket');
		$status_paket=$this->input->post('status_paket');
		$st_edited=$this->input->post('st_edited');
		$data=array(
			'nama_paket' => $nama_paket,
			'status_paket' => $status_paket,
		);
		if ($status_paket=='2'){
			$data['st_edited']=0;
		}
		// if ($st_edited=='0'){
			// $data['status_paket']=0;
			// $data['staktif']=0;
		// }else{
			
			// $data['status_paket']=2;
		// }
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_bedah',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	function batal_paket_narcose(){
		$paket_id=$this->input->post('paket_id');
		
		$st_edited=$this->input->post('st_edited');
		
		if ($st_edited=='0'){
			$data['status_paket']=0;
			$data['staktif']=0;
		}else{
			
			$data['status_paket']=2;
			$data['st_edited']=0;
		}
		
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_bedah',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	function simpan_paket_edit(){
		$paket_id=$this->input->post('paket_id');
		$status_paket=$this->input->post('status_paket');
		$data=array(
			'st_edited' => 1,
			'status_paket' => 1,
		);
		
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_bedah',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	function list_history_narcose_paket(){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$nama_paket=$this->input->post('nama_paket');
		$tipe_form=$this->input->post('tipe_form');
		
		if ($nama_paket!=''){
			$where .=" AND H.nama_paket LIKE '%".$nama_paket."%'";
		}
		
		if ($tgl_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tgl_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tgl_input_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.*,SUM(D.total_harga) as total_harga,MU.`name` as nama_user 
					,GROUP_CONCAT(D.id) as detail_id
					FROM mpaket_bedah H
					INNER JOIN mpaket_bedah_detail D ON D.paket_id=H.id
					INNER JOIN musers MU ON MU.id=H.created_by
					WHERE H.status_paket='2' AND H.staktif='1' AND H.tipe_form='$tipe_form'
					GROUP BY H.id
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_paket');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$detail_id="'".$r->detail_id."'";
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_narcose_paket('.$r->id.')" type="button" title="Detail" type="button"><i class="fa fa-eye"></i> Detail</button>';
		$btn_edit='<button class="btn btn-success  btn-xs" onclick="edit_narcose_paket('.$r->id.')" type="button" title="Edit Paket" type="button"><i class="fa fa-pencil"></i> Edit</button>';
		$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_narcose_paket('.$r->id.')" type="button" title="Hapus Paket" type="button"><i class="fa fa-trash"></i> Hapus</button>';
		$btn_copy='<button class="btn btn-warning  btn-xs" onclick="copy_narcose_paket('.$detail_id.')" type="button" title="Terapkan Paket" type="button"><i class="fa fa-copy"></i> Apply</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $btn_edit;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_copy;	
		$aksi .= '</div>';
		$result[] = ($r->nama_paket);
		$result[] = number_format($r->total_harga,0);
		$result[] = ($r->nama_user).'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  public function loadBarangCopy()
    {
        $kelompok_pasien     = $this->input->post('idkelompokpasien');
        $idunit     = $this->input->post('idunit');
        $array_paket_id     	= $this->input->post('array_paket_id');
        
        $from="(
			SELECT S.stok as stok_asli,MS.nama as nama_satuan 
				,B.*,H.kuantitas
				,(CASE
				WHEN $kelompok_pasien = 5 THEN
					marginumum
				WHEN $kelompok_pasien = 0 THEN
					marginumum
				WHEN $kelompok_pasien = 1 THEN
					marginasuransi
				WHEN $kelompok_pasien = 2 THEN
					marginjasaraharja
				WHEN $kelompok_pasien = 3 THEN
					marginbpjskesehatan
				WHEN $kelompok_pasien = 4 THEN
					marginbpjstenagakerja
			END) AS margin
				FROM `mpaket_bedah_detail` H
				LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan='$idunit'
				INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
				LEFT JOIN msatuan MS ON MS.id=B.idsatuan
				WHERE H.id IN (".$array_paket_id.")
		) as tbl";
        // print_r($from);exit();


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama');
        $this->column_order    = array('kode','nama');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = $r->nama;
            $row[] = $r->nama_satuan;
            $row[] = number_format(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100),0);

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = ($r->kuantitas>$r->stok_asli ? text_danger('TIDAK TERSEDIA'):text_primary('TERSEDIA'));
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
	
  function terapkan_copy(){
	  $user_id=$this->session->userdata('user_id');
	  $nama_user_transaksi=$this->session->userdata('user_name');
	$idunit=$this->input->post('idunit');
	$kelompok_pasien=$this->input->post('idkelompokpasien');
	$idbarang=$this->input->post('idbarang');
	$idpendaftaranoperasi=$this->input->post('pendaftaran_id');
	
	
	$nama_tabel_diskon=$this->input->post('nama_tabel_diskon');
	$nama_tabel_trx=$this->input->post('nama_tabel_trx');
	$idtipe=$this->input->post('idtipe');
	// print_r($idtipe);exit;
	$kelompokpasien=$this->input->post('idkelompokpasien');
	$idrekanan=$this->input->post('idrekanan');
	if ($kelompokpasien!='1'){
		$idrekanan=0;
	}
	$idunit=$this->input->post('idunit');
	$tujuan_pasien=$this->input->post('tujuan_pasien');
	$idruangan=$this->input->post('idruangan');
	$idkelas=$this->input->post('idkelas');
	$jenis_opr=$this->input->post('jenis_opr');
	
	if ($kelompok_pasien!='1'){
		$idrekanan=0;
	}
	  $q="
		SELECT S.stok as stok_asli,MS.nama as nama_satuan 
				,B.*,H.kuantitas
				,(CASE
				WHEN $kelompok_pasien = 5 THEN
					marginumum
				WHEN $kelompok_pasien = 0 THEN
					marginumum
				WHEN $kelompok_pasien = 1 THEN
					marginasuransi
				WHEN $kelompok_pasien = 2 THEN
					marginjasaraharja
				WHEN $kelompok_pasien = 3 THEN
					marginbpjskesehatan
				WHEN $kelompok_pasien = 4 THEN
					marginbpjstenagakerja
			END) AS margin
				FROM `mpaket_bedah_detail` H
				LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan='$idunit'
				INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
				LEFT JOIN msatuan MS ON MS.id=B.idsatuan
				WHERE H.id IN (".$idbarang.")
	  ";
	$list_barang=$this->db->query($q)->result();
	  // print_r($list_barang);exit;
	$jml_insert=0;
	foreach($list_barang as $r){
		if ($r->kuantitas<=$r->stok_asli){
			$jml_insert=$jml_insert+1;
			$kelompokpasien=$kelompok_pasien;
			$idbarang=$r->id;
			$idtipe=$r->idtipe;
			$kuantitas=$r->kuantitas;
			$hargadasar=$r->hargadasar;
			$harga_before_diskon=(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100));
			$q_diskon="
				SELECT M.* FROM (
					SELECT compare_value_11(
						MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.tujuan_pasien='0' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='2' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='$tujuan_pasien' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='$idkelas' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='0' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='0' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.tujuan_pasien='0' AND H.idruangan='0' AND H.idkelas='0' AND H.jenis_opr='$jenis_opr' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
						,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND (H.tujuan_pasien='0' OR H.tujuan_pasien='$tujuan_pasien') 
							AND (H.idruangan='0' OR H.idruangan='$idruangan') AND (H.idkelas='0' OR H.idkelas='$idkelas') AND (H.jenis_opr='$jenis_opr' OR H.jenis_opr='0') 
							AND (H.idrekanan='0' OR H.idrekanan='$idrekanan') AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					) as id FROM ".$nama_tabel_diskon." H
				
				) T INNER JOIN ".$nama_tabel_diskon." M
				WHERE M.id=T.id

			";
			// print_r($q_diskon);exit;
				$data_diskon=$this->db->query($q_diskon)->row();
				$diskon_rp=0;
				$diskon_persen=0;
				if ($data_diskon){
					if ($data_diskon->operand!=0){
						if ($data_diskon->operand=1){
							$diskon_persen=$data_diskon->diskon;
							$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
						}else{
							$diskon_persen=0;
							$diskon_rp=ceiling($data_diskon->diskon,100);
						}
					}
				}else{
					
				}
				// print_r($harga_before_diskon);exit;
			$hargajual_before=$harga_before_diskon-$diskon_rp;
			$hargajual=ceiling($hargajual_before,100);	
			$data=array(
				'idpendaftaranoperasi' => $idpendaftaranoperasi,
				'iduser_transaksi' => $user_id,
				'nama_user_transaksi' => $nama_user_transaksi,
				'tanggal_transaksi'=> date('Y-m-d H:i:s'),
				'idunit' => $idunit,
				'idtipe' => $r->idtipe,
				'idobat' => $r->id,
				'hargadasar' => RemoveComma($hargadasar),
				'margin' => ($r->margin),
				'hargajual' => $hargajual,
				'kuantitas' => ($kuantitas),
				'diskon' => $diskon_rp,
				'totalkeseluruhan' => $hargajual * $kuantitas,
				'statusverifikasi' => 0,
				// 'statusstok' => $r->stok_asli,
				'iduser_transaksi' => $user_id,
				'harga_before_diskon' => ($harga_before_diskon),
				'diskon_persen' => $diskon_persen,
				'pembulatan' => 0,
			);
			
			$result=$this->db->insert($nama_tabel_trx,$data);
		}
	}
	$this->output->set_output(json_encode($jml_insert));
	  
  }
  //AKHIR NARRCOSE
  
  function LoadJadwalPilih(){
	  $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$id=$this->input->post('id');
		$pendaftaran_bedah_id=$this->input->post('pendaftaran_bedah_id');
		
		$this->select = array();
		$from="
				(
					SELECT H.id,H.notransaksi,H.tanggaloperasi,MD.nama as nama_dokter,H.diagnosa,H.operasi,J.ref as jenis,H.namapasien  
					,H.waktumulaioperasi,H.waktuselesaioperasi
					FROM `tkamaroperasi_pendaftaran` H
					LEFT JOIN mdokter MD ON MD.id=H.dpjp_id
					LEFT JOIN merm_referensi J ON J.nilai=H.jenis_operasi_id AND J.ref_head_id='124'
					WHERE H.idpendaftaran='$id' AND H.`status` !='0'
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('notransaksi');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$result = array();
		if ($pendaftaran_bedah_id==$r->id){
		$btn_lihat=text_default('SEDANG DILIHAT');
			
		}else{
		$btn_lihat='<a href="'.site_url().'tkamar_bedah/tindakan/'.$r->id.'/erm_bedah/input_bedah_trx"  class="btn btn-primary menu_click  btn-xs" type="button" title="Detail" type="button"><i class="fa fa-eye"></i> Open</a>';
			
		}
		
		$result[] = $no;
		$aksi='';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= '</div>';
		$result[] = ($r->notransaksi);
		$result[] = ($r->namapasien);
		$result[] = text_primary(HumanTimeShort($r->waktumulaioperasi).' s/d '.HumanTimeShort($r->waktumulaioperasi)).'<br><br>'.text_warning(HumanDateShort($r->tanggaloperasi));
		$result[] = ($r->nama_dokter);
		$result[] = ($r->diagnosa);
		$result[] = ($r->operasi);
		$result[] = ($r->jenis);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
}
