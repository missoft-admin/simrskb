<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_analisa extends CI_Controller {

	/**
	 * Master Setting Analisa controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_analisa_model', 'model');
  }

	function index() {
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Analisa';
		$data['content'] 		= 'Msetting_analisa/index';
		$data['breadcrum'] 	= array(
			array("RSKB Halmahera",'#'),
			array("Setting Analisa",'msetting_analisa/index'),
			array("List",'#')
		);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create() {
		$data = array(
			'id' => '',
			'nama' => '',
			'jenis' => array(),
			'tipe' => array(),
			'poliklinik' => array(),
			'dokter' => array(),
			'kelompok_diagnosa' => array(),
			'kelompok_tindakan' => array(),
			'kelompok_tindakan_operasi' => array(),
			'lembaran' => array(),
			'keterangan' => '',
			'status' => '',
			'formData' => $this->model->formData(),
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Setting Analisa';
		$data['content'] 		= 'Msetting_analisa/manage';
		$data['breadcrum']	= array(
			array("RSKB Halmahera",'#'),
			array("Setting Analisa",'#'),
			array("Tambah",'msetting_analisa')
		);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id) {
		if($id != '') {
			$row = $this->model->getSpecified($id);
			if(isset($row->id)) {

				$poliklinik = array_map(function($item) {
						return $item->tipe . '_' . $item->id;
					},
					jsonDecode($row->poliklinik)
				);

				$data = array(
					'id' => $row->id,
					'nama' => $row->nama,
					'jenis' => jsonDecode($row->jenis),
					'tipe' => jsonDecode($row->tipe),
					'poliklinik' => $poliklinik,
					'dokter' => jsonDecode($row->dokter),
					'kelompok_diagnosa' => jsonDecode($row->kelompok_diagnosa),
					'kelompok_tindakan' => jsonDecode($row->kelompok_tindakan),
					'kelompok_tindakan_operasi' => jsonDecode($row->kelompok_tindakan_operasi),
					'lembaran' => jsonDecode($row->lembaran),
					'keterangan' => $row->keterangan,
					'status' => $row->status,
					'formData' => $this->model->formData(),
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Setting Analisa';
				$data['content']	 	= 'Msetting_analisa/manage';
				$data['breadcrum'] 	= array(
					array("RSKB Halmahera",'#'),
					array("Setting Analisa",'#'),
    			array("Ubah",'msetting_analisa')
				);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('msetting_analisa','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('msetting_analisa','location');
		}
	}

	function save() {
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE) {
			if($this->input->post('id') == '' ) {
				if($this->model->saveData()) {
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msetting_analisa/index/'.$this->input->post('idtipe'),'location');
				}
			} else {
				if($this->model->updateData()) {
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msetting_analisa/index/'.$this->input->post('idtipe'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id) {
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Msetting_analisa/manage';

		if($id=='') {
			$data['title'] = 'Tambah Setting Analisa';
			$data['breadcrum'] = array(
					array("RSKB Halmahera",'#'),
					array("Setting Analisa",'#'),
					array("Tambah",'msetting_analisa')
			);
		}else{
			$data['title'] = 'Ubah Setting Analisa';
			$data['breadcrum'] = array(
					array("RSKB Halmahera",'#'),
					array("Setting Analisa",'#'),
					array("Ubah",'msetting_analisa')
			);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function delete($id) {
		$this->model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('msetting_analisa','location');
	}

	function getIndex()
	{
			$this->select = array('*');
			$this->from   = 'msetting_analisa';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array();

			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;
					$row[] = $r->lembaran ? count(json_decode($r->lembaran)) : 0;
					$row[] = $r->keterangan;
					$row[] = LabelSettingAnalisa($r->status);
					$row[] = '<div class="btn-group">
						<a href="'.site_url().'msetting_analisa/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
						<a href="#" data-urlindex="'.site_url().'msetting_analisa" data-urlremove="'.site_url().'msetting_analisa/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
					</div>';

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
