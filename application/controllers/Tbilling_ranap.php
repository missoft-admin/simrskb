<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Hashids\Hashids;

// reference the Dompdf namespace

class Tbilling_ranap extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbilling_ranap_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Trawatinap_verifikasi_model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Thonor_dokter_model');
		$this->load->model('Tverifikasi_transaksi_model');
		$this->load->model('Tvalidasi_model');
		$this->load->model('Mbagi_hasil_model');
		$this->load->helper('path');
		// print_r('sini');exit;
		
  }
	function index(){
		$log['path_tindakan']='tbilling_ranap';
		$this->session->set_userdata($log);
		 get_ppa_login();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1939'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-30 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Tbilling_ranap_model->list_poli();
			$data['list_dokter'] 			= $this->Tbilling_ranap_model->list_dokter();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tab_utama'] 			= 3;
			$data['idalasan'] 			= '#';
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['tab_detail'] 			=1;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE()";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Tbilling_ranap/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Billing & Payment",'#'),
												  array("Rawat Inap & ODS",'tbilling_ranap')
												);
			$data_setting=$this->db->query("SELECT *FROM setting_deposit")->row_array();
			$data = array_merge($data,$data_setting, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$iddokter =$this->input->post('iddokter');
			$idtipe =$this->input->post('idtipe');
			$idtipe_asal =$this->input->post('idtipe_asal');
			$cari_pasien =$this->input->post('cari_pasien');
			$nopendaftaran=$this->input->post('nopendaftaran');
			$idruangan=$this->input->post('idruangan');
			$idkelas=$this->input->post('idkelas');
			$idbed=$this->input->post('idbed');
			$norujukan=$this->input->post('norujukan');
			$iddokter_perujuk=$this->input->post('iddokter_perujuk');
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$idrekanan=$this->input->post('idrekanan');
			$status_tindakan_index=$this->input->post('status_tindakan_index');
			
			$idpoliklinik_array=$this->input->post('idpoliklinik_array');
			if ($idpoliklinik_array){
				$idpoliklinik_array=implode(", ", $idpoliklinik_array);
				$where .=" AND (H.idpoliklinik_asal IN (".$idpoliklinik_array.")) ";
			}
			$iddokter_perujuk_array=$this->input->post('iddokter_perujuk_array');
			if ($iddokter_perujuk_array){
				$iddokter_perujuk_array=implode(", ", $iddokter_perujuk_array);
				$where .=" AND (H.iddokter_perujuk IN (".$iddokter_perujuk_array.")) ";
			}
			$iddokter_dpjp=$this->input->post('iddokter_dpjp');
			if ($iddokter_dpjp){
				$iddokter_dpjp=implode(", ", $iddokter_dpjp);
				$where .=" AND (H.iddokterpenanggungjawab IN (".$iddokter_dpjp.")) ";
			}
			$dpjp_pendukung_array=$this->input->post('dpjp_pendukung_array');
			if ($dpjp_pendukung_array){
				$dpjp_pendukung_array=implode(", ", $dpjp_pendukung_array);
				$where .=" AND (HP.iddokter IN (".$dpjp_pendukung_array.")) ";
			}
			$idkelompokpasien_array=$this->input->post('idkelompokpasien_array');
			if ($idkelompokpasien_array){
				$idkelompokpasien_array=implode(", ", $idkelompokpasien_array);
				$where .=" AND (H.idkelompokpasien IN (".$idkelompokpasien_array.")) ";
			}
			$idrekanan_array=$this->input->post('idrekanan_array');
			if ($idrekanan_array){
				$idrekanan_array=implode(", ", $idrekanan_array);
				$where .=" AND (H.idrekanan IN (".$idrekanan_array.")) ";
			}
			$idruangan_array=$this->input->post('idruangan_array');
			if ($idruangan_array){
				$idruangan_array=implode(", ", $idruangan_array);
				$where .=" AND (H.idruangan IN (".$idruangan_array.")) ";
			}
			$idkelas_array=$this->input->post('idkelas_array');
			if ($idkelas_array){
				$idkelas_array=implode(", ", $idkelas_array);
				$where .=" AND (H.idkelas IN (".$idkelas_array.")) ";
			}
			$idbed_array=$this->input->post('idbed_array');
			if ($idbed_array){
				$idbed_array=implode(", ", $idbed_array);
				$where .=" AND (H.idkelas IN (".$idbed_array.")) ";
			}
			$status_tindakan_array=$this->input->post('status_tindakan_array');
			if ($status_tindakan_array){
				$where .=" AND (";
				$where_tindakan='';
				
				if (in_array('2', $status_tindakan_array)){
					// if ($statuskasir=='1' && $statuspembayaran=='0'){
					$where_tindakan .=($where_tindakan==''?'':' OR ')."(H.statuskasir='1' AND H.statuspembayaran='0') ";
				}
				if (in_array('3', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.st_terima_ranap='1' AND H.status='1') ";
				}
				if (in_array('4', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuspembayaran='1') ";
				}
				if (in_array('5', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuscheckout='1') ";
				}
				$where .=$where_tindakan.")";
			}
			$no_medrec=$this->input->post('no_medrec');
			if ($no_medrec !=''){
				$where .=" AND (H.no_medrec LIKE '%".$no_medrec."%') ";
			}
			$namapasien=$this->input->post('namapasien');
			if ($namapasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$namapasien."%') ";
			}
			
			
			if ($iddokter!='#'){
				$where .=" AND (H.iddokterpenanggungjawab='$iddokter') ";
			}
			if ($idrekanan!='#'){
				$where .=" AND (H.idrekanan='$idrekanan') ";
			}
			if ($idkelompokpasien!='#'){
				$where .=" AND (H.idkelompokpasien='$idkelompokpasien') ";
			}
			if ($iddokter_perujuk!='#'){
				$where .=" AND (H.iddokter_perujuk='$iddokter_perujuk') ";
			}
			if ($idbed!='#'){
				$where .=" AND (H.idbed='$idbed') ";
			}
			if ($idruangan!='#'){
				$where .=" AND (H.idruangan='$idruangan') ";
			}
			if ($idkelas!='#'){
				$where .=" AND (H.idkelas='$idkelas') ";
			}
			if ($idtipe!='#'){
				$where .=" AND (H.idtipe='$idtipe') ";
			}
			if ($idtipe_asal!='#'){
				$where .=" AND (H.idtipe_asal='$idtipe_asal') ";
			}
			if ($norujukan !=''){
				$where .=" AND (H.nopermintaan LIKE '%".$norujukan."%') ";
			}
			if ($nopendaftaran !=''){
				$where .=" AND (H.nopendaftaran LIKE '%".$nopendaftaran."%' OR H.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			}
			
			if ($cari_pasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$cari_pasien."%' OR H.no_medrec LIKE '%".$cari_pasien."%') ";
			}
			// print_r($where);exit;
			$tab =$this->input->post('tab');
			if ($tab=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($tab=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1' AND H.statuscheckout='0') ";
			}
			if ($tab=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($tab=='5'){
				$where .=" AND (H.statuscheckout='1') ";
			}
			if ($status_tindakan_index=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($status_tindakan_index=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			}
			if ($status_tindakan_index=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($status_tindakan_index=='5'){
				$where .=" AND (H.statuscheckout='1') ";
				if ($tanggal_1 !=''){
					$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
				}else{
					$where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
				}
			}
			
			
			$tanggal_1_trx =$this->input->post('tanggal_1_trx');
			if ($tanggal_1_trx !=''){
				$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2_trx)."'";
			}
			$tanggal_1_pulang =$this->input->post('tanggal_1_pulang');
			if ($tanggal_1_pulang !=''){
				$where .=" AND DATE(H.tanggalcheckout) >='".YMDFormat($tanggal_1_pulang)."' AND DATE(H.tanggalcheckout) <='".YMDFormat($tanggal_2_pulang)."'";
			}
			$this->select = array();
			$get_alergi_sql=get_alergi_sql();
			$from="
					(
						SELECT COALESCE((DP.nominal),0)  as total_deposit,MAX(DH.id) as head_id,
						PB.status_verifikasi_bayar,PB.st_verifikasi_piutang
						,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
						,H.*,MKL.nama as nama_kelas 
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
						,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping,TT.nama_tindakan_setuju		
						,ST.nama_tindakan
						FROM trawatinap_pendaftaran H
						LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
						LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
						LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
						LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
						LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mbed MB on MB.id=H.idbed						
						LEFT JOIN musers UH on UH.id=H.deleted_by	
						LEFT JOIN (
							".$get_alergi_sql."
						) A ON A.idpasien=H.idpasien 
						LEFT JOIN (
							SELECT H.idtindakan as pendaftaran_id,H.status_verifikasi as status_verifikasi_bayar,D.st_verifikasi_piutang FROM trawatinap_tindakan_pembayaran H
							LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id AND D.st_verifikasi_piutang=1 AND D.idmetode IN (6,8)
							WHERE H.statusbatal='0'
							GROUP BY H.id

						) PB ON PB.pendaftaran_id=H.id
						LEFT JOIN (
							SELECT ST.idrawatinap,GROUP_CONCAT(CONCAT(ST.deskripsi, ' (',ST.nama_tindakan,')')) as nama_tindakan,ST.st_persetujuan 
							FROM `trawatinap_deposit_head` ST 
							WHERE ST.tipe_deposit AND ST.tipe_deposit='1' AND ST.`status` = '1'
							GROUP BY ST.idrawatinap
						) ST ON ST.idrawatinap=H.id
						LEFT JOIN (
							".get_tindakan_persetujuan()."
						) TT ON TT.idrawatinap=H.id
						
						LEFT JOIN (
							SELECT DP.idrawatinap,SUM(DP.nominal) as nominal FROM `trawatinap_deposit` DP 
							WHERE  DP.`status`='1' AND DP.st_proses='1'
							GROUP BY DP.idrawatinap
						) DP ON DP.idrawatinap=H.id 
						LEFT JOIN trawatinap_deposit_head DH ON DH.idrawatinap=H.id AND DH.`status`='1'
						WHERE H.idtipe IS NOT NULL ".$where."
						GROUP BY H.id
						
						ORDER BY H.tanggaldaftar DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_persetujuan_pulang=panel_persetujuan_pulang($r->status_proses_card_pass,$r->nopendaftaran);
		 
		  if ($r->nama_tindakan_setuju){
			  
			  $btn_tindakan_setujua='
				<div class="text-danger font-s13 push-5-t text-bold">
				<button class="btn btn-success  btn-xs" type="button" onclick="show_list_tindakan('.$r->id.')">'.$r->nama_tindakan_setuju.'</button>
				</div>
			  ';
		  }else{
			  $btn_tindakan_setujua='';
		  }
		  if ($r->idtipe=='1'){
			  $info_bed='<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>';
		  }else{
			  $info_bed='';
		  }
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_checkout='';
		  $btn_deposit='';
		  $btn_terima='';
		  $btn_layani='';
		  $btn_trx='';
		  $btn_panel='';
		  $pedamping='';
		  $btn_lock='';
		  $btn_tindakan='';
		  $btn_baru='';
		  $btn_add='';
		  if ($r->nama_tindakan){
			  $btn_tindakan='<div class="push-5-t"><button onclick="show_modal_persetujuan('.$r->id.')"  type="button" data-toggle="tooltip" title="PERSETUJUAN TINDAKAN "  class="btn btn-block btn-success btn-xs"><i class="fa fa-check"></i> PERSETUJUAN TINDAKAN</button> </div>';
		  }
		  $btn_verifikasi='';
		  $btn_verifikasi=($r->status_verifikasi_bayar?text_success('VERIFIED'):'');
		  $btn_verifikasi_piutang='';
		  $btn_verifikasi_piutang='&nbsp;&nbsp;'.($r->st_verifikasi_piutang?text_danger('VERIFIED PIUTANG'):'');
		  if ($r->statuskasir=='0'){//LOCK
			  $btn_lock='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Lock" onclick="show_lock('.$r->id.',1)" class="btn btn-danger btn-xs"><i class="fa fa-lock"></i> &nbsp;&nbsp;LOCK&nbsp;&nbsp;</a> </div>';
		  }else{
			  $btn_lock='<div class="push-5-t"><button type="button" data-toggle="tooltip" '.($r->statustransaksi=='1' && $r->statuspembayaran=='1'?'disabled':'').' title="Unlock" onclick="show_lock('.$r->id.',0)" class="btn btn-success btn-xs"><i class="fa fa-unlock"></i>&nbsp;&nbsp; UNLOCK&nbsp;&nbsp;</a> </div>';
			  
		  }
		  // $label_total='Total Keseluruhan : Rp. 1.780.000';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
			  if ($r->status!='0'){
					$btn_baru='<div class="push-5-t"><button onclick="show_modal_deposit_header('.$r->id.')"  type="button" data-toggle="tooltip" title="TINDAKAN BARU"  class="btn btn-block btn-primary btn-xs"><i class="fa fa-plus"></i> TINDAKAN BARU</button> </div>';
					if ($r->head_id){
						$btn_add ='<div class="push-5-t"><button onclick="show_modal_deposit_header_add('.$r->id.','.$r->head_id.')"  type="button" data-toggle="tooltip" title="TRANSAKSI DEPOSIT"  class="btn btn-block btn-warning btn-xs"><i class="fa fa-list-ul"></i> TRANSAKSI DEPOSIT</button> </div>';
					}
					$btn_trx='<div class="push-5-t"><a href="'.base_url().'tbilling_ranap/tindakan/'.$r->id.'/1/erm_trx/pembayaran_ri" target="_blank" type="button" data-toggle="tooltip" title="Transaksi"  class="btn btn-block btn-success btn-xs"><i class="fa fa-credit-card"></i> TRANSAKSI</a> </div>';
					$btn_panel=div_panel_kendali_ri_admin($user_acces_form,$r->id);
			  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_trx.'
									'.$btn_panel.'
									'.$btn_lock.'
									'.$btn_baru.'
									'.$btn_add.'
									'.$btn_tindakan.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_edit_tipe='<button class="btn btn-xs btn-default" onclick="modal_edit_tipe('.$r->id.','.$r->idtipepasien.')" type="button" data-toggle="tooltip" title="Edit Tipe Pasien" data-original-title="Edit Tipe Pasien"><i class="fa fa-pencil"></i></button>';
		  $btn_edit_catatan='<button class="btn btn-xs btn-default" onclick="modal_edit_catatan('.$r->id.')" type="button" data-toggle="tooltip" title="Edit Catatan" data-original-title="Edit Catatan Pasien"><i class="fa fa-pencil"></i></button>';
		  $catatan_pasien='Tipe Pasien : '.($r->idtipepasien=='1'?'Pasien RS':'Pasien Pribadi ').' '.$btn_edit_tipe.' | Catatan : '.($r->catatan?$r->catatan:' - ').$btn_edit_catatan;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="font-s13 push-5-t">'.$catatan_pasien.'</div>
									<div class="push-5-t">'.$btn_tindakan_setujua.$btn_persetujuan_pulang.'</div>
									<div class="text-danger font-s13 push-5-t text-bold">'.$btn_verifikasi.$btn_verifikasi_piutang.'</div>
									<input type="hidden" class="form-control st_tampil" value="0">
									<input type="hidden" class="form-control idranap" value="'.$r->id.'">
									<span class="h5 font-w500 text-primary push-5-t label_total_deposit">Total Deposit : Rp. '.number_format($r->total_deposit,0).'</span>  &nbsp;&nbsp;&nbsp;
									<span class="h5 font-w500 text-danger label_total_trx"></span>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> DPJP : '.($r->nama_dokter).'</div>
									'.$pedamping.'
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_ranap($r->status,$r->st_terima_ranap,$r->statuscheckout,$r->statuspembayaran,$r->statuskasir).'</div>
									'.$info_bed.'
									<div class="h5 push-10-t text-primary"><strong>'.GetTipeRujukanRawatInap($r->idtipe).'</strong></div>
									<div class="h5 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggaldaftar).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_data_deposit(){
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $q="SELECT H.no_medrec,CONCAT(H.title,' ',H.namapasien) as nama_pasien,H.nohp,H.namapenanggungjawab,H.teleponpenanggungjawab,
KP.nama as nama_kp,CASE WHEN H.idkelompokpasien='1' THEN  MR.nama ELSE '' END as nama_rekanan,MK.nama as nama_kelas,MB.nama as nama_bed,R.nama as nama_ruangan
,H.* FROM `trawatinap_pendaftaran` H
LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
LEFT JOIN mkelas MK ON MK.id=H.idkelas
LEFT JOIN mbed MB ON MB.id=H.idbed
LEFT JOIN mruangan R ON R.id=H.idruangan
		WHERE H.id='$pendaftaran_id'";
		
		$result=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($result));
		
  }
  function status_ranap($status,$st_terima_ranap,$statuscheckout,$statuspembayaran,$statuskasir){
	  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-facebook pull-left "></i> Login with Facebook</button>';
	  if ($status=='0'){
		  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button"><i class="fa fa-times pull-left "></i>DIBATALKAN</button>';
	  }else{
		  if ($st_terima_ranap=='0'){
			  $label='<button class="btn btn-xs btn-block btn-info push-10" type="button"><i class="fa fa-wrench pull-left "></i>MENUNGGU DIRAWAT</button>';
		  }
		  if ($st_terima_ranap=='1' && $statuscheckout=='0'){
			  $label='<button class="btn btn-xs btn-block btn-warning push-10" type="button"><i class="fa fa-bed pull-left"></i>DIRAWAT</button>';
		  }
		 
		  if ($statuskasir=='1' && $statuspembayaran=='0'){
			  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button">MENUNGGU TRANSAKSI</button>';
		  }elseif ($statuskasir=='1' && $statuspembayaran=='1'){
			  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button">SELESAI</button>';
		  }
		   
		  if ($statuscheckout=='1'){
			  $label='<button class="btn btn-xs btn-block btn-success push-10" type="button">PULANG</button>';
		  }
	  }
	  
	  return $label;
  }
  function update_kunjungan(){
	  $pendaftaran_id_lama=$this->input->post('pendaftaran_id_lama');
	  $pendaftaran_id_baru=$this->input->post('pendaftaran_id_baru');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$q="UPDATE tpoliklinik_pendaftaran SET statusrencana='0' WHERE id='$pendaftaran_id_lama'";
		$this->db->query($q);
		$q="UPDATE tpoliklinik_pendaftaran SET statusrencana='1' WHERE id='$pendaftaran_id_baru'";
		$this->db->query($q);
		$q="UPDATE trawatinap_pendaftaran SET idpoliklinik='$pendaftaran_id_baru' WHERE id='$pendaftaran_id_ranap'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function update_tipe_pasein(){
	  $id=$this->input->post('id');
	  $idtipepasien=$this->input->post('idtipepasien');
	 
		$q="UPDATE trawatinap_pendaftaran SET idtipepasien='$idtipepasien' WHERE id='$id'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function update_catatan(){
	  $id=$this->input->post('id');
	  $catatan=$this->input->post('catatan');
	 
		$q="UPDATE trawatinap_pendaftaran SET catatan='$catatan' WHERE id='$id'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function get_catatan(){
	  $id=$this->input->post('id');
	 
		$q="SELECT catatan FROM trawatinap_pendaftaran WHERE id='$id'";
		$hasil=$this->db->query($q)->row_array();
		
		$this->output->set_output(json_encode($hasil));
  }
	function set_lock(){
	  $id=$this->input->post('id');
		$statuskasir=$this->input->post('statuskasir');
		$data=array(
			'statuskasir' =>$statuskasir,

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_pendaftaran',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  public function getIndex_kasir($uri = 'index')
	{
		$this->select = [];
		$this->join = [];
		$this->where = [];

		$idpasien=$this->input->post('idpasien');
		$where = " WHERE P.idpasien='$idpasien'";
		if ($this->input->post('tanggaldari') == '') {
			$where .= " AND DATE(K.tanggal)='" . date('Y-m-d') . "'";
		} else {
			$where .= " AND DATE(K.tanggal)>='" . YMDFormat($this->input->post('tanggaldari')) . "' AND DATE(K.tanggal)<='" . YMDFormat($this->input->post('tanggalsampai')) . "'";
		}
		

		$from = "(SELECT K.id,K.idtindakan
        ,CASE WHEN K.idtipe='3' THEN K.tanggal ELSE K.tanggal END as tanggal,K.idtipe,K.nokasir,P.iddokter,P.idpoliklinik
				,CASE WHEN K.idtipe='3' THEN 'N' ELSE P.id END as idpendaftaran
				,CASE WHEN K.idtipe='3' THEN OB.nopenjualan ELSE P.nopendaftaran END as notrx
				,CASE WHEN K.idtipe='3' THEN OB.nomedrec ELSE P.no_medrec END as no_medrec
				,CASE WHEN K.idtipe='3' THEN '' ELSE P.title END as title
				,CASE WHEN K.idtipe='3' THEN OB.nama ELSE P.namapasien END as nama
				,CASE WHEN K.idtipe='3' THEN '' ELSE mp.nama END as namapoli
				,CASE WHEN K.idtipe='3' THEN pk_ob.id ELSE pk.id END as idkelompokpasien
				,CASE WHEN K.idtipe='3' THEN pk_ob.nama ELSE pk.nama END as namakelompok
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujukfarmasi END as rujukfarmasi
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujuklaboratorium END as rujuklaboratorium
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujukradiologi END as rujukradiologi
				,CASE WHEN K.idtipe='3' THEN '0' ELSE IF(T.rujukfisioterapi='',0,T.rujukfisioterapi)  END as rujukfisioterapi
				,RFa.`status` as far_status
				,RR.`status` as rad_status
				,RL.`status` as lab_status
				,RF.`status` as fis_status
				,K.`status`
				,K.`status_verifikasi`
				,GROUP_CONCAT(B.idmetode) as idmetode
				,GROUP_CONCAT(B.id) as idbayar
				,P.statusrencana,RI.id as ri_id
				,CASE WHEN K.idtipe = '3' THEN OB.status ELSE T.status END AS status_tindakan
				from tkasir K
				LEFT JOIN tpoliklinik_tindakan T ON T.id=K.idtindakan AND K.idtipe IN (1,2)
				LEFT JOIN tpoliklinik_pendaftaran P ON T.idpendaftaran=P.id
				LEFT JOIN mpoliklinik mp ON mp.id=P.idpoliklinik
				LEFT JOIN mpasien_kelompok pk ON pk.id=P.idkelompokpasien
				LEFT JOIN tpasien_penjualan OB ON OB.id=K.idtindakan AND K.idtipe='3'
				LEFT JOIN mpasien_kelompok pk_ob ON pk_ob.id=OB.idkelompokpasien
				LEFT JOIN trujukan_radiologi RR ON RR.idtindakan=T.id AND T.rujukradiologi='1'
				LEFT JOIN trujukan_laboratorium RL ON RL.idtindakan=T.id AND T.rujuklaboratorium='1'
				LEFT JOIN trujukan_fisioterapi RF ON RF.idtindakan=T.id AND T.rujukfisioterapi='1'
				LEFT JOIN tpasien_penjualan RFa ON RFa.idtindakan=T.id AND T.rujukfarmasi='1'
				LEFT JOIN tkasir_pembayaran B ON B.idkasir=K.id
				LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.`status` != 0 

				" . $where . '
				GROUP BY K.id

				ORDER BY K.id DESC) as tbl
        WHERE tbl.status_tindakan != 0';
		// print_r($from);exit;
		$this->order = [];
		$this->group = [];
		$this->from = $from;

		$this->column_search = [];
		$this->column_order = [];

		$list = $this->datatable->get_datatables();

		$data = [];
		$no = $_POST['start'];

		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$url_dok_rajal = site_url('tverifikasi_transaksi/upload_document/tkasir/');

		foreach ($list as $r) {
			$no++;
			$row = [];
			$action = '';
			if ($r->status == 0) {
				//Dibatalkan
				$tipeAction = '';
				if ($r->idtipe == '3') {
					if (button_roles('tkasir/transaksi_ob')) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi_ob/' . $r->idtindakan . '/' . $r->id . '/1' . '" target="_blank"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				} else {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi/' . $r->idpendaftaran . '/' . $r->id . '/1' . '" target="_blank" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				}

				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= $tipeAction . '<div class="btn-group">
                     <div class="btn-group dropright">
                       <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                         <span class="fa fa-print"></span>
                       </button>
                     <ul class="dropdown-menu">
                       <li>
                         <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '/1' . '">Rincian Pembayaran</a>
                         <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                       </li>
                     </ul>
                     </div>
                   </div>';
				}
			} elseif ($r->status == 1) {
				//Menunggu Di Proses

				$tipeAction = '';
				if ($r->idtipe == '3') {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi_ob/' . $r->idtindakan . '/' . $r->id . '/0' . '" target="_blank"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				} else {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						if ($r->ri_id == null) {
							$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi/' . $r->idpendaftaran . '/' . $r->id . '/0' . '" target="_blank"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
						}
					}
				}
				$action = '<div class="btn-group">' . $tipeAction;
				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= '<div class="btn-group">
                      <div class="btn-group dropright">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <span class="fa fa-print"></span>
                        </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '">Rincian Pembayaran</a>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                        </li>
                      </ul>
                      </div>
                    </div>';
				}
				if (UserAccesForm($user_acces_form, ['1088'])) {
					// $action .= '';
				}
				$action .= '</div>';
			} elseif ($r->status == 2) {
				//SELESAI
				//Telah di proses
				$tipeAction = '';
				$action = '<div class="btn-group">' . $tipeAction;
				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= '<div class="btn-group">
                      <div class="btn-group dropright">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <span class="fa fa-print"></span>
                        </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '">Rincian Pembayaran</a>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                        </li>
                      </ul>
                      </div>
                    </div>';

					$action .= '<div class="btn-group" role="group">
								<button class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false"  data-toggle="tooltip" title="Kwitansi">
									<span class="fa fa-list-alt "></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">';
					$action .= '<li><a tabindex="-1" data-toggle="modal" href="#modalKwitansi" onclick="ShowOld(' . $r->id . ',0,0);">Kwitansi All</a></li><li class="divider"></li>';
					$list_bayar = $r->idmetode;
					$list_bayar_id = $r->idbayar;
					$arr_bayar = explode(',', $list_bayar);
					$arr_bayar_id = explode(',', $list_bayar_id);
					foreach ($arr_bayar as $x => $x_value) {
						if (metode_pembayaran_kasir($arr_bayar[$x]) != '') {
							$action .= '<li><a tabindex="-1" data-toggle="modal" href="#modalKwitansi" onclick="ShowOld(' . $r->id . ',' . $arr_bayar_id[$x] . ',' . $arr_bayar[$x] . ');">Print Out Kwitansi ' . metode_pembayaran_kasir($arr_bayar[$x]) . '</a></li>';
						}
					}

					$action .= '</ul></div>';
				}
				if (UserAccesForm($user_acces_form, ['1088'])) {
					if ($r->status_verifikasi == 0) {
						$action .= '<button data-idkasir="' . $r->id . '" class="btn btn-danger btn-sm selectedBatal" id="btn_pembayaran" data-toggle="modal"  data-target="#modalPembatalan"  type="button"><i class="fa fa-trash"></i></button>';
					}
				}
				$action .= '<a class="view btn btn-sm btn-danger" href="' . $url_dok_rajal . $r->id . '" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
				$action .= '</div>';
			} else {
				$action = '';
			}

			$row[] = $no;
			$row[] = $r->tanggal;
			$row[] = $r->notrx;
			$row[] = $r->no_medrec;

			if ($r->title <> '') {
				$row[] = $r->title . '. ' . $r->nama;
			} else {
				$row[] = $r->nama;
			}

			$row[] = $r->namapoli;
			$row[] = $r->namakelompok;
			$row[] = '<p class="nice-copy">' .
											StatusIndexRujukan2($r->rujukfarmasi, 'FR', $r->far_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujuklaboratorium, 'LB', $r->lab_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujukradiologi, 'RD', $r->rad_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujukfisioterapi, 'FI', $r->fis_status) . '&nbsp;' .
										'</p>';
			$label_rencana = '';
			if ($r->statusrencana > 0) {
				$label_rencana = GetStatusRencana($r->statusrencana);
			}

			$row[] = $action;
			if ($r->ri_id != null && $r->status == '1') {
				$row[] = $label_rencana;
			} else {
				$row[] = StatusKasir($r->status) . '<br>' . $label_rencana;
			}

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
	public function success($id)
	{
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'pembayaran telah berhasil disimpan.');
		redirect('tbilling_ranap/tindakan/' . $id.'/1/erm_trx/pembayaran_ri', 'location');
	}
	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		$data_login_ppa=get_ppa_login();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		
		$data['tittle']='Penggunaan BMHP';
		
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tkonsul/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['idpendaftaran'] 			= $pendaftaran_id;
		
		$data['idpoli'] 			= '#';
		
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'ADM Rawat Inap & ODS';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("ADM Rawat Inap & ODS ",'#'),
											  array("ADM Rawat Inap & ODS",'tbilling_ranap')
											);
		
		$data['tanggal_transaksi']=date('d-m-Y H:i:s');
		$data['status_card']=$this->Tbilling_ranap_model->get_logic_card_pass($data['idkelompokpasien'],$data['idrekanan']);
		// print_r($data['status_card']);exit;
		// $data['pendaftaran_id']=$pendaftaran_id;
		// $data_pasien['namapasien']=$data['namapasien'];
		// $data_pasien['no_medrec']=$data['no_medrec'];
		if ($menu_kiri=='pembayaran_ri'){
			$data_setting=$this->db->query("SELECT *
											FROM setting_trx_ranap ")->row_array();
			// $data_dek=$data_setting['label_deskripsi_kwitansi'];
			// $deskripsi_kwitansi_label=$this->parser->parse_string($data_dek,$data_pasien);
			// $data['deskripsi_kwitansi_label']=$deskripsi_kwitansi_label;
			// print_r($data_setting);exit;
			$idpendaftaran=$pendaftaran_id;
			$totaldeposit = $this->Tbilling_ranap_model->getTotalDeposit($idpendaftaran)->totaldeposit;
			$user_form = $this->Tbilling_ranap_model->user_form($idpendaftaran);
			$pembayaran = $this->Trawatinap_tindakan_model->pembayaran($idpendaftaran);
			$data['nominal_sudah_dibayar']=$this->Tbilling_ranap_model->get_data_pembayaran($idpendaftaran);
			if ($data['idtipe']=='1'){
				$data['page']='rawatinap';
			}else{
				$data['page']='ods';
				
			}
			if ($data['statustransaksi']=='1'){
				$data['tab']='3';
			}else{
				if ($data['statusvalidasi']=='1'){
					$data['tab']='2';
				}else{
					$data['tab']='1';
				
				}
				
			}
			if ($data['status_proses_kwitansi']=='1'){
				$data['tab']='4';
			}
			if ($data['status_selesai_verifikasi'] > 0){
				$data['tab']='5';
			}
			
			// $row = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
			// $kwitansi = $this->Trawatinap_tindakan_model->getLastKwitansi($idpendaftaran);
			// $kwitansiImplant = $this->Trawatinap_tindakan_model->getLastKwitansiImplant($idpendaftaran);

			if ($pembayaran) {
				$idPembayaran = $pembayaran->id;
			} else {
				$idPembayaran = 0;
			}

			// if ($kwitansi) {
				// $kwitansi_terima_dari = $kwitansi->sudahterimadari;
			// } else {
				// $kwitansi_terima_dari = '';
			// }
			// $kwitansi_deskripsi = 'Tindakan dan Rawat Inap Pasien a/n ' . $row->namapasien;

			// if ($kwitansiImplant) {
				// $kwitansi_implant_terima_dari = $kwitansiImplant->sudahterimadari;
			// } else {
				// $kwitansi_implant_terima_dari = '';
			// }
			// $kwitansi_implant_deskripsi = 'Implant Pasien a/n ' . $row->namapasien;

			// $rowOperasi = $this->Trawatinap_tindakan_model->getDataOperasi($idpendaftaran);
			// $page='rawatinap';
			// $data = [
				// 'idpendaftaran' => $row->id,
				// 'nomedrec' => $row->nomedrec,
				// 'idpasien' => $row->idpasien,
				// 'idtipepasien' => $row->idtipepasien,
				// 'namapasien' => $row->namapasien,
				// 'alamatpasien' => $row->alamatpasien,
				// 'nohp' => ($row->hp ? $row->hp : $row->telepon),
				// 'namapenanggungjawab' => $row->nama_keluarga,
				// 'telppenanggungjawab' => $row->telepon_keluarga,
				// 'idkelompokpasien' => $row->idkelompokpasien,
				// 'namakelompok' => $row->namakelompok,
				// 'namadokterpenanggungjawab' => $row->namadokterpenanggungjawab,
				// 'idruangan' => $row->idruangan,
				// 'idkelas' => $row->idkelas,
				// 'namaruangan' => $row->namaruangan,
				// 'namakelas' => $row->namakelas,
				// 'namaperusahaan' => ($row->idkelompokpasien == 1 && $row->namarekanan ? $row->namarekanan : '-'),
				// 'namabed' => $row->namabed,
				// 'statuscheckout' => $row->statuscheckout,
				// 'statuskasir' => $row->statuskasir,
				// 'statusvalidasi' => $row->statusvalidasi,
				// 'statustransaksi' => $row->statustransaksi,
				// 'statuspembayaran' => $row->statuspembayaran,
				// 'catatan' => $row->catatan,
				// 'totaldeposit' => $totaldeposit,
				// 'last_sudah_terima_dari' => $kwitansi_terima_dari,
				// 'last_deskripsi' => $kwitansi_deskripsi,
				// 'implant_last_sudah_terima_dari' => $kwitansi_implant_terima_dari,
				// 'implant_last_deskripsi' => $kwitansi_implant_deskripsi,
				// 'page' => $page,
				// 'diagnosa' => ($rowOperasi ? $rowOperasi->diagnosa : ''),
				// 'operasi' => ($rowOperasi ? $rowOperasi->operasi : ''),
				// 'tanggaloperasi' => ($rowOperasi ? $rowOperasi->tanggaloperasi : ''),

				// 'pembayaran_tunai' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(1, $idPembayaran),
				// 'pembayaran_debit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(2, $idPembayaran),
				// 'pembayaran_kredit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(3, $idPembayaran),
				// 'pembayaran_transfer' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(4, $idPembayaran),
				// 'pembayaran_karyawan_pegawai' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(5, $idPembayaran),
				// 'pembayaran_karyawan_dokter' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(6, $idPembayaran),
				// 'pembayaran_tidak_tertagihkan' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(7, $idPembayaran),
				// 'pembayaran_kontraktor' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(8, $idPembayaran),
				// 'pembayaran_bpjstk' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_bpjstk($idPembayaran),

				// 'deposit_tunai' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(1, $idpendaftaran),
				// 'deposit_debit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(2, $idpendaftaran),
				// 'deposit_kredit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(3, $idpendaftaran),
				// 'deposit_transfer' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(4, $idpendaftaran),
				// 'deposit_all' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit_all($idpendaftaran),

				// 'list_kontraktor' => $this->Trawatinap_tindakan_model->list_kontraktor($idpendaftaran),
			// ];

			// $data['pembayaran_non_kontraktor'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran_non_kontraktor($idPembayaran);
			// $data['pembayaran_excess'] = ($data['pembayaran_kontraktor'] && ($data['pembayaran_non_kontraktor'] || $data['deposit_all']) ? 1 : 0);

			$data['idPembayaran'] = $idPembayaran;
			$data['error'] = '';
			$data['title'] = 'Verifikasi Tindakan Rawat Inap';
			$data['totaldeposit'] = $totaldeposit;
			// print_r($totaldeposit);exit;

			$data['list_bank'] = $this->Trawatinap_tindakan_model->getBank();
			
			if ($pembayaran) {
				$data['status_verifikasi'] = DMYFormat($pembayaran->status_verifikasi);
				$data['tanggal_pembayaran'] = DMYFormat($pembayaran->tanggal);
				$data['waktu_pembayaran'] = HISTimeFormat($pembayaran->tanggal);
				$data['total_pembayaran'] = $this->Trawatinap_tindakan_model->sum_pembayaran($pembayaran->idtindakan);
				$data['detail_pembayaran'] = $this->Tbilling_ranap_model->detail_pembayaran($pendaftaran_id);
				$data['diskon_rp'] = $pembayaran->diskonrp;
			} else {
				$data['status_verifikasi'] = 0;
				$data['tanggal_pembayaran'] = date('Y-m-d');
				$data['waktu_pembayaran'] = date('H:i:s');
				$data['total_pembayaran'] = 0;
				$data['detail_pembayaran'] = [];
				$data['diskon_rp'] = 0;
			}

			$data['bataltransaksi'] = $this->Trawatinap_tindakan_model->getCountBatalTransaksi($idpendaftaran);
			$data['historyBatal'] = $this->Tbilling_ranap_model->getHistoryBatal($idpendaftaran);
			$data['historyProsesKwitansi'] = $this->Trawatinap_tindakan_model->getHistoryProsesKwitansi($idpendaftaran);
			$data['historyProsesKwitansiImplant'] = $this->Trawatinap_tindakan_model->getHistoryProsesKwitansiImplant($idpendaftaran);
			
			
			$data = array_merge($data,$data_setting,$user_form);
		}
		
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
		$data['st_ranap']=$st_ranap;
		$data['trx_id']=$trx_id;
		$this->parser->parse('module_template', $data);
	}
	function list_pemabayaran()
	{
			$id=$this->input->post('id');
			$this->select = array();
			$from="
					(
						SELECT *FROM trawatinap_tindakan_pembayaran_detail H
						WHERE H.idtindakan='$id'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  
          $result[] =$no;
          $result[] = metodePembayaran($r->idmetode);
          $result[] =$r->keterangan;
          $result[] =number_format($r->nominal,0);
          $result[] ='';
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function proses_kwitansi(){
	    $id=$this->input->post('id');
		$data=array(
			// 'statusverifikasi' =>1,
			'st_proses' =>1,
			'generate_by' => $this->session->userdata('user_id'),
			'generate_date' => date('Y-m-d H:i:s'),

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_kwitansi',$data);
		// $this->Tverifikasi_transaksi_model->insert_jurnal_deposit($id);
		$this->print_kwitansi($id,1);
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  function list_kwitansi()
  {
	  $q="SELECT waktu_batal_kwitansi FROM setting_trx_ranap";
		$waktu_batal_kwitansi=$this->db->query($q)->row('waktu_batal_kwitansi');
		
		$id=$this->input->post('id');
		$this->select = array();
		$from="
				(
					SELECT 
					IFNULL(DATEDIFF(NOW(),
					H.tanggal),0) as umur_batal
					,M.jenis_kwitansi
					,MU.`name` as nama_user_input
					,MD.`name` as nama_hapus
					,H.* FROM trawatinap_kwitansi H
					LEFT JOIN mjenis_kwitansi M ON M.id=H.tipe_proses
					LEFT JOIN musers MU ON MU.id=H.iduser_input
					LEFT JOIN musers MD ON MD.id=H.iduserdelete
					WHERE H.idrawatinap='$id' 
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $btn_edit='';$btn_hapus='';$btn_proses='';$btn_pdf='';$btn_pengajuan_hapus='';$btn_cetak='';$btn_approval='';$btn_hapus_fix='';
			
			if ($r->status=='1'){
				
				if ($r->st_proses=='0'){
					$btn_edit='<button class="btn btn-success" title="Edit" onclick="edit_kwitansi('.$r->id.')" type="button"><i class="fa fa-pencil"></i></button>';
					if ($r->umur_batal <= $waktu_batal_kwitansi){
						$btn_hapus='<button class="btn btn-danger" title="Hapus Draft" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
					}
					
					$btn_proses='<button class="btn btn-primary" title="Generate / Proses" onclick="proses_kwitansi('.$r->id.')" type="button"><i class="fa fa-file-pdf-o"></i> GENERATE</button>';
				}else{
					if ($r->hapus_proses=='1'){
						$btn_approval='<button class="btn btn-danger" title="Lihat Proses Approval" onclick="list_user('.$r->id.')" type="button"><i class="si si-user-following"></i></button>';
						if ($r->st_approval=='1'){
							$btn_hapus_fix='<button class="btn btn-danger" title="Hapus Kwitansi" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
						}
						if ($r->st_approval=='2'){
						$btn_pdf='<a href="'.base_url().'assets/upload/kwitansi/'.$r->nokwitansi.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
							// $btn_hapus_fix='<button class="btn btn-danger" title="Hapus Deposit" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
						}
					}else{
						$btn_pdf='<a href="'.base_url().'assets/upload/kwitansi/'.$r->nokwitansi.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
						if ($r->umur_batal <= $waktu_batal_kwitansi){
							$btn_pengajuan_hapus='<button class="btn btn-danger" title="Hapus" onclick="load_user_approval('.$r->id.')" type="button"><i class="si si-ban"></i> BATAL</button>';
						}
					}
				}
				if ($r->hapus_proses){
					
				}
				$aksi='
					<div class="btn-group btn-group-xs" role="group">
						'.$btn_edit.'
						'.$btn_hapus.'
						'.$btn_proses.'
						'.$btn_pdf.'
						'.$btn_pengajuan_hapus.'
						'.$btn_cetak.'
						'.$btn_approval.'
						'.$btn_hapus_fix.'
					</div>
				';
			}else{
				if ($r->hapus_proses=='1'){
				$btn_pdf='<br><a href="'.base_url().'assets/upload/kwitansi/'.$r->nokwitansi.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-xs btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
				}
				$aksi='Deleted By : '.$r->nama_hapus.'<br>'.HumanDateLong($r->deleted_date);
			}
          $result[] =$no;
          $result[] = ($r->nama_user_input).'<br>'.HumanDateLong($r->created_date);
          $result[] =$r->jenis_kwitansi;
          $result[] =$r->sudahterimadari;
          $result[] =$r->deskripsi;
          $result[] =number_format($r->nominal,0);
		  if ($r->status=='1'){
				if ($r->hapus_proses=='1'){
					if ($r->st_approval=='0'){
						$result[] =text_warning('MENUGGU APPROVAL');
					}
					if ($r->st_approval=='2'){
						$result[] =text_danger('APPROVAL DITOLAK').'<br><br>'.text_success('TELAH DIPROSES');
					}
					if ($r->st_approval=='1'){
						$result[] =text_primary('DISETUJUI');
					}
				}else{
					$result[] =($r->st_proses=='1'?text_success('TELAH DIPROSES'):text_default('MENUNGGU DIPROSES'));
				}
		  }else{
			$result[] =text_danger('DIBATALKAN');
		  }
          $result[] =$aksi;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_user_approval()
    {
	  $id=$this->input->post('id');
	  $where='';
	  $from="(
				SELECT S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak,S.deskrpisi 
				FROM trawatinap_kwitansi H
				INNER JOIN mlogic_trx_ranap S ON S.jenis=H.tipe_proses
				INNER JOIN musers U ON U.id=S.iduser
				WHERE H.id='$id' AND S.status='1'
				ORDER BY S.step,S.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();
		// print_r($from);exit;
      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='0';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->deskrpisi;
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_user($id){
		$q="SELECT *FROM trawatinap_kwitansi_approval H WHERE H.kwitansi_id='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td>'.$r->deskrpisi.'</td>';
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';
			$content .='</tr>';

		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
  function simpan_proses_peretujuan(){
		$id=$this->input->post('id');
		
		$result=$this->Tbilling_ranap_model->simpan_proses_peretujuan($id);
		echo json_encode($result);
	}
  public function get_alasan(){
	  $id=$this->input->post('id');
	  $q="
		SELECT *FROM trawatinap_kwitansi D

			WHERE D.id='$id'
	  
	  ";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_kwitansi(){
	    $id=$this->input->post('id');
		// $this->Tverifikasi_transaksi_model->remove_jurnal_deposit($id);
	    $alasanhapus=$this->input->post('alasanhapus');
		$data=array(
			// 'statusverifikasi' =>0,
			'status' =>0,
			'alasanhapus' => $alasanhapus,
			'iduserdelete' => $this->session->userdata('user_id'),
			'deleted_date' => date('Y-m-d H:i:s'),

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_kwitansi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  public function update_selesai(){
	  $status_selesai_verifikasi=$this->input->post('status_selesai_verifikasi');
	  $idpendaftaran=$this->input->post('idpendaftaran');
	  $idPembayaran=$this->input->post('idPembayaran');
	  
	  if ($status_selesai_verifikasi=='1'){//SELESAI
			 $data=array(
				'status_selesai' => 1,
				'tanggal_selesai' => date('Y-m-d H:i:s'),
				'user_id_selesai' => $this->session->userdata('user_id'),
			  );
	  }else{//Verifikasi
			   $data=array(
					'status_selesai_verifikasi' => 1,
					'tanggal_selesai_verifikasi' => date('Y-m-d H:i:s'),
					'user_id_selesai_verifikasi' => $this->session->userdata('user_id'),
			  );
	  }
	 
	  
	  $this->db->where('id',$idpendaftaran);
	  $hasil=$this->db->update('trawatinap_pendaftaran',$data);
	  if ($status_selesai_verifikasi=='2'){//verifikasi
		  $this->Tverifikasi_transaksi_model->updateStatus($idPembayaran, 'rawatinap', '1');
		  $this->db->select("trawatinap_pendaftaran.id AS idpendaftaran,
				trawatinap_pendaftaran.nopendaftaran,
				trawatinap_pendaftaran.idpasien,
				trawatinap_pendaftaran.no_medrec,
				trawatinap_pendaftaran.title,
				trawatinap_pendaftaran.namapasien,
				trawatinap_pendaftaran.idtipe,
				trawatinap_pendaftaran.idtipepasien,
				trawatinap_pendaftaran.idkelas,
				trawatinap_pendaftaran.idruangan,
				trawatinap_pendaftaran.idbed,
				IF(trawatinap_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
				trawatinap_pendaftaran.idkelompokpasien AS idkelompok,
				mpasien_kelompok.nama AS namakelompok,
				trawatinap_pendaftaran.idrekanan,
				mrekanan.nama AS namarekanan,
				trawatinap_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan,
				mdokterperujuk.idkategori AS idkategori_perujuk,
				mdokterperujuk_kategori.nama AS namakategori_perujuk,
				mdokterperujuk.id AS iddokter_perujuk,
				mdokterperujuk.nama AS namadokter_perujuk,
				mdokterperujuk.pajakranap AS pajak_perujuk,
				mdokterperujuk.potonganrsranap AS potonganrs_perujuk,
				tpoliklinik_pendaftaran.idtipe AS idasalpasien,
				tpoliklinik_pendaftaran.idpoliklinik AS idpoliklinik");
			$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan');
			$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik');
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
			$this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
			$this->db->join('mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
			$this->db->join('mdokter_kategori mdokterperujuk_kategori', 'mdokterperujuk_kategori.id = mdokterperujuk.idkategori', 'LEFT');
			$this->db->where('trawatinap_tindakan_pembayaran.id', $idPembayaran);
			$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
			$this->db->limit(1);
			$query = $this->db->get('trawatinap_tindakan_pembayaran');
			$row = $query->row();

			// Get Status Pembayaran Kasir Rawat Jalan
			$statusKasirRajal = $this->Trawatinap_tindakan_model->getDataPoliTindakan($row->idpendaftaran)->status_kasir;
			$dataPendaftaran = [
				'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
				'idpasien' => $row->idpasien,
				'no_medrec' => $row->no_medrec,
				'title' => $row->title,
				'namapasien' => $row->namapasien,
				'kasir_id' => $idPembayaran,
				'idkelas' => $row->idkelas,
				'idruangan' => $row->idruangan,
				'idbed' => $row->idbed,
				'idtipe' => $row->idtipe,
				'nopendaftaran' => $row->nopendaftaran,
				'idpendaftaran' => $row->idpendaftaran,
				'idtipepasien' => $row->idtipepasien,
				'idasalpasien' => $row->idasalpasien,
				'idpoliklinik' => $row->idpoliklinik,
				'jenis_pasien' => $row->jenispasien,
				'idkelompok' => $row->idkelompok,
				'namakelompok' => $row->namakelompok,
				'idrekanan' => $row->idrekanan,
				'namarekanan' => $row->namarekanan,
				'idkategori_perujuk' => $row->idkategori_perujuk,
				'namakategori_perujuk' => $row->namakategori_perujuk,
				'iddokter_perujuk' => $row->iddokter_perujuk,
				'namadokter_perujuk' => $row->namadokter_perujuk,
				'potonganrs_perujuk' => $row->potonganrs_perujuk,
				'pajak_perujuk' => $row->pajak_perujuk,
				'status_asuransi' => ($row->idkelompok != 5 ? 1 : 0),
				'status_kasir_rajal' => $statusKasirRajal,
			];
			// print_r($dataPendaftaran);exit;
			if ($row->idpendaftaran) {
				$this->Thonor_dokter_model->generateTransaksiHonorDokterRawatInap($dataPendaftaran);
			}
	  }
	  $this->output->set_output(json_encode($hasil));
  }
  public function updateCardPass(){
	  $idpendaftaran=$this->input->post('idpendaftaran');
	  $info_proses_card_pass=$this->input->post('info_proses_card_pass');
	  $tanggal_proses_card_pass=$this->input->post('tanggal_proses_card_pass');
	 
	  $data=array(
		'status_proses_card_pass' => '1',
		'info_proses_card_pass' => $info_proses_card_pass,
		'tanggal_proses_card_pass' => YMDTimeFormat($tanggal_proses_card_pass),
		'user_id_proses_card_pass' => $this->session->userdata('user_id'),
	  );
	  
	  $this->db->where('id',$idpendaftaran);
	  $hasil=$this->db->update('trawatinap_pendaftaran',$data);
	  $this->cetak_card_pass($idpendaftaran,1);
	  $this->output->set_output(json_encode($hasil));
  }
  public function updateProsesKwitansiDetail(){
	  $idrawatinap=$this->input->post('idrawatinap');
	  $idedit=$this->input->post('idedit');
	  $q="SELECT H.label_kode_kwitansi FROM setting_trx_ranap H";
	  $kodekwitansi=$this->db->query($q)->row('label_kode_kwitansi');
	  $st_proses=$this->input->post('st_proses');
	  // $tipekontraktor=$this->input->post('tipekontraktor');
	  // $idkontraktor=$this->input->post('idkontraktor');
	  $data=array(
		'idrawatinap' => $this->input->post('idrawatinap'),
		'tipe_proses' => $this->input->post('tipe_proses'),
		'tanggal' => YMDFormat($this->input->post('tanggal')),
		'sudahterimadari' => $this->input->post('sudahterimadari'),
		'deskripsi' => $this->input->post('deskripsi'),
		'iduser_input' => $this->session->userdata('user_id'),
		'status' => 1,
		'created_date' => date('Y-m-d H:i:s'),
		'st_proses' => $st_proses,
		// 'file_pdf' => $this->input->post('file_pdf'),
		'generate_by' => $this->session->userdata('user_id'),
		'generate_date' => date('Y-m-d H:i:s'),
		'st_approval' => 0,
		'hapus_proses' => 0,
		'kodekwitansi' => $kodekwitansi,
		'nominal' => $this->input->post('nominal'),
		'noregister' => $this->input->post('noregister'),
		'tipekontraktor' => $this->input->post('tipekontraktor'),
		'idkontraktor' => $this->input->post('idkontraktor'),

	  );
	  if ($idedit){
		  $this->db->where('id',$idedit);
		  $hasil=$this->db->update('trawatinap_kwitansi',$data);
		  $id=$idedit;
	  }else{
		  $hasil=$this->db->insert('trawatinap_kwitansi',$data);
		  $id=$this->db->insert_id();
	  }
	  if ($st_proses=='1'){
		  $this->print_kwitansi($id,1);
	  }
	  $this->output->set_output(json_encode($hasil));
  }
  function find_jenis_kwitansi(){
	  $q="SELECT H.st_kwitansi_exces,H.st_kwitansi_all,H.st_kwitansi_kontraktor,H.label_deskripsi_kwitansi FROM setting_trx_ranap H";
	  $data_setting=$this->db->query($q)->row();
	  $idrawatinap=$this->input->post('idrawatinap');
	   $query_kw="SELECT *FROM trawatinap_kwitansi H WHERE H.idrawatinap='$idrawatinap' AND H.`status`<> 0 ";
	  $arr_kwitansi=$this->db->query($query_kw)->result_array();
	  
	  $q="
		SELECT D.id,M.jenis_kwitansi,MK.nama as nama_proses,MR.nama as nama_rekanan,D.idmetode,D.tipekontraktor,D.idkontraktor,D.ket_cc,D.keterangan,SUM(D.nominal) as nominal 
			FROM `trawatinap_tindakan_pembayaran` H
			LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON H.id=D.idtindakan
			LEFT JOIN mjenis_kwitansi M ON M.id=D.idmetode
			LEFT JOIN mpasien_kelompok MK ON MK.id=D.tipekontraktor
			LEFT JOIN mrekanan MR ON MR.id=D.idkontraktor
			WHERE H.statusbatal='0' AND H.idtindakan='$idrawatinap'
			GROUP BY H.idtindakan,D.idmetode,D.idkontraktor
			ORDER BY M.id ASC
	  ";
	  $list_hasil=$this->db->query($q)->result();
	  $hasil='<option value="#" selected>-Pilih Jenis Kwitansi-</option>';
		if ($data_setting->st_kwitansi_all=='1'){
			$tipe_proses='0';
			$label_kwitansi='';
			$st_insert='0';
			 $data_kwitansi = array_filter($arr_kwitansi, function($var) use ($tipe_proses) { 
				return ($var['tipe_proses'] == $tipe_proses);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_kwitansi = reset($data_kwitansi);
			  if ($data_kwitansi){
				  $st_insert='1';
				if ($data_kwitansi['st_proses']=='1'){
					$label_kwitansi=' - TELAH DIPROSES';
				}else{
					$label_kwitansi=' - MENUNGGU DIPROSES';
					
				}
			  }
				  
		  $hasil .='<option value="0" data-st_insert="'.$st_insert.'" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi (All) '.$label_kwitansi.'</option>';
		}
	  foreach ($list_hasil as $row){
		  $label_kwitansi='';
		  $st_insert='0';
		  $tipe_proses=$row->idmetode;
		 $data_kwitansi = array_filter($arr_kwitansi, function($var) use ($tipe_proses) { 
			return ($var['tipe_proses'] == $tipe_proses);
		  });	
			// print_r($arr_jadwal);exit;
		  $data_kwitansi = reset($data_kwitansi);
		  if ($data_kwitansi){
			  $st_insert='1';
			if ($data_kwitansi['st_proses']=='1'){
				$label_kwitansi=' - TELAH DIPROSES';
			}else{
				$label_kwitansi=' - MENUNGGU DIPROSES';
				
			}
		  }
		  if ($row->idmetode=='8'){
			  if ($data_setting->st_kwitansi_kontraktor=='1'){
				$hasil .='<option value="'.$row->idmetode.'" data-st_insert="'.$st_insert.'" data-tipekontraktor="'.$row->tipekontraktor.'" data-idkontraktor="'.$row->idkontraktor.'">Proses '.$row->jenis_kwitansi.($row->tipekontraktor=='1'?' ('.$row->nama_rekanan.')':'').' '.$label_kwitansi.' </option>';
			  }
		  }else{
				$hasil .='<option value="'.$row->idmetode.'" data-st_insert="'.$st_insert.'" data-tipekontraktor="'.$row->tipekontraktor.'" data-idkontraktor="'.$row->idkontraktor.'">Proses '.$row->jenis_kwitansi.' '.$label_kwitansi.'</option>';
		  }
		  
	  }
	  if ($data_setting->st_kwitansi_exces=='1'){
		  $tipe_proses='9';
			$label_kwitansi='';
			$st_insert='0';
			 $data_kwitansi = array_filter($arr_kwitansi, function($var) use ($tipe_proses) { 
				return ($var['tipe_proses'] == $tipe_proses);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_kwitansi = reset($data_kwitansi);
			  if ($data_kwitansi){
				  $st_insert='1';
				if ($data_kwitansi['st_proses']=='1'){
					$label_kwitansi=' - TELAH DIPROSES';
				}else{
					$label_kwitansi=' - MENUNGGU DIPROSES';
					
				}
			  }
		  $hasil .='<option value="9" data-st_insert="'.$st_insert.'" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi Excess '.$label_kwitansi.'</option>';
	  }
	  $data['hasil']=$hasil;
	  $data_pasien=$this->db->query("SELECT *FROM trawatinap_pendaftaran H
									WHERE H.id='$idrawatinap'")->row_array();
	  $data_dek=$data_setting->label_deskripsi_kwitansi;
	$deskripsi_kwitansi_label=$this->parser->parse_string($data_dek,$data_pasien);
	$data['deskripsi_kwitansi_label']=$deskripsi_kwitansi_label;
	// print_r($data);exit;
	  $this->output->set_output(json_encode($data));
  }
  function find_edit_kwitansi(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM trawatinap_kwitansi H where id='$id'";
	  $data=$this->db->query($q)->row_array();
	  $data['tanggal']=HumanDateShort($data['tanggal']);
	  $this->output->set_output(json_encode($data));
  }
  function find_tanggal_setoran(){
	  $tanggal_trx= YMDFormat($this->input->post('tanggal'));
	  $q="SELECT * FROM `tsetoran_ri` H
			WHERE H.tanggal_trx = '$tanggal_trx' AND H.`status` IN (2,3,4)";
			// print_r($q);exit;
	  $data=$this->db->query($q)->row();
	  if ($data){
		$arr=true;
	  }else{
		$arr=false;
	  }
	  $this->output->set_output(json_encode($arr));
  }
  function find_nominal_kwitansi(){
	  $idrawatinap=$this->input->post('idrawatinap');
	  $tipekontraktor=$this->input->post('tipekontraktor');
	  $idkontraktor=$this->input->post('idkontraktor');
	  $idmetode=$this->input->post('idmetode');
	  $gt_rp=$this->input->post('gt_rp');
	  if ($idmetode=='0'){//ALLL
		  $q="
			SELECT D.id,M.jenis_kwitansi,MK.nama as nama_proses,MR.nama as nama_rekanan,D.idmetode,D.tipekontraktor,D.idkontraktor,D.ket_cc,D.keterangan,'$gt_rp' as nominal 
			FROM `trawatinap_tindakan_pembayaran` H
			LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON H.id=D.idtindakan
			LEFT JOIN mjenis_kwitansi M ON M.id=D.idmetode
			LEFT JOIN mpasien_kelompok MK ON MK.id=D.tipekontraktor
			LEFT JOIN mrekanan MR ON MR.id=D.idkontraktor
			WHERE H.statusbatal='0' AND H.idtindakan='$idrawatinap'
			
			";
	  }elseif($idmetode=='9'){
		    $q="
			SELECT D.id,M.jenis_kwitansi,MK.nama as nama_proses,MR.nama as nama_rekanan,D.idmetode,D.tipekontraktor,D.idkontraktor,D.ket_cc,D.keterangan,SUM(D.nominal) as nominal 
			FROM `trawatinap_tindakan_pembayaran` H
			LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON H.id=D.idtindakan
			LEFT JOIN mjenis_kwitansi M ON M.id=D.idmetode
			LEFT JOIN mpasien_kelompok MK ON MK.id=D.tipekontraktor
			LEFT JOIN mrekanan MR ON MR.id=D.idkontraktor
			WHERE H.statusbatal='0' AND H.idtindakan='$idrawatinap' AND D.idmetode IN (1,2,3,4)
			
			";
	  }else{
		   $q="
			SELECT D.id,M.jenis_kwitansi,MK.nama as nama_proses,MR.nama as nama_rekanan,D.idmetode,D.tipekontraktor,D.idkontraktor,D.ket_cc,D.keterangan,SUM(D.nominal) as nominal 
			FROM `trawatinap_tindakan_pembayaran` H
			LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON H.id=D.idtindakan
			LEFT JOIN mjenis_kwitansi M ON M.id=D.idmetode
			LEFT JOIN mpasien_kelompok MK ON MK.id=D.tipekontraktor
			LEFT JOIN mrekanan MR ON MR.id=D.idkontraktor
			WHERE H.statusbatal='0' AND H.idtindakan='$idrawatinap' AND D.idmetode='$idmetode' AND D.tipekontraktor='$tipekontraktor' AND D.idkontraktor='$idkontraktor'
			
			";
	  }
	 
	  $data=$this->db->query($q)->row_array();
	  if ($idmetode=='9'){
		  $totaldeposit = $this->Tbilling_ranap_model->getTotalDeposit($idrawatinap)->totaldeposit;
		 $nominal=$data['nominal'] + $totaldeposit;
		 $data['nominal']=$nominal;
	  }
	// print_r($data);exit;
	  $this->output->set_output(json_encode($data));
  }
  public function print_kwitansi($id,$st_create='0')
	{
		$dompdf = new Dompdf();
		$q="SELECT * FROM setting_trx_ranap ";
		$data_setting=$this->db->query($q)->row_array();
		$q="SELECT * FROM setting_trx_ranap_label_kwitansi ";
		$data_settin_label=$this->db->query($q)->row_array();
		$row = $this->Tbilling_ranap_model->getDetailKwitansi($id);
		
		$data = array_merge($row,$data_setting,$data_settin_label);
		$html = $this->load->view('Tpendaftaran_poli_ttv/erm_trx/section_pembayaran/kwitansi', $data, true);
		// print_r($html);exit;
		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		if ($st_create=='1'){
			$nama_file=$data['nokwitansi'].'.pdf';
			$uploadDir_asset = './assets/upload/kwitansi/';
			$file_hapus=$uploadDir_asset.$nama_file;
			// unlink($file_hapus);
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
			if ($data['st_proses']=='1'){
				$this->kirim_email($id);
			}
		}else{
			// $dompdf->stream('Kwitansi.pdf', ['Attachment' => 0]);
			$dompdf->stream($data['judul_kwitansi_ina'].'.pdf', array("Attachment"=>0));
			
		}
	}
	public function cetak_card_pass($id,$st_create='0')
	{
		$dompdf = new Dompdf();
		$q="SELECT * FROM setting_trx_ranap ";
		$data_setting=$this->db->query($q)->row_array();
		$q="SELECT * FROM setting_trx_ranap_label_card ";
		$data_settin_label=$this->db->query($q)->row_array();
		$row = $this->Tbilling_ranap_model->getDetailPasien($id);
		
		$data = array_merge($row,$data_setting,$data_settin_label);
		$html = $this->load->view('Tpendaftaran_poli_ttv/erm_trx/section_pembayaran/card_pass', $data, true);
		$dompdf->loadHtml($html);
		// print_r($st_create);exit;
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		if ($st_create=='1'){
			$nama_file='CP_'.$row['nopendaftaran'].'.pdf';
			$uploadDir_asset = './assets/upload/kwitansi/';
			$file_hapus=$uploadDir_asset.$nama_file;
			// unlink($file_hapus);
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
			$this->kirim_email_card($id);
		}else{
			// $dompdf->stream('Kwitansi.pdf', ['Attachment' => 0]);
			$dompdf->stream($data['judul_card_ina'].'.pdf', array("Attachment"=>0));
			
		}
	}
	public function kirim_email_card($id)
    {
		$q="SELECT email_card_pass FROM setting_trx_ranap";
		$list_email=$this->db->query($q)->row('email_card_pass');
		
        $emailArray = explode(',', $list_email);
		$q="
			SELECT H.judul_card_ina as judul
			,CONCAT('<p>','Kepada Yth, Rekan Sejawat <br>Terlampir Card Pass, Atas Perhatiannya Kami ucapkan terima kasih</p>') as bodi
			,'RSKB Halmahera Siaga' as footer_email FROM setting_trx_ranap_label_card H LIMIT 1
		";
		$pengaturan_email=$this->db->query($q)->row_array();
		// $this->output->set_output(json_encode($hasil));
        $q="SELECT *FROM trawatinap_pendaftaran WHERE id='$id'";
		
        $data_transaksi = $this->db->query($q)->row();
      
       $nama_file='CP_'.$data_transaksi->nopendaftaran.'.pdf';
		$uploadDir_asset = './assets/upload/kwitansi/';
		$mail = new PHPMailer(true);
		$file_hapus=$uploadDir_asset.$nama_file;
		try {
			// Server settings
			$this->configureEmail($mail);

			// Recipients
			$mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
			foreach ($emailArray as $email) {
				$mail->addAddress($email, $data_transaksi->nopendaftaran);
			}
			$mail->addAttachment($uploadDir_asset.$nama_file);


			// Content
			$bodyHTML = $pengaturan_email['bodi'];

			$mail->isHTML(true);
			$mail->Subject = strip_tags($pengaturan_email['judul']).' '.$data_transaksi->nopendaftaran;
			$mail->Body = $bodyHTML;

			$mail->send();

			$response = [
				'status' => true,
				'message' => 'success',
			];
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		} catch (Exception $e) {
			$response = [
				'status' => false,
				'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
			];

			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		}
	 }
	public function kirim_email($id)
    {
		$q="SELECT email_generate_kwitansi FROM setting_trx_ranap";
		$list_email=$this->db->query($q)->row('email_generate_kwitansi');
		
        $emailArray = explode(',', $list_email);
		$q="
			SELECT H.judul_kwitansi_ina as judul
			,CONCAT('<p>','Kepada Yth, Rekan Sejawat <br>Terlampir Kwitansi, Atas Perhatiannya Kami ucapkan terima kasih</p>') as bodi
			,'RSKB Halmahera Siaga' as footer_email FROM setting_trx_ranap_label_kwitansi H LIMIT 1
		";
		$pengaturan_email=$this->db->query($q)->row_array();
		// $this->output->set_output(json_encode($hasil));
        $q="SELECT *FROM trawatinap_kwitansi WHERE id='$id'";
		
        $data_transaksi = $this->db->query($q)->row();
      
       $nama_file=$data_transaksi->nokwitansi.'.pdf';
		$uploadDir_asset = './assets/upload/kwitansi/';
		$mail = new PHPMailer(true);
		$file_hapus=$uploadDir_asset.$nama_file;
		try {
			// Server settings
			$this->configureEmail($mail);

			// Recipients
			$mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
			foreach ($emailArray as $email) {
				$mail->addAddress($email, $data_transaksi->nokwitansi);
			}
			$mail->addAttachment($uploadDir_asset.$nama_file);


			// Content
			$bodyHTML = $pengaturan_email['bodi'];

			$mail->isHTML(true);
			$mail->Subject = strip_tags($pengaturan_email['judul']).' '.$data_transaksi->nokwitansi;
			$mail->Body = $bodyHTML;

			$mail->send();

			$response = [
				'status' => true,
				'message' => 'success',
			];
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		} catch (Exception $e) {
			$response = [
				'status' => false,
				'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
			];

			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		}
	 }
	  private function configureEmail($mail)
    {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $mail->Host = 'mail.halmaherasiaga.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'erm@halmaherasiaga.com';
        $mail->Password = '{T,_Xun}9{@5';
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port = 465;
    }
	public function prosesBatalValidasi()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$ruangranap = json_decode($this->input->post('ruangranap'));
		$admranap = json_decode($this->input->post('admranap'));

		$this->db->where('id', $idpendaftaran);
		$this->db->set('statusvalidasi', 0);
		$this->db->set('statustransaksi', 0);
		if ($this->db->update('trawatinap_pendaftaran')) {
			$this->db->where('idrawatinap',$idpendaftaran);
			$this->db->delete('trawatinap_ruangan');
			
			$this->db->where('idpendaftaran',$idpendaftaran);
			$this->db->delete('trawatinap_administrasi');
			
			return true;
		} else {
			return false;
		}
	}
	public function prosesTransaksi(){
		$idpendaftaran = $this->input->post('idpendaftaran');

		$this->db->where('id', $idpendaftaran);
		$this->db->set('statuskasir', 1);
		$this->db->set('statustransaksi', 1);
		$this->db->set('user_id_statusbayar', $this->session->userdata('user_id'));
		$this->db->set('tanggal_statusbayar', date('Y-m-d H:i:s'));
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}
	public function prosesKembaliValidasi(){
		$idpendaftaran = $this->input->post('idpendaftaran');

		$this->db->where('id', $idpendaftaran);
		$this->db->set('statuskasir', 0);
		$this->db->set('statustransaksi', 0);
		$this->db->set('user_id_statusbayar', null);
		$this->db->set('tanggal_statusbayar', null);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}
	public function updateProsesKwitansi()
	{
		$idpendaftaran = $this->input->post('id');

		$this->db->where('id', $idpendaftaran);
		$this->db->set('status_proses_kwitansi', 1);
		$this->db->set('user_id_proses_kwitansi', $this->session->userdata('user_id'));
		$this->db->set('tanggal_proses_kwitansi', date('Y-m-d H:i:s'));
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}
	public function simpan_transaksi()
	{
		return $this->Tbilling_ranap_model->simpan_transaksi();
	}
	public function prosesValidasi()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$ruangranap = json_decode($this->input->post('ruangranap'));
		$admranap = json_decode($this->input->post('admranap'));
		$data_update=array(
			'statusvalidasi' => 1,
			'tanggal_statusvalidasi' => date('Y-m-d H:i:s'),
			'user_id_statusvalidasi' => $this->session->userdata('user_id'),
		  );
		$this->db->where('id', $idpendaftaran);
		// $this->db->set('statusvalidasi', 1);
		if ($this->db->update('trawatinap_pendaftaran',$data_update)) {
			foreach ($ruangranap as $row) {
				$data = [];
				$data['idrawatinap'] = $row[9];
				$data['idtarif'] = $row[10];
				$data['namatarif'] = $row[11];
				$data['idruangan'] = $row[12];
				$data['idkelas'] = $row[13];
				$data['tanggaldari'] = $row[14];
				$data['tanggalsampai'] = $row[15];
				$data['jumlahhari'] = RemoveComma($row[16]);
				$data['jasasarana'] = RemoveComma($row[17]);
				$data['jasapelayanan'] = RemoveComma($row[18]);
				$data['bhp'] = RemoveComma($row[19]);
				$data['biayaperawatan'] = RemoveComma($row[20]);
				$data['total'] = RemoveComma($row[21]);
				$data['totalkeseluruhan'] = RemoveComma($row[7]);

				$this->db->insert('trawatinap_ruangan', $data);
			}

			foreach ($admranap as $row) {
				$data = [];
				$data['idpendaftaran'] = $row[7];
				$data['idadministrasi'] = $row[8];
				$data['namatarif'] = $row[9];
				$data['jasasarana'] = RemoveComma($row[10]);
				$data['jasapelayanan'] = RemoveComma($row[11]);
				$data['bhp'] = RemoveComma($row[12]);
				$data['biayaperawatan'] = RemoveComma($row[13]);
				$data['total'] = RemoveComma($row[14]);
				$data['mintarif'] = RemoveComma($row[15]);
				$data['maxtarif'] = RemoveComma($row[16]);
				$data['persentasetarif'] = RemoveComma($row[17]);
				$data['tarif'] = RemoveComma($row[18]);
				$data['tarifsetelahdiskon'] = RemoveComma($row[18]);

				$this->db->insert('trawatinap_administrasi', $data);
			}

			return true;
		} else {
			return false;
		}
	}
	public function upload_pembayaran() {
       $uploadDir = './assets/upload/deposit/';
		if (!empty($_FILES)) {
			 $transaksi_id = $this->input->post('idupload_pembayaran');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['transaksi_id'] 	= $transaksi_id;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('trawatinap_tindakan_pembayaran_detail_upload', $detail);
		}
    }
	function refresh_image_pembayaran($id){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT  H.* from trawatinap_tindakan_pembayaran_detail_upload H

			WHERE H.transaksi_id='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/deposit/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			if (UserAccesForm($user_acces_form,array('2613'))){
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file_pembayaran" type="button" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			}else{
				
			$tabel .='<td class="text-left"></td>';
			}
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		$arr['detail']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function hapus_file_pembayaran(){
		$id=$this->input->post('id');
		$q="select file_name from trawatinap_tindakan_pembayaran_detail_upload where id='$id'
			";
		$row = $this->db->query($q)->row();
		if(file_exists('./assets/upload/deposit/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/deposit/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from trawatinap_tindakan_pembayaran_detail_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	public function upload_card_pass() {
       $uploadDir = './assets/upload/deposit/';
		if (!empty($_FILES)) {
			 $transaksi_id = $this->input->post('idupload_card');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['transaksi_id'] 	= $transaksi_id;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('trawatinap_tindakan_card_pass_upload', $detail);
		}
    }
	function refresh_image_card_pass($id){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT  H.* from trawatinap_tindakan_card_pass_upload H

			WHERE H.transaksi_id='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/deposit/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			if (UserAccesForm($user_acces_form,array('2613'))){
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file_card_pass" type="button" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			}else{
				
			$tabel .='<td class="text-left"></td>';
			}
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		$arr['detail']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function hapus_file_card_pass(){
		$id=$this->input->post('id');
		$q="select file_name from trawatinap_tindakan_card_pass_upload where id='$id'
			";
		$row = $this->db->query($q)->row();
		if(file_exists('./assets/upload/deposit/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/deposit/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from trawatinap_tindakan_card_pass_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	public function prosesBatalTransaksi()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$alasanbatal = $this->input->post('alasanbatal');
		$alasan_batal_transaksi = $this->input->post('alasan_batal_transaksi');

		$data = [
			'statusbatal' => 1,
			'tanggalbatal' => date('Y-m-d H:i:s'),
			'alasanbatal' => $alasanbatal,
			'alasan_batal_transaksi' => $alasan_batal_transaksi,
			'iduser_batal' => $this->session->userdata('user_id'),
		];

		$this->db->where('idtindakan', $idpendaftaran);
		$this->db->where('statusbatal', 0);
		if ($this->db->update('trawatinap_tindakan_pembayaran', $data)) {
			$data = [
				'statuskasir' => 1,
				'statustransaksi' => 0,
				'statuspembayaran' => 0,
			];
			$this->db->where('id', $idpendaftaran);
			$this->db->update('trawatinap_pendaftaran', $data);
			return true;
		} else {
			return false;
		}
	}
	
}
