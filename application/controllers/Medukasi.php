<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medukasi extends CI_Controller {

	/**
	 * Master Edukasi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Medukasi_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Edukasi';
		$data['content'] 		= 'Medukasi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Edukasi",'#'),
									    			array("List",'medukasi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'judul' 					=> '',
			'isi' 		=> '',
			'isi_header' 		=> '',
			'idtipe' 		=> '',
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Edukasi';
		$data['content'] 		= 'Medukasi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Edukasi",'#'),
									    			array("Tambah",'medukasi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Medukasi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'judul' 					=> $row->judul,
					'isi' 		=> $row->isi,
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Edukasi';
				$data['content']	 	= 'Medukasi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Edukasi",'#'),
											    			array("Ubah",'medukasi')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('medukasi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('medukasi');
		}
	}
	
	function delete($id){
		$this->Medukasi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('medukasi','location');
	}

	function save(){
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Medukasi_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('medukasi/update/'.$id,'location');
				}
			} else {
				if($this->Medukasi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('medukasi/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Medukasi/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Edukasi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Edukasi",'#'),
															array("Tambah",'medukasi')
													);
		}else{
			$data['title'] = 'Ubah Master Edukasi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Edukasi",'#'),
															array("Ubah",'medukasi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'medukasi';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'judul' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('judul');
      $this->column_order    = array('judul');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->judul;
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('1648'))){
            $aksi .= '<a href="'.site_url().'medukasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('1649'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'medukasi" data-urlremove="'.site_url().'medukasi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/medukasi_dokumen/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'medukasi_dokumen';

                    $data = array();
                    $data['idtransaksi']  = $this->input->post('idtransaksi');
                    $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->Medukasi_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	 public function delete_file()
    {
		$idfile=$this->input->post('id');
        $this->db->where('id', $idfile);
        $query = $this->db->get('medukasi_dokumen');
        $row = $query->row();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $idfile);
            if ($this->db->delete('medukasi_dokumen')) {
                if (file_exists('./assets/upload/medukasi_dokumen/'.$row->filename) && $row->filename !='') {
                    unlink('./assets/upload/medukasi_dokumen/'.$row->filename);
                }
            }
        }
		$this->output->set_output(json_encode(true));
    }
}
