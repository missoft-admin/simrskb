<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkelompok_tindakan extends CI_Controller {

	/**
	 * Kelompok Tindakan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mkelompok_tindakan_model');
  }

	function index(){
		
		$data = array(
			'id' 						=> '',
			'lokasi_tubuh_id' 					=> '#',
			'kelompok_operasi_id' 			=> '#',
			'status_aktif' 			=> '#',
			'status' 				=> '1'
		);
		$data['lokasi_tubuh_list'] = $this->mkelompok_tindakan_model->lokasi_tubuh_list();
		$data['kelompok_operasi_list'] = $this->mkelompok_tindakan_model->kelompok_operasi_list();
		$data['error'] 			= '';
		$data['title'] 			= 'Kelompok Tindakan';
		$data['content'] 		= 'Mkelompok_tindakan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Kelompok Tindakan",'#'),
									    			array("List",'Mkelompok_tindakan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'lokasi_tubuh_id' 					=> '#',
			'kelompok_operasi_id' 			=> '#',
			'status' 				=> '1'
		);

		$data['lokasi_tubuh_list'] = $this->mkelompok_tindakan_model->lokasi_tubuh_list();
		$data['kelompok_operasi_list'] = $this->mkelompok_tindakan_model->kelompok_operasi_list();
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Kelompok Tindakan';
		$data['content'] 		= 'Mkelompok_tindakan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Kelompok Tindakan",'#'),
									    			array("Tambah",'Mkelompok_tindakan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function js_icd()
	{
		$arr = $this->mkelompok_tindakan_model->js_icd();
		echo json_encode($arr);
	}
	function simpan_add()
	{
		$data=array(
			'kelompok_tindakan_id'=>$this->input->post('id'),
			'icd9_id'=>$this->input->post('icd_id'),
			'jenis_id'=>$this->input->post('jenis_id')
			
		);
		
       // print_r($data);
		$result = $this->db->insert('mkelompok_tindakan_icd',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function simpan_edit()
	{
		$data=array(
			'kelompok_tindakan_id'=>$this->input->post('id'),
			'icd9_id'=>$this->input->post('icd_id'),
			'jenis_id'=>$this->input->post('jenis_id')
			
		);
		$this->db->where('id',$this->input->post('tedit'));
		
       // print_r($data);
		$result = $this->db->update('mkelompok_tindakan_icd',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function hapus_icd()
	{
		$this->db->where('id',$this->input->post('tedit'));		
       // print_r($data);
		$result = $this->db->delete('mkelompok_tindakan_icd');
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function load_icd()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *FROM (
				SELECT H.id,I.deskripsi,I.kode,H.icd9_id,H.jenis_id,CASE WHEN H.jenis_id='1' THEN 'Primary' ELSE 'Secondary' END as jenis from mkelompok_tindakan_icd H
				INNER JOIN icd_9 I ON I.id=H.icd9_id 
				WHERE H.kelompok_tindakan_id='$id' ORDER BY H.jenis_id,H.id ASC
				) as T1) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('jenis','deskripsi','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->kode.'-'.$r->deskripsi;
            $row[] = $r->jenis;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus" title="Hapus"><i class="fa fa-close"></i></button>';
				
			
			$aksi.='</div>';
			
			$row[] = $aksi;			
            $row[] = $r->id;
            $row[] = $r->icd9_id;
            $row[] = $r->jenis_id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function update($id){
		
		if($id != ''){
			$row = $this->mkelompok_tindakan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'kelompok_operasi_id' 					=> $row->kelompok_operasi_id,
					'status' 				=> $row->status
				);
				$data['lokasi_tubuh_list'] = $this->mkelompok_tindakan_model->lokasi_tubuh_list();
				$data['kelompok_operasi_list'] = $this->mkelompok_tindakan_model->kelompok_operasi_list();
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Kelompok Tindakan';
				$data['content']	 	= 'Mkelompok_tindakan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Kelompok Tindakan",'#'),
											    			array("Ubah",'mkelompok_tindakan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mkelompok_tindakan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mkelompok_tindakan');
		}
	}

	function delete($id){
		// print_r($id);exit();
		$this->mkelompok_tindakan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mkelompok_tindakan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->mkelompok_tindakan_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkelompok_tindakan/update/'.$id,'location');
				}
			} else {
				if($this->mkelompok_tindakan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkelompok_tindakan/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mkelompok_tindakan/manage';

		if($id==''){
			$data['title'] = 'Tambah Kelompok Tindakan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kelompok Tindakan",'#'),
															array("Tambah",'Mkelompok_tindakan')
													);
		}else{
			$data['title'] = 'Ubah Kelompok Tindakan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kelompok Tindakan",'#'),
															array("Ubah",'Mkelompok_tindakan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
		$status_aktif     = $this->input->post('status_aktif');
		$kelompok_operasi_id     = $this->input->post('kelompok_operasi_id');
		
		$iduser=$this->session->userdata('user_id');
		
		$where='';
		if ($status_aktif !='#'){
			if ($status_aktif=='1') {
				$where .=" AND iddet IS NOT NULL";				
			}else{
				$where .=" AND iddet IS NULL";					
			}
		}
		if ($kelompok_operasi_id !='#'){			
			$where .=" AND kelompok_operasi_id ='$kelompok_operasi_id'";					
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$from="(SELECT *FROM (SELECT *FROM (SELECT H.id,H.nama,
					H.kelompok_operasi_id,
					K.nama as kel_operasi,H.status,GROUP_CONCAT(D.id)  as iddet FROM mkelompok_tindakan H
					LEFT JOIN mkelompok_operasi K ON K.id=H.kelompok_operasi_id
					LEFT JOIN mkelompok_tindakan_icd D ON D.kelompok_tindakan_id=H.id
					WHERE H.`status`='1'
					GROUP BY H.id
					ORDER BY H.id DESC) as T1
					WHERE id <> '' ".$where."
					) as T1) as tbl";
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama','kel_operasi');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->kel_operasi;
		  $status='';
		  if ($r->iddet){
			  $status='<span class="label label-success">AKTIF</span>';
		  }else{
			$status='<span class="label label-danger">TIDAK AKTIF</span>';
		  }
          $row[] = $status;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('295'))){
          	$aksi .= '<a href="'.site_url().'mkelompok_tindakan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('296'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mkelompok_tindakan" data-urlremove="'.site_url().'mkelompok_tindakan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
				  
		
  }
	function ajaxSave(){
		if ($this->mkelompok_tindakan_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
