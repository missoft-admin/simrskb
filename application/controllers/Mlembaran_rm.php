<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlembaran_rm extends CI_Controller {

	/**
	 * Master Lembaran controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mlembaran_rm_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Lembaran';
		$data['content'] 		= 'Mlembaran_rm/index';
		$data['breadcrum'] 	= array(
			array("RSKB Halmahera",'#'),
			array("Master Lembaran",'mlembaran_rm/index'),
			array("List",'#')
		);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' => '',
			'nama' => '',
			'referensi' => array(),
			'kriteria' => array(),
			'detail' => array(),
			'deskripsi' => '',
			'status' => '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Lembaran';
		$data['content'] 		= 'Mlembaran_rm/manage';
		$data['breadcrum']	= array(
			array("RSKB Halmahera",'#'),
			array("Master Lembaran",'#'),
			array("Tambah",'mlembaran_rm')
		);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id) {
		if($id != ''){
			$row = $this->Mlembaran_rm_model->getSpecified($id);
			if(isset($row->id)) {
				$data = array(
					'id' => $row->id,
					'nama' => $row->nama,
					'referensi' => jsonDecode($row->referensi),
					'kriteria' => jsonDecode($row->kriteria),
					'detail' => jsonDecode($row->detail),
					'deskripsi' => $row->deskripsi,
					'status' => $row->status,
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Lembaran';
				$data['content']	 	= 'Mlembaran_rm/manage';
				$data['breadcrum'] 	= array(
					array("RSKB Halmahera",'#'),
					array("Master Lembaran",'#'),
    			array("Ubah",'mlembaran_rm')
				);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mlembaran_rm','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mlembaran_rm','location');
		}
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mlembaran_rm_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlembaran_rm/index/'.$this->input->post('idtipe'),'location');
				}
			} else {
				if($this->Mlembaran_rm_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlembaran_rm/index/'.$this->input->post('idtipe'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mlembaran_rm/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Lembaran';
			$data['breadcrum'] = array(
					array("RSKB Halmahera",'#'),
					array("Master Lembaran",'#'),
					array("Tambah",'mlembaran_rm')
			);
		}else{
			$data['title'] = 'Ubah Master Lembaran';
			$data['breadcrum'] = array(
					array("RSKB Halmahera",'#'),
					array("Master Lembaran",'#'),
					array("Ubah",'mlembaran_rm')
			);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function delete($id){
		$this->Mlembaran_rm_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mlembaran_rm','location');
	}

	function getIndex()
	{
			$this->select = array('*');
			$this->from   = 'mlembaran_rm';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array();

			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$referensi = str_replace("_", " ", "<span class='label label-primary'>".implode("</span>&nbsp;<span class='label label-primary'>", array_map('strtoupper', jsonDecode($r->referensi)))."</span>");

					$row[] = $no;
					$row[] = $r->nama;
					$row[] = $referensi;
					$row[] = $r->deskripsi;
					$row[] = '<div class="btn-group">
						<a href="'.site_url().'mlembaran_rm/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
						<a href="#" data-urlindex="'.site_url().'mlembaran_rm" data-urlremove="'.site_url().'mlembaran_rm/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
					</div>';

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
