<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Mheader_surat extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mheader_surat_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2246'))){
			$data=array();
			$data = $this->Mheader_surat_model->get_data();
			$data['jenis_isi'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Header Surat';
			$data['content'] 		= 'Mheader_surat/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("General Consist",'mheader_surat')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save(){
		if ($this->Mheader_surat_model->save()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mheader_surat','location');
		}
	}
	
}
