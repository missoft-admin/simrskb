<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Mnomorakuntansi extends CI_Controller
{
	/**
	 * No. Akuntansi controller.
	 * Developer @gunalirezqimauludi
	 */

	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mnomorakuntansi_model', 'model');
		$this->load->model('Mkategori_akun_model');
	}

	public function index()
	{
        $data = [
			'kategoriakun' => '',
			'headerakun' => '',
			'nomorakun' => '',
			'status' => '',
			'jikabertambah' => '',
			'jikaberkurang' => '',
        ];

		$data['error'] = '';
		$data['title'] = 'No. Akuntansi';
		$data['content'] = 'Mnomorakuntansi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['No. Akuntansi', 'Mnomorakuntansi'],
			['List', '']
		];

		$data['list_index'] = $this->model->getAll();
		$data['list_header'] = $this->Mkategori_akun_model->list_header();
		$data['list_akun'] = $this->Mkategori_akun_model->list_akun();
		$data['list_kategori'] = $this->Mkategori_akun_model->list_kategori();
		// print_r($data['list_akun']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_index()
    {
		
		$idkategori     		= $this->input->post('idkategori');
		$status     		= $this->input->post('status');
		$bertambah     		= $this->input->post('bertambah');
		$berkurang     		= $this->input->post('berkurang');
		$headerakun     	= $this->input->post('headerakun');
		if ($headerakun){
			$headerakun=implode(',', $headerakun);			
		}
		$noakun     	= $this->input->post('noakun');
		if ($noakun){
			$noakun=implode(',', $noakun);			
		}
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
        if ($bertambah !='#'){
			$where .=" AND makun_nomor.bertambah='$bertambah'";
		}
		if ($berkurang !='#'){
			$where .=" AND makun_nomor.berkurang='$berkurang'";
		}
		if ($status !='#'){
			$where .=" AND makun_nomor.status='$status'";
		}
		if ($idkategori !='#'){
			$where .=" AND makun_nomor.idkategori='$idkategori'";
		}
        if ($headerakun){
			$where .=" AND makun_nomor.noheader IN (".$headerakun.")";
		}
		if ($noakun){
			$where .=" AND makun_nomor.id IN (".$noakun.")";
		}
		$from="(SELECT
                makun_nomor.id,
                CONCAT( akun_header.noakun, ' - ', akun_header.namaakun ) AS noheader,
                makun_nomor.noakun,
                makun_nomor.namaakun,
                makun_nomor.level,
                mkategori_akun.nama AS namakategori,
                makun_nomor.status,
                makun_nomor.saldo,COUNT(A.id) as jml_anak
            FROM
                makun_nomor
                LEFT JOIN makun_nomor akun_header ON akun_header.noakun = makun_nomor.noheader 
                LEFT JOIN makun_nomor A ON A.noheader = makun_nomor.noakun AND A.status='1'
                LEFT JOIN mkategori_akun mkategori_akun ON mkategori_akun.id = makun_nomor.idkategori 
            WHERE
                makun_nomor.status  IS NOT NULL ".$where."
            GROUP BY makun_nomor.id
            ORDER BY
                makun_nomor.noakun ASC
				) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('noakun','namaakun','namakategori');
        $this->column_order    = array();
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
			
            $row[] = $r->noheader;
            $row[] = $r->noakun;
            $row[] = TreeView($r->level, $r->namaakun);
            $row[] = $r->namakategori;
            $row[] =  StatusRow($r->status);
            $row[] = number_format($r->saldo,2);
			$aksi   = '<div class="btn-group">';
			$aksi .= '<a href="'.base_url().'mnomorakuntansi/view/'.$r->id.'" class="btn btn-sm btn-info" data-toggle="tooltip" title="Lihat Akun"><i class="fa fa-eye"></i></a>';
			if ($r->status=='1'){				
				$aksi .= '<a href="'.base_url().'mnomorakuntansi/edit/'.$r->id.'" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah Akun"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a href="'.base_url().'mnomorakuntansi/detail/'.$r->id.'" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Rincian Transaksi Akun"><i class="fa fa-list"></i></a>';
				if ($r->jml_anak==0){				
					// $aksi .= '<button class="btn btn-sm btn-success" data-toggle="tooltip" onclick="atur_saldo('.$r->id.')" title="Pengaturan Saldo"><i class="fa fa-gear"></i></button>';
				}
					$aksi .= '<a href="'.base_url().'mnomorakuntansi/remove/'.$r->id.'" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a>';
				$aksi .= '<a href="'.base_url().'mnomorakuntansi/print/'.$r->id.'" class="btn btn-sm btn-success" data-toggle="tooltip" title="Cetak Akun"><i class="fa fa-print"></i></a>';
			}else{
				$aksi .= '<a href="'.base_url().'mnomorakuntansi/aktifkan/'.$r->id.'" class="btn btn-sm btn-success" data-toggle="tooltip" title="Aktifkan"><i class="fa fa-check"></i> Aktifkan</a>';
			}
			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function export()
    {
		// print_r($this->input->post());exit();
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
		$data=array(
			'kategori' =>'Semua Kategori',
			'status_nama' =>'Semua',
			'header_nama' =>'Semua',
			'akun_nama' =>'Semua',
		);
		
		$idkategori     		= $this->input->post('idkategori');
		$status     		= $this->input->post('status');
		$bertambah     		= $this->input->post('bertambah');
		$berkurang     		= $this->input->post('berkurang');
		$headerakun     	= $this->input->post('headerakun');
		if ($headerakun){
			$headerakun=implode(',', $headerakun);			
		}
		$noakun     	= $this->input->post('noakun');
		if ($noakun){
			$noakun=implode(',', $noakun);			
		}
		$where='';
		
		if ($bertambah !='#'){
			$where .=" AND makun_nomor.bertambah='$bertambah'";
		}
		if ($berkurang !='#'){
			$where .=" AND makun_nomor.berkurang='$berkurang'";
		}
		if ($status !='#'){
			$data['status_nama']=($status=='1'?'ACTIVE':'NOT ACTIVE');
			$where .=" AND makun_nomor.status='$status'";
		}
		if ($idkategori !='#'){
			$data['kategori']=$this->model->get_kategori($idkategori);
			$where .=" AND makun_nomor.idkategori='$idkategori'";
		}
        if ($headerakun){
			$data['header_nama']=$this->model->get_header($headerakun);
			$where .=" AND makun_nomor.noheader IN (".$headerakun.")";
		}
		if ($noakun){
			$data['akun_nama']=$this->model->get_akun($noakun);
			$where .=" AND makun_nomor.id IN (".$noakun.")";
		}
        $from = "
					SELECT
					makun_nomor.id,
					CONCAT( akun_header.noakun, ' - ', akun_header.namaakun ) AS noheader,
					makun_nomor.noakun,
					makun_nomor.namaakun,
					makun_nomor.level,
					mkategori_akun.nama AS namakategori,
					makun_nomor.status,
					makun_nomor.saldo,COUNT(A.id) as jml_anak
				FROM
					makun_nomor
					LEFT JOIN makun_nomor akun_header ON akun_header.noakun = makun_nomor.noheader 
					LEFT JOIN makun_nomor A ON A.noheader = makun_nomor.noakun AND A.status='1'
					LEFT JOIN mkategori_akun mkategori_akun ON mkategori_akun.id = makun_nomor.idkategori 
				WHERE
					makun_nomor.status  IS NOT NULL ".$where."
				GROUP BY makun_nomor.id
				ORDER BY
					makun_nomor.noakun ASC
				";
		// print_r($from);exit();
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($from)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail,$data);
		}
		if ($btn=='2'){
			// print_r($data);exit();
			$this->pdf($row_detail,$data);
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$data);
		}
    }
	public function pdf($row_detail,$row){
		// print_r($row_detail);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$row);
        
        $data['detail'] = $row_detail;
		// print_r($data);exit();
		$data = array_merge($data, backend_info());

        $html = $this->load->view('Mnomorakuntansi/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('No. Akuntansi.pdf', array("Attachment"=>0));
	}
	function excel($row_detail,$data){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('No. Akuntansi');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);
		
		$activeSheet->setCellValue('B5', "KATEGORI ");
		$activeSheet->setCellValue('C5', ": ".$data['kategori']);
        $activeSheet->setCellValue('B6', "AKUN HEADER ");
		$activeSheet->setCellValue('C6', ": ".$data['header_nama']);
        $activeSheet->setCellValue('B7', "NO AKUN ");
		$activeSheet->setCellValue('C7', ": ".$data['akun_nama']);
        $activeSheet->setCellValue('B8', "STATUS ");
		$activeSheet->setCellValue('C8', ": ".$data['status_nama']);
		
		// Set Title
		$activeSheet->setCellValue('B9', "LIST NO AKUNTANSI");
		$activeSheet->mergeCells('B9:H9');
		$activeSheet->getStyle('B9')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$activeSheet->setCellValue('B10', "NO");
		$activeSheet->setCellValue('C10', "NO HEADER");
		$activeSheet->setCellValue('D10', "NO AKUN");
		$activeSheet->setCellValue('E10', "NAMA AKUN");
		$activeSheet->setCellValue('F10', "KATEGORI");
		$activeSheet->setCellValue('G10', "STATUS");
		$activeSheet->setCellValue('H10', "SALDO");
		$activeSheet->getStyle('B10:H10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B10:H10")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		$x = 10;
		$debet = 0;
		$kredit= 0;
		$no=0;
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				$no =$no+1;
				
			
	            $activeSheet->setCellValue("B$x", number_format($no));
	            $activeSheet->setCellValue("C$x", $row->noheader);
	            $activeSheet->setCellValue("D$x", $row->noakun);
	            $activeSheet->setCellValue("E$x", TreeView_excel($row->level, $row->namaakun));
	            $activeSheet->setCellValue("F$x", $row->namakategori);
	            $activeSheet->setCellValue("G$x", ($row->status?'ACTIVE':'NOT ACTIVE'));
	            $activeSheet->setCellValue("H$x", ($row->saldo));
				
	           
				
				
			}
		}
		$x = $x+1;
		
		$activeSheet->getStyle("D11:D$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("H11:H$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		// $activeSheet->getStyle("E11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("H11:H$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B10:H$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("H")->setWidth(45);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="No. Akuntansi '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
	
	public function create($disabel='')
	{
		$data = [
			'id' => '',
			'kategoriakun' => '',
			'tipeakun' => '',
			'noheader' => '',
			'noakun' => '',
			'namaakun' => '',
			'level' => '',
			'bertambah' => '',
			'berkurang' => '',
			'possaldo' => '',
			'poslaporan' => '',
		];

		$data['disabel'] = $disabel;
		$data['error'] = '';
		$data['title'] = 'Tambah Data';
		$data['content'] = 'Mnomorakuntansi/manage';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['No. Akuntansi', 'Mnomorakuntansi'],
			['Tambah Data', '']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function edit($id)
	{
		if ($id != '') {
			$row = $this->model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
                    'id' => $row->id,
                    'kategoriakun' => $row->idkategori,
                    'tipeakun' => $row->idtipe,
                    'noheader' => $row->noheader,
                    'noakun' => $row->noakun,
                    'namaakun' => $row->namaakun,
                    'level' => $row->level,
                    'bertambah' => $row->bertambah,
                    'berkurang' => $row->berkurang,
                    'possaldo' => $row->possaldo,
                    'poslaporan' => $row->poslaporan,
				];

				$data['disabel'] = '';
				$data['error'] = '';
				$data['title'] = 'Ubah Data';
				$data['content'] = 'Mnomorakuntansi/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', 'dashboard'],
					['Akuntansi', '#'],
					['No. Akuntansi', 'Mnomorakuntansi'],
					['Ubah', '']
				];

                if ($row->idtipe == '2') {
                    $data['list_akun'] = $this->model->getAkunHeader($row->idkategori);
                } else if ($row->idtipe == '3') {
                    $data['list_akun'] = $this->model->getSubAkunHeader($row->idkategori);
                }

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mnomorakuntansi', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('mnomorakuntansi');
		}
	}
	public function view($id)
	{
		if ($id != '') {
			$row = $this->model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
                    'id' => $row->id,
                    'kategoriakun' => $row->idkategori,
                    'tipeakun' => $row->idtipe,
                    'noheader' => $row->noheader,
                    'noakun' => $row->noakun,
                    'namaakun' => $row->namaakun,
                    'level' => $row->level,
                    'bertambah' => $row->bertambah,
                    'berkurang' => $row->berkurang,
                    'possaldo' => $row->possaldo,
                    'poslaporan' => $row->poslaporan,
				];

				$data['disabel'] = 'disabled';
				$data['error'] = '';
				$data['title'] = 'Data Akun';
				$data['content'] = 'Mnomorakuntansi/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', 'dashboard'],
					['Akuntansi', '#'],
					['No. Akuntansi', 'Mnomorakuntansi'],
					['Ubah', '']
				];

                if ($row->idtipe == '2') {
                    $data['list_akun'] = $this->model->getAkunHeader($row->idkategori);
                } else if ($row->idtipe == '3') {
                    $data['list_akun'] = $this->model->getSubAkunHeader($row->idkategori);
                }

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mnomorakuntansi', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('mnomorakuntansi');
		}
	}

	public function save()
	{
		$this->form_validation->set_rules('noheader', 'No Header', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('noakun', 'No. Akun', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('namaakun', 'Nama Akun', 'trim|required|min_length[1]');

		if ($this->form_validation->run() == true) {
			if ($this->model->insertDuplicate()) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect('mnomorakuntansi', 'location');
			}
		} else {
			$this->failed_save($this->input->post('noakun'));
		}
	}

	public function failed_save()
	{
		$data = $this->input->post();
		$data['error'] = validation_errors();
		$data['content'] = 'Mnomorakuntansi/manage';

		$data['title'] = 'Ubah Data';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['No. Akuntansi', 'Mnomorakuntansi'],
			['Ubah', '']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function remove($id)
	{
		$this->model->remove($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah terhapus.');
		redirect('mnomorakuntansi', 'location');
	}
	public function aktifkan($id)
	{
		$this->model->aktifkan($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah diaktifkan.');
		redirect('mnomorakuntansi', 'location');
	}

	public function checkDuplicateAkun()
	{
		return $this->model->checkDuplicateAkun();
	}

	public function getNomorHeaderUtama($idkategori)
	{
		return $this->model->getNomorHeaderUtama($idkategori);
	}

	public function getNomorAkunAvailable($idkategori, $noheader, $level)
	{
		return $this->model->getNomorAkunAvailable($idkategori, $noheader, $level);
	}

	public function getAkunHeader($idkategori)
	{
		$output = $this->model->getAkunHeader($idkategori);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
	}

	public function getSubAkunHeader($idkategori)
	{
		$output = $this->model->getSubAkunHeader($idkategori);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
	}
	function get_data_saldo($id){
		$q="SELECT H.id,H.noakun,H.namaakun,H.saldo
			,(SELECT DATE_FORMAT(IFNULL(MAX(M.tanggal_saldo),'1970-01-01'),'%d-%m-%Y')  as tanggal from makun_nomor_saldo M WHERE M.idakun=H.id) as tanggal_terakhir
			FROM makun_nomor H 
			WHERE H.id='$id'";
		$arr=$this->db->query($q)->row_array();		
		$this->output->set_output(json_encode($arr));

	}
	function komparasi_tanggal(){
		$tanggal_saldo =YMDFormat($this->input->post('tanggal_saldo'));
		$tanggal_terakhir =YMDFormat($this->input->post('tanggal_terakhir'));
		if ($tanggal_saldo < $tanggal_terakhir){
			$arr=false;
		}else{
			$arr=true;
		}
		
		$this->output->set_output(json_encode($arr));

	}
	public function simpan_saldo()
    {		
        $idakun = $this->input->post('idakun');
        $tanggal_saldo =YMDFormat($this->input->post('tanggal_saldo'));
        $saldo_berjalan =RemoveComma($this->input->post('saldo_berjalan'));
        $saldo_set =RemoveComma($this->input->post('saldo_set'));
        
		$data =array(
            'idakun'=>$idakun,
            'tanggal_saldo'=>$tanggal_saldo,
            'saldo_berjalan'=>$saldo_berjalan,
            'saldo_set'=>$saldo_set,
            'saldo_akhir'=>$saldo_set,
            'userid_set'=>$this->session->userdata('user_id'),
            'username_set'=>$this->session->userdata('user_name'),
            'tanggal_set'=>date('Y-m-d H:i:s'),
        );
		$result=$this->db->insert('makun_nomor_saldo',$data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	//DETAIL Transaksi
	function detail($idakun,$disabel=''){
		$row=$this->model->getSpecified($idakun);
		if ($row){
			$data=array(
				'idakun' =>$row->id,
				'noakun' =>$row->noakun,
				'namaakun' =>$row->namaakun,
				'tanggal_trx1' =>date('d-m-Y'),
				'tanggal_trx2' =>date('d-m-Y'),
				'notransaksi' =>'',
				'nojurnal' =>'',
			);
			
		}
		$data['list_ref'] = $this->model->list_ref();
		$data['disabel'] = $disabel;
		$data['error'] = '';
		$data['title'] = 'Transaksi No. Akuntansi';
		$data['content'] = 'Mnomorakuntansi/index_detail';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['Transaksi No. Akuntansi', 'Mnomorakuntansi'],
			['List', '']
		];

		// print_r($data['list_akun']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_detail()
    {
		
		$idakun     		= $this->input->post('idakun');
		$ref_validasi     		= $this->input->post('ref_validasi');		
		$tanggal_trx1     		= $this->input->post('tanggal_trx1');		
		$tanggal_trx2     		= $this->input->post('tanggal_trx2');		
		$notransaksi     		= $this->input->post('notransaksi');		
		$nojurnal     		= $this->input->post('nojurnal');		
		if ($ref_validasi){
			$ref_validasi=implode(',',$ref_validasi);			
		}
		
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
       
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi LIKE '%$notransaksi%'";
		}
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal LIKE '%$nojurnal%'";
		}
        if ($ref_validasi){
			$where .=" AND H.ref_tabel_validasi IN (".$ref_validasi.")";
		}
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_trx2)."'";
        }
		// if ($noakun){
			// $where .=" AND H.id IN (".$noakun.")";
		// }
		$from="(
			SELECT H.id,H.tanggal,ref_tabel_validasi,H.idakun,H.idvalidasi,H.notransaksi,H.nojurnal 
					,A.noakun,A.namaakun,H.debet,H.kredit,H.keterangan,J.nama as asal,H.link_transaksi,H.link_validasi,H.link_rekap

					FROM `jurnal_umum` H
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					LEFT JOIN ref_jenis_jurnal J ON J.ref_validasi=H.ref_tabel_validasi
					WHERE H.`status`='1' AND H.idakun='$idakun' ".$where."
					ORDER BY H.tanggal,ref_tabel_validasi
				) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('noakun','namaakun','namakategori');
        $this->column_order    = array();
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->id;
            $row[] = HumanDateShort($r->tanggal);
            $row[] = $r->nojurnal;
            $row[] = $r->notransaksi;
            $row[] = $r->noakun.' - '.$r->namaakun;
            $row[] = number_format($r->debet,2, ".", ",");
            $row[] = number_format($r->kredit,2, ".", ",");
            $row[] = $r->keterangan;
            $row[] = $r->asal;
				$aksi   = '<div class="btn-group">';
				if ($r->link_transaksi){
				$aksi .= '<a href="'.base_url().$r->link_transaksi.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';					
				}
				if ($r->link_validasi){
				$aksi .= '<a href="'.base_url().$r->link_validasi.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
				}
				if ($r->link_rekap){
				$aksi .= '<a href="'.base_url().$r->link_rekap.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-danger"><i class="si si-doc"></i></a>';
				}
				$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
}
?>