<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_kas_lain extends CI_Controller {

	/**
	 * Setting Jurnal Hutang controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_jurnal_kas_lain_model');
  }

	function index(){
		
		$data = $this->Msetting_jurnal_kas_lain_model->getSpecified('1');
		// $data['list_akun'] = $this->Msetting_jurnal_kas_lain_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Kas Lainnya';
		$data['content'] 		= 'Msetting_jurnal_kas_lain/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Hutang",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting_mutasi'=>$this->input->post('st_auto_posting_mutasi'),
			'batas_batal_mutasi'=>$this->input->post('batas_batal_mutasi'),
			'st_auto_posting_pengajuan'=>$this->input->post('st_auto_posting_pengajuan'),
			'batas_batal_pengajuan'=>$this->input->post('batas_batal_pengajuan'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_kas_lain',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	

}
