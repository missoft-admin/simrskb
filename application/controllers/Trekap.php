<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Trekap extends CI_Controller {

	/**
	 * Rekap Gaji controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trekap_model');
		$this->load->model('Mrka_bendahara_model');
  }
  function insert_validasi_gaji($id){
	  $this->Trekap_model->insert_validasi_gaji($id);
  }
	function detail($id,$disabel=''){
		if($id != ''){
			$row = $this->Trekap_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'notransaksi' 					=> $row->notransaksi,
					'periode' 		=> $row->periode,
					'total_trx' 		=> $row->total_trx,
					'status' 				=> $row->status
				);
				
				$data['disabel'] 			= $disabel;
				$data['error'] 			= '';
				$data['title'] 			= 'Detail Rekapan';
				$data['content']	 	= 'Trekap/detail';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Detail Rekapan",'#'),
											    			array("Ubah",'trekap')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('trekap','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('trekap');
		}
	}
	function detail_var($id,$disabel='disabled'){
		if($id != ''){
			$row = $this->Trekap_model->getSpecified_master($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'idrekap' 					=> $row->idrekap,
					'idmrekap' 					=> $row->idmrekap,
					'st_pendapatan' 					=> $row->st_pendapatan,
					'nama' 					=> $row->nama,
					'nominal' 		=> $row->total,
				);
				
				$data['disabel'] 			= $disabel;
				$data['error'] 			= '';
				$data['title'] 			= 'VARIABLE REKAPAN';
				$data['content']	 	= 'Trekap/detail_var';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Detail Rekapan",'#'),
											    			array("Ubah",'trekap')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('trekap','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('trekap');
		}
	}
	function pembayaran($id,$tipe_bayar,$disabel=''){
		if($id != ''){
			$row = $this->Trekap_model->getSpecified_master($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'idrekap' 					=> $row->idrekap,
					'idmrekap' 					=> $row->idmrekap,
					'st_idtipe' 					=> $row->st_idtipe,
					'st_pendapatan' 					=> $row->st_pendapatan,
					'nama' 					=> $row->nama,
					'nominal' 		=> $row->total,
				);
				
				$data['list_pembayaran'] 			= array();
				$data['list_rekening'] 	= $this->Trekap_model->list_rekening($row->idmrekap);
				$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
				$data['tipe_bayar'] 			= $tipe_bayar;
				$data['disabel'] 			= $disabel;
				$data['error'] 			= '';
				$data['title'] 			= 'Pembayaran Rekapan';
				$data['content']	 	= 'Trekap/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Detail Rekapan",'#'),
											    			array("Ubah",'trekap')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('trekap','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('trekap');
		}
	}
	function index(){
		$data = array(
			'tahun' => date('Y'),
		);
		$data['list_jenis'] 			= $this->Trekap_model->list_jenis();
		$data['error'] 			= '';
		$data['title'] 			= 'Rekap Gaji';
		$data['content'] 		= 'Trekap/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Rekap Gaji",'trekap/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function save(){
		// print_r($this->input->post());exit();
		$idrekap=$this->input->post('trekap_id');
		$id=$this->Trekap_model->saveData();
		if($id){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			// print_r($idrekap);exit();
			redirect('trekap/detail/'.$idrekap,'location');
		}

		
	}
	function getIndex($uri = '')
	{
		$where='';
		$where_status='';
		$periode1=$this->input->post('periode1');
		$periode2=$this->input->post('periode2');
		$status=$this->input->post('status');
		
		if ($periode1!=''){
			$where .=" AND H.periode >='".YMDFormat($periode1)."'";
		}
		if ($periode2!=''){
			$where .=" AND H.periode <='".YMDFormat($periode2)."'";
		}
		
		if ($status!='#'){
			if ($status=='3'){
				$where .=" AND H.st_verifikasi='1'";
			}
			if ($status=='2'){
				$where .=" AND H.status='2' AND H.st_verifikasi= 0";
			}
			if ($status=='1'){
				$where .=" AND H.status='1'";
			}
			
		}
			$this->select = array();
			$this->from   = "
							(
							SELECT H.*,SUM(RM.total) as total_rekap,COUNT(RM.id) as jml_detail,SUM(RM.st_verifikasi) as jml_verif,COUNT(DISTINCT Doc.id) as doc 
							FROM trekap H
							LEFT JOIN trekap_master RM ON RM.idrekap=H.id AND RM.`status`='1'
							LEFT JOIN trekap_dokumen Doc ON Doc.idtransaksi=H.id
							WHERE H.`status` > 0 ".$where."
							GROUP BY H.id
							) as tbl 
			";
			$this->join 	= array();
			$this->where  = array();


			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('notransaksi', 'tahun', 'bulan', 'subyek');
			$this->column_order    = array('notransaksi', 'tahun', 'bulan', 'subyek');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();
					$status='';
					if ($r->status=='1'){						
						$status='<span class="label label-danger">BELUM DIPROSES</span>';						
					}else{
						if ($r->st_verifikasi=='0'){	
							$status='<span class="label label-primary">START REKAPAN</span>';		
						
						}else{
							$status='<span class="label label-success">SELESAI VERIFIKASI</span>';	
						}
					}
					$row[] = $r->id;
					$row[] = $no;
					$row[] = $r->notransaksi;
					$row[] = HumanDateShort($r->periode);
					$row[] = number_format($r->total_trx);
					$row[] = number_format($r->total_rekap);
					$row[] = $status;
					$aksi = '<div class="btn-group">';
					
						// $aksi .= '<div class="btn-group">';
                                                
					if ($r->status != 1 ) {
						$aksi .= '<a href="'.site_url().'trekap/detail/'.$r->id.'/disabled" data-toggle="tooltip" title="Input Gaji" class="btn btn-default btn-xs"><i class="fa fa-eye"></i> </a>';
						if ($r->st_verifikasi=='0'){							
							$aksi .= '<a href="'.site_url().'trekap/detail/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
							if ($r->jml_detail == $r->jml_verif){
								$aksi .= '<button title="Verifikasi" class="btn btn-success btn-xs verifikasi"><i class="fa fa-check"></i> Verifikasi</button>';
							}
						}
						$aksi .= '<div class="btn-group" role="group">
							<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li class="dropdown-header">Print Rekap Gaji</li>
								<li><a tabindex="-1" href="'.site_url().'trekap/print_rincian/'.$r->id.'/1" target="_blank">Rekap</a></li>
								<li><a tabindex="-1" href="'.site_url().'trekap/print_rincian/'.$r->id.'/2" target="_blank">Detail</a></li>
								
							</ul>
						</div>';
						if ($r->st_verifikasi=='0'){	
						$aksi .= '<button title="Batal Rekapan" class="btn btn-danger btn-xs batalkan"><i class="si si-ban"></i></button>';
						}
					}else{
						$aksi .= '<button title="Start Rekapan" class="btn btn-primary btn-xs start"><i class="si si-arrow-right"></i> Start Rekapan</button>';
						$aksi .= '<button title="Batal Rekapan" class="btn btn-danger btn-xs batalkan"><i class="si si-ban"></i> Batalkan</button>';
					}
					if ($r->doc){
						$aksi .= '<a href="'.site_url().'trekap/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-warning" ><i class="fa fa-file-image-o"></i></a>';
					}else{
						$aksi .= '<a href="'.site_url().'trekap/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-danger" ><i class="fa fa-upload"></i></a>';
						  
					}
					$aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function getDetail()
	{
		$where='';
		$id=$this->input->post('id');
		$disabel=$this->input->post('disabel');
		
			$this->select = array();
			$this->from   = "
							(
							SELECT IFNULL(SUM(B.nominal_bayar),0) as bayar,IFNULL(SUM(P.nominal_bayar),0) as pendapatan
							,GROUP_CONCAT(M.nama SEPARATOR ', ') as pembayaran
							,GROUP_CONCAT(MP.nama SEPARATOR ', ') as pembayaran_pendapatan
							,H.* 
							from trekap_master H
							LEFT JOIN trekap_master_pendapatan P ON P.trekap_master_id=H.id
							LEFT JOIN trekap_master_pembayaran B ON B.trekap_master_id=H.id
							LEFT JOIN msumber_kas M ON M.id=B.sumber_kas_id
							LEFT JOIN msumber_kas MP ON MP.id=P.sumber_kas_id
							WHERE H.`status`='1' AND H.idrekap='$id'
							GROUP BY H.id
						
							) as tbl 
			";
			$this->join 	= array();
			$this->where  = array();


			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array();

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$tipe_bayar=GetTipeBayarGaji($r->st_idtipe,$r->st_pendapatan);
					// 1. Jika Variable Gaji Tipe Pendapatan & Variable Rekap bukan pendapatan = tombol pembayaran muncul 
					// 2. Jika Variable Gaji Tipe Potongan & Variable Rekap bukan pendapatan = tidak muncul tombol apapun (hanya verifikasi) 
					// 3. Jika variable gaji tipe potongan & variable rekap adalah pendapatan = hanya muncul tombol untuk proses pendapatan
					// $tipe_bayar='SLA';
					$no++;
					$row = array();
					$pendapatan='';
					if ($r->st_pendapatan=='1'){						
						$pendapatan='<i class="si si-check text-success fa-2x"></i>';						
					}else{
						$pendapatan='<i class="si si-close text-danger fa-2x"></i>';			
						
					}
					$status='';
					if ($r->status=='1'){						
						$status='<span class="label label-danger">BELUM DIPROSES</span>';						
					}else{
						$status='<span class="label label-primary">START REKAPAN</span>';		
					}
					$row[] = $r->id;
					$row[] = $no;
					$row[] = $r->nama;
					$row[] = number_format($r->total);
					$row[] = ($r->st_idtipe=='1' ? text_primary('PENDAPATAN'):text_danger('POTONGAN')).' '.$tipe_bayar;
					$row[] = ($pendapatan);
					$row[] = $r->pembayaran.$r->pembayaran_pendapatan;
					$aksi = '<div class="btn-group">';
					
					
							$aksi .= '<a href="'.site_url().'trekap/detail_var/'.$r->id.'" '.$disabel.' data-toggle="tooltip" title="Input Gaji" class="btn btn-default btn-xs"><i class="fa fa-list"></i> </a>';
						if ($r->st_verifikasi=='0' && $r->total > 0){
							if ($tipe_bayar !='2'){
							$aksi .= '<a href="'.site_url().'trekap/pembayaran/'.$r->id.'/'.$tipe_bayar.'" '.$disabel.' data-toggle="tooltip" title="Pembayaran" class="btn btn-primary btn-xs"><i class="fa fa-credit-card"></i></a>';
								
							}
						}
						if ($r->st_verifikasi=='0'){
							if ($r->bayar == $r->total){
								$aksi .= '<button title="Verifikasi" class="btn btn-danger btn-xs verifikasi" '.$disabel.'><i class="fa fa-check"></i> Verifikasi</button>';								
							}else{
								if ($tipe_bayar=='2'){
									$aksi .= '<button title="Verifikasi" class="btn btn-danger btn-xs verifikasi" '.$disabel.'><i class="fa fa-check"></i> Verifikasi</button>';	
								}else{
									if ($r->pendapatan == $r->total){
										$aksi .= '<button title="Verifikasi" class="btn btn-danger btn-xs verifikasi" '.$disabel.'><i class="fa fa-check"></i> Verifikasi</button>';
									}
								}
							}
						}
						
						$aksi .= '<button title="Print" disabled class="btn btn-success btn-xs print" '.$disabel.'><i class="fa fa-print"></i></button>';
					
					$aksi .= '</div>';
					if ($r->st_verifikasi=='1'){
						$aksi .=' <span class="label label-success">TELAH DIVERIFIKASI</span> ';
					}
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function get_master_variable()
	{
		$where='';
		$id=$this->input->post('id');
		
			$this->select = array();
			$this->from   = "
			(
			SELECT *FROM trekap_master_variable WHERE trekap_master_id='$id'
			) as tbl";
			$this->join 	= array();
			$this->where  = array();


			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('nama_variable','nama_jenis');
			$this->column_order    = array();

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();
					
					$row[] = $r->id;
					$row[] = $r->nama_jenis;
					$row[] = $r->nama_variable;
					$row[] = number_format($r->nominal);
					$aksi = '<div class="btn-group">';
					
					$aksi .= '</div>';
					
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function getPembayaran()
	{
		$where='';
		$id=$this->input->post('id');
		$disabel=$this->input->post('disabel');
		
			$this->select = array();
			$this->from   = "
							(
							SELECT JK.nama as nama_jenis,SK.nama as nama_sumber,B.* from trekap_master_pembayaran B
							LEFT JOIN mjenis_kas JK ON JK.id=B.jenis_kas_id
							LEFT JOIN msumber_kas SK ON SK.id=B.sumber_kas_id
							WHERE B.trekap_master_id='$id'						
							) as tbl 
			";
			$this->join 	= array();
			$this->where  = array();


			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('nama_jenis','nama_sumber','ket_pembayaran','nama_rekening');
			$this->column_order    = array();

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();
					
					$row[] = $r->id;
					$row[] = $r->jenis_kas_id;
					$row[] = $r->sumber_kas_id;
					$row[] = $r->idmetode;//3
					$row[] = $r->nama_jenis;//4
					$row[] = $r->nama_sumber;//5
					$row[] = cara_bayar2($r->idmetode);//6
					$row[] = HumanDateShort($r->tanggal_pencairan);//7
					$row[] = $r->ket_pembayaran;//8
					$row[] = $r->nama_rekening;//9
					$row[] = number_format($r->nominal_bayar);//10
					$aksi = '<div class="btn-group">';
					
						$aksi .= '<button  title="Edit" '.$disabel.' class="btn btn-success btn-xs edit_pembayaran" type="button"><i class="fa fa-pencil"></i></button>';
						$aksi .= '<button title="Hapus" '.$disabel.' class="btn btn-danger btn-xs hapus_pembayaran" type="button"><i class="fa fa-close"></i></button>';
					
					$aksi .= '</div>';
					$row[] = $aksi;//11
					$row[] = $r->idbank;//12
					$row[] = $r->nama_bank;//13
					$row[] = $r->norek;//14
					$row[] = $r->atas_nama;//15

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function getPendapatan()
	{
		$where='';
		$id=$this->input->post('id');
		
			$this->select = array();
			$this->from   = "
							(
							SELECT JK.nama as nama_jenis,SK.nama as nama_sumber,B.* from trekap_master_pendapatan B
							LEFT JOIN mjenis_kas JK ON JK.id=B.jenis_kas_id
							LEFT JOIN msumber_kas SK ON SK.id=B.sumber_kas_id
							WHERE B.trekap_master_id='$id'						
							) as tbl 
			";
			$this->join 	= array();
			$this->where  = array();


			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('nama_jenis','nama_sumber','ket_pembayaran','nama_rekening');
			$this->column_order    = array();

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();
					
					$row[] = $r->id;
					$row[] = $r->jenis_kas_id;
					$row[] = $r->sumber_kas_id;
					$row[] = $r->idmetode;//3
					$row[] = $r->nama_jenis;//4
					$row[] = $r->nama_sumber;//5
					$row[] = cara_bayar2($r->idmetode);//6
					$row[] = HumanDateShort($r->tanggal_pencairan);//7
					$row[] = $r->ket_pembayaran;//8
					$row[] = number_format($r->nominal_bayar);//9
					$aksi = '<div class="btn-group">';
					
						$aksi .= '<button  title="Edit" class="btn btn-success btn-xs edit_pendapatan" type="button"><i class="fa fa-pencil"></i></button>';
						$aksi .= '<button title="Hapus" class="btn btn-danger btn-xs hapus_pendapatan" type="button"><i class="fa fa-close"></i></button>';
					
					$aksi .= '</div>';
					$row[] = $aksi;//10
					
					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function start()
	{
		$idrekap=$this->input->post('idrekap');
		$q="SELECT M.* FROM `trekap_detail` H 
			INNER JOIN tgaji_detail GD ON GD.idgaji=H.idgaji
			INNER JOIN mvariable V ON V.id=GD.idvariable
			INNER JOIN mrekap_var R ON R.idvariable=GD.idvariable
			INNER JOIN mrekap M ON M.id=R.idrekap
			WHERE H.idrekap='$idrekap' AND M.`status`='1'
			GROUP BY R.idrekap
			ORDER BY M.id";
		$master=$this->db->query($q)->result();
		foreach($master as $row){
			$data_det=array(
				'idrekap'=>$idrekap,
				'idmrekap'=>$row->id,
				'nama'=>$row->nama,
				'st_pendapatan'=>$row->st_pendapatan,
				'idakun'=>$row->idakun,
				'noakun'=>$row->noakun,
				'total'=>0,
				'status'=>1,
			);
			$idmrekap=$row->id;
			// print_r($data_det);exit();
			$result=$this->db->insert('trekap_master',$data_det);
			$trekap_master_id=$this->db->insert_id();
			$q_set="SELECT G.id as tgaji_id,G.periode,D.id as tgaji_detail_id,D.idsetting,G.idjenis,D.idheader, D.idvariable,D.idsub,D.nominal 
				,D.nama_variable,J.nama as nama_jenis,D.idtipe
				from tgaji G 
				LEFT JOIN tgaji_detail D ON D.idgaji=G.id AND D.idsub !='1'
				LEFT JOIN mjenis_gaji J ON J.id=G.idjenis
				WHERE G.id IN (SELECT TD.idgaji FROM trekap_detail TD WHERE TD.idrekap='$idrekap') AND G.`status`='1' AND G.st_verifikasi='1'
				AND D.idsetting IN (SELECT MV.idsetting from mrekap_var MV WHERE MV.idrekap='$idmrekap')";
			$hasil=$this->db->query($q_set)->result();
			foreach($hasil as $r){
				$data=array(
					'trekap_master_id'=>$trekap_master_id,
					'tgaji_id'=>$r->tgaji_id,
					'tgaji_detail_id'=>$r->tgaji_detail_id,
					'idjenis'=>$r->idjenis,
					'idsetting'=>$r->idsetting,
					'idvariable'=>$r->idvariable,
					'idtipe'=>$r->idtipe,
					'nama_variable'=>$r->nama_variable,
					'nama_jenis'=>$r->nama_jenis,
					'nominal'=>$r->nominal,
				);
				$result=$this->db->insert('trekap_master_variable',$data);
			}
		}
		$this->db->where('id',$idrekap);
		$this->db->update('trekap',array('status'=>2));
		$this->output->set_output(json_encode($result));
	}
	function save_pembayaran()
	{
		$iddet=$this->input->post('iddet');
		$data=array(
			'trekap_master_id'=>$this->input->post('trekap_master_id'),
			'jenis_kas_id'=>$this->input->post('jenis_kas_id'),
			'sumber_kas_id'=>$this->input->post('sumber_kas_id'),
			'idmetode'=>$this->input->post('idmetode'),
			'nominal_bayar'=> RemoveComma($this->input->post('nominal_bayar')),
			'ket_pembayaran'=>$this->input->post('ket_pembayaran'),
			'idbank'=>$this->input->post('idbank'),
			'nama_bank'=>$this->input->post('nama_bank'),
			'norek'=>$this->input->post('norek'),
			'atas_nama'=>$this->input->post('atas_nama'),
			'nama_rekening'=>$this->input->post('nama_rekening'),
			'tanggal_pencairan'=>YMDFormat($this->input->post('tanggal_pencairan')),
		);
		if ($iddet==''){
		$result=$this->db->insert('trekap_master_pembayaran',$data);
		}else{
			$this->db->where('id',$iddet);
			$result=$this->db->update('trekap_master_pembayaran',$data);
		}
		
		$this->output->set_output(json_encode($result));
	}
	function verifikasi()
	{
		$idrekap=$this->input->post('idrekap');
		$q="SELECT M.st_pendapatan,M.st_idtipe FROM `trekap_master` M WHERE M.id='$idrekap'";
		$r=$this->db->query($q)->row();
		$data_det=array(
			'tipe_bayar' =>GetTipeBayarGaji($r->st_idtipe,$r->st_pendapatan),
			'st_verifikasi' =>1,
		);
		$this->db->where('id',$idrekap);
		$result=$this->db->update('trekap_master',$data_det);	
		
		$this->output->set_output(json_encode($result));
	}
	function validasi()
	{
		$idrekap=$this->input->post('idrekap');
		$this->Trekap_model->insert_validasi_gaji($idrekap);
		
		$data_det=array(
			// 'tipe_bayar' =>GetTipeBayarGaji($r->st_idtipe,$r->st_pendapatan),
			'st_verifikasi' =>1,
		);
		$this->db->where('id',$idrekap);
		$result=$this->db->update('trekap',$data_det);	
		
		$this->output->set_output(json_encode($result));
	}
	function save_pendapatan()
	{
		$iddet=$this->input->post('iddet');
		$data=array(
			'trekap_master_id'=>$this->input->post('trekap_master_id'),
			'jenis_kas_id'=>$this->input->post('jenis_kas_id'),
			'sumber_kas_id'=>$this->input->post('sumber_kas_id'),
			'idmetode'=>$this->input->post('idmetode'),
			'nominal_bayar'=> RemoveComma($this->input->post('nominal_bayar')),
			'ket_pembayaran'=>$this->input->post('ket_pembayaran'),
			'tanggal_pencairan'=>YMDFormat($this->input->post('tanggal_pencairan')),
		);
		if ($iddet==''){
		$result=$this->db->insert('trekap_master_pendapatan',$data);
		}else{
			$this->db->where('id',$iddet);
			$result=$this->db->update('trekap_master_pendapatan',$data);
		}
		
		$this->output->set_output(json_encode($result));
	}
	function get_total_bayar($id)
	{
		// $id=$this->input->post('id');
		$q="SELECT SUM(B.nominal_bayar) as total FROM `trekap_master_pembayaran` B
				WHERE B.trekap_master_id='$id'";
		$arr['total']=$this->db->query($q)->row('total');
		$q="SELECT SUM(B.nominal_bayar) as total FROM `trekap_master_pendapatan` B
				WHERE B.trekap_master_id='$id'";
		$arr['total_pendapatan']=$this->db->query($q)->row('total');
		// print_r($arr);exit();
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($data));
	}
	function batalkan()
	{
		$iduser=$this->session->userdata('user_id');
		$idrekap=$this->input->post('idrekap');
		$this->db->where('id',$idrekap);
		$result=$this->db->update('trekap',array('status'=>0,'deleted_by'=>$iduser,'deleted_date'=>date('Y-m-d H:i:s')));
	
		$this->output->set_output(json_encode($result));
	}
	function hapus_pembayaran()
	{
		$iduser=$this->session->userdata('user_id');
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('trekap_master_pembayaran');
	
		$this->output->set_output(json_encode($result));
	}
	function hapus_pendapatan()
	{
		$iduser=$this->session->userdata('user_id');
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('trekap_master_pendapatan');
	
		$this->output->set_output(json_encode($result));
	}
	public function print_rincian($id,$jenis)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        
		$data = $this->Trekap_model->getSpecifiedHeader($id);
		// print_r($data);exit();
        $data['userinput']= $this->session->userdata('user_name');
		// print_r($data);exit();
		$data['list_detail']=$this->Trekap_model->list_detail($id,$jenis);
		if ($jenis=='1'){
        $html = $this->load->view('Trekap/print_rincian', $data, true);
		}else{
        $html = $this->load->view('Trekap/print_rincian_detail', $data, true);
			
		}
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('History Rincian Tagihan.pdf', array("Attachment"=>0));
    }
	public function upload_document($idtransaksi)
    {
       
        $data = $this->Trekap_model->getSpecifiedHeader2($idtransaksi);
		// print_r($data);exit();
       

        $data['error']      = '';
        $data['title'] = 'Upload Dokumen Rekap';
        $data['content'] = 'Trekap/upload_dokumen';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Rekap", 'Trekap/index'),
            array("Upload Dokumen", '#')
        );

        // $data['listFiles'] = array();
        $data['listFiles'] = $this->Trekap_model->getListUploadedDocument($idtransaksi);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/rekap_gaji/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'trekap_dokumen';

                    $data = array();
                    $data['idtransaksi']  = $this->input->post('idtransaksi');
                    $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->Trekap_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	 public function delete_file($idfile)
    {
        $this->db->where('id', $idfile);
        $query = $this->db->get('trekap_dokumen');
        $row = $query->row();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $idfile);
            if ($this->db->delete('trekap_dokumen')) {
                if (file_exists('./assets/upload/rekap_gaji/'.$row->filename) && $row->filename !='') {
                    unlink('./assets/upload/rekap_gaji/'.$row->filename);
                }
            }
        }

        return true;
    }
}
