<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mgcs_motion extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mgcs_motion_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Mgcs_motion_model->get_gcs_motion();
		if (UserAccesForm($user_acces_form,array('1718'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi GCS Motion';
			$data['content'] 		= 'Mgcs_motion/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi GCS Motion",'mgcs_motion')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_gcs_motion(){
		$gcs_motion_id=$this->input->post('gcs_motion_id');
		$data=array(
			'gcs_motion_1'=>$this->input->post('gcs_motion_1'),
			'gcs_motion_2'=>$this->input->post('gcs_motion_2'),
			'kategori_gcs_motion'=>$this->input->post('kategori_gcs_motion'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($gcs_motion_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mgcs_motion',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$gcs_motion_id);
		    $hasil=$this->db->update('mgcs_motion',$data);
		}
		  
		  json_encode($hasil);
	}
	function update_satuan(){
		$satuan_gcs_motion=$this->input->post('satuan_gcs_motion');
		$data=array(
			'satuan_gcs_motion'=>$this->input->post('satuan_gcs_motion'),
			
		);
		$hasil=$this->db->update('mgcs_motion_satuan',$data);
		
		json_encode($hasil);
	}
	
	function load_gcs_motion()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mgcs_motion` H
							where H.staktif='1'
							ORDER BY H.gcs_motion_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('gcs_motion_1','gcs_motion_1','kategori_gcs_motion');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->gcs_motion_1.' - '.$r->gcs_motion_2);
          $result[] = $r->kategori_gcs_motion;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1720'))){
		  $aksi .= '<button onclick="edit_gcs_motion('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1721'))){
		  $aksi .= '<button onclick="hapus_gcs_motion('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_gcs_motion(){
	  $gcs_motion_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$gcs_motion_id);
		$hasil=$this->db->update('mgcs_motion',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_gcs_motion(){
	  $gcs_motion_id=$this->input->post('id');
	  $q="SELECT *FROM mgcs_motion H WHERE H.id='$gcs_motion_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
