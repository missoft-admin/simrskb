<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_print_out extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_print_out_model');
		$this->load->helper('path');
		
  }
	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1543'))){
			$data = $this->Antrian_print_out_model->get_index_setting();
			$data['tab'] 			= 1;
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Print Out';
			$data['content'] 		= 'Antrian_print_out/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Antrian Setting",'#'),
												  array("Index Setting",'antrian_print_out')
												);
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	//HAPUS
	function save_general(){
		if ($this->Antrian_print_out_model->save_general()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('antrian_print_out','location');
		}
		
	}
	
	function simpan_running(){
		$idrunning=$this->input->post('idrunning');
		$isi=$this->input->post('isi');
		$nourut=$this->input->post('nourut');
		$data['isi']=$isi;
		$data['nourut']=$nourut;
		$data['nourut']=$nourut;
		if ($idrunning==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('antrian_print_out_running_text',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idrunning);
			$hasil=$this->db->update('antrian_print_out_running_text',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function hapus_running(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('antrian_print_out_running_text',$data);
		json_encode($hasil);
	}
	function load_running_text(){
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.isi FROM antrian_print_out_running_text H WHERE H.status='1' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('isi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->isi;

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_running('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		$aksi .= '<button onclick="hapus_running('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_running(){
		$id=$this->input->post('id');
		$q="select *FROM antrian_print_out_running_text H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	
	
}
