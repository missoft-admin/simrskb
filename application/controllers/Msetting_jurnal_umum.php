<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_umum extends CI_Controller {

	/**
	 * Setting Jurnal Umum controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_umum_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_umum_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_umum_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Umum';
		$data['content'] 		= 'Msetting_jurnal_umum/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Umum",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	function update(){
		$id_edit=1;
		$data=array(
			'batas_batal_dep'=>$this->input->post('batas_batal_dep'),
			'batas_batal_pen'=>$this->input->post('batas_batal_pen'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_umum',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Hutang
	function load_rekon()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT M.id,M.nama,M.batas FROM msetting_jurnal_umum_rekon M
				WHERE M.status='1'
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->nama);
            $row[] = ($r->batas);
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-success edit"><i class="fa fa-pencil"></i></button>';				
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_rekon('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_rekon(){
		$iddet=$this->input->post('iddet');
		$batas=$this->input->post('xbatas');
		$nama=$this->input->post('xnama');
		$data=array(
			'setting_id'=>1,
			'batas'=>$batas,
			'nama'=>$nama,
		);
		if ($iddet){
			$this->db->where('id',$iddet);
			$result = $this->db->update('msetting_jurnal_umum_rekon',$data);			
		}else{
			$result = $this->db->insert('msetting_jurnal_umum_rekon',$data);			
		}
		$this->output->set_output(json_encode($result));
	}
	
	function cek_duplicate_rekon($tipe_distributor,$iddistributor){
		
		$gabung=$tipe_distributor.'-'.$iddistributor;
		$q="SELECT *FROM msetting_jurnal_umum_rekon S
			WHERE CONCAT(S.tipe_distributor,'-',S.iddistributor)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_rekon($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->update('msetting_jurnal_umum_rekon',array('status'=>0));
		echo json_encode($result);
	}

}
