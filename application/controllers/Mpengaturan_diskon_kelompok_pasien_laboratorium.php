<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mpengaturan_diskon_kelompok_pasien_laboratorium extends CI_Controller
{
    /**
     * Pengaturan Diskon Laboratorium controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpengaturan_diskon_kelompok_pasien_laboratorium_model');
        $this->load->helper('path');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Pengaturan Diskon Laboratorium';
        $data['content'] = 'Mpengaturan_diskon_kelompok_pasien_laboratorium/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pengaturan Diskon Laboratorium', '#'],
            ['List', 'mpengaturan_diskon_kelompok_pasien_laboratorium'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function setting_rawat_jalan($id): void
    {
        if ('' !== $id) {
            $row = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'nama' => $row->nama,
                ];

                $data['error'] = '';
                $data['title'] = 'Setting Diskon Laboratorium - Rawat Jalan';
                $data['content'] = 'Mpengaturan_diskon_kelompok_pasien_laboratorium/setting_rawat_jalan';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Setting Diskon Laboratorium', '#'],
                    ['Rawat Jalan', 'mpengaturan_diskon_kelompok_pasien_laboratorium'],
                ];
                
                $data['list_pelayanan_head_parent_tarif'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getTarifPelayananHeadParent();
                $data['list_radiologi_head_parent_tarif'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getTarifRadiologiHeadParent(0);
                
                $data['list_setting_diskon_all'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonAll(1, $id);
                $data['list_setting_diskon_adm'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonAdm(1, $id);
                $data['list_setting_diskon_pelayanan'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonPelayanan(1, $id);
                $data['list_setting_diskon_radiologi'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonRadiologi(1, $id);
                $data['list_setting_diskon_laboratorium'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonLaboratorium(1, $id);
                $data['list_setting_diskon_fisioterapi'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonFisioterapi(1, $id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mpengaturan_diskon_kelompok_pasien_laboratorium', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mpengaturan_diskon_kelompok_pasien_laboratorium');
        }
    }
	public function setting_rawat_inap($id): void
    {
        if ('' !== $id) {
            $row = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'nama' => $row->nama,
                ];

                $data['error'] = '';
                $data['title'] = 'Setting Diskon - Rawat Inap';
                $data['content'] = 'Mpengaturan_diskon_kelompok_pasien_laboratorium/setting_rawat_inap';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Setting Diskon', '#'],
                    ['Rawat Inap', 'mpengaturan_diskon_kelompok_pasien_laboratorium/setting_rawat_inap'],
                ];
                
                $data['list_pelayanan_head_parent_tarif_ri'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getTarifPelayananHeadParent_ri();
                $data['list_radiologi_head_parent_tarif'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getTarifRadiologiHeadParent(0);
                
                $data['list_setting_diskon_all'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonAll(1, $id);
                $data['list_setting_diskon_ranap_visite'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonRanapVisite(1, $id);
				
                $data['list_setting_diskon_pelayanan'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonPelayananRanap(1, $id);
                // $data['list_setting_diskon_pelayanan'] = array();
                // $data['list_setting_diskon_radiologi'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonRadiologi(1, $id);
                // $data['list_setting_diskon_laboratorium'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonLaboratorium(1, $id);
                // $data['list_setting_diskon_fisioterapi'] = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->getSettingDiskonFisioterapi(1, $id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mpengaturan_diskon_kelompok_pasien_laboratorium', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mpengaturan_diskon_kelompok_pasien_laboratorium');
        }
    }

    public function getIndex(): void
    {
        $this->select = ['*'];
        $this->from = 'mpasien_kelompok';
        $this->join = [];
        $this->where = [
            'status' => '1',
        ];
        $this->order = [
            'id' => 'DESC',
        ];
        $this->group = [];

        $this->column_search = ['nama'];
        $this->column_order = ['nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama;

            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'mpengaturan_diskon_kelompok_pasien_laboratorium/setting_rawat_jalan/'.$r->id.'" data-toggle="tooltip" title="Setting Diskon Rawat Jalan" class="btn btn-success btn-sm"><i class="fa fa-cog"></i> Rawat Jalan</a>';
            $aksi .= '<a href="'.site_url().'mpengaturan_diskon_kelompok_pasien_laboratorium/setting_rawat_inap/'.$r->id.'" data-toggle="tooltip" title="Setting Diskon Rawat Jalan" class="btn btn-primary btn-sm"><i class="fa fa-cog"></i> Rawat Inap</a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function save_setting_all(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $status_tujuan_poliklinik = $this->input->post('status_tujuan_poliklinik');
        $tujuan_poliklinik = $this->input->post('tujuan_poliklinik');
        $status_pasien = $this->input->post('status_pasien');
        $kasus = $this->input->post('kasus');
        $status_diskon_rp = $this->input->post('status_diskon_rp');
        $diskon_rp = $this->input->post('diskon_rp');
        $status_diskon_persen = $this->input->post('status_diskon_persen');
        $diskon_persen = $this->input->post('diskon_persen');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'status_tujuan_poliklinik' => $status_tujuan_poliklinik,
            'tujuan_poliklinik' => $tujuan_poliklinik,
            'status_pasien' => $status_pasien,
            'kasus' => $kasus,
            'status_diskon_rp' => $status_diskon_rp,
            'diskon_rp' => $diskon_rp,
            'status_diskon_persen' => $status_diskon_persen,
            'diskon_persen' => $diskon_persen,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_rajal_all', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function save_setting_adm(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $status_tujuan_poliklinik = $this->input->post('status_tujuan_poliklinik');
        $tujuan_poliklinik = $this->input->post('tujuan_poliklinik');
        $status_pasien = $this->input->post('status_pasien');
        $kasus = $this->input->post('kasus');
        $jenis_tarif = $this->input->post('tipe_tarif');
        $tarif_pelayanan = $this->input->post('tarif_pelayanan');
        $status_diskon_all = $this->input->post('status_diskon_all');
        $tipe_diskon_all = $this->input->post('tipe_diskon_all');
        $diskon_all_nilai = $this->input->post('diskon_all_nilai');
        $status_diskon_jasasarana = $this->input->post('status_diskon_jasasarana');
        $tipe_diskon_jasasarana = $this->input->post('tipe_diskon_jasasarana');
        $diskon_jasasarana_nilai = $this->input->post('diskon_jasasarana_nilai');
        $status_diskon_jasapelayanan = $this->input->post('status_diskon_jasapelayanan');
        $tipe_diskon_jasapelayanan = $this->input->post('tipe_diskon_jasapelayanan');
        $diskon_jasapelayanan_nilai = $this->input->post('diskon_jasapelayanan_nilai');
        $status_diskon_bhp = $this->input->post('status_diskon_bhp');
        $tipe_diskon_bhp = $this->input->post('tipe_diskon_bhp');
        $diskon_bhp_nilai = $this->input->post('diskon_bhp_nilai');
        $status_diskon_biayaperawatan = $this->input->post('status_diskon_biayaperawatan');
        $tipe_diskon_biayaperawatan = $this->input->post('tipe_diskon_biayaperawatan');
        $diskon_biayaperawatan_nilai = $this->input->post('diskon_biayaperawatan_nilai');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'status_tujuan_poliklinik' => $status_tujuan_poliklinik,
            'tujuan_poliklinik' => $tujuan_poliklinik,
            'status_pasien' => $status_pasien,
            'kasus' => $kasus,
            'jenis_tarif' => $jenis_tarif,
            'tarif_pelayanan' => $tarif_pelayanan,
            'status_diskon_all' => $status_diskon_all,
            'tipe_diskon_all' => $tipe_diskon_all,
            'diskon_all' => $diskon_all_nilai,
            'status_diskon_jasasarana' => $status_diskon_jasasarana,
            'tipe_diskon_jasasarana' => $tipe_diskon_jasasarana,
            'diskon_jasasarana' => $diskon_jasasarana_nilai,
            'status_diskon_jasapelayanan' => $status_diskon_jasapelayanan,
            'tipe_diskon_jasapelayanan' => $tipe_diskon_jasapelayanan,
            'diskon_jasapelayanan' => $diskon_jasapelayanan_nilai,
            'status_diskon_bhp' => $status_diskon_bhp,
            'tipe_diskon_bhp' => $tipe_diskon_bhp,
            'diskon_bhp' => $diskon_bhp_nilai,
            'status_diskon_biaya_perawatan' => $status_diskon_biayaperawatan,
            'tipe_diskon_biaya_perawatan' => $tipe_diskon_biayaperawatan,
            'diskon_biaya_perawatan' => $diskon_biayaperawatan_nilai,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_rajal_adm', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function save_setting_visite_ranap(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $idruangan = $this->input->post('idruangan');
        $idkelas = $this->input->post('idkelas');
        $tarif_pelayanan = $this->input->post('tarif_pelayanan');
        $status_diskon_all = $this->input->post('status_diskon_all');
        $tipe_diskon_all = $this->input->post('tipe_diskon_all');
        $diskon_all_nilai = $this->input->post('diskon_all_nilai');
        $status_diskon_jasasarana = $this->input->post('status_diskon_jasasarana');
        $tipe_diskon_jasasarana = $this->input->post('tipe_diskon_jasasarana');
        $diskon_jasasarana_nilai = $this->input->post('diskon_jasasarana_nilai');
        $status_diskon_jasapelayanan = $this->input->post('status_diskon_jasapelayanan');
        $tipe_diskon_jasapelayanan = $this->input->post('tipe_diskon_jasapelayanan');
        $diskon_jasapelayanan_nilai = $this->input->post('diskon_jasapelayanan_nilai');
        $status_diskon_bhp = $this->input->post('status_diskon_bhp');
        $tipe_diskon_bhp = $this->input->post('tipe_diskon_bhp');
        $diskon_bhp_nilai = $this->input->post('diskon_bhp_nilai');
        $status_diskon_biayaperawatan = $this->input->post('status_diskon_biayaperawatan');
        $tipe_diskon_biayaperawatan = $this->input->post('tipe_diskon_biayaperawatan');
        $diskon_biayaperawatan_nilai = $this->input->post('diskon_biayaperawatan_nilai');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'idruangan' => $idruangan,
            'idkelas' => $idkelas,
            'tarif_pelayanan' => $tarif_pelayanan,
            'status_diskon_all' => $status_diskon_all,
            'tipe_diskon_all' => $tipe_diskon_all,
            'diskon_all' => $diskon_all_nilai,
            'status_diskon_jasasarana' => $status_diskon_jasasarana,
            'tipe_diskon_jasasarana' => $tipe_diskon_jasasarana,
            'diskon_jasasarana' => $diskon_jasasarana_nilai,
            'status_diskon_jasapelayanan' => $status_diskon_jasapelayanan,
            'tipe_diskon_jasapelayanan' => $tipe_diskon_jasapelayanan,
            'diskon_jasapelayanan' => $diskon_jasapelayanan_nilai,
            'status_diskon_bhp' => $status_diskon_bhp,
            'tipe_diskon_bhp' => $tipe_diskon_bhp,
            'diskon_bhp' => $diskon_bhp_nilai,
            'status_diskon_biaya_perawatan' => $status_diskon_biayaperawatan,
            'tipe_diskon_biaya_perawatan' => $tipe_diskon_biayaperawatan,
            'diskon_biaya_perawatan' => $diskon_biayaperawatan_nilai,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_ranap_visite', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function save_setting_pelayanan_ranap(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $idruangan = $this->input->post('idruangan');
        $idkelas = $this->input->post('idkelas');
        $head_parent_tarif = $this->input->post('head_parent_tarif');
        $tarif_pelayanan = $this->input->post('tarif_pelayanan');
        $status_diskon_all = $this->input->post('status_diskon_all');
        $tipe_diskon_all = $this->input->post('tipe_diskon_all');
        $diskon_all_nilai = $this->input->post('diskon_all_nilai');
        $status_diskon_jasasarana = $this->input->post('status_diskon_jasasarana');
        $tipe_diskon_jasasarana = $this->input->post('tipe_diskon_jasasarana');
        $diskon_jasasarana_nilai = $this->input->post('diskon_jasasarana_nilai');
        $status_diskon_jasapelayanan = $this->input->post('status_diskon_jasapelayanan');
        $tipe_diskon_jasapelayanan = $this->input->post('tipe_diskon_jasapelayanan');
        $diskon_jasapelayanan_nilai = $this->input->post('diskon_jasapelayanan_nilai');
        $status_diskon_bhp = $this->input->post('status_diskon_bhp');
        $tipe_diskon_bhp = $this->input->post('tipe_diskon_bhp');
        $diskon_bhp_nilai = $this->input->post('diskon_bhp_nilai');
        $status_diskon_biayaperawatan = $this->input->post('status_diskon_biayaperawatan');
        $tipe_diskon_biayaperawatan = $this->input->post('tipe_diskon_biayaperawatan');
        $diskon_biayaperawatan_nilai = $this->input->post('diskon_biayaperawatan_nilai');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'idruangan' => $idruangan,
            'idkelas' => $idkelas,
            'head_parent_tarif' => $head_parent_tarif,
            'tarif_pelayanan' => $tarif_pelayanan,
            'status_diskon_all' => $status_diskon_all,
            'tipe_diskon_all' => $tipe_diskon_all,
            'diskon_all' => $diskon_all_nilai,
            'status_diskon_jasasarana' => $status_diskon_jasasarana,
            'tipe_diskon_jasasarana' => $tipe_diskon_jasasarana,
            'diskon_jasasarana' => $diskon_jasasarana_nilai,
            'status_diskon_jasapelayanan' => $status_diskon_jasapelayanan,
            'tipe_diskon_jasapelayanan' => $tipe_diskon_jasapelayanan,
            'diskon_jasapelayanan' => $diskon_jasapelayanan_nilai,
            'status_diskon_bhp' => $status_diskon_bhp,
            'tipe_diskon_bhp' => $tipe_diskon_bhp,
            'diskon_bhp' => $diskon_bhp_nilai,
            'status_diskon_biaya_perawatan' => $status_diskon_biayaperawatan,
            'tipe_diskon_biaya_perawatan' => $tipe_diskon_biayaperawatan,
            'diskon_biaya_perawatan' => $diskon_biayaperawatan_nilai,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_ranap_pelayanan', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function save_setting_pelayanan(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $status_tujuan_poliklinik = $this->input->post('status_tujuan_poliklinik');
        $tujuan_poliklinik = $this->input->post('tujuan_poliklinik');
        $status_pasien = $this->input->post('status_pasien');
        $kasus = $this->input->post('kasus');
        $head_parent_tarif = $this->input->post('head_parent_tarif');
        $tarif_pelayanan = $this->input->post('tarif_pelayanan');
        $status_diskon_all = $this->input->post('status_diskon_all');
        $tipe_diskon_all = $this->input->post('tipe_diskon_all');
        $diskon_all_nilai = $this->input->post('diskon_all_nilai');
        $status_diskon_jasasarana = $this->input->post('status_diskon_jasasarana');
        $tipe_diskon_jasasarana = $this->input->post('tipe_diskon_jasasarana');
        $diskon_jasasarana_nilai = $this->input->post('diskon_jasasarana_nilai');
        $status_diskon_jasapelayanan = $this->input->post('status_diskon_jasapelayanan');
        $tipe_diskon_jasapelayanan = $this->input->post('tipe_diskon_jasapelayanan');
        $diskon_jasapelayanan_nilai = $this->input->post('diskon_jasapelayanan_nilai');
        $status_diskon_bhp = $this->input->post('status_diskon_bhp');
        $tipe_diskon_bhp = $this->input->post('tipe_diskon_bhp');
        $diskon_bhp_nilai = $this->input->post('diskon_bhp_nilai');
        $status_diskon_biayaperawatan = $this->input->post('status_diskon_biayaperawatan');
        $tipe_diskon_biayaperawatan = $this->input->post('tipe_diskon_biayaperawatan');
        $diskon_biayaperawatan_nilai = $this->input->post('diskon_biayaperawatan_nilai');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'status_tujuan_poliklinik' => $status_tujuan_poliklinik,
            'tujuan_poliklinik' => $tujuan_poliklinik,
            'status_pasien' => $status_pasien,
            'kasus' => $kasus,
            'head_parent_tarif' => $head_parent_tarif,
            'tarif_pelayanan' => $tarif_pelayanan,
            'status_diskon_all' => $status_diskon_all,
            'tipe_diskon_all' => $tipe_diskon_all,
            'diskon_all' => $diskon_all_nilai,
            'status_diskon_jasasarana' => $status_diskon_jasasarana,
            'tipe_diskon_jasasarana' => $tipe_diskon_jasasarana,
            'diskon_jasasarana' => $diskon_jasasarana_nilai,
            'status_diskon_jasapelayanan' => $status_diskon_jasapelayanan,
            'tipe_diskon_jasapelayanan' => $tipe_diskon_jasapelayanan,
            'diskon_jasapelayanan' => $diskon_jasapelayanan_nilai,
            'status_diskon_bhp' => $status_diskon_bhp,
            'tipe_diskon_bhp' => $tipe_diskon_bhp,
            'diskon_bhp' => $diskon_bhp_nilai,
            'status_diskon_biaya_perawatan' => $status_diskon_biayaperawatan,
            'tipe_diskon_biaya_perawatan' => $tipe_diskon_biayaperawatan,
            'diskon_biaya_perawatan' => $diskon_biayaperawatan_nilai,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_rajal_pelayanan', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function save_setting_radiologi(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $status_tujuan_poliklinik = $this->input->post('status_tujuan_poliklinik');
        $tujuan_poliklinik = $this->input->post('tujuan_poliklinik');
        $status_pasien = $this->input->post('status_pasien');
        $kasus = $this->input->post('kasus');
        $jenis_tarif = $this->input->post('tipe_tarif');
        $head_parent_tarif = $this->input->post('head_parent_tarif');
        $tarif_pelayanan = $this->input->post('tarif_pelayanan');
        $status_diskon_all = $this->input->post('status_diskon_all');
        $tipe_diskon_all = $this->input->post('tipe_diskon_all');
        $diskon_all_nilai = $this->input->post('diskon_all_nilai');
        $status_diskon_jasasarana = $this->input->post('status_diskon_jasasarana');
        $tipe_diskon_jasasarana = $this->input->post('tipe_diskon_jasasarana');
        $diskon_jasasarana_nilai = $this->input->post('diskon_jasasarana_nilai');
        $status_diskon_jasapelayanan = $this->input->post('status_diskon_jasapelayanan');
        $tipe_diskon_jasapelayanan = $this->input->post('tipe_diskon_jasapelayanan');
        $diskon_jasapelayanan_nilai = $this->input->post('diskon_jasapelayanan_nilai');
        $status_diskon_bhp = $this->input->post('status_diskon_bhp');
        $tipe_diskon_bhp = $this->input->post('tipe_diskon_bhp');
        $diskon_bhp_nilai = $this->input->post('diskon_bhp_nilai');
        $status_diskon_biayaperawatan = $this->input->post('status_diskon_biayaperawatan');
        $tipe_diskon_biayaperawatan = $this->input->post('tipe_diskon_biayaperawatan');
        $diskon_biayaperawatan_nilai = $this->input->post('diskon_biayaperawatan_nilai');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'status_tujuan_poliklinik' => $status_tujuan_poliklinik,
            'tujuan_poliklinik' => $tujuan_poliklinik,
            'status_pasien' => $status_pasien,
            'kasus' => $kasus,
            'jenis_tarif' => $jenis_tarif,
            'head_parent_tarif' => $head_parent_tarif,
            'tarif_pelayanan' => $tarif_pelayanan,
            'status_diskon_all' => $status_diskon_all,
            'tipe_diskon_all' => $tipe_diskon_all,
            'diskon_all' => $diskon_all_nilai,
            'status_diskon_jasasarana' => $status_diskon_jasasarana,
            'tipe_diskon_jasasarana' => $tipe_diskon_jasasarana,
            'diskon_jasasarana' => $diskon_jasasarana_nilai,
            'status_diskon_jasapelayanan' => $status_diskon_jasapelayanan,
            'tipe_diskon_jasapelayanan' => $tipe_diskon_jasapelayanan,
            'diskon_jasapelayanan' => $diskon_jasapelayanan_nilai,
            'status_diskon_bhp' => $status_diskon_bhp,
            'tipe_diskon_bhp' => $tipe_diskon_bhp,
            'diskon_bhp' => $diskon_bhp_nilai,
            'status_diskon_biaya_perawatan' => $status_diskon_biayaperawatan,
            'tipe_diskon_biaya_perawatan' => $tipe_diskon_biayaperawatan,
            'diskon_biaya_perawatan' => $diskon_biayaperawatan_nilai,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_rajal_radiologi', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function save_setting_laboratorium(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $status_tujuan_poliklinik = $this->input->post('status_tujuan_poliklinik');
        $tujuan_poliklinik = $this->input->post('tujuan_poliklinik');
        $status_pasien = $this->input->post('status_pasien');
        $kasus = $this->input->post('kasus');
        $jenis_tarif = $this->input->post('tipe_tarif');
        $head_parent_tarif = $this->input->post('head_parent_tarif');
        $tarif_pelayanan = $this->input->post('tarif_pelayanan');
        $status_diskon_all = $this->input->post('status_diskon_all');
        $tipe_diskon_all = $this->input->post('tipe_diskon_all');
        $diskon_all_nilai = $this->input->post('diskon_all_nilai');
        $status_diskon_jasasarana = $this->input->post('status_diskon_jasasarana');
        $tipe_diskon_jasasarana = $this->input->post('tipe_diskon_jasasarana');
        $diskon_jasasarana_nilai = $this->input->post('diskon_jasasarana_nilai');
        $status_diskon_jasapelayanan = $this->input->post('status_diskon_jasapelayanan');
        $tipe_diskon_jasapelayanan = $this->input->post('tipe_diskon_jasapelayanan');
        $diskon_jasapelayanan_nilai = $this->input->post('diskon_jasapelayanan_nilai');
        $status_diskon_bhp = $this->input->post('status_diskon_bhp');
        $tipe_diskon_bhp = $this->input->post('tipe_diskon_bhp');
        $diskon_bhp_nilai = $this->input->post('diskon_bhp_nilai');
        $status_diskon_biayaperawatan = $this->input->post('status_diskon_biayaperawatan');
        $tipe_diskon_biayaperawatan = $this->input->post('tipe_diskon_biayaperawatan');
        $diskon_biayaperawatan_nilai = $this->input->post('diskon_biayaperawatan_nilai');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'status_tujuan_poliklinik' => $status_tujuan_poliklinik,
            'tujuan_poliklinik' => $tujuan_poliklinik,
            'status_pasien' => $status_pasien,
            'kasus' => $kasus,
            'jenis_tarif' => $jenis_tarif,
            'head_parent_tarif' => $head_parent_tarif,
            'tarif_pelayanan' => $tarif_pelayanan,
            'status_diskon_all' => $status_diskon_all,
            'tipe_diskon_all' => $tipe_diskon_all,
            'diskon_all' => $diskon_all_nilai,
            'status_diskon_jasasarana' => $status_diskon_jasasarana,
            'tipe_diskon_jasasarana' => $tipe_diskon_jasasarana,
            'diskon_jasasarana' => $diskon_jasasarana_nilai,
            'status_diskon_jasapelayanan' => $status_diskon_jasapelayanan,
            'tipe_diskon_jasapelayanan' => $tipe_diskon_jasapelayanan,
            'diskon_jasapelayanan' => $diskon_jasapelayanan_nilai,
            'status_diskon_bhp' => $status_diskon_bhp,
            'tipe_diskon_bhp' => $tipe_diskon_bhp,
            'diskon_bhp' => $diskon_bhp_nilai,
            'status_diskon_biaya_perawatan' => $status_diskon_biayaperawatan,
            'tipe_diskon_biaya_perawatan' => $tipe_diskon_biayaperawatan,
            'diskon_biaya_perawatan' => $diskon_biayaperawatan_nilai,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_rajal_laboratorium', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function save_setting_fisioterapi(): void
    {
        $tipe = $this->input->post('tipe');
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $tujuan_pasien = $this->input->post('tujuan_pasien');
        $status_tujuan_poliklinik = $this->input->post('status_tujuan_poliklinik');
        $tujuan_poliklinik = $this->input->post('tujuan_poliklinik');
        $status_pasien = $this->input->post('status_pasien');
        $kasus = $this->input->post('kasus');
        $head_parent_tarif = $this->input->post('head_parent_tarif');
        $tarif_pelayanan = $this->input->post('tarif_pelayanan');
        $status_diskon_all = $this->input->post('status_diskon_all');
        $tipe_diskon_all = $this->input->post('tipe_diskon_all');
        $diskon_all_nilai = $this->input->post('diskon_all_nilai');
        $status_diskon_jasasarana = $this->input->post('status_diskon_jasasarana');
        $tipe_diskon_jasasarana = $this->input->post('tipe_diskon_jasasarana');
        $diskon_jasasarana_nilai = $this->input->post('diskon_jasasarana_nilai');
        $status_diskon_jasapelayanan = $this->input->post('status_diskon_jasapelayanan');
        $tipe_diskon_jasapelayanan = $this->input->post('tipe_diskon_jasapelayanan');
        $diskon_jasapelayanan_nilai = $this->input->post('diskon_jasapelayanan_nilai');
        $status_diskon_bhp = $this->input->post('status_diskon_bhp');
        $tipe_diskon_bhp = $this->input->post('tipe_diskon_bhp');
        $diskon_bhp_nilai = $this->input->post('diskon_bhp_nilai');
        $status_diskon_biayaperawatan = $this->input->post('status_diskon_biayaperawatan');
        $tipe_diskon_biayaperawatan = $this->input->post('tipe_diskon_biayaperawatan');
        $diskon_biayaperawatan_nilai = $this->input->post('diskon_biayaperawatan_nilai');

        $data = [
            'tipe' => $tipe,
            'idkelompokpasien' => $idkelompokpasien,
            'idrekanan' => $idrekanan,
            'tujuan_pasien' => $tujuan_pasien,
            'status_tujuan_poliklinik' => $status_tujuan_poliklinik,
            'tujuan_poliklinik' => $tujuan_poliklinik,
            'status_pasien' => $status_pasien,
            'kasus' => $kasus,
            'head_parent_tarif' => $head_parent_tarif,
            'tarif_pelayanan' => $tarif_pelayanan,
            'status_diskon_all' => $status_diskon_all,
            'tipe_diskon_all' => $tipe_diskon_all,
            'diskon_all' => $diskon_all_nilai,
            'status_diskon_jasasarana' => $status_diskon_jasasarana,
            'tipe_diskon_jasasarana' => $tipe_diskon_jasasarana,
            'diskon_jasasarana' => $diskon_jasasarana_nilai,
            'status_diskon_jasapelayanan' => $status_diskon_jasapelayanan,
            'tipe_diskon_jasapelayanan' => $tipe_diskon_jasapelayanan,
            'diskon_jasapelayanan' => $diskon_jasapelayanan_nilai,
            'status_diskon_bhp' => $status_diskon_bhp,
            'tipe_diskon_bhp' => $tipe_diskon_bhp,
            'diskon_bhp' => $diskon_bhp_nilai,
            'status_diskon_biaya_perawatan' => $status_diskon_biayaperawatan,
            'tipe_diskon_biaya_perawatan' => $tipe_diskon_biayaperawatan,
            'diskon_biaya_perawatan' => $diskon_biayaperawatan_nilai,
        ];

        $result = $this->db->insert('merm_pengaturan_diskon_rajal_fisioterapi', $data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function get_tarif_adm(): void
    {
        $idtipe = $_GET['idtipe'];
        $idjenis = $_GET['idjenis'];
        $status = $_GET['status'];

        // Fetch data based on parameters (replace this with your actual data fetching logic)
        if ($idjenis) {
            $data = get_all('mtarif_administrasi', ['idtipe' => $idtipe, 'idjenis' => $idjenis, 'status' => $status]);
        } else {
            $data = get_all('mtarif_administrasi', ['idtipe' => $idtipe, 'status' => $status]);
        }

        // Prepare the response data
        $response = [];
        foreach ($data as $row) {
            $response[] = [
                'id' => $row->id,
                'nama' => GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )',
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }
	public function get_tarif_visite(): void
    {
        $idruangan = $_GET['idruangan'];
		if ($idruangan!='0'){
			$data = get_all('mtarif_visitedokter', [ 'idruangan' =>$idruangan,'status'=>1]);
			
		}else{
			
			$data = get_all('mtarif_visitedokter', [ 'status'=>1]);
		}

       

        // Prepare the response data
        $response = [];
        foreach ($data as $row) {
            $response[] = [
                'id' => $row->id,
                'nama' => $row->nama,
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }

    public function get_tarif_pelayanan(): void
    {
        $this->db->where('mtarif_rawatjalan.status', '1');
        $this->db->where('mtarif_rawatjalan.level !=', '0');

        $idparent = $_GET['path'];

        if ('0' !== $idparent) {
            $this->db->where('LEFT(mtarif_rawatjalan.path, '.strlen($idparent).') = ' . $idparent);
        }

        $this->db->order_by('mtarif_rawatjalan.path', 'ASC');
        $query = $this->db->get('mtarif_rawatjalan');

        // Prepare the response data
        $response = [];
        foreach ($query->result() as $row) {
            $response[] = [
                'id' => $row->id,
                'path' => $row->path,
                'nama' => TreeView($row->level, $row->nama),
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }
	public function get_tarif_pelayanan_ri(): void
    {
        $this->db->where('mtarif_rawatinap.status', '1');
        $this->db->where('mtarif_rawatinap.level', '0');

        // $idparent = $_GET['path'];
        $idruangan = $_GET['idruangan'];
		if ($idruangan!='0'){
			$this->db->where('mtarif_rawatinap.idruangan', $idruangan);
		}

        // if ('0' !== $idparent) {
            // $this->db->where('LEFT(mtarif_rawatinap.path, '.strlen($idparent).') = ' . $idparent);
        // }

        $this->db->order_by('mtarif_rawatinap.path', 'ASC');
        $query = $this->db->get('mtarif_rawatinap');

        // Prepare the response data
        $response = [];
        foreach ($query->result() as $row) {
            $response[] = [
                'id' => $row->id,
                'path' => $row->path,
                'nama' => TreeView($row->level, $row->nama),
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }
	public function get_tarif_pelayanan_detail_ri(): void
    {
        $this->db->where('mtarif_rawatinap.status', '1');
        $this->db->where('mtarif_rawatinap.level', '1');

        // $idparent = $_GET['path'];
        $path = $_GET['path'];
		if ($path!='0'){
			$this->db->where('mtarif_rawatinap.headerpath', $path);
		}

        // if ('0' !== $idparent) {
            // $this->db->where('LEFT(mtarif_rawatinap.path, '.strlen($idparent).') = ' . $idparent);
        // }

        $this->db->order_by('mtarif_rawatinap.path', 'ASC');
        $query = $this->db->get('mtarif_rawatinap');

        // Prepare the response data
        $response = [];
        foreach ($query->result() as $row) {
            $response[] = [
                'id' => $row->id,
                'path' => $row->path,
                'nama' => TreeView($row->level, $row->nama),
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }

    public function get_tarif_radiologi(): void
    {
        $tipe_tarif = $_GET['tipe_tarif'];
        $head_parent_tarif = $_GET['head_parent_tarif'];
        $status = $_GET['status'];

        $this->db->select("mtarif_radiologi.id,
        mtarif_radiologi.idtipe,
        mtarif_radiologi.idkelompok,
        mtarif_radiologi.path,
        mtarif_radiologi.level,
        (
            CASE 
                WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
                    CONCAT(mtarif_radiologi.nama,  ' (', mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ')')
            ELSE
                mtarif_radiologi.nama
            END
        ) AS nama");

        $this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
        $this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');

        $this->db->where('mtarif_radiologi.status', '1');
        
        if ($status == 'head') {
            $this->db->where('mtarif_radiologi.level =', '0');
        } else {
            $this->db->where('mtarif_radiologi.level !=', '0');
        }

        if ('0' !== $tipe_tarif) {
            $this->db->where('mtarif_radiologi.idtipe', $tipe_tarif);
        }

        if ('0' !== $head_parent_tarif) {
            $this->db->where('LEFT(mtarif_radiologi.path, '.strlen($head_parent_tarif).') = ' . $head_parent_tarif);
        }

        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');

        // Prepare the response data
        $response = [];
        foreach ($query->result() as $row) {
            $response[] = [
                'id' => $row->id,
                'path' => $row->path,
                'nama' => TreeView($row->level, $row->nama),
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }

    public function get_tarif_laboratorium(): void
    {
        $tipe_tarif = $_GET['tipe_tarif'];
        $head_parent_tarif = $_GET['head_parent_tarif'];
        $status = $_GET['status'];

        $this->db->where('mtarif_laboratorium.status', '1');
        
        if ($status == 'head') {
            $this->db->where('mtarif_laboratorium.level =', '0');
        } else {
            $this->db->where('mtarif_laboratorium.level !=', '0');
        }

        if ('0' !== $tipe_tarif) {
            $this->db->where('mtarif_laboratorium.idtipe', $tipe_tarif);
        }

        if ('0' !== $head_parent_tarif) {
            $this->db->where('LEFT(mtarif_laboratorium.path, '.strlen($head_parent_tarif).') = ' . $head_parent_tarif);
        }

        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');

        // Prepare the response data
        $response = [];
        foreach ($query->result() as $row) {
            $response[] = [
                'id' => $row->id,
                'path' => $row->path,
                'nama' => TreeView($row->level, $row->nama),
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }

    public function get_tarif_fisioterapi(): void
    {
        $head_parent_tarif = $_GET['head_parent_tarif'];
        $status = $_GET['status'];

        $this->db->where('mtarif_fisioterapi.status', '1');
        
        if ($status == 'head') {
            $this->db->where('mtarif_fisioterapi.level =', '0');
        } else {
            $this->db->where('mtarif_fisioterapi.level !=', '0');
        }

        if ('0' !== $head_parent_tarif) {
            $this->db->where('LEFT(mtarif_fisioterapi.path, '.strlen($head_parent_tarif).') = ' . $head_parent_tarif);
        }

        $this->db->order_by('mtarif_fisioterapi.path', 'ASC');
        $query = $this->db->get('mtarif_fisioterapi');

        // Prepare the response data
        $response = [];
        foreach ($query->result() as $row) {
            $response[] = [
                'id' => $row->id,
                'path' => $row->path,
                'nama' => TreeView($row->level, $row->nama),
                // Add more fields as needed
            ];
        }

        // Set the response header to indicate JSON content
        header('Content-Type: application/json');

        // Send the JSON-encoded response to the client
        $this->output->set_output(json_encode($response));
    }

    public function delete_setting_all($id): void
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('merm_pengaturan_diskon_rajal_all');

        if ($result) {
            $this->output->set_output(json_encode(['status' => 'success', 'message' => 'Setting berhasil dihapus.']));
        } else {
            $this->output->set_output(json_encode(['status' => 'error', 'message' => 'Gagal menghapus setting.']));
        }
    }

    public function delete_setting_adm($id): void
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('merm_pengaturan_diskon_rajal_adm');

        if ($result) {
            $this->output->set_output(json_encode(['status' => 'success', 'message' => 'Setting berhasil dihapus.']));
        } else {
            $this->output->set_output(json_encode(['status' => 'error', 'message' => 'Gagal menghapus setting.']));
        }
    }

    public function delete_setting_pelayanan($id): void
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('merm_pengaturan_diskon_rajal_pelayanan');

        if ($result) {
            $this->output->set_output(json_encode(['status' => 'success', 'message' => 'Setting berhasil dihapus.']));
        } else {
            $this->output->set_output(json_encode(['status' => 'error', 'message' => 'Gagal menghapus setting.']));
        }
    }
	public function delete_setting_pelayanan_ri($id): void
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('merm_pengaturan_diskon_ranap_pelayanan');

        if ($result) {
            $this->output->set_output(json_encode(['status' => 'success', 'message' => 'Setting berhasil dihapus.']));
        } else {
            $this->output->set_output(json_encode(['status' => 'error', 'message' => 'Gagal menghapus setting.']));
        }
    }

    public function delete_setting_radiologi($id): void
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('merm_pengaturan_diskon_rajal_radiologi');

        if ($result) {
            $this->output->set_output(json_encode(['status' => 'success', 'message' => 'Setting berhasil dihapus.']));
        } else {
            $this->output->set_output(json_encode(['status' => 'error', 'message' => 'Gagal menghapus setting.']));
        }
    }

    public function delete_setting_laboratorium($id): void
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('merm_pengaturan_diskon_rajal_laboratorium');

        if ($result) {
            $this->output->set_output(json_encode(['status' => 'success', 'message' => 'Setting berhasil dihapus.']));
        } else {
            $this->output->set_output(json_encode(['status' => 'error', 'message' => 'Gagal menghapus setting.']));
        }
    }

    public function delete_setting_fisioterapi($id): void
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('merm_pengaturan_diskon_rajal_fisioterapi');

        if ($result) {
            $this->output->set_output(json_encode(['status' => 'success', 'message' => 'Setting berhasil dihapus.']));
        } else {
            $this->output->set_output(json_encode(['status' => 'error', 'message' => 'Gagal menghapus setting.']));
        }
    }
	public function get_obat()
    {
        $cari 	= $this->input->post('search');
        $idtipe 	= $this->input->post('idtipe');
        $data_obat = $this->Mpengaturan_diskon_kelompok_pasien_laboratorium_model->get_obat($idtipe,$cari);
        $this->output->set_output(json_encode($data_obat));
    }
	function simpan_bmhp(){
		// print_r($this->input->post());exit;
		$hasil=$this->get_duplicat_logic_bmhp($this->input->post('idkelompokpasien'),$this->input->post('idrekanan'),$this->input->post('idtipe_poli'),$this->input->post('idpoli'),$this->input->post('statuspasienbaru'),$this->input->post('pertemuan_id'),$this->input->post('idtipe'),$this->input->post('idbarang'));
		if ($hasil==null){
			
			$this->tipe = 1;
			$this->idtipe_poli = $this->input->post('idtipe_poli');
			$this->idkelompokpasien = $this->input->post('idkelompokpasien');
			$this->idrekanan = $this->input->post('idrekanan');
			$this->idpoli = $this->input->post('idpoli');
			$this->statuspasienbaru = $this->input->post('statuspasienbaru');
			$this->pertemuan_id = $this->input->post('pertemuan_id');
			$this->idtipe = $this->input->post('idtipe');
			$this->idbarang = $this->input->post('idbarang');
			$this->operand = $this->input->post('operand');
			$this->diskon = $this->input->post('diskon');
			$this->status = 1;
	
			
		  $hasil=$this->db->insert('merm_pengaturan_diskon_rajal_bmhp',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function get_duplicat_logic_bmhp($idkelompokpasien,$idrekanan,$idtipe_poli,$idpoli,$statuspasienbaru,$pertemuan_id,$idtipe,$idbarang){
		$idpoli=($idpoli=='#'?0:$idpoli);
		$pertemuan_id=($pertemuan_id=='#'?0:$pertemuan_id);

		$q="SELECT *FROM merm_pengaturan_diskon_rajal_bmhp 
				WHERE tipe='1' AND idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan'  AND idtipe_poli='$idtipe_poli' 
				AND idpoli='$idpoli'  
				AND statuspasienbaru='$statuspasienbaru' 
				AND pertemuan_id='$pertemuan_id' AND idtipe='$idtipe'  AND idbarang='$idbarang' ";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function simpan_bmhp_ri(){
		// print_r($this->input->post());exit;
		$hasil=$this->get_duplicat_logic_bmhp_ri($this->input->post('idkelompokpasien'),$this->input->post('idrekanan'),$this->input->post('tujuan_pasien'),$this->input->post('idruangan'),$this->input->post('idkelas'),$this->input->post('idtipe'),$this->input->post('idbarang'));
		if ($hasil==null){
			
			$this->tipe = 1;
			$this->tujuan_pasien = $this->input->post('tujuan_pasien');
			$this->idkelompokpasien = $this->input->post('idkelompokpasien');
			$this->idrekanan = $this->input->post('idrekanan');
			$this->idruangan = $this->input->post('idruangan');
			$this->idkelas = $this->input->post('idkelas');
			$this->idtipe = $this->input->post('idtipe');
			$this->idbarang = $this->input->post('idbarang');
			$this->operand = $this->input->post('operand');
			$this->diskon = $this->input->post('diskon');
			$this->status = 1;
	
			
		  $hasil=$this->db->insert('merm_pengaturan_diskon_ranap_bmhp',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function get_duplicat_logic_bmhp_ri($idkelompokpasien,$idrekanan,$tujuan_pasien,$idruangan,$idkelas,$idtipe,$idbarang){
		// $idpoli=($idpoli=='#'?0:$idpoli);

		$q="SELECT *FROM merm_pengaturan_diskon_ranap_bmhp 
				WHERE tipe='1' AND idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan'  AND tujuan_pasien='$tujuan_pasien' 
				AND idruangan='$idruangan'  
				AND idkelas='$idkelas' 
				AND idtipe='$idtipe'  AND idbarang='$idbarang' ";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
	function load_bmhp()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idpoli !='0' THEN MP.nama ELSE 'ALL' END as nama_poli,CASE WHEN H.idtipe !='0' THEN TB.nama_tipe ELSE 'ALL TIPE' END as tipe_barang
						,CASE WHEN H.idbarang !='0' THEN MB.nama ELSE 'ALL BARANG' END as nama_barang
						,CASE WHEN H.pertemuan_id !='0' THEN K.ref ELSE 'ALL' END as kasus
						,CASE WHEN H.idtipe_poli ='1' THEN 'IGD' ELSE 'POLIKLINIK' END as nama_tipe
						,H.* FROM merm_pengaturan_diskon_rajal_bmhp H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN merm_referensi K ON K.nilai=H.pertemuan_id AND K.ref_head_id='15'
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe
						LEFT JOIN view_barang_all MB ON MB.idtipe=H.idtipe AND H.idbarang=MB.id
						WHERE idkelompokpasien='$idkelompokpasien'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_poli','kasus','nama_tipe');
			$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
				 $aksi='';
			  $result[] = GetTipePasienPiutang($r->idtipe_poli);
			  $result[] = ($r->idpoli=='0'?text_default($r->nama_poli):$r->nama_poli);
			  $result[] = GetPertemuan($r->statuspasienbaru);
			  $result[] = ($r->pertemuan_id=='0'?text_default($r->kasus):$r->kasus);
			  $result[] = ($r->idtipe=='0'?text_default('ALL TIPE'):$r->tipe_barang);
			  $result[] = ($r->idbarang=='0'?text_default('ALL BARANG'):$r->nama_barang);
			  $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):number($r->diskon,1).$this->jenis_operand($r->operand));
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="hapus_bmhp('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function load_bmhp_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idruangan !='0' THEN MR.nama ELSE 'ALL' END as nama_ruangan
						,CASE WHEN H.idkelas !='0' THEN MK.nama ELSE 'ALL' END as nama_kelas
						,CASE WHEN H.idtipe !='0' THEN TB.nama_tipe ELSE 'ALL TIPE' END as tipe_barang
						,CASE WHEN H.idbarang !='0' THEN MB.nama ELSE 'ALL BARANG' END as nama_barang
						,CASE WHEN H.tujuan_pasien ='1' THEN 'RAWAT INAP' ELSE 'ODS' END as nama_tipe
						,H.* 
						FROM merm_pengaturan_diskon_ranap_bmhp H
						LEFT JOIN mruangan MR ON MR.id=H.idruangan
						LEFT JOIN mkelas MK ON MK.id=H.idkelas
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe
						LEFT JOIN view_barang_all MB ON MB.idtipe=H.idtipe AND H.idbarang=MB.id
						WHERE H.idkelompokpasien='$idkelompokpasien'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ruangan','nama_kelas','nama_tipe');
			$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
				 $aksi='';
			  $result[] = GetStatusRencana($r->tujuan_pasien);
			  $result[] = ($r->idruangan=='0'?text_default($r->nama_ruangan):$r->nama_ruangan);
			  $result[] = ($r->idkelas=='0'?text_default($r->nama_kelas):$r->nama_kelas);
			  $result[] = ($r->idtipe=='0'?text_default('ALL TIPE'):$r->tipe_barang);
			  $result[] = ($r->idbarang=='0'?text_default('ALL BARANG'):$r->nama_barang);
			  $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):number($r->diskon,1).$this->jenis_operand($r->operand));
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="hapus_bmhp('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function jenis_operand($operand){
		  if ($operand=='0'){
			  return '';
		  }
		  if ($operand=='1'){
			  return ' %';
		  }
		  if ($operand=='2'){
			  return '';
		  }
	  }
	  function hapus_bmhp(){
		  $id=$this->input->post('id');
		  $this->db->where('id',$id);
		  $hasil=$this->db->delete('merm_pengaturan_diskon_rajal_bmhp',$this);
		  
		  json_encode($hasil);
		  
	  }
	  function hapus_bmhp_ri(){
		  $id=$this->input->post('id');
		  $this->db->where('id',$id);
		  $hasil=$this->db->delete('merm_pengaturan_diskon_ranap_bmhp',$this);
		  
		  json_encode($hasil);
		  
	  }
	//FARMASI
	function simpan_farmasi(){
		// print_r($this->input->post());exit;
		$hasil=$this->get_duplicat_logic_farmasi($this->input->post('idkelompokpasien'),$this->input->post('idrekanan'),$this->input->post('idtipe_poli'),$this->input->post('idpoli'),$this->input->post('statuspasienbaru'),$this->input->post('pertemuan_id'),$this->input->post('idtipe'),$this->input->post('idbarang'));
		if ($hasil==null){
			
			$this->tipe = 1;
			$this->idtipe_poli = $this->input->post('idtipe_poli');
			$this->idkelompokpasien = $this->input->post('idkelompokpasien');
			$this->idrekanan = $this->input->post('idrekanan');
			$this->idpoli = $this->input->post('idpoli');
			$this->statuspasienbaru = $this->input->post('statuspasienbaru');
			$this->pertemuan_id = $this->input->post('pertemuan_id');
			$this->idtipe = $this->input->post('idtipe');
			$this->idbarang = $this->input->post('idbarang');
			$this->operand = $this->input->post('operand');
			$this->diskon = $this->input->post('diskon');
			$this->status = 1;
	
			
		  $hasil=$this->db->insert('merm_pengaturan_diskon_rajal_farmasi',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function get_duplicat_logic_farmasi($idkelompokpasien,$idrekanan,$idtipe_poli,$idpoli,$statuspasienbaru,$pertemuan_id,$idtipe,$idbarang){
		$idpoli=($idpoli=='#'?0:$idpoli);
		$$pertemuan_id=($pertemuan_id=='#'?0:$pertemuan_id);

		$q="SELECT *FROM merm_pengaturan_diskon_rajal_farmasi 
				WHERE tipe='1' AND idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan'  AND idtipe_poli='$idtipe_poli' 
				AND idpoli='$idpoli'  
				AND statuspasienbaru='$statuspasienbaru' 
				AND pertemuan_id='$pertemuan_id' AND idtipe='$idtipe'  AND idbarang='$idbarang' ";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_farmasi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idpoli !='0' THEN MP.nama ELSE 'ALL' END as nama_poli,CASE WHEN H.idtipe !='0' THEN TB.nama_tipe ELSE 'ALL TIPE' END as tipe_barang
						,CASE WHEN H.idbarang !='0' THEN MB.nama ELSE 'ALL BARANG' END as nama_barang
						,CASE WHEN H.pertemuan_id !='0' THEN K.ref ELSE 'ALL' END as kasus
						,CASE WHEN H.idtipe_poli ='1' THEN 'IGD' ELSE 'POLIKLINIK' END as nama_tipe
						,H.* FROM merm_pengaturan_diskon_rajal_farmasi H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN merm_referensi K ON K.nilai=H.pertemuan_id AND K.ref_head_id='15'
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe
						LEFT JOIN view_barang_all MB ON MB.idtipe=H.idtipe AND H.idbarang=MB.id
						WHERE idkelompokpasien='$idkelompokpasien'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_poli','kasus','nama_tipe');
			$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
				 $aksi='';
			  $result[] = GetTipePasienPiutang($r->idtipe_poli);
			  $result[] = ($r->idpoli=='0'?text_default($r->nama_poli):$r->nama_poli);
			  $result[] = GetPertemuan($r->statuspasienbaru);
			  $result[] = ($r->pertemuan_id=='0'?text_default($r->kasus):$r->kasus);
			  $result[] = ($r->idtipe=='0'?text_default('ALL TIPE'):$r->tipe_barang);
			  $result[] = ($r->idbarang=='0'?text_default('ALL BARANG'):$r->nama_barang);
			  $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):number($r->diskon,1).$this->jenis_operand($r->operand));
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="hapus_farmasi('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	 
	  function hapus_farmasi(){
		  $id=$this->input->post('id');
		  $this->db->where('id',$id);
		  $hasil=$this->db->delete('merm_pengaturan_diskon_rajal_farmasi',$this);
		  
		  json_encode($hasil);
		  
	  }
	//NARCOSE
	
	function simpan_narcose_bedah_ri(){
		// print_r($this->input->post());exit;
		$hasil=$this->get_duplicat_logic_narcose_bedah_ri($this->input->post('idkelompokpasien'),$this->input->post('idrekanan'),$this->input->post('tujuan_pasien'),$this->input->post('idruangan'),$this->input->post('idkelas'),$this->input->post('idtipe'),$this->input->post('idbarang'));
		if ($hasil==null){
			
			$this->tipe = 1;
			$this->tujuan_pasien = $this->input->post('tujuan_pasien');
			$this->idkelompokpasien = $this->input->post('idkelompokpasien');
			$this->idrekanan = $this->input->post('idrekanan');
			$this->idruangan = $this->input->post('idruangan');
			$this->idkelas = $this->input->post('idkelas');
			$this->jenis_opr = $this->input->post('jenis_opr');
			$this->idtipe = $this->input->post('idtipe');
			$this->idbarang = $this->input->post('idbarang');
			$this->operand = $this->input->post('operand');
			$this->diskon = $this->input->post('diskon');
			$this->status = 1;
	
			
		  $hasil=$this->db->insert('merm_pengaturan_diskon_ranap_narcose_bedah',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function get_duplicat_logic_narcose_bedah_ri($idkelompokpasien,$idrekanan,$tujuan_pasien,$idruangan,$idkelas,$idtipe,$idbarang){
		// $idpoli=($idpoli=='#'?0:$idpoli);

		$q="SELECT *FROM merm_pengaturan_diskon_ranap_narcose_bedah 
				WHERE tipe='1' AND idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan'  AND tujuan_pasien='$tujuan_pasien' 
				AND idruangan='$idruangan'  
				AND idkelas='$idkelas' 
				AND idtipe='$idtipe'  AND idbarang='$idbarang' ";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
	
	  function load_narcose_bedah_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idruangan !='0' THEN MR.nama ELSE 'ALL' END as nama_ruangan
						,CASE WHEN H.idkelas !='0' THEN MK.nama ELSE 'ALL' END as nama_kelas
						,CASE WHEN H.idtipe !='0' THEN TB.nama_tipe ELSE 'ALL TIPE' END as tipe_barang
						,CASE WHEN H.idbarang !='0' THEN MB.nama ELSE 'ALL BARANG' END as nama_barang
						,CASE WHEN H.jenis_opr !='0' THEN JO.ref ELSE 'ALL JENIS' END as nama_jenis
						,CASE WHEN H.tujuan_pasien ='1' THEN 'RAWAT INAP' ELSE 'ODS' END as nama_tipe
						,H.* 
						FROM merm_pengaturan_diskon_ranap_narcose_bedah H
						LEFT JOIN mruangan MR ON MR.id=H.idruangan
						LEFT JOIN mkelas MK ON MK.id=H.idkelas
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe
						LEFT JOIN merm_referensi JO ON JO.nilai=H.jenis_opr AND JO.ref_head_id='124'
						LEFT JOIN view_barang_all MB ON MB.idtipe=H.idtipe AND H.idbarang=MB.id
						WHERE H.idkelompokpasien='$idkelompokpasien'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ruangan','nama_kelas','nama_tipe');
			$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
				 $aksi='';
			  $result[] = GetStatusRencana($r->tujuan_pasien);
			  $result[] = ($r->idruangan=='0'?text_default($r->nama_ruangan):$r->nama_ruangan);
			  $result[] = ($r->idkelas=='0'?text_default($r->nama_kelas):$r->nama_kelas);
			  $result[] = ($r->jenis_opr=='0'?text_default('ALL JENIS'):$r->nama_jenis);
			  $result[] = ($r->idtipe=='0'?text_default('ALL TIPE'):$r->tipe_barang);
			  $result[] = ($r->idbarang=='0'?text_default('ALL BARANG'):$r->nama_barang);
			  $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):number($r->diskon,1).$this->jenis_operand($r->operand));
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="hapus_narcose_bedah('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	 
	  function hapus_narcose_bedah_ri(){
		  $id=$this->input->post('id');
		  $this->db->where('id',$id);
		  $hasil=$this->db->delete('merm_pengaturan_diskon_ranap_narcose_bedah',$this);
		  
		  json_encode($hasil);
		  
	  }
	  function simpan_obat_bedah_ri(){
		// print_r($this->input->post());exit;
		$hasil=$this->get_duplicat_logic_obat_bedah_ri($this->input->post('idkelompokpasien'),$this->input->post('idrekanan'),$this->input->post('tujuan_pasien'),$this->input->post('idruangan'),$this->input->post('idkelas'),$this->input->post('idtipe'),$this->input->post('idbarang'));
		if ($hasil==null){
			
			$this->tipe = 1;
			$this->tujuan_pasien = $this->input->post('tujuan_pasien');
			$this->idkelompokpasien = $this->input->post('idkelompokpasien');
			$this->idrekanan = $this->input->post('idrekanan');
			$this->idruangan = $this->input->post('idruangan');
			$this->idkelas = $this->input->post('idkelas');
			$this->jenis_opr = $this->input->post('jenis_opr');
			$this->idtipe = $this->input->post('idtipe');
			$this->idbarang = $this->input->post('idbarang');
			$this->operand = $this->input->post('operand');
			$this->diskon = $this->input->post('diskon');
			$this->status = 1;
	
			
		  $hasil=$this->db->insert('merm_pengaturan_diskon_ranap_obat_bedah',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function get_duplicat_logic_obat_bedah_ri($idkelompokpasien,$idrekanan,$tujuan_pasien,$idruangan,$idkelas,$idtipe,$idbarang){
		// $idpoli=($idpoli=='#'?0:$idpoli);

		$q="SELECT *FROM merm_pengaturan_diskon_ranap_obat_bedah 
				WHERE tipe='1' AND idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan'  AND tujuan_pasien='$tujuan_pasien' 
				AND idruangan='$idruangan'  
				AND idkelas='$idkelas' 
				AND idtipe='$idtipe'  AND idbarang='$idbarang' ";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
	
	  function load_obat_bedah_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idruangan !='0' THEN MR.nama ELSE 'ALL' END as nama_ruangan
						,CASE WHEN H.idkelas !='0' THEN MK.nama ELSE 'ALL' END as nama_kelas
						,CASE WHEN H.idtipe !='0' THEN TB.nama_tipe ELSE 'ALL TIPE' END as tipe_barang
						,CASE WHEN H.idbarang !='0' THEN MB.nama ELSE 'ALL BARANG' END as nama_barang
						,CASE WHEN H.jenis_opr !='0' THEN JO.ref ELSE 'ALL JENIS' END as nama_jenis
						,CASE WHEN H.tujuan_pasien ='1' THEN 'RAWAT INAP' ELSE 'ODS' END as nama_tipe
						,H.* 
						FROM merm_pengaturan_diskon_ranap_obat_bedah H
						LEFT JOIN mruangan MR ON MR.id=H.idruangan
						LEFT JOIN mkelas MK ON MK.id=H.idkelas
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe
						LEFT JOIN merm_referensi JO ON JO.nilai=H.jenis_opr AND JO.ref_head_id='124'
						LEFT JOIN view_barang_all MB ON MB.idtipe=H.idtipe AND H.idbarang=MB.id
						WHERE H.idkelompokpasien='$idkelompokpasien'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ruangan','nama_kelas','nama_tipe');
			$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
				 $aksi='';
			  $result[] = GetStatusRencana($r->tujuan_pasien);
			  $result[] = ($r->idruangan=='0'?text_default($r->nama_ruangan):$r->nama_ruangan);
			  $result[] = ($r->idkelas=='0'?text_default($r->nama_kelas):$r->nama_kelas);
			  $result[] = ($r->jenis_opr=='0'?text_default('ALL JENIS'):$r->nama_jenis);
			  $result[] = ($r->idtipe=='0'?text_default('ALL TIPE'):$r->tipe_barang);
			  $result[] = ($r->idbarang=='0'?text_default('ALL BARANG'):$r->nama_barang);
			  $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):number($r->diskon,1).$this->jenis_operand($r->operand));
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="hapus_obat_bedah('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	 
	  function hapus_obat_bedah_ri(){
		  $id=$this->input->post('id');
		  $this->db->where('id',$id);
		  $hasil=$this->db->delete('merm_pengaturan_diskon_ranap_obat_bedah',$this);
		  
		  json_encode($hasil);
		  
	  }
	  function simpan_alkes_bedah_ri(){
		// print_r($this->input->post());exit;
		$hasil=$this->get_duplicat_logic_alkes_bedah_ri($this->input->post('idkelompokpasien'),$this->input->post('idrekanan'),$this->input->post('tujuan_pasien'),$this->input->post('idruangan'),$this->input->post('idkelas'),$this->input->post('idtipe'),$this->input->post('idbarang'));
		if ($hasil==null){
			
			$this->tipe = 1;
			$this->tujuan_pasien = $this->input->post('tujuan_pasien');
			$this->idkelompokpasien = $this->input->post('idkelompokpasien');
			$this->idrekanan = $this->input->post('idrekanan');
			$this->idruangan = $this->input->post('idruangan');
			$this->idkelas = $this->input->post('idkelas');
			$this->jenis_opr = $this->input->post('jenis_opr');
			$this->idtipe = $this->input->post('idtipe');
			$this->idbarang = $this->input->post('idbarang');
			$this->operand = $this->input->post('operand');
			$this->diskon = $this->input->post('diskon');
			$this->status = 1;
	
			
		  $hasil=$this->db->insert('merm_pengaturan_diskon_ranap_alkes_bedah',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function get_duplicat_logic_alkes_bedah_ri($idkelompokpasien,$idrekanan,$tujuan_pasien,$idruangan,$idkelas,$idtipe,$idbarang){
		// $idpoli=($idpoli=='#'?0:$idpoli);

		$q="SELECT *FROM merm_pengaturan_diskon_ranap_alkes_bedah 
				WHERE tipe='1' AND idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan'  AND tujuan_pasien='$tujuan_pasien' 
				AND idruangan='$idruangan'  
				AND idkelas='$idkelas' 
				AND idtipe='$idtipe'  AND idbarang='$idbarang' ";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
	
	  function load_alkes_bedah_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idruangan !='0' THEN MR.nama ELSE 'ALL' END as nama_ruangan
						,CASE WHEN H.idkelas !='0' THEN MK.nama ELSE 'ALL' END as nama_kelas
						,CASE WHEN H.idtipe !='0' THEN TB.nama_tipe ELSE 'ALL TIPE' END as tipe_barang
						,CASE WHEN H.idbarang !='0' THEN MB.nama ELSE 'ALL BARANG' END as nama_barang
						,CASE WHEN H.jenis_opr !='0' THEN JO.ref ELSE 'ALL JENIS' END as nama_jenis
						,CASE WHEN H.tujuan_pasien ='1' THEN 'RAWAT INAP' ELSE 'ODS' END as nama_tipe
						,H.* 
						FROM merm_pengaturan_diskon_ranap_alkes_bedah H
						LEFT JOIN mruangan MR ON MR.id=H.idruangan
						LEFT JOIN mkelas MK ON MK.id=H.idkelas
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe
						LEFT JOIN merm_referensi JO ON JO.nilai=H.jenis_opr AND JO.ref_head_id='124'
						LEFT JOIN view_barang_all MB ON MB.idtipe=H.idtipe AND H.idbarang=MB.id
						WHERE H.idkelompokpasien='$idkelompokpasien'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ruangan','nama_kelas','nama_tipe');
			$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
				 $aksi='';
			  $result[] = GetStatusRencana($r->tujuan_pasien);
			  $result[] = ($r->idruangan=='0'?text_default($r->nama_ruangan):$r->nama_ruangan);
			  $result[] = ($r->idkelas=='0'?text_default($r->nama_kelas):$r->nama_kelas);
			  $result[] = ($r->jenis_opr=='0'?text_default('ALL JENIS'):$r->nama_jenis);
			  $result[] = ($r->idtipe=='0'?text_default('ALL TIPE'):$r->tipe_barang);
			  $result[] = ($r->idbarang=='0'?text_default('ALL BARANG'):$r->nama_barang);
			  $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):number($r->diskon,1).$this->jenis_operand($r->operand));
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="hapus_alkes_bedah('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	 
	  function hapus_alkes_bedah_ri(){
		  $id=$this->input->post('id');
		  $this->db->where('id',$id);
		  $hasil=$this->db->delete('merm_pengaturan_diskon_ranap_alkes_bedah',$this);
		  
		  json_encode($hasil);
		  
	  }
	  function simpan_implan_bedah_ri(){
		// print_r($this->input->post());exit;
		$hasil=$this->get_duplicat_logic_implan_bedah_ri($this->input->post('idkelompokpasien'),$this->input->post('idrekanan'),$this->input->post('tujuan_pasien'),$this->input->post('idruangan'),$this->input->post('idkelas'),$this->input->post('idtipe'),$this->input->post('idbarang'));
		if ($hasil==null){
			
			$this->tipe = 1;
			$this->tujuan_pasien = $this->input->post('tujuan_pasien');
			$this->idkelompokpasien = $this->input->post('idkelompokpasien');
			$this->idrekanan = $this->input->post('idrekanan');
			$this->idruangan = $this->input->post('idruangan');
			$this->idkelas = $this->input->post('idkelas');
			$this->jenis_opr = $this->input->post('jenis_opr');
			$this->idtipe = $this->input->post('idtipe');
			$this->idbarang = $this->input->post('idbarang');
			$this->operand = $this->input->post('operand');
			$this->diskon = $this->input->post('diskon');
			$this->status = 1;
	
			
		  $hasil=$this->db->insert('merm_pengaturan_diskon_ranap_implan_bedah',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function get_duplicat_logic_implan_bedah_ri($idkelompokpasien,$idrekanan,$tujuan_pasien,$idruangan,$idkelas,$idtipe,$idbarang){
		// $idpoli=($idpoli=='#'?0:$idpoli);

		$q="SELECT *FROM merm_pengaturan_diskon_ranap_implan_bedah 
				WHERE tipe='1' AND idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan'  AND tujuan_pasien='$tujuan_pasien' 
				AND idruangan='$idruangan'  
				AND idkelas='$idkelas' 
				AND idtipe='$idtipe'  AND idbarang='$idbarang' ";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
	
	  function load_implan_bedah_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idruangan !='0' THEN MR.nama ELSE 'ALL' END as nama_ruangan
						,CASE WHEN H.idkelas !='0' THEN MK.nama ELSE 'ALL' END as nama_kelas
						,CASE WHEN H.idtipe !='0' THEN TB.nama_tipe ELSE 'ALL TIPE' END as tipe_barang
						,CASE WHEN H.idbarang !='0' THEN MB.nama ELSE 'ALL BARANG' END as nama_barang
						,CASE WHEN H.jenis_opr !='0' THEN JO.ref ELSE 'ALL JENIS' END as nama_jenis
						,CASE WHEN H.tujuan_pasien ='1' THEN 'RAWAT INAP' ELSE 'ODS' END as nama_tipe
						,H.* 
						FROM merm_pengaturan_diskon_ranap_implan_bedah H
						LEFT JOIN mruangan MR ON MR.id=H.idruangan
						LEFT JOIN mkelas MK ON MK.id=H.idkelas
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe
						LEFT JOIN merm_referensi JO ON JO.nilai=H.jenis_opr AND JO.ref_head_id='124'
						LEFT JOIN view_barang_all MB ON MB.idtipe=H.idtipe AND H.idbarang=MB.id
						WHERE H.idkelompokpasien='$idkelompokpasien'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ruangan','nama_kelas','nama_tipe');
			$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
				 $aksi='';
			  $result[] = GetStatusRencana($r->tujuan_pasien);
			  $result[] = ($r->idruangan=='0'?text_default($r->nama_ruangan):$r->nama_ruangan);
			  $result[] = ($r->idkelas=='0'?text_default($r->nama_kelas):$r->nama_kelas);
			  $result[] = ($r->jenis_opr=='0'?text_default('ALL JENIS'):$r->nama_jenis);
			  $result[] = ($r->idtipe=='0'?text_default('ALL TIPE'):$r->tipe_barang);
			  $result[] = ($r->idbarang=='0'?text_default('ALL BARANG'):$r->nama_barang);
			  $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):number($r->diskon,1).$this->jenis_operand($r->operand));
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="hapus_implan_bedah('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	 
	  function hapus_implan_bedah_ri(){
		  $id=$this->input->post('id');
		  $this->db->where('id',$id);
		  $hasil=$this->db->delete('merm_pengaturan_diskon_ranap_implan_bedah',$this);
		  
		  json_encode($hasil);
		  
	  }
}
