<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvendor extends CI_Controller {

	/**
	 * Vendor controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mvendor_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Vendor';
		$data['content'] 		= 'Mvendor/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Vendor",'#'),
									    			array("List",'mvendor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'alamat' 				=> '',
			'telepon' 			=> '',
			'norekening' 		=> '',
			'atasnama' 			=> '',
			'bank' 					=> '',
			'cabang' 					=> '',
			'bank_id' 					=> '#',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Vendor';
		$data['content'] 		= 'Mvendor/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Vendor",'#'),
									    			array("Tambah",'mvendor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mvendor_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'alamat' 				=> $row->alamat,
					'telepon' 			=> $row->telepon,
					'norekening' 		=> $row->norekening,
					'atasnama' 			=> $row->atasnama,
					'bank' 					=> $row->bank,
					'status' 				=> $row->status,
					'cabang' 				=> $row->cabang,
					'bank_id' 				=> $row->bank_id
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Vendor';
				$data['content']	 	= 'Mvendor/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Vendor",'#'),
											    			array("Ubah",'mvendor')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mvendor','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mvendor');
		}
	}

	function delete($id){
		$this->Mvendor_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mvendor','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mvendor_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvendor','location');
				}
			} else {
				if($this->Mvendor_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvendor','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mvendor/manage';

		if($id==''){
			$data['title'] = 'Tambah Vendor';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Vendor",'#'),
															array("Tambah",'mvendor')
													);
		}else{
			$data['title'] = 'Ubah Vendor';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Vendor",'#'),
															array("Ubah",'mvendor')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$this->select = array();
			$this->from   = 'mvendor';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama','alamat','telepon');
      $this->column_order    = array('nama','alamat','telepon');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->alamat;
          $row[] = $r->telepon;

			$aksi = '';
			// if(button_roles('mvendor/update')) {
				$aksi .= '<a href="'.site_url().'mvendor/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
			// }
			// if(button_roles('mvendor/delete')) {
				$aksi .= '<a href="#" data-urlindex="'.site_url().'mvendor" data-urlremove="'.site_url().'mvendor/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
			// }
			$aksi .= '</div>';
			$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
