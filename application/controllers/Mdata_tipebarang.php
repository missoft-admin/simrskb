<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_tipebarang extends CI_Controller {

    function __construct() {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->model('Mdata_tipebarang_model','tipebarang');
    }
    function load($val) {
        $row=$this->tipebarang->load_by_id($val);
        echo json_encode($row);
    }
    function loadbyjenis($val) {
        $result=$this->tipebarang->load_by_jenis($val);
        echo json_encode($result);
    }
}