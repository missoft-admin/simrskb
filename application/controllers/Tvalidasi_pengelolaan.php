<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_pengelolaan extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tvalidasi_pengelolaan_model','model');
	}

	function index() {
		$data=array(
			'st_posting'=>'#',
			'tipe'=>'#',
			'status_penerimaan'=>'#',
			'nojurnal'=>'',
			'no_terima'=>'',
			'jenis_retur'=>'',
			'status'=>'#',
			'asal_transaksi'=>'',
			'tanggalterima2'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		$data['title'] 			= 'Validasi Jurnal Selish Hutang & Piutang';
		$data['content'] 		= 'Tvalidasi_pengelolaan/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("VValidasi Retur",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		if ($data['st_posting']=='1'){
			$disabel='disabled';
		}
		// print_r($data);exit();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Detail Validasi Akuntansi';
		$data['content'] 		= 'Tvalidasi_pengelolaan/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Retur",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		$batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_pengelolaan WHERE id='1'")->row('batas_batal');
		
		$iddistributor=$this->input->post('iddistributor');
		$nojurnal=$this->input->post('nojurnal');
		$no_terima=$this->input->post('no_terima');
		$jenis_retur=$this->input->post('jenis_retur');
		$status_penerimaan=$this->input->post('status_penerimaan');
		$asal_transaksi=$this->input->post('asal_transaksi');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal LIKE '%".$nojurnal."%' ";
		}
		if ($no_terima !=''){
			$where .=" AND H.notransaksi LIKE '%".$no_terima."%' ";
		}
		// if ($jenis_retur !='#'){
			// $where .=" AND H.jenis_retur='$jenis_retur' ";
		// }
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT DATEDIFF(NOW(), H.posting_date) as umur_posting,H.idtransaksi,
					H.id,H.st_posting,H.nojurnal,H.notransaksi
					,H.tanggal_transaksi,H.tanggal_hitung,H.nama_pengelolaan,H.deskripsi_pengelolaan,H.nominal
					
					FROM tvalidasi_pengelolaan H
					WHERE H.`status` !='0' ".$where."

					ORDER BY H.id DESC
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nojurnal','nama_pengelolaan','notransaksi');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			// if ($r->stverif_kbo>=3){
				// $disabel_btn='disabled';
			// }
			if ($r->umur_posting>$batas_batal){
				$disabel_btn='disabled';
			}
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
           
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            $row[] = $r->nojurnal;
            $row[] = $r->notransaksi;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = $r->nama_pengelolaan;//9
            $row[] = HumanDateShort($r->tanggal_hitung);
            $row[] = $r->deskripsi_pengelolaan;//9
            $row[] = number_format($r->nominal,2);
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING'));
					$aksi .= '<a href="'.site_url().'tpengelolaan/update/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.site_url().'tvalidasi_pengelolaan/detail/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
					if ($r->st_posting=='1'){
					$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
					}else{
					$aksi .= '<button title="Posting" class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';
						
					}
					$aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function nama_jenis($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-danger">HUTANG</span>';
		}elseif($tipe=='2'){
			$tipe='<span class="label label-primary">PIUTANG</span>';
		}else{
			$tipe='<span class="label label-success">PEMBAYARAN</span>';
		}
		
		return $tipe;
	}
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pengelolaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_pengelolaan', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pengelolaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_transaksi_detail($iddet,$tipe='1')
    {
		if ($tipe=='1'){
        $q="SELECT *from tvalidasi_pengelolaan_barang D WHERE D.id='$iddet'";
			
		}else{
			
        $q="SELECT *from tvalidasi_pengelolaan_ganti D WHERE D.id='$iddet'";
		}
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function save_detail(){
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		if($this->model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_pengelolaan/detail/'.$id,'location');
		}
	
	}
	function load_index(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		$q_ringkasan=$this->model->query_ringkasan($id);
        $q = "
			SELECT T.idvalidasi,T.jenis_id,T.jenis_nama,T.idakun,A.noakun,A.namaakun
			,CASE WHEN T.posisi_akun='D' THEN  T.nominal ELSE 0 END as debet
			,CASE WHEN T.posisi_akun='K' THEN  T.nominal ELSE 0 END as kredit
			,T.keterangan,T.id,T.tabel
			FROM (
				".$q_ringkasan."
			) T 
			LEFT JOIN makun_nomor A ON A.id=T.idakun
			ORDER BY T.posisi_akun,T.jenis_id
			";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			$aksi       = '<div class="btn-group">';
			$select_akun='<select name="idakun[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$this->nama_jenis($r->jenis_id).'<input type="hidden" name="iddet[]" value="'.$r->id.'"><input type="hidden" name="idtabel[]" value="'.$r->tabel.'"></td>';
			$tbl .='<td>'.$r->jenis_nama.'</td>';
			$tbl .='<td>'.$r->keterangan.'</td>';
			$tbl .='<td>'.$select_akun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="5" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		$q_ringkasan=$this->model->query_ringkasan($id);
        $q = "
			SELECT T.idvalidasi,T.jenis_id,T.jenis_nama,T.idakun,A.noakun,A.namaakun
			,SUM(CASE WHEN T.posisi_akun='D' THEN  T.nominal ELSE 0 END) as debet
			,SUM(CASE WHEN T.posisi_akun='K' THEN  T.nominal ELSE 0 END) as kredit
			,GROUP_CONCAT(T.keterangan) as keterangan
			FROM (
			".$q_ringkasan."
			) T 
			LEFT JOIN makun_nomor A ON A.id=T.idakun
			GROUP BY T.idakun
			ORDER BY T.posisi_akun,T.jenis_id
			";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		return $hasil;
	}
}
