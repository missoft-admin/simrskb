<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlogic_mutasi extends CI_Controller {

	/**
	 * Pengaturan Logic controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mlogic_mutasi_model');
  }

	function index(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '#',
			'status' 				=> '1'
		);
		$data['error'] 			= '';
		$data['title'] 			= 'Pengaturan Logic Mutasi';
		$data['content'] 		= 'Mlogic_mutasi/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Pengaturan Logic",'#'),
									    			array("List",'Mlogic_mutasi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_edit(){
		$id     = $this->input->post('id');
		$q="SELECT D.*
,U.`name` as user_nama FROM `mlogic_mutasi` D
				LEFT JOIN musers U ON U.id=D.iduser
				WHERE D.id='$id'";
		$arr['detail'] =$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr['detail']));
	}
	function list_user($step,$dari='0',$ke='0')
	{
		$arr['detail'] = $this->mlogic_mutasi_model->list_user($step,$dari,$ke);
		$this->output->set_output(json_encode($arr));
	}
	
	function simpan_detail()
	{
		$id_edit=$this->input->post('id_edit');
		$data=array(
				'iduser'=>$this->input->post('iduser'),
				'step'=>$this->input->post('step'),
				'proses_setuju'=>$this->input->post('proses_setuju'),
				'proses_tolak'=>$this->input->post('proses_tolak'),
				'dari'=>$this->input->post('dari'),
				'ke'=>$this->input->post('ke'),
				'operand'=>$this->input->post('operand'),
				'nominal'=>RemoveComma($this->input->post('nominal')),
				
				
			);
		// if ($data['operand']=='>='){
			// $data['nominal']=0;
		// }else{
			// $data['nominal']=RemoveComma($this->input->post('nominal'));
		// }
		if ($id_edit==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');			
			$result = $this->db->insert('mlogic_mutasi',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');		
			$this->db->where('id',$id_edit);
			$result = $this->db->update('mlogic_mutasi',$data);
		}
       
		
		$this->output->set_output(json_encode($result));
	}
	function simpan_edit()
	{
		$data=array(
			'deskripsi'=>$this->input->post('deskripsi'),
			'tanggal_hari'=>$this->input->post('tanggal_hari'),
			
		);
		$this->db->where('id',$this->input->post('tedit'));
		
       // print_r($data);
		$result = $this->db->update('mlogic_setting',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	
	function hapus_det()
	{
		$id=$this->input->post('id');
		$this->db->where('id',$this->input->post('id'));	
		$data=array(
			'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
		);
       // print_r($data);
	   
		$result = $this->db->update('mlogic_mutasi',$data);
		echo json_encode($result);
	}
	
	
	function load_detail()
    {
		
		$idlogic     = $this->input->post('idlogic');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT D.*,U.`name` as user_nama,
				DR.nama as dari_nama,K.nama as ke_nama
				FROM `mlogic_mutasi` D
				LEFT JOIN msumber_kas DR ON DR.id=D.dari
				LEFT JOIN msumber_kas K ON K.id=D.ke
								LEFT JOIN musers U ON U.id=D.iduser
								WHERE  D.status='1'
								ORDER BY D.dari,D.ke,D.step
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->step;
            
            $row[] = $r->dari_nama;
            $row[] = $r->ke_nama;
            $row[] = (($r->operand=='>=' && $r->nominal=='0')?'BEBAS':$r->operand.' '.number_format($r->nominal,0));
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button  type="button" class="btn btn-xs btn-primary edit" title="Edit"><i class="fa fa-pencil"></i></button>';				
			$aksi 		.= '<button type="button" class="btn btn-xs btn-danger hapus_det" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
            $row[] = $r->user_nama;
			$row[] = ($r->proses_setuju=='1'?'Langsung':'Menunggu User');
            $row[] = ($r->proses_tolak=='1'?'Langsung':'Menunggu User');
			$row[] = $aksi;			
            $row[] = $r->id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function delete($id){
		// print_r($id);exit();
		$this->mlogic_mutasi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mlogic','location');
	}
	function aktifkan($id){
		// print_r($id);exit();
		$this->mlogic_mutasi_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah Aktif kembali.');
		redirect('mlogic','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->mlogic_mutasi_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlogic/update/'.$id,'location');
				}
			} else {
				if($this->mlogic_mutasi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlogic/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mlogic_mutasi/manage';

		if($id==''){
			$data['title'] = 'Tambah Pengaturan Logic';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Logic",'#'),
															array("Tambah",'Mlogic_mutasi')
													);
		}else{
			$data['title'] = 'Ubah Pengaturan Logic';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Logic",'#'),
															array("Ubah",'Mlogic_mutasi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	
}
