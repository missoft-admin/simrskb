<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Hashids\Hashids;
class Ttindakan_ranap_deposit extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Ttindakan_ranap_deposit_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tcheckin_model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Tverifikasi_transaksi_model');
		$this->load->model('Trawatinap_verifikasi_model');
		$this->load->helper('path');
		// print_r('sini');exit;
		
  }
	function index(){
		$log['path_tindakan']='ttindakan_ranap_deposit';
		$this->session->set_userdata($log);
		 get_ppa_login();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('2608'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-30 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Ttindakan_ranap_deposit_model->list_poli();
			$data['list_dokter'] 			= $this->Ttindakan_ranap_deposit_model->list_dokter();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tab_utama'] 			= 3;
			$data['idalasan'] 			= '#';
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['tab_detail'] 			=1;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE()";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Ttindakan_ranap_deposit/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Rawat Inap & ODS",'#'),
												  array("Deposit Pasien Rawat Inap",'ttindakan_ranap_deposit')
												);
			$data_setting=$this->db->query("SELECT *FROM setting_deposit")->row_array();
			$data_setting_label=$this->db->query("SELECT *FROM setting_deposit_label")->row_array();
			$data = array_merge($data,$data_setting_label,$data_setting, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$where_2='';
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$iddokter =$this->input->post('iddokter');
			$idtipe =$this->input->post('idtipe');
			$idtipe_asal =$this->input->post('idtipe_asal');
			$cari_pasien =$this->input->post('cari_pasien');
			$nopendaftaran=$this->input->post('nopendaftaran');
			$idruangan=$this->input->post('idruangan');
			$idkelas=$this->input->post('idkelas');
			$idbed=$this->input->post('idbed');
			$norujukan=$this->input->post('norujukan');
			$iddokter_perujuk=$this->input->post('iddokter_perujuk');
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$idrekanan=$this->input->post('idrekanan');
			$status_tindakan_index=$this->input->post('status_tindakan_index');
			
			$idpoliklinik_array=$this->input->post('idpoliklinik_array');
			if ($idpoliklinik_array){
				$idpoliklinik_array=implode(", ", $idpoliklinik_array);
				$where .=" AND (H.idpoliklinik_asal IN (".$idpoliklinik_array.")) ";
			}
			$iddokter_perujuk_array=$this->input->post('iddokter_perujuk_array');
			if ($iddokter_perujuk_array){
				$iddokter_perujuk_array=implode(", ", $iddokter_perujuk_array);
				$where .=" AND (H.iddokter_perujuk IN (".$iddokter_perujuk_array.")) ";
			}
			$iddokter_dpjp=$this->input->post('iddokter_dpjp');
			if ($iddokter_dpjp){
				$iddokter_dpjp=implode(", ", $iddokter_dpjp);
				$where .=" AND (H.iddokterpenanggungjawab IN (".$iddokter_dpjp.")) ";
			}
			$dpjp_pendukung_array=$this->input->post('dpjp_pendukung_array');
			if ($dpjp_pendukung_array){
				$dpjp_pendukung_array=implode(", ", $dpjp_pendukung_array);
				$where .=" AND (HP.iddokter IN (".$dpjp_pendukung_array.")) ";
			}
			$idkelompokpasien_array=$this->input->post('idkelompokpasien_array');
			if ($idkelompokpasien_array){
				$idkelompokpasien_array=implode(", ", $idkelompokpasien_array);
				$where .=" AND (H.idkelompokpasien IN (".$idkelompokpasien_array.")) ";
			}
			$idrekanan_array=$this->input->post('idrekanan_array');
			if ($idrekanan_array){
				$idrekanan_array=implode(", ", $idrekanan_array);
				$where .=" AND (H.idrekanan IN (".$idrekanan_array.")) ";
			}
			$idruangan_array=$this->input->post('idruangan_array');
			if ($idruangan_array){
				$idruangan_array=implode(", ", $idruangan_array);
				$where .=" AND (H.idruangan IN (".$idruangan_array.")) ";
			}
			$idkelas_array=$this->input->post('idkelas_array');
			if ($idkelas_array){
				$idkelas_array=implode(", ", $idkelas_array);
				$where .=" AND (H.idkelas IN (".$idkelas_array.")) ";
			}
			$idbed_array=$this->input->post('idbed_array');
			if ($idbed_array){
				$idbed_array=implode(", ", $idbed_array);
				$where .=" AND (H.idkelas IN (".$idbed_array.")) ";
			}
			$status_tindakan_array=$this->input->post('status_tindakan_array');
			if ($status_tindakan_array){
				$where .=" AND (";
				$where_tindakan='';
				
				if (in_array('2', $status_tindakan_array)){
					// if ($statuskasir=='1' && $statuspembayaran=='0'){
					$where_tindakan .=($where_tindakan==''?'':' OR ')."(H.statuskasir='1' AND H.statuspembayaran='0') ";
				}
				if (in_array('3', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.st_terima_ranap='1' AND H.status='1') ";
				}
				if (in_array('4', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuspembayaran='1') ";
				}
				if (in_array('5', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuscheckout='1') ";
				}
				$where .=$where_tindakan.")";
			}
			$no_medrec=$this->input->post('no_medrec');
			if ($no_medrec !=''){
				$where .=" AND (H.no_medrec LIKE '%".$no_medrec."%') ";
			}
			$namapasien=$this->input->post('namapasien');
			if ($namapasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$namapasien."%') ";
			}
			
			
			if ($iddokter!='#'){
				$where .=" AND (H.iddokterpenanggungjawab='$iddokter') ";
			}
			if ($idrekanan!='#'){
				$where .=" AND (H.idrekanan='$idrekanan') ";
			}
			if ($idkelompokpasien!='#'){
				$where .=" AND (H.idkelompokpasien='$idkelompokpasien') ";
			}
			if ($iddokter_perujuk!='#'){
				$where .=" AND (H.iddokter_perujuk='$iddokter_perujuk') ";
			}
			if ($idbed!='#'){
				$where .=" AND (H.idbed='$idbed') ";
			}
			if ($idruangan!='#'){
				$where .=" AND (H.idruangan='$idruangan') ";
			}
			if ($idkelas!='#'){
				$where .=" AND (H.idkelas='$idkelas') ";
			}
			if ($idtipe!='#'){
				$where .=" AND (H.idtipe='$idtipe') ";
			}
			if ($idtipe_asal!='#'){
				$where .=" AND (H.idtipe_asal='$idtipe_asal') ";
			}
			if ($norujukan !=''){
				$where .=" AND (H.nopermintaan LIKE '%".$norujukan."%') ";
			}
			if ($nopendaftaran !=''){
				$where .=" AND (H.nopendaftaran LIKE '%".$nopendaftaran."%' OR H.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			}
			
			if ($cari_pasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$cari_pasien."%' OR H.no_medrec LIKE '%".$cari_pasien."%') ";
			}
			// print_r($where);exit;
			$tab =$this->input->post('tab');
			$where .=" AND (H.st_terima_ranap='1' AND H.status='1' AND H.statuscheckout='0') ";
			if ($tab=='2'){//Memiliki Deposit
				$where_2 .=" AND total_deposit > 0 ";
			}
			
			if ($tab=='4'){//Tidak ada Deposit
				$where_2 .=" AND total_deposit <= 0 ";
			}
			
			if ($tab=='5'){//Persetujuan
				$where .=" AND (ST.nama_tindakan IS NOT NULL) ";
			}
			if ($status_tindakan_index=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($status_tindakan_index=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			}
			if ($status_tindakan_index=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($status_tindakan_index=='5'){
				$where .=" AND (H.statuscheckout='1') ";
				if ($tanggal_1 !=''){
					$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
				}else{
					$where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
				}
			}
			
			
			$tanggal_1_trx =$this->input->post('tanggal_1_trx');
			if ($tanggal_1_trx !=''){
				$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2_trx)."'";
			}
			$tanggal_1_pulang =$this->input->post('tanggal_1_pulang');
			if ($tanggal_1_pulang !=''){
				$where .=" AND DATE(H.tanggalcheckout) >='".YMDFormat($tanggal_1_pulang)."' AND DATE(H.tanggalcheckout) <='".YMDFormat($tanggal_2_pulang)."'";
			}
			$this->select = array();
			$get_alergi_sql=get_alergi_sql();
			$from="
					(
						SELECT COALESCE(DP.nominal,0)  as total_deposit,MAX(DH.id) as head_id,
						PB.status_verifikasi_bayar,PB.st_verifikasi_piutang
						,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
						,H.*,MKL.nama as nama_kelas 
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
						,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping
						,ST.nama_tindakan,TT.nama_tindakan_setuju
						FROM trawatinap_pendaftaran H
						LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
						LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
						LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
						LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
						LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mbed MB on MB.id=H.idbed						
						LEFT JOIN musers UH on UH.id=H.deleted_by	
						LEFT JOIN (
							".$get_alergi_sql."
						) A ON A.idpasien=H.idpasien 
						LEFT JOIN (
							SELECT H.idtindakan as pendaftaran_id,H.status_verifikasi as status_verifikasi_bayar,D.st_verifikasi_piutang FROM trawatinap_tindakan_pembayaran H
							LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id AND D.st_verifikasi_piutang=1 AND D.idmetode IN (6,8)
							WHERE H.statusbatal='0'
							GROUP BY H.id

						) PB ON PB.pendaftaran_id=H.id
						LEFT JOIN (
							SELECT DP.idrawatinap,SUM(DP.nominal) as nominal FROM `trawatinap_deposit` DP 
							WHERE  DP.`status`='1' AND DP.st_proses='1'
							GROUP BY DP.idrawatinap
						) DP ON DP.idrawatinap=H.id 
						LEFT JOIN trawatinap_deposit_head DH ON DH.idrawatinap=H.id AND DH.`status`='1'
						LEFT JOIN (
							SELECT ST.idrawatinap,GROUP_CONCAT(CONCAT(ST.deskripsi, ' (',ST.nama_tindakan,')')) as nama_tindakan,ST.st_persetujuan 
							FROM `trawatinap_deposit_head` ST 
							WHERE ST.tipe_deposit AND ST.tipe_deposit='1' AND ST.`status` = '1'
							GROUP BY ST.idrawatinap
						) ST ON ST.idrawatinap=H.id
						LEFT JOIN (
							".get_tindakan_persetujuan()."
						) TT ON TT.idrawatinap=H.id
						
						WHERE H.idtipe IS NOT NULL ".$where."
						GROUP BY H.id
						
						ORDER BY H.tanggaldaftar DESC
					) as tbl WHERE tbl.id IS NOT NULL ".$where_2."
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  $btn_persetujuan_pulang=panel_persetujuan_pulang($r->status_proses_card_pass,$r->nopendaftaran);
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  if ($r->idtipe=='1'){
			  $info_bed='<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>';
		  }else{
			  $info_bed='';
		  }
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_tindakan='';
		  if ($r->nama_tindakan){
			  $btn_tindakan='<div class="push-5-t"><button onclick="show_modal_persetujuan('.$r->id.')"  type="button" data-toggle="tooltip" title="PERSETUJUAN TINDAKAN "  class="btn btn-block btn-success btn-xs"><i class="fa fa-check"></i> PERSETUJUAN TINDAKAN</button> </div>';
		  }
		  $btn_checkout='';
		  $btn_deposit='';
		  $btn_terima='';
		  $btn_baru='';
		  $btn_add='';
		  $btn_trx='';
		  $btn_panel='';
		  $pedamping='';
		  $btn_lock='';
		  $btn_verifikasi='';
		  $btn_verifikasi=($r->status_verifikasi_bayar?text_success('VERIFIED'):'');
		  $btn_verifikasi_piutang='';
		  $btn_verifikasi_piutang='&nbsp;&nbsp;'.($r->st_verifikasi_piutang?text_danger('VERIFIED PIUTANG'):'');
		 
		  // $label_total='Total Keseluruhan : Rp. 1.780.000';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
		  if ($r->status!='0'){
				$btn_baru='<div class="push-5-t"><button onclick="show_modal_deposit_header('.$r->id.')"  type="button" data-toggle="tooltip" title="TINDAKAN BARU"  class="btn btn-block btn-primary btn-xs"><i class="fa fa-plus"></i> TINDAKAN BARU</button> </div>';
				if ($r->head_id){
					$btn_add ='<div class="push-5-t"><button onclick="show_modal_deposit_header_add('.$r->id.','.$r->head_id.')"  type="button" data-toggle="tooltip" title="TRANSAKSI DEPOSIT"  class="btn btn-block btn-warning btn-xs"><i class="fa fa-list-ul"></i> TRANSAKSI DEPOSIT</button> </div>';
				}
				$btn_trx='<div class="push-5-t"><a href="'.base_url().'trawatinap_tindakan/verifikasi/'.$r->id.'" target="_blank" type="button" data-toggle="tooltip" title="Transaksi"  class="btn btn-block btn-success btn-xs"><i class="fa fa-credit-card"></i> TRANSAKSI</a> </div>';
				$btn_deposit='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Deposit" onclick="show_modal_deposit('.$r->id.')"  class="btn btn-block btn-info btn-xs"><i class="fa fa-credit-card"></i> DEPOSIT</button> </div>';
			  // if ($r->st_terima_ranap=='1' && $r->statuspembayaran=='1'){
				  // if (UserAccesForm($user_acces_form,array('1931'))){
					// $btn_checkout='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Checkout" onclick="show_modal_checkout('.$r->id.')" class="btn btn-block btn-danger btn-xs"><i class="si si-logout"></i> DATA CHECKOUT</a> </div>';
				  // }
				 
			  // }
				  $btn_panel=div_panel_kendali_ri_admin($user_acces_form,$r->id);
		  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_baru.'
									'.$btn_add.'
									'.$btn_tindakan.'
									
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_edit_tipe='<button class="btn btn-xs btn-default" onclick="modal_edit_tipe('.$r->id.','.$r->idtipepasien.')" type="button" data-toggle="tooltip" title="Edit Tipe Pasien" data-original-title="Edit Tipe Pasien"><i class="fa fa-pencil"></i></button>';
		  $btn_edit_catatan='<button class="btn btn-xs btn-default" onclick="modal_edit_catatan('.$r->id.')" type="button" data-toggle="tooltip" title="Edit Catatan" data-original-title="Edit Catatan Pasien"><i class="fa fa-pencil"></i></button>';
		  $catatan_pasien='Tipe Pasien : '.($r->idtipepasien=='1'?'Pasien RS':'Pasien Pribadi ').' | Catatan : '.($r->catatan?$r->catatan:' - ');
		  $btn_2='';
		  if ($r->nama_tindakan_setuju){
			  
		  $btn_tindakan_setujua='
			<div class="text-danger font-s13 push-5-t text-bold">
			<button class="btn btn-success  btn-xs" type="button" onclick="show_list_tindakan('.$r->id.')">'.$r->nama_tindakan_setuju.'</button>
			</div>
		  ';
		  }else{
			  $btn_tindakan_setujua='';
		  }
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="font-s13 push-5-t">'.$catatan_pasien.'</div>
									'.$btn_tindakan_setujua.$btn_persetujuan_pulang.'
									<input type="hidden" class="form-control st_tampil" value="0">
									<input type="hidden" class="form-control idranap" value="'.$r->id.'">
									<span class="h5 font-w500 text-primary label_total_deposit">Total Deposit : Rp. '.number_format($r->total_deposit,0).'</span>  &nbsp;&nbsp;&nbsp;
									<span class="h5 font-w500 text-danger label_total_trx"></span>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> DPJP : '.($r->nama_dokter).'</div>
									'.$pedamping.'
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_deposit($r->total_deposit).'</div>
									<div class="h5 push-10-t text-primary"><strong>'.GetTipeRujukanRawatInap($r->idtipe).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_index_all_deposite()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$where='';
			$where_2='';
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$iddokter =$this->input->post('iddokter');
			$idtipe =$this->input->post('idtipe');
			$idtipe_asal =$this->input->post('idtipe_asal');
			$cari_pasien =$this->input->post('cari_pasien');
			$nopendaftaran=$this->input->post('nopendaftaran');
			$idruangan=$this->input->post('idruangan');
			$idkelas=$this->input->post('idkelas');
			$idbed=$this->input->post('idbed');
			$norujukan=$this->input->post('norujukan');
			$iddokter_perujuk=$this->input->post('iddokter_perujuk');
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$idrekanan=$this->input->post('idrekanan');
			$status_tindakan_index=$this->input->post('status_tindakan_index');
			
			$idpoliklinik_array=$this->input->post('idpoliklinik_array');
			if ($idpoliklinik_array){
				$idpoliklinik_array=implode(", ", $idpoliklinik_array);
				$where .=" AND (MP.idpoliklinik_asal IN (".$idpoliklinik_array.")) ";
			}
			$iddokter_perujuk_array=$this->input->post('iddokter_perujuk_array');
			if ($iddokter_perujuk_array){
				$iddokter_perujuk_array=implode(", ", $iddokter_perujuk_array);
				$where .=" AND (MP.iddokter_perujuk IN (".$iddokter_perujuk_array.")) ";
			}
			$iddokter_dpjp=$this->input->post('iddokter_dpjp');
			if ($iddokter_dpjp){
				$iddokter_dpjp=implode(", ", $iddokter_dpjp);
				$where .=" AND (MP.iddokterpenanggungjawab IN (".$iddokter_dpjp.")) ";
			}
			$dpjp_pendukung_array=$this->input->post('dpjp_pendukung_array');
			if ($dpjp_pendukung_array){
				$dpjp_pendukung_array=implode(", ", $dpjp_pendukung_array);
				$where .=" AND (HP.iddokter IN (".$dpjp_pendukung_array.")) ";
			}
			$idkelompokpasien_array=$this->input->post('idkelompokpasien_array');
			if ($idkelompokpasien_array){
				$idkelompokpasien_array=implode(", ", $idkelompokpasien_array);
				$where .=" AND (MP.idkelompokpasien IN (".$idkelompokpasien_array.")) ";
			}
			$idrekanan_array=$this->input->post('idrekanan_array');
			if ($idrekanan_array){
				$idrekanan_array=implode(", ", $idrekanan_array);
				$where .=" AND (MP.idrekanan IN (".$idrekanan_array.")) ";
			}
			$idruangan_array=$this->input->post('idruangan_array');
			if ($idruangan_array){
				$idruangan_array=implode(", ", $idruangan_array);
				$where .=" AND (MP.idruangan IN (".$idruangan_array.")) ";
			}
			$idkelas_array=$this->input->post('idkelas_array');
			if ($idkelas_array){
				$idkelas_array=implode(", ", $idkelas_array);
				$where .=" AND (MP.idkelas IN (".$idkelas_array.")) ";
			}
			$idbed_array=$this->input->post('idbed_array');
			if ($idbed_array){
				$idbed_array=implode(", ", $idbed_array);
				$where .=" AND (MP.idkelas IN (".$idbed_array.")) ";
			}
			$status_tindakan_array=$this->input->post('status_tindakan_array');
			if ($status_tindakan_array){
				$where .=" AND (";
				$where_tindakan='';
				
				if (in_array('2', $status_tindakan_array)){
					// if ($statuskasir=='1' && $statuspembayaran=='0'){
					$where_tindakan .=($where_tindakan==''?'':' OR ')."(MP.statuskasir='1' AND MP.statuspembayaran='0') ";
				}
				if (in_array('3', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(MP.st_terima_ranap='1' AND MP.status='1') ";
				}
				if (in_array('4', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(MP.statuspembayaran='1') ";
				}
				if (in_array('5', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(MP.statuscheckout='1') ";
				}
				$where .=$where_tindakan.")";
			}
			$no_medrec=$this->input->post('no_medrec');
			if ($no_medrec !=''){
				$where .=" AND (MP.no_medrec LIKE '%".$no_medrec."%') ";
			}
			$namapasien=$this->input->post('namapasien');
			if ($namapasien !=''){
				$where .=" AND (MP.namapasien LIKE '%".$namapasien."%') ";
			}
			
			
			if ($iddokter!='#'){
				$where .=" AND (MP.iddokterpenanggungjawab='$iddokter') ";
			}
			if ($idrekanan!='#'){
				$where .=" AND (MP.idrekanan='$idrekanan') ";
			}
			if ($idkelompokpasien!='#'){
				$where .=" AND (MP.idkelompokpasien='$idkelompokpasien') ";
			}
			if ($iddokter_perujuk!='#'){
				$where .=" AND (MP.iddokter_perujuk='$iddokter_perujuk') ";
			}
			if ($idbed!='#'){
				$where .=" AND (MP.idbed='$idbed') ";
			}
			if ($idruangan!='#'){
				$where .=" AND (MP.idruangan='$idruangan') ";
			}
			if ($idkelas!='#'){
				$where .=" AND (MP.idkelas='$idkelas') ";
			}
			if ($idtipe!='#'){
				$where .=" AND (MP.idtipe='$idtipe') ";
			}
			if ($idtipe_asal!='#'){
				$where .=" AND (MP.idtipe_asal='$idtipe_asal') ";
			}
			if ($norujukan !=''){
				$where .=" AND (MP.nopermintaan LIKE '%".$norujukan."%') ";
			}
			if ($nopendaftaran !=''){
				$where .=" AND (MP.nopendaftaran LIKE '%".$nopendaftaran."%' OR MP.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			}
			
			if ($cari_pasien !=''){
				$where .=" AND (MP.namapasien LIKE '%".$cari_pasien."%' OR MP.no_medrec LIKE '%".$cari_pasien."%') ";
			}
			// print_r($where);exit;
			$tab =$this->input->post('tab');
			
			if ($status_tindakan_index=='2'){
				$where .=" AND (MP.statuskasir='1'  AND MP.statuspembayaran='0') ";
			}
			
			if ($status_tindakan_index=='3'){
				$where .=" AND (MP.st_terima_ranap='1' AND MP.status='1') ";
			}
			if ($status_tindakan_index=='4'){
				$where .=" AND (MP.statuspembayaran='1') ";
			}
			if ($status_tindakan_index=='5'){
				$where .=" AND (MP.statuscheckout='1') ";
				if ($tanggal_1 !=''){
					$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
				}else{
					$where .=" AND DATE(MP.tanggaldaftar) >='".date('Y-m-d')."'";
				}
			}
			
			
			$tanggal_1_trx =$this->input->post('tanggal_1_trx');
			if ($tanggal_1_trx !=''){
				$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tanggal_1_trx)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tanggal_2_trx)."'";
			}
			$tanggal_1_pulang =$this->input->post('tanggal_1_pulang');
			if ($tanggal_1_pulang !=''){
				$where .=" AND DATE(MP.tanggalcheckout) >='".YMDFormat($tanggal_1_pulang)."' AND DATE(MP.tanggalcheckout) <='".YMDFormat($tanggal_2_pulang)."'";
			}
			
			$q="SELECT hari_batal_deposit FROM setting_deposit";
			$hari_batal_deposit=$this->db->query($q)->row('hari_batal_deposit');
			$this->select = array();
			$get_alergi_sql=get_alergi_sql();
			$from="
					(
						SELECT COUNT(F.id) as jml_bukti,
						IFNULL(DATEDIFF(NOW(),
								D.tanggal),0) as umur_batal
						,D.id,D.head_id,H.tipe_deposit,H.deskripsi,D.nodeposit,D.created_date,MC.`name` as nama_created,MJ.nama as nama_jenis_deposit
						,D.tanggal
						,(CASE
									WHEN D.idmetodepembayaran = 1 THEN 'Tunai'
									WHEN D.idmetodepembayaran = 2 THEN 'Debit'
									WHEN D.idmetodepembayaran = 3 THEN 'Kredit'
									WHEN D.idmetodepembayaran = 4 THEN 'Transfer'
								END) AS metodepembayaran
						,D.idbank,MB.nama as nama_bank,D.nominal,D.terimadari,D.st_proses,D.status,D.st_approval
						,MD.`name` as nama_hapus,D.deleted_date,D.hapus_proses,MP.no_medrec,MP.namapasien
						FROM trawatinap_pendaftaran MP
						INNER JOIN trawatinap_deposit D  ON MP.id=D.idrawatinap
						LEFT JOIN trawatinap_deposit_head H ON H.id=D.head_id
						
						LEFT JOIN musers MC ON MC.id=D.iduserinput
						LEFT JOIN musers MD ON MD.id=D.iduserdelete
						LEFT JOIN mdeposit_jenis MJ ON MJ.id=D.jenis_deposit_id
						LEFT JOIN mbank MB ON MB.id=D.idbank
					    LEFT JOIN trawatinap_deposit_upload F ON F.deposit_id=D.id
				
						WHERE MP.id IS NOT NULL ".$where."
						GROUP BY D.id
						ORDER BY D.id DESC
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nodeposit');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	  $total=0;
      foreach ($list as $r) {
			
			$no++;
				
			$result = array();
			$btn_edit='';$btn_hapus='';$btn_proses='';$btn_pdf='';$btn_pengajuan_hapus='';$btn_cetak='';$btn_approval='';$btn_hapus_fix='';
			if ($r->jml_bukti > 0){
				$btn_bukti='<button class="btn btn-default btn-xs" title="Bukti" onclick="show_bukti('.$r->id.')" type="button"><i class="fa fa-file-image-o"></i> '.$r->jml_bukti.' Upload</button>';
			}else{
				$btn_bukti='<button class="btn btn-warning btn-xs" title="Bukti" onclick="show_bukti('.$r->id.')" type="button"><i class="fa fa-file-image-o"></i> Upload Bukti</button>';
				
			}
			if ($r->status=='1'){
				if ($r->st_proses=='1'){
				$total=$total + $r->nominal;
					
				}
				
				if ($r->st_proses=='0'){
					$btn_edit='<button class="btn btn-success" title="Edit" onclick="edit_deposit('.$r->id.','.$r->head_id.')" type="button"><i class="fa fa-pencil"></i></button>';
					if ($r->umur_batal <= $hari_batal_deposit){
						$btn_hapus='<button class="btn btn-danger" title="Hapus Draft" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
					}
					
					$btn_proses='<button class="btn btn-primary" title="Generate / Proses" onclick="proses_deposit('.$r->id.')" type="button"><i class="fa fa-file-pdf-o"></i> GENERATE</button>';
				}else{
					if ($r->hapus_proses=='1'){
						$btn_approval='<button class="btn btn-danger" title="Lihat Proses Approval" onclick="list_user('.$r->id.')" type="button"><i class="si si-user-following"></i></button>';
						if ($r->st_approval=='1'){
							$btn_hapus_fix='<button class="btn btn-danger" title="Hapus Deposit" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
						}
						if ($r->st_approval=='2'){
							$btn_pdf='<a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
							// $btn_hapus_fix='<button class="btn btn-danger" title="Hapus Deposit" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
						}
						
					}else{
						$btn_pdf='<a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
						if ($r->umur_batal <= $hari_batal_deposit){
							$btn_pengajuan_hapus='<button class="btn btn-danger" title="Hapus" onclick="load_user_approval('.$r->id.')" type="button"><i class="si si-ban"></i> BATAL</button>';
						}
					}
				}
				
				$aksi='
					<div class="btn-group btn-group-xs" role="group">
						'.$btn_edit.'
						'.$btn_hapus.'
						'.$btn_proses.'
						'.$btn_pdf.'
						'.$btn_pengajuan_hapus.'
						'.$btn_cetak.'
						'.$btn_approval.'
						'.$btn_hapus_fix.'
					</div>
				';
			}else{
				// if ($r->hapus_proses=='1'){
				// $btn_pdf='<br><a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-xs btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
				// }
				$aksi='Deleted By : '.$r->nama_hapus.'<br>'.HumanDateLong($r->deleted_date);
			}
			
			$result[] = $aksi;
			$result[] = ($r->no_medrec.' - '.$r->namapasien);
			$result[] = $r->nodeposit.'<br>'.$r->nama_created.'<br>'.HumanDateLong($r->created_date);
			$result[] = ($r->nama_jenis_deposit);
			$result[] = HumanDateShort($r->tanggal);
			$result[] = ($r->metodepembayaran);
			$result[] = ($r->nama_bank);
			$result[] = number_format($r->nominal,0);
			$result[] = ($r->terimadari);
			$result[] = $btn_bukti;
			$status_text='';
			if ($r->status=='1'){
				if ($r->hapus_proses=='1'){
					if ($r->st_approval=='0'){
						$status_text .=text_warning('MENUGGU APPROVAL');
					}
					if ($r->st_approval=='2'){
						$status_text .=text_danger('APPROVAL DITOLAK').'<br><br>'.text_success('TELAH DIPROSES');
					}
					if ($r->st_approval=='1'){
						$status_text .=text_primary('DISETUJUI');
					}
					
				}else{
					$status_text .=($r->st_proses=='1'?text_success('TELAH DIPROSES'):text_default('BELUM DIPROSES'));
				}
			}else{
			$status_text .=text_danger('DIBATALKAN');
				
			}
			$result[] = ($status_text);
			$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  public function saveDataDeposit()
	{
		
		$data = [];
		$edit_id = $this->input->post('edit_id');
		$st_proses = $this->input->post('st_proses');
		$data['idrawatinap'] = $this->input->post('idrawatinap');
		$data['tanggal'] = YMDFormat($this->input->post('tanggal'));
		$data['idmetodepembayaran'] = $this->input->post('idmetodepembayaran');
		$data['idbank'] = $this->input->post('idbank');
		$data['nominal'] = RemoveComma($this->input->post('nominal'));
		$data['terimadari'] = $this->input->post('terimadari');
		$data['idtipe'] = $this->input->post('idtipe');
		$data['head_id'] = $this->input->post('head_id');
		$data['st_proses'] = $this->input->post('st_proses');
		$data['jenis_deposit_id'] = $this->input->post('jenis_deposit_id');
		if ($st_proses=='1'){
			$data['statusverifikasi'] = 1;
			$data['generate_by'] = $this->session->userdata('user_id');
			$data['generate_date'] = date('Y-m-d H:i:s');
		}
		
		if ($edit_id==''){
			$data['iduserinput'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');
		
			if ($this->db->insert('trawatinap_deposit', $data)) {
				if ($st_proses=='1'){
					$assesmen_id=$this->db->insert_id();
					if ($st_proses=='1'){
						$this->Tverifikasi_transaksi_model->insert_jurnal_deposit($assesmen_id);
						
					}
					$this->cetak_deposit($assesmen_id,1);
				}
				return true;
			} else {
				return false;
			}
		}else{
			if ($st_proses=='1'){
				$this->Tverifikasi_transaksi_model->insert_jurnal_deposit($edit_id);
			}
			$data['iduseredit'] = $this->session->userdata('user_id');
			$data['edited_date'] = date('Y-m-d H:i:s');
		
			$this->db->where('id',$edit_id);
			if ($this->db->update('trawatinap_deposit', $data)) {
				$this->cetak_deposit($edit_id,1);
				return true;
			} else {
				return false;
			}
		}
	}
	
  function get_data_deposit(){
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $q="SELECT H.no_medrec,CONCAT(H.title,' ',H.namapasien) as nama_pasien,H.nohp,H.namapenanggungjawab,H.teleponpenanggungjawab,
		KP.nama as nama_kp,CASE WHEN H.idkelompokpasien='1' THEN  MR.nama ELSE '' END as nama_rekanan,MK.nama as nama_kelas,MB.nama as nama_bed,R.nama as nama_ruangan
		,H.* FROM `trawatinap_pendaftaran` H
		LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
		LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
		LEFT JOIN mkelas MK ON MK.id=H.idkelas
		LEFT JOIN mbed MB ON MB.id=H.idbed
		LEFT JOIN mruangan R ON R.id=H.idruangan
		WHERE H.id='$pendaftaran_id'";
		
		$result=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($result));
		
  }
  function simpan_setuju_tindakan(){
	  $id=$this->input->post('id');
	  $q="SELECT RB.nopermintaan,H.perencanaan_id
			FROM trawatinap_deposit_head H
			LEFT JOIN tpoliklinik_ranap_rencana_biaya RB ON RB.assesmen_id=H.perencanaan_id
			WHERE H.id='$id'";
	  $row=$this->db->query($q)->row();
	  $nopermintaan=$row->nopermintaan;
	  $perencanaan_id=$row->perencanaan_id;
	  $setuju_date=YMDTimeFormat($this->input->post('setuju_date'));
	  $data=array(
		'st_persetujuan' => 1,
		'setuju_user_id' => $this->session->userdata('user_id'),
		'setuju_date' => $setuju_date,
		'nopermintaan' => $nopermintaan,
	  );
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('trawatinap_deposit_head',$data);
	  $this->cetak_estimasi_biaya($perencanaan_id,$id,1);
	  $this->output->set_output(json_encode($hasil));
  }
  function get_data_tindakan(){
	  $idpersetujuan=$this->input->post('idpersetujuan');
	  $q="SELECT ST.idrawatinap,(CONCAT(ST.deskripsi, ' (',ST.nama_tindakan,')')) as nama_tindakan,ST.st_persetujuan,ST.setuju_date,MU.`name` as nama_user,ST.created_date ,ST.id
		FROM `trawatinap_deposit_head` ST 
		LEFT JOIN musers MU ON MU.id=ST.setuju_user_id
		WHERE ST.tipe_deposit AND ST.tipe_deposit='1' AND ST.`status` = '1' AND ST.id='$idpersetujuan'";
		
		$result=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($result));
		
  }
  function status_deposit($total_deposit){
	  if ($total_deposit=='0'){
		  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button"><i class="fa fa-times pull-left "></i>BELUM MEMILIKI DEPOSIT</button>';
	  }else{
			  $label='<button class="btn btn-xs btn-block btn-success push-10" type="button">MEMILIKI DEPOSIT</button>';
	  }
	  
	  return $label;
  }
  function update_kunjungan(){
	  $pendaftaran_id_lama=$this->input->post('pendaftaran_id_lama');
	  $pendaftaran_id_baru=$this->input->post('pendaftaran_id_baru');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$q="UPDATE tpoliklinik_pendaftaran SET statusrencana='0' WHERE id='$pendaftaran_id_lama'";
		$this->db->query($q);
		$q="UPDATE tpoliklinik_pendaftaran SET statusrencana='1' WHERE id='$pendaftaran_id_baru'";
		$this->db->query($q);
		$q="UPDATE trawatinap_pendaftaran SET idpoliklinik='$pendaftaran_id_baru' WHERE id='$pendaftaran_id_ranap'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function update_tipe_pasein(){
	  $id=$this->input->post('id');
	  $idtipepasien=$this->input->post('idtipepasien');
	 
		$q="UPDATE trawatinap_pendaftaran SET idtipepasien='$idtipepasien' WHERE id='$id'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function create_tindakan_new(){
		$idrawatinap=$this->input->post('idrawatinap');
		$tipe_deposit=$this->input->post('tipe_deposit');
		$nourut=$this->input->post('nourut');
		$nama_tindakan=$this->input->post('nama_tindakan');
		$deskripsi=$this->input->post('deskripsi');
		$diagnosa=$this->input->post('diagnosa');
		$total_estimasi=$this->input->post('total_estimasi');
		$persentase_acuan=$this->input->post('persentase_acuan');
		$nominal_acuan=$this->input->post('nominal_acuan');
		$perencanaan_id=$this->input->post('perencanaan_id');
		$perencanaan_nama=$this->input->post('perencanaan_nama');
		$deskripsi=$this->input->post('deskripsi');
		$data=array(
			'idrawatinap' => $idrawatinap,
			'tipe_deposit' => $tipe_deposit,
			'nourut' => $nourut,
			'nama_tindakan' => $nama_tindakan,
			'deskripsi' => $deskripsi,
			'diagnosa' => $diagnosa,
			'total_estimasi' => $total_estimasi,
			'persentase_acuan' => $persentase_acuan,
			'nominal_acuan' => $nominal_acuan,
			'perencanaan_id' => $perencanaan_id,
			'perencanaan_nama' => $perencanaan_nama,
			'deskripsi' => $deskripsi,

		);
		$data['created_by'] = $this->session->userdata('user_id');
		$data['created_date'] = date('Y-m-d H:i:s');
		$this->db->insert('trawatinap_deposit_head',$data);
		$hasil['id']=$this->db->insert_id();
		$this->output->set_output(json_encode($hasil));
  }
  function update_catatan(){
	  $id=$this->input->post('id');
	  $catatan=$this->input->post('catatan');
	 
		$q="UPDATE trawatinap_pendaftaran SET catatan='$catatan' WHERE id='$id'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function get_catatan(){
	  $id=$this->input->post('id');
	 
		$q="SELECT catatan FROM trawatinap_pendaftaran WHERE id='$id'";
		$hasil=$this->db->query($q)->row_array();
		
		$this->output->set_output(json_encode($hasil));
  }
	function set_lock(){
	  $id=$this->input->post('id');
		$statuskasir=$this->input->post('statuskasir');
		$data=array(
			'statuskasir' =>$statuskasir,

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_pendaftaran',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  function hapus_deposit(){
	    $id=$this->input->post('id');
		$this->Tverifikasi_transaksi_model->remove_jurnal_deposit($id);
	    $alasanhapus=$this->input->post('alasanhapus');
		$data=array(
			'statusverifikasi' =>0,
			'status' =>0,
			'alasanhapus' => $alasanhapus,
			'iduserdelete' => $this->session->userdata('user_id'),
			'deleted_date' => date('Y-m-d H:i:s'),

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_deposit',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  function hapus_header_deposit(){
	    $id=$this->input->post('id');
		$data=array(
			'status' =>0,
			'deleted_by' => $this->session->userdata('user_id'),
			'deleted_date' => date('Y-m-d H:i:s'),

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_deposit_head',$data);
		
		$this->db->where('head_id',$id);
		$this->db->where('st_proses','1');
		$result=$this->db->update('trawatinap_deposit',array('status'=>0));
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  function proses_deposit(){
	    $id=$this->input->post('id');
		$data=array(
			'statusverifikasi' =>1,
			'st_proses' =>1,
			'generate_by' => $this->session->userdata('user_id'),
			'generate_date' => date('Y-m-d H:i:s'),

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_deposit',$data);
		$this->Tverifikasi_transaksi_model->insert_jurnal_deposit($id);
		$this->cetak_deposit($id,1);
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  public function get_data_pasien(){
	  $idrawatinap=$this->input->post('idrawatinap');
	  $q="
		SELECT H.id,H.idpasien,CONCAT(H.namapasien,' - ',H.no_medrec,' - ',JK.ref) as nama,H.namapasien 
		,CONCAT(DATE_FORMAT(H.tanggal_lahir,'%d-%m-%Y'), ' - ',H.umurtahun,' Tahun ',H.umurbulan,' Bulan ',H.umurhari,' Hari') as umur
		,PK.nama as nama_kelompok
		,CASE WHEN H.idkelompokpasien='1' THEN MR.nama ELSE '' END as nama_asuransi
		,MD.nama as nama_dokter
		,CASE WHEN H.idtipe='1' THEN CONCAT('RAWAT INAP',' (',H.nopendaftaran,')')  ELSE CONCAT('ODS ',' (',H.nopendaftaran,')') END as nama_pelayanan
		,CONCAT(COALESCE(R.nama,''),' ',COALESCE(K.nama,''),' ',COALESCE(MB.nama,'')) as ruangan
		,DATE_FORMAT(H.tanggaldaftar,'%d-%m-%Y') as tanggaldaftar,H.idkelas
		FROM `trawatinap_pendaftaran` H
		LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
		LEFT JOIN mpasien_kelompok PK ON PK.id=H.idkelompokpasien
		LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
		LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
		LEFT JOIN mruangan R ON R.id=H.idruangan
		LEFT JOIN mkelas K ON K.id=H.idkelas
		LEFT JOIN mbed MB ON MB.id=H.idbed
		WHERE H.id='$idrawatinap'
	  
	  ";
	  $hasil=$this->db->query($q)->row_array();
	  
	  $this->output->set_output(json_encode($hasil));
  }
  function get_data_konservatif(){
	  $idrawatinap=$this->input->post('idrawatinap');
	  $q="SELECT H.perencanaan_id,A.dengan_diagnosa ,S.kali_deposit
			,H.idkelompokpasien,H.idruangan,H.idkelas
			FROM `trawatinap_pendaftaran` H
			LEFT JOIN tpoliklinik_ranap_perencanaan A ON A.assesmen_id=H.perencanaan_id
			,setting_deposit S
			WHERE H.id='$idrawatinap'
		";
		$hasil=$this->db->query($q)->row_array();
		$idkelompokpasien=$hasil['idkelompokpasien'];
		$idruangan=$hasil['idruangan'];
		$idkelas=$hasil['idkelas'];
		$kali_deposit=$hasil['kali_deposit'];
		$tarif=$this->Trawatinap_tindakan_model->getTarifRuangan($idkelompokpasien, $idruangan, $idkelas);
		$nilai_acuan=$kali_deposit * $tarif['total'];
		$hasil['dengan_diagnosa']=str_replace("&nbsp;","", strip_tags($hasil['dengan_diagnosa']));
		$hasil['nilai_acuan']=$nilai_acuan;
		// print_r($tarif['total']);exit;
		$this->output->set_output(json_encode($hasil));
	  
  }
  public function load_data_estimasi(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $q="
		SELECT H.assesmen_id,H.total_estimasi,H.dengan_diagnosa,H.rencana_tindakan 
		,UC.nama as user_created,S.persen_deposit,H.total_estimasi*S.persen_deposit/100 as minimal_acuan
		,H.nopermintaan,H.created_date
		FROM tpoliklinik_ranap_rencana_biaya H
		LEFT JOIN mppa UC ON UC.id=H.created_ppa
		,setting_deposit S
			WHERE H.assesmen_id='$assesmen_id'
	  
	  ";
	  $hasil=$this->db->query($q)->row_array();
	  $hasil['dengan_diagnosa']=str_replace("&nbsp;","", strip_tags($hasil['dengan_diagnosa']));
	  $hasil['rencana_tindakan']=str_replace("&nbsp;","", strip_tags($hasil['rencana_tindakan']));
	  $hasil['created_date']=HumanDateShort($hasil['created_date']);
	  // $hasil['rencana_tindakan']=strip_tags($hasil['rencana_tindakan']);
	  $this->output->set_output(json_encode($hasil));
  }
  public function get_tipe(){
	  $tipe_deposit=$this->input->post('tipe_deposit');
	  $idrawatinap=$this->input->post('idrawatinap');
	  $q="
		SELECT  
		COALESCE(COUNT(H.id)) + 1 as jml_trx
		FROM trawatinap_deposit_head H
		WHERE H.idrawatinap='$idrawatinap' AND H.tipe_deposit='$tipe_deposit' AND H.status='1'
	  
	  ";
	  $jml_trx=$this->db->query($q)->row('jml_trx');
	  if ($tipe_deposit=='0'){
		$hasil['deskripsi']='';
		  
	  }else{
		$hasil['deskripsi']=($tipe_deposit=='1'?'Tindakan Ke ':'Konservatif Ke ').$jml_trx;
		  
	  }
	  $this->output->set_output(json_encode($hasil));
  }
  public function load_data_head(){
	  $head_id=$this->input->post('head_id');
	  $q="
		SELECT H.*
		FROM trawatinap_deposit_head H
		WHERE H.id='$head_id'
	  
	  ";
	  $hasil=$this->db->query($q)->row_array();
	  
	  $this->output->set_output(json_encode($hasil));
  }
  public function get_total_proses(){
	  $idrawatinap=$this->input->post('idrawatinap');
	  $head_id=$this->input->post('head_id');
	  $q="
		SELECT SUM(H.nominal) as nominal FROM trawatinap_deposit H
		WHERE H.idrawatinap='$idrawatinap' AND H.head_id='$head_id' AND H.`status`='1' AND H.st_proses='1'
	  
	  ";
	  // print_r($q);exit;
	  $row=$this->db->query($q)->row();
	  if ($row){
		  $hasil['nominal']=$row->nominal;
	  }else{
		  $hasil['nominal']=0;
	  }
	  
	  $this->output->set_output(json_encode($hasil));
  }
  public function edit_deposit(){
	  $id=$this->input->post('id');
	  $q="
		SELECT D.id,D.head_id,H.tipe_deposit,H.deskripsi,D.nodeposit,D.created_date,MC.`name` as nama_created,MJ.nama as nama_jenis_deposit
			,D.tanggal,D.idmetodepembayaran,D.jenis_deposit_id
			,(CASE
						WHEN D.idmetodepembayaran = 1 THEN 'Tunai'
						WHEN D.idmetodepembayaran = 2 THEN 'Debit'
						WHEN D.idmetodepembayaran = 3 THEN 'Kredit'
						WHEN D.idmetodepembayaran = 4 THEN 'Transfer'
					END) AS metodepembayaran
			,D.idbank,MB.nama as nama_bank,D.nominal,D.terimadari,D.st_proses,D.status
			FROM trawatinap_deposit_head H
			INNER JOIN trawatinap_deposit D ON H.id=D.head_id
			LEFT JOIN musers MC ON MC.id=D.iduserinput
			LEFT JOIN mdeposit_jenis MJ ON MJ.id=D.jenis_deposit_id
			LEFT JOIN mbank MB ON MB.id=D.idbank

			WHERE D.id='$id'
	  
	  ";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q)->row_array();
	  $hasil['tanggal']=HumanDateShort($hasil['tanggal']);
	  
	  $this->output->set_output(json_encode($hasil));
  }
  public function get_alasan(){
	  $id=$this->input->post('id');
	  $q="
		SELECT *FROM trawatinap_deposit D

			WHERE D.id='$id'
	  
	  ";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
  }
  function list_estimasi_admin(){
	
	$where='';
	$idpasien=$this->input->post('idpasien');
	
	$this->select = array();
	$from="
			(
				SELECT 
				DATEDIFF(NOW(), H.created_date) as selisih,C.ref as nama_cito,I.ref as nama_icu
				,MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
				,D.pasien_ttd,MK.nama as nama_kelas
				FROM tpoliklinik_ranap_rencana_biaya H
				LEFT JOIN tpoliklinik_ranap_rencana_biaya_detail D ON D.assesmen_id=H.assesmen_id
				INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
				LEFT JOIN mdokter MD ON MD.id=H.dpjp
				LEFT JOIN mkelas MK ON MK.id=D.kelas
				LEFT JOIN merm_referensi C ON C.nilai=H.cito AND C.ref_head_id='123'
				LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
				LEFT JOIN mppa UC ON UC.id=H.created_ppa
				LEFT JOIN mppa UE ON UE.id=H.edited_ppa
				WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.status_persetujuan_pasien NOT IN (0,3)
				ORDER BY H.assesmen_id DESC
			) as tbl
		";
	// print_r($from);exit();
	$this->from   = $from;
	$this->join 	= array();
	
	
	$this->order  = array();
	$this->group  = array();
	$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
	$this->column_order  = array();

	$list = $this->datatable->get_datatables(true);
	$data = array();
	$no = $_POST['start'];
	
foreach ($list as $r) {	
	$no++;
	if ($r->status_proses=='1' && $r->status_persetujuan_pasien=='2'){
		$aksi='<button class="btn btn-xs btn-success" type="button" title="Pilih Estimasi Biaya" onclick="pilih_estimasi('.$r->assesmen_id.')"><i class="fa fa-check"></i> Pilih</button>';
	}else{
		$aksi='';
	}
	
	$no++;
		
	$result = array();
		
	$result[] = $aksi;
	$result[] = number_format($r->total_estimasi,0);
	$result[] = '<span><strong>'.($r->nopermintaan).'</strong></span>'.'<br><span class="text-muted">'.($r->user_created).'<br>'.HumanDateLong($r->created_date).'</span>';
	$result[] = ($r->dengan_diagnosa);
	$result[] = ($r->rencana_tindakan);
	$result[] = ($r->nama_dpjp);
	$result[] = status_estimasi_biaya($r->status_proses,$r->status_persetujuan_pasien,$r->alasan_menolak,$r->nama_kelas).($r->alasan_menolak?'<br>'.$r->alasan_menolak:'');
	$data[] = $result;
  }
  $output = array(
	  "draw" => $_POST['draw'],
	  "recordsTotal" => $this->datatable->count_all(true),
	  "recordsFiltered" => $this->datatable->count_all(true),
	  "data" => $data
  );
  echo json_encode($output);
  }
  public function cetak_deposit($assesmen_id,$st_create='0'){
	// use Dompdf\Options;
	$options = new Options();
	$options->set('is_remote_enabled',true);
	$dompdf = new Dompdf($options);
	$data=array();
	$q="SELECT 
		*

		FROM setting_deposit_label ";
	$data_header=$this->db->query($q)->row_array();
	
	$q="SELECT 
		*

		FROM setting_deposit ";
	$data_setting=$this->db->query($q)->row_array();
	$data_dek=$data_setting['desk_deposit'];
	$q="SELECT 
			MR.nama as nama_ruangan,MK.nama as nama_kelas
			,D.id,D.head_id,H.tipe_deposit,H.deskripsi,D.nodeposit,D.created_date,MC.`name` as nama_created,ME.`name` as nama_edited,MJ.nama as nama_jenis_deposit
			,D.tanggal,D.edited_date
			,(CASE
						WHEN D.idmetodepembayaran = 1 THEN 'Tunai'
						WHEN D.idmetodepembayaran = 2 THEN 'Debit'
						WHEN D.idmetodepembayaran = 3 THEN 'Kredit'
						WHEN D.idmetodepembayaran = 4 THEN 'Transfer'
					END) AS metodepembayaran
			,D.idbank,MB.nama as nama_bank,D.nominal,D.terimadari,D.st_proses,D.status,MP.nopendaftaran,D.iduserinput,D.iduseredit,D.generate_by,D.generate_date
			,MP.namapasien,MP.no_medrec
			FROM trawatinap_deposit_head H
			
			INNER JOIN trawatinap_deposit D ON H.id=D.head_id
			LEFT JOIN trawatinap_pendaftaran MP ON MP.id=D.idrawatinap
			LEFT JOIN mkelas MK ON MK.id=MP.idkelas
			LEFT JOIN mruangan MR ON MR.id=MP.idruangan
			LEFT JOIN musers MC ON MC.id=D.iduserinput
			LEFT JOIN musers ME ON ME.id=D.iduseredit
			LEFT JOIN mdeposit_jenis MJ ON MJ.id=D.jenis_deposit_id
			LEFT JOIN mbank MB ON MB.id=D.idbank

			WHERE D.id='$assesmen_id'";
	$data_assesmen=$this->db->query($q)->row_array();
	$data_assesmen['title']=$data_assesmen['nodeposit'];
	// $data_assesmen['judul_surat']='';
	// print_r($data);exit;
	$data_assesmen['untuk']=$this->parser->parse_string($data_dek,$data_assesmen);
	// print_r($data_kawan);exit;
	$data = array_merge($data,$data_assesmen,$data_header, backend_info());
	
	$html = $this->load->view('Ttindakan_ranap_deposit/pdf_kwitansi', $data,true);
	// print_r($html);exit;
	$dompdf->loadHtml($html);
	
	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');

	// Render the HTML as PDF
	$dompdf->render();

	// // Output the generated PDF to Browser
	if ($st_create=='1'){
		$nama_file=$data_assesmen['title'].'.pdf';
		$uploadDir_asset = './assets/upload/deposit/';
		$file_hapus=$uploadDir_asset.$nama_file;
		// unlink($file_hapus);
		$output = $dompdf->output();
		file_put_contents($uploadDir_asset.$nama_file, $output);     
		$this->output->set_content_type($html);
		if ($data_assesmen['st_proses']=='1'){
		$this->kirim_email($assesmen_id);
			
		}
	}else{
	$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		
	}
}
public function kirim_email($id)
    {
		$q="SELECT email_deposit FROM setting_deposit";
		$list_email=$this->db->query($q)->row('email_deposit');
		
        $emailArray = explode(',', $list_email);
		$q="
			SELECT H.judul_ina as judul
			,CONCAT('<p>','Kepada Yth, Rekan Sejawat <br>Terlampir Kwitansi Deposit, Atas Perhatiannya Kami ucapkan terima kasih</p>') as bodi
			,'RSKB Halmahera Siaga' as footer_email FROM setting_deposit_label H LIMIT 1
		";
		$pengaturan_email=$this->db->query($q)->row_array();
		// $this->output->set_output(json_encode($hasil));
        $q="SELECT *FROM trawatinap_deposit WHERE id='$id'";
		
        $data_transaksi = $this->db->query($q)->row();
      
       $nama_file=$data_transaksi->nodeposit.'.pdf';
	$uploadDir_asset = './assets/upload/deposit/';
	$mail = new PHPMailer(true);
	$file_hapus=$uploadDir_asset.$nama_file;
	try {
		// Server settings
		$this->configureEmail($mail);

		// Recipients
		$mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
		foreach ($emailArray as $email) {
			$mail->addAddress($email, $data_transaksi->nodeposit);
		}
		$mail->addAttachment($uploadDir_asset.$nama_file);


		// Content
		$bodyHTML = $pengaturan_email['bodi'];

		$mail->isHTML(true);
		$mail->Subject = strip_tags($pengaturan_email['judul']).' '.$data_transaksi->nodeposit;
		$mail->Body = $bodyHTML;

		$mail->send();

		$response = [
			'status' => true,
			'message' => 'success',
		];
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();

		exit;
	} catch (Exception $e) {
		$response = [
			'status' => false,
			'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
		];

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();

		exit;
	}
 }
 public function kirim_email_tindakan($id)
    {
		$q="SELECT email_tindakan as email_deposit,judul_email FROM setting_deposit";
		
		$row=$this->db->query($q)->row();
		$list_email=$row->email_deposit;
		$judul_email=$row->judul_email;
		
        $emailArray = explode(',', $list_email);
		$q="
			SELECT H.judul_ina as judul
			,CONCAT('<p>','Kepada Yth, Rekan Sejawat <br>Terlampir Acc Tindakan, Atas Perhatiannya Kami ucapkan terima kasih</p>') as bodi
			,'RSKB Halmahera Siaga' as footer_email FROM setting_deposit_label H LIMIT 1
		";
		$pengaturan_email=$this->db->query($q)->row_array();
		// $this->output->set_output(json_encode($hasil));
        $q="SELECT CONCAT(MP.title,'. ',MP.namapasien) as namapasien,MP.no_medrec,H.nopermintaan,MP.nopendaftaran FROM trawatinap_deposit_head H
			LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
			WHERE H.id='$id'";
		
        $data_transaksi = $this->db->query($q)->row_array();
		$data_untuk_email=$this->parser->parse_string($judul_email,$data_transaksi);
       $nama_file=$data_transaksi['nopermintaan'].'.pdf';
	$uploadDir_asset = './assets/upload/deposit/';
	$mail = new PHPMailer(true);
	$file_hapus=$uploadDir_asset.$nama_file;
	try {
		// Server settings
		$this->configureEmail($mail);

		// Recipients
		$mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
		foreach ($emailArray as $email) {
			$mail->addAddress($email, $data_transaksi['nopermintaan']);
		}
		$mail->addAttachment($uploadDir_asset.$nama_file);


		// Content
		$bodyHTML = $pengaturan_email['bodi'];

		$mail->isHTML(true);
		$mail->Subject = $data_untuk_email;
		$mail->Body = $bodyHTML;

		$mail->send();

		$response = [
			'status' => true,
			'message' => 'success',
		];
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();

		exit;
	} catch (Exception $e) {
		$response = [
			'status' => false,
			'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
		];

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();

		exit;
	}
 }
  private function configureEmail($mail)
    {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $mail->Host = 'mail.halmaherasiaga.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'erm@halmaherasiaga.com';
        $mail->Password = '{T,_Xun}9{@5';
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port = 465;
    }
private function sendEmail($pengaturan_email, $data_transaksi, $emailArray, $id_log)
{
	// print_r($data_transaksi);exit;
	
}
  public function list_head(){
	  $idrawatinap=$this->input->post('idrawatinap');
	  $q="
		SELECT *FROM trawatinap_deposit_head H
			WHERE H.idrawatinap='$idrawatinap' AND H.status='1' ORDER BY H.id ASC
	  
	  ";
	  // print_r($q);exit;
	  $list_opsi=$this->db->query($q)->result();
	  $opsi='';
	  foreach ($list_opsi as $r){
		  $opsi .='<option value="'.$r->id.'">'.$r->deskripsi.($r->tipe_deposit=='1'?' ('.$r->nama_tindakan.')':'').'</option>';
	  }
	  $hasil['opsi']=$opsi;
	  $this->output->set_output(json_encode($hasil));
  }
 
	function list_deposit(){
		$tabel='';
		$head_id=$this->input->post('head_id');
		$q="SELECT hari_batal_deposit FROM setting_deposit";
		$hari_batal_deposit=$this->db->query($q)->row('hari_batal_deposit');
		$q="
			SELECT COUNT(F.id) as jml_bukti,
			IFNULL(DATEDIFF(NOW(),
					D.tanggal),0) as umur_batal
			,D.id,D.head_id,H.tipe_deposit,H.deskripsi,D.nodeposit,D.created_date,MC.`name` as nama_created,MJ.nama as nama_jenis_deposit
			,D.tanggal
			,(CASE
						WHEN D.idmetodepembayaran = 1 THEN 'Tunai'
						WHEN D.idmetodepembayaran = 2 THEN 'Debit'
						WHEN D.idmetodepembayaran = 3 THEN 'Kredit'
						WHEN D.idmetodepembayaran = 4 THEN 'Transfer'
					END) AS metodepembayaran
			,D.idbank,MB.nama as nama_bank,D.nominal,D.terimadari,D.st_proses,D.status,D.st_approval
			,MD.`name` as nama_hapus,D.deleted_date,D.hapus_proses
			FROM trawatinap_deposit D
			LEFT JOIN trawatinap_deposit_head H ON H.id=D.head_id
			LEFT JOIN musers MC ON MC.id=D.iduserinput
			LEFT JOIN musers MD ON MD.id=D.iduserdelete
			LEFT JOIN mdeposit_jenis MJ ON MJ.id=D.jenis_deposit_id
			LEFT JOIN mbank MB ON MB.id=D.idbank
		  LEFT JOIN trawatinap_deposit_upload F ON F.deposit_id=D.id

			WHERE D.idrawatinap='$head_id'
			GROUP BY D.id
			ORDER BY D.id DESC
		";
		// print_r($q);exit;
		$row=$this->db->query($q)->result();
		$total=0;
		foreach($row as $r){
			$btn_edit='';$btn_hapus='';$btn_proses='';$btn_pdf='';$btn_pengajuan_hapus='';$btn_cetak='';$btn_approval='';$btn_hapus_fix='';
			if ($r->jml_bukti > 0){
				$btn_bukti='<button class="btn btn-default btn-xs" title="Bukti" onclick="show_bukti('.$r->id.')" type="button"><i class="fa fa-file-image-o"></i> '.$r->jml_bukti.' Upload</button>';
			}else{
				$btn_bukti='<button class="btn btn-warning btn-xs" title="Bukti" onclick="show_bukti('.$r->id.')" type="button"><i class="fa fa-file-image-o"></i> Upload Bukti</button>';
				
			}
			if ($r->status=='1'){
				if ($r->st_proses=='1'){
				$total=$total + $r->nominal;
					
				}
				
				if ($r->st_proses=='0'){
					$btn_edit='<button class="btn btn-success" title="Edit" onclick="edit_deposit('.$r->id.','.$r->head_id.')" type="button"><i class="fa fa-pencil"></i></button>';
					if ($r->umur_batal <= $hari_batal_deposit){
						$btn_hapus='<button class="btn btn-danger" title="Hapus Draft" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
					}
					
					$btn_proses='<button class="btn btn-primary" title="Generate / Proses" onclick="proses_deposit('.$r->id.')" type="button"><i class="fa fa-file-pdf-o"></i> GENERATE</button>';
				}else{
					if ($r->hapus_proses=='1'){
						$btn_approval='<button class="btn btn-danger" title="Lihat Proses Approval" onclick="list_user('.$r->id.')" type="button"><i class="si si-user-following"></i></button>';
						if ($r->st_approval=='1'){
							$btn_hapus_fix='<button class="btn btn-danger" title="Hapus Deposit" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
						}
						if ($r->st_approval=='2'){
						$btn_pdf='<a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
							// $btn_hapus_fix='<button class="btn btn-danger" title="Hapus Deposit" onclick="show_hapus_biasa('.$r->id.')" type="button"><i class="fa fa-trash"></i></button>';
						}
					}else{
						$btn_pdf='<a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
						if ($r->umur_batal <= $hari_batal_deposit){
							$btn_pengajuan_hapus='<button class="btn btn-danger" title="Hapus" onclick="load_user_approval('.$r->id.')" type="button"><i class="si si-ban"></i> BATAL</button>';
						}
					}
				}
				if ($r->hapus_proses){
					
				}
				$aksi='
					<div class="btn-group btn-group-xs" role="group">
						'.$btn_edit.'
						'.$btn_hapus.'
						'.$btn_proses.'
						'.$btn_pdf.'
						'.$btn_pengajuan_hapus.'
						'.$btn_cetak.'
						'.$btn_approval.'
						'.$btn_hapus_fix.'
					</div>
				';
			}else{
				if ($r->hapus_proses=='1'){
				$btn_pdf='<br><a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-xs btn-primary" ><i class="fa fa-file-pdf-o"></i></a>';
				}
				$aksi='Deleted By : '.$r->nama_hapus.'<br>'.HumanDateLong($r->deleted_date);
			}
			// $btn_cetak='<a class="btn btn-success" href="'.base_url().'ttindakan_ranap_deposit/cetak_deposit/'.$r->id.'/0"><i class="fa fa-print"></i></a>';
			
			
			
			
			$tabel .='<tr>';
				$tabel .='<td class="text-center">'.$aksi.'</td>';
				$tabel .='<td class="text-center">'.($r->tipe_deposit=='1'?text_warning('PEMBEDAHAN'):text_success('KONSERVATIF')).'<br>'.$r->deskripsi.'</td>';
				$tabel .='<td class="text-center">'.$r->nodeposit.'<br>'.$r->nama_created.'<br>'.HumanDateLong($r->created_date).'</td>';
				$tabel .='<td class="text-center">'.$r->nama_jenis_deposit.'</td>';
				$tabel .='<td class="text-center">'.HumanDateShort($r->tanggal).'</td>';
				$tabel .='<td class="text-center">'.($r->metodepembayaran).'</td>';
				$tabel .='<td class="text-center">'.($r->nama_bank).'</td>';
				$tabel .='<td class="text-right">'.number_format($r->nominal,0).'</td>';
				$tabel .='<td class="text-left">'.($r->terimadari).'</td>';
				$tabel .='<td class="text-center">'.$btn_bukti.'</td>';
				if ($r->status=='1'){
					if ($r->hapus_proses=='1'){
						if ($r->st_approval=='0'){
							$tabel .='<td class="text-center">'.text_warning('MENUGGU APPROVAL').'</td>';
						}
						if ($r->st_approval=='2'){
							$tabel .='<td class="text-center">'.text_danger('APPROVAL DITOLAK').'<br><br>'.text_success('TELAH DIPROSES').'</td>';
						}
						if ($r->st_approval=='1'){
							$tabel .='<td class="text-center">'.text_primary('DISETUJUI').'</td>';
						}
						
					}else{
						$tabel .='<td class="text-center">'.($r->st_proses=='1'?text_success('TELAH DIPROSES'):text_default('BELUM DIPROSES')).'</td>';
					}
				}else{
				$tabel .='<td class="text-center">'.text_danger('DIBATALKAN').'</td>';
					
				}
			$tabel .='</tr>';
		}
		$foot='
			<tr style="background-color:white;">
				<td colspan="7" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
				<td id="depositTotal" style="font-weight:bold;" class="text-right">'.number_format($total,0).'</td>
				<td></td>
				<td></td>
				<td><input type="hidden" id="deposit_diterima_all" value="'.$total.'"></td>
			</tr>
		';
		
		$hasil['tabel']=$tabel;
		$hasil['foot']=$foot;
		$this->output->set_output(json_encode($hasil));
		
	}
	function get_total_keseluruhan_rannap(){
		$idrawatinap=$this->input->post('idrawatinap');
		//#1 ADM
		$total_adm_ranap=0;
		$total_adm=0;
		$total_ko=0;
		$total_ko_fc=0;
		$total_fisio=0;
		$total_radiologi=0;
		$total_ruangan=0;
		$q="SELECT statusvalidasi FROM trawatinap_pendaftaran H WHERE H.id='$idrawatinap'";
		$statusvalidasi=$this->db->query($q)->row('statusvalidasi');
		if ($statusvalidasi=='1'){
			
			$q="
				SELECT SUM(trawatinap_ruangan.totalkeseluruhan) as total_ruangan
				FROM trawatinap_ruangan
				WHERE idrawatinap = '$idrawatinap' AND `status`='1'
			";
			$total_ruangan=$this->db->query($q)->row('total_ruangan');
			if ($total_ruangan){}else{$total_ruangan=0;};
		}else{
			
		$totalKeseluruhan=0;
		if ($statusvalidasi=='0'){
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapRuanganGroup($idrawatinap) as $row) { 
				 $tarifRanapRuangan = $this->Trawatinap_tindakan_model->getTarifRuangan($row->idkelompokpasien, $row->idruangan, $row->idkelas);
				 $totalKeseluruhan = $totalKeseluruhan + $tarifRanapRuangan['total'] * $row->jumlahhari;
			}
			$total_ruangan=$total_ruangan+$totalKeseluruhan;
			if ($total_ruangan){}else{$total_ruangan=0;};
		}
		}
		//VISITE DONE
		$total_visite=0;
		$q="
			SELECT SUM(H.totalkeseluruhan) as total_visite
			FROM trawatinap_visite H
			WHERE H.idrawatinap = '$idrawatinap' AND `status`='1'
		";
		$total_visite=$this->db->query($q)->row('total_visite');
		if ($total_visite){}else{$total_visite=0;};
		//TINDKAAN (DONE)
		$total_tindakan=0;
		$q="
			SELECT SUM(trawatinap_tindakan.totalkeseluruhan) as total_tindakan
			FROM trawatinap_tindakan
			WHERE idrawatinap = '$idrawatinap' AND `status`='1'
		";
		$total_tindakan=$this->db->query($q)->row('total_tindakan');
		if ($total_tindakan){}else{$total_tindakan=0;};
		
		$q="
			SELECT
				SUM(tpoliklinik_administrasi.totalkeseluruhan) AS totalkeseluruhan
			  FROM
				tpoliklinik_pendaftaran
			  LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
			  LEFT JOIN mtarif_administrasi ON mtarif_administrasi.id = mpasien_kelompok.tadm_rawatjalan OR mtarif_administrasi.id = mpasien_kelompok.tkartu_rawatjalan
			  LEFT JOIN trawatinap_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
			  LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trawatinap_pendaftaran.id AND tverifikasi_transaksi_detail.id_kelompok = 9
			  JOIN tpoliklinik_administrasi ON tpoliklinik_administrasi.idpendaftaran = tpoliklinik_pendaftaran.id
			  JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id AND tpoliklinik_tindakan.status = 1
			  JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
			  WHERE
				mtarif_administrasi.idjenis = 2 AND
				tpoliklinik_administrasi.status = '1' AND
				tkasir.status != '2' AND
				trawatinap_pendaftaran.id = '$idrawatinap'
		";
		$total_adm=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_adm){}else{$total_adm=0;};
		
		$q="SELECT SUM(D.totalkeseluruhan) as totalkeseluruhan FROM `tkamaroperasi_pendaftaran` H
			INNER JOIN tkamaroperasi_ruangan D ON D.idpendaftaranoperasi=H.id
			WHERE H.idpendaftaran='$idrawatinap' AND H.idasalpendaftaran='2'";
		$total_sewa_ruangan_ko=$this->db->query($q)->row('totalkeseluruhan');
		
		if ($total_sewa_ruangan_ko){}else{$total_sewa_ruangan_ko=0;};
		$q="
			SELECT SUM(H.total) as total FROM (
					SELECT COALESCE(H.total_da,0) + COALESCE(H.total_do,0) + COALESCE(H.total_daa,0) 
					+ COALESCE(H.total_dao,0) + COALESCE(H.total_sewa,0)  as total FROM tkamaroperasi_pendaftaran H
				WHERE H.idpendaftaran='$idrawatinap' AND H.idasalpendaftaran='2'
				
				) H
		";
		$total_ko=$this->db->query($q)->row('total');
		if ($total_ko){}else{$total_ko=0;};
		
		$total_ko=$total_ko+$total_sewa_ruangan_ko;
		
		$q="
			SELECT SUM(I.totalkeseluruhan) as total FROM tkamaroperasi_pendaftaran H
			LEFT JOIN tkamaroperasi_implan I ON I.idpendaftaranoperasi=H.id
			WHERE H.idpendaftaran = '$idrawatinap' 
				AND H.idasalpendaftaran = '2'
		";
		$ko_implan=$this->db->query($q)->row('total');
		if ($ko_implan){}else{$ko_implan=0;};
		$q="
			SELECT SUM(I.totalkeseluruhan) as total FROM tkamaroperasi_pendaftaran H
			LEFT JOIN tkamaroperasi_narcose I ON I.idpendaftaranoperasi=H.id
			WHERE H.idpendaftaran = '$idrawatinap' 
				AND H.idasalpendaftaran = '2'
		";
		$ko_narcose=$this->db->query($q)->row('total');
		if ($ko_narcose){}else{$ko_narcose=0;};
		$q="
			SELECT SUM(I.totalkeseluruhan) as total FROM tkamaroperasi_pendaftaran H
			LEFT JOIN tkamaroperasi_obat I ON I.idpendaftaranoperasi=H.id
			WHERE H.idpendaftaran = '$idrawatinap' 
				AND H.idasalpendaftaran = '2'
		";
		$ko_obat=$this->db->query($q)->row('total');
		if ($ko_obat){}else{$ko_obat=0;};
		$q="
			SELECT SUM(I.totalkeseluruhan) as total FROM tkamaroperasi_pendaftaran H
			LEFT JOIN tkamaroperasi_alkes I ON I.idpendaftaranoperasi=H.id
			WHERE H.idpendaftaran = '$idrawatinap' 
				AND H.idasalpendaftaran = '2'
		";
		$ko_alkes=$this->db->query($q)->row('total');
		if ($ko_alkes){}else{$ko_alkes=0;};
		// $total_ko=$total_ko+$ko_alkes+$ko_obat+$ko_narcose+$ko_implan;
		
		$q="
			SELECT SUM(H.totalkeseluruhan) as total_fullcare FROM tkamaroperasi_fullcare H
			WHERE H.`status`='1' AND H.idpendaftaran='$idrawatinap'
			GROUP BY H.idpendaftaran
		";
		$total_ko_fc=$this->db->query($q)->row('tkamaroperasi_fullcare');
		if ($total_ko_fc){}else{$total_ko_fc=0;};
		//#FISIO
		
		$q="
			SELECT SUM(T.totalkeseluruhan) as total_fisio FROM (
				SELECT
						view_rincian_fisioterapi_rajal.totalkeseluruhan AS totalkeseluruhan
					  FROM
						(
							SELECT
								
								trujukan_fisioterapi_detail.totalkeseluruhan AS totalkeseluruhan
								
							FROM
								tpoliklinik_pendaftaran
							LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
							JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
							JOIN trujukan_fisioterapi ON tpoliklinik_tindakan.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan IN ( 1, 2 )
							JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
							LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk)
						  LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
							JOIN mtarif_fisioterapi ON trujukan_fisioterapi_detail.idfisioterapi = mtarif_fisioterapi.id
							LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trujukan_fisioterapi_detail.id AND tverifikasi_transaksi_detail.id_kelompok = 9
							JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 ) AND tkasir.`status` <> 2
						  WHERE
							trawatinap_pendaftaran.id = '$idrawatinap'  AND trujukan_fisioterapi_detail.status <> 0
						) AS view_rincian_fisioterapi_rajal
				   

					  UNION ALL

					  SELECT
						
						view_rincian_fisioterapi_ranap.totalkeseluruhan AS totalkeseluruhan
						
					  FROM
						(
							SELECT
								
								trujukan_fisioterapi_detail.totalkeseluruhan AS totalkeseluruhan
							FROM
								trawatinap_pendaftaran
							JOIN trujukan_fisioterapi ON trawatinap_pendaftaran.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan = 3
							JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
							LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk)
							LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
							JOIN mtarif_fisioterapi ON trujukan_fisioterapi_detail.idfisioterapi = mtarif_fisioterapi.id
						  WHERE
							trawatinap_pendaftaran.id = '$idrawatinap'  AND trujukan_fisioterapi_detail.status <> 0
						) AS view_rincian_fisioterapi_ranap
					  ) T 
		";
		$total_fisio=$this->db->query($q)->row('total_fisio');
		if ($total_fisio){}else{$total_fisio=0;};
		//#Radiologi
		
		$q="
			SELECT SUM(T.totalkeseluruhan) as total_radiologi FROM (
				SELECT
						view_rincian_radiologi_rajal.iddetail AS iddetail,
						view_rincian_radiologi_rajal.idrawatinap AS idrawatinap,
						view_rincian_radiologi_rajal.idkelas AS idkelas,
						view_rincian_radiologi_rajal.idrujukan AS idrujukan,
						view_rincian_radiologi_rajal.norujukan AS norujukan,
						view_rincian_radiologi_rajal.tanggal AS tanggal,
						view_rincian_radiologi_rajal.idtipe AS idtipe,
						view_rincian_radiologi_rajal.idradiologi AS idtarif,
						view_rincian_radiologi_rajal.idradiologi AS idradiologi,
						view_rincian_radiologi_rajal.namatarif AS namatarif,
						view_rincian_radiologi_rajal.idkategori AS idkategori,
						view_rincian_radiologi_rajal.namakategori AS namakategori,
						view_rincian_radiologi_rajal.iddokter AS iddokter,
						view_rincian_radiologi_rajal.namadokter AS namadokter,
						view_rincian_radiologi_rajal.jasasarana AS jasasarana,
						view_rincian_radiologi_rajal.jasasarana_disc AS jasasarana_disc,
						view_rincian_radiologi_rajal.jasapelayanan AS jasapelayanan,
						view_rincian_radiologi_rajal.jasapelayanan_disc AS jasapelayanan_disc,
						view_rincian_radiologi_rajal.bhp AS bhp,
						view_rincian_radiologi_rajal.bhp_disc AS bhp_disc,
						view_rincian_radiologi_rajal.biayaperawatan AS biayaperawatan,
						view_rincian_radiologi_rajal.biayaperawatan_disc AS biayaperawatan_disc,
						view_rincian_radiologi_rajal.total AS subtotal,
						view_rincian_radiologi_rajal.kuantitas AS kuantitas,
						view_rincian_radiologi_rajal.diskon AS diskon,
						view_rincian_radiologi_rajal.total * view_rincian_radiologi_rajal.kuantitas AS total,
						view_rincian_radiologi_rajal.totalkeseluruhan AS totalkeseluruhan,
						view_rincian_radiologi_rajal.status AS status,
						view_rincian_radiologi_rajal.statusverifikasi AS statusverifikasi,
						view_rincian_radiologi_rajal.status_tindakan AS status_tindakan,
						view_rincian_radiologi_rajal.jasamedis AS jasamedis,
						view_rincian_radiologi_rajal.potongan_rs AS potongan_rs,
						view_rincian_radiologi_rajal.pajak_dokter AS pajak_dokter,
						view_rincian_radiologi_rajal.periode_pembayaran AS periode_pembayaran,
						view_rincian_radiologi_rajal.periode_jatuhtempo AS periode_jatuhtempo,
						view_rincian_radiologi_rajal.status_jasamedis AS status_jasamedis,
						view_rincian_radiologi_rajal.status_tindakan AS status_expertise
					  FROM
						(
							SELECT
								trujukan_radiologi_detail.id AS iddetail,
								trawatinap_pendaftaran.id AS idrawatinap,
								trawatinap_pendaftaran.idkelas AS idkelas,
								tpoliklinik_pendaftaran.id AS idpoliklinik,
								trujukan_radiologi_detail.idrujukan AS idrujukan,
								trujukan_radiologi.norujukan AS norujukan,
								cast( trujukan_radiologi.tanggal AS date ) AS tanggal,
								mtarif_radiologi.idtipe AS idtipe,
								trujukan_radiologi_detail.idradiologi AS idradiologi,(
								CASE
									WHEN ( mtarif_radiologi.idtipe = 1 ) THEN
										concat(
											mtarif_radiologi.nama,
											' (',
											mtarif_radiologi_expose.nama,
											' ',
											mtarif_radiologi_film.nama,
											')'
										) ELSE mtarif_radiologi.nama
									END
								) AS namatarif,
							mdokter_kategori.id AS idkategori,
							mdokter_kategori.nama AS namakategori,
								COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi) AS iddokter,
								mdokter.nama AS namadokter,
								trujukan_radiologi_detail.jasasarana AS jasasarana,
								trujukan_radiologi_detail.jasasarana_disc AS jasasarana_disc,
								trujukan_radiologi_detail.jasapelayanan AS jasapelayanan,
								trujukan_radiologi_detail.jasapelayanan_disc AS jasapelayanan_disc,
								trujukan_radiologi_detail.bhp AS bhp,
								trujukan_radiologi_detail.bhp_disc AS bhp_disc,
								trujukan_radiologi_detail.biayaperawatan AS biayaperawatan,
								trujukan_radiologi_detail.biayaperawatan_disc AS biayaperawatan_disc,
								trujukan_radiologi_detail.total AS total,
								trujukan_radiologi_detail.kuantitas AS kuantitas,
								trujukan_radiologi_detail.diskon AS diskon,
								trujukan_radiologi_detail.totalkeseluruhan AS totalkeseluruhan,
								trujukan_radiologi_detail.status AS status,
								trujukan_radiologi_detail.statusverifikasi AS statusverifikasi,
								8 AS kelompok,
								tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
								tverifikasi_transaksi_detail.id AS idverifdetail,
								tkasir.status AS status_kasir,
							COALESCE(trujukan_radiologi_detail.status_tindakan, trujukan_radiologi.statuspasien) AS status_tindakan,
							(
							CASE
								WHEN mtarif_radiologi.idtipe = '1' THEN
								  trujukan_radiologi_detail.nominal_jasamedis * trujukan_radiologi_detail.kuantitas
							  ELSE trujukan_radiologi_detail.jasapelayanan
							END
							) AS jasamedis,
							trujukan_radiologi_detail.potongan_rs AS potongan_rs,
							trujukan_radiologi_detail.pajak_dokter AS pajak_dokter,
							trujukan_radiologi_detail.periode_pembayaran AS periode_pembayaran,
							trujukan_radiologi_detail.periode_jatuhtempo AS periode_jatuhtempo,
							trujukan_radiologi_detail.status_jasamedis AS status_jasamedis,
							trujukan_radiologi_detail.status_tindakan AS status_expertise
							FROM
								tpoliklinik_pendaftaran
							LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
							JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
							JOIN trujukan_radiologi ON tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN ( 1, 2 )
							JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
							LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi)
							LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
							JOIN mtarif_radiologi ON trujukan_radiologi_detail.idradiologi = mtarif_radiologi.id
							LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi.idexpose = mtarif_radiologi_expose.id
							LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi.idfilm = mtarif_radiologi_film.id
							LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trujukan_radiologi_detail.id AND tverifikasi_transaksi_detail.id_kelompok = 8
							JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
							WHERE
								trujukan_radiologi.status <> 0 AND
							trawatinap_pendaftaran.id = '$idrawatinap' AND trujukan_radiologi_detail.status <> 0
						) AS view_rincian_radiologi_rajal
					  WHERE
						view_rincian_radiologi_rajal.status_kasir <> 2

					  UNION ALL

					  SELECT
						view_rincian_radiologi_ranap.iddetail AS iddetail,
						view_rincian_radiologi_ranap.idrawatinap AS idrawatinap,
						view_rincian_radiologi_ranap.idkelas AS idkelas,
						view_rincian_radiologi_ranap.idrujukan AS idrujukan,
						view_rincian_radiologi_ranap.norujukan AS norujukan,
						view_rincian_radiologi_ranap.tanggal AS tanggal,
						view_rincian_radiologi_ranap.idtipe AS idtipe,
						view_rincian_radiologi_ranap.idradiologi AS idtarif,
						view_rincian_radiologi_ranap.idradiologi AS idradiologi,
						view_rincian_radiologi_ranap.namatarif AS namatarif,
						view_rincian_radiologi_ranap.idkategori AS idkategori,
						view_rincian_radiologi_ranap.namakategori AS namakategori,
						view_rincian_radiologi_ranap.iddokter AS iddokter,
						view_rincian_radiologi_ranap.namadokter AS namadokter,
						view_rincian_radiologi_ranap.jasasarana AS jasasarana,
						view_rincian_radiologi_ranap.jasasarana_disc AS jasasarana_disc,
						view_rincian_radiologi_ranap.jasapelayanan AS jasapelayanan,
						view_rincian_radiologi_ranap.jasapelayanan_disc AS jasapelayanan_disc,
						view_rincian_radiologi_ranap.bhp AS bhp,
						view_rincian_radiologi_ranap.bhp_disc AS bhp_disc,
						view_rincian_radiologi_ranap.biayaperawatan AS biayaperawatan,
						view_rincian_radiologi_ranap.biayaperawatan_disc AS biayaperawatan_disc,
						view_rincian_radiologi_ranap.total AS subtotal,
						view_rincian_radiologi_ranap.kuantitas AS kuantitas,
						view_rincian_radiologi_ranap.diskon AS diskon,
						view_rincian_radiologi_ranap.total * view_rincian_radiologi_ranap.kuantitas AS total,
						view_rincian_radiologi_ranap.totalkeseluruhan AS totalkeseluruhan,
						view_rincian_radiologi_ranap.status AS status,
						view_rincian_radiologi_ranap.statusverifikasi AS statusverifikasi,
						view_rincian_radiologi_ranap.status_tindakan AS status_tindakan,
						view_rincian_radiologi_ranap.jasamedis AS jasamedis,
						view_rincian_radiologi_ranap.potongan_rs AS potongan_rs,
						view_rincian_radiologi_ranap.pajak_dokter AS pajak_dokter,
						view_rincian_radiologi_ranap.periode_pembayaran AS periode_pembayaran,
						view_rincian_radiologi_ranap.periode_jatuhtempo AS periode_jatuhtempo,
						view_rincian_radiologi_ranap.status_jasamedis AS status_jasamedis,
						view_rincian_radiologi_ranap.status_tindakan AS status_expertise
					  FROM
						(
							SELECT
								trujukan_radiologi_detail.id AS iddetail,
								trawatinap_pendaftaran.id AS idrawatinap,
								trawatinap_pendaftaran.idkelas AS idkelas,
								trujukan_radiologi_detail.idrujukan AS idrujukan,
								trujukan_radiologi.norujukan AS norujukan,
								cast( trujukan_radiologi.tanggal AS date ) AS tanggal,
								mtarif_radiologi.idtipe AS idtipe,
								trujukan_radiologi_detail.idradiologi AS idradiologi,
							  (
								CASE
									WHEN ( mtarif_radiologi.idtipe = 1 ) THEN
									concat(
										mtarif_radiologi.nama,
										' (',
										mtarif_radiologi_expose.nama,
										' ',
										mtarif_radiologi_film.nama,
										')'
									) ELSE mtarif_radiologi.nama
								END
								) AS namatarif,
							mdokter_kategori.id AS idkategori,
							mdokter_kategori.nama AS namakategori,
								COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi) AS iddokter,
								mdokter.nama AS namadokter,
								trujukan_radiologi_detail.jasasarana AS jasasarana,
								trujukan_radiologi_detail.jasasarana_disc AS jasasarana_disc,
								trujukan_radiologi_detail.jasapelayanan AS jasapelayanan,
								trujukan_radiologi_detail.jasapelayanan_disc AS jasapelayanan_disc,
								trujukan_radiologi_detail.bhp AS bhp,
								trujukan_radiologi_detail.bhp_disc AS bhp_disc,
								trujukan_radiologi_detail.biayaperawatan AS biayaperawatan,
								trujukan_radiologi_detail.biayaperawatan_disc AS biayaperawatan_disc,
								trujukan_radiologi_detail.total AS total,
								trujukan_radiologi_detail.kuantitas AS kuantitas,
								trujukan_radiologi_detail.diskon AS diskon,
								trujukan_radiologi_detail.totalkeseluruhan AS totalkeseluruhan,
								trujukan_radiologi_detail.status AS status,
								trujukan_radiologi_detail.statusverifikasi AS statusverifikasi,
							COALESCE(trujukan_radiologi_detail.status_tindakan, trujukan_radiologi.statuspasien) AS status_tindakan,
							(
							CASE
								WHEN mtarif_radiologi.idtipe = '1' THEN
								  trujukan_radiologi_detail.nominal_jasamedis * trujukan_radiologi_detail.kuantitas
							  ELSE trujukan_radiologi_detail.jasapelayanan
							END
							) AS jasamedis,
							trujukan_radiologi_detail.potongan_rs AS potongan_rs,
							trujukan_radiologi_detail.pajak_dokter AS pajak_dokter,
							trujukan_radiologi_detail.periode_pembayaran AS periode_pembayaran,
							trujukan_radiologi_detail.periode_jatuhtempo AS periode_jatuhtempo,
							trujukan_radiologi_detail.status_jasamedis AS status_jasamedis,
							trujukan_radiologi_detail.status_tindakan AS status_expertise
							FROM
							  trawatinap_pendaftaran
							JOIN trujukan_radiologi ON trawatinap_pendaftaran.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan = 3
							JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
							LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi)
							LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
							JOIN mtarif_radiologi ON trujukan_radiologi_detail.idradiologi = mtarif_radiologi.id
							LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi.idexpose = mtarif_radiologi_expose.id
							LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi.idfilm = mtarif_radiologi_film.id
							WHERE
							  trujukan_radiologi.status <> 0 AND
							trawatinap_pendaftaran.id = '$idrawatinap' AND trujukan_radiologi_detail.status <> 0
						) AS view_rincian_radiologi_ranap

						ORDER BY tanggal
				) T
				
		";
		$total_radiologi=$this->db->query($q)->row('total_radiologi');
		if ($total_radiologi){}else{$total_radiologi=0;};
		//#Lab
		
		$q="
						SELECT SUM(T.totalkeseluruhan) as total_lab FROM (
			SELECT
					SUM(view_rincian_laboratorium_rajal.totalkeseluruhan) AS totalkeseluruhan
				  FROM
					(
						SELECT
						  trujukan_laboratorium_detail.id AS iddetail,
						  trawatinap_pendaftaran.id AS idrawatinap,
						  trawatinap_pendaftaran.idkelas AS idkelas,
						  tpoliklinik_pendaftaran.id AS idpoliklinik,
						  trujukan_laboratorium_detail.idrujukan AS idrujukan,
						  trujukan_laboratorium.norujukan AS norujukan,
						  cast( trujukan_laboratorium.tanggal AS date ) AS tanggal,
						  mtarif_laboratorium.idtipe AS idtipe,
						  trujukan_laboratorium_detail.idlaboratorium AS idlaboratorium,
						  mtarif_laboratorium.nama AS namatarif,
						mdokter_kategori.id AS idkategori,
						mdokter_kategori.nama AS namakategori,
						  COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk) AS iddokter,
						  mdokter.nama AS namadokter,
						  trujukan_laboratorium_detail.jasasarana AS jasasarana,
						  trujukan_laboratorium_detail.jasasarana_disc AS jasasarana_disc,
						  trujukan_laboratorium_detail.jasapelayanan AS jasapelayanan,
						  trujukan_laboratorium_detail.jasapelayanan_disc AS jasapelayanan_disc,
						(trujukan_laboratorium_detail.jasapelayanan - trujukan_laboratorium_detail.jasapelayanan_disc) * trujukan_laboratorium_detail.kuantitas AS jasamedis,
						  trujukan_laboratorium_detail.bhp AS bhp,
						  trujukan_laboratorium_detail.bhp_disc AS bhp_disc,
						  trujukan_laboratorium_detail.biayaperawatan AS biayaperawatan,
						  trujukan_laboratorium_detail.biayaperawatan_disc AS biayaperawatan_disc,
						  trujukan_laboratorium_detail.total AS total,
						  trujukan_laboratorium_detail.kuantitas AS kuantitas,
						  trujukan_laboratorium_detail.diskon AS diskon,
						  trujukan_laboratorium_detail.totalkeseluruhan AS totalkeseluruhan,
						  trujukan_laboratorium_detail.status AS STATUS,
						  trujukan_laboratorium_detail.statusrincianpaket AS statusrincianpaket,
						  trujukan_laboratorium_detail.statusverifikasi AS statusverifikasi,
						  6 AS kelompok,
						  tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
						  tverifikasi_transaksi_detail.id AS idverifdetail,
						  tkasir.status AS status_kasir,
						trujukan_laboratorium_detail.potongan_rs AS potongan_rs,
						trujukan_laboratorium_detail.pajak_dokter AS pajak_dokter,
						trujukan_laboratorium_detail.periode_pembayaran AS periode_pembayaran,
						trujukan_laboratorium_detail.periode_jatuhtempo AS periode_jatuhtempo,
						trujukan_laboratorium_detail.status_jasamedis AS status_jasamedis
						FROM
						  tpoliklinik_pendaftaran
						LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
						JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
						JOIN trujukan_laboratorium ON tpoliklinik_tindakan.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan IN ( 1, 2 )
						JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
						LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk)
						LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
						JOIN mtarif_laboratorium ON trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id
						LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trujukan_laboratorium_detail.id AND tverifikasi_transaksi_detail.id_kelompok = 6
						JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
						WHERE
						  trujukan_laboratorium.status <> 0 AND
						trawatinap_pendaftaran.id = '$idrawatinap' AND
						
						trujukan_laboratorium_detail.statusrincianpaket = 0 AND trujukan_laboratorium_detail.status <> 0
					) AS view_rincian_laboratorium_rajal
				  WHERE
					view_rincian_laboratorium_rajal.status_kasir <> 2

				  UNION ALL

				  SELECT
					
					SUM(view_rincian_laboratorium_ranap.totalkeseluruhan) AS totalkeseluruhan
				  FROM
					(
						SELECT
							trujukan_laboratorium_detail.id AS iddetail,
							trawatinap_pendaftaran.id AS idrawatinap,
							trawatinap_pendaftaran.idkelas AS idkelas,
							trujukan_laboratorium_detail.idrujukan AS idrujukan,
							trujukan_laboratorium.norujukan AS norujukan,
							cast( trujukan_laboratorium.tanggal AS date ) AS tanggal,
							mtarif_laboratorium.idtipe AS idtipe,
							trujukan_laboratorium_detail.idlaboratorium AS idlaboratorium,
							mtarif_laboratorium.nama AS namatarif,
						mdokter_kategori.id AS idkategori,
						mdokter_kategori.nama AS namakategori,
							COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk) AS iddokter,
							mdokter.nama AS namadokter,
							trujukan_laboratorium_detail.jasasarana AS jasasarana,
							trujukan_laboratorium_detail.jasasarana_disc AS jasasarana_disc,
							trujukan_laboratorium_detail.jasapelayanan AS jasapelayanan,
							trujukan_laboratorium_detail.jasapelayanan_disc AS jasapelayanan_disc,
						(trujukan_laboratorium_detail.jasapelayanan - trujukan_laboratorium_detail.jasapelayanan_disc) * trujukan_laboratorium_detail.kuantitas AS jasamedis,
							trujukan_laboratorium_detail.bhp AS bhp,
							trujukan_laboratorium_detail.bhp_disc AS bhp_disc,
							trujukan_laboratorium_detail.biayaperawatan AS biayaperawatan,
							trujukan_laboratorium_detail.biayaperawatan_disc AS biayaperawatan_disc,
							trujukan_laboratorium_detail.total AS total,
							trujukan_laboratorium_detail.kuantitas AS kuantitas,
							trujukan_laboratorium_detail.diskon AS diskon,
							trujukan_laboratorium_detail.totalkeseluruhan AS totalkeseluruhan,
							trujukan_laboratorium_detail.status AS status,
							trujukan_laboratorium_detail.statusrincianpaket AS statusrincianpaket,
							trujukan_laboratorium_detail.statusverifikasi AS statusverifikasi,
						trujukan_laboratorium_detail.potongan_rs AS potongan_rs,
						trujukan_laboratorium_detail.pajak_dokter AS pajak_dokter,
						trujukan_laboratorium_detail.periode_pembayaran AS periode_pembayaran,
						trujukan_laboratorium_detail.periode_jatuhtempo AS periode_jatuhtempo,
						trujukan_laboratorium_detail.status_jasamedis AS status_jasamedis
						FROM
							trawatinap_pendaftaran
						JOIN trujukan_laboratorium ON trawatinap_pendaftaran.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan = 3
						JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
						LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk)
						LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
						JOIN mtarif_laboratorium ON trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id
						WHERE
							trujukan_laboratorium.status <> 0 AND
						trawatinap_pendaftaran.id = '$idrawatinap' AND
						
						trujukan_laboratorium_detail.statusrincianpaket = 0 AND trujukan_laboratorium_detail.status <> 0
					) AS view_rincian_laboratorium_ranap
			) T 
					
				
		";
				
		$total_lab=0;
		$total_lab=$this->db->query($q)->row('total_lab');
		if ($total_lab){}else{$total_lab=0;};
		//#RAJAL FARMASI
		$total_obat_rajal=0;
		$q="
			SELECT
			SUM(tpasien_penjualan.totalharga) as total_obat_rajal
				FROM
					tpasien_penjualan
					
					JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan AND tpasien_penjualan.asalrujukan IN (1, 2)
					JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
					JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id
				WHERE
				trawatinap_pendaftaran.id = '$idrawatinap' AND tpasien_penjualan.`status` <> 0 AND tkasir.`status` != '2'
		";
		$total_obat_rajal=$this->db->query($q)->row('total_obat_rajal');
		if ($total_obat_rajal){}else{$total_obat_rajal=0;};
		//ALKSE RAJAL
		$totalkeseluruhan_alkes_rajal=0;
		$q="
			SELECT SUM(H.totalkeseluruhan) as totalkeseluruhan_alkes_rajal FROM (
				SELECT
				tpoliklinik_alkes.totalkeseluruhan
				
			  FROM
				tpoliklinik_alkes
			  LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_alkes.idtindakan AND tpoliklinik_tindakan.status = 1
			  LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
			  LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
			  LEFT JOIN trawatinap_tindakan ON trawatinap_tindakan.idrawatinap = trawatinap_pendaftaran.id
			  
			  LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tpoliklinik_alkes.id AND tverifikasi_transaksi_detail.id_kelompok = 7
			  JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
			  WHERE
				tpoliklinik_alkes.status = 1 AND
				trawatinap_pendaftaran.id = '$idrawatinap'
				
			  GROUP BY
				trawatinap_tindakan.idrawatinap,
				tpoliklinik_alkes.id
				
				) H 
		";
		$totalkeseluruhan_alkes_rajal=$this->db->query($q)->row('totalkeseluruhan_alkes_rajal');
		if ($totalkeseluruhan_alkes_rajal){}else{$totalkeseluruhan_alkes_rajal=0;};
		//OBAT RAJAL
		$totalkeseluruhan_obat_rajal=0;
		$q="
			SELECT SUM(H.totalkeseluruhan) as totalkeseluruhan_obat_rajal FROM (
SELECT
      	
      	tpoliklinik_obat.totalkeseluruhan AS totalkeseluruhan 
      FROM
      	tpoliklinik_obat
      LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_obat.idtindakan AND tpoliklinik_tindakan.status = 1
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN trawatinap_tindakan ON trawatinap_tindakan.idrawatinap = trawatinap_pendaftaran.id
      LEFT JOIN mdata_obat ON mdata_obat.id = tpoliklinik_obat.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tpoliklinik_obat.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tpoliklinik_obat.id AND tverifikasi_transaksi_detail.id_kelompok = 7
      JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
      WHERE
      	tpoliklinik_obat.status = 1 AND
        trawatinap_pendaftaran.id = '$idrawatinap' AND
        tpoliklinik_pendaftaran.idtipe = 2
      GROUP BY
      	trawatinap_tindakan.idrawatinap,
      	tpoliklinik_obat.id
				
				) H
		";
		$totalkeseluruhan_obat_rajal=$this->db->query($q)->row('totalkeseluruhan_obat_rajal');
		if ($totalkeseluruhan_obat_rajal){}else{$totalkeseluruhan_obat_rajal=0;};
		//TINDKAN RAJAL IGD
		$totalkeseluruhan_tindakan_igd=0;
		$q="
			SELECT
			SUM(view_rincian_rajal_tindakan.totalkeseluruhan) AS totalkeseluruhan_tindakan_igd
			FROM
			(
				SELECT
					tpoliklinik_pelayanan.id AS iddetail,
					tpoliklinik_pendaftaran.id AS idpoliklinik,
					tpoliklinik_pelayanan.idtindakan AS idtindakan,
					trawatinap_pendaftaran.id AS idrawatinap,
					tpoliklinik_pendaftaran.idtipe AS idtipe,
					tpoliklinik_pelayanan.totalkeseluruhan AS totalkeseluruhan
				FROM
					tpoliklinik_pelayanan
				LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_pelayanan.idtindakan AND tpoliklinik_tindakan.status = 1
				LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
				LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
				LEFT JOIN trawatinap_tindakan ON trawatinap_tindakan.idrawatinap = trawatinap_pendaftaran.id
				LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pelayanan.iddokter
				LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
				LEFT JOIN mtarif_rawatjalan ON mtarif_rawatjalan.id = tpoliklinik_pelayanan.idpelayanan
				LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tpoliklinik_pelayanan.id AND tverifikasi_transaksi_detail.id_kelompok = 9
				JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
				WHERE
					tpoliklinik_pelayanan.status = 1 AND
				trawatinap_pendaftaran.id = '$idrawatinap'
				GROUP BY
					trawatinap_tindakan.idrawatinap,
					tpoliklinik_pelayanan.id
			) AS view_rincian_rajal_tindakan
			JOIN tkasir ON tkasir.idtindakan = view_rincian_rajal_tindakan.idtindakan AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status <> 2 )
			WHERE
			view_rincian_rajal_tindakan.idrawatinap = '$idrawatinap' AND
			view_rincian_rajal_tindakan.idtipe = 2
		";
		$totalkeseluruhan_tindakan_igd=$this->db->query($q)->row('totalkeseluruhan_tindakan_igd');
		if ($totalkeseluruhan_tindakan_igd){}else{$totalkeseluruhan_tindakan_igd=0;};
		//FARMASI
		$total_farmasi=0;
		// $q="
			// SELECT SUM(H.totalharga) as total_farmasi FROM `tpasien_penjualan` H
			// WHERE H.idtindakan='$idrawatinap' AND H.asalrujukan='3' AND H.`status` !='0'
		// ";
		
		$q="
			SELECT
 
				SUM(view_union_farmasi.totalharga) AS totalkeseluruhan
			  
			  FROM
				(
				SELECT
					tpasien_penjualan_nonracikan.id AS iddetail,
					tpasien_penjualan_nonracikan.idtipe AS idtipe,
					tpasien_penjualan_nonracikan.totalharga AS totalharga
					
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
					
				WHERE
					trawatinap_pendaftaran.id = '$idrawatinap'
					AND tpasien_penjualan.STATUS <> 0

				UNION ALL

				SELECT
					tpasien_penjualan_racikan.id AS iddetail,
					3 AS idtipe,
					tpasien_penjualan_racikan.totalharga AS totalharga
							
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
				WHERE
					trawatinap_pendaftaran.id = '$idrawatinap'
					AND tpasien_penjualan.STATUS <> 0
				) AS view_union_farmasi
				LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi.iddetail
				AND tverifikasi_transaksi_detail.id_kelompok = 18
			  WHERE
			  view_union_farmasi.idtipe = 3
		";
		$total_farmasi=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_farmasi){}else{$total_farmasi=0;};
		$total_retur_farmasi=0;
		$q="
				SELECT
      	
				SUM(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan
				
			  FROM
				(
					SELECT
						tpasien_pengembalian_detail.id AS iddetail,
								tpasien_penjualan.idtindakan AS idtindakan,
						tpasien_pengembalian_detail.totalharga AS totalharga
					
					FROM
						tpasien_pengembalian
					JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan 
					JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
					
					WHERE
						tpasien_pengembalian_detail.kuantitas > 0 AND tpasien_penjualan.asalrujukan='3' AND tpasien_pengembalian_detail.idtipe='3'
				) AS view_union_farmasi_retur
			  INNER JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = view_union_farmasi_retur.idtindakan
			  LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
			  WHERE
				trawatinap_pendaftaran.id = '$idrawatinap'
      	
		";
		
		$total_retur_farmasi=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_retur_farmasi){}else{$total_retur_farmasi=0;};
		//ALKDES
		$total_alkes=0;
		// $q="
			// SELECT SUM(H.totalkeseluruhan) as total_alkes
			// FROM trawatinap_alkes H
			// WHERE H.idrawatinap = '$idrawatinap' AND `status`='1'
		// ";
		$q="SELECT
      	
      	SUM(view_union_farmasi.totalharga) AS totalkeseluruhan
      FROM
      	(
      	SELECT
      		tpasien_penjualan.id AS id,
      		trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      		tpasien_penjualan.STATUS AS STATUS,
      		tpasien_penjualan.nopenjualan AS nopenjualan,
      		tpasien_penjualan_nonracikan.id AS iddetail,
      		tpasien_penjualan.idtindakan AS idtindakan,
      		tpasien_penjualan.tanggal AS tanggal,
      		tpasien_penjualan.asalrujukan AS asalrujukan,
      		(CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.id
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.id
    			END) AS idtarif,
      		(CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.nama
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.nama
    			END) AS nama,
          (CASE
  					WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
  					mdata_alkes.idkategori
  					WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
  					mdata_obat.idkategori
  				END) AS idkategori,
				  (CASE
						WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
						mdata_alkes.kode
						WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
						mdata_obat.kode
					END) AS kode,
					tpasien_penjualan_nonracikan.idtipe AS idtipe,
					tpasien_penjualan_nonracikan.kuantitas AS kuantitas,
					tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
					tpasien_penjualan_nonracikan.margin AS margin,
					tpasien_penjualan_nonracikan.harga AS harga,
					tpasien_penjualan_nonracikan.diskon_rp AS diskon,
					tpasien_penjualan_nonracikan.tuslah AS tuslah,
					tpasien_penjualan_nonracikan.totalharga AS totalharga,
					munitpelayanan.id AS idunitpelayanan,
					munitpelayanan.nama AS unitpelayanan,
					tpasien_penjualan_nonracikan.statusverifikasi AS statusverifikasi,
					0 AS statusracikan
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
					LEFT JOIN mdata_obat ON tpasien_penjualan_nonracikan.idbarang = mdata_obat.id
					LEFT JOIN munitpelayanan ON tpasien_penjualan_nonracikan.idunit = munitpelayanan.id
					LEFT JOIN mdata_alkes ON tpasien_penjualan_nonracikan.idbarang = mdata_alkes.id
				WHERE
					trawatinap_pendaftaran.id = '$idrawatinap'
					AND tpasien_penjualan.STATUS <> 0

        UNION ALL

				SELECT
					tpasien_penjualan.id AS id,
					trawatinap_pendaftaran.id AS idrawatinap,
					trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
					tpasien_penjualan.STATUS AS STATUS,
					tpasien_penjualan.nopenjualan AS nopenjualan,
					tpasien_penjualan_racikan.id AS iddetail,
					tpasien_penjualan.idtindakan AS idtindakan,
					tpasien_penjualan.tanggal AS tanggal,
					tpasien_penjualan.asalrujukan AS asalrujukan,
					0 AS idtarif,
					tpasien_penjualan_racikan.namaracikan AS nama,
					0 AS idkategori,
					'' AS kode,
					3 AS idtipe,
					1 AS kuantitas,
					tpasien_penjualan_racikan.totalharga AS hargadasar,
					0 AS margin,
					tpasien_penjualan_racikan.totalharga AS harga,
					0 AS diskon,
					tpasien_penjualan_racikan.tuslah AS tuslah,
					tpasien_penjualan_racikan.totalharga AS totalharga,
					1 AS idunitpelayanan,
					'Farmasi' AS unitpelayanan,
					tpasien_penjualan_racikan.statusverifikasi AS statusverifikasi,
					1 AS statusracikan
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
				WHERE
					trawatinap_pendaftaran.id = '$idrawatinap'
					AND tpasien_penjualan.STATUS <> 0
				) AS view_union_farmasi
				LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi.iddetail
				AND tverifikasi_transaksi_detail.id_kelompok = 18
			WHERE
			view_union_farmasi.idtipe = 1 AND
      view_union_farmasi.idkategori != 2";
		$total_alkes=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_alkes){}else{$total_alkes=0;};
		$total_alkes_farmasi=0;
		
		$q="SELECT
      	
      	SUM(trawatinap_alkes.totalkeseluruhan) AS totalkeseluruhan
      FROM
      	trawatinap_alkes
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_alkes.idrawatinap
      LEFT JOIN mdata_alkes ON mdata_alkes.id = trawatinap_alkes.idalkes
      
      WHERE
      	trawatinap_alkes.status = 1 AND
      	trawatinap_alkes.idrawatinap = '$idrawatinap' AND
      	mdata_alkes.idkategori != 2";
		$total_alkes_farmasi=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_alkes_farmasi){}else{$total_alkes_farmasi=0;};
		$total_alkes_retur=0;
		$q="SELECT
      	
      	SUM(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan
      FROM
      	(
      		SELECT
      			tpasien_pengembalian.id AS id,
      			tpasien_pengembalian.nopengembalian AS nopengembalian,
      			tpasien_pengembalian_detail.id AS iddetail,
      			tpasien_penjualan.idtindakan AS idtindakan,
      			tpasien_pengembalian.tanggal AS tanggal,
      			tpasien_pengembalian_detail.idunit AS idunitpelayanan,
      			munitpelayanan.nama AS unitpelayanan,
      			tpasien_penjualan.asalrujukan AS asalrujukan,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.id
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.id
      				END
      			) AS idtarif,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.nama
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.nama
      				END
      			) AS nama,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.idkategori
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.idkategori
      				END
      			) AS idkategori,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.kode
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.kode
      				END
      			) AS kode,
      			tpasien_pengembalian_detail.idtipe AS idtipe,
      			tpasien_pengembalian_detail.kuantitas AS kuantitas,
      			tpasien_pengembalian_detail.harga_dasar AS hargadasar,
      			tpasien_pengembalian_detail.margin AS margin,
      			tpasien_pengembalian_detail.harga AS harga,
      			tpasien_pengembalian_detail.diskon_rp AS diskon,
      			0 AS tuslah,
      			tpasien_pengembalian_detail.totalharga AS totalharga,
      			tpasien_pengembalian_detail.statusverifikasi AS statusverifikasi,
      			tpasien_pengembalian_detail.stracikan AS statusracikan
      		FROM
      			tpasien_pengembalian
      		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan
      		JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
      		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
      		LEFT JOIN mdata_obat ON tpasien_pengembalian_detail.idbarang = mdata_obat.id
      		LEFT JOIN mdata_alkes ON tpasien_pengembalian_detail.idbarang = mdata_alkes.id
      		WHERE
      			tpasien_pengembalian_detail.kuantitas > 0
      	) AS view_union_farmasi_retur
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = view_union_farmasi_retur.idtindakan AND view_union_farmasi_retur.asalrujukan IN ( 2, 3 )
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
      WHERE
      	trawatinap_pendaftaran.id = '$idrawatinap' AND
      	view_union_farmasi_retur.idtipe = 1 AND
      	view_union_farmasi_retur.idkategori != 2";
		$total_alkes_retur=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_alkes_retur){}else{$total_alkes_retur=0;};
		$total_alkes=$total_alkes+$total_alkes_farmasi+$total_alkes_retur;
		
		
		$total_alat_bantu=0;
		$q="SELECT
      
      	SUM(trawatinap_alkes.totalkeseluruhan) AS totalkeseluruhan
      FROM
      	trawatinap_alkes
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_alkes.idrawatinap
      LEFT JOIN mdata_alkes ON mdata_alkes.id = trawatinap_alkes.idalkes
      LEFT JOIN munitpelayanan ON munitpelayanan.id = trawatinap_alkes.idunit
      WHERE
      	trawatinap_alkes.status = 1 AND
      	trawatinap_alkes.idrawatinap = '$idrawatinap' AND
      	mdata_alkes.idkategori = 2";
		
		$total_alat_bantu=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_alat_bantu){}else{$total_alat_bantu=0;};
		$total_alat_bantu_2=0;
		$q="SELECT
      	
      	SUM(view_union_farmasi.totalharga) AS totalkeseluruhan
      FROM
      	(
      	SELECT
      		tpasien_penjualan.id AS id,
      		trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien,
      		tpasien_penjualan.STATUS AS STATUS,
      		tpasien_penjualan.nopenjualan AS nopenjualan,
      		tpasien_penjualan_nonracikan.id AS iddetail,
      		tpasien_penjualan.idtindakan AS idtindakan,
      		tpasien_penjualan.tanggal AS tanggal,
      		tpasien_penjualan.asalrujukan AS asalrujukan,
          (CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.id
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.id
    			END) AS idbarang,
          (CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.nama
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.nama
    			END) AS nama,
          (CASE
  					WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
  					mdata_alkes.idkategori
  					WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
  					mdata_obat.idkategori
  				END) AS idkategori,
				  (CASE
						WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
						mdata_alkes.kode
						WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
						mdata_obat.kode
					END) AS kode,
					tpasien_penjualan_nonracikan.idtipe AS idtipe,
					tpasien_penjualan_nonracikan.kuantitas AS kuantitas,
					tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
					tpasien_penjualan_nonracikan.margin AS margin,
					tpasien_penjualan_nonracikan.harga AS harga,
					tpasien_penjualan_nonracikan.diskon_rp AS diskon,
					tpasien_penjualan_nonracikan.tuslah AS tuslah,
					tpasien_penjualan_nonracikan.totalharga AS totalharga
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
					LEFT JOIN mdata_obat ON tpasien_penjualan_nonracikan.idbarang = mdata_obat.id
					LEFT JOIN munitpelayanan ON tpasien_penjualan_nonracikan.idunit = munitpelayanan.id
					LEFT JOIN mdata_alkes ON tpasien_penjualan_nonracikan.idbarang = mdata_alkes.id
				WHERE
					trawatinap_pendaftaran.id = '$idrawatinap'
					AND tpasien_penjualan.STATUS <> 0

        UNION ALL

				SELECT
					tpasien_penjualan.id AS id,
					trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien,
					tpasien_penjualan.STATUS AS STATUS,
					tpasien_penjualan.nopenjualan AS nopenjualan,
					tpasien_penjualan_racikan.id AS iddetail,
					tpasien_penjualan.idtindakan AS idtindakan,
					tpasien_penjualan.tanggal AS tanggal,
					tpasien_penjualan.asalrujukan AS asalrujukan,
          0 AS idbarang,
					tpasien_penjualan_racikan.namaracikan AS nama,
					0 AS idkategori,
					'' AS kode,
					3 AS idtipe,
					1 AS kuantitas,
					tpasien_penjualan_racikan.totalharga AS hargadasar,
					0 AS margin,
					tpasien_penjualan_racikan.totalharga AS harga,
					0 AS diskon,
					tpasien_penjualan_racikan.tuslah AS tuslah,
					tpasien_penjualan_racikan.totalharga AS totalharga
					
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
				WHERE
					trawatinap_pendaftaran.id = '$idrawatinap'
					AND tpasien_penjualan.STATUS <> 0
				) AS view_union_farmasi
				LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi.iddetail
				AND tverifikasi_transaksi_detail.id_kelompok = 18
			WHERE
			view_union_farmasi.idtipe = 1 AND
      view_union_farmasi.idkategori = 2";
		
		$total_alat_bantu_2=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_alat_bantu_2){}else{$total_alat_bantu_2=0;};
		$total_alat_bantu_retur=0;
		
		$q="SELECT
      
      	SUM(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan
      FROM
      	(
      		SELECT
      			tpasien_pengembalian.id AS id,
      			tpasien_pengembalian.nopengembalian AS nopengembalian,
      			tpasien_pengembalian_detail.id AS iddetail,
      			tpasien_penjualan.idtindakan AS idtindakan,
      			tpasien_pengembalian.tanggal AS tanggal,
      			tpasien_pengembalian_detail.idunit AS idunitpelayanan,
      			munitpelayanan.nama AS unitpelayanan,
      			tpasien_penjualan.asalrujukan AS asalrujukan,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.nama
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.nama
      				END
      			) AS nama,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.idkategori
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.idkategori
      				END
      			) AS idkategori,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.kode
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.kode
      				END
      			) AS kode,
      			tpasien_pengembalian_detail.idtipe AS idtipe,
      			tpasien_pengembalian_detail.kuantitas AS kuantitas,
      			tpasien_pengembalian_detail.harga_dasar AS hargadasar,
      			tpasien_pengembalian_detail.margin AS margin,
      			tpasien_pengembalian_detail.harga AS harga,
      			tpasien_pengembalian_detail.diskon_rp AS diskon,
      			0 AS tuslah,
      			tpasien_pengembalian_detail.totalharga AS totalharga,
      			tpasien_pengembalian_detail.statusverifikasi AS statusverifikasi,
      			tpasien_pengembalian_detail.stracikan AS statusracikan
      		FROM
      			tpasien_pengembalian
      		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan
      		JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
      		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
      		LEFT JOIN mdata_obat ON tpasien_pengembalian_detail.idbarang = mdata_obat.id
      		LEFT JOIN mdata_alkes ON tpasien_pengembalian_detail.idbarang = mdata_alkes.id
      		WHERE
      			tpasien_pengembalian_detail.kuantitas > 0
      	) AS view_union_farmasi_retur
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = view_union_farmasi_retur.idtindakan AND view_union_farmasi_retur.asalrujukan IN ( 2, 3 )
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
      WHERE
      	trawatinap_pendaftaran.id = '$idrawatinap' AND
      	view_union_farmasi_retur.idtipe = 1 AND
      	view_union_farmasi_retur.idkategori = 2";
		
		$total_alat_bantu_retur=$this->db->query($q)->row('totalkeseluruhan');
		if ($total_alat_bantu_retur){}else{$total_alat_bantu_retur=0;};
		$total_alat_bantu=$total_alat_bantu + $total_alat_bantu_2 + $total_alat_bantu_retur;
		
		//OBAT
		$total_obat=0;
		$q="
			SELECT SUM(H.totalkeseluruhan) as total_obat
			FROM trawatinap_obat H
			WHERE H.idrawatinap = '$idrawatinap' AND `status`='1'
		";
		$total_obat=$this->db->query($q)->row('total_obat');
		
		if ($total_obat){}else{$total_obat=0;};
		
		
		//RUANGAN DONE
		$total_obat_ranap=$total_obat+$total_farmasi+$total_retur_farmasi;
		$totalSebelumAdm=$total_ruangan+$total_tindakan +$total_visite +$total_obat_ranap+$total_alkes+$total_alat_bantu+$totalkeseluruhan_tindakan_igd+$totalkeseluruhan_obat_rajal+$totalkeseluruhan_alkes_rajal + $total_lab+ $total_radiologi+ $total_fisio+ $total_ko + $ko_alkes+ $ko_obat+ $ko_narcose+ $ko_implan+ $total_adm;
		
		if ($statusvalidasi=='1'){
			$q="
			SELECT SUM(H.tarifsetelahdiskon) as total_adm_ranap FROM trawatinap_administrasi H
				WHERE H.idpendaftaran='$idrawatinap' AND H.`status`='1'
			";
			$total_adm_ranap=$this->db->query($q)->row('total_adm_ranap');
			if ($total_adm_ranap){}else{$total_adm_ranap=0;};
		}else{
			$total_adm_ranap=0;
			$q="
					SELECT
					trawatinap_pendaftaran.id AS idrawatinap,
					trawatinap_pendaftaran.nopendaftaran,
					trawatinap_pendaftaran.tanggaldaftar AS tanggal,
					mtarif_administrasi.id AS idtarif,
					'Biaya Administrasi' AS namatarif,
					mtarif_administrasi.idtipe,
					mtarif_administrasi.idjenis,
					mtarif_administrasi.jasasarana,
					mtarif_administrasi.jasapelayanan,
					mtarif_administrasi.bhp,
					mtarif_administrasi.biayaperawatan,
					mtarif_administrasi.total,
					mtarif_administrasi.mintarif,
					mtarif_administrasi.maxtarif,
					mtarif_administrasi.persentasetarif,
					($totalSebelumAdm * (mtarif_administrasi.persentasetarif / 100)) AS totalkeseluruhan,
					(CASE 
						WHEN ($totalSebelumAdm * (mtarif_administrasi.persentasetarif / 100)) < mtarif_administrasi.mintarif THEN
							mtarif_administrasi.mintarif
						WHEN ($totalSebelumAdm * (mtarif_administrasi.persentasetarif / 100)) > mtarif_administrasi.maxtarif THEN
							mtarif_administrasi.maxtarif
						ELSE
							($totalSebelumAdm * (mtarif_administrasi.persentasetarif / 100))
					END) AS tarif,
						28 AS `kelompok`,
					`tverifikasi_transaksi_detail`.`harga_revisi` AS `harga_revisi`,
					`tverifikasi_transaksi_detail`.`id` AS `idverifdetail` 
				FROM
					trawatinap_pendaftaran
				JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien 
				JOIN mtarif_administrasi ON mtarif_administrasi.id = mpasien_kelompok.tadm_rawatinap OR mtarif_administrasi.id = mpasien_kelompok.tkartu_rawatinap
				left join tverifikasi_transaksi_detail on tverifikasi_transaksi_detail.id_detail = mtarif_administrasi.id and tverifikasi_transaksi_detail.id_kelompok = 28
				WHERE mtarif_administrasi.idjenis = 1 AND trawatinap_pendaftaran.id = '$idrawatinap'
			";
			$dataAdminRanap = $this->db->query($q)->result();
			foreach ($dataAdminRanap as $row) {
				$total_adm_ranap = $total_adm_ranap + $row->tarif;
			}
		}
			
		$totalkeseluruhan=$totalSebelumAdm + $total_adm_ranap;
		// $totalkeseluruhan=0;
		
		
		$data['totalkeseluruhan']=$totalkeseluruhan;
		$data['totalkeseluruhan_format']=number_format($totalkeseluruhan,0);
		$data['total_adm_ranap']=$total_adm_ranap;
		$data['total_adm']=$total_adm;
		$data['total_ruangan']=$total_ruangan;
		$data['total_tindakan']=$total_tindakan;
		$data['total_visite']=$total_visite;
		$data['total_obat']=$total_obat;
		$data['total_retur_farmasi']=$total_retur_farmasi;
		$data['total_alkes']=$total_alkes;
		$data['total_ko']=$total_ko;
		$data['total_sewa_ruangan_ko']=$total_sewa_ruangan_ko;
		$data['ko_obat']=$ko_obat;
		$data['ko_alkes']=$ko_alkes;
		$data['ko_implan']=$ko_implan;
		$data['ko_narcose']=$ko_narcose;
		$data['total_ko_fc']=$total_ko_fc;
		$data['total_fisio']=$total_fisio;
		$data['total_radiologi']=$total_radiologi;
		$data['total_lab']=$total_lab;
		$data['total_obat_rajal']=$total_obat_rajal;
		$data['totalkeseluruhan_alkes_rajal']=$totalkeseluruhan_alkes_rajal;
		$data['totalkeseluruhan_obat_rajal']=$totalkeseluruhan_obat_rajal;
		$data['totalkeseluruhan_tindakan_igd']=$totalkeseluruhan_tindakan_igd;
		$data['total_farmasi']=$total_farmasi;
		$data['total_alat_bantu']=$total_alat_bantu;
		
		$data['total_obat_ranap']=$total_obat_ranap;
		$this->output->set_output(json_encode($data));
	}
	function load_user_approval()
    {
	  $id=$this->input->post('id');
	  $where='';
	  $from="(
				SELECT S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak,S.deskrpisi FROM trawatinap_deposit H
				INNER JOIN mlogic_deposit S ON S.tipe_deposit=H.jenis_deposit_id
				INNER JOIN musers U ON U.id=S.iduser
				WHERE H.id='$id' AND S.status='1'
				ORDER BY S.step,S.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();
		// print_r($from);exit;
      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='0';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->deskrpisi;
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
   function simpan_proses_peretujuan(){
		$id=$this->input->post('id');
		
		$result=$this->Ttindakan_ranap_deposit_model->simpan_proses_peretujuan($id);
		echo json_encode($result);
	}
	function list_user($id){
		$q="SELECT *FROM trawatinap_deposit_approval H WHERE H.deposit_id='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td>'.$r->deskrpisi.'</td>';
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';
			$content .='</tr>';

		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
	public function upload() {
       $uploadDir = './assets/upload/deposit/';
		if (!empty($_FILES)) {
			 $deposit_id = $this->input->post('idupload');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['deposit_id'] 	= $deposit_id;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('trawatinap_deposit_upload', $detail);
		}
    }
	function refresh_image($id){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT  H.* from trawatinap_deposit_upload H

			WHERE H.deposit_id='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/deposit/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			if (UserAccesForm($user_acces_form,array('2613'))){
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" type="button" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			}else{
				
			$tabel .='<td class="text-left"></td>';
			}
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		$arr['detail']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function list_tindakan($id){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT st_setuju_tanpa_deposit FROM setting_deposit";
		$st_setuju_tanpa_deposit=$this->db->query($q)->row('st_setuju_tanpa_deposit');
		$q="
			SELECT H.id as last_id FROM trawatinap_deposit_head H
				WHERE H.idrawatinap='$id' AND H.st_persetujuan='1'
				ORDER BY H.setuju_date DESC
				LIMIT 1
		";
		$last_id=$this->db->query($q)->row('last_id');
		
		$q="SELECT ST.id,ST.idrawatinap,(CONCAT(ST.deskripsi, ' (',ST.nama_tindakan,')')) as nama_tindakan,ST.st_persetujuan
			,ST.setuju_date,SUM(COALESCE(D.nominal,0)) as nominal_deposit,TD.total_deposit_all
			,MU.`name` as nama_user,ST.created_date,ST.perencanaan_id,RB.nopermintaan
				FROM `trawatinap_deposit_head` ST 
				LEFT JOIN trawatinap_deposit D ON D.head_id=ST.id AND D.st_proses='1' AND D.`status`='1'
				LEFT JOIN (
					SELECT D.idrawatinap,SUM(D.nominal) as total_deposit_all 
						FROM trawatinap_deposit D
						WHERE D.`status`='1' AND D.st_proses='1'
						GROUP BY D.idrawatinap
				) TD ON TD.idrawatinap=ST.idrawatinap
				LEFT JOIN musers MU ON MU.id=ST.setuju_user_id
				LEFT JOIN tpoliklinik_ranap_rencana_biaya RB ON RB.assesmen_id=ST.perencanaan_id
				WHERE ST.tipe_deposit AND ST.tipe_deposit='1' AND ST.`status` = '1' AND ST.idrawatinap='$id'
				GROUP BY ST.id
			";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			$aksi='';
			if ($r->st_persetujuan=='0'){
				if (UserAccesForm($user_acces_form,array('2622'))){
					if ($st_setuju_tanpa_deposit=='0'){
						if ($r->nominal_deposit>0){
							$aksi='<button class="btn btn-xs btn-success" type="button" onclick="show_setuju_tindakan('.$r->id.')" title="Setuju Tindakan"><li class="fa fa-check"></li> SETUJU TINDAKAN</button>';
						}else{
							$aksi='<button class="btn btn-xs btn-success" type="button" disabled onclick="show_setuju_tindakan('.$r->id.')" title="Setuju Tindakan"><li class="fa fa-check"></li> SETUJU TINDAKAN</button>';
							
						}
					}else{
						$aksi='<button class="btn btn-xs btn-success" type="button" onclick="show_setuju_tindakan('.$r->id.')" title="Setuju Tindakan"><li class="fa fa-check"></li> SETUJU TINDAKAN</button>';
					}
				}
			}else{
				if ($last_id==$r->id){
				$aksi=text_primary('TINDAKAN SAAT INI');
					
				}else{
					
				$aksi=text_danger('SELESAI');
				}
				
			}
			if ($r->st_persetujuan=='0'){
				$btn_cetak=' <a class="btn btn-danger btn-xs" target="_blank" href="'.site_url().'ttindakan_ranap_deposit/cetak_estimasi_biaya/'.$r->perencanaan_id.'/'.$r->id.'/0"  type="button" title="Estimas Biaya" type="button"><i class="fa fa-file-pdf-o"></i></a>';
			}else{
				$btn_cetak='';
				$btn_cetak=' <a href="'.base_url().'assets/upload/deposit/'.$r->nopermintaan.'.pdf" title="Estimasi Biaya" target="_blank" class="btn btn-danger btn-xs" ><i class="fa fa-file-pdf-o"></i></a>';
			}
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.HumanDateLong($r->created_date).'</td>';
			$tabel .='<td class="text-left">'.($r->nama_tindakan).'</td>';
			$tabel .='<td class="text-right">'.($r->nominal_deposit>0?number_format($r->nominal_deposit,0):text_warning('BELUM DEPOSIT')).'</td>';
			$tabel .='<td class="text-right">'.($r->total_deposit_all>0?number_format($r->total_deposit_all,0):text_warning('BELUM DEPOSIT')).'</td>';
			$tabel .='<td class="text-center">'.($r->st_persetujuan?$r->nama_user.'-'.HumanDateLong($r->setuju_date):text_default('BELUM DISETUJUI')).'</td>';
			$tabel .='<td class="text-center">'.$aksi.$btn_cetak.'</td>';
			$tabel .='</tr>';
		}
		$arr['detail']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function list_tindakan_opr($id){
		$q="SELECT st_setuju_tanpa_deposit FROM setting_deposit";
		$st_setuju_tanpa_deposit=$this->db->query($q)->row('st_setuju_tanpa_deposit');
		$q="
			SELECT H.id as last_id FROM trawatinap_deposit_head H
				WHERE H.idrawatinap='$id' AND H.st_persetujuan='1'
				ORDER BY H.setuju_date DESC
				LIMIT 1
		";
		$last_id=$this->db->query($q)->row('last_id');
		
		$q="SELECT ST.id,ST.idrawatinap,(CONCAT(ST.deskripsi, ' (',ST.nama_tindakan,')')) as nama_tindakan,ST.st_persetujuan
			,ST.setuju_date,SUM(COALESCE(D.nominal,0)) as nominal_deposit,TD.total_deposit_all
			,MU.`name` as nama_user,ST.created_date,ST.perencanaan_id,RB.nopermintaan
				FROM `trawatinap_deposit_head` ST 
				LEFT JOIN trawatinap_deposit D ON D.head_id=ST.id AND D.st_proses='1' AND D.`status`='1'
				LEFT JOIN (
					SELECT D.idrawatinap,SUM(D.nominal) as total_deposit_all 
						FROM trawatinap_deposit D
						WHERE D.`status`='1' AND D.st_proses='1'
						GROUP BY D.idrawatinap
				) TD ON TD.idrawatinap=ST.idrawatinap
				LEFT JOIN musers MU ON MU.id=ST.setuju_user_id
				LEFT JOIN tpoliklinik_ranap_rencana_biaya RB ON RB.assesmen_id=ST.perencanaan_id
				WHERE ST.tipe_deposit AND ST.tipe_deposit='1' AND ST.`status` = '1' AND ST.idrawatinap='$id'
				GROUP BY ST.id
			";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			$aksi='';
			if ($r->st_persetujuan=='1'){
				
				if ($last_id==$r->id){
					$aksi=text_primary('TINDAKAN SAAT INI');
					
				}else{
					
					$aksi=text_danger('SELESAI');
				}
				
			}
			if ($r->st_persetujuan=='0'){
				$btn_cetak=' <a class="btn btn-danger btn-xs" target="_blank" href="'.site_url().'ttindakan_ranap_deposit/cetak_estimasi_biaya/'.$r->perencanaan_id.'/'.$r->id.'/0"  type="button" title="Estimas Biaya" type="button"><i class="fa fa-file-pdf-o"></i></a>';
			}else{
				$btn_cetak='';
				$btn_cetak=' <a href="'.base_url().'assets/upload/deposit/'.$r->nopermintaan.'.pdf" title="Estimasi Biaya" target="_blank" class="btn btn-danger btn-xs" ><i class="fa fa-file-pdf-o"></i></a>';
			}
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.HumanDateLong($r->created_date).'</td>';
			$tabel .='<td class="text-left">'.($r->nama_tindakan).'</td>';
			$tabel .='<td class="text-right">'.($r->nominal_deposit>0?number_format($r->nominal_deposit,0):text_warning('BELUM DEPOSIT')).'</td>';
			$tabel .='<td class="text-right">'.($r->total_deposit_all>0?number_format($r->total_deposit_all,0):text_warning('BELUM DEPOSIT')).'</td>';
			$tabel .='<td class="text-center">'.($r->st_persetujuan?$r->nama_user.'-'.HumanDateLong($r->setuju_date):text_default('BELUM DISETUJUI')).'</td>';
			$tabel .='<td class="text-center">'.$aksi.$btn_cetak.'</td>';
			$tabel .='</tr>';
		}
		$arr['detail']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	public function cetak_estimasi_biaya($assesmen_id,$head_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_rencana_biaya_label WHERE id='1'";
		$data=$this->db->query($q)->row_array();
		$q="SELECT H.label_estimasi,st_qrcode FROM setting_deposit H";
		$data_setting_deposit=$this->db->query($q)->row_array();
			// print_r($data);exit;
		$q="
			SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,C.ref as nama_cito,I.ref as nama_icu
					,MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					,D.pasien_ttd,MK.nama as nama_kelas
					FROM tpoliklinik_ranap_rencana_biaya H
					LEFT JOIN tpoliklinik_ranap_rencana_biaya_detail D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mkelas MK ON MK.id=D.kelas
					LEFT JOIN merm_referensi C ON C.nilai=H.cito AND C.ref_head_id='123'
					LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			MK.nama as nama_ko,JO.nama as nama_jo,C.ref as nama_cito_pilih,I.ref as nama_icu_pilih
			,MPK.nama as nama_KP,MR.nama as asuransi,K.nama as nama_kelas
			,H.*

			FROM tpoliklinik_ranap_rencana_biaya_detail H 
			LEFT JOIN mkelompok_operasi MK ON MK.id=H.kelompok_operasi_id
			LEFT JOIN erm_jenis_operasi JO ON JO.id=H.jenis_operasi_id
			LEFT JOIN merm_referensi C ON C.nilai=H.cito_pilih AND C.ref_head_id='123'
			LEFT JOIN merm_referensi I ON I.nilai=H.icu_pilih AND I.ref_head_id='122'
			LEFT JOIN mpasien_kelompok MPK ON MPK.id=H.kelompok_pasien_id
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN mkelas K ON K.id=H.kelas
			WHERE H.assesmen_id='$assesmen_id'";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		// print_r($q);exit;
		$q="
			SELECT mppa.nama as nama_setujua,H.setuju_date,H.id as mppa_id,H.st_persetujuan
			FROM trawatinap_deposit_head H
			LEFT JOIN mppa ON mppa.user_id=H.setuju_user_id
			WHERE H.id='$head_id'
		";
		$ttd=$this->db->query($q)->row_array();
		$data=array_merge($data,$data_header,$data_detail,$data_setting_deposit,$ttd);
        $data['title']=$data['nopermintaan'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Ttindakan_ranap_deposit/pdf_estimasi_biaya', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');

        // Render the HTML as PDF
        $dompdf->render();
		 if ($st_create=='1'){
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/deposit/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
			$this->kirim_email_tindakan($head_id);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
        // // Output the generated PDF to Browser
        // $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
	}
	function hapus_file(){
		$id=$this->input->post('id');
		$q="select file_name from trawatinap_deposit_upload where id='$id'
			";
		$row = $this->db->query($q)->row();
		if(file_exists('./assets/upload/deposit/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/deposit/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from trawatinap_deposit_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
}
