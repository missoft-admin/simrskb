<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_bpjskesehatan extends CI_Controller {

	/**
	 * Tarif BPJS Kesehatan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_bpjskesehatan_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Tarif BPJS Kesehatan';
		$data['content'] 		= 'Mtarif_bpjskesehatan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif BPJS Kesehatan",'#'),
									    			array("List",'mtarif_bpjskesehatan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 								=> '',
			'kode' 							=> '',
			'nama' 							=> '',
			'idtipe' 						=> '',
			'tarifrawatjalan' 	=> '',
			'tarifkelas1' 			=> '',
			'tarifkelas2' 			=> '',
			'tarifkelas3' 			=> '',
			'status' 						=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif BPJS Kesehatan';
		$data['content'] 		= 'Mtarif_bpjskesehatan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif BPJS Kesehatan",'#'),
									    			array("Tambah",'mtarif_bpjskesehatan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_bpjskesehatan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 								=> $row->id,
					'kode' 							=> $row->kode,
					'nama' 							=> $row->nama,
					'idtipe' 						=> $row->idtipe,
					'tarifrawatjalan' 	=> $row->tarifrawatjalan,
					'tarifkelas1' 			=> $row->tarifkelas1,
					'tarifkelas2' 			=> $row->tarifkelas2,
					'tarifkelas3' 			=> $row->tarifkelas3,
					'status' 						=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif BPJS Kesehatan';
				$data['content']	 	= 'Mtarif_bpjskesehatan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif BPJS Kesehatan",'#'),
											    			array("Ubah",'mtarif_bpjskesehatan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_bpjskesehatan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_bpjskesehatan');
		}
	}

	function delete($id){
		$this->Mtarif_bpjskesehatan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_bpjskesehatan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('idtipe', 'Tipe', 'trim|required');
		// $this->form_validation->set_rules('tarifrawatjalan', 'Tarif Rawat Jalan', 'trim|required');
		// $this->form_validation->set_rules('tarifkelas1', 'Tarif Kelas 1', 'trim|required');
		// $this->form_validation->set_rules('tarifkelas2', 'Tarif Kelas 2', 'trim|required');
		// $this->form_validation->set_rules('tarifkelas3', 'Tarif Kelas 3', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_bpjskesehatan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_bpjskesehatan','location');
				}
			} else {
				if($this->Mtarif_bpjskesehatan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_bpjskesehatan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtarif_bpjskesehatan/manage';

		if($id==''){
			$data['title'] = 'Tambah Tarif BPJS Kesehatan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Tarif BPJS Kesehatan",'#'),
															array("Tambah",'mtarif_bpjskesehatan')
													);
		}else{
			$data['title'] = 'Ubah Tarif BPJS Kesehatan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Tarif BPJS Kesehatan",'#'),
															array("Ubah",'mtarif_bpjskesehatan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mtarif_bpjskesehatan';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();
			if (UserAccesForm($user_acces_form,array('164'))){
				$this->column_search   = array('kode','nama');
			}else{
				$this->column_search   = array();
			}
			
			$this->column_order    = array('kode','nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->kode;
					$row[] = $r->nama;
					$row[] = number_format($r->tarifrawatjalan);
					$row[] = number_format($r->tarifkelas1);
					$row[] = number_format($r->tarifkelas2);
					$row[] = number_format($r->tarifkelas3);
					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('166'))){
		                $aksi .= '<a href="'.site_url().'mtarif_bpjskesehatan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('167'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_bpjskesehatan" data-urlremove="'.site_url().'mtarif_bpjskesehatan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
		            $aksi .= '</div>';
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
