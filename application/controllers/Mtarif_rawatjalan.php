<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_rawatjalan extends CI_Controller {

	/**
	 * Tarif Rawat Jalan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_rawatjalan_model');
  }

	function index($idpath='0'){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Tarif Rawat Jalan';
		$data['content'] 		= 'Mtarif_rawatjalan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Rawat Jalan",'mtarif_rawatjalan/index'),
														array("List",'#')
													);
		//getLevel0
		$data['idpath'] 			= $idpath;
		$data['list_level0'] = $this->Mtarif_rawatjalan_model->getLevel0();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'nama' 						=> '',
			'jasasarana' 			=> '0',
			'jasapelayanan' 	=> '0',
			'bhp' 						=> '0',
			'biayaperawatan' 	=> '0',
			'total' 					=> '0',
			'idkelompok' 			=> '1',
			'headerpath' 			=> '',
			'old_headerpath' 	=> '',
			'path' 						=> '',
			'level' 					=> '',
			'status' 					=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif Rawat Jalan';
		$data['content'] 		= 'Mtarif_rawatjalan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Rawat Jalan",'#'),
									    			array("Tambah",'mtarif_rawatjalan')
													);

		$data['list_parent'] = $this->Mtarif_rawatjalan_model->getAllParent();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_rawatjalan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'nama' 						=> $row->nama,
					'jasasarana' 			=> $row->jasasarana,
					'jasapelayanan' 	=> $row->jasapelayanan,
					'bhp' 						=> $row->bhp,
					'biayaperawatan' 	=> $row->biayaperawatan,
					'total' 					=> $row->total,
					'idkelompok' 			=> $row->idkelompok,
					'headerpath' 			=> $row->headerpath,
					'old_headerpath' 	=> $row->headerpath,
					'path' 						=> $row->path,
					'level' 					=> $row->level,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif Rawat Jalan';
				$data['content']	 	= 'Mtarif_rawatjalan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Rawat Jalan",'#'),
											    			array("Ubah",'mtarif_rawatjalan')
															);

				$data['list_parent'] = $this->Mtarif_rawatjalan_model->getAllParent($row->headerpath);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_rawatjalan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_rawatjalan','location');
		}
	}

	function delete($id){
		$this->Mtarif_rawatjalan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_rawatjalan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_rawatjalan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_rawatjalan','location');
				}
			} else {
				if($this->Mtarif_rawatjalan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_rawatjalan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mtarif_rawatjalan/manage';

		$data['list_parent'] = $this->Mtarif_rawatjalan_model->getAllParent();

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mtarif_rawatjalan')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mtarif_rawatjalan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function get_child_level($headerpath) {
		$arr = $this->Mtarif_rawatjalan_model->getPathLevel($headerpath);
		$this->output->set_output(json_encode($arr));
	}

	function filter(){
		if($this->input->post('idpath') != '0'){
			$idpath = $this->input->post('idpath');
			redirect("mtarif_rawatjalan/index/$idpath",'location');
		}else{
			redirect('mtarif_rawatjalan/index/0','location');
		}
	}

	function getIndex($idpath='0')
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$panjang=strlen($idpath);
			$this->select = array();
			$this->from   = 'mtarif_rawatjalan';
			$this->join 	= array();
			if ($idpath !='0'){
				$this->where  = array(
					'left(path,'.$panjang.')' => $idpath,
					'status' => '1'
				);
			}else{
				$this->where  = array(
				'status' => '1'
				);
			}


			$this->order  = array(
				'path' => 'ASC'
			);
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$aksi = '<div class="btn-group">';
          if (UserAccesForm($user_acces_form,array('141'))){
              $aksi .= '<a href="'.site_url().'mtarif_rawatjalan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          if (UserAccesForm($user_acces_form,array('142'))){
              $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_rawatjalan" data-urlremove="'.site_url().'mtarif_rawatjalan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
					if ($r->idkelompok == 0) {
						$aksi .= '<a href="'.site_url().'mtarif_rawatjalan/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
					}
          $aksi .= '</div>';

					$row[] = $no;
					$row[] = TreeView($r->level, $r->nama);
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}

	function setting($id){
		if($id != ''){
			$row = $this->Mtarif_rawatjalan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 										=> $row->id,
					'nama' 									=> $row->nama,
					'jasasarana' 						=> $row->jasasarana,
					'group_jasasarana' 			=> $row->group_jasasarana,
					'jasapelayanan' 				=> $row->jasapelayanan,
					'group_jasapelayanan'		=> $row->group_jasapelayanan,
					'bhp' 									=> $row->bhp,
					'group_bhp' 						=> $row->group_bhp,
					'biayaperawatan' 				=> $row->biayaperawatan,
					'group_biayaperawatan' 	=> $row->group_biayaperawatan,
					'total' 								=> $row->total,
					'group_jasasarana_diskon' =>$row->group_jasasarana_diskon,
					'group_jasapelayanan_diskon' =>$row->group_jasapelayanan_diskon,
					'group_bhp_diskon' =>$row->group_bhp_diskon,
					'group_biayaperawatan_diskon' =>$row->group_biayaperawatan_diskon,
					'jasasarana_diskon' =>$row->jasasarana_diskon,
					'jasapelayanan_diskon' =>$row->jasapelayanan_diskon,
					'bhp_diskon' =>$row->bhp_diskon,
					'biayaperawatan_dikson' =>$row->biayaperawatan_dikson,
					'group_diskon_all' =>$row->group_diskon_all,

				);
				$data['error'] 			= '';
				$data['title'] 			= 'Setting Group Pembayaran Tarif Rawat Jalan';
				$data['content']	 	= 'Mtarif_rawatjalan/setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Rawat Jalan",'#'),
											    			array("Setting",'mtarif_rawatjalan')
															);

				$data['list_group_pembayaran'] = $this->Mtarif_rawatjalan_model->getGroupPembayaran();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_rawatjalan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_rawatjalan','location');
		}
	}

	function save_setting() {
		if($this->Mtarif_rawatjalan_model->updateSetting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mtarif_rawatjalan/index/'.$this->input->post('idtipe'), 'location');
		}
	}
}
