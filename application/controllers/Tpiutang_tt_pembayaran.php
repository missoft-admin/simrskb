<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpiutang_tt_pembayaran extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_tt_pembayaran_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'0',
			// 'tgl_trx1'=>$tgl_pertama,
			// 'tgl_trx2'=>date('d-m-Y'),
			
			'tgl_trx1'=>'',
			'tgl_trx2'=>'',
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Pembayaran Piutang Tidak Tertagih';
		$data['content'] 		= 'Tpiutang_tt_pembayaran/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Pembayaran Transaksi",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');

		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$tgl_trx1=$this->input->post('tgl_trx1');
		$tgl_trx2=$this->input->post('tgl_trx2');
		$tanggaldaftar1=$this->input->post('tanggaldaftar1');
		$tanggaldaftar2=$this->input->post('tanggaldaftar2');
		
		// print_r($tipe);exit();
		$where1='';
		$where2='';
		$where='';
		if ($no_reg !=''){
			$where1 .=" AND H.nopendaftaran='$no_reg' ";
		}
		if ($no_medrec !=''){
			$where1 .=" AND H.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND H.namapasien LIKE '%".$nama_pasien."%' ";
		}

		if ($idkelompokpasien !='#'){
			$where1 .=" AND H.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			$where1 .=" AND TK.idrekanan='$idrekanan' ";
		}
		if ($tipe !='#'){
			$where1 .=" AND H.idtipe='$tipe' ";
		}
		if ($status !='#'){
			$where1 .=" AND H.st_lunas='$status' ";
		}


		if ('' != $tgl_trx1) {
            $where1 .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tgl_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tgl_trx2)."'";
        }
		if ('' != $tanggaldaftar1) {
            $where1 .= " AND DATE(H.tanggal_kunjungan) >='".YMDFormat($tanggaldaftar1)."' AND DATE(H.tanggal_kunjungan) <='".YMDFormat($tanggaldaftar2)."'";
        }
        $from = "(
					SELECT H.*,'1' as st_verifikasi_piutang,K.nama as kel_nama
					,B.id as bayar_id,B.st_posting
					FROM `tpiutang_tt_verifikasi` H
					LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
					LEFT JOIN tpiutang_tt_pembayaran B ON B.piutang_id=H.id
					WHERE H.`status`='1' ".$where1."
					
				) as tbl WHERE idtipe IS NOT NULL";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('no_medrec','namapasien','nopendaftaran');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
			$status_verifikasi='';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_tt_pembayaran/');
            $url_kasir        = site_url('tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $url_klaim       = site_url('tklaim_rincian/');
            $url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
			if ($r->st_posting=='1'){
				$status_verifikasi='<br><br>'.text_primary('SELESAI VERIFIKASI');
			}
            $row[] = $r->idtipe;
            $row[] = $r->pembayaran_id;
            $row[] = $r->pendaftaran_id;
            $row[] = $r->kasir_id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_kunjungan).'<br>'.$r->nopendaftaran;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = GetTipePasienPiutang($r->idtipe);
            // $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->no_medrec.'<br>'.($r->title?$r->title.'. ':'').$r->namapasien;
            $row[] = $r->kel_nama;
            $row[] = number_format($r->nominal_tagihan,0);
            $row[] = number_format($r->nominal_bayar,0);
            $row[] = number_format($r->nominal_tagihan - $r->nominal_bayar,0);
            $row[] = HumanDateShort($r->tanggal_jatuh_tempo);
			$row[] = ($r->cara_bayar=='1'?text_success('NON CICILAN'):text_danger('CICILAN '.$r->cicilan_ke.' / '.$r->jml_cicilan));				
			$row[] = ($r->st_lunas=='1'?text_success('LUNAS'):text_default('MENUGGU PEMBAYARAN')).$status_verifikasi;
			
			if ($r->st_lunas=='0'){
				$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url.'bayar/'.$r->id.'" target="_blank"  type="button"  title="Add Pembayaran"><i class="fa fa-credit-card"></i></a>';
				
			}else{				
				$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url.'bayar/'.$r->id.'/disabled" target="_blank"  type="button"  title="Lihat Pembayaran"><i class="fa fa-credit-card"></i></a>';
				if ($r->st_posting=='0'){
				$aksi .= '<button class="view btn btn-xs btn-success" type="button"  title="Verifikasi" onclick="verifikasi('.$r->bayar_id.')"><i class="fa fa-check"></i> Verifikasi</button>';
					
				}
			}
				
			
			if ($r->idtipe < 3){
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
			}else{
				$aksi .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Ranap</li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Global (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
							
						</ul>
					</div>';
			}
			
			

			$aksi.='</div>';
            
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function bayar($id,$disabel=''){
		
		$data=$this->model->detail($id);
		$data['grandtotalnominal'] 				= '0';
		$data['totalnominal'] 				= '0';
		$data['tgl_trx'] 				= '';
		$data['tgl_trx2'] 				= '';
		$data['st_kbo'] 				= '';
		$data['list_pembayaran'] 				= array();
	
			$data['disabel'] 				= $disabel;
		$data['error'] 				= '';
		$data['list_bank'] 	= $this->model->list_bank();
		// $data['list_rekanan'] 	= $this->model->list_rekanan();
		
		$data['title'] 				= 'Pembayaran Detail Piutang ';
		$data['content'] 			= 'Tpiutang_tt_pembayaran/manage_bayar';
		// print_r($data);exit();
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Piutang Rincian",'tpiutang_tt_monitoring/index'), 
										array("List",'#') 
									);
		// print_r($data);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function hapus(){
		
		$iddet=$this->input->post('id');
		
			$data_header=array(
				'status' => 0,
				
				'deleted_by' => $this->session->userdata('user_id'),
				'deleted_nama' => $this->session->userdata('user_name'),
				'deleted_date' => date('Y-m-d H:i:s'),
				
			);
			$this->db->where('id',$iddet);
		$result=$this->db->update('tpiutang_tt_pembayaran',$data_header);
			
		$this->output->set_output(json_encode($result));
	}
	function edit(){
		
		$id=$this->input->post('id');
		
		$result=$this->db->query("SELECT *FROM tpiutang_tt_pembayaran WHERE id='$id'")->row_array();
			
		$this->output->set_output(json_encode($result));
	}
	function save_bayar(){
		
		$iddet=$this->input->post('iddet');
		$piutang_id=$this->input->post('piutang_id');
		$idmetode=$this->input->post('idmetode');
		$idbank=($idmetode=='1'?'0':$this->input->post('idbank'));
		$nominal=RemoveComma($this->input->post('nominal'));
		$ket=$this->input->post('ket');
		if ($iddet==''){
			$data_header=array(
				'piutang_id' => $piutang_id,
				'idmetode' => $idmetode,
				'idbank' => $idbank,
				'tanggal_transaksi' => date('Y-m-d'),
				'nominal' => $nominal,
				'ket' => $ket,
				'created_by' => $this->session->userdata('user_id'),
				'created_nama' => $this->session->userdata('user_name'),
				'created_date' => date('Y-m-d H:i:s'),
				
			);
		$result=$this->db->insert('tpiutang_tt_pembayaran',$data_header);
		}else{
			$data_header=array(
				'piutang_id' => $piutang_id,
				'idmetode' => $idmetode,
				'idbank' => $idbank,
				'tanggal_transaksi' => date('Y-m-d'),
				'nominal' => $nominal,
				'ket' => $ket,
				'edited_by' => $this->session->userdata('user_id'),
				'edited_nama' => $this->session->userdata('user_name'),
				'edited_date' => date('Y-m-d H:i:s'),
				
			);
			$this->db->where('id',$iddet);
			$result=$this->db->update('tpiutang_tt_pembayaran',$data_header);
		}
			
			
		$this->output->set_output(json_encode($result));
	}
	function load_bayar(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT 
				H.id,H.idmetode,CASE WHEN H.idmetode='1' THEN 'TUNAI' ELSE CONCAT('TRANSFER : ',mbank.nama) END as cara_bayar
				,H.idbank,H.nominal,H.ket
				FROM tpiutang_tt_pembayaran H
				LEFT JOIN mbank ON mbank.id=H.idbank AND H.idmetode='2'
				WHERE H.status='1' AND H.piutang_id='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			$total=$total + $r->nominal;
			$aksi       = '<div class="btn-group">';
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input class="iddet" type="hidden" name="iddet[]" value="'.$r->id.'">';
			$tbl .='<td class="text-center">'.$r->cara_bayar.'</td>';
			$tbl .='<td class="text-center">'.$r->ket.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal,2).'</td>';
			$aksi .= '<button class="view btn btn-xs btn-info edit" type="button" '.$disabel.' title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="view btn btn-xs btn-danger hapus" type="button" '.$disabel.'  title="Edit"><i class="fa fa-trash"></i></button>';
			$aksi .='</div>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>PEMBAYARAN Rp.</strong></td>';
		$tbl .='<td class="text-right"><input type="text"  class="form-control input-sm decimal" readonly id="bayar_rp" name="bayar_rp" value="'.$total.'" /></td>';		
		$tbl .='</tr>';
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>SISA Rp.</strong></td>';
		$tbl .='<td class="text-right"><input type="text"  class="form-control input-sm decimal" readonly id="sisa_rp" name="sisa_rp" value="0" /></td>';		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function verifikasi(){
		$id=$this->input->post('id');
		$data=array(
			'st_posting' =>1,
			'posting_by' =>$this->session->userdata('user_id'),
			'posting_nama' =>$this->session->userdata('user_name'),
			'posting_date' =>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$result=$this->db->update('tpiutang_tt_pembayaran',$data);
		
		$this->output->set_output(json_encode($result));
	}
}
