<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apm extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Apm_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		
			$data = $this->Apm_model->get_index_setting();
			$data['list_running'] = $this->Apm_model->list_running();
			$data['list_slider'] = $this->Apm_model->list_slider();
			$data['title_atas'] 			= 'APM ';
			$data['title'] 			= 'APM Setting Index';
			$data['content'] 		= 'Apm/index';
			$data['error'] 		= '';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("APM Setting",'#'),
												  array("Index Setting",'apm_setting')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_apm', $data);
		
	}
	function save_general(){
		if ($this->Apm_model->save_general()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('apm_setting/index/1','location');
		}
		
	}
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/apm_setting/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png';

                $files[] = $file['name'];
				$new_name = time().'-'.$file['name'];
				$config['file_name'] = $new_name;
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'apm_setting_file';

                    $data = array();
                    // $data['idtransaksi']  = $this->input->post('idtransaksi');
                    // $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);
					
                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image(){		
		$arr['detail'] = $this->Apm_model->refresh_image();
		$this->output->set_output(json_encode($arr));
	}
	function removeFile(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
        $query = $this->db->get('apm_setting_file');
        $row = $query->row();
		if(file_exists('./assets/upload/apm_setting/'.$row->filename) && $row->filename !='') {
			unlink('./assets/upload/apm_setting/'.$row->filename);
		}else{
			
		}
		$result=$this->db->query("delete from apm_setting_file WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function simpan_running(){
		$idrunning=$this->input->post('idrunning');
		$isi=$this->input->post('isi');
		$nourut=$this->input->post('nourut');
		$data['isi']=$isi;
		$data['nourut']=$nourut;
		$data['nourut']=$nourut;
		if ($idrunning==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('apm_setting_running_text',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idrunning);
			$hasil=$this->db->update('apm_setting_running_text',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function hapus_running(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('apm_setting_running_text',$data);
		json_encode($hasil);
	}
	function load_running_text(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.isi FROM apm_setting_running_text H WHERE H.status='1' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('isi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->isi;

		$aksi = '<div class="btn-group">';
		if (UserAccesForm($user_acces_form,array('1521'))){
		$aksi .= '<button onclick="edit_running('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		}
		if (UserAccesForm($user_acces_form,array('1522'))){
		$aksi .= '<button onclick="hapus_running('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		}
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_running(){
		$id=$this->input->post('id');
		$q="select *FROM apm_setting_running_text H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function simpan_logic(){
		
		$this->jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$this->idtipe=$this->input->post('idtipe');
		$this->idpoliklinik=$this->input->post('idpoli');
		$this->iddokter=$this->input->post('iddokter');
		$this->st_gc=$this->input->post('st_gc');
		$this->st_sp=$this->input->post('st_sp');
		$this->st_sc=$this->input->post('st_sc');
		
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('apm_pendaftaran_logic',$this);
		  
		  json_encode($hasil);
	}
	function load_logic()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
							H.*,MP.nama as poli,MD.nama as dokter 
							FROM apm_pendaftaran_logic H
							LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							LEFT JOIN mdokter MD ON MD.id=H.iddokter
							ORDER BY H.jenis_pertemuan_id,H.idtipe,H.idpoliklinik
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('dokter','poli');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetJenisKunjungan($r->jenis_pertemuan_id);
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = ($r->idpoliklinik=='0'?text_default('ALL POLIKLINIK'):$r->poli);
          $result[] = ($r->iddokter=='0'?text_default('ALL DOKTER'):$r->dokter);
          $result[] = ($r->st_gc?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sp?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sc?'YA':text_danger('TIDAK'));
		  if (UserAccesForm($user_acces_form,array('1478'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_logic('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_logic(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('apm_pendaftaran_logic',$this);
	  
	  json_encode($hasil);
	  
  }
  function list_poli_2(){
		$idtipe=$this->input->post('idtipe');
		$jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN apm_pendaftaran_logic T ON T.idpoliklinik=H.id AND T.jenis_pertemuan_id='$jenis_pertemuan_id' 
			WHERE H.idtipe='$idtipe' AND T.idpoliklinik IS NULL AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="0" selected>- All Poliklinik -</option>';
		if ($jenis_pertemuan_id!='#'){
			foreach($hasil as $row){
				$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
			}
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	//HAPUS
	
}
