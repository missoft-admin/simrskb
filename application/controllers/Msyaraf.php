<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msyaraf extends CI_Controller {

	/**
	 * Master Sistem Pernyarafan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msyaraf_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Sistem Pernyarafan';
		$data['content'] 		= 'Msyaraf/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Sistem Pernyarafan",'#'),
									    			array("List",'msyaraf')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nourut' 					=> '1',
			'nama' 					=> '',
			'isi_footer' 		=> '',
			'isi_header' 		=> '',
			'staktif' 				=> '',
			'st_locked' 				=> '0'
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Sistem Pernyarafan';
		$data['content'] 		= 'Msyaraf/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Sistem Pernyarafan",'#'),
									    			array("Tambah",'msyaraf')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Msyaraf_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'isi_header' 		=> $row->isi_header,
					'nourut' 		=> $row->nourut,
					'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif,
					'st_locked' 				=> $row->st_locked
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Sistem Pernyarafan';
				$data['content']	 	= 'Msyaraf/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Sistem Pernyarafan",'#'),
											    			array("Ubah",'msyaraf')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('msyaraf','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('msyaraf');
		}
	}
	function hapus_nrs(){
	  $id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$hasil=$this->db->update('msyaraf_cahaya',$data);
	  
	  json_encode($hasil);
	  
  }
  function hapus_wbf(){
	  $id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$hasil=$this->db->update('msyaraf_scale_image',$data);
	  
	  json_encode($hasil);
	  
  }
	function simpan_nrs(){
		$nrs_id=$this->input->post('nrs_id');
			$data=array(
				'syaraf_id'=>$this->input->post('syaraf_id'),
				'nilai'=>$this->input->post('nilai'),
				'keterangan'=>$this->input->post('keterangan'),
				'staktif'=>1,
			);
			if ($nrs_id==''){
				$data['created_by']=$this->session->userdata('user_id');
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil['st_save']=$this->db->insert('msyaraf_cahaya',$data);
				$hasil['st_duplikat']=0;
			}else{
				$data['edited_by']=$this->session->userdata('user_id');
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$nrs_id);
				$hasil['st_save']=$this->db->update('msyaraf_cahaya',$data);
				$hasil['st_duplikat']=0;
			}
		
		  $this->output->set_output(json_encode($hasil));
	}
	function load_nrs()
	{
			$syaraf_id=$this->input->post('syaraf_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `msyaraf_cahaya` H
						where H.staktif='1' AND H.syaraf_id='$syaraf_id'
						GROUP BY H.id ASC


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('keterangan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nilai;
          $result[] = $r->keterangan;
		  $aksi='';
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nrs('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nrs('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function find_nrs(){
	  $syaraf_id=$this->input->post('id');
	  $q="SELECT *FROM msyaraf_cahaya H WHERE H.id='$syaraf_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
	function cek_duplikate_nrs($nourut,$nrs_id){
		$q="SELECT *FROM msyaraf_cahaya WHERE staktif='1' AND nourut='$nourut' AND (id!='$nrs_id')";
		// print_r($q);exit;
		$has=$this->db->query($q)->row('id');
		if ($has){
			return false;
		}else{
			return true;
		}
	}
	//WBF
	function simpan_wbf(){
		$wbf_id=$this->input->post('wbf_id');
		if ($this->cek_duplikate_wbf($this->input->post('nourut'),$wbf_id)){
			$data=array(
				'syaraf_id'=>$this->input->post('syaraf_id'),
				'nourut'=>$this->input->post('nourut'),
				'nilai'=>$this->input->post('nilai'),
				'keterangan'=>$this->input->post('keterangan'),
				'warna'=>$this->input->post('warna'),
				'staktif'=>1,
			);
			if ($wbf_id==''){
				$data['created_by']=$this->session->userdata('user_id');
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil['st_save']=$this->db->insert('msyaraf_scale_image',$data);
				$hasil['st_duplikat']=0;
			}else{
				$data['edited_by']=$this->session->userdata('user_id');
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$wbf_id);
				$hasil['st_save']=$this->db->update('msyaraf_scale_image',$data);
				$hasil['st_duplikat']=0;
			}
		}else{
			$hasil['st_save']=false;
			$hasil['st_duplikat']=1;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function load_wbf()
	{
			$syaraf_id=$this->input->post('syaraf_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `msyaraf_scale_image` H
						where H.staktif='1' AND H.syaraf_id='$syaraf_id'
						GROUP BY H.nourut ASC


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('keterangan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->nourut;
          $result[] = $r->keterangan;
          $result[] = $r->nilai;
		  $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.';color:#000" type="button" >'.$r->warna.'</button>';
		  if ($r->gambar){
			  $img=$r->gambar;
		  }else{
			  $img='default.jpg';
		  }
		  $gambar='
			<div>
				<img src="'.site_url().'assets/upload/wbf/'.$img.'" style="width:80px;height:80px" alt="Avatar">
			</div>
			<div>
				<button onclick="ganti_wbf('.$r->id.')" type="button" title="Ganti Gambar" class="btn btn-default btn-xs block"><i class="fa fa-pencil"></i></button>
			</div>
		   ';
          // $result[] ='<image class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.';color:#000" type="button" >'.$r->warna.'</button>';
          $result[] =$gambar;
		  $aksi='';
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_wbf('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_wbf('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function find_wbf(){
	  $syaraf_id=$this->input->post('id');
	  $q="SELECT *FROM msyaraf_scale_image H WHERE H.id='$syaraf_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
	function cek_duplikate_wbf($nourut,$wbf_id){
		$q="SELECT *FROM msyaraf_scale_image WHERE staktif='1' AND nourut='$nourut' AND (id!='$wbf_id')";
		// print_r($q);exit;
		$has=$this->db->query($q)->row('id');
		if ($has){
			return false;
		}else{
			return true;
		}
	}
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/wbf/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];
				$new_name = time().'-'.$file['name'];
				$config['file_name'] = $new_name;
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'msyaraf_scale_image';

                    $data = array();
                    $id  = $this->input->post('gambar_id');
                    $data['gambar'] = $image_upload['file_name'];

					$this->db->where('id',$id);
                    $this->db->update($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	//
	function delete($id){
		$this->Msyaraf_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('msyaraf','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('isi_header', 'Isi Header', 'trim|required');
		$this->form_validation->set_rules('nourut', 'Nourut', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Msyaraf_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msyaraf/update/'.$id,'location');
				}
			} else {
				if($this->Msyaraf_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msyaraf/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Msyaraf/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Sistem Pernyarafan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Sistem Pernyarafan",'#'),
															array("Tambah",'msyaraf')
													);
		}else{
			$data['title'] = 'Ubah Master Sistem Pernyarafan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Sistem Pernyarafan",'#'),
															array("Ubah",'msyaraf')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'msyaraf';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('nama','isi_header');
      $this->column_order    = array('nama','isi_header');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = ($r->nourut);
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('1962'))){
            $aksi .= '<a href="'.site_url().'msyaraf/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
		
		if (UserAccesForm($user_acces_form,array('1963'))){
			$aksi .= '<a href="#" data-urlindex="'.site_url().'msyaraf" data-urlremove="'.site_url().'msyaraf/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		}
		
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_parameter()
	{
			$syaraf_id=$this->input->post('syaraf_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*,
						GROUP_CONCAT(CONCAT(M.deskripsi_nama,' (',M.skor,')') SEPARATOR '#') as jawaban
						FROM `msyaraf_param` H
						LEFT JOIN msyaraf_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
						where H.staktif='1' AND H.syaraf_id='$syaraf_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('parameter_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->parameter_nama.($r->jawaban?'&nbsp;&nbsp;'.$this->render_jawaban($r->jawaban):'');
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="add_jawaban('.$r->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
		  $aksi .= '<button onclick="edit_parameter('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_parameter('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function render_jawaban($jawaban){
	  $hasil='';
	  $arr=explode('#',$jawaban);
	  foreach($arr as $index=>$val){
		  $hasil .='<span class="badge ">'.$val.'</span> ';
	  }
	  return $hasil;
  }
  function load_nilai()
	{
			$syaraf_id=$this->input->post('syaraf_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `msyaraf_setting_nilai` H
						where H.staktif='1' AND H.syaraf_id='$syaraf_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor_1.' - '.$r->skor_2;
          $result[] = $r->ref_nilai;
          $result[] = ($r->st_tindakan=='1'?text_success('YA'):text_default('TIDAK'));
          $result[] = $r->nama_tindakan;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_nilai(){
		$parameter_id=$this->input->post('parameter_id');
		$syaraf_id=$this->input->post('syaraf_id');
		$skor_1=$this->input->post('skor_1');
		$skor_2=$this->input->post('skor_2');
		$ref_nilai=$this->input->post('ref_nilai');
		$ref_nilai_id=$this->input->post('ref_nilai_id');
		$data=array(
			'syaraf_id'=>$this->input->post('syaraf_id'),
			'skor_1'=>$this->input->post('skor_1'),
			'skor_2'=>$this->input->post('skor_2'),
			'ref_nilai'=>$this->input->post('ref_nilai'),
			'ref_nilai_id'=>$this->input->post('ref_nilai_id'),
			'st_tindakan'=>$this->input->post('st_tindakan'),
			'nama_tindakan'=>$this->input->post('nama_tindakan'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('msyaraf_setting_nilai',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('msyaraf_setting_nilai',$data);
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function simpan_parameter(){
		$syaraf_id=$this->input->post('syaraf_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'syaraf_id'=>$this->input->post('syaraf_id'),
			'parameter_nama'=>$this->input->post('parameter_nama'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('msyaraf_param',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('msyaraf_param',$data);
		}
		  
		  json_encode($hasil);
	}
	function simpan_jawaban(){
		$skor_id=$this->input->post('skor_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'parameter_id'=>$this->input->post('parameter_id'),
			'skor'=>$this->input->post('skor'),
			'deskripsi_nama'=>$this->input->post('deskripsi_nama'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('msyaraf_param_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('msyaraf_param_skor',$data);
		}
		  
		  json_encode($hasil);
	}
	function hapus_nilai(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('msyaraf_setting_nilai',$data);
	  
	  json_encode($hasil);
	  
  }
  function hapus_parameter(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('msyaraf_param',$data);
	  
	  json_encode($hasil);
	  
  }
  
  function hapus_jawaban(){
	  $skor_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$skor_id);
		$hasil=$this->db->update('msyaraf_param_skor',$data);
	  
	  json_encode($hasil);
	  
  }
  function set_default_jawaban(){
	  $id=$this->input->post('id');
	  $parameter_id=$this->input->post('param_id');
	  $q="update msyaraf_param_skor SET st_default='0' where parameter_id='$parameter_id' AND st_default='1'";
	  $this->db->query($q);
	  $data=array(
			'st_default'=>1,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$hasil=$this->db->update('msyaraf_param_skor',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_parameter(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM msyaraf_param H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_nilai(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM msyaraf_setting_nilai H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_jawaban(){
	  $skor_id=$this->input->post('id');
	  $q="SELECT *FROM msyaraf_param_skor H WHERE H.id='$skor_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function load_jawaban()
	{
		$parameter_id=$this->input->post('parameter_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `msyaraf_param_skor` H
							where H.staktif='1' AND H.parameter_id='$parameter_id'
							ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor;
          $result[] = $r->deskripsi_nama.' '.($r->st_default=='1'?text_success('Default'):'');
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_jawaban('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  if ($r->st_default=='0'){
			$aksi .= '<button onclick="set_default('.$r->parameter_id.','.$r->id.')" type="button" title="Set Default" class="btn btn-success btn-xs "><i class="fa fa-check"></i> Default</button>';	
		  }
		  
		  $aksi .= '<button onclick="hapus_jawaban('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
