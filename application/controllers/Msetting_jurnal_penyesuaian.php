<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_penyesuaian extends CI_Controller {

	/**
	 * Setting Jurnal Penyesuaian controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_penyesuaian_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_penyesuaian_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_penyesuaian_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Penyesuaian';
		$data['content'] 		= 'Msetting_jurnal_penyesuaian/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Penyesuaian",'#'),
									    			array("List",'mlogic_penyesuaian')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_pegawai(){
		$idtipe=$this->input->post('idtipe');
		$idkategori=$this->input->post('idkategori');
		$where='';
		if ($idkategori !='#'){
			$where .=" AND M.idkategori='$idkategori'";
		}
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai M  M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}
		
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_sumber_kas($idtipe){
			$q="SELECT * FROM msumber_kas M WHERE M.jenis_kas_id='$idtipe' AND M.status='1' ORDER BY M.nama ASC";
		$opsi='';
		if ($idtipe !='#'){
			
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			// 'idakun_kredit'=>$this->input->post('idakun_kredit'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_penyesuaian',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Penyesuaian
	function load_penyesuaian()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT 
					H.id,H.jenis_kas_id,H.sumber_kas_id,H.idakun
					,J.nama as jenis,S.nama as sumber,CONCAT(A.noakun,' - ',A.namaakun) as akun
					FROM `msetting_jurnal_penyesuaian_detail` H
					LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
					LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					ORDER BY H.jenis_kas_id,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','sumber','jenis');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->jenis);
            $row[] = ($r->sumber_kas_id=='0'?text_default('All Sumber Kas'):$r->sumber);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_penyesuaian('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_penyesuaian(){
		$id_edit=$this->input->post('id_edit');
		$sumber_kas_id=($this->input->post('sumber_kas_id')=='#'?0:$this->input->post('sumber_kas_id'));
		$jenis_kas_id=($this->input->post('jenis_kas_id')=='#'?0:$this->input->post('jenis_kas_id'));
		$idakun=($this->input->post('idakun')=='#'?0:$this->input->post('idakun'));
		$data=array(
			'setting_id'=>1,
			'jenis_kas_id'=>$jenis_kas_id,
			'sumber_kas_id'=>$sumber_kas_id,
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_penyesuaian($jenis_kas_id,$sumber_kas_id)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_penyesuaian_detail',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_penyesuaian($jenis_kas_id,$sumber_kas_id){
		
		$gabung=$jenis_kas_id.'-'.$sumber_kas_id;
		$q="SELECT *FROM msetting_jurnal_penyesuaian_detail S
			WHERE CONCAT(S.jenis_kas_id,'-',S.sumber_kas_id)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_penyesuaian($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_penyesuaian_detail');
		echo json_encode($result);
	}
	
}
