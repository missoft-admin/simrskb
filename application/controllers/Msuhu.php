<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msuhu extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msuhu_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Msuhu_model->get_suhu();
		if (UserAccesForm($user_acces_form,array('1605'))){
			
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Suhu Tubuh';
			$data['content'] 		= 'Msuhu/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Suhu Tubuh",'msuhu')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function update_satuan(){
		$satuan_suhu=$this->input->post('satuan_suhu');
		$data=array(
			'satuan_suhu'=>$this->input->post('satuan_suhu'),
			
		);
		$hasil=$this->db->update('msuhu_satuan',$data);
		
		json_encode($hasil);
	}
  function simpan_suhu(){
		$suhu_id=$this->input->post('suhu_id');
		$data=array(
			'suhu_1'=>$this->input->post('suhu_1'),
			'suhu_2'=>$this->input->post('suhu_2'),
			'kategori_suhu'=>$this->input->post('kategori_suhu'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($suhu_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('msuhu',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$suhu_id);
		    $hasil=$this->db->update('msuhu',$data);
		}
		  
		  json_encode($hasil);
	}
	
	function load_suhu()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `msuhu` H
							where H.staktif='1'
							ORDER BY H.suhu_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('suhu_1','suhu_1','kategori_suhu');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->suhu_1.' - '.$r->suhu_2);
          $result[] = $r->kategori_suhu;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_suhu('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_suhu('.$r->id.')" type="button" title="Hapus Suhu" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_suhu(){
	  $suhu_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$suhu_id);
		$hasil=$this->db->update('msuhu',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_suhu(){
	  $suhu_id=$this->input->post('id');
	  $q="SELECT *FROM msuhu H WHERE H.id='$suhu_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
