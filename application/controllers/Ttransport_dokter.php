<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ttransport_dokter extends CI_Controller
{

    /**
     * Verifikasi Honor Dokter IGD controller.
     * Developer @gunalirezqimauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Ttransport_dokter_model', 'model');
        $this->load->model('Thonor_dokter_model');
    }

    function index()
    {
        $data = array();
        $data['error']      = '';
        $data['title']      = 'Verifikasi Honor Dokter IGD';
        $data['content']    = 'Ttransport_dokter/index';
        $data['breadcrum']  = array(
										            array("RSKB Halmahera",'#'),
										            array("Verifikasi Honor Dokter IGD",'#'),
										            array("List",'ttransport_dokter')
											        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function update($periode)
    {
        if ($periode != '') {
            $data['error']      = '';
            $data['title']      = 'Ubah Verifikasi Honor Dokter IGD';
            $data['content']    = 'Ttransport_dokter/manage';
            $data['breadcrum']  = array(
										                array("RSKB Halmahera",'#'),
										                array("Verifikasi Honor Dokter IGD",'#'),
										                array("Ubah",'ttransport_dokter')
										            );

            $data['periode']     = $periode.'-01';
            $data['list_dokter'] = $this->model->getAllDokterUmum();
            $data['list_jadwal'] = $this->model->getListJadwal($periode);

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('ttransport_dokter');
        }
    }

    function updateTransaksi()
    {
        $idtransaksi = $this->input->post('idtransaksi');
        $tanggal = $this->input->post('tanggal');
        $status_verifikasi = $this->input->post('status_verifikasi');

        $data = array(
          array(
            'nomor_urut' => 1,
            'iddokter' => $this->input->post('iddokter_pagi'),
            'nominal_transport' => RemoveComma($this->input->post('transport_pagi')),
            'tanggal_pembayaran' => YMDFormat($this->input->post('tanggal_pembayaran_pagi')),
            'tanggal_jatuhtempo' => YMDFormat($this->input->post('tanggal_jatuhtempo_pagi')),
          ),
          array(
            'nomor_urut' => 2,
            'iddokter' => $this->input->post('iddokter_siang'),
            'nominal_transport' => RemoveComma($this->input->post('transport_siang')),
            'tanggal_pembayaran' => YMDFormat($this->input->post('tanggal_pembayaran_siang')),
            'tanggal_jatuhtempo' => YMDFormat($this->input->post('tanggal_jatuhtempo_siang')),
          ),
          array(
            'nomor_urut' => 3,
            'iddokter' => $this->input->post('iddokter_malam'),
            'nominal_transport' => RemoveComma($this->input->post('transport_malam')),
            'tanggal_pembayaran' => YMDFormat($this->input->post('tanggal_pembayaran_malam')),
            'tanggal_jatuhtempo' => YMDFormat($this->input->post('tanggal_jatuhtempo_malam')),
          ),
        );

        $this->db->set('iddokter_pagi', $data[0]['iddokter']);
        $this->db->set('nominal_transport_pagi', $data[0]['nominal_transport']);
        $this->db->set('tanggal_pembayaran_pagi', $data[0]['tanggal_pembayaran']);
        $this->db->set('tanggal_jatuhtempo_pagi', $data[0]['tanggal_jatuhtempo']);

        $this->db->set('iddokter_siang', $data[1]['iddokter']);
        $this->db->set('nominal_transport_siang', $data[1]['nominal_transport']);
        $this->db->set('tanggal_pembayaran_siang', $data[1]['tanggal_pembayaran']);
        $this->db->set('tanggal_jatuhtempo_siang', $data[1]['tanggal_jatuhtempo']);

        $this->db->set('iddokter_malam', $data[2]['iddokter']);
        $this->db->set('nominal_transport_malam', $data[2]['nominal_transport']);
        $this->db->set('tanggal_pembayaran_malam', $data[2]['tanggal_pembayaran']);
        $this->db->set('tanggal_jatuhtempo_malam', $data[2]['tanggal_jatuhtempo']);

        $this->db->set('status_verifikasi', $status_verifikasi);

        $this->db->where('id', $idtransaksi);
        if($this->db->update('mdokter_jadwal')){

          if ($status_verifikasi == 1) {
            foreach ($data as $row) {
              $isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($row['iddokter'], $row['tanggal_pembayaran']);
              if ($isStopPeriode == null) {
                $idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($row['iddokter'], $row['tanggal_pembayaran']);

                $iddokter = $row['iddokter'];
                $jasamedis = $row['nominal_transport'];

                $this->db->select("mdokter.id,
                    mdokter.nama,
                    mdokter_kategori.id AS idkategori,
                    mdokter_kategori.nama AS namakategori,
                    mdokter.potonganrspagi,
                    mdokter.pajak");
                $this->db->join("mdokter_kategori", "mdokter_kategori.id=mdokter.idkategori");
                $this->db->where("mdokter.id", $iddokter);
                $this->db->where("mdokter.status", 1);
                $dokter = $this->db->get("mdokter")->row();

                $namadokter = $dokter->nama;

                // $potongan_rs = $dokter->potonganrspagi;
                // $nominal_potongan_rs = $jasamedis * ($dokter->potonganrspagi / 100);
                $potongan_rs = 0;
                $nominal_potongan_rs = 0;

                $pajak_dokter = $dokter->pajak;
                $nominal_pajak_dokter = $jasamedis * ($dokter->pajak / 100);

                $jasamedis_netto = $jasamedis - $nominal_pajak_dokter;

                if ($idhonor == null) {
                    $dataHonorDokter = array(
                      'iddokter' => $iddokter,
                      'namadokter' => $namadokter,
                      'tanggal_pembayaran' => $row['tanggal_pembayaran'],
                      'tanggal_jatuhtempo' => $row['tanggal_jatuhtempo'],
                      'nominal' => 0,
                      'created_at' => date('Y-m-d H:i:s'),
                      'created_by' => $this->session->userdata('user_id')
                    );

                    if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
                        $idhonor = $this->db->insert_id();
                    }
                }
              }

              $dataDetailHonorDokter = array(
                'idhonor' => $idhonor,
                'idtransaksi' => $idtransaksi,
                'jenis_transaksi' => 'pendapatan',
                'jenis_tindakan' => 'PENDAPATAN TRANSPORT',
                'reference_table' => 'mdokter_jadwal',
                'iddetail' => $row['nomor_urut'],
                'namatarif' => 'Transport Dokter '.GetDayIndonesia($tanggal, true, 'number'),
                'idkategori' => $dokter->idkategori,
                'namakategori' => $dokter->namakategori,
                'iddokter' => $row['iddokter'],
                'namadokter' => $namadokter,
                'jasamedis' => $jasamedis,
                'potongan_rs' => $potongan_rs,
                'nominal_potongan_rs' => $nominal_potongan_rs,
                'pajak_dokter' => $pajak_dokter,
                'nominal_pajak_dokter' => $nominal_pajak_dokter,
                'jasamedis_netto' => $jasamedis_netto,
                'tanggal_pemeriksaan' => $tanggal,
                'tanggal_pembayaran' => $row['tanggal_pembayaran'],
                'tanggal_jatuhtempo' => $row['tanggal_jatuhtempo']
              );

              $this->db->insert('thonor_dokter_detail', $dataDetailHonorDokter);
            }
          } else {
            $this->db->where('idtransaksi', $idtransaksi);
            $this->db->where('jenis_transaksi', 'pendapatan');
            $this->db->where('jenis_tindakan', 'PENDAPATAN TRANSPORT');
            $this->db->delete('thonor_dokter_detail');
          }

          return true;
        }else{
          return false;
        }
    }

    function getIndex()
    {
        $this->select = array('mdokter_jadwal.*', 'MIN(mdokter_jadwal.status_verifikasi) AS status_verifikasi');
        $this->from   = 'mdokter_jadwal';
        $this->join 	= array();
        $this->where  = array();
        $this->order  = array(
          'periode' => 'DESC'
        );
        $this->group  = array('periode');

        $this->column_search  = array('DATE_FORMAT(CONCAT(periode,"-01"),"%M %Y")');
        $this->column_order   = array('DATE_FORMAT(CONCAT(periode,"-01"),"%M %Y")');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row) {
            $no++;
            $result = array();

            $action = '';
            if($row->status_verifikasi == 0){
                if(button_roles('ttransport_dokter/update')) {
                  $action = '<a href="'.site_url().'ttransport_dokter/update/'.$row->periode.'" class="btn btn-xs btn-success btn-sm"><i class="fa fa-check"></i> Verifikasi</a>';
                }
            }else{
              $action = '<a href="#" data-toggle="tooltip" title="Terkunci" class="btn btn-xs btn-danger btn-sm"><i class="fa fa-lock"></i></a>';
            }

            $result[] = $no;
            $result[] = GetMonthIndo($row->periode);
            $result[] = StatusVerifTransaksi($row->status_verifikasi);
            $result[] = $action;

            $data[] = $result;
        }
        $output = array(
  	      "draw" => $_POST['draw'],
  	      "recordsTotal" => $this->datatable->count_all(),
  	      "recordsFiltered" => $this->datatable->count_all(),
  	      "data" => $data
        );
        echo json_encode($output);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
