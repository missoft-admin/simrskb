<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tmutasi_kas_approval extends CI_Controller {

	/**
	 * Mutasi Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tmutasi_kas_approval_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_bendahara_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array(
			'tanggal_setoran2'=>'',
			'tanggal_setoran1'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
			'status'=>'0',
			'notransaksi'=>'',
			'deskripsi'=>'',
			'dari'=>'#',
			'ke'=>'#',
		);
		$data['list_sumber_kas'] 	= $this->Tmutasi_kas_approval_model->list_sumber_kas();
		$data['error'] 			= '';
		$data['title'] 			= 'Mutasi Kas Approval';
		$data['content'] 		= 'Tmutasi_kas_approval/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Mutasi Kas Approval",'#'),
									    			array("List",'tmutasi_kas_approval')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	
	  $notransaksi=$this->input->post('notransaksi');
	  $dari=$this->input->post('dari');
	  $ke=$this->input->post('ke');
	  $tanggal_trx1=$this->input->post('tanggal_trx1');
	  $tanggal_trx2=$this->input->post('tanggal_trx2');
	  $status=$this->input->post('status');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  // $where .=" AND H.st_approval='1'";
	  if ($notransaksi !=''){
		  $where .=" AND H.notransaksi='$notransaksi'";
	  }
	  if ($status !='#'){
		  if ($status=='0'){
			  $where .=" AND H.status_approval='0'";
		  }
		  if ($status=='1'){
			  $where .=" AND H.status_approval='1'";
		  }
		  if ($status=='2'){
			  $where .=" AND H.status_approval='2'";
		  }
		 
	  }
	   if ($tanggal_trx1 !='' || $tanggal_trx2 !=''){
		  $where .=" AND (H.tanggal_trx >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_trx <='".YMDFormat($tanggal_trx2)."' )";
	  }
	  if ($dari !='#'){
			$where .=" AND H.dari='$dari'";
	  }
	  if ($ke !='#'){
			$where .=" AND H.ke='$ke'";
	  }
	 
	  $from="(
				SELECT MD.nama as nama_dari,MK.nama as nama_ke,U.`name` as nama_user, H.* ,COUNT(DISTINCT Doc.id) as doc
					,MAX(AP.step) as step
					FROM tmutasi_kas H  
					LEFT JOIN tmutasi_kas_approval AP ON AP.idtransaksi=H.id AND AP.st_aktif='1'
					LEFT JOIN msumber_kas MD ON MD.id=H.dari
					LEFT JOIN msumber_kas MK ON MK.id=H.ke
					LEFT JOIN musers U ON U.id=H.user_created
					LEFT JOIN tmutasi_kas_dokumen Doc ON Doc.idtransaksi=H.id
					WHERE H.st_approval='1' AND  AP.iduser IN ('".$iduser."') ".$where." 
					GROUP BY H.id	
					ORDER BY H.id DESC
				) as tbl";
	// print_r($tanggal_setoran1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_ke','nama_dari','nama_user','notransaksi');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
            $row = array();
			$query=$this->get_respon($r->id,$r->step);
            $action = '';
            $status = '';
			if ($r->status=='1'){
					$action .= '<a href="'.site_url().'tmutasi_kas/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
				if ($r->st_verifikasi=='0'){
					if ($r->st_approval=='0'){
						$action .= '<a href="'.site_url().'tmutasi_kas/update/'.$r->id.'" data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>';
						
					}
					
					
					if ($r->st_approval=='1' && $r->status_approval=='0'){
						$status='<span class="label label-primary" data-toggle="tooltip" title="MENUNGGU">APPROVAL</span>';
						$status .='&nbsp;<button class="btn btn-danger btn-xs" onclick="lihat_user('.$r->id.')"><i class="si si-user-following"></i></button>';
					}else{
						$status='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">BELUM DIVERIFIKASI</span>';
						
					}
				}else{
					$status='<span class="label label-success" data-toggle="tooltip" title="DIBATALKAN">SUDAH DIVERIFIKASI</span>';
				}
				
				
			}else{
				$action='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
				$status='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
			}
  			$respon='';
			if ($query){
				foreach ($query as $res){
					if ($res->iduser==$iduser && $res->step==$r->step){
						if ($res->approve=='0'){
						  $action .= '<button title="Setuju" class="btn btn-primary btn-xs setuju" data-id="'.$res->id.'"><i class="fa fa-check"></i> Setuju</button>';
						  $action .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" data-id="'.$res->id.'"><i class="si si-ban"></i></button>';
						}else{

						  // $action .= '<button title="Batalkan Pesetujuan / Tolak" class="btn btn-primary btn-xs batal" data-id="'.$res->id.'"><i class="fa fa-refresh"></i></button>';
						}
					}
					if ($res->iduser==$iduser){
						$respon .='<span class="label label-warning" data-toggle="tooltip">STEP '.$res->step.'</span> : '.status_approval($res->approve).'<br>';
					}
				}
			}
			$action .= '<a href="'.site_url().'tpenyesuaian_kas/detail/'.$r->dari.'" target="_blank" data-toggle="tooltip" title="Log Mutasi '.$r->nama_dari.'" class="btn btn-warning btn-xs"><i class="si si-doc"></i></a>';
			$action .= '<a href="'.site_url().'tpenyesuaian_kas/detail/'.$r->ke.'" target="_blank" data-toggle="tooltip" title="Log Mutasi '.$r->nama_ke.'" class="btn btn-danger btn-xs"><i class="si si-doc"></i></a>';
			$action .= '<a href="'.site_url().'tmutasi_kas/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Cetak Kwitansi" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
            $row[] = $r->id;
            $row[] = $no;
  			$row[] = $r->notransaksi;
  			$row[] = HumanDateShort($r->tanggal_trx);
  			$row[] = ($r->nama_dari);
  			$row[] = ($r->nama_ke);
  			$row[] = ($r->deskripsi);
            $row[] = number_format($r->nominal);
  			$row[] = ($r->nama_user);
            $row[] = $status;
            $row[] = $respon;
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_respon($idtransaksi,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from tmutasi_kas_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idtransaksi='$idtransaksi'";
	  return $this->db->query($q)->result();
  }
  function setuju_batal($id,$status,$idkasbon=''){
		// $arr=array();
		$q="call update_mutasi_kas('$id', $status) ";
		$result=$this->db->query($q);
		// $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM tkasbon H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
  }
  function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_mutasi_kas('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('tmutasi_kas_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
  }
	
}
