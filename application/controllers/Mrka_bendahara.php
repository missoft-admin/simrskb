<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_bendahara extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_bendahara_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_pengajuan_model');
		$this->load->model('Mrka_belanja_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-30 days"));
		date_add($date2,date_interval_create_from_date_string("30 days"));
		$date1= date_format($date1,"d-m-Y");
		$date2= date_format($date2,"d-m-Y");
		$data = array(
			'tanggal_pengajuan2'=>'',
			'tanggal_pengajuan1'=>'',
			'tanggal_dibutuhkan1'=>$date1,
			'tanggal_dibutuhkan2'=>$date2,
			'idjenis'=>'#',
			'status'=>'3',
		);
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 		= $this->Mrka_bendahara_model->list_jenis_all();
		$data['error'] 			= '';
		$data['title'] 			= 'Bendahara';
		$data['content'] 		= 'Mrka_bendahara/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("List RKA",'#'),
									    			array("List",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $where='';		
	  $no_pengajuan=$this->input->post('no_pengajuan');
	  $nama_kegiatan=$this->input->post('nama_kegiatan');
	  $tipe_rka=$this->input->post('tipe_rka');
	  $idunit_pengaju=$this->input->post('idunit_pengaju');
	  $idjenis=$this->input->post('idjenis');
	  $tanggal_pengajuan1= ($this->input->post('tanggal_pengajuan1'));
	  $tanggal_pengajuan2= ($this->input->post('tanggal_pengajuan2'));
	  $tanggal_dibutuhkan1= ($this->input->post('tanggal_dibutuhkan1'));
	  $tanggal_dibutuhkan2= ($this->input->post('tanggal_dibutuhkan2'));
	  $status= ($this->input->post('status'));
	  if ($status !='#'){
		  if ($status =='3'){
			  $where .=" AND (H.status='$status' OR H.st_validasi='0')";
		  }else{
			$where .=" AND H.status='$status'";
			  
		  }
	  }else{
		  $where .=" AND H.`status` IN ('3','4','5')";		  
	  }
	  // $where='';
	  // $where .=" AND H.status='3'";
	  if ($no_pengajuan !=''){
		  $where .=" AND H.no_pengajuan='$no_pengajuan'";
	  }
	  
	  if ($nama_kegiatan !=''){
		  $where .=" AND H.nama_kegiatan LIKE '%".$nama_kegiatan."%'";
	  }
	  if ($tipe_rka !='#'){
		  $where .=" AND H.tipe_rka='$tipe_rka'";
	  }
	  if ($idunit_pengaju !='#'){
		  $where .=" AND H.idunit_pengaju='$idunit_pengaju'";
	  }
	  if ($idjenis !='#'){
		  $where .=" AND H.idjenis='$idjenis'";
	  }
	  if ($tanggal_pengajuan1 !='' || $tanggal_pengajuan2 !=''){
		  $where .=" AND (H.tanggal_pengajuan >='".YMDFormat($tanggal_pengajuan1)."' AND H.tanggal_pengajuan <='".YMDFormat($tanggal_pengajuan2)."' )";
	  }
	  if ($tanggal_dibutuhkan1 !='' || $tanggal_dibutuhkan2 !=''){
		  $where .=" AND (H.tanggal_dibutuhkan >='".YMDFormat($tanggal_dibutuhkan1)."' AND H.tanggal_dibutuhkan <='".YMDFormat($tanggal_dibutuhkan2)."' )";
	  }
		
	  $from="(
				SELECT H.idpengajuan_asal,H.id,H.no_pengajuan,H.created_date,H.tanggal_pengajuan,H.tipe_rka,H.nama_kegiatan,H.catatan 
				,U.nama as unit_pengaju,H.idunit_pengaju,H.grand_total,H.`status`,H.idjenis
				,H.tanggal_dibutuhkan,H.idunit,cek_logic(H.id) as nama_logic				
				,J.nama as jenis,UL.nama as nama_unit_lain
				,H.st_bendahara,H.st_user_unit,H.st_user_unit_lain
				,H.st_notif,H.st_pencairan,H.nominal_bayar
				,H.st_validasi,H.nominal_bayar_verifikasi,H.nominal_pencairan,H.st_berjenjang,H.total_sisa
				,(SELECT SUM(PA.total_harga) as gt_asal FROM rka_pengajuan_detail_asal PA WHERE PA.idpengajuan=H.id) as gt_asal
				,H.st_approval_lanjutan,H.status_approval_lanjutan
				from rka_pengajuan H
				LEFT JOIN munitpelayanan U ON U.id=H.idunit_pengaju
				LEFT JOIN mjenis_pengajuan_rka J ON J.id=H.idjenis
				LEFT JOIN munitpelayanan UL ON UL.id=H.unit_lain
				INNER JOIN rka_pengajuan_bendahara B ON B.idrka=H.id 
				WHERE H.status IS NOT NULL ".$where." AND B.iduser IN(".$this->session->userdata('user_id').")
				GROUP BY H.id
				ORDER BY H.tanggal_pengajuan,H.id DESC
				) as tbl";
				
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_kegiatan','no_pengajuan','catatan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $iduser=$this->session->userdata('user_id');
      foreach ($list as $r) {
		  
          $no++;
          $row = array();
		  $button_asal='';
		  if ($r->idpengajuan_asal){
			  $button_asal='<br><a href="'.site_url().'mrka_pengajuan/update/'.$r->idpengajuan_asal.'/disabled" target="_blank" title="Lihat Pengajau Asal"><i class="fa fa-folder-open-o"></i></a>';
		  }
		  $unit='';
		  $status_pengajuan='';
		  $notif='';
		  $pencairan='';
		  if ($r->st_notif){
			$notif='<span class="label label-primary">Sudah Notif</span>';			  
		  }
		  if ($r->st_pencairan){
			$pencairan='<span class="label label-danger">Bisa Dicairkan</span>';			  
		  }
		  $aksi = '<div class="btn-group">';
		  // 1405
		  if ($r->st_approval_lanjutan=='0'){
			  if ($r->gt_asal < $r->grand_total){
				$aksi .= '<button data-toggle="tooltip" title="Approval Lanjutan" onclick="approval_lanjutan('.$r->id.')" class="btn btn-primary btn-sm"><i class="si si-action-redo"></i><i class="si si-user-following"></i></button>';
			  }
		  }else{
				$aksi .= '<button data-toggle="tooltip" title="Approval Lanjutan" onclick="lihat_approval_lanjutan('.$r->id.')" class="btn btn-danger btn-sm"><i class="si si-user-following"></i></button>';
			  
		  }
		  if (UserAccesForm($user_acces_form,array('1405'))){
		  $aksi .= '<a href="'.site_url().'mrka_pengajuan/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Detail" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
		  }
		  if (UserAccesForm($user_acces_form,array('1399'))){
			$aksi .= '<button title="Upload Dokument" class="btn btn-warning btn-sm" onclick="upload_image('.$r->id.')"><i class="fa fa-file-image-o"></i></button>';
		  }
		  if ($r->st_approval_lanjutan=='0' || $r->status_approval_lanjutan=='1'){
		  if (UserAccesForm($user_acces_form,array('1406'))){
			  if ($r->status > 2 && $r->status < 5){
				  $aksi .= '<a href="'.site_url().'mrka_bendahara/proses_bendahara/'.$r->id.'" data-toggle="tooltip" title="Proses Pengajuan" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
			  }else{
				  $aksi .= '<a href="'.site_url().'mrka_bendahara/proses_bendahara/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat Data" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>';
				  
			  }
		  }
		  }
		 if ($r->st_approval_lanjutan=='0' || $r->status_approval_lanjutan=='1'){
		  if ($r->st_notif=='0'){
			  // if ($r->nominal_bayar == $r->grand_total){
			  if (UserAccesForm($user_acces_form,array('1408'))){
					  $aksi .= '<button type="button" data-toggle="tooltip" title="Notif" class="btn btn-primary btn-sm notif"><i class="glyphicon glyphicon-log-in"></i></button>';
			  }
			  // }
		  }
		 }
		 if ($r->st_approval_lanjutan=='0' || $r->status_approval_lanjutan=='1'){
		   if ($r->st_pencairan=='0'){
			  // if ($r->nominal_bayar == $r->grand_total){
			  if ($r->st_berjenjang=='0'){
				  
				  if (UserAccesForm($user_acces_form,array('1409'))){
						  $aksi .= '<button type="button" data-toggle="tooltip" title="Pencairan" class="btn btn-danger btn-sm pencairan"><i class="fa fa-angle-double-right"></i></button>';
				  }
			  }
			  // }
		  }
		 }
		  if (UserAccesForm($user_acces_form,array('1409'))){
			 if ($r->st_berjenjang=='1' && $r->nominal_bayar_verifikasi>0){
				 $aksi .= '<a href="'.site_url().'mrka_bendahara/proses_pencairan/'.$r->id.'" data-toggle="tooltip" title="Pengambilan Pencairan" class="btn btn-primary btn-sm"><i class="fa fa-handshake-o"></i></a>';
			 }
			 if ($r->st_berjenjang=='1' && $r->nominal_pencairan>0){
				 if (($r->nominal_bayar_verifikasi - $r->total_sisa) != $r->grand_total){
					$aksi .= '<button data-toggle="tooltip" title="Realisasi Anggaran" disabled class="btn btn-danger btn-sm"><i class="fa fa-book"></i></button>';
				 }else{
					$aksi .= '<a href="'.site_url().'mrka_bendahara/proses_realisasi/'.$r->id.'" data-toggle="tooltip" title="Realisasi Anggaran" class="btn btn-danger btn-sm"><i class="fa fa-book"></i></a>';
					 
				 }
				 
			 }
		  }
		  
		  // if (UserAccesForm($user_acces_form,array('1428'))){
			// $aksi .= '<a href="#" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
		  // }
		  
			$aksi .= '</div>';
		
			if ($r->status!='1'){
				$nama_logic='<button title="Lihat User" class="btn btn-danger btn-xs user"><i class="si si-user"></i></button>';
			  // if ($r->status=='3'){
			  // }else{
				// $nama_logic='';
			  // }			 
		  }
		  $catatan='';
		  if ($r->catatan){
			  $catatan='<br>'.'<p class="text-primary"><i class="fa fa-sticky-note-o"></i> '.$r->catatan.'</p>';
		  }
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $no;//3
          $row[] = $r->no_pengajuan.'<br>'.HumanDateLong($r->created_date);
          $row[] = HumanDateShort($r->tanggal_pengajuan).$button_asal;
          $row[] = tipe_rka($r->tipe_rka);
          $row[] = $r->nama_kegiatan.$catatan;
          $row[] = $r->unit_pengaju;
          $row[] = HumanDateShort($r->tanggal_dibutuhkan);
          $row[] = number_format($r->grand_total,0);
          $row[] = $r->jenis;
		  if ($r->st_approval_lanjutan=='1' && $r->status_approval_lanjutan=='0'){
			$row[] = text_warning('Menunggu Approval Lanjutan');
		  }elseif ($r->st_approval_lanjutan=='1' && $r->status_approval_lanjutan=='2'){
			$row[] = text_danger('DITOLAK APPROVAL LANJUTAN');

		  }else{
			$row[] = status_pengajuan($r->status).' '.$nama_logic.'<br>'.$notif.' '.$pencairan;
			  
		  }
		  if ($r->st_bendahara=='1'){
			  $unit='<span class="label label-info">Bendahara</span>';
		  }
		  if ($r->st_user_unit=='1'){
			  $unit .=($unit!=''?'<br><br> ':'').'<span class="label label-primary">'.$r->unit_pengaju.'</span>';
		  }
		  if ($r->st_user_unit_lain=='1'){
			  $unit .=($unit!=''?'<br><br>':'').'<span class="label label-danger">'.$r->nama_unit_lain.'</span>';
		  }
		
          $row[] = $unit;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_respon($idrka,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from rka_pengajuan_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idrka='$idrka'";
	  return $this->db->query($q)->result();
  }
  function lihat_approval_lanjutan($id){
		$q="SELECT *FROM rka_pengajuan_approval_lanjutan H WHERE H.idrka='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';			
			$content .='</tr>';
			
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
  function load_user_approval()
    {
	  $idrka=$this->input->post('idrka');	 
	  $where='';
	  $from="(
				SELECT rka_pengajuan.grand_total,U.`name` as user_nama, mlogic_detail.*  FROM rka_pengajuan 
				LEFT JOIN mlogic_unit ON  rka_pengajuan.idunit_pengaju = mlogic_unit.idunit
				LEFT JOIN mlogic_detail ON  mlogic_unit.idlogic = mlogic_detail.idlogic AND rka_pengajuan.tipe_rka = mlogic_detail.tipe_rka AND rka_pengajuan.idjenis = mlogic_detail.idjenis AND mlogic_detail.`status` = 1
				LEFT JOIN  musers U ON U.id=mlogic_detail.iduser
				WHERE rka_pengajuan.id ='$idrka' AND  calculate_logic(mlogic_detail.operand,rka_pengajuan.grand_total,mlogic_detail.nominal) = 1
				ORDER BY mlogic_detail.step,mlogic_detail.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
          $row[] = $r->step;
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_user_approval_lanjutan()
    {
	  $idrka=$this->input->post('idrka');	 
	  $where='';
	  $disabel='';
	  $from="(
				SELECT * FROM rka_pengajuan_approval_lanjutan WHERE idrka='$idrka' ORDER BY step,id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
          $row[] = $r->step;
          $row[] = $r->user_nama;
		  $btn ='<div class="btn-group"><button type="button" '.$disabel.' class="btn btn-xs btn-info" onclick="edit_approval('.$r->id.','.$r->iduser.','.$r->step.')"><i class="glyphicon glyphicon-pencil"></i></button>';
		  $btn .='<button type="button" '.$disabel.' class="btn btn-xs btn-danger" onclick="hapus_approval('.$r->id.')"><i class="glyphicon glyphicon-remove"></i></button></div>';
          $row[] = $btn;
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function setuju_batal($id,$status){
		$q="call update_rka('$id', $status) ";
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function setuju_notif($id,$status){
		$q="UPDATE rka_pengajuan set st_notif='1' WHERE id='$id'";
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function setuju_pencairan($id,$status){
		
		// $q="UPDATE rka_pengajuan set st_pencairan='1',status='4' WHERE id='$id'";
		$q="UPDATE rka_pengajuan set st_pencairan='1',status='4' WHERE id='$id'";
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function verifikasi_pencairan(){
		$id=$this->input->post('id');
		$q="UPDATE rka_pengajuan_pencairan set st_verifikasi='1' WHERE id='$id'";
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function hapus_pencairan(){
		$id=$this->input->post('id');
		$q="DELETE FROM rka_pengajuan_pencairan WHERE id='$id'";
		
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function hapus_approval(){
		$id=$this->input->post('id');
		$q="DELETE FROM rka_pengajuan_approval_lanjutan WHERE id='$id'";
		
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function simpan_proses_peretujuan($id){
		$q="SELECT H.id FROM rka_pengajuan_approval_lanjutan H
		WHERE H.idrka='$id'
		ORDER BY H.step,H.id ASC
		LIMIT 1";
		$min_id=$this->db->query($q)->row('id');
		$q="UPDATE rka_pengajuan_approval_lanjutan SET st_aktif='1' WHERE id='$min_id'";		
		$result=$this->db->query($q);
		

		$q="UPDATE rka_pengajuan SET st_approval_lanjutan='1',status_approval_lanjutan='0' WHERE id='$id'";		
		$result=$this->db->query($q);

		echo json_encode($result);
	}
	function edit_pencairan(){
		$id=$this->input->post('id');
		$q="SELECT * FROM rka_pengajuan_pencairan WHERE id='$id'";
		
		$result=$this->db->query($q)->row_array();
		$result['tanggal']=HumanDateShort($result['tanggal']);
		echo json_encode($result);
	}
	function save_informasi(){
		$idrka=$this->input->post('idrka');
		$tgl_reminder=$this->input->post('tgl_reminder');
		$informasi=$this->input->post('informasi');
		$informasi_id=$this->input->post('informasi_id');
		if ($informasi_id==''){
			$data=array(
				'tanggal'=>date('Y-m-d H:i:s'),
				'user_nama'=>$this->session->userdata('user_name'),
				'user_id'=>$this->session->userdata('user_id'),
				'informasi'=>$informasi,
				'idrka'=>$idrka,
				'tgl_reminder'=>YMDFormat($tgl_reminder),
			);
			$result=$this->db->insert('rka_pengajuan_info',$data);
		}else{
			$data=array(
				'user_nama'=>$this->session->userdata('user_name'),
				'user_id'=>$this->session->userdata('user_id'),
				'informasi'=>$informasi,
				'tgl_reminder'=>YMDFormat($tgl_reminder),
			);
			$result=$this->db->update('rka_pengajuan_info',$data,array('id',$informasi_id));
		}
		echo json_encode($result);
	}
	function save_pengambilan(){
		$st_verifikasi=$this->input->post('st_verifikasi');
		$idrka=$this->input->post('id');
		$id_pengambilan=$this->input->post('id_pengambilan');
		$tanggal_pengambilan=YMDFormat($this->input->post('tanggal_pengambilan'));
		$nominal=RemoveComma($this->input->post('nominal_pengambilan'));
		$keterangan=$this->input->post('ket_pengambilan');
		$iduser_pengambil=$this->input->post('iduser_pengambil');
		if ($id_pengambilan==''){
			$data=array(
				'idrka'=>$idrka,
				'tanggal'=>$tanggal_pengambilan,
				'nominal'=>$nominal,
				'keterangan'=>$keterangan,
				'iduser_pengambil'=>$iduser_pengambil,
				'st_verifikasi'=>$st_verifikasi,
				'status'=>'1',
				'created_by'=>$this->session->userdata('user_id'),
				'created_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->insert('rka_pengajuan_pencairan',$data);
		}else{
			$data=array(
				'idrka'=>$idrka,
				'tanggal'=>$tanggal_pengambilan,
				'nominal'=>$nominal,
				'keterangan'=>$keterangan,
				'iduser_pengambil'=>$iduser_pengambil,
				'st_verifikasi'=>$st_verifikasi,
				'edited_by'=>$this->session->userdata('user_id'),
				'edited_date'=>date('Y-m-d H:i:s'),
			);
			// print_r($id_pengambilan);
			$this->db->where('id',$id_pengambilan);
			$result=$this->db->update('rka_pengajuan_pencairan',$data);
		}
		echo json_encode($result);
	}
	function generate_cicilan($id){
		$q="SELECT *FROM rka_pengajuan WHERE id='$id'";
		$data_header=$this->db->query($q)->row_array();
		$nama_kegiatan=$data_header['nama_kegiatan'];
		// print_r($data_header);exit;
		if ($data_header['cara_pembayaran']=='2'){//CICILAN
			$q1="SELECT H.idpengajuan,H.tanggal_cicilan,SUM(H.nominal_cicilan) as nominal_cicilan,GROUP_CONCAT(DISTINCT H.keterangan_cicilan) as keterangan_cicilan
				,MAX(H.st_proses_bendahara) as st_proses_bendahara,H.jenis_cicilan 
				FROM `rka_pengajuan_cicilan` H

				WHERE H.idpengajuan='$id'
				GROUP BY tanggal_cicilan,H.jenis_cicilan
				ORDER BY H.tanggal_cicilan
				";
			$result=$this->db->query($q1)->result();
			foreach ($result as $row){
				// $data_header['nama_kegiatan']=$nama_kegiatan.' '.$row->keterangan_cicilan;
				$data_header['tanggal_dibutuhkan']=YMDFormat($row->tanggal_cicilan);
				$data_header['cara_pembayaran']=1;
				$data_header['jenis_pembayaran']=2;
				$data_header['grand_total']=$row->nominal_cicilan;
				$data_header['status']=($row->st_proses_bendahara=='1'?'3':'1');	
				$data_header['st_bendahara']=0;	
				$data_header['st_notif']=0;	
				$data_header['st_pencairan']=0;	
				if ($row->st_proses_bendahara=='0'){
					$data_header['st_user_unit']=0;	
					$data_header['st_user_unit_lain']=0;	
					$data_header['st_user_bendahara']=0;	
					$data_header['unit_lain']=0;	
				}
				
				$data_header['st_generate_cicilan']=1;	
				$data_header['id']=null;	
				$data_header['idpengajuan_asal']=$id;	
				$data_header['sisa_cicilan']=0;	
				$data_header['jml_kali_cicilan']=0;	
				$data_header['per_bulan_cicilan']=0;	
				$data_header['xtanggal_cicilan_pertama']=null;	
				$data_header['catatan']=$row->keterangan_cicilan.' No Pengajuan Awal : '.$data_header['no_pengajuan'];	
				// print_r($data_header);exit();
				$this->db->insert('rka_pengajuan',$data_header);
				$idpengajuan=$this->db->insert_id();
				if ($row->st_proses_bendahara=='1'){
					$q="INSERT INTO rka_pengajuan_bendahara (idrka,tipe,iduser,nama_user,idunit,`status`)
								SELECT '$idpengajuan',H.tipe,H.iduser,H.nama_user,H.idunit,H.`status` FROM `rka_pengajuan_bendahara` H WHERE H.idrka='$id'";
					$this->db->query($q);	
					
					$q="INSERT INTO rka_pengajuan_approval (idrka,step,iduser,user_nama,proses_setuju,proses_tolak,approve,tangaal_approve,st_aktif,user_eksekusi,alasan_tolak,idlogic)
					SELECT '$idpengajuan',step,iduser,user_nama,proses_setuju,proses_tolak,approve,tangaal_approve,st_aktif,user_eksekusi,alasan_tolak,idlogic FROM `rka_pengajuan_approval` H WHERE H.idrka='$id'";
					$this->db->query($q);	
				}
				$q="SELECT D.nama_barang,D.merk_barang,D.satuan 
					,H.nominal_cicilan,H.harga_pokok,H.harga_diskon,H.harga_ppn,H.keterangan_cicilan
					,D.idklasifikasi_det
					FROM `rka_pengajuan_cicilan` H
					LEFT JOIN rka_pengajuan_detail D ON D.id=H.idpengajuan_detail
					WHERE H.tanggal_cicilan='".$row->tanggal_cicilan."' AND H.jenis_cicilan='".$row->jenis_cicilan."' AND H.idpengajuan='$id'";
				$list_detail=$this->db->query($q)->result();
				foreach($list_detail as $rd){
					$data_detail=array(
						'idpengajuan' => $idpengajuan,
						'nama_barang' => $rd->nama_barang.'<br>'.$rd->keterangan_cicilan,
						'merk_barang' => $rd->merk_barang,
						'kuantitas'=>1,
						'satuan' => $rd->satuan,
						'harga_satuan'=>$rd->nominal_cicilan,
						'total_harga'=>$rd->nominal_cicilan,
						'harga_pokok'=>$rd->harga_pokok,
						'harga_diskon'=>$rd->harga_diskon,
						'harga_ppn'=>$rd->harga_ppn,
						'keterangan'=>$rd->keterangan_cicilan,
						'idklasifikasi_det'=>$rd->idklasifikasi_det,
						'status'=>1,
						
						'created_by'=> $this->session->userdata('user_id'),
						'created_nama'=> $this->session->userdata('user_name'),
						'created_date'=>  date('Y-m-d H:i:s'),
					); 
					$this->db->insert('rka_pengajuan_detail', $data_detail);
				}
				
				  
			}
		}elseif ($data_header['cara_pembayaran']=='3'){//TERMIN FIX
			$q="delete FROM rka_pengajuan WHERE rka_pengajuan.idpengajuan_asal='$id'";
			$this->db->query($q);
			$q="UPDATE rka_pengajuan_detail H SET H.nominal_dp='0',H.nominal_dp_ppn='0' WHERE H.idpengajuan='$id'";
			$this->db->query($q);
			// exit;
			$q1="SELECT *from rka_pengajuan_termin where idpengajuan='$id' ORDER BY tanggal_termin";
			$result=$this->db->query($q1)->result();
			$his_hp='';
			foreach ($result as $row){
				// $data_header['nama_kegiatan']=$nama_kegiatan.' '.$row->judul_termin;
				$data_header['tanggal_dibutuhkan']=YMDFormat($row->tanggal_termin);
				$data_header['cara_pembayaran']=1;
				$data_header['jenis_pembayaran']=2;
				$data_header['grand_total']=$row->nominal_termin;
				if ($row->st_proses_bendahara=='0'){
					$data_header['st_user_unit']=0;	
					$data_header['st_user_unit_lain']=0;	
					$data_header['st_user_bendahara']=0;	
					$data_header['unit_lain']=0;	
				}
				// $data_header['status']=1;	
				$data_header['status']=($row->st_proses_bendahara=='1'?'3':'1');	
				$data_header['st_bendahara']=0;	
				$data_header['st_notif']=0;	
				$data_header['st_pencairan']=0;	
				
				$data_header['st_generate_cicilan']=1;	
				$data_header['id']=null;	
				$data_header['idpengajuan_asal']=$id;	
				$data_header['catatan']=$row->judul_termin.' No Pengajuan Awal : '.$data_header['no_pengajuan'];	
				// print_r($data_header);exit();
				$this->db->insert('rka_pengajuan',$data_header);
				$idpengajuan=$this->db->insert_id();
				if ($row->st_proses_bendahara=='1'){
					$q="INSERT INTO rka_pengajuan_bendahara (idrka,tipe,iduser,nama_user,idunit,`status`)
								SELECT '$idpengajuan',H.tipe,H.iduser,H.nama_user,H.idunit,H.`status` FROM `rka_pengajuan_bendahara` H WHERE H.idrka='$id'";
					$this->db->query($q);	
					
					$q="INSERT INTO rka_pengajuan_approval (idrka,step,iduser,user_nama,proses_setuju,proses_tolak,approve,tangaal_approve,st_aktif,user_eksekusi,alasan_tolak,idlogic)
					SELECT '$idpengajuan',step,iduser,user_nama,proses_setuju,proses_tolak,approve,tangaal_approve,st_aktif,user_eksekusi,alasan_tolak,idlogic FROM `rka_pengajuan_approval` H WHERE H.idrka='$id'";
					$this->db->query($q);	
				}
				$q="SELECT H.id,H.idklasifikasi_det,H.nama_barang,H.merk_barang,H.satuan
					,((H.harga_pokok * H.kuantitas)-(H.harga_diskon * H.kuantitas) - H.nominal_dp) as harga_pokok 
					,0 as harga_diskon 
					,(H.harga_ppn * H.kuantitas)-H.nominal_dp_ppn as harga_ppn
					,H.total_harga,H.nominal_dp,H.nominal_dp_ppn,H.keterangan
					FROM `rka_pengajuan_detail` H WHERE H.idpengajuan='$id' AND (H.nominal_dp + H.nominal_dp_ppn) < H.total_harga
					ORDER BY H.id ASC";
				$list_detail=$this->db->query($q)->result();
				$uang_bayar=$row->nominal_termin;
				foreach($list_detail as $rd){
					// print_r($rd->harga_pokok);exit;
					// $his_hp .=' '.$rd->harga_pokok;
					$harga_pokok=0;
					$harga_ppn=0;
					$nominal_cicilan=0;
					
					if ($uang_bayar > 0){
						if ($rd->harga_pokok != $rd->nominal_dp){
							if ($rd->harga_pokok > $uang_bayar){
								$harga_pokok=$uang_bayar;
								$uang_bayar=0;
							}else{
								$harga_pokok=$rd->harga_pokok;
								$uang_bayar=$uang_bayar-$rd->harga_pokok;
							}
						}
						if ($uang_bayar > 0){//PPN
							if ($rd->harga_ppn != $rd->nominal_dp_ppn){
								if ($rd->harga_ppn > $uang_bayar){
									$harga_ppn=$uang_bayar;
									$uang_bayar=0;
								}else{
									$harga_ppn=$rd->harga_ppn;
									$uang_bayar=$uang_bayar-$rd->harga_ppn;
								}
							}
						}
						$nominal_cicilan=$harga_pokok+$harga_ppn;
						$data_detail=array(
							'idpengajuan' => $idpengajuan,
							'nama_barang' => $rd->nama_barang.'<br>'.$row->judul_termin,
							'merk_barang' => $rd->merk_barang,
							'kuantitas'=>1,
							'satuan' => $rd->satuan,
							'harga_satuan'=>$nominal_cicilan,
							'total_harga'=>$nominal_cicilan,
							'harga_pokok'=>$harga_pokok,
							'harga_diskon'=>0,
							'harga_ppn'=>$harga_ppn,
							'keterangan'=>$rd->keterangan,
							'idklasifikasi_det'=>$rd->idklasifikasi_det,
							'nominal_dp'=>$rd->nominal_dp,
							'nominal_dp_ppn'=>$rd->nominal_dp_ppn,
							'status'=>1,
							
							'created_by'=> $this->session->userdata('user_id'),
							'created_nama'=> $this->session->userdata('user_name'),
							'created_date'=>  date('Y-m-d H:i:s'),
						); 
						
					  
					  $this->db->insert('rka_pengajuan_detail', $data_detail);
					  $q="UPDATE rka_pengajuan_detail H SET H.nominal_dp=(H.nominal_dp + $harga_pokok) ,H.nominal_dp_ppn=(H.nominal_dp_ppn+$harga_ppn) WHERE H.id='".$rd->id."'";
					  $this->db->query($q);
					}
				}
			}
			$q="UPDATE rka_pengajuan_detail H SET H.nominal_dp='0',H.nominal_dp_ppn='0' WHERE H.idpengajuan='$id'";
			$this->db->query($q);
		}
		// print_r($his_hp);
		// exit;
		$result=$this->db->update('rka_pengajuan',array('status'=>'99'),array('id'=>$id));
		echo json_encode($result);
		// print_r($data_header);exit();
	}
	function save_pembayaran(){
		$idpembayaran=$this->input->post('idpembayaran');
		$idrka=$this->input->post('idrka');
		$jenis_kas_id=$this->input->post('jenis_kas_id');
		$sumber_kas_id=$this->input->post('sumber_kas_id');
		$nominal_bayar= RemoveComma($this->input->post('nominal_bayar'));
		$idmetode=$this->input->post('idmetode');
		$ket_pembayaran=$this->input->post('ket_pembayaran');
		$tanggal_pencairan=$this->input->post('tanggal_pencairan');
		
		if ($idpembayaran==''){
			$data=array(
				'idrka'=>$idrka,
				'jenis_kas_id'=>$jenis_kas_id,
				'sumber_kas_id'=>$sumber_kas_id,
				'nominal_bayar'=>$nominal_bayar,
				'idmetode'=>$idmetode,
				'ket_pembayaran'=>$ket_pembayaran,
				'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
				'created_by'=>$this->session->userdata('user_id'),
				'created_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->insert('rka_pengajuan_pembayaran',$data);
		}else{
			$data=array(
				'idrka'=>$idrka,
				'jenis_kas_id'=>$jenis_kas_id,
				'sumber_kas_id'=>$sumber_kas_id,
				'nominal_bayar'=>$nominal_bayar,
				'idmetode'=>$idmetode,
				'ket_pembayaran'=>$ket_pembayaran,
				'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
				'edited_by'=>$this->session->userdata('user_id'),
				'edited_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->update('rka_pengajuan_pembayaran',$data,array('id'=>$idpembayaran));
		}
		echo json_encode($result);
	}
	function save_pengembalian(){
		$idpembayaran=$this->input->post('idpembayaran');
		$idrka=$this->input->post('idrka');
		$jenis_kas_id=$this->input->post('jenis_kas_id');
		$sumber_kas_id=$this->input->post('sumber_kas_id');
		$nominal_bayar= RemoveComma($this->input->post('nominal_bayar'));
		$idmetode=$this->input->post('idmetode');
		$ket_pembayaran=$this->input->post('ket_pembayaran');
		$tanggal_pencairan=$this->input->post('tanggal_pencairan');
		
		if ($idpembayaran==''){
			$data=array(
				'idrka'=>$idrka,
				'jenis_kas_id'=>$jenis_kas_id,
				'sumber_kas_id'=>$sumber_kas_id,
				'nominal_bayar'=>$nominal_bayar,
				'idmetode'=>$idmetode,
				'ket_pembayaran'=>$ket_pembayaran,
				'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
				'created_by'=>$this->session->userdata('user_id'),
				'created_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->insert('rka_pengajuan_pengembalian',$data);
		}else{
			$data=array(
				'idrka'=>$idrka,
				'jenis_kas_id'=>$jenis_kas_id,
				'sumber_kas_id'=>$sumber_kas_id,
				'nominal_bayar'=>$nominal_bayar,
				'idmetode'=>$idmetode,
				'ket_pembayaran'=>$ket_pembayaran,
				'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
				'edited_by'=>$this->session->userdata('user_id'),
				'edited_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->update('rka_pengajuan_pengembalian',$data,array('id'=>$idpembayaran));
		}
		echo json_encode($result);
	}
	function hapus_informasi($id){
		$result=$this->db->query("Delete FROM rka_pengajuan_info WHERE id='$id'");
		echo json_encode($result);
	}
	function selesai(){
		$id=$this->input->post('id');
		$result=$this->db->query("UPDATE rka_pengajuan set status='4' WHERE id='$id'");
		echo json_encode($result);
	}
	function hapus_pembayaran(){
		$id=$this->input->post('id');
		$result=$this->db->query("Delete FROM rka_pengajuan_pembayaran WHERE id='$id'");
		echo json_encode($result);
	}
	function hapus_pengembalian(){
		$id=$this->input->post('id');
		$result=$this->db->query("Delete FROM rka_pengajuan_pengembalian WHERE id='$id'");
		echo json_encode($result);
	}
	function load_informasi($id){
		$q="SELECT *FROM rka_pengajuan_info H WHERE H.idrka='$id' ORDER BY H.tgl_reminder ASC";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr>';
			
			$content .='<td>'.$no.'</td>';
			$content .='<td>'.HumanDateShort($r->tanggal).'</td>';
			$content .='<td>'.($r->informasi).'</td>';
			$content .='<td>'.($r->user_nama).'</td>';
			$content .='<td>'.HumanDateShort($r->tgl_reminder).'</td>';
			$content .='<td><button type="button" class="btn btn-sm btn-success edit_info"  title="Edit"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-sm btn-danger hapus_info"  title="Hapus"><i class="fa fa-times"></i></button></td>';
			$content .='<td hidden>'.($r->id).'</td>';
			$content .='</tr>';
			$no=$no+1;
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
	
	}
	
	function save(){
		// print_r($this->input->post());exit();
		$id=$this->Mrka_bendahara_model->saveData();
		if($id==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mrka_bendahara','location');
		}
	}
	function save_realisasi(){
		$id=$this->input->post('id');
		$iddet=$this->input->post('iddet');
		$harga_pokok=$this->input->post('harga_pokok');
		$harga_diskon=$this->input->post('harga_diskon');
		$harga_ppn=$this->input->post('harga_ppn');
		$total_harga=$this->input->post('total_harga');
		$harga_satuan=$this->input->post('harga_satuan');
		$btn_simpan=$this->input->post('st_simpan');
		// print_r($btn_simpan);exit();
		foreach($iddet as $index=>$val){
			$data_detail=array(
				'harga_pokok' =>RemoveComma($harga_pokok[$index]),
				'harga_diskon' =>RemoveComma($harga_diskon[$index]),
				'harga_ppn' =>RemoveComma($harga_ppn[$index]),
				'harga_satuan' =>RemoveComma($harga_satuan[$index]),
				'total_harga' =>RemoveComma($total_harga[$index]),
			);
			$this->db->where('id',$val);
			$this->db->update('rka_pengajuan_detail',$data_detail);
		}
		
		$total_realisasi=RemoveComma($this->input->post('total_realisasi'));
		$total_sisa=RemoveComma($this->input->post('total_sisa'));
		$data_header=array(
			'grand_total' =>$total_realisasi,
			'total_realisasi' =>$total_realisasi,
			'total_sisa' =>$total_sisa,
		);
		$this->db->where('id',$id);
		$status=$this->db->update('rka_pengajuan',$data_header);
		if ($btn_simpan=='2'){
			// print_r('sini');exit;
			$status=$this->Mrka_bendahara_model->insert_validasi_rka_realisasi($id);
		}
		if($status==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mrka_bendahara','location');
		}
	}
	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_rka('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('rka_pengajuan_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
	function proses_bendahara($id,$disabel=''){
		
			$data = $this->Mrka_pengajuan_model->getSpecified($id);
			$data['tanggal_dibutuhkan']=HumanDateShort($data['tanggal_dibutuhkan']);
			$data['tanggal_pengajuan']=HumanDateShort($data['tanggal_pengajuan']);
			$data['list_klasifikasi'] 		= $this->Mrka_pengajuan_model->list_klasifikasi();
			$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
			$data['list_unit_lain'] 		= $this->Mrka_bendahara_model->list_unit_lain();
			$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
			// $data['list_detail'] 		= $this->Mrka_pengajuan_model->list_detail($id);
			$data['list_detail'] 		= array();
			$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
			$data['list_dp_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,1);
			$data['list_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,2);
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			// print_r($data['list_cicilan'] );exit();
			$data['list_dp_termin'] 	=  $this->Mrka_pengajuan_model->list_dp_termin($id);
			$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
			$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
			if ($data['st_realisasi']=='1'){
			$data['disabel'] 			= 'disabled';
				
			}else{
			$data['disabel'] 			= $disabel;
				
			}
			$data['disabel_proses_pencarian'] 			= 'disabled';
			$data['proses_pencairan'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Review Pengajuan Kegiatan';
			$data['content'] 		= 'Mrka_bendahara/manage';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
														array("Tambah",'mrka_pengajuan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	function proses_pencairan($id,$disabel=''){
		
			$data = $this->Mrka_pengajuan_model->getSpecified($id);
			$data['tanggal_dibutuhkan']=HumanDateShort($data['tanggal_dibutuhkan']);
			$data['tanggal_pengajuan']=HumanDateShort($data['tanggal_pengajuan']);
			$data['list_klasifikasi'] 		= $this->Mrka_pengajuan_model->list_klasifikasi();
			$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
			$data['list_unit_lain'] 		= $this->Mrka_bendahara_model->list_unit_lain();
			$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
			// $data['list_detail'] 		= $this->Mrka_pengajuan_model->list_detail($id);
			$data['list_detail'] 		= array();
			$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
			$data['list_dp_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,1);
			$data['list_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,2);
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			// print_r($data['list_cicilan'] );exit();
			$data['list_dp_termin'] 	=  $this->Mrka_pengajuan_model->list_dp_termin($id);
			$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
			$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
			$data['disabel'] 			= 'disabled';
			$data['disabel_proses_pencarian'] 			= '';
			$data['proses_pencairan'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Review Pengajuan Kegiatan';
			$data['content'] 		= 'Mrka_bendahara/manage';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
														array("Tambah",'mrka_pengajuan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	function proses_realisasi($id,$disabel=''){
		
			$data = $this->Mrka_pengajuan_model->getSpecified($id);
			// print_r($data);exit();
			if ($data['st_realisasi']=='0'){
				$q="delete from rka_pengajuan_detail_asal where idpengajuan='$id'";
				$this->db->query($q);
				$q="INSERT IGNORE rka_pengajuan_detail_asal (id,idpengajuan,idklasifikasi_det,nama_barang,merk_barang,kuantitas,satuan,harga_pokok,harga_diskon,harga_ppn,harga_satuan,total_harga,keterangan,created_by,created_nama,created_date,edited_by,edited_nama,edited_date,deleted_by,deleted_nama,deleted_date,status,nominal_dp,nominal_dp_ppn)
					SELECT 
					id,idpengajuan,idklasifikasi_det,nama_barang,merk_barang,kuantitas,satuan,harga_pokok,harga_diskon,harga_ppn,harga_satuan,total_harga,keterangan,created_by,created_nama,created_date,edited_by,edited_nama,edited_date,deleted_by,deleted_nama,deleted_date,status,nominal_dp,nominal_dp_ppn
					FROM 
					rka_pengajuan_detail
					WHERE idpengajuan='$id'";
				$this->db->query($q);
				$this->db->query("UPDATE rka_pengajuan set st_realisasi='1' where id='$id'");
			}
			$q="SELECT R.*
				,A.harga_pokok as harga_pokok_asal 
				,A.harga_ppn as harga_ppn_asal 
				,A.harga_diskon as harga_diskon_asal 
				,A.harga_satuan as harga_satuan_asal 
				,A.total_harga as total_harga_asal 
				FROM `rka_pengajuan_detail` R
				LEFT JOIN rka_pengajuan_detail_asal A ON A.id=R.id
				WHERE R.idpengajuan='$id'";
			$list_detail=$this->db->query($q)->result();
			
			$data['tanggal_dibutuhkan']=HumanDateShort($data['tanggal_dibutuhkan']);
			$data['tanggal_pengajuan']=HumanDateShort($data['tanggal_pengajuan']);
			$data['list_klasifikasi'] 		= $this->Mrka_pengajuan_model->list_klasifikasi();
			$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
			$data['list_unit_lain'] 		= $this->Mrka_bendahara_model->list_unit_lain();
			$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
			// $data['list_detail'] 		= $this->Mrka_pengajuan_model->list_detail($id);
			$data['list_detail'] 		= array();
			$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
			$data['list_dp_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,1);
			$data['list_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,2);
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			// print_r($data['list_cicilan'] );exit();
			$data['list_dp_termin'] 	=  $this->Mrka_pengajuan_model->list_dp_termin($id);
			$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
			$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
			
			$data['list_detail'] 			= $list_detail;
			if ($data['st_validasi']=='1'){
				$data['disabel'] 			= 'disabled';
				
			}else{
				$data['disabel'] 			= $disabel;
				
			}
			$data['error'] 			= '';
			$data['title'] 			= 'PROSES REALISASI';
			$data['content'] 		= 'Mrka_bendahara/manage_realisasi';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("RKA PENGAJUAN",'#'),
														array("Realisasi",'mrka_pengajuan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	public function upload() {
       $uploadDir = './assets/upload/rka_pengajuan';
		if (!empty($_FILES)) {
			 $idrka = $this->input->post('idrka');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['idrka'] 	= $idrka;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('rka_pengajuan_upload', $detail);
		}
    }
	public function upload_bukti() {
       $uploadDir = './assets/upload/rka_pengajuan/upload_bukti';
		if (!empty($_FILES)) {
			 $idrka = $this->input->post('idrka');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['idrka'] 	= $idrka;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('rka_pengajuan_upload_bukti', $detail);
		}
    }
	function get_pembayaran($id){		
		$arr['detail'] = $this->Mrka_bendahara_model->get_pembayaran($id);
		$arr['detail']['tanggal_pencairan']=HumanDateShort($arr['detail']['tanggal_pencairan']);
		$this->output->set_output(json_encode($arr));
	}
	function get_pengembalian($id){		
		$arr['detail'] = $this->Mrka_bendahara_model->get_pengembalian($id);
		$arr['detail']['tanggal_pencairan']=HumanDateShort($arr['detail']['tanggal_pencairan']);
		$this->output->set_output(json_encode($arr));
	}
	function refresh_image($id){		
		$arr['detail'] = $this->Mrka_bendahara_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	function refresh_image_bukti($id){		
		$arr['detail'] = $this->Mrka_bendahara_model->refresh_image_bukti($id);
		$this->output->set_output(json_encode($arr));
	}
	function refresh_pembayaran($id,$disabel=''){	
// rka_pengajuan_pembayaran	
		$q="SELECT  T.st_berjenjang,J.idakun,H.*,JK.nama as jenis_kas,SK.nama as sumber_kas,T.cara_pembayaran from rka_pengajuan_pembayaran H
			LEFT JOIN mjenis_kas JK ON JK.id=H.jenis_kas_id
			LEFT JOIN msumber_kas SK ON SK.id=H.sumber_kas_id
			LEFT JOIN rka_pengajuan T ON T.id=H.idrka			
			LEFT JOIN mjenis_pengajuan_rka J ON J.id=T.idjenis
			WHERE H.idrka='$id'
			ORDER BY H.tanggal_pencairan ASC
			";
			// print_r($q);exit;
		$row= $this->db->query($q)->result();
		$tabel='';
		$total=0;
		$no=1;
		$tgl_awal='';
		foreach ($row as $r){
			$total=$total+$r->nominal_bayar;
			if ($r->idakun){
				$disabel_verif='';
			}else{
				$disabel_verif='disabled';
			}
			if ($r->cara_pembayaran!='1'){
				$disabel_verif='disabled';
			}
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$r->jenis_kas.'</td>';
			$tabel .='<td class="text-center">'.$r->sumber_kas.'</td>';
			$tabel .='<td class="text-center">'.metodePembayaran_bendahara($r->idmetode).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal_bayar).'</td>';
			$tabel .='<td class="text-center">'.($r->tanggal_pencairan?HumanDateShort($r->tanggal_pencairan):'').'</td>';
			$tabel .='<td class="text-left">'.$r->ket_pembayaran.'</td>';
			$action="";
			if ($r->st_verifikasi=='0'){
				
				$action .='<button type="button" class="btn btn-sm btn-success edit_pembayaran" '.$disabel.' title="Edit"><i class="fa fa-pencil"></i></button>';
				$action .='<button type="button" '.$disabel.' class="btn btn-sm btn-danger hapus_pembayaran"  title="Hapus"><i class="fa fa-times"></i></button>';
				if ($r->st_berjenjang){
					if ($tgl_awal!=$r->tanggal_pencairan){
						$action .='<button type="button" '.$disabel.' class="btn btn-sm btn-primary" '.$disabel_verif.' onclick="verifikasi_berjenjang(\''.$r->idrka.'\',\''.YMDFormat($r->tanggal_pencairan).'\',\''.($r->idakun).'\')"  title="Validasi"><i class="fa fa-check"></i> Validasi </button>';
						
					}else{					
						$action .='<button type="button" disabled class="btn btn-sm btn-info"  title="Validasi"><i class="fa fa-check"></i> Tanggal Sama</button>';
					}
				}
			}else{
				$action=text_success('TELAH DIVALIDASI');
			}
			$tgl_awal=$r->tanggal_pencairan;
			$tabel .='<td>'.$action.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		//PENGEMBALIAN
		$q="SELECT  T.st_berjenjang,J.idakun,H.*,JK.nama as jenis_kas,SK.nama as sumber_kas from rka_pengajuan_pengembalian H
			LEFT JOIN mjenis_kas JK ON JK.id=H.jenis_kas_id
			LEFT JOIN msumber_kas SK ON SK.id=H.sumber_kas_id
			LEFT JOIN rka_pengajuan T ON T.id=H.idrka			
			LEFT JOIN mjenis_pengajuan_rka J ON J.id=T.idjenis
			WHERE H.idrka='$id'
			ORDER BY H.tanggal_pencairan ASC
			";
		$row= $this->db->query($q)->result();
		foreach ($row as $r){
			$total=$total+($r->nominal_bayar*-1);
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$r->jenis_kas.'</td>';
			$tabel .='<td class="text-center">'.$r->sumber_kas.'</td>';
			$tabel .='<td class="text-center">'.metodePembayaran_bendahara($r->idmetode).'</td>';
			$tabel .='<td class="text-right text-danger"><strong>'.number_format($r->nominal_bayar*-1).'</strong></td>';
			$tabel .='<td class="text-center">'.($r->tanggal_pencairan?HumanDateShort($r->tanggal_pencairan):'').'</td>';
			$tabel .='<td class="text-left">'.$r->ket_pembayaran.'</td>';
			$action="";
			if ($r->st_verifikasi=='0'){
				
				// $action .='<button type="button" class="btn btn-sm btn-success edit_pengembalian" '.$disabel.' title="Edit"><i class="fa fa-pencil"></i></button>';
				// $action .='<button type="button" '.$disabel.' class="btn btn-sm btn-danger hapus_pengembalian"  title="Hapus"><i class="fa fa-times"></i></button>';				
				// $action .='<button type="button" '.$disabel.' class="btn btn-sm btn-primary" '.$disabel_verif.' onclick="verifikasi_berjenjang(\''.$r->idrka.'\',\''.YMDFormat($r->tanggal_pencairan).'\',\''.($r->idakun).'\')"  title="Validasi"><i class="fa fa-check"></i> Validasi</button>';
					
				$action=text_danger('BELUM DIVALIDASI');
			}else{
				$action=text_success('TELAH DIVALIDASI');
			}
			$tabel .='<td>'.$action.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		$arr['detail'] = $tabel;
		$arr['nominal_bayar'] = $total;
		$this->output->set_output(json_encode($arr));
	}
	function refresh_pengembalian($id,$disabel=''){	
// rka_pengajuan_pengembalian	
		$q="SELECT  T.st_berjenjang,J.idakun,H.*,JK.nama as jenis_kas,SK.nama as sumber_kas from rka_pengajuan_pengembalian H
			LEFT JOIN mjenis_kas JK ON JK.id=H.jenis_kas_id
			LEFT JOIN msumber_kas SK ON SK.id=H.sumber_kas_id
			LEFT JOIN rka_pengajuan T ON T.id=H.idrka			
			LEFT JOIN mjenis_pengajuan_rka J ON J.id=T.idjenis
			WHERE H.idrka='$id'
			ORDER BY H.tanggal_pencairan ASC
			";
		$row= $this->db->query($q)->result();
		$tabel='';
		$total=0;
		$no=1;
		$tgl_awal='';
		foreach ($row as $r){
			$total=$total+$r->nominal_bayar;
			if ($r->idakun){
				$disabel_verif='';
			}else{
				$disabel_verif='disabled';
			}
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$r->jenis_kas.'</td>';
			$tabel .='<td class="text-center">'.$r->sumber_kas.'</td>';
			$tabel .='<td class="text-center">'.metodePembayaran_bendahara($r->idmetode).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal_bayar).'</td>';
			$tabel .='<td class="text-center">'.($r->tanggal_pencairan?HumanDateShort($r->tanggal_pencairan):'').'</td>';
			$tabel .='<td class="text-left">'.$r->ket_pembayaran.'</td>';
			$action="";
			if ($r->st_verifikasi=='0'){
				
				$action .='<button type="button" class="btn btn-sm btn-success edit_pengembalian" '.$disabel.' title="Edit"><i class="fa fa-pencil"></i></button>';
				$action .='<button type="button" '.$disabel.' class="btn btn-sm btn-danger hapus_pengembalian"  title="Hapus"><i class="fa fa-times"></i></button>';				
				// $action .='<button type="button" '.$disabel.' class="btn btn-sm btn-primary" '.$disabel_verif.' onclick="verifikasi_berjenjang(\''.$r->idrka.'\',\''.YMDFormat($r->tanggal_pencairan).'\',\''.($r->idakun).'\')"  title="Validasi"><i class="fa fa-check"></i> Validasi</button>';
					
				
			}else{
				$action=text_success('TELAH DIVALIDASI');
			}
			$tgl_awal=$r->tanggal_pencairan;
			
			$tabel .='<td>'.$action.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		
		$arr['detail'] = $tabel;
		$arr['nominal_bayar'] = $total;
		
		$q="SELECT SUM(H.nominal) as total_validasi_jurnal FROM `tvalidasi_kas_06_pengajuan_piutang` H WHERE H.idtransaksi='$id' AND posisi_akun='D'";
		print_r($q);exit;
		$arr['total_validasi_jurnal'] = $this->db->query($q)->row('total_validasi_jurnal');
		$this->output->set_output(json_encode($arr));
	}
	function refresh_pencairan($id,$disabel=''){	
// rka_pengajuan_pembayaran	
		$q="SELECT H.st_berjenjang,D.id,D.tanggal,D.nominal,D.keterangan,D.iduser_pengambil,U.`name`,D.st_verifikasi FROM `rka_pengajuan_pencairan` D
		LEFT JOIN rka_pengajuan H ON H.id=D.idrka
		LEFT JOIN musers U ON U.id=D.iduser_pengambil
		WHERE D.idrka='$id'
		ORDER BY D.id ASC
			";
		$row= $this->db->query($q)->result();
		$tabel='';
		$total=0;
		$no=1;
		foreach ($row as $r){
			$total=$total+$r->nominal;
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.HumanDateShort($r->tanggal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-center">'.$r->keterangan.'</td>';
			$tabel .='<td class="text-center">'.$r->name.'</td>';
			$action="";
			if ($r->st_verifikasi=='0'){
				
				// $action .='<button type="button" class="btn btn-sm btn-primary " onclick="edit_pencairan(\''.$r->id.'\') '.$disabel.' title="Edit"><i class="fa fa-pencil"></i></button>';
				$action .='<button type="button" '.$disabel.' class="btn btn-sm btn-primary" onclick="edit_pencairan(\''.$r->id.'\')"  title="Edit"><i class="fa fa-pencil"></i></button>';
				$action .='<button type="button" '.$disabel.' class="btn btn-sm btn-success" onclick="verifikasi_pencairan(\''.$r->id.'\')"  title="Validasi"><i class="fa fa-check"></i></button>';
				$action .='<button type="button" '.$disabel.' class="btn btn-sm btn-danger" onclick="hapus_pencairan(\''.$r->id.'\')"  title="Hapus"><i class="fa fa-trash"></i></button>';
				// $action .='<button type="button" '.$disabel.' class="btn btn-sm btn-danger" onclick="hapus_pencairan('.$r->id.') title="Hapus"><i class="fa fa-trash"></i></button>';
				$action .='<a href="#" type="button" '.$disabel.' class="btn btn-sm btn-success" "  title="Validasi"><i class="fa fa-print"></i></a>';
				
			}else{
				$action=text_success('TELAH DIVALIDASI');
			}
			$tabel .='<td class="text-center">'.$action.'</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		$q="SELECT  T.st_berjenjang,J.idakun,H.*,JK.nama as jenis_kas,SK.nama as sumber_kas from rka_pengajuan_pengembalian H
			LEFT JOIN mjenis_kas JK ON JK.id=H.jenis_kas_id
			LEFT JOIN msumber_kas SK ON SK.id=H.sumber_kas_id
			LEFT JOIN rka_pengajuan T ON T.id=H.idrka			
			LEFT JOIN mjenis_pengajuan_rka J ON J.id=T.idjenis
			WHERE H.idrka='$id'
			ORDER BY H.tanggal_pencairan ASC
			";
		$row= $this->db->query($q)->result();
		foreach ($row as $r){
			$total=$total+($r->nominal_bayar*-1);
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.HumanDateShort($r->tanggal_pencairan).'</td>';
			$tabel .='<td class="text-right text-danger text-bold"><strong>'.number_format($r->nominal_bayar*-1).'</strong></td>';
			$tabel .='<td class="text-center">'.$r->ket_pembayaran.'</td>';
			$tabel .='<td class="text-center">-</td>';
			$action="";
			if ($r->st_verifikasi=='0'){
				$action=text_danger('BELUM DIVALIDASI');
			}else{
				$action=text_success('TELAH DIVALIDASI');
			}
			$tabel .='<td class="text-center">'.$action.'</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		$arr['detail'] = $tabel;
		$arr['total_pengambilan'] = $total;
		$this->output->set_output(json_encode($arr));
	}
	function verifikasi_berjenjang(){
		$idrka=$this->input->post('idrka');
		$tanggal_pencairan=$this->input->post('tanggal_pencairan');
		// print_r($tanggal_pencairan);exit;
		$idakun=$this->input->post('idakun');
		$q="SELECT *FROM rka_pengajuan_pembayaran H WHERE H.idrka='$idrka' AND H.st_verifikasi='0' AND H.tanggal_pencairan='$tanggal_pencairan'";
		$list_bayar=$this->db->query($q)->result();
		
		$total_transaksi=0;
		$arr_id=array();
		foreach ($list_bayar as $row){
			$q="update rka_pengajuan_pembayaran SET st_verifikasi='1' WHERE id='".$row->id."'";
			$arr=$this->db->query($q);
			$total_transaksi=$total_transaksi+$row->nominal_bayar;
			array_push($arr_id,$row->id);
		}	
		//CODING VALIDASI KE JURNAL
		$arr=$this->Mrka_bendahara_model->insert_validasi_rka_berjenjang($idrka,$total_transaksi,$arr_id,$idakun,$tanggal_pencairan);
		
		$this->output->set_output(json_encode($arr));
	}
	function tes_validasi_berjejang(){
		$idrka=1;
		$tanggal_pencairan='2022-03-02';
		$idakun=20;
		$total_transaksi=0;
		$q="SELECT *FROM rka_pengajuan_pembayaran H WHERE H.idrka='1' AND H.st_verifikasi='0' AND H.tanggal_pencairan='$tanggal_pencairan'";
		$list_bayar=$this->db->query($q)->result();
		$arr_id=array();
		foreach ($list_bayar as $row){
			$total_transaksi=$total_transaksi+$row->nominal_bayar;			
			array_push($arr_id,$row->id);
		}	
		// print_r(implode(",",$arr_id));exit;
		$this->Mrka_bendahara_model->insert_validasi_rka_berjenjang($idrka,$total_transaksi,$arr_id,$idakun,$tanggal_pencairan);
		
	}
	function list_sumber_kas(){		
		$jenis_kas_id=$this->input->post('jenis_kas_id');
		$sumber_kas_id=$this->input->post('sumber_kas_id');
		// print_r($sumber_kas_id);exitx`();
		$opsi='';
		$q="SELECT *from msumber_kas M WHERE M.jenis_kas_id='$jenis_kas_id'";
		$rows=$this->db->query($q)->result();
		$opsi .='<option value="#" selected>- Pilih Opsi -</option>';
		foreach($rows as $row){
			$opsi .='<option value="'.$row->id.'" '.($row->st_default=='1' && $sumber_kas_id==''?'selected':'').' '.($sumber_kas_id==$row->id?'selected':'').'>'.$row->nama.'</option>';
		}
		$arr['detail'] = $opsi;
		$this->output->set_output(json_encode($arr));
	}
	
	function load_user_add(){		
		$step=$this->input->post('step');
		$idrka=$this->input->post('idrka');
		$id_edit=$this->input->post('id_edit');
		
		$opsi='';
		$q="SELECT U.id,U.`name` FROM musers U
			WHERE U.`status`='1' 
			AND U.id NOT IN (SELECT L.iduser FROM rka_pengajuan_approval_lanjutan L WHERE L.idrka='$idrka' AND L.step='$step' AND L.id !='$id_edit')
			";
		$rows=$this->db->query($q)->result();
		$opsi .='<option value="#" selected>- Pilih Opsi -</option>';
		foreach($rows as $row){
			$opsi .='<option value="'.$row->id.'">'.$row->name.'</option>';
		}
		$arr['detail'] = $opsi;
		$this->output->set_output(json_encode($arr));
	}
	function simpan_detail_lanjutan()
	{
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'idrka'=>$this->input->post('idrka'),
			'step'=>$this->input->post('step'),
			'iduser'=>$this->input->post('iduser'),
			'user_nama'=>$this->input->post('user_nama'),
			'proses_setuju'=>1,
			'proses_tolak'=>1,
			'approve'=>0,
			'st_aktif'=>0,
			
		);
		
       // print_r($data);
	   if ($id_edit){
		   $this->db->where('id',$id_edit);
			$result = $this->db->update('rka_pengajuan_approval_lanjutan',$data);		   
	   }else{
		   $result = $this->db->insert('rka_pengajuan_approval_lanjutan',$data);		
	   }
	   $this->output->set_output(json_encode($result));
		
	}
	function get_data_rek(){		
		$rekening_id=$this->input->post('rekening_id');
		
		$q="SELECT M.bank,H.norekening,H.atasnama,H.cabang FROM mdistributor_bank H
			LEFT JOIN ref_bank M ON M.id=H.bankid
			WHERE H.id='$rekening_id'";
		$rows=$this->db->query($q)->row_array();		
		$this->output->set_output(json_encode($rows));
	}
	function hapus_file(){
		$id=$this->input->post('id');
		$row = $this->Mrka_bendahara_model->get_file_name($id);
		if(file_exists('./assets/upload/rka_pengajuan/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/rka_pengajuan/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from rka_pengajuan_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function hapus_file_bukti(){
		$id=$this->input->post('id');
		$row = $this->Mrka_bendahara_model->get_file_name_bukti($id);
		if(file_exists('./assets/upload/rka_pengajuan/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/rka_pengajuan/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from rka_pengajuan_upload_bukti WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
}
