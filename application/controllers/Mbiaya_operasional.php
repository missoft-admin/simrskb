<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbiaya_operasional extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mbiaya_operasional_model');
		$this->load->helper('path');
		
  }

	function index($idkategori='0',$tipe_pemilik='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['idkategori'] 			= $idkategori;
			$data['tipe_pemilik'] 			= $tipe_pemilik;
			$data['tipe_pemilik'] 			= '#';
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Biaya Operasional';
			$data['content'] 		= 'Mbiaya_operasional/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Biaya Operasional",'#'),
												  array("List",'mbiaya_operasional')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'tipe_pemilik' 	=> '',
			'nama' 					=> '',
			'idpegawai' 				=> '',
			'iddokter' 				=> '',
			'estimasi' 	=> '',
			'keterangan' 	    => '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Biaya Operasional';
		$data['content'] 		= 'Mbiaya_operasional/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Biaya Operasional",'#'),
								            array("Tambah",'mbiaya_operasional')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mbiaya_operasional_model->getSpecified($id);
			
			
			
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Biaya Operasional';
			$data['content']    = 'Mbiaya_operasional/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Biaya Operasional",'#'),
										array("Ubah",'mbiaya_operasional')
										);

			// $data['statusAvailableApoteker'] = $this->Mbiaya_operasional_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mbiaya_operasional');
		}
	}

	function delete($id){
		
		$result=$this->Mbiaya_operasional_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mbiaya_operasional','location');
	}
	function aktifkan($id){
		
		$result=$this->Mbiaya_operasional_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
	
		if($this->input->post('id') == '' ) {
			if($this->Mbiaya_operasional_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mbiaya_operasional','location');
			}
		} else {
			if($this->Mbiaya_operasional_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mbiaya_operasional','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mbiaya_operasional/manage';

		if($id==''){
			$data['title'] = 'Tambah Biaya Operasional';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Biaya Operasional",'#'),
							               array("Tambah",'mbiaya_operasional')
								           );
		}else{
			$data['title'] = 'Ubah Biaya Operasional';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Biaya Operasional",'#'),
							               array("Ubah",'mbiaya_operasional')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex($idkategori='0',$tipe_pemilik='0')
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			
			if ($status !='#'){
				$where .=" AND status='$status'";
			}
			if ($nama !=''){
				$where .=" AND nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT 
						*

						from mbiaya_operasional M


					) as tbl WHERE id IS NOT NULL ".$where."
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','estimasi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->nama;
          $result[] = number_format($r->estimasi,0);
         $result[] = StatusBarang($r->status);
          // $result[] = GetKategoriPegawai($r->idkategori);
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'mbiaya_operasional/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<button title="Hapus" class="btn btn-danger btn-xs removeData"><i class="fa fa-trash-o"></i></button>';
			}else{
				$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
}
