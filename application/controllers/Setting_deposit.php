<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_deposit extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_deposit_model');
		$this->load->model('Antrian_poli_kode_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='1'){		
			if (UserAccesForm($user_acces_form,array('2620'))){
				$tab='5';
			}
			if (UserAccesForm($user_acces_form,array('2607'))){
				$tab='4';
			}
			if (UserAccesForm($user_acces_form,array('2606'))){
				$tab='3';
			}
			if (UserAccesForm($user_acces_form,array('2605'))){
				$tab='2';
			}
			if (UserAccesForm($user_acces_form,array('2604'))){
				$tab='1';
			}
		}
		// print_r($tab);exit;
		if (UserAccesForm($user_acces_form,array('2604','2605','2606','2607','2620'))){
			$q="SELECT st_edit_deposit,st_draft_deposit,st_proses_selesai_deposit,persen_deposit,kali_deposit,email_deposit,hari_batal_deposit,qr_deposit,desk_deposit
			,st_setuju_tanpa_deposit,st_edit_tanggal_persetujuan,st_qrcode,email_tindakan,label_estimasi,judul_email

			FROM setting_deposit";
			$data=$this->db->query($q)->row_array();
			// if ($tab=='4'){
				$q="SELECT * FROM setting_deposit_label";
				$data_lembar=$this->db->query($q)->row_array();
				$data = array_merge($data,$data_lembar);
			// }
		
			$data['tab'] 			= $tab;
			
			$data['jenis_isi'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Deposit';
			$data['content'] 		= 'Setting_deposit/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pengaturan Deposit",'#'),
												  array("Setting Akses Setting",'setting_deposit/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function simpan_general(){
		$data=array(
			'st_edit_deposit' => $this->input->post('st_edit_deposit'),
			'st_draft_deposit' => $this->input->post('st_draft_deposit'),
			'st_proses_selesai_deposit' => $this->input->post('st_proses_selesai_deposit'),
			'persen_deposit' => $this->input->post('persen_deposit'),
			'kali_deposit' => $this->input->post('kali_deposit'),
			'email_deposit' => $this->input->post('email_deposit'),
			'hari_batal_deposit' => $this->input->post('hari_batal_deposit'),
			'qr_deposit' => $this->input->post('qr_deposit'),
			'desk_deposit' => $this->input->post('desk_deposit'),
		);
		// print_r($data);exit;
		$hasil=$this->db->update('setting_deposit',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_tab_5(){
		$data=array(
			'st_setuju_tanpa_deposit' => $this->input->post('st_setuju_tanpa_deposit'),
			'st_edit_tanggal_persetujuan' => $this->input->post('st_edit_tanggal_persetujuan'),
			'st_qrcode' => $this->input->post('st_qrcode'),
			'email_tindakan' => $this->input->post('email_tindakan'),
			'label_estimasi' => $this->input->post('label_estimasi'),
			'judul_email' => $this->input->post('judul_email'),
		);
		// print_r($data);exit;
		$hasil=$this->db->update('setting_deposit',$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	function save_label_lembar(){
		// print_r($this->input->post());exit;
			$data=array(
				'alamat_form' => $this->input->post('alamat_form'),
				'telepone_form' => $this->input->post('telepone_form'),
				'email_form' => $this->input->post('email_form'),
				'judul_ina' => $this->input->post('judul_ina'),
				'judul_eng' => $this->input->post('judul_eng'),
				
				'no_register_ina' => $this->input->post('no_register_ina'),
				'no_register_eng' => $this->input->post('no_register_eng'),
				'no_kwitansi_ina' => $this->input->post('no_kwitansi_ina'),
				'no_kwitansi_eng' => $this->input->post('no_kwitansi_eng'),
				'tanggal_ina' => $this->input->post('tanggal_ina'),
				'tanggal_eng' => $this->input->post('tanggal_eng'),
				'sudah_terima_ina' => $this->input->post('sudah_terima_ina'),
				'sudah_terima_eng' => $this->input->post('sudah_terima_eng'),
				'banyaknya_ina' => $this->input->post('banyaknya_ina'),
				'banyaknya_eng' => $this->input->post('banyaknya_eng'),
				'untuk_ina' => $this->input->post('untuk_ina'),
				'untuk_eng' => $this->input->post('untuk_eng'),
				'jumlah_ina' => $this->input->post('jumlah_ina'),
				'jumlah_eng' => $this->input->post('jumlah_eng'),
				'yg_menerima_ina' => $this->input->post('yg_menerima_ina'),
				'yg_menerima_eng' => $this->input->post('yg_menerima_eng'),
				'footer_ina' => $this->input->post('footer_ina'),
				'footer_eng' => $this->input->post('footer_eng'),
				'generated_ina' => $this->input->post('generated_ina'),
				'generated_eng' => $this->input->post('generated_eng'),

			);
			if (isset($_FILES['logo_form'])) {
				// print_r('sini');exit;
				$config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $data['logo_form'] = $image_upload['file_name'];
					
                } 
			}
			$hasil=$this->db->update('setting_deposit_label',$data);
			if ($hasil){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
				redirect('setting_deposit/index/4','location');
			}
	}
	function save_pernyataan(){
		// print_r($this->input->post());exit;
			$data=array(
				'alamat_form' => $this->input->post('alamat_form'),
				'telepone_form' => $this->input->post('telepone_form'),
				'email_form' => $this->input->post('email_form'),
				'judul_ina' => $this->input->post('judul_ina'),
				'judul_eng' => $this->input->post('judul_eng'),
				'header_ina' => $this->input->post('header_ina'),
				'header_eng' => $this->input->post('header_eng'),
				'no_register_ina' => $this->input->post('no_register_ina'),
				'no_register_eng' => $this->input->post('no_register_eng'),
				'no_rekam_medis_ina' => $this->input->post('no_rekam_medis_ina'),
				'no_rekam_medis_eng' => $this->input->post('no_rekam_medis_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'umur_ina' => $this->input->post('umur_ina'),
				'umur_eng' => $this->input->post('umur_eng'),
				'ttl_ina' => $this->input->post('ttl_ina'),
				'ttl_eng' => $this->input->post('ttl_eng'),
				'jk_ina' => $this->input->post('jk_ina'),
				'jk_eng' => $this->input->post('jk_eng'),
				'pekerjaan_ina' => $this->input->post('pekerjaan_ina'),
				'pekerjaan_eng' => $this->input->post('pekerjaan_eng'),
				'pendidikan_ina' => $this->input->post('pendidikan_ina'),
				'pendidikan_eng' => $this->input->post('pendidikan_eng'),
				'agama_ina' => $this->input->post('agama_ina'),
				'agama_eng' => $this->input->post('agama_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'provinsi_ina' => $this->input->post('provinsi_ina'),
				'provinsi_eng' => $this->input->post('provinsi_eng'),
				'kab_ina' => $this->input->post('kab_ina'),
				'kab_eng' => $this->input->post('kab_eng'),
				'kec_ina' => $this->input->post('kec_ina'),
				'kec_eng' => $this->input->post('kec_eng'),
				'kel_ina' => $this->input->post('kel_ina'),
				'kel_eng' => $this->input->post('kel_eng'),
				'kode_pos_ina' => $this->input->post('kode_pos_ina'),
				'kode_pos_eng' => $this->input->post('kode_pos_eng'),
				'rt_ina' => $this->input->post('rt_ina'),
				'rt_eng' => $this->input->post('rt_eng'),
				'rw_ina' => $this->input->post('rw_ina'),
				'rw_eng' => $this->input->post('rw_eng'),
				'wn_ina' => $this->input->post('wn_ina'),
				'wn_eng' => $this->input->post('wn_eng'),
				'telephone_ina' => $this->input->post('telephone_ina'),
				'telephone_eng' => $this->input->post('telephone_eng'),
				'nohp_ina' => $this->input->post('nohp_ina'),
				'nohp_eng' => $this->input->post('nohp_eng'),
				'nik_ina' => $this->input->post('nik_ina'),
				'nik_eng' => $this->input->post('nik_eng'),
				'label_header_pj_ina' => $this->input->post('label_header_pj_ina'),
				'label_header_pj_eng' => $this->input->post('label_header_pj_eng'),
				'nama_pj_ina' => $this->input->post('nama_pj_ina'),
				'nama_pj_eng' => $this->input->post('nama_pj_eng'),
				'ttl_pj_ina' => $this->input->post('ttl_pj_ina'),
				'ttl_pj_eng' => $this->input->post('ttl_pj_eng'),
				'umur_pj_ina' => $this->input->post('umur_pj_ina'),
				'umur_pj_eng' => $this->input->post('umur_pj_eng'),
				'jk_pj_ina' => $this->input->post('jk_pj_ina'),
				'jk_pj_eng' => $this->input->post('jk_pj_eng'),
				'pekerjaan_pj_ina' => $this->input->post('pekerjaan_pj_ina'),
				'pekerjaan_pj_eng' => $this->input->post('pekerjaan_pj_eng'),
				'pendidikan_pj_ina' => $this->input->post('pendidikan_pj_ina'),
				'pendidikan_pj_eng' => $this->input->post('pendidikan_pj_eng'),
				'agama_pj_ina' => $this->input->post('agama_pj_ina'),
				'agama_pj_eng' => $this->input->post('agama_pj_eng'),
				'alamat_pj_ina' => $this->input->post('alamat_pj_ina'),
				'alamat_pj_eng' => $this->input->post('alamat_pj_eng'),
				'provinsi_pj_ina' => $this->input->post('provinsi_pj_ina'),
				'provinsi_pj_eng' => $this->input->post('provinsi_pj_eng'),
				'kab_pj_ina' => $this->input->post('kab_pj_ina'),
				'kab_pj_eng' => $this->input->post('kab_pj_eng'),
				'kec_pj_ina' => $this->input->post('kec_pj_ina'),
				'kec_pj_eng' => $this->input->post('kec_pj_eng'),
				'kel_pj_ina' => $this->input->post('kel_pj_ina'),
				'kel_pj_eng' => $this->input->post('kel_pj_eng'),
				'kode_pos_pj_ina' => $this->input->post('kode_pos_pj_ina'),
				'kode_pos_pj_eng' => $this->input->post('kode_pos_pj_eng'),
				'rt_pj_ina' => $this->input->post('rt_pj_ina'),
				'rt_pj_eng' => $this->input->post('rt_pj_eng'),
				'rw_pj_ina' => $this->input->post('rw_pj_ina'),
				'rw_pj_eng' => $this->input->post('rw_pj_eng'),
				'wn_pj_ina' => $this->input->post('wn_pj_ina'),
				'wn_pj_eng' => $this->input->post('wn_pj_eng'),
				'telephone_pj_ina' => $this->input->post('telephone_pj_ina'),
				'telephone_pj_eng' => $this->input->post('telephone_pj_eng'),
				'nohp_pj_ina' => $this->input->post('nohp_pj_ina'),
				'nohp_pj_eng' => $this->input->post('nohp_pj_eng'),
				'nik_pj_ina' => $this->input->post('nik_pj_ina'),
				'nik_pj_eng' => $this->input->post('nik_pj_eng'),
				'hubungan_pj_ina' => $this->input->post('hubungan_pj_ina'),
				'hubungan_pj_eng' => $this->input->post('hubungan_pj_eng'),
				'detail_pendaftaran_ina' => $this->input->post('detail_pendaftaran_ina'),
				'detail_pendaftaran_eng' => $this->input->post('detail_pendaftaran_eng'),
				'tipe_pasien_ina' => $this->input->post('tipe_pasien_ina'),
				'tipe_pasien_eng' => $this->input->post('tipe_pasien_eng'),
				// 'asal_pasien_ina' => $this->input->post('asal_pasien_ina'),
				// 'asal_pasien_eng' => $this->input->post('asal_pasien_eng'),
				// 'poliklinik_ina' => $this->input->post('poliklinik_ina'),
				// 'poliklinik_eng' => $this->input->post('poliklinik_eng'),
				// 'tgl_pendaftaran_ina' => $this->input->post('tgl_pendaftaran_ina'),
				// 'tgl_pendaftaran_eng' => $this->input->post('tgl_pendaftaran_eng'),
				// 'kasus_ina' => $this->input->post('kasus_ina'),
				// 'kasus_eng' => $this->input->post('kasus_eng'),
				// 'dirawat_ke_ina' => $this->input->post('dirawat_ke_ina'),
				// 'dirawat_ke_eng' => $this->input->post('dirawat_ke_eng'),
				// 'cara_masuk_ina' => $this->input->post('cara_masuk_ina'),
				// 'cara_masuk_eng' => $this->input->post('cara_masuk_eng'),
				'kel_pasien_ina' => $this->input->post('kel_pasien_ina'),
				'kel_pasien_eng' => $this->input->post('kel_pasien_eng'),
				'dpjp_utama_ina' => $this->input->post('dpjp_utama_ina'),
				'dpjp_utama_eng' => $this->input->post('dpjp_utama_eng'),
				'ruangan_ina' => $this->input->post('ruangan_ina'),
				'ruangan_eng' => $this->input->post('ruangan_eng'),
				'kelas_ina' => $this->input->post('kelas_ina'),
				'kelas_eng' => $this->input->post('kelas_eng'),
				'bed_ina' => $this->input->post('bed_ina'),
				'bed_eng' => $this->input->post('bed_eng'),
				// 'pernyataan_persetujuan_ina' => $this->input->post('pernyataan_persetujuan_ina'),
				// 'pernyataan_persetujuan_eng' => $this->input->post('pernyataan_persetujuan_eng'),
				// 'identifikasi_privasi_ina' => $this->input->post('identifikasi_privasi_ina'),
				// 'identifikasi_privasi_eng' => $this->input->post('identifikasi_privasi_eng'),
				// 'khusus_asuransi_ina' => $this->input->post('khusus_asuransi_ina'),
				// 'khusus_asuransi_eng' => $this->input->post('khusus_asuransi_eng'),
				// 'lain_lain_ina' => $this->input->post('lain_lain_ina'),
				// 'lain_lain_eng' => $this->input->post('lain_lain_eng'),
				// 'ttd_pendaftaran_ina' => $this->input->post('ttd_pendaftaran_ina'),
				// 'ttd_pendaftaran_eng' => $this->input->post('ttd_pendaftaran_eng'),
				'ttd_pasien_ina' => $this->input->post('ttd_pasien_ina'),
				'ttd_pasien_eng' => $this->input->post('ttd_pasien_eng'),
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'paragraf_3_ina' => $this->input->post('paragraf_3_ina'),
				'paragraf_3_eng' => $this->input->post('paragraf_3_eng'),

			);
			if (isset($_FILES['logo_form'])) {
				// print_r('sini');exit;
				$config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $data['logo_form'] = $image_upload['file_name'];
					
                } 
			}
			$hasil=$this->db->update('setting_ranap_pernyataan',$data);
			if ($hasil){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
				redirect('setting_deposit/index/5','location');
			}
	}
	
	public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/logo_setting')) {
            mkdir('assets/upload/logo_setting', 0755, true);
        }

        if (isset($_FILES['logo_form'])) {
            if ($_FILES['logo_form']['name'] != '') {
                $config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $this->logo_form = $image_upload['file_name'];

                    return  $this->logo_form;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	
  //TIPE
  
  function simpan_master_deposit(){
		$user_id=$this->session->userdata('user_id');
		$tipe_id = $this->input->post('tipe_id');
		$nama = $this->input->post('nama');
		$jenis_deposit_id = $this->input->post('jenis_deposit_id');
		$data=array(
			'tipe_id' => $tipe_id,
			'nama' => $nama,
			
		);
		
		if ($jenis_deposit_id==''){
			$data['created_by']=$user_id;
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('mdeposit_jenis',$data);
		}else{
			$hasil=$this->db->where('id',$jenis_deposit_id);
			$hasil=$this->db->update('mdeposit_jenis',$data);
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_jenis_deposit()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
						FROM mdeposit_jenis H
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->tipe_id=='1'?('DEPOSIT AWAL'):'NON DEPOSIT AWAL');
          $result[] = ($r->nama);
         
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_tipe('.$r->id.')" type="button" title="Edit Logic" class="btn btn-success btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_tipe('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
    function hapus_tipe(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('mdeposit_jenis');
	  
	  json_encode($hasil);
	  
  }
  function edit_tipe(){
	  $id=$this->input->post('id');
	   
	   $q="SELECT *FROM mdeposit_jenis WHERE id='$id'";
	   $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
  }
  
 function get_edit(){
		$id     = $this->input->post('id');
		$q="SELECT D.*
,CASE WHEN D.tipe_deposit='0' THEN 'DEPOSIT' WHEN D.tipe_deposit='1' THEN 'OBAT' ELSE 'TRANSAKSI' END as tipe_nama
,U.`name` as user_nama FROM `mlogic_deposit` D
				LEFT JOIN musers U ON U.id=D.iduser
				WHERE D.id='$id'";
		$arr['detail'] =$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr['detail']));
	}
	function list_user($step,$tipe_deposit='0')
	{
		 $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mlogic_deposit WHERE mlogic_deposit.step='$step' 
				AND mlogic_deposit.tipe_deposit='$tipe_deposit' 
				AND mlogic_deposit.status='1') AND M.status='1'";
		$arr['detail'] = $this->db->query($q)->result();
		$this->output->set_output(json_encode($arr));
	}
	
	function simpan_detail()
	{
		$id_edit=$this->input->post('id_edit');
		$data=array(
				'iduser'=>$this->input->post('iduser'),
				'step'=>$this->input->post('step'),
				'proses_setuju'=>$this->input->post('proses_setuju'),
				'proses_tolak'=>$this->input->post('proses_tolak'),
				'tipe_deposit'=>$this->input->post('tipe_deposit'),
				'deskrpisi'=>$this->input->post('deskrpisi'),
				
				
			);
		// if ($data['deskrpisi']=='>='){
			// $data['nominal']=0;
		// }else{
			// $data['nominal']=RemoveComma($this->input->post('nominal'));
		// }
		if ($id_edit==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');			
			$result = $this->db->insert('mlogic_deposit',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');		
			$this->db->where('id',$id_edit);
			$result = $this->db->update('mlogic_deposit',$data);
		}
       
		
		$this->output->set_output(json_encode($result));
	}
	function simpan_edit()
	{
		$data=array(
			'deskripsi'=>$this->input->post('deskripsi'),
			'tanggal_hari'=>$this->input->post('tanggal_hari'),
			
		);
		$this->db->where('id',$this->input->post('tedit'));
		
       // print_r($data);
		$result = $this->db->update('mlogic_setting',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	
	function hapus_det()
	{
		$id=$this->input->post('id');
		$this->db->where('id',$this->input->post('id'));	
		$data=array(
			'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
		);
       // print_r($data);
	   
		$result = $this->db->update('mlogic_deposit',$data);
		echo json_encode($result);
	}
	
	
	function load_detail()
    {
		
		$idlogic     = $this->input->post('idlogic');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT D.*,U.`name` as user_nama,
				J.nama as tipe_nama
				FROM `mlogic_deposit` D
				LEFT JOIN mdeposit_jenis J ON J.id=D.tipe_deposit
		LEFT JOIN musers U ON U.id=D.iduser
		WHERE  D.status='1'
		ORDER BY D.tipe_deposit,D.step
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->step;
            
            $row[] = $r->tipe_nama;
            $row[] = $r->deskrpisi;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button  type="button" class="btn btn-xs btn-primary edit" title="Edit"><i class="fa fa-pencil"></i></button>';				
			$aksi 		.= '<button type="button" class="btn btn-xs btn-danger hapus_det" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
            $row[] = $r->user_nama;
			$row[] = ($r->proses_setuju=='1'?'Langsung':'Menunggu User');
            $row[] = ($r->proses_tolak=='1'?'Langsung':'Menunggu User');
			$row[] = $aksi;			
            $row[] = $r->id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	
}
