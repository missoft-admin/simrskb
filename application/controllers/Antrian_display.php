<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_display extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_display_model');
		$this->load->helper('path');
		
  }
	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1537'))){
			
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Display Antrian';
			$data['content'] 		= 'Antrian_display/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Antrian Setting",'#'),
												  array("Index Setting",'antrian_display')
												);
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select *FROM antrian_display H ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_display');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
		  $result[] = $r->nama_display;
		  $result[] = $r->judul_header;
		  $result[] = $r->alamat;
		 $result[] = ($r->telepone);
		  $result[] = ($r->status?text_primary('AKTIF'):text_danger('TIDAK AKTIF'));
		  $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1539'))){
				$aksi .= '<a href="'.site_url().'antrian_display/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1540'))){
				$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
				$aksi .= '<a href="'.site_url().'antrian_tv/display/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Lihat" class="btn btn-info btn-xs"><i class="fa fa-tv"></i></a>';
			}else{
				if (UserAccesForm($user_acces_form,array('1539'))){
				$aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
				}
			}
		  $aksi .= '</div>';
		  $result[] = $aksi;

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function create(){
			$data=array(
				'id' => '',
				'nama_display' => '',
				'bg_color' => '#14adc4',
				'header_logo' => '',
				'judul_header' => '',
				'alamat' => '',
				'telepone' => '',
				'email' => '',
				'website' => '',
				'judul_sub_header' => '',
			);
			$data['list_sound']=array();
			$data['error'] 			= '';
			$data['title'] 			= 'Create Display Antrian';
			$data['content'] 		= 'Antrian_display/manage';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Antrian Setting",'#'),
												  array("Index Display",'antrian_display')
												);
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	function update($id){
			$data=$this->Antrian_display_model->get_index_setting($id);
			$data['list_sound']=$this->Antrian_display_model->list_sound($id);
			$data['list_counter']=$this->Antrian_display_model->list_counter();
			$data['error'] 			= '';
			$data['title'] 			= 'Edit Display Antrian';
			$data['content'] 		= 'Antrian_display/manage';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Antrian Setting",'#'),
												  array("Index Display",'antrian_display')
												);
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	function delete($id){
		
		$result=$this->Antrian_display_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('antrian_layanan','location');
	}
	function aktifkan($id){
		
		$result=$this->Antrian_display_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save_general(){
		if ($this->Antrian_display_model->save_general()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('antrian_display','location');
		}
		
	}
	
	function simpan_running(){
		$display_id=$this->input->post('display_id');
		$idrunning=$this->input->post('idrunning');
		$isi=$this->input->post('isi');
		$nourut=$this->input->post('nourut');
		$data['display_id']=$display_id;
		$data['isi']=$isi;
		$data['nourut']=$nourut;
		$data['nourut']=$nourut;
		if ($idrunning==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('antrian_display_running_text',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idrunning);
			$hasil=$this->db->update('antrian_display_running_text',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function hapus_running(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('antrian_display_running_text',$data);
		json_encode($hasil);
	}
	function load_running_text(){
		$display_id=$this->input->post('display_id');
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.isi FROM antrian_display_running_text H WHERE H.display_id='$display_id' AND H.status='1' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('isi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->isi;

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_running('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	
		$aksi .= '<button onclick="hapus_running('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_running(){
		$id=$this->input->post('id');
		$q="select *FROM antrian_display_running_text H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	
	function save_sound(){
		if($this->Antrian_display_model->save_sound()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','sound telah disimpan.');
			redirect('antrian_display/update/'.$this->input->post('display_id'),'location');
		}
	}
	function hapus_sound($id,$display_id){
		if($this->Antrian_display_model->hapus_sound($id)){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','sound telah disimpan.');
			redirect('antrian_display/update/'.$display_id,'location');
		}
	}
	public function upload_video() {
       $uploadDir = './assets/upload/video';
		if (!empty($_FILES)) {
			 $display_id = $this->input->post('display_id');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['display_id'] 	= $display_id;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('antrian_display_video', $detail);
		}
    }
	function refresh_video($id){		
		$arr['detail'] = $this->Antrian_display_model->refresh_video($id);
		$this->output->set_output(json_encode($arr));
	}
	function hapus_file(){
		$id=$this->input->post('id');
		$row = $this->Antrian_display_model->get_file_name($id);
		if(file_exists('./assets/upload/video/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/video/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from antrian_display_video WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function simpan_counter(){
		$display_id=$this->input->post('display_id');
		$idcounter=$this->input->post('idcounter');
		// print_r($idcounter);exit;
		$counter_id=$this->input->post('counter_id');
		$nourut=$this->input->post('nourut_counter');
		$data['display_id']=$display_id;
		$data['counter_id']=$counter_id;
		$data['nourut']=$nourut;
		if ($idcounter==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('antrian_display_counter',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idcounter);
			$hasil=$this->db->update('antrian_display_counter',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function hapus_counter(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('antrian_display_counter',$data);
		json_encode($hasil);
	}
	function load_counter(){
		$display_id=$this->input->post('display_id');
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.counter_id,M.nama_counter 
		FROM antrian_display_counter H 
		LEFT JOIN antrian_pelayanan_counter M ON M.id=H.counter_id
		WHERE H.display_id='$display_id' AND H.status='1' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('counter_id');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->nama_counter;

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_counter('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	
		$aksi .= '<button onclick="hapus_counter('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_counter(){
		$id=$this->input->post('id');
		$q="select *FROM antrian_display_counter H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
}
