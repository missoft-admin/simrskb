<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtriage extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtriage_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1679'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Master Triage';
			$data['content'] 		= 'Mtriage/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Master Triage",'#'),
												  array("List",'mtriage')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'nama_lain' 					=> '',
			'warna' 					=> '#000',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Mtriage_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		// $data['list_user'] 			= $this->Mtriage_model->list_user('');
		$data['title'] 			= 'Tambah Master Triage';
		$data['content'] 		= 'Mtriage/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Master Triage",'#'),
								            array("Tambah",'mtriage')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mtriage_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Master Triage';
			$data['content']    = 'Mtriage/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Master Triage",'#'),
										array("Ubah",'mtriage')
										);

			// $data['statusAvailableApoteker'] = $this->Mtriage_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtriage');
		}
	}
	function setting($id){
		
		if($id != ''){
			$data= $this->Mtriage_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Master Triage';
			$data['content']    = 'Mtriage/manage_setting';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Master Triage",'#'),
										array("Ubah",'mtriage')
										);

			// $data['statusAvailableApoteker'] = $this->Mtriage_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtriage');
		}
	}

	function delete($id){
		
		$result=$this->Mtriage_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mtriage','location');
	}
	function pilih($id){
		
		$result=$this->Mtriage_model->pilih($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mtriage','location');
	}
	function aktifkan($id){
		
		$result=$this->Mtriage_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if ($this->input->post('btn_simpan')=='2'){
			$q="UPDATE mtriage SET staktif='0' WHERE staktif='1'";
			$this->db->query($q);
		}
		if($this->input->post('id') == '' ) {
			if($this->Mtriage_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mtriage/create','location');
			}
		} else {
			if($this->Mtriage_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mtriage/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtriage/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Triage';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Master Triage",'#'),
							               array("Tambah",'mtriage')
								           );
		}else{
			$data['title'] = 'Ubah Master Triage';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Master Triage",'#'),
							               array("Ubah",'mtriage')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						FROM mtriage H 
						
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama;
          $result[] = $r->nama_lain;
           $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'; color:black" type="button" >'.$r->warna.'</button>';
          $result[] = ($r->staktif=='1'?text_primary('AKTIF'):text_default('TIDAK AKTIF'));
         
          $aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('1706'))){
			$aksi .= '<a href="'.site_url().'mtriage/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
			}
			if ($r->staktif=='0'){
				
				if (UserAccesForm($user_acces_form,array('1708'))){
					$aksi .= '<button title="Aktifkan" type="button" onclick="aktifkan('.$r->id.')" class="btn btn-warning btn-xs "><i class="si si-check"></i></button>';
				}
			}
			if ($r->staktif=='1'){
				if (UserAccesForm($user_acces_form,array('1709'))){
					$aksi .= '<a href="'.site_url().'mtriage/setting/'.$r->id.'" data-toggle="tooltip" title="Ubah Setting" class="btn btn-success btn-xs"><i class="si si-settings"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1707'))){
					$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}
		
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function simpan_nilai(){
		$setting_id=$this->input->post('setting_id');
		$mtriage_pemeriksaan_id=$this->input->post('mtriage_pemeriksaan_id');
		$nilai=$this->input->post('nilai');
		$penilaian=$this->input->post('penilaian');
		$mtriage_id=$this->input->post('mtriage_id');
		$res=$this->cek_duplicate($mtriage_id,$mtriage_pemeriksaan_id,$nilai,$setting_id);
		if ($res==true){
			$data=array(
				'mtriage_pemeriksaan_id'=>$this->input->post('mtriage_pemeriksaan_id'),
				'nilai'=>$this->input->post('nilai'),
				'penilaian'=>$this->input->post('penilaian'),
				'mtriage_id'=>$this->input->post('mtriage_id'),
				'staktif'=>1,
			);
			if ($setting_id==''){
				$data['created_by']=$this->session->userdata('user_id');
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('mtriage_setting',$data);
			}else{
				$data['edited_by']=$this->session->userdata('user_id');
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$setting_id);
				$hasil=$this->db->update('mtriage_setting',$data);
			}
		}else{
			$hasil=false;
		}
		  $this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate($mtriage_id,$mtriage_pemeriksaan_id,$nilai,$setting_id){
		$q="SELECT H.id FROM mtriage_setting H WHERE H.mtriage_id='$mtriage_id' AND H.mtriage_pemeriksaan_id='$mtriage_pemeriksaan_id' AND H.nilai='$nilai' AND H.id != '$setting_id'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			return false;
		}else{
			return true;
		}
	}
	function load_nilai()
	{
			$mtriage_id=$this->input->post('mtriage_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						,M.nama nama_pemeriksaan
						FROM `mtriage_setting` H
						LEFT JOIN mtriage_pemeriksaan M ON M.id=H.mtriage_pemeriksaan_id
						where H.staktif='1' AND H.mtriage_id='$mtriage_id'
						GROUP BY H.id
						ORDER BY H.mtriage_pemeriksaan_id,H.nilai


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('penilaian','nama_pemeriksaan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->nama_pemeriksaan;
          $result[] = $r->nilai;
          $result[] = $r->penilaian;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	  function find_nilai(){
		  $setting_id=$this->input->post('setting_id');
		  $q="SELECT *FROM mtriage_setting H WHERE H.id='$setting_id'";
		  $hasil=$this->db->query($q)->row_array();
		  // print_r($hasil);exit;
		  $this->output->set_output(json_encode($hasil));
	  }
	  function hapus_nilai(){
		  $parameter_id=$this->input->post('id');
		  $data=array(
				'staktif'=>0,
			);
			
			$data['deleted_by']=$this->session->userdata('user_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
			$hasil=$this->db->update('mtriage_setting',$data);
		  
		  json_encode($hasil);
		  
	  }
}
