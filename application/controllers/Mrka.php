<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Create RKA';
		$data['content'] 		= 'Mrka/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("List",'mrka')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create($disabel=''){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'periode' 					=> date('Y'),
			'status' 				=> ''
		);

		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Create RKA';
		$data['content'] 		= 'Mrka/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("Tambah",'mrka')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id,$disabel=''){
		if($id != ''){
			$row = $this->Mrka_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'periode' 					=> $row->periode,
					'status' 				=> $row->status
				);
				$data['disabel'] 			= $disabel;
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Create RKA';
				$data['content']	 	= 'Mrka/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Create RKA",'#'),
											    			array("Ubah",'mrka')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mrka','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mrka');
		}
	}

	function delete($id){
		$this->Mrka_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mrka','location');
	}
	function update_status($id,$status){
		$result=$this->Mrka_model->update_status($id,$status);
		echo json_encode($result);
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mrka_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrka/update/'.$id,'location');
				}
			} else {
				if($this->Mrka_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrka/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mrka/manage';

		if($id==''){
			$data['title'] = 'Tambah Create RKA';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
															array("Tambah",'mrka')
													);
		}else{
			$data['title'] = 'Ubah Create RKA';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
															array("Ubah",'mrka')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $periode=$this->input->post('periode');
	  $where='';
	  if ($periode !='#'){
		  $where .=" AND M.periode='$periode'";
	  }
		
	  $from="(SELECT * from mrka M
				WHERE M.id is not null ".$where."
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					if (UserAccesForm($user_acces_form,array('1378'))){
						$aksi .= '<a href="'.site_url().'mrka_kegiatan/add/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
					}
					if ($r->status !='0'){
						if (UserAccesForm($user_acces_form,array('1379'))){
						$aksi .= '<a href="'.site_url().'mrka/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						}
						
						if (UserAccesForm($user_acces_form,array('1425'))){
						if ($r->status=='1'){//Publis
							$aksi .= '<button title="Publish" class="btn btn-success btn-sm publish"><i class="si si-arrow-right"></i></button>';
						}
						}
						if (UserAccesForm($user_acces_form,array('1380'))){
						if ($r->status=='2'){//Publish
							$aksi .= '<button title="Actived" class="btn btn-primary btn-sm aktif"><i class="si si-control-play"></i></button>';
						}
						}
						if ($r->status=='3'){//Active
							if (UserAccesForm($user_acces_form,array('1381'))){
							$aksi .= '<button title="Hasil " class="btn btn-danger btn-sm hasil"><i class="fa fa-arrow-down"></i></button>';
							}
							if (UserAccesForm($user_acces_form,array('1426'))){
							$aksi .= '<button title="Selesai" class="btn btn-warning btn-sm selesai"><i class="si si-check"></i></button>';
							}
						}
						if ($r->status=='4'){//Selesai
							if (UserAccesForm($user_acces_form,array('1381'))){
							$aksi .= '<button title="Hasil " class="btn btn-danger btn-sm hasil"><i class="fa fa-arrow-down"></i></button>';
							}
						}
					
							if (UserAccesForm($user_acces_form,array('1382'))){
						$aksi .= '<a href="#" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
							}
					}
					if ($r->status!='4' && $r->status!='0'){//Draft
							if (UserAccesForm($user_acces_form,array('1383'))){
					// $aksi .= '<a href="#" data-urlindex="'.site_url().'mrka" data-urlremove="'.site_url().'mrka/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					$aksi .= '<button title="Hapus" class="btn btn-danger btn-sm hapus"><i class="fa fa-trash-o"></i></button>';
					}
					}
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->periode;
          $row[] = status_rka($r->status);
          $row[] = $r->created_nama.' '.HumanDateLong($r->created_date);
          $row[] = $aksi;
          $row[] = $r->id;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_program()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $idrka=$this->input->post('idrka');
	  $idprespektif=$this->input->post('idprespektif');
	  $disabel=$this->input->post('disabel');
	  $where='';
	  if ($idprespektif !='#'){
		  $where .=" AND D.idprespektif='$idprespektif'";
	  }
		
	  $from="(
				SELECT D.idprespektif,D.id,H.nama as prespektif, D.nama as program,M.`status`,M.idprogram,M.idprespektif as idprespektif_trx FROM mprogram_rka D
				LEFT JOIN mprespektif_rka H ON H.id=D.idprespektif
				LEFT JOIN mrka_program M ON M.idrka='$idrka' AND M.idprogram=D.id
				WHERE D.`status`='1' ".$where."
				ORDER BY H.urutan ASC, D.urutan ASC
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('program');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $nama_presprektif='';
      foreach ($list as $r) {
          $no++;
		  if ($nama_presprektif !=$r->idprespektif){
			  $row = array();
			  $row[] = $r->idprespektif;
			  $row[] = $r->status;
			  $check='';
			  if ($r->status=='1'){
				  $check="checked";			  
			  }
			  $row[] = '<label class="css-input css-checkbox css-checkbox-success remove-margin-t remove-margin-b">
							<input type="checkbox" '.$check.'  name="check-all[]" class="chek1" '.$disabel.'><span></span>
						</label>';
			  $row[] = '<h3><span class="label label-success">'.$r->prespektif.'</span></h3>';
			  $nama_presprektif=$r->idprespektif;
			  $data[] = $row;
		  }else{
			  
		  }
		  
		  $row = array();
		  $row[] = $r->idprespektif;
		  $row[] = $r->id;
		  if ($r->status=='1'){
			   $row[] ='<label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
							<input type="checkbox" checked name="check-all2[]" class="chek2" '.$disabel.'><span></span>
						</label>';
		  }else{
			  $row[] ='<label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
							<input type="checkbox" name="check-all2[]" class="chek2" '.$disabel.'><span></span>
						</label>';
		  }
		 
		  $row[] = $r->program;
		  
		  $data[] = $row;
		  if ($disabel ==''){
			   if ($r->idprogram==''){
				  $data_det=array(
					'idrka'=>$idrka,
					'idprespektif'=>$r->idprespektif,
					'idprogram'=>$r->id,
					'status'=>0,
				  );
				  $this->db->insert('mrka_program',$data_det);  
				  
			  }
			  if ($r->idprespektif != $r->idprespektif_trx){
				  $this->db->query("UPDATE mrka_program SET idprespektif='".$r->idprespektif."' WHERE idprogram='".$r->id."' AND idrka='".$idrka."'");
			  }
		  }
		  // print_r($row);exit();
          
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function update_status_detail(){
		$idrka=$this->input->post('idrka');
		$idprespektif=$this->input->post('idprespektif');
		$status=$this->input->post('status');
		
	    $this->db->where('idprespektif',$idprespektif);
	    $this->db->where('idrka',$idrka);
		$result = $this->db->update('mrka_program',array('status'=>$status));
		echo json_encode($result);
	}
	function update_status_all(){
		$idrka=$this->input->post('idrka');
		$status=$this->input->post('status');
		
	    $this->db->where('idrka',$idrka);
		$result = $this->db->update('mrka_program',array('status'=>$status));
		echo json_encode($result);
	}
	function update_status_program(){
		$idrka=$this->input->post('idrka');
		$idprogram=$this->input->post('idprogram');
		$status=$this->input->post('status');
		
	    $this->db->where('idprogram',$idprogram);
	    $this->db->where('idrka',$idrka);
		$result = $this->db->update('mrka_program',array('status'=>$status));
		echo json_encode($result);
	}
}
