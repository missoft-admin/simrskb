<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Tindakan_rj extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Tindakan_rj_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tcheckin_model');
		$this->load->helper('path');
		
  }
  function dokter($pendaftaran_id=''){
	  $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data=array();
		$q="SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan
						,H.* FROM tpoliklinik_pendaftaran H
						INNER JOIN mdokter MD ON MD.id=H.iddokter
						INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
						INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						WHERE H.id='$pendaftaran_id'";
		$data=$this->db->query($q)->row_array();
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['error'] 			= '';
		$data['title'] 			= 'My Pasien';
		$data['content'] 		= 'Tindakan_rj/index';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan Poliklinik",'#'),
											  array("My Pasien",'tpendaftaran_poli_pasien')
											);

		// print_r($data);exit;
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
  }
  
	function index($tab='1'){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='1' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('1597'))){
			$data = $this->db->query($q)->row_array();	
			// print_r($data);exit;	
			// $data['list_ruangan'] 			= $this->Tindakan_rj_model->list_ruangan();
			$data['list_poli'] 			= $this->Tindakan_rj_model->list_poli($data['login_mppa_id']);
			$data['list_dokter'] 			= $this->Tindakan_rj_model->list_dokter();
			
			$data['idpoli'] 			= '#';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			
			$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
			$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'My Pasien';
			$data['content'] 		= 'Tindakan_rj/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("My Pasien",'tpendaftaran_poli_pasien')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function menu_pelayanan($pendaftaran_id=''){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data=array();
		$q="SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan
						,H.* FROM tpoliklinik_pendaftaran H
						INNER JOIN mdokter MD ON MD.id=H.iddokter
						INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
						INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						WHERE H.id='$pendaftaran_id'";
		$data=$this->db->query($q)->row_array();
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['error'] 			= '';
		$data['title'] 			= 'My Pasien';
		$data['content'] 		= 'Tindakan_rj/home';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan Poliklinik",'#'),
											  array("My Pasien",'tpendaftaran_poli_pasien')
											);

		// print_r($data);exit;
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_layanan', $data);
		
	}
	
	function getIndex_all()
	{
			
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
			}
			if ($tab=='2'){
				$where .=" AND (H.status_input_dokter) = '0'";
			}
			if ($tab=='3'){
				$where .=" AND (H.status_input_dokter) = '1'";
			}
			if ($idpoli!='#'){
				$where .=" AND (H.idpoliklinik) = '$idpoli'";
			}
		
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan
						,H.* FROM tpoliklinik_pendaftaran H
						INNER JOIN mdokter MD ON MD.id=H.iddokter
						INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
						INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						WHERE H.tanggal IS NOT NULL AND H.idtipe='1' AND H.iddokter='$iddokter' ".$where."

						
						ORDER BY H.tanggal DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"><a href="'.base_url().'tpendaftaran_poli_pasien/menu_pelayanan/'.$r->id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs"><i class="fa fa-user-md"></i> LAYANNI</a> </div>
									<div class="push-5-t">
										<div class="btn-group btn-block">
											<div class="btn-group">
												<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
													<span class="fa fa-caret-down"></span> PANEL KENDALI
												</button>
												<ul class="dropdown-menu">
													<li class="dropdown-header">ACTION</li>
													<li>
														<a tabindex="-1" href="javascript:void(0)"> OPTION 1</a>
													</li>
													<li>
														<a tabindex="-1" href="javascript:void(0)"> OPTION 2</a>
													</li>
													<li class="divider"></li>
													<li class="dropdown-header">More</li>
													<li>
														<a tabindex="-1" href="javascript:void(0)"> Lihat Profile..</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Call"  class="btn btn-block btn-success btn-xs"><i class="fa fa-save"></i> FINAL TRANSAKSI</button> </div>
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h4 font-w700 text-primary"> '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button"> Tidak Ada ALergi</button>
										<button class="btn btn-danger  btn-sm" type="button"><i class="fa fa-info"></i></button>
									</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
          $result[] =($r->status_input_dokter?text_succes('TELAH DIPERIKSA'):text_warning('BELUM DIPERIKSA'));
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
 
}
