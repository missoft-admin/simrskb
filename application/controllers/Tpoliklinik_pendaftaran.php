<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;
use Dompdf\Options;

class Tpoliklinik_pendaftaran extends CI_Controller
{
	/**
	 * Pendaftaran Poliklinik & IGD controller.
	 * Developer @RendyIchtiarSaputra + @GunaliRezqiMauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Mrumahsakit_model');
	}

	public function index()
	{
		$data = [
			'idstatus' => '#',
			'nomedrec' => '',
			'namapasien' => '',
			'idkelompokpasien' => '',
			'iddokter' => '',
			// 'idtipe' => '#',
			'idpoliklinik' => '',
			'idalasan' => '7',
			'tanggaldaftar' => date('d/m/Y'),
			'tanggaldaftar2' => date('d/m/Y'),
		];
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['1331'])) {
			$data['idtipe'] = '1';
		}
		if (UserAccesForm($user_acces_form, ['321'])) {
			$data['idtipe'] = '2';
		}
		if (UserAccesForm($user_acces_form, ['322'])) {
			$data['idtipe'] = '#';
		}
		$this->session->set_userdata($data);
		$data['error'] = '';
		$data['title'] = 'Pendaftaran Poliklinik & IGD';
		$data['content'] = 'Tpoliklinik_pendaftaran/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Pendaftaran Poliklinik & IGD', '#'],
			['List', 'tpoliklinik_pendaftaran']
		];

		$data['list_kelompok_pasien'] = $this->Tpoliklinik_pendaftaran_model->getKelompokPasien();
		$data['list_dokter'] = $this->Tpoliklinik_pendaftaran_model->getDokter();
		$data['list_alasan'] = $this->Tpoliklinik_pendaftaran_model->getAlasan();
		$data['list_poli'] = $this->Tpoliklinik_pendaftaran_model->getPoliklinik();
		// print_r($data['list_alasan']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'idstatus' => $this->input->post('idstatus'),
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idkelompokpasien' => $this->input->post('idkelompokpasien'),
			'iddokter' => $this->input->post('iddokter'),
			'idtipe' => $this->input->post('idtipe'),
			'idpoliklinik' => $this->input->post('idpoliklinik'),
			'tanggaldaftar' => $this->input->post('tanggaldaftar'),
			'tanggaldaftar2' => $this->input->post('tanggaldaftar2'),
		];

		$this->session->set_userdata($data);

		$data['idalasan'] = '7';
		$data['error'] = '';
		$data['title'] = 'Pendaftaran Poliklinik & IGD';
		$data['content'] = 'Tpoliklinik_pendaftaran/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Pendaftaran Poliklinik & IGD', '#'],
			['List', 'tpoliklinik_pendaftaran']
		];
		$data['list_poli'] = $this->Tpoliklinik_pendaftaran_model->getPoliklinik();
		$data['list_kelompok_pasien'] = $this->Tpoliklinik_pendaftaran_model->getKelompokPasien();
		$data['list_dokter'] = $this->Tpoliklinik_pendaftaran_model->getDokter();
		$data['list_alasan'] = $this->Tpoliklinik_pendaftaran_model->getAlasan();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create()
	{
		// $this->Tpoliklinik_pendaftaran_model->get_duplicate_hariini('180388','2','119');
		$kelompokpasien = $this->Tpoliklinik_pendaftaran_model->getLastKelompokPasien();
		$data = [
			'id' => '',
			'title' => '',
			'nama' => '',
			'created' => '',
			'jenis_kelamin' => '',
			'alamat_jalan' => '',
			'provinsi_id' => '',
			'kabupaten_id' => '',
			'kecamatan_id' => '',
			'kelurahan_id' => '',
			'kodepos' => '',
			'ktp' => '',
			'jenis_id' => '',
			'hp' => '',
			'telepon' => '',
			'email' => '',
			'tempat_lahir' => '',
			'tanggal_lahir' => '',
			'umur_tahun' => '',
			'umur_bulan' => '',
			'umur_hari' => '',
			'golongan_darah' => '',
			'agama_id' => '',
			'warganegara' => '',
			'suku' => '',
			'status_kawin' => '',
			'pendidikan_id' => '',
			'pekerjaan_id' => '',
			'hubungan_dengan_pasien' => '',
			'alamat_keluarga' => '',
			'nama_keluarga' => '',
			'telepon_keluarga' => '',
			'ktp_keluarga' => '',
			'idtipe' => '',
			'tanggaldaftar' => date('d/m/Y'),
			'waktudaftar' => date('H:i:s'),
			'idpasien' => '',
			'no_medrec' => '',
			'idasalpasien' => '1',
			'idtipepasien' => '1',
			'idkelompokpasien' => '',
			'idkelompokpasien2' => '',
			'idjenispasien' => '',
			'idjenispertemuan' => '2',
			'idpoliklinik' => '1',
			'iddokter' => '',
			'catatan' => '',
			'idrujukan' => '',
			'idrekanan' => '',
			'idrekanan2' => '',
			'idtarifbpjskesehatan' => '',
			'idtarifbpjskesehatan2' => '',
			'idtarifbpjstenagakerja' => '',
			'idtarifbpjstenagakerja2' => '',
			'namapenanggungjawab' => '',
			'teleponpenanggungjawab' => '',
			'noidentitaspenanggungjawab' => '',
			'tgl_hari' => '',
			'tgl_bulan' => '',
			'tgl_tahun' => '',
			'status' => '',
			'kelompokpasien' => 5,
		];
		// print_r($data);exit();
		$data['error'] = '';
		$data['title_header'] = 'Pendaftaran Poliklinik & IGD';
		$data['content'] = 'Tpoliklinik_pendaftaran/manage';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Pendaftaran Poliklinik & IGD', '#'],
			['Tambah', 'tpoliklinik_pendaftaran']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function update($id)
	{
		if ($id != '') {
			$row = $this->Tpoliklinik_pendaftaran_model->getSpecified($id);
			$pasien = get_by_field('id', $row->idpasien, 'mfpasien');
			if (isset($row->id)) {
				$data = [
					'no_medrec' => $pasien->no_medrec,
					'title' => $pasien->title,
					'nama' => $pasien->nama,
					'jenis_kelamin' => $pasien->jenis_kelamin,
					'alamat_jalan' => $pasien->alamat_jalan,
					'provinsi_id' => $pasien->provinsi_id,
					'kabupaten_id' => $pasien->kabupaten_id,
					'kecamatan_id' => $pasien->kecamatan_id,
					'kelurahan_id' => $pasien->kelurahan_id,
					'kodepos' => $pasien->kodepos,
					'ktp' => $pasien->ktp,
					'jenis_id' => $pasien->jenis_id,
					'hp' => $pasien->hp,
					'telepon' => $pasien->telepon,
					'email' => $pasien->email,
					'tempat_lahir' => $pasien->tempat_lahir,
					'tanggal_lahir' => $pasien->tanggal_lahir,
					'umur_tahun' => $pasien->umur_tahun,
					'umur_bulan' => $pasien->umur_bulan,
					'umur_hari' => $pasien->umur_hari,
					'golongan_darah' => $pasien->golongan_darah,
					'agama_id' => $pasien->agama_id,
					'warganegara' => $pasien->warganegara,
					'suku' => $pasien->suku,
					'status_kawin' => $pasien->status_kawin,
					'pendidikan_id' => $pasien->pendidikan_id,
					'pekerjaan_id' => $pasien->pekerjaan_id,
					'hubungan_dengan_pasien' => $pasien->hubungan_dengan_pasien,
					'alamat_keluarga' => $pasien->alamat_keluarga,
					'nama_keluarga' => $pasien->nama_keluarga,
					'telepon_keluarga' => $pasien->telepon_keluarga,
					'ktp_keluarga' => $pasien->ktp_keluarga,

					'id' => $row->id,
					'idtipe' => $row->idtipe,
					'tanggaldaftar' => DMYFormat($row->tanggaldaftar),
					'waktudaftar' => HISTimeFormat($row->tanggaldaftar),
					'idpasien' => $row->idpasien,
					'idasalpasien' => $row->idasalpasien,
					'idtipepasien' => $row->idtipepasien,
					'idkelompokpasien' => $row->idkelompokpasien,
					'idkelompokpasien2' => $row->idkelompokpasien2,
					'idjenispasien' => $row->idjenispasien,
					'idjenispertemuan' => $row->idjenispertemuan,
					'idpoliklinik' => $row->idpoliklinik,
					'iddokter' => $row->iddokter,
					'catatan' => $row->catatan,
					'idrujukan' => $row->idrujukan,
					'idrekanan' => $row->idrekanan,
					'idrekanan2' => $row->idrekanan2,
					'idtarifbpjskesehatan' => $row->idtarifbpjskesehatan,
					'idtarifbpjskesehatan2' => $row->idtarifbpjskesehatan2,
					'idtarifbpjstenagakerja' => $row->idtarifbpjstenagakerja,
					'idtarifbpjstenagakerja2' => $row->idtarifbpjstenagakerja2,
					'namapenanggungjawab' => $row->namapenanggungjawab,
					'teleponpenanggungjawab' => $row->teleponpenanggungjawab,
					'noidentitaspenanggungjawab' => $row->noidentitaspenanggungjawab,
					'status' => $row->status,
				];
				// print_r($data);exit();
				$data['error'] = '';
				$data['title_header'] = 'Pendaftaran Poliklinik & IGD';
				$data['content'] = 'Tpoliklinik_pendaftaran/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Pendaftaran Poliklinik & IGD', '#'],
					['Ubah', 'tpoliklinik_pendaftaran']
				];

				$string = $pasien->tanggal_lahir;
				$timestamp = strtotime($string);
				$data['tgl_hari'] = date('d', $timestamp);
				$data['tgl_bulan'] = date('m', $timestamp);
				$data['tgl_tahun'] = date('Y', $timestamp);

				$data['historykunjungan'] = $this->Tpoliklinik_pendaftaran_model->getHistoryKunjungan($row->idpasien);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('tpoliklinik_pendaftaran/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('tpoliklinik_pendaftaran/index');
		}
	}

	public function save()
	{
		$id = $this->input->post('id');

		if ($id == '') {
			if ($id = $this->Tpoliklinik_pendaftaran_model->saveData()) {
				$data = [];
				$data['idpendaftaran'] = $id;
				$data = array_merge($data, backend_info());
				$this->parser->parse('Tpoliklinik_pendaftaran/redirect/save_pendaftaran', $data);
			}
		} else {
			if ($this->Tpoliklinik_pendaftaran_model->updateData($id)) {
				$data = [];
				$data['idpendaftaran'] = $id;
				$data = array_merge($data, backend_info());
				$this->parser->parse('Tpoliklinik_pendaftaran/redirect/save_pendaftaran', $data);
			}
		}
	}

	public function after_save()
	{
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah disimpan.');
		redirect('tpoliklinik_pendaftaran/index', 'location');
	}

	public function delete()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$idalasan = $this->input->post('idalasan');
		$data = [];
		$data['idalasan'] = $idalasan;
		$data['status'] = 0;
		$data['deleted_by'] = $this->session->userdata('user_id');
		$data['deleted_date'] = date('Y-m-d H:i:s');
		$this->db->where('id', $idpendaftaran);
		if ($this->db->update('tpoliklinik_pendaftaran', $data)) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function batal()
	{
		print_r('Batal');
		exit();
		$idpendaftaran = $this->input->post('idpendaftaran');
		$keterangan = $this->input->post('keterangan');

		$data = [];
		$data['status'] = 0;
		$this->db->where('tanggaldaftar', $idpendaftaran);
		if ($this->db->update('tpoliklinik_pendaftaran', $data)) {
			$det = [];
			$det['keterangan'] = $keterangan;
			$det['status'] = 0;
			if ($this->db->insert('malasan_batal', $det)) {
				return true;
			}
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function print_document($idpendaftaran, $idtipe)
	{
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$row = $this->Tpoliklinik_pendaftaran_model->getPrintInfo($idpendaftaran);
		// print_r($row);exit();
		$data = [
			'print' => $idtipe,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'jk' => $row->jeniskelamin,
			'jeniskelamin' => ($row->jeniskelamin == 1 ? 'Laki-laki' : 'Perempuan'),
			'tipe_nama' => ($row->idtipe == 1 ? 'Rawat Jalan' : 'IGD'),
			'jeniskelamin_panjang' => ($row->jeniskelamin == 1 ? 'Laki-laki' : 'Perempuan'),
			'alamat' => $row->alamat . ', DS/KEL. ' . $row->desa . ' KEC. ' . $row->kec . ' ' . $row->kab,
			'tanggal' => $row->tanggal,
			'tanggaldaftar' => $row->tanggaldaftar,
			'tanggallahir' => $row->tanggallahir,
			'umurtahun' => $row->umurtahun,
			'umurbulan' => $row->umurbulan,
			'umurhari' => $row->umurhari,
			'notelepon' => $row->telepon,
			'idpekerjaan' => $row->pekerjaan_id,
			'pekerjaan' => $row->namapekerjaan,
			'agama' => $row->namaagama,
			'golongandarah' => $row->golongandarah,
			'statuskawin' => $row->statuskawin,
			'asalpasien' => $row->asalpasien,
			'namakelompok' => $row->namakelompok,
			'namarekanan' => $row->namarekanan,
			'namapoliklinik' => $row->namapoliklinik,
			'namadokter' => $row->namadokter,
			'noantrian' => $row->noantrian,
			'namauserinput' => $row->namauserinput,
			'nopendaftaran' => $row->nopendaftaran,
			'namapenanggungjawab' => $row->namapenanggungjawab,
			'kab' => $row->kab,
			'kec' => $row->kec,
			'desa' => $row->desa,
			'idpasien' => $row->idpasien,
			'title' => $row->title,
			'kode_antrian' => $row->kode_antrian,
			'base_url' => site_url(),
		];

		// print_r($data);exit();
		$nama_file='';
		if ($idtipe == 1) {
			$nama_file='Bukti Pendaftaran ';
			$html = $this->load->view('Tpoliklinik_pendaftaran/print/print_pendaftaran', $data, true);
		} elseif ($idtipe == 2) {
			$nama_file='Tracer ';
			$html = $this->load->view('Tpoliklinik_pendaftaran/print/print_tracer', $data, true);
		} elseif ($idtipe == 3) {
			$nama_file='Kartu Status ';
			$data['list_riwayat'] = $this->Tpoliklinik_pendaftaran_model->list_riwayat($data['idpasien']);
			$html = $this->load->view('Tpoliklinik_pendaftaran/print/print_status', $data, true);
		// print_r($data['list_riwayat']);exit();
		} elseif ($idtipe == 4) {
			$nama_file='KIB ';
			$html = $this->load->view('Tpoliklinik_pendaftaran/print/print_kib', $data, true);
		} elseif ($idtipe == 5) {
			$nama_file='Sticker ID ';
			$html = $this->load->view('Tpoliklinik_pendaftaran/print/print_sticker', $data, true);
		} elseif ($idtipe == 6) {
			$nama_file='Surat Keterangan Diagnosa ';
			$html = $this->load->view('Tpoliklinik_pendaftaran/print/sk_diagnosa', $data, true);
		} elseif ($idtipe == 7) {
			$nama_file='Bukti Pendaftaran ';
			$html = $this->load->view('Tpoliklinik_pendaftaran/print/print_pendaftaran_dot', $data, true);
		}

		// print_r($html);exit();
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		if ($idtipe == 1) {
			$customPaper = [0, 0, 226, 4000];
			$dompdf->set_paper($customPaper);
		} elseif ($idtipe == 2) {
			$customPaper = [0, 0, 226, 4000];
			$dompdf->set_paper($customPaper);
		} elseif ($idtipe == 3) {
			$dompdf->setPaper('A4', 'portrait');
		} elseif ($idtipe == 4) {
			$customPaper = [0, 0, 226, 150];
			$dompdf->set_paper($customPaper);
		} elseif ($idtipe == 5) {
			$dompdf->setPaper('A4', 'portrait');
		}elseif ($idtipe == 7) {
			$customPaper = [0, 0, 226, 4000];
		}

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream($nama_file.'.pdf', ['Attachment' => 0]);
	}

	// Get Alamat (Kabupaten, Kecamatan, Kelurahan, KodePos)
	public function getKabupaten()
	{
		$kode = $this->input->post('kodeprovinsi');
		$data = get_all('mfwilayah', ['parent_id' => $kode]);
		$this->output->set_output(json_encode($data));
	}

	public function getKecamatan()
	{
		$kode = $this->input->post('kodekab');
		$data = get_all('mfwilayah', ['parent_id' => $kode]);
		$this->output->set_output(json_encode($data));
	}

	public function getKelurahan()
	{
		$kode = $this->input->post('kodekec');
		$data = get_all('mfwilayah', ['parent_id' => $kode]);
		$this->output->set_output(json_encode($data));
	}

	public function getKodepos()
	{
		$kode = $this->input->post('kodekel');
		$data = get_all('mfwilayah', ['id' => $kode]);
		$this->output->set_output(json_encode($data));
	}

	// Get Rujukan
	public function getRujukan()
	{
		$idasalpasien = $this->input->post('asal');
		if ($idasalpasien == 2) {
			$idtipe = 1;
		} else {
			$idtipe = 2;
		}

		$data = get_all('mrumahsakit', ['idtipe' => $idtipe]);
		$this->output->set_output(json_encode($data));
	}

	// Get Perhitungan Umur
	public function getDate()
	{
		$tanggal = $this->input->post('tanggal');
		$data = '{"date":[';
		$r = $this->db->query("SELECT TIMESTAMPDIFF(YEAR,'" . $tanggal . "',NOW()) tahun,
							   TIMESTAMPDIFF(MONTH,DATE_ADD('" . $tanggal . "',INTERVAL TIMESTAMPDIFF(YEAR,'" . $tanggal . "',NOW()) YEAR),NOW()) bulan,
							   TIMESTAMPDIFF(DAY,DATE_ADD(DATE_ADD('" . $tanggal . "',INTERVAL TIMESTAMPDIFF(YEAR,'" . $tanggal . "',NOW()) YEAR),INTERVAL TIMESTAMPDIFF(MONTH,DATE_ADD('" . $tanggal . "',INTERVAL TIMESTAMPDIFF(YEAR,'" . $tanggal . "',NOW()) YEAR),NOW()) MONTH),NOW()) hari
							   FROM mrumahsakit LIMIT 1")->row();
		$data .= '{"tahun":"' . $r->tahun . '","bulan":"' . $r->bulan . '","hari":"' . $r->hari . '"},';
		$data = substr($data, 0, strlen($data) - 1);
		$data .= ']}';
		echo $data;
	}

	public function getRekanan()
	{
		$data = get_all('mrekanan', ['status' => 1]);
		$this->output->set_output(json_encode($data));
	}

	public function getDokter($idpoliklinik)
	{
		$this->db->select('mdokter.id, mdokter.nama');
		$this->db->join('mdokter', 'mdokter.id = mpoliklinik_dokter.iddokter');
		$this->db->where('mpoliklinik_dokter.idpoliklinik', $idpoliklinik);
		$query = $this->db->get('mpoliklinik_dokter');

		$this->output->set_output(json_encode($query->result()));
	}

	public function getKelompok()
	{
		$kelompok = $this->input->post('kelompok');
		$data = '{"kelompok":[';
		$datas = $this->db->query("SELECT * FROM mpasien_kelompok WHERE id NOT IN('0',$kelompok)")->result();
		foreach ($datas as $r) {
			$data .= '{"id":"' . $r->id . '","nama":"' . $r->nama . '"},';
		}
		$data = substr($data, 0, strlen($data) - 1);
		$data .= ']}';
		echo $data;
	}

	public function getBpjsKesehatan()
	{
		$data = get_all('mtarif_bpjskesehatan', ['status' => 1]);
		$this->output->set_output(json_encode($data));
	}

	public function getBpjsTenagakerja()
	{
		$data = get_all('mtarif_bpjstenagakerja', ['status' => 1]);
		$this->output->set_output(json_encode($data));
	}

	public function rujukanBaru()
	{
		if ($this->Mrumahsakit_model->saveData()) {
			$asal = $this->input->post('idtipe');
			$data = get_all('mrumahsakit', ['idtipe' => $asal]);
			$this->output->set_output(json_encode($data));
		}
	}

	public function getPoli()
	{
		$tipepoli = $this->input->post('tipepoli');
		if ($tipepoli != '#') {
			$data = get_all('mpoliklinik', ['idtipe' => $tipepoli, 'status' => 1]);
		} else {
			$data = get_all('mpoliklinik', ['status' => 1]);
		}
		$this->output->set_output(json_encode($data));
	}

	public function getDataPasien($id)
	{
		$data = $this->Tpoliklinik_pendaftaran_model->get_pasien($id);
		// if ($row){
		// $data=$this->get_record_pasien($id,$row);
		// }else{

		// }
		// $data = get_all('mfpasien',array('id' => $id));
		$this->output->set_output(json_encode($data));
	}

	public function getPasien()
	{
		$this->select = [];
		$this->join = [];
		$this->where = [];

		if (isset($_POST['snomedrec']) && $_POST['snomedrec'] != '') {
			$this->where = array_merge($this->where, ['mfpasien.no_medrec LIKE ' => '%' . $_POST['snomedrec'] . '%']);
		}

		if (isset($_POST['snamapasien']) && $_POST['snamapasien'] != '') {
			$this->where = array_merge($this->where, ['mfpasien.nama LIKE ' => '%' . $_POST['snamapasien'] . '%']);
		}

		if (isset($_POST['snama_keluarga']) && $_POST['snama_keluarga'] != '') {
			$this->where = array_merge($this->where, ['mfpasien.nama_keluarga' => $_POST['snama_keluarga']]);
		}

		if (isset($_POST['salamat']) && $_POST['salamat'] != '') {
			$this->where = array_merge($this->where, ['mfpasien.alamat_jalan LIKE ' => '%' . $_POST['salamat'] . '%']);
		}

		if (isset($_POST['stanggallahir']) && $_POST['stanggallahir'] != '') {
			$this->where = array_merge($this->where, ['DATE(mfpasien.tanggal_lahir) LIKE ' => '%' . YMDFormat($_POST['stanggallahir']) . '%']);
		}

		if (isset($_POST['snotelepon']) && $_POST['snotelepon'] != '') {
			$this->where = array_merge($this->where, ['mfpasien.telepon LIKE ' => '%' . $_POST['snotelepon'] . '%']);
		}
		$def = 'A';
		// if (isset($_POST['snomedrec']) && isset($_POST['snamapasien'])){
		// $this->where = array_merge($this->where, array('mfpasien.nama LIKE' => '%'.$def.'%'));
		// }
		$this->order = [
			'id' => 'DESC'
		];
		$this->group = [];
		$this->from = 'mfpasien';

		$this->column_search = ['no_medrec', 'nama', 'nama_keluarga', 'tanggal_lahir', 'alamat_jalan', 'telepon', 'telepon_keluarga'];
		$this->column_order = ['no_medrec', 'nama', 'nama_keluarga', 'tanggal_lahir', 'alamat_jalan', 'telepon', 'telepon_keluarga'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$row[] = "<a href='#' class='selectPasien' data-idpasien='" . $r->id . "' data-dismiss='modal'>" . $r->no_medrec . '</span></a>';
			$row[] = $r->nama;
			$row[] = ($r->jenis_kelamin == 1 ? 'Laki-laki' : 'Perempuan');
			$row[] = HumanDateShort($r->tanggal_lahir);
			$row[] = $r->alamat_jalan;
			$row[] = $r->telepon;
			$row[] = $r->nama_keluarga;
			$row[] = $r->telepon_keluarga;
			// $row[] = ($r->status_kawin == 1 ? 'Belum Menikah' : 'Sudah Menikah');

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getDataHistory($idpasien)
	{
		$history = $this->db->query("SELECT mpoliklinik.nama AS poli, mpoliklinik.idtipe, mdokter.nama, tanggaldaftar, idpasien FROM tpoliklinik_pendaftaran LEFT JOIN mpoliklinik ON(mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik) LEFT JOIN mdokter ON(mdokter.id = tpoliklinik_pendaftaran.iddokter) WHERE idpasien = '" . $idpasien . "' ")->result();

		$html = '<div class="block">
        <div class="block-content">
          <ul class="list list-activity push">';

		foreach ($history as $row) {
			$tipe = ($row->idtipe == 1 ? 'Poliklinik' : 'IGD');
			$html .= '<li>
          <i class="si si-pencil text-info"></i>
          <div class="font-w600">' . $tipe . ' - ' . $row->poli . '</div>
          <div><a href="javascript:void(0)">' . $row->nama . '</a></div>
          <div><small class="text-muted">' . $row->tanggaldaftar . '</small></div>
        </li>';
		}

		$html .= '</ul>
        </div>
      </div>';

		$this->output
	  ->set_output($html)
	  ->_display();
		exit;
	}

	public function get_duplicate_hariini($idpasien, $idpoliklinik, $iddokter, $hari, $bulan, $tahun)
	{
		$hasil = $this->Tpoliklinik_pendaftaran_model->get_duplicate_hariini($idpasien, $idpoliklinik, $iddokter, $hari, $bulan, $tahun);
		$this->output->set_output(json_encode($hasil));
	}

	public function getIndex($uri)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];

		$this->select = [
			'tpoliklinik_pendaftaran.*',
			'mfpasien.nama AS nmpasien',
			'mfpasien.alamat_jalan',
			'mfpasien.no_medrec',
			'mdokter.nama AS nmdokter',
			'mpoliklinik.nama AS namapoliklinik',
			'mpasien_kelompok.nama AS kelompokpasien',
			'tkasir.status AS statuskasir'
		];

		$this->from = 'tpoliklinik_pendaftaran';

		$this->join = [
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', ''],
			['mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', ''],
			['mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT'],
			['mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', ''],
			['tpoliklinik_tindakan', 'tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id', 'LEFT'],
			['tkasir', 'tkasir.idtipe IN (1,2) AND tkasir.idtindakan = tpoliklinik_tindakan.id', 'LEFT']
		];

		$this->where = [];

		if (navigation_roles('tpoliklinik_pendaftaran/getIndexPoli') || navigation_roles('tpoliklinik_pendaftaran/getIndexIgd')) {
		} elseif (navigation_roles('tpoliklinik_pendaftaran/getIndexPoli')) {
			$this->where = ['tpoliklinik_pendaftaran.idtipe' => 1];
		} else {
			$this->where = ['tpoliklinik_pendaftaran.idtipe' => 2];
		}

		// FILTER
		if ($uri == 'filter') {
			if ($this->session->userdata('nomedrec') != null) {
				$this->where = array_merge($this->where, ['mfpasien.no_medrec LIKE' => '%' . $this->session->userdata('nomedrec') . '%']);
			}
			if ($this->session->userdata('namapasien') != null) {
				$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('idkelompokpasien') != '#') {
				$this->where = array_merge($this->where, ['mpasien_kelompok.id' => $this->session->userdata('idkelompokpasien')]);
			}
			if ($this->session->userdata('iddokter') != '#') {
				$this->where = array_merge($this->where, ['mdokter.id' => $this->session->userdata('iddokter')]);
			}
			if ($this->session->userdata('idtipe') != '#') {
				$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.idtipe' => $this->session->userdata('idtipe')]);
			}
			if ($this->session->userdata('idpoliklinik') != '#') {
				$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.idpoliklinik' => $this->session->userdata('idpoliklinik')]);
			}
			if ($this->session->userdata('idstatus') != '#') {
				if ($this->session->userdata('idstatus') != '3') {
					$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.statustindakan' => $this->session->userdata('idstatus')]);
					$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.status' => 1]);
				} else {
					$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.status' => 0]);
				}
			}
			if ($this->session->userdata('tanggaldaftar') != null && $this->session->userdata('tanggaldaftar2') != null) {
				$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar) >=' => YMDFormat($this->session->userdata('tanggaldaftar'))]);
				$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar) <=' => YMDFormat($this->session->userdata('tanggaldaftar2'))]);
			}
		} else {
			$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar)' => date('Y-m-d')]);
			if ($this->session->userdata('idtipe') != '#') {
				$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.idtipe' => $this->session->userdata('idtipe')]);
			}
		}

		$this->order = [
			'tpoliklinik_pendaftaran.noantrian' => 'ASC',
			'tpoliklinik_pendaftaran.tanggaldaftar' => 'DESC',
		];

		$this->group = ['tpoliklinik_pendaftaran.id'];

		$this->column_search = ['tpoliklinik_pendaftaran.nopendaftaran', 'tpoliklinik_pendaftaran.tanggaldaftar', 'tpoliklinik_pendaftaran.noantrian', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.alamat_jalan', 'mpoliklinik.nama', 'mpasien_kelompok.nama'];
		$this->column_order = ['tpoliklinik_pendaftaran.nopendaftaran', 'tpoliklinik_pendaftaran.tanggaldaftar', 'tpoliklinik_pendaftaran.noantrian', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.alamat_jalan', 'mpoliklinik.nama', 'mpasien_kelompok.nama'];
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->statuskasir == 2) {
				$action = '<div class="btn-group">
                <div class="btn-group dropup">
                  <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-print"></span>
                  </button>
                <ul class="dropdown-menu">
                  <li>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'trm_layanan_berkas/print_document/' . $r->id . '/' . $r->idtipe . '/' . $r->id . '">Pinjam Berkas Pelayanan</a>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/1">Pendaftaran</a>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/7">Pendaftaran DM</a>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/2">Tracer</a>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/3">Kartu Status</a>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/4">KIB</a>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->id . '">Sticker ID</a>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/6">SK Diagnosa</a>
                  </li>
                </ul>
                </div>
              </div>';
			} else {
				if ($r->status == '0') {
					$action = '<a href="#" data-toggle="modal" data-target="#data-pendaftaran" id="dataPendaftaran" title="View All"><i class="fa fa-eye"></i></a>';
				} else {
					$action = '<div class="btn-group">
							  <div class="btn-group dropup">
								<button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
								  <span class="fa fa-print"></span>
								</button>
							  <ul class="dropdown-menu">
								<li>';
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trm_layanan_berkas/print_document/' . $r->id . '/' . $r->idtipe . '/' . $r->id . '">Pinjam Berkas Pelayanan</a>';
					if (UserAccesForm($user_acces_form, ['323'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/1">Pendaftaran</a>';
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/7">Pendaftaran DM</a>';
					}
					if (UserAccesForm($user_acces_form, ['324'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/2">Tracer</a>';
					}
					if (UserAccesForm($user_acces_form, ['325'])) {
						$action .= ' <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/3">Kartu Status</a>';
					}
					if (UserAccesForm($user_acces_form, ['326'])) {
						$action .= '  <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/4">KIB</a>';
					}
					if (UserAccesForm($user_acces_form, ['327'])) {
						$action .= '  <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->id . '">Sticker ID</a>';
					}
					if (UserAccesForm($user_acces_form, ['328'])) {
						$action .= '  <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->id . '/6">SK Diagnosa</a>';
					}
					$action .= '	</li>
							  </ul>
							  </div>
							</div>';
					if (UserAccesForm($user_acces_form, ['319'])) {
						$action .= '<a href="' . site_url() . 'tpoliklinik_pendaftaran/update/' . $r->id . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
					}
					if (UserAccesForm($user_acces_form, ['320'])) {
						$action .= '<button class="btn btn-danger btn-sm delete" title="Hapus" onclick="myDelete(' . $r->id . ')"><i class="fa fa-trash-o"></i></button>';
					}
				}
			}

			$row[] = $no;
			$row[] = $r->nopendaftaran;
			$row[] = $r->tanggaldaftar;
			$row[] = $r->noantrian;
			$row[] = $r->no_medrec;
			$row[] = $r->nmpasien;
			$row[] = $r->alamat_jalan;
			$row[] = $r->namapoliklinik;
			$row[] = $r->nmdokter;
			$row[] = $r->kelompokpasien;
			if ($r->statuskasir == 2) {
				$row[] = '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Telah Ditransaksikan</span>';
			} else {
				if ($r->status == '0') {
					$row[] = StatusOK($r->status);
				} else {
					$row[] = StatusTindakan($r->statustindakan);
				}
			}

			$row[] = '<div class="btn-group">' . $action . '</div>';

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function manage_print($id)
	{
		$data = [];
		$data['error'] = '';
		$data['start_awal'] = 1;
		$data['jml'] = 10;
		// $data['start_awal'] 	= 1;
		$data['idpendaftaran'] = $id;
		$data['tab'] = 0;
		if ($this->session->userdata('session_print_sticker')) {
			$data['margin'] = $this->session->userdata('session_print_sticker');
		} else {
			$data['margin'] = 'tengah';
		}
		$data['title'] = 'Print E-Ticket';
		$data['content'] = 'Tpoliklinik_pendaftaran/manage_print';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Farmasi', 'tpasien_penjualan'],
			['Print', 'tpasien_penjualan/manage_print']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function print_e_ticket()
	{
		$data = [];

		$idpendaftaran = $this->input->post('idpendaftaran');
		$row = $this->Tpoliklinik_pendaftaran_model->getPrintInfo($idpendaftaran);
		// print_r($row);exit();
		$data = [
			// 'print'           => $idtipe,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'title' => $row->title,
			'jeniskelamin' => ($row->jeniskelamin == 1 ? 'L' : 'P'),
			'alamat' => $row->alamat,
			'tanggallahir' => $row->tanggallahir,
			'umurtahun' => $row->umurtahun,
			'umurbulan' => $row->umurbulan,
			'umurhari' => $row->umurhari,
			'notelepon' => $row->telepon,
			'pekerjaan' => $row->namapekerjaan,

		];

		$data_margin = ['session_print_sticker' => $this->input->post('margin')];
		$this->session->set_userdata($data_margin);
		// $this->session->user
		// print_r($data);exit();
		// $obat_kosong = set_obat_kosong();
		// $obat_kosong = new stdClass();
		// $obat_kosong->nomedrec = '';
		// $obat_kosong->namapasien = '';
		// $obat_kosong->jeniskelamin = '';
		// $obat_kosong->alamat = '';
		// $obat_kosong->tanggallahir = '';
		// $obat_kosong->umurtahun = '';
		// $obat_kosong->umurbulan = '';
		// $obat_kosong->umurhari = '';
		// $obat_kosong->pekerjaan = '';

		$data['margin'] = $this->input->post('margin');
		$data['start_awal'] = $this->input->post('start_awal');
		$data['jml'] = $this->input->post('jml');
		$data['start_akhir'] = $this->input->post('jml') + $this->input->post('start_awal') - 1;

		$data_array = [];

		$data_akhir = cek_akhir_row($data['start_akhir']);

		for ($i = 1; $i <= $data_akhir; $i++) {
			if ($i < $data['start_awal']) {
				$data_array[$i] = '0';
			} else {
				if ($i > $data['start_akhir']) {
					$data_array[$i] = '0';
				} else {
					$data_array[$i] = '1';
				}
			}
		}
		$data['data_array'] = $data_array;
		$data['data_akhir'] = $data_akhir;
		// print_r($data_array);exit();
		// print_r($chck_racikan);exit();
		// if ($where_obat) {
		// $data['list_data_ticket_obat']	 = $this->tpaspen->list_data_ticket_obat($where_obat, $idpenjualan);
		// // $data['list_data_ticket_obat']	 = array_merge($data_kosong,$data['list_data_ticket_obat']);
		// } else {
		// $data['list_data_ticket_obat']	 = array();
		// }
		// if ($where_racikan) {
		// $data['list_data_ticket_racikan']	 = $this->tpaspen->list_data_ticket_racikan($where_racikan, $idpenjualan);
		// // print_r($data['list_data_ticket_racikan']);exit();
		// // $data['list_data_ticket_racikan']	 = array_merge($data_kosong,$data['list_data_ticket_racikan']);
		// } else {
		// $data['list_data_ticket_racikan']=array();
		// }

		// $data['list_data_ticket_obat_all'] = array_merge($data_kosong, $data);

		// print_r("<pre>");
		// print_r($data['list_data_ticket_obat']);
		// print_r("<br/>");

		// print_r($data['list_data_ticket_racikan']);
		// print_r("</pre>");
		// exit();
		//
		// print_r($data);exit();
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$html = $this->parser->parse('Tpoliklinik_pendaftaran/print/cetak_sticker', array_merge($data, backend_info()), true);
		// print_r($html);exit();
		$html = $this->parser->parse_string($html, $data);

		$dompdf->loadHtml($html);
		// (Optional) Setup the paper size and orientation

		// $customPaper = array(0,0,462,600);
		// $dompdf->set_paper($customPaper, 'portrait');
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Faktur .pdf', ['Attachment' => 0]);
	}
}
