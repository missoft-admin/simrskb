<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcahaya extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mcahaya_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Mcahaya_model->get_cahaya();
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1726'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Replex Cahaya';
			$data['content'] 		= 'Mcahaya/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Replex Cahaya",'mcahaya')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_cahaya(){
		$cahaya_id=$this->input->post('cahaya_id');
		$data=array(
			'cahaya_1'=>$this->input->post('cahaya_1'),
			'cahaya_2'=>$this->input->post('cahaya_2'),
			'kategori_cahaya'=>$this->input->post('kategori_cahaya'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($cahaya_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mcahaya',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$cahaya_id);
		    $hasil=$this->db->update('mcahaya',$data);
		}
		  
		  json_encode($hasil);
	}
	function update_satuan(){
		$satuan_cahaya=$this->input->post('satuan_cahaya');
		$data=array(
			'satuan_cahaya'=>$this->input->post('satuan_cahaya'),
			
		);
		$hasil=$this->db->update('mcahaya_satuan',$data);
		
		json_encode($hasil);
	}
	
	function load_cahaya()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mcahaya` H
							where H.staktif='1'
							ORDER BY H.cahaya_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('cahaya_1','cahaya_1','kategori_cahaya');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->cahaya_1.' - '.$r->cahaya_2);
          $result[] = $r->kategori_cahaya;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1728'))){
		  $aksi .= '<button onclick="edit_cahaya('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1729'))){
		  $aksi .= '<button onclick="hapus_cahaya('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_cahaya(){
	  $cahaya_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$cahaya_id);
		$hasil=$this->db->update('mcahaya',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_cahaya(){
	  $cahaya_id=$this->input->post('id');
	  $q="SELECT *FROM mcahaya H WHERE H.id='$cahaya_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
