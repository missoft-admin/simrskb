<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpasien_title extends CI_Controller {

	/**
	 * Title Pasien controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpasien_title_model');
		$this->load->helper('path');
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('22'))){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Title Pasien';
		$data['content'] 		= 'Mpasien_title/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Title Pasien",'#'),
									    			array("List",'mpasien_title')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'singkatan' 		=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Title Pasien';
		$data['content'] 		= 'Mpasien_title/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Title Pasien",'#'),
									    			array("Tambah",'mpasien_title')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mpasien_title_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'singkatan' 		=> $row->singkatan,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Title Pasien';
				$data['content']	 	= 'Mpasien_title/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Title Pasien",'#'),
											    			array("Ubah",'mpasien_title')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpasien_title','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasien_title');
		}
	}

	function delete($id){
		
		$this->Mpasien_title_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mpasien_title','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('singkatan', 'Singkatan', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mpasien_title_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpasien_title','location');
				}
			} else {
				if($this->Mpasien_title_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpasien_title','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpasien_title/manage';

		if($id==''){
			$data['title'] = 'Tambah Title Pasien';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Title Pasien",'#'),
															array("Tambah",'mpasien_title')
													);
		}else{
			$data['title'] = 'Ubah Title Pasien';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Title Pasien",'#'),
															array("Ubah",'mpasien_title')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$this->select = array();
			$this->from   = 'mpasien_title';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama','singkatan');
      $this->column_order    = array('nama','singkatan');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					if($r->statuslock == 0){
						$action = '<div class="btn-group">
												<a href="'.site_url().'mpasien_title/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
												<a href="#" data-urlindex="'.site_url().'mpasien_title" data-urlremove="'.site_url().'mpasien_title/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
											</div>';
					}else{
						$action = '<a href="#" data-toggle="tooltip" title="Terkunci" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>';
					}

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->singkatan;
          $row[] = $action;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
