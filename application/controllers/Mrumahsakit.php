<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrumahsakit extends CI_Controller
{
	/**
	 * RS & Klinik controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrumahsakit_model');
		$this->load->helper('path');
	}

	public function index()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['27'])) {
			$data = [
				'idtipe' => '',
				'idkategori' => '',
			];

			$data['error'] = '';
			$data['title'] = 'RS & Klinik';
			$data['content'] = 'Mrumahsakit/index';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['RS & Klinik', '#'],
				['List', 'mrumahsakit']
			];

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		} else {
			redirect('page404');
		}
	}

	public function filter()
	{
		$data = [
			'idtipe' => $this->input->post('idtipe'),
			'idkategori' => $this->input->post('idkategori'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'RS & Klinik';
		$data['content'] = 'Mrumahsakit/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['RS & Klinik', '#'],
			['List', 'mrumahsakit']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create()
	{
		$data = [
			'id' => '',
			'idtipe' => '',
			'nama' => '',
			'idkategori' => '',
			'jenis_berekanan' => '',
			'persentase' => '',
			'ratetarif' => '',
			'berdasarkan' => '',
			'tentukantarif' => '',
			'alkes_poli' => '',
			'obat_poli' => '',
			'alkes_farmasi' => '',
			'obat_farmasi' => '',
			'status' => '',
			'idtemp' => hash('ripemd160', $this->session->userdata('user_id') . '' . date("Y-m-d"))
		];

		$data['error'] = '';
		$data['title'] = 'Tambah RS & Klinik';
		$data['content'] = 'Mrumahsakit/manage';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['RS & Klinik', '#'],
			['Tambah', 'mrumahsakit']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function update($id)
	{
		if ($id != '') {
			$row = $this->Mrumahsakit_model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
					'id' => $row->id,
					'idtipe' => $row->idtipe,
					'nama' => $row->nama,
					'idkategori' => $row->idkategori,
					'jenis_berekanan' => $row->jenis_berekanan,
					'persentase' => $row->persentase,
					'ratetarif' => $row->ratetarif,
					'berdasarkan' => $row->berdasarkan,
					'tentukantarif' => $row->tentukantarif,
					'alkes_poli' => $row->alkes_poli,
					'obat_poli' => $row->obat_poli,
					'alkes_farmasi' => $row->alkes_farmasi,
					'obat_farmasi' => $row->obat_farmasi,
					'status' => $row->status,
					'idtemp' => $row->id
				];
				$data['error'] = '';
				$data['title'] = 'Ubah RS & Klinik';
				$data['content'] = 'Mrumahsakit/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['RS & Klinik', '#'],
					['Ubah', 'mrumahsakit']
				];

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mrumahsakit/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('mrumahsakit/index');
		}
	}

	public function delete($id)
	{
		$this->Mrumahsakit_model->softDelete($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah terhapus.');
		redirect('mrumahsakit/index', 'location');
	}

	public function save()
	{
		$this->form_validation->set_rules('idtipe', 'Tipe', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('idkategori', 'Kategori', 'trim|required');

		if ($this->form_validation->run() == true) {
			if ($this->input->post('id') == '') {
				if ($this->Mrumahsakit_model->saveData()) {
					$this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'data telah disimpan.');
					redirect('mrumahsakit/index', 'location');
				}
			} else {
				if ($this->Mrumahsakit_model->updateData()) {
					$this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'data telah disimpan.');
					redirect('mrumahsakit/index', 'location');
				}
			}
		} else {
			$this->failed_save($this->input->post('id'));
		}
	}

	public function failed_save($id)
	{
		$data = $this->input->post();
		$data['error'] = validation_errors();
		$data['content'] = 'Mrumahsakit/manage';

		if ($id == '') {
			$data['title'] = 'Tambah RS & Klinik';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['RS & Klinik', '#'],
				['Tambah', 'mrumahsakit']
			];
		} else {
			$data['title'] = 'Ubah RS & Klinik';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['RS & Klinik', '#'],
				['Ubah', 'mrumahsakit']
			];
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function getIndex($uri)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$this->select = [];
		$this->from = 'mrumahsakit';
		$this->join = [];

		// FILTER
		if ($uri == 'filter') {
			$this->where = [];
			if ($this->session->userdata('idtipe') != '#') {
				$this->where = array_merge($this->where, ['idtipe' => $this->session->userdata('idtipe')]);
			}
			if ($this->session->userdata('idkategori') != '#') {
				$this->where = array_merge($this->where, ['idkategori' => $this->session->userdata('idkategori')]);
			}
		} else {
			$this->where = [
				'status' => '1'
			];
		}

		$this->order = [
			'nama' => 'ASC'
		];
		$this->group = [];

		$this->column_search = ['nama'];
		$this->column_order = ['nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$row[] = $no;
			$row[] = $r->nama;
			$row[] = ($r->idtipe == 1 ? 'Klinik' : 'Rumah Sakit');
			$row[] = ($r->idkategori == 1 ? 'Berekanan' : 'Tidak Berekanan');
			$row[] = $r->persentase;
			$aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form, ['30'])) {
				$aksi .= '<a href="' . site_url() . 'mrumahsakit/update/' . $r->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
			}
			if (UserAccesForm($user_acces_form, ['31'])) {
				$aksi .= '<a href="#" href="#" data-urlindex="' . site_url() . 'mrumahsakit" data-urlremove="' . site_url() . 'mrumahsakit/delete/' . $r->id . '" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
			}
			$aksi .= '</div>';
			$row[] = $aksi;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getTarifLaboratorium()
	{
		$idpath = $this->input->post('idpath');
		$panjang = strlen($idpath);
		$idtipe = $this->input->post('idtipe');
		$namatarif = $this->input->post('namatarif');

		$where = '';
		if ($idpath != '#') {
			$where .= ' AND left(mtarif_laboratorium.path,' . $panjang . ")='$idpath'";
		}
		if ($idtipe != '#') {
			$where .= " AND mtarif_laboratorium.idtipe='$idtipe'";
		}
		if ($namatarif != '') {
			$where .= " AND mtarif_laboratorium.nama LIKE '%" . $namatarif . "%'";
		}

		$this->select = [];
		$this->from = "
			(
				SELECT 
					mtarif_laboratorium.id,
					mtarif_laboratorium.idtipe,
					mtarif_laboratorium.idkelompok,
					mtarif_laboratorium.path,
					mtarif_laboratorium.level,
					mtarif_laboratorium.idpaket,
					mtarif_laboratorium.nama 
				FROM 
					mtarif_laboratorium
				WHERE 
					mtarif_laboratorium.status = '1' AND
					mtarif_laboratorium.id NOT IN (
						SELECT 
							mrumahsakit_tarif.idtarif
						FROM
							mrumahsakit_tarif 
						WHERE 
							mrumahsakit_tarif.status = '1' AND 
							mrumahsakit_tarif.reference_table = 'mtarif_laboratorium'
					) 
				" . $where . '
			) AS result
			ORDER BY path ASC';
		$this->join = [];
		$this->where = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = ['tipe_nama', 'nama'];
		$this->column_order = [];

		$list = $this->datatable->get_datatables(true);

		$data = [];
		foreach ($list as $r) {
			$row = [];
			$row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok == '0') {
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasasarana"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasapelayanan"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="biayaperawatan"><span></span></label>';
			} else {
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			}
			$row[] = $r->id;
			$row[] = 'mtarif_laboratorium';

			$data[] = $row;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(true),
			'recordsFiltered' => $this->datatable->count_all(true),
			'data' => $data
		];

		echo json_encode($output);
	}

	public function getTarifFisioterapi()
	{
		$idpath = $this->input->post('idpath');
		$panjang = strlen($idpath);
		$namatarif = $this->input->post('namatarif');

		$where = '';
		if ($idpath != '#') {
			$where .= ' AND left(mtarif_fisioterapi.path,' . $panjang . ")='$idpath'";
		}

		if ($namatarif != '') {
			$where .= " AND mtarif_fisioterapi.nama LIKE '%" . $namatarif . "%'";
		}

		$this->select = [];
		$this->from = "
			(
				SELECT 
					mtarif_fisioterapi.id,
					mtarif_fisioterapi.idkelompok,
					mtarif_fisioterapi.path,
					mtarif_fisioterapi.level,
					mtarif_fisioterapi.nama 
				FROM 
					mtarif_fisioterapi
				WHERE 
					mtarif_fisioterapi.status = '1' AND
					mtarif_fisioterapi.id NOT IN (
						SELECT 
							mrumahsakit_tarif.idtarif 
						FROM
							mrumahsakit_tarif 
						WHERE 
							mrumahsakit_tarif.status = '1' AND 
							mrumahsakit_tarif.reference_table = 'mtarif_fisioterapi'
					) 
				" . $where . '
			) AS result
			ORDER BY path ASC';
		$this->join = [];
		$this->where = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = ['tipe_nama', 'nama'];
		$this->column_order = [];

		$list = $this->datatable->get_datatables(true);

		$data = [];
		foreach ($list as $r) {
			$row = [];
			$row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok == '0') {
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasasarana"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasapelayanan"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="biayaperawatan"><span></span></label>';
			} else {
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			}
			$row[] = $r->id;
			$row[] = 'mtarif_fisioterapi';

			$data[] = $row;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(true),
			'recordsFiltered' => $this->datatable->count_all(true),
			'data' => $data
		];

		echo json_encode($output);
	}

	public function getTarifRawatJalan()
	{
		$idpath = $this->input->post('idpath');
		$panjang = strlen($idpath);
		$namatarif = $this->input->post('namatarif');

		$where = '';
		if ($idpath != '#') {
			$where .= ' AND left(mtarif_rawatjalan.path,' . $panjang . ")='$idpath'";
		}
		if ($namatarif != '') {
			$where .= " AND mtarif_rawatjalan.nama LIKE '%" . $namatarif . "%'";
		}

		$this->select = [];
		$this->from = "
			(
				SELECT 
					mtarif_rawatjalan.id,
					mtarif_rawatjalan.idkelompok,
					mtarif_rawatjalan.path,
					mtarif_rawatjalan.level,
					mtarif_rawatjalan.nama 
				FROM 
					mtarif_rawatjalan
				WHERE 
					mtarif_rawatjalan.status = '1' AND
					mtarif_rawatjalan.id NOT IN (
						SELECT 
							mrumahsakit_tarif.idtarif 
						FROM
							mrumahsakit_tarif 
						WHERE 
							mrumahsakit_tarif.status = '1' AND 
							mrumahsakit_tarif.reference_table = 'mtarif_rawatjalan'
					) 
				" . $where . '
			) AS result
			ORDER BY path ASC';
		$this->join = [];
		$this->where = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = ['tipe_nama', 'nama'];
		$this->column_order = [];

		$list = $this->datatable->get_datatables(true);

		$data = [];
		foreach ($list as $r) {
			$row = [];
			$row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok == '0') {
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasasarana"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasapelayanan"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="biayaperawatan"><span></span></label>';
			} else {
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			}
			$row[] = $r->id;
			$row[] = 'mtarif_rawatjalan';


			$data[] = $row;

		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(true),
			'recordsFiltered' => $this->datatable->count_all(true),
			'data' => $data
		];

		echo json_encode($output);
	}

	public function getTarifRadiologi()
	{
		$idpath = $this->input->post('idpath');
		$panjang = strlen($idpath);
		$idtipe = $this->input->post('idtipe');
		$namatarif = $this->input->post('namatarif');

		$where = '';
		if ($idpath != '#') {
			$where .= ' AND left(mtarif_radiologi.path,' . $panjang . ")='$idpath'";
		}
		if ($idtipe != '#') {
			$where .= " AND mtarif_radiologi.idtipe='$idtipe'";
		}
		if ($namatarif != '') {
			$where .= " AND mtarif_radiologi.nama LIKE '%" . $namatarif . "%'";
		}

		$this->select = [];
		$this->from = "
			(
				SELECT 
					mtarif_radiologi.id,
					mtarif_radiologi.idtipe,
					mtarif_radiologi.idkelompok,
					mtarif_radiologi.path,
					mtarif_radiologi.level,
					(
						CASE 
							WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
								CONCAT(mtarif_radiologi.nama,  ' (', mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ')')
						ELSE
							mtarif_radiologi.nama
						END
					) AS namatarif
				FROM 
					mtarif_radiologi
				LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi_film.id = mtarif_radiologi.idfilm
				LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi_expose.id = mtarif_radiologi.idexpose
				WHERE 
					mtarif_radiologi.status = '1' AND
					mtarif_radiologi.id NOT IN (
						SELECT 
							mrumahsakit_tarif.idtarif 
						FROM
							mrumahsakit_tarif 
						WHERE 
							mrumahsakit_tarif.status = '1' AND 
							mrumahsakit_tarif.reference_table = 'mtarif_radiologi'
					) 
				" . $where . '
			) AS result
			ORDER BY path ASC';
		$this->join = [];
		$this->where = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = ['tipe_nama', 'nama'];
		$this->column_order = [];

		$list = $this->datatable->get_datatables(true);

		$data = [];
		foreach ($list as $r) {
			$row = [];
			$row[] = TreeView($r->level, $r->namatarif);
			if ($r->idkelompok == '0') {
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasasarana"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jasapelayanan"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp"><span></span></label>';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="biayaperawatan"><span></span></label>';
			} else {
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			}
			$row[] = $r->id;
			$row[] = 'mtarif_radiologi';

			$data[] = $row;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(true),
			'recordsFiltered' => $this->datatable->count_all(true),
			'data' => $data
		];

		echo json_encode($output);
	}

	public function saveTarifTertentu()
	{
		$data = [];
		$data['idrumahsakit'] = $this->input->post('idrumahsakit');
		$data['reference_table'] = $this->input->post('reference_table');
		$data['idtarif'] = $this->input->post('idtarif');
		$data['namatarif'] = $this->input->post('namatarif');
		$data['jasasarana'] = $this->input->post('jasasarana');
		$data['jasapelayanan'] = $this->input->post('jasapelayanan');
		$data['bhp'] = $this->input->post('bhp');
		$data['biayaperawatan'] = $this->input->post('biayaperawatan');

		if ($this->db->replace('mrumahsakit_tarif', $data)) {
			return true;
		}
	}

	public function getTarifTertentu($id)
	{
		$this->db->where('idrumahsakit', $id);
		$this->db->where('status', '1');
		$query = $this->db->get('mrumahsakit_tarif');
		$data = $query->result();

		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data, JSON_PRETTY_PRINT));
	}

	public function removeTarifTertentu($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('mrumahsakit_tarif');
		return true;
	}
}
