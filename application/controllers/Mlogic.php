<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlogic extends CI_Controller {

	/**
	 * Pengaturan Logic controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mlogic_model');
  }

	function index(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '#',
			'status' 				=> '1'
		);
		$data['error'] 			= '';
		$data['title'] 			= 'Pengaturan Logic';
		$data['content'] 		= 'Mlogic/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Pengaturan Logic",'#'),
									    			array("List",'Mlogic')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_unit($id)
	{
		$arr['detail'] = $this->mlogic_model->list_unit($id);
		$this->output->set_output(json_encode($arr));
	}
	function get_edit(){
		$id     = $this->input->post('id');
		$q="SELECT D.*,J.nama as jenis_nama,U.`name` as user_nama FROM `mlogic_detail` D
				LEFT JOIN mjenis_pengajuan_rka J ON J.id=D.idjenis
				LEFT JOIN musers U ON U.id=D.iduser
				WHERE D.id='$id'";
		$arr['detail'] =$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr['detail']));
	}
	function list_user($id,$step,$tipe_rka,$idjenis)
	{
		$arr['detail'] = $this->mlogic_model->list_user($id,$step,$tipe_rka,$idjenis);
		$this->output->set_output(json_encode($arr));
	}
	function list_user_bendahara($id,$tipe_rka,$idjenis)
	{
		$arr['detail'] = $this->mlogic_model->list_user_bendahara($id,$tipe_rka,$idjenis);
		$this->output->set_output(json_encode($arr));
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> '1'
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengaturan Logic';
		$data['content'] 		= 'Mlogic/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Pengaturan Logic",'#'),
									    			array("Tambah",'Mlogic')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function simpan_add()
	{
		$data=array(
			'idlogic'=>$this->input->post('id'),
			'idunit'=>$this->input->post('idunit'),
			'created_by'=>$this->session->userdata('user_id'),
			'created_date'=>date('Y-m-d H:i:s'),
			
		);
		
       // print_r($data);
		$result = $this->db->insert('mlogic_unit',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function simpan_user()
	{
		$data=array(
			'idlogic'=>$this->input->post('id'),
			'iduser'=>$this->input->post('iduser'),
			'tipe_rka'=>$this->input->post('tipe_rka'),
			'idjenis'=>$this->input->post('idjenis'),
			'created_by'=>$this->session->userdata('user_id'),
			'created_date'=>date('Y-m-d H:i:s'),
			
		);
		
       // print_r($data);
		$result = $this->db->insert('mlogic_user_bendahara',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function simpan_detail()
	{
		$id_edit=$this->input->post('id_edit');
		$data=array(
				'idlogic'=>$this->input->post('idlogic'),
				'iduser'=>$this->input->post('iduser'),
				'step'=>$this->input->post('step'),
				'proses_setuju'=>$this->input->post('proses_setuju'),
				'proses_tolak'=>$this->input->post('proses_tolak'),
				'tipe_rka'=>$this->input->post('tipe_rka'),
				'idjenis'=>$this->input->post('idjenis'),
				'operand'=>$this->input->post('operand'),
				'nominal'=>RemoveComma($this->input->post('nominal')),
				
				
			);
		// if ($data['operand']=='>='){
			// $data['nominal']=0;
		// }else{
			// $data['nominal']=RemoveComma($this->input->post('nominal'));
		// }
		if ($id_edit==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');			
			$result = $this->db->insert('mlogic_detail',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');		
			$this->db->where('id',$id_edit);
			$result = $this->db->update('mlogic_detail',$data);
		}
       
		
		$this->output->set_output(json_encode($result));
	}
	function simpan_edit()
	{
		$data=array(
			'deskripsi'=>$this->input->post('deskripsi'),
			'tanggal_hari'=>$this->input->post('tanggal_hari'),
			
		);
		$this->db->where('id',$this->input->post('tedit'));
		
       // print_r($data);
		$result = $this->db->update('mlogic_setting',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function hapus_unit()
	{
		$this->db->where('idlogic',$this->input->post('idlogic'));		
		$this->db->where('idunit',$this->input->post('idunit'));		
       // print_r($data);
		$result = $this->db->delete('mlogic_unit');
		$this->output->set_output(json_encode($result));
	}
	function hapus_user()
	{
		$this->db->where('idlogic',$this->input->post('idlogic'));		
		$this->db->where('iduser',$this->input->post('iduser'));		
       // print_r($data);
		$result = $this->db->delete('mlogic_user_bendahara');
		$this->output->set_output(json_encode($result));
	}
	function hapus_det()
	{
		$id=$this->input->post('id');
		$this->db->where('id',$this->input->post('id'));	
		$data=array(
			'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
		);
       // print_r($data);
	   
		$result = $this->db->update('mlogic_detail',$data);
		echo json_encode($result);
	}
	
	function load_unit()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT M.id,M.nama from mlogic_unit L
			LEFT JOIN munitpelayanan M ON M.id=L.idunit
			WHERE L.idlogic='$id'
			ORDER BY M.nama ASC
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->nama;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus" type="button" title="Hapus"><i class="fa fa-close"></i></button>';
				
			
			$aksi.='</div>';
			
			$row[] = $aksi;			
            $row[] = $r->id;//3
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_user()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT M.id,M.name as nama,J.nama jenis_nama,L.tipe_rka,L.idjenis 
			From mlogic_user_bendahara L
			LEFT JOIN mjenis_pengajuan_rka J ON J.id=L.idjenis
			LEFT JOIN musers M ON M.id=L.iduser
			WHERE L.idlogic='$id'
			ORDER BY L.idlogic,L.tipe_rka,L.idjenis ASC
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama','jenis_nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = ($r->tipe_rka=='1'?'RKA':'NON RKA');
            $row[] = $r->jenis_nama;
            $row[] = $r->nama;
            $aksi       = '<div class="btn-group">';				
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus_user" type="button" title="Hapus"><i class="fa fa-close"></i></button>';			
			$aksi.='</div>';
			
			$row[] = $aksi;			
            $row[] = $r->id;//2
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function load_detail()
    {
		
		$idlogic     = $this->input->post('idlogic');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT D.*,J.nama as jenis_nama,U.`name` as user_nama FROM `mlogic_detail` D
				LEFT JOIN mjenis_pengajuan_rka J ON J.id=D.idjenis
				LEFT JOIN musers U ON U.id=D.iduser
				WHERE D.idlogic='$idlogic' AND D.status='1'
				ORDER BY D.tipe_rka,D.idjenis,D.step,D.id 
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama','tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->step;
            
            $row[] = ($r->tipe_rka=='1'?'RKA':'NON RKA');
            $row[] = $r->jenis_nama;
            $row[] = (($r->operand=='>=' && $r->nominal=='0')?'BEBAS':$r->operand.' '.number_format($r->nominal,0));
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button  type="button" class="btn btn-xs btn-primary edit" title="Edit"><i class="fa fa-pencil"></i></button>';				
			$aksi 		.= '<button type="button" class="btn btn-xs btn-danger hapus_det" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
            $row[] = $r->user_nama;
			$row[] = ($r->proses_setuju=='1'?'Langsung':'Menunggu User');
            $row[] = ($r->proses_tolak=='1'?'Langsung':'Menunggu User');
			$row[] = $aksi;			
            $row[] = $r->id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function update($id){
		
		if($id != ''){
			$row = $this->mlogic_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Pengaturan Logic';
				$data['content']	 	= 'Mlogic/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Pengaturan Logic",'#'),
											    			array("Ubah",'mlogic')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mlogic','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mlogic');
		}
	}
	
	function delete($id){
		// print_r($id);exit();
		$this->mlogic_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mlogic','location');
	}
	function aktifkan($id){
		// print_r($id);exit();
		$this->mlogic_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah Aktif kembali.');
		redirect('mlogic','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->mlogic_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlogic/update/'.$id,'location');
				}
			} else {
				if($this->mlogic_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlogic/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mlogic/manage';

		if($id==''){
			$data['title'] = 'Tambah Pengaturan Logic';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Logic",'#'),
															array("Tambah",'Mlogic')
													);
		}else{
			$data['title'] = 'Ubah Pengaturan Logic';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Logic",'#'),
															array("Ubah",'Mlogic')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
		$status_aktif     = $this->input->post('status_aktif');
		$kelompok_operasi_id     = $this->input->post('kelompok_operasi_id');
		
		$iduser=$this->session->userdata('user_id');
		
		$where='';
		if ($status_aktif !='#'){
			if ($status_aktif=='1') {
				$where .=" AND M.status=='1'";				
			}else{
				$where .=" AND M.status=='0'";					
			}
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$from="(SELECT *FROM mlogic M
			
			GROUP BY M.id
					) as tbl";
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $no;
          $row[] = $r->nama;
		  $status='';
		  if ($r->status=='1'){
			  $status='<span class="label label-success">AKTIF</span>';
		  }else{
			$status='<span class="label label-danger">TIDAK AKTIF</span>';
		  }
          $row[] = $status;
          $aksi = '<div class="btn-group">';
          
		  if ($r->status=='1'){
          	$aksi .= '<a href="'.site_url().'mlogic/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mlogic" data-urlremove="'.site_url().'mlogic/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
				
			}else{
			
			$aksi .= '<a href="#" data-urlindex="'.site_url().'mjenis_pengajuan" data-urlremove="'.site_url().'mlogic/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
			
		  // if (UserAccesForm($user_acces_form,array('315'))){
          // }
          
		  // if (UserAccesForm($user_acces_form,array('316'))){
          // }
          $aksi .= '</div>';
          $row[] = $aksi;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
				  
		
  }
	function ajaxSave(){
		if ($this->mlogic_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
