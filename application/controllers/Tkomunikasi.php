<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tkomunikasi extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->helper('path');
		
  }

  function get_logic_formulir($pendaftaran_id){
	  $q="SELECT H.*
		, (SELECT  H.tanggal FROM tpoliklinik_pendaftaran D WHERE D.idpasien=H.idpasien AND H.id!=D.id AND D.idpoliklinik=H.idpoliklinik ORDER BY D.tanggal DESC LIMIT 1) as tgl_akhir
		FROM tpoliklinik_pendaftaran H
		WHERE H.id='74342'
		GROUP BY H.id
		";
	  $data_pasien=$this->db->query($q)->row();
	  $hasil='#';
	  if ($data_pasien){
		  
	  }
	  echo $hasil;
  }
  function index($tab='1'){
	    $log['path_tindakan']='tkomunikasi';
		$this->session->set_userdata($log);
		get_ppa_login();
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='".$this->session->userdata('user_id')."' AND H.tipepegawai='2' AND H.staktif='1'";
		// // $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		// $data = $this->db->query($q)->row_array();	
		// if (){}
		if (UserAccesForm($user_acces_form,array('2188'))){
			// print_r($data);exit;	
			// $data['list_ruangan'] 			= $this->Tpendaftaran_poli_ttv_model->list_ruangan();
			// $data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			// $data['list_dokter'] 			= $this->Tpendaftaran_poli_ttv_model->list_dokter();
			// $data['list_ruang'] 			= $this->Tpendaftaran_poli_ttv_model->list_ruang();
			
			$data['mppa_id'] 			= $login_ppa_id;
			$data['st_ranap'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			$date1=date_create(date('Y-m-d'));
			date_add($date1,date_interval_create_from_date_string("-7 days"));
			$date1= date_format($date1,"Y-m-d");
			$data['tanggal_permintaan_1'] 			= DMYFormat($date1);
			$data['tanggal_permintaan_2'] 			= DMYFormat(date('Y-m-d'));
			$data['tanggal_1'] 			= '';
			$data['tanggal_2'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Pemeriksaan Komunkasi Efektif';
			$data['content'] 		= 'Tkomunikasi/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("Pemeriksaan Komunkasi Efektif",'tpendaftaran_poli_ttv')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()	{
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			// $data_user=get_acces();
			// $user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$st_ranap =$this->input->post('st_ranap');
			$mppa_id =$this->input->post('mppa_id');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$tanggal_permintaan_1 =$this->input->post('tanggal_permintaan_1');
			$tanggal_permintaan_2 =$this->input->post('tanggal_permintaan_2');
			$tab =$this->input->post('tab');
			if ($tab=='1'){
				$where .=" AND (H.st_verifikasi) = '0'";
			}
			if ($tab=='2'){
				$where .=" AND (H.st_verifikasi) = '1'";
			}
			if ($st_ranap!='#'){
				$where .=" AND (H.st_ranap) = '$st_ranap'";
			}
			if ($tanggal_permintaan_1 !=''){
				$where .=" AND DATE(H.tanggal_laporan) >='".YMDFormat($tanggal_permintaan_1)."' AND DATE(H.tanggal_laporan) <='".YMDFormat($tanggal_permintaan_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal_laporan) >='".date('Y-m-d')."'";
			}
			if ($tanggal_1 !=''){
				$where .=" AND ((DATE(MP.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tanggal_2)."')";
				$where .=" OR (DATE(MR.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(MR.tanggaldaftar) <='".YMDFormat($tanggal_2)."'))";
			}
			if ($pencarian!=''){
				$where .=" AND ((MR.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (MP.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (MP.no_medrec LIKE '%".$pencarian."%')";
				$where .=" OR (MR.no_medrec LIKE '%".$pencarian."%')";
				$where .=" )";
			}
			
			// if ($idpoli!='#'){
				// $where .=" AND (H.idpoliklinik) = '$idpoli'";
			// }else{
				// $where .=" AND H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_emergency)";
				// $where .=" AND (H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_direct)) ";
			// }
			// if ($iddokter!='#'){
				// $where .=" AND (H.iddokter) = '$iddokter'";
			// }
			// if ($ruangan_id!='#'){
				// $where .=" AND (H.ruangan_id) = '$ruangan_id'";
			// }
		
			// print_r($tab);exit;
			$sql_alergi=get_alergi_sql();
			$this->select = array();
			$from="
					(
						SELECT 
					CASE WHEN H.st_ranap='1' THEN MR.tanggaldaftar ELSE MP.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,MPOL.nama as nama_poli,MPOL.idtipe as idtipe_poli
					,CASE WHEN H.st_ranap='1' THEN MPK.nama ELSE MPKP.nama END as nama_kel_pasien
					,CASE WHEN H.st_ranap='1' THEN rek_ri.nama ELSE rek_rj.nama END as nama_rekanan
					,mruangan.nama as nama_ruangan,mbed.nama as nama_bed,mkelas.nama as nama_kelas
					,CASE WHEN H.st_ranap='1' THEN MDR.nama ELSE MD.nama END as nama_dokter
					,CASE WHEN H.st_ranap='1' THEN MR.idkelompokpasien ELSE MP.idkelompokpasien END as idkelompokpasien
					,CASE WHEN H.st_ranap='1' THEN MR.no_medrec ELSE MP.no_medrec END as no_medrec
					,CASE WHEN H.st_ranap='1' THEN MR.title ELSE MP.title END as title
					,CASE WHEN H.st_ranap='1' THEN MR.namapasien ELSE MP.namapasien END as namapasien
					,CASE WHEN H.st_ranap='1' THEN MR.umurhari ELSE MP.umurhari END as umurhari
					,CASE WHEN H.st_ranap='1' THEN MR.umurbulan ELSE MP.umurbulan END as umurbulan
					,CASE WHEN H.st_ranap='1' THEN MR.umurtahun ELSE MP.umurtahun END as umurtahun
					,CASE WHEN H.st_ranap='1' THEN MR.jenis_kelamin ELSE MP.jenis_kelamin END as jenis_kelamin
					,H.*,ppa_verif.nama as nama_verif,ppa_kerjakan.nama as pelaksana
					,PL.nama as yang_dilaporkan,PB.nama as pemberi
					,MPOL.nama as poli,mppa.nama as nama_mppa,RJ.ref as nama_jenis 
					,mfpasien.tanggal_lahir,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
					FROM `tpoliklinik_komunikasi` H
					INNER JOIN mfpasien ON mfpasien.id=H.idpasien
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
					LEFT JOIN mruangan ON mruangan.id=MR.idruangan
					LEFT JOIN mpasien_kelompok MPK ON MPK.id=MR.idkelompokpasien
					LEFT JOIN mpasien_kelompok MPKP ON MPKP.id=MP.idkelompokpasien
					LEFT JOIN mrekanan rek_rj ON rek_rj.id=MP.idrekanan
					LEFT JOIN mrekanan rek_ri ON rek_ri.id=MR.idrekanan
					LEFT JOIN mbed ON mbed.id=MR.idbed
					LEFT JOIN mkelas ON mkelas.id=MR.idkelas
					LEFT JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mdokter MDR ON MDR.id=MR.iddokterpenanggungjawab
					LEFT JOIN merm_referensi R ON R.nilai=H.profesi_id AND R.ref_head_id='21'
					LEFT JOIN merm_referensi RJ ON RJ.nilai=H.jenis_laporan AND RJ.ref_head_id='298'
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					LEFT JOIN mppa ppa_verif ON ppa_verif.id=H.verifikasi_ppa
					LEFT JOIN mppa ppa_kerjakan ON ppa_kerjakan.id=H.ppa_pelaksana
					LEFT JOIN mppa PL ON PL.id=H.dilaporkan_ppa
					LEFT JOIN mppa PB ON PB.id=H.ppa_pemberi
					LEFT JOIN (
						".$sql_alergi."
					) A ON A.idpasien=H.idpasien
					WHERE H.idpasien='149' AND H.status_assemen='2' AND H.tipe_input != '3'
					AND ((H.tipe_input='1' AND H.ppa_pemberi='$mppa_id') OR (H.tipe_input='2' AND H.dilaporkan_ppa='$mppa_id'))
					".$where."
					GROUP BY H.assesmen_id
					ORDER BY H.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();
		$def_image='';
		if ($r->jenis_kelamin=='1'){
			$def_image='def_1.png';
		}else{
			$def_image='def_2.png';
			
		}
		// tpendaftaran_poli_ttv/tindakan/74329/erm_rj/input_ttv
		// $btn_1='';
		// $btn_1 .='<table class="block-table text-left">
						// <tbody>
							// <tr>
								
								// <td class="bg-white" style="width: 100%;">
									// <div class="push-5-t"><a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->assesmen_id.'/erm_rj/input_ttv" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> LAYANI</a> </div>
									// <div class="push-5-t"><button type="button" data-toggle="tooltip" title="Call"  class="btn btn-block btn-success btn-xs"><i class="fa fa-save"></i> FINAL TRANSAKSI</button> </div>
								// </td>
							// </tr>
						// </tbody>
					// </table>';
			// // if ($r->status=='0'){
			// // $btn_1 ='
				// // <table class="block-table text-left">
					// // <tbody>
						// // <tr>
							
							// // <td class="bg-white" style="width: 100%;">
								// // <div class="push-5-t"><button type="button" data-toggle="tooltip" title="batalkan"  class="btn btn-block btn-danger btn-xs "><i class="fa fa-times"></i> DIBATALKAN</button> </div>
								// // <div class="text-center push-5-t"><strong>
									// // '.get_nama_user($r->deleted_by).'<br>'.$r->deleted_date.'</strong>
								// // </div>
							// // </td>
						// // </tr>
					// // </tbody>
				// // </table>';
			// // }				
		
		// $result[] = $btn_1;
		if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tkomunikasi/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_komunikasi/input_komunikasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';

		}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tkomunikasi/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_komunikasi/input_komunikasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';

		}
		
		$btn_2='';
		$btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class=" text-muted  push-5-t"> '.($r->jenis_kelamin=='1'?'LAKI-LAKI':'PEREMPUAN').', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Th '.$r->umurbulan.' Bln '.$r->umurhari.' Hr </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
										<div class="btn-group" role="group">
											<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
											<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
										</div>
									</div>

									<br>

								</td>
							</tr>
						</tbody>
					</table>';
		$result[] = $btn_2;
		$btn_verifikasi='';
		$btn_pelaksanaan='';
		
		$status_verifikasi='';
		$ket_verifikasi='';
		$status_dikerjakan=($r->st_pelaksanaan=='1'?'<button class="btn btn-primary  btn-xs"  type="button" title="Lihat" onclick="lihat_pelaksanaan('.$r->assesmen_id.')" type="button">TELAH DILAKSANAKAN</button>':text_default('BELUM DILAKSANAKAN'));
		
		if ($r->st_verifikasi=='1'){
			$btn_edit='';
			$btn_hapus='';
			$status_verifikasi='&nbsp;&nbsp;&nbsp;'.text_success('SUDAH DIVERIFIKASI');
			$ket_verifikasi .='| &nbsp;&nbsp;<span class="text-danger"> Verifikasi At '.HumanDateLong($r->verifikasi_date).'</span>';
		}else{
			$status_verifikasi='&nbsp;&nbsp;'.text_danger('MENUNGGU DIVERIFIKASI');
			$btn_verifikasi='<button class="btn btn-primary  btn-xs"  type="button" title="Verifikasi" onclick="verifikasi_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-check"></i> Verifikasi</button>';
			if ($r->tipe_input=='1'){
				if ($r->ppa_pemberi != $login_ppa_id){//Jika Create yang bersangkutan
					$btn_verifikasi='';
				}
			}
			if ($r->tipe_input=='2'){
				if ($r->dilaporkan_ppa != $login_ppa_id){//Jika Create yang bersangkutan
					$btn_verifikasi='';
				}
			}
			
				
			
		}
		// $btn_verifikasi='tidak';
		$btn_5='';
		if ($r->tipe_input=='1'){
			$info_baris2='
				Pemberi Perintah : '.$r->pemberi.' | '.tanggal_indo_DMY($r->tanggal_laporan).' at '.HumanTimeShort($r->tanggal_laporan).' '.$ket_verifikasi;
		}
		if ($r->tipe_input=='2'){
			$info_baris2='
				Dilaporkan Kepada : '.$r->yang_dilaporkan.' | '.tanggal_indo_DMY($r->tanggal_laporan).' at '.HumanTimeShort($r->tanggal_laporan).' '.$ket_verifikasi;
		}
		if ($r->tipe_input=='1'){
			
		$btn_5='<table class="block-table text-left">
						<tbody>
							<tr>
								<td colspan="4">
									<div class="h5"><strong>'.strtoupper(tipe_komunikasi_sbar($r->tipe_input,$r->nama_jenis)).$status_verifikasi.' '.$status_dikerjakan.'</strong>&nbsp;&nbsp;&nbsp;'.$btn_lihat.$btn_verifikasi.'</div>
									<div class="push-5-t"><strong>'.$info_baris2.'</strong></div>
								</td>
								
							</tr>
							<tr>
								<td width="25%"><div class="h5 text-primary">SITUATION</div><div class="push-5-t">'.$r->situation.'</div></td>
								<td width="25%"><div class="h5 text-primary">BACKGROUND</div><div class="push-5-t">'.$r->background.'</div></td>
								<td width="25%"><div class="h5 text-primary">ASSESMENT</div><div class="push-5-t">'.$r->assemen.'</div></td>
								<td width="25%"><div class="h5 text-primary">RECOMENDATION</div><div class="push-5-t">'.$r->recomendation.'</div></td>
								
							</tr>
							
						</tbody>
					</table>';
		}else{
			$btn_5='<table class="block-table text-left">
						<tbody>
							<tr>
								<td>
									<div class="h5"><strong>'.strtoupper(tipe_komunikasi_sbar($r->tipe_input,$r->nama_jenis)).$status_verifikasi.' '.$status_dikerjakan.'</strong>&nbsp;&nbsp;&nbsp;'.$btn_lihat.$btn_verifikasi.'</div>
									
									<div class="push-5-t"><strong>'.$info_baris2.' '.$r->dilaporkan_ppa.' - '.$login_ppa_id.'</strong></div>
								</td>
								
							</tr>
							<tr>
								<td width="100%"><div class="h5 text-primary">LAPORAN NILAI KRITIS</div><div class="push-5-t">'.$r->nilai_kritis.'</div></td>
								
							</tr>
							
						</tbody>
					</table>';
		}
		
		$result[] = $btn_5;
		$btn_3='';
		if ($r->st_ranap=='0'){
			$btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe_poli).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->idkelompokpasien=='1'?'ASURANSI : '.$r->nama_rekanan:$r->nama_kel_pasien).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
		}else{
			$btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->idkelompokpasien=='1'?'ASURANSI : '.$r->nama_rekanan:$r->nama_kel_pasien).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
		}
		
		$result[] = $btn_3;
		// $btn_4='';
		// $btn_4 .='<table class="block-table text-center">
						// <tbody>
							// <tr>
								
								// <td class="bg-white" style="width: 100%;">
									// <div class="h5 text-primary"> '.($r->status_input_ttv?text_success('TELAH DIPERIKSA'):text_warning('BELUM DIPERIKSA')).'</div>
									// <div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								// </td>
							// </tr>
						// </tbody>
					// </table>';
		// $result[] =$btn_4;
		

		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
	
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_satuan_ttv=array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		$data_login_ppa=get_ppa_login();
		// print_r($data_login_ppa);exit;
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tpendaftaran_poli_ttv_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['st_ranap'] 			= $st_ranap;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['assesmen_id'] 			= '';
		$data['status_assesmen'] 			= '1';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Pemeriksaan Komunkasi Efektif';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan Poliklinik ",'#'),
											  array("Pemeriksaan Komunkasi Efektif",'tpendaftaran_poli_ttv')
											);
		
		
		//SBAR
		if ($menu_kiri=='input_komunikasi' || $menu_kiri=='his_komunikasi'){
			$data['template_assesmen_id']='';
			if ($trx_id!='#'){
				if ($menu_kiri=='his_komunikasi'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_komunikasi_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_komunikasi_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_komunikasi_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_komunikasi($pendaftaran_id,$st_ranap);
			// print_r($data_assemen);exit;
				
			}
			if ($data_assemen){
				if ($data_assemen['tipe_input']=='1'){
					$data_assemen['tipe_input_nama']=' (S B A R)';
				}
				if ($data_assemen['tipe_input']=='2'){
					$data_assemen['tipe_input_nama']=' (HASIL KRITIS)';
				}
				if ($data_assemen['tipe_input']=='3'){
					$data_assemen['tipe_input_nama']=' (NOTES)';
				}
				
			}else{
				$data_assemen['tipe_input']='0';
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['tipe_input_nama']='';
				$data_assemen['profesi_id']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_komunikasi'){
				
				
				
			}else{
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_komunikasi();
			$data_assemen['list_template_assement']=$list_template_assement;
			
			$setting_komunikasi=$this->Tpendaftaran_poli_ttv_model->setting_komunikasi();
			$setting_komunikasi_field=$this->Tpendaftaran_poli_ttv_model->setting_komunikasi_field($data_login_ppa['login_profesi_id']);
			// print_r($setting_komunikasi_field);exit;
			$jml_verif=0;
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="SELECT COUNT(H.assesmen_id) as jml_verif FROM tpoliklinik_komunikasi H
					WHERE H.ppa_pemberi='$login_ppa_id' AND H.st_verifikasi='0' AND H.status_assemen='2' AND H.tipe_input='1'";
				$jml_verif=$this->db->query($q)->row('jml_verif');
				$q="SELECT COUNT(H.assesmen_id) as jml_verif FROM tpoliklinik_komunikasi H
					WHERE H.dilaporkan_ppa='$login_ppa_id' AND H.st_verifikasi='0' AND H.status_assemen='2' AND H.tipe_input='2'";
				$jml_verif_kritis=$this->db->query($q)->row('jml_verif');
			$data['jml_verif']=$jml_verif;
			$data['jml_verif_kritis']=$jml_verif_kritis;
			$data = array_merge($data,$data_assemen,$setting_komunikasi,$setting_komunikasi_field);
			$data['pendaftaran_id']=$pendaftaran_id;
		}

		$data['trx_id']=$trx_id;
		// print_r($data);exit;
		$data = array_merge($data, backend_info());
			// print_r($list_template_assement);exit;
		$this->parser->parse('module_template', $data);
		
	}
	//SBAR
	function batal_template_komunikasi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_komunikasi',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function create_komunikasi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $tipe_input=$this->input->post('tipe_input');
	  $st_ranap=$this->input->post('st_ranap');
		
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'profesi_id' => $login_profesi_id,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'tipe_input' => $tipe_input,
		'st_ranap' => $st_ranap,
		'created_ppa' => $login_ppa_id,
		'tanggal_laporan' => date('Y-m-d H:i:s'),
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  if ($tipe_input=='1'){
		  $data['ppa_menerima']=$login_ppa_id;
		  $data['ppa_pemberi']='';
	  }
	  $hasil=$this->db->insert('tpoliklinik_komunikasi',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	function lihat_pelaksanaan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $assesmen_id=$this->input->post('assesmen_id');
		
	  $q="
		SELECT DATE_FORMAT(H.waktu_pelaksanaan,'%d-%m-%Y %h:%i')as waktu_pelaksanaan,H.ket_pelaksanaan,mppa.nama FROM `tpoliklinik_komunikasi` H
		LEFT JOIN mppa ON mppa.id=H.ppa_pelaksana
		WHERE H.assesmen_id='$assesmen_id'
	  ";
	  $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_komunikasi(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$tanggal_laporan= YMDFormat($this->input->post('tanggallaporan')). ' ' .$this->input->post('waktulaporan');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$tulbakon=$this->input->post('tulbakon');
		if ($tulbakon){
			$tulbakon=implode(",", $tulbakon);
		// print_r($tulbakon);
			
		}else{
			$tulbakon='';
		}
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'tanggal_laporan'=> $tanggal_laporan,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			'situation' => $this->input->post('situation'),
			'background' => $this->input->post('background'),
			'assemen' => $this->input->post('assemen'),
			'recomendation' => $this->input->post('recomendation'),
			'nilai_kritis' => $this->input->post('nilai_kritis'),
			'jenis_laporan' => $this->input->post('jenis_laporan'),
			'dilaporkan_ppa' => $this->input->post('dilaporkan_ppa'),
			'ppa_pemberi' => $this->input->post('ppa_pemberi'),
			'notes' => $this->input->post('notes'),
			'tulbakon' => $tulbakon,
		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			if ($login_pegawai_id=='1'){//Jika Dokter
				if ($login_pegawai_id==$dokter_pjb){
					$data['verifikasi_ppa']=$login_ppa_id;
					$data['st_verifikasi']=1;
					$data['verifikasi_date']=date('Y-m-d H:i:s');
				}
			}
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_komunikasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function list_history_pengkajian_komunikasi()
    {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_komunikasi($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$profesi_id=$this->input->post('profesi_id');
		$st_owned=$this->input->post('st_owned');
		$mppa_id=$this->input->post('mppa_id');
		$st_verifikasi=$this->input->post('st_verifikasi');
		$st_pelaksanaan=$this->input->post('st_pelaksanaan');
		// $iddokter=$this->input->post('iddokter');
		// $idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$jenis_inputan=$this->input->post('jenis_inputan');
		
		$where='';
		if ($st_owned!='#'){
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			
		}			
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($profesi_id){
			$profesi_id=implode(", ", $profesi_id);
			$where .=" AND (H.profesi_id) IN (".$profesi_id.")";
		}
		if ($jenis_inputan){
			$jenis_inputan=implode(", ", $jenis_inputan);
			$where .=" AND (H.tipe_input) IN (".$jenis_inputan.")";
		}
		if ($mppa_id){
			$mppa_id=implode(", ", $mppa_id);
			$where .=" AND (H.created_ppa) IN (".$mppa_id.")";
		}
		// if ($iddokter!='#'){
			// $where .=" AND MP.iddokter = '".$iddokter."'";
		// }
		// if ($idpoli!='#'){
			// $where .=" AND MP.idpoliklinik = '".$idpoli."'";
		// }
		if ($st_verifikasi!='#'){
			$where .=" AND H.st_verifikasi = '".$st_verifikasi."' AND H.tipe_input !='3'";
		}
		if ($st_pelaksanaan!='#'){
			$where .=" AND H.st_pelaksanaan = '".$st_pelaksanaan."' AND H.tipe_input='1'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_komunikasi")->row_array();
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,ppa_verif.nama as nama_verif,ppa_kerjakan.nama as pelaksana
					,PL.nama as yang_dilaporkan,PB.nama as pemberi
					,CASE WHEN H.st_ranap='1' THEN MR.tanggaldaftar ELSE MP.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,mppa.nama as nama_mppa,RJ.ref as nama_jenis 
					,H.*
					FROM `tpoliklinik_komunikasi` H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
					INNER JOIN merm_referensi R ON R.nilai=H.profesi_id AND R.ref_head_id='21'
					LEFT JOIN merm_referensi RJ ON RJ.nilai=H.jenis_laporan AND RJ.ref_head_id='298'
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					LEFT JOIN mppa ppa_verif ON ppa_verif.id=H.verifikasi_ppa
					LEFT JOIN mppa ppa_kerjakan ON ppa_kerjakan.id=H.ppa_pelaksana
					LEFT JOIN mppa PL ON PL.id=H.dilaporkan_ppa
					LEFT JOIN mppa PB ON PB.id=H.ppa_pemberi

					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','singkatan_kajian');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tkomunikasi/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_komunikasi/input_komunikasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tkomunikasi/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_komunikasi/input_komunikasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			$btn_ttd='';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_komunikasi']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_komunikasi']=='0'){
			  $btn_hapus='';
			}
			if ($r->profesi_id!=$login_profesi_id){
				$btn_edit='';
				$btn_hapus='';
				$btn_duplikasi='';
			}
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			$btn_pelaksanaan='';
			
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_komunikasi/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i> '.$r->keterangan_edit.'</a>';
			}
			$status_verifikasi='';
			$status_dikerjakan=($r->st_pelaksanaan=='1'?'<button class="btn btn-primary  btn-xs"  type="button" title="Lihat" onclick="lihat_pelaksanaan('.$r->assesmen_id.')" type="button">TELAH DILAKSANAKAN</button>':text_default('BELUM DILAKSANAKAN'));
			
			if ($r->st_verifikasi){
				$btn_edit='';
				$btn_hapus='';
				$status_verifikasi='&nbsp;&nbsp;&nbsp;'.text_success('SUDAH DIVERIFIKASI');
				// $status_verifikasi .='&nbsp;&nbsp;'.text_default(HumanDateLong($r->verifikasi_date).' ('.$r->nama_verif.')');
			}else{
				$status_verifikasi='&nbsp;&nbsp;'.text_danger('MENUNGGU DIVERIFIKASI');
				$btn_verifikasi='<button class="btn btn-primary  btn-xs" '.$btn_disabel_edit.' type="button" title="Verifikasi" onclick="verifikasi_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-check"></i> Verifikasi</button>';
				if ($r->tipe_input=='1'){
					if ($r->ppa_pemberi!=$login_ppa_id){//Jika Create yang bersangkutan
						$btn_verifikasi=$r->ppa_pemberi.'-'.$login_ppa_id;
					}
				}
				if ($r->tipe_input=='2'){
					if ($r->dilaporkan_ppa!=$login_ppa_id){//Jika Create yang bersangkutan
						$btn_verifikasi='';
					}
				}
				
					
				
			}
			if ($r->tipe_input=='3'){
				$btn_verifikasi='';
				$status_verifikasi='';
				$status_dikerjakan='';
			}
			if ($r->tipe_input != '1'){
				$status_dikerjakan='';
			}
			if ($r->st_pelaksanaan=='0'){
				if ($r->tipe_input=='1'){
					$btn_pelaksanaan='<button class="btn btn-warning  btn-xs" '.$btn_disabel_edit.' type="button" title="Pelaksanaan" onclick="laksanakan_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-hand-grab-o"></i> Pelaksanaan</button>';
					// if ($r->ppa_pemberi!=$login_pegawai_id){//Jika Create yang bersangkutan
						// $btn_pelaksanaan='';
					// }
				}
				
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_hapus;	
			if ($logic_akses_assesmen['st_lihat_komunikasi']=='1'){
			$aksi .= $btn_lihat;	
			}
			$aksi .= $aksi_edit;	
			
			$aksi .= $btn_verifikasi;	
			$aksi .= $btn_pelaksanaan;	
			$aksi .= '</div>';
			$jml_kolom=0;
			$info_baris2='';
			$isi_tabel='';
			if ($r->tipe_input=='1'){
				$info_baris2='
					Pemberi Perintah : '.$r->pemberi.' | '.tanggal_indo_DMY($r->tanggal_laporan).' at '.HumanTimeShort($r->tanggal_laporan).' | ';
			}
			if ($r->tipe_input=='2'){
				$info_baris2='
					Dilaporkan Kepada : '.$r->yang_dilaporkan.' | '.tanggal_indo_DMY($r->tanggal_laporan).' at '.HumanTimeShort($r->tanggal_laporan).' | 
				';
			}
			if ($r->tipe_input=='3'){
				$jml_kolom='2';
				$isi_tabel='
					<td width="80%">
						<div class="h5 text-primary">NOTES</div>
						<div class="push-5-t">'.$r->notes.'</div>
					</td>
					<td width="20%">
						
					</td>
					
				';
			}
			if ($r->tipe_input=='2'){
				$jml_kolom='2';
				$isi_tabel='
					<td width="80%">
						<div class="h5 text-primary">LAPORAN NILAI KRITIS</div>
						<div class="push-5-t">'.$r->nilai_kritis.'</div>
					</td>
					<td width="20%">
						<div class="push-5-t text-center"><strong>YANG MELAPORKAN</strong><br>'.$r->nama_mppa.'<br>'.HumanDateLong($r->tanggal_input).'</div>
						<div class="push-5-t text-center"><strong>'.($r->st_verifikasi=='1'?'VERIFIKASI':'').'</strong><br>'.($r->st_verifikasi=='1'?$r->nama_verif:'').'<br>'.($r->st_verifikasi=='1'?HumanDateLong($r->verifikasi_date):'').'</div>
					</td>
					
				';
			}
			if ($r->tipe_input=='1'){
				$jml_kolom='4';
				$isi_tabel='
					<td width="20%"><div class="h5 text-primary">SITUATION</div><div class="push-5-t">'.$r->situation.'</div></td>
					<td width="20%"><div class="h5 text-primary">BACKGROUND</div><div class="push-5-t">'.$r->background.'</div></td>
					<td width="20%"><div class="h5 text-primary">ASSESMENT</div><div class="push-5-t">'.$r->assemen.'</div></td>
					<td width="20%"><div class="h5 text-primary">RECOMENDATION</div><div class="push-5-t">'.$r->recomendation.'</div></td>
					<td width="20%">
						<div class="push-5-t text-center"><strong>'.($r->st_verifikasi=='1'?'VERIFIKASI PEMBERI PERINTAH':'').'</strong><br>'.($r->st_verifikasi=='1'?$r->nama_verif:'').'<br>'.($r->st_verifikasi=='1'?HumanDateLong($r->verifikasi_date):'').'</div>
						<div class="push-5-t text-center"><strong>'.($r->st_pelaksanaan=='1'?'PELAKSANA':'').'</strong><br>'.($r->st_pelaksanaan=='1'?$r->pelaksana:'').'<br>'.($r->st_pelaksanaan=='1'?HumanDateShort($r->waktu_pelaksanaan):'').'</div>
					</td>
				';
				$tabel='<table class="table table-condensed" style="border-spacing:0;border-collapse: collapse!important;">
						<tbody>
							<tr>
								<td colspan="'.$jml_kolom.'" class="bg-white" style="width: 80%;">
									<div class="h5"><strong>'.strtoupper(tipe_komunikasi_sbar($r->tipe_input,$r->nama_jenis)).$status_verifikasi.' '.$status_dikerjakan.'</strong>&nbsp;&nbsp;&nbsp;'.$aksi.'</div>
									<div class="push-5-t">'.$info_baris2.$r->nopendaftaran.' | Add On '.tanggal_indo_DMY($r->tanggal_input).' at '.HumanTimeShort($r->tanggal_input).' Oleh '.$r->nama_mppa.'</div>
								</td>
								<td style="width: 20%;">
									<div class="push-5-t text-center"><strong>YANG MELAPORKAN</strong><br>'.$r->nama_mppa.'<br>'.HumanDateLong($r->tanggal_input).'</div>
								</td>
							</tr>
							<tr>
								'.$isi_tabel.'				
							</tr>
						</tbody>
					</table>';
			}else{
				$tabel='<table class="table table-condensed" style="border-spacing:0;border-collapse: collapse!important;">
					<tbody>
						<tr>
							<td colspan="'.$jml_kolom.'" class="bg-white" style="width: 100%;">
								<div class="h5"><strong>'.strtoupper(tipe_komunikasi_sbar($r->tipe_input,$r->nama_jenis)).$status_verifikasi.' '.$status_dikerjakan.'</strong>&nbsp;&nbsp;&nbsp;'.$aksi.'</div>
								<div class="push-5-t">'.$info_baris2.$r->nopendaftaran.' | Add On '.tanggal_indo_DMY($r->tanggal_input).' at '.HumanTimeShort($r->tanggal_input).' Oleh '.$r->nama_mppa.'</div>
							</td>
						</tr>
						<tr>
							'.$isi_tabel.'				
						</tr>
					</tbody>
				</table>';
			}
			
			
			 $result[] =$tabel;
			$data[] = $result;
			
		  }
		  $q="SELECT COUNT(H.assesmen_id) as jml_verif FROM tpoliklinik_komunikasi H
				WHERE H.ppa_pemberi='$login_ppa_id' AND H.st_verifikasi='0' AND H.status_assemen='2' AND H.tipe_input='1' AND H.idpasien='$idpasien'";
			$jml_verif=$this->db->query($q)->row('jml_verif');
			$q="SELECT COUNT(H.assesmen_id) as jml_verif FROM tpoliklinik_komunikasi H
				WHERE H.dilaporkan_ppa='$login_ppa_id' AND H.st_verifikasi='0' AND H.status_assemen='2' AND H.tipe_input='2' AND H.idpasien='$idpasien'";
			$jml_verif_kritis=$this->db->query($q)->row('jml_verif');
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data,
			  "data_jml_verif_sbar" => $jml_verif,
			  "data_jml_verif_kritis" => $jml_verif_kritis,
		  );
		  echo json_encode($output);
		}
		function verifikasi_komunikasi_all(){
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$idpasien=$this->input->post('idpasien');
			$q="SELECT H.assesmen_id FROM tpoliklinik_komunikasi H
					WHERE H.ppa_pemberi='$login_ppa_id' AND H.st_verifikasi='0' AND H.status_assemen='2' AND H.idpasien='$idpasien' AND H.tipe_input='1'";
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$data=array(
					'st_verifikasi' => 1,
					'verifikasi_ppa' => $login_ppa_id,
					'verifikasi_date' =>date('Y-m-d H:i:s'),

				);
				$this->db->where('assesmen_id',$r->assesmen_id);
				$result=$this->db->update('tpoliklinik_komunikasi',$data);
			}


			// $ttv_id=$this->db->insert_id();

			if ($result){
			$hasil=$data;
			}else{
			$hasil=false;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function verifikasi_komunikasi_all_kritis(){
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$idpasien=$this->input->post('idpasien');
			$q="SELECT H.assesmen_id FROM tpoliklinik_komunikasi H
					WHERE H.dilaporkan_ppa='$login_ppa_id' AND H.st_verifikasi='0' AND H.status_assemen='2' AND H.idpasien='$idpasien' AND H.tipe_input='2'";
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$data=array(
					'st_verifikasi' => 1,
					'verifikasi_ppa' => $login_ppa_id,
					'verifikasi_date' =>date('Y-m-d H:i:s'),

				);
				$this->db->where('assesmen_id',$r->assesmen_id);
				$result=$this->db->update('tpoliklinik_komunikasi',$data);
			}


			// $ttv_id=$this->db->insert_id();

			if ($result){
			$hasil=$data;
			}else{
			$hasil=false;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function verifikasi_komunikasi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data=array(
			'st_verifikasi' => 1,
			'verifikasi_ppa' => $login_ppa_id,
			'verifikasi_date' =>date('Y-m-d H:i:s'),

		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_komunikasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
		function save_edit_komunikasi(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_edit_id=$this->input->post('alasan_id');
			$keterangan_edit=$this->input->post('keterangan_edit');
			$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_komunikasi WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			
				
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_komunikasi',$data);
			$data=$this->db->query("SELECT * FROM tpoliklinik_komunikasi WHERE assesmen_id='$assesmen_id'")->row_array();
		
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		
			$this->output->set_output(json_encode($hasil));
		}
		// function save_edit_komunikasi(){
			// $assesmen_id=$this->input->post('assesmen_id');
			// $alasan_edit_id=$this->input->post('alasan_id');
			// $keterangan_edit=$this->input->post('keterangan_edit');
			// $jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_komunikasi WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// // $jml_edit=$this->input->post('jml_edit');
			// $res=$this->simpan_history_komunikasi($assesmen_id,$jml_edit);
			// // print_r($res);exit;
			// if ($res){
				
				// $data=array(
					// 'status_assemen' => 1,
					// 'st_edited' => 1,
					// 'alasan_edit_id' =>$alasan_edit_id,
					// 'keterangan_edit' =>$keterangan_edit,

				// );
				// $login_ppa_id=$this->session->userdata('login_ppa_id');
				// $data['edited_ppa']=$login_ppa_id;
				// $data['edited_date']=date('Y-m-d H:i:s');
				// $this->db->where('assesmen_id',$assesmen_id);
				// $result=$this->db->update('tpoliklinik_komunikasi',$data);
				// $data=$this->db->query("SELECT * FROM tpoliklinik_komunikasi WHERE assesmen_id='$assesmen_id'")->row_array();
			
				// if ($result){
					// $hasil=$data;
				// }else{
					// $hasil=false;
				// }
			// }else{
				// $hasil=$res;
			// }
			// $this->output->set_output(json_encode($hasil));
		// }
		function simpan_history_komunikasi($assesmen_id,$jml_edit){
			$hasil=false;
			// $jml_edit=$jml_edit+1;
			$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_komunikasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
			// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
			if ($baris==0){
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="INSERT IGNORE INTO tpoliklinik_komunikasi_his 
							(versi_edit,
							assesmen_id,template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date)
					SELECT '$jml_edit',
					'$assesmen_id',template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date 
					FROM tpoliklinik_komunikasi 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
			  
			}
		  return $hasil;
		}
		
		function batal_komunikasi(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $st_edited=$this->input->post('st_edited');
		  $jml_edit=$this->input->post('jml_edit');
		  // $data=array(
			// 'deleted_ppa' => $login_ppa_id,
			// 'deleted_date' => date('Y-m-d H:i:s'),
			
		  // );
		   if ($jml_edit=='0'){
			  $data['edited_ppa']=null;
			  $data['edited_date']=null;
			  $data['alasan_edit_id']='';
			  $data['keterangan_edit']='';
			  
		  }
		  if ($st_edited=='1'){
			  $data['status_assemen']='2';
		  }else{
			  $data['status_assemen']='0';
			  
		  }
		  $this->db->where('assesmen_id',$assesmen_id);
		  $hasil=$this->db->update('tpoliklinik_komunikasi',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
	  // function batal_komunikasi(){
		  // $login_ppa_id=$this->session->userdata('login_ppa_id');
		  // $pendaftaran_id=$this->input->post('pendaftaran_id');
		  // $assesmen_id=$this->input->post('assesmen_id');
		  // $st_edited=$this->input->post('st_edited');
		  // $jml_edit=$this->input->post('jml_edit');
		  // $data=array(
			// 'deleted_ppa' => $login_ppa_id,
			// 'deleted_date' => date('Y-m-d H:i:s'),
			
		  // );
		   // if ($jml_edit=='0'){
			  // $data['edited_ppa']=null;
			  // $data['edited_date']=null;
			  // $data['alasan_edit_id']='';
			  // $data['keterangan_edit']='';
			  
		  // }
		  // if ($st_edited=='1'){
				// $q="SELECT * FROM tpoliklinik_komunikasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				// $data_his_edit=$this->db->query($q)->row();
				// if ($data_his_edit){
					// $data['edited_ppa']=$data_his_edit->edited_ppa;
					// $data['edited_date']=$data_his_edit->edited_date;
					// $data['alasan_edit_id']=$data_his_edit->alasan_edit_id;
					// $data['keterangan_edit']=$data_his_edit->keterangan_edit;
					// $data['tanggal_input']=$data_his_edit->tanggal_input;
					// $data['subjectif']=$data_his_edit->subjectif;
					// $data['objectif']=$data_his_edit->objectif;
					// $data['assemen']=$data_his_edit->assemen;
					// $data['planing']=$data_his_edit->planing;
					// $data['intruksi']=$data_his_edit->intruksi;
					// $data['intervensi']=$data_his_edit->intervensi;
					// $data['evaluasi']=$data_his_edit->evaluasi;
					// $data['reassesmen']=$data_his_edit->reassesmen;

				// }
				
				
				// $q="DELETE FROM tpoliklinik_komunikasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				// $this->db->query($q);
				
				
			  // $data['status_assemen']='2';
		  // }else{
			  // $data['status_assemen']='0';
			  
		  // }
		  // $this->db->where('assesmen_id',$assesmen_id);
		  // $hasil=$this->db->update('tpoliklinik_komunikasi',$data);
		  // $this->output->set_output(json_encode($hasil));
	  // }
  function create_with_template_komunikasi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_komunikasi (
			template_assesmen_id,nama_template,
			st_ranap,pendaftaran_id,pendaftaran_id_ranap,idpasien,profesi_id,tanggal_input,tanggal_laporan
			,status_assemen,tipe_input,situation,background,assemen,recomendation,tulbakon,ppa_menerima,ppa_pemberi
			,st_verifikasi,verifikasi_ppa,verifikasi_date,st_pelaksanaan,waktu_pelaksanaan,ket_pelaksanaan,ppa_pelaksana
			,dilaporkan_ppa,jenis_laporan,nilai_kritis,notes
			)
			SELECT '$template_assesmen_id',nama_template,
			'$st_ranap','$pendaftaran_id','$pendaftaran_id_ranap','$idpasien',profesi_id,NOW(),NOW()
			,1,tipe_input,situation,background,assemen,recomendation,tulbakon,ppa_menerima,ppa_pemberi
			,0,verifikasi_ppa,null,0,null,'',null
			,dilaporkan_ppa,jenis_laporan,nilai_kritis,notes
			FROM `tpoliklinik_komunikasi`

			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	 
	  $q="UPDATE tpoliklinik_komunikasi 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
   }
   function hapus_record_komunikasi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_komunikasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_pelaksanaan(){
		$assesmen_id=$this->input->post('assesmen_id');
		$ket_pelaksanaan=$this->input->post('ket_pelaksanaan');
		$waktu_pelaksanaan= YMDFormat($this->input->post('waktu_pelaksanaan')). ' ' .$this->input->post('time_pelaksanaan');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data=array(
			'st_pelaksanaan' => 1,
			'ppa_pelaksana' => $login_ppa_id,
			'ket_pelaksanaan' => $ket_pelaksanaan,
			'waktu_pelaksanaan' =>($waktu_pelaksanaan),

		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_komunikasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_index_template_komunikasi()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,M.nama as nama_mppa,RJ.ref as jenis
						 FROM tpoliklinik_komunikasi H 
						 LEFT JOIN mppa M ON M.id=H.created_ppa
						 LEFT JOIN merm_referensi RJ ON RJ.nilai=H.jenis_laporan AND RJ.ref_head_id='298'
						 WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.assesmen_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		// $result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->assesmen_id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		
		$result[] =tipe_komunikasi_sbar($r->tipe_input,$r->jenis);
		$result[] =$r->nama_template;
		$result[] =$r->jumlah_template;
		$result[] =$aksi;
			$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }	
  function create_template_komunikasi(){
	   $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $tipe_input=$this->input->post('tipe_input');
	  $st_ranap=$this->input->post('st_ranap');
		
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'profesi_id' => $login_profesi_id,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'tipe_input' => $tipe_input,
		'st_ranap' => $st_ranap,
		'created_ppa' => $login_ppa_id,
		'tanggal_laporan' => date('Y-m-d H:i:s'),
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  if ($tipe_input=='1'){
		  $data['ppa_menerima']=$login_ppa_id;
		  $data['ppa_pemberi']=$login_ppa_id;
	  }
	  $hasil=$this->db->insert('tpoliklinik_komunikasi',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	function edit_template_komunikasi(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_komunikasi',$data);
		
		$this->output->set_output(json_encode($result));
  }

}	
