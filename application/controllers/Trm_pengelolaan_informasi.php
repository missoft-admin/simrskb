<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pengelolaan_informasi extends CI_Controller
{

    /**
     * Pengelolaan & Pengambilan Informasi Medis controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_pengelolaan_informasi_model', 'model');
    }

    public function index()
    {
        $data = array(
            'notransaksi' => '',
            'nama_pemohon' => '',
            'tanggal_dari' => '',
            'tanggal_sampai' => '',
            'jenis_pengajuan' => '',
            'nomedrec' => '',
            'nama_pasien' => '',
            'estimasi_dari' => '',
            'estimasi_sampai' => '',
            'status_pengajuan' => '',
        );

        $data['error'] = '';
        $data['title'] = 'Pengelolaan & Pengambilan Informasi Medis';
        $data['content'] = 'Trm_pengelolaan_informasi/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan & Pengambilan Informasi Medis",'#'),
            array("List",'trm_pengelolaan_informasi')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'notransaksi' => $this->input->post('notransaksi'),
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'tanggal_dari' => $this->input->post('tanggal_dari'),
            'tanggal_sampai' => $this->input->post('tanggal_sampai'),
            'jenis_pengajuan' => $this->input->post('jenis_pengajuan'),
            'nomedrec' => $this->input->post('nomedrec'),
            'nama_pasien' => $this->input->post('nama_pasien'),
            'estimasi_dari' => $this->input->post('estimasi_dari'),
            'estimasi_sampai' => $this->input->post('estimasi_sampai'),
            'status_pengajuan' => $this->input->post('status_pengajuan'),
        );

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Pengelolaan & Pengambilan Informasi Medis';
        $data['content'] = 'Trm_pengelolaan_informasi/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan & Pengambilan Informasi Medis",'#'),
            array("Filter",'trm_pengelolaan_informasi')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function view_data($id='')
    {
        if($id != ''){
			$row = $this->model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' => $row->id,
					'tanggal_penyerahan' => $row->tanggal,
                    'nama_penerima' => $row->nama_penerima,
                    'keterangan' => $row->keterangan,
                    'file' => $row->file,
				);

				$data['error'] = '';
                $data['title'] = "Pengelolaan & Pengambilan Informasi Medis";
                $data['content'] = 'Trm_pengelolaan_informasi/manage';
                $data['breadcrum'] = array(
                    array("RSKB Halmahera",'#'),
                    array("Pengelolaan & Pengambilan Informasi Medis",'#'),
                    array('Proses Pengambilan','Trm_pengelolaan_informasi')
                );

                $data['dataPengelolaan'] = $this->model->getDataKunjungan(array($id));

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('trm_pengelolaan_informasi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('trm_pengelolaan_informasi');
		}
    }

    public function proses_pengambilan($id='')
    {
        if ($id) {
            $_SESSION['data_pengajuan_informasi'] = array($id);
        }

        $data = array(
            'id' => '',
            'tanggal_penyerahan' => '',
            'nama_penerima' => '',
            'keterangan' => '',
            'file' => '',
            'status' => '',
            'idpengajuan' => ($id ? $id : ''),
        );

        $data['error'] = '';
        $data['title'] = "Pengelolaan & Pengambilan Informasi Medis";
        $data['content'] = 'Trm_pengelolaan_informasi/manage';
        $data['breadcrum'] = array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan & Pengambilan Informasi Medis",'#'),
            array('Proses Pengambilan','Trm_pengelolaan_informasi')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function proses_verifikasi($id='')
    {
        if ($id) {
            $idPengajuan = array($id);
        } else {
            $idPengajuan = $_SESSION['data_pengajuan_informasi'];
        }

        foreach ($idPengajuan as $id) {
            $this->db->set('status_verifikasi', '1');
            $this->db->set('verified_at', date("Y-m-d H:i:s"));
            $this->db->set('verified_by', $this->session->userdata('user_id'));
            $this->db->where('id', $id);
            $this->db->where('status_verifikasi', '0');
            $this->db->update('trm_pengajuan_informasi_detail');
        }

        $this->session->set_flashdata('confirm',true);
        $this->session->set_flashdata('message_flash','data telah diverifikasi.');
        redirect('trm_pengelolaan_informasi','location');
    }

    public function proses()
    {
        $array_id = $this->input->post('check_status');
        $proses_pengambilan = $this->input->post('proses_pengambilan');
        $proses_verifikasi = $this->input->post('proses_verifikasi');

        $_SESSION['data_pengajuan_informasi'] = $array_id;

        if ($proses_pengambilan) {
            redirect('trm_pengelolaan_informasi/proses_pengambilan','location');
        } else if ($proses_verifikasi) {
            $this->proses_verifikasi();
        }
    }

    public function save()
    {
        if($this->model->saveData()){
            $this->session->set_flashdata('confirm',true);
            $this->session->set_flashdata('message_flash','data telah disimpan.');
            redirect('trm_pengelolaan_informasi', 'location');
        }
    }

    public function getIndex($uri='index')
    {
        $this->select = array(
            'trm_pengajuan_informasi_detail.id',
            'trm_pengajuan_informasi.notransaksi AS no_pengajuan',
            'trm_pengajuan_informasi.tanggal AS tanggal_pengajuan',
            'trm_layanan_berkas.no_medrec',
            'trm_layanan_berkas.namapasien AS nama_pasien',
            'trm_layanan_berkas.tujuan AS jenis_kunjungan',
            'mdokter.nama AS nama_dokter',
            'mpengajuan_skd.nama AS jenis_pengajuan',
            'trm_pengajuan_informasi.nama_pemohon',
            'trm_pengajuan_informasi.keterangan',
            'trm_pengajuan_informasi_detail.estimasi_selesai',
            'trm_pengajuan_informasi_detail.status_verifikasi',
            'trm_pengajuan_informasi_detail.status_pengajuan',
        );

        $this->from = 'trm_pengajuan_informasi_detail';

        $this->join = array(
            array('trm_pengajuan_informasi', 'trm_pengajuan_informasi.id = trm_pengajuan_informasi_detail.idtransaksi', 'LEFT'),
            array('trm_layanan_berkas', 'trm_layanan_berkas.id = trm_pengajuan_informasi_detail.idberkas', 'LEFT'),
            array('mdokter', 'mdokter.id = trm_layanan_berkas.iddokter', 'LEFT'),
            array('mpengajuan_skd', 'mpengajuan_skd.id = trm_pengajuan_informasi_detail.jenis_pengajuan', 'LEFT'),
        );

        // FILTER
        $this->where  = array(
            'trm_pengajuan_informasi_detail.status = ' => '1',
        );

        if ($uri == 'filter') {
            if ($this->session->userdata('notransaksi') != null) {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.notransaksi' => $this->session->userdata('no_pengajuan')));
            }
            if ($this->session->userdata('nama_pemohon') != null) {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.nama_pemohon LIKE' => '%'.$this->session->userdata('nama_pemohon').'%'));
            }
            if ($this->session->userdata('tanggal_dari') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal) >=' => YMDFormat($this->session->userdata('tanggal_dari'))));
            }
            if ($this->session->userdata('tanggal_sampai') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal) <=' => YMDFormat($this->session->userdata('tanggal_sampai'))));
            }
            if ($this->session->userdata('jenis_pengajuan') != '#') {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi_detail.jenis_pengajuan' => YMDFormat($this->session->userdata('jenis_pengajuan'))));
            }
            if ($this->session->userdata('no_medrec') != null) {
                $this->where = array_merge($this->where, array('trm_layanan_berkas.no_medrec' => $this->session->userdata('no_medrec')));
            }
            if ($this->session->userdata('nama_pasien') != null) {
                $this->where = array_merge($this->where, array('trm_layanan_berkas.namapasien' => $this->session->userdata('nama_pasien')));
            }
            if ($this->session->userdata('estimasi_dari') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi_detail.estimasi_selesai) >=' => YMDFormat($this->session->userdata('estimasi_dari'))));
            }
            if ($this->session->userdata('estimasi_sampai') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi_detail.estimasi_selesai) <=' => YMDFormat($this->session->userdata('estimasi_sampai'))));
            }
            if ($this->session->userdata('status_pengajuan') != '#') {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi_detail.status' => $this->session->userdata('status_pengajuan')));
            }
        } else {
            $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal)' => date("Y-m-d")));
        }

        $this->order = array();

        $this->group = array();

        $this->column_search = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');
        $this->column_order = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');

        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $r) {
            $row = array();
            
            if ($r->status_verifikasi == 0) {
                $action = '<a href="'.site_url().'trm_pengelolaan_informasi/proses_verifikasi/'.$r->id.'" data-toggle="tooltip" title="Menyelesaikan Pengajuan" class="btn btn-primary btn-sm"><i class="fa fa-check-circle"></i></a>';
            } else if ($r->status_verifikasi == 1 && $r->status_pengajuan == 0) {
                $action = '<a href="'.site_url().'trm_pengelolaan_informasi/proses_pengambilan/'.$r->id.'" data-toggle="tooltip" title="Pengambilan Informasi" class="btn btn-danger btn-sm"><i class="fa fa-arrow-circle-right"></i></a>';
            } else if ($r->status_pengajuan == 1) {
                $action = '<a href="'.site_url().'trm_pengelolaan_informasi/view_data/'.$r->id.'" data-toggle="tooltip" title="Rincian Pengambilan Informasi" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
            }

            $row[] = '<label class="css-input css-checkbox css-checkbox-success"><input type="checkbox" class="checkboxTrx" name="check_status[]" value="'.$r->id.'"><span></span></label>';
            $row[] = $r->no_pengajuan;
            $row[] = DMYFormat($r->tanggal_pengajuan);
            $row[] = $r->no_medrec;
            $row[] = $r->nama_pasien;
            $row[] = GetJenisLayananBerkas($r->jenis_kunjungan);
            $row[] = $r->nama_dokter;
            $row[] = $r->jenis_pengajuan;
            $row[] = $r->nama_pemohon;
            $row[] = $r->keterangan;
            $row[] = DMYFormat($r->estimasi_selesai);
            $row[] = StatusPengajuanInformasi($r->status_pengajuan);
            $row[] = getStatusEstimasiPengajuanInformasi($r->tanggal_pengajuan, $r->estimasi_selesai);
            $row[] = '<div class="btn-group">
                ' . $action . '
                <a href="'.site_url().'trm_pengelolaan_informasi/print/'.$r->id.'" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
            </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        echo json_encode($output);
    }

    function getDataKunjungan()
    {
        $idPengajuan = $_SESSION['data_pengajuan_informasi'];

        if ($idPengajuan) {
            $response = $this->model->getDataKunjungan($idPengajuan);
        } else {
            $response = array();
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
    }

    function removeDataKunjungan($id)
    {
        $response = $this->model->removeDataKunjungan($id);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
    }
}
