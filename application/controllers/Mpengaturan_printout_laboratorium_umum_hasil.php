<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_printout_laboratorium_umum_hasil extends CI_Controller {

	/**
	 * Pengaturan Printout Permintaan Laboratorium Umum Hasil controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_printout_laboratorium_umum_hasil_model');
		$this->load->helper('path');
  }

	function index($tipe = 'FORMAT_1')
	{
		$row = $this->Mpengaturan_printout_laboratorium_umum_hasil_model->getSpecified($tipe);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'nama_file' => $row->nama_file,
				'logo' => $row->logo,
				'logo_header' => $row->logo_header,
				'logo_footer' => $row->logo_footer,
				'alamat' => $row->alamat,
				'phone' => $row->phone,
				'website' => $row->website,
				'label_header' => $row->label_header,
				'label_header_eng' => $row->label_header_eng,
				'tampilkan_noregister' => $row->tampilkan_noregister,
				'label_noregister' => $row->label_noregister,
				'label_noregister_eng' => $row->label_noregister_eng,
				'tampilkan_nomedrec' => $row->tampilkan_nomedrec,
				'label_nomedrec' => $row->label_nomedrec,
				'label_nomedrec_eng' => $row->label_nomedrec_eng,
				'tampilkan_nama' => $row->tampilkan_nama,
				'label_nama' => $row->label_nama,
				'label_nama_eng' => $row->label_nama_eng,
				'tampilkan_kelompok_pasien' => $row->tampilkan_kelompok_pasien,
				'label_kelompok_pasien' => $row->label_kelompok_pasien,
				'label_kelompok_pasien_eng' => $row->label_kelompok_pasien_eng,
				'tampilkan_nama_asuransi' => $row->tampilkan_nama_asuransi,
				'label_nama_asuransi' => $row->label_nama_asuransi,
				'label_nama_asuransi_eng' => $row->label_nama_asuransi_eng,
				'tampilkan_alamat' => $row->tampilkan_alamat,
				'label_alamat' => $row->label_alamat,
				'label_alamat_eng' => $row->label_alamat_eng,
				'tampilkan_dokter_perujuk' => $row->tampilkan_dokter_perujuk,
				'label_dokter_perujuk' => $row->label_dokter_perujuk,
				'label_dokter_perujuk_eng' => $row->label_dokter_perujuk_eng,
				'tampilkan_nomor_laboratorium' => $row->tampilkan_nomor_laboratorium,
				'label_nomor_laboratorium' => $row->label_nomor_laboratorium,
				'label_nomor_laboratorium_eng' => $row->label_nomor_laboratorium_eng,
				'tampilkan_tanggal_lahir' => $row->tampilkan_tanggal_lahir,
				'label_tanggal_lahir' => $row->label_tanggal_lahir,
				'label_tanggal_lahir_eng' => $row->label_tanggal_lahir_eng,
				'tampilkan_umur' => $row->tampilkan_umur,
				'label_umur' => $row->label_umur,
				'label_umur_eng' => $row->label_umur_eng,
				'tampilkan_rujukan' => $row->tampilkan_rujukan,
				'label_rujukan' => $row->label_rujukan,
				'label_rujukan_eng' => $row->label_rujukan_eng,
				'tampilkan_barcode' => $row->tampilkan_barcode,
				'label_barcode' => $row->label_barcode,
				'label_barcode_eng' => $row->label_barcode_eng,
				'tampilkan_nama_pemeriksaan' => $row->tampilkan_nama_pemeriksaan,
				'label_nama_pemeriksaan' => $row->label_nama_pemeriksaan,
				'label_nama_pemeriksaan_eng' => $row->label_nama_pemeriksaan_eng,
				'tampilkan_hasil' => $row->tampilkan_hasil,
				'label_hasil' => $row->label_hasil,
				'label_hasil_eng' => $row->label_hasil_eng,
				'tampilkan_flag' => $row->tampilkan_flag,
				'label_flag' => $row->label_flag,
				'label_flag_eng' => $row->label_flag_eng,
				'tampilkan_flag_tidak_normal' => $row->tampilkan_flag_tidak_normal,
				'label_flag_tidak_normal' => $row->label_flag_tidak_normal,
				'label_flag_tidak_normal_eng' => $row->label_flag_tidak_normal_eng,
				'tampilkan_flag_kritis' => $row->tampilkan_flag_kritis,
				'label_flag_kritis' => $row->label_flag_kritis,
				'label_flag_kritis_eng' => $row->label_flag_kritis_eng,
				'tampilkan_satuan' => $row->tampilkan_satuan,
				'label_satuan' => $row->label_satuan,
				'label_satuan_eng' => $row->label_satuan_eng,
				'tampilkan_nilai_normal' => $row->tampilkan_nilai_normal,
				'label_nilai_normal' => $row->label_nilai_normal,
				'label_nilai_normal_eng' => $row->label_nilai_normal_eng,
				'tampilkan_nilai_kritis' => $row->tampilkan_nilai_kritis,
				'label_nilai_kritis' => $row->label_nilai_kritis,
				'label_nilai_kritis_eng' => $row->label_nilai_kritis_eng,
				'tampilkan_metode' => $row->tampilkan_metode,
				'label_metode' => $row->label_metode,
				'label_metode_eng' => $row->label_metode_eng,
				'tampilkan_sumber_spesimen' => $row->tampilkan_sumber_spesimen,
				'label_sumber_spesimen' => $row->label_sumber_spesimen,
				'label_sumber_spesimen_eng' => $row->label_sumber_spesimen_eng,
				'tampilkan_keterangan' => $row->tampilkan_keterangan,
				'label_keterangan' => $row->label_keterangan,
				'label_keterangan_eng' => $row->label_keterangan_eng,
				'tampilkan_petugas_profesi' => $row->tampilkan_petugas_profesi,
				'label_petugas_profesi' => $row->label_petugas_profesi,
				'label_petugas_profesi_eng' => $row->label_petugas_profesi_eng,
				'tampilkan_catatan' => $row->tampilkan_catatan,
				'label_catatan' => $row->label_catatan,
				'label_catatan_eng' => $row->label_catatan_eng,
				'tampilkan_interpretasi_hasil' => $row->tampilkan_interpretasi_hasil,
				'label_interpretasi_hasil' => $row->label_interpretasi_hasil,
				'label_interpretasi_hasil_eng' => $row->label_interpretasi_hasil_eng,
				'tampilkan_penanggung_jawab' => $row->tampilkan_penanggung_jawab,
				'label_penanggung_jawab' => $row->label_penanggung_jawab,
				'label_penanggung_jawab_eng' => $row->label_penanggung_jawab_eng,
				'tampilkan_tanda_tangan' => $row->tampilkan_tanda_tangan,
				'label_tanda_tangan' => $row->label_tanda_tangan,
				'label_tanda_tangan_eng' => $row->label_tanda_tangan_eng,
				'tampilkan_waktu_input_order' => $row->tampilkan_waktu_input_order,
				'label_waktu_input_order' => $row->label_waktu_input_order,
				'label_waktu_input_order_eng' => $row->label_waktu_input_order_eng,
				'tampilkan_waktu_sampling' => $row->tampilkan_waktu_sampling,
				'label_waktu_sampling' => $row->label_waktu_sampling,
				'label_waktu_sampling_eng' => $row->label_waktu_sampling_eng,
				'tampilkan_waktu_input_hasil' => $row->tampilkan_waktu_input_hasil,
				'label_waktu_input_hasil' => $row->label_waktu_input_hasil,
				'label_waktu_input_hasil_eng' => $row->label_waktu_input_hasil_eng,
				'tampilkan_waktu_cetak' => $row->tampilkan_waktu_cetak,
				'label_waktu_cetak' => $row->label_waktu_cetak,
				'label_waktu_cetak_eng' => $row->label_waktu_cetak_eng,
				'tampilkan_jumlah_cetak' => $row->tampilkan_jumlah_cetak,
				'label_jumlah_cetak' => $row->label_jumlah_cetak,
				'label_jumlah_cetak_eng' => $row->label_jumlah_cetak_eng,
				'tampilkan_user_input_order' => $row->tampilkan_user_input_order,
				'label_user_input_order' => $row->label_user_input_order,
				'label_user_input_order_eng' => $row->label_user_input_order_eng,
				'tampilkan_user_sampling' => $row->tampilkan_user_sampling,
				'label_user_sampling' => $row->label_user_sampling,
				'label_user_sampling_eng' => $row->label_user_sampling_eng,
				'tampilkan_user_input_hasil' => $row->tampilkan_user_input_hasil,
				'label_user_input_hasil' => $row->label_user_input_hasil,
				'label_user_input_hasil_eng' => $row->label_user_input_hasil_eng,
				'tampilkan_user_cetak' => $row->tampilkan_user_cetak,
				'label_user_cetak' => $row->label_user_cetak,
				'label_user_cetak_eng' => $row->label_user_cetak_eng,
				'footer_notes' => $row->footer_notes,
				'footer_notes_eng' => $row->footer_notes_eng,
			);

			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Printout Permintaan Laboratorium Umum Hasil';
			$data['content']	 	= 'Mpengaturan_printout_laboratorium_umum_hasil/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Printout Permintaan Laboratorium Umum Hasil",'#'),
															array("Ubah",'mpengaturan_printout_laboratorium_umum_hasil')
														);
			
			$data['tab'] = $tipe;
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpengaturan_printout_laboratorium_umum_hasil','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_printout_laboratorium_umum_hasil_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpengaturan_printout_laboratorium_umum_hasil/index/' . $this->input->post('tipe'),'location');
		}
	}

	public function updateDataLabel() {
			// Ambil data dari permintaan AJAX
			$tipe = $this->input->post('tipe');
			$labelId = $this->input->post('labelId');
			$labelEngId = $this->input->post('labelEngId');
			$textareaContent = $this->input->post('textareaContent');
			$textareaEngContent = $this->input->post('textareaEngContent');

			// Menyusun data untuk diupdate
			$data = array(
					$labelId => $textareaContent,
					$labelEngId => $textareaEngContent,
			);

			// Panggil method model untuk melakukan update
			$result = $this->Mpengaturan_printout_laboratorium_umum_hasil_model->updateDataLabel($tipe, $data);

			// Berikan respons ke klien (browser)
			if ($result) {
					echo json_encode(array('status' => 'success', 'message' => 'Data berhasil diupdate.'));
			} else {
					echo json_encode(array('status' => 'error', 'message' => 'Gagal mengupdate data.'));
			}
	}
}
