<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_poli_kode extends CI_Controller {


	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_poli_kode_model');
		$this->load->helper('path');
		
  }
	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1550'))){
			// $data = $this->Antrian_poli_kode_model->get_pendaftaran_setting();
			// print_r($data);exit;
			$data['list_sound'] 			= $this->Antrian_poli_kode_model->list_sound();
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Kode';
			$data['content'] 		= 'Antrian_poli_kode/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Antrian Sistem",'#'),
												  array("Pengaturan Kode Antrian",'antrian_poli_kode')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_poliklinik_tujuan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.idtipe,M.nama,GROUP_CONCAT(MD.nama) as nama_dokter,H.kodeantrian_poli
							FROM antrian_poli_kode H
							LEFT JOIN mpoliklinik M ON M.id=H.idpoli
							LEFT JOIN antrian_poli_kode_dokter D ON D.idpoli=H.idpoli
							LEFT JOIN mdokter MD ON MD.id=D.iddokter
							GROUP BY H.idpoli
							ORDER BY M.idtipe,M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','nama_dokter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		  $nama_dokter='';
		  if ($r->nama_dokter){
			  $nama_dokter='<br>( '.$r->nama_dokter.' )';
		  }else{
			  $nama_dokter='<br>'.text_danger('Belum ada dokter');
		  }
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = '<strong>'.$r->nama.'</strong>'.$nama_dokter;
          $result[] = ($r->kodeantrian_poli);
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1551'))){
			$aksi .= '<a href="'.base_url().'antrian_poli_kode/edit/'.$r->id.'" class="btn btn-xs btn-success" type="button"  title="Setting Kode"><i class="si si-settings"></i></a>';	
		  }
		  $aksi .= '<button class="btn btn-xs btn-primary" type="button" onclick="load_dokter(\''.$r->id.'\',\''.$r->nama.'\')"  title="Add Dokter"><i class="fa fa-user-md"></i> Dokter</button>';	
		  $aksi .= '<button onclick="hapus_poli_tujuan('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // if (UserAccesForm($user_acces_form,array('1482'))){
		  // $aksi .= '<button onclick="hapus_poli_tujuan('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function edit($idpoli){
	  $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1551'))){
			$data = $this->Antrian_poli_kode_model->get_poli($idpoli);
			$data['nama_tipe']=GetAsalRujukanR($data['idtipe']);
			$data['list_dokter'] 			= $this->Antrian_poli_kode_model->list_dokter($idpoli);
			$data['list_sound'] 			= $this->Antrian_poli_kode_model->list_sound();
			$data['st_gc'] 			= '1';
			$data['st_sp'] 			= '1';
			$data['st_sc'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Kode';
			$data['content'] 		= 'Antrian_poli_kode/manage';
			$data['breadcrum'] 	= array(
								  array("RSKB Halmahera",'#'),
								  array("Antrian Sistem",'#'),
								  array("Pengaturan Kode Antrian",'antrian_poli_kode')
								);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
  }
 function save(){

		if($this->Antrian_poli_kode_model->updateData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('antrian_poli_kode/index','location');
		}
	
 }
	public function load_list_poli()
	{
		// $idpoli=$this->input->post('idpoli');
		$q="SELECT CASE WHEN M.idtipe='1' THEN 'Poliklinik' ELSE 'IGD' END as tipe,M.id,M.nama FROM `mpoliklinik` M
			WHERE M.`status`='1' AND M.id NOT IN (SELECT idpoli FROM antrian_poli_kode)
			ORDER BY M.idtipe,M.nama";
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	function simpan_poli(){
		$data=array(
			'idpoli' =>$this->input->post('idpoli'),
			'kodeantrian_poli' =>$this->input->post('kodeantrian_poli'),
			'sound_poli_id' =>$this->input->post('sound_poli_id'),
			'created_by' =>$this->session->userdata('user_id'),
			'created_date' =>date('Y-md H:i:s'),
		);
		$hasil=$this->db->insert('antrian_poli_kode',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function load_dokter()
	{
			$id=$this->input->post('id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.iddokter IS NOT NULL THEN '1' ELSE '0' END as pilih 
						FROM mpoliklinik_dokter MS 
						INNER JOIN mdokter M ON MS.iddokter=M.id
						LEFT JOIN antrian_poli_kode_dokter H ON H.iddokter=M.id  AND H.idpoli=MS.idpoliklinik
						WHERE M.`status`='1' AND MS.idpoliklinik='$id' AND MS.`status`='1'
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function check_save_dokter(){
		$pilih=$this->input->post('pilih');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idpoli',$idpoli);
			$this->db->where('iddokter',$iddokter);
			$hasil=$this->db->delete('antrian_poli_kode_dokter');
		}else{
			$this->idpoli=$idpoli;
			$this->iddokter=$iddokter;
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('antrian_poli_kode_dokter',$this);
		}
		
		  
		  json_encode($hasil);
	}
	function hapus_poli_tujuan(){
	  $id=$this->input->post('id');
	  $this->db->where('idpoli',$id);
	  $hasil=$this->db->delete('antrian_poli_kode',$this);
	  
	  json_encode($hasil);
	  
  }
  function simpan_sound(){
		$idpoli=$this->input->post('idpoli');
		$idsound=$this->input->post('idsound');
		$sound_id=$this->input->post('sound_id');
		$nourut=$this->input->post('nourut');
		$data['idpoli']=$idpoli;
		$data['sound_id']=$sound_id;
		$data['nourut']=$nourut;
		if ($idsound==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('antrian_poli_kode_sound',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idsound);
			$hasil=$this->db->update('antrian_poli_kode_sound',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function load_sound(){
		$idpoli=$this->input->post('idpoli');
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.sound_id,M.nama_asset,M.file_sound 
		
			FROM antrian_poli_kode_sound H
			LEFT JOIN antrian_asset_sound M ON H.sound_id=M.id
			 WHERE H.status='1' AND H.idpoli='$idpoli' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('sound_id');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->nama_asset.' ('.$r->file_sound.')';

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_sound('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		$aksi .= '<button onclick="hapus_sound('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_sound(){
		$id=$this->input->post('id');
		$q="select *FROM antrian_poli_kode_sound H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function load_data_sound(){
		$idpoli=$this->input->post('idpoli');
		$q="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') file
			FROM `antrian_poli_kode_sound` H
			LEFT JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.idpoli='$idpoli' AND H.status='1'
			ORDER BY H.nourut ASC";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function hapus_sound(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('antrian_poli_kode_sound',$data);
		json_encode($hasil);
	}
}
