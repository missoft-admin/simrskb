<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtemplate_invasif extends CI_Controller {

	/**
	 * Master Template invasif controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtemplate_invasif_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Template invasif';
		$data['content'] 		= 'Mtemplate_invasif/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template invasif",'#'),
									    			array("List",'mtemplate_invasif')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Template invasif';
		$data['content'] 		= 'Mtemplate_invasif/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template invasif",'#'),
									    			array("Tambah",'mtemplate_invasif')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtemplate_invasif_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					// 'isi_header' 		=> $row->isi_header,
					// 'nilai_tertimbang' 		=> $row->nilai_tertimbang,
					// 'nilai_satuan' 		=> $row->nilai_satuan,
					// 'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif
				);
				$data['inisial'] 			= '';
				$data['st_nilai'] 			= '1';
				$data['st_default'] 			= '0';
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Template invasif';
				$data['content']	 	= 'Mtemplate_invasif/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Template invasif",'#'),
											    			array("Ubah",'mtemplate_invasif')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtemplate_invasif','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtemplate_invasif');
		}
	}
	

	function delete($id){
		$this->Mtemplate_invasif_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtemplate_invasif','location');
	}
	function load_index_informasi_invasif(){
	  $id=$this->input->post('id');
	 
	  $q="SELECT H.* FROM mtemplate_invasif_detail H
			WHERE H.template_id='$id'

			ORDER BY H.nourut";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		  // style="text-align: center;"
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-left">'.$r->info_ina.'</td>';
			$tabel .='<td class="text-left">'.$r->info_eng.'</td>';
			$aksi = '<div class="btn-group">';
            $aksi .= '<button data-toggle="tooltip" type="button" onclick="edit_informasi('.$r->id.')" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>';
            $aksi .= '<button data-toggle="tooltip" type="button" onclick="hapus_informasi('.$r->id.')" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        $aksi .= '</div>';
		
			$tabel .='<td class="text-center"><strong>'.$aksi.'</strong></td>';
		  $tabel .='</tr>';
	  } 
	 
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function update_info_eng_invasif(){
		$assesmen_id=$this->input->post('assesmen_id');
		$id=$this->input->post('informasi_id');
		$info_eng=$this->input->post('isi');
		
		
		$data=array(
			'info_eng' => $info_eng,
			);
		$this->db->where('id',$id);
		$result=$this->db->update('mtemplate_invasif_detail',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mtemplate_invasif_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtemplate_invasif/update/'.$id,'location');
				}
			} else {
				if($this->Mtemplate_invasif_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtemplate_invasif/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtemplate_invasif/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Template invasif';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template invasif",'#'),
															array("Tambah",'mtemplate_invasif')
													);
		}else{
			$data['title'] = 'Ubah Master Template invasif';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template invasif",'#'),
															array("Ubah",'mtemplate_invasif')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mtemplate_invasif';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('nama','isi_header');
      $this->column_order    = array('nama','isi_header');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('2423'))){
            $aksi .= '<a href="'.site_url().'mtemplate_invasif/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('2424'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mtemplate_invasif" data-urlremove="'.site_url().'mtemplate_invasif/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_informasi(){
		$template_id=$this->input->post('template_id');
		$informasi_id=$this->input->post('informasi_id');
		$data=array(
			'template_id'=>$this->input->post('template_id'),
			'info_ina'=>$this->input->post('info_ina'),
			'info_eng'=>$this->input->post('info_eng'),
			'nourut'=>$this->input->post('nourut'),
		);
		if ($informasi_id==''){
		    $hasil=$this->db->insert('mtemplate_invasif_detail',$data);
		}else{
			$this->db->where('id',$informasi_id);
		    $hasil=$this->db->update('mtemplate_invasif_detail',$data);
		}
		  
		  json_encode($hasil);
	}
	function find_informasi(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM mtemplate_invasif_detail H WHERE H.id='$id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_informasi(){
	  $parameter_id=$this->input->post('id');
	 
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->delete('mtemplate_invasif_detail');
	  
	  json_encode($hasil);
	  
  }
 
}
