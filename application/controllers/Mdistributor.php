<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdistributor extends CI_Controller {

	/**
	 * Distributor controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdistributor_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Distributor';
		$data['content'] 		= 'Mdistributor/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Distributor",'#'),
									    			array("List",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'kode' 					=> '',
			'nama' 					=> '',
			'alamat' 				=> '',
			'telepon' 			=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Distributor';
		$data['content'] 		= 'Mdistributor/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Distributor",'#'),
									    			array("Tambah",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mdistributor_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'kode' 					=> $row->kode,
					'nama' 					=> $row->nama,
					'alamat' 				=> $row->alamat,
					'telepon' 			=> $row->telepon,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Distributor';
				$data['content']	 	= 'Mdistributor/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Distributor",'#'),
											    			array("Ubah",'mdistributor')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdistributor','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdistributor');
		}
	}

	function delete($id){
		
		$this->Mdistributor_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mdistributor','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mdistributor_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdistributor','location');
				}
			} else {
				if($this->Mdistributor_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdistributor','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mdistributor/manage';

		if($id==''){
			$data['title'] = 'Tambah Distributor';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Distributor",'#'),
															array("Tambah",'mdistributor')
													);
		}else{
			$data['title'] = 'Ubah Distributor';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Distributor",'#'),
															array("Ubah",'mdistributor')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  
$data_user=get_acces();
$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'mdistributor';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama','alamat','telepon');
      $this->column_order    = array('nama','alamat','telepon');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->kode;
          $row[] = $r->nama;
          $row[] = $r->alamat;
          $row[] = $r->telepon;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('46'))){
          	$aksi .= '<a href="'.site_url().'mdistributor/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('47'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mdistributor" data-urlremove="'.site_url().'mdistributor/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function ajaxSave(){
		if ($this->Mdistributor_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
