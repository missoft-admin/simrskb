<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_retur extends CI_Controller {

	/**
	 * Setting Jurnal Retur controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_retur_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_retur_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_retur_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Retur';
		$data['content'] 		= 'Msetting_jurnal_retur/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Retur",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			'idakun_kbo'=>$this->input->post('idakun_kbo'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_retur',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Hutang
	function load_hutang()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.tipe_distributor,H.iddistributor
				,CASE WHEN H.tipe_distributor='1' THEN M.nama ELSE V.nama END as nama_distributor,H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM `msetting_jurnal_retur_hutang` H
				LEFT JOIN mdistributor M ON M.id=H.iddistributor AND H.tipe_distributor='1'
				LEFT JOIN mvendor V ON V.id=H.iddistributor AND H.tipe_distributor='3'
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.tipe_distributor,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','nama_distributor');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->tipe_distributor=='1'?text_success('Pemesanan Gudang'):text_primary('Pengajuan'));
            $row[] = ($r->iddistributor=='0'?text_default('All Distributor'):$r->nama_distributor);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_hutang('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_hutang(){
		$id_edit=$this->input->post('id_edit');
		$iddistributor=($this->input->post('iddistributor')=='#'?0:$this->input->post('iddistributor'));
		$data=array(
			'setting_id'=>1,
			'iddistributor'=>$iddistributor,
			'tipe_distributor'=>$this->input->post('tipe_distributor'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_hutang($data['tipe_distributor'],$iddistributor)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_retur_hutang',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_hutang($tipe_distributor,$iddistributor){
		
		$gabung=$tipe_distributor.'-'.$iddistributor;
		$q="SELECT *FROM msetting_jurnal_retur_hutang S
			WHERE CONCAT(S.tipe_distributor,'-',S.iddistributor)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_hutang($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_retur_hutang');
		echo json_encode($result);
	}

}
