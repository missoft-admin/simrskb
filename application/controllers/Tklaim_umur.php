<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tklaim_umur extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tklaim_umur_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tgl_trx1'=>$tgl_pertama,
			'tgl_trx2'=>date('d-m-Y'),
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Umur Piutang';
		$data['content'] 		= 'Tklaim_umur/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Umur Piutang",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		
		$where1='';
		$where2='';
		
		
		if ($idkelompokpasien !='#'){
			$where1 .=" AND H.idkelompokpasien='$idkelompokpasien' ";
			$where2 .=" AND H.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			// $where1 .=" AND H.idkontraktor='$idrekanan' ";
			if ($idkelompokpasien =='1'){
				$where2 .=" AND H.idrekanan='$idrekanan' ";
			}
		}
		
        $from = "(
					SELECT '2' as jenis_rekanan, H.idkelompokpasien,H.idrekanan,KP.nama as rekanan_nama, SUM(D.tidak_terbayar) as sisa,SUM(D.nominal) as total_tagihan
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) <=1 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_1
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =2 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_2
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =3 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_3
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =4 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_4
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =5 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_5
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =6 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_6
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =12 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_12
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) >12 AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) <24 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_24_kecil
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) >=24 THEN D.tidak_terbayar ELSE 0 END) as bulan_24_besar

					from tklaim_detail D
					LEFT JOIN tklaim H ON D.klaim_id=H.id
					LEFT JOIN mrekanan R ON R.id=H.idrekanan
					LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
					WHERE H.idkelompokpasien !='1'  AND H.`status` !=0 AND H.status_kirim='1' ".$where1."
					GROUP BY H.idkelompokpasien

					UNION ALL

					SELECT '1' as jenis_rekanan, H.idkelompokpasien,H.idrekanan,R.nama as rekanan_nama, SUM(D.tidak_terbayar) as sisa,SUM(D.nominal) as total_tagihan
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) <=1 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_1
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =2 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_2
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =3 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_3
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =4 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_4
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =5 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_5
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =6 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_6
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) =12 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_12
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) >12 AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) <24 AND D.status_lunas='0' THEN D.tidak_terbayar ELSE 0 END) as bulan_24_kecil
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) >=24 THEN D.tidak_terbayar ELSE 0 END) as bulan_24_besar

					from tklaim_detail D
					LEFT JOIN tklaim H ON D.klaim_id=H.id
					LEFT JOIN mrekanan R ON R.id=H.idrekanan
					LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
					WHERE H.idkelompokpasien ='1'  AND H.`status` !=0  AND H.status_kirim='1' ".$where2."
					GROUP BY H.idrekanan
				) as tbl ";
				
		//Jenis Rekanan : 1 : Asuransi !=1 SELAIN ASURANSI
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $url        = site_url('tklaim_umur/');
            $row[] = $no;
            $row[] = $r->rekanan_nama;
            $row[] = number_format($r->sisa,2);  
			$row[] = ($r->bulan_1 > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/1" target="_blank">'.number_format($r->bulan_1,2).'</a>':'0');
			$row[] = ($r->bulan_2 > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/2" target="_blank">'.number_format($r->bulan_2,2).'</a>':'0');
			$row[] = ($r->bulan_3 > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/3" target="_blank">'.number_format($r->bulan_3,2).'</a>':'0');
			$row[] = ($r->bulan_4 > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/4" target="_blank">'.number_format($r->bulan_4,2).'</a>':'0');
			$row[] = ($r->bulan_5 > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/5" target="_blank">'.number_format($r->bulan_5,2).'</a>':'0');
			$row[] = ($r->bulan_6 > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/6">'.number_format($r->bulan_6,2).'</a>':'0');
			$row[] = ($r->bulan_12 > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/12" target="_blank">'.number_format($r->bulan_12,2).'</a>':'0');
			$row[] = ($r->bulan_24_kecil > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/13" target="_blank">'.number_format($r->bulan_24_kecil,2).'</a>':'0');
			$row[] = ($r->bulan_24_besar > 0 ?'<a href="'.$url.'detail/'.$r->jenis_rekanan.'/'.$r->idkelompokpasien.'/'.$r->idrekanan.'/24" target="_blank">'.number_format($r->bulan_24_besar,2).'</a>':'0');
            $row[] = number_format($r->total_tagihan,2);  
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function detail($jenis_rekanan,$idkelompokpasien,$idrekanan,$bulan) {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		$data=array(
			'jenis_rekanan'=>$jenis_rekanan,
			'idkelompokpasien'=>$idkelompokpasien,
			'idrekanan'=>$idrekanan,
			'bulan'=>$bulan,			
		);
		$total=$this->model->get_total_piutang($jenis_rekanan,$idkelompokpasien,$idrekanan,$bulan);
		$data['nama_asuransi'] 			= $this->model->get_nama_asuransi($idkelompokpasien,$idrekanan);
		$data['total'] 			= $total;
		$data['umur'] 			= $this->get_umur($bulan);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		// $data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		// $data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Umur Piutang Detail';
		$data['content'] 		= 'Tklaim_umur/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Umur Piutang",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_detail(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		
		$jenis_rekanan=$this->input->post('jenis_rekanan');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$bulan=$this->input->post('bulan');
		
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where2='';
		
		if ($no_medrec !=''){
			$where2 .=" AND no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where2 .=" AND namapasien LIKE '%".$nama_pasien."%' ";
		}
		if ($no_reg !=''){
			$where2 .=" AND no_reg='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where2 .= " AND DATE(tanggaldaftar) >='".YMDFormat($tgl_trx)."' AND DATE(tanggaldaftar) <='".YMDFormat($tgl_trx2)."'";
        }
		
		$where1='';
		if ($jenis_rekanan=='1'){//ASURANSI
			$where1 .="AND H.idrekanan='$idrekanan'";			
		}else{
			$where1 .="AND H.idkelompokpasien='$idkelompokpasien'";	
		}
		if ($bulan <12){
			$where1 .=" AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30)=".$bulan.")";
		
		}elseif($bulan==12){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30)=".$bulan.")";
		}elseif($bulan==13){
			$where1 .="AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) >12 AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) <24";
		}elseif($bulan==24){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) > 24)";
		}
		
        $from = "(
					SELECT H.id,H.no_klaim,H.tipe,D.pendaftaran_id,D.kasir_id
					,CASE WHEN H.tipe='1' THEN TP.tanggaldaftar ELSE TR.tanggaldaftar END tanggaldaftar
					,CASE WHEN H.tipe='1' THEN K.tanggal ELSE TB.tanggal END tanggal_trx
					,CASE WHEN H.tipe='1' THEN TP.nopendaftaran ELSE TR.nopendaftaran END no_reg
					,CASE WHEN H.tipe='1' THEN TP.no_medrec ELSE TR.no_medrec END no_medrec
					,CASE WHEN H.tipe='1' THEN CONCAT(TP.title,'. ',TP.namapasien) ELSE CONCAT(TR.title,'. ',TR.namapasien) END namapasien
					,CASE WHEN H.tipe='1' THEN K.total ELSE TB.totalharusdibayar END total_trx
					,CASE WHEN H.tipe='1' THEN KD.nominal ELSE TBD.nominal END bayar
					,H.tanggal_tagihan,D.nominal_bayar,D.tidak_terbayar,D.pembayaran_id as idpembayaran,D.kasir_id as idkasir,D.pendaftaran_id as idpendaftaran
					FROM tklaim H
					LEFT JOIN tklaim_detail D ON D.klaim_id=H.id
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=D.pendaftaran_id AND D.tipe='1'
					LEFT JOIN trawatinap_pendaftaran TR ON TR.id=D.pendaftaran_id AND D.tipe='2'
					LEFT JOIN tkasir K ON K.id=D.kasir_id AND D.tipe='1'
					LEFT JOIN tkasir_pembayaran KD ON KD.id=D.pembayaran_id AND D.tipe='1'
					LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.id=D.kasir_id
					LEFT JOIN trawatinap_tindakan_pembayaran_detail TBD ON TBD.id=D.pembayaran_id AND D.tipe='2'

					WHERE H.status_kirim='1' ".$where1."

				) as tbl WHERE tbl.id is not null ".$where2;
				
		//Jenis Rekanan : 1 : Asuransi !=1 SELAIN ASURANSI
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
		$url        = site_url('tkontrabon_verifikasi/');
		$url_kasir        = site_url('tkasir/');
		$url_rajal        = site_url('trawatinap_tindakan/');
		$url_batal       = site_url('tgudang_pengembalian/');
		$url_klaim       = site_url('tklaim_rincian/');
		$url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
		$url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $row[] = $no;
            $row[] = HumanDateLong($r->tanggaldaftar);
            $row[] = HumanDateLong($r->tanggal_trx);
			$row[] = ($r->tipe=='1' ? '<span class="label label-primary">RJ</span>':'<span class="label label-success">RI / ODS</span>');
            $row[] = $r->no_reg;  
            $row[] = $r->no_medrec;  
            $row[] = $r->namapasien;  
            $row[] = number_format($r->total_trx,2);  
            $row[] = number_format($r->bayar,2);  
            $row[] = number_format($r->total_trx-$r->bayar,2);  
            $row[] = HumanDateShort($r->tanggal_tagihan);
            $row[] = $r->no_klaim;  
			if ($r->tipe=='1'){
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'kwitansi_verifikasi/'.$r->idpembayaran.'" target="_blank"  type="button"  title="Kwitansi"><i class="fa fa-list-alt "></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-danger" href="'.$url_dok_rajal.$r->idkasir.'" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
			}else{
				$aksi .= '<a  class="view btn btn-xs btn-success" href="'.$url_rajal.'verifikasi/'.$r->idpendaftaran.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_rajal.'print_rincian_global/'.$r->idpembayaran.'/1" target="_blank"  type="button"  title="Print Rincian Global"><i class="fa fa-list-alt "></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_rajal.'print_rincian_biaya/'.$r->idpendaftaran.'/1" target="_blank"  type="button"  title="Print Rincian Biaya"><i class="fa fa-print"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-danger" href="'.$url_dok_ranap.$r->idkasir.'" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
			}
			$aksi .= '<a class="view btn btn-xs btn-warning" href="'.$url_klaim.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'/1" target="_blank"  type="button"  title="Rincian Tagihan"><i class="fa fa-external-link"></i></a>';
			$aksi.='</div>';
            $row[] = $aksi;//12
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function get_umur($bulan){
		if ($bulan <12){
			$bulan=$bulan.' Bulan';
		
		}elseif($bulan==12){
			$bulan='1 Tahun';
		}elseif($bulan==13){
			$bulan='Lebih 1 Tahun';
		}elseif($bulan==24){
			$bulan='Lebih 2 Tahun';
		}
		return $bulan;
	}
}
