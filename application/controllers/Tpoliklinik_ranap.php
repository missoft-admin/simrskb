<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class Tpoliklinik_ranap extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpoliklinik_ranap_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpoliklinik_ranap_model');
		$this->load->model('Tpoliklinik_rm_order_model');
		$this->load->model('Tpendaftaran_ranap_erm_model');
		$this->load->helper('path');
		
  }
	function estimasi_admin($tab='1'){
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1909'))){
			
			$data['tab'] 			= $tab;
			
			$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
			$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'Perawat';
			$data['content'] 		= 'Tpoliklinik_ranap/index_admin';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("Estimasi Biaya",'tpoliklinik_ranap/estimasi_admin')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function estimasi_proses($assesmen_id,$disabel=''){
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1910'))){
			$data=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_rencana_biaya($assesmen_id);
			// print_r($data);exit;
			$q="SELECT * FROM tpoliklinik_ranap_rencana_biaya_detail WHERE assesmen_id='$assesmen_id'";
			$data_detail=$this->db->query($q)->row_array();
			if (!$data_detail){
				$pendaftaran_id=$data['pendaftaran_id'];
				$q="SELECT H.idkelompokpasien,H.idrekanan,H.namapasien FROM tpoliklinik_pendaftaran H WHERE H.id='$pendaftaran_id'";
				$data_kelompok=$this->db->query($q)->row();
				$data_detail=array(
					'assesmen_id' => $assesmen_id,
					'icu_pilih' => $data['icu'],
					'cito_pilih' => $data['cito'],
					// 'nama_pasien_ttd' => $data['nama_pasien'],
					'nama_pasien_ttd' => $data_kelompok->namapasien,
					'kelompok_pasien_id' => $data_kelompok->idkelompokpasien,
					'idrekanan' => ($data_kelompok->idkelompokpasien==1?$data_kelompok->idrekanan:0),
					'kelas' => 1,
					'jumlah_icu' => 1,
					'lama' => 1,
				);
			// print_r($data_detail);exit;
				$this->db->insert('tpoliklinik_ranap_rencana_biaya_detail',$data_detail);
				$q="INSERT INTO tpoliklinik_ranap_rencana_biaya_keterangan (assesmen_id,`no`,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
					SELECT '$assesmen_id',H.`no`,H.isi as pertanyaan,GROUP_CONCAT(D.ref_id) as group_jawaban_id 
					,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,H.jenis_isi 
					FROM setting_rencana_biaya_isi H
					LEFT JOIN setting_rencana_biaya_isi_jawaban D ON H.id=D.general_isi_id
					LEFT JOIN merm_referensi R ON R.id=D.ref_id
					WHERE H.`status`='1'
					GROUP BY H.id";
				$this->db->query($q);
				
				$q="SELECT * FROM tpoliklinik_ranap_rencana_biaya_detail WHERE assesmen_id='$assesmen_id'";
				$data_detail=$this->db->query($q)->row_array();
				// print_r($data_detail);exit;
			}
			
			$data_label=$this->Tpoliklinik_ranap_model->setting_label_estimasi_biaya();
			$status_persetujuan_pasien=$data['status_persetujuan_pasien'];
			if ($status_persetujuan_pasien=='2'){
				$disabel='disabled';
			}
			$data['tab'] 			= $data_detail['kelas'];
			$data['error'] 			= '';
			$data['disabel'] 			= $disabel;
			$data['title'] 			= 'Proses Estimasi Biaya';
			$data['content'] 		= 'Tpoliklinik_ranap/proses_admin';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("Estimasi Biaya Proses",'tpoliklinik_ranap/estimasi_admin')
												);

			$data = array_merge($data,$data_label,$data_detail, backend_info());
			// print_r($data);exit;
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function save_ttd_2(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$nama_tabel = 'tpoliklinik_ranap_rencana_biaya_detail';
		$pasien_ttd = $this->input->post('signature64');
		$nama_pasien_ttd = $this->input->post('nama_pasien_ttd');
		$data=array(
			'pasien_ttd' =>$pasien_ttd,
			'nama_pasien_ttd' =>$nama_pasien_ttd,
			'tanggal_ttd_pasien' =>date('Y-m-d H:i:s'),
		);
		// print_r($data);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ttd(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		// $nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'pasien_ttd' =>'',
			// 'nama_pasien_ttd' =>'',
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_ranap_rencana_biaya_detail',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function get_tarif(){
		$assesmen_id = $this->input->post('assesmen_id');
		$kelompok_operasi_id = $this->input->post('kelompok_operasi_id');
		$jenis_operasi_id = $this->input->post('jenis_operasi_id');
		$lama = $this->input->post('lama');
		$icu_pilih = $this->input->post('icu_pilih');
		$cito_pilih = $this->input->post('cito_pilih');
		$kelompok_pasien_id = $this->input->post('kelompok_pasien_id');
		$idrekanan = $this->input->post('idrekanan');
		if ($this->input->post('kelas')=='0'){
			$kelas = 3;
		}else{
			$kelas = $this->input->post('kelas');
		}
		$jumlah_icu = $this->input->post('jumlah_icu');
		$lama = $this->input->post('lama');
		$harga_kamar_ko=0;
		$harga_do=0;
		$harga_da=0;
		$harga_obat_ko=0;
		$harga_ruang=0;
		$harga_obat_ruang=0;
		$harga_penunjang_ruang=0;
		$harga_visit_ruang=0;
		$harga_icu=0;
		$q="SELECT *FROM setting_rencana_biaya_persen";
		$data_persen=$this->db->query($q)->row();
		$dokter_anestesi=$data_persen->dokter_anestesi;
		$assisten_operator=$data_persen->assisten_operator;
		$assisten_anestesi=$data_persen->assisten_anestesi;
		
		
		$q="SELECT S.idjenisoperasi,T.id,D.total 
			FROM `msetting_tarif_jenis_operasi` S 
			INNER JOIN mtarif_operasi T ON T.idjenis=S.idjenisoperasi AND T.idtipe='1'
			INNER JOIN mtarif_operasi_detail D ON D.idtarif=T.id AND D.kelas='$kelas'
			WHERE S.idkelompokpasien='$kelompok_pasien_id' AND S.idrekanan='$idrekanan'  AND S.kelompok_operasi_id='$kelompok_operasi_id' 
			AND S.cito='$cito_pilih' AND S.jenis_operasi_id='$jenis_operasi_id' ";
		// print_r($q);exit;
		$harga_kamar_ko=$this->db->query($q)->row('total');
		$harga_kamar_ko=($harga_kamar_ko?$harga_kamar_ko:0);
		$q="SELECT S.idjenisoperasi,T.id,D.total 
			FROM `msetting_tarif_jenis_operasi` S 
			INNER JOIN mtarif_operasi T ON T.idjenis=S.idjenisoperasi AND T.idtipe='3'
			INNER JOIN mtarif_operasi_detail D ON D.idtarif=T.id AND D.kelas='$kelas'
			WHERE S.idkelompokpasien='$kelompok_pasien_id' AND S.idrekanan='$idrekanan'  AND S.kelompok_operasi_id='$kelompok_operasi_id' 
			AND S.cito='$cito_pilih' AND S.jenis_operasi_id='$jenis_operasi_id' ";
		// print_r($q);exit;
		$harga_do=$this->db->query($q)->row('total');
		$harga_do=($harga_do?$harga_do:0);
		$q="SELECT 
			CASE 
			WHEN '$kelas'=1 THEN harga_obat_1
			WHEN '$kelas'=2 THEN harga_obat_2
			WHEN '$kelas'=3 THEN harga_obat_3
			WHEN '$kelas'=4 THEN harga_obat_u
			ELSE 0 END as total

			FROM setting_rencana_biaya_obat S

			WHERE S.kelompok_operasi_id='$kelompok_operasi_id' AND S.jenis_operasi_id='$jenis_operasi_id' AND S.`status`='1'
			LIMIT 1";
		$harga_obat_ko=$this->db->query($q)->row('total');
		$harga_obat_ko=($harga_obat_ko?$harga_obat_ko:0);
		$q="SELECT  
				CASE 
				WHEN '$kelas'=1 THEN M.harga_1 
				WHEN '$kelas'=2 THEN M.harga_2 
				WHEN '$kelas'=3 THEN M.harga_3 
				WHEN '$kelas'=4 THEN M.harga_u
				ELSE 0 END total

				FROM (
				SELECT 
				compare_value_7(
						MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='0' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='0' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='0' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='0' AND H.idrekanan='0',H.id,NULL))
						
						) as id

				FROM `setting_rencana_biaya_ranap` H

				WHERE H.`status`='1'
				) S INNER JOIN setting_rencana_biaya_ranap M ON M.id=S.id
				";
		$harga_obat_ruang=$this->db->query($q)->row('total');
		$harga_obat_ruang=($harga_obat_ruang?$harga_obat_ruang:0);
		
		$q="SELECT  
				CASE 
				WHEN '$kelas'=1 THEN M.harga_1 
				WHEN '$kelas'=2 THEN M.harga_2 
				WHEN '$kelas'=3 THEN M.harga_3 
				WHEN '$kelas'=4 THEN M.harga_u
				ELSE 0 END total

				FROM (
				SELECT 
				compare_value_7(
						MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='0' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='0' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='0' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='0' AND H.idrekanan='0',H.id,NULL))
						
						) as id

				FROM `setting_rencana_biaya_penunjang` H

				WHERE H.`status`='1'
				) S INNER JOIN setting_rencana_biaya_penunjang M ON M.id=S.id
				";
		$harga_penunjang_ruang=$this->db->query($q)->row('total');
		$harga_penunjang_ruang=($harga_penunjang_ruang?$harga_penunjang_ruang:0);
		
		$q="SELECT  
				CASE 
				WHEN '$kelas'=1 THEN M.harga_1 
				WHEN '$kelas'=2 THEN M.harga_2 
				WHEN '$kelas'=3 THEN M.harga_3 
				WHEN '$kelas'=4 THEN M.harga_u
				ELSE 0 END total

				FROM (
				SELECT 
				compare_value_7(
						MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='0' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='0' AND H.idrekanan='$idrekanan',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='$kelompok_pasien_id' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu='$icu_pilih' AND H.idkelompok_pasien='0' AND H.idrekanan='0',H.id,NULL))
						,MAX(IF(H.icu=0 AND H.idkelompok_pasien='0' AND H.idrekanan='0',H.id,NULL))
						
						) as id

				FROM `setting_rencana_biaya_visit` H

				WHERE H.`status`='1'
				) S INNER JOIN setting_rencana_biaya_visit M ON M.id=S.id
				";
		$harga_visit_ruang=$this->db->query($q)->row('total');
		$harga_visit_ruang=($harga_visit_ruang?$harga_visit_ruang:0);
		
		$q="SELECT H.total FROM (
			SELECT  M.id as kp,0 as idrekanan,M.truang_perawatan as idtarif FROM mpasien_kelompok M
			UNION ALL
			SELECT 1 as kp,M.id as idrekanan,M.truang_perawatan as idtarif FROM mrekanan M WHERE (M.tfisioterapi > 0) 
			) T 
			INNER JOIN mtarif_ruangperawatan_detail H ON H.idtarif=T.idtarif AND H.kelas='$kelas'
			
			WHERE T.kp='$kelompok_pasien_id' AND (T.idrekanan='0' OR T.idrekanan='$idrekanan')

			ORDER BY T.idrekanan DESC
				";
		$harga_ruang=$this->db->query($q)->row('total');
		$harga_ruang=($harga_ruang?$harga_ruang:0);
		
		$q="SELECT H.total FROM (
			SELECT  M.id as kp,0 as idrekanan,M.truang_icu as idtarif FROM mpasien_kelompok M
			UNION ALL
			SELECT 1 as kp,M.id as idrekanan,M.truang_icu as idtarif FROM mrekanan M WHERE (M.tfisioterapi > 0) 
			) T 
			INNER JOIN mtarif_ruangperawatan_detail H ON H.idtarif=T.idtarif AND H.kelas='$kelas'
			
			WHERE T.kp='$kelompok_pasien_id' AND (T.idrekanan='0' OR T.idrekanan='$idrekanan')

			ORDER BY T.idrekanan DESC
				";
		$harga_icu=$this->db->query($q)->row('total');
		$harga_icu=($harga_icu?$harga_icu:0);
		
		$arr['harga_kamar_ko']=$harga_kamar_ko;
		$arr['harga_do']=$harga_do;
		$harga_da=($harga_do * $dokter_anestesi/100);
		$arr['harga_da']= $harga_da;
		$arr['harga_daa']= ($harga_do * $assisten_operator/100) + ($harga_da * $assisten_anestesi/100);
		$arr['harga_obat_ko']=$harga_obat_ko;
		if ($this->input->post('kelas')=='0'){
			$arr['harga_ruang']=0;
		}else{
			$arr['harga_ruang']=$harga_ruang * $lama;
		}
		$arr['harga_obat_ruang']=$harga_obat_ruang;
		$arr['harga_penunjang_ruang']=$harga_penunjang_ruang;
		$arr['harga_visit_ruang']=$harga_visit_ruang;
		$arr['harga_icu']=$harga_icu * $jumlah_icu;
		
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya_detail',$arr);
		
		$this->output->set_output(json_encode($arr));
	}
	function update_record(){
		
		$transaksi_id= $this->input->post('transaksi_id');
		$this->jumlah_implant = $this->input->post('jumlah_implant');
		$this->total_biaya_implant = $this->input->post('total_biaya_implant');
		$this->pilih = $this->input->post('pilih');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$hasil=$this->db->where('id',$transaksi_id);
		$hasil=$this->db->update('tpoliklinik_ranap_rencana_biaya_implant',$this);
		
		$this->output->set_output(json_encode($hasil));
	}
	function load_index_keterangan(){
		$assesmen_id=$this->input->post('assesmen_id');
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='16' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*			,CASE 
						WHEN H.jenis_isi=1 THEN H.jawaban_id
						WHEN H.jenis_isi=2 THEN H.jawaban_freetext
						WHEN H.jenis_isi=3 THEN H.jawaban_ttd
					 END as jawaban
					 FROM tpoliklinik_ranap_rencana_biaya_keterangan H WHERE H.assesmen_id='$assesmen_id'
						ORDER BY H.no ASC
					 ";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  $tabel .='<td width="5%" >'.($row->no).'</td>';
		  $tabel .='<td width="75%" ><input type="hidden" class="sc_id" value="'.$row->id.'">'.($row->pertanyaan).'</td>';
	  
		  $tabel .='<td width="20%" class="text-center">'.$this->opsi_nilai_sc($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>';
			  
		  
		  $tabel .='</tr>';
		  
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function hapus_paraf(){
		
		$id = $this->input->post('pendaftaran_id');
		$nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'jawaban_ttd' =>'',
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function opsi_nilai_sc($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_sc has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_sc_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'tpoliklinik_ranap_rencana_biaya_keterangan'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sc"><i class="fa fa-paint-brush"></i> SIGNATURE</button>';
			  }else{
				  $btn_paraf='
					<div class="img-container text-center">
						<img class="img-responsive" style="width:40%;align:center" src="'.$jawaban_id.'" alt="">
						<div class="img-options">
							<div class="img-options-content">
								<button class="btn btn-sm btn-success" onclick="modal_faraf('.$id.','.$nama_tabel.')" ><i class="fa fa-pencil"></i> Edit</button>
								<a class="btn btn-sm btn-danger" onclick="hapus_paraf('.$id.','.$nama_tabel.')" ><i class="fa fa-times"></i> Delete</a>
							</div>
						</div>
					</div>
				  ';
				  // $btn_paraf="<img style='display:block; width:100px;height:100px;' id='base64image' src='".$jawaban_id."' />";
				  // $btn_paraf .= '<br><div class="btn-group btn-group-sm" role="group"><button onclick="hapus_paraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Hapus Tanda tangan" class="btn btn-danger btn-xs btn_ttd_sc"><i class="fa fa-trash"></i></button>';
				  // $btn_paraf .= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sc"><i class="fa fa-paint-brush"></i></button>';
				  // $btn_paraf .= '</div>';
			  }
			  return $btn_paraf;
		}
	}
	function update_nilai_sc(){
	  $sc_id=$this->input->post('sc_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$sc_id);
	  $result=$this->db->update('tpoliklinik_ranap_rencana_biaya_keterangan',$data);
	  $this->output->set_output(json_encode($result));
	}
	function load_implant_edit()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,HH.status_assemen 
						FROM tpoliklinik_ranap_rencana_biaya_implant H
						INNER JOIN tpoliklinik_ranap_rencana_biaya HH ON HH.assesmen_id=H.assesmen_id
						WHERE H.assesmen_id='$assesmen_id'
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_implant','ukuran_implant','merk_implant','catatan_implant');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nama_implant);
          $result[] = ($r->ukuran_implant);
          $result[] = ($r->merk_implant);
          $result[] = ($r->catatan_implant).'<input type="hidden" class="form-control input-sm number input_edit text-center trx_id" value="'.$r->id.'">';
          $result[] = '<input type="text" class="form-control input-sm number input_edit text-center jumlah_implant" value="'.$r->jumlah_implant.'">';
          $result[] = '<input type="text" '.($r->pilih=='0'?'disabled':'').' class="form-control input-sm number input_edit text-right total_biaya_implant" value="'.$r->total_biaya_implant.'">';
         
          $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input data-id='.($r->id?$r->id:0).'  type="checkbox" '.($r->pilih?'checked':'').' class="chck_pilih"><span></span>
					</label>'.'<input type="hidden" class="form-control input-sm number input_edit text-center pilih" value="'.$r->pilih.'">';	
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  	function save_ttd(){
		
		$id = $this->input->post('pendaftaran_id');
		$nama_tabel = $this->input->post('nama_tabel');
		$jawaban_ttd = $this->input->post('signature64');
		$data=array(
			'jawaban_ttd' =>$jawaban_ttd,
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function index($tab='1'){
		get_ppa_login();
		$log['path_tindakan']='tpoliklinik_ranap';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('1597'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				$data['tab_utama']=11;
			
				$data['list_poli'] 			= $this->Tpoliklinik_ranap_model->list_tujuan_fisio();
				// $data['list_poli'] 			= $this->Tpoliklinik_ranap_model->list_poliklinik();
				$data['list_dokter'] 			= $this->Tpoliklinik_ranap_model->list_ppa_dokter();
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= DMYFormat(date('2023-09-01'));
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'My Pasien';
				$data['content'] 		= 'Tpoliklinik_ranap/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Tindakan Poliklinik",'#'),
													  array("My Pasien",'tpendaftaran_poli_pasien')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	
	function list_estimasi_admin(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		// $logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_estimasi_biaya($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// $data_user=get_acces();
		// $user_acces_form=$data_user['user_acces_form'];
		$where='';
		$tab=$this->input->post('tab');
		$tanggal_1 = $this->input->post('tanggal_1');
		$tanggal_2 = $this->input->post('tanggal_2');
		$nopermintaan = $this->input->post('nopermintaan');
		$iddokter = $this->input->post('iddokter');
		$created_ppa = $this->input->post('created_ppa');
		$petugas_billilng_ppa_id = $this->input->post('petugas_billilng_ppa_id');
		$notransaksi = $this->input->post('notransaksi');
		$tipe = $this->input->post('tipe');
		$tab = $this->input->post('tab');
		$namapasien = $this->input->post('namapasien');
		$no_medrec = $this->input->post('no_medrec');
		
		
		if ($tab=='2'){
			$where .=" AND H.status_proses = '0'";
		}
		if ($tab=='3'){
			$where .=" AND H.status_proses = '1'";
		}
		if ($tab=='4'){
			$where .=" AND H.status_persetujuan_pasien = '2'";
		}
		if ($tab=='5'){
			$where .=" AND H.status_persetujuan_pasien = '3'";
		}
		if ($tab=='6'){
			$where .=" AND H.status_persetujuan_pasien = '1'";
		}
		if ($tanggal_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
		}
		
		if ($nopermintaan!=''){
			$where .=" AND H.nopermintaan LIKE '%".$nopermintaan."%'";
		}
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND H.dpjp ='".$iddokter."'";
		}
		if ($tipe!='#'){
			$where .=" AND H.tipe ='".$tipe."'";
		}
		if ($created_ppa!='#'){
			$where .=" AND H.created_ppa ='".$created_ppa."'";
		}
		if ($petugas_billilng_ppa_id!='#'){
			$where .=" AND D.petugas_billilng_ppa_id ='".$petugas_billilng_ppa_id."'";
		}
		if ($namapasien!=''){
			$where .=" AND H.nama_pasien LIKE '%".$namapasien."%'";
		}
		if ($no_medrec!=''){
			$where .=" AND H.nomedrec_pasien LIKE '%".$no_medrec."%'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,C.ref as nama_cito,I.ref as nama_icu
					,MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					,D.pasien_ttd,MK.nama as nama_kelas
					FROM tpoliklinik_ranap_rencana_biaya H
					LEFT JOIN tpoliklinik_ranap_rencana_biaya_detail D ON D.assesmen_id=H.assesmen_id
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mkelas MK ON MK.id=D.kelas
					LEFT JOIN merm_referensi C ON C.nilai=H.cito AND C.ref_head_id='123'
					LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
		$akses_general=$this->db->query("SELECT *FROM setting_rencana_biaya")->row_array();
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_edit='';$bnt_setuju='';$btn_tolak='';$btn_berunding='';$btn_lihat='';$btn_cetak='';$aksi_edit='';
			$btn_edit='<a href="'.site_url('tpoliklinik_ranap/estimasi_proses/'.$r->assesmen_id).'" class="btn btn-primary btn-xs" type="button" title="Proses" type="button"><i class="fa fa-pencil"></i></a>';
			if ($r->total_estimasi>0){
				if ($r->status_proses>0){
					$bnt_setuju='&nbsp;<button class="btn btn-success  btn-xs" '.$btn_disabel_edit.' type="button" title="Setuju" onclick="set_setuju('.$r->assesmen_id.',2)" type="button"><i class="fa fa-check"></i></button>';
					$btn_tolak='&nbsp;<button class="btn btn-danger  btn-xs" '.$btn_disabel_edit.' type="button" title="Menolak" onclick="show_menolak('.$r->assesmen_id.')" type="button"><i class="si si-ban"></i></button>';
					if ($r->status_persetujuan_pasien!='1'){
						$btn_berunding='&nbsp;<button class="btn btn-warning  btn-xs" '.$btn_disabel_edit.' type="button" title="Berunding" onclick="set_setuju('.$r->assesmen_id.',1)" type="button"><i class="fa fa-spinner"></i></button>';
					}
				}
			}
			$btn_lihat='&nbsp;<a href="'.site_url('tpoliklinik_ranap/estimasi_proses/'.$r->assesmen_id).'/disabled" class="btn btn-default btn-xs" type="button" title="Lihat" type="button"><i class="fa fa-eye"></i></a>';
			if ($r->status_proses>0){
			$btn_cetak='&nbsp;<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tpoliklinik_ranap/cetak_estimasi_biaya/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></a>';
			}
			$aksi='';
		$btn_edit=($r->status_persetujuan_pasien<2?$btn_edit:'');
		$bnt_setuju=($r->status_persetujuan_pasien<2?$bnt_setuju:'');
		$btn_tolak=($r->status_persetujuan_pasien<2?$btn_tolak:'');
		$btn_berunding=($r->status_persetujuan_pasien<2?$btn_berunding:'');
		if ($r->pasien_ttd=='' || $r->pasien_ttd==null){
			$bnt_setuju='';
			// exit;
		}
		$aksi=$btn_edit.$bnt_setuju.$btn_tolak.$btn_berunding.$btn_lihat.$btn_cetak.$aksi_edit;
		$result[] = $aksi;
		$result[] = '<span><strong>'.($r->nopermintaan).'</strong></span>'.'<br><span class="text-muted">'.($r->user_created).'<br>'.HumanDateLong($r->created_date).'</span>';
		$result[] = ($r->nama_pasien.' - '.$r->nomedrec_pasien);
		$result[] = ('<span class="text-muted">'.($r->tipe=='3'?'RAWAT INAP':'ODS').'</span>');
		$result[] = ($r->nama_icu);
		$result[] = ($r->nama_cito);
		$result[] = ($r->dengan_diagnosa);
		$result[] = ($r->rencana_tindakan);
		$result[] = ($r->nama_dpjp);
		$result[] = status_estimasi_biaya($r->status_proses,$r->status_persetujuan_pasien,$r->alasan_menolak,$r->nama_kelas).($r->alasan_menolak?'<br>'.$r->alasan_menolak:'');
		$result[] = number_format($r->total_estimasi);
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  
	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		// print_r($trx_id);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data_satuan_ttv=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		// print_r($data);exit;
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpoliklinik_ranap/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['error'] 			= '';
		$data['status_assemen'] 			= '0';
		$data['assesmen_id'] 			= '';
		$data['st_ranap'] 			= $st_ranap;
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Konsultasi';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Konsultasi ",'#'),
							  array("Dokter",'tpoliklinik_ranap')
							);
		$data_assemen=array('idpoli'=>'','iddokter_peminta'=>'');
		// print_r($data_assemen);exit;
		//CAtatan Evaluasi MPP
		if ($menu_kiri=='input_imp_mpp' || $menu_kiri=='his_imp_mpp'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_imp_mpp'){
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_imp_mpp_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpoliklinik_ranap_model->get_data_imp_mpp_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_imp_mpp_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tpoliklinik_ranap_model->get_data_imp_mpp($pendaftaran_id,$st_ranap);
				// print_r($data_assemen);exit;
				
				
			}
			if ($data_assemen){
				// $data_assemen['']=$arr=explode(',',$str);
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['tanggal_kontrol']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_ttv_model->list_dokter();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			$list_template_assement=$this->Tpoliklinik_ranap_model->list_template_imp_mpp();
			
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['template_id']='';
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_imp_mpp();
			// print_r($setting_assesmen);exit;
			// print_r($data_assemen);exit;
			$data = array_merge($data,$setting_assesmen,$data_assemen);
			// $data['pendaftaran_id']=$pendaftaran_id;
		}
		// EVALUASI MPP
		if ($menu_kiri=='input_evaluasi_mpp' || $menu_kiri=='his_evaluasi_mpp'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_evaluasi_mpp'){
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_evaluasi_mpp_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpoliklinik_ranap_model->get_data_evaluasi_mpp_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_evaluasi_mpp_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tpoliklinik_ranap_model->get_data_evaluasi_mpp($pendaftaran_id,$st_ranap);
				// print_r($data_assemen);exit;
				
				
			}
			if ($data_assemen){
				// $data_assemen['']=$arr=explode(',',$str);
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['tanggal_kontrol']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_ttv_model->list_dokter();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			$list_template_assement=$this->Tpoliklinik_ranap_model->list_template_evaluasi_mpp();
			
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['template_id']='';
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_evaluasi_mpp();
			// print_r($setting_assesmen);exit;
			// print_r($data_assemen);exit;
			$data = array_merge($data,$setting_assesmen,$data_assemen);
			// $data['pendaftaran_id']=$pendaftaran_id;
		}
		
		//MPP
		if ($menu_kiri=='input_mpp' || $menu_kiri=='his_mpp'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_mpp'){
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_mpp_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpoliklinik_ranap_model->get_data_mpp_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_mpp_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tpoliklinik_ranap_model->get_data_mpp($pendaftaran_id,$st_ranap);
				// print_r($data_assemen);exit;
				
				
			}
			if ($data_assemen){
				// $data_assemen['']=$arr=explode(',',$str);
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['tanggal_kontrol']='';
				$data_assemen['gambar_id']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_ttv_model->list_dokter();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			$list_template_assement=$this->Tpoliklinik_ranap_model->list_template_mpp();
			
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['template_id']='';
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_mpp();
			// print_r($setting_assesmen);exit;
			// print_r($data_assemen);exit;
			$data = array_merge($data,$setting_assesmen,$data_assemen);
			// $data['pendaftaran_id']=$pendaftaran_id;
		}
		
		//PERENCANAAN
		if ($menu_kiri=='input_perencanaan_ranap' || $menu_kiri=='his_perencanaan_ranap'){
			$data_label=$this->Tpoliklinik_ranap_model->setting_label_perencanaan();
			if ($trx_id=='#'){
				
				$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_per($pendaftaran_id,$st_ranap);
				// print_r($data_assemen);exit;
				
			}else{
				if ($menu_kiri=='his_perencanaan_ranap'){
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_rencana_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpoliklinik_ranap_model->get_data_assesmen_rencana_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
				}else{
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_per($trx_id);
				}
					// print_r($data_assemen);exit;
			}
			if ($data_assemen){
				$data_assemen['rencana_pemeriksaan_arr']=($data_assemen['rencana_pemeriksaan']?explode(",",$data_assemen['rencana_pemeriksaan']):array());
				$data_assemen['rencana_pelayanan_arr']=($data_assemen['rencana_pelayanan']?explode(",",$data_assemen['rencana_pelayanan']):array());
				$rencana_pemeriksaan=$data_assemen['rencana_pemeriksaan'];
				if ($rencana_pemeriksaan){
					$q="SELECT GROUP_CONCAT(H.ref SEPARATOR ', ' ) as hasil  FROM merm_referensi H
						WHERE H.ref_head_id='120' AND (H.nilai) IN (".$rencana_pemeriksaan.") ";
					$data['rencana_pemeriksaan_str']=$this->db->query($q)->row('hasil');
				}else{
					$data['rencana_pemeriksaan_str']='';
				}
				$rencana_pelayanan=$data_assemen['rencana_pelayanan'];
				if ($rencana_pelayanan){
					$q="SELECT GROUP_CONCAT(H.ref SEPARATOR ', ' ) as hasil  FROM merm_referensi H
						WHERE H.ref_head_id='121' AND (H.nilai) IN (".$rencana_pelayanan.") ";
					$data_assemen['rencana_pelayanan_str']=$this->db->query($q)->row('hasil');
				}else{
					$data_assemen['rencana_pelayanan_str']='';
				}
			}else{
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['rencana_pemeriksaan_arr']=array();
				$data_assemen['rencana_pelayanan_arr']=array();
			}
				
			$data = array_merge($data, $data_label,$data_assemen);
		}
		//PERENCANAAN BEDAH
		if ($menu_kiri=='input_perencanaan_bedah' || $menu_kiri=='his_perencanaan_bedah'){
			$data_label=$this->Tpoliklinik_ranap_model->setting_label_perencanaan_bedah();
			if ($trx_id=='#'){
				
				$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_per_bedah($pendaftaran_id,$st_ranap);
				// print_r($data_assemen);exit;
				
			}else{
				if ($menu_kiri=='his_perencanaan_bedah'){
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_rencana_bedah_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpoliklinik_ranap_model->get_data_assesmen_rencana_bedah_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_per_bedah($trx_id);
				}
			}
			if ($data_assemen){
				
			}else{
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['rencana_pemeriksaan_arr']=array();
				$data_assemen['rencana_pelayanan_arr']=array();
			}
				
			$data = array_merge($data, $data_label,$data_assemen);
		}
		//ESTIMASI BIAYA
		if ($menu_kiri=='input_estimasi_biaya' || $menu_kiri=='his_estimasi_biaya'){
			$data_label=$this->Tpoliklinik_ranap_model->setting_label_estimasi_biaya();
			if ($trx_id=='#'){
				
				$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_rencana_biaya($pendaftaran_id,$st_ranap);
				// print_r($data_assemen);exit;
				
			}else{
				if ($menu_kiri=='his_estimasi_biaya'){
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_estimasi_biaya_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpoliklinik_ranap_model->get_data_assesmen_estimasi_biasa_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_rencana_biaya($trx_id);
				}
			}
			if ($data_assemen){
				
			}else{
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['rencana_pemeriksaan_arr']=array();
				$data_assemen['rencana_pelayanan_arr']=array();
			}
				
			$data = array_merge($data, $data_label,$data_assemen);
		}
		
		
		$data['versi_edit']=$versi_edit;
		$data['trx_id']=$trx_id;
		// $data['pendaftaran_id']=$pendaftaran_id;
		$data['list_ppa_dokter']=$this->Tpoliklinik_ranap_model->list_ppa_dokter();
		$data['list_tujuan']=$this->Tpoliklinik_ranap_model->list_tujuan();
		$data['list_dokter']=$this->Tpoliklinik_ranap_model->list_dokter_all();
		$data['list_dokter_all']=$this->Tpoliklinik_ranap_model->list_dokter_all();
		// print_r($data['list_dokter_all']);exit;
		$data['list_poli']=$this->Tpoliklinik_ranap_model->list_poliklinik();
		$data = array_merge($data, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	
  function create_assesmen_perencanaan_ranap(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_permintaan' => date('Y-m-d H:i:s'),
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'dpjp' => $dpjp,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'tanggal_masuk' => date('Y-m-d'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_ranap_perencanaan',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_rencana_ranap(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$rencana_pelayanan=$this->input->post('rencana_pelayanan');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		if ($rencana_pelayanan){
			$rencana_pelayanan=implode(",", $rencana_pelayanan);
		}
		$rencana_pemeriksaan=$this->input->post('rencana_pemeriksaan');
		if ($rencana_pemeriksaan){
			$rencana_pemeriksaan=implode(",", $rencana_pemeriksaan);
		}
		$data=array(
			'dengan_diagnosa' => $this->input->post('dengan_diagnosa'),
			'catatan' => $this->input->post('catatan'),
			'tipe' => $this->input->post('tipe'),
			'dpjp' => $this->input->post('dpjp'),
			'tanggal_masuk' => YMDFormat($this->input->post('tanggal_masuk')),
			'rencana_pemeriksaan' => $rencana_pemeriksaan,
			'rencana_pelayanan' => $rencana_pelayanan,
			// 'dpjp' => $this->input->post('dpjp'),
			'status_assemen' =>  $this->input->post('status_assemen'),

		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_ranap_perencanaan_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_perencanaan',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_rencana_ranap(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_ranap_perencanaan',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_perencanaan_ranap(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_rencana_ranap($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$tipe=$this->input->post('tipe');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($tipe!='#'){
			$where .=" AND H.tipe = '".$tipe."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					FROM tpoliklinik_ranap_perencanaan H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
		$akses_general=$this->db->query("SELECT *FROM setting_rencana_ranap")->row_array();
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_perencanaan/input_perencanaan_ranap/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tpoliklinik_ranap/cetak_rencana_ranap/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_rencana_ranap']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_rencana_ranap']=='0'){
			  $btn_hapus='';
			}
			if ($logic_akses_assesmen['st_cetak_rencana_ranap']=='0'){
			  $btn_cetak='';
			}
			// if ($r->nopermintaan){
				
			// }else{
				// $btn_email='';
				// $btn_email_his='';
			// }
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_perencanaan_ranap/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				$ket_edit='<br>'.($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			}
			
			// if ($r->profesi_id !=$login_profesi_id){
				// $btn_edit='';
				// $btn_hapus='';
			// }
			$jenis_surat=1;
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
		$aksi=$btn_edit.$btn_duplikasi.$btn_hapus.$btn_lihat.$btn_cetak.$aksi_edit.$ket_edit.$btn_email.$btn_email_his;
		$result[] = $aksi;
		$result[] = '<span>'.($r->nopermintaan).'</span>';
		$result[] = ($r->nopendaftaran);
		$result[] = ($r->tipe=='3'?'RAWAT INAP':'ODS');
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->dengan_diagnosa);
		$result[] =$r->catatan;
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$result[] = ($r->st_kunjungan==1?text_primary('Pasien Masuk Rawat'):text_default('Pasien Belum Masuk'));
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function list_perencanaan_ranap_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_rencana_ranap($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$tipe=$this->input->post('tipe');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($tipe!='#'){
			$where .=" AND H.tipe = '".$tipe."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					FROM tpoliklinik_ranap_perencanaan H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
		$akses_general=$this->db->query("SELECT *FROM setting_rencana_ranap")->row_array();
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_perencanaan/input_perencanaan_ranap/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tpoliklinik_ranap/cetak_rencana_ranap/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_rencana_ranap']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_rencana_ranap']=='0'){
			  $btn_hapus='';
			}
			if ($logic_akses_assesmen['st_cetak_rencana_ranap']=='0'){
			  $btn_cetak='';
			}
			
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_perencanaan_ranap/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				$ket_edit='<br>'.($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			}
			
			// if ($r->profesi_id !=$login_profesi_id){
				// $btn_edit='';
				// $btn_hapus='';
			// }
			$jenis_surat=1;
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
					
		$aksi=$btn_edit.$btn_duplikasi.$btn_hapus.$btn_lihat.$btn_cetak.$aksi_edit.$ket_edit.$btn_email.$btn_email_his;
		$result[] = $aksi;
		$result[] = '<span>'.($r->nopermintaan).'</span>';
		$result[] = ($r->nopendaftaran);
		$result[] = ($r->tipe=='3'?'RAWAT INAP':'ODS');
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->dengan_diagnosa);
		$result[] =$r->catatan;
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$result[] = ($r->st_kunjungan==1?text_primary('Pasien Masuk Rawat'):text_default('Pasien Belum Masuk'));
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function hapus_record_rencana_ranap(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ranap_perencanaan',$data);
		
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	public function cetak_rencana_ranap($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_rencana_ranap_label WHERE id='1'";
		$data=$this->db->query($q)->row_array();
		$q="SELECT JK.ref as jenis_kelamin
			,MDO.nama as nama_dokter
			,CASE WHEN MC.tipepegawai='2' THEN MD2.nama ELSE MD.nama END as nama_dokter_ttd
			,CASE WHEN MC.tipepegawai='2' THEN MD2.id ELSE MD.id END as iddokter_ttd
			,H.* 
			FROM tpoliklinik_ranap_perencanaan H
			LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
			LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=MP.jenis_kelamin
			LEFT JOIN mdokter MDO ON MDO.id=H.dpjp
			LEFT JOIN mdokter MD ON MD.id=MP.iddokter
			LEFT JOIN mppa MC ON MC.id=H.created_ppa 
			LEFT JOIN mdokter MD2 ON MD2.id = MC.pegawai_id AND MC.tipepegawai='2'
			WHERE H.assesmen_id='$assesmen_id'";
		
		$header=$this->db->query($q)->row_array();
		$data=array_merge($data,$header);
		// print_r($header);exit;
        $data['title']=$data['judul_per_ina'] .' '.$data['nopermintaan'];
		$data = array_merge($data, backend_info());
		$rencana_pemeriksaan=$data['rencana_pemeriksaan'];
		if ($rencana_pemeriksaan){
			$q="SELECT GROUP_CONCAT(H.ref SEPARATOR ', ' ) as hasil  FROM merm_referensi H
				WHERE H.ref_head_id='120' AND (H.nilai) IN (".$rencana_pemeriksaan.") ";
			$data['rencana_pemeriksaan_str']=$this->db->query($q)->row('hasil');
		}else{
			$data['rencana_pemeriksaan_str']='';
		}
		$rencana_pelayanan=$data['rencana_pelayanan'];
		if ($rencana_pelayanan){
			$q="SELECT GROUP_CONCAT(H.ref SEPARATOR ', ' ) as hasil  FROM merm_referensi H
				WHERE H.ref_head_id='121' AND (H.nilai) IN (".$rencana_pelayanan.") ";
			$data['rencana_pelayanan_str']=$this->db->query($q)->row('hasil');
		}else{
			$data['rencana_pelayanan_str']='';
		}
		// $data_ttd=$this->Tpoliklinik_ranap_model->get_ttd_dokter($data['dpjp']);
		// print_r(base_64_encode(file_get_contents($data_ttd)));exit();
		// print_r("<pre>");
		// print_r($data_ttd);
		// print_r("</pre>");
		// exit();
		// $data['data_ttd']=$data_ttd;
        $html = $this->load->view('Tpoliklinik_ranap/pdf_rencana_ranap', $data,true);
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        if ($st_create=='1'){
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	
	}
	public function kirim_email()
    {
		$assesmen_id=$this->input->post('assesmen_id');
		$jenis_surat=$this->input->post('jenis_surat');
		// $jenis_surat=1;
		$list_email=$this->input->post('list_email');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
        $data_udpate=array(
			'assesmen_id' => $assesmen_id,
			'sent_by' => $login_ppa_id,
			'sent_date' => date('Y-m-d H:i:s'),
			'jenis_surat' => $jenis_surat,
			'alamat_email' => $list_email,
			'status_terkirim' => 0,

		);
		if ($jenis_surat=='1'){$this->cetak_rencana_ranap($assesmen_id,1);};//Rencana Ranap
		if ($jenis_surat=='2'){$this->Tpoliklinik_rm_order_model->cetak_rencana_rehab($assesmen_id,1);};//Rencana Rehab
		if ($jenis_surat=='3'){$this->Tpendaftaran_ranap_erm_model->cetak_ringkasan_pulang($assesmen_id,1);};//Assesmen Pulang
		if ($jenis_surat=='4'){$this->Tpendaftaran_ranap_erm_model->cetak_assesmen_ringkasan_pulang($assesmen_id,1);};//Ringkasan Pulang
		if ($jenis_surat=='5'){$this->Tpendaftaran_ranap_erm_model->cetak_info_ods($assesmen_id,1);};//INFO ODS
		
        $hasil=$this->db->insert('tpoliklinik_ranap_perencanaan_email', $data_udpate);
		$id_log=$this->db->insert_id();
        $emailArray = explode(',', $list_email);
		if ($jenis_surat=='1'){
			$q="
				SELECT H.judul_per_ina as judul
				,CONCAT('<p>','Kepada Yth, {nama_pasien}<br><br>Terlampir ',H.judul_per_ina,' atas nama {nama_pasien}<br><br>Atas Perhatiannya Kami ucapkan terima kasih</p>') as body
				,'RSKB Halmahera Siaga' as footer_email FROM setting_rencana_ranap_label H LIMIT 1
			";
		}
		if ($jenis_surat=='2'){
			$q="
				SELECT H.judul_per_ina as judul
				,CONCAT('<p>','Kepada Yth, {nama_pasien}<br><br>Terlampir ',H.judul_per_ina,' atas nama {nama_pasien}<br><br>Atas Perhatiannya Kami ucapkan terima kasih</p>') as body
				,'RSKB Halmahera Siaga' as footer_email FROM setting_rm_label_perencanaan H LIMIT 1
			";
		}
		if ($jenis_surat=='3'){
			$q="
				SELECT H.judul_header_ina as judul
				,CONCAT('<p>','Kepada Yth, {nama_pasien}<br><br>Terlampir ',H.judul_header_ina,' atas nama {nama_pasien}<br><br>Atas Perhatiannya Kami ucapkan terima kasih</p>') as body
				,'RSKB Halmahera Siaga' as footer_email FROM setting_assesmen_pulang H LIMIT 1
			";
		}
		if ($jenis_surat=='4'){
			$q="
				SELECT H.judul_header_ina as judul
				,CONCAT('<p>','Kepada Yth, {nama_pasien}<br><br>Terlampir ',H.judul_header_ina,' atas nama {nama_pasien}<br><br>Atas Perhatiannya Kami ucapkan terima kasih</p>') as body
				,'RSKB Halmahera Siaga' as footer_email FROM setting_ringkasan_pulang H LIMIT 1
			";
		}
		if ($jenis_surat=='5'){
			$q="
				SELECT 'FORMULIR PEMEBERIAN INFORMASI ONE DAY SURGEY' judul
				,CONCAT('<p>','Kepada Yth, {nama_pasien}<br><br>Terlampir ',H.judul_header,' atas nama {nama_pasien}<br><br>Atas Perhatiannya Kami ucapkan terima kasih</p>') as body
				,'RSKB Halmahera Siaga' as footer_email FROM setting_info_ods H LIMIT 1
			";
		}
		
		$pengaturan_email=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
        // $pengaturan_email = (array) get_row('merm_pengaturan_pengiriman_email_laboratorium');
		if ($jenis_surat=='1'){$q="SELECT nopermintaan,nama_pasien from tpoliklinik_ranap_perencanaan WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='2'){$q="SELECT nopermintaan,nama_pasien from tpoliklinik_rm_perencanaan WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='3'){$q="SELECT M.nama as nopermintaan,M.nama as nama_pasien FROM `tranap_assesmen_pulang` H INNER JOIN mfpasien M ON M.id=H.idpasien WHERE H.assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='4'){$q="SELECT M.no_medrec as nopermintaan,M.nama as nama_pasien FROM `tranap_ringkasan_pulang` H INNER JOIN mfpasien M ON M.id=H.idpasien WHERE H.assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='5'){$q="SELECT M.no_medrec as nopermintaan,M.nama as nama_pasien FROM `tranap_info_ods` H INNER JOIN mfpasien M ON M.id=H.idpasien WHERE H.assesmen_id='$assesmen_id'";}
        $data_transaksi = $this->db->query($q)->row();
        // $email_tujuan = $this->input->post('email_kirim_hasil');

        // $file = $this->model->get_file_hasil_pemeriksaan($id);
        // $hasil_pemeriksaan_id = $file->id;
        // $hasil_pemeriksaan_file = $file->nama_file;

        // $fileName = $hasil_pemeriksaan_file;
        // $folderPath = "report/hasil_pemeriksaan_laboratorium_bank_darah/{$id}";
        $filePath = "";

        // $this->model->log_kirim_hasil($this->prepareLogDataEmail($hasil_pemeriksaan_id, $dataUpdate));

        $this->sendEmail($pengaturan_email, $data_transaksi, $emailArray, $id_log);
    }
	private function sendEmail($pengaturan_email, $data_transaksi, $emailArray, $id_log)
	{
		// print_r($data_transaksi);exit;
		$nama_file=$pengaturan_email['judul'].' '.$data_transaksi->nopermintaan.'.pdf';
		// $nama_file=$data_transaksi->nopermintaan.'.pdf';
		// print_r($nama_file);exit;
		$uploadDir_asset = './assets/upload/lap_bedah/';
		
		$mail = new PHPMailer(true);
		$file_hapus=$uploadDir_asset.$nama_file;
		try {
			// Server settings
			$this->configureEmail($mail);

			// Recipients
			$mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
			foreach ($emailArray as $email) {
				$mail->addAddress($email, $data_transaksi->nama_pasien);
			}
			$mail->addAttachment($uploadDir_asset.$nama_file);

			
			// Content
			$bodyHTML = str_replace('{nama_pasien}', '<b>' . $data_transaksi->nama_pasien . '</b>', $pengaturan_email['body']) . '<br>' . $pengaturan_email['footer_email'];

			$mail->isHTML(true);
			$mail->Subject = strip_tags($pengaturan_email['judul']).' '.strip_tags($data_transaksi->nopermintaan);
			$mail->Body = $bodyHTML;

			$mail->send();

			$response = [
				'status' => true,
				'message' => 'success',
			];
			$this->db->where('id',$id_log);
			$this->db->update('tpoliklinik_ranap_perencanaan_email',array('status_terkirim'=>1));
			unlink($file_hapus);
			
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		} catch (Exception $e) {
			$response = [
				'status' => false,
				'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
			];

			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		}
	}
	 private function configureEmail($mail)
    {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $mail->Host = 'mail.halmaherasiaga.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'erm@halmaherasiaga.com';
        $mail->Password = '{T,_Xun}9{@5';
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port = 465;
    }
	function create_pdf($assesmen_id){
	  $this->cetak_rencana_ranap($assesmen_id,1);
    }
	
	function list_surat_email(){
		$assesmen_id=$this->input->post('assesmen_id');
		$jenis_surat=$this->input->post('jenis_surat');
		$this->select = array();
		$from="
				(
					SELECT H.*,mppa.nama FROM tpoliklinik_ranap_perencanaan_email H
					LEFT JOIN mppa ON mppa.id=H.sent_by
					WHERE H.assesmen_id='$assesmen_id' AND H.jenis_surat='$jenis_surat'
					ORDER BY H.id ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
			
		$result[] = ($no);
		$result[] = ($r->nama);
		$result[] = HumanDateLong($r->sent_date);
		$result[] = ($r->alamat_email);
		$result[] = ($r->status_terkirim=='1'?text_success('TERKIRIM'):text_danger('GAGAL'));
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
	function create_with_template_rencana_ranap(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_ranap_perencanaan (
			pendaftaran_id,idpasien,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,tanggal_masuk,rencana_pemeriksaan,rencana_pelayanan
			,created_date,created_ppa,status_assemen
			)
			SELECT 
			  '$pendaftaran_id','$idpasien',iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,NOW(),rencana_pemeriksaan,rencana_pelayanan
			 ,NOW(),'$login_ppa_id',1
			FROM `tpoliklinik_ranap_perencanaan`

			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
	  $hasil=$this->db->query($q);
	 
	  $this->output->set_output(json_encode($hasil));
   }
   function save_edit_assesmen_rencana_ranap(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_ranap_perencanaan WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_assesmen_rencana_ranap($assesmen_id,$jml_edit);
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			// $login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_ranap_perencanaan',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$hasil=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_per($assesmen_id);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_history_assesmen_rencana_ranap($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_ranap_perencanaan_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			
			$q="INSERT IGNORE INTO tpoliklinik_ranap_perencanaan_his (versi_edit,assesmen_id,
				nopermintaan,idtipe,idpoliklinik,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,deleted_ppa,deleted_date,tanggal_permintaan,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,tanggal_masuk,rencana_pemeriksaan,rencana_pelayanan,nama_pasien,nomedrec_pasien,ttl_pasien,umur_pasien,edited_ppa,edited_date,jumlah_kunjungan,st_kunjungan,pendaftaran_ranap_id,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited)
				SELECT '$jml_edit',assesmen_id,
				nopermintaan,idtipe,idpoliklinik,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,deleted_ppa,deleted_date,tanggal_permintaan,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,tanggal_masuk,rencana_pemeriksaan,rencana_pelayanan,nama_pasien,nomedrec_pasien,ttl_pasien,umur_pasien,edited_ppa,edited_date,jumlah_kunjungan,st_kunjungan,pendaftaran_ranap_id,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited
				FROM tpoliklinik_ranap_perencanaan 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		
		}
	  return $hasil;
	}
	//BEDAH
	
	function create_assesmen_perencanaan_bedah(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_permintaan' => date('Y-m-d H:i:s'),
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'pendaftaran_id' => $pendaftaran_id,
		'st_ranap' => $st_ranap,
		'dpjp' => $dpjp,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'tanggal_bedah' => date('Y-m-d'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_ranap_perencanaan_bedah',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_rencana_bedah(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$rencana_pelayanan=$this->input->post('rencana_pelayanan');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		
		$rencana_tindakan=$this->input->post('rencana_tindakan');
		
		$tanggal_bedah=YMDFormat($this->input->post('tanggalbedah')).' '.HISTimeFormat($this->input->post('waktubedah'));
		$data=array(
			'dengan_diagnosa' => $this->input->post('dengan_diagnosa'),
			'catatan' => $this->input->post('catatan'),
			'tipe' => $this->input->post('tipe'),
			'dpjp' => $this->input->post('dpjp'),
			'tanggal_bedah' => $tanggal_bedah,
			'rencana_tindakan' => $rencana_tindakan,
			'status_assemen' =>  $this->input->post('status_assemen'),

		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_ranap_perencanaan_bedah_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_perencanaan_bedah',$data);
		if ($status_assemen=='2'){
			$q="SELECT *FROM tpoliklinik_ranap_perencanaan_bedah H WHERE H.assesmen_id='$assesmen_id'";
			
			$data_ass=$this->db->query($q)->row();
			$data_ko=array(
				'idasalpendaftaran'=>($data_ass->idtipe=='1'?'1':'2'),
				'idpendaftaran'=>($data_ass->idtipe=='1'?$data_ass->pendaftaran_id:$data_ass->pendaftaran_id_ranap),
				'tipe'=>($data_ass->tipe=='3'?'1':'2'),
				'idpasien'=>$data_ass->idpasien,
				'namapasien'=>$data_ass->nama_pasien,
				'tanggaloperasi'=> YMDFormat($data_ass->tanggal_bedah),
				'waktumulaioperasi'=>HumanTime($this->input->post('waktumulaioperasi')),
				'waktuselesaioperasi'=>null,
				'diagnosa'=>$data_ass->dengan_diagnosa,
				'operasi'=>$data_ass->rencana_tindakan,
				'namapetugas'=>$this->session->userdata('user_name'),
				'idapprove'=>$this->session->userdata('user_id'),
				'catatan'=>$data_ass->catatan,
				'dpjp_id'=>$data_ass->dpjp,
				'st_ranap'=>$data_ass->st_ranap,
				'statussetuju'=>0,
				'statusdatang'=>$data_ass->st_ranap,
				'status'=>'1',
				'tanggal_diajukan'=>date('Y-m-d H:i:s'),
				'user_pengaju'=>$this->session->userdata('user_id'),
			);
			$this->db->insert('tkamaroperasi_pendaftaran',$data_ko);
			$pendaftaran_bedah_id= $this->db->insert_id();
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->update('tpoliklinik_ranap_perencanaan_bedah',array('pendaftaran_bedah_id'=>$pendaftaran_bedah_id));
		}
		
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_rencana_bedah(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_ranap_perencanaan_bedah',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_perencanaan_bedah(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_rencana_bedah($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$tipe=$this->input->post('tipe');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($tipe!='#'){
			$where .=" AND H.tipe = '".$tipe."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					FROM tpoliklinik_ranap_perencanaan_bedah H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
		$akses_general=$this->db->query("SELECT *FROM setting_rencana_bedah")->row_array();
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_perencanaan/input_perencanaan_bedah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_perencanaan/input_perencanaan_bedah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tpoliklinik_ranap/cetak_rencana_bedah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_rencana_bedah']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_rencana_bedah']=='0'){
			  $btn_hapus='';
			}
			if ($logic_akses_assesmen['st_cetak_rencana_bedah']=='0'){
			  $btn_cetak='';
			}
			
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_perencanaan_bedah/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				$ket_edit='<br>'.($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			}
			
			// if ($r->profesi_id !=$login_profesi_id){
				// $btn_edit='';
				// $btn_hapus='';
			// }
		$aksi=$btn_edit.$btn_duplikasi.$btn_hapus.$btn_lihat.$btn_cetak.$aksi_edit.$ket_edit;
		$result[] = $aksi;
		$result[] = '<span>'.($r->nopermintaan).'</span>';
		$result[] = ($r->nopendaftaran);
		$result[] = ($r->tipe=='3'?'RAWAT INAP':'ODS');
		$result[] = ($r->nama_dpjp);
		$result[] = HumanDateShort($r->tanggal_bedah).' '.HumanTimeShort($r->tanggal_bedah);
		$result[] = ($r->dengan_diagnosa);
		$result[] =$r->catatan;
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$result[] = ($r->st_kunjungan==1?text_primary('Pasien Masuk Rawat'):text_default('Pasien Belum Masuk'));
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function list_perencanaan_bedah_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_rencana_bedah($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$tipe=$this->input->post('tipe');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($tipe!='#'){
			$where .=" AND H.tipe = '".$tipe."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					FROM tpoliklinik_ranap_perencanaan_bedah H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
		$akses_general=$this->db->query("SELECT *FROM setting_rencana_bedah")->row_array();
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/input_perencanaan_bedah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tpoliklinik_ranap/cetak_rencana_bedah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_rencana_bedah']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_rencana_bedah']=='0'){
			  $btn_hapus='';
			}
			if ($logic_akses_assesmen['st_cetak_rencana_bedah']=='0'){
			  $btn_cetak='';
			}
			
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_perencanaan_bedah/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				$ket_edit='<br>'.($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			}
			
			// if ($r->profesi_id !=$login_profesi_id){
				// $btn_edit='';
				// $btn_hapus='';
			// }
		$aksi=$btn_edit.$btn_duplikasi.$btn_hapus.$btn_lihat.$btn_cetak.$aksi_edit.$ket_edit;
		$result[] = $aksi;
		$result[] = '<span>'.($r->nopermintaan).'</span>';
		$result[] = ($r->nopendaftaran);
		$result[] = ($r->tipe=='3'?'RAWAT INAP':'ODS');
		$result[] = ($r->nama_dpjp);
		$result[] = HumanDateShort($r->tanggal_bedah).' '.HumanTimeShort($r->tanggal_bedah);
		$result[] = ($r->dengan_diagnosa);
		$result[] =$r->catatan;
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$result[] = ($r->st_kunjungan==1?text_primary('Pasien Masuk Rawat'):text_default('Pasien Belum Masuk'));
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function hapus_record_rencana_bedah(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ranap_perencanaan_bedah',$data);
		
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	public function cetak_rencana_bedah($assesmen_id){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_rencana_bedah_label WHERE id='1'";
		$data=$this->db->query($q)->row_array();
		$q="SELECT JK.ref as jenis_kelamin,MD.nama as nama_dokter,H.* FROM tpoliklinik_ranap_perencanaan_bedah H
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=MP.jenis_kelamin
			LEFT JOIN mdokter MD ON MD.id=H.dpjp
			WHERE H.assesmen_id='$assesmen_id'";
		
		$header=$this->db->query($q)->row_array();
		// print_r($data);exit;
		$data=array_merge($data,$header);
        $data['title']=$data['judul_per_ina'] .' '.$data['nopermintaan'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpoliklinik_ranap/pdf_rencana_bedah', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
	}
	function create_with_template_rencana_bedah(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_ranap_perencanaan_bedah (
			pendaftaran_id,idpasien,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,tanggal_bedah,rencana_tindakan
			,created_date,created_ppa,status_assemen
			)
			SELECT 
			  '$pendaftaran_id','$idpasien',iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,NOW(),rencana_tindakan
			 ,NOW(),'$login_ppa_id',1
			FROM `tpoliklinik_ranap_perencanaan_bedah`

			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
	  $hasil=$this->db->query($q);
	 
	  $this->output->set_output(json_encode($hasil));
   }
   function save_edit_assesmen_rencana_bedah(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_ranap_perencanaan_bedah WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_assesmen_rencana_bedah($assesmen_id,$jml_edit);
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			// $login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_ranap_perencanaan_bedah',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$hasil=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_per($assesmen_id);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_history_assesmen_rencana_bedah($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_ranap_perencanaan_bedah_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			
			$q="INSERT IGNORE INTO tpoliklinik_ranap_perencanaan_bedah_his (versi_edit,assesmen_id,
				nopermintaan,idtipe,idpoliklinik,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,deleted_ppa,deleted_date,tanggal_permintaan,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,tanggal_bedah,rencana_tindakan,nama_pasien,nomedrec_pasien,ttl_pasien,umur_pasien,edited_ppa,edited_date,jumlah_kunjungan,st_kunjungan,pendaftaran_bedah_id,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited)
				SELECT '$jml_edit',assesmen_id,
				nopermintaan,idtipe,idpoliklinik,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,deleted_ppa,deleted_date,tanggal_permintaan,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,tanggal_bedah,rencana_tindakan,nama_pasien,nomedrec_pasien,ttl_pasien,umur_pasien,edited_ppa,edited_date,jumlah_kunjungan,st_kunjungan,pendaftaran_bedah_id,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited
				FROM tpoliklinik_ranap_perencanaan_bedah 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		
		}
	  return $hasil;
	}
	
	//ESTIMASI
	
	function create_template_estimasi_biaya(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_permintaan' => date('Y-m-d H:i:s'),
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'dpjp' => $dpjp,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		// 'tanggal_bedah' => date('Y-m-d'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_ranap_rencana_biaya',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	 function list_template_estimasi_biaya()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tpoliklinik_ranap_rencana_biaya H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_assesmen_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function create_assesmen_estimasi_biaya(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  $st_ranap=$this->input->post('st_ranap');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_permintaan' => date('Y-m-d H:i:s'),
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'dpjp' => $dpjp,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		// 'tanggal_bedah' => date('Y-m-d'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_ranap_rencana_biaya',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_estimasi_biaya(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$rencana_pelayanan=$this->input->post('rencana_pelayanan');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$nama_template=$this->input->post('nama_template');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		
		$rencana_tindakan=$this->input->post('rencana_tindakan');
		
		$data=array(
			'dengan_diagnosa' => $this->input->post('dengan_diagnosa'),
			'catatan' => $this->input->post('catatan'),
			'tipe' => $this->input->post('tipe'),
			'dpjp' => $this->input->post('dpjp'),
			'icu' => $this->input->post('icu'),
			'cito' => $this->input->post('cito'),
			'rencana_tindakan' => $rencana_tindakan,
			'keterangan_hapus' => $keterangan_hapus,
			'nama_template' => $nama_template,
			'status_assemen' =>  $this->input->post('status_assemen'),

		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_ranap_rencana_biaya_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function simpan_header_estimasi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'kelompok_operasi_id' => $this->input->post('kelompok_operasi_id'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'lama' => $this->input->post('lama'),
			'icu_pilih' => $this->input->post('icu_pilih'),
			'cito_pilih' => $this->input->post('cito_pilih'),
			'kelompok_pasien_id' => $this->input->post('kelompok_pasien_id'),
			'idrekanan' => $this->input->post('idrekanan'),
			'kelas' => $this->input->post('kelas'),
			'jumlah_icu' => $this->input->post('jumlah_icu'),
		);
		// print_r($assesmen_id);exit();
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya_detail',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function simpan_proses(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_nama_ppa=$this->session->userdata('login_nama_ppa');
		$assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'kelompok_operasi_id' => $this->input->post('kelompok_operasi_id'),
			'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'lama' => $this->input->post('lama'),
			'icu_pilih' => $this->input->post('icu_pilih'),
			'cito_pilih' => $this->input->post('cito_pilih'),
			'kelompok_pasien_id' => $this->input->post('kelompok_pasien_id'),
			'idrekanan' => $this->input->post('idrekanan'),
			'kelas' => $this->input->post('kelas'),
			'harga_kamar_ko' => $this->input->post('harga_kamar_ko'),
			'harga_obat_ko' => $this->input->post('harga_obat_ko'),
			'harga_implan' => $this->input->post('harga_implan'),
			'harga_do' => $this->input->post('harga_do'),
			'harga_da' => $this->input->post('harga_da'),
			'harga_daa' => $this->input->post('harga_daa'),
			'total_biaya_bedah' => $this->input->post('total_biaya_bedah'),
			'harga_ruang' => $this->input->post('harga_ruang'),
			'harga_obat_ruang' => $this->input->post('harga_obat_ruang'),
			'harga_penunjang_ruang' => $this->input->post('harga_penunjang_ruang'),
			'harga_visit_ruang' => $this->input->post('harga_visit_ruang'),
			'jumlah_icu' => $this->input->post('jumlah_icu'),
			'harga_icu' => $this->input->post('harga_icu'),
			'total_biaya_ranap' => $this->input->post('total_biaya_ranap'),
			'total_estimasi_all' => $this->input->post('total_estimasi_all'),
			'petugas_billing_ttd' => $login_nama_ppa,
			'petugas_billilng_ppa_id' => $login_ppa_id,
			'tanggal_proses' => date('Y-m-d H:i:s'),

		);
		// print_r($data);exit();
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya_detail',$data);
		$data_header=array(
			'status_proses' =>1,
			'total_estimasi' =>$this->input->post('total_estimasi_all'),
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya',$data_header);
		
		$this->output->set_output(json_encode($data));
	}
	function simpan_setuju(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_nama_ppa=$this->session->userdata('login_nama_ppa');
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_menolak=$this->input->post('alasan_menolak');
		$status_persetujuan_pasien=$this->input->post('status_persetujuan_pasien');
		$data=array(
			// 'kelompok_operasi_id' => $this->input->post('kelompok_operasi_id'),
			// 'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
			'lama' => $this->input->post('lama'),
			'icu_pilih' => $this->input->post('icu_pilih'),
			'cito_pilih' => $this->input->post('cito_pilih'),
			'kelompok_pasien_id' => $this->input->post('kelompok_pasien_id'),
			'idrekanan' => $this->input->post('idrekanan'),
			'kelas' => $this->input->post('kelas'),
			'harga_kamar_ko' => $this->input->post('harga_kamar_ko'),
			'harga_obat_ko' => $this->input->post('harga_obat_ko'),
			'harga_implan' => $this->input->post('harga_implan'),
			'harga_do' => $this->input->post('harga_do'),
			'harga_da' => $this->input->post('harga_da'),
			'harga_daa' => $this->input->post('harga_daa'),
			'total_biaya_bedah' => $this->input->post('total_biaya_bedah'),
			'harga_ruang' => $this->input->post('harga_ruang'),
			'harga_obat_ruang' => $this->input->post('harga_obat_ruang'),
			'harga_penunjang_ruang' => $this->input->post('harga_penunjang_ruang'),
			'harga_visit_ruang' => $this->input->post('harga_visit_ruang'),
			'jumlah_icu' => $this->input->post('jumlah_icu'),
			'harga_icu' => $this->input->post('harga_icu'),
			'total_biaya_ranap' => $this->input->post('total_biaya_ranap'),
			'total_estimasi_all' => $this->input->post('total_estimasi_all'),
			'petugas_billing_ttd' => $login_nama_ppa,
			'petugas_billilng_ppa_id' => $login_ppa_id,
			'tanggal_proses' => date('Y-m-d H:i:s'),

		);
		// print_r($assesmen_id);exit();
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya_detail',$data);
		$data_header=array(
			'status_proses' =>1,
			'total_estimasi' =>$this->input->post('total_estimasi_all'),
			'status_persetujuan_pasien' =>$this->input->post('status_persetujuan_pasien'),
			'alasan_menolak' =>$this->input->post('alasan_menolak'),
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya',$data_header);
		
		$this->output->set_output(json_encode($data));
	}
	function simpan_setuju_direct(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_nama_ppa=$this->session->userdata('login_nama_ppa');
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_menolak=$this->input->post('alasan_menolak');
		$status_persetujuan_pasien=$this->input->post('status_persetujuan_pasien');
		
		$data_header=array(
			'status_proses' =>1,
			'status_persetujuan_pasien' =>$this->input->post('status_persetujuan_pasien'),
			'alasan_menolak' =>$this->input->post('alasan_menolak'),
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_ranap_rencana_biaya',$data_header);
		
		$this->output->set_output(json_encode($data_header));
	}
	function batal_assesmen_estimasi_biaya(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  
	 
	  if ($st_edited=='1'){
		  $data=array(
			'edited_ppa' => null,
			'edited_date' => null,
			'st_edited' => 0,
		  );
		  $data['status_assemen']='2';  
		  $this->db->where('assesmen_id',$assesmen_id);
		  $this->db->delete('tpoliklinik_ranap_rencana_biaya_implant');
		  $q="SELECT *FROM tpoliklinik_ranap_rencana_biaya_implant_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		  $list_data=$this->db->query($q)->result();
		  foreach ($list_data as $r){
			  $data_edit=array(
				'id' => $r->id,
				'assesmen_id' => $r->assesmen_id,
				'pendaftaran_id'=> $r->pendaftaran_id,
				'idpasien'=> $r->idpasien,
				'nama_implant' => $r->nama_implant,
				'nama_implant' => $r->nama_implant,
				'ukuran_implant' => $r->ukuran_implant,
				'merk_implant' => $r->merk_implant,
				'catatan_implant' => $r->catatan_implant,
				'jumlah_implant' => $r->jumlah_implant,
				'kuantitas_implant' => $r->kuantitas_implant,
				'total_biaya_implant' => $r->total_biaya_implant,
				'created_by'=> $r->created_by,
				'created_date'=> $r->created_date,
				'edited_by'=> $r->edited_by,
				'edited_date'=> $r->edited_date,

				);
			  $this->db->insert('tpoliklinik_ranap_rencana_biaya_implant',$data_edit);
		  }
		  $q="delete from tpoliklinik_ranap_rencana_biaya_his where assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		  $this->db->query($q);
		  $q="delete from tpoliklinik_ranap_rencana_biaya_implant_his where assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		  $this->db->query($q);
	  }else{
		  $data=array(
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
			'st_edited' => 0,
			'edited_ppa' => null,
			'edited_date' => null,
			
		  );
		  $data['status_assemen']='0';
	  }
	 
	  
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_ranap_rencana_biaya',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_estimasi_biaya(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_estimasi_biaya($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$tipe=$this->input->post('tipe');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($tipe!='#'){
			$where .=" AND H.tipe = '".$tipe."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,C.ref as nama_cito,I.ref as nama_icu
					,MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					FROM tpoliklinik_ranap_rencana_biaya H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN merm_referensi C ON C.nilai=H.cito AND C.ref_head_id='123'
					LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
		$akses_general=$this->db->query("SELECT *FROM setting_rencana_biaya")->row_array();
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/input_estimasi_biaya/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tpoliklinik_ranap/cetak_estimasi_biaya/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_estimasi_biaya']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_estimasi_biaya']=='0'){
			  $btn_hapus='';
			}
			if ($logic_akses_assesmen['st_cetak_estimasi_biaya']=='0'){
			  $btn_cetak='';
			}
			
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_estimasi_biaya/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				$ket_edit='<br>'.($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			}
			
			// if ($r->profesi_id !=$login_profesi_id){
				// $btn_edit='';
				// $btn_hapus='';
			// }
		$aksi=$btn_edit.$btn_duplikasi.$btn_hapus.$btn_lihat.$btn_cetak.$aksi_edit.$ket_edit;
		$result[] = $aksi;
		$result[] = '<span><strong>'.($r->nopermintaan).'</strong></span>'.'<br><span class="text-primary">'.($r->nopendaftaran).'</span>';
		$result[] = ('<span class="text-primary">TIPE PELAYANAN </span>: <span class="text-muted">'.($r->tipe=='3'?'RAWAT INAP':'ODS').'</span><br><span class="text-primary">ICU </span>: <span class="text-muted">'.$r->nama_icu.'</span><br><span class="text-primary">CITO </span> : <span class="text-muted">'.$r->nama_cito).'</span>';
		$result[] = ($r->dengan_diagnosa);
		$result[] = ($r->rencana_tindakan);
		$result[] = ($r->catatan);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->status_proses==1?text_primary('Telah Diproses'):text_default('Menunggu Diproses'));
		$result[] = ($r->total_estimasi);
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function list_estimasi_biaya_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_estimasi_biaya($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$tipe=$this->input->post('tipe');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($tipe!='#'){
			$where .=" AND H.tipe = '".$tipe."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,C.ref as nama_cito,I.ref as nama_icu
					,MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					FROM tpoliklinik_ranap_rencana_biaya H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN merm_referensi C ON C.nilai=H.cito AND C.ref_head_id='123'
					LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
		$akses_general=$this->db->query("SELECT *FROM setting_rencana_biaya")->row_array();
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/input_estimasi_biaya/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tpoliklinik_ranap/cetak_estimasi_biaya/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_estimasi_biaya']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_estimasi_biaya']=='0'){
			  $btn_hapus='';
			}
			if ($logic_akses_assesmen['st_cetak_estimasi_biaya']=='0'){
			  $btn_cetak='';
			}
			
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_estimasi_biaya/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				$ket_edit='<br>'.($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			}
			
			// if ($r->profesi_id !=$login_profesi_id){
				// $btn_edit='';
				// $btn_hapus='';
			// }
		$aksi=$btn_edit.$btn_duplikasi.$btn_hapus.$btn_lihat.$btn_cetak.$aksi_edit.$ket_edit;
		$result[] = $aksi;
		$result[] = '<span><strong>'.($r->nopermintaan).'</strong></span>'.'<br><span class="text-primary">'.($r->nopendaftaran).'</span>';
		$result[] = ('<span class="text-primary">TIPE PELAYANAN </span>: <span class="text-muted">'.($r->tipe=='3'?'RAWAT INAP':'ODS').'</span><br><span class="text-primary">ICU </span>: <span class="text-muted">'.$r->nama_icu.'</span><br><span class="text-primary">CITO </span> : <span class="text-muted">'.$r->nama_cito).'</span>';
		$result[] = ($r->dengan_diagnosa);
		$result[] = ($r->rencana_tindakan);
		$result[] = ($r->catatan);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->status_proses==1?text_primary('Telah Diproses'):text_default('Menunggu Diproses'));
		$result[] = ($r->total_estimasi);
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function hapus_record_estimasi_biaya(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ranap_rencana_biaya',$data);
		
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	public function cetak_estimasi_biaya($assesmen_id){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_rencana_biaya_label WHERE id='1'";
		$data=$this->db->query($q)->row_array();
			// print_r($data);exit;
		$q="
			SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,C.ref as nama_cito,I.ref as nama_icu
					,MD.nama as nama_dpjp,H.*,MP.nopendaftaran ,UC.nama as user_created,UE.nama as nama_edit
					,D.pasien_ttd,MK.nama as nama_kelas
					FROM tpoliklinik_ranap_rencana_biaya H
					LEFT JOIN tpoliklinik_ranap_rencana_biaya_detail D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mkelas MK ON MK.id=D.kelas
					LEFT JOIN merm_referensi C ON C.nilai=H.cito AND C.ref_head_id='123'
					LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			MK.nama as nama_ko,JO.nama as nama_jo,C.ref as nama_cito_pilih,I.ref as nama_icu_pilih
			,MPK.nama as nama_KP,MR.nama as asuransi,K.nama as nama_kelas
			,H.*

			FROM tpoliklinik_ranap_rencana_biaya_detail H 
			LEFT JOIN mkelompok_operasi MK ON MK.id=H.kelompok_operasi_id
			LEFT JOIN erm_jenis_operasi JO ON JO.id=H.jenis_operasi_id
			LEFT JOIN merm_referensi C ON C.nilai=H.cito_pilih AND C.ref_head_id='123'
			LEFT JOIN merm_referensi I ON I.nilai=H.icu_pilih AND I.ref_head_id='122'
			LEFT JOIN mpasien_kelompok MPK ON MPK.id=H.kelompok_pasien_id
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN mkelas K ON K.id=H.kelas
			WHERE H.assesmen_id='$assesmen_id'";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		// print_r($q);exit;
		$data=array_merge($data,$data_header,$data_detail);
        $data['title']=$data['judul_per_ina'] .' '.$data['nopermintaan'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpoliklinik_ranap/pdf_estimasi_biaya', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
	}
	function create_with_template_estimasi_biaya(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $idtipe=$this->session->userdata('idtipe');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_ranap_rencana_biaya (
			pendaftaran_id,idpasien,idtipe,idpoliklinik,template_assesmen_id
			,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,icu,cito,rencana_tindakan,total_estimasi
			,created_date,created_ppa,status_assemen
			)
			SELECT 
			  '$pendaftaran_id','$idpasien','$idtipe','$idpoliklinik','$template_assesmen_id'
			  ,'$iddokter_peminta',dengan_diagnosa,catatan,tipe,'$dpjp',icu,cito,rencana_tindakan,total_estimasi
			 ,NOW(),'$login_ppa_id',1
			FROM `tpoliklinik_ranap_rencana_biaya`

			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	  $q="SELECT *FROM tpoliklinik_ranap_rencana_biaya_implant WHERE assesmen_id='$template_assesmen_id'";
	  $list_data=$this->db->query($q)->result();
	  foreach($list_data as $row){
		  $data=array(
				'assesmen_id' => $assesmen_id,
				'pendaftaran_id' => $pendaftaran_id,
				'idpasien' => $idpasien,
				'nama_implant' => $row->nama_implant,
				'ukuran_implant' => $row->ukuran_implant,
				'merk_implant' => $row->merk_implant,
				'catatan_implant' => $row->catatan_implant,
				'jumlah_implant' => $row->jumlah_implant,
				'kuantitas_implant' => $row->kuantitas_implant,
				'total_biaya_implant' => $row->total_biaya_implant,
				'created_by' => $login_ppa_id,
				'created_date' => date('Y-m-d H:i:s'),

		  );
		  $hasil=$this->db->insert('tpoliklinik_ranap_rencana_biaya_implant',$data);
	  }
	  //Update jumlah_template
	  $q="UPDATE tpoliklinik_ranap_rencana_biaya 
		SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
		$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
   }
   function save_edit_assesmen_estimasi_biaya(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_ranap_rencana_biaya WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_assesmen_estimasi_biaya($assesmen_id,$jml_edit);
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			// $login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_ranap_rencana_biaya',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$hasil=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_rencana_biaya($assesmen_id);
		$this->output->set_output(json_encode($hasil));
	}
	function edit_template_assesmen_estimasi_biaya(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		
		// $res=$this->simpan_history_assesmen_estimasi_biaya($assesmen_id,$jml_edit);
			
			$data=array(
				'status_assemen' => 3,
				'st_edited' => 1,
				// 'alasan_edit_id' =>$alasan_edit_id,
				// 'keterangan_edit' =>$keterangan_edit,

			);
			// $login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_ranap_rencana_biaya',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		
		$hasil=$this->Tpoliklinik_ranap_model->get_data_assesmen_trx_rencana_biaya($assesmen_id);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_history_assesmen_estimasi_biaya($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_ranap_rencana_biaya_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			
			$q="INSERT IGNORE INTO tpoliklinik_ranap_rencana_biaya_his (versi_edit,assesmen_id,
				template_assesmen_id,nama_template,jumlah_template,nopermintaan,idtipe,idpoliklinik,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,deleted_ppa,deleted_date,tanggal_permintaan,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,icu,cito,rencana_tindakan,total_estimasi,status_proses,status_persetujuan_pasien,alasan_menolak,nama_pasien,nomedrec_pasien,ttl_pasien,umur_pasien,jk_pasien,edited_ppa,edited_date,jumlah_kunjungan,st_kunjungan,pendaftaran_bedah_id,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited
				)
				SELECT '$jml_edit',assesmen_id,
				template_assesmen_id,nama_template,jumlah_template,nopermintaan,idtipe,idpoliklinik,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,deleted_ppa,deleted_date,tanggal_permintaan,iddokter_peminta,dengan_diagnosa,catatan,tipe,dpjp,icu,cito,rencana_tindakan,total_estimasi,status_proses,status_persetujuan_pasien,alasan_menolak,nama_pasien,nomedrec_pasien,ttl_pasien,umur_pasien,jk_pasien,edited_ppa,edited_date,jumlah_kunjungan,st_kunjungan,pendaftaran_bedah_id,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited
				FROM tpoliklinik_ranap_rencana_biaya 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_ranap_rencana_biaya_implant_his (versi_edit,assesmen_id,id,
				pendaftaran_id,idpasien,nama_implant,ukuran_implant,merk_implant,catatan_implant,jumlah_implant,kuantitas_implant,total_biaya_implant,created_by,created_date,edited_by,edited_date
				)
				SELECT '$jml_edit',assesmen_id,id,
				pendaftaran_id,idpasien,nama_implant,ukuran_implant,merk_implant,catatan_implant,jumlah_implant,kuantitas_implant,total_biaya_implant,created_by,created_date,edited_by,edited_date

				FROM tpoliklinik_ranap_rencana_biaya_implant 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		
		}
	  return $hasil;
	}
	function simpan_implant(){
		
		$trx_implant_id= $this->input->post('trx_implant_id');
		$this->assesmen_id = $this->input->post('assesmen_id');
		$this->pendaftaran_id = $this->input->post('pendaftaran_id');
		$this->pendaftaran_id_ranap = $this->input->post('pendaftaran_id_ranap');
		$this->st_ranap = $this->input->post('st_ranap');
		$this->idpasien = $this->input->post('idpasien');
		$this->nama_implant = $this->input->post('nama_implant');
		$this->ukuran_implant = $this->input->post('ukuran_implant');
		$this->merk_implant = $this->input->post('merk_implant');
		$this->catatan_implant = $this->input->post('catatan_implant');
		$this->jumlah_implant = $this->input->post('jumlah_implant');
		$this->kuantitas_implant = $this->input->post('jumlah_implant');
		$this->total_biaya_implant = $this->input->post('total_biaya_implant');

		
		if ($trx_implant_id==''){
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			$hasil=$this->db->insert('tpoliklinik_ranap_rencana_biaya_implant',$this);
			
		}else{
			$this->edited_by = $this->session->userdata('user_id');
			$this->edited_date = date('Y-m-d H:i:s');
			$hasil=$this->db->where('id',$trx_implant_id);
			$hasil=$this->db->update('tpoliklinik_ranap_rencana_biaya_implant',$this);
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	
	function load_implant()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,HH.status_assemen 
						FROM tpoliklinik_ranap_rencana_biaya_implant H
						LEFT JOIN tpoliklinik_ranap_rencana_biaya HH ON HH.assesmen_id=H.assesmen_id
						WHERE H.assesmen_id='$assesmen_id'
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_implant','ukuran_implant','merk_implant','catatan_implant');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nama_implant);
          $result[] = ($r->ukuran_implant);
          $result[] = ($r->merk_implant);
          $result[] = ($r->catatan_implant);
          $result[] = ($r->jumlah_implant);
          $result[] = ($r->total_biaya_implant);
         
          $aksi = '<div class="btn-group">';
		  if ($r->status_assemen!='2'){
			  $aksi .= '<button onclick="edit_implant('.$r->id.')" type="button" title="Edit Implant" class="btn btn-primary btn-xs btn_kolom"><i class="fa fa-pencil"></i></button>';	
			  $aksi .= '<button onclick="hapus_implant('.$r->id.')" type="button" title="Hapus Implant" class="btn btn-danger btn-xs  btn_kolom"><i class="fa fa-trash"></i></button>';	
		  }else{
			  $aksi .= '<button  type="button" title="Hapus Implant" class="btn btn-danger btn-xs  btn_kolom"><i class="fa fa-lock"></i> READONLY</button>';	
			  
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
   function hapus_implant(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('tpoliklinik_ranap_rencana_biaya_implant');
	  
	  $this->output->set_output(json_encode($hasil));
	  
	}
	function get_edit_implant(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM tpoliklinik_ranap_rencana_biaya_implant where id='$id'";
	  $hasil=$this->db->query($q)->row_array();
	  
	  $this->output->set_output(json_encode($hasil));
	  
	}
	
	 // MPP
  
function create_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
	  $idtipe=$this->input->post('idtipe');
	 
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		// 'tanggal_pemeriksaan' => date('Y-m-d H:i:s'),
		'st_ranap' => $st_ranap,
		'idtipe' => $idtipe,
		'idpasien' => $idpasien,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	 
	  $hasil=$this->db->insert('tranap_mpp',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tranap_mpp_pengkajian (assesmen_id,param_id,parameter_nama
		,group_nilai_id,group_nilai,group_nilai_nama)
		SELECT 
		'$assesmen_id' as assesmen_id,H.id,H.parameter
		,GROUP_CONCAT(S.id SEPARATOR ',') as group_nilai_id,GROUP_CONCAT(S.skor) as group_nilai,GROUP_CONCAT(S.deskripsi_nama SEPARATOR '#') as group_nilai_nama
		FROM setting_mpp_param H 
		LEFT JOIN setting_mpp_param_skor S ON S.parameter_id=H.id
		WHERE H.staktif='1' AND S.staktif='1'
		GROUP BY H.id
		ORDER BY H.id ASC";
	  $hasil=$this->db->query($q);
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
  function batal_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
	  }
	  if ($st_edited=='1'){
		
		  $q="DELETE FROM tranap_mpp WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  $q="DELETE FROM tranap_mpp_pengkajian WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
			$q="INSERT IGNORE INTO tranap_mpp (
			assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,berat_badan_biasa,berat_badan,waktu,tinggi_badan,nilai_imt,nilai_imt_id,nilai_imt_text,diagnosa_medis,nilai_mpp,nilai_mpp_id,penilaian_mpp,tindakan_mpp,keterangan 
			) 
				SELECT
				assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,berat_badan_biasa,berat_badan,waktu,tinggi_badan,nilai_imt,nilai_imt_id,nilai_imt_text,diagnosa_medis,nilai_mpp,nilai_mpp_id,penilaian_mpp,tindakan_mpp,keterangan
				FROM tranap_mpp_x_his WHERE assesmen_id = '$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tranap_mpp_pengkajian 
					(id,assesmen_id,param_id,parameter_nama,group_nilai_id,group_nilai,group_nilai_nama,nilai,nilai_nama)
					SELECT id,assesmen_id,param_id,parameter_nama,group_nilai_id,group_nilai,group_nilai_nama,nilai,nilai_nama
					FROM tranap_mpp_x_his_pengkajian  WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);  
			  
			$q="DELETE FROM tranap_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tranap_mpp_x_his_pengkajian WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_mpp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  
	function save_mpp(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$st_edited = $this->input->post('st_edited');
		$status_assemen = $this->input->post('status_assemen');
		$assesmen_id = $this->input->post('assesmen_id');
		
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			// 'tanggal_pemeriksaan' =>YMDFormat($this->input->post('tanggal_pemeriksaan')),
			'total_skor' => $this->input->post('total_skor'),
			'kesimpulan_mpp_id' => $this->input->post('kesimpulan_mpp_id'),
			'kesimpulan_mpp_text' => $this->input->post('kesimpulan_mpp_text'),
			'tindakan_id' => $this->input->post('tindakan_id'),
			'tindakan_nama' => $this->input->post('tindakan_nama'),
			'keterangan' => $this->input->post('keterangan'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tranap_mpp_x_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_mpp',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	// function get_mpp(){
	  
	  // $masa_tubuh=$this->input->post('masa_tubuh');
	  // $q="SELECT H.id as nilai_imt_id,H.kategori_berat as nilai_imt_text 
				// FROM mberat H 
				// WHERE '$masa_tubuh' BETWEEN H.berat_1 AND H.berat_2 AND H.staktif='1' LIMIT 1";
		// $hasil=$this->db->query($q)->row_array();
		// $this->output->set_output(json_encode($hasil));
	// }
	
  function list_history_pengkajian_mpp()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_mpp($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idrawat_ranap=$this->input->post('idrawat_ranap');
		$idtipe=$this->input->post('idtipe');
		$idruang=$this->input->post('idruang');
		$idbed=$this->input->post('idbed');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$st_ranap=$this->input->post('st_ranap');
		$where='';
		
		if ($idrawat_ranap != '#'){
			if ($st_ranap=='1'){
			$where .=" AND MP.id = '".$idrawat_ranap."'";
				
			}else{
				
			$where .=" AND MJ.id = '".$idrawat_ranap."'";
			}
		}
		if ($idruang != '#'){
			$where .=" AND MP.idruangan = '".$idruang."'";
		}
		if ($idkelas != '#'){
			$where .=" AND MP.idkelas = '".$idkelas."'";
		}
		if ($idbed != '#'){
			$where .=" AND MP.idbed = '".$idbed."'";
		}
		if ($notransaksi != ''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter != '#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		
		if ($mppa_id != '#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1  != ''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1  != ''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_assesmen_ri")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE MJ.nopendaftaran END nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE MJ.tanggaldaftar END tanggaldaftar
					,H.idtipe
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.tanggal_input as tanggal_pengkajian,H.assesmen_id,H.pendaftaran_id_ranap,H.st_ranap,H.pendaftaran_id,MPP.nama as nama_poli
					FROM `tranap_mpp` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran MJ ON MJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPP ON MPP.id=MJ.idpoliklinik
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id_ranap.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default menu_click  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/input_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
		$btn_lihat='<a class="btn btn-default menu_click  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/input_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id != $r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id != $r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_mpp']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_mpp']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			if ($r->st_ranap=='1'){
			$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/his_mpp/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning menu_click "  title="Info Edit"><i class="fa fa-info"></i></a>';
				
			}else{
			$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/his_mpp/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning menu_click "  title="Info Edit"><i class="fa fa-info"></i></a>';
				
			}
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		if ($logic_akses_assesmen['st_lihat_mpp']=='1'){
		$aksi .= $btn_lihat;	
		}
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->nopendaftaran);
		if ($r->st_ranap=='1'){
		$result[] = GetAsalRujukanRI($r->idtipe).(' <strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:''));
			
		}else{
			
		$result[] = GetAsalRujukanKwitansi($r->idtipe).' - <strong>'.$r->nama_poli.'</strong>';
		}
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa);
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	function create_template_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $idpasien=$this->input->post('idpasien');
	 
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		// 'tanggal_pemeriksaan' => date('Y-m-d H:i:s'),
		'idpasien' => $idpasien,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  
	  $hasil=$this->db->insert('tranap_mpp',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tranap_mpp_pengkajian (assesmen_id,param_id,parameter_nama
		,group_nilai_id,group_nilai,group_nilai_nama)
		SELECT 
		'$assesmen_id' as assesmen_id,H.id,H.parameter
		,GROUP_CONCAT(S.id SEPARATOR ',') as group_nilai_id,GROUP_CONCAT(S.skor) as group_nilai,GROUP_CONCAT(S.deskripsi_nama SEPARATOR '#') as group_nilai_nama
		FROM setting_mpp_param H 
		LEFT JOIN setting_mpp_param_skor S ON S.parameter_id=H.id
		WHERE H.staktif='1' AND S.staktif='1'
		GROUP BY H.id
		ORDER BY H.id ASC";
	  $hasil=$this->db->query($q);
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	function batal_template_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_mpp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function list_index_template_mpp()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tranap_mpp H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_record_mpp(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_mpp',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function edit_template_mpp(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'pendaftaran_id_ranap' => $this->input->post('pendaftaran_id_ranap'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_mpp',$data);
		
		$this->output->set_output(json_encode($result));
	}
  function create_with_template_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $st_ranap=$this->input->post('st_ranap');
	  $idtipe=$this->input->post('idtipe');
	  $idpasien=$this->input->post('idpasien');
	  $q="INSERT INTO tranap_mpp (
			template_assesmen_id,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,created_date,created_ppa,template_id,status_assemen,idpasien
			,total_skor,kesimpulan_mpp_id,kesimpulan_mpp_text,tindakan_id,tindakan_nama,keterangan
			)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id','$pendaftaran_id_ranap','$st_ranap','$idtipe',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen,'$idpasien'
			,total_skor,kesimpulan_mpp_id,kesimpulan_mpp_text,tindakan_id,tindakan_nama,keterangan
			FROM `tranap_mpp`

			WHERE assesmen_id='$template_assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tranap_mpp_pengkajian (
			assesmen_id,param_id,parameter_nama,group_nilai_id,group_nilai,group_nilai_nama,nilai,nilai_nama,skor
			) 
			SELECT
			'$assesmen_id',param_id,parameter_nama,group_nilai_id,group_nilai,group_nilai_nama,nilai,nilai_nama,skor
			FROM tranap_mpp_pengkajian WHERE assesmen_id = '$template_assesmen_id'";
			$this->db->query($q);
			
	   $q="UPDATE tranap_mpp 
		SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
		$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_edit_mpp(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tranap_mpp WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_mpp($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tranap_mpp',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$q="SELECT *FROM tranap_mpp WHERE assesmen_id='$assesmen_id'";
				$data=$this->db->query($q)->row_array();
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_mpp($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tranap_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tranap_mpp_x_his 
						(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date
						,edited_ppa,edited_date,
						total_skor,kesimpulan_mpp_id,kesimpulan_mpp_text,tindakan_id,tindakan_nama,keterangan
						)
				SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,
				'$login_ppa_id',NOW(),
				total_skor,kesimpulan_mpp_id,kesimpulan_mpp_text,tindakan_id,tindakan_nama,keterangan
				FROM tranap_mpp 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		  
			$q="INSERT INTO tranap_mpp_x_his_pengkajian 
						(versi_edit,
						id,assesmen_id,param_id,parameter_nama,group_nilai_id,group_nilai,group_nilai_nama,nilai,nilai_nama,skor)
						SELECT 
						'$jml_edit',
						id,assesmen_id,param_id,parameter_nama,group_nilai_id,group_nilai,group_nilai_nama,nilai,nilai_nama,skor
						FROM tranap_mpp_pengkajian WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
		}
	  return $hasil;
	}
	function load_skrining_mpp(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $footer_pengkajian=$this->input->post('footer_pengkajian');
	  $q="SELECT * FROM `tranap_mpp_pengkajian` H WHERE H.assesmen_id='$assesmen_id'";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  $total=0;
	  $nilai_max=0;
	  $nilai_max_nama='';
	  foreach($hasil as $r){
		  $no=$no+1;
		 $opsi.='<tr>
					<td class="text-center">'.$no.'<input class="nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td>'.$r->parameter_nama.'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td><select class="form-control nilai " style="width: 100%;" data-placeholder="Pilih Opsi" required>
						'.$this->opsi_nilai_mpp($r->group_nilai_id,$r->group_nilai,$r->group_nilai_nama,$r->nilai).'
					</select>
					<td class="text-center">'.$r->skor.'</td>
					</td>
					
				</tr>';
			$total=$total+$r->skor;
	  }
	  $opsi.='<tr><td colspan="2" class="text-left">'.$footer_pengkajian.'</td>';
	  $opsi.='<td class="text-right"><strong>PENILAIAN AKHIR</strong></td>';
	  $opsi.='<td class="text-center"><input id="total_skor" class="total_skor form-control text-center" readonly type="text"  value="'.$total.'"></td></tr>';
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function opsi_nilai_mpp($group_nilai_id,$group_nilai,$group_nilai_nama,$param_nilai_id){
	  $q="SELECT * FROM `setting_mpp_param_skor` H WHERE H.id IN ($group_nilai_id)";
	  // $group_nilai_id=explode("#", $group_nilai_id);
	  // // print_r($group_nilai_id);exit;
	  // $group_nilai=explode(",", $group_nilai);
	  // $group_nilai_nama=explode("#", $group_nilai_nama);
	  $hasil='';
	  $row=$this->db->query($q)->result();
		  $hasil .='<option data-nilai="0" value="" '.($param_nilai_id==''?'selected':'').'>-Opsi-</option>';
	  foreach($row as $r){
		  $nilai=$r->id;
		  $nilai_nama=$r->deskripsi_nama;
		  $nilai_abjad=$r->skor;
		  
		  $hasil .='<option data-skor="'.$nilai_abjad.'" data-nama="'.$nilai_nama.'" value="'.$nilai.'" '.($param_nilai_id==$nilai?'selected':'').'>'.$nilai_nama.'</option>';
	  }
	  return $hasil;
  }
   function update_nilai_mpp(){
	  $id=$this->input->post('risiko_nilai_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nilai=$this->input->post('nilai');
	  $nilai_nama=$this->input->post('nilai_nama');
	  $skor=$this->input->post('skor');
	  $data=array(
		'nilai' =>$nilai,
		'nilai_nama' =>$nilai_nama,
		'skor' =>$skor,
	  );
	  $this->db->where('id',$id);
	  $result=$this->db->update('tranap_mpp_pengkajian',$data);
	  $total_skor=$this->db->query("SELECT SUM(COALESCE(H.skor,0)) as total_skor FROM tranap_mpp_pengkajian H WHERE H.assesmen_id='$assesmen_id'")->row('total_skor');
	  
	  $q="
	  SELECT '$total_skor' as total_skor,H.kesimpulan_mpp_id,MG.ref as kesimpulan_mpp_text,H.tindakan_id,MT.ref as tindakan_nama 
		FROM setting_mpp_nilai H
		LEFT JOIN merm_referensi MG ON MG.nilai=H.kesimpulan_mpp_id AND MG.ref_head_id='300'
		LEFT JOIN merm_referensi MT ON MT.nilai=H.tindakan_id AND MT.ref_head_id='301'
		WHERE '$total_skor' BETWEEN H.skor_1 AND H.skor_2 AND H.staktif='1'
	  ";
	  $data=$this->db->query($q)->row_array();
	  if ($data){
		  
	  }else{
		  $data=array(
			'total_skor' => $total_skor,
			'kesimpulan_mpp_id' => '',
			'kesimpulan_mpp_text' => '',
			'tindakan_id' => '',
			'tindakan_nama' => '',

		  );
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->update('tranap_mpp',$data);
	  
	  $this->output->set_output(json_encode($data));
  }
  function get_perhitungan_skor_mpp(){
      // $nilai_imt_id=$this->input->post('nilai_imt_id');
      $assesmen_id=$this->input->post('assesmen_id');
	  $total_skor=$this->db->query("SELECT SUM(COALESCE(H.skor,0)) as total_skor FROM tranap_mpp_pengkajian H WHERE H.assesmen_id='$assesmen_id'")->row('total_skor');
	  
	 $q="
	  SELECT '$total_skor' as total_skor,H.kesimpulan_mpp_id,MG.ref as kesimpulan_mpp_text,H.tindakan_id,MT.ref as tindakan_nama 
		FROM setting_mpp_nilai H
		LEFT JOIN merm_referensi MG ON MG.nilai=H.kesimpulan_mpp_id AND MG.ref_head_id='300'
		LEFT JOIN merm_referensi MT ON MT.nilai=H.tindakan_id AND MT.ref_head_id='301'
		WHERE '$total_skor' BETWEEN H.skor_1 AND H.skor_2 AND H.staktif='1'
	  ";
	  $data=$this->db->query($q)->row_array();;
	  // $data['penilaian_mpp']=$this->db->query($q)->row('penilaian_mpp');
	  
	  $this->output->set_output(json_encode($data));
  }
   // MPP EVALUSASI
   function create_evaluasi_mpp(){
	$login_ppa_id=$this->session->userdata('login_ppa_id');
	$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	$pendaftaran_id=$this->input->post('pendaftaran_id');
	$template_id=$this->input->post('template_id');
	$idpasien=$this->input->post('idpasien');
	$st_ranap=$this->input->post('st_ranap');
	$idtipe=$this->input->post('idtipe');

	$data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'st_ranap' => $st_ranap,
		'idtipe' => $idtipe,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,

	);
	$hasil=$this->db->insert('tranap_evaluasi_mpp',$data);
	$assesmen_id=$this->db->insert_id();

	$this->output->set_output(json_encode($hasil));
}
	
  function batal_evaluasi_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
	  }
	  if ($st_edited=='1'){
		
		  $q="DELETE FROM tranap_evaluasi_mpp WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		 
			$q="INSERT IGNORE INTO tranap_evaluasi_mpp (
			assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,
			indentifikasi_skrining,fisik,riwayat_kesehatan,perilaku,kesehatan_mental,lingkungan,dukungan_kel,finansial,finansial_ket,obat_alternative,obat_alternative_lain,trauma,pemahanan,harapan,perkiraan_rawat,discharge_planing,discharge_planing_lain,perencanaan_lanjutan,aspek_legal,aspek_legal_lain,kesempatan,perencanaan_mpp,jangka_pendek,jangka_panjang,kebutuhan_perjalanan,lain_lain,rencana_penunjang,rencana_penunjang_lain,tempat_perawatan,tempat_perawatan_lain,revisi
			) 
				SELECT
				assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,
				indentifikasi_skrining,fisik,riwayat_kesehatan,perilaku,kesehatan_mental,lingkungan,dukungan_kel,finansial,finansial_ket,obat_alternative,obat_alternative_lain,trauma,pemahanan,harapan,perkiraan_rawat,discharge_planing,discharge_planing_lain,perencanaan_lanjutan,aspek_legal,aspek_legal_lain,kesempatan,perencanaan_mpp,jangka_pendek,jangka_panjang,kebutuhan_perjalanan,lain_lain,rencana_penunjang,rencana_penunjang_lain,tempat_perawatan,tempat_perawatan_lain,revisi
				FROM tranap_evaluasi_mpp_x_his WHERE assesmen_id = '$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
			
			$q="DELETE FROM tranap_evaluasi_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_evaluasi_mpp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  
	function save_evaluasi_mpp(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$tanggal_masuk_rs= YMDFormat($this->input->post('tanggalmasukrs')). ' ' .$this->input->post('waktumasukrs');
		$st_edited = $this->input->post('st_edited');
		$status_assemen = $this->input->post('status_assemen');
		$assesmen_id = $this->input->post('assesmen_id');
		
		$kesempatan = $this->input->post('kesempatan');
		if ($kesempatan){
			$kesempatan=implode(",", $kesempatan);
		}
		$perencanaan_mpp = $this->input->post('perencanaan_mpp');
		if ($perencanaan_mpp){
			$perencanaan_mpp=implode(",", $perencanaan_mpp);
		}
		$rencana_penunjang = $this->input->post('rencana_penunjang');
		if ($rencana_penunjang){
			$rencana_penunjang=implode(",", $rencana_penunjang);
		}
		$tempat_perawatan = $this->input->post('tempat_perawatan');
		if ($tempat_perawatan){
			$tempat_perawatan=implode(",", $tempat_perawatan);
		}
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			
			'indentifikasi_skrining' => $this->input->post('indentifikasi_skrining'),
			'fisik' => $this->input->post('fisik'),
			'riwayat_kesehatan' => $this->input->post('riwayat_kesehatan'),
			'perilaku' => $this->input->post('perilaku'),
			'kesehatan_mental' => $this->input->post('kesehatan_mental'),
			'lingkungan' => $this->input->post('lingkungan'),
			'dukungan_kel' => $this->input->post('dukungan_kel'),
			'finansial' => $this->input->post('finansial'),
			'finansial_ket' => $this->input->post('finansial_ket'),
			'obat_alternative' => $this->input->post('obat_alternative'),
			'obat_alternative_lain' => $this->input->post('obat_alternative_lain'),
			'trauma' => $this->input->post('trauma'),
			'pemahanan' => $this->input->post('pemahanan'),
			'harapan' => $this->input->post('harapan'),
			'perkiraan_rawat' => $this->input->post('perkiraan_rawat'),
			'discharge_planing' => $this->input->post('discharge_planing'),
			'discharge_planing_lain' => $this->input->post('discharge_planing_lain'),
			'perencanaan_lanjutan' => $this->input->post('perencanaan_lanjutan'),
			'aspek_legal' => $this->input->post('aspek_legal'),
			'aspek_legal_lain' => $this->input->post('aspek_legal_lain'),
			'kesempatan' => $kesempatan,
			'perencanaan_mpp' => $perencanaan_mpp,
			'jangka_pendek' => $this->input->post('jangka_pendek'),
			'jangka_panjang' => $this->input->post('jangka_panjang'),
			'kebutuhan_perjalanan' => $this->input->post('kebutuhan_perjalanan'),
			'lain_lain' => $this->input->post('lain_lain'),
			'rencana_penunjang' => $rencana_penunjang,
			'rencana_penunjang_lain' => $this->input->post('rencana_penunjang_lain'),
			'tempat_perawatan' => $tempat_perawatan,
			'tempat_perawatan_lain' => $this->input->post('tempat_perawatan_lain'),
			'revisi' => $this->input->post('revisi'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tranap_evaluasi_mpp_x_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_evaluasi_mpp',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_catatan_evaluasi_mpp(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal= YMDFormat($this->input->post('waktupencatatan')). ' ' .$this->input->post('waktupencatatan');
		$catatan_id=$this->input->post('catatan_id');
		
		$data=array(
			'tanggal'=> $tanggal,
			'assesmen_id' => $this->input->post('assesmen_id'),
			'pendaftaran_id_ranap' => $this->input->post('pendaftaran_id_ranap'),
			'idpasien' => $this->input->post('idpasien'),
			'catatan' => $this->input->post('catatan'),
			

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($catatan_id==''){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
			$result=$this->db->insert('tranap_evaluasi_mpp_catatan',$data);
		}else{
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$catatan_id);
			$result=$this->db->update('tranap_evaluasi_mpp_catatan',$data);
		}
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	// function get_gizi(){
	  
	  // $masa_tubuh=$this->input->post('masa_tubuh');
	  // $q="SELECT H.id as nilai_imt_id,H.kategori_berat as nilai_imt_text 
				// FROM mberat H 
				// WHERE '$masa_tubuh' BETWEEN H.berat_1 AND H.berat_2 AND H.staktif='1' LIMIT 1";
		// $hasil=$this->db->query($q)->row_array();
		// $this->output->set_output(json_encode($hasil));
	// }
	
  function list_history_pengkajian_evaluasi_mpp()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_evaluasi_mpp($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idrawat_ranap=$this->input->post('idrawat_ranap');
		$idtipe=$this->input->post('idtipe');
		$idruang=$this->input->post('idruang');
		$idbed=$this->input->post('idbed');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$st_ranap=$this->input->post('st_ranap');
		$where='';
		
		if ($idrawat_ranap != '#'){
			if ($st_ranap=='1'){
			$where .=" AND MP.id = '".$idrawat_ranap."'";
				
			}else{
				
			$where .=" AND MJ.id = '".$idrawat_ranap."'";
			}
		}
		if ($idruang != '#'){
			$where .=" AND MP.idruangan = '".$idruang."'";
		}
		if ($idkelas != '#'){
			$where .=" AND MP.idkelas = '".$idkelas."'";
		}
		if ($idbed != '#'){
			$where .=" AND MP.idbed = '".$idbed."'";
		}
		if ($notransaksi != ''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%'";
			$where .=" OR MJ.nopendaftaran LIKE '%".$notransaksi."%')";
		}
		if ($iddokter != '#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		
		if ($mppa_id != '#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1  != ''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1  != ''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_evaluasi_mpp")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE MJ.nopendaftaran END nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE MJ.tanggaldaftar END tanggaldaftar
					,H.idtipe
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.tanggal_input as tanggal_pengkajian,H.assesmen_id,H.pendaftaran_id_ranap,H.pendaftaran_id,H.st_ranap,MPP.nama as nama_poli
					FROM `tranap_evaluasi_mpp` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran MJ ON MJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPP ON MPP.id=MJ.idpoliklinik
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id_ranap.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/input_evaluasi_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
			
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/input_evaluasi_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		}
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id != $r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id != $r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_evaluasi_mpp']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_evaluasi_mpp']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			if ($r->st_ranap){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/his_evaluasi_mpp/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
			}else{
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/his_evaluasi_mpp/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				
			}
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		if ($logic_akses_assesmen['st_lihat_evaluasi_mpp']=='1'){
		$aksi .= $btn_lihat;	
		}
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->nopendaftaran);
		if ($r->st_ranap=='1'){
		$result[] = GetAsalRujukanRI($r->idtipe).(' <strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:''));
			
		}else{
			
		$result[] = GetAsalRujukanKwitansi($r->idtipe).' - <strong>'.$r->nama_poli.'</strong>';
		}
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa);
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	function create_template_evaluasi_mpp(){
	  	$login_ppa_id=$this->session->userdata('login_ppa_id');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$st_ranap=$this->input->post('st_ranap');
		$idtipe=$this->input->post('idtipe');
		$template_id=$this->input->post('template_id');
		$idpasien=$this->input->post('idpasien');
		
		$data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idtipe' => $idtipe,
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,

		);
		
		$hasil=$this->db->insert('tranap_evaluasi_mpp',$data);
		$assesmen_id=$this->db->insert_id();

		$this->output->set_output(json_encode($hasil));

    }
	
	function batal_template_evaluasi_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_evaluasi_mpp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function list_index_template_evaluasi_mpp()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tranap_evaluasi_mpp H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_record_evaluasi_mpp(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_evaluasi_mpp',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_catatan_evaluasi_mpp(){
		$id=$this->input->post('id');
		
		$data=array(
			'status' => 0,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$result=$this->db->update('tranap_evaluasi_mpp_catatan',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function edit_template_evaluasi_mpp(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'pendaftaran_id_ranap' => $this->input->post('pendaftaran_id_ranap'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_evaluasi_mpp',$data);
		
		$this->output->set_output(json_encode($result));
	}
  function create_with_template_evaluasi_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idtipe=$this->input->post('idtipe');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="INSERT INTO tranap_evaluasi_mpp (
			template_assesmen_id,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,created_date,created_ppa,template_id,status_assemen,idpasien
			,st_ranap,idtipe
			,indentifikasi_skrining,fisik,riwayat_kesehatan,perilaku,kesehatan_mental,lingkungan,dukungan_kel,finansial,finansial_ket,obat_alternative,obat_alternative_lain,trauma,pemahanan,harapan,perkiraan_rawat,discharge_planing,discharge_planing_lain,perencanaan_lanjutan,aspek_legal,aspek_legal_lain,kesempatan,perencanaan_mpp,jangka_pendek,jangka_panjang,kebutuhan_perjalanan,lain_lain,rencana_penunjang,rencana_penunjang_lain,tempat_perawatan,tempat_perawatan_lain,revisi
			
			)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id','$pendaftaran_id_ranap',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen,'$idpasien'
			,'$st_ranap','$idtipe'
			,indentifikasi_skrining,fisik,riwayat_kesehatan,perilaku,kesehatan_mental,lingkungan,dukungan_kel,finansial,finansial_ket,obat_alternative,obat_alternative_lain,trauma,pemahanan,harapan,perkiraan_rawat,discharge_planing,discharge_planing_lain,perencanaan_lanjutan,aspek_legal,aspek_legal_lain,kesempatan,perencanaan_mpp,jangka_pendek,jangka_panjang,kebutuhan_perjalanan,lain_lain,rencana_penunjang,rencana_penunjang_lain,tempat_perawatan,tempat_perawatan_lain,revisi
			FROM `tranap_evaluasi_mpp`

			WHERE assesmen_id='$template_assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q);
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_edit_evaluasi_mpp(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tranap_evaluasi_mpp WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_evaluasi_mpp($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tranap_evaluasi_mpp',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$data=$this->db->query("SELECT * FROM tranap_evaluasi_mpp WHERE assesmen_id='$assesmen_id'")->row_array();
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_evaluasi_mpp($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tranap_evaluasi_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tranap_evaluasi_mpp_x_his 
						(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date
						,edited_ppa,edited_date,
						indentifikasi_skrining,fisik,riwayat_kesehatan,perilaku,kesehatan_mental,lingkungan,dukungan_kel,finansial,finansial_ket,obat_alternative,obat_alternative_lain,trauma,pemahanan,harapan,perkiraan_rawat,discharge_planing,discharge_planing_lain,perencanaan_lanjutan,aspek_legal,aspek_legal_lain,kesempatan,perencanaan_mpp,jangka_pendek,jangka_panjang,kebutuhan_perjalanan,lain_lain,rencana_penunjang,rencana_penunjang_lain,tempat_perawatan,tempat_perawatan_lain,revisi
						)
				SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,
				'$login_ppa_id',NOW(),
				indentifikasi_skrining,fisik,riwayat_kesehatan,perilaku,kesehatan_mental,lingkungan,dukungan_kel,finansial,finansial_ket,obat_alternative,obat_alternative_lain,trauma,pemahanan,harapan,perkiraan_rawat,discharge_planing,discharge_planing_lain,perencanaan_lanjutan,aspek_legal,aspek_legal_lain,kesempatan,perencanaan_mpp,jangka_pendek,jangka_panjang,kebutuhan_perjalanan,lain_lain,rencana_penunjang,rencana_penunjang_lain,tempat_perawatan,tempat_perawatan_lain,revisi
				FROM tranap_evaluasi_mpp 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		  
			
			
		}
	  return $hasil;
	}
	// MPP CATATAN MPP
   function create_imp_mpp(){
	$login_ppa_id=$this->session->userdata('login_ppa_id');
	$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	$pendaftaran_id=$this->input->post('pendaftaran_id');
	$template_id=$this->input->post('template_id');
	$idpasien=$this->input->post('idpasien');
	$st_ranap=$this->input->post('st_ranap');
	$idtipe=$this->input->post('idtipe');

	$data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'idpasien' => $idpasien,
		'st_ranap' => $st_ranap,
		'idtipe' => $idtipe,
		'created_ppa' => $login_ppa_id,
		'tanggal_implementasi' => date('Y-m-d H:i:s'),
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,

	);
	$hasil=$this->db->insert('tranap_imp_mpp',$data);
	$assesmen_id=$this->db->insert_id();

	$this->output->set_output(json_encode($hasil));
}
	
  function batal_imp_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
	  }
	  if ($st_edited=='1'){
		
		  $q="DELETE FROM tranap_imp_mpp WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		 
			$q="INSERT IGNORE INTO tranap_imp_mpp (
			assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,
			tanggal_implementasi,item_kegiatan,catatan
			) 
				SELECT
				assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,
				tanggal_implementasi,item_kegiatan,catatan
				FROM tranap_imp_mpp_x_his WHERE assesmen_id = '$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
			
			$q="DELETE FROM tranap_imp_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_imp_mpp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  
	function save_imp_mpp(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$st_edited = $this->input->post('st_edited');
		$status_assemen = $this->input->post('status_assemen');
		$assesmen_id = $this->input->post('assesmen_id');
		
		$item_kegiatan = $this->input->post('item_kegiatan');
		if ($item_kegiatan){
			$item_kegiatan=implode(",", $item_kegiatan);
		}
		
		$data=array(
			'tanggal_implementasi'=> $tanggal_input,
			'tanggal_input'=> $tanggal_input,
			'item_kegiatan'=> $item_kegiatan,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			
			'catatan' => $this->input->post('catatan'),
			

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tranap_imp_mpp_x_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_imp_mpp',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
  function list_history_pengkajian_imp_mpp()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_imp_mpp($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idrawat_ranap=$this->input->post('idrawat_ranap');
		$idtipe=$this->input->post('idtipe');
		$idruang=$this->input->post('idruang');
		$idbed=$this->input->post('idbed');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$st_ranap=$this->input->post('st_ranap');
		
		
		$where='';
		$item_kegiatan_id=$this->input->post('item_kegiatan_id');
		if ($item_kegiatan_id){
			$item_kegiatan_id=implode(", ", $item_kegiatan_id);
			// $where .=" AND H.id = '".$idrawat_ranap."'";
		}
		
		if ($idrawat_ranap != '#'){
			if ($st_ranap=='1'){
			$where .=" AND MP.id = '".$idrawat_ranap."'";
				
			}else{
				
			$where .=" AND MJ.id = '".$idrawat_ranap."'";
			}
		}
		if ($idruang != '#'){
			// $where .=" AND MP.idruangan = '".$idruang."'";
		}
		if ($idkelas != '#'){
			$where .=" AND MP.idkelas = '".$idkelas."'";
		}
		if ($idbed != '#'){
			$where .=" AND MP.idbed = '".$idbed."'";
		}
		if ($notransaksi != ''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%'";
			$where .=" OR MJ.nopendaftaran LIKE '%".$notransaksi."%')";
		}
		if ($iddokter != '#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		
		if ($mppa_id != '#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1  != ''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1  != ''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_imp_mpp")->row_array();
		$q="SELECT H.nilai as id,H.ref as nama FROM `merm_referensi` H WHERE H.ref_head_id='316' AND H.`status`='1'";
		$arr_ref=$this->db->query($q)->result();
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE MJ.nopendaftaran END nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE MJ.tanggaldaftar END tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MP.title ELSE MJ.title END title
					,CASE WHEN H.st_ranap='1' THEN MP.namapasien ELSE MJ.namapasien END namapasien
					,CASE WHEN H.st_ranap='1' THEN MP.no_medrec ELSE MJ.no_medrec END no_medrec
					,H.idtipe,H.catatan,H.item_kegiatan
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.tanggal_input as tanggal_pengkajian,H.assesmen_id,H.pendaftaran_id_ranap,H.pendaftaran_id,H.st_ranap,MPP.nama as nama_poli
					FROM `tranap_imp_mpp` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran MJ ON MJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPP ON MPP.id=MJ.idpoliklinik
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id_ranap.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/input_imp_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
			
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/input_imp_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		}
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id != $r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id != $r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_imp_mpp']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_imp_mpp']=='0'){
		  $btn_hapus='';
		}
		
		$aksi='';
		$aksi_edit='';
		$ket_edit='';
		if ($r->jml_edit>0){
		$ket_edit=' | Edit Oleh : '.$r->nama_edit.' | Ket : '.$r->keterangan_edit;
			if ($r->st_ranap){
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/his_imp_mpp/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
			}else{
				$aksi_edit='<a href="'.base_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/his_imp_mpp/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i></a>';
				
			}
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		if ($logic_akses_assesmen['st_lihat_imp_mpp']=='1'){
			$aksi .= $btn_lihat;	
		}
		$aksi .= $aksi_edit;	
		
		$aksi .= '</div>';
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" style="width: 100%;">
									'.$aksi.$this->get_item_kegiatan_imp_mpp($arr_ref,$r->item_kegiatan).'
									<div class="text-muted font-s13 push-5-t">'.$r->nopendaftaran.' | '.HumanDateShort($r->tanggaldaftar).' | Add On : '.HumanDateLong($r->tanggal_pengkajian).' | Oleh '.($r->nama_mppa).$ket_edit.'</div>
									<div class="h5 font-w700 text-primary">KEGIATAN MANAJEMEN PELAYANAN PASIEN</div>
									<div class="h5 text-muted push-5-t">'.$r->catatan.' </div>
									
								</td>
							</tr>
						</tbody>
					</table>';
		$result[] = $btn_1;
					
		$result[] = '
			<img class="" style="width:100px" src="'.base_url().'qrcode/qr_code_ppa/'.$r->created_ppa.'" alt="" title="">
			<br>
			('.$r->nama_mppa.')
		';
		// $result[] = $aksi;
		// $result[] = HumanDateLong($r->tanggaldaftar);
		// $result[] = ($r->nopendaftaran);
		// if ($r->st_ranap=='1'){
		// $result[] = GetAsalRujukanRI($r->idtipe).(' <strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:''));
			
		// }else{
			
		// $result[] = GetAsalRujukanKwitansi($r->idtipe).' - <strong>'.$r->nama_poli.'</strong>';
		// }
		// $result[] = HumanDateLong($r->tanggal_pengkajian);
		// $result[] = ($r->nama_mppa);
		// $result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	function get_item_kegiatan_imp_mpp($arr_ref,$item_kegiatan){
		$arr_group_nilai=explode(",",$item_kegiatan);
		  $opsi ='<br><div class="h5 font-w700 push-5-t">'; 
		  foreach($arr_ref as $row){
			  if (in_array($row->id,$arr_group_nilai)){
				 $opsi .=text_warning($row->nama).'&nbsp;'; 
			  }
		  }
		  $opsi .='</div>'; 
		  return $opsi;
	}
	function create_template_imp_mpp(){
	  	$login_ppa_id=$this->session->userdata('login_ppa_id');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$st_ranap=$this->input->post('st_ranap');
		$idtipe=$this->input->post('idtipe');
		$template_id=$this->input->post('template_id');
		$idpasien=$this->input->post('idpasien');
		
		$data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idtipe' => $idtipe,
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'tanggal_implementasi' => date('Y-m-d H:i:s'),
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,

		);
		
		$hasil=$this->db->insert('tranap_imp_mpp',$data);
		$assesmen_id=$this->db->insert_id();

		$this->output->set_output(json_encode($hasil));

    }
	
	function batal_template_imp_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_imp_mpp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function list_index_template_imp_mpp()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tranap_imp_mpp H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_record_imp_mpp(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_imp_mpp',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_catatan_imp_mpp(){
		$id=$this->input->post('id');
		
		$data=array(
			'status' => 0,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$result=$this->db->update('tranap_imp_mpp_catatan',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function edit_template_imp_mpp(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'pendaftaran_id_ranap' => $this->input->post('pendaftaran_id_ranap'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_imp_mpp',$data);
		
		$this->output->set_output(json_encode($result));
	}
  function create_with_template_imp_mpp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idtipe=$this->input->post('idtipe');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="INSERT INTO tranap_imp_mpp (
			template_assesmen_id,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,created_date,created_ppa,template_id,status_assemen,idpasien
			,st_ranap,idtipe
			,tanggal_implementasi,item_kegiatan,catatan
			
			)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id','$pendaftaran_id_ranap',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen,'$idpasien'
			,'$st_ranap','$idtipe'
			,tanggal_implementasi,item_kegiatan,catatan
			FROM `tranap_imp_mpp`

			WHERE assesmen_id='$template_assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q);
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_edit_imp_mpp(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tranap_imp_mpp WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_imp_mpp($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tranap_imp_mpp',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$data=$this->db->query("SELECT * FROM tranap_imp_mpp WHERE assesmen_id='$assesmen_id'")->row_array();
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_imp_mpp($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tranap_imp_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tranap_imp_mpp_x_his 
						(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date
						,edited_ppa,edited_date,
						tanggal_implementasi,item_kegiatan,catatan

						)
				SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idtipe,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,
				'$login_ppa_id',NOW(),
				tanggal_implementasi,item_kegiatan,catatan
				FROM tranap_imp_mpp 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		  
			
			
		}
	  return $hasil;
	}
	
 
}	
