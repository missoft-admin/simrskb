<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Tpasien_pengembalian extends CI_Controller {

	/*
	 ##################################################
	 ###### Farmasi - Penjualan Obat. 			  #####
	 ###### Developer @Deni Purnama				  #####
	 ###### Email: denipurnama371@gmail.com 	  #####
	 ##################################################
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpasien_pengembalian_model','tpaspeng');
  }

    function index(){
        // $data = array();
		 $data = array(
            'namapasien' => '',
            'tanggaldari' => date('d/m/Y', strtotime("-1 week")),
            'tanggalsampai' => date("d/m/Y"),
            'idasalpasien' => '#',
            'idkelompokpasien' => '',
            'status' => '',
        );

        $data['error']          = '';
        $data['title']          = 'Pengembalian';
        $data['content']        = 'Tpasien_pengembalian/index';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Farmasi",'#'),
                                array("Retur",'tpasien_pengembalian')
                                );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function filter()
    {
        $data = array(
          'namapasien' => $this->input->post('namapasien'),
          'tanggaldari' => $this->input->post('tanggaldari'),
          'tanggalsampai' => $this->input->post('tanggalsampai'),
          'idasalpasien' => $this->input->post('idasalpasien'),
          'idkelompokpasien' => $this->input->post('idkelompokpasien'),
          'status' => $this->input->post('status'),
        );

				// print_r($data);exit();
        // $this->session->set_userdata($data);

        $data['error'] 			= '';
        $data['tab'] 				= 0;
        $data['title'] 			= 'Pengembalian';
        $data['content'] 		= 'Tpasien_pengembalian/index';
        $data['breadcrum'] 	= array(
                                  array("RSKB Halmahera",'#'),
                                  array("Farmasi",'#'),
                                  array("Pengembalian",'tpasien_pengembalian')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function create(){
		$data = array();
		$data['error'] 			= '';
		$data['form_edit'] 			= '0';
		$data['idpenjualan']='';
		$data['idpasien']='';
		$data['nomedrec']='';
		$data['nama_pasien']='';
		$data['kelompok_pasien']='';
		$data['jenis_pasien']='';
		$data['kode_ket']='';
		$data['kode_kes']='';
		$data['nama_rekanan']='';
		$data['nopenjualan']='';
		$data['umur']='';
		$data['dokter_poli']='';
		$data['total_barang']='';
		$data['alasan']='';
		$data['total_harga']='';
		$data['totalharga']='0';
		$data['rows']=array();
		$data['title'] 			= 'Tambah Retur';
		$data['content'] 		= 'Tpasien_pengembalian/manage';
		$data['breadcrum'] 	= array(
								array("RSKB Halmahera",'#'),
								array("Farmasi",'#'),
								array("Tambah Retur",'tpasien_pengembalian')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

    function save(){
        // print_r($this->input->post());exit();
		$idpenjualan=$this->input->post('idpenjualan');
		$form_edit=$this->input->post('form_edit');
		date_default_timezone_set('Asia/Jakarta');
		$setwaktu= $this->input->post('manage-input-tanggal').date('H:i:s');
		$now=date('Y-m-d H:i:s',strtotime($setwaktu));

		  $retur['tanggal']         = $now;
		  $retur['nopengembalian']  = createKode('tpasien_pengembalian','nopengembalian','FP');
		  $retur['idpenjualan']     = $idpenjualan;
		  $retur['idpasien']        = $this->input->post('idpasien');
		  $retur['totalbarang']     = 0;
		  $retur['totalharga']      = $this->input->post('grand_total');
		  $retur['alasan']          = $this->input->post('alasan');
		  $retur['status']          = 1;
		  $retur['created_by']          = $this->session->userdata('user_id');
		  $retur['created_date']          = date("Y-m-d H:i:s");
		// if ($form_edit=='1'){
			// $retur['id']        = $this->input->post('id');
		// }
		if ($form_edit=='0'){
			$this->tpaspeng->saveData($retur);
		}
    
			$status=true;
			if ($form_edit=='0'){
				$iddetail=$this->db->insert_id();
			}else{
				$iddetail=$this->input->post('id');
			}
            if ($form_edit == '1'){$this->tpaspeng->delete_non_racikan($iddetail);}
			$detail_list_nonracikan = json_decode($this->input->post('pos_tabel_non_racikan'));
			// print_r($detail_list_nonracikan);exit();
			$total_barang=0;
			foreach($detail_list_nonracikan as $DL):
				if ($DL[13] != '' || $DL[13] > 0){
					$nonrac = array();
					$nonrac['idpengembalian']= $iddetail;
					$nonrac['idpenjualan_detail']= $DL[14];
					$nonrac['idtipe']= $DL[11];
					$nonrac['idbarang']= $DL[10];
					$nonrac['kuantitas']= $DL[13];
					$nonrac['harga_dasar']= $DL[15];
					$nonrac['margin']= $DL[16];
					$nonrac['harga']= $DL[9]+$DL[18];
					$nonrac['diskon']= $DL[17];
					$nonrac['diskon_rp']= $DL[18];
					$nonrac['totalharga']= $DL[12];
					$nonrac['alasan']= $this->input->post('alasan');
					$nonrac['stracikan']= 0;
					$nonrac['status']= 1;

					// print_r($nonrac);exit();
					$total_barang=$total_barang+1;
					if($this->tpaspeng->saveDataNonracikan($nonrac)):
						$status = true;
					endif;
				}
			endforeach;
			$this->tpaspeng->update_total_barang($iddetail,$total_barang);
        // }else{
            // $status=false;
        // }
	if($status){
        $_SESSION['status']='ToastrSukses("Data berhasil disimpan","Info")';
        redirect('tpasien_pengembalian','location');
    }else{
        $_SESSION['status']='Toastr("Data gagal disimpan","Info")';
        redirect('tpasien_pengembalian','location');
    }
}
	function edit($id){
		$data = array();
		$data['error'] 			= '';
		$data['form_edit'] 			= '1';
		$data_header=$this->tpaspeng->get_pengembalian_detail($id);
		if ($data_header){
			$data['id']=$id;
			$data['idpenjualan']=$data_header->idpenjualan;
			$data['alasan']=$data_header->alasan;
			$data['idpasien']=$data_header->idpasien;
			$data['nomedrec']=$data_header->nomedrec;
			$data['nama_pasien']=$data_header->nama;
			$data['kelompok_pasien']=$data_header->nama_kelompok;
			$data['nopenjualan']=$data_header->nopenjualan;
			$data['umur']=$data_header->umurtahun." Thn ".$data_header->umurbulan." Bln ".$data_header->umurhari." Hari";
			$data['dokter_poli']=$data_header->asal_pasien;
			$data['total_barang']=$data_header->totalbarang;
			$data['total_harga']=$data_header->totalharga;
			$data['totalharga']=$data_header->total_harga;
			$data['rows']=$this->tpaspeng->get_retur_obat_detail($id,$data['idpenjualan']);
			// print_r($data['rows']);exit();
		}else{
			$data['idpenjualan']='';
			$data['idpasien']='';
			$data['nomedrec']='';
			$data['nama_pasien']='';
			$data['kelompok_pasien']='';
			$data['nopenjualan']='';
			$data['umur']='';
			$data['dokter_poli']='';
			$data['total_barang']='';
			$data['total_harga']='0';
			$data['totalharga']='0';
			$data['alasan']='';
			$data['rows']=array();
		}
		$data['title'] 			= 'Edit Retur';
		$data['content'] 		= 'Tpasien_pengembalian/manage';
		$data['breadcrum'] 	= array(
								array("RSKB Halmahera",'#'),
								array("Farmasi",'#'),
								array("Edit Retur",'tpasien_pengembalian')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function getNoPen(){
		$getPasPen      =getwhere('status',2,'tpasien_penjualan')->result();
		// $getPasPenDet   =getwhere('id',$_POST['id'],'tpasien_penjualan')->row();
			$data = '{"datapaspen":[';
			foreach($getPasPen as $get){
								$data .= '"'.$get->nopenjualan.'",';
								// $data .= '"'.$get->nama.'",';
							}
			$data = substr($data, 0, strlen($data) - 1);
			$data .= ']}';
		echo $data;
	}



	function print_transaksi($id){
			// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);
		$data = array();
		$data['title'] 				= 'Dashboard';
		$data['error'] 		= '';
		$data['list_data'] 		= $this->tpaspeng->list_data($id);
		// $data['list_data_racikan'] 		= $this->tpaspeng->list_data_racikan($id);
		$header 		= $this->tpaspeng->get_header($id);
		
		if ($header){
			$data['id']=$header->id;
			$data['nopengembalian']=$header->nopengembalian;
			$data['nomedrec']=$header->nomedrec;
			$data['nama']=$header->nama;
			$data['umur']=$header->umurtahun;
			$data['umur_bulan']=$header->umurbulan;
			$data['umur_hari']=$header->umurhari;
			$data['asalrujukan']=GetAsalRujukan($header->asalrujukan);
			// $data['resep']=$header->statusresep;
			$data['totalharga']=$header->totalharga;
			$data['created_nama']=$header->created_nama;

		}
		$html = $this->parser->parse('Tpasien_pengembalian/kwitansi_besar',array_merge($data,backend_info()),TRUE);
		$html = $this->parser->parse_string($html,$data);
		// print_r($data);exit();
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Faktur .pdf', array("Attachment"=>0));
	}
	function print_transaksi_small($id){
		// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);
		$data = array();
		$data['title'] 				= 'Dashboard';
		$data['error'] 		= '';
		$data['list_data'] 		= $this->tpaspeng->list_data($id);
		$header 		= $this->tpaspeng->get_header($id);
		// print_r($header);exit();
		if ($header){
			$data['id']=$header->id;
			$data['alasan']=$header->alasan;
			$data['nopengembalian']=$header->nopengembalian;
			$data['nopenjualan']=$header->nopenjualan;
			$data['nomedrec']=$header->nomedrec;
			$data['nama']=$header->nama;
			$data['umur']=$header->umurtahun;
			$data['umur_bulan']=$header->umurbulan;
			$data['umur_hari']=$header->umurhari;
			$data['asalrujukan']=GetAsalRujukan($header->asalrujukan);
			$data['asalrujukan']=GetAsalRujukan($header->asalrujukan);
			// $data['resep']=$header->statusresep;
			$data['totalharga']=$header->totalharga;
			$data['created_nama']=$header->created_nama;

		}
		$html = $this->parser->parse('Tpasien_pengembalian/kwitansi_kecil',array_merge($data,backend_info()),TRUE);
		$html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation

		// $customPaper = array(0,0,226,4000);
		// $dompdf->set_paper($customPaper);
		// $dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Faktur .pdf', array("Attachment"=>0));
	}
	function getDataPengembalian($uri='filter'){

		 $this->select = array(
			'tpasien_pengembalian.*','tpasien_penjualan.nopenjualan','tpasien_penjualan.nomedrec','tpasien_penjualan.nama'

		  );

		  $this->from   = 'tpasien_pengembalian';
		  $this->join 	=array(
					array('tpasien_penjualan', 'tpasien_penjualan.id = tpasien_pengembalian.idpenjualan', '')
		  );
		  $this->where  = array();
		  // if($uri == 'filter'){
	    		if ($this->input->post('namapasien') != '') {
				  $this->where = array_merge($this->where, array('tpasien_penjualan.nama LIKE' => '%'.$this->input->post('namapasien').'%'));
				}
	    		if ($this->input->post('tanggaldari') != '') {
				  $this->where = array_merge($this->where, array('DATE(tpasien_pengembalian.tanggal) >= ' => YMDFormat($this->input->post('tanggaldari'))));
				}
				if ($this->input->post('tanggalsampai') != '') {
				  $this->where = array_merge($this->where, array('DATE(tpasien_pengembalian.tanggal) <= ' => YMDFormat($this->input->post('tanggalsampai'))));
				}
					if ($this->input->post('idasalpasien') != "#") {
				  $this->where = array_merge($this->where, array('tpasien_penjualan.asalrujukan' => $this->input->post('idasalpasien')));
				}
				if ($this->input->post('idkelompokpasien') != "#") {
				  $this->where = array_merge($this->where, array('tpasien_penjualan.idkelompokpasien' => $this->input->post('idkelompokpasien')));
				}
					// if ($this->input->post('status') != "0") {
				  // $this->where = array_merge($this->where, array('tpasien_penjualan.status' => $this->input->post('status')));
				// }
	      // }else{
          // $this->where  = array();
        // }
		  $this->order  = array(
			'tpasien_pengembalian.id' => 'DESC'
		  );
		  $this->group  = array();

		$this->column_search   = array('tpasien_pengembalian.tanggal','tpasien_pengembalian.nopengembalian','tpasien_penjualan.nopenjualan','tpasien_penjualan.nomedrec','tpasien_penjualan.nama');
		$this->column_order    = array('tanggal','nopengembalian','nopenjualan','nomedrec','nama','totalbarang','tpasien_pengembalian.totalharga');
		$list = $this->datatable->get_datatables();
		// print_r($list);exit();
		$data = array();
		$no = $_POST['start'];
		// print_r($list);exit();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		foreach ($list as $r) {
			$no++;
			$row = array();

			if($r->status == 0){
				$action = '<label>-</label>';
			}else if($r->status == 1){
			  $action = '<div class="btn-group">';
			  if (UserAccesForm($user_acces_form,array('1067'))){
				  $action .= '<a href="'.site_url().'tpasien_pengembalian/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
			  }
			   if (UserAccesForm($user_acces_form,array('1068','1069'))){
			  $action .= '<div class="btn-group">';
			  $action .= '<div class="btn-group dropup">';
			  $action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="fa fa-print"></span> </button>';
			  $action .= '<ul class="dropdown-menu"> <li>';
			   if (UserAccesForm($user_acces_form,array('1068'))){
				  $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_pengembalian/print_transaksi/'.$r->id.'">Kwitansi Besar</a>';
			  }
			   if (UserAccesForm($user_acces_form,array('1069'))){
				  $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_pengembalian/print_transaksi_small/'.$r->id.'">Kwitansi Kecil</a>';
			  }
			  $action .= '</li></ul>';
			  $action .= '</div>';
			  $action .= '</div>';
			  $action .= '</div>';
			  $action .= '</div>';
			   }
			}else if($r->status == 2){
			  $action = '<div class="btn-group">';
			  if(button_roles('tpasien_penjualan/serahkan')) {
				  $action = '<a href="'.site_url().'tpasien_penjualan/serahkan/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Serahkan" onclick="serahkan_row(this); return true;"><i class="fa fa-check"></i></a>';
			  }
			  $action = '<div class="btn-group">';
			  $action = '<div class="btn-group dropup">';
			  $action = '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="fa fa-print"></span> </button>';
			  $action = '<ul class="dropdown-menu"> <li>';
			  if(button_roles('tpasien_penjualan/print_transaksi')) {
				  $action = '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->id.'">Kwitansi</a>';
			  }
			  if(button_roles('tpasien_penjualan/print_transaksi_small')) {
				  $action = '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->id.'">Kwitansi Farmasi</a>';
			  }
			  if(button_roles('tpasien_penjualan/manage_print')) {
				  $action = '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->id.'">E-Ticket</a>';
			  }
			  $action = '</li></ul>';
			  $action = '</div>';
			  $action = '</div>';
			  $action = '</div>';
			  $action = '&nbsp;';
			  $action = '<div class="btn-group">';
			  if (UserAccesForm($user_acces_form,array('1067'))){
				  $action = '<a href="'.site_url().'tpasien_penjualan/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
			  }
			  $action = '</div>';
			}else{

			}

			$row[] = $no;
			$row[] = HumanDateLong($r->tanggal);
			$row[] = $r->nopengembalian;
			$row[] = $r->nopenjualan;
			$row[] = $r->nomedrec;
			$row[] = $r->nama;
			$row[] = $r->totalbarang;
			$row[] =number_format($r->totalharga,0);
			$row[] = $action;

			$data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}
	function get_penjualan()
	{
		  // $iddariunit     = $this->input->post('idunit');
        $snomedrec     		= $this->input->post('snomedrec');
        $snamapasien     	= $this->input->post('snamapasien');
        $snopenjualan     	= $this->input->post('snopenjualan');
        $sasalpasien    	= $this->input->post('sasalpasien');
        $sbarang    	= $this->input->post('sbarang');

        
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';
        if ($snomedrec != "") {
            $where .= " AND H.nomedrec LIKE '%".$snomedrec."%' ";
        }
		if ($snamapasien != "") {
            $where .= " AND H.nama LIKE '%".$snamapasien."%' ";
        }
		if ($snopenjualan != "") {
            $where .= " AND H.nopenjualan LIKE '%".$snopenjualan."%' ";
        }
		if ($sasalpasien != "#") {
            $where .= " AND H.asalrujukan= '".$sasalpasien."' ";
        }
		if ($sbarang != "") {
            $where .= " AND B.nama LIKE '%".$sbarang."%' ";
        }
        
        $from="(SELECT H.id,H.tanggal,H.nomedrec,H.nopenjualan,H.nama,H.asalrujukan,H.totalbarang,H.totalharga,RI.statuskasir from tpasien_penjualan H
				LEFT JOIN tpasien_penjualan_nonracikan D ON D.idpenjualan=H.id
				LEFT JOIN view_barang_farmasi B ON B.id=D.idbarang AND B.idtipe=D.idtipe
				LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idtindakan AND H.asalrujukan='3'
				WHERE H.`status`='3' ".$where."
				GROUP BY H.id 
				ORDER BY H.tanggal DESC) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('nomedrec','nopenjualan','nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();

		$no = $_POST['start'];
		// print_r($list);exit();
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $r->tanggal;
			if ($r->statuskasir=='1'){
				$row[] = $r->nopenjualan.'<br>'.'<span class="label label-danger"><i class="fa fa-lock"></i> Lock</span>';
			}else{
				$row[] = "<a href='#' class='selectPenjualan' data-idpenjualan='".$r->id."' data-nopenjualan='".$r->nopenjualan."' data-dismiss='modal'>".$r->nopenjualan."</span></a>";
				
			}
			$row[] = $r->nomedrec;
			$row[] = $r->nama;

			$row[] = GetAsalRujukan($r->asalrujukan);

			$row[] = $r->totalbarang;
			$row[] = $r->totalharga;


			$data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}
	function get_penjualan_detail($id) {
		$data_penjualan = $this->tpaspeng->get_penjualan_detail($id);
		$this->output->set_output(json_encode($data_penjualan));
	}
	function get_penjualan_obat_detail($id) {
		$arr['detail'] = $this->tpaspeng->get_penjualan_obat_detail($id);
		$this->output->set_output(json_encode($arr));
	}
}
