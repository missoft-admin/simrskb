<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbagi_hasil_setting extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mbagi_hasil_setting_model');
		$this->load->helper('path');
		
  }

	function index($idkategori='0',$tipe_pemilik='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['idkategori'] 			= $idkategori;
			$data['tipe_pemilik'] 			= $tipe_pemilik;
			$data['tipe_pemilik'] 			= '#';
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Bagi Hasil Approval';
			$data['content'] 		= 'Mbagi_hasil_setting/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Setting Bagi Hasil Approval",'#'),
												  array("List",'mbagi_hasil_setting')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function update($id,$config=''){
		
		if($id != ''){
			$data= $this->Mbagi_hasil_setting_model->getSpecified($id);
			// $data['list_ps']= $this->Mbagi_hasil_setting_model->list_ps();
			
			
			
			$data['config'] 		= $config;
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Setting Bagi Hasil Approval';
			$data['content']    = 'Mbagi_hasil_setting/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Setting Bagi Hasil Approval",'#'),
										array("Ubah",'mbagi_hasil_setting')
										);

			// $data['statusAvailableApoteker'] = $this->Mbagi_hasil_setting_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mbagi_hasil_setting');
		}
	}
	// function update($id,$config=''){
		
		// if($id != ''){
			// $data= $this->Mbagi_hasil_setting_model->getSpecified($id);
			// // $data['list_ps']= $this->Mbagi_hasil_setting_model->list_ps();
			
			
			
			// $data['config'] 		= $config;
			// $data['error'] 			= '';
			// $data['title'] 			= 'Ubah Setting Bagi Hasil Approval';
			// $data['content']    = 'Mbagi_hasil_setting/manage';
			// $data['breadcrum'] 	= array(
										// array("RSKB Halmahera",'#'),
										// array("Setting Bagi Hasil Approval",'#'),
										// array("Ubah",'mbagi_hasil_setting')
										// );

			// // $data['statusAvailableApoteker'] = $this->Mbagi_hasil_setting_model->getStatusAvailableApoteker();
			
			// $data = array_merge($data, backend_info());
			// $this->parser->parse('module_template', $data);
			
		// }else{
			// $this->session->set_flashdata('error',true);
			// $this->session->set_flashdata('message_flash','data tidak ditemukan.');
			// redirect('mbagi_hasil_setting');
		// }
	// }

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$nama = $this->input->post('nama');
			
		
			if ($nama !=''){
				$where .=" AND nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,M.bagian_rs,M.bagian_ps,M.tanggal_mulai,M.status
						,COUNT(DISTINCT L.step) as jml_step,COUNT(L.id)  as jml_user
						from mbagi_hasil M

						LEFT JOIN mbagi_hasil_logic L ON L.mbagi_hasil_id=M.id AND L.`status`='1'
						WHERE M.status='1'
						GROUP BY M.id
					) as tbl WHERE id IS NOT NULL ".$where."
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->nama;
          $result[] = number_format($r->bagian_rs,1).' %';
          $result[] = number_format($r->bagian_ps,1).' %';
          $result[] = HumanDateShort($r->tanggal_mulai);
          $result[] = number_format($r->jml_step,0).' Step';
          $result[] = number_format($r->jml_user,0).' User';
          $result[] = ($r->jml_step > 0 ?'<button class="btn btn-success btn-xs"><i class="fa fa-check"></i> SUDAH DISETTING</button>':'<button class="btn btn-danger btn-xs"><i class="fa fa-close"></i> BELUM DISETTING</button>');
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'mbagi_hasil_setting/update/'.$r->id.'/config" data-toggle="tooltip" title="Setting" class="btn btn-primary btn-xs"><i class="si si-settings"></i> Setting</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  //TANGGAL
  function load_setting()
    {
		$mbagi_hasil_id     		= $this->input->post('mbagi_hasil_id');


		$this->select = array();
		$this->join 	= array();
		$this->where  = array();


		$from="(
		SELECT M.id,M.mbagi_hasil_id,M.step,M.operand,M.nominal,U.`name` as user_nama,M.iduser,M.proses_tolak,M.proses_setuju FROM `mbagi_hasil_logic` M
LEFT JOIN musers U ON U.id=M.iduser
WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.status='1'
			) as tbl  ORDER BY step,id ASC";

		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('mpasien_kelompok/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

           $row[] = $r->id;//0
			$row[] = $r->step;//1
            $row[] = $r->step;
			 $row[] = (($r->operand=='>=' && $r->nominal=='0')?'BEBAS':$r->operand.' '.number_format($r->nominal,0));
            $row[] = $r->user_nama;
			$row[] = ($r->proses_setuju=='1'?'Langsung':'Menunggu User');
            $row[] = ($r->proses_tolak=='1'?'Langsung':'Menunggu User');

			$aksi       = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-primary edit" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="btn btn-xs btn-danger hapus" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function get_edit(){
		$id     = $this->input->post('id');
		$q="SELECT M.id,M.mbagi_hasil_id,M.step,M.operand,M.nominal,U.`name` as user_nama,M.iduser,M.proses_tolak,M.proses_setuju FROM `mbagi_hasil_logic` M
LEFT JOIN musers U ON U.id=M.iduser
WHERE M.id='$id'";
		$arr['detail'] =$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr['detail']));
	}
  public function save_setting()
    {
		$id_edit=$this->input->post('id_edit');
		$data=array(
				'mbagi_hasil_id'=>$this->input->post('mbagi_hasil_id'),
				'iduser'=>$this->input->post('iduser'),
				'step'=>$this->input->post('step'),
				'proses_setuju'=>$this->input->post('proses_setuju'),
				'proses_tolak'=>$this->input->post('proses_tolak'),
				'operand'=>$this->input->post('operand'),
				'nominal'=>RemoveComma($this->input->post('nominal')),
			);
	
		if ($id_edit==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');			
			$result = $this->db->insert('mbagi_hasil_logic',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');		
			$this->db->where('id',$id_edit);
			$result = $this->db->update('mbagi_hasil_logic',$data);
		}
       
		
		$this->output->set_output(json_encode($result));
    }
	public function hapus()
    {
		
        $id_edit = $this->input->post('id_edit');
		$this->db->where('id',$id_edit);	
		$data=array(
			'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
		);
       // print_r($data);
	   
		$result = $this->db->update('mbagi_hasil_logic',$data);
		echo json_encode($result);
    }
	public function hapus_setting()
    {
        $tanggal_id = $this->input->post('tanggal_id');

		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$tanggal_id);;
		$result=$this->db->update('mbagi_hasil_setting_tanggal',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function cek_duplicate_tanggal($mbagi_hasil_id,$tanggal_hari,$tanggal_id=''){
		if ($tanggal_id ==''){
			$q="SELECT COUNT(M.id)  as jml From mbagi_hasil_setting_tanggal M
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.tanggal_hari='$tanggal_hari' AND M.status='1'";			
		}else{
			$q="SELECT COUNT(M.id)  as jml From mbagi_hasil_setting_tanggal M
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.tanggal_hari='$tanggal_hari' AND M.id !='$tanggal_id' AND M.status='1'";
		}
			
		
		
		$arr['detail'] =$this->db->query($q)->row_array();		
		// $data['jml']=$row;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($data));
		
	}
	function list_user($id,$step)
	{
		$arr['detail'] = $this->Mbagi_hasil_setting_model->list_user($id,$step);
		$this->output->set_output(json_encode($arr));
	}
}
