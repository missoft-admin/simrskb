<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medc extends CI_Controller {

	/**
	 * Master EDC controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Medc_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master EDC';
		$data['content'] 		= 'Medc/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master EDC",'#'),
									    			array("List",'medc')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'lokasi'			=> '',
			'bank_id' 				=> '',
			'deskripsi' 				=> '',
			'status' 				=> '',
		);

		$data['list_user'] 			= $this->Medc_model->list_user('');
		$data['list_kas'] 			= $this->Medc_model->list_kas('');
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master EDC';
		$data['content'] 		= 'Medc/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master EDC",'#'),
									    			array("Tambah",'medc')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Medc_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'lokasi' 		=> $row->lokasi,
					'bank_id' 		=> $row->bank_id,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['list_user'] 			= $this->Medc_model->list_user($row->id);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master EDC';
				$data['content']	 	= 'Medc/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master EDC",'#'),
											    			array("Ubah",'medc')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('medc','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('medc');
		}
	}

	function delete($id){
		$this->Medc_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('medc','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		// print_r($this->input->post());exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Medc_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('medc','location');
				}
			} else {
				if($this->Medc_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('medc','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Medc/manage';

		if($id==''){
			$data['title'] = 'Tambah Master EDC';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master EDC",'#'),
															array("Tambah",'medc')
													);
		}else{
			$data['title'] = 'Ubah Master EDC';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master EDC",'#'),
															array("Ubah",'medc')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			$from="(SELECT medc.*,B.nama as bank
					 FROM medc 
					LEFT JOIN mbank B ON B.id=medc.bank_id) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
						if ($r->status=='1'){
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'medc/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'medc" data-urlremove="'.site_url().'medc/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						}else{
							$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_obat" data-urlremove="'.site_url().'medc/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
						}
					// }
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->bank;
          $row[] = $r->lokasi;
          $row[] = get_status($r->status);
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function aktifkan($id){
		
		$this->Medc_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('medc','location');
	}

}
