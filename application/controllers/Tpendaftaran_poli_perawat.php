<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpendaftaran_poli_perawat extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_perawat_model');
		$this->load->model('Tpendaftaran_poli_ttv_model','ttv_model');
		$this->load->helper('path');
		
  }

  function get_logic_formulir($pendaftaran_id){
	  $q="SELECT H.*
		, (SELECT  H.tanggal FROM tpoliklinik_pendaftaran D WHERE D.idpasien=H.idpasien AND H.id!=D.id AND D.idpoliklinik=H.idpoliklinik ORDER BY D.tanggal DESC LIMIT 1) as tgl_akhir
		FROM tpoliklinik_pendaftaran H
		WHERE H.id='74342'
		GROUP BY H.id
		";
	  $data_pasien=$this->db->query($q)->row();
	  $hasil='#';
	  if ($data_pasien){
		  
	  }
	  echo $hasil;
  }
  function index($tab='1'){
	    $log['path_tindakan']='tpendaftaran_poli_perawat';
		$this->session->set_userdata($log);
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1783'))){
			// print_r($data);exit;	
			// $data['list_ruangan'] 			= $this->Tpendaftaran_poli_perawat_model->list_ruangan();
			$data['list_poli'] 			= $this->ttv_model->list_poli();
			$data['list_dokter'] 			= $this->ttv_model->list_dokter();
			$data['list_ruang'] 			= $this->ttv_model->list_ruang();
			
			$data['iddokter'] 			= '#';
			$data['ruangan_id'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			
			$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
			$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'Perawat';
			$data['content'] 		= 'Tpendaftaran_poli_perawat/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("Perawat",'tpendaftaran_poli_perawat')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()	{
			$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$ruangan_id =$this->input->post('ruangan_id');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
				$where .=" OR (H.nopendaftaran LIKE '%".$pencarian."%')";
			}
			if ($tab=='2'){
				$where .=" AND (H.status_input_perawat) = '0'";
			}
			if ($tab=='3'){
				$where .=" AND (H.status_input_perawat) = '1'";
			}
			if ($idpoli!='#'){
				$where .=" AND (H.idpoliklinik) = '$idpoli'";
			}else{
				$where .=" AND (H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_emergency)) ";
				
			}
			if ($iddokter!='#'){
				$where .=" AND (H.iddokter) = '$iddokter'";
			}
			if ($ruangan_id!='#'){
				$where .=" AND (H.ruangan_id) = '$ruangan_id'";
			}
		
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT 
							TP.suhu,TP.nadi,TP.nafas,TP.td_sistole,TP.td_diastole,TP.tinggi_badan,TP.berat_badan
							,mnadi.warna as warna_nadi
							,mnafas.warna as warna_nafas
							,mtd_sistole.warna as warna_sistole
							,mtd_diastole.warna as warna_diastole
							,msuhu.warna as warna_suhu
							,TP.berat_badan/((TP.tinggi_badan/100)*2) as masa_tubuh,mberat.warna as warna_berat
							,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
							,MK.nama as nama_kelompok,MR.nama as nama_rekanan
							,H.*
							,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header 
							FROM tpoliklinik_pendaftaran H
							LEFT JOIN mdokter MD ON MD.id=H.iddokter
							LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
							LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
							LEFT JOIN (
								".get_alergi_sql()."
							) A ON A.idpasien=H.idpasien
							LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
							LEFT JOIN (SELECT * FROM (SELECT * FROM tpoliklinik_ttv WHERE status_ttv > 1 ORDER BY tanggal_input DESC) XX GROUP BY pendaftaran_id) TP ON TP.pendaftaran_id = H.id
							LEFT JOIN mnadi ON TP.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
							LEFT JOIN mnafas ON TP.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
							LEFT JOIN mtd_sistole ON TP.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
							LEFT JOIN mtd_diastole ON TP.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
							LEFT JOIN msuhu ON TP.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
							LEFT JOIN mberat ON TP.berat_badan/((TP.tinggi_badan/100)*2) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'

							WHERE H.tanggal IS NOT NULL  ".$where."
							GROUP BY H.id
						ORDER BY H.tanggal DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  // tpendaftaran_poli_perawat/tindakan/74329/erm_rj/input_ttv
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"><a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_cppt" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> LAYANI</a> </div>
									'.div_panel_kendali($user_acces_form,$r->id).'
									<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Call"  class="btn btn-block btn-success btn-xs"><i class="fa fa-save"></i> FINAL TRANSAKSI</button> </div>
								</td>
							</tr>
						</tbody>
					</table>';
		  if ($r->status=='0'){
			  $btn_1 ='
			 <table class="block-table text-left">
				<tbody>
					<tr>
						
						<td class="bg-white" style="width: 100%;">
							<div class="push-5-t"><button type="button" data-toggle="tooltip" title="batalkan"  class="btn btn-block btn-danger btn-xs "><i class="fa fa-times"></i> DIBATALKAN</button> </div>
							<div class="text-center push-5-t"><strong>
								'.get_nama_user($r->deleted_by).'<br>'.$r->deleted_date.'</strong>
							</div>
						</td>
					</tr>
				</tbody>
			</table>';
		  }			
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
										<div class="btn-group" role="group">
											<button class="btn btn-default btn-sm" type="button" '.($r->riwayat_alergi_header?'':'disabled').' onclick="lihat_his_alergi_pasien('.$r->idpasien.')">'.text_alergi($r->riwayat_alergi_header).'</button>
											<button class="btn btn-danger  btn-sm" type="button" '.($r->riwayat_alergi_header?'':'disabled').' onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
											&nbsp;&nbsp;&nbsp;'.($r->statuspasienbaru=='0'?text_info('Pasien Lama'):text_danger('Pasien Baru')).'
										</div>
									</div>

									<br>

									'.generate_erm_pemeriksaan_dropdown($r->id, $r->idtipe).'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class=" text-muted " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nadi :</div>
									<div class=" text-muted " style="color:'.$r->warna_nadi.'"> '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nafas :</div>
									<div class=" text-muted " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class=" text-muted "> <label  class="text-muted" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="text-muted " style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class=" text-muted " style="color:'.$r->warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class=" text-muted style="color:'.$r->warna_berat.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								
							</tr>
						</tbody>
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->idkelompokpasien=='1'?'ASURANSI : '.$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								'.($r->status!='0' ?'
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.($r->status_input_perawat?text_success('TELAH DIPERIKSA'):text_warning('BELUM DIPERIKSA')).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>':$btn_1).'
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	
}	
