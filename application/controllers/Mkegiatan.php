<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkegiatan extends CI_Controller {

	/**
	 * Master Kegiatan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mkegiatan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Kegiatan';
		$data['content'] 		= 'Mkegiatan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Kegiatan",'#'),
									    			array("List",'mkegiatan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'status_kegiatan' 					=> '1',
			'nama' 					=> '',
			'idklasifikasi' 					=> '',
			'urutan' 					=> '',
			'deskripsi'			=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Kegiatan';
		$data['content'] 		= 'Mkegiatan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Kegiatan",'#'),
									    			array("Tambah",'mkegiatan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mkegiatan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'status_kegiatan' 					=> $row->status_kegiatan,
					'nama' 					=> $row->nama,
					'idklasifikasi' 					=> $row->idklasifikasi,
					'urutan' 					=> $row->urutan,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Kegiatan';
				$data['content']	 	= 'Mkegiatan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Kegiatan",'#'),
											    			array("Ubah",'mkegiatan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mkegiatan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mkegiatan');
		}
	}

	function delete($id){
		$this->Mkegiatan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mkegiatan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mkegiatan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkegiatan','location');
				}
			} else {
				if($this->Mkegiatan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkegiatan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mkegiatan/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Kegiatan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Kegiatan",'#'),
															array("Tambah",'mkegiatan')
													);
		}else{
			$data['title'] = 'Ubah Master Kegiatan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Kegiatan",'#'),
															array("Ubah",'mkegiatan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $idklasifikasi=$this->input->post('idklasifikasi');
	  $nama=$this->input->post('nama');
	  $where='';
	  if ($idklasifikasi !='#'){
		  $where .=" AND M.idklasifikasi='$idklasifikasi'";
	  }
	  if ($nama !=''){
		  $where .=" AND M.nama LIKE '%".$nama."%'";
	  }
		
	  $from="(SELECT M.id,M.status_kegiatan,M.nama,M.urutan,M.deskripsi,P.nama as klasifikasi from mkegiatan_rka M
				LEFT JOIN mklasifikasi P ON P.id=M.idklasifikasi
				WHERE M.`status`='1' ".$where."
				
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array('urutan','nama','klasifikasi','deskripsi','status_kegiatan');
			$this->group  = array();

      $this->column_search   = array('urutan','nama','klasifikasi','deskripsi','status_kegiatan');
      $this->column_order    = array('urutan','nama','klasifikasi','deskripsi','status_kegiatan');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'mkegiatan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mkegiatan" data-urlremove="'.site_url().'mkegiatan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->klasifikasi;
          $row[] = ($r->status_kegiatan=='1'?'<span class="label label-primary">PENGULANGAN</span>':'<span class="label label-danger">NON</span>');
          $row[] = $r->urutan;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }

}
