<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbank extends CI_Controller {

	/**
	 * Bank controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mbank_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Bank';
		$data['content'] 		= 'Mbank/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Bank",'#'),
									    			array("List",'mbank')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'atasnama' 			=> '',
			'norekening' 		=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Bank';
		$data['content'] 		= 'Mbank/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Bank",'#'),
									    			array("Tambah",'mbank')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mbank_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'atasnama' 			=> $row->atasnama,
					'norekening' 		=> $row->norekening,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Bank';
				$data['content']	 	= 'Mbank/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Bank",'#'),
											    			array("Ubah",'mbank')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mbank','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mbank');
		}
	}

	function delete($id){
		
		$this->Mbank_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mbank','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('atasnama', 'Atas Nama', 'trim|required');
		$this->form_validation->set_rules('norekening', 'No. Rekening', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mbank_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mbank','location');
				}
			} else {
				if($this->Mbank_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mbank','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mbank/manage';

		if($id==''){
			$data['title'] = 'Tambah Bank';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Bank",'#'),
															array("Tambah",'mbank')
													);
		}else{
			$data['title'] = 'Ubah Bank';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Bank",'#'),
															array("Ubah",'mbank')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mbank';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama','atasnama','norekening');
      $this->column_order    = array('nama','atasnama','norekening');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->atasnama;
          $row[] = $r->norekening;
	        $aksi = '<div class="btn-group">';
	            if (UserAccesForm($user_acces_form,array('209'))){
	                $aksi .= '<a href="'.site_url().'mbank/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
	            }
	            if (UserAccesForm($user_acces_form,array('210'))){
	                $aksi .= '<a href="#" data-urlindex="'.site_url().'mrak" data-urlremove="'.site_url().'mbank/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
	            }
	            $aksi .= '</div>';
	        $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
