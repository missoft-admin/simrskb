<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

defined('BASEPATH') or exit('No direct script access allowed');

class Lsurvey extends CI_Controller
{

    /**
     * Laporan Survey Internal controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Lsurvey_model');
        $this->load->model('Tpendaftaran_poli_ttv_model');
        $this->load->model('Tsurvey_model', 'Tsurvey_model');
    }

    public function internal()
    {
		$tanggal_awal = date("Y-m-01",time());
		$tanggal_akhir = date("Y-m-t",time());
		$tanggal_1=date_create($tanggal_awal);
		$tanggal_2=date_create($tanggal_akhir);
		$tanggal_1= date_format($tanggal_1,"d/m/Y");
		$tanggal_2= date_format($tanggal_2,"d/m/Y");
        $data = array(
            'tipe_laporan' => 'internal',
            'msurvey_kepuasan_id' => '1',
            'periode_tanggal' => '2',
            'tanggal_1_trx' => $tanggal_1,
            'tanggal_2_trx' => $tanggal_2,
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Laporan Survey Internal';
        $data['content'] 		= 'Lsurvey/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Laporan Survey Internal",'lsurvey/index'),
            array("List",'#')
        );

		$data['list_survey'] = $this->Lsurvey_model->list_survey();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function external()
    {
		$tanggal_awal = date("Y-m-01",time());
		$tanggal_akhir = date("Y-m-t",time());
		$tanggal_1=date_create($tanggal_awal);
		$tanggal_2=date_create($tanggal_akhir);
		$tanggal_1= date_format($tanggal_1,"d/m/Y");
		$tanggal_2= date_format($tanggal_2,"d/m/Y");
        $data = array(
            'tipe_laporan' => 'external',
            'msurvey_kepuasan_id' => '1',
            'periode_tanggal' => '2',
            'tanggal_1_trx' => $tanggal_1,
            'tanggal_2_trx' => $tanggal_2,
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Laporan Survey External';
        $data['content'] 		= 'Lsurvey/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Laporan Survey Internal",'lsurvey/index'),
            array("List",'#')
        );

		$data['list_survey'] = $this->Lsurvey_model->list_survey();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function generate_header(){
		$msurvey_kepuasan_id=$this->input->post('msurvey_kepuasan_id');
		$q="
			SELECT H.inisial FROM `msurvey_kepuasan_param` H WHERE H.staktif='1' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' ORDER BY H.id ASC
		";
		$opsi='';
		$list_data=$this->db->query($q)->result();
		$no=0;
		foreach($list_data as $row){
			$no++;
			$opsi .='<th>'.$row->inisial.'</th>';
		}
			$opsi .='<th>TOTAL SKOR</th>';
			$opsi .='<th>ACTION</th>';
		$arr['detail']=$opsi;
		$arr['no']=$no+2;
		$this->output->set_output(json_encode($arr));
	}
	function generate_header_ex(){
		$msurvey_kepuasan_id=$this->input->post('msurvey_kepuasan_id');
		$tanggal_1_trx = $this->input->post('tanggal_1_trx');			
		$tanggal_2_trx = $this->input->post('tanggal_2_trx');
		$q="SELECT T.*,N.flag_nilai,N.ref_nilai FROM (
			SELECT FORMAT(AVG(H.kepuasan_mas),0) as kepuasan_mas,H.msurvey_kepuasan_id  FROM tpoliklinik_survey_kepuasan H
			WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id'
			AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
			) T
			LEFT JOIN msurvey_kepuasan_setting_nilai_mas N ON N.msurvey_kepuasan_id=T.msurvey_kepuasan_id
			WHERE T.kepuasan_mas BETWEEN N.skor_1 AND N.skor_2";
		$arr=$this->db->query($q)->row_array();	
		$q="
			SELECT H.inisial FROM `msurvey_kepuasan_param` H WHERE H.staktif='1' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' ORDER BY H.id ASC
		";
		$opsi='';
		$list_data=$this->db->query($q)->result();
		$no=0;
		foreach($list_data as $row){
			$no++;
			$opsi .='<th>'.$row->inisial.'</th>';
		}
			$opsi .='<th>NILAI PER UNSUR</th>';
			$opsi .='<th>NRR PER UNSUR</th>';
			$opsi .='<th>NRR TERTIMBANG PER UNSUR</th>';
			$opsi .='<th>SATUAN KEPUASAN MASYARAKAT</th>';
			$opsi .='<th>HASIL</th>';
			$opsi .='<th>ACTION</th>';
		$arr['detail']=$opsi;
		$arr['no']=$no+6;

		

		$this->output->set_output(json_encode($arr));
	}
	
    function loadInternal()
   {
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$user_id=$this->session->userdata('user_id');
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$arr_data=array();
		$msurvey_kepuasan_id = $this->input->post('msurvey_kepuasan_id');
		$tanggal_1_trx = $this->input->post('tanggal_1_trx');			
		$tanggal_2_trx = $this->input->post('tanggal_2_trx');	
		
		$q="
			SELECT H.id as param_id,H.inisial FROM `msurvey_kepuasan_param` H WHERE H.staktif='1' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' ORDER BY H.id ASC
		";
		
		$list_header=$this->db->query($q)->result();
		
		$q="
			SELECT M.deskripsi_nama as jawaban,D.*
			FROM tpoliklinik_survey_kepuasan H
			LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
			LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=D.jawaban_id
			WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' 
			AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
			ORDER BY H.assesmen_id ASC,D.id ASC
		";
		$jawaban_arr=$this->db->query($q)->result_array();
		
		$q="
			SELECT D.assesmen_id,D.param_id,SUM(D.jawaban_skor) as jawaban_skor
			FROM tpoliklinik_survey_kepuasan H
			LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
			LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=D.jawaban_id
			WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' 
			AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
			GROUP by D.param_id
			ORDER BY H.assesmen_id ASC,D.id ASC
		";
		$jawaban_arr_total=$this->db->query($q)->result_array();
		
			
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id
					,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,CASE WHEN H.st_ranap='0' THEN MP.title ELSE RI.title END as title
					,CASE WHEN H.st_ranap='0' THEN MP.namapasien ELSE RI.namapasien END as namapasien
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE RI.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MPOL.nama ELSE 
						CONCAT(COALESCE(mruangan.nama,''),' - ',COALESCE(mkelas.nama,''),' - ',COALESCE(mbed.nama,''))
					
					END as poli
					,CASE WHEN H.st_ranap='0' THEN MP.iddokter ELSE RI.iddokterpenanggungjawab END as iddokter
					,CASE WHEN H.st_ranap='0' THEN MD.nama ELSE MDRI.nama END as dokter
					,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd,H.total_skor_survey_kepuasan,H.hasil_penilaian,H.st_ranap,H.pendaftaran_id_ranap,H.nama_profile
					FROM `tpoliklinik_survey_kepuasan` H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
					LEFT JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mdokter MDRI ON MDRI.id=RI.iddokterpenanggungjawab
					LEFT JOIN mruangan ON mruangan.id=RI.idruangan
					LEFT JOIN mbed ON mbed.id=RI.idbed
					LEFT JOIN mkelas ON mkelas.id=RI.idkelas
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id'  
					AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
			
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = 0;
		
		  foreach ($list as $r) {
			  $result = array();
			  $assesmen_id=$r->assesmen_id;
			  
			  $no++;
			  $result[] = $no;
			  $result[] = $r->nopendaftaran;
			  $result[] = $r->title.' '.$r->namapasien;
			  $result[] = $r->nama_profile;
			  foreach($list_header as $row){
				  $param_id=$row->param_id;
				  $data_survey = array_filter($jawaban_arr, function($var) use ($assesmen_id,$param_id) { 
					return ($var['assesmen_id'] == $assesmen_id && $var['param_id'] == $param_id);
				  });	
					// print_r($arr_jadwal);exit;
				  $data_survey = reset($data_survey);
				  if ($data_survey){
					  $jawaban='<strong>'.strtoupper($data_survey['jawaban']).'</strong><br>('.$data_survey['jawaban_skor'].')';
				  }else{
					  $jawaban='';
				  }
				  $result[] = $jawaban;
			  }
				if ($r->st_ranap=='1'){
				$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_survey/input_survey/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					
				}else{
					$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id.'/0/erm_survey/input_survey/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					
				}
				$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurvey/cetak_survey_kepuasan/'.$r->assesmen_id.'/1/0"  type="button" title="Cetak" type="button"><i class="fa fa-print"></i></a>';

			  $result[] = '<strong>'.number_format($r->total_skor_survey_kepuasan,0).'</strong>';
			  $result[] = $btn_lihat.$btn_cetak;
			  $data[] = $result;
			  
		  }
		  $result = array();
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '<strong>TOTAL</strong>';
		  $total=0;
		  foreach($list_header as $row){
			  $param_id=$row->param_id;
			  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_survey = reset($data_survey);
			  if ($data_survey){
				  $jawaban='<strong>'.strtoupper($data_survey['jawaban_skor']).'</strong>';
				  $total=$total+$data_survey['jawaban_skor'];
			  }else{
				  $jawaban='';
			  }
			  $result[] = $jawaban;
		  }
		  $result[] = '<strong>'.$total.'</strong>';
		  $result[] = '';
		  $data[] = $result;
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	}
	function loadexternal()
   {
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$user_id=$this->session->userdata('user_id');
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$arr_data=array();
		$msurvey_kepuasan_id = $this->input->post('msurvey_kepuasan_id');
		$tanggal_1_trx = $this->input->post('tanggal_1_trx');			
		$tanggal_2_trx = $this->input->post('tanggal_2_trx');	
		
		$q="
			SELECT H.id as param_id,H.inisial FROM `msurvey_kepuasan_param` H WHERE H.staktif='1' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' ORDER BY H.id ASC
		";
		
		$list_header=$this->db->query($q)->result();
		
		$q="
			SELECT M.deskripsi_nama as jawaban,D.*
			FROM tpoliklinik_survey_kepuasan H
			LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
			LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=D.jawaban_id
			WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' 
			AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
			ORDER BY H.assesmen_id ASC,D.id ASC
		";
		$jawaban_arr=$this->db->query($q)->result_array();
		
		$q="
			SELECT T.*,N.flag_nilai,N.ref_nilai FROM (
				SELECT T.*,
				CASE WHEN T.nilai_per_unsur > 0 THEN T.nilai_per_unsur/T.jumlah_jawaban ELSE 0 END as nrr_per_unsur
				,M.nilai_tertimbang
				,CASE WHEN T.nilai_per_unsur > 0 THEN (T.nilai_per_unsur/T.jumlah_jawaban)*M.nilai_tertimbang ELSE 0 END as nrr_tertimbang
				,CASE WHEN T.nilai_per_unsur > 0 THEN (T.nilai_per_unsur/T.jumlah_jawaban)*M.nilai_satuan ELSE 0 END as satuan_kepuasan_mas
				FROM (
				SELECT H.msurvey_kepuasan_id, D.param_id
							,SUM(CASE WHEN D.st_nilai=1 THEN COALESCE(D.jawaban_skor,0) ELSE 0 END) as nilai_per_unsur 
							,SUM(CASE WHEN D.st_nilai=1 THEN 1 ELSE 0 END) as jumlah_jawaban 
							FROM tpoliklinik_survey_kepuasan H
							LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
							LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=D.jawaban_id
							WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' 
							AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
							
							GROUP by D.param_id
							ORDER BY H.assesmen_id ASC,D.id ASC
				) T 
				LEFT JOIN msurvey_kepuasan M ON M.id=T.msurvey_kepuasan_id
				) T 
				LEFT JOIN msurvey_kepuasan_setting_nilai_mas N ON N.msurvey_kepuasan_id=T.msurvey_kepuasan_id
				WHERE T.satuan_kepuasan_mas BETWEEN N.skor_1 AND N.skor_2

		";
		$jawaban_arr_total=$this->db->query($q)->result_array();
		
			
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id
					,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,CASE WHEN H.st_ranap='0' THEN MP.title ELSE RI.title END as title
					,CASE WHEN H.st_ranap='0' THEN MP.namapasien ELSE RI.namapasien END as namapasien
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE RI.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MPOL.nama ELSE 
						CONCAT(COALESCE(mruangan.nama,''),' - ',COALESCE(mkelas.nama,''),' - ',COALESCE(mbed.nama,''))
					
					END as poli
					,CASE WHEN H.st_ranap='0' THEN MP.iddokter ELSE RI.iddokterpenanggungjawab END as iddokter
					,CASE WHEN H.st_ranap='0' THEN MD.nama ELSE MDRI.nama END as dokter
					,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd,H.total_skor_survey_kepuasan,H.hasil_penilaian,H.st_ranap,H.pendaftaran_id_ranap,H.nama_profile
					,H.nilai_per_unsur
					,H.jumlah_jawaban
					,H.nrr_per_unsur
					,H.nrr_tertimbang
					,H.kepuasan_mas
					,H.flag_nilai_mas
					,H.hasil_flag

					FROM `tpoliklinik_survey_kepuasan` H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
					LEFT JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mdokter MDRI ON MDRI.id=RI.iddokterpenanggungjawab
					LEFT JOIN mruangan ON mruangan.id=RI.idruangan
					LEFT JOIN mbed ON mbed.id=RI.idbed
					LEFT JOIN mkelas ON mkelas.id=RI.idkelas
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id'  
					AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
			
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = 0;
		
		  foreach ($list as $r) {
			  $result = array();
			  $assesmen_id=$r->assesmen_id;
			  
			  $no++;
			  $result[] = $no;
			  $result[] = $r->nopendaftaran;
			  $result[] = $r->title.' '.$r->namapasien;
			  $result[] = $r->nama_profile;
			  foreach($list_header as $row){
				  $param_id=$row->param_id;
				  $data_survey = array_filter($jawaban_arr, function($var) use ($assesmen_id,$param_id) { 
					return ($var['assesmen_id'] == $assesmen_id && $var['param_id'] == $param_id);
				  });	
					// print_r($arr_jadwal);exit;
				  $data_survey = reset($data_survey);
				  if ($data_survey){
					  $jawaban='<strong>'.strtoupper($data_survey['jawaban']).'</strong><br>('.$data_survey['jawaban_skor'].')';
				  }else{
					  $jawaban='';
				  }
				  $result[] = $jawaban;
			  }
				
			  $result[] = '<strong>'.number_format($r->nilai_per_unsur,0).'</strong>';
			  $result[] = '<strong>'.number_format($r->nrr_per_unsur,5).'</strong>';
			  $result[] = '<strong>'.number_format($r->nrr_tertimbang,5).'</strong>';
			  $result[] = '<strong>'.number_format($r->kepuasan_mas,0).'</strong>';
			  $result[] = '<strong>'.$r->flag_nilai_mas.'<br>'.($r->hasil_flag).'</strong>';
				if ($r->st_ranap=='1'){
				$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_survey/input_survey/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					
				}else{
					$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id.'/0/erm_survey/input_survey/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					
				}
				$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurvey/cetak_survey_kepuasan/'.$r->assesmen_id.'/2/0"  type="button" title="Cetak" type="button"><i class="fa fa-print"></i></a>';

			  $result[] = $btn_lihat.$btn_cetak;
			  $data[] = $result;
			  
		  }
		  $result = array();
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '<strong>NILAI PER UNSUR</strong>';
		  $total=0;
		  foreach($list_header as $row){
			  $param_id=$row->param_id;
			  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_survey = reset($data_survey);
			  if ($data_survey){
				  $jawaban='<strong>'.strtoupper($data_survey['nilai_per_unsur']).'</strong>';
				  $total=$total+$data_survey['nilai_per_unsur'];
			  }else{
				  $jawaban='';
			  }
			  $result[] = $jawaban;
		  }
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $data[] = $result;


		  $result = array();
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '<strong>NRR PER UNSUR</strong>';
		  $total=0;
		  foreach($list_header as $row){
			  $param_id=$row->param_id;
			  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_survey = reset($data_survey);
			  if ($data_survey){
				  $jawaban='<strong>'.strtoupper($data_survey['nrr_per_unsur']).'</strong>';
				  $total=$total+$data_survey['nrr_per_unsur'];
			  }else{
				  $jawaban='';
			  }
			  $result[] = $jawaban;
		  }
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $data[] = $result;

		  $result = array();
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '<strong>NRR TERTIMBANG PER UNSUR</strong>';
		  $total=0;
		  foreach($list_header as $row){
			  $param_id=$row->param_id;
			  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_survey = reset($data_survey);
			  if ($data_survey){
				  $jawaban='<strong>'.strtoupper($data_survey['nrr_tertimbang']).'</strong>';
				  $total=$total+$data_survey['nrr_tertimbang'];
			  }else{
				  $jawaban='';
			  }
			  $result[] = $jawaban;
		  }
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $data[] = $result;

		$result = array();
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '<strong>SATUAN KEPUASAN MASYARAKAT</strong>';
		  $total=0;
		  foreach($list_header as $row){
			  $param_id=$row->param_id;
			  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_survey = reset($data_survey);
			  if ($data_survey){
				  $jawaban='<strong>'.strtoupper($data_survey['satuan_kepuasan_mas']).'</strong>';
				  $total=$total+$data_survey['satuan_kepuasan_mas'];
			  }else{
				  $jawaban='';
			  }
			  $result[] = $jawaban;
		  }
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $data[] = $result;

		$result = array();
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '<strong>HASIL SURVEY</strong>';
		  $total=0;
		  foreach($list_header as $row){
			  $param_id=$row->param_id;
			  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });	
				// print_r($arr_jadwal);exit;
			  $data_survey = reset($data_survey);
			  if ($data_survey){
				  $jawaban='<strong>'.strtoupper($data_survey['flag_nilai'].'<br>'.$data_survey['ref_nilai']).'</strong>';
			  }else{
				  $jawaban='';
			  }
			  $result[] = $jawaban;
		  }
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $result[] = '';
		  $data[] = $result;

		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	}
	function list_tipe($idtipe='1'){
		$tanggal_1=date_create(date('Y-m-d'));
		$tanggal_2=date_create(date('Y-m-d'));
		if ($idtipe=='1'){
			$tanggal_1=date_create(date('Y-m-d'));
			$tanggal_2=date_create(date('Y-m-d'));
		}
		if ($idtipe=='2'){
			$tanggal_awal = date("Y-m-01",time());
			$tanggal_akhir = date("Y-m-t",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		if ($idtipe=='3'){
			$tanggal_awal = date("Y-01-01",time());
			$tanggal_akhir = date("Y-12-t",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		if ($idtipe=='4'){
			$tanggal_awal = date("Y-01-01",time());
			$tanggal_akhir = date("Y-03-31",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		if ($idtipe=='5'){
			$tanggal_awal = date("Y-04-01",time());
			$tanggal_akhir = date("Y-06-30",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		if ($idtipe=='6'){
			$tanggal_awal = date("Y-07-01",time());
			$tanggal_akhir = date("Y-09-30",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		if ($idtipe=='7'){
			$tanggal_awal = date("Y-10-01",time());
			$tanggal_akhir = date("Y-12-t",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		if ($idtipe=='8'){
			$tanggal_awal = date("Y-01-01",time());
			$tanggal_akhir = date("Y-06-30",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		if ($idtipe=='9'){
			$tanggal_awal = date("Y-07-01",time());
			$tanggal_akhir = date("Y-12-t",time());
			$tanggal_1=date_create($tanggal_awal);
			$tanggal_2=date_create($tanggal_akhir);
		}
		
		
		$tanggal_1= date_format($tanggal_1,"d/m/Y");
		$tanggal_2= date_format($tanggal_2,"d/m/Y");
		$arr['tanggal_1']=$tanggal_1;
		$arr['tanggal_2']=$tanggal_2;
		$this->output->set_output(json_encode($arr));
	}
    public function cetak(){
		$msurvey_kepuasan_id=$this->input->post('msurvey_kepuasan_id');
		$periode_tanggal=$this->input->post('periode_tanggal');
		$tanggal_1_trx=$this->input->post('tanggal_1_trx');
		$tanggal_2_trx=$this->input->post('tanggal_2_trx');
		$btn_print=$this->input->post('btn_print');
		$tipe_laporan=$this->input->post('tipe_laporan');
		$data=array(
			'msurvey_kepuasan_id' => $msurvey_kepuasan_id,
			'periode_tanggal' => $periode_tanggal,
			'tanggal_1_trx' => $tanggal_1_trx,
			'tanggal_2_trx' => $tanggal_2_trx,
			'btn_print' => $btn_print,
			'tipe_laporan' => $tipe_laporan,
		);
		// print_r($this->input->post('tanggal_2_trx'));exit;
		$q="
			SELECT H.id as param_id,H.inisial FROM `msurvey_kepuasan_param` H WHERE H.staktif='1' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' ORDER BY H.id ASC
		";
		
		$list_header=$this->db->query($q)->result();
		
		$q="
			SELECT M.deskripsi_nama as jawaban,D.*
			FROM tpoliklinik_survey_kepuasan H
			LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
			LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=D.jawaban_id
			WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' 
			AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
			ORDER BY H.assesmen_id ASC,D.id ASC
		";
		$jawaban_arr=$this->db->query($q)->result_array();
		
		if ($tipe_laporan=='internal'){
			$q="
			SELECT D.assesmen_id,D.param_id,SUM(D.jawaban_skor) as jawaban_skor
			FROM tpoliklinik_survey_kepuasan H
			LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
			LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=D.jawaban_id
			WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' 
			AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
			GROUP by D.param_id
			ORDER BY H.assesmen_id ASC,D.id ASC
		";
		}else{
			$q="
			SELECT T.*,N.flag_nilai,N.ref_nilai FROM (
				SELECT T.*,
				CASE WHEN T.nilai_per_unsur > 0 THEN T.nilai_per_unsur/T.jumlah_jawaban ELSE 0 END as nrr_per_unsur
				,M.nilai_tertimbang
				,CASE WHEN T.nilai_per_unsur > 0 THEN (T.nilai_per_unsur/T.jumlah_jawaban)*M.nilai_tertimbang ELSE 0 END as nrr_tertimbang
				,CASE WHEN T.nilai_per_unsur > 0 THEN (T.nilai_per_unsur/T.jumlah_jawaban)*M.nilai_satuan ELSE 0 END as satuan_kepuasan_mas
				FROM (
				SELECT H.msurvey_kepuasan_id, D.param_id
							,SUM(CASE WHEN D.st_nilai=1 THEN COALESCE(D.jawaban_skor,0) ELSE 0 END) as nilai_per_unsur 
							,SUM(CASE WHEN D.st_nilai=1 THEN 1 ELSE 0 END) as jumlah_jawaban 
							FROM tpoliklinik_survey_kepuasan H
							LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
							LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=D.jawaban_id
							WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id' 
							AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
							
							GROUP by D.param_id
							ORDER BY H.assesmen_id ASC,D.id ASC
				) T 
				LEFT JOIN msurvey_kepuasan M ON M.id=T.msurvey_kepuasan_id
				) T 
				LEFT JOIN msurvey_kepuasan_setting_nilai_mas N ON N.msurvey_kepuasan_id=T.msurvey_kepuasan_id
				WHERE T.satuan_kepuasan_mas BETWEEN N.skor_1 AND N.skor_2

		";
		}
		// print_r($q);exit;
		
		$jawaban_arr_total=$this->db->query($q)->result_array();
		
		$q="
			SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id
					,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,CASE WHEN H.st_ranap='0' THEN MP.title ELSE RI.title END as title
					,CASE WHEN H.st_ranap='0' THEN MP.namapasien ELSE RI.namapasien END as namapasien
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE RI.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MPOL.nama ELSE 
						CONCAT(COALESCE(mruangan.nama,''),' - ',COALESCE(mkelas.nama,''),' - ',COALESCE(mbed.nama,''))
					
					END as poli
					,CASE WHEN H.st_ranap='0' THEN MP.iddokter ELSE RI.iddokterpenanggungjawab END as iddokter
					,CASE WHEN H.st_ranap='0' THEN MD.nama ELSE MDRI.nama END as dokter
					,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd,H.total_skor_survey_kepuasan,H.hasil_penilaian,H.st_ranap,H.pendaftaran_id_ranap,H.nama_profile
					,H.nilai_per_unsur
					,H.jumlah_jawaban
					,H.nrr_per_unsur
					,H.nrr_tertimbang
					,H.kepuasan_mas
					,H.flag_nilai_mas
					,H.hasil_flag

					FROM `tpoliklinik_survey_kepuasan` H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
					LEFT JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mdokter MDRI ON MDRI.id=RI.iddokterpenanggungjawab
					LEFT JOIN mruangan ON mruangan.id=RI.idruangan
					LEFT JOIN mbed ON mbed.id=RI.idbed
					LEFT JOIN mkelas ON mkelas.id=RI.idkelas
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id'  
					AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2_trx)."'
					ORDER BY H.tanggal_input DESC
		";
		$list_data=$this->db->query($q)->result();
		$setting_survey=$this->Tpendaftaran_poli_ttv_model->setting_survey();
		$setting_survey_label=$this->Tsurvey_model->setting_survey_label();
		$data['list_data']=$list_data;
		$data['list_header']=$list_header;
		$data['jawaban_arr']=$jawaban_arr;
		$data['jawaban_arr_total']=$jawaban_arr_total;
		// print_r($jawaban_arr_total);exit;
		$data = array_merge($data,$setting_survey,$setting_survey_label);
		
		
		if ($btn_print=='1'){
			$this->print_laporan_survey($data);
		}else{
			
			$this->export_excel($data);
		}
		// print_r($this->input->post());exit;
	}
    public function print_laporan_survey($data)
    {
		$st_create='0';
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		// $data=array();
		
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
        $data['title']=$data['judul_header_ina'] ;
        $nama_file=$data['judul_header_ina'] .' '.$data['tipe_laporan'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Lsurvey/survey_pdf', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'landscape');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
		
    }

    
	public function export_excel($data)
    {
		

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Kartu Stok');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(50);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

				// Set Title
				

				// Set Periode
				$activeSheet->setCellValue('B6', "Periode");
				$activeSheet->setCellValue('C6', $data['tanggal_1_trx'].' s/d '.$data['tanggal_2_trx']);

				// Set Header Column
				$activeSheet->setCellValue('B8', "NO");
				$activeSheet->setCellValue('C8', "NO PENDAFTARAN");
				$activeSheet->setCellValue('D8', "DATA PASIEN");
				$activeSheet->setCellValue('E8', "KORESPONDEN");
				$last_abjad="E";
				foreach($data['list_header'] as $row){
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.'8', $row->inisial);
				}
				$tipe_laporan=$data['tipe_laporan'];
				if ($tipe_laporan=='internal'){
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.'8', 'TOTAL SKOR');
				}else{
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.'8', 'NILAI PER UNSUR');
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.'8', 'NRR PER UNSUR');
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.'8', 'NRR TERTIMBANG PER UNSUR');
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.'8', 'SATUAN KEPUASAN MASYARAKAT');
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.'8', 'HASIL');
				}
				
				
				
				$activeSheet->getStyle('B8:'.$last_abjad.'8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle('B8:'.$last_abjad.'8')->applyFromArray(
					array(
						'borders' => array(
							'allborders' => array(
							  'style' => PHPExcel_Style_Border::BORDER_THIN,
			        ),
              'top' => array(
								'style' => PHPExcel_Style_Border::BORDER_THICK,
							)
						),
						'font'  => array(
							'bold'  => true,
							'size'  => 11,
							'name'  => 'Calibri'
						)
					)
				);
				$activeSheet->setCellValue('B5', $data['judul_header_ina'].' '.strtoupper($data['tipe_laporan']));
				$activeSheet->mergeCells('B5:'.$last_abjad.'5');
				$activeSheet->setCellValue('B5', $data['judul_header_ina'].' '.strtoupper($data['tipe_laporan']));
				$activeSheet->mergeCells('B5:'.$last_abjad.'5');
				// $activeSheet->mergeCells('B6:'.$last_abjad.'6');
				$activeSheet->getStyle('B5')->getFont()->setBold(true)->setSize(12);
				$activeSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$x = 8;
				$list_data=$data['list_data'];
				$no=1;
				$total=0;
				$jawaban_arr=$data['jawaban_arr'];
				$list_header=$data['list_header'];
				$jawaban_arr_total=$data['jawaban_arr_total'];
				$last_abjad_external="E";
				foreach($list_data as $r){
    				$x = $x+1;
					$assesmen_id=$r->assesmen_id;
					$activeSheet->setCellValue('B'.$x, $no);
					$activeSheet->setCellValue('C'.$x, $r->nopendaftaran);
					$activeSheet->setCellValue('D'.$x, $r->title.' '.$r->namapasien);
					$activeSheet->setCellValue('E'.$x, $r->nama_profile);
					$last_abjad="E";
					
					foreach($list_header as $row){
						$param_id=$row->param_id;
						  $data_survey = array_filter($jawaban_arr, function($var) use ($assesmen_id,$param_id) { 
							return ($var['assesmen_id'] == $assesmen_id && $var['param_id'] == $param_id);
						  });		
							// print_r($arr_jadwal);exit;
						  $data_survey = reset($data_survey);
						  if ($data_survey){
							   $jawaban=strtoupper($data_survey['jawaban']).' ('.$data_survey['jawaban_skor'].')';
							  $total=$total+$data_survey['jawaban_skor'];
						  }else{
							  $jawaban='';
						  }
						  
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $jawaban);
					}
					if ($tipe_laporan=='internal'){
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $total);
					}else{
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $r->nilai_per_unsur);
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $r->nrr_per_unsur);
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $r->nrr_tertimbang);
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $r->kepuasan_mas);
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $r->flag_nilai_mas.' '.$r->hasil_flag);
						$last_abjad_external=$last_abjad;
					}
					$activeSheet->getStyle("F$x:".$last_abjad."$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    				$no = $no+1;
					
				}
				if ($tipe_laporan=='internal'){
					$activeSheet->getStyle("B9:".$last_abjad."$x")->applyFromArray(
						array(
							'borders' => array(
								'allborders' => array(
										'style' => PHPExcel_Style_Border::BORDER_THIN,
								)
							)
						)
					);
				}else{
					$xxx=$x+5;
					$activeSheet->getStyle("B9:".$last_abjad."$xxx")->applyFromArray(
						array(
							'borders' => array(
								'allborders' => array(
										'style' => PHPExcel_Style_Border::BORDER_THIN,
								)
							)
						)
					);
				}
				
				if ($tipe_laporan=='internal'){
					$x++;
					$activeSheet->setCellValue("B".$x,"TOTAL");
					$activeSheet->mergeCells('B'.$x.':E'.$x);
					$last_abjad="E";
					foreach($list_header as $row){
					  $param_id=$row->param_id;
					  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
						return ($var['param_id'] == $param_id);
					  });	
						// print_r($arr_jadwal);exit;
					  $data_survey = reset($data_survey);
					  if ($data_survey){
						  $jawaban=''.strtoupper($data_survey['jawaban_skor']).'';
						  $total=$total+$data_survey['jawaban_skor'];
					  }else{
						  $jawaban='';
					  }
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $jawaban);
						
					}
					$last_abjad=increment_single_string($last_abjad);
					$activeSheet->setCellValue($last_abjad.$x, $total);
					$activeSheet->getStyle("B$x:".$last_abjad."$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				}else{
					$x++;
					$activeSheet->setCellValue("B".$x,"NILAI PER UNSUR");
					$activeSheet->mergeCells('B'.$x.':E'.$x);
					$last_abjad="E";
					 foreach($list_header as $row){
						  $param_id=$row->param_id;
						  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
							return ($var['param_id'] == $param_id);
						  });	
							// print_r($arr_jadwal);exit;
						  $data_survey = reset($data_survey);
						  if ($data_survey){
							  $jawaban=''.strtoupper($data_survey['nilai_per_unsur']).'';
						  }else{
							  $jawaban='';
						  }
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $jawaban);
					 }
					 
					 $x++;
					$activeSheet->setCellValue("B".$x,"NRR PER UNSUR");
					$activeSheet->mergeCells('B'.$x.':E'.$x);
					$last_abjad="E";
					 foreach($list_header as $row){
						  $param_id=$row->param_id;
						  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
							return ($var['param_id'] == $param_id);
						  });	
							// print_r($arr_jadwal);exit;
						  $data_survey = reset($data_survey);
						  if ($data_survey){
							  $jawaban=''.strtoupper($data_survey['nrr_per_unsur']).'';
						  }else{
							  $jawaban='';
						  }
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $jawaban);
					 }
					
					$x++;
					$activeSheet->setCellValue("B".$x,"NRR TERTIMBANG PER UNSUR");
					$activeSheet->mergeCells('B'.$x.':E'.$x);
					$last_abjad="E";
					 foreach($list_header as $row){
						  $param_id=$row->param_id;
						  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
							return ($var['param_id'] == $param_id);
						  });	
							// print_r($arr_jadwal);exit;
						  $data_survey = reset($data_survey);
						  if ($data_survey){
							  $jawaban=''.strtoupper($data_survey['nrr_tertimbang']).'';
						  }else{
							  $jawaban='';
						  }
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $jawaban);
					 }
					 
					 $x++;
					$activeSheet->setCellValue("B".$x,"SATUAN KEPUASAN MASYARAKAT");
					$activeSheet->mergeCells('B'.$x.':E'.$x);
					$last_abjad="E";
					 foreach($list_header as $row){
						  $param_id=$row->param_id;
						  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
							return ($var['param_id'] == $param_id);
						  });	
							// print_r($arr_jadwal);exit;
						  $data_survey = reset($data_survey);
						  if ($data_survey){
							  $jawaban=''.strtoupper($data_survey['satuan_kepuasan_mas']).'';
						  }else{
							  $jawaban='';
						  }
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $jawaban);
					 }

					 $x++;
					$activeSheet->setCellValue("B".$x,"HASIL SURVEY");
					$activeSheet->mergeCells('B'.$x.':E'.$x);
					$last_abjad="E";
					 foreach($list_header as $row){
						  $param_id=$row->param_id;
						  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
							return ($var['param_id'] == $param_id);
						  });	
							// print_r($arr_jadwal);exit;
						  $data_survey = reset($data_survey);
						  if ($data_survey){
							  $jawaban=''.strtoupper($data_survey['flag_nilai']).' ('.strtoupper($data_survey['ref_nilai']).')';
						  }else{
							  $jawaban='';
						  }
						$last_abjad=increment_single_string($last_abjad);
						$activeSheet->setCellValue($last_abjad.$x, $jawaban);
					 }
				}
				
				$activeSheet->getStyle("B9:".$last_abjad."$x")->applyFromArray(
					array(
						'borders' => array(
							'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
							)
						)
					)
				);
				
        // if (COUNT($data)) {
            // foreach ($data as $row) {

							// $sisaStok = $sisaStok + $row->penerimaan - $row->pengeluaran;

	    				// // Set Content
	            // $activeSheet->setCellValue("B$x", $row->tanggal);
	            // $activeSheet->setCellValue("C$x", $row->user);
	            // $activeSheet->setCellValue("D$x", $row->jenis_trx);
	            // $activeSheet->setCellValue("E$x", $row->notransaksi);
	            // $activeSheet->setCellValue("F$x", $row->tujuan);
	            // $activeSheet->setCellValue("G$x", $row->keterangan);
	            // $activeSheet->setCellValue("H$x", $row->penerimaan);
	            // $activeSheet->setCellValue("I$x", $row->pengeluaran);
	            // $activeSheet->setCellValue("J$x", $sisaStok);
	    				// $activeSheet->getStyle("B$x:C$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    				// $activeSheet->getStyle("E$x:F$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	            // $activeSheet->getStyle("H$x:J$x")->getNumberFormat()->setFormatCode('#,##0');
			    		// $activeSheet->getStyle("H$x:J$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			    		// $activeSheet->getStyle("B$x:J$x")->applyFromArray(
			        		// array(
			        			// 'borders' => array(
			        				// 'allborders' => array(
			        						// 'style' => PHPExcel_Style_Border::BORDER_THIN,
			        				// )
			        			// )
			        		// )
			    		// );
        // }
			// }

	    // $x = $x+1;

    	// // Set Info Print
    	// $activeSheet->setCellValue("B$x", 'Sistem Informasi RSKB Halmahera - Tanggal Cetak '.date("d/m/Y - h:i:s"));
    	// $activeSheet->getStyle("B$x")->getFont()->setSize(11);
    	// $activeSheet->mergeCells("B$x:J$x");
    	// $activeSheet->getStyle("B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    	// $x = $x+1;

    	// // Set Auto Width Column
    	// $activeSheet->getColumnDimension("A")->setWidth(5);
    	// $activeSheet->getColumnDimension("B")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("C")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("D")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("E")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("F")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("G")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("I")->setAutoSize(true);
		
		foreach (range('A', $last_abjad_external) as $column){
			$activeSheet->getColumnDimension($column)->setAutoSize(true);
		}  
		$judul_header_ina=$data['judul_header_ina'];
    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$judul_header_ina.'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
    }
}
