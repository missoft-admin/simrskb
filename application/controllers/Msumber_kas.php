<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msumber_kas extends CI_Controller {

	/**
	 * Master Sumber Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msumber_kas_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Sumber Kas';
		$data['content'] 		= 'Msumber_kas/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Sumber Kas",'#'),
									    			array("List",'msumber_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'jenis_kas_id'			=> '',
			'bank_id' 				=> '',
			'idakun' 				=> '',
			'noakun' 				=> '',
			'status' 				=> '',
		);

		$data['list_user'] 			= $this->Msumber_kas_model->list_user('');
		$data['list_kas'] 			= $this->Msumber_kas_model->list_kas('');
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Sumber Kas';
		$data['content'] 		= 'Msumber_kas/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Sumber Kas",'#'),
									    			array("Tambah",'msumber_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Msumber_kas_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'jenis_kas_id' 		=> $row->jenis_kas_id,
					'bank_id' 		=> $row->bank_id,
					'idakun' 		=> $row->idakun,
					'noakun' 		=> $row->noakun,
					'saldo' 		=> $row->saldo,
					'tanggal_saldo' 		=> $row->tanggal_saldo,
					'status' 				=> $row->status
				);
				// print_r($data);exit();
				$data['list_user'] 			= $this->Msumber_kas_model->list_user($row->id);
				$data['list_kas'] 			= $this->Msumber_kas_model->list_kas($row->id);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Sumber Kas';
				$data['content']	 	= 'Msumber_kas/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Sumber Kas",'#'),
											    			array("Ubah",'msumber_kas')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('msumber_kas','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('msumber_kas');
		}
	}

	function delete($id){
		$this->Msumber_kas_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('msumber_kas','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		// print_r($this->input->post());exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Msumber_kas_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msumber_kas','location');
				}
			} else {
				if($this->Msumber_kas_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msumber_kas','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Msumber_kas/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Sumber Kas';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Sumber Kas",'#'),
															array("Tambah",'msumber_kas')
													);
		}else{
			$data['title'] = 'Ubah Master Sumber Kas';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Sumber Kas",'#'),
															array("Ubah",'msumber_kas')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			$from="(SELECT MK.id,MK.nama,JK.nama as jenis_kas
					,MB.nama as bank, A.namaakun,MK.noakun
					from msumber_kas MK
					LEFT JOIN mjenis_kas JK ON JK.id=MK.jenis_kas_id
					LEFT JOIN mbank MB ON MB.id=MK.bank_id
					LEFT JOIN makun_nomor A ON A.noakun=MK.noakun
					WHERE MK.`status`='1') as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'msumber_kas/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'msumber_kas" data-urlremove="'.site_url().'msumber_kas/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->jenis_kas;
          $row[] = $r->nama;
          $row[] = $r->bank;
          $row[] = $r->noakun.' - '.$r->namaakun;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function get_noakun($id){
		
		$q="SELECT M.noakun FROM makun_nomor M WHERE M.id='$id'";
		$opsi=$this->db->query($q)->row('noakun');
		$arr=$opsi;
		$this->output->set_output(json_encode($arr));
	}

}
