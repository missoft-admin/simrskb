<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_tv extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_tv_model');
		$this->load->helper('path');
		
  }
	function index($tv_id='1'){
		redirect('antrian_tv/display/1', 'location');			
		
	}
	function display($tv_id='1'){
		$data = $this->Antrian_tv_model->get_index_setting($tv_id);
		$data['list_running'] = $this->Antrian_tv_model->list_running($tv_id);
		$data['title'] 			= 'APM Setting Index';
		$data['title_atas'] 		= $data['nama_display'];
		$data['content'] 		= 'Antrian_tv/index';
		$data['tv_id'] 		= $tv_id;
		$data['error'] 		= '';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("APM Setting",'#'),
											  array("Index Setting",'antrian_tv')
											);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_apm', $data);
	}
	 function get_video(){
		 $display_id=$this->input->post('display_id');
		$q="SELECT GROUP_CONCAT(H.file_name) as nama_file FROM antrian_display_video H

			WHERE H.display_id='$display_id' 
			ORDER BY H.id ASC";
			
		$hasil=$this->db->query($q)->row('nama_file');
		
		
		$this->output->set_output(json_encode($hasil));
  }
  
  function get_antrian(){
	  
	  $display_id=$this->input->post('display_id');
	$q="SELECT H.id,H.antrian_harian_id,H.kodeantrian,MC.nama_counter,H.st_panggil_suara,H.sound_play FROM antrian_harian_call H
			LEFT JOIN antrian_pelayanan_counter MC ON MC.id=H.counter_id
			WHERE H.tanggal=CURRENT_DATE() AND H.st_panggil_suara='0'

			ORDER BY H.id ASC
			LIMIT 1";
			//AND H.st_panggil_suara='0'
	 $hasil=$this->db->query($q)->row();
	 $data['antrian_atas']=$hasil;
	 // $q="SELECT H.kodeantrian,MC.nama_counter,MC.st_istirahat,H.waktu_dilayani FROM `antrian_harian` H
		// LEFT JOIN antrian_pelayanan_counter MC ON MC.id=H.counter_id
		// WHERE H.tanggal=CURRENT_DATE() AND H.st_panggil
		// ORDER BY H.waktu_dilayani DESC
		// LIMIT 1,6";
	$q="SELECT H.counter_id,M.st_istirahat,M.st_login,M.`status`,M.antrian_harian_id,M.nama_counter
			,CASE WHEN AH.tanggal=CURRENT_DATE() THEN AH.kodeantrian ELSE '-' END as kodeantrian

			FROM antrian_display_counter H
			LEFT JOIN antrian_pelayanan_counter M ON M.id=H.counter_id AND M.`status`='1'
			LEFT JOIN antrian_harian AH ON AH.id=M.antrian_harian_id
			WHERE H.display_id='$display_id' AND H.`status`='1'
			GROUP BY H.counter_id
			ORDER BY H.nourut ASC

			";
	 $list_antrian=$this->db->query($q)->result();
	 $tabel='';
	 
	 foreach($list_antrian as $row){
		 $kodeantrian=$row->kodeantrian;
		 if ($row->st_istirahat=='1'){
			 $kodeantrian='ISTIRAHAT';
		 }
		 if ($row->st_login=='0'){
			 $kodeantrian='-';
		 }
		 $tabel .= '<div class="col-sm-2">
					<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
						<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
							<div class="h3 font-w400 text-white" style="border-bottom: 1px solid white;padding:0">'.$row->nama_counter.'</div>
							<div class="h1 font-w700 text-white push-5-t">'.$kodeantrian.'</div>
						</div>
						
					</a>
			</div>';
		 
	 }
	 $data['list_antrian']=$tabel;
	 $this->output->set_output(json_encode($data));
  }
  function get_antrian_awal(){
	  $display_id=$this->input->post('display_id');
	$q="SELECT H.id,H.antrian_harian_id,H.kodeantrian,MC.nama_counter,H.st_panggil_suara,H.sound_play FROM antrian_harian_call H
			LEFT JOIN antrian_pelayanan_counter MC ON MC.id=H.counter_id
			WHERE H.tanggal=CURRENT_DATE() AND H.st_panggil_suara='1'

			ORDER BY H.id DESC
			LIMIT 1";
			//AND H.st_panggil_suara='0'
	 $hasil=$this->db->query($q)->row();
	 $data['antrian_atas']=$hasil;
	 // $q="SELECT H.kodeantrian,MC.nama_counter,MC.st_istirahat,H.waktu_dilayani FROM `antrian_harian` H
		// LEFT JOIN antrian_pelayanan_counter MC ON MC.id=H.counter_id
		// WHERE H.tanggal=CURRENT_DATE() AND H.st_panggil
		// ORDER BY H.waktu_dilayani DESC
		// LIMIT 1,6";

		$q="SELECT H.counter_id,M.st_istirahat,M.st_login,M.`status`,M.antrian_harian_id,M.nama_counter
			,CASE WHEN AH.tanggal=CURRENT_DATE() THEN AH.kodeantrian ELSE '-' END as kodeantrian

			FROM antrian_display_counter H
			LEFT JOIN antrian_pelayanan_counter M ON M.id=H.counter_id AND M.`status`='1'
			LEFT JOIN antrian_harian AH ON AH.id=M.antrian_harian_id
			WHERE H.display_id='$display_id' AND H.`status`='1'
			GROUP BY H.counter_id
			ORDER BY H.nourut ASC

			";
	 $list_antrian=$this->db->query($q)->result();
	 $tabel='';
	 foreach($list_antrian as $row){
		  $kodeantrian=$row->kodeantrian;
		 if ($row->st_istirahat=='1'){
			 $kodeantrian='ISTIRAHAT';
		 }
		 if ($row->st_login=='0'){
			 $kodeantrian='-';
		 }
		 $tabel .= '<div class="col-sm-2">
					<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
						<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
							<div class="h3 font-w400 text-white" style="border-bottom: 1px solid white;padding:0">'.$row->nama_counter.'</div>
							<div class="h1 font-w700 text-white push-5-t">'.$kodeantrian.'</div>
						</div>
						
					</a>
			</div>';
		 
	 }
	 $data['list_antrian']=$tabel;
	 $this->output->set_output(json_encode($data));
  }
  function call_update_panggil(){
	  $id=$this->input->post('id');
	  $data=array(
	  'st_panggil_suara'=>1,
	  );
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('antrian_harian_call',$data);
	  $this->output->set_output(json_encode($hasil));
  }
	//HAPUS
	
}
