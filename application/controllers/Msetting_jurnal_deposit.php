<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_deposit extends CI_Controller {

	/**
	 * Setting Jurnal Deposit controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_deposit_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_deposit_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_deposit_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Deposit';
		$data['content'] 		= 'Msetting_jurnal_deposit/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Deposit",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			'idakun_tunai'=>$this->input->post('idakun_tunai'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_deposit',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Hutang
	function load_pendapatan()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idtipe,H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM `msetting_jurnal_deposit_pendapatan` H
				LEFT JOIN makun_nomor A ON A.id=H.idakun

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','nama_distributor');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = metode_pembayaran($r->idtipe);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_pendapatan('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_pendapatan(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$this->input->post('idtipe'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_pendapatan($data['idtipe'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_deposit_pendapatan',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_pendapatan($idtipe){
		$q="SELECT *FROM msetting_jurnal_deposit_pendapatan S
			WHERE S.idtipe='$idtipe'";
		return $this->db->query($q)->row('id');
	}	
	function hapus_pendapatan($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_deposit_pendapatan');
		echo json_encode($result);
	}
	

}
