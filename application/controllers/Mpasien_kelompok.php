<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpasien_kelompok extends CI_Controller {

	/**
	 * Kelompok Pasien controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpasien_kelompok_model');
		$this->load->helper('path');
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('25'))){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Kelompok Pasien';
		$data['content'] 		= 'Mpasien_kelompok/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Kelompok Pasien",'#'),
									    			array("List",'mpasien_kelompok')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function update($id){

		if($id != ''){
			$row = $this->Mpasien_kelompok_model->getSpecified($id);
			$jenis_operasi = $this->Mpasien_kelompok_model->getJenisOperasi($id);
			if(isset($row->id)){
				$data = array(
					'id' 															=> $row->id,
					'nama' 														=> $row->nama,
					'tadm_rawatjalan'									=> $row->tadm_rawatjalan,
					'tkartu_rawatjalan'								=> $row->tkartu_rawatjalan,
					'tadm_rawatinap'									=> $row->tadm_rawatinap,
					'tkartu_rawatinap'								=> $row->tkartu_rawatinap,
					'toperasi_sewaalat'								=> $row->toperasi_sewaalat,
					'toperasi_tindakan'								=> $row->toperasi_tindakan,
					'toperasi_jenis'									=> $jenis_operasi,
					'tradiologi_xray'									=> $row->tradiologi_xray,
					'tradiologi_usg'									=> $row->tradiologi_usg,
					'tradiologi_ctscan'								=> $row->tradiologi_ctscan,
					'tradiologi_mri'									=> $row->tradiologi_mri,
					'tradiologi_bmd'									=> $row->tradiologi_bmd,
					'tlaboratorium_umum'							=> $row->tlaboratorium_umum,
					'tlaboratorium_pa'								=> $row->tlaboratorium_pa,
					'tlaboratorium_pmi'								=> $row->tlaboratorium_pmi,
					'tfisioterapi'										=> $row->tfisioterapi,
					'tigd'											=> $row->tigd,
					'trawatjalan'											=> $row->trawatjalan,
					'trawatinap_fullcare1'						=> $row->trawatinap_fullcare1,
					'trawatinap_ecg1'									=> $row->trawatinap_ecg1,
					'trawatinap_visite1'							=> $row->trawatinap_visite1,
					'trawatinap_visite1_spesial'							=> $row->trawatinap_visite1_spesial,
					'trawatinap_sewaalat1'						=> $row->trawatinap_sewaalat1,
					'trawatinap_ambulance1'						=> $row->trawatinap_ambulance1,
					'trawatinap_fullcare2'						=> $row->trawatinap_fullcare2,
					'trawatinap_ecg2'									=> $row->trawatinap_ecg2,
					'trawatinap_visite2'							=> $row->trawatinap_visite2,
					'trawatinap_visite2_spesial'							=> $row->trawatinap_visite2_spesial,
					'trawatinap_sewaalat2'						=> $row->trawatinap_sewaalat2,
					'trawatinap_ambulance2'						=> $row->trawatinap_ambulance2,
					'trawatinap_fullcare3'						=> $row->trawatinap_fullcare3,
					'trawatinap_ecg3'									=> $row->trawatinap_ecg3,
					'trawatinap_visite3'							=> $row->trawatinap_visite3,
					'trawatinap_visite3_spesial'							=> $row->trawatinap_visite3_spesial,
					'trawatinap_sewaalat3'						=> $row->trawatinap_sewaalat3,
					'trawatinap_ambulance3'						=> $row->trawatinap_ambulance3,
					'trawatinap_fullcare4'						=> $row->trawatinap_fullcare4,
					'trawatinap_ecg4'									=> $row->trawatinap_ecg4,
					'trawatinap_visite4'							=> $row->trawatinap_visite4,
					'trawatinap_visite4_spesial'							=> $row->trawatinap_visite4_spesial,
					'trawatinap_sewaalat4'						=> $row->trawatinap_sewaalat4,
					'trawatinap_ambulance4'						=> $row->trawatinap_ambulance4,
					'trawatinap_fullcare5'						=> $row->trawatinap_fullcare5,
					'truang_perawatan'								=> $row->truang_perawatan,
					'truang_hcu'											=> $row->truang_hcu,
					'truang_icu'											=> $row->truang_icu,
					'truang_isolasi'									=> $row->truang_isolasi,
					'status' 													=> $row->status
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Kelompok Pasien';
				$data['content']	 	= 'Mpasien_kelompok/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Kelompok Pasien",'#'),
											    			array("Ubah",'mpasien_kelompok')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpasien_kelompok','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasien_kelompok');
		}
	}
	function setting($id){

		if($id != ''){
			$row = $this->Mpasien_kelompok_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 															=> $row->id,
					'nama' 														=> $row->nama,
					'st_multiple' 														=> $row->st_multiple,
					
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Setting Jatuh Tempo';
				$data['content']	 	= 'Mpasien_kelompok/setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Setting Jatuh Tempo",'#'),
											    			array("Setting",'mpasien_kelompok')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpasien_kelompok','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasien_kelompok');
		}
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->Mpasien_kelompok_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mpasien_kelompok','location');
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpasien_kelompok/manage';

		$data['title'] = 'Ubah Kelompok Pasien';
		$data['breadcrum'] = array(
														array("RSKB Halmahera",'#'),
														array("Kelompok Pasien",'#'),
														array("Ubah",'mpasien_kelompok')
												);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$from="(
				SELECT mpasien_kelompok.id,mpasien_kelompok.nama,GROUP_CONCAT(S.tanggal_hari ORDER BY tanggal_hari ASC SEPARATOR ', ') as tanggal_tagihan FROM mpasien_kelompok 
				LEFT JOIN mpasien_kelompok_setting S ON mpasien_kelompok.id=S.idkelompokpasien AND  S.`status`='1'
				WHERE mpasien_kelompok.status='1'  
				GROUP BY mpasien_kelompok.id
				ORDER BY mpasien_kelompok.id ASC
				)tbl ";
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
				$no++;
				$row = array();

				$row[] = $no;
				$row[] = $r->nama;
				$row[] ='<span class="label label-danger">'. $r->tanggal_tagihan.'</span>';
				$aksi = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('26'))){
					$aksi .= '<a href="'.site_url().'mpasien_kelompok/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					if ($r->id !='5'){
						$aksi .= '<a href="'.site_url().'mpasien_kelompok/setting/'.$r->id.'" data-toggle="tooltip" title="Setting Jatuh Tempo" class="btn btn-success btn-sm"><i class="si si-settings"></i></a>';
					}
				}
				$aksi .= '</div>';
				$row[] = $aksi;
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	//Load_Jatuhtempo
	function Load_Jatuhtempo()
    {
		$idkelompokpasien     		= $this->input->post('idkelompokpasien');

		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *from mpasien_kelompok_setting 
				WHERE status='1' AND idkelompokpasien='$idkelompokpasien'
				ORDER BY tipe, tanggal_hari ASC
			) as tbl  ";
		
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('mpasien_kelompok/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();			
            // $row[] = $no;
			$row[] = $r->id;//0
			$row[] = $r->tipe;//1
            $row[] = ($r->tipe=='1')?'<span class="label label-success">Rawat Jalan</span>':'<span class="label label-primary">Rawat Inap</span>';//2
            $row[] = $r->tanggal_hari;//3
			$row[] = $r->batas_kirim;		//4
			$row[] = $r->jml_hari;	//5
            $row[] =$r->created_nama.'-'.HumanDateLong($r->updated_date);//6
            
			$aksi       = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-primary edit" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="btn btn-xs btn-danger hapus" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';
				
			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function save_jt()
    {
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $tanggal_hari = $this->input->post('tanggal_hari');
        $jml_hari = $this->input->post('jml_hari');
        $batas_kirim = $this->input->post('batas_kirim');
        $tipe = $this->input->post('tipe');
        
		$data =array(
            'idkelompokpasien'=>$idkelompokpasien,
            'tanggal_hari'=>$tanggal_hari,
            'jml_hari'=>$jml_hari,
            'tipe'=>$tipe,
            'batas_kirim'=>$batas_kirim,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),

           
        );
		$result=$this->db->insert('mpasien_kelompok_setting',$data);	
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function save_multiple()
    {
        $idkelompokpasien = $this->input->post('idkelompokpasien');
        $st_multiple = $this->input->post('st_multiple');
        
		$data =array(
            'st_multiple'=>$st_multiple,
            
        );
		$this->db->where('id',$idkelompokpasien);
		$result=$this->db->update('mpasien_kelompok',$data);	
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_data()
    {
        $id_jt = $this->input->post('id_jt');
        
		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
			'deleted_date'=>date('Y-m-d H:i:s'),

           
        );
		$this->db->where('id',$id_jt);;
		$result=$this->db->update('mpasien_kelompok_setting',$data);	
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function update_date()
    {
        $id_jt = $this->input->post('id_jt');
         $tipe = $this->input->post('tipe');
         $tanggal_hari = $this->input->post('tanggal_hari');
        $jml_hari = $this->input->post('jml_hari');
        $batas_kirim = $this->input->post('batas_kirim');
		$data =array(
            'tanggal_hari'=>$tanggal_hari,
            'tipe'=>$tipe,
            'jml_hari'=>$jml_hari,
            'batas_kirim'=>$batas_kirim,
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


           
        );
		$this->db->where('id',$id_jt);;
		$result=$this->db->update('mpasien_kelompok_setting',$data);	
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function cek_duplicate($idkelompokpasien,$tanggal_hari,$tipe,$id_jt=''){
		if ($id_jt==''){
			$q="SELECT COUNT(M.id)  as jml From mpasien_kelompok_setting M
				WHERE M.idkelompokpasien='$idkelompokpasien' AND M.tipe='$tipe' AND M.tanggal_hari='$tanggal_hari' AND M.`status`='1'";
		}else{
			$q="SELECT COUNT(M.id)  as jml,M.tanggal_hari From mpasien_kelompok_setting M
				WHERE M.idkelompokpasien='$idkelompokpasien' AND M.tipe='$tipe' AND M.tanggal_hari='$tanggal_hari' AND M.`status`='1' AND M.id !='$id_jt'";
		}
		
		$arr['detail'] =$this->db->query($q)->row_array();		
		// $data['jml']=$row;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($data));
		
	}
}
