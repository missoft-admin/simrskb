<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_sewaalat extends CI_Controller
{

    /**
     * Tarif Sewa Alat controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mtarif_sewaalat_model');
        $this->load->model('Mtarif_administrasi_model');
    }

    public function index()
    {
        $data = array();
        $data['error']      = '';
        $data['title']      = 'Tarif Sewa Alat';
        $data['content']    = 'Mtarif_sewaalat/index';
        $data['breadcrum']  = array(
                                            array("RSKB Halmahera",'#'),
                                            array("Tarif Sewa Alat",'mtarif_sewaalat'),
                                            array("List",'#')
                                            );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {
        $data = array(
            'id'                => '',
            'nama'              => '',
            'idkelompok'        => '1',
            'headerpath'        => '0',
            'old_headerpath'    => '',
            'path'              => '',
            'level'             => '',
            'status'            => ''
        );

        $data['error']        = '';
        $data['title']        = 'Tambah Tarif Sewa Alat';
        $data['content']      = 'Mtarif_sewaalat/manage';
        $data['breadcrum']    = array(
                                  array("RSKB Halmahera",'#'),
                                                array("Tarif Sewa Alat",'mtarif_sewaalat'),
                                    array("Tambah",'mtarif_sewaalat')
                                );

        $data['list_parent'] = $this->Mtarif_sewaalat_model->getAllParent();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id)
    {
        if ($id != '') {
            $row = $this->Mtarif_sewaalat_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id'                 => $row->id,
                    'nama'               => $row->nama,
                    'idkelompok'         => $row->idkelompok,
                    'headerpath'         => $row->headerpath,
                    'old_headerpath'     => $row->headerpath,
                    'path'               => $row->path,
                    'level'              => $row->level,
                    'status'             => $row->status
                );

                $data['error']      = '';
                $data['title']      = 'Ubah Tarif Sewa Alat';
                $data['content']    = 'Mtarif_sewaalat/manage';
                $data['breadcrum']  = array(
                  array("RSKB Halmahera",'#'),
                  array("Tarif Sewa Alat",'mtarif_sewaalat'),
                  array("Ubah",'mtarif_sewaalat')
                );

                $data['list_parent'] = $this->Mtarif_sewaalat_model->getAllParent($row->headerpath, $row->level);
                $data['list_detailtarif'] = $this->Mtarif_sewaalat_model->getDetailTarif($row->id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_sewaalat', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_sewaalat', 'location');
        }
    }

    public function delete($id)
    {
        $this->Mtarif_sewaalat_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mtarif_sewaalat', 'location');
    }

    public function save()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->Mtarif_sewaalat_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_sewaalat/'.$this->input->post('idtipe'), 'location');
                }
            } else {
                if ($this->Mtarif_sewaalat_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_sewaalat/'.$this->input->post('idtipe'), 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id)
    {
        $data = $this->input->post();
        $data['error']       = validation_errors();
        $data['content']     = 'Mtarif_sewaalat/manage';

        $data['list_parent'] = $this->Mtarif_sewaalat_model->getAllParent();

        if ($id=='') {
            $data['title'] = 'Tambah Satuan';
            $data['breadcrum'] = array(
                array("RSKB Halmahera",'#'),
                array("Tarif Sewa Alat",'mtarif_sewaalat'),
                array("Tambah",'mtarif_sewaalat')
            );
        } else {
            $data['title'] = 'Ubah Satuan';
            $data['breadcrum'] = array(
              array("RSKB Halmahera",'#'),
              array("Tarif Sewa Alat",'mtarif_sewaalat'),
              array("Ubah",'mtarif_sewaalat')
           );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function get_child_level($headerpath)
    {
        $arr = $this->Mtarif_sewaalat_model->getPathLevel($headerpath);
        $this->output->set_output(json_encode($arr));
    }

    public function getIndex()
    {
        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];

        $this->select = array();
        $this->from   = 'mtarif_operasi_sewaalat';
        $this->join 	= array();
        $this->where  = array(
          'status' => '1'
        );
        $this->order  = array(
          'path' => 'ASC'
        );
        $this->group  = array();

        if (UserAccesForm($user_acces_form, array('174'))) {
            $this->column_search   = array('nama');
        } else {
            $this->column_search   = array();
        }
        $this->column_order    = array('nama');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = TreeView($r->level, $r->nama);
            $aksi = '<div class="btn-group">';
            if (UserAccesForm($user_acces_form, array('176'))) {
                $aksi .= '<a href="'.site_url().'mtarif_sewaalat/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            }
            if (UserAccesForm($user_acces_form, array('177'))) {
                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_sewaalat" data-urlremove="'.site_url().'mtarif_sewaalat/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            }
            if ($r->idkelompok == 0) {
				$aksi .='<div class="btn-group">
							<button class="btn btn-success  btn-sm" type="button"><i class="fa fa-cog"></i></button>
							<div class="btn-group">
								<button class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="'.site_url().'mtarif_sewaalat/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" >Setting Tarif</a></li>										
									<li><a href="'.site_url().'mtarif_sewaalat/setting_diskon/'.$r->id.'" data-toggle="tooltip" title="Setting">Setting Diskon</a></li>										
								</ul>
							</div>
						</div>';
                // $aksi .= '<a href="'.site_url().'mtarif_sewaalat/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
            }
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->datatable->count_all(),
                "recordsFiltered" => $this->datatable->count_all(),
                "data" => $data
            );
        echo json_encode($output);
    }

    public function setting($id)
    {
        if ($id != '') {
            $row = $this->Mtarif_sewaalat_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' 							=> $row->id,
                    'nama' 						=> $row->nama,
                    'idkelompok' 			=> $row->idkelompok,
                    'headerpath' 			=> $row->headerpath,
                    'old_headerpath' 	=> $row->headerpath,
                    'path' 						=> $row->path,
                    'level' 					=> $row->level,
                    'status' 					=> $row->status
                );

                $data['error']      = '';
                $data['title']      = 'Setting Group Pembayaran Tarif Sewa Alat';
                $data['content']    = 'Mtarif_sewaalat/setting';
                $data['breadcrum']  = array(
                                    array("RSKB Halmahera",'#'),
                                    array("Tarif Sewa Alat",'mtarif_sewaalat/index'),
                                    array("Setting",'mtarif_sewaalat')
                            );

                $data['list_tarif'] = $this->Mtarif_sewaalat_model->getDetailTarif($row->id);
                $data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_sewaalat/index/'.$uri, 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_sewaalat/index/'.$uri, 'location');
        }
    }
	public function setting_diskon($id)
    {
        if ($id != '') {
            $row = $this->Mtarif_sewaalat_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' 							=> $row->id,
                    'nama' 						=> $row->nama,
                    'idkelompok' 			=> $row->idkelompok,
                    'headerpath' 			=> $row->headerpath,
                    'old_headerpath' 	=> $row->headerpath,
                    'path' 						=> $row->path,
                    'level' 					=> $row->level,
                    'group_diskon_all' 					=> $row->group_diskon_all,
                    'status' 					=> $row->status
                );

                $data['error']      = '';
                $data['title']      = 'Setting Group Pembayaran Diskon Tarif Sewa Alat';
                $data['content']    = 'mtarif_sewaalat/setting_diskon';
                $data['breadcrum']  = array(
                                    array("RSKB Halmahera",'#'),
                                    array("Tarif Sewa Alat",'mtarif_sewaalat/index'),
                                    array("Setting",'mtarif_sewaalat')
                            );

                $data['list_tarif'] = $this->Mtarif_sewaalat_model->getDetailTarif($row->id);
				// print_r($data['list_tarif']);exit();
                $data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_sewaalat/index/'.$uri, 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_sewaalat/index/'.$uri, 'location');
        }
    }

    public function save_setting()
    {
        if ($this->Mtarif_sewaalat_model->updateSetting()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mtarif_sewaalat', 'location');
        }
    }
	public function save_setting_diskon()
    {
        if ($this->Mtarif_sewaalat_model->updateSetting_diskon()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mtarif_sewaalat', 'location');
        }
    }
}
