<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_implan_new extends CI_Controller {

	/**
	 * Implan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_implan_new_model');
		$this->load->helper('path');
  }

	function index($idkategori='0'){
		
		$data = array();
		$data['idkategori']      = $idkategori;
		$data['error'] 			= '';
		$data['title'] 			= 'Implan';
		$data['content'] 		= 'Mdata_implan_new/index';
		$data['breadcrum'] 	= array(
									array("RSKB Halmahera",'#'),
									array("Implan",'#'),
								array("List",'mdata_implan_new')
								);
		$data['list_kategori'] = $this->Mdata_implan_new_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 					=> '',
			'kode' 				=> '',
			'idkategori' 	=> '',
			'nama' 				=> '',
			'idsatuan' 		=> '',
			'ppn'         => '',
			'hargabeli'  	=> '',
			'hargadasar'  => '',
			'catatan' 		=> '',
			'status' 			=> '',
			'idsatuanbesar' 					=> '',
			'hargasatuanbesar' 				=> '',
			'jumlahsatuanbesar' 			=> '',
			'nama_generik' 			=> '',
			'merk' 			=> '',
			'nama_industri' 			=> '',
			'formularium' 			=> '',
		);
		$data['list_data_history'] = array();
		$data['tab'] 			= '1';
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Implan';
		$data['content'] 		= 'Mdata_implan_new/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Implan",'#'),
									    			array("Tambah",'mdata_implan_new')
													);

		$data['list_kategori'] = $this->Mdata_implan_new_model->getKategori();
		$data['list_satuan'] = $this->Mdata_implan_new_model->getSatuan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mdata_implan_new_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 					=> $row->id,
					'kode' 				=> $row->kode,
					'idkategori' 	=> $row->idkategori,
					'nama' 				=> $row->nama,
					'idsatuan' 		=> $row->idsatuan,
					'ppn'         => $row->ppn,
					'hargabeli'  	=> $row->hargabeli,
					'hargadasar'  => $row->hargadasar,
					'catatan' 		=> $row->catatan,
					'status' 			=> $row->status,
					'idsatuanbesar' 					=> $row->idsatuanbesar,
					'hargasatuanbesar' 				=> $row->hargasatuanbesar,
					'jumlahsatuanbesar' 			=> $row->jumlahsatuanbesar,
					'nama_generik' 			=> $row->nama_generik,
					'merk' 			=> $row->merk,
					'nama_industri' 			=> $row->nama_industri,
					'formularium' 			=> $row->formularium,
				);
				$data['error'] 			= '';
				$data['tab'] 			= '1';
				$data['title'] 			= 'Ubah Implan';
				$data['content']	 	= 'Mdata_implan_new/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Implan",'#'),
											    			array("Ubah",'mdata_implan_new')
															);

				$data['list_kategori'] = $this->Mdata_implan_new_model->getKategori();
				$data['list_satuan'] = $this->Mdata_implan_new_model->getSatuan();
				$data['list_data_history'] = $this->Mdata_implan_new_model->list_data_history($id);
				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdata_implan_new','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_implan_new');
		}
	}

	function delete($id){
		
		$this->Mdata_implan_new_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mdata_implan_new','location');
	}

	function save(){
		$this->form_validation->set_rules('idkategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mdata_implan_new_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_implan_new','location');
				}
			} else {
				if($this->Mdata_implan_new_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_implan_new','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 					= validation_errors();
		$data['content'] 				= 'Mdata_implan_new/manage';

		$data['list_kategori'] 	= $this->Mdata_implan_new_model->getKategori();
		$data['list_satuan'] = $this->Mdata_implan_new_model->getSatuan();

		if($id==''){
			$data['title'] = 'Tambah Implan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Implan",'#'),
															array("Tambah",'mdata_implan_new')
													);
		}else{
			$data['title'] = 'Ubah Implan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Implan",'#'),
															array("Ubah",'mdata_implan_new')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function filter(){
		// if($this->input->post('idkategori') != '0'){
			// $idkategori = $this->input->post('idkategori');
			// redirect("mdata_implan_new/index/$idkategori",'location');
		// }else{
			// redirect('mdata_implan_new/index/0','location');
		// }
		$data = array();
		$data['idkategori']      = $this->input->post('idkategori');
		$data['error'] 			= '';
		$data['title'] 			= 'Implan';
		$data['content'] 		= 'Mdata_implan_new/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Implan",'#'),
									    			array("List",'mdata_implan_new')
													);
		$data['list_kategori'] = $this->Mdata_implan_new_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	// function getIndex($idkategori='0')
	// {
			// $data_user=get_acces();
			// $user_acces_form=$data_user['user_acces_form'];
			
			// $idkategori=$this->input->post('idkategori');
			// $nama=$this->input->post('nama');
			// $status=$this->input->post('status');
			// $row=$this->Mdata_implan_new_model->get_array_kategori($idkategori);
		
			
			// $this->select = array('mdata_implan.*', 'mdata_kategori.nama AS namakategori');
			// $this->from   = 'mdata_implan';
			// $this->join 	= array(
				// array("mdata_kategori", "mdata_kategori.id = mdata_implan.idkategori", "")
			// );
			// if ($status!='#'){
				// $this->where  = array(
					// 'mdata_implan.status' => $status
				// );
			// }
			
			// $this->where_in = array(
					// 'mdata_implan.idkategori' => $row
				// );
			// $this->order  = array(
				// 'mdata_implan.kode' => 'DESC'
			// );
			// $this->group  = array();

			// $this->column_search   = array('mdata_implan.kode','mdata_kategori.nama','mdata_implan.nama');
			// $this->column_order    = array('mdata_implan.kode','mdata_kategori.nama','mdata_implan.nama');

			// $list = $this->datatable->get_datatables();
			// $data = array();
			// $no = $_POST['start'];
			// foreach ($list as $r) {
					// $no++;
					// $row = array();

					// $row[] = $no;
					// $row[] = $r->kode;
					// $row[] = $r->nama;
					// $row[] = $r->namakategori;
					// $row[] = number_format($r->hargasatuanbesar,0);
					// $row[] = number_format($r->hargadasar,0);
					// $row[] = StatusBarang($r->status);
					// $aksi = '<div class="btn-group">';
					// if ($r->status=='1'){
						// if (UserAccesForm($user_acces_form,array('102','105'))){
							// $aksi .= '<a href="'.site_url().'mdata_implan_new/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						// }
						 // if (UserAccesForm($user_acces_form,array('106'))){
							// $aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_implan_new" data-urlremove="'.site_url().'mdata_implan_new/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						// }
					// }else{
						// $aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_obat" data-urlremove="'.site_url().'mdata_implan_new/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					// }
		            // $aksi .= '</div>';
					// $row[] = $aksi;

					// $data[] = $row;
			// }
			// $output = array(
				// "draw" => $_POST['draw'],
				// "recordsTotal" => $this->datatable->count_all(),
				// "recordsFiltered" => $this->datatable->count_all(),
				// "data" => $data
			// );
			// echo json_encode($output);
	// }
	function getIndex()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$status=$this->input->post('status');
		$idkategori=$this->input->post('idkategori');
		$nama=$this->input->post('nama');
		$where='';
		if ($nama!=''){
			$where .=" AND H.nama LIKE '%".ReplaceKutip($nama)."%'";
		}
		if ($idkategori!='0'){
			$row=$this->Mdata_implan_new_model->get_array_kategori($idkategori);
			$row=implode(", ", $row);
			// print_r($row);exit;
			if ($row){
				
			$where .=' AND H.idkategori IN ('.$row.')';
			}
		}
		
		if ($status!='#'){
			
			$where .=' AND H.status='.$status;
		}
		
		$from="(
			SELECT K.nama as namakategori,H.* FROM mdata_implan H
				INNER JOIN mdata_kategori K ON K.id=H.idkategori
				WHERE H.id IS NOT NULL ".$where."
				ORDER BY H.kode ASC
			) tbl
		";
			$this->select = array();
			$this->from   =  $from;
			$this->join 	= array();
			$this->wher = array();
			
			
			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('kode','nama','namakategori');
			$this->column_order    = array();
			
			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->kode;
					$row[] = $r->nama;
					$row[] = $r->namakategori;
					$row[] = number_format($r->hargasatuanbesar,0);
					$row[] = number_format($r->hargadasar,0);
					$row[] = StatusBarang($r->status);
					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
						if (UserAccesForm($user_acces_form,array('113','114','115','116'))){
							$aksi .= '<a href="'.site_url().'mdata_implan_new/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						}
						if (UserAccesForm($user_acces_form,array('117'))){
							$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_implan_new" data-urlremove="'.site_url().'mdata_implan_new/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						}
					}else{
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_implan_new" data-urlremove="'.site_url().'mdata_implan_new/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
		            $aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function aktifkan($id){
		
		$this->Mdata_implan_new_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mdata_implan_new','location');
	}
	function load_pembelian(){
		$id=$this->input->post('id');
		$nopemesanan=$this->input->post('nopemesanan');
		$nopenerimaan=$this->input->post('nopenerimaan');
		$tanggalpenerimaan_1=$this->input->post('tanggalpenerimaan_1');
		$tanggalpenerimaan_2=$this->input->post('tanggalpenerimaan_2');
		$iddis=$this->input->post('iddis');
		$nofakturexternal=$this->input->post('nofakturexternal');
		$nobatch=$this->input->post('nobatch');
		$tanggalkadaluarsa_1=$this->input->post('tanggalkadaluarsa_1');
		$tanggalkadaluarsa_2=$this->input->post('tanggalkadaluarsa_2');
		$where='';
		if ($nopemesanan!=''){
			$where .=" AND P.nopemesanan LIKE '%".$nopemesanan."%'";
		}
		if ($nopenerimaan!=''){
			$where .=" AND H.nopenerimaan LIKE '%".$nopenerimaan."%'";
		}
		
		if ($iddis) {
			$iddis=implode(", ", $iddis);
            $where .= " AND H.iddistributor IN (".$iddis.")";
        }
		if ($nofakturexternal) {
            $where .=" AND H.nofakturexternal LIKE '%".$nofakturexternal."%'";
        }
		if ($nobatch) {
            $where .=" AND D.nobatch LIKE '%".$nobatch."%'";
        }
		if ($tanggalkadaluarsa_1 !='') {
            $where .= " AND DATE(D.tanggalkadaluarsa) >='".YMDFormat($tanggalkadaluarsa_1)."' AND DATE(D.tanggalkadaluarsa) <='".YMDFormat($tanggalkadaluarsa_2)."'";
        }
		if ($tanggalpenerimaan_1 !='') {
            $where .= " AND DATE(H.tanggalpenerimaan) >='".YMDFormat($tanggalpenerimaan_1)."' AND DATE(H.tanggalpenerimaan) <='".YMDFormat($tanggalpenerimaan_2)."'";
        }
		$q="
			SELECT P.nopemesanan,P.tanggal,H.nopenerimaan,H.nofakturexternal,D.totalharga,H.tanggalpenerimaan,MD.nama as nama_distributor,D.harga_after_diskon,D.nominaldiskon,D.nominalppn,D.harga_after_ppn 
			,D.kuantitas,D.namasatuan,D.nobatch,D.tanggalkadaluarsa
			,H.iddistributor
			FROM tgudang_penerimaan H
			INNER JOIN tgudang_penerimaan_detail D ON D.idpenerimaan=H.id
			INNER JOIN tgudang_pemesanan P ON P.id=H.idpemesanan
			INNER JOIN mdistributor MD ON MD.id=H.iddistributor
			WHERE D.idbarang='$id' AND D.idtipe='2' ".$where."
			ORDER BY H.tanggalpenerimaan ASC
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_pembelian='0';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left">'.$no.'</td>';
			$tabel .='<td class="text-center">'.($r->nopemesanan).'<br>'.HumanDatelong($r->tanggal).'</td>';
			$tabel .='<td class="text-center">'.($r->nopenerimaan).'<br>'.HumanDatelong($r->tanggalpenerimaan).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_distributor).'<br>Faktur : '.($r->nofakturexternal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga_after_diskon,0).'<br>Dic :'.number_format($r->nominaldiskon,0).'<br>PPN : '.number_format($r->nominalppn,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga_after_ppn,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->namasatuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalharga,0).'</td>';
			$tabel .='<td class="text-right">'.($r->nobatch).'</td>';
			$tabel .='<td class="text-right">'.HumanDateShort($r->tanggalkadaluarsa).'</td>';
			
			$tabel .='</tr>';
			$total_pembelian=$total_pembelian+$r->totalharga;
			
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_pembelian']=number_format($total_pembelian,0);
		  $this->output->set_output(json_encode($arr));
	}
	function load_history_harga(){
		$id=$this->input->post('id');
		$tanggalupdate_1=$this->input->post('tanggalupdate_1');
		$tanggalupdate_2=$this->input->post('tanggalupdate_2');
		$where='';
		
		if ($tanggalupdate_1 !='') {
            $where .= " AND DATE(H.edited_date) >='".YMDFormat($tanggalupdate_1)."' AND DATE(H.edited_date) <='".YMDFormat($tanggalupdate_2)."'";
        }
		$q="
			SELECT SB.nama as satuan_besar,SK.nama as satuan_kecil,M.`name` as nama_user
				,H.* 
				FROM `mdata_barang_harga` H
				LEFT JOIN msatuan SB ON SB.id=H.idsatuanbesar
				LEFT JOIN msatuan SK ON SK.id=H.idsatuankecil
				LEFT JOIN musers M ON M.id=H.edited_by
			WHERE H.idbarang='$id' AND H.idtipe='2' ".$where."
			ORDER BY H.id 
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.($r->st_pembelian=='1'?text_warning('PEMBELIAN'):text_primary('MANUAL')).'</td>';
			$tabel .='<td class="text-center">'.($r->satuan_besar).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->hargasatuanbesar,0).'</td>';
			$tabel .='<td class="text-center">'.number_format($r->jumlahsatuanbesar,0).' '.$r->satuan_kecil.'</td>';
			$tabel .='<td class="text-center">'.($r->satuan_kecil).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->hargadasar,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->ppn,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->hargabeli,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.($r->edited_date?HumanDatelong($r->edited_date):'').'</td>';
			
			$tabel .='</tr>';
			
		  }
			
		  $arr['tabel']=$tabel;
		  $this->output->set_output(json_encode($arr));
	}
}
