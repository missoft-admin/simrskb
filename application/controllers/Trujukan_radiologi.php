<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Trujukan_radiologi extends CI_Controller
{
	/**
	 * Rujukan Radiologi controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->model('Trujukan_radiologi_model');
	}

	public function index()
	{
		$data = [
			'nomedrec' => '',
			'namapasien' => '',
			'idasalpasien' => '',
			'iddokterperujuk' => '',
			'iddokterradiologi' => '',
			'idtindakan' => '',
			'tanggaldari' => date('d/m/Y', strtotime('-1 day')),
			'tanggalsampai' => date('d/m/Y'),
		];

		$data['error'] = '';
		$data['title'] = 'Radiologi';
		$data['content'] = 'Trujukan_radiologi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan', '#'],
			['Radiologi', '#'],
			['List', 'trujukan_radiologi']
		];

		$data['list_tindakan'] = $this->Trujukan_radiologi_model->getListTarifRadiologi();
		$data['list_dokterperujuk'] = $this->Trujukan_radiologi_model->getListDokterPerujuk();
		$data['list_dokterradiologi'] = $this->Trujukan_radiologi_model->getListDokterRadiologi();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idasalpasien' => $this->input->post('idasalpasien'),
			'tanggaldari' => $this->input->post('tanggaldari'),
			'tanggalsampai' => $this->input->post('tanggalsampai'),
			'iddokterperujuk' => $this->input->post('iddokterperujuk'),
			'iddokterradiologi' => $this->input->post('iddokterradiologi'),
			'idtindakan' => $this->input->post('idtindakan'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Radiologi';
		$data['content'] = 'Trujukan_radiologi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan', '#'],
			['Radiologi', '#'],
			['List', 'trujukan_radiologi']
		];

		$data['list_tindakan'] = $this->Trujukan_radiologi_model->getListTarifRadiologi();
		$data['list_dokterperujuk'] = $this->Trujukan_radiologi_model->getListDokterPerujuk();
		$data['list_dokterradiologi'] = $this->Trujukan_radiologi_model->getListDokterRadiologi();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_radiologi_model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
					'id' => $id,
					'idpendaftaran' => $row->idpendaftaran,
					'tanggalrujukan' => date('d/m/Y'),
					'wakturujukan' => date('H:i:s'),
					'norujukan' => $row->norujukan,
					'nomedrec' => $row->nomedrec,
					'titlepasien' => $row->title,
					'namapasien' => $row->namapasien,
					'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
					'alamat' => $row->alamat,
					'provinsi' => $row->prov,
					'kota' => $row->kota,
					'kecamatan' => $row->kec,
					'kelurahan' => $row->kel,
					'tanggallahir' => $row->tanggallahir,
					'umur_tahun' => $row->umur_tahun,
					'umur_bulan' => $row->umur_bulan,
					'umur_hari' => $row->umur_hari,
					'namarekanan' => $row->namarekanan,
					'iddokterperujuk' => $row->iddokterperujuk,
					'namadokterperujuk' => $row->namadokterperujuk,
					'iddokterradiologi' => $row->iddokterradiologi,
					'namadokterradiologi' => $row->namadokterradiologi,
					'idkelompokpasien' => $row->idkelompokpasien,
					'namakelompok' => $row->namakelompok,
					'idrekanan' => $row->idrekanan,
					'asalrujukan' => $row->asalrujukan,
					'idtipe' => $row->idtipe,
					'poliklinik' => $row->namapoliklinik,
					'idkelas' => $row->idkelas,
					'kelas' => $row->namakelas,
					'ruangan' => $row->namaruangan,
					'idtarif_bpjstk' => $row->idtarif_bpjstk,
					'tarif_bpjstk' => $row->tarif_bpjstk,
					'kodebpjskesehatan' => $row->kodebpjskesehatan,
					'tarif_bpjs_kesehatan' => $row->tarif_bpjs_kesehatan,
					'nomorfoto' => 'AUTO_GENERATE',
					'tanggalpengambilan' => date('Y-m-d'),
					'jumlahexpose' => $row->jumlahexpose,
					'jumlahfilm' => $row->jumlahfilm,
					'qp' => $row->qp,
					'mas' => $row->mas,
					'idpemeriksa' => $row->idpemeriksa,
					'tindakan' => $row->tindakan,

					'tanggal_input_pemeriksaan' => $row->tanggal_input_pemeriksaan,
					'iduser_input_pemeriksaan' => $row->iduser_input_pemeriksaan,
					'statuskasir' => $row->statuskasir,
					'statuspasien' => '',
				];

				$data['error'] = '';
				$data['title'] = 'Radiologi';
				$data['content'] = 'Trujukan_radiologi/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Rujukan', '#'],
					['Radiologi', '#'],
					['Pendaftaran', 'trujukan_radiologi']
				];

				$data['list_dokterperujuk'] = $this->Trujukan_radiologi_model->getListDokterPerujuk();
				$data['list_dokterradiologi'] = $this->Trujukan_radiologi_model->getListDokterRadiologi();

				$data['list_pemeriksa'] = $this->Trujukan_radiologi_model->getListPemeriksaRadiologi();

				$data['list_film'] = $this->Trujukan_radiologi_model->getListFilmRadiologi();
				$data['list_expose'] = $this->Trujukan_radiologi_model->getListExposeRadiologi();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('trujukan_radiologi/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_radiologi/index');
		}
	}

	public function edit($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_radiologi_model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
					'id' => $id,
					'idpendaftaran' => $row->idpendaftaran,
					'tanggalrujukan' => DMYFormat($row->tanggalrujukan),
					'wakturujukan' => HISTimeFormat($row->tanggalrujukan),
					'norujukan' => $row->norujukan,
					'nomedrec' => $row->nomedrec,
					'titlepasien' => $row->title,
					'namapasien' => $row->namapasien,
					'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
					'alamat' => $row->alamat,
					'provinsi' => $row->prov,
					'kota' => $row->kota,
					'kecamatan' => $row->kec,
					'kelurahan' => $row->kel,
					'tanggallahir' => $row->tanggallahir,
					'umur_tahun' => $row->umur_tahun,
					'umur_bulan' => $row->umur_bulan,
					'umur_hari' => $row->umur_hari,
					'namarekanan' => $row->namarekanan,
					'iddokterperujuk' => $row->iddokterperujuk,
					'namadokterperujuk' => $row->namadokterperujuk,
					'iddokterradiologi' => $row->iddokterradiologi,
					'namadokterradiologi' => $row->namadokterradiologi,
					'idkelompokpasien' => $row->idkelompokpasien,
					'namakelompok' => $row->namakelompok,
					'idrekanan' => $row->idrekanan,
					'asalrujukan' => $row->asalrujukan,
					'idtipe' => $row->idtipe,
					'poliklinik' => $row->namapoliklinik,
					'idkelas' => $row->idkelas,
					'kelas' => $row->namakelas,
					'ruangan' => $row->namaruangan,
					'idtarif_bpjstk' => $row->idtarif_bpjstk,
					'tarif_bpjstk' => $row->tarif_bpjstk,
					'kodebpjskesehatan' => $row->kodebpjskesehatan,
					'tarif_bpjs_kesehatan' => $row->tarif_bpjs_kesehatan,
					'nomorfoto' => $row->nomorfoto,
					'tanggalpengambilan' => $row->tanggalpengambilan,
					'jumlahexpose' => $row->jumlahexpose,
					'jumlahfilm' => $row->jumlahfilm,
					'qp' => $row->qp,
					'mas' => $row->mas,
					'idpemeriksa' => $row->idpemeriksa,
					'tindakan' => $row->tindakan,

					'tanggal_input_pemeriksaan' => $row->tanggal_input_pemeriksaan,
					'iduser_input_pemeriksaan' => $row->iduser_input_pemeriksaan,
					'statuskasir' => $row->statuskasir,
					'statuspasien' => $row->statuspasien,
				];

				$data['error'] = '';
				$data['title'] = 'Radiologi';
				$data['content'] = 'Trujukan_radiologi/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Rujukan', '#'],
					['Radiologi', '#'],
					['Ubah', 'trujukan_radiologi']
				];

				$data['list_dokterperujuk'] = $this->Trujukan_radiologi_model->getListDokterPerujuk();
				$data['list_dokterradiologi'] = $this->Trujukan_radiologi_model->getListDokterRadiologi();

				$data['list_pemeriksa'] = $this->Trujukan_radiologi_model->getListPemeriksaRadiologi();

				$data['list_film'] = $this->Trujukan_radiologi_model->getListFilmRadiologi();
				$data['list_expose'] = $this->Trujukan_radiologi_model->getListExposeRadiologi();

				$data['list_detail'] = $this->Trujukan_radiologi_model->getListDetail($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('trujukan_radiologi/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_radiologi/index');
		}
	}

	public function review($id, $statuspasien = 0)
	{
		if ($id != '') {
			$row = $this->Trujukan_radiologi_model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
					'id' => $id,
					'tanggalexpertise' => $row->tanggalexpertise ? DMYFormat($row->tanggalexpertise) : date('Y-m-d'),
					'waktuexpertise' => $row->tanggalexpertise ? HISTimeFormat($row->tanggalexpertise) : date('H:i:s'),
					'idpoliklinik' => $row->idpoliklinik,
					'tanggalrujukan' => $row->tanggalrujukan,
					'norujukan' => $row->norujukan,
					'nomedrec' => $row->nomedrec,
					'titlepasien' => $row->title,
					'namapasien' => $row->namapasien,
					'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
					'alamat' => $row->alamat,
					'provinsi' => $row->prov,
					'kota' => $row->kota,
					'kecamatan' => $row->kec,
					'kelurahan' => $row->kel,
					'tanggallahir' => $row->tanggallahir,
					'umur_tahun' => $row->umur_tahun,
					'umur_bulan' => $row->umur_bulan,
					'umur_hari' => $row->umur_hari,
					'namarekanan' => $row->namarekanan,
					'iddokterperujuk' => $row->iddokterperujuk,
					'namadokterperujuk' => $row->namadokterperujuk,
					'iddokterradiologi' => $row->iddokterradiologi,
					'namadokterradiologi' => $row->namadokterradiologi,
					'idkelompokpasien' => $row->idkelompokpasien,
					'namakelompok' => $row->namakelompok,
					'idrekanan' => $row->idrekanan,
					'asalrujukan' => $row->asalrujukan,
					'idtipe' => $row->idtipe,
					'poliklinik' => $row->namapoliklinik,
					'idkelas' => $row->idkelas,
					'kelas' => $row->namakelas,
					'ruangan' => $row->namaruangan,
					'idtarif_bpjstk' => $row->idtarif_bpjstk,
					'tarif_bpjstk' => $row->tarif_bpjstk,
					'kodebpjskesehatan' => $row->kodebpjskesehatan,
					'tarif_bpjs_kesehatan' => $row->tarif_bpjs_kesehatan,
					'nomorfoto' => $row->nomorfoto,
					'tanggalpengambilan' => $row->tanggalpengambilan,
					'tindakan' => $row->tindakan
				];

				$data['error'] = '';
				$data['title'] = 'Radiologi';
				$data['content'] = 'Trujukan_radiologi/review';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Rujukan', '#'],
					['Radiologi', '#'],
					['Review', 'trujukan_radiologi']
				];

				$data['list_dokterperujuk'] = $this->Trujukan_radiologi_model->getListDokterPerujuk();
				$data['list_dokterradiologi'] = $this->Trujukan_radiologi_model->getListDokterRadiologi();
				$data['list_tindakan'] = $this->Trujukan_radiologi_model->getListDetailGroup($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('trujukan_radiologi/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_radiologi/index');
		}
	}

	// function view($id)
	// {
	//     $data = array();
	//     $data['error']     = '';
	//     $data['title']     = 'Radiologi';
	//     $data['content']   = 'Trujukan_radiologi/view';
	//     $data['breadcrum'] = array(
	//                             array("RSKB Halmahera", '#'),
	//                             array("Rujukan", '#'),
	//                             array("Radiologi", '#'),
	//                             array("View", 'trujukan_radiologi')
	//                          );
	//
	//     $data['list_tindakan'] = $this->Trujukan_radiologi_model->getListDetail($id);
	//
	//     $data = array_merge($data, backend_info());
	//     $this->parser->parse('module_template', $data);
	// }

	public function print_transaksi($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_radiologi_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => $row->tanggalrujukan,
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterradiologi' => $row->iddokterradiologi,
				'namadokterradiologi' => $row->namadokterradiologi,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'asalrujukan' => $row->asalrujukan,
				'idkelas' => $row->idkelas,
				'nomorfoto' => $row->nomorfoto,
				'tanggalpengambilan' => $row->tanggalpengambilan
			];

			$data['error'] = '';
			$data['title'] = 'Pemeriksaan Radiologi';

			$data['detail_rujukan'] = $this->Trujukan_radiologi_model->getListDetail($id);
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_radiologi/print/pemeriksaan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_radiologi/index');
		}
	}

	public function print_review($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_radiologi_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => $row->tanggalrujukan,
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterradiologi' => $row->iddokterradiologi,
				'namadokterradiologi' => $row->namadokterradiologi,
				'nipdokterradiologi' => $row->nipdokterradiologi,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'asalrujukan' => $row->asalrujukan,
				'idkelas' => $row->idkelas,
				'nomorfoto' => $row->nomorfoto,
				'tanggalpengambilan' => $row->tanggalpengambilan,
				'tanggal_input_pemeriksaan' => DMYFormat($row->tanggal_input_pemeriksaan)
			];

			$data['error'] = '';
			$data['title'] = 'Hasil Pemeriksaan Radiologi';

			$data['detail_rujukan'] = $this->Trujukan_radiologi_model->getListDetailGroup($id);
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_radiologi/print/hasil_pemeriksaan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_radiologi/index');
		}
	}

	public function save()
	{
		$data = [];
		$data['idrujukan'] = $_POST['id'];
		if ($this->Trujukan_radiologi_model->save()) {
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_radiologi/redirect/save_tindakan', $data);
		}
	}

	public function save_review()
	{
		$data = [];
		$data['idrujukan'] = $_POST['id'];
		if ($this->Trujukan_radiologi_model->saveReview()) {
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_radiologi/redirect/save_review', $data);
		}
	}

	public function after_save()
	{
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah disimpan.');
		redirect('trujukan_radiologi/index', 'location');
	}

	public function delete($id)
	{
		if ($this->Trujukan_radiologi_model->cancelTrx($id)) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah disimpan.');
			redirect('trujukan_radiologi/index', 'location');
		}
	}

	public function reject($id, $statuspasien, $idkelas)
	{
		if ($this->Trujukan_radiologi_model->rejectTrx($id, $statuspasien, $idkelas)) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah disimpan.');
			redirect('trujukan_radiologi/index', 'location');
		}
	}

	public function print_bukti_pemeriksaan($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_radiologi_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => $row->tanggalrujukan,
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterradiologi' => $row->iddokterradiologi,
				'namadokterradiologi' => $row->namadokterradiologi,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'asalrujukan' => $row->asalrujukan
			];

			$data['error'] = '';
			$data['title'] = 'Pemeriksaan Radiologi';

			$data['list_tindakan'] = $this->Trujukan_radiologi_model->getListDetail($id);
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_radiologi/print/bukti_pemeriksaan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_radiologi/index');
		}
	}

	public function print_bukti_pemeriksaan_thermal($id)
	{
		// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$data = [];
		$data['listTindakan'] = $this->Trujukan_radiologi_model->getListDetail($id);

		$row = $this->Trujukan_radiologi_model->getSpecified($id);
		if ($row) {
			$data['norujukan'] = $row->norujukan;
			$data['tanggal'] = $row->tanggalrujukan;
			$data['nomedrec'] = $row->nomedrec;
			$data['namapasien'] = $row->namapasien;
			$data['tanggallahir'] = $row->tanggallahir;
			$data['umur'] = $row->umur_tahun;
			$data['umurbulan'] = $row->umur_bulan;
			$data['umurhari'] = $row->umur_hari;
			$data['asalrujukan'] = GetAsalRujukan($row->asalrujukan);
			$data['dokterperujuk'] = $row->namadokterperujuk;
			$data['kelompokpasien'] = $row->namakelompok;
			$data['tarifaktif'] = '-';
			$data['userinput'] = 'Nama User Input';
		}

		$html = $this->parser->parse('Trujukan_radiologi/print/bukti_pemeriksaan_thermal', array_merge($data, backend_info()), true);
		$html = $this->parser->parse_string($html, $data);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$customPaperThermal = [0, 0, 226, 4000];
		$dompdf->set_paper($customPaperThermal);

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Bukti Pemeriksaan Radiologi.pdf', ['Attachment' => 0]);
	}

	public function print_bukti_pengambilan($id)
	{
		if ($id != '') {
			$data = [];
			$data['listTindakan'] = $this->Trujukan_radiologi_model->getListDetail($id);

			$row = $this->Trujukan_radiologi_model->getSpecified($id);
			if ($row) {
				$data['norujukan'] = $row->norujukan;
				$data['tanggal'] = $row->tanggalrujukan;
				$data['nomedrec'] = $row->nomedrec;
				$data['namapasien'] = $row->namapasien;
				$data['tanggallahir'] = $row->tanggallahir;
				$data['umur'] = $row->umur_tahun;
				$data['umurbulan'] = $row->umur_bulan;
				$data['umurhari'] = $row->umur_hari;
				$data['asalrujukan'] = GetAsalRujukan($row->asalrujukan);
				$data['dokterperujuk'] = $row->namadokterperujuk;
				$data['nomorfoto'] = $row->nomorfoto;
				$data['tanggalpengambilan'] = $row->tanggalpengambilan;
				$data['kelompokpasien'] = $row->namakelompok;
				$data['tarifaktif'] = '-';
				$data['userinput'] = $row->namauser;
			}

			$data['error'] = '';
			$data['title'] = 'Bukti Pengambilan Radiologi';

			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_radiologi/print/bukti_pengambilan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_radiologi/index');
		}
	}

	public function print_bukti_pengambilan_thermal($id)
	{
		// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$data = [];
		$data['listTindakan'] = $this->Trujukan_radiologi_model->getListDetail($id);

		$row = $this->Trujukan_radiologi_model->getSpecified($id);
		if ($row) {
			$data['norujukan'] = $row->norujukan;
			$data['tanggal'] = $row->tanggalrujukan;
			$data['nomedrec'] = $row->nomedrec;
			$data['namapasien'] = $row->namapasien;
			$data['tanggallahir'] = $row->tanggallahir;
			$data['umur'] = $row->umur_tahun;
			$data['umurbulan'] = $row->umur_bulan;
			$data['umurhari'] = $row->umur_hari;
			$data['asalrujukan'] = GetAsalRujukan($row->asalrujukan);
			$data['dokterperujuk'] = $row->namadokterperujuk;
			$data['nomorfoto'] = $row->nomorfoto;
			$data['tanggalpengambilan'] = $row->tanggalpengambilan;
			$data['kelompokpasien'] = $row->namakelompok;
			$data['tarifaktif'] = '-';
			$data['userinput'] = $row->namauser;
		}

		$html = $this->parser->parse('Trujukan_radiologi/print/bukti_pengambilan_thermal', array_merge($data, backend_info()), true);
		$html = $this->parser->parse_string($html, $data);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$customPaperThermal = [0, 0, 226, 4000];
		$dompdf->set_paper($customPaperThermal);

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Bukti Pengambilan radiologi.pdf', ['Attachment' => 0]);
	}

	public function manage_print_label($id)
	{
		$data = [];
		$data['error'] = '';
		$data['start_awal'] = 1;
		$data['idrujukan'] = $id;
		$data['tab'] = 0;
		$data['title'] = 'Print Label Radiologi';
		$data['content'] = 'Trujukan_radiologi/manage_print';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan', '#'],
			['Radiologi', '#'],
			['Manage Print', 'trujukan_radiologi']
		];

		$data['list_tindakan'] = $this->Trujukan_radiologi_model->getListGroupLabel($id);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function print_label()
	{
		$data = [];

		$idrujukan = $this->input->post('idrujukan');
		$chck_tindakan = $this->input->post('chck_tindakan');
		$idtindakan = $this->input->post('idtindakan');

		$i = 0;
		$where_tindakan = [];
		if ($chck_tindakan) {
			foreach ($chck_tindakan as $index => $value) {
				if ($value == 1) {
					$where_tindakan[$i] = $idtindakan[$index];
					$i = $i + 1;
				}
			}
		}

		// $tindakanNull = array(
		//   'norujukan' => '',
		//   'tanggal' => '',
		//   'nomedrec' => '',
		//   'namapasien' => '',
		//   'tanggallahir' => '',
		//   'umur' => '',
		//   'umurbulan' => '',
		//   'umurhari' => '',
		//   'asalrujukan' => '',
		//   'dokterperujuk' => '',
		//   'nomorfoto' => '',
		//   'tanggalpengambilan' => '',
		//   'kelompokpasien' => '',
		//   'tarifaktif' => '',
		//   'userinput' => ''
		// );

		$tindakanNull = new stdClass();
		$tindakanNull->norujukan = '';
		$tindakanNull->tanggal = '';
		$tindakanNull->nomedrec = '';
		$tindakanNull->namapasien = '';
		$tindakanNull->tanggallahir = '';
		$tindakanNull->umur = '';
		$tindakanNull->umurbulan = '';
		$tindakanNull->umurhari = '';
		$tindakanNull->asalrujukan = '';
		$tindakanNull->dokterperujuk = '';
		$tindakanNull->nomorfoto = '';
		$tindakanNull->tanggalpengambilan = '';
		$tindakanNull->kelompokpasien = '';
		$tindakanNull->tarifaktif = '';
		$tindakanNull->userinput = '';

		$data['start_awal'] = $this->input->post('start_awal');
		$data['start_akhir'] = $this->input->post('start_awal') + $this->input->post('counter') - 1;

		$dataNull = [];
		for ($i = $data['start_awal']; $i > 1; $i--) {
			$dataNull[] = $tindakanNull;
		}

		if ($where_tindakan) {
			$data['list_tindakan'] = $this->Trujukan_radiologi_model->getListGroupLabelSelected($idrujukan, $where_tindakan);
		} else {
			$data['list_tindakan'] = [];
		}
		// print_r($data['list_tindakan']);exit();

		$row = $this->Trujukan_radiologi_model->getSpecified($idrujukan);
		$data['header'] = [
			'norujukan' => $row->norujukan,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'jeniskelamin' => $row->jeniskelamin,
			'tanggallahir' => "$row->tanggallahir / $row->umur_tahun Th $row->umur_bulan Bln",
			'tanggalpemeriksaan' => $row->tanggalrujukan,
			'nomorfoto' => $row->nomorfoto,
			'jenispemeriksaan' => 'X-RAY',
			'pemeriksaan' => 'ANKLE',
			'posisi' => 'AP',
			'asalpasien' => GetAsalRujukan($row->asalrujukan),
			'dokterperujuk' => $row->namadokterperujuk,
			'petugas' => $row->namapetugas
		];
		$data['label'] = array_merge($dataNull, $data['list_tindakan']);

		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$html = $this->parser->parse('Trujukan_radiologi/print/label', array_merge($data, backend_info()), true);
		$html = $this->parser->parse_string($html, $data);

		$dompdf->loadHtml($html);
		// (Optional) Setup the paper size and orientation

		// $customPaper = array(0,0,430,600);
		// $dompdf->set_paper($customPaper, 'portrait');
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Faktur.pdf', ['Attachment' => 0]);
	}

	public function getIndex($uri)
	{
		$id_user = $this->session->userdata('user_id');
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];

		$where = '';
		if (UserAccesForm($user_acces_form, ['1226'])) {
			$where2 = 'WHERE tmp.tmp_dokter IS NOT NULL ';
		} else {
			$where2 = "WHERE tmp.tmp_dokter IS NOT NULL AND tmp.tmp_dokter !='-'";
		}
		if ($uri == 'filter') {
			if ($this->session->userdata('nomedrec') != null) {
				// $this->where = array_merge($this->where, array('mfpasien.no_medrec LIKE' => '%'.$this->session->userdata('nomedrec').'%'));
				$where .= " AND mfpasien.no_medrec LIKE '%" . $this->session->userdata('nomedrec') . "%'";
			}
			if ($this->session->userdata('namapasien') != null) {
				// $this->where = array_merge($this->where, array('mfpasien.nama LIKE' => '%'.$this->session->userdata('namapasien').'%'));
				$where .= " AND mfpasien.nama LIKE '%" . $this->session->userdata('namapasien') . "%'";
			}
			if ($this->session->userdata('idasalpasien') != 0) {
				// $this->where = array_merge($this->where, array('trujukan_radiologi.asalrujukan' => $this->session->userdata('idasalpasien')));
				$where .= " AND trujukan_radiologi.asalrujukan = '" . $this->session->userdata('idasalpasien') . "'";
			}
			if ($this->session->userdata('tanggaldari') != null) {
				// $this->where = array_merge($this->where, array('DATE(trujukan_radiologi.tanggal) >=' => YMDFormat($this->session->userdata('tanggaldari'))));
				$where .= " AND DATE(trujukan_radiologi.tanggal) >= '" . YMDFormat($this->session->userdata('tanggaldari')) . "'";
			}
			if ($this->session->userdata('tanggalsampai') != null) {
				// $this->where = array_merge($this->where, array('DATE(trujukan_radiologi.tanggal) <=' => YMDFormat($this->session->userdata('tanggalsampai'))));
				$where .= " AND DATE(trujukan_radiologi.tanggal) <= '" . YMDFormat($this->session->userdata('tanggalsampai')) . "'";
			}
			if ($this->session->userdata('iddokterperujuk') != 0) {
				// $this->where = array_merge($this->where, array('trujukan_radiologi.iddokterperujuk' => $this->session->userdata('iddokterperujuk')));
				$where .= " AND trujukan_radiologi.iddokterperujuk = '" . $this->session->userdata('iddokterperujuk') . "'";
			}
			if ($this->session->userdata('iddokterradiologi') != 0) {
				// $this->where = array_merge($this->where, array('trujukan_radiologi.iddokterradiologi' => $this->session->userdata('iddokterradiologi')));
				$where .= " AND trujukan_radiologi.iddokterradiologi = '" . $this->session->userdata('iddokterradiologi') . "'";
			}
			if ($this->session->userdata('idtindakan') != 0) {
				// $this->where = array_merge($this->where, array('trujukan_radiologi_detail.idradiologi' => $this->session->userdata('idtindakan')));
				$where .= " AND trujukan_radiologi_detail.idradiologi = '" . $this->session->userdata('idtindakan') . "'";
			}

			// $this->where = array_merge($this->where, array('tmp_dokter' => null));
		} else {
			$where .= " AND DATE(trujukan_radiologi.tanggal) >= '" . YMDFormat(date('Y-m-d')) . "'";
		}

		$this->select = [];
		$from = "SELECT *FROM (SELECT `trujukan_radiologi`.`id`, (CASE
									WHEN trujukan_radiologi.asalrujukan = 3 THEN
										trawatinap_pendaftaran.id
									ELSE
										tpoliklinik_pendaftaran.id
								END) AS idpendaftaran, `mfpasien`.`id` AS `idpasien`, `trujukan_radiologi`.`tanggal`, `trujukan_radiologi`.`noantrian`, `trujukan_radiologi`.`norujukan`, (CASE
									WHEN trujukan_radiologi.asalrujukan = 3 THEN
										trawatinap_pendaftaran.nopendaftaran
									ELSE
										tpoliklinik_pendaftaran.nopendaftaran
								END) AS nopendaftaran,
                (CASE
									WHEN trujukan_radiologi.asalrujukan = 3 THEN
										trawatinap_pendaftaran.idtipe
									ELSE
										tpoliklinik_pendaftaran.idtipe
								END) AS idtipe, `trujukan_radiologi`.`nomorfoto`, `mfpasien`.`no_medrec` AS `nomedrec`, `mfpasien`.`nama` AS `namapasien`, COALESCE ( mdokterperujuk.nama, '-' ) AS namadokterperujuk, COALESCE ( mdokterradiologi.nama, '-' ) AS namadokterradiologi, `trujukan_radiologi`.`asalrujukan`, `trujukan_radiologi`.`status`, `trujukan_radiologi`.`statuspasien`, (CASE WHEN trujukan_radiologi.iddokterradiologi='0' THEN '-' ELSE musers_dokter_radiologi.iddokter END) as tmp_dokter,musers_dokter_radiologi.tipe as dokter_user,
                tkasir.status AS statuskasir,
                COALESCE(trawatinap_pendaftaran.idkelas, 0) AS idkelas
					FROM `trujukan_radiologi`
					LEFT JOIN `trawatinap_pendaftaran` ON `trujukan_radiologi`.`idtindakan` = `trawatinap_pendaftaran`.`id` AND `trujukan_radiologi`.`asalrujukan` = 3
					LEFT JOIN `tpoliklinik_tindakan` ON `tpoliklinik_tindakan`.`id` = `trujukan_radiologi`.`idtindakan` AND `trujukan_radiologi`.`asalrujukan` IN (1, 2)  AND tpoliklinik_tindakan.status = 1
					LEFT JOIN `tpoliklinik_pendaftaran` ON `tpoliklinik_pendaftaran`.`id` = `tpoliklinik_tindakan`.`idpendaftaran` OR `tpoliklinik_pendaftaran`.`id` = `trawatinap_pendaftaran`.`idpoliklinik`
					LEFT JOIN `mfpasien` ON `mfpasien`.`id` = `tpoliklinik_pendaftaran`.`idpasien`
					LEFT JOIN `mdokter` `mdokterperujuk` ON `mdokterperujuk`.`id` = `trujukan_radiologi`.`iddokterperujuk`
					LEFT JOIN `mdokter` `mdokterradiologi` ON `mdokterradiologi`.`id` = `trujukan_radiologi`.`iddokterradiologi`
					LEFT JOIN `trujukan_radiologi_detail` ON `trujukan_radiologi_detail`.`idradiologi` = `trujukan_radiologi`.`id`
					LEFT JOIN `musers_dokter_radiologi` ON `musers_dokter_radiologi`.`iddokter` = `trujukan_radiologi`.`iddokterradiologi` AND musers_dokter_radiologi.iduser='$id_user'
					LEFT JOIN `tkasir` ON `tkasir`.`idtipe` IN (1,2) AND `tkasir`.`idtindakan` = `tpoliklinik_tindakan`.`id`
					WHERE trujukan_radiologi.id <> '0' " . $where . '
					GROUP BY `trujukan_radiologi`.`id`
                    ORDER BY `trujukan_radiologi`.`noantrian` ASC) as tmp ' . $where2;
		$this->from = '( ' . $from . ' ) as TBL';
		$iduser = $this->session->userdata('user_id');
		$this->join = [];

		$this->where = [];

		// FILTER

		$this->group = [];

		$this->order = [];

		$this->column_search = [];
		$this->column_order = [];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];

		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->status == 0) {
				$action = '<label>-</label>';
			} elseif ($r->status == 1) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['996'])) {
					if ($r->dokter_user != '2') {
						$action .= '<a href="' . site_url() . 'trujukan_radiologi/create/' . $r->id . '" data-toggle="tooltip" title="Tindakan" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					}
				}
				if (UserAccesForm($user_acces_form, ['1001'])) {
					if ($r->dokter_user != '2') {
						$action .= '<a href="#" data-urlindex="' . site_url() . 'trujukan_radiologi" data-urlremove="' . site_url() . 'trujukan_radiologi/delete/' . $r->id . '" data-messagewarning="Batalkan record tersebut?" data-messagesuccess="Data telah dibatalkan." data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash"></i></a>';
					}
				}
				if (UserAccesForm($user_acces_form, ['1005'])) {
					$action .= '<div class="btn-group">
                            <div class="btn-group dropup">';
					$action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"><span class="fa fa-print"></span></button>';
					$action .= '<ul class="dropdown-menu">
                                <li>';
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';

					$action .= '</li></ul>';

					$action .= '</div>
                          </div>
        	             </div>';
				}
			} elseif ($r->status == 2) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['1002'])) {
					if ($r->dokter_user != '2') {
						$action .= '<a href="#" data-urlindex="' . site_url() . 'trujukan_radiologi" data-urlremove="' . site_url() . 'trujukan_radiologi/review/' . $r->id . '/1" data-messagewarning="Langsung record tersebut?" data-messagesuccess="Data telah dilangsung." data-toggle="tooltip" title="Langsung" class="btn btn-warning btn-sm removeData"><i class="fa fa-check"></i></a>';
					}
				}

				if (UserAccesForm($user_acces_form, ['1003'])) {
					if ($r->dokter_user != '2') {
						$action .= '<a href="#" data-urlindex="' . site_url() . 'trujukan_radiologi" data-urlremove="' . site_url() . 'trujukan_radiologi/reject/' . $r->id . '/2/' . $r->idkelas . '" data-messagewarning="Menolak record tersebut?" data-messagesuccess="Data telah ditolak." data-toggle="tooltip" title="Menolak" class="btn btn-danger btn-sm removeData"><i class="fa fa-ban"></i></a>';
					}
				}

				$action .= '<div class="btn-group">
                          <div class="btn-group dropup">';
				$action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                              <span class="fa fa-print"></span>
                            </button>';
				$action .= '<ul class="dropdown-menu">
                              <li>';
				if (UserAccesForm($user_acces_form, ['1005'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';
				}
				if (UserAccesForm($user_acces_form, ['1006'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_transaksi/' . $r->id . '">Cetak Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1007'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan/' . $r->id . '">Bukti Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1008'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan_thermal/' . $r->id . '">Bukti Pemeriksaan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1009'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan/' . $r->id . '">Bukti Pengambilan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1010'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan_thermal/' . $r->id . '">Bukti Pengambilan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1011'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/manage_print_label/' . $r->id . '">Cetak Label</a>';
				}
				$action .= '</li>
                            </ul>
                          </div>
                        </div>
                    	</div>&nbsp;';
				if (UserAccesForm($user_acces_form, ['1012'])) {
					$action .= '<div class="btn-group">
                        <a href="' . site_url() . 'trujukan_radiologi/edit/' . $r->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>

                      </div>';
				}
			}

			if ($r->status == 2 && $r->statuspasien == 1) {
				$action = '<div class="btn-group">';
				if ($r->dokter_user != '2') {
					$action .= '<a href="' . site_url() . 'trujukan_radiologi/review/' . $r->id . '/1" data-toggle="tooltip" title="Lihat" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>';
				}
				$action .= '<div class="btn-group">
                          <div class="btn-group dropup">';
				$action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                              <span class="fa fa-print"></span>
                            </button>';
				$action .= '<ul class="dropdown-menu">
                              <li>';
				if (UserAccesForm($user_acces_form, ['1005'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';
				}
				if (UserAccesForm($user_acces_form, ['1006'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_transaksi/' . $r->id . '">Cetak Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1007'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan/' . $r->id . '">Bukti Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1008'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan_thermal/' . $r->id . '">Bukti Pemeriksaan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1009'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan/' . $r->id . '">Bukti Pengambilan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1010'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan_thermal/' . $r->id . '">Bukti Pengambilan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1011'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/manage_print_label/' . $r->id . '">Cetak Label</a>';
				}
				if (UserAccesForm($user_acces_form, ['1202'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_review/' . $r->id . '">Cetak Hasil</a>';
				}
				$action .= '</li>
                            </ul>
                          </div>
                        </div>
                      </div>';

				if (UserAccesForm($user_acces_form, ['1012'])) {
					$action .= '&nbsp;<div class="btn-group">
                        <a href="' . site_url() . 'trujukan_radiologi/edit/' . $r->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                      </div>';
				}
			} elseif ($r->status == 2 && $r->statuspasien == 2) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['1004'])) {
					if ($r->dokter_user != '2') {
						$action .= ' <a href="#" data-urlindex="' . site_url() . 'trujukan_radiologi" data-urlremove="' . site_url() . 'trujukan_radiologi/review/' . $r->id . '/3" data-messagewarning="Kembali record tersebut?" data-messagesuccess="Data telah dikembalikan." data-toggle="tooltip" title="Kembali" class="btn btn-warning btn-sm removeData"><i class="fa fa-reply"></i></a>';
					}
				}
				$action .= '<div class="btn-group">';
				$action .= '<div class="btn-group dropup">';
				$action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                              <span class="fa fa-print"></span>
                            </button>';
				$action .= '<ul class="dropdown-menu">
                              <li>';
				if (UserAccesForm($user_acces_form, ['1005'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';
				}
				if (UserAccesForm($user_acces_form, ['1006'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_transaksi/' . $r->id . '">Cetak Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1007'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan/' . $r->id . '">Bukti Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1008'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan_thermal/' . $r->id . '">Bukti Pemeriksaan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1009'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan/' . $r->id . '">Bukti Pengambilan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1010'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan_thermal/' . $r->id . '">Bukti Pengambilan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1011'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/manage_print_label/' . $r->id . '">Cetak Label</a>';
				}
				if (UserAccesForm($user_acces_form, ['1202'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_review/' . $r->id . '">Cetak Hasil</a>';
				}
				$action .= ' </li>
                            </ul>
                          </div>
                        </div>
                    	</div>';
				if (UserAccesForm($user_acces_form, ['1012'])) {
					$action .= '&nbsp;<div class="btn-group">
                        <a href="' . site_url() . 'trujukan_radiologi/edit/' . $r->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                      </div>';
				}
			} elseif ($r->status == 2 && $r->statuspasien == 3) {
				$action = '<div class="btn-group">';
				if ($r->dokter_user != '2') {
					$action .= '<a href="' . site_url() . 'trujukan_radiologi/review/' . $r->id . '/3" data-toggle="tooltip" title="Lihat" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>';
				}
				$action .= ' <div class="btn-group">
                            <div class="btn-group dropup">';
				$action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="fa fa-print"></span>
                              </button>';
				$action .= '<ul class="dropdown-menu">
                                <li>';
				if (UserAccesForm($user_acces_form, ['1005'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';
				}
				if (UserAccesForm($user_acces_form, ['1006'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_transaksi/' . $r->id . '">Cetak Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1007'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan/' . $r->id . '">Bukti Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1008'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pemeriksaan_thermal/' . $r->id . '">Bukti Pemeriksaan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1009'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan/' . $r->id . '">Bukti Pengambilan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1010'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_bukti_pengambilan_thermal/' . $r->id . '">Bukti Pengambilan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1011'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/manage_print_label/' . $r->id . '">Cetak Label</a>';
				}
				if (UserAccesForm($user_acces_form, ['1202'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_radiologi/print_review/' . $r->id . '">Cetak Hasil</a>';
				}
				$action .= '</li>
                              </ul>
                            </div>
                          </div>
                         </div>';
				if (UserAccesForm($user_acces_form, ['1012'])) {
					$action .= '&nbsp;<div class="btn-group">
                        <a href="' . site_url() . 'trujukan_radiologi/edit/' . $r->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                      </div>';
				}
			}

			if ($r->statuskasir != 2) {
				$action .= '&nbsp;<a href="#" data-urlindex="' . site_url() . 'trujukan_radiologi" data-urlremove="' . site_url() . 'trujukan_radiologi/delete/' . $r->id . '" data-messagewarning="Batalkan record tersebut?" data-messagesuccess="Data telah dibatalkan." data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash"></i></a>';
			}

			$row[] = $no;
			$row[] = $r->tanggal . '-' . $r->status . '-' . $r->statuspasien;
			$row[] = $r->noantrian;
			$row[] = $r->norujukan;
			$row[] = $r->nopendaftaran;
			$row[] = ($r->nomorfoto ? $r->nomorfoto : '-');
			$row[] = $r->nomedrec;
			$row[] = $r->namapasien;
			$row[] = $r->namadokterperujuk;
			$row[] = $r->namadokterradiologi;
			if ($r->asalrujukan == 3 && $r->idtipe == 2) {
				$row[] = 'One Day Surgery (ODS)';
			} else {
				$row[] = GetAsalRujukan($r->asalrujukan);
			}
			$row[] = StatusRujukanRadiologi($r->status, $r->statuspasien);
			$row[] = $action;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getTindakan($idkelas, $idkelompokpasien, $idrekanan, $idtipe, $idexpose, $idfilm)
	{
		if ($idkelompokpasien == 1) {
			// Perusahaan Asuransi && Rekanan
			$this->db->select('mtarif_radiologi.path');
			if ($idtipe == 1) {
				$this->db->join('mrekanan', 'mrekanan.tradiologi_xray = mtarif_radiologi.id');
			} elseif ($idtipe == 2) {
				$this->db->join('mrekanan', 'mrekanan.tradiologi_usg = mtarif_radiologi.id');
			} elseif ($idtipe == 3) {
				$this->db->join('mrekanan', 'mrekanan.tradiologi_ctscan = mtarif_radiologi.id');
			} elseif ($idtipe == 4) {
				$this->db->join('mrekanan', 'mrekanan.tradiologi_mri = mtarif_radiologi.id');
			} elseif ($idtipe == 5) {
				$this->db->join('mrekanan', 'mrekanan.tradiologi_bmd = mtarif_radiologi.id');
			}

			$this->db->where('mrekanan.id', $idrekanan);
			$query = $this->db->get('mtarif_radiologi');
			$row = $query->row();
			$rowpath = '';
			if ($row) {
				$rowpath = $row->path;
			} else {
				// Perusahaan Asuransi && Rekanan NULL
				$this->db->select('mtarif_radiologi.path');
				if ($idtipe == 1) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_xray = mtarif_radiologi.id');
				} elseif ($idtipe == 2) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_usg = mtarif_radiologi.id');
				} elseif ($idtipe == 3) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_ctscan = mtarif_radiologi.id');
				} elseif ($idtipe == 4) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_mri = mtarif_radiologi.id');
				} elseif ($idtipe == 5) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_bmd = mtarif_radiologi.id');
				}

				$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
				$query = $this->db->get('mtarif_radiologi');
				$row = $query->row();
				$rowpath = '';
				if ($row) {
					$rowpath = $row->path;
				}
			}
		} else {
			// Non Rekanan
			$this->db->select('mtarif_radiologi.path');
			if ($idtipe == 1) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_xray = mtarif_radiologi.id');
			} elseif ($idtipe == 2) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_usg = mtarif_radiologi.id');
			} elseif ($idtipe == 3) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_ctscan = mtarif_radiologi.id');
			} elseif ($idtipe == 4) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_mri = mtarif_radiologi.id');
			} elseif ($idtipe == 5) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_bmd = mtarif_radiologi.id');
			}

			$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
			$query = $this->db->get('mtarif_radiologi');
			$row = $query->row();
			$rowpath = '';
			if ($row) {
				$rowpath = $row->path;
			}
		}

		$this->select = [
			'mtarif_radiologi.*',
			'mtarif_radiologi_detail.*',
			'mtarif_radiologi_expose.nama AS expose',
			'mtarif_radiologi_film.nama AS film'
		];

		$this->from = 'mtarif_radiologi';

		$this->join = [
			['mtarif_radiologi_detail', 'mtarif_radiologi_detail.idtarif = mtarif_radiologi.id', 'LEFT'],
			['mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT'],
			['mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT']
		];

		if ($idtipe == 1) {
			$this->where = [
				'mtarif_radiologi.idtipe' => $idtipe,
				'mtarif_radiologi_detail.kelas' => $idkelas,
				'mtarif_radiologi.status' => '1',
				'mtarif_radiologi.path LIKE' => $rowpath . '%'
			];
			if ($idexpose != '0') {
				$this->where['mtarif_radiologi.idexpose'] = $idexpose;
			}
			if ($idfilm != '0') {
				$this->where['mtarif_radiologi.idfilm'] = $idfilm;
			}
		} else {
			$this->where = [
				'mtarif_radiologi.idtipe' => $idtipe,
				'mtarif_radiologi_detail.kelas' => $idkelas,
				'mtarif_radiologi.status' => '1',
				'mtarif_radiologi.path LIKE' => $rowpath . '%'
			];
		}

		// $this->like = array(
		//   'mtarif_radiologi.path' => $rowpath.'%'
		// );

		$this->order = [
			"SUBSTRING_INDEX(CONCAT( mtarif_radiologi.path ,'.'),'.',1) + 0,
			SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_radiologi.path ,'.'),'.',2),'.',-1) + 0,
			SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_radiologi.path ,'.'),'.',3),'.',-1) + 0,
			SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_radiologi.path ,'.'),'.',4),'.',-1) + 0" => '',
		];
		
		$this->group = [];

		$this->column_search = ['mtarif_radiologi.nama'];
		$this->column_order = ['mtarif_radiologi.nama'];

		$list = $this->datatable->get_datatables();

		// print_r($this->db->last_query());exit();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->idtipe == 1) {
				$labelXray = ' ( ' . $r->expose . ' ' . $r->film . ' )';
			} else {
				$labelXray = '';
			}

			if ($r->idkelompok == 1) {
				$action = TreeView($r->level, $r->nama);
			} else {
				$action = '<a href="javascript:void(0)" data-idtindakan="' . $r->id . '" data-idtiperadiologi="' . $r->idtipe . '" id="tindakan-select">' . TreeView($r->level, $r->nama) . $labelXray . '</a>';
			}

			$row[] = $no;
			$row[] = $action;
			$row[] = number_format($r->jasasarana);
			$row[] = number_format($r->jasapelayanan);
			$row[] = number_format($r->bhp);
			$row[] = number_format($r->biayaperawatan);
			$row[] = number_format($r->total);

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}
