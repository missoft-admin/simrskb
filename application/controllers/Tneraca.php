<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tneraca extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tneraca_model','model');
		$this->load->helper('path');
		
  }

	function index(){
		
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Laporan Neraca';
			$data['content'] 		= 'Tneraca/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Laporan Neraca",'#'),
												  array("List",'tneraca')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex()
   {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$user_id=$this->session->userdata('user_id');
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			
			if ($status !='#'){
				$where .=" AND status='$status'";
			}
			if ($nama !=''){
				$where .=" AND nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT *
						from tneraca_setting M
						LEFT JOIN tneraca_setting_user U ON U.template_id=M.id
						WHERE U.iduser IN (".$user_id.") AND M.status='1'
						GROUP BY M.id
					) as tbl WHERE id IS NOT NULL ".$where."
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->nama;
         $result[] = StatusBarang($r->status);
         
          $aksi = '<div class="btn-group">';
				$aksi .= '<a href="'.site_url().'tneraca/detail/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'tneraca/detail/'.$r->id.'" data-toggle="tooltip" title="Detail" class="btn btn-primary btn-xs"><i class="fa fa-th-list"></i></a>';
				$aksi .= '<a href="'.site_url().'tneraca/edit/'.$r->id.'" data-toggle="tooltip" title="Adjustment" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a href="'.site_url().'tneraca/manage/'.$r->id.'" data-toggle="tooltip" title="Management Anggaran" class="btn btn-danger btn-xs"><i class="si si-docs"></i></a>';
				$aksi .= '<button data-toggle="tooltip" title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></button>';
			}else{
				$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function detail($id){
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('Y-m-01', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		// print_r($tgl_terakhir);exit();
		
		
		// $date1=date_create('2021-01-01');
		
		// $date1= date_format($date1,"Y-m-d");
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		// if (UserAccesForm($user_acces_form,array('2'))){
			$data = $this->model->get_template($id);
			$data['list_section'] 			=$this->model->list_section($id);
			$data['list_kategori'] 			=$this->model->list_kategori($id);
			$data['list_akun'] 			=$this->model->list_akun($id);
			$data['tanggal_trx1'] 			=HumanDateShort($tgl_pertama);
			$data['tanggal_trx2'] 			=HumanDateShort($tgl_terakhir);
			// $data['tanggal_trx2'] 			= date('d-m-Y');
			$data['disabel'] 			= '';
			$data['template_id'] 			= $id;
			$data['error'] 			= '';
			$data['title'] 			= 'Laporan Neraca';
			$data['content'] 		= 'Tneraca/index_detail';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Laporan Neraca",'#'),
												  array("List",'tneraca')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		// }else{
			// redirect('page404');
		// }
	}
	function edit($id){
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('Y-m-01', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		// print_r($tgl_terakhir);exit();
		
		
		// $date1=date_create('2021-01-01');
		
		// $date1= date_format($date1,"Y-m-d");
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		// if (UserAccesForm($user_acces_form,array('2'))){
			$data = $this->model->get_template($id);
			$data['list_section'] 			=$this->model->list_section($id);
			$data['list_kategori'] 			=$this->model->list_kategori($id);
			$data['list_akun'] 			=$this->model->list_akun($id);
			$data['tanggal_trx1'] 			=HumanDateShort($tgl_pertama);
			$data['tanggal_trx2'] 			=HumanDateShort($tgl_terakhir);
			// $data['tanggal_trx2'] 			= date('d-m-Y');
			$data['disabel'] 			= '';
			$data['template_id'] 			= $id;
			$data['error'] 			= '';
			$data['title'] 			= 'Adjustment Laporan Neraca';
			$data['content'] 		= 'Tneraca/index_detail';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Laporan Neraca",'#'),
												  array("List",'tneraca')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		// }else{
			// redirect('page404');
		// }
	}
	
	
	function generate_periode(){
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$st_terakhir='0';
			$where='';
			$tanggal_trx1 = YMDFormat($this->input->post('tanggal_trx1'));	
			
			$tanggal_trx2 = YMDFormat($this->input->post('tanggal_trx2'));	
			$tgl_terakhir = date('Y-m-t', strtotime($tanggal_trx2));
			if ($tanggal_trx2==$tgl_terakhir){
				$st_terakhir='1';
			}
				// print_r($st_terakhir);exit();
			$tanggal__compare_trx1 = YMDFormat($this->input->post('tanggal__compare_trx1'));	
			$tanggal__compare_trx2 = YMDFormat($this->input->post('tanggal__compare_trx2'));	
			$compare_by = $this->input->post('compare_by');	
			$tipe_periode = $this->input->post('tipe_periode');	
			$jml_periode = $this->input->post('jml_periode');	
			$st_compare = $this->input->post('st_compare');	
			
			$option='';
			if ($st_compare=='1'){				
			
				if ($compare_by=='1'){
					if ($tanggal__compare_trx1 < $tanggal_trx1){
						$tgl=HumanDateShort($tanggal__compare_trx1).' s/d '.HumanDateShort($tanggal__compare_trx2);
						$tgl_val=($tanggal__compare_trx1).'_'.($tanggal__compare_trx2);
						$option .='<option value="'.$tgl_val.'" selected>'.$tgl.'</option>';
						$tgl=HumanDateShort($tanggal_trx1).' s/d '.HumanDateShort($tanggal_trx2);
						$tgl_val=($tanggal_trx1).'_'.($tanggal_trx2);				
						$option .='<option value="'.$tgl_val.'" selected>'.$tgl.'</option>';		
					}else{
						$tgl=HumanDateShort($tanggal_trx1).' s/d '.HumanDateShort($tanggal_trx2);
						$tgl_val=($tanggal_trx1).'_'.($tanggal_trx2);				
						$option .='<option value="'.$tgl_val.'" selected>'.$tgl.'</option>';
						$tgl=HumanDateShort($tanggal__compare_trx1).' s/d '.HumanDateShort($tanggal__compare_trx2);
						$tgl_val=($tanggal__compare_trx1).'_'.($tanggal__compare_trx2);
						$option .='<option value="'.$tgl_val.'" selected>'.$tgl.'</option>';
							
					}
							
				}else{
					if ($tipe_periode=='1'){//SEBELUM
						for($i=$jml_periode;$i>=0;$i--){
							$tgl_1=MinMonthToDate($tanggal_trx1,($i));
							$tgl_2=MinMonthToDate($tanggal_trx2,($i));
							if ($st_terakhir=='1'){
								$tgl_2=date('Y-m-t', strtotime($tgl_2));
							}
							$tgl=HumanDateShort($tgl_1).' s/d '.HumanDateShort($tgl_2);
							$tgl_val=($tgl_1).'_'.($tgl_2);
							$option .='<option value="'.$tgl_val.'" selected>'.$tgl.'</option>';		
						}
					}else{//SESUDAH					
						// print_r($jml_periode);exit();
						for($i=0;$i<=$jml_periode;$i++){
							$tgl_1=addMonthToDate($tanggal_trx1,($i));
							$tgl_2=addTime($tanggal_trx2,0,$i,0);
							if ($st_terakhir=='1'){
								$tgl_2=date('Y-m-t', strtotime($tgl_2));
							}
							$tgl=HumanDateShort($tgl_1).' s/d '.HumanDateShort($tgl_2);
							$tgl_val=($tgl_1).'_'.($tgl_2);
							$option .='<option value="'.$tgl_val.'" selected>'.$tgl.'</option>';		
							// print_r($option);exit();
						}
					}
				}
			}else{
				$tgl=HumanDateShort($tanggal_trx1).' s/d '.HumanDateShort($tanggal_trx2);
				$tgl_val=($tanggal_trx1).'_'.($tanggal_trx2);				
				$option .='<option value="'.$tgl_val.'" selected>'.$tgl.'</option>';		
			}
		  
		 $arr['detail']=$option;
		  echo json_encode($arr);
	  
		
	}
	function LoadNeraca()
   {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$user_id=$this->session->userdata('user_id');
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$arr_data=array();
			$where='';
			$section="''";
			$kategori="''";
			$akun="''";
			$template_id = $this->input->post('template_id');			
			$tanggal_trx1 = YMDFormat($this->input->post('tanggal_trx1'));	
			$tanggal_trx2 = YMDFormat($this->input->post('tanggal_trx2'));	
			$periode = $this->input->post('periode');	
			$jml_periode=count($periode);
			$idsection = $this->input->post('idsection');	
			$idakun = $this->input->post('idakun');	
			$idkategori = $this->input->post('idkategori');	
			if ($idsection){
				$section="'".implode(',',$idsection)."'";			
			}
			if ($idkategori){
				$kategori="'".implode(',',$idkategori)."'";			
			}
			if ($idakun){
				$akun="'".implode(',',$idakun)."'";			
			}
			$tanggal=explode("_", $periode[0]);
			$tgl_1=YMDFormat($tanggal[0]);
			$tgl_2=YMDFormat($tanggal[1]);
			// print_r($tgl_1);exit();
			// $from="call lapNeraca_SQL('$template_id', '$tanggal_trx1','$tanggal_trx2')";
			$from="call lapNeraca('$template_id','$tgl_1','$tgl_2',".$section.",".$kategori.",".$akun.")";	
			// print_r($from);exit;
			$query = $this->db->query($from);
			$list = $query->result();
			mysqli_next_result($this->db->conn_id );
			$query->free_result();
			// $list->next_result();
			// $list->free_result();
			if ($jml_periode>1){
				foreach($periode as $x => $val) {
					if ($x > 0){
						//$tmp=mysqli_next_result($this->db->conn_id );
						//@mysqli_free_result($tmp);
						
						$tanggal_arr=explode("_", $periode[$x]);
						$tanggal_1=YMDFormat($tanggal_arr[0]);
						$tanggal_2=YMDFormat($tanggal_arr[1]);
						$q="call lapNeraca('$template_id','$tanggal_1','$tanggal_2',".$section.",".$kategori.",".$akun.")";					
						$arr_query = $this->db->query($q);
						$arr_data[$x-1] = $arr_query->result();
						mysqli_next_result($this->db->conn_id );
						$arr_query->free_result();
						
						// mysqli_next_result($this->db->conn_id );
						// mysqli_free_result($arr_data[$x-1]);
					}
				}
						// print_r($arr_data);exit();
				
			}
			
			// print_r(count($periode));exit();
			  // print_r($arr_data);exit;
			  $no = 0;
			  $nilai_lama = 0;
			  $total_rows=count($list);
			  foreach ($list as $r) {
				  $result = array();

				  $result[] = $r->lev;
				  $result[] = $r->idakun;
				  $result[] = $r->st_formula;
				  $result[] = ($r->idakun==null?'<strong>':'&nbsp;&nbsp;&nbsp;&nbsp;'.$r->noakun.'&nbsp;&nbsp;&nbsp;&nbsp;').$r->nama.($r->idakun==null?'</strong>':'');
				  if ($r->idakun==null){
					  $result[] = ($r->idakun==null?'<strong>':'').number_format($r->nilai,2).($r->idakun==null?'</strong>':'');	
				  }else{
					$result[] = '<a href="'.site_url().'tbuku_besar/index/'.$r->idakun.'/'.DMYFormat2(YMDFormat($tgl_1)).'/'.DMYFormat2(YMDFormat($tgl_2)).'" target="_blank">'.number_format($r->nilai,2).'</a>';					  
				  }
				  // $result[] = ($r->idakun==null?'<strong>':'').'<a href="'.site_url().'tbuku_besar/index/'.$r->id.'/'.DMYFormat2(YMDFormat($tanggal_trx1)).'/'.DMYFormat2(YMDFormat($tanggal_trx2)).'" target="_blank">'.number_format($r->nilai,2).'</a>'.($r->idakun==null?'</strong>':'');
				  $nilai_lama=$r->nilai;
				  foreach($periode as $x => $val) {
					  if ($x>0){
						$tanggal_arr=explode("_", $periode[$x]);
						$tanggal_1=YMDFormat($tanggal_arr[0]);
						$tanggal_2=YMDFormat($tanggal_arr[1]);
						
						$nilai_baru=$arr_data[$x-1][$no]->nilai;
						$nilai_rp=$nilai_baru-$nilai_lama;
						if ($nilai_lama!=0){
							$nilai_persen=($nilai_rp*100)/$nilai_lama;				
							if ($nilai_lama<0 && $nilai_baru>0){
								$nilai_persen=$nilai_persen*(-1);
							}
						}else{
							$nilai_persen=0;
						}
						
						$result[] =format_kenaikan($nilai_rp);
						$result[] =format_kenaikan_persen($nilai_persen);
						if ($r->idakun==null){
							$result[] = ($r->idakun==null?'<strong>':'').number_format($nilai_baru,2).($r->idakun==null?'</strong>':'');	
						}else{
							$result[] = '<a href="'.site_url().'tbuku_besar/index/'.$r->idakun.'/'.DMYFormat2(YMDFormat($tanggal_1)).'/'.DMYFormat2(YMDFormat($tanggal_2)).'" target="_blank">'.number_format($nilai_baru,2).'</a>';					  
						}
						$nilai_lama=$nilai_baru;
					  }
				  }
				  
				  // $result[] = number_format($r->nilai,2);
				  // $result[] = ($r->nilai);
				 

				  $data[] = $result;
				  $no++;
			  }
			  $output = array(
				  "draw" => $_POST['draw'],
				  "recordsTotal" => $total_rows,
				  "recordsFiltered" => $total_rows,
				  "data" => $data
			  );
			  echo json_encode($output);
	}
	function manage($id){
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('Y-01-01', strtotime($hari_ini));
		$tgl_terakhir = date('Y-12-31', strtotime($hari_ini));
		// print_r($tgl_terakhir);exit();
		
		
		// $date1=date_create('2021-01-01');
		
		// $date1= date_format($date1,"Y-m-d");
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		// if (UserAccesForm($user_acces_form,array('2'))){
			$data = $this->model->get_template($id);
			$data['list_section'] 			=$this->model->list_section($id);
			$data['list_kategori'] 			=$this->model->list_kategori($id);
			$data['list_akun'] 			=$this->model->list_akun($id);
			$data['tanggal_trx1'] 			=HumanDateShort($tgl_pertama);
			$data['tanggal_trx2'] 			=HumanDateShort($tgl_terakhir);
			// $data['tanggal_trx2'] 			= date('d-m-Y');
			$data['disabel'] 			= '';
			$data['template_id'] 			= $id;
			$data['error'] 			= '';
			$data['title'] 			= 'Laporan Anggaran Neraca';
			$data['content'] 		= 'Tneraca/index_manage';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Laporan Neraca",'#'),
												  array("List",'tneraca')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		// }else{
			// redirect('page404');
		// }
	}
	function load_periode_manage(){
		$opsi='';
		$tanggal_trx1 = YMDFormat($this->input->post('tanggal_trx1'));	
		$tanggal_trx2 = YMDFormat($this->input->post('tanggal_trx2'));
		$tahun_awal=YFormat($tanggal_trx1);
		$bulan_awal=MFormat($tanggal_trx1);
		$tahun_akhir=YFormat($tanggal_trx2);
		$bulan_akhir=MFormat($tanggal_trx2);
		
		// print_r($tahun_awal.'-'.$bulan_awal);exit();
		for ($i=(int)$bulan_awal;$i<=12;$i++){
			$periode=$tahun_awal.str_pad($i, 2, '0', STR_PAD_LEFT);
			$opsi .='<option value="'. $periode.'">'.get_periode_2($periode).'</option>';
			
		}
		if ($tahun_awal != $tahun_akhir){
			for ($i=1;$i<=(int)$bulan_akhir;$i++){
				$periode=$tahun_akhir.str_pad($i, 2, '0', STR_PAD_LEFT);
				$opsi .='<option value="'. $periode.'">'.get_periode_2($periode).'</option>';
				
			}
		}
		// $opsi .='<option value="'. $row->periode.'" selected>'.get_periode_2($row->periode).'</option>';
		// $q="SELECT DISTINCT(H.periode) FROM tneraca_setting_anggaran_detail H WHERE H.tahun='$tahun' AND H.template_id='$template_id' ORDER BY H.periode";
		// $rows=$this->db->query($q)->result();
		// foreach($rows as $row){
		// }
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));

	}
}
