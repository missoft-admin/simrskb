<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpiutang_tt_umur extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_tt_umur_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tgl_trx1'=>$tgl_pertama,
			'tgl_trx2'=>date('d-m-Y'),
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Umur Piutang';
		$data['content'] 		= 'Tpiutang_tt_umur/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Umur Piutang",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		
		$where1='';
		$where2='';
		
		
		if ($idkelompokpasien !='#'){
			$where1 .=" AND H.idkelompokpasien='$idkelompokpasien' ";
			$where2 .=" AND H.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			// $where1 .=" AND H.idkontraktor='$idrekanan' ";
			if ($idkelompokpasien =='1'){
				$where2 .=" AND H.idrekanan='$idrekanan' ";
			}
		}
		
        $from = "(
					SELECT H.idtipe, H.idkelompokpasien,H.idrekanan,KP.nama as rekanan_nama, SUM(H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus) as sisa,SUM(H.nominal_tagihan) as total_tagihan
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) <=1 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_1
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) =2 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_2
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) =3 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_3
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) =4 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_4
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) =5 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_5
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) =6 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_6
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) =12 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_12
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) >12 AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) <24 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_24_kecil
					,SUM(CASE WHEN ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) >=24 THEN H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus ELSE 0 END) as bulan_24_besar

					from tpiutang_tt_verifikasi H
					LEFT JOIN mrekanan R ON R.id=H.idrekanan
					LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
					WHERE H.`status` !=0 AND H.st_lunas='0' 
					GROUP BY H.idtipe

				) as tbl ";
				
		//Jenis Rekanan : 1 : Asuransi !=1 SELAIN ASURANSI
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $url        = site_url('tpiutang_tt_umur/');
            $row[] = $no;
            $row[] = GetTipePasienPiutang($r->idtipe);
            $row[] = number_format($r->sisa,2);  
			$row[] = ($r->bulan_1 > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/1" target="_blank">'.number_format($r->bulan_1,2).'</a>':'0');
			$row[] = ($r->bulan_2 > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/2" target="_blank">'.number_format($r->bulan_2,2).'</a>':'0');
			$row[] = ($r->bulan_3 > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/3" target="_blank">'.number_format($r->bulan_3,2).'</a>':'0');
			$row[] = ($r->bulan_4 > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/4" target="_blank">'.number_format($r->bulan_4,2).'</a>':'0');
			$row[] = ($r->bulan_5 > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/5" target="_blank">'.number_format($r->bulan_5,2).'</a>':'0');
			$row[] = ($r->bulan_6 > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/6">'.number_format($r->bulan_6,2).'</a>':'0');
			$row[] = ($r->bulan_12 > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/12" target="_blank">'.number_format($r->bulan_12,2).'</a>':'0');
			$row[] = ($r->bulan_24_kecil > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/13" target="_blank">'.number_format($r->bulan_24_kecil,2).'</a>':'0');
			$row[] = ($r->bulan_24_besar > 0 ?'<a href="'.$url.'detail/'.$r->idtipe.'/24" target="_blank">'.number_format($r->bulan_24_besar,2).'</a>':'0');
            $row[] = number_format($r->total_tagihan,2);  
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function detail($idtipe,$bulan) {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		$data=array(
			'idtipe'=>$idtipe,
			'bulan'=>$bulan,			
		);
		$total=$this->model->get_total_piutang($idtipe,$bulan);
		$data['nama_asuransi'] 			=GetTipePasienPiutangBiasa($idtipe);
		$data['total'] 			= $total;
		$data['umur'] 			= $this->get_umur($bulan);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		// $data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		// $data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Umur Piutang Detail';
		$data['content'] 		= 'Tpiutang_tt_umur/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Umur Piutang",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_detail(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		
		$idtipe=$this->input->post('idtipe');
		$bulan=$this->input->post('bulan');
		
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where2='';
		$where1='';
		$where1 .="AND H.idtipe='$idtipe'";			
		if ($no_medrec !=''){
			$where1 .=" AND no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND namapasien LIKE '%".$nama_pasien."%' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(tanggal_kunjungan) >='".YMDFormat($tgl_trx)."' AND DATE(tanggal_kunjungan) <='".YMDFormat($tgl_trx2)."'";
        }
		
		
		if ($bulan ==1){
			$where1 .=" AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30)<=".$bulan.")";
		
		}elseif($bulan<12){
			$where1 .=" AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30)=".$bulan.")";
			
		}elseif($bulan==12){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30)=".$bulan.")";
		}elseif($bulan==13){
			$where1 .="AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) >12 AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) <24";
		}elseif($bulan==24){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) > 24)";
		}
		
       $from = "(SELECT ceil(DATEDIFF(CURRENT_DATE,H.tanggal_jatuh_tempo) / 30) as umur,ceil(DATEDIFF(CURRENT_DATE,H.tanggal_jatuh_tempo)) as umur_hari,H.*,'1' as st_verifikasi_piutang,K.nama as kel_nama
					FROM `tpiutang_tt_verifikasi` H
					LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
					WHERE H.`status`='1' AND H.st_lunas='0' ".$where1."
					
				) as tbl WHERE idtipe IS NOT NULL";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('no_medrec','namapasien','nopendaftaran');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url_rincian        = site_url('tpiutang_tt_monitoring/');
            $url        = site_url('tpiutang_tt_pembayaran/');
            $url_kasir        = site_url('tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $url_klaim       = site_url('tklaim_rincian/');
            $url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
			// if ($r->no_klaim){
				// $no_klaim='<br><br><span class="label label-warning">'.$r->no_klaim.'</span>';			
			// }else{
				// $no_klaim='';				
			// }
            $row[] = $r->idtipe;
            $row[] = $r->pembayaran_id;
            $row[] = $r->pendaftaran_id;
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_kunjungan).'<br>'.$r->nopendaftaran;
            $row[] = GetTipePasienPiutang($r->idtipe);
            // $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec.'<br>'.($r->title?$r->title.'. ':'').$r->namapasien;//7
            $row[] = $r->no_medrec;
            $row[] = $r->kel_nama;//9
            $row[] = number_format($r->tidak_tertagih,2);//10
            $row[] = number_format($r->nominal_tagihan - $r->nominal_bayar - $r->nominal_dihapus,2);//11
            $row[] = number_format($r->nominal_lain,2);//12
            $row[] = HumanDateShort($r->tanggal_jatuh_tempo);//13
			$row[] = ($r->cara_bayar=='1'?text_success('NON CICILAN'):text_danger('CICILAN '.$r->cicilan_ke.' / '.$r->jml_cicilan));			//14	
            $row[] = ($r->umur);//15
			$row[] = (date('Y-m-d') <= $r->tanggal_jatuh_tempo?text_info('BELUM JATUH TEMPO'):text_danger('JATUH TEMPO'));//16
			$row[] = ($r->umur_hari > 0 ?text_danger('LEBIH DARI '.$r->umur_hari.' HARI'):text_info(($r->umur_hari*-1).' HARI JATUH TEMPO'));//17
			// if ($r->st_lunas=='0'){
				// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url.'bayar/'.$r->id.'" target="_blank"  type="button"  title="Add Penghapusan"><i class="fa fa-credit-card"></i></a>';
				
			// }else{
				
			// $aksi .= '<a class="view btn btn-xs btn-default" href="'.$url.'bayar/'.$r->id.'/disabled" target="_blank"  type="button"  title="Add Penghapusan"><i class="fa fa-credit-card"></i></a>';
			// }
			$aksi .= '<a class="view btn btn-xs btn-warning" href="'.$url_rincian.'rincian_tagihan/'.$r->tanggal_jatuh_tempo.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-external-link-square"></i></a>';
			if ($r->idtipe < 3){
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'kwitansi_verifikasi/'.$r->pembayaran_id.'" target="_blank"  type="button"  title="Kwitansi"><i class="fa fa-list-alt "></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
			}else{
				$aksi .= '<a  class="view btn btn-xs btn-success" href="'.$url_ranap.'verifikasi/'.$r->pendaftaran_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Ranap</li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Global (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
							
						</ul>
					</div>';
			}
			// $aksi .= '<button title="Edit  Jatuh Tempo"  class="btn btn-xs btn-danger ganti"><i class="fa fa-calendar"></i></button>';
			
			

			$aksi.='</div>';
			// $aksi .= ' <button title="Hapus piutang"  class="btn btn-xs btn-danger hapus"><i class="fa fa-trash"></i></button>';
            
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function get_umur($bulan){
		if ($bulan <12){
			$bulan=$bulan.' Bulan';
		
		}elseif($bulan==12){
			$bulan='1 Tahun';
		}elseif($bulan==13){
			$bulan='Lebih 1 Tahun';
		}elseif($bulan==24){
			$bulan='Lebih 2 Tahun';
		}
		return $bulan;
	}
}
