<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpiutang_verifikasi extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_verifikasi_model','model');
	}

	function index() {
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		// $date2=(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-1 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"d-m-Y");
		$date2= date_format($date2,"d-m-Y");
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'0',
			'tgl_trx1'=>$date1,
			'tgl_trx2'=>$date2,
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_poli'] 	= $this->model->list_poli();
		$data['list_dokter'] 	= $this->model->list_dokter();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Verifikasi Piutang';
		$data['content'] 		= 'Tpiutang_verifikasi/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Verifikasi Transaksi",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('');
		
		
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$tipe_tagihan=$this->input->post('tipe_tagihan');
		$peg_id=$this->input->post('peg_id');
		$idpoliklinik=$this->input->post('idpoliklinik');
		$iddokter=$this->input->post('iddokter');
		$tgl_trx1=$this->input->post('tgl_trx1');
		$tgl_trx2=$this->input->post('tgl_trx2');
		$tanggaldaftar1=$this->input->post('tanggaldaftar1');
		$tanggaldaftar2=$this->input->post('tanggaldaftar2');
		// tgl_trx2
		$where1='';
		$where2='';
		$where3='';
		$where='';
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
			$where2 .=" AND RI.nopendaftaran='$no_reg' ";
			$where3 .=" AND TD.nopenjualan='$no_reg' ";
		}
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
			$where2 .=" AND RI.no_medrec='$no_medrec' ";
			$where3 .=" AND TD.nomedrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien LIKE '%".$nama_pasien."%' ";
			$where2 .=" AND RI.namapasien LIKE '%".$nama_pasien."%' ";
			$where3 .=" AND TD.nama LIKE '%".$nama_pasien."%' ";
		}
		
		// if ($idkelompokpasien !='#'){
			// $where1 .=" AND TK.tipekontraktor='$idkelompokpasien' ";
			// $where2 .=" AND PD.tipekontraktor='$idkelompokpasien' ";
		// }
		// if ($idrekanan !='#'){
			// $where1 .=" AND TK.idkontraktor='$idrekanan' ";
			// $where2 .=" AND PD.idkontraktor='$idrekanan' ";
		// }
		if ($tipe !='#'){
			$where .=" AND tipe='$tipe' ";
		}
		if ($status !='#'){
			$where1 .=" AND TK.st_verifikasi_piutang='$status' ";
			$where2 .=" AND PD.st_verifikasi_piutang='$status' ";
			$where3 .=" AND TK.st_verifikasi_piutang='$status' ";
		}
		if ($tipe_tagihan !='#'){
			$where .=" AND tipepegawai='$tipe_tagihan' ";
		}
		if ($idpoliklinik !='#'){
			$where .=" AND idpoliklinik='$idpoliklinik' ";
		}
		if ($iddokter !='#'){
			$where .=" AND iddokter_periksa='$iddokter' ";
		}
		if ($peg_id !=''){
			if ($tipe_tagihan=='1'){
				$where .=" AND idpegawai='$peg_id' ";
				// $where2 .=" AND TP.idpegawai='$peg_id' ";
			}elseif ($tipe_tagihan=='2'){
				$where .=" AND iddokter='$peg_id' ";
			}
			
		}
		
		
		 if ('' != $tgl_trx1) {
            $where1 .= " AND DATE(K.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(K.tanggal) <='".YMDFormat($tgl_trx2)."'";
            $where3 .= " AND DATE(K.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(K.tanggal) <='".YMDFormat($tgl_trx2)."'";
            $where2 .= " AND DATE(PDH.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(PDH.tanggal) <='".YMDFormat($tgl_trx2)."'";
        }
		if ('' != $tanggaldaftar1) {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
            $where2 .= " AND DATE(RI.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(RI.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
            $where3 .= " AND DATE(TD.tanggal) >='".YMDFormat($tanggaldaftar1)."' AND DATE(TD.tanggal) <='".YMDFormat($tanggaldaftar2)."'";
        }
        $from = "(
					SELECT TK.id as idpembayaran,'1' as tipe,'RJ' as tipe_nama,TP.id as idpendaftaran
					,TP.tanggaldaftar,K.tanggal as tanggal_kasir,TP.nopendaftaran, TP.idpasien,TP.no_medrec,TP.namapasien
					,TK.nominal,TK.`status`,TK.st_verifikasi_piutang
					,TK.tanggal_jatuh_tempo
					,K.id as idkasir,K.total as tot_trx,TK.tipepegawai,CASE WHEN TK.tipepegawai='1' THEN  MP.nama  ELSE MD.nama END namapegawai,TK.idpegawai,TK.iddokter
					,mpoliklinik.nama as namapoliklinik,TP.idpoliklinik,TP.iddokter as iddokter_periksa,mdokter.nama as namadokter_periksa
					,cek_jatuh_tempo_piutang(TK.tipepegawai,TK.idpegawai,TK.iddokter) as next_date
					,TK.tanggal_id,TK.idpengaturan,tpiutang.status as st_piutang,KD.id as piutang_detail_id
					,S.deskripsi,TK.cara_bayar,TK.jml_cicilan
					from tkasir K
					INNER JOIN tkasir_pembayaran TK  ON TK.idkasir=K.id
					LEFT JOIN tpoliklinik_tindakan TD ON TD.id=K.idtindakan
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.idpendaftaran
					LEFT JOIN mpegawai MP ON MP.id=TK.idpegawai
					LEFT JOIN mdokter MD ON MD.id=TK.iddokter
					LEFT JOIN mpiutang_dokter SD ON SD.iddokter=TK.iddokter AND TK.tipepegawai='2'
					LEFT JOIN mpiutang_pegawai SP ON SP.idpegawai=TK.idpegawai AND TK.tipepegawai='1'
					LEFT JOIN mpoliklinik ON TP.idpoliklinik=mpoliklinik.id
					LEFT JOIN mdokter ON mdokter.id=TP.iddokter
					LEFT JOIN tpiutang_detail KD ON KD.pembayaran_id=TK.id AND KD.tipe='1'
					LEFT JOIN tpiutang ON tpiutang.id=KD.piutang_id
					LEFT JOIN mpiutang_setting S ON S.id=TK.tanggal_id
					WHERE TK.idmetode='5' AND K.`status` !='0' AND K.idtipe IN (1,2) ".$where1."
					
					
					UNION ALL
					
					SELECT TK.id as idpembayaran,K.idtipe as tipe,'Pembelian Obat' as tipe_nama,TD.id as idpendaftaran
					,TD.tanggal as tanggaldaftar,K.tanggal as tanggal_kasir,TD.nopenjualan as nopendaftaran, TD.idpasien,TD.nomedrec as no_medrec,TD.nama as namapasien
					,TK.nominal,TK.`status`,TK.st_verifikasi_piutang
					,TK.tanggal_jatuh_tempo
					,K.id as idkasir,K.total as tot_trx,TK.tipepegawai,CASE WHEN TK.tipepegawai='1' THEN  MP.nama  ELSE MD.nama END namapegawai,TK.idpegawai,TK.iddokter
					,'Obat Bebas' as namapoliklinik,'FARMASI' as idpoliklinik,'0' as iddokter_periksa,'' as namadokter_periksa
					,cek_jatuh_tempo_piutang(TK.tipepegawai,TK.idpegawai,TK.iddokter) as next_date
					,TK.tanggal_id,TK.idpengaturan,tpiutang.status as st_piutang,KD.id as piutang_detail_id
					,S.deskripsi,TK.cara_bayar,TK.jml_cicilan
					from tkasir K
					INNER JOIN tkasir_pembayaran TK  ON TK.idkasir=K.id
					LEFT JOIN tpasien_penjualan TD ON TD.id=K.idtindakan
					LEFT JOIN mpegawai MP ON MP.id=TK.idpegawai
					LEFT JOIN mdokter MD ON MD.id=TK.iddokter
					LEFT JOIN mpiutang_dokter SD ON SD.iddokter=TK.iddokter AND TK.tipepegawai='2'
					LEFT JOIN mpiutang_pegawai SP ON SP.idpegawai=TK.idpegawai AND TK.tipepegawai='1'
					
					LEFT JOIN tpiutang_detail KD ON KD.pembayaran_id=TK.id AND KD.tipe='3'
					LEFT JOIN tpiutang ON tpiutang.id=KD.piutang_id
					LEFT JOIN mpiutang_setting S ON S.id=TK.tanggal_id
					WHERE K.idtipe='3' AND TK.idmetode='5' AND K.`status` !='0' ".$where3."
					GROUP BY K.id
					
					UNION ALL

					SELECT 		PD.id as idpembayaran
										, '2' as tipe,'RI/ODS' as tipe_nama,RI.id as idpendaftaran
					,RI.tanggaldaftar,PDH.tanggal as tanggal_kasir,RI.nopendaftaran,RI.idpasien,RI.no_medrec,RI.namapasien,PD.nominal,RI.`status`
					,PD.st_verifikasi_piutang
					,PD.tanggal_jatuh_tempo
					,'0' as idkasir,PDH.totalharusdibayar as tot_trx
					,CASE WHEN PD.idmetode='5' THEN 1 ELSE 2 END as tipepegawai
					,CASE WHEN PD.idmetode='5' THEN MP.nama ELSE MD.nama END as namapegawai
					,CASE WHEN PD.idmetode='5' THEN PD.idpegawai ELSE '0' END as idpegawai
					,CASE WHEN PD.idmetode='6' THEN PD.idpegawai ELSE '0' END as iddokter					
					,mpoliklinik.nama as namapoliklinik,TP.idpoliklinik,TP.iddokter as iddokter_periksa,mdokter.nama as namadokter_periksa
					,cek_jatuh_tempo_piutang(PD.idmetode,PD.idpegawai,PD.idpegawai) as next_date
					,PD.tanggal_id,PD.idpengaturan,tpiutang.status as st_piutang,KD.id as piutang_detail_id
					,S.deskripsi,PD.cara_bayar,PD.jml_cicilan
					FROM trawatinap_tindakan_pembayaran PDH
					LEFT JOIN trawatinap_tindakan_pembayaran_detail PD ON PDH.id=PD.idtindakan
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=PDH.idtindakan
					LEFT JOIN mpegawai MP ON MP.id=PD.idpegawai	AND PD.idmetode='5'			
					LEFT JOIN mdokter MD ON MD.id=PD.idpegawai	AND PD.idmetode='6'
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=RI.idpoliklinik
					LEFT JOIN mpoliklinik ON TP.idpoliklinik=mpoliklinik.id
					LEFT JOIN mdokter ON mdokter.id=TP.iddokter
					LEFT JOIN tpiutang_detail KD ON KD.pembayaran_id=PD.id AND KD.tipe='2'
					LEFT JOIN tpiutang ON tpiutang.id=KD.piutang_id
					LEFT JOIN mpiutang_setting S ON S.id=PD.tanggal_id
					WHERE PD.idmetode IN ('5','6') AND PDH.statusbatal='0'  ".$where2."
					GROUP BY PDH.id
				) as tbl WHERE tipe is not null ".$where." ORDER BY tanggaldaftar ASC";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_kasir        = site_url('tkasir/');
            $url_farmasi        = site_url('tpasien_penjualan/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->tipe;
            $row[] = $r->idpembayaran;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = HumanDateShort($r->tanggal_kasir);
            $row[] = ($r->tipe=='1' ? '<span class="label label-primary">'.$r->tipe_nama.'</span>':'<span class="label label-success">'.$r->tipe_nama.'</span>');
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = ($r->tipepegawai=='1' ? '<span class="label label-primary">PEGAWAI</span>':'<span class="label label-success">DOKTER</span>');
            $row[] = $r->namapegawai;
			if ($r->piutang_detail_id){	
				if ($r->st_piutang=='2'){			
					$row[] = '<span class="label label-warning">SUDAH DIRPOSES</span>';	
				}else{
					$row[] = '<span class="label label-success">GENERATE TAGIHAN</span>';	
				}					
			}else{
				$row[] = status_piutang($r->st_verifikasi_piutang);
				
			}
            $row[] = number_format($r->nominal,0);
            $row[] =($r->jml_cicilan=='1' ? '<span class="label label-primary">SEKALI BAYAR</span>':'<span class="label label-danger">'.$r->jml_cicilan.' X CICILAN</span>');//cara_bayar
            $row[] = $r->deskripsi;
			if ($r->st_verifikasi_piutang=='0'){
				// if ($r->tanggal_jatuh_tempo==null || ($r->tanggal_jatuh_tempo < date('Y-m-d'))){
					if ($r->next_date){
						$row[] = HumanDateShort($r->next_date);
					}else{
						$row[] = '<span class="label label-danger">BELUM DISETTING</span>';
						$date_setting='0';
					}
									
				// }else{
					// $row[] = HumanDateShort($r->tanggal_jatuh_tempo);					
				// }
			}else{
				$row[] = HumanDateShort($r->tanggal_jatuh_tempo);
			}
			
			
			if($r->st_verifikasi_piutang=='0'){
				$aksi .= '<button title="Verifikasi" '.($date_setting=='0'?"disabled":"").' class="btn btn-xs btn-success verifikasi"><i class="fa fa-check"></i></button>';
			}
			if ($r->st_verifikasi_piutang=='1'){
				if ($r->piutang_detail_id){
						
				}else{
					$aksi .= '<button title="Pindahkan Tanggal"  class="btn btn-xs btn-primary ganti"><i class="si si-login"></i></button>';
				}
				if ($r->st_piutang!='2'){					
					$aksi .= '<button title="Proses Tagihan"  class="btn btn-xs btn-danger cara_tagihan"><i class="si si-arrow-right"></i></button>';					
				}
			}
			if ($r->tipe=='1'){		
				
				$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'kwitansi_verifikasi/'.$r->idpembayaran.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-list-alt "></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			}else{
				if ($r->tipe=='3'){//OB
					$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_farmasi.'edit/'.$r->idpendaftaran.'/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
					$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_farmasi.'print_transaksi/'.$r->idpendaftaran.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
				}
				// $aksi .= '<a disabled class="view btn btn-xs btn-success" href="'.$url_kasir.'transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				// $aksi .= '<a disabled class="view btn btn-xs btn-success" href="'.$url_kasir.'kwitansi_verifikasi/'.$r->idpembayaran.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-list-alt "></i></a>';
				// $aksi .= '<a disabled class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			}
			$tanggal_id='';$idpengaturan='';
			if ($r->st_verifikasi_piutang=='0'){//UPDATE TANGGAL 
				// if ($r->tanggal_jatuh_tempo==null || ($r->tanggal_jatuh_tempo < date('Y-m-d'))){
				if ($r->tanggal_jatuh_tempo != $r->next_date){
					if ($r->tipe=='1' || $r->tipe=='3'){//RAJAL
						if ($r->next_date){
							if ($r->tipepegawai=='1'){
								$q_setting="SELECT tgl.id,tgl.idpengaturan, tgl.tanggal_next FROM mpiutang_pegawai MP
								LEFT JOIN (
									SELECT RS.idpengaturan,RS.id,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
									,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
									from mpiutang_setting RS
									WHERE RS.`status`='1'
								) tgl ON tgl.idpengaturan=MP.idpengaturan
								WHERE MP.idpegawai=".$r->idpegawai."
								ORDER BY tgl.tanggal_next ASC
								limit 1";
							}else{
								$q_setting="SELECT tgl.id,tgl.idpengaturan, tgl.tanggal_next FROM mpiutang_dokter MP
								LEFT JOIN (
									SELECT RS.idpengaturan,RS.id,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
									,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
									from mpiutang_setting RS
									WHERE RS.`status`='1'
								) tgl ON tgl.idpengaturan=MP.idpengaturan
								WHERE MP.iddokter=".$r->iddokter."
								ORDER BY tgl.tanggal_next ASC
								limit 1";
							}
							$row_setting=$this->db->query($q_setting)->row();
							$tanggal_id=$row_setting->id;	
							$idpengaturan=$row_setting->idpengaturan;	
								
							$q="UPDATE tkasir_pembayaran SET tanggal_jatuh_tempo='".YMDFormat($r->next_date)."',tanggal_id='".$row_setting->id."',idpengaturan='".$row_setting->idpengaturan."' WHERE id='".$r->idpembayaran."'";
							$this->db->query($q);
						}
					}else{//RANAP
						if ($r->next_date){
							if ($r->tipepegawai=='1'){
								$q_setting="SELECT tgl.id,tgl.idpengaturan, tgl.tanggal_next FROM mpiutang_pegawai MP
								LEFT JOIN (
									SELECT RS.idpengaturan,RS.id,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
									,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
									from mpiutang_setting RS
									WHERE RS.`status`='1'
								) tgl ON tgl.idpengaturan=MP.idpengaturan
								WHERE MP.idpegawai=".$r->idpegawai."
								ORDER BY tgl.tanggal_next ASC
								limit 1";
							}else{
								$q_setting="SELECT tgl.id,tgl.idpengaturan, tgl.tanggal_next FROM mpiutang_dokter MP
								LEFT JOIN (
									SELECT RS.idpengaturan,RS.id,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
									,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
									from mpiutang_setting RS
									WHERE RS.`status`='1'
								) tgl ON tgl.idpengaturan=MP.idpengaturan
								WHERE MP.iddokter=".$r->iddokter."
								ORDER BY tgl.tanggal_next ASC
								limit 1";
							}
							$row_setting=$this->db->query($q_setting)->row();
							$tanggal_id=$row_setting->id;	
							$idpengaturan=$row_setting->idpengaturan;	
							$q="UPDATE trawatinap_tindakan_pembayaran_detail SET tanggal_jatuh_tempo='".YMDFormat($r->next_date)."',tanggal_id='".$row_setting->id."',idpengaturan='".$row_setting->idpengaturan."' WHERE id='".$r->idpembayaran."'";
							$this->db->query($q);
						}
					}
				}
			}
			
			$aksi.='</div>';
            $row[] = $aksi;//16
            $row[] = ($r->tanggal_id ? $r->tanggal_id : $tanggal_id);//17
            $row[] = ($r->idpengaturan ? $r->idpengaturan : $idpengaturan);//18
            $row[] = $r->tipepegawai;//19
            $row[] = $r->idpegawai;//20
            $row[] = $r->iddokter;//21
            $row[] = $r->piutang_detail_id;//22
            $row[] = $r->nominal;//23
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function cek_date(){
		$tgl='2011-20-01';
		$tgl_asli=(int)substr($tgl,-2);
		print_r($tgl_asli);
	}
	
	public function verifikasi()
    {
		
        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $tanggal_id = $this->input->post('tanggal_id');
        $idpengaturan = $this->input->post('idpengaturan');
        $tanggal_jt =YMDFormat($this->input->post('tanggal_jt'));
		if ($tipe=='1' || $tipe=='3'){//RAJAL
			$nominal=$this->db->query("SELECT nominal FROM  tkasir_pembayaran WHERE id='$id'")->row('nominal');
		}else{
			$nominal=$this->db->query("SELECT nominal FROM  trawatinap_tindakan_pembayaran_detail WHERE id='$id'")->row('nominal');
			// $nominal=$this->db->query('trawatinap_tindakan_pembayaran_detail',$data);
		}
		
		$data=array(
			'nomial_per_bulan'=>$nominal,
			'cara_penagihan'=>'1',
			'cara_bayar'=>'1',
			'jml_cicilan'=>'1',
			'st_verifikasi_piutang'=>'1',
			'tanggal_jatuh_tempo'=>$tanggal_jt,
			'user_verifikasi'=>$this->session->userdata('user_name'),
			'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
			'tanggal_id'=>$tanggal_id,
			'idpengaturan'=>$idpengaturan,
		);
		
		$this->db->where('id',$id);
		if ($tipe=='1' || $tipe=='3'){//RAJAL
			$result=$this->db->update('tkasir_pembayaran',$data);
		}else{
			$result=$this->db->update('trawatinap_tindakan_pembayaran_detail',$data);
		}

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function generate_tagihan()
    {
		
		$where='';
        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $tanggal_id = $this->input->post('tanggal_id');
        $idpengaturan = $this->input->post('idpengaturan');
        $tanggal_jt =YMDFormat($this->input->post('tanggal_jt'));
		$q_ada="SELECT K.id from tpiutang K
			WHERE K.tanggal_id='$tanggal_id'  AND K.tanggal_tagihan='$tanggal_jt' AND status>1 ";
		$klaim_ada=$this->db->query($q_ada)->row('id');
		if ($klaim_ada==null){
				$q="SELECT K.id from tpiutang K
					WHERE K.tanggal_id='$tanggal_id' AND K.tanggal_tagihan='$tanggal_jt' AND status=1 ";
				$piutang_id=$this->db->query($q)->row('id');
				if ($piutang_id==''){
					
					$data_info=array(
						// 'tipe' =>$tipe,
						'tanggal_id' =>$tanggal_id,
						'idpengaturan' =>$idpengaturan,
						'tanggal_tagihan' =>$tanggal_jt,
						// 'batas_kirim' =>$batas_kirim,
						'jatuh_tempo_bayar' =>$tanggal_jt,
						'created_date' =>date('Y-m-d H:i:s'),
						'created_user_id' =>$this->session->userdata('user_id'),
						'created_user' =>$this->session->userdata('user_name'),
						'status' =>'1',
					);
					// print_r($data_info);
					$this->db->insert('tpiutang', $data_info);
					$piutang_id=$this->db->insert_id();
				}
				$data_det=array(
						'piutang_id' =>$piutang_id,
						'tipe' =>$tipe,
						'pembayaran_id' =>$id,
						'user_verifikasi'=>$this->session->userdata('user_name'),
						'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
						'tanggal_tagihan' =>$tanggal_jt,
					);
				$result=$this->db->insert('tpiutang_detail', $data_det);
				$piutang_detail_id=$this->db->insert_id();
				$row_detail=$this->db->query("SELECT *FROM tpiutang_detail WHERE id='$piutang_detail_id'")->row_array();
				$data_bayar=array(
					'piutang_detail_id' =>$piutang_detail_id,
					'tanggal_tagihan' =>$row_detail['tanggal_tagihan'],
					'nomial_tagihan' =>$row_detail['nominal'],
					'nominal_bayar' =>0,
					'status' =>0,
					'tanggal_pembayaran' =>null,
					'cicilan_ke' =>0,
					'created_user'=>$this->session->userdata('user_name'),
					'created_user_id'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$result=$this->db->insert('tpiutang_pembayaran', $data_bayar);
		}else{
			$result=false;
		}

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function verif_semua()
    {
		$arr_id = $this->input->post('arr_id');
		// print_r($arr_id);exit();	
		$arr_tipe = $this->input->post('arr_tipe');
		$arr_tanggal_id = $this->input->post('arr_tanggal_id');
		$arr_idpengaturan = $this->input->post('arr_idpengaturan');
		$arr_tanggal_jt =$this->input->post('arr_tanggal_jt');
		// $result=false;
		foreach ($arr_id as $x => $val){
			$id = $arr_id[$x];
			$tipe = $arr_tipe[$x];
			$tanggal_id = $arr_tanggal_id[$x];
			$idpengaturan = $arr_idpengaturan[$x];
			$tanggal_jt =YMDFormat($arr_tanggal_jt[$x]);
			if ($tipe=='1' || $tipe=='3'){//RAJAL
				$nominal=$this->db->query("SELECT nominal FROM  tkasir_pembayaran WHERE id='$id'")->row('nominal');
			}else{
				$nominal=$this->db->query("SELECT nominal FROM  trawatinap_tindakan_pembayaran_detail WHERE id='$id'")->row('nominal');
				// $nominal=$this->db->query('trawatinap_tindakan_pembayaran_detail',$data);
			}
			$data=array(
				'nomial_per_bulan'=>$nominal,
				'cara_penagihan'=>'1',
				'cara_bayar'=>'1',
				'jml_cicilan'=>'1',
				'st_verifikasi_piutang'=>'1',
				'tanggal_jatuh_tempo'=>$tanggal_jt,
				'user_verifikasi'=>$this->session->userdata('user_name'),
				'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
				'tanggal_id'=>$tanggal_id,
				'idpengaturan'=>$idpengaturan,
			);
			
			$this->db->where('id',$id);
			if ($tipe=='1' || $tipe=='3'){//RAJAL
				$result=$this->db->update('tkasir_pembayaran',$data);
			}else{
				$result=$this->db->update('trawatinap_tindakan_pembayaran_detail',$data);
			}

		}
	 $this->output->set_output(json_encode($result));
		
        
    }
	function js_pegawai_dokter()
	{
		if ($this->input->post('tipe')=='1' || $this->input->post('tipe')=='2'){
				
		$arr = $this->model->js_pegawai_dokter();
		}else{
			$arr='';
		}
		echo json_encode($arr);
	}
	function list_tanggal($tipepegawai='1',$idpeg_dok=''){
		
		if ($tipepegawai=='1'){//FOKUS KE PEGAWAI	
			$q="SELECT DATE_FORMAT(tgl.tanggal_next,'%d-%m-%Y') as tanggal_next  FROM mpiutang_pegawai MP
				LEFT JOIN (
					SELECT RS.idpengaturan,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
					,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
					from mpiutang_setting RS
					WHERE RS.`status`='1'
				) tgl ON tgl.idpengaturan=MP.idpengaturan
				LEFT JOIN tpiutang on tpiutang.tanggal_tagihan=tgl.tanggal_next  AND tpiutang.`status` > 1
				WHERE MP.idpegawai='$idpeg_dok' AND tpiutang.id is null
				ORDER BY tgl.tanggal_next ASC";
		}else{//FOKUS KE DOKTER
			$q="SELECT DATE_FORMAT(tgl.tanggal_next,'%d-%m-%Y') as tanggal_next  FROM mpiutang_dokter MP
				LEFT JOIN (
					SELECT RS.idpengaturan,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
					,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
					from mpiutang_setting RS
					WHERE RS.`status`='1'
				) tgl ON tgl.idpengaturan=MP.idpengaturan
				LEFT JOIN tpiutang on tpiutang.tanggal_tagihan=tgl.tanggal_next  AND tpiutang.`status` > 1
				WHERE MP.iddokter='$idpeg_dok' AND tpiutang.id is null
				ORDER BY tgl.tanggal_next ASC";
		}
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->tanggal_next.'">'.$r->tanggal_next.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function edit_tanggal_satu(){		
		// data: {piutang_id_detail: piutang_id_detail,tanggal_tagihan:tanggal_tagihan,xtipepegawai:xtipepegawai,xidpeg_dok:xidpeg_dok,tipe:tipe},
		
		$id=$this->input->post('piutang_id_detail');
		$tanggal_tagihan=YMDFormat($this->input->post('tanggal_tagihan'));
		$tgl_asli=(int)substr($tanggal_tagihan,-2);
		$xtipepegawai=$this->input->post('xtipepegawai');
		
		$xidpeg_dok=$this->input->post('xidpeg_dok');
		$tipe=$this->input->post('tipe');
		
		
		$hasil_tipe_pegawai=$xtipepegawai;
		
		
		$hasil_idpegawai='0';
		$hasil_iddokter='0';
		$nama_asuransi='';
		if ($xtipepegawai=='1'){
			$nama_asuransi=$this->db->query("SELECT nama from mpegawai WHERE id='$xidpeg_dok'")->row('nama');
		}else{
			$nama_asuransi=$this->db->query("SELECT nama from mdokter WHERE id='$xidpeg_dok'")->row('nama');
		}
		
		if ($tipe=='2'){
			if ($xtipepegawai=='1'){
				$hasil_tipe_pegawai='5';				
			}else{
				$hasil_tipe_pegawai='6';
			}
		}else{
			if ($xtipepegawai=='1'){//pegawai
				$hasil_idpegawai=$xidpeg_dok;
			}else{
				$hasil_iddokter=$xidpeg_dok;
			}
			$hasil_tipe_pegawai=$xtipepegawai;
		}
		if ($xtipepegawai=='1'){
			$q_setting="SELECT tgl.id,tgl.idpengaturan,DATE_FORMAT(tgl.tanggal_next,'%d-%m-%Y') as tanggal_next  FROM mpiutang_pegawai MP
				LEFT JOIN (
					SELECT RS.idpengaturan,RS.id,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
					,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
					from mpiutang_setting RS
					WHERE RS.`status`='1'
				) tgl ON tgl.idpengaturan=MP.idpengaturan
				LEFT JOIN tpiutang on tpiutang.tanggal_tagihan=tgl.tanggal_next  AND tpiutang.`status` > 1
				WHERE MP.idpegawai='$xidpeg_dok' AND tpiutang.id is null AND tgl.tanggal_next='$tanggal_tagihan'
				ORDER BY tgl.tanggal_next ASC";
		}else{
			$q_setting="SELECT tgl.id,tgl.idpengaturan,DATE_FORMAT(tgl.tanggal_next,'%d-%m-%Y') as tanggal_next  FROM mpiutang_dokter MP
				LEFT JOIN (
					SELECT RS.idpengaturan,RS.id,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl 
					,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
					from mpiutang_setting RS
					WHERE RS.`status`='1'
				) tgl ON tgl.idpengaturan=MP.idpengaturan
				LEFT JOIN tpiutang on tpiutang.tanggal_tagihan=tgl.tanggal_next  AND tpiutang.`status` > 1
				WHERE MP.iddokter='$xidpeg_dok' AND tpiutang.id is null AND tgl.tanggal_next='$tanggal_tagihan'
				ORDER BY tgl.tanggal_next ASC";
		}
		$row_setting=$this->db->query($q_setting)->row();
		$idpengaturan=$row_setting->idpengaturan;		
		$tanggal_id=$row_setting->id;		
		if ($tipe=='1' || $tipe=='3'){//RAJAL
			$xnama_asuransi='Pembayaran Karyawan / Dokter :'.$nama_asuransi.' [edited]';
			$result=$this->db->query("UPDATE tkasir_pembayaran set tanggal_id='$tanggal_id',tipepegawai='$hasil_tipe_pegawai',idpegawai='$hasil_idpegawai',iddokter='$hasil_iddokter',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='$id'");
		}else{
			$xnama_asuransi='Perusahaan Karyawan / Dokter :'.$nama_asuransi.' [edited]';
			$result=$this->db->query("UPDATE trawatinap_tindakan_pembayaran_detail set tanggal_id='$tanggal_id',idmetode='$hasil_tipe_pegawai',idpegawai='$xidpeg_dok',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='$id'");
			
		}
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $q="SELECT K.id from tpiutang K
				// WHERE K.tanggal_id='$tanggal_id' AND K.tanggal_tagihan='$tanggal_tagihan' AND status=1 ";
		// $piutang_id=$this->db->query($q)->row('id');
		// $detail_piutang=$this->db->query("SELECT tpiutang_detail.* from tpiutang_detail LEFT JOIN tpiutang ON tpiutang.id=tpiutang_detail.piutang_id where tpiutang_detail.id='$id'")->result();
		
		
		// if ($piutang_id==''){//JIKA TIDAK ADA
				// $data_info=array(
					// // 'tipe' =>$tipe,
					// 'tanggal_id' =>$tanggal_id,
					// 'idpengaturan' =>$idpengaturan,
					// 'tanggal_tagihan' =>$tanggal_tagihan,
					// // 'batas_kirim' =>$batas_kirim,
					// 'jatuh_tempo_bayar' =>$tanggal_tagihan,
					// 'created_date' =>date('Y-m-d H:i:s'),
					// 'created_user_id' =>$this->session->userdata('user_id'),
					// 'created_user' =>$this->session->userdata('user_name'),
					// 'status' =>'1',
				// );
				// // print_r($data_info);
				// $this->db->insert('tpiutang', $data_info);
				// $piutang_id=$this->db->insert_id();
		// }
		// foreach ($detail_piutang as $r){
			// $this->db->query("DELETE FROM tpiutang_detail WHERE id='".$r->id."'");
			
			// if ($r->tipe=='1'){
				// $xnama_asuransi='Pembayaran Karyawan / Dokter :'.$nama_asuransi.' [edited]';
				// $this->db->query("UPDATE tkasir_pembayaran set tipepegawai='$hasil_tipe_pegawai',idpegawai='$hasil_idpegawai',iddokter='$hasil_iddokter',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
			// }else{
				// $xnama_asuransi='Perusahaan Karyawan / Dokter :'.$nama_asuransi.' [edited]';
				// $this->db->query("UPDATE trawatinap_tindakan_pembayaran_detail set idmetode='$hasil_tipe_pegawai',idpegawai='$xidpeg_dok',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
				
			// }
			// //INSERT HISTORY
			// // $data_his=array(
					// // 'tpiutang_id' =>$r->piutang_id,
					// // 'tpiutang_id_detail' =>$r->id,
					// // 'pembayaran_id' =>$r->pembayaran_id,
					// // 'tipe' =>$tipe,
					// // 'idkelompokpasien_asal' =>$r->idkelompokpasien,
					// // 'idrekanan_asal' =>$r->idrekanan,
					// // 'idkelompokpasien' =>$idkelompokpasien,
					// // 'idrekanan' =>$idrekanan,
					// // 'user_nama' =>$this->session->userdata('user_name'),
					// // 'tanggal_pindah' =>date('Y-m-d H:i:s'),
					// // 'ket' =>'Pindah Satuan',
				// // );
				// // $this->db->insert('tpiutang_history', $data_his);
			
			// $data_det=array(
					// 'piutang_id' =>$piutang_id,
					// 'tipe' =>$tipe,
					// 'pembayaran_id' =>$r->pembayaran_id,
					// 'user_verifikasi'=>$this->session->userdata('user_name'),
					// 'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					// 'tanggal_tagihan' =>$tanggal_tagihan,
				// );
			// $result=$this->db->insert('tpiutang_detail', $data_det);
		// }
	}
	function get_data_kirim($id,$tipe){
	if ($tipe=='1' || $tipe=='3'){//RJ
			$arr['detail']=$this->db->query("SELECT *FROM tkasir_pembayaran WHERE id='$id'")->row_array();
		}else{
			$arr['detail']=$this->db->query("SELECT *FROM trawatinap_tindakan_pembayaran_detail WHERE id='$id'")->row_array();
		}
		
		$this->output->set_output(json_encode($arr));
	}
	function simulasi(){
		
		$idpengaturan=$this->input->post('idpengaturan');
		$jml_cicilan=$this->input->post('jml');
		$cara_penagihan=$this->input->post('cara');
		$tgl_start=YMDFormat($this->input->post('tgl'));
		
		// $idpengaturan='1';
		// $jml_cicilan='2';
		// $cara_penagihan='2';
		// $tgl_start='2021-03-23';
		
		$opsi='';
		$tgl_hari_ini=date('Y-m-d');
		$tgl_hari_ini=date_create($tgl_hari_ini);
		
		// print_r('hasil');exit();
		if ($cara_penagihan=='1'){
			for ($i=0;$i<$jml_cicilan;$i++){				
				$date=date_create($tgl_start);
				date_add($date,date_interval_create_from_date_string("$i month"));
				$tgl_tagihan = date_format($date,"d-m-Y");
				$opsi .='<option value="'. $tgl_tagihan.'" selected>'.'Ke-'.($i+1).' : '.$tgl_tagihan.'</option>';
			}
		}else{
			$q="SELECT S.tanggal_hari from mpiutang_setting S
				WHERE S.idpengaturan='$idpengaturan' AND S.`status`='1'
				ORDER BY S.tanggal_hari ASC";
			$rows= $this->db->query($q)->result();
			// print_r($rows);exit();
			$tahun=date('Y');
			$bulan=date('m');
			$x=0;
			if (count($rows)>0){
				for ($i=0;$i<=$jml_cicilan;$i++){
					foreach($rows as $row){
						$str_tgl=$tahun.'-'.sprintf("%02s", $bulan).'-'.sprintf("%02s", $row->tanggal_hari);
						$tgl_tagihan=date_create($str_tgl);
						if ($tgl_tagihan>=$tgl_hari_ini){		
							if ($x<$jml_cicilan){
							$opsi .='<option value="'.date_format($tgl_tagihan,"d-m-Y").'" selected>'.'Ke-'.($x+1).' : '.date_format($tgl_tagihan,"d-m-Y").'</option>';
							$x=$x+1;
							}
						}
						
					}
					$i=$x;
					if ($bulan==12){
						$bulan=1;
						$tahun=$tahun+1;
					}else{
						$bulan=(int)$bulan+1;
					
					}		
				}
			}			
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
		// print_r($arr);exit();
	}
	function save_cicilan(){
		$idpembayaran=$this->input->post('id_detail');
		$idpengaturan=$this->input->post('idpengaturan');
		$tanggal_id=$this->input->post('tanggal_id');
		$tipe=$this->input->post('tipe');
		$cara_penagihan=$this->input->post('cara_penagihan');
		$cara_bayar=$this->input->post('cara_bayar');
		$jml_cicilan=RemoveComma($this->input->post('jml'));
		$nomial_per_bulan=RemoveComma($this->input->post('xperbulan'));
		// $xperbulan=RemoveComma($this->input->post('xperbulan'));
		$xsetiap_bulan=$this->input->post('xsetiap_bulan');
		$data=array(			
			'nomial_per_bulan'=>$nomial_per_bulan,
			'jml_cicilan'=>$jml_cicilan,
			'cara_penagihan'=>$cara_penagihan,
			'cara_bayar'=>$cara_bayar,
			'tanggal_id'=>$tanggal_id,
			'idpengaturan'=>$idpengaturan,
		);
		$this->db->where('id',$idpembayaran);
		$this->db->query("DELETE FROM tpiutang_detail WHERE pembayaran_id='$idpembayaran' AND tipe='$tipe'");
		if ($tipe=='1' || $tipe=='3'){//RAJAL
			$result=$this->db->update('tkasir_pembayaran',$data);
		}else{
			$result=$this->db->update('trawatinap_tindakan_pembayaran_detail',$data);
		}
		
		$tgl_start=YMDFormat($this->input->post('tgl'));
		
		
		// $tgl_tagihan=implode(",", $xsetiap_bulan);
		$tgl_tagihan='';
		$tagihan_ke=1;
		foreach ($xsetiap_bulan as $val){
				$tanggal_jt =YMDFormat($val);
				$q_ada="SELECT K.id from tpiutang K
					WHERE K.tanggal_id='$tanggal_id'  AND K.tanggal_tagihan='$tanggal_jt' AND status>1 ";
				$klaim_ada=$this->db->query($q_ada)->row('id');
				if ($klaim_ada==null){
						$q="SELECT K.id from tpiutang K
							WHERE K.tanggal_id='$tanggal_id' AND K.tanggal_tagihan='$tanggal_jt' AND status=1 ";
						$piutang_id=$this->db->query($q)->row('id');
						if ($piutang_id==''){
							
							$data_info=array(
								'tanggal_id' =>$tanggal_id,
								'idpengaturan' =>$idpengaturan,
								'tanggal_tagihan' =>$tanggal_jt,
								'jatuh_tempo_bayar' =>$tanggal_jt,
								'created_date' =>date('Y-m-d H:i:s'),
								'created_user_id' =>$this->session->userdata('user_id'),
								'created_user' =>$this->session->userdata('user_name'),
								'status' =>'1',
							);
							// print_r($data_info);
							$this->db->insert('tpiutang', $data_info);
							$piutang_id=$this->db->insert_id();
						}
						$data_det=array(
								'piutang_id' =>$piutang_id,
								'tipe' =>$tipe,
								'pembayaran_id' =>$idpembayaran,
								'nominal' =>$nomial_per_bulan,
								'cicilan_ke' =>$tagihan_ke,
								'jml_cicilan' =>$jml_cicilan,
								'cara_bayar' =>$cara_bayar,
								'cara_penagihan' =>$cara_penagihan,
								'user_verifikasi'=>$this->session->userdata('user_name'),
								'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
								'tanggal_tagihan' =>$tanggal_jt,
							);
						$result=$this->db->insert('tpiutang_detail', $data_det);
						$tagihan_ke=$tagihan_ke+1;
				}else{
					$result=false;
				}
		}
		
		$this->output->set_output(json_encode($result));
	}
	function generate_semua(){
		
		$arr_id = $this->input->post('arr_id');
		$arr_tipe = $this->input->post('arr_tipe');
		
		$result=false;
		foreach ($arr_id as $x => $val){
			$id = $arr_id[$x];
			$tipe = $arr_tipe[$x];
			if ($tipe=='1' || $tipe=='3'){
				$q_row=$this->db->query("SELECT *FROM tkasir_pembayaran WHERE id='$id'")->row();				
			}else{
				$q_row=$this->db->query("SELECT *FROM trawatinap_tindakan_pembayaran_detail WHERE id='$id'")->row();
			}
			$this->db->query("DELETE FROM tpiutang_detail WHERE pembayaran_id='$id' AND tipe='$tipe'");
			$tanggal_jt =$q_row->tanggal_jatuh_tempo;
			$tanggal_id =$q_row->tanggal_id;
			$idpengaturan =$q_row->idpengaturan;
			$nominal =$q_row->nominal;
			$q_ada="SELECT K.id from tpiutang K
				WHERE K.tanggal_id='$tanggal_id'  AND K.tanggal_tagihan='$tanggal_jt' AND status>1 ";
			$klaim_ada=$this->db->query($q_ada)->row('id');
			if ($klaim_ada==null){
					$q="SELECT K.id from tpiutang K
						WHERE K.tanggal_id='$tanggal_id' AND K.tanggal_tagihan='$tanggal_jt' AND status=1 ";
					$piutang_id=$this->db->query($q)->row('id');
					if ($piutang_id==''){
						
						$data_info=array(
							'tanggal_id' =>$tanggal_id,
							'idpengaturan' =>$idpengaturan,
							'tanggal_tagihan' =>$tanggal_jt,
							'jatuh_tempo_bayar' =>$tanggal_jt,
							'created_date' =>date('Y-m-d H:i:s'),
							'created_user_id' =>$this->session->userdata('user_id'),
							'created_user' =>$this->session->userdata('user_name'),
							'status' =>'1',
						);
						// print_r($data_info);
						$this->db->insert('tpiutang', $data_info);
						$piutang_id=$this->db->insert_id();
					}
					$data_det=array(
							'piutang_id' =>$piutang_id,
							'tipe' =>$tipe,
							'pembayaran_id' =>$id,
							'nominal' =>$nominal,
							'user_verifikasi'=>$this->session->userdata('user_name'),
							'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
							'tanggal_tagihan' =>$tanggal_jt,
						);
					$result=$this->db->insert('tpiutang_detail', $data_det);
					
			}else{
				$result=false;
			}
			
		}
		$this->output->set_output(json_encode($result));
	}
}
