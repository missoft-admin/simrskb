<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_monitoring extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_monitoring_model');
		$this->load->model('Antrian_layanan_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1552'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Pengaturan Monitoring Display Web';
			$data['content'] 		= 'Antrian_monitoring/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pengaturan Monitoring Display Web",'#'),
												  array("List",'antrian_monitoring')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama_monitoring' 					=> '',
			'tipe_antrian' 					=> '',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Antrian_monitoring_model->list_layanan('');
		// $data['list_detail'] 			= $this->Antrian_layanan_model->list_detail();
		// $data['list_user'] 			= $this->Antrian_monitoring_model->list_user('');
		$data['title'] 			= 'Tambah Pengaturan Monitoring Display Web';
		$data['content'] 		= 'Antrian_monitoring/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Pengaturan Monitoring Display Web",'#'),
								            array("Tambah",'antrian_monitoring')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Antrian_monitoring_model->getSpecified($id);
			$data['list_pelayanan'] 			= $this->Antrian_monitoring_model->list_pelayanan();
			$data['list_tujuan'] 			= $this->Antrian_monitoring_model->list_tujuan();
			
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Monitoring Display Web';
			$data['content']    = 'Antrian_monitoring/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Pengaturan Monitoring Display Web",'#'),
										array("Ubah",'antrian_monitoring')
										);

			// $data['statusAvailableApoteker'] = $this->Antrian_monitoring_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('antrian_monitoring');
		}
	}

	function delete($id){
		
		$result=$this->Antrian_monitoring_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('antrian_monitoring','location');
	}
	function aktifkan($id){
		
		$result=$this->Antrian_monitoring_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if($this->input->post('id') == '' ) {
			if($this->Antrian_monitoring_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('antrian_monitoring/create','location');
			}
		} else {
			if($this->Antrian_monitoring_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('antrian_monitoring/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Antrian_monitoring/manage';

		if($id==''){
			$data['title'] = 'Tambah Pengaturan Monitoring Display Web';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pengaturan Monitoring Display Web",'#'),
							               array("Tambah",'antrian_monitoring')
								           );
		}else{
			$data['title'] = 'Ubah Pengaturan Monitoring Display Web';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pengaturan Monitoring Display Web",'#'),
							               array("Ubah",'antrian_monitoring')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						FROM antrian_monitoring H 
						
						GROUP BY H.id
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_monitoring');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama_monitoring;

          $result[] = ($r->status?text_primary('AKTIF'):text_danger('TIDAK AKTIF'));
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1554'))){
				$aksi .= '<a href="'.site_url().'antrian_monitoring/update/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-primary btn-xs"><i class="si si si-settings"></i></a>';
				}
				// if (UserAccesForm($user_acces_form,array('1555'))){
				// $aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				// }
			}else{
				if (UserAccesForm($user_acces_form,array('1556'))){
				$aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
				}
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_detail(){
		$antrian_id=$this->input->post('antrian_id');
		$tipe_antrian=$this->input->post('tipe_antrian');
		$iddetail=$this->input->post('iddetail');
		$detail_id=$this->input->post('detail_id');
		$nourut=$this->input->post('nourut');
		$data['antrian_id']=$antrian_id;
		$data['tipe_antrian']=$tipe_antrian;
		$data['detail_id']=$detail_id;
		$data['nourut']=$nourut;
		if ($iddetail==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('antrian_monitoring_detail',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$iddetail);
			$hasil=$this->db->update('antrian_monitoring_detail',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function load_detail(){
		$antrian_id=$this->input->post('antrian_id');
		$tipe_antrian=$this->input->post('tipe_antrian');
		$this->select = array();
	  $from="(
			SELECT H.id,H.detail_id,H.nourut,CASE WHEN H.tipe_antrian='1' THEN MP.nama_pelayanan ELSE MT.nama_tujuan END as nama FROM `antrian_monitoring_detail` H
			LEFT JOIN antrian_pelayanan MP ON MP.id=H.detail_id AND H.tipe_antrian='1'
			LEFT JOIN mtujuan MT ON MT.id=H.detail_id AND H.tipe_antrian='2'
			WHERE H.antrian_id='$antrian_id'
			ORDER BY H.nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->nama;

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_detail('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		$aksi .= '<button onclick="hapus_detail('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_detail(){
		$id=$this->input->post('id');
		$q="select *FROM antrian_monitoring_detail H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function load_data_detail(){
		$antrian_id=$this->input->post('antrian_id');
		$q="SELECT GROUP_CONCAT(A.file_detail SEPARATOR ':') file
			FROM `antrian_monitoring_detail` H
			LEFT JOIN antrian_asset_detail A ON A.id=H.detail_id
			WHERE H.antrian_id='$antrian_id' AND H.status='1'
			ORDER BY H.nourut ASC";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function hapus_detail(){
		$id=$this->input->post('id');
		
		$this->db->where('id',$id);
		$hasil=$this->db->delete('antrian_monitoring_detail');
		json_encode($hasil);
	}
}
