<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mdokter_jadwal extends CI_Controller
{
    /**
     * Jadwal Dokter IGD controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mdokter_jadwal_model');
        $this->load->helper('path');
    }

    public function index()
    {
        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];

        if (UserAccesForm($user_acces_form, array('16'))) {
            $data = array();
            $data['error']      = '';
            $data['title']      = 'Jadwal Dokter IGD';
            $data['content']    = 'Mdokter_jadwal/index';
            $data['breadcrum']  = array(
              array("RSKB Halmahera",'#'),
              array("Jadwal Dokter IGD",'#'),
              array("List",'mdokter_jadwal')
            );

            $data['status_jadwal']  = $this->Mdokter_jadwal_model->checkStatusJadwal();

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            redirect('page404');
        }
    }

    public function add()
    {
        $data['error']      = '';
        $data['title']      = 'Tambah Jadwal Dokter IGD';
        $data['content']    = 'Mdokter_jadwal/manage';
        $data['breadcrum']  = array(
          array("RSKB Halmahera",'#'),
          array("Jadwal Dokter IGD",'#'),
          array("Tambah",'mdokter_jadwal')
        );

        $data['periode']     = date("Y-m");
        $data['list_dokter'] = $this->Mdokter_jadwal_model->getAllDokterUmum();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($periode)
    {
        if ($periode != '') {
            $data['error']      = '';
            $data['title']      = 'Ubah Jadwal Dokter IGD';
            $data['content']    = 'Mdokter_jadwal/manage';
            $data['breadcrum']  = array(
                array("RSKB Halmahera",'#'),
                array("Jadwal Dokter IGD",'#'),
                array("Ubah",'mdokter_jadwal')
            );

            $data['periode']     = $periode;
            $data['list_dokter'] = $this->Mdokter_jadwal_model->getAllDokterUmum();
            $data['list_jadwal'] = $this->Mdokter_jadwal_model->getListJadwal($periode);

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mdokter_jadwal');
        }
    }

    public function save()
    {
        if ($this->Mdokter_jadwal_model->save()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mdokter_jadwal', 'location');
        }
    }

    public function getIndex()
    {
        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];

        $this->select = array();
        $this->from   = 'mdokter_jadwal';
        $this->join 	= array();
        $this->where  = array();
        $this->order  = array(
          'periode' => 'DESC'
        );
        $this->group  = array('periode');

        $this->column_search  = array('DATE_FORMAT(CONCAT(periode,"-01"),"%M %Y")');
        $this->column_order   = array('DATE_FORMAT(CONCAT(periode,"-01"),"%M %Y")');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row) {
            $no++;
            $result = array();

            $tanggalbuat = date('Y-m-01', strtotime($row->periode));
            $tanggalakhir = date('Y-m-05', strtotime("+1 month", strtotime($row->periode)));
            $action = '';
            if ($tanggalakhir > date("Y-m-d")) {
                if (UserAccesForm($user_acces_form, array('17'))) {
                    $action .= '<a href="'.site_url().'mdokter_jadwal/update/'.$row->periode.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
                }
            } else {
                $action .= '<a href="#" data-toggle="tooltip" title="Terkunci" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>';
            }

            $result[] = $no;
            $result[] = GetMonthIndo($row->periode);
            $result[] = '<div class="btn-group">'.$action.'</div>';

            $data[] = $result;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
