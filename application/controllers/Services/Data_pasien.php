<?php

class Data_pasien extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Data_pasien_model', 'data_pasien_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }

    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function title_pasien(): void
    {
        $data = $this->data_pasien_model->title_pasien();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function search_pasien(): void
    {
        $input = json_decode(file_get_contents('php://input'));
        
        $nomedrec = strtoupper($input->no_medrec);
        $tanggalLahir = strtoupper($input->tanggal_lahir);

        $data = $this->data_pasien_model->search_pasien($nomedrec, $tanggalLahir);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
}
