<?php

class Setting_app extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Setting_app_model', 'setting_app_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }


    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function get_data_gallery(): void
    {
        $data = $this->setting_app_model->get_data_gallery();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_data_event(): void
    {
        $data = $this->setting_app_model->get_data_event();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_data_event_detail($event_id): void
    {
        $data = $this->setting_app_model->get_data_event_detail($event_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_data_informasi(): void
    {
        $data = $this->setting_app_model->get_data_informasi();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_data_informasi_detail($infromasi_id): void
    {
        $data = $this->setting_app_model->get_data_informasi_detail($infromasi_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_app(): void
    {
        $data = $this->setting_app_model->get_setting_app();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rajal(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rajal();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rajal_pembayaran(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rajal_pembayaran();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rajal_asuransi(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rajal_asuransi();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rajal_poliklinik(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rajal_poliklinik();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rehab(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rehab();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rehab_pembayaran(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rehab_pembayaran();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rehab_asuransi(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rehab_asuransi();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_pendaftaran_rehab_poliklinik(): void
    {
        $data = $this->setting_app_model->get_setting_pendaftaran_rehab_poliklinik();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();

        exit;
    }
}
