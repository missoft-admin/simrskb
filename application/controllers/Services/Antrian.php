<?php

class Antrian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Antrian_model', 'antrian_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }

    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function get_nomor_antrian(): void
    {
        $q = "UPDATE antrian_pelayanan INNER JOIN (
            SELECT D.antrian_id,H.nama_pelayanan,D.jam_buka,D.jam_tutup
            ,CASE WHEN D.jam_buka <=CURRENT_TIME THEN 1 ELSE 0 END st_open
            ,0 open_by,'BUKA OTOMATIS' as open_nama,NOW() as open_date
            FROM date_row 
            INNER JOIN antrian_pelayanan_hari D ON D.kodehari=date_row.hari
            INNER JOIN antrian_pelayanan H ON H.id=D.antrian_id AND H.st_open='0' 
            WHERE date_row.tanggal=CURRENT_DATE() AND D.st_24_jam='0' AND  D.jam_buka <=CURRENT_TIME AND D.jam_tutup>=CURRENT_TIME AND H.st_open='0' AND D.auto_open='1'
                        
            ) T ON T.antrian_id=antrian_pelayanan.id
			SET antrian_pelayanan.st_open=1,antrian_pelayanan.open_by=T.open_by,antrian_pelayanan.open_date=T.open_date,antrian_pelayanan.open_nama=T.open_nama";
		$this->db->query($q);
		
	    $q = "UPDATE antrian_pelayanan INNER JOIN (
			SELECT D.antrian_id,H.nama_pelayanan,D.jam_buka,D.jam_tutup
			,CASE WHEN D.jam_tutup <=CURRENT_TIME THEN 1 ELSE 0 END st_tutup
			,0 tutup_by,'TUTUP OTOMATIS' as tutup_nama,NOW() as tutup_date
			FROM date_row 
			INNER JOIN antrian_pelayanan_hari D ON D.kodehari=date_row.hari
			INNER JOIN antrian_pelayanan H ON H.id=D.antrian_id AND H.st_open='1' 
			WHERE date_row.tanggal=CURRENT_DATE() AND D.st_24_jam='0' AND D.jam_tutup <=CURRENT_TIME
			) T ON T.antrian_id=antrian_pelayanan.id AND T.st_tutup='1' AND antrian_pelayanan.st_open='1'
			SET antrian_pelayanan.st_open=0,antrian_pelayanan.tutup_by=T.tutup_by,antrian_pelayanan.tutup_date=T.tutup_date,antrian_pelayanan.tutup_nama=T.tutup_nama";
		$this->db->query($q);
		
		$q = "SELECT D.antrian_id,AP.nama_pelayanan,AP.kode,AP.kuota
			,SUM(CASE WHEN AH.st_panggil='0' THEN 1 ELSE 0 END) as sisa_antrian
			,SUM(CASE WHEN AH.st_panggil IS NOT NULL THEN 1 ELSE 0 END) as total_antrian
			,COALESCE(MAX(AH.noantrian),0) +1 as antrian_ahir,
			CONCAT(AP.kode, LPAD(COALESCE(MAX(AH.noantrian), 0) + 1, 3, '0')) AS kode_antrian_akhir
			FROM date_row H
			INNER JOIN antrian_pelayanan_hari D ON D.kodehari=H.hari AND D.status=1 
			INNER JOIN antrian_pelayanan AP ON AP.id=D.antrian_id AND AP.st_open='1'
			LEFT JOIN antrian_harian AH ON AH.antrian_id=AP.id AND AH.tanggal=H.tanggal
			WHERE H.tanggal=CURRENT_DATE()
			GROUP BY AP.id
			ORDER BY AP.urutan";
			
		$data = $this->db->query($q)->result();
        
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    function get_print_nomor_antrian($antrianId){
	    $q = "INSERT INTO antrian_harian (antrian_id,kodehari,namahari,kode,tanggal,waktu_ambil,st_panggil)
            SELECT '$antrianId' as antrian_id,MH.kodehari,MH.namahari,AP.kode,DR.tanggal,NOW() as waktu_ambil,0 as st_panggil FROM `date_row` DR
            LEFT JOIN merm_hari MH ON MH.kodehari=DR.hari
            LEFT JOIN antrian_pelayanan AP ON AP.id='$antrianId'
            WHERE DR.tanggal=CURRENT_DATE()";
		
        $data = $this->db->query($q);

        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
    
    public function get_antrian_seluruh_dokter(): void
    {
        $data = $this->antrian_model->get_antrian_seluruh_dokter();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_antrian_penunjang(): void
    {
        $data = $this->antrian_model->get_antrian_penunjang();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_antrian_berdasarkan_dokter(): void
    {
        $data = $this->antrian_model->get_antrian_berdasarkan_dokter();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
    
    public function get_antrian_pendaftaran(): void
    {
        $data = $this->antrian_model->get_antrian_pendaftaran();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
}
