<?php

class Apm extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Apm_model', 'apm_model');
        $this->load->model('Services/General_model', 'general_model');
        $this->load->model('Tbooking_rehab_model', 'tbooking_rehab_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }

    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function search_booking_code($booking_code): void
    {
        $data = $this->apm_model->search_booking_code($booking_code);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_informasi_pasien($pasien_id): void
    {
        $data = $this->apm_model->get_pasien($pasien_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_informasi_pendaftaran($pendaftaran_id): void
    {
        $data = $this->apm_model->get_informasi_pendaftaran($pendaftaran_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'id' => $data->id,
                    'no_medrec' => $data->no_medrec,
                    'nama_pasien' => $data->title_pasien.'. '.$data->nama_pasien,
                    'tanggal_lahir' => $data->tanggal_lahir,
                    'tujuan_poliklinik' => $data->tujuan_poliklinik,
                    'foto_dokter' => $data->foto_dokter,
                    'nama_dokter' => $data->nama_dokter,
                    'kategori_dokter' => $data->kategori_dokter,
                    'jenis_pembayaran' => '5' != $data->idkelompokpasien ? 'Pembayaran Asuransi' : 'Pembayaran',
                    'nama_asuransi' => '1' == $data->idkelompokpasien ? $data->nama_asuransi : $data->nama_kelompok_pasien,
                    'nomor_antrian' => 'Antrian Ke '.$data->nomor_antrian.' ('.kekata($data->nomor_antrian).' )',
                    'nomor_antrian_pendaftaran' => 'Antrian Ke '.$data->nomor_antrian_pendaftaran.' ('.kekata($data->nomor_antrian_pendaftaran).' )',
                    'nomor_pendaftaran' => $data->nomor_pendaftaran,
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_pendaftaran_poli(): void
    {
        $pasien_id = $this->input->post('pasien_id');
        $pasien = $this->apm_model->get_pasien($pasien_id);

        $dataPasien = [
            'statuspasienbaru' => 0,
            'idpasien' => $pasien->id,
            'no_medrec' => $pasien->no_medrec,
            'title' => $pasien->title,
            'namapasien' => $pasien->nama,
            'jenis_kelamin' => $pasien->jenis_kelamin,
            'tempat_lahir' => $pasien->tempat_lahir,
            'tanggal_lahir' => $pasien->tanggal_lahir,
            'golongan_darah' => $pasien->golongan_darah,
            'agama_id' => $pasien->agama_id,
            'warganegara' => $pasien->warganegara,
            'suku_id' => $pasien->suku,
            'statuskawin' => $pasien->status_kawin,
            'alamatpasien' => $pasien->alamat_jalan,
            'provinsi_id' => $pasien->provinsi_id,
            'kabupaten_id' => $pasien->kabupaten_id,
            'kecamatan_id' => $pasien->kecamatan_id,
            'kelurahan_id' => $pasien->kelurahan_id,
            'kodepos' => $pasien->kodepos,
            'telepon' => $pasien->telepon,
            'nohp' => $pasien->hp,
            'email' => $pasien->email,
            'pendidikan' => $pasien->pendidikan_id,
            'pekerjaan' => $pasien->pekerjaan_id,
            'namapenanggungjawab' => $pasien->nama_keluarga,
            'hubungan' => $pasien->hubungan_dengan_pasien,
            'teleponpenanggungjawab' => $pasien->telepon_keluarga,
            'noidentitaspenanggungjawab' => $pasien->ktp_keluarga,
            'alamatpenanggungjawab' => $pasien->alamat_keluarga,
        ];

        $dataRegistrasi = [
            'idtipe' => 1,
            'tanggaldaftar' => date('Y-m-d H:i:s'),
            'idpoliklinik' => $this->input->post('poliklinik_id'),
            'iddokter' => $this->input->post('dokter_id'),
            'tanggal' => $this->input->post('tanggal_janji_temu'),
            'jadwal_id' => $this->input->post('waktu_janji_temu'),
            'pertemuan_id' => $this->input->post('jenis_pertemuan'),
            'idkelompokpasien' => $this->input->post('cara_pembayaran'),
            'idrekanan' => ('1' == $this->input->post('cara_pembayaran') ? $this->input->post('rekanan_id') : null),
            'foto_kartu' => $this->upload_foto_kartu(),
            'st_gc' => $this->input->post('status_general_consent'),
            'st_general' => ('1' == $this->input->post('status_general_consent') ? 0 : 1),
            'st_sp' => $this->input->post('status_skrining_pasien'),
            'st_skrining' => ('1' == $this->input->post('status_skrining_pasien') ? 0 : 1),
            'st_sc' => $this->input->post('status_skrining_covid'),
            'st_covid' => ('1' == $this->input->post('status_skrining_covid') ? 0 : 1),
            'created_date' => date('Y-m-d H:i:s'),
            'idasalpasien' => 1,
            'idtipepasien' => 1,
            'reservasi_tipe_id' => 1,
            'reservasi_cara' => 0,
            'status_reservasi' => 3,
        ];

        $data = $this->apm_model->post_pendaftaran_poli(array_merge($dataPasien, $dataRegistrasi));
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'pendaftaran_id' => $data,
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function upload_foto_kartu()
    {
        if (isset($_FILES['foto_kartu'])) {
            $config['upload_path'] = './assets/upload/foto_kartu';
            $config['allowed_types'] = 'jpg|jpeg|png|bmp';
            $config['encrypt_name'] = true;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('foto_kartu')) {
                $error = $this->upload->display_errors();

                return null;
            }

            $uploadData = $this->upload->data();

            return $uploadData['file_name'];
        }

        return null;
    }

    public function get_general_consent($pendaftaran_id): void
    {
        $data = $this->apm_model->get_general_consent($pendaftaran_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'information' => $data['head'],
                    'form' => $data['detail'],
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_skrining_pasien($pendaftaran_id): void
    {
        $data = $this->apm_model->get_skrining_pasien($pendaftaran_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_skrining_covid($pendaftaran_id): void
    {
        $data = $this->apm_model->get_skrining_covid($pendaftaran_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function check_if_registered($pasien_id, $poliklinik_id, $dokter_id, $tanggal, $jadwal_id): void
    {
        $data = $this->apm_model->check_if_registered($pasien_id, $poliklinik_id, $dokter_id, $tanggal, $jadwal_id);
        
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'registered',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'not_register',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_form_general_consent(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->apm_model->post_form_general_consent($input->gc_id, $input->jawaban);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_general_consent(): void
    {
        $dataGC = [
            'pendaftaran_id' => $this->input->post('pendaftaran_id'),
            'persetujuan' => $this->input->post('persetujuan'),
            'pasien_dapat_menandatangani' => $this->input->post('pasien_dapat_menandatangani'),
            'siapa_yang_menandatangani' => $this->input->post('siapa_yang_menandatangani'),
            'nama' => $this->input->post('nama'),
            'hubungan' => $this->input->post('hubungan'),
            'tanda_tangan' => $this->input->post('tanda_tangan'),
        ];

        $data = $this->apm_model->post_general_consent($dataGC);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_form_skrining_pasien(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->apm_model->post_form_skrining_pasien($input->sp_id, $input->jenis_isi, $input->jawaban);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_skrining_pasien(): void
    {
        $dataSP = [
            'pendaftaran_id' => $this->input->post('pendaftaran_id'),
        ];

        $data = $this->apm_model->post_skrining_pasien($dataSP);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_form_skrining_covid(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->apm_model->post_form_skrining_covid($input->sc_id, $input->jenis_isi, $input->jawaban);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_skrining_covid(): void
    {
        $dataSC = [
            'pendaftaran_id' => $this->input->post('pendaftaran_id'),
        ];

        $data = $this->apm_model->post_skrining_covid($dataSC);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();

        exit;
    }

    public function post_checkin(): void
    {
        $dataCheckin = [
            'reservasi_id' => $this->input->post('reservasi_id'),
        ];

        $data = $this->apm_model->post_checkin($dataCheckin);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();

        exit;
    }
}
