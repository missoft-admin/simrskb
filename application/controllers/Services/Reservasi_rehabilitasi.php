<?php

require 'vendor/autoload.php';

use Hashids\Hashids;

class Reservasi_rehabilitasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Reservasi_rehabilitasi_model', 'reservasi_rehabilitasi_model');
        $this->load->model('Tbooking_rehab_model', 'tbooking_rehab_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }

    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function get_jadwal_rehab($idpoli): void
    {
        $data = $this->reservasi_rehabilitasi_model->get_jadwal_rehab($idpoli);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_jam_rehab($idpoli, $tanggal): void
    {
        $data = $this->reservasi_rehabilitasi_model->get_jam_rehab($idpoli, $tanggal);
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_logic_pendaftaran_rehab($jenis_pertemuan_id, $idpoliklinik, $iddokter): void
    {
        $st_gc = '0';
        $st_sp = '0';
        $st_sc = '0';

        $hasil = get_logic_pendaftaran_rehab($jenis_pertemuan_id, $idpoliklinik, $iddokter);
        if ($hasil) {
            $st_gc = $hasil->st_gc;
            $st_sp = $hasil->st_sp;
            $st_sc = $hasil->st_sc;
        }

        $data['st_gc'] = $st_gc;
        $data['st_sp'] = $st_sp;
        $data['st_sc'] = $st_sc;

        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_informasi_reservasi($reservasi_id): void
    {
        $data = $this->reservasi_rehabilitasi_model->get_informasi_reservasi($reservasi_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'id' => $data->id,
                    'tanggal_reservasi' => $data->tanggal_reservasi,
                    'waktu_reservasi' => $data->waktu_reservasi,
                    'no_medrec' => $data->no_medrec,
                    'nama_pasien' => $data->title_pasien.'. '.$data->nama_pasien,
                    'tanggal_lahir' => $data->tanggal_lahir,
                    'tujuan_poliklinik' => $data->tujuan_poliklinik,
                    'foto_dokter' => $data->foto_dokter,
                    'nama_dokter' => $data->nama_dokter,
                    'kategori_dokter' => $data->kategori_dokter,
                    'jenis_pembayaran' => '5' != $data->idkelompokpasien ? 'Pembayaran Asuransi' : 'Pembayaran',
                    'nama_asuransi' => '1' == $data->idkelompokpasien ? $data->nama_asuransi : $data->nama_kelompok_pasien,
                    'nomor_antrian' => 'Antrian Ke '.$data->nomor_antrian.' ('.kekata($data->nomor_antrian).' )',
                    'nomor_antrian_pendaftaran' => 'Antrian Ke '.$data->nomor_antrian_pendaftaran.' ('.kekata($data->nomor_antrian_pendaftaran).' )',
                    'kode_booking' => $data->kode_booking,
                    'nomor_pendaftaran' => $data->nomor_pendaftaran,
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_reservasi_rehab(): void
    {
        $akun_anggota_id = $this->input->post('akun_anggota_id');
        $pasien = $this->reservasi_rehabilitasi_model->get_pasien($akun_anggota_id);

        $dataPasien = [
            'statuspasienbaru' => 0,
            'idpasien' => $pasien->id,
            'no_medrec' => $pasien->no_medrec,
            'title' => $pasien->title,
            'namapasien' => $pasien->nama,
            'jenis_kelamin' => $pasien->jenis_kelamin,
            'tempat_lahir' => $pasien->tempat_lahir,
            'tanggal_lahir' => $pasien->tanggal_lahir,
            'golongan_darah' => $pasien->golongan_darah,
            'agama_id' => $pasien->agama_id,
            'warganegara' => $pasien->warganegara,
            'suku' => $pasien->suku,
            'statuskawin' => $pasien->status_kawin,
            'alamatpasien' => $pasien->alamat_jalan,
            'provinsi_id' => $pasien->provinsi_id,
            'kabupaten_id' => $pasien->kabupaten_id,
            'kecamatan_id' => $pasien->kecamatan_id,
            'kelurahan_id' => $pasien->kelurahan_id,
            'kodepos' => $pasien->kodepos,
            'telepon' => $pasien->telepon,
            'nohp' => $pasien->hp,
            'email' => $pasien->email,
            'pendidikan_id' => $pasien->pendidikan_id,
            'pekerjaan_id' => $pasien->pekerjaan_id,
            'namapenanggungjawab' => $pasien->nama_keluarga,
            'hubungan' => $pasien->hubungan_dengan_pasien,
            'teleponpenanggungjawab' => $pasien->telepon_keluarga,
            'noidentitaspenanggungjawab' => $pasien->ktp_keluarga,
            'alamatpenanggungjawab' => $pasien->alamat_keluarga,
        ];

        $hashids = new Hashids('RSKBHALMAHERA', 0, 'ABCEFGHJKLMNPRSTUVWXYZ123456789');
        $notransaksi = $this->tbooking_rehab_model->get_notransaksi();
        $nobooking = substr($notransaksi, -8);
        $kode_booking = $hashids->encode($nobooking);

        $dataRegistrasi = [
            'notransaksi' => $notransaksi,
            'nobooking' => $nobooking,
            'kode_booking' => $kode_booking,
            'waktu_reservasi' => date('Y-m-d H:i:s'),
            'idpoli' => $this->input->post('poliklinik_id'),
            'idpoliklinik' => $this->input->post('poliklinik_id'),
            'tanggal' => $this->input->post('tanggal_janji_temu'),
            'jam_id' => $this->input->post('waktu_janji_temu'),
            'pertemuan_id' => $this->input->post('jenis_pertemuan'),
            'idkelompokpasien' => $this->input->post('cara_pembayaran'),
            'idrekanan' => ('1' == $this->input->post('cara_pembayaran') ? $this->input->post('rekanan_id') : null),
            'foto_kartu' => $this->upload_foto_kartu(),
            'st_gc' => $this->input->post('status_general_consent'),
            'st_general' => ('1' == $this->input->post('status_general_consent') ? 0 : 1),
            'st_sp' => $this->input->post('status_skrining_pasien'),
            'st_skrining' => ('1' == $this->input->post('status_skrining_pasien') ? 0 : 1),
            'st_sc' => $this->input->post('status_skrining_covid'),
            'st_covid' => ('1' == $this->input->post('status_skrining_covid') ? 0 : 1),
            'created_date' => date('Y-m-d H:i:s'),
            'jadwal_id' => null,
            'iddokter' => null,
            'idasalpasien' => 1,
            'idtipepasien' => 1,
            'reservasi_tipe_id' => 2,
            'reservasi_cara' => 1,
            'status_reservasi' => 1,
            'status_kehadiran' => 0,
            'notif_wa' => 1,
            'notif_email' => 1,
        ];

        $data = $this->reservasi_rehabilitasi_model->post_reservasi_rehab(array_merge($dataPasien, $dataRegistrasi));
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'reservasi_id' => $data,
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function upload_foto_kartu()
    {
        if (isset($_FILES['foto_kartu'])) {
            $config['upload_path'] = './assets/upload/foto_kartu';
            $config['allowed_types'] = 'jpg|jpeg|png|bmp';
            $config['encrypt_name'] = true;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('foto_kartu')) {
                $error = $this->upload->display_errors();

                return null;
            }

            $uploadData = $this->upload->data();

            return $uploadData['file_name'];
        }

        return null;
    }

    public function check_if_registered($akun_anggota_id, $poliklinik_id, $tanggal, $jam_id): void
    {
        $pasien = $this->reservasi_rehabilitasi_model->get_pasien($akun_anggota_id);
        $data = $this->reservasi_rehabilitasi_model->check_if_registered($pasien->id, $poliklinik_id, $tanggal, $jam_id);
        
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'registered',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'not_register',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
}
