<?php

class Setting_apm extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Setting_apm_model', 'setting_apm_model');
        $this->load->model('Services/Data_dokter_model', 'data_dokter_model');
        $this->load->model('Services/Reservasi_poliklinik_model', 'reservasi_poliklinik_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }

    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function get_data_gallery(): void
    {
        $data = $this->setting_apm_model->get_data_gallery();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_apm(): void
    {
        $data = $this->setting_apm_model->get_setting_apm();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_setting_running_text(): void
    {
        $data = $this->setting_apm_model->get_setting_running_text();
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_logic_pendaftaran_apm($jenis_pertemuan_id, $idpoliklinik, $iddokter): void
    {
        $st_gc = '0';
        $st_sp = '0';
        $st_sc = '0';

        $hasil = get_logic_pendaftaran_apm($jenis_pertemuan_id, $idpoliklinik, $iddokter);
        if ($hasil) {
            $st_gc = $hasil->st_gc;
            $st_sp = $hasil->st_sp;
            $st_sc = $hasil->st_sc;
        }

        $data['st_gc'] = $st_gc;
        $data['st_sp'] = $st_sp;
        $data['st_sc'] = $st_sc;

        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_logic_checkin_apm($jenis_pertemuan_id, $idpoliklinik, $iddokter): void
    {
        $st_gc = '0';
        $st_sp = '0';
        $st_sc = '0';

        $hasil = get_logic_checkin_apm($jenis_pertemuan_id, $idpoliklinik, $iddokter);
        if ($hasil) {
            $st_gc = $hasil->st_gc;
            $st_sp = $hasil->st_sp;
            $st_sc = $hasil->st_sc;
        }

        $data['st_gc'] = $st_gc;
        $data['st_sp'] = $st_sp;
        $data['st_sc'] = $st_sc;

        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_jadwal_dokter($idpoliklinik = 0, $iddokter = 0) {
        $data = array();

        $dokter_data = $this->data_dokter_model->get_data_dokter($idpoliklinik, $iddokter);

        foreach ($dokter_data as $dokter) {
            // Get data poliklinik
            if ($idpoliklinik) {
                $data_poliklinik = $this->setting_apm_model->get_data_poliklinik($idpoliklinik, $dokter->id);
            } else {
                $data_poliklinik = $this->data_dokter_model->get_data_poliklinik($dokter->id);
            }

            $tempPoliklinik = array(); // Array sementara untuk mengumpulkan data poliklinik

            foreach ($data_poliklinik as $poliklinik) {
                // Get jadwal dokter
                $jadwal_dokter = $this->reservasi_poliklinik_model->get_jadwal_dokter_poliklinik($poliklinik->value, $dokter->id);

                // Get jadwal hari dan waktu
                $processed_jadwal = array();
                foreach ($jadwal_dokter as $jadwal) {
                    $waktu = $this->reservasi_poliklinik_model->get_jam_dokter_poliklinik($poliklinik->value, $dokter->id, $jadwal->tanggal);
                    $jadwal->waktu = $waktu;
                    $processed_jadwal[] = $jadwal;
                }

                // Tambahkan data poliklinik ke array sementara jika belum ada
                if (!isset($tempPoliklinik[$poliklinik->value])) {
                    $tempPoliklinik[$poliklinik->value] = array(
                        "id" => $poliklinik->value,
                        "nama" => $poliklinik->label,
                        "jadwal" => $processed_jadwal
                    );
                } else {
                    // Jika data poliklinik sudah ada, tambahkan jadwalnya saja
                    $tempPoliklinik[$poliklinik->value]['jadwal'] = array_merge($tempPoliklinik[$poliklinik->value]['jadwal'], $processed_jadwal);
                }
            }

            // Konversi array sementara poliklinik ke array yang sesuai dengan format
            $poliklinikArray = array_values($tempPoliklinik);

            $schedule = array(
                "id" => $dokter->id,
                "foto_dokter" => $dokter->foto_dokter,
                "nama_dokter" => $dokter->nama_dokter,
                "nip_dokter" => $dokter->nip_dokter,
                "kategori_dokter" => $dokter->kategori_dokter,
                "poliklinik" => $poliklinikArray
            );

            $data[] = $schedule;
        }

        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();

        exit;
    }

    public function get_pembayaran(): void
    {
        $data = $this->setting_apm_model->get_pembayaran();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_asuransi(): void
    {
        $data = $this->setting_apm_model->get_asuransi();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_poliklinik(): void
    {
        $data = $this->setting_apm_model->get_poliklinik();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_dokter(): void
    {
        $data = $this->setting_apm_model->get_dokter();
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
}
