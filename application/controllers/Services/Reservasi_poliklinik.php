<?php

require 'vendor/autoload.php';

use Hashids\Hashids;

class Reservasi_poliklinik extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Reservasi_poliklinik_model', 'reservasi_poliklinik_model');
        $this->load->model('Services/General_model', 'general_model');
        $this->load->model('Tbooking_rehab_model', 'tbooking_rehab_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }

    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function get_dokter_poliklinik($idpoli): void
    {
        $data = $this->reservasi_poliklinik_model->get_dokter_poliklinik($idpoli);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_jadwal_dokter_poliklinik($idpoli, $iddokter): void
    {
        $data = $this->reservasi_poliklinik_model->get_jadwal_dokter_poliklinik($idpoli, $iddokter);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_jam_dokter_poliklinik($idpoli, $iddokter, $tanggal): void
    {
        $data = $this->reservasi_poliklinik_model->get_jam_dokter_poliklinik($idpoli, $iddokter, $tanggal);
        $response = [
            'status' => true,
            'message' => 'success',
            'data' => $data,
        ];

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_jenis_pertemuan(): void
    {
        $data = $this->general_model->data_referensi(15);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_logic_pendaftaran_poli($jenis_pertemuan_id, $idpoliklinik, $iddokter): void
    {
        $st_gc = '0';
        $st_sp = '0';
        $st_sc = '0';

        $hasil = get_logic_pendaftaran_poli($jenis_pertemuan_id, $idpoliklinik, $iddokter);
        if ($hasil) {
            $st_gc = $hasil->st_gc;
            $st_sp = $hasil->st_sp;
            $st_sc = $hasil->st_sc;
        }

        $data['st_gc'] = $st_gc;
        $data['st_sp'] = $st_sp;
        $data['st_sc'] = $st_sc;

        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_informasi_pasien($akun_anggota_id): void
    {
        $data = $this->reservasi_poliklinik_model->get_pasien($akun_anggota_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_informasi_reservasi($reservasi_id): void
    {
        $data = $this->reservasi_poliklinik_model->get_informasi_reservasi($reservasi_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'id' => $data->id,
                    'tanggal_reservasi' => $data->tanggal_reservasi,
                    'waktu_reservasi' => $data->waktu_reservasi,
                    'no_medrec' => $data->no_medrec,
                    'nama_pasien' => $data->title_pasien.'. '.$data->nama_pasien,
                    'tanggal_lahir' => $data->tanggal_lahir,
                    'tujuan_poliklinik' => $data->tujuan_poliklinik,
                    'foto_dokter' => $data->foto_dokter,
                    'nama_dokter' => $data->nama_dokter,
                    'kategori_dokter' => $data->kategori_dokter,
                    'jenis_pembayaran' => '5' != $data->idkelompokpasien ? 'Pembayaran Asuransi' : 'Pembayaran',
                    'nama_asuransi' => '1' == $data->idkelompokpasien ? $data->nama_asuransi : $data->nama_kelompok_pasien,
                    'nomor_antrian' => 'Antrian Ke '.$data->nomor_antrian.' ('.kekata($data->nomor_antrian).' )',
                    'nomor_antrian_pendaftaran' => 'Antrian Ke '.$data->nomor_antrian_pendaftaran.' ('.kekata($data->nomor_antrian_pendaftaran).' )',
                    'kode_booking' => $data->kode_booking,
                    'nomor_pendaftaran' => $data->nomor_pendaftaran,
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_reservasi_poli(): void
    {
        $akun_anggota_id = $this->input->post('akun_anggota_id');
        $pasien = $this->reservasi_poliklinik_model->get_pasien($akun_anggota_id);

        $dataPasien = [
            'statuspasienbaru' => 0,
            'idpasien' => $pasien->id,
            'no_medrec' => $pasien->no_medrec,
            'title' => $pasien->title,
            'namapasien' => $pasien->nama,
            'jenis_kelamin' => $pasien->jenis_kelamin,
            'tempat_lahir' => $pasien->tempat_lahir,
            'tanggal_lahir' => $pasien->tanggal_lahir,
            'golongan_darah' => $pasien->golongan_darah,
            'agama_id' => $pasien->agama_id,
            'warganegara' => $pasien->warganegara,
            'suku' => $pasien->suku,
            'statuskawin' => $pasien->status_kawin,
            'alamatpasien' => $pasien->alamat_jalan,
            'provinsi_id' => $pasien->provinsi_id,
            'kabupaten_id' => $pasien->kabupaten_id,
            'kecamatan_id' => $pasien->kecamatan_id,
            'kelurahan_id' => $pasien->kelurahan_id,
            'kodepos' => $pasien->kodepos,
            'telepon' => $pasien->telepon,
            'nohp' => $pasien->hp,
            'email' => $pasien->email,
            'pendidikan_id' => $pasien->pendidikan_id,
            'pekerjaan_id' => $pasien->pekerjaan_id,
            'namapenanggungjawab' => $pasien->nama_keluarga,
            'hubungan' => $pasien->hubungan_dengan_pasien,
            'teleponpenanggungjawab' => $pasien->telepon_keluarga,
            'noidentitaspenanggungjawab' => $pasien->ktp_keluarga,
            'alamatpenanggungjawab' => $pasien->alamat_keluarga,
        ];

        $hashids = new Hashids('RSKBHALMAHERA', 0, 'ABCEFGHJKLMNPRSTUVWXYZ123456789');
        $notransaksi = $this->tbooking_rehab_model->get_notransaksi();
        $nobooking = substr($notransaksi, -8);
        $kode_booking = $hashids->encode($nobooking);

        $dataRegistrasi = [
            'notransaksi' => $notransaksi,
            'nobooking' => $nobooking,
            'kode_booking' => $kode_booking,
            'waktu_reservasi' => date('Y-m-d H:i:s'),
            'idpoli' => $this->input->post('poliklinik_id'),
            'idpoliklinik' => $this->input->post('poliklinik_id'),
            'iddokter' => $this->input->post('dokter_id'),
            'tanggal' => $this->input->post('tanggal_janji_temu'),
            'jadwal_id' => $this->input->post('waktu_janji_temu'),
            'pertemuan_id' => $this->input->post('jenis_pertemuan'),
            'idkelompokpasien' => $this->input->post('cara_pembayaran'),
            'idrekanan' => ('1' == $this->input->post('cara_pembayaran') ? $this->input->post('rekanan_id') : null),
            'foto_kartu' => $this->upload_foto_kartu(),
            'st_gc' => $this->input->post('status_general_consent'),
            'st_general' => ('1' == $this->input->post('status_general_consent') ? 0 : 1),
            'st_sp' => $this->input->post('status_skrining_pasien'),
            'st_skrining' => ('1' == $this->input->post('status_skrining_pasien') ? 0 : 1),
            'st_sc' => $this->input->post('status_skrining_covid'),
            'st_covid' => ('1' == $this->input->post('status_skrining_covid') ? 0 : 1),
            'created_date' => date('Y-m-d H:i:s'),
            'jam_id' => null,
            'idasalpasien' => 1,
            'idtipepasien' => 1,
            'reservasi_tipe_id' => 1,
            'reservasi_cara' => 1,
            'status_reservasi' => 1,
            'status_kehadiran' => 0,
            'notif_wa' => 1,
            'notif_email' => 1,
        ];

        $data = $this->reservasi_poliklinik_model->post_reservasi_poli(array_merge($dataPasien, $dataRegistrasi));
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'reservasi_id' => $data,
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function upload_foto_kartu()
    {
        if (isset($_FILES['foto_kartu'])) {
            $config['upload_path'] = './assets/upload/foto_kartu';
            $config['allowed_types'] = 'jpg|jpeg|png|bmp';
            $config['encrypt_name'] = true;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('foto_kartu')) {
                $error = $this->upload->display_errors();

                return null;
            }

            $uploadData = $this->upload->data();

            return $uploadData['file_name'];
        }

        return null;
    }

    public function get_general_consent($reservasi_id): void
    {
        $data = $this->reservasi_poliklinik_model->get_general_consent($reservasi_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'information' => $data['head'],
                    'form' => $data['detail'],
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_skrining_pasien($reservasi_id): void
    {
        $data = $this->reservasi_poliklinik_model->get_skrining_pasien($reservasi_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_skrining_covid($reservasi_id): void
    {
        $data = $this->reservasi_poliklinik_model->get_skrining_covid($reservasi_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_form_general_consent(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->reservasi_poliklinik_model->post_form_general_consent($input->gc_id, $input->jawaban);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_general_consent(): void
    {
        $dataGC = [
            'reservasi_id' => $this->input->post('reservasi_id'),
            'persetujuan' => $this->input->post('persetujuan'),
            'pasien_dapat_menandatangani' => $this->input->post('pasien_dapat_menandatangani'),
            'siapa_yang_menandatangani' => $this->input->post('siapa_yang_menandatangani'),
            'nama' => $this->input->post('nama'),
            'hubungan' => $this->input->post('hubungan'),
            'tanda_tangan' => $this->input->post('tanda_tangan'),
        ];

        $data = $this->reservasi_poliklinik_model->post_general_consent($dataGC);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_form_skrining_pasien(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->reservasi_poliklinik_model->post_form_skrining_pasien($input->sp_id, $input->jenis_isi, $input->jawaban);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_skrining_pasien(): void
    {
        $dataSP = [
            'reservasi_id' => $this->input->post('reservasi_id'),
        ];

        $data = $this->reservasi_poliklinik_model->post_skrining_pasien($dataSP);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_form_skrining_covid(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->reservasi_poliklinik_model->post_form_skrining_covid($input->sc_id, $input->jenis_isi, $input->jawaban);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function post_skrining_covid(): void
    {
        $dataSC = [
            'reservasi_id' => $this->input->post('reservasi_id'),
        ];

        $data = $this->reservasi_poliklinik_model->post_skrining_covid($dataSC);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();

        exit;
    }

    public function check_if_registered($akun_anggota_id, $poliklinik_id, $dokter_id, $tanggal, $jadwal_id): void
    {
        $pasien = $this->reservasi_poliklinik_model->get_pasien($akun_anggota_id);
        $data = $this->reservasi_poliklinik_model->check_if_registered($pasien->id, $poliklinik_id, $dokter_id, $tanggal, $jadwal_id);
        
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'registered',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'not_register',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
}
