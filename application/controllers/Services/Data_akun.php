<?php

require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class Data_akun extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_cors_headers();
        $this->load->model('Services/Data_akun_model', 'data_akun_model');
    }

    private function add_cors_headers()
    {
        $allowed_origins = [
            'https://sahabat-siaga.vercel.app',
            'https://sahabat-siaga-apm.vercel.app',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
            header('Access-Control-Max-Age: 86400');
        }
    }

    public function index(): void
    {
        echo 'Directory access is forbidden';
    }

    public function registrasi(): void
    {
        $dataRegistrasi = [
            'title' => $this->input->post('title'),
            'nama' => $this->input->post('nama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'golongan_darah' => $this->input->post('golongan_darah'),
            'agama_id' => $this->input->post('agama_id'),
            'warganegara' => $this->input->post('warganegara'),
            'suku' => $this->input->post('suku'),
            'status_kawin' => $this->input->post('status_kawin'),
            'alamat' => $this->input->post('alamat'),
            'provinsi_id' => $this->input->post('provinsi_id'),
            'kabupaten_id' => $this->input->post('kabupaten_id'),
            'kecamatan_id' => $this->input->post('kecamatan_id'),
            'kelurahan_id' => $this->input->post('kelurahan_id'),
            'telepon' => $this->input->post('telepon'),
            'pendidikan_id' => $this->input->post('pendidikan_id'),
            'pekerjaan_id' => $this->input->post('pekerjaan_id'),
            'nama_keluarga' => $this->input->post('nama_keluarga'),
            'hubungan_dengan_pasien' => $this->input->post('hubungan_dengan_pasien'),
            'alamat_keluarga' => $this->input->post('alamat_keluarga'),
            'telepon_keluarga' => $this->input->post('telepon_keluarga'),
            'status_pernah_berobat' => $this->input->post('status_pernah_berobat'),
            'foto_ktp' => $this->upload_foto_ktp(),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'kode_verifikasi' => str_pad(strval(rand(0, 9999)), 4, '0', STR_PAD_LEFT),
        ];

        $data = $this->data_akun_model->registrasi_akun($dataRegistrasi);
        if ($data) {
            $this->kirim_kode_verifikasi($data, 'registration');
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function registrasi_pasien_apm(): void
    {
        $dataRegistrasi = [
            'title' => $this->input->post('title'),
            'nama' => $this->input->post('nama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'golongan_darah' => $this->input->post('golongan_darah'),
            'agama_id' => $this->input->post('agama_id'),
            'warganegara' => $this->input->post('warganegara'),
            'suku' => $this->input->post('suku'),
            'status_kawin' => $this->input->post('status_kawin'),
            'alamat' => $this->input->post('alamat'),
            'provinsi_id' => $this->input->post('provinsi_id'),
            'kabupaten_id' => $this->input->post('kabupaten_id'),
            'kecamatan_id' => $this->input->post('kecamatan_id'),
            'kelurahan_id' => $this->input->post('kelurahan_id'),
            'telepon' => $this->input->post('telepon'),
            'pendidikan_id' => $this->input->post('pendidikan_id'),
            'pekerjaan_id' => $this->input->post('pekerjaan_id'),
            'nama_keluarga' => $this->input->post('nama_keluarga'),
            'hubungan_dengan_pasien' => $this->input->post('hubungan_dengan_pasien'),
            'alamat_keluarga' => $this->input->post('alamat_keluarga'),
            'telepon_keluarga' => $this->input->post('telepon_keluarga'),
            'status_pernah_berobat' => $this->input->post('status_pernah_berobat'),
            'foto_ktp' => $this->upload_foto_ktp(),
        ];

        $data = $this->data_akun_model->registrasi_pasien_apm($dataRegistrasi);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function tambah_anggota_baru(): void
    {
        $dataAnggota = [
            'title' => $this->input->post('title'),
            'nama' => $this->input->post('nama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'golongan_darah' => $this->input->post('golongan_darah'),
            'agama_id' => $this->input->post('agama_id'),
            'warganegara' => $this->input->post('warganegara'),
            'suku' => $this->input->post('suku'),
            'status_kawin' => $this->input->post('status_kawin'),
            'alamat' => $this->input->post('alamat'),
            'provinsi_id' => $this->input->post('provinsi_id'),
            'kabupaten_id' => $this->input->post('kabupaten_id'),
            'kecamatan_id' => $this->input->post('kecamatan_id'),
            'kelurahan_id' => $this->input->post('kelurahan_id'),
            'telepon' => $this->input->post('telepon'),
            'pendidikan_id' => $this->input->post('pendidikan_id'),
            'pekerjaan_id' => $this->input->post('pekerjaan_id'),
            'nama_keluarga' => $this->input->post('nama_keluarga'),
            'hubungan_dengan_pasien' => $this->input->post('hubungan_dengan_pasien'),
            'alamat_keluarga' => $this->input->post('alamat_keluarga'),
            'telepon_keluarga' => $this->input->post('telepon_keluarga'),
            'status_pernah_berobat' => $this->input->post('status_pernah_berobat'),
            'foto_ktp' => $this->upload_foto_ktp(),
            'parent_akun_id' => $this->input->post('parent_akun_id'),
        ];

        $data = $this->data_akun_model->tambah_anggota_baru($dataAnggota);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function tambah_anggota_lama(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $dataAnggota = [
            'akun_id' => $input->akun_id,
            'pasien_id' => $input->pasien_id,
            'nama' => $input->nama,
            'hubungan_dengan_pasien' => $input->hubungan_dengan_pasien,
        ];

        $data = $this->data_akun_model->tambah_anggota_lama($dataAnggota);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function verifikasi_akun(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->data_akun_model->verifikasi_akun($input->akun_id, $input->kode_verifikasi);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function login(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $data = $this->data_akun_model->login($input->email, $input->password);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_akun_anggota($akun_id): void
    {
        $data = $this->data_akun_model->get_akun_anggota($akun_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_detail_akun_anggota($akun_anggota_id): void
    {
        $data = $this->data_akun_model->get_detail_akun_anggota($akun_anggota_id);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();

        exit;
    }

    public function upload_foto_ktp()
    {
        $config['upload_path'] = './assets/upload/foto_kartu';
        $config['allowed_types'] = 'jpg|jpeg|png|bmp';
        $config['encrypt_name'] = true;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('foto_ktp')) {
            $error = $this->upload->display_errors();

            return null;
        }
        $uploadData = $this->upload->data();

        return $uploadData['file_name'];
    }

    public function get_nomedrec()
    {
        $data = $this->data_akun_model->get_nomedrec();
        print_r($data);exit();
    }

    public function kirim_kode_verifikasi($akun_id, $status = '')
    {
        if ($status == 'resend') {
            $this->db->update('merm_akun', [
                'kode_verifikasi' => str_pad(strval(rand(0, 9999)), 4, '0', STR_PAD_LEFT)
            ], ['id' => $akun_id]);
        }

        $data = $this->data_akun_model->get_detail_akun($akun_id);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 0;
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->Host = 'mail.halmaherasiaga.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'erm@halmaherasiaga.com';
            $mail->Password = '{T,_Xun}9{@5';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port = 465;

            //Recipients
            $mail->setFrom('erm@halmaherasiaga.com', 'Sahabat Siaga');
            $mail->addAddress($data->email, $data->nama);

            //Content
            $mail->isHTML(true);
            $mail->Subject = "Bergabunglah dengan Sahabat Siaga - Kode Verifikasi Akun Anda";
            $mail->Body = "Halo <b>" . $data->nama . "</b>,
            <br><br>
            Selamat datang di Sahabat Siaga, teman sejati dalam setiap langkah perjalanan kesehatan Anda!
            <br><br>
            Berikut adalah Kode Verifikasi Pendaftaran Akun Anda: <b>" . $data->kode_verifikasi . "</b>
            <br><br>
            Kami menghargai setiap langkah yang Anda ambil dalam mencapai kesehatan yang optimal, dan verifikasi akun adalah salah satunya. Jika Anda membutuhkan bantuan atau informasi lebih lanjut, jangan ragu untuk menghubungi tim kami.
            <br><br>
            Privasi dan keamanan data Anda adalah prioritas utama kami. Data pribadi Anda akan selalu dijaga dengan ketat sesuai dengan kebijakan privasi kami.
            <br><br>
            Jangan tunda lagi! Segera verifikasi akun Anda dan bergabunglah dengan Sahabat Siaga untuk memulai perjalanan kesehatan yang luar biasa.
            <br><br>
            Terima kasih atas kepercayaan Anda menjadi bagian dari Sahabat Siaga, rekan sejati Anda dalam kesehatan dan kebahagiaan.
            <br><br>
            Salam Sehat,
            <br>
            Tim Sahabat Siaga";

            $mail->send();

            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
            ];
        }

        if ($status != 'registration') {
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                ->_display();
    
            exit;
        }
    }

    public function ubah_akun(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $dataAkun = [
            'id' => $input->akun_id,
            'nama' => $input->nama,
            'email' => $input->email,
        ];

        $data = $this->data_akun_model->ubah_akun($dataAkun);
        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function ubah_password(): void
    {
        $input = json_decode(file_get_contents('php://input'));

        $dataAkun = [
            'id' => $input->akun_id,
            'password_lama' => $input->password_lama,
            'password_baru' => $input->password_baru,
        ];

        $data = $this->data_akun_model->ubah_password($dataAkun);
        if ($data == 'success') {
            $response = [
                'status' => true,
                'message' => 'Password Akun telah berhasil diubah!',
            ];
        } else if ($data == 'error') {
            $response = [
                'status' => false,
                'message' => 'Gagal mengubah password. Silakan coba lagi.',
            ];
        } else if ($data == 'old_password_not_match') {
            $response = [
                'status' => false,
                'message' => 'Password lama tidak sesuai. Gagal mengubah password.',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
}
