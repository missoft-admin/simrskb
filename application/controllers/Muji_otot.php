<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muji_otot extends CI_Controller {

	/**
	 * Master Uji Kekuatan Otot controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Muji_otot_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Uji Kekuatan Otot';
		$data['content'] 		= 'Muji_otot/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Uji Kekuatan Otot",'#'),
									    			array("List",'muji_otot')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'nourut' 						=> '1',
			'id' 						=> '',
			'nama' 					=> '',
			'isi_footer' 		=> '',
			'isi_header' 		=> '',
			'idtipe' 		=> '',
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Uji Kekuatan Otot';
		$data['content'] 		= 'Muji_otot/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Uji Kekuatan Otot",'#'),
									    			array("Tambah",'muji_otot')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Muji_otot_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nourut' 					=> $row->nourut,
					'nama' 					=> $row->nama,
					'isi_header' 		=> $row->isi_header,
					'idtipe' 		=> $row->idtipe,
					'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Uji Kekuatan Otot';
				$data['content']	 	= 'Muji_otot/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Uji Kekuatan Otot",'#'),
											    			array("Ubah",'muji_otot')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('muji_otot','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('muji_otot');
		}
	}
	function setting_range_nilai($id){
		if($id != ''){
			$row = $this->Muji_otot_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'isi_header' 		=> $row->isi_header,
					'idtipe' 		=> $row->idtipe,
					'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Uji Kekuatan Otot';
				$data['content']	 	= 'Muji_otot/manage_setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Uji Kekuatan Otot",'#'),
											    			array("Ubah",'muji_otot')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('muji_otot','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('muji_otot');
		}
	}

	function delete($id){
		$this->Muji_otot_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('muji_otot','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('isi_header', 'Isi Header', 'trim|required');
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Muji_otot_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('muji_otot/update/'.$id,'location');
				}
			} else {
				if($this->Muji_otot_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('muji_otot/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Muji_otot/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Uji Kekuatan Otot';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Uji Kekuatan Otot",'#'),
															array("Tambah",'muji_otot')
													);
		}else{
			$data['title'] = 'Ubah Master Uji Kekuatan Otot';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Uji Kekuatan Otot",'#'),
															array("Ubah",'muji_otot')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'muji_otot';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('nama','isi_header');
      $this->column_order    = array('nama','isi_header');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama.' '.GetTipePasienPiutang($r->idtipe);
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('1966'))){
            $aksi .= '<a href="'.site_url().'muji_otot/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('1967'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'muji_otot" data-urlremove="'.site_url().'muji_otot/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_parameter()
	{
			$uji_otot_id=$this->input->post('uji_otot_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*,
						GROUP_CONCAT(CONCAT(M.deskripsi_nama,' (',M.urutan,')') SEPARATOR '#') as jawaban
						FROM `muji_otot_param` H
						LEFT JOIN muji_otot_param_detail M ON M.parameter_id=H.id AND M.staktif='1'
						where H.staktif='1' AND H.uji_otot_id='$uji_otot_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('parameter_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->nourut;
          $result[] = $r->parameter_nama.($r->jawaban?'&nbsp;&nbsp;'.$this->render_jawaban($r->jawaban):'');
        
		  $aksi='';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="add_jawaban('.$r->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
		  $aksi .= '<button onclick="edit_parameter('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_parameter('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function render_jawaban($jawaban){
	  $hasil='';
	  $arr=explode('#',$jawaban);
	  foreach($arr as $index=>$val){
		  $hasil .='<span class="badge ">'.$val.'</span> ';
	  }
	  return $hasil;
  }
  function load_nilai()
	{
			$uji_otot_id=$this->input->post('uji_otot_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `muji_otot_setting_nilai` H
						where H.staktif='1' AND H.uji_otot_id='$uji_otot_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor_1.' - '.$r->skor_2;
          $result[] = $r->ref_nilai;
          $result[] = ($r->st_tindakan=='1'?text_success('YA'):text_default('TIDAK'));
          $result[] = $r->nama_tindakan;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_nilai(){
		$parameter_id=$this->input->post('parameter_id');
		$uji_otot_id=$this->input->post('uji_otot_id');
		$skor_1=$this->input->post('skor_1');
		$skor_2=$this->input->post('skor_2');
		$ref_nilai=$this->input->post('ref_nilai');
		$ref_nilai_id=$this->input->post('ref_nilai_id');
		$data=array(
			'uji_otot_id'=>$this->input->post('uji_otot_id'),
			'skor_1'=>$this->input->post('skor_1'),
			'skor_2'=>$this->input->post('skor_2'),
			'ref_nilai'=>$this->input->post('ref_nilai'),
			'ref_nilai_id'=>$this->input->post('ref_nilai_id'),
			'st_tindakan'=>$this->input->post('st_tindakan'),
			'nama_tindakan'=>$this->input->post('nama_tindakan'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('muji_otot_setting_nilai',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('muji_otot_setting_nilai',$data);
		}
		  
		  json_encode($hasil);
	}
	function simpan_parameter(){
		$uji_otot_id=$this->input->post('uji_otot_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'nourut'=>$this->input->post('nourut'),
			'uji_otot_id'=>$this->input->post('uji_otot_id'),
			'parameter_nama'=>$this->input->post('parameter_nama'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('muji_otot_param',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('muji_otot_param',$data);
		}
		  
		  json_encode($hasil);
	}
	function simpan_jawaban(){
		$nilai_id=$this->input->post('nilai_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'parameter_id'=>$this->input->post('parameter_id'),
			'nilai'=>$this->input->post('nilai'),
			'warna'=>$this->input->post('warna'),
			'urutan'=>$this->input->post('urutan'),
			'deskripsi_nama'=>$this->input->post('deskripsi_nama'),
			'staktif'=>1,
		);
		if ($nilai_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('muji_otot_param_detail',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$nilai_id);
		    $hasil=$this->db->update('muji_otot_param_detail',$data);
		}
		  
		  json_encode($hasil);
	}
	function hapus_nilai(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('muji_otot_setting_nilai',$data);
	  
	  json_encode($hasil);
	  
  }
  function hapus_parameter(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('muji_otot_param',$data);
	  
	  $this->output->set_output(json_encode($hasil));
	  
  }
  function hapus_jawaban(){
	  $nilai_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$nilai_id);
		$hasil=$this->db->update('muji_otot_param_detail',$data);
	  
	  $this->output->set_output(json_encode($hasil));
	  
  }

  function find_parameter(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM muji_otot_param H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_nilai(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM muji_otot_setting_nilai H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_jawaban(){
	  $nilai_id=$this->input->post('id');
	  $q="SELECT *FROM muji_otot_param_detail H WHERE H.id='$nilai_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function load_jawaban()
	{
		$parameter_id=$this->input->post('parameter_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `muji_otot_param_detail` H
							where H.staktif='1' AND H.parameter_id='$parameter_id'
							ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->urutan;
			 $aksi='';
          $result[] = $r->deskripsi_nama;
          $result[] = $r->nilai;
			$result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.';color:#000" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_jawaban('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_jawaban('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_tabel_jawaban(){
	  $uji_otot_id=$this->input->post('uji_otot_id');
	  $q="
		SELECT *FROM (
			SELECT H.id,0 as lev,LPAD(H.nourut,3,'0') as xnourut,H.nourut,H.parameter_nama as nama,'0' as nilai,'' as warna FROM muji_otot_param H WHERE H.uji_otot_id='$uji_otot_id' AND H.staktif='1'
			UNION ALL
			SELECT D.id,1 as lev,CONCAT(LPAD(H.nourut,3,'0'),'-',LPAD(D.urutan,3,'0')) as xnourut,D.urutan as nourut,D.deskripsi_nama as nama,D.nilai,D.warna
			FROM muji_otot_param_detail D
			INNER JOIN muji_otot_param H ON H.id=D.parameter_id
			WHERE H.uji_otot_id='$uji_otot_id' AND H.staktif='1' AND D.staktif='1'
			) T 
			ORDER BY T.xnourut ASC
	  ";
	  $tabel='';
	  $list_data=$this->db->query($q)->result();
	  foreach($list_data as $row){
		  $tabel .='<tr>';
		  if ($row->lev=='1'){
			  $aksi = '<button onclick="hapus_jawaban('.$row->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $tabel .='<td>'.$row->nourut.'</td>';
			  $tabel .='<td>'.$row->nama.'</td>';
			  $tabel .='<td>'.$row->nilai.'</td>';
			  $btn_warna ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$row->warna.';color:#000" type="button" >'.$row->warna.'</button>';
			  $tabel .='<td>'.$btn_warna.'</td>';
			  $tabel .='<td>'.$aksi.'</td>';
		  }else{
			  $aksi='';
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="add_jawaban('.$row->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
			  $aksi .= '<button onclick="edit_parameter('.$row->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
			  $aksi .= '<button onclick="hapus_parameter('.$row->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $tabel .='<td colspan="4" class="bg-info"><strong>'.$row->nourut.'. '.$row->nama.'</strong></td>';
			  $tabel .='<td class="bg-info">'.$aksi.'</td>';
		  }
			  
		  $tabel .='</tr>';
	  }
	  $this->output->set_output(json_encode($tabel));
  }
}
