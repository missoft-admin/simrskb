<?php defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 1000);
ini_set('memory_limit', '512M');

defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Tklaim_rincian extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tklaim_rincian_model','model');
		$this->load->model('Tvalidasi_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Trawatinap_verifikasi_model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$date=date_create($hari_ini);
		date_add($date,date_interval_create_from_date_string("-30 days"));
		$tgl_pertama = date_format($date,"d-m-Y");
		
		$date=date_create($hari_ini);
		date_add($date,date_interval_create_from_date_string("30 days"));
		$tgl_terakhir = date_format($date,"d-m-Y");
		
		// $tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		// $tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tanggal_tagihan'=>$tgl_pertama,
			'tanggal_tagihan2'=>$tgl_terakhir,
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Rincian Penagihan';
		$data['content'] 		= 'Tklaim_rincian/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Rincian Tagihan",'tklaim_rincian/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$no_klaim=$this->input->post('no_klaim');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$status_pengiriman=$this->input->post('status_pengiriman');
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where1='';
		$where2='';
		$where='';
		
		if ($no_klaim !=''){
			$where1 .=" AND TK.no_klaim LIKE '%".$no_klaim."%' ";
		}
		
		if ($idkelompokpasien !='#'){
			$where1 .=" AND TK.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			$where1 .=" AND TK.idrekanan='$idrekanan' ";
		}
		if ($tipe !='#'){
			$where1 .=" AND TK.tipe='$tipe' ";
		}
		if ($status !='#'){
			$where1 .=" AND TK.status_kirim='$status' ";
		}
		if ($status_pengiriman !='#'){
			if ($status_pengiriman=='1'){
				$where .=" WHERE selisih='0' ";
			}elseif ($status_pengiriman=='2'){
				$where .=" WHERE selisih > 1 ";
			}elseif ($status_pengiriman=='3'){
				$where .=" WHERE selisih < 0 ";
			}
		}
		
		
		if ('' != $tanggal_tagihan) {
            $where1 .= " AND DATE(TK.tanggal_tagihan) >='".YMDFormat($tanggal_tagihan)."' AND DATE(TK.tanggal_tagihan) <='".YMDFormat($tanggal_tagihan2)."'";
        }
		
        $from = "(
					SELECT CASE WHEN RS.id IS NULL THEN 2 ELSE 1 END as st_cari,TK.id,TK.no_klaim,TK.tipe,CASE WHEN TK.idkelompokpasien !='1' THEN kel.nama ELSE R.nama END as rekanan_nama,TK.idkelompokpasien
					,TK.idrekanan,TK.jml_faktur,TK.total_tagihan,TK.tanggal_tagihan
					,DATEDIFF(TK.tanggal_kirim, TK.batas_kirim) as selisih,TK.tanggal_kirim,TK.jatuh_tempo_bayar,TK.kirim_date,TK.noresi
					,TK.`status`,TK.status_kirim
					,TK.st_multiple,CONCAT(MP.no_medrec,' - ',MP.nama) nama_pasien,TK.idpasien
					FROM tklaim TK
					LEFT JOIN mpasien_kelompok kel ON kel.id=TK.idkelompokpasien
					LEFT JOIN mrekanan R ON R.id=TK.idrekanan
					LEFT JOIN mrekanan_setting RS ON RS.idrekanan=TK.idrekanan AND  TK.idkelompokpasien='1' AND RS.`status`='1'
					LEFT JOIN mfpasien MP ON MP.id=TK.idpasien AND TK.st_multiple='2'
					WHERE TK.`status` > 0 ".$where1."
					GROUP BY TK.id
				) as tbl ".$where." ORDER BY tanggal_tagihan DESC";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->tipe;
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->no_klaim;
            $row[] = $r->rekanan_nama.($r->st_multiple=='2'?'<br>'.$r->nama_pasien:'');
            $row[] = ($r->tipe=='1' ? '<span class="label label-primary">RJ</span>':'<span class="label label-success">RI/ODS</span>');
            $row[] = $r->jml_faktur;
            $row[] = number_format($r->total_tagihan,2);
            $row[] = HumanDateShort($r->tanggal_tagihan);
			if ($r->status=='1'){
				$row[] = ($r->status_kirim=='1' ? '<span class="label label-success">TELAH DIKIRIM</span>':'<span class="label label-danger">BELUM DIKIRIM</span>');
			}elseif ($r->status=='2'){
				$row[] = ($r->status_kirim=='1' ? '<span class="label label-success">TELAH DIKIRIM</span>':'<span class="label label-danger">BELUM DIKIRIM</span> <span class="label label-warning">STOP</span>');
			}elseif ($r->status=='3'){
				$row[] = ($r->status_kirim=='1' ? '<span class="label label-success">TELAH DIKIRIM</span>':'<span class="label label-danger">BELUM DIKIRIM</span> <span class="label label-warning">STOP</span>');
				
			}
            $row[] = ($r->selisih==''?'':status_selisih($r->selisih));
            $row[] = HumanDateShort($r->jatuh_tempo_bayar);
            $row[] = ($r->status_kirim=='1' ?HumanDateShort($r->tanggal_kirim):'<span class="label label-danger">BELUM DIKIRIM</span>');;
            $row[] = $r->noresi;

			
			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'/'.$r->status_kirim.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
			if ($r->status=='1'){
				$aksi .= '<button title="Stop Tagihan"  class="btn btn-xs btn-warning stop"><i class="fa fa-stop-circle"></i></button>';
			}elseif ($r->status=='2'){
				$aksi .= '<button title="Buka Tagihan"  class="btn btn-xs btn-success buka"><i class="fa fa-play-circle"></i></button>';
			}
			if ($r->status_kirim=='0'){
			$aksi .= '<button title="Kirim"  class="btn btn-xs btn-primary kirim"><i class="fa fa-send"></i></button>';
			$aksi .= '<button title="Memajukan Tagihan"  class="btn btn-xs btn-danger ganti"><i class="si si-arrow-right"></i></button>';
			}
			if ($r->status_kirim=='1'){
			$aksi .= '<button title="No Resi"  class="btn btn-xs btn-primary kirim"><i class="si si-docs"></i></button>';
			}
			 $aksi .= '<div class="btn-group">
				  <div class="btn-group dropright">
					<button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
					  <span class="fa fa-print"></span>
					</button>
				  <ul class="dropdown-menu">
					<li>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_kwitansi/'.$r->id.'">Kwitansi</a>';
			if ($r->tipe=='1'){
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_rekap/'.$r->id.'">Rekapitulasi Tagihan</a>';
			}
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_surat/'.$r->id.'">Surat Tagihan</a>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_history_tagihan/'.$r->id.'">Tagihan Sebelumnya</a>';
			$aksi .= '</li>
				  </ul>
				  </div>
				</div>';
			$aksi.='</div>';
            $row[] = $aksi;           
            $row[] = $r->idkelompokpasien;  //15         
            $row[] = $r->idrekanan;           //16
            $row[] = $r->st_cari;           //17
            $row[] = $r->st_multiple;    //18       
            $row[] = $r->idpasien;           //19
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_rajal(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$klaim_id=$this->input->post('klaim_id');
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where1='';
		
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tgl_trx)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tgl_trx2)."'";
        }
		
        $from = "(
					SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,M.nama as poli,D.nama as namadokter,K.total,TD.nominal as tot_asuransi
					,TH.idkelompokpasien,TH.idrekanan,TH.tanggal_tagihan,TH.tipe
					from tklaim_detail TD
					LEFT JOIN tklaim TH ON TH.id=TD.klaim_id
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mpoliklinik M ON TP.idpoliklinik=M.id
					LEFT JOIN mdokter D ON D.id=TP.iddokter
					LEFT JOIN tkasir K ON K.id=TD.kasir_id
					WHERE TD.klaim_id='$klaim_id' ".$where1."
					GROUP BY TP.id

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = '';
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = $r->poli;
            $row[] = $r->namadokter;
           
            $row[] = number_format($r->total,0);
            $row[] = number_format($r->tot_asuransi,0);
            $row[] = number_format($r->total-$r->tot_asuransi,0);
			
			$aksi .= '<button title="Memajukan Tagihan" '.$disabel.' class="btn btn-xs btn-danger ganti"><i class="si si-arrow-right"></i></button>';
			$aksi .= '<div class="btn-group">
				  <div class="btn-group dropright">
					<button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
					  <span class="fa fa-print"></span>
					</button>
				  <ul class="dropdown-menu">
					<li>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_kwitansi_detail/'.$r->id.'">Kwitansi</a>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_surat_detail/'.$r->id.'">Surat Tagihan</a>';
			$aksi .= '</li>
				  </ul>
				  </div>
				</div>';
			$aksi.='</div>';
			
			// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
		
			
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_ranap(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$klaim_id=$this->input->post('klaim_id');
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where1='';
		
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tgl_trx)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tgl_trx2)."'";
        }
		
        $from = "(
					SELECT TD.id,TD.pendaftaran_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,K.nama as kelas,R.nama as ruangan,B.nama as bed,TR.tanggaldari,TR.tanggalsampai,TB.total		
					from tklaim_detail TD
					LEFT JOIN trawatinap_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mkelas K ON K.id=TP.idkelas
					LEFT JOIN mruangan R ON R.id=TP.idruangan
					LEFT JOIN mbed B ON B.id=TP.idbed
					LEFT JOIN trawatinap_ruangan TR ON TR.idrawatinap=TP.id
					LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.idtindakan=TP.id AND TB.statusbatal=0
					WHERE TD.klaim_id='$klaim_id' ".$where1."
					GROUP BY TP.id

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
		$url_rajal        = site_url('trawatinap_tindakan/');

        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = $r->kelas.'  -  '.$r->bed;
            $row[] = HumanDateShort($r->tanggaldari);
            $row[] = HumanDateShort($r->tanggalsampai);
            $row[] = number_format($r->total,0);
           
			$aksi .= '<button title="Memajukan Tagihan" '.$disabel.' class="btn btn-xs btn-danger ganti"><i class="si si-arrow-right"></i></button>';
			$aksi .= '<div class="btn-group">
				  <div class="btn-group dropright">
					<button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
					  <span class="fa fa-print"></span>
					</button>
				  <ul class="dropdown-menu">
					<li>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_kwitansi_detail/'.$r->id.'">Kwitansi</a>';
			$aksi .= '<a tabindex="-1" target="_blank" href="'.$url.'print_surat_detail/'.$r->id.'">Surat Tagihan</a>';
			$aksi .= '</li>
				  </ul>
				  </div>
				</div>';
			// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_rajal.'print_rincian_biaya/'.$r->pendaftaran_id.'/1" target="_blank"  type="button"  title="Print Rincian Biaya"><i class="fa fa-print"></i></a>';
			// $aksi .= '<button disabled class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
		
			
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function rincian_tagihan($id,$tipe,$disabel='0'){
		
		$data=$this->model->detail($id);
		// print_r($data);exit();
		$data['grandtotalnominal'] 				= '0';
		$data['totalnominal'] 				= '0';
		$data['tgl_trx'] 				= '';
		$data['tgl_trx2'] 				= '';
		$data['st_kbo'] 				= '';
		if ($disabel!='0'){
			$data['disabel'] 				= 'disabled';
			
		}else{
			$data['disabel'] 				= '';
		}
		$data['error'] 				= '';
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		
		if ($tipe=='1'){
			$data['title'] 				= 'Rekapitulasi Piutang <h3><span class="label label-primary">Rawat Jalan</span></h3>';
			$data['content'] 			= 'Tklaim_rincian/detail_rincian';
		}else{
			$data['title'] 				= 'Rekapitulasi Piutang <h3><span class="label label-success">Rawat Inap</span></h3>';
			$data['content'] 			= 'Tklaim_rincian/detail_rincian_ri';
		}
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Klaim Rincian",'tKlaim Rincian/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function cek_date(){
		$tgl='2011-20-01';
		$tgl_asli=(int)substr($tgl,-2);
		print_r($tgl_asli);
	}
	
	public function verifikasi()
    {
		
		$where='';
        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $tipekontraktor = $this->input->post('tipekontraktor');
        $idkontraktor = $this->input->post('idkontraktor');
        $tanggal_jt =YMDFormat($this->input->post('tanggal_jt'));
		$tgl_angka=(int)substr($tanggal_jt,-2);
		// if ($tipe='1'){
			if ($tipekontraktor=='1'){
				$where .=" AND  K.idrekanan='$idkontraktor'";
			}
			$q="SELECT K.id from tklaim K
				WHERE K.tipe='$tipe' AND K.idkelompokpasien='$tipekontraktor'  AND K.tanggal_tagihan='$tanggal_jt' AND status=1 ".$where;
			$klaim_id=$this->db->query($q)->row('id');
			if ($klaim_id==''){
				if ($tipekontraktor=='1'){
				$q_set="SELECT * FROM (
						SELECT '1' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mrekanan_setting M
						WHERE M.idrekanan='$idkontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

						UNION ALL

						SELECT '2' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mpasien_kelompok_setting M
						WHERE M.idkelompokpasien='$tipekontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

						) T ORDER BY T.tipe ASC LIMIT 1";
				}else{
					$q_set="SELECT * FROM (
						SELECT '2' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mpasien_kelompok_setting M
						WHERE M.idkelompokpasien='$tipekontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

						) T ORDER BY T.tipe ASC LIMIT 1";
				}
				$row=$this->db->query($q_set)->row();
				if ($row){
					$var_batas_kirim=$row->batas_kirim;
					$var_jml_hari=$row->jml_hari;
					
					$date=date_create($tanggal_jt);
					date_add($date,date_interval_create_from_date_string("$var_batas_kirim days"));
					$batas_kirim = date_format($date,"Y-m-d");
					
					$date2=date_create($tanggal_jt);
					date_add($date2,date_interval_create_from_date_string("$var_jml_hari days"));
					$jatuh_tempo_bayar = date_format($date2,"Y-m-d");
					
					
					// $batas_kirim
				}else{
					$batas_kirim='';
					$jatuh_tempo_bayar='';
				}
				
				$data_info=array(
					'tipe' =>$tipe,
					'idkelompokpasien' =>$tipekontraktor,
					'idrekanan' =>$idkontraktor,
					'idrekanan' =>$idkontraktor,
					'tanggal_tagihan' =>$tanggal_jt,
					'batas_kirim' =>$batas_kirim,
					'jatuh_tempo_bayar' =>$jatuh_tempo_bayar,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_user_id' =>$this->session->userdata('user_id'),
					'created_user' =>$this->session->userdata('user_name'),
					'status' =>'1',
				);
				// print_r($data_info);
				$this->db->insert('tklaim', $data_info);
				$klaim_id=$this->db->insert_id();
			}
			$data_det=array(
					'klaim_id' =>$klaim_id,
					'tipe' =>$tipe,
					'pembayaran_id' =>$id,
					'user_verifikasi'=>$this->session->userdata('user_name'),
					'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					'tanggal_tagihan' =>$tanggal_jt,
				);
			$result=$this->db->insert('tklaim_detail', $data_det);
			
		// }

		

        // $data =array(
            // 'st_verifikasi_piutang'=>'1',
			// 'user_verifikasi'=>$this->session->userdata('user_name'),
            // 'tanggal_verifikasi'=>date('Y-m-d H:i:s')
        // );
        // $this->db->where('id', $id);
		// if ($tipe=='1'){
			// $result=$this->db->update('tkasir_pembayaran', $data);			
		// }else{
			// $result=$this->db->update('trawatinap_tindakan_pembayaran_detail', $data);
		// }
        if ($result) {
            $this->output->set_output(json_encode($data_info));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	function list_tanggal($idkelompokpasien='1',$idrekanan='#',$tipe='1',$st_multiple='1',$idpasien=''){
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$st_multiple=$this->input->post('st_multiple');
		$idpasien=$this->input->post('idpasien');
		// print_r($idkelompokpasien);exit();
		$st_cari='2';
		if ($idkelompokpasien=='1'){
			$q_cari="SELECT COUNT(S.id) as jml FROM mrekanan_setting S WHERE S.idrekanan='$idrekanan' AND S.`status`='1'";
			$hasil=$this->db->query($q_cari)->row('jml');
			if ($hasil > 0){
				$st_cari='1';
			}
		}
		if ($st_cari=='1'){//FOKUS KE SETTING REKANAN
			$q="SELECT DATE_FORMAT(T.tanggal_next,'%d-%m-%Y') as tanggal_next FROM (SELECT TS.*,TK.tanggal_tagihan FROM (
					SELECT '1' as tipe,RS.tanggal_hari 
									,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl
									,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH) ELSE (SELECT tgl) END as tanggal_next
									from mrekanan_setting RS 
									WHERE RS.idrekanan=$idrekanan AND RS.`status`='1' AND RS.tipe='$tipe'
					) TS LEFT JOIN tklaim TK ON TS.tanggal_next=TK.tanggal_tagihan AND TK.idrekanan='$idrekanan' AND TK.tipe='$tipe' AND TK.`status` > 1
					ORDER BY TS.tanggal_next ASC
				) T WHERE T.tanggal_tagihan IS NULL";
		}else{//FOKUS KE SETTING KELOMPOK PASIEN
			if ($st_multiple=='1'){
				$q="SELECT DATE_FORMAT(T.tanggal_next,'%d-%m-%Y') as tanggal_next FROM (SELECT TS.*,TK.tanggal_tagihan
						FROM (
						SELECT '2' as tipe,KS.tanggal_hari 
						,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(KS.tanggal_hari,2,'0')) as tgl
						,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH) ELSE (SELECT tgl) END as tanggal_next
						FROM mpasien_kelompok_setting KS 
						WHERE KS.idkelompokpasien='$idkelompokpasien' AND KS.`status`='1' AND KS.tipe='$tipe'
					) TS  LEFT JOIN tklaim TK ON TS.tanggal_next=TK.tanggal_tagihan AND TK.idkelompokpasien='$idkelompokpasien' AND TK.tipe='$tipe' AND TK.`status` > 1
					ORDER BY TS.tanggal_next ASC
				) T WHERE T.tanggal_tagihan IS NULL";
			}else{
				$q="SELECT DATE_FORMAT(T.tanggal_next,'%d-%m-%Y') as tanggal_next FROM (SELECT TS.*,TK.tanggal_tagihan
						FROM (
						SELECT '2' as tipe,KS.tanggal_hari 
						,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(KS.tanggal_hari,2,'0')) as tgl
						,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH) ELSE (SELECT tgl) END as tanggal_next
						FROM mpasien_kelompok_setting KS 
						WHERE KS.idkelompokpasien='$idkelompokpasien' AND KS.`status`='1' AND KS.tipe='$tipe'
					) TS  LEFT JOIN tklaim TK ON TS.tanggal_next=TK.tanggal_tagihan AND TK.idkelompokpasien='$idkelompokpasien' AND TK.tipe='$tipe' AND TK.`status` > 1 AND TK.idpasien='$idpasien'
					ORDER BY TS.tanggal_next ASC
				) T WHERE T.tanggal_tagihan IS NULL";

			}
		}
		// print_r($q);exit();
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->tanggal_next.'">'.$r->tanggal_next.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function get_data_kirim($id){
		
		$arr['detail']=$this->db->query("SELECT noresi,keterangan,date_format(tanggal_kirim,'%d-%m-%Y')as tanggal_kirim FROM tklaim WHERE id='$id'")->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function get_st_multiple(){
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		
		$arr['st_multiple']=$this->db->query("SELECT M.st_multiple FROM mpasien_kelompok M WHERE M.id='$idkelompokpasien'")->row('st_multiple');
		// print_r($idkelompokpasien);exit();
		$this->output->set_output(json_encode($arr));
	}
	function edit_tanggal_all(){		
		$id=$this->input->post('klaim_id');
		$tanggal_tagihan=YMDFormat($this->input->post('tanggal_tagihan'));
		$tgl_asli=(int)substr($tanggal_tagihan,-2);
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$st_multiple=$this->input->post('st_multiple');
		$idpasien=$this->input->post('idpasien');
		$nama_asuransi='';
		if ($idkelompokpasien=='1'){
			$nama_asuransi=$this->db->query("SELECT nama from mrekanan WHERE id='$idrekanan'")->row('nama');
		}else{
			$nama_asuransi=$this->db->query("SELECT nama from mpasien_kelompok WHERE id='$idkelompokpasien'")->row('nama');
		}
		$where='';
		$id_setting='';
		if ($idkelompokpasien=='1'){
			$q_cari="SELECT *FROM mrekanan_setting S WHERE S.idrekanan='$idrekanan' AND S.`status`='1' AND tanggal_hari='$tgl_asli'";
			$hasil=$this->db->query($q_cari)->row();
			if ($hasil){
				$id_setting=$hasil->id;
				$var_batas_kirim=$hasil->batas_kirim;
				$var_jml_hari=$hasil->jml_hari;
			}
		}
		if ($id_setting==''){
			$q_cari="SELECT *FROM mpasien_kelompok_setting S WHERE S.idkelompokpasien='$idkelompokpasien' AND S.`status`='1' AND tanggal_hari='$tgl_asli'";
			$hasil=$this->db->query($q_cari)->row();
			if ($hasil){
				$id_setting=$hasil->id;
				$var_batas_kirim=$hasil->batas_kirim;
				$var_jml_hari=$hasil->jml_hari;
			}
		}
		
		if ($idkelompokpasien=='1'){
				$where .=" AND  K.idrekanan='$idrekanan'";
		}
		if ($st_multiple=='2'){
			$where .=" AND  K.st_multiple='$st_multiple' AND  K.idpasien='$idpasien'";
		}else{
			$where .=" AND  K.st_multiple='$st_multiple' ";
			$idpasien=null;
		}
		$q="SELECT K.id from tklaim K
				WHERE K.tipe='$tipe' AND K.idkelompokpasien='$idkelompokpasien'  AND K.tanggal_tagihan='$tanggal_tagihan' AND status=1 ".$where;
		$klaim_id=$this->db->query($q)->row('id');
		$detail_klaim=$this->db->query("SELECT tklaim_detail.*,tklaim.idkelompokpasien,tklaim.idrekanan from tklaim_detail LEFT JOIN tklaim ON tklaim.id=tklaim_detail.klaim_id where tklaim_detail.klaim_id='$id'")->result();
		$date=date_create($tanggal_tagihan);
		date_add($date,date_interval_create_from_date_string("$var_batas_kirim days"));
		$batas_kirim = date_format($date,"Y-m-d");
		
		$date2=date_create($tanggal_tagihan);
		date_add($date2,date_interval_create_from_date_string("$var_jml_hari days"));
		$jatuh_tempo_bayar = date_format($date2,"Y-m-d");
		
		if ($klaim_id==''){//JIKA TIDAK ADA
			$data_info=array(
					'tipe' =>$tipe,
					'idkelompokpasien' =>$idkelompokpasien,
					'idrekanan' =>$idrekanan,
					'st_multiple' =>$st_multiple,
					'idpasien' =>$idpasien,
					'tanggal_tagihan' =>$tanggal_tagihan,
					'batas_kirim' =>$batas_kirim,
					'jatuh_tempo_bayar' =>$jatuh_tempo_bayar,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_user_id' =>$this->session->userdata('user_id'),
					'created_user' =>$this->session->userdata('user_name'),
					'status' =>'1',
				);
				// print_r($data_info);
				$this->db->insert('tklaim', $data_info);
				$klaim_id=$this->db->insert_id();
		}
		foreach ($detail_klaim as $r){
			$this->db->query("DELETE FROM tklaim_detail WHERE id='".$r->id."'");
			if ($idrekanan=='#'){
				$idrekanan='';
			}
			if ($r->tipe=='1'){
				$xnama_asuransi='Pembayaran Asuransi :'.$nama_asuransi.' [edited]';
				$this->db->query("UPDATE tkasir_pembayaran set tipekontraktor='$idkelompokpasien',idkontraktor='$idrekanan',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
			}else{
				$xnama_asuransi='Perusahaan Asuransi :'.$nama_asuransi.' [edited]';
				$this->db->query("UPDATE trawatinap_tindakan_pembayaran_detail set tipekontraktor='$idkelompokpasien',idkontraktor='$idrekanan',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
				
			}
			//INSERT HISTORY
			$data_his=array(
					'tklaim_id' =>$r->klaim_id,
					'tklaim_id_detail' =>$r->id,
					'pembayaran_id' =>$r->pembayaran_id,
					'st_multiple' =>$r->st_multiple,
					'idpasien' =>$r->idpasien,
					'tipe' =>$tipe,
					'idkelompokpasien_asal' =>$r->idkelompokpasien,
					'idrekanan_asal' =>$r->idrekanan,
					'idkelompokpasien' =>$idkelompokpasien,
					'idrekanan' =>$idrekanan,
					'user_nama' =>$this->session->userdata('user_name'),
					'tanggal_pindah' =>date('Y-m-d H:i:s'),
					'ket' =>'Pindah Satuan',
				);
				$this->db->insert('tklaim_history', $data_his);
			
			
			$data_det=array(
					'klaim_id' =>$klaim_id,
					'tipe' =>$tipe,
					'pembayaran_id' =>$r->pembayaran_id,
					'user_verifikasi'=>$this->session->userdata('user_name'),
					'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					'tanggal_tagihan' =>$tanggal_tagihan,
				);
			$result=$this->db->insert('tklaim_detail', $data_det);
		}
	}
	function edit_tanggal_satu(){		
		$id=$this->input->post('klaim_id');
		$tanggal_tagihan=YMDFormat($this->input->post('tanggal_tagihan'));
		$tgl_asli=(int)substr($tanggal_tagihan,-2);
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$st_multiple=$this->input->post('st_multiple');
		$idpasien=$this->input->post('idpasien');
		$nama_asuransi='';
		if ($idkelompokpasien=='1'){
			$nama_asuransi=$this->db->query("SELECT nama from mrekanan WHERE id='$idrekanan'")->row('nama');
		}else{
			$nama_asuransi=$this->db->query("SELECT nama from mpasien_kelompok WHERE id='$idkelompokpasien'")->row('nama');
		}
		$where='';
		$id_setting='';
		if ($idkelompokpasien=='1'){
			$q_cari="SELECT *FROM mrekanan_setting S WHERE S.idrekanan='$idrekanan' AND S.`status`='1' AND tanggal_hari='$tgl_asli'";
			$hasil=$this->db->query($q_cari)->row();
			if ($hasil){
				$id_setting=$hasil->id;
				$var_batas_kirim=$hasil->batas_kirim;
				$var_jml_hari=$hasil->jml_hari;
			}
		}
		if ($id_setting==''){
			$q_cari="SELECT *FROM mpasien_kelompok_setting S WHERE S.idkelompokpasien='$idkelompokpasien' AND S.`status`='1' AND tanggal_hari='$tgl_asli'";
			$hasil=$this->db->query($q_cari)->row();
			if ($hasil){
				$id_setting=$hasil->id;
				$var_batas_kirim=$hasil->batas_kirim;
				$var_jml_hari=$hasil->jml_hari;
			}
		}
		
		if ($idkelompokpasien=='1'){
				$where .=" AND  K.idrekanan='$idrekanan'";
		}
		if ($st_multiple=='2'){
			$where .=" AND  K.st_multiple='$st_multiple' AND  K.idpasien='$idpasien'";
		}else{
			$where .=" AND  K.st_multiple='$st_multiple' ";
			$idpasien=null;
		}
		$q="SELECT K.id from tklaim K
				WHERE K.tipe='$tipe' AND K.idkelompokpasien='$idkelompokpasien'  AND K.tanggal_tagihan='$tanggal_tagihan' AND status=1 ".$where;
		$klaim_id=$this->db->query($q)->row('id');
		$detail_klaim=$this->db->query("SELECT tklaim_detail.*,tklaim.idkelompokpasien,tklaim.idrekanan from tklaim_detail LEFT JOIN tklaim ON tklaim.id=tklaim_detail.klaim_id where tklaim_detail.id='$id'")->result();
		$date=date_create($tanggal_tagihan);
		date_add($date,date_interval_create_from_date_string("$var_batas_kirim days"));
		$batas_kirim = date_format($date,"Y-m-d");
		
		$date2=date_create($tanggal_tagihan);
		date_add($date2,date_interval_create_from_date_string("$var_jml_hari days"));
		$jatuh_tempo_bayar = date_format($date2,"Y-m-d");
		
		if ($klaim_id==''){//JIKA TIDAK ADA
				$data_info=array(
					'tipe' =>$tipe,
					'idkelompokpasien' =>$idkelompokpasien,
					'idrekanan' =>$idrekanan,
					'st_multiple' =>$st_multiple,
					'idpasien' =>$idpasien,
					'tanggal_tagihan' =>$tanggal_tagihan,
					'batas_kirim' =>$batas_kirim,
					'jatuh_tempo_bayar' =>$jatuh_tempo_bayar,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_user_id' =>$this->session->userdata('user_id'),
					'created_user' =>$this->session->userdata('user_name'),
					'status' =>'1',
				);
				// print_r($data_info);
				$this->db->insert('tklaim', $data_info);
				$klaim_id=$this->db->insert_id();
		}
		foreach ($detail_klaim as $r){
			$this->db->query("DELETE FROM tklaim_detail WHERE id='".$r->id."'");
			if ($idrekanan=='#'){
				$idrekanan='';
			}
			if ($r->tipe=='1'){
				$xnama_asuransi='Pembayaran Asuransi :'.$nama_asuransi.' [edited]';
				$this->db->query("UPDATE tkasir_pembayaran set tipekontraktor='$idkelompokpasien',idkontraktor='$idrekanan',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
			}else{
				$xnama_asuransi='Perusahaan Asuransi :'.$nama_asuransi.' [edited]';
				$this->db->query("UPDATE trawatinap_tindakan_pembayaran_detail set tipekontraktor='$idkelompokpasien',idkontraktor='$idrekanan',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
				
			}
			//INSERT HISTORY
			$data_his=array(
					'tklaim_id' =>$r->klaim_id,
					'tklaim_id_detail' =>$r->id,
					'pembayaran_id' =>$r->pembayaran_id,
					'st_multiple' =>$r->st_multiple,
					'idpasien' =>$r->idpasien,
					'tipe' =>$tipe,
					'idkelompokpasien_asal' =>$r->idkelompokpasien,
					'idrekanan_asal' =>$r->idrekanan,
					'idkelompokpasien' =>$idkelompokpasien,
					'idrekanan' =>$idrekanan,
					'user_nama' =>$this->session->userdata('user_name'),
					'tanggal_pindah' =>date('Y-m-d H:i:s'),
					'ket' =>'Pindah Satuan',
				);
				$this->db->insert('tklaim_history', $data_his);
			
			$data_det=array(
					'klaim_id' =>$klaim_id,
					'tipe' =>$tipe,
					'pembayaran_id' =>$r->pembayaran_id,
					'user_verifikasi'=>$this->session->userdata('user_name'),
					'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					'tanggal_tagihan' =>$tanggal_tagihan,
				);
			$result=$this->db->insert('tklaim_detail', $data_det);
		}
	}
	function stop(){
		$id=$this->input->post('id');
		$result=$this->db->query("update tklaim SET status='2' WHERE id='$id'");
		$this->output->set_output(json_encode($result));
	}
	function buka(){
		$id=$this->input->post('id');
		$result=$this->db->query("update tklaim SET status='1' WHERE id='$id'");
		$this->output->set_output(json_encode($result));
	}
	function test_validasi_pendapatanRajal($id){
		$q="SELECT H.pendaftaran_id,H.kasir_id,H.tipe from tklaim_detail H WHERE H.klaim_id='$id'";
		$row=$this->db->query($q)->result();
		foreach($row as $r){
			if ($r->tipe=='1'){
				$this->Tvalidasi_model->GenerateValidasiPendapatanRajal($r->pendaftaran_id,$r->kasir_id,'2');				
			}
		}
	}
	function kirim(){
		$id=$this->input->post('id');
		$tanggal_kirim=YMDFormat($this->input->post('tanggal_kirim'));
		$noresi=$this->input->post('noresi');
		$keterangan=$this->input->post('keterangan');
		$q="SELECT D.pendaftaran_id,D.kasir_id,D.tipe FROM tklaim H
			LEFT JOIN tklaim_detail D ON D.klaim_id=H.id
			WHERE H.id='$id' AND H.status_kirim='0'";
		$row=$this->db->query($q)->result();
		foreach($row as $r){
			if ($r->tipe=='1'){
				$this->Tvalidasi_model->GenerateValidasiPendapatanRajal($r->pendaftaran_id,$r->kasir_id,'2',$tanggal_kirim);				
			}
			if ($r->tipe=='2'){
				// print_r($r);exit();
				$this->Tvalidasi_model->GenerateValidasiPendapatanRanap($r->pendaftaran_id,$r->kasir_id,'2',$tanggal_kirim);				
			}
		}
		// $this->Tvalidasi_model->GenerateValidasiPendapatanRajal();
		
		$data=array(
			'status_kirim' =>'1',
			'status' =>'3',
			'noresi' =>$noresi,
			'tanggal_kirim' =>$tanggal_kirim,
			'keterangan' =>$keterangan,			
			'kirim_user' =>$this->session->userdata('user_name'),
			'kirim_date' =>date('Y-m-d H:i:s'),
		);
		$this->db->where('id', $id);
		$result=$this->db->update('tklaim', $data);		
		$this->output->set_output(json_encode($result));
	}
	public function print_kwitansi($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

        $row = $this->model->Get_Kwitansi($id);
        if ($row->tipe=='1'){
			$deskripsi="Biaya Rawat Jalan Pasien".$row->nama.", Perincian Terlampir";
		}else{
			$deskripsi="Biaya Rawat Inap Pasien".$row->nama.", Perincian Terlampir";
			
		}
	
        $data = array(
            'id' => $row->id,
            'tanggal' => date('Y-m-d H:i:s'),
            'no_kwitansi' => $row->no_klaim,
            'sudahterimadari' => $row->nama,
            'deskripsi' => $deskripsi,
            'nominal' => $row->total_tagihan,
            'userinput' => $this->session->userdata('user_name'),
            'status' => 1,
        );

        $html = $this->load->view('Tklaim_rincian/kwitansi', $data, true);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Kwitansi.pdf', array("Attachment"=>0));
    }
	public function print_kwitansi_detail($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

        $row = $this->model->Get_KwitansiDetail($id);
        if ($row->tipe=='1'){
			$deskripsi="Biaya Rawat Jalan Pasien ".$row->namapasien." (".$row->nama_perusahaan."), Perincian Terlampir";
		}else{
			$deskripsi="Biaya Rawat Inap Pasien ".$row->namapasien." (".$row->nama_perusahaan."), Perincian Terlampir";
			
		}
	
        $data = array(
            // 'id' => $row->id,
            'tanggal' => date('Y-m-d H:i:s'),
            'no_kwitansi' => $row->no_klaim,
            'no_invoice' =>'KWR-'.$row->nopendaftaran,
            'sudahterimadari' => $row->nama_perusahaan,
            'deskripsi' => $deskripsi,
            'nominal' => $row->nominal,
            'userinput' => $this->session->userdata('user_name'),
            'status' => 1,
        );
		// print_r($data);exit();
        $html = $this->load->view('Tklaim_rincian/kwitansi_detail', $data, true);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Kwitansi.pdf', array("Attachment"=>0));
    }
	public function print_surat($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        
		$row = $this->model->Get_Kwitansi($id);
		// print_r($row);exit;
        $data = array(
          'nosurat' => $row->no_klaim,
          'namapasien' => $row->namapasien,
          'idpasien' => $row->idpasien,
          'namakontraktor' => $row->nama,
          'tagihanrekanan' => $row->total_tagihan,
          'tipe' => $row->tipe,
		  'userinput' => $this->session->userdata('user_name'),
        );

        $html = $this->load->view('Tklaim_rincian/surat_tagihan', $data, true);

        $dompdf->loadHtml($html);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Tagihan.pdf', array("Attachment"=>0));
    }
	public function print_surat_detail($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        
		$row = $this->model->Get_KwitansiDetail($id);
        $data = array(
          'nosurat' => $row->no_klaim,
		  'no_invoice' =>'KWR-'.$row->nopendaftaran,
          'namakontraktor' => $row->nama_perusahaan,
          'namapasien' => $row->namapasien,
          'tagihanrekanan' => $row->nominal,
          'tipe' => $row->tipe,
		  'userinput' => $this->session->userdata('user_name'),
        );

        $html = $this->load->view('Tklaim_rincian/surat_tagihan_detail', $data, true);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Tagihan.pdf', array("Attachment"=>0));
    }
	public function print_rekap($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        
		$row = $this->model->Get_Kwitansi($id);
        $data = array(
          'nosurat' => $row->no_klaim,
          'namakontraktor' => $row->nama,
          'tagihanrekanan' => $row->total_tagihan,
          'tipe' => $row->tipe
        );
		$data['list_detail']=$this->model->detail_rajal($id);
        $html = $this->load->view('Tklaim_rincian/print_rekap_rajal', $data, true);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Tagihan.pdf', array("Attachment"=>0));
    }
	public function print_history_tagihan($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        
		$data = $this->model->Get_KwitansiHistory($id);
        $data['userinput']= $this->session->userdata('user_name');
		// print_r($data);exit();
		$data['list_detail']=$this->model->detail_history($id,$data['idkelompokpasien'],$data['idrekanan']);
        $html = $this->load->view('Tklaim_rincian/print_rekap_history', $data, true);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('History Rincian Tagihan.pdf', array("Attachment"=>0));
    }
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/klaim_resi/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'tklaim_resi';

                    $data = array();
                    $data['klaim_id']  = $this->input->post('idtransaksi');
                    $data['user_upload']  = $this->session->userdata('user_name');
                    $data['tanggal_upload']  = date('Y-m-d H:i:s');
                    $data['file_name'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	function hapus_file(){
		$id=$this->input->post('id');
		$row =$this->db->query("select file_name from tklaim_resi where id='$id'")->row();
		if(file_exists('./assets/upload/klaim_resi/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/klaim_resi/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from tklaim_resi WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
}
