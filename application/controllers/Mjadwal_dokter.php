<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mjadwal_dokter extends CI_Controller
{
    /**
     * Jadwal Dokter controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mjadwal_dokter_model', 'model');
    }

    public function index()
    {
        $data = array(
          'idpoliklinik' => '',
          'iddokter' => '',
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Jadwal Dokter';
        $data['content'] 		= 'Mjadwal_dokter/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Jadwal Dokter",'#'),
            array("List",'mjadwal_dokter')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	

    public function filter()
    {
        $data = array(
          'idpoliklinik' => $this->input->post('idpoliklinik'),
          'iddokter' => $this->input->post('iddokter'),
        );

        $this->session->set_userdata($data);

        $data['error'] 			= '';
        $data['title'] 			= 'Jadwal Dokter';
        $data['content'] 		= 'Mjadwal_dokter/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Jadwal Dokter",'#'),
            array("List",'mjadwal_dokter')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {
        $data = array(
            'id' => '',
            'nama' => '',
            'estimasi' => '',
            'status' => ''
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Tambah Jadwal Dokter';
        $data['content'] 		= 'Mjadwal_dokter/manage';
        $data['breadcrum']	= array(
            array("RSKB Halmahera",'#'),
            array("Jadwal Dokter",'#'),
            array("Tambah",'mjadwal_dokter')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id, $idpoliklinik, $idjadwal='')
    {
        if ($id != '') {
            $row = $this->model->getSpecified($id, $idpoliklinik, $idjadwal);
            if (isset($row->id)) {

                if ($idjadwal) {
                  $data = array(
                      'idjadwal' => $row->idjadwal,
                      'idpoliklinik' => $row->idpoliklinik,
                      'poliklinik' => $row->poliklinik,
                      'iddokter' => $row->id,
                      'namadokter' => $row->nama,
                      'hari' => $row->hari,
                      'jam_dari' => $row->jam_dari,
                      'jam_sampai' => $row->jam_sampai,
                      'idruangan' => $row->idruangan,
                      'keterangan' => $row->keterangan,
                  );
                } else {
                  $data = array(
                      'idjadwal' => '',
                      'idpoliklinik' => $row->idpoliklinik,
                      'poliklinik' => $row->poliklinik,
                      'iddokter' => $row->id,
                      'namadokter' => $row->nama,
                      'hari' => '',
                      'jam_dari' => '',
                      'jam_sampai' => '',
                      'idruangan' => '',
                      'keterangan' => '',
                  );
                }

                $data['error'] 			= '';
                $data['title'] 			= 'Ubah Jadwal Dokter '.$row->nama;
                $data['content']	 	= 'Mjadwal_dokter/manage';
                $data['breadcrum'] 	= array(
                    array("RSKB Halmahera",'#'),
                    array("Jadwal Dokter",'#'),
                    array("Ubah",'mjadwal_dokter')
                );

                $data['list_jadwal'] = $this->model->getJadwalDokter($row->id, $row->idpoliklinik);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mjadwal_dokter', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mjadwal_dokter');
        }
    }
	public function manage_slot($id, $idpoliklinik, $idjadwal='')
    {
        if ($id != '') {
            $row = $this->model->getSpecified($id, $idpoliklinik, $idjadwal);
            if (isset($row->id)) {

                if ($idjadwal) {
                  $data = array(
                      'idjadwal' => $row->idjadwal,
                      'idpoliklinik' => $row->idpoliklinik,
                      'poliklinik' => $row->poliklinik,
                      'iddokter' => $row->id,
                      'namadokter' => $row->nama,
                      'hari' => $row->hari,
                      'jam_dari' => $row->jam_dari,
                      'jam_sampai' => $row->jam_sampai,
                      'idruangan' => $row->idruangan,
                      'keterangan' => $row->keterangan,
                  );
                } else {
                  $data = array(
                      'idjadwal' => '',
                      'idpoliklinik' => $row->idpoliklinik,
                      'poliklinik' => $row->poliklinik,
                      'iddokter' => $row->id,
                      'namadokter' => $row->nama,
                      'hari' => '',
                      'jam_dari' => '',
                      'jam_sampai' => '',
                      'idruangan' => '',
                      'keterangan' => '',
                  );
                }

                $data['error'] 			= '';
                $data['title'] 			= 'Ubah Slot Harian Dokter '.$row->nama;
                $data['content']	 	= 'Mjadwal_dokter/manage_slot';
                $data['breadcrum'] 	= array(
                    array("RSKB Halmahera",'#'),
                    array("Slot Harian Dokter",'#'),
                    array("Ubah",'mjadwal_dokter')
                );

                $data['list_jadwal'] = $this->model->getJadwalDokter($row->id, $row->idpoliklinik);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mjadwal_dokter', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mjadwal_dokter');
        }
    }
	
    public function save()
    {
        if ($this->model->saveData()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mjadwal_dokter/update/' . $this->input->post('iddokter'). '/' . $this->input->post('idpoliklinik'), 'location');
        }
    }
	public function save_slot()
    {
        if ($this->model->save_slot()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mjadwal_dokter/manage_slot/' . $this->input->post('iddokter'). '/' . $this->input->post('idpoliklinik'), 'location');
        }
    }

    public function delete($iddokter, $idjadwal)
    {
        if ($this->model->softDelete($idjadwal)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah dihapus.');
            redirect('mjadwal_dokter/update/'. $iddokter.'/'.$idjadwal, 'location');
        }
    }
	public function delete_detail($iddokter,$idpoliklinik, $idjadwal)
    {
        if ($this->model->hardDelete($idjadwal)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah dihapus.');
            redirect('mjadwal_dokter/update/'. $iddokter.'/'.$idpoliklinik, 'location');
        }
    }

    public function getIndex($uri='')
    {
        $prefix = "<label class='label label-primary'>";
        $suffix = "</label>";

        $this->select = array(
          'mdokter.id AS iddokter',
          'mdokter.nama AS dokter',
          'mpoliklinik.id AS idpoliklinik',
          'mpoliklinik.nama AS poliklinik',
          'GROUP_CONCAT(CASE
              WHEN mjadwal_dokter.hari = "Senin" AND mjadwal_dokter.status=1 THEN
                CONCAT("' . $prefix . '", mjadwal_dokter.jam_dari, " - ", mjadwal_dokter.jam_sampai, "' . $suffix . '", "<br><br>")
              ELSE ""
          END SEPARATOR "") AS senin',

          'GROUP_CONCAT(CASE
              WHEN mjadwal_dokter.hari = "Selasa" AND mjadwal_dokter.status=1 THEN
                CONCAT("' . $prefix . '", mjadwal_dokter.jam_dari, " - ", mjadwal_dokter.jam_sampai, "' . $suffix . '", "<br><br>")
              ELSE ""
          END SEPARATOR "") AS selasa',

          'GROUP_CONCAT(CASE
              WHEN mjadwal_dokter.hari = "Rabu" AND mjadwal_dokter.status=1 THEN
                CONCAT("' . $prefix . '", mjadwal_dokter.jam_dari, " - ", mjadwal_dokter.jam_sampai, "' . $suffix . '", "<br><br>")
              ELSE ""
          END SEPARATOR "") AS rabu',

          'GROUP_CONCAT(CASE
              WHEN mjadwal_dokter.hari = "Kamis" AND mjadwal_dokter.status=1 THEN
                CONCAT("' . $prefix . '", mjadwal_dokter.jam_dari, " - ", mjadwal_dokter.jam_sampai, "' . $suffix . '", "<br><br>")
              ELSE ""
          END SEPARATOR "") AS kamis',

          'GROUP_CONCAT(CASE
              WHEN mjadwal_dokter.hari = "Jumat" AND mjadwal_dokter.status=1 THEN
                CONCAT("' . $prefix . '", mjadwal_dokter.jam_dari, " - ", mjadwal_dokter.jam_sampai, "' . $suffix . '", "<br><br>")
              ELSE ""
          END SEPARATOR "") AS jumat',

          'GROUP_CONCAT(CASE
              WHEN mjadwal_dokter.hari = "Sabtu" AND mjadwal_dokter.status=1 THEN
                CONCAT("' . $prefix . '", mjadwal_dokter.jam_dari, " - ", mjadwal_dokter.jam_sampai, "' . $suffix . '", "<br><br>")
              ELSE ""
          END SEPARATOR "") AS sabtu',

          'GROUP_CONCAT(CASE
              WHEN mjadwal_dokter.hari = "Minggu" AND mjadwal_dokter.status=1 THEN
                CONCAT("' . $prefix . '", mjadwal_dokter.jam_dari, " - ", mjadwal_dokter.jam_sampai, "' . $suffix . '", "<br><br>")
              ELSE ""
          END SEPARATOR "") AS minggu',
        );

        $this->from = 'mdokter';

        $this->join = array(
          array('mpoliklinik_dokter', 'mpoliklinik_dokter.idpoliklinik = mpoliklinik_dokter.idpoliklinik AND mpoliklinik_dokter.iddokter = mdokter.id', ''),
          array('mpoliklinik', 'mpoliklinik.id = mpoliklinik_dokter.idpoliklinik', ''),
          array('mjadwal_dokter', 'mjadwal_dokter.iddokter = mpoliklinik_dokter.iddokter AND mjadwal_dokter.idpoliklinik = mpoliklinik_dokter.idpoliklinik', 'LEFT'),
        );

        $this->order  = array(
          'mdokter.nama' => 'ASC'
        );

        $this->where  = array();
            // $this->where = array_merge($this->where, array('mjadwal_dokter.status' => $this->session->userdata('idpoliklinik')));

        if ($uri == 'filter') {
          if ($this->session->userdata('idpoliklinik') != '#') {
            $this->where = array_merge($this->where, array('mpoliklinik.id' => $this->session->userdata('idpoliklinik')));
          }
          if ($this->session->userdata('iddokter') != '#') {
            $this->where = array_merge($this->where, array('mdokter.id' => $this->session->userdata('iddokter')));
          }
        }

        $this->group  = array('mdokter.id', 'mpoliklinik.id');

        $this->column_search   = array('mdokter.nama, mpoliklinik.nama');

        $this->column_order    = array('mdokter.nama, mpoliklinik.nama');

        $list = $this->datatable->get_datatables();

        $no = $_POST['start'];

        $data = array();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->dokter;
            $row[] = $r->poliklinik;
            $row[] = $r->senin;
            $row[] = $r->selasa;
            $row[] = $r->rabu;
            $row[] = $r->kamis;
            $row[] = $r->jumat;
            $row[] = $r->sabtu;
            $row[] = $r->minggu;
            $row[] = '<div class="btn-group">
							<a href="'.site_url().'mjadwal_dokter/update/'.$r->iddokter.'/'.$r->idpoliklinik.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>
							<a href="'.site_url().'mjadwal_dokter/manage_slot/'.$r->iddokter.'/'.$r->idpoliklinik.'" data-toggle="tooltip" title="Slot Harian" class="btn btn-primary btn-sm"><i class="si si-settings"></i></a>
						</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function display()
    {
        if ( isset($_GET['iddokter']) && isset($_GET['idpoliklinik']) ) {
          $data = array(
            'iddokter' => $_GET['iddokter'],
            'idpoliklinik' => $_GET['idpoliklinik'],
          );
        } else {
          $data = array(
            'iddokter' => '',
            'idpoliklinik' => '',
          );
        }

        $data['error'] 			= '';
        $data['title'] 			= 'Display Jadwal Dokter';
        $data['content'] 		= 'Mjadwal_dokter/display';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Jadwal Dokter",'#'),
            array("Display",'mjadwal_dokter')
        );

        $data['dataDokter'] = $this->model->getScheduleDokter($data['iddokter'], $data['idpoliklinik']);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function slot_harian()
    {
        $data = array(
          'idpol' => '#',
          'iddokter' => '#',
        );
		$tahun=date('Y');
		$bulan=date('m');
		$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D

WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE() AND D.hari='2'";
		$tanggal=$this->db->query($q)->row();
		
		$data['list_dokter'] 			= $this->model->list_dokter();
		$data['list_poli'] 			= $this->model->list_poli();
		$data['idpoli'] 			= '#';
		$data['present'] 			= '2';
		// $data['tanggal'] 			= $tanggal;
		$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
		$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
        $data['error'] 			= '';
        $data['title'] 			= 'Jadwal Dokter Harian';
        $data['content'] 		= 'Mjadwal_dokter/slot_harian';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Jadwal Dokter Slot",'#'),
            array("List",'mjadwal_dokter')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function hasil_generate(){
			$idpoli=$this->input->post('idpoli');
			$iddokter=$this->input->post('iddokter');
		  $tanggaldari=YMDFormat($this->input->post('tanggaldari'));
		  $tanggalsampai=YMDFormat($this->input->post('tanggalsampai'));
		  $where_dokter='';
		  if ($iddokter!='#'){
			  $where_dokter .=" AND H.iddokter='$iddokter'";
		  }
		  if ($idpoli!='#'){
			  $where_dokter .=" AND H.idpoliklinik='$idpoli'";
		  }
		 
		  $q_dokter="SELECT H.iddokter,MD.nama,MD.foto,MD.jeniskelamin,MD.nip,MP.nama as poli,H.idpoliklinik FROM mjadwal_dokter H
				LEFT JOIN mdokter MD ON MD.id=H.iddokter
				LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
				WHERE H.kuota >= 0 ".$where_dokter."
				GROUP BY H.idpoliklinik,H.iddokter";
		  $dokter=$this->db->query($q_dokter)->result();
		  
		  $q="SELECT D.tanggal,D.idpoli,D.iddokter,D.kodehari,D.jadwal_id,CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as jam,D.kuota,D.jumlah_reservasi,D.saldo_kuota,MH.namahari
,CD.sebab,CASE WHEN CD.id IS NOT NULL THEN 1 ELSE 0 END as st_cuti,CD.alasan,D.st_reservasi_online
					FROM app_reservasi_tanggal D
					INNER JOIN mjadwal_dokter MJ ON MJ.id=D.jadwal_id
					LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
					LEFT JOIN mcuti_dokter CD ON D.tanggal BETWEEN CD.tanggal_dari AND CD.tanggal_sampai AND CD.status='1' AND CD.iddokter=D.iddokter
					WHERE (D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai') AND D.reservasi_tipe_id='1'

					ORDER BY D.tanggal ASC
				";
			$record_detail=$this->db->query($q)->result_array();
			
			// $q="SELECT H.id,H.tanggal,H.idpoli,H.jam_id,H.kodehari,H.idpasien,H.namapasien,H.no_medrec,idkelompokpasien
				// ,MK.nama as kelompok_pasien,MR.nama as rekanan
				// FROM `app_reservasi_tanggal_pasien` H
				// LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
				// LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
				// WHERE (H.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai') AND H.idpoli='$idpoli'";
		  // $pasien=$this->db->query($q)->result_array();
		  $q="
				SELECT D.*,MH.namahari,CASE WHEN ML.id IS NOT NULL THEN 1 ELSE 0 END st_libur 
				FROM date_row D 
				LEFT JOIN mholiday ML ON D.tanggal BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MH ON MH.kodehari=D.hari 
				WHERE D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai'";
		  $record=$this->db->query($q)->result();
		  
		  $tabel='<table class="table table table-bordered table-condensed" id="index_ls">';
		  $tabel .='<thead>';
		  $header='<tr>';
				$header .='<td class="text-center column-day" style="width:20%"></td>';
		  foreach($record as $r){
				if ($r->st_libur){
				$header .='<td class="text-center column-libur">'.$r->namahari.'<br>'.DMYFormat2($r->tanggal).'</td>';
					
				}else{
				$header .='<td class="text-center column-day">'.$r->namahari.'<br>'.DMYFormat2($r->tanggal).'</td>';
					
				}
		  }
		  $header .='</tr>';
		  $tabel .='</thead>';
		  $tabel .='<tbody>';
		  
		  $tabel .=$this->generate_detail_ls($dokter,$record_detail,$record,$header);
		  $tabel .='';
		  
		  $tabel .='</tbody>';
		  $tabel .='</tabel>';
		 
		 $arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function generate_detail_ls($dokter,$record_detail,$record,$header){
		// print_r($record_detail);exit;
		$jml_colspan=count($record);
		$tabel ='';
		foreach($dokter as $r){
			$foto='';
			if ($r->foto){
				$foto=$r->foto;
			}else{
				$foto=($r->jeniskelamin=='1'?'default-man.jpeg':'default-woman.jpeg');
				// $foto='default-woman.jpeg';
			}
			$iddokter=$r->iddokter;
			$idpoli=$r->idpoliklinik;
			// $foto=$r->foto;
			$nama=$r->nama;
			$nip=$r->nip;
			$jeniskelamin=$r->jeniskelamin;
			$poli=strtoupper($r->poli);
			$tabel .=$header;
			$tabel .='<tr>';
			$tabel .='<td class="column-poli text-center" colspan="'.($jml_colspan+1).'">'.$poli.'</td>';
								for($i=1;$i<$jml_colspan;$i++){
									$tabel .='<td style="display: none"></td>';
								}
			// $tabel .='';
			$tabel .='</tr>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center ">
							<div class="profile-dockter">
								<div class="image">
                                    <img src="'.base_url().'assets/upload/dokter/'.$foto.'" style="width:30%;height:30%">
                                </div>
								<div class="information">
                                    <div class="name">'.$nama.'</div>
                                    <div class="uniqueId">'.$nip .'</div>
                                </div>
                            </div>
								</td>';
			foreach($record as $tgl){
				$tgl_harian=$tgl->tanggal;
				$list_kuota='';
				$list_saldo='';
				$data_kuota = array_filter($record_detail, function($var) use ($tgl_harian,$idpoli,$iddokter) { 
				return ($var['tanggal'] == $tgl_harian && $var['idpoli'] == $idpoli && $var['iddokter'] == $iddokter);
				});
				$st_reservasi_online=text_default('OFFLINE');
				$tgl_nama="'".$tgl->tanggal."'";
				if ($data_kuota){
					foreach($data_kuota as $data){
						$saldo_kuota=$data['saldo_kuota'];
						if ($data['st_reservasi_online']=='1'){
							$st_reservasi_online=text_warning('ONLINE');
						}
						if ($saldo_kuota<=0){
							$list_saldo='<br>'.text_danger('FULL');
						}else{
							$list_saldo='<br>'.text_primary('SISA KUOTA '.$saldo_kuota);
							
						}
						$jadwal_id=$data['jadwal_id'];
						$class_libur='';
						if ($tgl->st_libur){
							$class_libur=' default ';
						}else{
							
							$class_libur=' success ';
						}
						if ($data['st_cuti']=='1'){
							$list_kuota .='<div class="timetable danger">
									<div class="time">'. strtoupper($data['sebab']).'</div>
									<div class="room"> <br>('.$data['alasan'].')</div>
								</div> ';
						}else{
							
							$list_kuota .='
								<a class="block block-link-hover3" href="javascript:void(0)">
                         
							<div class="timetable '.$class_libur.' " onclick="add_kuota('.$tgl_nama.','.$iddokter.','.$jadwal_id.')" >
									<div class="time">'.$data['jam'].'<br>'.$st_reservasi_online.'</div>
									<div class="room"> <br>('.$data['kuota'].' SLOT)'.$list_saldo.'</div>
								</div> 
                                        </a>
							';
						}
							
					}
				}else{
					$list_kuota .='';
				}
				
				$tabel .='<td class="text-center">'.$list_kuota.'</td>';
				
			}
		$tabel .='</tr>';
		}
		return $tabel;
	}
 function get_kuota_detail(){
	  $iddokter=$this->input->post('iddokter');
	  $tanggal=$this->input->post('tanggal');
	  $jadwal_id=$this->input->post('jadwal_id');
	 $q="
		SELECT H.tanggal,CONCAT(MH.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y')) as periode_nama 
			,CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as jam
			,H.jadwal_id,H.iddokter,H.kuota,H.saldo_kuota,H.jumlah_reservasi,H.catatan,MD.nama as nama_dokter,H.st_reservasi_online,MP.nama as nama_poli
			FROM app_reservasi_tanggal H
			LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
			LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
			LEFT JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
			LEFT JOIN mdokter  MD ON MD.id=MJ.iddokter
			WHERE H.iddokter='$iddokter' AND H.tanggal='$tanggal' AND H.reservasi_tipe_id='1' AND H.jadwal_id='$jadwal_id'
	 ";
	  // print_r($q);exit;
	  $arr=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($arr));
  }
function save_kuota_detail(){
	  $iddokter=$this->input->post('iddokter');
	  $tanggal=$this->input->post('tanggal');
	  $jadwal_id=$this->input->post('jadwal_id');
	  $kuota=$this->input->post('kuota');
	  $st_reservasi_online=$this->input->post('st_reservasi_online');
	  $this->db->where('iddokter',$iddokter);
	  $this->db->where('tanggal',$tanggal);
	  $this->db->where('jadwal_id',$jadwal_id);
	  $data_edit=array(
		'st_edited' =>1,
		'kuota' =>$kuota,
		'st_reservasi_online' =>$st_reservasi_online,
		'edited_name' =>$this->session->userdata('user_name'),
		'edited_date' =>date('Y-m-d H:i:s'),
	  );
	  // print_r($data_edit);exit;
	  $arr=$this->db->update('app_reservasi_tanggal',$data_edit);
	  $this->output->set_output(json_encode($arr));
  }
   function save_catatan_detail(){
	   $iddokter=$this->input->post('iddokter');
	  $tanggal=$this->input->post('tanggal');
	  $jadwal_id=$this->input->post('jadwal_id');
	  $catatan=$this->input->post('catatan');
	  $this->db->where('iddokter',$iddokter);
	  $this->db->where('tanggal',$tanggal);
	  $this->db->where('jadwal_id',$jadwal_id);
	  $data_edit=array(
		'st_edited' =>1,
		'catatan' =>$catatan,
		'edited_name' =>$this->session->userdata('user_name'),
		'edited_date' =>date('Y-m-d H:i:s'),
	  );
	  $arr=$this->db->update('app_reservasi_tanggal',$data_edit);
	  // print_r($arr);exit;
	  $this->output->set_output(json_encode($arr));
  }
  function stop_kuota(){
	   $iddokter=$this->input->post('iddokter');
	  $tanggal=$this->input->post('tanggal');
	  $jadwal_id=$this->input->post('jadwal_id');
	  $q="update app_reservasi_tanggal set app_reservasi_tanggal.kuota=app_reservasi_tanggal.jumlah_reservasi 
WHERE app_reservasi_tanggal.iddokter='$iddokter' AND app_reservasi_tanggal.tanggal='$tanggal' AND app_reservasi_tanggal.jadwal_id='$jadwal_id'";
	  // print_r($q);exit;
	  $arr=$this->db->query($q);
	  $this->output->set_output(json_encode($arr));
  }
}
