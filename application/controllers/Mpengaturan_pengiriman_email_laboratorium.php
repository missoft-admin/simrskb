<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_pengiriman_email_laboratorium extends CI_Controller {

	/**
	 * Pengaturan Form Pengiriman Email Laboratorium controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_pengiriman_email_laboratorium_model');
		$this->load->helper('path');
  }

	function index()
	{
		$row = $this->Mpengaturan_pengiriman_email_laboratorium_model->getSpecified(1);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'judul' => $row->judul,
				'body' => $row->body,
				'footer_pengirim' => $row->footer_pengirim,
				'footer_email' => $row->footer_email,
			);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Form Pengiriman Email Laboratorium';
			$data['content']	 	= 'Mpengaturan_pengiriman_email_laboratorium/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Form Pengiriman Email Laboratorium",'#'),
															array("Ubah",'mpengaturan_pengiriman_email_laboratorium')
														);

			$data['option_file_dikirim'] = $this->Mpengaturan_pengiriman_email_laboratorium_model->getPengaturanFileDikirim(1);
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpengaturan_pengiriman_email_laboratorium/index','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_pengiriman_email_laboratorium_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpengaturan_pengiriman_email_laboratorium/index','location');
		}
	}
}
