<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Executive_dashboard extends CI_Controller {

	/**
	 * Executive Dashboard controller.
	 * Developer @Gunali Rezqi Mauludi
	 */

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Executive_dashboard_model', 'model');
        $this->load->model('munitpelayanan_model', 'unit_pelayanan_model');
		$this->load->model('Tstockopname_model','stokopname_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->helper('path');
	}

	function index() {
		$setting = $this->model->get_settings();
		$data = array(
			"periode_tanggal_pemesanan" => $setting->data_pemesanan,
			"periode_tanggal_penerimaan" => $setting->data_penerimaan,
			"grafik_harian_periode_tanggal_pemesanan" => $setting->grafik_pemesanan_harian,
			"grafik_harian_periode_tanggal_penerimaan" => $setting->grafik_penerimaan_harian,
			"grafik_rekap_periode" => $setting->grafik_pemesanan_rekap,
			"grafik_penerimaan_rekap_periode" => $setting->grafik_penerimaan_rekap,
			"grafik_harian_tipe_barang_periode_tanggal_pemesanan" => $setting->grafik_pemesanan_harian_per_tipe_barang,
			"grafik_harian_tipe_barang_periode_tanggal_penerimaan" => $setting->grafik_penerimaan_harian_per_tipe_barang,
			"grafik_tahunan_periode_tanggal_penerimaan" => explode(',', $setting->grafik_tahunan_periode_tanggal_penerimaan),
			"grafik_tahunan_tipe_barang_periode_tanggal_penerimaan" => explode(',', $setting->grafik_tahunan_tipe_barang_periode_tanggal_penerimaan),
		);
		
		$setting_top_pembelian_obat = $this->model->get_settings_gudang(1);
		$data['setting_top_pembelian_obat_periode'] = $setting_top_pembelian_obat->periode;
		$data['setting_top_pembelian_obat_label'] = $setting_top_pembelian_obat->label;
		$data['setting_top_pembelian_obat_showing'] = $setting_top_pembelian_obat->showing;
		$data['setting_top_pembelian_obat_order'] = $setting_top_pembelian_obat->order;

		$setting_top_pembelian_alkes = $this->model->get_settings_gudang(2);
		$data['setting_top_pembelian_alkes_periode'] = $setting_top_pembelian_alkes->periode;
		$data['setting_top_pembelian_alkes_label'] = $setting_top_pembelian_alkes->label;
		$data['setting_top_pembelian_alkes_showing'] = $setting_top_pembelian_alkes->showing;
		$data['setting_top_pembelian_alkes_order'] = $setting_top_pembelian_alkes->order;

		$setting_top_pembelian_logistik = $this->model->get_settings_gudang(3);
		$data['setting_top_pembelian_logistik_periode'] = $setting_top_pembelian_logistik->periode;
		$data['setting_top_pembelian_logistik_label'] = $setting_top_pembelian_logistik->label;
		$data['setting_top_pembelian_logistik_showing'] = $setting_top_pembelian_logistik->showing;
		$data['setting_top_pembelian_logistik_order'] = $setting_top_pembelian_logistik->order;

		$setting_top_pembelian_implan = $this->model->get_settings_gudang(3);
		$data['setting_top_pembelian_implan_periode'] = $setting_top_pembelian_implan->periode;
		$data['setting_top_pembelian_implan_label'] = $setting_top_pembelian_implan->label;
		$data['setting_top_pembelian_implan_showing'] = $setting_top_pembelian_implan->showing;
		$data['setting_top_pembelian_implan_order'] = $setting_top_pembelian_implan->order;

		$setting_top_pembelian_distributor = $this->model->get_settings_gudang(3);
		$data['setting_top_pembelian_distributor_periode'] = $setting_top_pembelian_distributor->periode;
		$data['setting_top_pembelian_distributor_label'] = $setting_top_pembelian_distributor->label;
		$data['setting_top_pembelian_distributor_showing'] = $setting_top_pembelian_distributor->showing;
		$data['setting_top_pembelian_distributor_order'] = $setting_top_pembelian_distributor->order;

		$data['unit_pelayanan_stokopname'] = $this->unit_pelayanan_model->getDefaultUnitPelayananUser();
		$data['periode_stokopname'] = $this->stokopname_model->list_periode_desc();

		$data['error'] = '';
		$data['title'] = 'Dashboard Reservasi';
		$data['content'] = 'Executive_dashboard/index';
		$data['breadcrum'] 	= array(
			array("RSKB Halmahera",'#'),
			array("Executive Dashboard",'executive_dashboard')
		);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function load_data_pemesanan_gudang()
	{
		$where='';
		
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) >='".YMDFormat($tanggal_dari)."' AND DATE(tgudang_pemesanan.tgl_pemesanan) <='".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) ='".date('Y-m-d')."'";
		}
		
		$this->select = array();
		$from="
			(
				SELECT
					tgudang_pemesanan.id,
					tgudang_pemesanan.nopemesanan AS nomor_pemesanan,
					tgudang_pemesanan.tgl_pemesanan AS tanggal_pemesanan,
					mdistributor.nama AS nama_distributor,
					tgudang_pemesanan.totalbarang AS total_item,
					tgudang_pemesanan.totalharga AS total_nominal
				FROM
					tgudang_pemesanan
				INNER JOIN mdistributor ON tgudang_pemesanan.iddistributor = mdistributor.id 
				WHERE
					tgudang_pemesanan.status IN (2, 3, 4, 5)
					".$where."
			) as tbl  
		";
		
		$this->from = $from;
		$this->join = array();
		
		$this->order = array();
		$this->group = array();
		$this->column_search = array('nomor_pemesanan');
		$this->column_order = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();

		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$result = array();
			
			$result[] = $r->nomor_pemesanan;
			$result[] = $r->tanggal_pemesanan;
			$result[] = $r->nama_distributor;
			$result[] = number_format($r->total_item);
			$result[] = number_format($r->total_nominal);

			$aksi = '<a href="'.base_url().'tgudang_penerimaan/print_data/'.$r->id.'/0" target="_blank" data-toggle="tooltip" title="Cetak" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
			$aksi .= '<a href="'.base_url().'tgudang_penerimaan/detail/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Lihat Rincian" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';

			$result[] = $aksi;

			$data[] = $result;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(true),
			"recordsFiltered" => $this->datatable->count_all(true),
			"data" => $data
		);
		echo json_encode($output);
	}
	
	function load_summary_pemesanan_gudang(){
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) >= '".YMDFormat($tanggal_dari)."' AND DATE(tgudang_pemesanan.tgl_pemesanan) <= '".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) = '".date('Y-m-d')."'";
		}

		$sql = "SELECT
			COUNT(tgudang_pemesanan.id) AS total_item,
			SUM(tgudang_pemesanan.totalharga) AS total_nominal
		FROM
			tgudang_pemesanan
		INNER JOIN mdistributor ON tgudang_pemesanan.iddistributor = mdistributor.id 
		WHERE
			tgudang_pemesanan.status IN (2, 3, 4, 5)".$where;
		$query = $this->db->query($sql);
		$row = $query->row();

		$data = [];
		$data['jumlah_pemesanan_gudang'] = number_format($row->total_item);
		$data['total_nominal_pemesanan_gudang'] = number_format($row->total_nominal);

		$this->output->set_output(json_encode($data));
	}

	function load_grafik_harian_pemesanan_gudang() {
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) >= '".YMDFormat($tanggal_dari)."' AND DATE(tgudang_pemesanan.tgl_pemesanan) <= '".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) = '".date('Y-m-d')."'";
		}

		$sql = "SELECT
			DATE(tgudang_pemesanan.tgl_pemesanan) AS tanggal,
			tgudang_pemesanan.tipepemesanan,
			CASE 
				WHEN tgudang_pemesanan.tipepemesanan = 1 THEN 'NON LOGISTIK'
				WHEN tgudang_pemesanan.tipepemesanan = 2 THEN 'LOGISTIK'
			END AS nama_tipe,
			SUM(tgudang_pemesanan.totalharga) AS total_nominal
		FROM
			tgudang_pemesanan
		WHERE
			tgudang_pemesanan.status IN (2, 3, 4, 5)
			".$where."
		GROUP BY
			DATE(tgudang_pemesanan.tgl_pemesanan), tgudang_pemesanan.tipepemesanan
		ORDER BY
			DATE(tgudang_pemesanan.tgl_pemesanan)";
		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}

	function load_grafik_harian_penerimaan_gudang() {
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) >= '".YMDFormat($tanggal_dari)."' AND DATE(tgudang_penerimaan.tgl_terima) <= '".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) = '".date('Y-m-d')."'";
		}

		$sql = "SELECT
			DATE(tgudang_penerimaan.tgl_terima) AS tanggal,
			tgudang_penerimaan.tipepenerimaan,
			CASE 
				WHEN tgudang_penerimaan.tipepenerimaan = 1 THEN 'NON LOGISTIK'
				WHEN tgudang_penerimaan.tipepenerimaan = 2 THEN 'LOGISTIK'
			END AS nama_tipe,
			SUM(tgudang_penerimaan.totalharga) AS total_nominal
		FROM
			tgudang_penerimaan
		WHERE
			tgudang_penerimaan.status = 1
			".$where."
		GROUP BY
			DATE(tgudang_penerimaan.tgl_terima), tgudang_penerimaan.tipepenerimaan
		ORDER BY
			DATE(tgudang_penerimaan.tgl_terima)";
		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}

	function load_grafik_rekap_pemesanan_gudang() {
		$periode = $this->input->post('periode');  // Menangkap pilihan periode (1 = Bulanan, 2 = Semester, 3 = Triwulan)
		$tahun = $this->input->post('tahun');  // Menangkap tahun yang dipilih
		$where = '';

		// Menentukan periode berdasarkan input
		if ($periode == 1) {  // Bulanan
			$groupBy = "YEAR(tgudang_pemesanan.tgl_pemesanan), MONTH(tgudang_pemesanan.tgl_pemesanan)";
			$select = "CONCAT(MONTHNAME(tgudang_pemesanan.tgl_pemesanan), ' ', YEAR(tgudang_pemesanan.tgl_pemesanan)) AS periode";
		} elseif ($periode == 2) {  // Semester
			$groupBy = "YEAR(tgudang_pemesanan.tgl_pemesanan), CASE WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 1 AND 6 THEN 1 ELSE 2 END";
			$select = "CASE WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 1 AND 6 THEN 'Semester 1' ELSE 'Semester 2' END AS periode";
		} elseif ($periode == 3) {  // Triwulan
			$groupBy = "YEAR(tgudang_pemesanan.tgl_pemesanan), CASE WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 1 AND 3 THEN 1 WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 4 AND 6 THEN 2 WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 7 AND 9 THEN 3 ELSE 4 END";
			$select = "CASE WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 1 AND 3 THEN 'Triwulan 1' WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 4 AND 6 THEN 'Triwulan 2' WHEN MONTH(tgudang_pemesanan.tgl_pemesanan) BETWEEN 7 AND 9 THEN 'Triwulan 3' ELSE 'Triwulan 4' END AS periode";
		}

		if ($tahun != '') {
			$where .= " AND YEAR(tgudang_pemesanan.tgl_pemesanan) = '".$tahun."'";
		}

		$sql = "SELECT
					$select,
					tgudang_pemesanan.tipepemesanan,
					CASE 
						WHEN tgudang_pemesanan.tipepemesanan = 1 THEN 'NON LOGISTIK'
						WHEN tgudang_pemesanan.tipepemesanan = 2 THEN 'LOGISTIK'
					END AS nama_tipe,
					SUM(tgudang_pemesanan.totalharga) AS total_nominal
				FROM
					tgudang_pemesanan
				WHERE
					tgudang_pemesanan.status IN (2, 3, 4, 5)
					$where
				GROUP BY
					$groupBy, tgudang_pemesanan.tipepemesanan";

		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}

	function load_grafik_rekap_penerimaan_gudang() {
		$periode = $this->input->post('periode');  // Menangkap pilihan periode (1 = Bulanan, 2 = Semester, 3 = Triwulan)
		$tahun = $this->input->post('tahun');  // Menangkap tahun yang dipilih
		$where = '';

		// Menentukan periode berdasarkan input
		if ($periode == 1) {  // Bulanan
			$groupBy = "YEAR(tgudang_penerimaan.tgl_terima), MONTH(tgudang_penerimaan.tgl_terima)";
			$select = "CONCAT(MONTHNAME(tgudang_penerimaan.tgl_terima), ' ', YEAR(tgudang_penerimaan.tgl_terima)) AS periode";
		} elseif ($periode == 2) {  // Semester
			$groupBy = "YEAR(tgudang_penerimaan.tgl_terima), CASE WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 1 AND 6 THEN 1 ELSE 2 END";
			$select = "CASE WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 1 AND 6 THEN 'Semester 1' ELSE 'Semester 2' END AS periode";
		} elseif ($periode == 3) {  // Triwulan
			$groupBy = "YEAR(tgudang_penerimaan.tgl_terima), CASE WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 1 AND 3 THEN 1 WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 4 AND 6 THEN 2 WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 7 AND 9 THEN 3 ELSE 4 END";
			$select = "CASE WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 1 AND 3 THEN 'Triwulan 1' WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 4 AND 6 THEN 'Triwulan 2' WHEN MONTH(tgudang_penerimaan.tgl_terima) BETWEEN 7 AND 9 THEN 'Triwulan 3' ELSE 'Triwulan 4' END AS periode";
		}

		if ($tahun != '') {
			$where .= " AND YEAR(tgudang_penerimaan.tgl_terima) = '".$tahun."'";
		}

		$sql = "SELECT
					$select,
					tgudang_penerimaan.tipepenerimaan,
					CASE 
						WHEN tgudang_penerimaan.tipepenerimaan = 1 THEN 'NON LOGISTIK'
						WHEN tgudang_penerimaan.tipepenerimaan = 2 THEN 'LOGISTIK'
					END AS nama_tipe,
					SUM(tgudang_penerimaan.totalharga) AS total_nominal
				FROM
					tgudang_penerimaan
				WHERE
					tgudang_penerimaan.status = 1
					$where
				GROUP BY
					$groupBy, tgudang_penerimaan.tipepenerimaan";

		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}

	function load_grafik_harian_pemesanan_gudang_tipe_barang() {
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) >= '".YMDFormat($tanggal_dari)."' AND DATE(tgudang_pemesanan.tgl_pemesanan) <= '".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_pemesanan.tgl_pemesanan) = '".date('Y-m-d')."'";
		}

		$sql = "SELECT
			DATE(tgudang_pemesanan.tgl_pemesanan) AS tanggal,
			CASE tgudang_pemesanan_detail.idtipe 
                WHEN '1' THEN 'Alat Kesehatan'
                WHEN '2' THEN 'Implan'
                WHEN '3' THEN 'Obat'
                WHEN '4' THEN 'Logistik'
                ELSE 'Other'
            END AS tipe_barang,
			SUM(tgudang_pemesanan.totalharga) AS total_nominal
		FROM
			tgudang_pemesanan
		LEFT JOIN tgudang_pemesanan_detail ON tgudang_pemesanan_detail.idpemesanan = tgudang_pemesanan.id 
		LEFT JOIN view_barang_all ON view_barang_all.id = tgudang_pemesanan_detail.idbarang AND tgudang_pemesanan_detail.idtipe = view_barang_all.idtipe
		WHERE
			tgudang_pemesanan.status IN (2, 3, 4, 5)
			AND tgudang_pemesanan_detail.status =  1
			".$where."
		GROUP BY
			DATE(tgudang_pemesanan.tgl_pemesanan), tgudang_pemesanan_detail.idtipe
		ORDER BY
			DATE(tgudang_pemesanan.tgl_pemesanan)";
		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}

	function load_grafik_harian_penerimaan_gudang_tipe_barang() {
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) >= '".YMDFormat($tanggal_dari)."' AND DATE(tgudang_penerimaan.tgl_terima) <= '".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) = '".date('Y-m-d')."'";
		}

		$sql = "SELECT
			DATE(tgudang_penerimaan.tgl_terima) AS tanggal,
			CASE tgudang_penerimaan_detail.idtipe 
                WHEN '1' THEN 'Alat Kesehatan'
                WHEN '2' THEN 'Implan'
                WHEN '3' THEN 'Obat'
                WHEN '4' THEN 'Logistik'
                ELSE 'Other'
            END AS tipe_barang,
			SUM(tgudang_penerimaan.totalharga) AS total_nominal
		FROM
			tgudang_penerimaan
		LEFT JOIN tgudang_penerimaan_detail ON tgudang_penerimaan_detail.idpenerimaan = tgudang_penerimaan.id 
		LEFT JOIN view_barang_all ON view_barang_all.id = tgudang_penerimaan_detail.idbarang AND tgudang_penerimaan_detail.idtipe = view_barang_all.idtipe
		WHERE
			tgudang_penerimaan.status = 1
			AND tgudang_penerimaan_detail.status =  1
			".$where."
		GROUP BY
			DATE(tgudang_penerimaan.tgl_terima), tgudang_penerimaan_detail.idtipe
		ORDER BY
			DATE(tgudang_penerimaan.tgl_terima)";
		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}

	function load_grafik_tahunan_penerimaan_gudang_tipe_barang() {
		$periode = "'" . implode("', '", $this->input->post('periode')) . "'";

		$where ='';
		if ($periode != '') {
			$where .=" AND YEAR(tgudang_penerimaan.tgl_terima) IN (".$periode.")";
		}else{
			$where .=" AND YEAR(tgudang_penerimaan.tgl_terima) = '".date('Y')."'";
		}

		$sql = "SELECT
			YEAR(tgudang_penerimaan.tgl_terima) AS tahun,
			CASE tgudang_penerimaan_detail.idtipe 
                WHEN '1' THEN 'Alat Kesehatan'
                WHEN '2' THEN 'Implan'
                WHEN '3' THEN 'Obat'
                WHEN '4' THEN 'Logistik'
                ELSE 'Other'
            END AS tipe_barang,
			SUM(tgudang_penerimaan.totalharga) AS total_nominal
		FROM
			tgudang_penerimaan
		LEFT JOIN tgudang_penerimaan_detail ON tgudang_penerimaan_detail.idpenerimaan = tgudang_penerimaan.id 
		LEFT JOIN view_barang_all ON view_barang_all.id = tgudang_penerimaan_detail.idbarang AND tgudang_penerimaan_detail.idtipe = view_barang_all.idtipe
		WHERE
			tgudang_penerimaan.status = 1
			AND tgudang_penerimaan_detail.status =  1
			".$where."
		GROUP BY
			YEAR(tgudang_penerimaan.tgl_terima), tgudang_penerimaan_detail.idtipe
		ORDER BY
			YEAR(tgudang_penerimaan.tgl_terima)";
		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}
	
	function load_grafik_tahunan_penerimaan_gudang() {
		$periode = "'" . implode("', '", $this->input->post('periode')) . "'";

		$where ='';
		if ($periode != '') {
			$where .=" AND YEAR(tgudang_penerimaan.tgl_terima) IN (".$periode.")";
		}else{
			$where .=" AND YEAR(tgudang_penerimaan.tgl_terima) = '".date('Y')."'";
		}

		$sql = "SELECT
			YEAR(tgudang_penerimaan.tgl_terima) AS tahun,
			tgudang_penerimaan.tipepenerimaan,
			CASE 
				WHEN tgudang_penerimaan.tipepenerimaan = 1 THEN 'NON LOGISTIK'
				WHEN tgudang_penerimaan.tipepenerimaan = 2 THEN 'LOGISTIK'
			END AS nama_tipe,
			SUM(tgudang_penerimaan.totalharga) AS total_nominal
		FROM
			tgudang_penerimaan
		LEFT JOIN tgudang_penerimaan_detail ON tgudang_penerimaan_detail.idpenerimaan = tgudang_penerimaan.id 
		LEFT JOIN view_barang_all ON view_barang_all.id = tgudang_penerimaan_detail.idbarang AND tgudang_penerimaan_detail.idtipe = view_barang_all.idtipe
		WHERE
			tgudang_penerimaan.status = 1
			AND tgudang_penerimaan_detail.status =  1
			".$where."
		GROUP BY
			YEAR(tgudang_penerimaan.tgl_terima), tgudang_penerimaan.tipepenerimaan
		ORDER BY
			YEAR(tgudang_penerimaan.tgl_terima)";
		$query = $this->db->query($sql);
		$data = $query->result();

		$this->output->set_output(json_encode($data));
	}

	function load_data_penerimaan_gudang()
	{
		$where='';
		
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) >='".YMDFormat($tanggal_dari)."' AND DATE(tgudang_penerimaan.tgl_terima) <='".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) ='".date('Y-m-d')."'";
		}
		
		$this->select = array();
		$from="
			(
				SELECT
					tgudang_penerimaan.id,
					tgudang_penerimaan.nopenerimaan AS nomor_penerimaan,
					tgudang_penerimaan.tgl_terima AS tanggal_penerimaan,
					mdistributor.nama AS nama_distributor,
					tgudang_penerimaan.totalbarang AS total_item,
					tgudang_penerimaan.totalharga AS total_nominal
				FROM
					tgudang_penerimaan
				INNER JOIN mdistributor ON tgudang_penerimaan.iddistributor = mdistributor.id 
				WHERE
					tgudang_penerimaan.status = 1
					".$where."
			) as tbl  
		";
		
		$this->from = $from;
		$this->join = array();
		
		$this->order = array();
		$this->group = array();
		$this->column_search = array('nomor_penerimaan');
		$this->column_order = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();

		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$result = array();
			
			$result[] = $r->nomor_penerimaan;
			$result[] = $r->tanggal_penerimaan;
			$result[] = $r->nama_distributor;
			$result[] = number_format($r->total_item);
			$result[] = number_format($r->total_nominal);

			$aksi = '<a href="'.base_url().'tgudang_penerimaan/print_data/'.$r->id.'/0" target="_blank" data-toggle="tooltip" title="Cetak" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
			$aksi .= '<a href="'.base_url().'tgudang_penerimaan/detail/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Lihat Rincian" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';

			$result[] = $aksi;

			$data[] = $result;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(true),
			"recordsFiltered" => $this->datatable->count_all(true),
			"data" => $data
		);
		echo json_encode($output);
	}
	
	function load_summary_penerimaan_gudang(){
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) >= '".YMDFormat($tanggal_dari)."' AND DATE(tgudang_penerimaan.tgl_terima) <= '".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_penerimaan.tgl_terima) = '".date('Y-m-d')."'";
		}

		$sql = "SELECT
			COUNT(tgudang_penerimaan.id) AS total_item,
			SUM(tgudang_penerimaan.totalharga) AS total_nominal
		FROM
			tgudang_penerimaan
		INNER JOIN mdistributor ON tgudang_penerimaan.iddistributor = mdistributor.id 
		WHERE
			tgudang_penerimaan.status = 1".$where;
		$query = $this->db->query($sql);
		$row = $query->row();

		$data = [];
		$data['jumlah_penerimaan_gudang'] = number_format($row->total_item);
		$data['total_nominal_penerimaan_gudang'] = number_format($row->total_nominal);

		$this->output->set_output(json_encode($data));
	}

	function update_periode_filter(){
		$field = $this->input->post('field');

		if ($field == 'grafik_tahunan_tipe_barang_periode_tanggal_penerimaan' || $field == 'grafik_tahunan_periode_tanggal_penerimaan') {
			$value = implode(',', $this->input->post('value'));
		} else {
			$value = $this->input->post('value');
		}

		$this->db->where('id', 1);
		if ($this->db->update('msetting_executive_dashboard', [
			$field => $value
		])) {
			$response = array(
				'status' => true,
			);
		} else {
			$response = array(
				'status' => false,
			);
		}

		$this->output->set_output(json_encode($response));	
	}

	function load_data_top_pembelian_barang()
	{
		$where='';
		
		$idtipe = $this->input->post('idtipe');
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_penerimaan.tanggalpenerimaan) >='".YMDFormat($tanggal_dari)."' AND DATE(tgudang_penerimaan.tanggalpenerimaan) <='".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_penerimaan.tanggalpenerimaan) ='".date('Y-m-d')."'";
		}
		
		$this->select = array();
		$from="
			(
				SELECT
					(CASE
						WHEN tgudang_penerimaan_detail.idtipe = 1 THEN
							mdata_alkes.nama
						WHEN tgudang_penerimaan_detail.idtipe = 2 THEN
							mdata_implan.nama
						WHEN tgudang_penerimaan_detail.idtipe = 3 THEN
							mdata_obat.nama
						WHEN tgudang_penerimaan_detail.idtipe = 4 THEN
							mdata_logistik.nama
					END) AS nama_barang,
					tgudang_penerimaan_detail.kuantitas AS kuantitas_barang,
					tgudang_penerimaan_detail.totalharga AS total_nominal
				FROM
					tgudang_penerimaan_detail
				JOIN tgudang_penerimaan ON tgudang_penerimaan.id = tgudang_penerimaan_detail.idpenerimaan
				LEFT JOIN mdata_alkes ON mdata_alkes.id = tgudang_penerimaan_detail.idbarang AND tgudang_penerimaan_detail.idtipe = 1
				LEFT JOIN mdata_implan ON mdata_implan.id = tgudang_penerimaan_detail.idbarang AND tgudang_penerimaan_detail.idtipe = 2
				LEFT JOIN mdata_obat ON mdata_obat.id = tgudang_penerimaan_detail.idbarang AND tgudang_penerimaan_detail.idtipe = 3
				LEFT JOIN mdata_logistik ON mdata_logistik.id = tgudang_penerimaan_detail.idbarang AND tgudang_penerimaan_detail.idtipe = 4
				WHERE
					tgudang_penerimaan.status = 1 AND
					tgudang_penerimaan_detail.idtipe = ".$idtipe."
					".$where."
				ORDER BY tgudang_penerimaan_detail.totalharga DESC
			) as tbl  
		";
		
		$this->from = $from;
		$this->join = array();
		
		$this->order = array();
		$this->group = array();
		$this->column_search = array('nama_barang');
		$this->column_order = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();

		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$result = array();
			
			$result[] = $no;
			$result[] = $r->nama_barang;
			$result[] = number_format($r->kuantitas_barang);
			$result[] = number_format($r->total_nominal);

			$data[] = $result;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(true),
			"recordsFiltered" => $this->datatable->count_all(true),
			"data" => $data
		);
		echo json_encode($output);
	}

	function load_data_top_distributor_pembelian_barang()
	{
		$where='';
		
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');

		$where ='';
		if ($tanggal_dari != '') {
			$where .=" AND DATE(tgudang_penerimaan.tanggalpenerimaan) >='".YMDFormat($tanggal_dari)."' AND DATE(tgudang_penerimaan.tanggalpenerimaan) <='".YMDFormat($tanggal_sampai)."'";
		}else{
			$where .=" AND DATE(tgudang_penerimaan.tanggalpenerimaan) ='".date('Y-m-d')."'";
		}
		
		$this->select = array();
		$from="
			(
				SELECT
					mdistributor.nama AS nama_distributor,
					tgudang_penerimaan.totalharga AS total_nominal
				FROM
					tgudang_penerimaan
				JOIN mdistributor ON mdistributor.id = tgudang_penerimaan.iddistributor
				WHERE
					tgudang_penerimaan.status = 1
					".$where."
				ORDER BY tgudang_penerimaan.totalharga DESC
			) as tbl  
		";
		
		$this->from = $from;
		$this->join = array();
		
		$this->order = array();
		$this->group = array();
		$this->column_search = array('nama_distributor');
		$this->column_order = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();

		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$result = array();
			
			$result[] = $no;
			$result[] = $r->nama_distributor;
			$result[] = number_format($r->total_nominal);

			$data[] = $result;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(true),
			"recordsFiltered" => $this->datatable->count_all(true),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function get_nomor_stokopname()
	{
		$unit_pelayanan = $this->input->post('unit_pelayanan');
		$periode = $this->input->post('periode');

		$data = get_all('tstockopname', [
			'idunitpelayanan' => $unit_pelayanan,
			'periode' => $periode
		]);
		$this->output->set_output(json_encode($data));
	}

	function load_data_stokopname()
	{
		$id_stokopname = $this->input->post('nomor_stokopname');
		
		$this->select = array();
		$from="
			(
				SELECT
					tstockopname_detail.so_id,
					view_barang.idtipe,
					tstockopname_detail.idbarang,
					tstockopname_detail.idunitpelayanan,
					view_barang.nama AS nama_barang,
					view_barang.namatipe AS tipe_barang,
					tstockopname_detail.stoksistem AS stok_sistem,
					tstockopname_detail.stokfisik AS stok_fisik,
					tstockopname_detail.selisihstok AS selisih_stok,
					tstockopname_detail.catatan AS catatan,
					view_barang.hargabeli AS hpp,
					(view_barang.hargabeli * tstockopname_detail.stokfisik) AS nilai,
					(view_barang.hargabeli * tstockopname_detail.selisihstok) AS nilai_selisih,
					tstockopname_detail.namauser_update AS update_by
				FROM tstockopname_detail 					
				INNER JOIN view_barang ON view_barang.id = tstockopname_detail.idbarang AND view_barang.idtipe=tstockopname_detail.idtipe
				WHERE tstockopname_detail.so_id = '$id_stokopname' AND tstockopname_detail.status = 2
				ORDER BY view_barang.idtipe, view_barang.nama
			) as tbl  
		";
		
		$this->from = $from;
		$this->join = array();
		
		$this->order = array();
		$this->group = array();
		$this->column_search = array('nama_barang');
		$this->column_order = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();

		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$result = array();
			
			$result[] = $no;
			$result[] = $r->nama_barang;
			$result[] = $r->tipe_barang;
			$result[] = number_format($r->stok_sistem);
			$result[] = number_format($r->stok_fisik);
			$result[] = number_format($r->selisih_stok);
			$result[] = $r->catatan;
			$result[] = number_format($r->hpp);
			$result[] = number_format($r->nilai);
			$result[] = number_format($r->nilai_selisih);
			$result[] = $r->update_by;

			$action = '<a href="'.base_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->idbarang.'/'.$r->idunitpelayanan.'" target="_blank" data-toggle="tooltip" title="Kartu Stok" class="btn btn-success btn-xs"><i class="fa fa-database"></i></a>';
			$action .= '<a href="'.base_url().'tstockopname/rekap/'. $r->so_id. '" target="_blank" data-toggle="tooltip" title="Lihat Rincian" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
			$result[] = $action;

			$data[] = $result;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(true),
			"recordsFiltered" => $this->datatable->count_all(true),
			"data" => $data
		);
		echo json_encode($output);
	}
}
