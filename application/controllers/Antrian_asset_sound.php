<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_asset_sound extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_asset_sound_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1546'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Asset Sound';
			$data['content'] 		= 'Antrian_asset_sound/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Asset Sound",'#'),
												  array("List",'antrian_asset_sound')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama_asset' 					=> '',
			'file_sound' 					=> '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Asset Sound';
		$data['content'] 		= 'Antrian_asset_sound/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Asset Sound",'#'),
								            array("Tambah",'antrian_asset_sound')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Antrian_asset_sound_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Asset Sound';
			$data['content']    = 'Antrian_asset_sound/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Asset Sound",'#'),
										array("Ubah",'antrian_asset_sound')
										);

			// $data['statusAvailableApoteker'] = $this->Antrian_asset_sound_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('antrian_asset_sound');
		}
	}

	function delete($id){
		
		$result=$this->Antrian_asset_sound_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('antrian_asset_sound','location');
	}
	function aktifkan($id){
		
		$result=$this->Antrian_asset_sound_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if($this->input->post('id') == '' ) {
			if($this->Antrian_asset_sound_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('antrian_asset_sound/create','location');
			}
		} else {
			if($this->Antrian_asset_sound_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('antrian_asset_sound/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Antrian_asset_sound/manage';

		if($id==''){
			$data['title'] = 'Tambah Asset Sound';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Asset Sound",'#'),
							               array("Tambah",'antrian_asset_sound')
								           );
		}else{
			$data['title'] = 'Ubah Asset Sound';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Asset Sound",'#'),
							               array("Ubah",'antrian_asset_sound')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex($idkategori='0',$idakun='0')
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						FROM antrian_asset_sound H 
						
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_asset','file_sound');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama_asset;
		  $nama_file="'".$r->file_sound."'";
          $result[] = $r->file_sound.'&nbsp;&nbsp;<button class="btn btn-default btn-xs" onclick="play_audio('.$nama_file.')"><i class="fa fa-play"></i> Play</button>';
		 
          $result[] = ($r->status?text_primary('AKTIF'):text_danger('TIDAK AKTIF'));
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1548'))){
				$aksi .= '<a href="'.site_url().'antrian_asset_sound/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1535'))){
				$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}else{
				if (UserAccesForm($user_acces_form,array('1548'))){
				$aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
				}
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
}
