<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_logistik extends CI_Controller {

	/**
	 * Logistik controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_logistik_model');
		$this->load->helper('path');
  }

	function index($idkategori='0'){
		
		$data = array();
		$data['idkategori'] 			= $idkategori;
		$data['error'] 			= '';
		$data['title'] 			= 'Logistik';
		$data['content'] 		= 'Mdata_logistik/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Logistik",'#'),
									    			array("List",'mdata_logistik')
													);
		$data['list_kategori'] = $this->Mdata_logistik_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 							=> '',
			'kode' 						=> '',
			'idkategori' 			=> '',
			'nama' 						=> '',
			'idsatuan' 				=> '',
			'lokasirak' 			=> '',
			'ppn'            	=> '',
			'hargabeli'       => '',
			'hargadasar'      => '',
			'margin'      		=> '',
			'stokreorder' 		=> '',
			'stokminimum' 		=> '',
			'catatan' 				=> '',
			'status' 					=> '',
			'idsatuanbesar' 					=> '',
			'hargasatuanbesar' 				=> '',
			'jumlahsatuanbesar' 			=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Logistik';
		$data['content'] 		= 'Mdata_logistik/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Logistik",'#'),
									    			array("Tambah",'mdata_logistik')
													);

		$data['list_kategori'] = $this->Mdata_logistik_model->getKategori();
		$data['list_satuan'] = $this->Mdata_logistik_model->getSatuan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mdata_logistik_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'kode'						=> $row->kode,
					'idkategori'			=> $row->idkategori,
					'nama' 						=> $row->nama,
					'idsatuan' 				=> $row->idsatuan,
					'lokasirak' 			=> $row->lokasirak,
					'ppn'             => $row->ppn,
					'hargabeli'       => $row->hargabeli,
					'hargadasar'      => $row->hargadasar,
					'margin'      		=> $row->margin,
					'stokreorder' 		=> $row->stokreorder,
					'stokminimum' 		=> $row->stokminimum,
					'catatan' 				=> $row->catatan,
					'status' 					=> $row->status,
					'idsatuanbesar' 					=> $row->idsatuanbesar,
					'hargasatuanbesar' 				=> $row->hargasatuanbesar,
					'jumlahsatuanbesar' 			=> $row->jumlahsatuanbesar
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Logistik';
				$data['content']	 	= 'Mdata_logistik/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Logistik",'#'),
											    			array("Ubah",'mdata_logistik')
															);

				$data['list_kategori'] = $this->Mdata_logistik_model->getKategori();
				$data['list_satuan'] = $this->Mdata_logistik_model->getSatuan();
				// print_r($data['list_satuan']);exit();
				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdata_logistik','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_logistik');
		}
	}

	function delete($id){
		
		$this->Mdata_logistik_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mdata_logistik','location');
	}

	function save(){
		$this->form_validation->set_rules('idkategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('idsatuan', 'Satuan', 'trim|required');
		$this->form_validation->set_rules('lokasirak', 'Lokasi Rak', 'trim|required');
		$this->form_validation->set_rules('stokreorder', 'Stok Reorder', 'trim|required');
		$this->form_validation->set_rules('stokminimum', 'Stok Minimum', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mdata_logistik_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_logistik','location');
				}
			} else {
				if($this->Mdata_logistik_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_logistik','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 					= validation_errors();
		$data['content'] 				= 'Mdata_logistik/manage';

		$data['list_kategori'] 	= $this->Mdata_logistik_model->getKategori();
		$data['list_satuan'] 		= $this->Mdata_logistik_model->getSatuan();

		if($id==''){
			$data['title'] = 'Tambah Logistik';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Logistik",'#'),
															array("Tambah",'mdata_logistik')
													);
		}else{
			$data['title'] = 'Ubah Logistik';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Logistik",'#'),
															array("Ubah",'mdata_logistik')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function filter(){
		// if($this->input->post('idkategori') != '0'){
			// $idkategori = $this->input->post('idkategori');
			// redirect("mdata_logistik/index/$idkategori",'location');
		// }else{
			// redirect('mdata_logistik/index/0','location');
		// }
		$data = array();
		$data['idkategori'] 			= $this->input->post('idkategori');
		$data['error'] 			= '';
		$data['title'] 			= 'Logistik';
		$data['content'] 		= 'Mdata_logistik/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Logistik",'#'),
									    			array("List",'mdata_logistik')
													);
		$data['list_kategori'] = $this->Mdata_logistik_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex($idkategori='0')
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
		
			$idkategori=$this->input->post('idkategori');
			$row=$this->Mdata_logistik_model->get_array_kategori($idkategori);
			
			$this->select = array('mdata_logistik.*', 'mdata_kategori.nama AS namakategori', 'msatuan.nama AS namasatuan');
			$this->from   = 'mdata_logistik';
			$this->join 	= array(
				array("mdata_kategori", "mdata_kategori.id = mdata_logistik.idkategori", "LEFT"),
				array("msatuan", "msatuan.id = mdata_logistik.idsatuan", "LEFT")
			);
			
			// $this->where  = array(
			// 'mdata_logistik.status' => '1'
			// );
			$this->where_in = array(
					'mdata_logistik.idkategori' => $row
			);
			
			$this->order  = array(
				'mdata_logistik.kode' => 'DESC'
			);
			$this->group  = array();

			$this->column_search   = array('mdata_logistik.kode','mdata_kategori.nama','mdata_logistik.nama');
			$this->column_order    = array('mdata_logistik.kode','mdata_kategori.nama','mdata_logistik.nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->kode;
					$row[] = $r->nama;
					$row[] = $r->namakategori;
					$row[] = number_format($r->hargasatuanbesar,0);
					$row[] = number_format($r->hargadasar,0);
					$row[] = StatusBarang($r->status);
					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
		            if (UserAccesForm($user_acces_form,array('124','127'))){
		                $aksi .= '<a href="'.site_url().'mdata_logistik/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('128'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_logistik" data-urlremove="'.site_url().'mdata_logistik/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
					}else{
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_obat" data-urlremove="'.site_url().'mdata_logistik/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
		            $aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function aktifkan($id){
		
		$this->Mdata_logistik_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mdata_logistik','location');
	}
}
