<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_fisioterapi extends CI_Controller {

	/**
	 * Tarif Fisioterapi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_fisioterapi_model');
		$this->load->model('Mtarif_administrasi_model');
  }

	function index($idpath='0'){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Tarif Fisioterapi';
		$data['content'] 		= 'Mtarif_fisioterapi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Fisioterapi",'mtarif_fisioterapi/index'),
														array("List",'#')
													);
		$data['idpath'] 			= $idpath;
		$data['list_level0'] = $this->Mtarif_fisioterapi_model->getLevel0();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'nama' 						=> '',
			'idkelompok' 			=> '1',
			'headerpath' 			=> '0',
			'old_headerpath' 	=> '',
			'path' 						=> '',
			'level' 					=> '',
			'status' 					=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif Fisioterapi';
		$data['content'] 		= 'Mtarif_fisioterapi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Fisioterapi",'#'),
									    			array("Tambah",'mtarif_fisioterapi')
													);

		$data['list_parent'] = $this->Mtarif_fisioterapi_model->getAllParent();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_fisioterapi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'nama' 						=> $row->nama,
					'idkelompok' 			=> $row->idkelompok,
					'headerpath' 			=> $row->headerpath,
					'old_headerpath' 	=> $row->headerpath,
					'path' 						=> $row->path,
					'level' 					=> $row->level,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif Fisioterapi';
				$data['content']	 	= 'Mtarif_fisioterapi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Fisioterapi",'#'),
											    			array("Ubah",'mtarif_fisioterapi')
															);

				$data['list_parent'] = $this->Mtarif_fisioterapi_model->getAllParent($row->headerpath, $row->level);
				$data['list_tarif'] = $this->Mtarif_fisioterapi_model->getAllTarif($row->id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_fisioterapi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_fisioterapi','location');
		}
	}

	function delete($id){
		$this->Mtarif_fisioterapi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_fisioterapi','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_fisioterapi_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_fisioterapi','location');
				}
			} else {
				if($this->Mtarif_fisioterapi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_fisioterapi','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mtarif_fisioterapi/manage';

		$data['list_parent'] = $this->Mtarif_fisioterapi_model->getAllParent();

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mtarif_fisioterapi')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mtarif_fisioterapi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function get_child_level($headerpath) {
		$arr = $this->Mtarif_fisioterapi_model->getPathLevel($headerpath);
		$this->output->set_output(json_encode($arr));
	}
	function filter(){
		if($this->input->post('idpath') != '0'){
			$idpath = $this->input->post('idpath');
			redirect("mtarif_fisioterapi/index/$idpath",'location');
		}else{
			redirect('mtarif_fisioterapi/index/0','location');
		}
	}
	function getIndex($idpath='0')
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$panjang=strlen($idpath);
			$this->select = array();
			$this->from   = 'mtarif_fisioterapi';
			$this->join 	= array();
			if ($idpath !='0'){
				$this->where  = array(
					'left(path,'.$panjang.')' => $idpath,
					'status' => '1'
				);

			}else{
				$this->where  = array(
					'status' => '1'
				);
			}
			$this->order  = array(
				'path' => 'ASC'
			);
			$this->group  = array();

			if (UserAccesForm($user_acces_form,array('159'))){
				$this->column_search   = array('nama');
			}else{
				$this->column_search   = array();
			}
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = TreeView($r->level, $r->nama);
					$aksi = '<div class="btn-group">';
	          if (UserAccesForm($user_acces_form,array('161'))){
	              $aksi .= '<a href="'.site_url().'mtarif_fisioterapi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
	          }
	          if (UserAccesForm($user_acces_form,array('162'))){
	              $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_fisioterapi" data-urlremove="'.site_url().'mtarif_fisioterapi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
	          }
	          if ($r->idkelompok == 0) {
	            $aksi .= '<a href="'.site_url().'mtarif_fisioterapi/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
	          }
	          $aksi .= '</div>';
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}

	public function setting($id)
	{
			if ($id != '') {
					$row = $this->Mtarif_fisioterapi_model->getSpecified($id);
					if (isset($row->id)) {
							$data = array(
								'id' 							=> $row->id,
								'nama' 						=> $row->nama,
								'idkelompok' 			=> $row->idkelompok,
								'headerpath' 			=> $row->headerpath,
								'old_headerpath' 	=> $row->headerpath,
								'path' 						=> $row->path,
								'level' 					=> $row->level,
								'status' 					=> $row->status,
								'group_diskon_all' 					=> $row->group_diskon_all
							);

							$data['error']      = '';
							$data['title']      = 'Setting Group Pembayaran Tarif Fisioterapi';
							$data['content']    = 'Mtarif_fisioterapi/setting';
							$data['breadcrum']  = array(
									array("RSKB Halmahera",'#'),
									array("Tarif Fisioterapi",'mtarif_fisioterapi/index'),
									array("Setting",'mtarif_fisioterapi')
							);

							$data['list_tarif'] = $this->Mtarif_fisioterapi_model->getAllTarif($row->id);
							$data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

							$data = array_merge($data, backend_info());
							$this->parser->parse('module_template', $data);
					} else {
							$this->session->set_flashdata('error', true);
							$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
							redirect('mtarif_fisioterapi/index/'.$uri, 'location');
					}
			} else {
					$this->session->set_flashdata('error', true);
					$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
					redirect('mtarif_fisioterapi/index/'.$uri, 'location');
			}
	}

	function save_setting() {
		if($this->Mtarif_fisioterapi_model->updateSetting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mtarif_fisioterapi', 'location');
		}
	}
}
