<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tmonitoring extends CI_Controller {

	/**
	 * Setoran Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tmonitoring_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_bendahara_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array(
			'tanggal_setoran2'=>'',
			'tanggal_setoran1'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
			'status'=>'#',
			'notransaksi'=>'',
		);
		$data['list_user'] 		= $this->Tmonitoring_model->list_user();
		
		$data['error'] 			= '';
		$data['title'] 			= 'Monitoring Pendapatan';
		$data['content'] 		= 'Tmonitoring/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("MONITORING PENDAPATAN",'#'),
									    			array("List",'Tmonitoring')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
		
		// var jenis_trx=$("#jenis_trx").val();
		// var idmetode=$("#idmetode").val();		
		// var tanggal_trx1=$("#tanggal_trx1").val();
		// var tanggal_trx2=$("#tanggal_trx2").val();
		// var user_id=$("#user_id").val();
		
	  $jenis_trx=$this->input->post('jenis_trx');
	  $idmetode=$this->input->post('idmetode');
	  $tanggal_trx1=$this->input->post('tanggal_trx1');
	  $tanggal_trx2=$this->input->post('tanggal_trx2');
	  $user_id=$this->input->post('user_id');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($jenis_trx !='#'){
		  $where .=" AND trx.jenis_trx='$jenis_trx'";
	  }
	  if ($user_id !='#'){
		  $where .=" AND trx.user_id='$user_id'";
	  }
	  if ($idmetode !='#'){
		  if ($idmetode=='1'){
			$where .=" AND trx.idmetodepembayaran='$idmetode'";			  
		  }else{
			  $where .=" AND trx.idmetodepembayaran !='1'";
		  }
	  }
	  
	   if ($tanggal_trx1 !='' || $tanggal_trx2 !=''){
		  $where .=" AND (tsetoran_kas.tanggal_trx >='".YMDFormat($tanggal_trx1)."' AND tsetoran_kas.tanggal_trx <='".YMDFormat($tanggal_trx2)."' )";
	  }
	 
	  $from="(
				SELECT trx.*,tsetoran_kas.id as tsetoran_kas_id FROM
					(
					SELECT  H.id,H.tanggal,U.`name` as nama_user,'DEPOSIT (RI)' as jenis_trx,H.idmetodepembayaran
							,CONCAT(P.no_medrec,' - ', P.title,'. ',P.namapasien,'<br>',KP.nama,' ',COALESCE(MR.nama,'')) as keterangan
								,CASE WHEN H.idmetodepembayaran='1' THEN H.nominal ELSE 0 END as tunai
								,0 as pengeluaran
								,CASE WHEN H.idmetodepembayaran!='1' THEN H.nominal ELSE 0 END as non_tunai
								,'RI' as tipe_trx,U.id as user_id,P.id as idpendaftaran
								FROM trawatinap_deposit H
								LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idrawatinap
								LEFT JOIN mpasien_kelompok KP ON KP.id=P.idkelompokpasien
								LEFT JOIN mrekanan MR ON MR.id=P.idrekanan
								LEFT JOIN musers U ON U.id=H.iduserinput
								WHERE  H.`status`='1'
								
					UNION ALL


					SELECT D.id,
							DATE(H.tanggal) as tanggal,U.`name` as nama_user,'PELUNASAN (RI)' as jenis_trx,D.idmetode as idmetodepembayaran
							,CONCAT(P.no_medrec,' - ', P.title,'. ',P.namapasien,'<br>',KP.nama,' ',COALESCE(MR.nama,'')) as keterangan
							,CASE WHEN D.idmetode='1' THEN D.nominal ELSE 0 END as tunai
								,0 as pengeluaran
								,CASE WHEN D.idmetode!='1' THEN D.nominal ELSE 0 END as non_tunai
							,'RI' as tipe_trx,U.id as user_id,P.id as idpendaftaran
							FROM 
							trawatinap_tindakan_pembayaran H
							LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
							LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idtindakan
							LEFT JOIN mpasien_kelompok KP ON KP.id=P.idkelompokpasien
							LEFT JOIN mrekanan MR ON MR.id=P.idrekanan
							LEFT JOIN musers U ON U.id=H.iduser_input
							WHERE D.idmetode IN (1,2,3,4) AND H.statusbatal='0'

					UNION ALL

					SELECT T.id,T.tanggal,T.nama_user,T.jenis_trx,T.idmetodepembayaran,T.keterangan,SUM(T.tunai) as tunai,SUM(T.pengeluaran) as pengeluaran,SUM(T.non_tunai) as non_tunai,'RJ' as tipe_trx,T.user_id,'' as idpendaftaran FROM (
					SELECT K.id,DATE(K.tanggal) as tanggal,U.`name` as nama_user,'PENDAPATAN (RJ)' as jenis_trx,KB.idmetode as idmetodepembayaran,CONCAT('RAJAL ',DATE_FORMAT(K.tanggal,'%d-%m-%Y')) as keterangan
								  ,SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END) as tunai
								  ,0 as pengeluaran
								,SUM(CASE WHEN KB.idmetode!='1' THEN KB.nominal ELSE 0 END) as non_tunai
									,CASE WHEN KB.idmetode ='1' THEN 1 ELSE 2 END as jenis_bayar,U.id as user_id
								  from tkasir K
									LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
									LEFT JOIN musers U ON U.id=K.created_by
									WHERE K.`status`='2' AND KB.idmetode IN (2,3,4)
									GROUP BY DATE(K.tanggal),KB.idmetode ORDER BY U.`name` DESC
									)T GROUP BY T.tanggal, T.jenis_bayar
									
					UNION ALL
					
					SELECT H.id,H.tanggal_trx as tanggal,U.`name` as nama_user
					,'PENDAPATAN (RJ)' as jenist_trx,'1' as idmetodepembayaran,CONCAT('RAJAL ',DATE_FORMAT(H.tanggal_trx,'%d-%m-%Y')) as keterangan
					,H.cash_rajal as tunai,'0' as pengeluaran,'0' as non_tunai,'RJ' as tipe_trx,H.user_created as user_id,'' as idpendaftaran
					from tsetoran_kas H
					LEFT JOIN musers U ON U.id=H.user_created
					WHERE H.`status`='1'
					GROUP BY DATE(H.tanggal_trx)
									
					UNION ALL

					SELECT T.id,DATE(T.tanggal) as tanggal,U.`name` as nama_user,'REFUND (RJ)' as jenis_trx,T.metode as idmetodepembayaran
					,'REFUND OBAT'
					,0 as tunai,SUM(T.nominal) as pengeluaran,0 as non_tunai
					,'RJ' as tipe_trx,U.id as user_id,'' as idpendaftaran
					 FROM trefund T
					 LEFT JOIN musers U ON U.id=T.created_by
					 LEFT JOIN mfpasien M ON M.id=T.idpasien
					WHERE T.`status`='1' AND T.tipe='1'
					
					UNION ALL
					SELECT T.id,DATE(T.tanggal) as tanggal,U.`name` as nama_user,'REFUND (RJ)' as jenis_trx,T.metode as idmetodepembayaran
					,'REFUND TRANSAKSI RAJAL'
					,0 as tunai,SUM(T.nominal) as pengeluaran,0 as non_tunai
					,'RJ' as tipe_trx,U.id as user_id,'' as idpendaftaran
					 FROM trefund T
					 LEFT JOIN musers U ON U.id=T.created_by
					 LEFT JOIN mfpasien M ON M.id=T.idpasien
					WHERE T.`status`='1' AND T.tipe='2' AND T.tipetransaksi='rawatjalan' AND T.metode='1'
					GROUP BY DATE(T.tanggal),T.tipe,T.tipetransaksi


					UNION ALL

					SELECT T.id,DATE(T.tanggal) as tanggal,U.`name` as nama_user,'REFUND (RI)' as jenis_trx,T.metode as idmetodepembayaran
					,'REFUND DEPOSIT'
					,0 as tunai,SUM(T.nominal) as pengeluaran,0 as non_tunai
					,'RI' as tipe_trx,U.id as user_id,'' as idpendaftaran
					 FROM trefund T
					 LEFT JOIN musers U ON U.id=T.created_by
					 LEFT JOIN mfpasien M ON M.id=T.idpasien
					WHERE T.`status`='1' AND T.tipe='0' AND T.metode='1'
					GROUP BY DATE(T.tanggal),T.tipe,T.tipetransaksi
					
					UNION ALL
					
					SELECT T.id,DATE(T.tanggal) as tanggal,U.`name` as nama_user,'REFUND (RI)' as jenis_trx,T.metode as idmetodepembayaran
					,'REFUND TRANSAKSI RANAP'
					,0 as tunai,SUM(T.nominal) as pengeluaran,0 as non_tunai
					,'RI' as tipe_trx,U.id as user_id,'' as idpendaftaran
					 FROM trefund T
					 LEFT JOIN musers U ON U.id=T.created_by
					 LEFT JOIN mfpasien M ON M.id=T.idpasien
					WHERE T.`status`='1' AND T.tipe='2' AND T.tipetransaksi='rawatinap'AND T.metode='1'
					GROUP BY DATE(T.tanggal),T.tipe,T.tipetransaksi

					UNION ALL

					SELECT H.id,DATE(H.tanggal) as tanggal,U.`name` as nama_user,'PENDAPATAN LAIN' as jenis_trx,D.idmetode as idmetodepembayaran
					,MP.keterangan
					,SUM(CASE WHEN D.idmetode='2' THEN D.nominal_bayar ELSE 0 END) as tunai ,0 as pengeluaran
					,SUM(CASE WHEN D.idmetode!='2' THEN D.nominal_bayar ELSE 0 END) as non_tunai,'' as tipe_trx,U.id as user_id ,'' as idpendaftaran
					from tbendahara_pendapatan H
					LEFT JOIN tbendahara_pendapatan_detail D ON D.idtransaksi=H.id
					LEFT JOIN mdari M ON M.id=H.terimadari
					LEFT JOIN mpendapatan MP ON MP.id=H.idpendapatan
					LEFT JOIN musers U ON U.id=H.user_created 
					WHERE H.`status`='1'
					GROUP BY H.id
					) trx
					INNER JOIN tsetoran_kas ON trx.tanggal = tsetoran_kas.tanggal_trx
					WHERE tsetoran_kas.id IS NOT NULL ".$where."
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	  $url_ranap        = site_url('trawatinap_tindakan/');
      foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $status = '';
			// if ($r->status=='1'){
				// $action .= '<a href="'.site_url().'tsetoran_kas/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
			
				// if ($r->st_verifikasi=='0'){
					// $action .= '<button class="btn btn-primary btn-sm verif"><i class="fa fa-check"></i></button>';
					// $action .= '<a href="'.site_url().'tsetoran_kas/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
					// $action .= '<button class="btn btn-danger btn-sm hapus"><i class="fa fa-trash"></i></button>';
					// $status='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">BELUM DIVERIFIKASI</span>';
				// }else{
					// $status='<span class="label label-success" data-toggle="tooltip" title="DIBATALKAN">SUDAH DIVERIFIKASI</span>';
				// }
				// $action .= '<a href="'.site_url().'tsetoran_kas/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Cetak Kwitansi" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
			// }else{
				// $action='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
				// $status='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
			// }
  					
            $row[] = $r->id;
  			$row[] = HumanDateShort($r->tanggal);
  			$row[] = $r->nama_user;
  			$row[] = $r->jenis_trx;
			if ($r->jenist_trx='PENDAPATAN LAIN'){
  			$row[] = ($r->idmetodepembayaran=='2'?'<span class="label label-primary">TUNAI</span>':'<span class="label label-success">NON TUNAI</span>');
				
			}else{
  			$row[] = ($r->idmetodepembayaran=='1'?'<span class="label label-primary">TUNAI</span>':'<span class="label label-success">NON TUNAI</span>');
				
			}
  			$row[] = $r->keterangan;
            $row[] = number_format($r->tunai);
            $row[] = number_format($r->pengeluaran);
            $row[] = number_format($r->non_tunai);
            $row[] = '';
			$action .= '<a href="'.site_url().'tsetoran_kas/update/'.$r->tsetoran_kas_id.'" target="_blank" data-toggle="tooltip" title="Detail" class="btn btn-info btn-xs"><i class="si si-arrow-down"></i></a>';
			// $action .= '<button data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></button>';
			if ($r->tipe_trx=='RI'){
				$action .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Ranap</li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->idpendaftaran.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->idpendaftaran.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->idpendaftaran.'/0" target="_blank"  > Rincian Global (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->idpendaftaran.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
							
						</ul>
					</div>';
				if ($r->jenis_trx!='DEPOSIT (RI)'){
					$action .= '<a href="'.site_url().'trawatinap_tindakan/verifikasi/'.$r->idpendaftaran.'" target="_blank"  data-toggle="tooltip" title="Lihat Transaksi" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>';
				}
			}
			if ($r->jenis_trx=='DEPOSIT (RI)'){
				$action .= '<button onclick="getHistoryDeposit('.$r->idpendaftaran.')" data-toggle="tooltip" title="DEPOSIT" class="btn btn-primary btn-xs"><i class="fa fa-credit-card-alt"></i></button>';
			}
			if ($r->jenis_trx=='PELUNASAN (RI)'){
				$action .= '<button onclick="getHistoryPelunasan('.$r->idpendaftaran.')" data-toggle="tooltip" title="DEPOSIT" class="btn btn-primary btn-xs"><i class="fa fa-credit-card-alt"></i></button>';
			}
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function getHistoryPelunasan($idpendaftaran){
	  
	  $q="SELECT
				trawatinap_tindakan_pembayaran.tanggal,
              trawatinap_tindakan_pembayaran_detail.tipekontraktor,
              trawatinap_tindakan_pembayaran_detail.idkontraktor,
            	(CASE
            		WHEN trawatinap_tindakan_pembayaran_detail.tipekontraktor = 1 THEN mrekanan.nama
            		ELSE mpasien_kelompok.nama
            	END) AS namakontraktor,trawatinap_tindakan_pembayaran_detail.idmetode
							,trawatinap_tindakan_pembayaran_detail.nominal,trawatinap_tindakan_pembayaran_detail.keterangan
            FROM trawatinap_tindakan_pembayaran_detail
            LEFT JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.id = trawatinap_tindakan_pembayaran_detail.idtindakan
            LEFT JOIN mrekanan ON mrekanan.id = trawatinap_tindakan_pembayaran_detail.idkontraktor AND trawatinap_tindakan_pembayaran_detail.tipekontraktor = 1
            LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_tindakan_pembayaran_detail.tipekontraktor
            WHERE trawatinap_tindakan_pembayaran.idtindakan ='$idpendaftaran'";
		$hasil=$this->db->query($q)->result();
		$tabel='';
		$btn='';
		$total=0;
		foreach($hasil as $r){
			$total=$total+$r->nominal;
			$btn = '<div class="btn-group">';
			$btn .= '<button disabled class="btn btn-sm btn-success">Print <i class="fa fa-print"></i></button>';
			$btn .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td>'.DMYTimeFormat($r->tanggal).'</td>';
			$tabel .='<td>'.metodePembayaran($r->idmetode).'</td>';
			$tabel .='<td>'.$r->keterangan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal,2).'</td>';
			$tabel .='<td>'.$btn.'</td>';
			$tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		$arr['total']=number_format($total,2);
		$this->output->set_output(json_encode($arr));
  }
  
}
