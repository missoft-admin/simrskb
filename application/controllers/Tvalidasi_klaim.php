<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_klaim extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tvalidasi_klaim_model','model');
	}

	function index() {
		$data=array(
			'st_posting'=>'#',
			'idkelompokpasien'=>'#',
			'idrekanan'=>'#',
			'nojurnal'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tanggalterima1'=>'',
			'tanggalterima2'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		// print_r($data['list_rekanan']);exit();
		$data['list_kp'] 	= $this->model->list_kp();
		$data['title'] 			= 'Validasi Jurnal Pelunasan Piutang';
		$data['content'] 		= 'Tvalidasi_klaim/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pelunasan Piutang",'tvalidasi_klaim/index'),
									array("List",'#')
								);

		// print_r($data);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		// print_r($data['list_rekanan']);exit();
		$data['list_kp'] 	= $this->model->list_kp();
		$data['title'] 			= 'Detail Validasi Akuntansi';
		$data['content'] 		= 'Tvalidasi_klaim/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pelunasan Piutang",'tvalidasi_klaim/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		$batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_klaim WHERE id='1'")->row('batas_batal');
		
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$nojurnal=$this->input->post('nojurnal');
		$no_terima=$this->input->post('no_terima');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		
		
		if ($idkelompokpasien !='#'){
			$where .=" AND H.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal='$nojurnal' ";
		}
		if ($no_terima !=''){
			$where .=" AND H.notransaksi='$no_terima' ";
		}
		
		if ($idrekanan !='#'){
			$where .=" AND H.idrekanan='$idrekanan' ";
		}
		
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT H.id,H.idtransaksi,H.st_posting,H.nojurnal,H.notransaksi,H.tanggal_transaksi,H.nama_rekanan,H.tipe
					,H.nominal_bayar,IFNULL(DATEDIFF(NOW(), H.posting_date),0) as umur_posting 
					FROM tvalidasi_klaim H WHERE H.`status`='1' ".$where."
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nojurnal','nama_bank','notransaksi','keterangan_klaim','nama_rekanan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
			if ($r->umur_posting>$batas_batal){
				$disabel_btn='disabled';
			}
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url_klaim        = site_url('tpendapatan/');
            $url       = site_url('tklaim_monitoring/');
            $url_validasi        = site_url('tvalidasi_klaim/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            $row[] = $r->nojurnal;
            $row[] = $r->notransaksi;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = $r->nama_rekanan;
            $row[] = number_format($r->nominal_bayar,2);
           
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING'));
					$aksi .= '<a href="'.$url.'koreksi_tagihan/'.$r->idtransaksi.'/'.$r->tipe.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
					if ($r->st_posting=='1'){
					$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
					}else{
					$aksi .= '<button title="Posting" class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';
						
					}
					// $aksi .= '<a href="'.$url_validasi.'print_kwitansi_deposit/'.$r->idtransaksi.'" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
					// $aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function idrekanan($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-success">TUNAI</span>';
		}elseif($tipe=='2'){
			$tipe='<span class="label label-danger">KREDIT</span>';
		}else{
			$tipe='<span class="label label-default">-</span>';
		}
		
		return $tipe;
	}
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_klaim', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_klaim', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_klaim', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_transaksi_detail($iddet)
    {
        $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idkelompokpasien=D.idkelompokpasien
WHERE D.id='$iddet'";
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function save_detail(){
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		if($this->model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_klaim','location');
		}
	
	}
	function load_detail(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT *FROM tvalidasi_klaim_detail H
				WHERE H.idvalidasi='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_piutang='<select name="idakun_piutang[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_piutang).'
						</select>';
			$select_income='<select name="idakun_income[]" '.$disabel.' '.($r->nominal_other_income=='0'?'disabled':'').'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_income).'
						</select>';
			$select_other_loss='<select name="idakun_other_loss[]" '.$disabel.' '.($r->nominal_other_loss=='0'?'disabled':'').' class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_other_loss).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet[]" value="'.$r->id.'">';
			$tbl .='<td>'.$r->nopendaftaran.'<br>'.$r->no_medrec.'<br>'.$r->title.'. '.$r->namapasien.'</td>';
			$tbl .='<td class="text-center">'.$r->poli.'</td>';
			$tbl .='<td>'.$r->dokter.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_piutang,2).'</td>';
			$tbl .='<td>'.$select_piutang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_other_income,2).'</td>';
			$tbl .='<td>'.$select_income.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_other_loss,2).'</td>';
			$tbl .='<td>'.$select_other_loss.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm " onclick="get_transaksi_detail('.$r->id_det.')"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_bayar(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT *FROM tvalidasi_klaim_bayar H
				WHERE H.idvalidasi='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_akun='<select name="idakun[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet_bayar[]" value="'.$r->id.'">';
			$tbl .='<td class="text-center">'.$r->jenis_kas_nama.'</td>';
			$tbl .='<td class="text-center">'.$r->sumber_nama.'</td>';
			$tbl .='<td class="text-center">'.$r->bank.'</td>';
			$tbl .='<td class="text-center">'.$r->metode_nama.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_bayar,2).'</td>';
			$tbl .='<td>'.$select_akun.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		
        $q = "
				SELECT T.noakun,T.namaakun,SUM(T.debet) debet,SUM(T.kredit) kredit FROM (
					SELECT 
					H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tklaim' as ref_tabel
					,'tvalidasi_klaim' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_piutang as idakun
					,A.noakun,A.namaakun
					,SUM(CASE WHEN D.posisi_piutang='D' THEN D.nominal_piutang ELSE 0 END) debet
					,SUM(CASE WHEN D.posisi_piutang='K' THEN D.nominal_piutang ELSE 0 END) kredit
					,CONCAT('PIUTANG ',H.notransaksi,' | ',H.nama_rekanan) as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
					,H.created_by,H.created_date,H.created_nama,D.posisi_piutang as posisi_akun
					FROM tvalidasi_klaim H
					LEFT JOIN tvalidasi_klaim_detail D ON D.idvalidasi=H.id
					LEFT JOIN makun_nomor A ON A.id=D.idakun_piutang
					WHERE H.id='$id' AND D.nominal_piutang > 0
					GROUP BY H.id,D.idakun_piutang,D.posisi_piutang

					UNION ALL

					SELECT 
					H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tklaim' as ref_tabel
					,'tvalidasi_klaim' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_income as idakun
					,A.noakun,A.namaakun
					,SUM(CASE WHEN D.posisi_income='D' THEN D.nominal_other_income ELSE 0 END) debet
					,SUM(CASE WHEN D.posisi_income='K' THEN D.nominal_other_income ELSE 0 END) kredit
					,CONCAT('OTHER INCOME PIUTANG ',H.notransaksi,' | ',H.nama_rekanan) as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
					,H.created_by,H.created_date,H.created_nama,D.posisi_income as posisi_akun
					FROM tvalidasi_klaim H
					LEFT JOIN tvalidasi_klaim_detail D ON D.idvalidasi=H.id
					LEFT JOIN makun_nomor A ON A.id=D.idakun_income
					WHERE H.id='$id' AND D.nominal_other_income > 0
					GROUP BY H.id,D.idakun_income,D.posisi_income


					UNION ALL

					SELECT 
					H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tklaim' as ref_tabel
					,'tvalidasi_klaim' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_other_loss as idakun
					,A.noakun,A.namaakun
					,SUM(CASE WHEN D.posisi_other_loss='D' THEN D.nominal_other_loss ELSE 0 END) debet
					,SUM(CASE WHEN D.posisi_other_loss='K' THEN D.nominal_other_loss ELSE 0 END) kredit
					,CONCAT('OTHER LOSS PIUTANG ',H.notransaksi,' | ',H.nama_rekanan) as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
					,H.created_by,H.created_date,H.created_nama,D.posisi_other_loss as posisi_akun
					FROM tvalidasi_klaim H
					LEFT JOIN tvalidasi_klaim_detail D ON D.idvalidasi=H.id
					LEFT JOIN makun_nomor A ON A.id=D.idakun_other_loss
					WHERE H.id='$id' AND D.nominal_other_loss > 0
					GROUP BY H.id,D.idakun_other_loss,D.posisi_other_loss


					UNION ALL

					SELECT 
					H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tklaim' as ref_tabel
					,'tvalidasi_klaim' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun as idakun
					,A.noakun,A.namaakun
					,SUM(CASE WHEN D.posisi_akun='D' THEN D.nominal_bayar ELSE 0 END) debet
					,SUM(CASE WHEN D.posisi_akun='K' THEN D.nominal_bayar ELSE 0 END) kredit
					,CONCAT('PEMBAYARAN PIUTANG ',H.notransaksi,' | ',H.nama_rekanan) as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
					,H.created_by,H.created_date,H.created_nama,D.posisi_akun
					FROM tvalidasi_klaim H
					LEFT JOIN tvalidasi_klaim_bayar D ON D.idvalidasi=H.id
					LEFT JOIN makun_nomor A ON A.id=D.idakun
					WHERE H.id='$id' AND D.nominal_bayar > 0
					GROUP BY H.id,D.idakun,D.posisi_akun
					) T 
					WHERE (T.debet + T.kredit) > 0
					GROUP BY T.idakun,T.posisi_akun
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td class="text-right">'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		$hasil .='<option value="0" '.($idakun=="0"?"selected":"").'>TIDAK ADA</option>';
		return $hasil;
	}
}
