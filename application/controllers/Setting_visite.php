<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_visite extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_visite_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('2218'))){
				$tab='1';
			}
		}
		if (UserAccesForm($user_acces_form,array('2218'))){
			$data = $this->Setting_visite_model->get_assesmen_setting();
			// $data_label = $this->Setting_visite_model->get_assesmen_label();
			// print_r($data);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			
			// $data['st_duplikate'] 			= '0';
			// $data['st_ruangan'] 			= '0';
			// $data['st_kelas'] 			= '0';
			// $data['st_diskon'] 			= '0';
			// $data['st_kuantitas'] 			= '0';
			$data['idtarif'] 			= '#';
			$data['idkelompokpasien'] 			= '#';
			$data['idrekanan'] 			= '0';
			$data['idruangan'] 			= '0';
			$data['kategori_dokter'] 			= '0';
			$data['iddokter'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Visite Dokter';
			$data['content'] 		= 'Setting_visite/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Bedah",'#'),
												  array("Pengaturan Visite Dokter",'setting_visite/index')
												);

			$data = array_merge($data,backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_assesmen(){
		if ($this->Setting_visite_model->save_assesmen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_visite/index/1','location');
		}
	}
	

  function simpan_tarif(){
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=($idkelompokpasien!='1'?'0':$this->input->post('idrekanan'));
		$idruangan=$this->input->post('idruangan');
		$kategori_dokter=$this->input->post('kategori_dokter');
		$iddokter=$this->input->post('iddokter');
		$idtarif=$this->input->post('idtarif');
		$iddetail=$this->input->post('iddetail');

		$hasil=$this->cek_duplicate_tarif($idkelompokpasien,$idrekanan,$idruangan,$kategori_dokter,$iddokter,$iddetail);
		if ($hasil==null){	
			$data=array(
				'idkelompokpasien' => $idkelompokpasien,
				'idrekanan' => $idrekanan,
				'idruangan' => $idruangan,
				'kategori_dokter' => $kategori_dokter,
				'iddokter' => $iddokter,
				'idtarif' => $idtarif,

			);
			if ($iddetail==''){
				$data['created_by']=$this->session->userdata('user_id');
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('setting_visite_tarif',$data);
			}else{
				$data['edited_by']=$this->session->userdata('user_id');
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$iddetail);
				$hasil=$this->db->update('setting_visite_tarif',$data);
			}
			
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_tarif($idkelompokpasien,$idrekanan,$idruangan,$kategori_dokter,$iddokter,$iddetail){
		$where='';
		if ($iddetail!=''){
			$where =" AND id !='$iddetail'";
		}
		$q="SELECT *FROM setting_visite_tarif WHERE idkelompokpasien='$idkelompokpasien' 
			AND idrekanan='$idrekanan' AND idruangan='$idruangan' AND kategori_dokter='$kategori_dokter' AND iddokter='$iddokter' ".$where;
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
	
	function load_tarif()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT
						MK.nama as nama_kelompok
						,MR.nama as nama_rekanan,MRU.nama as nama_ruangan,MKT.nama as nama_kategori,MD.nama as nama_dokter
						,MT.nama as nama_tarif, H.* 
						FROM `setting_visite_tarif` H
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mdokter_kategori MKT ON MKT.id=H.kategori_dokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN mtarif_visitedokter MT ON MT.id=H.idtarif
						ORDER BY H.idkelompokpasien,H.idrekanan,H.idruangan,H.kategori_dokter,H.iddokter
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_kelompok','nama_ruangan','nama_kategori','nama_tarif','nama_rekanan','nama_dokter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nama_kelompok);
          $result[] = ($r->idrekanan=='0'?text_default('SEMUA'):$r->nama_rekanan);
          $result[] = ($r->idruangan=='0'?text_default('SEMUA'):$r->nama_ruangan);
          $result[] = ($r->kategori_dokter=='0'?text_default('SEMUA'):$r->nama_kategori);
          $result[] = ($r->iddokter=='0'?text_default('SEMUA'):$r->nama_dokter);
          $result[] = ($r->nama_tarif);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_tarif('.$r->id.')" type="button" title="Edit Tarif" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_tarif('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_tarif(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_visite_tarif',$this);
	  
	  json_encode($hasil);
	  
  }

  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function find_poli($idtipe){
	  $q="SELECT *FROM mpoliklinik H WHERE H.idtipe='$idtipe' AND H.`status`='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function find_rekanan(){
	  $idtipe=$this->input->post('idkelompokpasien');
	  $q="SELECT *FROM mrekanan H WHERE H.status='1' ";
	  $opsi='<option value="0" selected>-SEMUA-</option>';
	  if ($idtipe=='1' || $idtipe=='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function get_tarif(){
	  $idtarif=$this->input->post('idtarif');
	  $q="SELECT *FROM setting_visite_tarif H WHERE H.id='$idtarif' ";
	  // print_r($q);exit;
	  $data=$this->db->query($q)->row_array();
	  
	  $this->output->set_output(json_encode($data));
  }
  function find_dokter($idtipe){
	  $where='';
	  if ($idtipe!='0'){
		  $where.=" AND H.idkategori='$idtipe'";
	  }
	  $q="SELECT *FROM mdokter H WHERE H.status='1' ".$where;
	  $opsi='<option value="0" selected>-SEMUA-</option>';
	  
	  $hasil=$this->db->query($q)->result();
	  foreach ($hasil as $r){
		$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
		  
	  }
	  $this->output->set_output(json_encode($opsi));
  }
}
