<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Trm_berkas extends CI_Controller
{

    /**
     * Berkas Rekam Medis controller
     * Developer Acep Kursina
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_berkas_model', 'model');
        $this->load->model('Trujukan_radiologi_model');
        $this->load->model('Trujukan_laboratorium_model');
        $this->load->model('Tpoliklinik_tindakan_model');
    }

    public function index()
    {
        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];
        if (UserAccesForm($user_acces_form, array('1167'))) {
            $data=array();
            $data['error'] = '';
            $data['title'] = 'Berkas Rekam Medis';
            $data['content'] = 'Trm_berkas/index';
            $data['tombol'] = 'index';
            $data['breadcrum'] = [
                ["RSKB Halmahera",'#'],
                ["Berkas Rekam Medis",'trm_berkas/index'],
                ["List",'#']
            ];
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            redirect('page404');
        }
    }

    public function getIndex()
    {
        $nomedrec     	= $this->input->post('nomedrec');
        $namapasien     	= $this->input->post('namapasien');
        $tanggal_lahir    	= $this->input->post('tanggal_lahir');
        $last_visit    	= $this->input->post('last_visit');
        $in_activedari     	= $this->input->post('in_activedari');
        $in_activesampai     	= $this->input->post('in_activesampai');
        $ret_dari     	= $this->input->post('ret_dari');
        $ret_sampai     	= $this->input->post('ret_sampai');
        $status_aktif     	= $this->input->post('status_aktif');
        $status_kodefikasi     	= $this->input->post('status_kodefikasi');

        $iduser=$this->session->userdata('user_id');

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where=" WHERE M.id <> '' ";
        if ($status_aktif  != '#') {
            $where .=" AND M.st_aktif ='".$status_aktif."'";
        }
        if ($status_kodefikasi  != '#') {
            $where .=" AND T.st_kodefikasi ='".$status_kodefikasi."'";
        }
        if ($tanggal_lahir != '') {
            $where.= " AND DATE(M.tanggal_lahir) ='".YMDFormat($tanggal_lahir)."'";
        }
        if ($last_visit != '') {
            $where.= " AND DATE(last_visit) ='".YMDFormat($last_visit)."'";
        }
        if ($in_activedari != '' && $in_activesampai != '') {
            $where.= " AND (DATE(est_in) >='".YMDFormat($in_activedari)."' AND DATE(est_in) <='".YMDFormat($in_activesampai)."') ";
        }
        if ($ret_dari != '' && $ret_sampai != '') {
            $where.= " AND (DATE(est_retensi) >='".YMDFormat($ret_dari)."' AND DATE(est_retensi) <='".YMDFormat($ret_sampai)."') ";
        }
        if ($nomedrec  != '') {
            $where .=" AND M.no_medrec LIKE '%".$nomedrec."%' ";
        }
        if ($namapasien  != '') {
            $where .=" AND M.nama LIKE '%".$namapasien."%' ";
        }

        $from="(SELECT M.id,M.no_medrec,M.nama,M.tanggal_lahir,M.last_visit,M.est_in,M.est_retensi,M.st_aktif,M.`status`
				,T.st_kodefikasi,T.st_verifikasi
				,SUM(CASE WHEN T.status_kirim='1' THEN 1 ELSE 0 END) jml_belum_kembali
				,SUM(CASE WHEN T.st_kodefikasi='0' THEN 1 ELSE 0 END) jml_belum_kode
				,SUM(CASE WHEN T.st_verifikasi='0' THEN 1 ELSE 0 END) jml_belum_verifikasi
				,SUM(CASE WHEN T.st_analisa='0' THEN 1 ELSE 0 END) jml_belum_analisa,
				M.lokasi_berkas
				from mfpasien M
				LEFT JOIN trm_layanan_berkas T ON T.idpasien=M.id

				".$where."
				GROUP BY M.id ) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $r->id;
            $row[] = $r->no_medrec;
            $row[] = $r->nama;
            $row[] =($r->tanggal_lahir)?HumanDateShort($r->tanggal_lahir):'';
            $row[] = $r->lokasi_berkas;
            $row[] =($r->last_visit)?HumanDateShort($r->last_visit):'';
            $row[] =($r->est_in)?HumanDateShort($r->est_in):'';
            $row[] =($r->est_retensi)?HumanDateShort($r->est_retensi):'';
            $row[] = $this->status_berkas($r->st_aktif);
            $row[] = text_danger($r->jml_belum_kembali).' '.text_info($r->jml_belum_kode).' '.text_success($r->jml_belum_verifikasi).' '.text_primary($r->jml_belum_analisa);
            $aksi='<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('1169'))){
            $aksi.='<a href="'.site_url().'trm_berkas/profile/'.$r->id.'" class="btn btn-xs btn-primary riwayat" title="Riwayat"><i class="fa fa-file-text-o"></i></a>';
			}
            if ($r->st_aktif=='1') {
				if (UserAccesForm($user_acces_form,array('1180'))){
                $aksi.='<button class="btn btn-xs btn-danger nonaktif" title="Non Aktifkan"><i class="si si-ban"></i></button>';
				}
            }
            if ($r->st_aktif=='4') {
				if (UserAccesForm($user_acces_form,array('1181'))){
                $aksi.='<button class="btn btn-xs btn-warning aktifkan" title="Aktifkan Kembali"><i class="glyphicon glyphicon-export"></i></button>';
				}
            }
			if (UserAccesForm($user_acces_form,array('1182'))){
            $aksi.='<a href="'.site_url().'trm_gabung/index/'.$r->id.'" target="_blank" class="btn btn-xs btn-primary" title="Gabung Medrec"><i class="fa fa-sign-in"></i></a>';
            }
			if (UserAccesForm($user_acces_form,array('1183'))){
			$aksi.='<a href="'.site_url().'trm_berkas/kodetifikasi/'.$r->id.'" class="btn btn-xs btn-info kodetifikasi_trx" title="Transaksi Kodefikasi"><i class="fa fa-newspaper-o"></i></a>';
            }
			if (UserAccesForm($user_acces_form,array('1191'))){
			$aksi.='<a href="'.site_url().'trm_analisa/proses/'.$r->id.'" class="btn btn-xs btn-warning analisa" title="Analisa Kuantitatif Rekam medis"><i class="si si-graph"></i></a>';
			}
			if (UserAccesForm($user_acces_form,array('1192'))){
            $aksi.='<button class="btn btn-xs btn-primary upload" title="Upload"><i class="fa fa-upload"></i></button>';
			}

            $aksi.='</div>';

            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_riwayat_berkas()
    {
        $idpasien    	= $this->input->post('idpasien');

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';

        $from="(SELECT REF.*,
					CASE
					WHEN REF.tujuan='3'THEN
						CONCAT(K.nama,'/',B.nama,' - ',D.nama)
					WHEN REF.tujuan='5'THEN
						REF.catatan
					ELSE
						CONCAT(P.nama,' - ',D.nama)
					END as detail,
				CASE WHEN REF.statuspasienbaru='0' THEN 'PASIEN LAMA' ELSE 'PASIEN BARU' END as status_pasien,
				U.name as nama_user_trx
				FROM trm_layanan_berkas as REF
				LEFT JOIN mdokter D ON D.id=REF.iddokter
				LEFT JOIN mpoliklinik P ON P.id=REF.idpoliklinik
				LEFT JOIN mbed B ON B.id=REF.bed
				LEFT JOIN musers U ON U.id=REF.user_trx
				LEFT JOIN mkelas K ON K.id=REF.kelas
				WHERE REF.idpasien='$idpasien' ORDER BY REF.tanggal_trx DESC) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] =HumanDateLong($r->tanggal_trx);
            $row[] = ($r->jenis_pelayanan=='1' ? text_success('PELAYANAN') : text_primary('PINJAM'));
            $row[] = ($r->statuspasienbaru=='1' ? text_danger('PASIEN BARU') : text_info('PASIEN LAMA'));
            $row[] = $r->no_medrec;
            $row[] = $r->title.' '.$r->namapasien;
            $row[] = $this->tujuan($r->tujuan);
            $row[] = $r->detail;
            $aksi='<div class="btn-group">';
            if ($r->status_kirim=='1') {
                $aksi.='<button class="btn btn-xs btn-primary kembali" title="Kembali Berkas"><i class="si si-arrow-left"></i></button>';
                $aksi.='<button class="btn btn-xs btn-info edit" title="Edit Berkas"><i class="fa fa-pencil"></i></button>';
            }

            $aksi.='<button class="btn btn-xs btn-success cetak" title="Print Berkas"><i class="fa fa-print"></i></button>';

            $aksi.='</div>';
            if ($r->status=='0') {
                $row[] =text_danger('DIBATALKAN');
                $row[] ='';
            } else {
                $row[] = ($r->status_kirim=='1' ? text_danger('BELUM KEMBALI') : text_success('TELAH KEMBALI'));
                ;
                $row[] =  ($r->status_kirim=='2' ? getDurasi($r->tanggal_kirim, $r->tanggal_kembali) : getDurasi($r->tanggal_kirim, date('Y-m-d H:i:s')));
            }
            $row[] = $r->nama_user_trx;
            $row[] = $r->user_kirim_nama;
            $row[] = $aksi;
            $row[] = $r->id;
            $row[] = $r->status_kirim;
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_berkas_pasien()
    {
        $idpasien    	= $this->input->post('idpasien');
        $filter_idjenis    	= $this->input->post('filter_idjenis');

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';
        if ($filter_idjenis != '#') {
            $where .=" AND REF.tujuan='$filter_idjenis'";
        }
        $from="(SELECT REF.*,
					CASE
					WHEN REF.tujuan='3'THEN
						CONCAT(K.nama,'/',B.nama,' - ',D.nama)
					WHEN REF.tujuan='5'THEN
						REF.catatan
					ELSE
						CONCAT(P.nama,' - ',D.nama)
					END as detail,D.nama as namadokter
				FROM trm_layanan_berkas as REF
				LEFT JOIN mdokter D ON D.id=REF.iddokter
				LEFT JOIN mpoliklinik P ON P.id=REF.idpoliklinik
				LEFT JOIN mbed B ON B.id=REF.bed
				LEFT JOIN musers U ON U.id=REF.user_trx
				LEFT JOIN mkelas K ON K.id=REF.kelas
				WHERE REF.idpasien='$idpasien' AND REF.status <>'0'  AND REF.status_kirim <>'0'  AND REF.tujuan <>'5' ".$where." ORDER BY REF.tanggal_trx DESC) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] =$this->tujuan($r->tujuan).'<br>'.HumanDateLong($r->tanggal_trx).'<br>'.$r->namadokter;
            $aksi='<div class="btn-group">';
            $Status='';
            if ($r->status_kirim=='1') {
				if (UserAccesForm($user_acces_form,array('1222'))){
                $aksi.='<button class="btn btn-xs btn-danger kembali" title="Kembalikan Berkas"><i class="si si-arrow-left"></i></button>';
				}
                $status=text_danger('BELUM KEMBALI');
            } elseif ($r->st_kodefikasi=='0') {
				if (UserAccesForm($user_acces_form,array('1186'))){
                $aksi.='<button class="btn btn-xs btn-primary kodetifikasi" title="Kodefikasi"><i class="si si-arrow-right"></i></button>';
				}
                $status=text_default('BELUM DIKODEFIKASI');
            } elseif ($r->st_kodefikasi=='1' && $r->st_verifikasi=='0') {
				if (UserAccesForm($user_acces_form,array('1188'))){
                $aksi.='<button class="btn btn-xs btn-success verifikasi" title="Verifikasi"><i class="si si-check"></i></button>';
				}
                $status=text_success('TELAH DIKODEFIKASI');
            } elseif ($r->st_verifikasi=='1') {
				if (UserAccesForm($user_acces_form,array('1188'))){
                $aksi.='<button class="btn btn-xs btn-success verifikasi" title="Verifikasi"><i class="si si-check"></i></button>';
				}
				if (UserAccesForm($user_acces_form,array('1190'))){
                $aksi.='<button class="btn btn-xs btn-default lihat" title="Lihat Hasil Kodetifikasi"><i class="si si-eye"></i></button>';
				}
                $status=text_primary('TELAH DIVERIFIKASI');
            }

            $aksi.='</div>';

            $row[] =$status;
            $row[] = $aksi;
            $row[] = $r->id;
            $row[] = $r->tujuan;
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_riwayat_kunjungan()
    {
        $idpasien = $this->input->post('idpasien');

        $this->select = array(
          'view_berkas_kunjungan_union.idpoliklinik',
          'view_berkas_kunjungan_union.idtipe',
          'view_berkas_kunjungan_union.tanggal',
          'view_berkas_kunjungan_union.tujuan',
          'view_berkas_kunjungan_union.klinik',
          'view_berkas_kunjungan_union.dokter',
          'view_berkas_kunjungan_union.user_daftar',
          'view_berkas_kunjungan_union.status',
          'view_berkas_kunjungan_union.tindaklanjut',
        );
        $this->join 	= array();
        $this->where  = array(
          'view_berkas_kunjungan_union.idpasien' => $idpasien
        );
        $this->order  = array();
        $this->group  = array();
        $this->from   = "view_berkas_kunjungan_union";
        $this->column_search   = array('tujuan','klinik','dokter','user_daftar','status','tindaklanjut');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);

        $data = array();
        foreach ($list as $r) {
            $row = array();
            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->tujuan;
            $row[] = $r->klinik;
            $row[] = $r->dokter;
            $row[] = $r->user_daftar;

            if ($r->status == 'Telah Ditindak') {
                $row[] = text_success($r->status);
            } else {
                $row[] = text_danger($r->status);
            }

            $row[] = $r->tindaklanjut;
            $row[] = '<a href="'.base_url().'tpoliklinik_pendaftaran/print_document/'.$r->idpoliklinik.'/'.$r->idtipe.'" target="_blank" class="btn btn-xs btn-primary print" title="Print"><i class="fa fa-print"></i></a>';

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_riwayat_pemeriksaan()
    {
        $idpasien = $this->input->post('idpasien');

        $this->select = array(
          'view_berkas_pemeriksaan_union.idpendaftaran',
          'view_berkas_pemeriksaan_union.idpoliklinik',
          'view_berkas_pemeriksaan_union.idtipe',
          'view_berkas_pemeriksaan_union.tanggal',
          'view_berkas_pemeriksaan_union.notransaksi',
          'view_berkas_pemeriksaan_union.tujuan',
          'view_berkas_pemeriksaan_union.klinik',
          'view_berkas_pemeriksaan_union.dokter',
          'view_berkas_pemeriksaan_union.user_daftar',
          'view_berkas_pemeriksaan_union.tindaklanjut',
        );
        $this->join 	= array();
        $this->where  = array(
          'view_berkas_pemeriksaan_union.idpasien' => $idpasien
        );
        $this->order  = array();
        $this->group  = array();
        $this->from   = "view_berkas_pemeriksaan_union";
        $this->column_search   = array('notransaksi','tujuan','klinik','dokter','user_daftar','tindaklanjut');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);

        $data = array();
        foreach ($list as $r) {
            $row = array();
            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->notransaksi;
            $row[] = $r->tujuan;
            $row[] = $r->klinik;
            $row[] = $r->dokter;
            $row[] = $r->user_daftar;
            $row[] = $r->tindaklanjut;

            if ($r->reference = 'rawatjalan') {
              $row[] = '<button class="btn btn-xs btn-success btn-detail-pemeriksaan" data-reference="'.$r->reference.'" data-idpendaftaran="'.$r->idpendaftaran.'"><i class="si si-arrow-down"></i></button>
                        <a href="'.base_url().'tpoliklinik_pendaftaran/print_document/'.$r->idpoliklinik.'/'.$r->idtipe.'" target="_blank" class="btn btn-xs btn-primary print" title="Print"><i class="fa fa-print"></i></a>';
            } else {
              $row[] = '<button class="btn btn-xs btn-success btn-detail-pemeriksaan" data-reference="'.$r->reference.'" data-idpendaftaran="'.$r->idpendaftaran.'"><i class="si si-arrow-down"></i></button>
                        <a href="'.base_url().'tpoliklinik_pendaftaran/print_document/'.$r->idpoliklinik.'/'.$r->idtipe.'" target="_blank" class="btn btn-xs btn-primary print" title="Print"><i class="fa fa-print"></i></a>';
            }

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_riwayat_transaksi()
    {
        $idpasien = $this->input->post('idpasien');

        $this->select = array(
          'view_berkas_transaksi_union.idpendaftaran',
          'view_berkas_transaksi_union.idkasir',
          'view_berkas_transaksi_union.tanggal',
          'view_berkas_transaksi_union.notransaksi',
          'view_berkas_transaksi_union.nokasir',
          'view_berkas_transaksi_union.tujuan',
          'view_berkas_transaksi_union.klinik',
          'view_berkas_transaksi_union.dokter',
          'view_berkas_transaksi_union.kelompok_pasien',
          'view_berkas_transaksi_union.user_kasir',
          'view_berkas_transaksi_union.total',
        );
        $this->join 	= array();
        $this->where  = array(
          'view_berkas_transaksi_union.idpasien' => $idpasien
        );
        $this->order  = array();
        $this->group  = array();
        $this->from   = "view_berkas_transaksi_union";
        $this->column_search   = array('notransaksi','nokasir','tujuan','klinik','dokter','kelompok','user_kasir');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);

        $data = array();
        foreach ($list as $r) {
            $row = array();
            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->notransaksi;
            $row[] = $r->nokasir;
            $row[] = $r->tujuan;
            $row[] = $r->klinik;
            $row[] = $r->dokter;
            $row[] = $r->kelompok_pasien;
            $row[] = $r->user_kasir;
            $row[] = number_format($r->total);

            if ($r->reference = 'rawatjalan') {
              $row[] = '<a href="'.base_url().'tkasir/print_transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'" target="_blank" class="btn btn-xs btn-primary print" title="Print"><i class="fa fa-print"></i></a>';
            } else {
              $row[] = '<a href="'.base_url().'trawatinap_tindakan/print_rincian_biaya/'.$r->idpendaftaran.'/0" target="_blank" class="btn btn-xs btn-primary print" title="Print"><i class="fa fa-print"></i></a>';
            }

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_riwayat_therapy()
    {
        $idpasien = $this->input->post('idpasien');

        $this->select = array(
          'view_berkas_terapi_union.idpendaftaran',
          'view_berkas_terapi_union.idfarmasi',
          'view_berkas_terapi_union.tanggal',
          'view_berkas_terapi_union.notransaksi',
          'view_berkas_terapi_union.tujuan',
          'view_berkas_terapi_union.klinik',
          'view_berkas_terapi_union.dokter',
          'view_berkas_terapi_union.kelompok_pasien',
          'view_berkas_terapi_union.total_obat_nonracikan',
          'view_berkas_terapi_union.total_obat_racikan',
          'view_berkas_terapi_union.total_alkes_nonracikan',
          'view_berkas_terapi_union.total_alkes_racikan',
        );
        $this->join 	= array();
        $this->where  = array(
          'view_berkas_terapi_union.idpasien' => $idpasien
        );
        $this->order  = array();
        $this->group  = array('view_berkas_terapi_union.idpendaftaran','view_berkas_terapi_union.idfarmasi');
        $this->from   = "view_berkas_terapi_union";
        $this->column_search   = array('notransaksi','tujuan','klinik','dokter','kelompok');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);

        $data = array();
        foreach ($list as $r) {
            $total_obat = $r->total_obat_nonracikan + $r->total_obat_racikan;
            $total_alkes = $r->total_alkes_nonracikan + $r->total_alkes_racikan;

            $row = array();
            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->notransaksi;
            $row[] = $r->tujuan;
            $row[] = $r->klinik;
            $row[] = $r->dokter;
            $row[] = $r->kelompok_pasien;
            $row[] = number_format($total_obat);
            $row[] = number_format($total_alkes);
            $row[] = number_format($total_obat + $total_alkes);
            $row[] = '<button class="btn btn-xs btn-success btn-detail-theraphy" data-idfarmasi="'.$r->idfarmasi.'"><i class="si si-arrow-down"></i></button>';

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_riwayat_radiologi()
    {
        $idpasien = $this->input->post('idpasien');

        $this->select = array(
          'view_berkas_radiologi_union.tanggal',
          'view_berkas_radiologi_union.idrujukan',
          'view_berkas_radiologi_union.notransaksi',
          'view_berkas_radiologi_union.tujuan',
          'view_berkas_radiologi_union.dokter_perujuk',
          'view_berkas_radiologi_union.kelompok_pasien',
          'view_berkas_radiologi_union.dokter_radiologi',
          'view_berkas_radiologi_union.petugas',
          'view_berkas_radiologi_union.total'
        );
        $this->join 	= array();
        $this->where  = array(
          'view_berkas_radiologi_union.idpasien' => $idpasien
        );
        $this->order  = array();
        $this->group  = array();
        $this->from   = "view_berkas_radiologi_union";
        $this->column_search   = array('notransaksi','tujuan','dokter_perujuk','kelompok_pasien','dokter_radiologi','petugas');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);

        $data = array();
        foreach ($list as $r) {
            $row = array();
            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->notransaksi;
            $row[] = $r->tujuan;
            $row[] = $r->dokter_perujuk;
            $row[] = $r->kelompok_pasien;
            $row[] = $r->dokter_radiologi;
            $row[] = $r->petugas;
            $row[] = number_format($r->total);
            $row[] = '<button class="btn btn-xs btn-success btn-detail-radiologi" data-idradiologi="'.$r->idrujukan.'"><i class="si si-arrow-down"></i></button>';

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function get_riwayat_icd9()
    {
       $idpasien    	= $this->input->post('idpasien');

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';

        $from="(SELECT H.id, H.jenis_pelayanan, H.iddokter,dok.nama as nama_dokter,H.idpasien,H.tanggal_trx,D.kelompok_tindakan_id,
				D.tindakan_id,D.icd_id,D.jenis_id,icd.kode,icd.deskripsi,T.nama as tindakan_layanan,K.nama as kel_tindakan,
				CASE WHEN D.jenis_id='1' THEN 'UTAMA' ELSE 'TAMBAHAN' END as jenis_nama,musers.`name` as user_trx,H.user_nama_verifikasi
					FROM trm_berkas_icd9 D
					INNER JOIN trm_layanan_berkas H ON H.id=D.idberkas
					LEFT JOIN mdokter dok ON dok.id=H.iddokter
					LEFT JOIN icd_9 icd ON icd.id=D.icd_id
					LEFT JOIN mtindakan2icd T ON T.id=D.tindakan_id
					LEFT JOIN mkelompok_tindakan K ON K.id=D.kelompok_tindakan_id
					LEFT JOIN musers ON musers.id=H.user_trx
					WHERE H.idpasien='$idpasien' AND D.`status`='1' AND D.jenis_id='1'  AND H.tujuan <> '5'
					ORDER BY H.tanggal_trx DESC) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array('nama_dokter','kel_tindakan','kode','deskripsi','tindakan_layanan','user_trx','user_nama_verifikasi');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $row = array();
            $row[] = DMYFormat($r->tanggal_trx);
            $row[] = $r->nama_dokter;
            $row[] = $r->kel_tindakan;
            $row[] = $r->kode;
            $row[] = $r->deskripsi;
            $row[] = $r->tindakan_layanan;
            $row[] = $r->jenis_nama;
            $row[] = $r->user_trx;
            $row[] = $r->user_nama_verifikasi;
            $row[] = '<button class="btn btn-xs btn-success btn-detail-icd9" data-idheader="'.$r->id.'"><i class="si si-arrow-down"></i></button>';

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function get_riwayat_icdx()
    {
       $idpasien    	= $this->input->post('idpasien');

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';

        $from="(SELECT T1.*,CASE WHEN T1.statuskasus='1' THEN 'KASUS BARU' WHEN T1.statuskasus='2' THEN 'KASUS LAMA' ELSE '-' END nama_kasus,
				CASE
					WHEN T1.tujuan='1' THEN  TP.diagnosa
					WHEN T1.tujuan='2' THEN  TP.diagnosa
					WHEN T1.tujuan='3' THEN  CO.diagnosa
					WHEN T1.tujuan='4' THEN  KO.diagnosa
				END as diagnosa

				FROM (SELECT H.id, H.tujuan, H.tanggal_trx,mdokter.nama as nama_dokter,H.idpasien,icd.kode,icd.deskripsi,D.jenis_id,H.id_trx,
				CASE
					WHEN D.jenis_id='1' THEN 'UTAMA'
					WHEN D.jenis_id='2' THEN 'TAMBAHAN'
					WHEN D.jenis_id='3' THEN 'KONTROL'
					WHEN D.jenis_id='4' THEN 'EXTERNAL CAUSE'
				END as jenis_nama,musers.`name` as user_trx,H.user_nama_verifikasi
				,TP.statuskasus
				FROM trm_berkas_icd10 D
				INNER JOIN trm_layanan_berkas H ON H.id=D.idberkas AND H.tujuan <> '5'
				LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=H.id_trx
				LEFT JOIN mdokter ON mdokter.id=H.iddokter
				LEFT JOIN icd_10 icd ON icd.id=D.icd_id
				LEFT JOIN musers ON musers.id=H.user_trx
				WHERE H.idpasien='$idpasien' AND D.jenis_id='1'
				ORDER BY H.tanggal_trx DESC) as T1
				LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=T1.id_trx AND (T1.tujuan='1' OR T1.tujuan='2')
				LEFT JOIN trawatinap_checkout CO ON CO.idrawatinap=T1.id_trx AND T1.tujuan='3'
				LEFT JOIN tkamaroperasi_pendaftaran KO ON KO.idpendaftaran=T1.id_trx AND T1.tujuan='4' ) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array('nama_dokter','diagnosa','kode','deskripsi','nama_kasus','user_trx','user_nama_verifikasi');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $row = array();
            $row[] = DMYFormat($r->tanggal_trx);
            $row[] = $r->nama_dokter;
            $row[] = '-';
            $row[] = $r->diagnosa;
            $row[] = $r->kode;
            $row[] = $r->deskripsi;
            $row[] = $r->jenis_nama;
            $row[] = $r->nama_kasus;
            $row[] = $r->user_trx;
            $row[] = $r->user_nama_verifikasi;
            $row[] = '<button class="btn btn-xs btn-success btn-detail-icdx" data-idheader_icdx="'.$r->id.'"><i class="si si-arrow-down"></i></button>';

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function getDataICDX($id)
    {
      $query = $this->db->query("SELECT T1.*,CASE WHEN T1.statuskasus='1' THEN 'KASUS BARU' WHEN T1.statuskasus='2' THEN 'KASUS LAMA' ELSE '-' END nama_kasus,
				CASE
					WHEN T1.tujuan='1' THEN  TP.diagnosa
					WHEN T1.tujuan='2' THEN  TP.diagnosa
					WHEN T1.tujuan='3' THEN  CO.diagnosa
					WHEN T1.tujuan='4' THEN  KO.diagnosa
				END as diagnosa

				FROM (SELECT H.id, H.tujuan, H.tanggal_trx,mdokter.nama as nama_dokter,H.idpasien,icd.kode,icd.deskripsi,D.jenis_id,H.id_trx,
				CASE
					WHEN D.jenis_id='1' THEN 'UTAMA'
					WHEN D.jenis_id='2' THEN 'TAMBAHAN'
					WHEN D.jenis_id='3' THEN 'KONTROL'
					WHEN D.jenis_id='4' THEN 'EXTERNAL CAUSE'
				END as jenis_nama,musers.`name` as user_trx,H.user_nama_verifikasi
				,TP.statuskasus
				FROM trm_berkas_icd10 D
				INNER JOIN trm_layanan_berkas H ON H.id=D.idberkas AND H.tujuan <> '5'
				LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=H.id_trx
				LEFT JOIN mdokter ON mdokter.id=H.iddokter
				LEFT JOIN icd_10 icd ON icd.id=D.icd_id
				LEFT JOIN musers ON musers.id=H.user_trx
				WHERE H.id='$id' AND D.jenis_id !='1'
				ORDER BY H.tanggal_trx DESC) as T1
				LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=T1.id_trx AND (T1.tujuan='1' OR T1.tujuan='2')
				LEFT JOIN trawatinap_checkout CO ON CO.idrawatinap=T1.id_trx AND T1.tujuan='3'
				LEFT JOIN tkamaroperasi_pendaftaran KO ON KO.idpendaftaran=T1.id_trx AND T1.tujuan='4' ");
      $result = $query->result();
      echo json_encode($result);
    }
    public function get_riwayat_laboratorium()
    {
        $idpasien = $this->input->post('idpasien');

        $this->select = array(
          'view_berkas_laboratorium_union.tanggal',
          'view_berkas_laboratorium_union.idrujukan',
          'view_berkas_laboratorium_union.notransaksi',
          'view_berkas_laboratorium_union.tujuan',
          'view_berkas_laboratorium_union.dokter_perujuk',
          'view_berkas_laboratorium_union.kelompok_pasien',
          'view_berkas_laboratorium_union.dokter_laboratorium',
          'view_berkas_laboratorium_union.petugas',
          'view_berkas_laboratorium_union.total'
        );
        $this->join 	= array();
        $this->where  = array(
          'view_berkas_laboratorium_union.idpasien' => $idpasien
        );
        $this->order  = array();
        $this->group  = array();
        $this->from   = "view_berkas_laboratorium_union";
        $this->column_search   = array('notransaksi','tujuan','dokter_perujuk','kelompok_pasien','dokter_laboratorium','petugas');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);

        $data = array();
        foreach ($list as $r) {
            $row = array();
            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->notransaksi;
            $row[] = $r->tujuan;
            $row[] = $r->dokter_perujuk;
            $row[] = $r->kelompok_pasien;
            $row[] = $r->dokter_laboratorium;
            $row[] = $r->petugas;
            $row[] = number_format($r->total);
            $row[] = '<button class="btn btn-xs btn-success btn-detail-laboratorium" data-idlaboratorium="'.$r->idrujukan.'"><i class="si si-arrow-down"></i></button>';

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function profile($idpasien)
    {
        $data=array();
        $data=$this->model->get_data_pribadi($idpasien);
        $data['idpasien'] = $idpasien;
        $data['error'] = '';
        $data['content'] = 'Trm_berkas/profile';
        $data['title'] = 'Berkas '.$data['nama'];
        $data['breadcrum'] = [
            ["RSKB Halmahera",'#'],
            ["Berkas Rekam Medis",'trm_berkas/index'],
            ["List",'#']
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function change_last_visit()
    {
        $last_visit=YMDFormat($this->input->post('last_visit'));
        $q="SELECT DATE_FORMAT(DATE_ADD('$last_visit', INTERVAL 5 YEAR),'%d-%m-%Y') as est_in,DATE_FORMAT(DATE_ADD('$last_visit', INTERVAL 7 YEAR),'%d-%m-%Y') as est_retensi ";
        $arr=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($arr));
    }

    public function change_est_in()
    {
        $est_in=YMDFormat($this->input->post('est_in'));
        $q="SELECT DATE_FORMAT(DATE_ADD('$est_in', INTERVAL 7 YEAR),'%d-%m-%Y') as est_retensi ";
        $arr=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($arr));
    }

    public function kodetifikasi($idpasien, $readonly='')
    {
        $data=array();
        $data=$this->model->get_data_pribadi($idpasien);
        $data['idpasien'] = $idpasien;
        $data['list_dokter'] =$this->model->list_dokter2();
        $data['error'] = '';
        $data['content'] = 'Trm_berkas/kodetifikasi';
        $data['breadcrum'] = [
            ["RSKB Halmahera",'#'],
            ["Berkas Rekam Medis",'trm_berkas/index'],
            ["List",'#']
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function save_status_berkas()
    {
        $id     = $this->input->post('idpasien');
        $data =array(
            'st_aktif'=>$this->input->post('st_aktif'),
            'lokasi_berkas'=>$this->input->post('lokasi_berkas'),
            'last_visit'=>YMDFormat($this->input->post('last_visit')),
            'est_in'=>YMDFormat($this->input->post('est_in')),
            'est_retensi'=>YMDFormat($this->input->post('est_retensi')),
        );
        $this->db->where('id', $id);
        $result=$this->db->update('mfpasien', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function aktifkan()
    {
        $id     = $this->input->post('id');
        $data =array(
            'st_aktif'=>'1',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('mfpasien', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function nonaktif()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_aktif'=>'4',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('mfpasien', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function status_berkas($id)
    {
        if ($id=='1') {
            return '<span class="label label-success">ACTIVE</span>';
        } elseif ($id=='2') {
            return '<span class="label label-default">IN ACTIVE</span>';
        } elseif ($id=='3') {
            return '<span class="label label-warning">RETENSI</span>';
        } elseif ($id=='4') {
            return '<span class="label label-danger">NOT ACTIVE</span>';
        }
    }

    public function tujuan($id)
    {
        if ($id=='1') {
            return '<span class="label label-success">POLIKLINIK</span>';
        } elseif ($id=='2') {
            return '<span class="label label-danger">IGD</span>';
        } elseif ($id=='3') {
            return '<span class="label label-primary">RAWAT INAP</span>';
        } elseif ($id=='4') {
            return '<span class="label label-default">ODS</span>';
        } elseif ($id=='5') {
            return '<span class="label label-warning">PINJAM</span>';
        }
    }

    public function tujuan_nama($id)
    {
        if ($id=='1') {
            return 'POLIKLINIK';
        } elseif ($id=='2') {
            return 'IGD';
        } elseif ($id=='3') {
            return 'RAWAT INAP';
        } elseif ($id=='4') {
            return 'ODS';
        } elseif ($id=='5') {
            return 'PINJAM';
        }
    }

    public function get_header_kodetifikasi()
    {
        $id = $this->input->post('idberkas');
        $idpasien = $this->input->post('idpasien');
        $row = $this->model->get_header_kodetifikasi($id);
        $arr = array(
            'tanggal_trx'=>HumanDateLong($row['tanggal_trx']),
            'idpoliklinik'=>$row['idpoliklinik'],
            'namapoliklinik'=>$row['namapoliklinik'],
            'tujuan'=>$this->tujuan_nama($row['tujuan']),
            'tujuan_id'=>$row['tujuan'],
            'namadokter'=>$row['namadokter'],
            'user_daftar'=>$row['user_daftar'],
            'id_trx'=>$row['id_trx'],
            'statuspasienbaru'=>$row['statuspasienbaru'],
            'status_pasien'=>$row['status_pasien'],
            'statuskasus'=>$row['statuskasus'],
            'statuskasus_nama'=>$row['statuskasus_nama'],
            'tanggal_kirim'=>HumanDateLong($row['tanggal_kirim']),
            'user_kirim_nama'=>$row['user_kirim_nama'],
            'tanggal_kembali'=>HumanDateLong($row['tanggal_kembali']),
            'durasi_kirim'=>getDurasi($row['tanggal_trx'], $row['tanggal_kembali']),
            'durasi_kembali'=>getDurasi($row['tanggal_kirim'], $row['tanggal_kembali']),
        );
        if ($row['tujuan']>2) {
            //INSERT DIAGNOSA RANAP & ODS
            $this->model->auto_insert_diagnosa_ko($id, $row['id_trx'], $row['tujuan']);//KAMAR OPERASI
            $this->model->auto_insert_diagnosa_utama_ranap($id, $row['id_trx']);//UTAMA
            $this->model->auto_insert_diagnosa_sebab_luar_ranap($id, $row['id_trx']);//SEBAB LUAR

            if ($row['iddokter_perujuk']=='') {
                $this->model->auto_insert_perujuk($id, $row['id_trx']);//DOKTER PERUJUK
            }
            $this->model->auto_insert_dokter($id, $row['id_trx']);//LIST DOKTER PERUJUK

            //TINDAKAN
            $this->model->auto_insert_tindakan_KO($id, $row['id_trx'], $row['tujuan']);//KAMAR OPERASI
            $this->model->auto_insert_tindakan_Ranap($id, $row['id_trx']);//KAMAR OPERASI

            $this->model->auto_insert_layanan_KO($id, $row['id_trx'], $row['tujuan']);//KAMAR OPERASI Tarif to icd Tindakan DO
            $this->model->auto_insert_layanan_sewa_alat_KO($id, $row['id_trx'], $row['tujuan']);//KAMAR OPERASI Tarif to icd Tindakan SEWA ALAT
        }
        if ($row['tujuan']<=2) {
            //INSERT DIAGNOSA UTAMA  POLI & IGD
            $this->model->auto_insert_diagnosa_utama_poli($id, $row['id_trx']);//UTAMA
            if ($row['statuspasienbaru']=='0' && $row['statuskasus']=='2' && $row['tujuan']<=2) {//INSERT KONTROL
                $this->model->auto_insert_diagnosa_kontrol_poli($id, $row['id_trx'], $row['idpoliklinik']);
            }
            $this->model->auto_insert_diagnosa_sebab_luar_poli($id, $row['id_trx']);//SEBAB LUAR

            //TINDAKAN
            $this->model->auto_insert_tindakan_poli($id, $row['id_trx']);
        }
        $this->model->auto_insert_tindakan_lab($id);
        $this->model->auto_insert_tindakan_rad($id);
        $this->model->auto_insert_tindakan_fisio($id);

        $this->output->set_output(json_encode($arr));
    }

    public function get_header_verifikasi()
    {
        $id = $this->input->post('idberkas');
        $idpasien = $this->input->post('idpasien');
        $row=$this->model->get_header_kodetifikasi($id);
        $arr=array(
            'tanggal_trx'=>HumanDateLong($row['tanggal_trx']),
            'idpoliklinik'=>$row['idpoliklinik'],
            'namapoliklinik'=>$row['namapoliklinik'],
            'tujuan'=>$this->tujuan_nama($row['tujuan']),
            'tujuan_id'=>$row['tujuan'],
            'namadokter'=>$row['namadokter'],
            'user_daftar'=>$row['user_daftar'],
            'id_trx'=>$row['id_trx'],
            'statuspasienbaru'=>$row['statuspasienbaru'],
            'status_pasien'=>$row['status_pasien'],
            'statuskasus'=>$row['statuskasus'],
            'statuskasus_nama'=>$row['statuskasus_nama'],
            'tanggal_kirim'=>HumanDateLong($row['tanggal_kirim']),
            'user_kirim_nama'=>$row['user_kirim_nama'],
            'tanggal_kembali'=>HumanDateLong($row['tanggal_kembali']),
            'durasi_kirim'=>getDurasi($row['tanggal_trx'], $row['tanggal_kembali']),
            'durasi_kembali'=>getDurasi($row['tanggal_kirim'], $row['tanggal_kembali']),
        );

        $this->output->set_output(json_encode($arr));
    }

    public function get_dokter()
    {
        $id     = $this->input->post('idberkas');
        $row=$this->model->get_dokter($id);
        $this->output->set_output(json_encode($row));
    }

    public function js_kelompok_diagnosa()
    {
        $arr = $this->model->js_kelompok_diagnosa();
        echo json_encode($arr);
    }

    public function js_kelompok_tindakan()
    {
        $arr = $this->model->js_kelompok_tindakan();
        echo json_encode($arr);
    }

    public function js_ket_kontrol()
    {
        $arr = $this->model->js_ket_kontrol();
        echo json_encode($arr);
    }

    public function js_sebab_luar()
    {
        $arr = $this->model->js_sebab_luar();
        echo json_encode($arr);
    }

    public function get_diagnosa($id)
    {
        $arr = $this->model->get_diagnosa($id);
        echo json_encode($arr);
    }

    public function get_kt($id, $tipe)
    {
        $arr = $this->model->get_kt($id, $tipe);
        echo json_encode($arr);
    }

    public function get_kontrol($id)
    {
        $arr = $this->model->get_kontrol($id);
        echo json_encode($arr);
    }

    public function get_sebab($id)
    {
        $arr = $this->model->get_sebab($id);
        echo json_encode($arr);
    }

    public function simpan_add($tipe)
    {
        $data=array(
            'tipe_tabel'=>$this->input->post('tipe_tabel'),
            'idberkas'=>$this->input->post('idberkas'),
            'icd_id'=>$this->input->post('icd_id'),
            'jenis_id'=>$this->input->post('jenis_id'),
            'cara_input'=>$this->input->post('cara_input'),
            'user_created'=>$this->session->userdata('user_id'),
            'user_name_created'=>$this->session->userdata('user_name'),
            'tanggal_created'=>date('Y-m-d H:i:s'),
        );

        if ($tipe=='1') {
            $data['kelompok_diagnosa_id']=$this->input->post('kelompok_diagnosa_id');
        } elseif ($tipe=='2') {
            $data['kontrol_id']=$this->input->post('kelompok_diagnosa_id');
        } else {
            $data['sebab_luar_id']=$this->input->post('kelompok_diagnosa_id');
        }

        $result = $this->db->insert('trm_berkas_icd10', $data);
        echo json_encode($result);
    }

    public function simpan_icd9_add()
    {
        $data=array(
            'tipe_tabel'=>$this->input->post('tipe_tabel'),
            'idberkas'=>$this->input->post('idberkas'),
            'icd_id'=>$this->input->post('icd_id'),
            'jenis_id'=>$this->input->post('jenis_id'),
            'cara_input'=>$this->input->post('cara_input'),
            'user_created'=>$this->session->userdata('user_id'),
            'user_name_created'=>$this->session->userdata('user_name'),
            'tanggal_created'=>date('Y-m-d H:i:s'),
        );

        if ($this->input->post('tipe_tabel')=='1') {
            $data['kelompok_tindakan_id']=$this->input->post('kelompok_tindakan_id');
        } elseif ($this->input->post('tipe_tabel')=='2') {
            $data['tindakan_id']=$this->input->post('kelompok_tindakan_id');
        }

        $result = $this->db->insert('trm_berkas_icd9', $data);
        echo json_encode($result);
    }
    public function simpan_add_dokter()
    {
        $data=array(
            'iddokter'=>$this->input->post('iddokter'),
            'idberkas'=>$this->input->post('idberkas'),
            'detail'=>'Visite',
            'tabel_asal'=>'Manual',
            'user_created'=>$this->session->userdata('user_id'),
            'user_name_created'=>$this->session->userdata('user_name'),
            'tanggal_created'=>date('Y-m-d H:i:s'),
        );

        $result = $this->db->insert('trm_berkas_dokter', $data);
        echo json_encode($result);
    }

    public function simpan_edit($tipe)
    {
        $id=$this->input->post('id');
        $data=array(
            'icd_id'=>$this->input->post('icd_id'),
            'jenis_id'=>$this->input->post('jenis_id'),
            'cara_input'=>$this->input->post('cara_input'),
            'user_edited'=>$this->session->userdata('user_id'),
            'user_nama_edited'=>$this->session->userdata('user_name'),
            'tanggal_edited'=>date('Y-m-d H:i:s'),
        );

        if ($tipe=='1') {
            $data['kelompok_diagnosa_id']=$this->input->post('kelompok_diagnosa_id');
        } elseif ($tipe=='2') {
            $data['kontrol_id']=$this->input->post('kelompok_diagnosa_id');
        } else {
            $data['sebab_luar_id']=$this->input->post('kelompok_diagnosa_id');
        }

        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_icd10', $data);
        echo json_encode($result);
    }

    public function simpan_icd9_edit()
    {
        $id=$this->input->post('id');
        $data=array(
            'tipe_tabel'=>$this->input->post('tipe_tabel'),
            'icd_id'=>$this->input->post('icd_id'),
            'jenis_id'=>$this->input->post('jenis_id'),
            'cara_input'=>$this->input->post('cara_input'),
            'user_edited'=>$this->session->userdata('user_id'),
            'user_nama_edited'=>$this->session->userdata('user_name'),
            'tanggal_edited'=>date('Y-m-d H:i:s'),
        );

        if ($this->input->post('tipe_tabel')=='1') {
            $data['kelompok_tindakan_id']=$this->input->post('kelompok_tindakan_id');
            $data['tindakan_id']='';
        } elseif ($this->input->post('tipe_tabel')=='2') {
            $data['kelompok_tindakan_id']='';
            $data['tindakan_id']=$this->input->post('kelompok_tindakan_id');
        }

        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_icd9', $data);
        echo json_encode($result);
    }

    public function save_kodefikasi()
    {
        $id=$this->input->post('id');
        $data=array(
            'st_kodefikasi'=>'1',
            'tgl_kodefikasi'=>date('Y-m-d H:i:s'),
            'user_nama_kodefikasi'=>$this->session->userdata('user_name'),
            'user_id_kodefikasi'=>$this->session->userdata('user_id'),

        );

        $this->db->where('id', $id);
        $result = $this->db->update('trm_layanan_berkas', $data);
        echo json_encode($result);
    }

    public function save_verifikasi()
    {
        $id=$this->input->post('id');
        $data=array(
            'st_verifikasi'=>'1',
            'tgl_verifikasi'=>date('Y-m-d H:i:s'),
            'user_nama_verifikasi'=>$this->session->userdata('user_name'),
            'user_id_verifikasi'=>$this->session->userdata('user_id'),

        );

        $this->db->where('id', $id);
        $result = $this->db->update('trm_layanan_berkas', $data);
        echo json_encode($result);
    }

    public function simpan_edit_dokter()
    {
        $id=$this->input->post('id_edit_dokter');
        $data=array(
            'iddokter'=>$this->input->post('iddokter'),
        );

        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_dokter', $data);
        echo json_encode($result);
    }

    public function hapus_icd()
    {
        $id=$this->input->post('id');
        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_icd10', array('status'=>'0'));
        echo json_encode($result);
    }

    public function ganti_sugges_icd10()
    {
        $id=$this->input->post('id');
        $icd_id=$this->input->post('icd_id');
        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_icd10', array('icd_id'=>$icd_id));
        echo json_encode($result);
    }

    public function ganti_sugges_icd9()
    {
        $id=$this->input->post('id');
        $icd_id=$this->input->post('icd_id');
        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_icd9', array('icd_id'=>$icd_id));
        echo json_encode($result);
    }

    public function hapus_icd9()
    {
        $id=$this->input->post('id');
        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_icd9', array('status'=>'0'));
        echo json_encode($result);
    }

    public function hapus_dokter()
    {
        $id=$this->input->post('id');
        $this->db->where('id', $id);
        $result = $this->db->update('trm_berkas_dokter', array('status'=>'0'));
        echo json_encode($result);
    }

    public function load_icd($tabel)
    {
        $idberkas     = $this->input->post('idberkas');

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $from="(SELECT
				H.id,H.idberkas,H.icd_id,CONCAT(ICD.kode,' - ',ICD.deskripsi) as icd_nama,
				H.kelompok_diagnosa_id,
				CASE WHEN H.tipe_tabel='1' THEN KD.nama
				WHEN H.tipe_tabel='2' THEN KD2.nama
			  WHEN H.tipe_tabel='3' THEN KD3.nama	END
				as kd_nama,
				CASE WHEN  H.jenis_id='1' THEN 'Utama'
				WHEN  H.jenis_id='2' THEN 'Tambahan'
				WHEN  H.jenis_id='3' THEN 'Kontrol'
				WHEN  H.jenis_id='4' THEN 'External Causes' END as jenis_nama,H.jenis_id,H.tipe_tabel,
				H.cara_input,CASE WHEN  H.cara_input='1' THEN 'Manual'
				WHEN  H.cara_input='2' AND H.tipe_tabel='1' THEN 'Input Diagnosa'
				WHEN  H.cara_input='2' AND H.tipe_tabel='2' THEN 'Input Kontrol'
				WHEN  H.cara_input='2' AND H.tipe_tabel='3' THEN 'Input Sebab Luar'
				WHEN  H.cara_input='3' THEN 'Auto Input'
				WHEN  H.cara_input='4' THEN 'Input EC' END as cara_input_nama,CONCAT('<small>',H.user_name_created,'<br>',DATE_FORMAT(H.tanggal_created,'%d-%m-%Y'),'</small>') as user_nama
				,H.kontrol_id,H.sebab_luar_id,H.auto_asal
				from trm_berkas_icd10 H
				LEFT JOIN mkelompok_diagnosa KD ON KD.id=H.kelompok_diagnosa_id
				LEFT JOIN mkontrol KD2 ON KD2.id=H.kontrol_id
				LEFT JOIN msebab_luar KD3 ON KD3.id=H.sebab_luar_id


				LEFT JOIN icd_10 ICD ON ICD.id=H.icd_id
				WHERE H.idberkas='$idberkas' AND H.status='1' ORDER BY jenis_id ASC
				) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('jenis','deskripsi','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->icd_nama;
            $row[] = $r->jenis_nama;
            $row[] = $r->kd_nama;
            if ($r->cara_input=='1') {//MANUAL
                $row[] =text_success($r->cara_input_nama);
            } elseif ($r->cara_input=='2') {//BY FILTER
                $row[] =text_danger($r->cara_input_nama);
            } elseif ($r->cara_input=='3') {//AUTO INPUT
                $row[] =text_primary($r->cara_input_nama.'-'.$r->auto_asal);
            } else {
                $row[] =text_default($r->cara_input_nama);
            }

            $row[] = $r->user_nama;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<button class="btn btn-xs btn-primary edit_1 disable" title="Edit"><i class="fa fa-pencil"></i></button>';
            $aksi 		.= '<button class="btn btn-xs btn-danger hapus_1 disable" title="Hapus"><i class="fa fa-close"></i></button>';
            if ($r->cara_input !='1') {
                $aksi 		.= '<button class="btn btn-xs btn-success sugges disable" title="Hapus"><i class="fa fa-keyboard-o"></i></button>';
            }
            $aksi.='</div>';

            $row[] = $aksi;
            $row[] = $r->id;//6
            $row[] = $r->icd_id;//7
            $row[] = $r->kelompok_diagnosa_id;//8
            $row[] = $r->jenis_id;//9
            $row[] = $r->cara_input;//10
            $row[] = $r->tipe_tabel;//11
            $row[] = $r->kontrol_id;//12
            $row[] = $r->sebab_luar_id;//13
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_suges10()
    {
        $tipe     = $this->input->post('tipe');
        $kel_id     = $this->input->post('kel_id');
        $icd_id    = $this->input->post('icd_id');


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();


        $from="(SELECT *FROM (
				SELECT T.*, CONCAT(icd.kode,' - ',icd.deskripsi) as icd_nama,
				CASE WHEN T.icd_id='$icd_id' THEN 'Terpilih' ELSE '' END as terpilih,
				CASE WHEN T.icd_id='$icd_id' THEN '1' ELSE '2' END as terpilih_id
				FROM (
				SELECT '1' as tipe,H.id,H.nama,D.icd10_id as icd_id,D.jenis_id from mkelompok_diagnosa H
				INNER JOIN mkelompok_diagnosa_icd D ON D.kelompok_diagnosa_id=H.id
				UNION
				SELECT '3' as tipe,H.id,H.nama,D.icd_id as icd_id,D.jenis_id from mkontrol H
				INNER JOIN mkontrol_icd D ON D.mkontrol_id=H.id
				UNION
				SELECT '4' as tipe,H.id,H.nama,D.icd_id as icd_id,D.jenis_id from msebab_luar H
				LEFT JOIN msebab_luar_icd D ON D.msebab_luar_id=H.id
				) T INNER JOIN icd_10 icd ON icd.id=T.icd_id
				) TBL
				WHERE TBL.tipe='$tipe' AND TBL.id='$kel_id'
				ORDER BY TBL.terpilih_id,TBL.tipe,TBL.id

				) as tabel";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('jenis','deskripsi','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->icd_nama;
            if ($r->jenis_id=='1') {//MANUAL
                $row[] =text_success('Primary');
            } else {
                $row[] =text_default('Secondary');
            }
            $row[] = text_danger($r->terpilih);

            $aksi       = '<div class="btn-group">';
            if ($r->terpilih_id	=='2') {
                $aksi 		.= '<button class="btn btn-xs btn-primary pilih_icd" title="Pilih ICD">Pilih ICD</button>';
            }

            $aksi.='</div>';

            $row[] = $aksi;
            $row[] = $r->icd_id;//7
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_suges9()
    {
        $tipe     = $this->input->post('tipe');
        $kel_id     = $this->input->post('kel_id');
        $icd_id    = $this->input->post('icd_id');


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();


        $from="(SELECT *FROM (
				SELECT T.*,CONCAT(icd.kode,' - ',icd.deskripsi) as icd_nama,
				CASE WHEN T.icd_id='$icd_id' THEN 'Terpilih' ELSE '' END as terpilih,
				CASE WHEN T.icd_id='$icd_id' THEN '1' ELSE '2' END as terpilih_id FROM(
				SELECT '1' as tipe,H.id,H.nama,D.icd9_id as icd_id,D.jenis_id from mkelompok_tindakan H
				INNER JOIN mkelompok_tindakan_icd D ON D.kelompok_tindakan_id=H.id
				UNION
				SELECT '2' as tipe,H.id,H.nama,D.icd9_id as icd_id,D.jenis_id from mtindakan2icd H
				INNER JOIN mtindakan2icd_icd D ON D.tindakan_id=H.id
				) T INNER JOIN icd_9 icd ON icd.id=T.icd_id
				) TBL WHERE TBL.tipe='$tipe' AND  TBL.id='$kel_id'
				ORDER BY TBL.terpilih_id,TBL.tipe,TBL.id
				) as tabel";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('jenis','deskripsi','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->icd_nama;
            if ($r->jenis_id=='1') {//MANUAL
                $row[] =text_success('Primary');
            } else {
                $row[] =text_default('Secondary');
            }
            $row[] = text_danger($r->terpilih);

            $aksi       = '<div class="btn-group">';
            if ($r->terpilih_id	=='2') {
                $aksi 		.= '<button class="btn btn-xs btn-primary pilih_icd_9" title="Pilih ICD">Pilih ICD</button>';
            }

            $aksi.='</div>';

            $row[] = $aksi;
            $row[] = $r->icd_id;//7
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_icd9()
    {
        $idberkas     = $this->input->post('idberkas');


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();


        $from="(SELECT H.id,H.idberkas,H.icd_id,
				CASE WHEN jenis_id='1' THEN 'Utama' ELSE 'Tambahan' END as jenis_nama,
				CONCAT(icd_9.kode,' - ',icd_9.deskripsi) as icd_nama,H.kelompok_tindakan_id,KT.nama as kt_nama,
				H.tindakan_id,MT.nama as tarif_nama,H.jenis_id,H.tipe_tabel,
						CASE
							 WHEN cara_input='1' THEN 'Manual'
							 WHEN cara_input='2' AND tipe_tabel='1' THEN 'Kelompok Tindakan'
							 WHEN cara_input='2' AND tipe_tabel='2' THEN 'Kelompok Tarif'
						   WHEN cara_input='3' THEN 'Auto Input'
						END as cara_input_nama,H.cara_input,
						CONCAT('<small>',H.user_name_created,'<br>',DATE_FORMAT(H.tanggal_created,'%d-%m-%Y'),'</small>') as user_nama
				,H.auto_asal
				from trm_berkas_icd9 H
				INNER JOIN icd_9 ON icd_9.id=H.icd_id
				LEFT JOIN mkelompok_tindakan KT ON KT.id=H.kelompok_tindakan_id
				LEFT JOIN mtindakan2icd MT ON MT.id=H.tindakan_id
				WHERE H.idberkas='$idberkas' AND H.status='1' ORDER BY H.jenis_id ASC
				) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('jenis','deskripsi','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->icd_nama;
            $row[] = $r->jenis_nama;
            $row[] = $r->kt_nama;
            $row[] = $r->tarif_nama;
            if ($r->cara_input=='1') {//MANUAL
                $row[] =text_success($r->cara_input_nama);
            } elseif ($r->cara_input=='2') {//BY FILTER
                $row[] =text_danger($r->cara_input_nama);
            } elseif ($r->cara_input=='3') {//AUTO INPUT
                $row[] =text_primary($r->cara_input_nama.' - '.$r->auto_asal);
            } else {
                $row[] =text_default($r->cara_input_nama);
            }
            $row[] = $r->user_nama;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<button class="btn btn-xs btn-primary edit_9 disable" title="Edit"><i class="fa fa-pencil"></i></button>';
            $aksi 		.= '<button class="btn btn-xs btn-danger hapus_9 disable" title="Hapus"><i class="fa fa-close"></i></button>';
            if ($r->cara_input !='1') {
                $aksi 		.= '<button class="btn btn-xs btn-success sugges_9 disable" title="Hapus"><i class="fa fa-keyboard-o"></i></button>';
            }
            $aksi.='</div>';

            $row[] = $aksi;
            $row[] = $r->id;//7
            $row[] = $r->icd_id;//8
            $row[] = $r->kelompok_tindakan_id;//9
            $row[] = $r->tindakan_id;//10
            $row[] = $r->jenis_id;//11
            $row[] = $r->cara_input;//12
            $row[] = $r->tipe_tabel;//13
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_operasi()
    {
        $idpasien     = $this->input->post('idpasien');
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();


        $from="(SELECT H.id,H.tanggaloperasi,H.waktumulaioperasi,H.waktuselesaioperasi,H.operasi,H.diagnosa,H.jenis_operasi_id,
			J.nama as jenis_operasi,GROUP_CONCAT(M.nama) as dokter_operator

			from tkamaroperasi_pendaftaran H
			INNER JOIN mjenis_operasi J ON J.id=H.jenis_operasi_id
			LEFT JOIN tkamaroperasi_jasado D ON D.idpendaftaranoperasi=H.id
			LEFT JOIN mdokter M ON M.id=D.iddokteroperator

			WHERE H.idpasien='$idpasien' AND H.`status`='2'
			GROUP BY H.id) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('dokter_operator','diagnosa','operasi','jenis_operasi');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = HumanDateShort($r->tanggaloperasi);
            $row[] = HumanTimeShort($r->waktumulaioperasi).' : '.HumanTimeShort($r->waktuselesaioperasi);
            $row[] = $r->diagnosa;//3
            $row[] = $r->operasi;//3
            $row[] = $r->jenis_operasi;//3
            $row[] = $r->dokter_operator;//3
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_lab()
    {
        $id     = $this->input->post('idberkas');
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();


        $from="(SELECT Tberkas.*,Det.idlaboratorium,Det.namatarif,M.nama,Det.hasil,Det.satuan,Det.keterangan,Det.statusrincianpaket
			,Det.nilainormal FROM (SELECT H.id,H.id_trx,H.tujuan,HP.id as poli_id,HRI.id as ri_id,
			CASE WHEN H.tujuan <= '2' THEN TP.id ELSE H.id_trx  END as idtindakan from trm_layanan_berkas H
			LEFT JOIN tpoliklinik_pendaftaran HP ON HP.id=H.id_trx AND (H.tujuan='1' OR H.tujuan='2')
			LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=HP.id
			LEFT JOIN trawatinap_pendaftaran HRI ON HRI.id=H.id_trx AND (H.tujuan='3' OR H.tujuan='4')
			WHERE H.id='$id') Tberkas
			INNER JOIN trujukan_laboratorium Lab ON Lab.idtindakan=Tberkas.idtindakan AND Lab.asalrujukan=Tberkas.tujuan
			INNER JOIN trujukan_laboratorium_detail Det ON Det.idrujukan=Lab.id
			INNER JOIN mtarif_laboratorium M ON M.id=Det.idlaboratorium

			ORDER BY Det.id) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('namatarif','satuan','keterangan');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            if ($r->statusrincianpaket !='0') {
                $row[] = $r->nama;//3
            } else {
                $row[] = '<b>'.$r->nama.'</b>';//3
            }
            $row[] = $r->hasil;//3
            $row[] = $r->satuan;//3
            $row[] = $r->nilainormal;//3
            $row[] = $r->keterangan;//3
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_rad()
    {
        $id     = $this->input->post('idberkas');
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();


        $from="(SELECT Det.idradiologi,Det.namatarif,Rev.hasilbacafoto,Rev.tindakanyangdilakukan,Det.idtipe FROM (SELECT H.id,H.id_trx,H.tujuan,HP.id as poli_id,HRI.id as ri_id,
				CASE WHEN H.tujuan <= '2' THEN TP.id ELSE H.id_trx  END as idtindakan from trm_layanan_berkas H
				LEFT JOIN tpoliklinik_pendaftaran HP ON HP.id=H.id_trx AND (H.tujuan='1' OR H.tujuan='2')
				LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=HP.id
				LEFT JOIN trawatinap_pendaftaran HRI ON HRI.id=H.id_trx AND (H.tujuan='3' OR H.tujuan='4')
				WHERE H.id='$id') Tberkas
				INNER JOIN trujukan_radiologi Lab ON Lab.idtindakan=Tberkas.idtindakan AND Lab.asalrujukan=Tberkas.tujuan
				INNER JOIN trujukan_radiologi_detail Det ON Det.idrujukan=Lab.id
				INNER JOIN trujukan_radiologi_review Rev ON Rev.idrujukan=Lab.id) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('namatarif','satuan','keterangan');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = GetTipeRadiologi($r->idtipe);//3
            $row[] = $r->namatarif;//3
            $row[] = $r->tindakanyangdilakukan;//3
            $row[] = $r->hasilbacafoto;//3
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_fisio()
    {
        $id     = $this->input->post('idberkas');
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();


        $from="(SELECT  Det.idfisioterapi,M.nama FROM (SELECT H.id,H.id_trx,H.tujuan,HP.id as poli_id,HRI.id as ri_id,
				CASE WHEN H.tujuan <= '2' THEN TP.id ELSE H.id_trx  END as idtindakan from trm_layanan_berkas H
				LEFT JOIN tpoliklinik_pendaftaran HP ON HP.id=H.id_trx AND (H.tujuan='1' OR H.tujuan='2')
				LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=HP.id
				LEFT JOIN trawatinap_pendaftaran HRI ON HRI.id=H.id_trx AND (H.tujuan='3' OR H.tujuan='4')
				WHERE H.id='$id') Tberkas
				INNER JOIN trujukan_fisioterapi Lab ON Lab.idtindakan=Tberkas.idtindakan AND Lab.asalrujukan=Tberkas.tujuan
				INNER JOIN trujukan_fisioterapi_detail Det ON Det.idrujukan=Lab.id
				INNER JOIN mtarif_fisioterapi M ON M.id=Det.idfisioterapi) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('namatarif','satuan','keterangan');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $r->nama;//3
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function load_konsulen()
    {
        $idberkas     = $this->input->post('idberkas');
        $iddokter_perujuk     = $this->input->post('iddokter_perujuk');
        $iddokter     = $this->input->post('iddokter');
        $q="SELECT B.iddokter,D.nama,B.detail,B.id,B.tabel_asal FROM trm_berkas_dokter B
				INNER JOIN mdokter D ON D.id=B.iddokter
				WHERE B.idberkas='$idberkas' AND B.status='1' AND (B.iddokter !='$iddokter' AND B.iddokter !='$iddokter_perujuk')
				ORDER BY B.id ASC";
        $query=$this->db->query($q);
        $result=$query->result();
        echo json_encode($result);
    }

    public function load_anasthesi()
    {
        $id     = $this->input->post('id_trx');

        $q="SELECT H.id,D.iddokteranastesi,M.nama from tkamaroperasi_pendaftaran H
			INNER JOIN tkamaroperasi_jasada D ON D.idpendaftaranoperasi=H.id
			INNER JOIN mdokter M ON M.id=D.iddokteranastesi
			WHERE H.idpendaftaran='$id'";
        $query=$this->db->query($q);
        $result=$query->result();
        echo json_encode($result);
    }

    public function getDataFarmasi($id)
    {
      $query = $this->db->query("SELECT
      	'nonracikan' AS tipe,
      	tpasien_penjualan.tanggal,
      	tpasien_penjualan.nopenjualan,
      	(CASE
      		WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN 'Alat Kesehatan'
      		WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN 'Obat'
      	END) AS kategori,
      	(CASE
      		WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN mdata_alkes.nama
      		WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN mdata_obat.nama
      	END) AS namabarang,
      	tpasien_penjualan_nonracikan.harga,
      	tpasien_penjualan_nonracikan.kuantitas,
      	tpasien_penjualan_nonracikan.totalharga
      FROM
      	tpasien_penjualan
      JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
      LEFT JOIN mdata_alkes ON mdata_alkes.id = tpasien_penjualan_nonracikan.idbarang AND tpasien_penjualan_nonracikan.idtipe = 1
      LEFT JOIN mdata_obat ON mdata_obat.id = tpasien_penjualan_nonracikan.idbarang AND tpasien_penjualan_nonracikan.idtipe = 3
      WHERE tpasien_penjualan.id = $id

      UNION ALL

      SELECT
      	'racikan' AS tipe,
      	tpasien_penjualan.tanggal,
      	tpasien_penjualan.nopenjualan,
      	(CASE
      		WHEN tpasien_penjualan_racikan_obat.idtipe = 1 THEN 'Alat Kesehatan'
      		WHEN tpasien_penjualan_racikan_obat.idtipe = 3 THEN 'Obat'
      	END) AS kategori,
      	(CASE
      		WHEN tpasien_penjualan_racikan_obat.idtipe = 1 THEN mdata_alkes.nama
      		WHEN tpasien_penjualan_racikan_obat.idtipe = 3 THEN mdata_obat.nama
      	END) AS namabarang,
      	tpasien_penjualan_racikan_obat.harga,
      	tpasien_penjualan_racikan_obat.kuantitas,
      	tpasien_penjualan_racikan_obat.totalharga
      FROM
      	tpasien_penjualan
      JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
      JOIN tpasien_penjualan_racikan_obat ON tpasien_penjualan_racikan_obat.idracikan = tpasien_penjualan_racikan.id
      LEFT JOIN mdata_alkes ON mdata_alkes.id = tpasien_penjualan_racikan_obat.idbarang AND tpasien_penjualan_racikan_obat.idtipe = 1
      LEFT JOIN mdata_obat ON mdata_obat.id = tpasien_penjualan_racikan_obat.idbarang AND tpasien_penjualan_racikan_obat.idtipe = 3
      WHERE tpasien_penjualan.id = $id");
      $result = $query->result();
      echo json_encode($result);
    }
	public function getDataICD9($id)
    {
      $query = $this->db->query("SELECT H.id,H.jenis_pelayanan, H.iddokter,dok.nama as nama_dokter,H.idpasien,H.tanggal_trx,D.kelompok_tindakan_id,D.tindakan_id,D.icd_id,D.jenis_id,icd.kode,icd.deskripsi,T.nama as tindakan_layanan,K.nama as kel_tindakan,CASE WHEN D.jenis_id='1' THEN 'UTAMA' ELSE 'TAMBAHAN' END as jenis_nama,musers.`name` as user_trx,H.user_nama_verifikasi
						FROM trm_berkas_icd9 D
						INNER JOIN trm_layanan_berkas H ON H.id=D.idberkas
						LEFT JOIN mdokter dok ON dok.id=H.iddokter
						LEFT JOIN icd_9 icd ON icd.id=D.icd_id
						LEFT JOIN mtindakan2icd T ON T.id=D.tindakan_id
						LEFT JOIN mkelompok_tindakan K ON K.id=D.kelompok_tindakan_id
						LEFT JOIN musers ON musers.id=H.user_trx
						WHERE H.id='$id' AND D.`status`='1' AND D.jenis_id='2'
						ORDER BY H.tanggal_trx DESC");
      $result = $query->result();
      echo json_encode($result);
    }

    public function getDataRadiologi($id) {
      $result = $this->Trujukan_radiologi_model->getListDetailGroup($id);
      echo json_encode($result);
    }

    public function getDataLaboratorium($id) {
      $result = array(
        'laboratorium' => $this->Trujukan_laboratorium_model->getListReview($id),
        'catatan' => $this->Trujukan_laboratorium_model->getSpecified($id)->catatan
      );

      echo json_encode($result);
    }

    public function getDataTindakan($reference, $id) {
      if ($reference == 'rawatjalan') {
        $result = $this->Tpoliklinik_tindakan_model->getSpecifiedTindakan($id);
      }

      echo json_encode($result);
    }

    public function getInfoSebabLuar($id) {
      if ($id != 0) {
        $result = get_by_field('id', $id, 'msebab_luar')->nama;
      } else {
        $result = '';
      }

      echo $result;
    }
}
