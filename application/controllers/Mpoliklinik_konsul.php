<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpoliklinik_konsul extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpoliklinik_konsul_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1792'))){
			
			$data['tab'] 			= $tab;
			$data['idtipe'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Poliklinik Konsul';
			$data['content'] 		= 'Mpoliklinik_konsul/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Poliklinik Konsul",'mpoliklinik_konsul')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_poli(){
		$idpoliklinik=$this->input->post('idpoli');
		$data=array(
			'idpoliklinik'=>$this->input->post('idpoli'),
		);
		$data['created_by']=$this->session->userdata('user_id');
		$data['created_date']=date('Y-m-d H:i:s');
		$hasil=$this->db->insert('mpoliklinik_konsul',$data);

		json_encode($hasil);
	}
	
	
	function load_poli()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,H.idpoliklinik,M.nama,M.idtipe,GROUP_CONCAT(MD.nama) as nama_dokter 
						FROM mpoliklinik_konsul H
						INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik
						LEFT JOIN mpoliklinik_konsul_dokter D ON D.idpoliklinik=H.idpoliklinik
						LEFT JOIN mdokter MD ON MD.id=D.iddokter
						GROUP BY H.idpoliklinik
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('idpoliklinik','nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $nama_dokter='';
		  if ($r->nama_dokter){
			  $nama_dokter='<br>( '.$r->nama_dokter.' )';
		  }else{
			  $nama_dokter='<br>'.text_danger('Belum ada dokter');
		  }
          $result[] = $no;
			 $aksi='';
          $result[] = GetTipePasienPiutang($r->idtipe);
          $result[] = $r->nama.$nama_dokter;
          $aksi = '<div class="btn-group">';
		   if (UserAccesForm($user_acces_form,array('1795'))){
		  $aksi .= '<button class="btn btn-xs btn-primary" type="button" onclick="load_dokter(\''.$r->id.'\',\''.$r->nama.'\')"  title="Setting Dokter"><i class="si si-settings"></i> Dokter</button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1794'))){
		  $aksi .= '<button onclick="hapus_poli('.$r->idpoliklinik.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_poli(){
	  $id=$this->input->post('id');
	 
		$this->db->where('idpoliklinik',$id);
		$hasil=$this->db->delete('mpoliklinik_konsul');
	  
	  json_encode($hasil);
	  
  }


  function find_poli($idtipe){
	  $q="SELECT H.id,H.nama FROM mpoliklinik H
			WHERE H.`status`='1' AND H.idtipe='$idtipe' AND H.id NOT IN (SELECT idpoliklinik FROM mpoliklinik_konsul)";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function load_dokter()
	{
			$id=$this->input->post('id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.iddokter IS NOT NULL THEN '1' ELSE '0' END as pilih 
						FROM mpoliklinik_dokter MS 
						INNER JOIN mdokter M ON MS.iddokter=M.id
						LEFT JOIN mpoliklinik_konsul_dokter H ON H.iddokter=M.id AND H.idpoliklinik=MS.idpoliklinik
						WHERE M.`status`='1' AND MS.idpoliklinik='$id' AND MS.`status`='1'
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function check_save_dokter(){
		$pilih=$this->input->post('pilih');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idpoliklinik',$idpoli);
			$this->db->where('iddokter',$iddokter);
			$hasil=$this->db->delete('mpoliklinik_konsul_dokter');
		}else{
			$this->idpoliklinik=$idpoli;
			$this->iddokter=$iddokter;
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('mpoliklinik_konsul_dokter',$this);
		}
		
		  
		  json_encode($hasil);
	}
}
