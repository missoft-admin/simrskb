<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_kegiatan_setting extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_kegiatan_setting_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_pengajuan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Reminder RKA';
		$data['content'] 		= 'Mrka_kegiatan_setting/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("List",'mrka_kegiatan_setting')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $user_id=$this->session->userdata('user_id');
	  
	  $idrka=$this->input->post('idrka');
	  $idprespektif=$this->input->post('idprespektif');
	  $idprogram=$this->input->post('idprogram');
	  $where='';
	  if ($idrka !='#'){
		  $where .=" AND K.idrka >='$idrka'";
	  }
	  
	  if ($idprespektif !='#'){
		  $where .=" AND K.idprespektif='$idprespektif'";
	  }
	  if ($idprogram !='#'){
		  $where .=" AND K.idprogram='$idprogram'";
	  }
		
	  $from="(	  
		SELECT M.periode,M.nama as rka_nama,K.id as idrka_kegiatan, K.idrka,K.idkegiatan, K.idprogram,K.idprespektif,K.idunit
		,MK.nama as kegiatan,prog.nama as program,MP.nama as prespektif
		,KN.bulan,KN.nominal,KN.st_setting
		 from mrka_kegiatan K
		 INNER JOIN mrka_kegiatan_nominal KN ON KN.idrka_kegiatan=K.id AND KN.nominal > 0
		LEFT JOIN mrka M ON M.id=K.idrka
		LEFT JOIN mkegiatan_rka MK ON MK.id=K.idkegiatan
		LEFT JOIN mprogram_rka prog ON prog.id=K.idprogram
		LEFT JOIN mprespektif_rka MP ON MP.id=K.idprespektif

		WHERE M.`status`='3' ".$where."
		
		
				) as tbl";
	//AND K.idunit IN (SELECT idunit from munitpelayanan_user_setting US WHERE US.userid='$user_id')
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('kegiatan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$row[] = $r->idrka_kegiatan;
			$row[] = $r->bulan;
			$row[] = $r->idprespektif;
			$row[] = $r->idprogram;
			$row[] = $r->idkegiatan;
			$row[] = $no;
			
			$row[] = $r->rka_nama;
			$row[] = $r->kegiatan;
			$row[] = $r->program;
			$row[] = $r->prespektif;
			$row[] = MONTHFormat($r->bulan).' '.$r->periode;
			$row[] = status_setting($r->st_setting);
			// $row[] = ($r->st_setting);
			$row[] = number_format($r->nominal,0);
			  
			$aksi = '<div class="btn-group">';
			if ($r->st_setting=='1' || $r->st_setting=='0'){
				if (UserAccesForm($user_acces_form,array('1392'))){

				$aksi .= '<button data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm setting"><i class="si si-settings"></i></button>';			
				}
				
			}
			if (UserAccesForm($user_acces_form,array('1393'))){
			$aksi .= '<a href="'.site_url().'mrka_kegiatan/edit_add_kegiatan/'.$r->idrka_kegiatan.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>';			
			}
			if (UserAccesForm($user_acces_form,array('1394'))){
			if ($r->st_setting=='2' || $r->st_setting=='0'){
			$aksi .= '<a href="'.site_url().'mrka_kegiatan_setting/create_rka/'.$r->idrka_kegiatan.'/'.$r->bulan.'" data-toggle="tooltip" title="Tambah Pengajuan" class="btn btn-danger btn-sm"><i class="si si-arrow-right"></i></a>';			
			}
			}
			$aksi .= '</div>';

			$row[] = $aksi;
			$data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_rka($id,$bulan){
		$q="SELECT rka.periode,N.*, K.nama as nama_kegiatan, M.* 
			FROM mrka_kegiatan M
			LEFT JOIN mkegiatan_rka K ON K.id=M.idkegiatan
			LEFT JOIN mrka rka ON rka.id=M.idrka
			LEFT JOIN mrka_kegiatan_nominal N ON N.idrka_kegiatan=M.id AND N.bulan='$bulan'
			WHERE M.id='$id'";
		$row=$this->db->query($q)->row_array();	
		$tgl_bayar=$row['periode'].'-'.str_pad($bulan,2,"0",STR_PAD_LEFT).'-01';
		$row['periode']=MONTHFormat($bulan);
		$row['tanggal_bayar']=($row['tanggal_bayar'] ?HumanDateShort($row['tanggal_bayar']):HumanDateShort($tgl_bayar));
		$this->output->set_output(json_encode($row));
	}
	function update_setting(){
		
		$nominal_bayar=RemoveComma($this->input->post('nominal_bayar'));
		$tipe_pembayaran=$this->input->post('tipe_pembayaran');
		$tanggal_bayar=$this->input->post('tanggal_bayar');
		$tgl_pilih=$this->input->post('tgl_pilih');
		$idrka_kegiatan=$this->input->post('idrka_kegiatan');
		$hari_muncul=RemoveComma($this->input->post('hari_muncul'));
		$nominal_muncul=RemoveComma($this->input->post('nominal_muncul'));	
		$idklasifikasi=$this->input->post('idklasifikasi');
		$jenis_muncul=$this->input->post('jenis_muncul');
		$bulan=$this->input->post('bulan');
		$idkegiatan=$this->input->post('idkegiatan');
		$idrka=$this->input->post('idrka');
		$nama_kegiatan=$this->input->post('nama_kegiatan');
			$data_edit=array(
					'hari_muncul' =>$hari_muncul,
					'nominal_muncul' =>$nominal_muncul,
					// 'nominal_bayar' =>$nominal_muncul,
					'jenis_muncul' =>$jenis_muncul,
					'tipe_pembayaran' =>$tipe_pembayaran,
					'st_setting' =>'1',
					'tanggal_bayar' =>YMDFormat($tanggal_bayar),
				);			
			$this->db->delete('mrka_kegiatan_detail',array('idrka_kegiatan'=>$idrka_kegiatan,'bulan'=>$bulan,'jenis_setting'=>'1'));
			if ($tipe_pembayaran=='1'){				
				$data_edit['nominal_bayar']=$nominal_muncul;
				// $this->db->delete('mrka_kegiatan_detail',array('idrka_kegiatan'=>$idrka_kegiatan,'bulan'=>$bulan));
				$data_detail=array(
					'idrka_kegiatan' =>$idrka_kegiatan,
					'idrka' =>$idrka,
					'idkegiatan' =>$idkegiatan,
					'bulan' =>$bulan,
					'tanggal_bayar' =>YMDFormat($tanggal_bayar),
					'jenis_setting' =>'1',
					'nama_barang' =>$nama_kegiatan,
					'merk_barang' =>'-',
					'kuantitas' =>'1',
					'satuan' =>'PAKET',
					'harga_satuan' =>$nominal_muncul,
					'total_harga' =>$nominal_muncul,
					'keterangan' =>$nama_kegiatan.' '.MONTHFormat($bulan),
				);
				$this->db->insert('mrka_kegiatan_detail',$data_detail);
			}else{
				$data_edit['nominal_bayar']=$nominal_bayar;
				if ($tgl_pilih){
					foreach($tgl_pilih as $index => $value){
						$data_detail=array(
						'idrka_kegiatan' =>$idrka_kegiatan,
						'idrka' =>$idrka,
						'idkegiatan' =>$idkegiatan,
						'bulan' =>$bulan,
						'tanggal_bayar' =>YMDFormat($value),
						'jenis_setting' =>'1',
						'nama_barang' =>$nama_kegiatan.' '.MONTHFormat($bulan).' '.HumanDateShort($value),
						'merk_barang' =>'-',
						'kuantitas' =>'1',
						'satuan' =>'PAKET',
						'harga_satuan' =>$nominal_bayar,
						'total_harga' =>$nominal_bayar,
						'keterangan' =>$nama_kegiatan.' '.MONTHFormat($bulan).' '.HumanDateShort($value),
						);
						$this->db->insert('mrka_kegiatan_detail',$data_detail);
					}
				}
			}
			$this->db->update('mrka_kegiatan_nominal',$data_edit,array('idrka_kegiatan'=>$idrka_kegiatan,'bulan'=>$bulan));
		
		$result=$this->db->update('mrka_kegiatan',array('idklasifikasi'=>$idklasifikasi),array('id'=>$idrka_kegiatan));
		$this->output->set_output(json_encode($result));
	}
	function create_rka($id='1',$bulan='',$disabel=''){
		$row=$this->Mrka_kegiatan_setting_model->getSpecified_idrka_kegiatan($id,$bulan);
		$tgl_bayar=$row->periode.'-'.str_pad($bulan,2,"0",STR_PAD_LEFT).'-01';
		// print_r($tgl_bayar);exit();
		$data = array(
			'idrka' 						=> $row->idrka,
			'idrka_kegiatan' 				=> $row->id,
			'idkegiatan' 				=> $row->idkegiatan,
			'idvendor' 					=> '#',
			'cara_pembayaran' 					=> '#',
			'nama' 					=> '',
			'bulan' 					=> $bulan,
			'bulan_nama' 					=> MONTHFormat($bulan),
			'nominal' 					=> $row->nominal,
			'periode' 					=> $row->periode,
			'idunit' 				=> '#',
			'idunit' 				=> $row->idunit,
			'nama_pemohon' 				=> $this->session->userdata('user_name'),
			'tanggal_pengajuan' 	=> date('d-m-Y'),
			'tanggal_dibutuhkan' 	=> date('d-m-Y'),
			'tipe_rka' 				=> '1',
			'idrka_kegiatan' 		=> $id,
			'nama_kegiatan' 		=> $row->nama_kegiatan,
			'grand_total' 		=> '0',
			'total_jenis_barang' 		=> '0',
			'idjenis' 				=> '#',
			
			'tanggal_kontrabon' 		=> '',
			'jenis_cicilan' 		=> '#',
			'cara_pembayaran' 		=> '#',
			'jenis_pembayaran' 		=> '#',
			'norek' 		=> '',
			'bank' 		=> '',
			'keterangan_bank' 		=> '',
			'tgl_bayar' 		=> HumanDateShort($tgl_bayar),
		);


		$data['list_dp_cicilan'] 		= array();
		$data['list_cicilan'] 		= array();
		$data['list_dp_termin'] 		= array();
		$data['list_detail'] 		= array();
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 	= $this->Mrka_pengajuan_model->list_jenis();
		$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
		$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
		$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Detail Kegiatan';
		$data['content'] 		= 'Mrka_kegiatan_setting/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("Tambah",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function save(){
		// print_r($this->input->post());exit();

			// if($this->input->post('id') == '' ) {
				$id=$this->Mrka_kegiatan_setting_model->save();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrka_kegiatan_setting','location');
				}
			// } else {
				// if($this->Mrka_kegiatan_setting_model->save()){
					// $this->session->set_flashdata('confirm',true);
					// $this->session->set_flashdata('message_flash','data telah disimpan.');
					// redirect('mrka_kegiatan_setting','location');
				// }
			// }
	}
	function list_tanggal(){
		$tanggal_bayar=$this->input->post('tanggal_bayar');
		$hari_muncul=$this->input->post('hari_muncul');
		$oldDateUnix = strtotime($tanggal_bayar);
		$tahun=date("Y", $oldDateUnix);
		$bulan=date("m", $oldDateUnix);
		$dari=YMDFormat($tanggal_bayar);
		
		$sampai=$this->lastOfMonth($tahun,$bulan);
		$opsi='<option value="'.$dari.'" selected>'.$tanggal_bayar.'</option>';
		$hitung=1;
		while (strtotime($dari) <= strtotime($sampai)) {
		
			if ($hitung > $hari_muncul){
				$opsi .='<option value="'.$dari.'" selected>'.HumanDateShort($dari).'</option>';
				$hitung=0;
			}
			$hitung = $hitung +1;
			 // $opsi .="$dari <br>";
			 $dari = date ("Y-m-d", strtotime("+1 day", strtotime($dari)));//looping tambah 1 date
		}
		// print_r($opsi);exit();
		// foreach($row as $r){
			// $opsi .='<option value="'. $r->tanggal_next.'">'.$r->tanggal_next.'</option>';
		// }
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function lastOfMonth($year, $month) {
		return (date("Y-m-d", strtotime('-1 second', strtotime('+1 month',strtotime($month . '/01/' . $year. ' 00:00:00')))));
	}
}
