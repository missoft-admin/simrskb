<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_administrasi extends CI_Controller {

	/**
	 * Tarif Administrasi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_administrasi_model');
		// $this->load->helper('path');
  }

	function index($idtipe=1){
		$data = array();
		$data['error'] 			= '';
		$data['idtipe']     = $idtipe;
		$data['title'] 			= 'Tarif Administrasi';
		$data['content'] 		= 'Mtarif_administrasi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Administrasi",'mtarif_administrasi/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter(){
		if($this->input->post('idtipe') != ''){
			$idtipe = $this->input->post('idtipe');
			redirect("mtarif_administrasi/index/$idtipe",'location');
		}else{
			redirect('mtarif_administrasi','location');
		}
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'idtipe' 					=> '0',
			'idjenis' 				=> '1',
			'nama' 						=> '',
			'jasasarana' 			=> '0',
			'jasapelayanan' 	=> '0',
			'bhp' 						=> '0',
			'biayaperawatan' 	=> '0',
			'total' 					=> '0',
			'mintarif' 				=> '0',
			'maxtarif' 				=> '0',
			'persentasetarif' => '0',
			'status' 					=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif Administrasi';
		$data['content'] 		= 'Mtarif_administrasi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Administrasi",'#'),
									    			array("Tambah",'mtarif_administrasi')
													);

		$data['list_parent'] = array();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_administrasi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'idtipe' 					=> $row->idtipe,
					'idjenis' 				=> $row->idjenis,
					'nama' 						=> $row->nama,
					'jasasarana' 			=> $row->jasasarana,
					'jasapelayanan' 	=> $row->jasapelayanan,
					'bhp' 						=> $row->bhp,
					'biayaperawatan' 	=> $row->biayaperawatan,
					'total' 					=> $row->total,
					'mintarif' 				=> $row->mintarif,
					'maxtarif' 				=> $row->maxtarif,
					'persentasetarif' => $row->persentasetarif,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif Administrasi';
				$data['content']	 	= 'Mtarif_administrasi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Administrasi",'#'),
											    			array("Ubah",'mtarif_administrasi')
															);

				$data['list_parent'] = $this->Mtarif_administrasi_model->getAllParent($row->idtipe);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_administrasi', 'location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_administrasi', 'location');
		}
	}

	function delete($id){
		$this->Mtarif_administrasi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_administrasi', 'location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_administrasi_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_administrasi/index/'.$this->input->post('idtipe'), 'location');
				}
			} else {
				if($this->Mtarif_administrasi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_administrasi/index/'.$this->input->post('idtipe'), 'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mtarif_administrasi/manage';

		$data['list_parent'] = $this->Mtarif_administrasi_model->getAllParent($this->input->post('idtipe'));

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mtarif_administrasi')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mtarif_administrasi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex($idtipe)
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'mtarif_administrasi';
			$this->join 	= array();
			$this->where  = array(
				'idtipe' => $idtipe,
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$aksi = '<div class="btn-group">';
					if (UserAccesForm($user_acces_form,array('136'))){
              $aksi .= '<a href="'.site_url().'mtarif_administrasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          if (UserAccesForm($user_acces_form,array('137'))){
              $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_administrasi" data-urlremove="'.site_url().'mtarif_administrasi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
					$aksi .= '<a href="'.site_url().'mtarif_administrasi/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
          $aksi .= '</div>';

					$row[] = $no;
					$row[] = GetJenisAdministrasi($r->idjenis).' ( '.$r->nama.' )';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}

	function setting($id){
		if($id != ''){
			$row = $this->Mtarif_administrasi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 										=> $row->id,
					'idtipe' 								=> $row->idtipe,
					'nama' 									=> $row->nama,
					'jasasarana' 						=> $row->jasasarana,
					'group_jasasarana' 			=> $row->group_jasasarana,
					'jasapelayanan' 				=> $row->jasapelayanan,
					'group_jasapelayanan'		=> $row->group_jasapelayanan,
					'bhp' 									=> $row->bhp,
					'group_bhp' 						=> $row->group_bhp,
					'biayaperawatan' 				=> $row->biayaperawatan,
					'group_biayaperawatan' 	=> $row->group_biayaperawatan,
					'total' 								=> $row->total,
					'group_jasasarana_diskon' =>$row->group_jasasarana_diskon,
					'group_jasapelayanan_diskon' =>$row->group_jasapelayanan_diskon,
					'group_bhp_diskon' =>$row->group_bhp_diskon,
					'group_biayaperawatan_diskon' =>$row->group_biayaperawatan_diskon,
					'jasasarana_diskon' =>$row->jasasarana_diskon,
					'jasapelayanan_diskon' =>$row->jasapelayanan_diskon,
					'bhp_diskon' =>$row->bhp_diskon,
					'biayaperawatan_dikson' =>$row->biayaperawatan_dikson,
					'group_diskon_all' =>$row->group_diskon_all,
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Setting Group Pembayaran Tarif Administrasi';
				$data['content']	 	= 'Mtarif_administrasi/setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Administrasi",'#'),
																array("Setting",'mtarif_administrasi')
															);

				$data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_administrasi', 'location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_administrasi', 'location');
		}
	}

	function save_setting() {
		if($this->Mtarif_administrasi_model->updateSetting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mtarif_administrasi/index/'.$this->input->post('idtipe'), 'location');
		}
	}
}
