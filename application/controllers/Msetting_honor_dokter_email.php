<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_honor_dokter_email extends CI_Controller {

	/**
	 * Setting Honor Dokter controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_honor_dokter_email_model');
  }

	function index() {
		$header = $this->Msetting_honor_dokter_email_model->getSettingHeader();
		$data = [
			'img_logo' => $header->img_logo,
			'img_ttd'	=> $header->img_ttd,
			'nama_ttd'	=> $header->nama_ttd,
			'nama_jabatan'	=> $header->nama_jabatan,
		];

		$data['error'] 			= '';
		$data['title'] 			= 'Setting General Email Honor Dokter';
		$data['content'] 		= 'Msetting_honor_dokter_email/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting General Email Honor Dokter",'#'),
									    			array("List",'msetting_honor_dokter_email')
													);

		$data['setting_catatan_footer']	= $this->Msetting_honor_dokter_email_model->getSettingCatatanFooter();
		$data['setting_kirim_email']	= $this->Msetting_honor_dokter_email_model->getSettingKirimEmail();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update() {
		if ($this->Msetting_honor_dokter_email_model->updateData()) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah disimpan.');
			redirect('msetting_honor_dokter_email', 'location');
		}
	}
}
