<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_pendapatan extends CI_Controller {

	/**
	 * Setting Jurnal Hutang controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_jurnal_pendapatan_model');
  }

	function index(){
		
		$data = $this->Msetting_jurnal_pendapatan_model->getSpecified('1');
		// $data['list_akun'] = $this->Msetting_jurnal_pendapatan_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Hutang';
		$data['content'] 		= 'Msetting_jurnal_pendapatan/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Hutang",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			// 'idakun_tunai'=>$this->input->post('idakun_tunai'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_pendapatan',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	

}
