<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_caller extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_caller_model');
		$this->load->helper('path');
		
  }
	function index(){
		// print_r(terbilang('001'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$user_id=$this->session->userdata('user_id');
		if (UserAccesForm($user_acces_form,array('1545'))){
			$DL =$this->Antrian_caller_model->get_data_login();
			if ($DL){
				$data=$DL;
			}else{
				$data['st_login']='0';
				$data['tanggal_login']='';
				$data['st_istirahat']='';
			}
			// print_r($data);exit;
			$data['list_counter'] 			= $this->Antrian_caller_model->list_counter();
			$data['list_counter2'] 			= $this->Antrian_caller_model->list_counter2();
			$data['user_id'] 			= $user_id;
			$data['tab'] 			= 1;
			$data['error'] 			= '';
			$data['title'] 			= 'Panggil Antrian';
			$data['content'] 		= 'Antrian_caller/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Antrian Transaksi",'#'),
												  array("Panggil Antrian",'antrian_caller')
												);
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function login(){
		$counter_id=$this->input->post('counter_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['user_login']=$user_id;
		$data['user_nama_login']=$user_nama_login;
		$data['tanggal_login']=date('Y-m-d H:i:s');
		$data['st_login']=1;
		$this->db->where('id',$counter_id);
		$hasil=$this->db->update('antrian_pelayanan_counter',$data);
		// print_r($counter_id);
		$this->output->set_output(json_encode($hasil));
		
	}
	function buka_antrian(){
		$antrian_id=$this->input->post('antrian_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['open_by']=$user_id;
		$data['open_nama']=$user_nama_login;
		$data['open_date']=date('Y-m-d H:i:s');
		$data['st_open']=1;
		$this->db->where('id',$antrian_id);
		$hasil=$this->db->update('antrian_pelayanan',$data);
		// print_r($counter_id);
		$this->output->set_output(json_encode($hasil));
		
	}
	function stop_antrian(){
		$antrian_id=$this->input->post('antrian_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['tutup_by']=$user_id;
		$data['tutup_nama']=$user_nama_login;
		$data['tutup_date']=date('Y-m-d H:i:s');
		$data['st_open']=0;
		$this->db->where('id',$antrian_id);
		$hasil=$this->db->update('antrian_pelayanan',$data);
		// print_r($counter_id);
		$this->output->set_output(json_encode($hasil));
		
	}
	function set_istirahat(){
		$counter_id=$this->input->post('counter_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		// $data['user_login']=$user_id;
		// $data['user_nama_login']=$user_nama_login;
		// $data['tanggal_login']=date('Y-m-d H:i:s');
		$data['st_istirahat']=1;
		$this->db->where('id',$counter_id);
		$hasil=$this->db->update('antrian_pelayanan_counter',$data);
		// print_r($counter_id);
		$this->output->set_output(json_encode($hasil));
		
	}
	function buka_istirahat(){
		$counter_id=$this->input->post('counter_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['antrian_harian_id']=null;
		// $data['user_nama_login']=$user_nama_login;
		// $data['tanggal_login']=date('Y-m-d H:i:s');
		$data['st_istirahat']=0;
		$this->db->where('id',$counter_id);
		$hasil=$this->db->update('antrian_pelayanan_counter',$data);
		// print_r($counter_id);
		$this->output->set_output(json_encode($hasil));
		
	}
	
	function logout(){
		$counter_id=$this->input->post('counter_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['user_login']=null;
		$data['user_nama_login']=null;
		$data['tanggal_login']='';
		$data['st_login']=0;
		$this->db->where('id',$counter_id);
		$hasil=$this->db->update('antrian_pelayanan_counter',$data);
		// print_r($counter_id);
		$this->output->set_output(json_encode($hasil));
		
	}
	function skip_antrian(){
		$antrian_id=$this->input->post('antrian_id');
		$antrian_harian_id=$this->input->post('antrian_harian_id');
		$counter_id=$this->input->post('counter_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		
		$data_insert=array(
			'st_panggil' => 2,
			
		);
		$this->db->where('id',$antrian_harian_id);
		$this->db->update('antrian_harian',$data_insert);
		
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_counter_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.counter_id='$counter_id' AND H.`status`='1'

			GROUP BY H.counter_id
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$q="SELECT H.id,H.antrian_id,H.noantrian,H.kodeantrian,H.tanggal 
			,H.kode as kode_alfa,LPAD(H.noantrian,3,'0') as kode_number
			FROM antrian_harian H
			WHERE H.st_panggil='0' AND H.antrian_id='$antrian_id' AND H.tanggal=CURRENT_DATE()
			ORDER BY H.noantrian ASC
			LIMIT 1";
		// print_r($q);exit;
		$data=$this->db->query($q)->row();
		if ($data){
			$sound_play='';
			$pelayanan_id=$data->antrian_id;
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.antrian_id='$pelayanan_id' AND H.`status`='1'

			GROUP BY H.antrian_id
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			$file_2=terbilang_sound($data->kode_number);
			$sound_play=$file_1.':'.$file_2.':'.$file_3;
			$data_insert=array(
				'antrian_harian_id' => $data->id,
				'pelayanan_id' => $data->antrian_id,
				'kodeantrian' => $data->kodeantrian,
				'counter_id' => $counter_id,
				'kode_alfa' => $data->kode_alfa,
				'kode_number' => $data->kode_number,
				'sound_play' => $sound_play,
				'tanggal' => $data->tanggal,
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			$this->db->insert('antrian_harian_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function call_antrian(){
		$antrian_id=$this->input->post('antrian_id');
		$counter_id=$this->input->post('counter_id');
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_counter_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.counter_id='$counter_id' AND H.`status`='1'

			GROUP BY H.counter_id
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$q="SELECT H.id,H.antrian_id,H.noantrian,H.kodeantrian,H.tanggal 
			,H.kode as kode_alfa,LPAD(H.noantrian,3,'0') as kode_number
			FROM antrian_harian H
			WHERE H.st_panggil='0' AND H.antrian_id='$antrian_id' AND H.tanggal=CURRENT_DATE()
			ORDER BY H.noantrian ASC
			LIMIT 1";
		// print_r($counter_id);
		$data=$this->db->query($q)->row();
		if ($data){
			$this->db->where('id',$data->id);
			$this->db->update('antrian_harian',array('st_panggil'=>1));
			$sound_play='';
			$pelayanan_id=$data->antrian_id;
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.antrian_id='$pelayanan_id' AND H.`status`='1'

			GROUP BY H.antrian_id
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			$file_2=terbilang_sound($data->kode_number);
			$sound_play=$file_1.':'.$file_2.':'.$file_3;
			$data_insert=array(
				'antrian_harian_id' => $data->id,
				'pelayanan_id' => $data->antrian_id,
				'kodeantrian' => $data->kodeantrian,
				'counter_id' => $counter_id,
				'kode_alfa' => $data->kode_alfa,
				'kode_number' => $data->kode_number,
				'sound_play' => $sound_play,
				'tanggal' => $data->tanggal,
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			$this->db->insert('antrian_harian_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function show_short_cut2(){
		$counter_id=$this->input->post('counter_id');
		$pelayanan_id=$this->input->post('pelayanan_id');
		$q="SELECT H.pelayanan_id,H.kodeantrian,H.antrian_harian_id FROM `antrian_harian_call` H WHERE H.counter_id='$counter_id' AND H.pelayanan_id='$pelayanan_id' AND H.st_panggil_suara='1' ORDER BY H.id DESC LIMIT 1";
		
		$data_insert=$this->db->query($q)->row_array();
			$hasil=$data_insert;
		// }else{
			// $hasil=false;
		// }
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function recall_antrian(){
		$antrian_harian_id=$this->input->post('antrian_harian_id');
		
		$counter_id=$this->input->post('counter_id');
		
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_counter_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.counter_id='$counter_id' AND H.`status`='1'

			GROUP BY H.counter_id
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$q="SELECT H.id,H.antrian_id,H.noantrian,H.kodeantrian,H.tanggal 
			,H.kode as kode_alfa,LPAD(H.noantrian,3,'0') as kode_number
			FROM antrian_harian H
			WHERE H.id='$antrian_harian_id'
			ORDER BY H.noantrian ASC
			LIMIT 1";
		// print_r($counter_id);
		$data=$this->db->query($q)->row();
		if ($data){
			$sound_play='';
			$pelayanan_id=$data->antrian_id;
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.antrian_id='$pelayanan_id' AND H.`status`='1'

			GROUP BY H.antrian_id
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			$file_2=terbilang_sound($data->kode_number);
			$sound_play=$file_1.':'.$file_2.':'.$file_3;
			$data_insert=array(
				'antrian_harian_id' => $data->id,
				'pelayanan_id' => $data->antrian_id,
				'kodeantrian' => $data->kodeantrian,
				'kode_alfa' => $data->kode_alfa,
				'kode_number' => $data->kode_number,
				'sound_play' => $sound_play,
				'counter_id' => $counter_id,
				'tanggal' => $data->tanggal,
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			// print_r($sound_angka);exit;
			$hasil=$this->db->insert('antrian_harian_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function cari_kode_booking(){
		$kode_booking=$this->input->post('kode_booking');
		$q="SELECT * FROM `app_reservasi_tanggal_pasien` H WHERE H.kode_booking='$kode_booking' AND H.tanggal >= CURRENT_DATE()";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function call_antrian_tf(){
		$id=$this->input->post('antrian_harian_id');
		
		$counter_id=$this->input->post('counter_id');
		
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_counter_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.counter_id='$counter_id' AND H.`status`='1'

			GROUP BY H.counter_id
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$q="SELECT H.id,H.antrian_id,H.antrian_harian_id,H.noantrian,H.kodeantrian,H.tanggal 
			,H.kode as kode_alfa,LPAD(H.noantrian,3,'0') as kode_number
			FROM antrian_harian_transfer H
			WHERE H.id='$id'
			LIMIT 1";
		// print_r($counter_id);
		$data=$this->db->query($q)->row();
		if ($data){
			$sound_play='';
			$pelayanan_id=$data->antrian_id;
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_pelayanan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.antrian_id='$pelayanan_id' AND H.`status`='1'

			GROUP BY H.antrian_id
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			$file_2=terbilang_sound($data->kode_number);
			$sound_play=$file_1.':'.$file_2.':'.$file_3;
			$data_insert=array(
				'antrian_harian_id' => $data->antrian_harian_id,
				'pelayanan_id' => $data->antrian_id,
				'kodeantrian' => $data->kodeantrian,
				'kode_alfa' => $data->kode_alfa,
				'kode_number' => $data->kode_number,
				'sound_play' => $sound_play,
				'counter_id' => $counter_id,
				'tanggal' => $data->tanggal,
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			// print_r($sound_angka);exit;
			$hasil=$this->db->insert('antrian_harian_call',$data_insert);
			
			$this->db->where('id',$id);
			$this->db->update('antrian_harian_transfer',array('st_panggil'=>1));
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function simpan_tf(){
		$user_nama_login=$this->session->userdata('user_name');
		$antrian_harian_id=$this->input->post('antrian_harian_id');
		$catatan=$this->input->post('catatan');
		$counter_id=$this->input->post('counter_id');
		
		
		$q="SELECT *FROM antrian_harian H WHERE H.id='$antrian_harian_id'";
		// print_r($counter_id);
		$data=$this->db->query($q)->row();
		if ($data){
			
			$data_insert=array(
				'antrian_harian_id' => $data->id,
				'antrian_id' => $data->antrian_id,
				'kodehari' => $data->kodehari,
				'namahari' => $data->namahari,
				'kode' => $data->kode,
				'noantrian' => $data->noantrian,
				'tanggal' => $data->tanggal,
				'waktu_ambil' => $data->waktu_ambil,
				'waktu_dilayani' => $data->waktu_dilayani,
				'st_panggil' => 0,
				'jml_panggil' => 0,
				'counter_id' => $counter_id,
				'catatan' => $catatan,
				'counter_id_asal' => $data->counter_id,
				'kodeantrian' => $data->kodeantrian,
				'tf_user' => $user_nama_login,
				'tf_date' => date('Y-m-d H:i:s'),
			);
			// print_r($sound_angka);exit;
			$hasil=$this->db->insert('antrian_harian_transfer',$data_insert);
			
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function test_angka($angka){
		$sound_angka=terbilang_sound($angka);
		print_r($sound_angka);exit;
	}
	
	function load_index_panggil(){
		$st_login=$this->input->post('st_login');
		$counter_id=$this->input->post('counter_id');
		$st_istirahat=$this->input->post('st_istirahat');
		$join_counter='';
		if ($st_login=='1'){
			$join_counter="INNER JOIN antrian_pelayanan_counter_pelayanan C ON C.counter_id='$counter_id' AND C.pelayanan_id=AP.id";
		}
		$this->select = array();
		  $from="(
			SELECT AH.id,D.antrian_id,AP.nama_pelayanan,AP.kode,AP.kuota,AP.open_nama,AP.tutup_nama
			,SUM(CASE WHEN AH.st_panggil='0' THEN 1 ELSE 0 END) as sisa_antrian
			,SUM(CASE WHEN AH.st_panggil IS NOT NULL THEN 1 ELSE 0 END) as total_antrian
			,COALESCE(MAX(AH.noantrian),0) +1 as antrian_ahir,AP.st_open
			,CASE WHEN AP.antrian_tanggal=CURRENT_DATE() THEN AP.antrian_harian_id ELSE '-' END antrian_harian_id
			,CASE WHEN AP.antrian_tanggal=CURRENT_DATE() THEN AP.kodantrian_dilayani ELSE '-' END antrian_dilayani
			,CASE WHEN AP.antrian_tanggal=CURRENT_DATE() THEN AP.counter_id_pemanggil ELSE null END counter_id_pemanggil
			,NN.kodeantrian as  next_nomor,CC.nama_counter,AP.st_tf
			,D.jam_buka,D.jam_tutup
			FROM date_row H
			INNER JOIN antrian_pelayanan_hari D ON D.kodehari=H.hari AND D.status=1 
			INNER JOIN antrian_pelayanan AP ON AP.id=D.antrian_id
			".$join_counter."
			LEFT JOIN antrian_harian AH ON AH.antrian_id=AP.id AND AH.tanggal=H.tanggal
			LEFT JOIN (
						SELECT B.antrian_id, MIN(B.kodeantrian) as kodeantrian FROM antrian_harian B WHERE B.tanggal=CURRENT_DATE() AND B.st_panggil='0' GROUP BY B.antrian_id
			) NN ON NN.antrian_id=D.antrian_id 
			
			LEFT JOIN antrian_pelayanan_counter CC ON CC.id=AP.counter_id_pemanggil
			WHERE H.tanggal=CURRENT_DATE()
			GROUP BY AP.id
			ORDER BY AP.urutan
			) as tbl";
			// print_r($from);exit();
			//antrian_pelayanan_counter
				$this->from   = $from;
				$this->join 	= array();
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('nama_pelayanan');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			$no++;
			$jam='<br><div class="text-muted">Jam Layanan ('.HumanTimeShort($r->jam_buka).' - '.HumanTimeShort($r->jam_tutup).')</div>';
			$antrian_dilayani="'".$r->antrian_dilayani."'";
			$result = array();
			$btn_stop_start = '&nbsp;&nbsp;';
				if ($r->st_open){
					$btn_stop_start .= '<button onclick="stop_antrian('.$r->antrian_id.')" type="button" title="Stop Pengambilan Tiket" class="btn btn-danger btn-sm "><i class="fa fa-power-off"></i></button>';	
					
				}else{
					$btn_stop_start .= '<button onclick="buka_antrian('.$r->antrian_id.')" type="button" title="Buka Pengambilan Tiket" class="btn btn-primary btn-sm "><i class="fa fa-folder-open-o"></i></button>';	
					
				}
			$result[] = $no;
			$result[] = $r->nama_pelayanan.$jam;
			$result[] = $r->st_open?text_primary('BUKA').($r->open_nama?'<br>'.$r->open_nama:''):text_danger('TUTUP').($r->tutup_nama?'<br>'.$r->tutup_nama:'');
			$result[] = $r->total_antrian;
			$result[] = $r->sisa_antrian;
			$result[] = $r->antrian_dilayani.'<br>'.text_success($r->nama_counter);
			$result[] = ($r->next_nomor?$r->next_nomor:'-');
			if ($st_login){
				if ($st_istirahat=='0'){
					
				$aksi = '<div class="btn-group">';
				if ($r->sisa_antrian){
				$aksi .= '<button onclick="call_antrian('.$r->antrian_id.')" type="button" title="Panggil" class="btn btn-primary btn-sm "><i class="fa fa-bell-o"></i></button>';	
				}
				if ($r->antrian_dilayani !='-'){
				if ($r->counter_id_pemanggil ==$counter_id){
						$aksi .= '<button onclick="recall_antrian('.$r->antrian_harian_id.')" type="button" title="Panggil Ulang" class="btn btn-info btn-sm "><i class="si si-refresh"></i></button>';	
						if ($r->st_tf =='0'){
						$aksi .= '<button onclick="transfer_antrian('.$r->antrian_harian_id.','.$antrian_dilayani.')" type="button" title="Transfer Antrian" class="btn btn-warning btn-sm "><i class="fa fa-mail-forward"></i></button>';	
						}
					}
				}
				 $aksi .= '</div> ';
				if ($r->next_nomor){
					$aksi .= '<div class="btn-group">';
					$aksi .= '<button onclick="skip_antrian('.$r->antrian_harian_id.','.$r->antrian_id.')" type="button" title="Lewati Antrian" class="btn btn-default btn-sm "><i class="fa fa-step-forward"></i></button>';	
					$aksi .= '</div>&nbsp;&nbsp;&nbsp;';
				}
				 $aksi .= '<div class="btn-group">';
				$aksi .= '<button onclick="set_istirahat()" type="button" title="Istirahat" class="btn btn-success btn-sm "><i class="fa fa-hand-stop-o"></i></button>';	
				$aksi .= '<button onclick="show_short_cut2('.$r->antrian_id.')" type="button" title="Shortcut" class="btn btn-info btn-sm "><i class="fa fa-keyboard-o"></i></button>';	
				
				$aksi .= '</div>';
				$aksi .=$btn_stop_start;
				}else{
					$aksi=text_warning('SEDANG ISTIRAHAT').' ';
				}
			}else{
				$aksi=text_danger('SILAHKAN LOGIN').' ';
			}
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	}
	function load_index_tf(){
		$st_login=$this->input->post('st_login');
		$counter_id=$this->input->post('counter_id');
		$st_istirahat=$this->input->post('st_istirahat');
		$st_panggil=$this->input->post('st_panggil');
		// print_r($counter_id);exit;
		$where='';
		if ($st_panggil!='#'){
			$where=" AND H.st_panggil='$st_panggil'";
		}
		
		$this->select = array();
		  $from="(
			SELECT C.nama_counter,H.* FROM antrian_harian_transfer H
				INNER JOIN antrian_pelayanan_counter C ON C.id=H.counter_id_asal
				WHERE H.counter_id='$counter_id' AND H.tanggal=CURRENT_DATE() ".$where." ORDER BY H.waktu_dilayani ASC
			) as tbl";
			// print_r($from);exit();
			//antrian_pelayanan_counter
				$this->from   = $from;
				$this->join 	= array();
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('nama_counter','kodeantrian');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			$no++;
			$result = array();
			$antrian_dilayani="'".$r->kodeantrian."'";
			$result[] = $no;
			$result[] = $r->kodeantrian;
			$result[] = $r->nama_counter;
			$result[] = '<strong>'.$r->tf_user.'</strong><br>'.HumanDateLong($r->tf_date);
			$result[] = ($r->st_panggil?text_primary('SUDAH DILAYANI'):text_default('BELUM DILAYANI'));
			$result[] = ($r->catatan);
			if ($st_login){
				if ($st_istirahat=='0'){
					$aksi = '<div class="btn-group">';
					if ($r->st_panggil=='0'){
						$aksi .= '<button onclick="call_antrian_tf('.$r->id.')" type="button" title="Panggil" class="btn btn-primary btn-sm "><i class="fa fa-bell-o"></i></button>';	
					}
					if ($r->st_panggil){
						$aksi .= '<button onclick="call_antrian_tf('.$r->id.')" type="button" title="Panggil Ulang" class="btn btn-info btn-sm "><i class="si si-refresh"></i></button>';	
					}
							$aksi .= '<button onclick="transfer_antrian('.$r->antrian_harian_id.','.$antrian_dilayani.')" type="button" title="Transfer Antrian" class="btn btn-warning btn-sm "><i class="fa fa-mail-forward"></i></button>';	
					 $aksi .= '</div> ';
					
					 $aksi .= '<div class="btn-group">';
					$aksi .= '<button onclick="set_istirahat()" type="button" title="Istirahat" class="btn btn-success btn-sm "><i class="fa fa-hand-stop-o"></i></button>';	
					
					$aksi .= '</div>';
				}else{
					$aksi=text_warning('SEDANG ISTIRAHAT').' ';
				}
			}else{
				$aksi=text_danger('SILAHKAN LOGIN').' ';
			}
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	}
	function load_counter_layanan(){
		
		$counter_id=$this->input->post('counter_id');
		$st_login=$this->input->post('st_login');
		if ($st_login=='0'){
			$counter_id='';
		}
		
		
		$q="SELECT H.id,H.nama_counter,A.kodeantrian,H.st_login,H.st_istirahat,H.user_nama_login FROM antrian_pelayanan_counter H
				LEFT JOIN antrian_harian A ON A.id=H.antrian_harian_id AND A.tanggal=CURRENT_DATE()
				WHERE H.`status`='1'
				ORDER BY H.nama_counter ASC";
		$list_row=$this->db->query($q)->result();
		$tabel='';
		foreach($list_row as $row){
			if ($row->st_istirahat){
			$kodeantrian='CLOSE';
				
			}else{
			$kodeantrian=$row->kodeantrian;
				
			}
			$tabel .='<div class="col-sm-12">
						<div class="block block-themed">
							<div class="block-header '.($counter_id==$row->id?'bg-danger':'bg-success').'">
								<h3 class="block-title text-center">'.$row->nama_counter.'</h3>
							</div>
							<div class="content-mini content-mini-full bg-primary-lighter">
								<div>
									<div class="h1 font-w700 text-black text-center">'.$kodeantrian.'</div>
								</div>
							</div>
						</div>
					</div>';
		}
		$this->output->set_output(json_encode($tabel));
	}
	function load_jumlah_transfer(){
		$counter_id=$this->input->post('counter_id');
		$st_login=$this->input->post('st_login');
		$q="SELECT COUNT(H.id) as jml_tf FROM `antrian_harian_transfer` H WHERE H.tanggal=CURRENT_DATE() AND H.st_panggil='0' AND H.counter_id='$counter_id'";
		$tabel=$this->db->query($q)->row('jml_tf');
		$this->output->set_output(json_encode($tabel));
		
	}
}
