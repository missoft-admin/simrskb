<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

defined('BASEPATH') or exit('No direct script access allowed');

class Lkasir extends CI_Controller
{

    /**
     * Laporan Kasir controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Lkasir_model');
        $this->load->model('munitpelayanan_model', 'unitpelayanan');
    }

    public function index()
    {
        $data = array(
          'jenis_laporan' => '#',
          'tanggaldari' => date('d-m-Y'),
          'tanggalsampai' => date('d-m-Y'),
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Laporan Kasir';
        $data['content'] 		= 'Lkasir/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Laporan Kasir",'Lkasir/index'),
            array("List",'#')
        );

        $data['list_user']=$this->Lkasir_model->list_user();
        $data['list_dokter']=$this->Lkasir_model->list_dokter();
        $data['list_unitpelayanan'] = $this->unitpelayanan->getDefaultUnitPelayananUser();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function print_laporan()
    {
        // print_r(ceiling(101, 100));exit();
        $jenis_laporan     = $this->input->post('jenis_laporan');
        $user_id     	= $this->input->post('user_id');

        $dokter_id    = $this->input->post('dokter_id');
        $tanggaldari    = YMDFormat($this->input->post('tanggaldari'));
        $tanggalsampai   = YMDFormat($this->input->post('tanggalsampai'));
        $q="UPDATE tpoliklinik_pelayanan T1 INNER JOIN (
			SELECT H.tanggal, Pel.id,Pel.iddokter,Tin.tanggal_tindakan,
			CASE WHEN DATE_FORMAT(Tin.tanggal_tindakan,'%H:%i') < '12:00' THEN mdokter.potonganrspagi  ELSE mdokter.potonganrssiang END as m_potongan,mdokter.pajak as m_pajak
			,Pel.pajak_dokter,Pel.pot_rs
			from tkasir H
			INNER JOIN tpoliklinik_pelayanan Pel ON Pel.idtindakan=H.idtindakan
			INNER JOIN tpoliklinik_tindakan Tin On Tin.id=H.idtindakan
			INNER JOIN mdokter ON mdokter.id=Pel.iddokter
			WHERE H.idtipe IN (1,2) AND H.`status`='2' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')='$tanggaldari' AND Pel.pajak_dokter=0) T2
			SET T1.pajak_dokter=T2.m_pajak,T1.pot_rs=T2.m_potongan
			WHERE T1.id=T2.id";

		$this->db->query($q);

		$data=array();
		if (in_array("#", $user_id)){
			$q_user="SELECT id,name from musers WHERE `status`='1' AND st_kasir='1'";
			$data['user_pilih']=' ALL USER';
		}else{
			$user_id=implode(", ", $user_id);
			$q_user="SELECT id,name from musers WHERE `status`='1' AND id IN ($user_id)";
			$data['user_pilih']=' PER USER';
		}
		$data['user_list']=$this->db->query($q_user)->result();
		if ($dokter_id){
			if (in_array("#", $dokter_id)){
				$q_dokter="SELECT id,nama from mdokter WHERE `status`='1'";
			}else{
				$dokter_id=implode(", ", $dokter_id);
				$q_dokter="SELECT id,nama from mdokter WHERE `status`='1' AND id IN ($dokter_id)";
			}
			$data['dokter_list']=$this->db->query($q_dokter)->result();
		}else{
			$data['dokter_list']=array();
		}
        // $this->db->query($q);



        $dompdf = new Dompdf();

        $data['tanggaldari']=$tanggaldari;
        $data['tanggalsampai']=$tanggalsampai;
        $data['nama_periode']=HumanDateShort($tanggaldari).' s/d '.HumanDateShort($tanggalsampai);

        $data=array_merge($data, backend_info());
        if ($jenis_laporan=='1') {
            $html = $this->load->view('Lkasir/print_penerimaan', $data, true);
        } elseif ($jenis_laporan=='2') {
            $data['list_penerimaan']=$this->Lkasir_model->list_penerimaan_dokter();
            // print_r($data['list_penerimaan']);exit();
            // $this->Lkasir_model->list_dokter();
            $html = $this->load->view('Lkasir/print_penerimaan_dokter', $data, true);
        } elseif ($jenis_laporan=='3') {
            $html = $this->load->view('Lkasir/print_penerimaan_ranap', $data, true);
        }
        $html = $this->parser->parse_string($html, $data);
        // print_r($html);exit();
        $dompdf->loadHtml($html);
		// $dompdf->set_option('isHtml5ParserEnabled', true);
        if ($jenis_laporan=='3') {
          $dompdf->setPaper('A4', 'landscape');
        } else {
          $dompdf->setPaper('letter', 'portrait');
        }

        $dompdf->render();

        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }

    // public function export_excel($idtipe, $idbarang, $idunit)
    // {
        // $header = $this->Lkasir_model->get_info($idtipe, $idbarang, $idunit);
        // $data = $this->Lkasir_model->getKartuStok($idtipe, $idbarang);

        // $this->load->library('excel');
        // $this->excel->setActiveSheetIndex(0);
        // $activeSheet = $this->excel->getActiveSheet();
        // $activeSheet->setTitle('Kartu Stok');

        // // Set Logo
        // $objDrawing = new PHPExcel_Worksheet_Drawing();
        // $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        // $objDrawing->setPath($logo);
        // $objDrawing->setCoordinates('B1');
        // $objDrawing->setResizeProportional(false);
        // $objDrawing->setHeight(70);
        // $objDrawing->setWidth(30);
        // $objDrawing->setWorksheet($activeSheet);

        // // Set Header
        // $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        // $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        // $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        // $activeSheet->mergeCells('B1:AA1');
        // $activeSheet->mergeCells('B2:AA2');
        // $activeSheet->mergeCells('B3:AA3');
        // $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        // $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        // $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

        // // Set Title
        // $activeSheet->setCellValue('B5', "KARTU STOK");
        // $activeSheet->mergeCells('B5:J5');
        // $activeSheet->getStyle('B5')->getFont()->setBold(true)->setSize(14);
        // $activeSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        // // Set Periode
        // $activeSheet->setCellValue('B7', "Unit Pelayanan");
        // $activeSheet->setCellValue('C7', $header['namaunit']);

        // $activeSheet->setCellValue('B8', "Kode Barang");
        // $activeSheet->setCellValue('C8', $header['kode']);

        // $activeSheet->setCellValue('B9', "Satuan Kecil");
        // $activeSheet->setCellValue('C9', $header['namasatuan']);

        // $activeSheet->setCellValue('E7', "Nama Barang");
        // $activeSheet->setCellValue('F7', $header['namabarang']);

        // $activeSheet->setCellValue('E8', "Tipe Barang");
        // $activeSheet->setCellValue('F8', $header['namatipe']);

        // $activeSheet->setCellValue('E9', "Kategori");
        // $activeSheet->setCellValue('F9', $header['namakategori']);

        // // Set Header Column
        // $activeSheet->setCellValue('B11', "Tanggal");
        // $activeSheet->setCellValue('C11', "User");
        // $activeSheet->setCellValue('D11', "Jenis Transaksi");
        // $activeSheet->setCellValue('E11', "No. Transaksi");
        // $activeSheet->setCellValue('F11', "Tujuan");
        // $activeSheet->setCellValue('G11', "Keterangan");
        // $activeSheet->setCellValue('H11', "Penerimaan");
        // $activeSheet->setCellValue('I11', "Pengeluaran");
        // $activeSheet->setCellValue('J11', "Sisa Stok");
        // $activeSheet->getStyle('B11:J12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $activeSheet->getStyle("B11:J12")->applyFromArray(
            // array(
                        // 'borders' => array(
                            // 'allborders' => array(
                              // 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    // ),
              // 'top' => array(
                                // 'style' => PHPExcel_Style_Border::BORDER_THICK,
                            // )
                        // ),
                        // 'font'  => array(
                            // 'bold'  => true,
                            // 'size'  => 11,
                            // 'name'  => 'Calibri'
                        // )
                    // )
        // );

        // $x = 11;
        // $sisaStok = 0;

        // if (COUNT($data)) {
            // foreach ($data as $row) {
                // $x = $x+1;

                // $sisaStok = $sisaStok + $row->penerimaan - $row->pengeluaran;

                // // Set Content
                // $activeSheet->setCellValue("B$x", $row->tanggal);
                // $activeSheet->setCellValue("C$x", $row->user);
                // $activeSheet->setCellValue("D$x", $row->jenis_trx);
                // $activeSheet->setCellValue("E$x", $row->notransaksi);
                // $activeSheet->setCellValue("F$x", $row->tujuan);
                // $activeSheet->setCellValue("G$x", $row->keterangan);
                // $activeSheet->setCellValue("H$x", $row->penerimaan);
                // $activeSheet->setCellValue("I$x", $row->pengeluaran);
                // $activeSheet->setCellValue("J$x", $sisaStok);
                // $activeSheet->getStyle("B$x:C$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $activeSheet->getStyle("E$x:F$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $activeSheet->getStyle("H$x:J$x")->getNumberFormat()->setFormatCode('#,##0');
                // $activeSheet->getStyle("H$x:J$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                // $activeSheet->getStyle("B$x:J$x")->applyFromArray(
                    // array(
                                // 'borders' => array(
                                    // 'allborders' => array(
                                            // 'style' => PHPExcel_Style_Border::BORDER_THIN,
                                    // )
                                // )
                            // )
                // );
            // }
        // }

        // $x = $x+1;

        // // Set Info Print
        // $activeSheet->setCellValue("B$x", 'Sistem Informasi RSKB Halmahera - Tanggal Cetak '.date("d/m/Y - h:i:s"));
        // $activeSheet->getStyle("B$x")->getFont()->setSize(11);
        // $activeSheet->mergeCells("B$x:J$x");
        // $activeSheet->getStyle("B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        // $x = $x+1;

        // // Set Auto Width Column
        // $activeSheet->getColumnDimension("A")->setWidth(5);
        // $activeSheet->getColumnDimension("B")->setAutoSize(true);
        // $activeSheet->getColumnDimension("C")->setAutoSize(true);
        // $activeSheet->getColumnDimension("D")->setAutoSize(true);
        // $activeSheet->getColumnDimension("E")->setAutoSize(true);
        // $activeSheet->getColumnDimension("F")->setAutoSize(true);
        // $activeSheet->getColumnDimension("G")->setAutoSize(true);
        // $activeSheet->getColumnDimension("H")->setAutoSize(true);
        // $activeSheet->getColumnDimension("I")->setAutoSize(true);
        // $activeSheet->getColumnDimension("J")->setAutoSize(true);

        // header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="Kartu Stok.xls"');
        // header('Cache-Control: max-age=0');

        // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // $objWriter->save('php://output');
    // }
}
