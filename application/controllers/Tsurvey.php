<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tsurvey extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->model('Tsurvey_model');
		$this->load->helper('path');
		
  }
	function cetak_survey_kepuasan($assesmen_id,$tipe='1'){
		$this->Tsurvey_model->cetak_survey_kepuasan($assesmen_id,$tipe,0);
		
	}
  
	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
	
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_satuan_ttv=array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		$data_login_ppa=get_ppa_login();
		// print_r($data_login_ppa);exit;
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tpendaftaran_poli_ttv_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['st_ranap'] 			= $st_ranap;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['assesmen_id'] 			= '';
		$data['status_assesmen'] 			= '1';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Survey Kepuasan Pasien Pada Unit Pelayanan';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan Poliklinik ",'#'),
											  array("Pemeriksaan Komunkasi Efektif",'tpendaftaran_poli_ttv')
											);
		
		
		//SBAR
		if ($menu_kiri=='input_survey' || $menu_kiri=='his_survey'){
			$data['template_assesmen_id']='';
			if ($trx_id!='#'){
				if ($menu_kiri=='his_survey'){
					$data_assemen=$this->Tsurvey_model->get_data_survey_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tsurvey_model->get_data_survey_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tsurvey_model->get_data_survey_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tsurvey_model->get_data_survey($pendaftaran_id,$st_ranap);
			// print_r($data_assemen);exit;
				
			}
			if ($data_assemen){
				
				
			}else{
				$data_assemen['total_skor_survey_kepuasan']='0';
				$data_assemen['msurvey_kepuasan_id']='0';
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['tipe_input_nama']='';
				$data_assemen['profesi_id']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_survey'){
				
				
				
			}else{
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$list_template_assement=$this->Tsurvey_model->list_template_survey();
			$data_assemen['list_template_assement']=$list_template_assement;
			
			$setting_survey=$this->Tpendaftaran_poli_ttv_model->setting_survey();
			$setting_survey_label=$this->Tsurvey_model->setting_survey_label();
			// print_r($setting_survey_field);exit;
			$jml_verif=0;
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			$login_ppa_id=$this->session->userdata('login_ppa_id');
				
			$data = array_merge($data,$data_assemen,$setting_survey,$setting_survey_label);
			$data['pendaftaran_id']=$pendaftaran_id;
		}

		$data['trx_id']=$trx_id;
		// print_r($data);exit;
		$data = array_merge($data, backend_info());
			// print_r($list_template_assement);exit;
		$this->parser->parse('module_template', $data);
		
	}
	function load_ttd(){
		$assesmen_id = $this->input->post('assesmen_id');
		$q="SELECT H.ttd,H.ttd_date,H.nama_profile as ttd_nama
			FROM tpoliklinik_survey_kepuasan H WHERE H.assesmen_id='$assesmen_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function load_profile(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$pendaftaran_id = $this->input->post('pendaftaran_id');
		$pendaftaran_id_ranap = $this->input->post('pendaftaran_id_ranap');
		$jenis_ttd = $this->input->post('jenis_ttd');
		$st_ranap = $this->input->post('st_ranap');
		if ($st_ranap=='1'){
			$q="
				SELECT H.namapasien,H.tanggal_lahir,H.umurtahun,H.jenis_kelamin,H.pendidikan,H.pekerjaan 
				,H.namapengantar,H.namapenanggungjawab
				FROM trawatinap_pendaftaran H
				WHERE H.id='$pendaftaran_id_ranap'
			";
		}else{
			$q="
				SELECT H.namapasien,H.tanggal_lahir,H.umurtahun,H.jenis_kelamin,H.pendidikan,H.pekerjaan 
				,H.namapengantar,H.namapenanggungjawab
				FROM tpoliklinik_pendaftaran H
				WHERE H.id='$pendaftaran_id'
			";
		}
		$row=$this->db->query($q)->row();
		if ($jenis_ttd=='3'){//PASIEN
			$hasil['nama']=$row->namapasien;
			$hasil['tanggal_lahir']=HumanDateShort($row->tanggal_lahir);
			$hasil['umur']=$row->umurtahun;
			$hasil['jenis_kelamin']=$row->jenis_kelamin;
			$hasil['pendidikan']=$row->pendidikan;
			$hasil['pekerjaan']=$row->pekerjaan;
			
		}
		if ($jenis_ttd=='1'){//PJ
			$hasil['nama']=$row->namapenanggungjawab;
			$hasil['tanggal_lahir']='';
			$hasil['umur']='';
			$hasil['jenis_kelamin']='';
			$hasil['pendidikan']='';
			$hasil['pekerjaan']='';
			
		}
		if ($jenis_ttd=='2'){//PJ
			$hasil['nama']=$row->namapengantar;
			$hasil['tanggal_lahir']='';
			$hasil['umur']='';
			$hasil['jenis_kelamin']='';
			$hasil['pendidikan']='';
			$hasil['pekerjaan']='';
			
		}
		if ($jenis_ttd=='4'){//PJ
			$hasil['nama']=$row->namapasien;
			$hasil['tanggal_lahir']='';
			$hasil['umur']='';
			$hasil['jenis_kelamin']='';
			$hasil['pendidikan']='';
			$hasil['pekerjaan']='';
			
		}
		// $hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_ttd(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_nama = $this->input->post('ttd_nama');
		$nama_tabel ='tpoliklinik_survey_kepuasan';
		$ttd = $this->input->post('signature64');
		$data=array(
			'ttd_nama' =>$ttd_nama,
			'ttd' =>$ttd,
			'ttd_date' =>date('Y-m-d H:i:s'),
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	function batal_template_survey_kepuasan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_survey_kepuasan',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function create_survey_kepuasan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $msurvey_kepuasan_id=$this->input->post('msurvey_kepuasan_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="
		SELECT H.isi_header,H.isi_footer,H.nilai_tertimbang,H.nilai_satuan,H.nama as nama_kajian FROM msurvey_kepuasan H WHERE H.id='$msurvey_kepuasan_id'
	  ";
	  $data_header=$this->db->query($q)->row();
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'msurvey_kepuasan_id' => $msurvey_kepuasan_id,
		'total_skor_survey_kepuasan' => 0,
		'created_ppa' => $login_ppa_id,
		'tanggal_survey' => date('Y-m-d H:i:s'),
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		'jenis_ttd' => '0',
		'isi_header' => $data_header->isi_header,
		'isi_footer' => $data_header->isi_footer,
		'nilai_tertimbang' => $data_header->nilai_tertimbang,
		'nilai_satuan' => $data_header->nilai_satuan,
		'nama_kajian' => $data_header->nama_kajian,
		
	  );
	  // $data = array_merge($data, $data_header);
	  $hasil=$this->db->insert('tpoliklinik_survey_kepuasan',$data);
	  $assesmen_id=$this->db->insert_id();
	  // $q="
		// SELECT '$assesmen_id' as assesmen_id,H.id as param_id,H.msurvey_kepuasan_id
		// ,H.inisial,H.parameter_nama as parameter_nama,H.st_nilai
		// FROM msurvey_kepuasan_param H
		// WHERE H.msurvey_kepuasan_id='$msurvey_kepuasan_id'
	  // ";
	  $q="
		SELECT '$assesmen_id' as assesmen_id,H.id as param_id,H.msurvey_kepuasan_id
		,H.inisial,H.parameter_nama as parameter_nama,H.st_nilai
		,CASE WHEN D.id IS NOT NULL THEN D.id ELSE NULL END as jawaban_id
		,CASE WHEN D.id IS NOT NULL THEN D.skor ELSE 0 END as jawaban_skor
		FROM msurvey_kepuasan_param H
		LEFT JOIN msurvey_kepuasan_param_skor D ON D.parameter_id=H.id AND D.st_default='1'
		WHERE H.msurvey_kepuasan_id='$msurvey_kepuasan_id'
		GROUP BY H.id
	  ";
	  $list_param=$this->db->query($q)->result();
	  foreach($list_param as $row){
		  $data_param=array(
				'assesmen_id' => $row->assesmen_id,
				'param_id' => $row->param_id,
				'msurvey_kepuasan_id' => $row->msurvey_kepuasan_id,
				'inisial' => $row->inisial,
				'parameter_nama' => $row->parameter_nama,
				'st_nilai' => $row->st_nilai,
				'jawaban_id' => $row->jawaban_id,
				'jawaban_skor' => $row->jawaban_skor,
		  );
		  $parameter_id=$row->param_id;
		  $this->db->insert('tpoliklinik_survey_kepuasan_param',$data_param);
		  $param_id=$this->db->insert_id();
		  $q_insert="
			INSERT INTO tpoliklinik_survey_kepuasan_param_skor (assesmen_id,parameter_id,skor,deskripsi_nama,master_id)
			SELECT '$assesmen_id' as assesmen_id,'$param_id' as parameter_id,H.skor,H.deskripsi_nama,id FROM msurvey_kepuasan_param_skor H
			WHERE H.parameter_id='$parameter_id'
		  ";
		  $this->db->query($q_insert);
	  }
	  $this->output->set_output(json_encode($hasil));
    }
	
	
  
	function save_survey_kepuasan(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$tanggal_survey= YMDFormat($this->input->post('tglsurvey')). ' ' .$this->input->post('waktusurvey');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$total_skor_survey_kepuasan=$this->input->post('total_skor_survey_kepuasan');
		$msurvey_kepuasan_id=$this->input->post('msurvey_kepuasan_id');
		$q="
			SELECT H.ref_nilai FROM msurvey_kepuasan_setting_nilai H
				WHERE ".$total_skor_survey_kepuasan." BETWEEN H.skor_1 AND H.skor_2 AND H.staktif='1' AND H.msurvey_kepuasan_id='$msurvey_kepuasan_id'
		";
		$hasil_penilaian=$this->db->query($q)->row('ref_nilai');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'tanggal_survey'=> $tanggal_survey,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			'jenis_ttd' => $this->input->post('jenis_ttd'),
			'total_skor_survey_kepuasan' => $this->input->post('total_skor_survey_kepuasan'),
			'nama_kajian' => $this->input->post('nama_kajian'),
			'isi_header' => $this->input->post('isi_header'),
			'isi_footer' => $this->input->post('isi_footer'),
			'nama_profile' => $this->input->post('nama_profile'),
			'jk_profile' => $this->input->post('jk_profile'),
			'mengetahui_rs' => $this->input->post('mengetahui_rs'),
			'ttl_profile' => YMDFormat($this->input->post('ttl_profile')),
			'pendidikan_profile' => $this->input->post('pendidikan_profile'),
			'lainnya_profile' => $this->input->post('lainnya_profile'),
			'umur_profile' => $this->input->post('umur_profile'),
			'pekerjaan_profile' => $this->input->post('pekerjaan_profile'),
			'kritik' => $this->input->post('kritik'),
			'hasil_penilaian' => $hasil_penilaian,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_survey_kepuasan_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_survey_kepuasan',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	 function load_nilai_param_survey_kepuasan(){
		 
		 $assesmen_id=$this->input->post('assesmen_id');
		 
		  $q="SELECT *FROM tpoliklinik_survey_kepuasan_param WHERE assesmen_id='$assesmen_id' ORDER BY id ASC";
		  $list_data=$this->db->query($q)->result();
		  $tabel='';
		  $nourut=1;
		  $total_skor=0;
		  foreach ($list_data as $row){
			  $warna='#fff';
			  // if ($row->st_header){
				// $warna='#e5eeff';  
			  // }
				$tabel .='<tr style="background-color:'.$warna.'">';
				$tabel .='<td><input type="hidden" class="parameter_id" value="'.$row->id.'">'.$nourut.'</td>';
				$tabel .='<td class="text-bold"><strong>'.$row->parameter_nama.'</strong></td>';
				$tabel .='<td >'.$this->opsi_nilai_survey_kepuasan($row->id,$row->jawaban_id).'</td>';
				$tabel .='<td class="text-bold" hidden><input readonly type="text" class="form-control parameter_skor" value="'.$row->jawaban_skor.'"></td>';
				$nourut=$nourut+1;
				$total_skor=$total_skor+$row->jawaban_skor;
			  $tabel .='</tr>';
		  }
		  $tabel.='<tr><td colspan="2" class="text-primary text-right"><strong>TOTAL PENILAIAN</strong></td><td><input readonly type="text" class="form-control total_skor" value="'.$total_skor.'"></td></tr>';
		  
		  $arr['tabel']=$tabel;
		  $arr['total_skor']=$total_skor;
		  $this->output->set_output(json_encode($arr));
	  }
	  function opsi_nilai_survey_kepuasan($parameter_id,$master_id){
		  $q="SELECT *FROM tpoliklinik_survey_kepuasan_param_skor WHERE parameter_id='$parameter_id' ORDER BY id ASC";
		  $list_nilai=$this->db->query($q)->result();
		  $opsi='<select class="js-select2 form-control nilai_survey_kepuasan"  style="width: 100%;" data-placeholder="Belum Mengisi">';
		  $opsi .='<option value="" '.($master_id==''?'selected':'').'>- Pilih Jawaban -</option>'; 
		  foreach($list_nilai as $row){
				 $opsi .='<option value="'.$row->master_id.'" data-skor="'.$row->skor.'" '.($master_id==$row->master_id?'selected':'').'>'.$row->deskripsi_nama.'</option>'; 
		  }
		  $opsi .="</select>";
		  return $opsi;
	  }
	  function update_nilai_survey_kepuasan(){
		$assesmen_id=$this->input->post('assesmen_id');
		$parameter_id=$this->input->post('parameter_id');
		$nilai_id=$this->input->post('nilai_id');
		$skor=$this->input->post('skor');
		
		
		$data=array(
			'jawaban_id' => $nilai_id,
			'jawaban_skor' => $skor,
			);
		$this->db->where('id',$parameter_id);
		$result=$this->db->update('tpoliklinik_survey_kepuasan_param',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_history_pengkajian_survey_kepuasan()
    {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tsurvey_model->logic_akses_survey($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		if ($mppa_id!='#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_survey_kepuasan")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id
					,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE RI.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MPOL.nama ELSE 
						CONCAT(COALESCE(mruangan.nama,''),' - ',COALESCE(mkelas.nama,''),' - ',COALESCE(mbed.nama,''))
					
					END as poli
					,CASE WHEN H.st_ranap='0' THEN MP.iddokter ELSE RI.iddokterpenanggungjawab END as iddokter
					,CASE WHEN H.st_ranap='0' THEN MD.nama ELSE MDRI.nama END as dokter
					,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd,H.total_skor_survey_kepuasan,H.hasil_penilaian,H.st_ranap,H.pendaftaran_id_ranap
					FROM `tpoliklinik_survey_kepuasan` H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
					LEFT JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mdokter MDRI ON MDRI.id=RI.iddokterpenanggungjawab
					LEFT JOIN mruangan ON mruangan.id=RI.idruangan
					LEFT JOIN mbed ON mbed.id=RI.idbed
					LEFT JOIN mkelas ON mkelas.id=RI.idkelas
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			if ($r->st_ranap=='1'){
				$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_survey/input_survey/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
				$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id.'/0/erm_survey/input_survey/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			$btn_cetak_1='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurvey/cetak_survey_kepuasan/'.$r->assesmen_id.'/1/0"  type="button" title="Cetak Internal" type="button"><i class="fa fa-print"></i> Internal</a>';
			$btn_cetak_2='<a class="btn btn-primary  btn-xs" target="_blank" href="'.site_url().'tsurvey/cetak_survey_kepuasan/'.$r->assesmen_id.'/2/0"  type="button" title="Cetak External" type="button"><i class="fa fa-print"></i> External</a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			$btn_ttd='';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_survey']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_survey']=='0'){
			  $btn_hapus='';
			}
			
			$result[] = $no;
			$aksi='';
			$aksi_edit='';
			
			if ($r->jml_edit>0){
				if ($r->st_ranap=='1'){
				$aksi_edit='<a href="'.base_url().'tsurvey/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_survey/his_survey/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
					
				}else{
					
				$aksi_edit='<a href="'.base_url().'tsurvey/tindakan/'.$r->pendaftaran_id.'/0/erm_survey/his_survey/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
				}
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_hapus;	
			if ($logic_akses_assesmen['st_lihat_survey']=='1'){
			$aksi .= $btn_lihat;	
			}
			if ($logic_akses_assesmen['st_cetak_survey']=='1'){
			$aksi .= $btn_cetak_1.$btn_cetak_2;	
			}
			$aksi .= $aksi_edit;	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = HumanDateLong($r->tanggaldaftar);
			$result[] = ($r->nopendaftaran);
			$result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
			$result[] = HumanDateLong($r->tanggal_pengkajian).'<br>'.text_success($r->hasil_penilaian).' '.text_danger($r->total_skor_survey_kepuasan);
			$result[] = ($r->nama_mppa).$btn_ttd;
			$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			$data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
		}
		
		function save_edit_survey_kepuasan(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_edit_id=$this->input->post('alasan_id');
			$keterangan_edit=$this->input->post('keterangan_edit');
			$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_survey_kepuasan WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// $jml_edit=$this->input->post('jml_edit');
			$res=$this->simpan_history_survey_kepuasan($assesmen_id,$jml_edit);
			// print_r($res);exit;
			if ($res){
				
				$data=array(
					'status_assemen' => 1,
					'st_edited' => 1,
					'alasan_edit_id' =>$alasan_edit_id,
					'keterangan_edit' =>$keterangan_edit,

				);
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$data['edited_ppa']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('assesmen_id',$assesmen_id);
				$result=$this->db->update('tpoliklinik_survey_kepuasan',$data);
				
				$data=$this->db->query("SELECT * FROM tpoliklinik_survey_kepuasan WHERE assesmen_id='$assesmen_id'")->row_array();
				if ($result){
					$hasil=$data;
				}else{
					$hasil=false;
				}
			}else{
				$hasil=$res;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function simpan_history_survey_kepuasan($assesmen_id,$jml_edit){
			$hasil=false;
			// $jml_edit=$jml_edit+1;
			$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_survey_kepuasan_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
			// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
			if ($baris==0){
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="INSERT IGNORE INTO tpoliklinik_survey_kepuasan_his 
							(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap
							,idpasien,created_date,created_ppa,msurvey_kepuasan_id,status_assemen
							,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,total_skor_survey_kepuasan,nama_kajian,isi_header,isi_footer,nilai_tertimbang,nilai_satuan,nama_profile,jk_profile,mengetahui_rs,ttl_profile,pendidikan_profile,lainnya_profile,umur_profile,pekerjaan_profile,tanggal_survey,kritik,hasil_penilaian,nilai_per_unsur,jumlah_jawaban,nrr_per_unsur,nrr_tertimbang,kepuasan_mas,flag_nilai_mas,hasil_flag
							,edited_ppa,edited_date,alasan_edit_id,keterangan_edit
							)
					SELECT '$jml_edit' as versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap
							,idpasien,created_date,created_ppa,msurvey_kepuasan_id,status_assemen
							,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,total_skor_survey_kepuasan,nama_kajian,isi_header,isi_footer,nilai_tertimbang,nilai_satuan,nama_profile,jk_profile,mengetahui_rs,ttl_profile,pendidikan_profile,lainnya_profile,umur_profile,pekerjaan_profile,tanggal_survey,kritik,hasil_penilaian,nilai_per_unsur,jumlah_jawaban,nrr_per_unsur,nrr_tertimbang,kepuasan_mas,flag_nilai_mas,hasil_flag
					,'$login_ppa_id',NOW(),alasan_edit_id,keterangan_edit FROM tpoliklinik_survey_kepuasan 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				$q="INSERT IGNORE INTO tpoliklinik_survey_kepuasan_param_his 
							(versi_edit,
							id,assesmen_id,param_id,msurvey_kepuasan_id,inisial,parameter_nama,jawaban_id,jawaban_skor,st_nilai
							)
					SELECT '$jml_edit',
							id,assesmen_id,param_id,msurvey_kepuasan_id,inisial,parameter_nama,jawaban_id,jawaban_skor,st_nilai
							FROM tpoliklinik_survey_kepuasan_param 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				$q="INSERT IGNORE INTO tpoliklinik_survey_kepuasan_param_skor_his 
							(versi_edit,
							id,assesmen_id,parameter_id,skor,deskripsi_nama,master_id
							)
					SELECT '$jml_edit',
							id,assesmen_id,parameter_id,skor,deskripsi_nama,master_id
							FROM tpoliklinik_survey_kepuasan_param_skor 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
			  
			}else{
				$q="DELETE FROM tpoliklinik_survey_kepuasan_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				$q="DELETE FROM tpoliklinik_survey_kepuasan_param_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				$q="DELETE FROM tpoliklinik_survey_kepuasan_param_skor_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				
			}
		  return $hasil;
		}
		
		function batal_survey_kepuasan(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $st_edited=$this->input->post('st_edited');
		  $jml_edit=$this->input->post('jml_edit');
		  $data=array(
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
			
		  );
		   if ($jml_edit=='0'){
			  $data['edited_ppa']=null;
			  $data['edited_date']=null;
			  $data['alasan_edit_id']='';
			  $data['keterangan_edit']='';
			  $data['st_edited']='0';
			  $data['jml_edit']='0';
			  
		  }else{
			  $jml_edit=$jml_edit-1;
			  $data['jml_edit']=$jml_edit;
		  }
		  if ($st_edited=='1'){
				$q="SELECT * FROM tpoliklinik_survey_kepuasan_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$data_his_edit=$this->db->query($q)->row();
				if ($data_his_edit){
					$data['edited_ppa']=$data_his_edit->edited_ppa;
					$data['edited_date']=$data_his_edit->edited_date;
					$data['jml_edit']=$data_his_edit->jml_edit;
					$data['alasan_edit_id']=$data_his_edit->alasan_edit_id;
					$data['keterangan_edit']=$data_his_edit->keterangan_edit;
					$data['total_skor_survey_kepuasan']=$data_his_edit->total_skor_survey_kepuasan;
					$data['hasil_penilaian']=$data_his_edit->hasil_penilaian;
					$data['nilai_per_unsur']=$data_his_edit->nilai_per_unsur;
					$data['jumlah_jawaban']=$data_his_edit->jumlah_jawaban;
					$data['nrr_per_unsur']=$data_his_edit->nrr_per_unsur;
					$data['nrr_tertimbang']=$data_his_edit->nrr_tertimbang;
					$data['kepuasan_mas']=$data_his_edit->kepuasan_mas;
					$data['flag_nilai_mas']=$data_his_edit->flag_nilai_mas;
					$data['hasil_flag']=$data_his_edit->hasil_flag;

				}
				$q="DELETE FROM tpoliklinik_survey_kepuasan_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				
				$q="DELETE FROM tpoliklinik_survey_kepuasan_param WHERE assesmen_id='$assesmen_id'";
				$this->db->query($q);
				
				$q="INSERT IGNORE INTO tpoliklinik_survey_kepuasan_param 
							(
							id,assesmen_id,param_id,msurvey_kepuasan_id,inisial,parameter_nama,jawaban_id,jawaban_skor,st_nilai
							)
					SELECT 
							id,assesmen_id,param_id,msurvey_kepuasan_id,inisial,parameter_nama,jawaban_id,jawaban_skor,st_nilai
							FROM tpoliklinik_survey_kepuasan_param_his
					WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
				$q="DELETE FROM tpoliklinik_survey_kepuasan_param_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				
				$q="DELETE FROM tpoliklinik_survey_kepuasan_param_skor WHERE assesmen_id='$assesmen_id' ";
				$this->db->query($q);
				
				$q="INSERT IGNORE INTO  tpoliklinik_survey_kepuasan_param_skor 
							(
							id,assesmen_id,parameter_id,skor,deskripsi_nama,master_id
							)
					SELECT 
							id,assesmen_id,parameter_id,skor,deskripsi_nama,master_id
							FROM tpoliklinik_survey_kepuasan_param_skor_his
					WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
				$q="DELETE FROM tpoliklinik_survey_kepuasan_param_skor_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				
			  $data['status_assemen']='2';
		  }else{
			  $data['status_assemen']='0';
			  
		  }
		  $this->db->where('assesmen_id',$assesmen_id);
		  $hasil=$this->db->update('tpoliklinik_survey_kepuasan',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
  function create_with_template_survey_kepuasan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
	  $q="INSERT INTO tpoliklinik_survey_kepuasan (
			template_assesmen_id,nama_template,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,idpasien,st_ranap,created_date,created_ppa,msurvey_kepuasan_id,status_assemen,
			ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,total_skor_survey_kepuasan,nama_kajian,isi_header,isi_footer,nilai_tertimbang,nilai_satuan,nama_profile,jk_profile,mengetahui_rs,ttl_profile,pendidikan_profile,lainnya_profile,umur_profile,pekerjaan_profile,tanggal_survey,kritik,hasil_penilaian,nilai_per_unsur,jumlah_jawaban,nrr_per_unsur,nrr_tertimbang,kepuasan_mas,flag_nilai_mas,hasil_flag
			)
			SELECT '$template_assesmen_id',nama_template,NOW(),'$pendaftaran_id','$pendaftaran_id_ranap','$idpasien','$st_ranap',NOW(),'$login_ppa_id',msurvey_kepuasan_id,1,
			ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,total_skor_survey_kepuasan,nama_kajian,isi_header,isi_footer,nilai_tertimbang,nilai_satuan,nama_profile,jk_profile,mengetahui_rs,ttl_profile,pendidikan_profile,lainnya_profile,umur_profile,pekerjaan_profile,tanggal_survey,kritik,hasil_penilaian,nilai_per_unsur,jumlah_jawaban,nrr_per_unsur,nrr_tertimbang,kepuasan_mas,flag_nilai_mas,hasil_flag
			FROM `tpoliklinik_survey_kepuasan`

			WHERE assesmen_id='$template_assesmen_id'";
	  $hasil=$this->db->query($q);
	 
	  $assesmen_id=$this->db->insert_id();
	  
	  $q="
			SELECT 
			id,assesmen_id,param_id,msurvey_kepuasan_id,inisial,parameter_nama,jawaban_id,jawaban_skor,st_nilai
			FROM `tpoliklinik_survey_kepuasan_param`

			WHERE assesmen_id='$template_assesmen_id'";
	  $list_param=$this->db->query($q)->result();
	   foreach($list_param as $row){
		   $x_cari=$row->id;
		  $data_param=array(
				'assesmen_id' => $assesmen_id,
				'param_id' => $row->param_id,
				'msurvey_kepuasan_id' => $row->msurvey_kepuasan_id,
				'inisial' => $row->inisial,
				'parameter_nama' => $row->parameter_nama,
				'st_nilai' => $row->st_nilai,
				'jawaban_id' => $row->jawaban_id,
				'jawaban_skor' => $row->jawaban_skor,
		  );
		  $this->db->insert('tpoliklinik_survey_kepuasan_param',$data_param);
		  $parameter_id=$this->db->insert_id();
		  $param_id=$this->db->insert_id();
		   $q="INSERT INTO tpoliklinik_survey_kepuasan_param_skor (
			assesmen_id,parameter_id,skor,deskripsi_nama,master_id
			)
			SELECT 
			'$assesmen_id','$parameter_id',skor,deskripsi_nama,master_id
			FROM `tpoliklinik_survey_kepuasan_param_skor`

			WHERE parameter_id='$x_cari'";
			$hasil=$this->db->query($q);
	  }
	 
	  
	  //Update jumlah_template
	  $q="UPDATE tpoliklinik_survey_kepuasan 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
   }
   function hapus_record_survey_kepuasan(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_survey_kepuasan',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_index_template_survey_kepuasan()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template,H.nama_kajian as singkatan_kajian
						 FROM tpoliklinik_survey_kepuasan H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.assesmen_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template) .' ('.$r->singkatan_kajian.')';
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }	
  function create_template_survey_kepuasan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	 $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $msurvey_kepuasan_id=$this->input->post('msurvey_kepuasan_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="
		SELECT H.isi_header,H.isi_footer,H.nilai_tertimbang,H.nilai_satuan,H.nama as nama_kajian FROM msurvey_kepuasan H WHERE H.id='$msurvey_kepuasan_id'
	  ";
	  $data_header=$this->db->query($q)->row();
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'msurvey_kepuasan_id' => $msurvey_kepuasan_id,
		'total_skor_survey_kepuasan' => 0,
		'created_ppa' => $login_ppa_id,
		'tanggal_survey' => date('Y-m-d H:i:s'),
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		'isi_header' => $data_header->isi_header,
		'isi_footer' => $data_header->isi_footer,
		'nilai_tertimbang' => $data_header->nilai_tertimbang,
		'nilai_satuan' => $data_header->nilai_satuan,
		'nama_kajian' => $data_header->nama_kajian,
		
	  );
	  
	  $hasil=$this->db->insert('tpoliklinik_survey_kepuasan',$data);
	  $assesmen_id=$this->db->insert_id();
	  // $q="
		// SELECT '$assesmen_id' as assesmen_id,H.id as param_id,H.msurvey_kepuasan_id
		// ,H.inisial,H.parameter_nama as parameter_nama,H.st_nilai
		// FROM msurvey_kepuasan_param H
		// WHERE H.msurvey_kepuasan_id='$msurvey_kepuasan_id'
	  // ";
	   $q="
		SELECT '$assesmen_id' as assesmen_id,H.id as param_id,H.msurvey_kepuasan_id
		,H.inisial,H.parameter_nama as parameter_nama,H.st_nilai
		,CASE WHEN D.id IS NOT NULL THEN D.id ELSE NULL END as jawaban_id
		,CASE WHEN D.id IS NOT NULL THEN D.skor ELSE 0 END as jawaban_skor
		FROM msurvey_kepuasan_param H
		LEFT JOIN msurvey_kepuasan_param_skor D ON D.parameter_id=H.id AND D.st_default='1'
		WHERE H.msurvey_kepuasan_id='$msurvey_kepuasan_id'
		GROUP BY H.id
	  ";
	  $list_param=$this->db->query($q)->result();
	  foreach($list_param as $row){
		  $data_param=array(
				'assesmen_id' => $row->assesmen_id,
				'param_id' => $row->param_id,
				'msurvey_kepuasan_id' => $row->msurvey_kepuasan_id,
				'inisial' => $row->inisial,
				'parameter_nama' => $row->parameter_nama,
				'st_nilai' => $row->st_nilai,
				'jawaban_skor' => $row->jawaban_skor,
				'jawaban_id' => $row->jawaban_id,
		  );
		  $parameter_id=$row->param_id;
		  $this->db->insert('tpoliklinik_survey_kepuasan_param',$data_param);
		  $param_id=$this->db->insert_id();
		  $q_insert="
			INSERT INTO tpoliklinik_survey_kepuasan_param_skor (assesmen_id,parameter_id,skor,deskripsi_nama,master_id)
			SELECT '$assesmen_id' as assesmen_id,'$param_id' as parameter_id,H.skor,H.deskripsi_nama,master_id FROM msurvey_kepuasan_param_skor H
			WHERE H.parameter_id='$parameter_id'
		  ";
		  $this->db->query($q_insert);
	  }
	  $this->output->set_output(json_encode($hasil));
    }
	
	function edit_template_survey_kepuasan(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'pendaftaran_id_ranap' => $this->input->post('pendaftaran_id_ranap'),
			'st_ranap' => $this->input->post('st_ranap'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_survey_kepuasan',$data);
		
		$this->output->set_output(json_encode($result));
  }
  
	
}	
