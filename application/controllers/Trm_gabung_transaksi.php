<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_gabung_transaksi extends CI_Controller
{

    /**
     * Gabung Transaksi controller.
     * Developer @GunaliRezqiMauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_gabung_transaksi_model', 'model');
    }

    function index($idpasien='')
    {
        $data = array();
        $data['error']     = '';
        $data['title']     = 'Gabung Transaksi';
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1196'))==false && UserAccesForm($user_acces_form,array('1194'))==true){
			redirect('trm_gabung_transaksi_history');
			
		}else{
		 $data['content']   = 'Trm_gabung_transaksi/manage';
		}
	
       
        $data['breadcrum'] = array(
					array("RSKB Halmahera",'#'),
          array("Gabung Transaksi",'#'),
          array("Tambah",'trm_gabung')
        );

        $data['idpasien']  = $idpasien;

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function save(){
			if($this->model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('trm_gabung_transaksi','location');
			}
  	}

    public function getTransaksi()
    {
      $tiperefund = $this->input->post('tiperefund');
      $tanggalawal = $this->input->post('tanggalawal');
      $tanggalakhir = $this->input->post('tanggalakhir');
      $idtipetransaksi = $this->input->post('idtipetransaksi');

      $data = array();
      $result = $this->model->getTransaksi($tanggalawal, $tanggalakhir, $idtipetransaksi);

      foreach ($result as $row) {
          $transaksi = array(
            'reference' => $row->reference,
      			'idtransaksi' => $row->idtransaksi,
      			'notransaksi' => $row->notransaksi,
            'idpasien' => $row->idpasien,
      			'namapasien' => $row->namapasien,
      			'nomedrec' => $row->nomedrec,
      			'tanggallahir' => $row->tanggallahir,
      			'jeniskelamin' => $row->jeniskelamin,
      			'alamatpasien' => $row->alamatpasien,
      			'tanggaltransaksi' => $row->tanggaltransaksi,
      			'jenistransaksi' => $row->tipetransaksi,
          );

          $rows = array();
          $rows[] = '<label class="label label-primary text-uppercase">' . $row->tipetransaksi. '</label>';;
          $rows[] = $row->notransaksi;
          $rows[] = $row->nomedrec;
          $rows[] = $row->namapasien;
          $rows[] = $row->namakelompokpasien;
          $rows[] = $row->namadokter;
          $rows[] = '<button class="btn btn-success btn-xs text-uppercase selectTransaksi" style="font-size:10px;" data-transaksi="'.encodeURIComponent(json_encode($transaksi)).'">Pilih Transaksi</button>';

          $data[] = $rows;
      }

      $output = array(
          "recordsTotal" => count($result),
          "recordsFiltered" => count($result),
          "data" => $data
      );

      echo json_encode($output);
    }
}
