<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Term_laboratorium_patologi_anatomi_permintaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Term_laboratorium_patologi_anatomi_permintaan_model', 'model');
        $this->load->model('Term_laboratorium_patologi_anatomi_model');
    }

    public function index($section_tab = '1', $status_pemeriksaan = '#'): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $data = [
                'tanggal' => date("d/m/Y"),
                'section_tab' => $section_tab,
                'status_pemeriksaan' => $status_pemeriksaan,
            ];

            $data['error'] = '';
            $data['title'] = 'Laboratorium';
            $data['content'] = 'Term_laboratorium_patologi_anatomi_permintaan/index';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Laboratorium', '#'],
                ['Permintaan', 'term_laboratorium_patologi_anatomi_permintaan'],
            ];

            $data['tujuan_laboratorium'] = $this->model->get_tujuan_laboratorium();
			$data['tujuan_laboratorium_array'] = implode(",", array_column($data['tujuan_laboratorium'], 'id'));

			$data['tujuan_pelayanan'] = implode(",", array_unique(array_column($data['tujuan_laboratorium'], 'tujuan')));

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function callAntrian(): void
    {
        $antrianId = $this->input->post('antrian_id');
        $tujuanPelayanan = $this->input->post('tujuan_pelayanan');

        $data = $this->db
            ->select('term_laboratorium_patologi_anatomi.id,
                tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                tpoliklinik_pendaftaran.kode_antrian,
                term_laboratorium_patologi_anatomi.nomor_antrian,
                term_laboratorium_patologi_anatomi.tujuan_pelayanan,
                DATE(term_laboratorium_patologi_anatomi.rencana_pemeriksaan) AS tanggal,
                tpoliklinik_pendaftaran.idpoliklinik,
                tpoliklinik_pendaftaran.iddokter')
            ->from('term_laboratorium_patologi_anatomi')
            ->join('trawatinap_pendaftaran', 'term_laboratorium_patologi_anatomi.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4)', 'LEFT')
            ->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2)', 'LEFT')
            ->join('trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT')
            ->where('term_laboratorium_patologi_anatomi.id', $antrianId)
            ->get()
            ->row();

        $poliklinikId = $data->idpoliklinik;
        $dokterId = $data->iddokter;
        $nomorAntrian = $data->nomor_antrian;

        $tujuanSoundQuery = $this->db
            ->select('GROUP_CONCAT(antrian_asset_sound.file_sound SEPARATOR ":") as file')
            ->from('mtujuan_sound')
            ->join('antrian_asset_sound', 'antrian_asset_sound.id = mtujuan_sound.sound_id')
            ->where('mtujuan_sound.tujuan_id', $tujuanPelayanan)
            ->where('mtujuan_sound.status', '1')
            ->order_by('mtujuan_sound.nourut', 'ASC')
            ->get();

        $fileRuangan = $tujuanSoundQuery->row('file');

        $userId = $this->session->userdata('user_id');
        $userNamaLogin = $this->session->userdata('user_name');

        if ($data) {
            $kodeNumber = str_pad($nomorAntrian, 3, "0", STR_PAD_LEFT);

            $dokterSoundQuery = $this->db
                ->select('GROUP_CONCAT(antrian_asset_sound.file_sound SEPARATOR ":") as file')
                ->from('antrian_poli_kode_dokter')
                ->join('antrian_asset_sound', 'antrian_asset_sound.id = antrian_poli_kode_dokter.sound_dokter_id')
                ->where('antrian_poli_kode_dokter.idpoli', $poliklinikId)
                ->where('antrian_poli_kode_dokter.iddokter', $dokterId)
                ->get();

            $fileDokter = $dokterSoundQuery->row('file') ?: '';

            $poliSoundQuery = $this->db
                ->select('GROUP_CONCAT(file_sound SEPARATOR \':\') as file')
                ->from('antrian_poli_kode_sound')
                ->join('antrian_asset_sound', 'antrian_asset_sound.id = antrian_poli_kode_sound.sound_id')
                ->where('antrian_poli_kode_sound.idpoli', $poliklinikId)
                ->where('antrian_poli_kode_sound.status', '1')
                ->order_by('antrian_poli_kode_sound.nourut', 'ASC')
                ->get();

            $filePoli = $poliSoundQuery->row('file') ?: '';

            if ($fileDokter) {
                $filePoli .= ':' . $fileDokter;
            }

            $fileTerbilang = terbilang_sound($kodeNumber);
            $soundPlay = "$filePoli:$fileTerbilang:$fileRuangan";

            $dataInsert=array(
				'transaksi_id' => $data->id,
				'tipe' => 1,
				'tujuan_antrian_id' => $tujuanPelayanan,
				'kode_antrian' => $data->kode_antrian,
				'noantrian' => $data->nomor_antrian,
				'sound_play' => $soundPlay,
				'tanggal' => date('Y-m-d'),
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $userId,
				'user_call_nama' => $userNamaLogin,
			);

            if ($this->db->insert('antrian_harian_khusus_call', $dataInsert)) {
                $dataUpdate = array(
                    'status_panggil' => 1,
                    'waktu_panggil' => date('Y-m-d H:i:s'),
                );

                $this->db->where('id', $antrianId);
                $this->db->update('term_laboratorium_patologi_anatomi', $dataUpdate);
            }

            $hasil = $dataInsert;
        } else {
            $hasil = false;
        }

        $this->output->set_output(json_encode($hasil));
    }

    public function callLewati(): void
    {
        $antrianIdNext = $this->input->post('antrian_id_next');
        $antrianIdSkip = $this->input->post('antrian_id');

        $dataUpdate = array(
            'status_panggil' => 2,
            'waktu_panggil' => date('Y-m-d H:i:s'),
        );

        $this->db->where('id', $antrianIdSkip);
        $this->db->update('term_laboratorium_patologi_anatomi', $dataUpdate);

        $data = $this->db
            ->select('term_laboratorium_patologi_anatomi.id,
                tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                tpoliklinik_pendaftaran.kode_antrian,
                term_laboratorium_patologi_anatomi.nomor_antrian,
                term_laboratorium_patologi_anatomi.tujuan_pelayanan,
                DATE(term_laboratorium_patologi_anatomi.rencana_pemeriksaan) AS tanggal,
                tpoliklinik_pendaftaran.idpoliklinik,
                tpoliklinik_pendaftaran.iddokter')
            ->from('term_laboratorium_patologi_anatomi')
            ->join('trawatinap_pendaftaran', 'term_laboratorium_patologi_anatomi.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4)', 'LEFT')
            ->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2)', 'LEFT')
            ->join('trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT')
            ->where('term_laboratorium_patologi_anatomi.id', $antrianIdNext)
            ->get()
            ->row();

            
        if ($data) {
            $poliklinikId = $data->idpoliklinik;
            $dokterId = $data->iddokter;
            $nomorAntrian = $data->nomor_antrian;
            $tujuanId = $this->input->post('tujuan_laboratorium');

            $tujuanSoundQuery = $this->db
                ->select('GROUP_CONCAT(antrian_asset_sound.file_sound SEPARATOR ":") as file')
                ->from('mtujuan_sound')
                ->join('antrian_asset_sound', 'antrian_asset_sound.id = mtujuan_sound.sound_id')
                ->where('mtujuan_sound.tujuan_id', $tujuanId)
                ->where('mtujuan_sound.status', '1')
                ->order_by('mtujuan_sound.nourut', 'ASC')
                ->get();

            $fileRuangan = $tujuanSoundQuery->row('file');

            $userId = $this->session->userdata('user_id');
            $userNamaLogin = $this->session->userdata('user_name');

            $kodeNumber = str_pad($nomorAntrian, 3, "0", STR_PAD_LEFT);

            $dokterSoundQuery = $this->db
                ->select('GROUP_CONCAT(antrian_asset_sound.file_sound SEPARATOR ":") as file')
                ->from('antrian_poli_kode_dokter')
                ->join('antrian_asset_sound', 'antrian_asset_sound.id = antrian_poli_kode_dokter.sound_dokter_id')
                ->where('antrian_poli_kode_dokter.idpoli', $poliklinikId)
                ->where('antrian_poli_kode_dokter.iddokter', $dokterId)
                ->get();

            $fileDokter = $dokterSoundQuery->row('file') ?: '';

            $poliSoundQuery = $this->db
                ->select('GROUP_CONCAT(antrian_asset_sound.file_sound SEPARATOR ":") as file')
                ->from('antrian_poli_kode_sound')
                ->join('antrian_asset_sound', 'antrian_asset_sound.id = antrian_poli_kode_sound.sound_id')
                ->where('antrian_poli_kode_sound.idpoli', $poliklinikId)
                ->where('antrian_poli_kode_sound.status', '1')
                ->order_by('antrian_poli_kode_sound.nourut', 'ASC')
                ->get();

            $filePoli = $poliSoundQuery->row('file') ?: '';

            if ($fileDokter) {
                $filePoli .= ':' . $fileDokter;
            }

            $fileTerbilang = terbilang_sound($kodeNumber);
            $soundPlay = "$filePoli:$fileTerbilang:$fileRuangan";

            $dataInsert=array(
				'transaksi_id' => $data->id,
				'tipe' => 1,
				'tujuan_antrian_id' => $tujuanId,
				'kode_antrian' => $data->kode_antrian,
				'noantrian' => $data->nomor_antrian,
				'sound_play' => $soundPlay,
				'tanggal' => date('Y-m-d'),
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $userId,
				'user_call_nama' => $userNamaLogin,
			);

            $this->db->insert('antrian_harian_khusus_call', $dataInsert);
            $hasil = $dataInsert;
        } else {
            $hasil = false;
        }

        $this->output->set_output(json_encode($hasil));
    }

    public function refreshDivCounter(): void
    {
        $tujuanPelayanan = $this->input->post('tujuan_pelayanan');
        $tujuanLaboratorium = $this->input->post('tujuan_laboratorium');
        
        $idNext = '0';
        $idLayani = '0';
        $divTabel = '';

        // Query untuk mendapatkan antrian yang sedang dilayani di tujuan laboratorium
        $this->db->select('term_laboratorium_patologi_anatomi.id,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.kode_antrian,
            term_laboratorium_patologi_anatomi.nomor_antrian');
        $this->db->from('mtujuan');
        $this->db->join('term_laboratorium_patologi_anatomi', 'term_laboratorium_patologi_anatomi.id = mtujuan.pendaftaran_dilayani');
        $this->db->join('trawatinap_pendaftaran', 'term_laboratorium_patologi_anatomi.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4)', 'LEFT');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2)', 'LEFT');
        $this->db->join('trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT');
        $this->db->where_in('mtujuan.id', $tujuanPelayanan);
        $this->db->where_in('term_laboratorium_patologi_anatomi.tujuan_laboratorium', $tujuanLaboratorium);
        $queryNext = $this->db->get();

        $dataNext = $queryNext->row_array();

        if ($dataNext) {
            $dataNext['button_disabled'] = '';
            $idLayani = $dataNext['id'];
        } else {
            $dataNext = array(
                'id' => '',
                'nama_pasien' => '-',
                'nomor_medrec' => '-',
                'kode_antrian' => '-',
                'nomor_antrian' => '',
                'button_disabled' => 'disabled',
            );
        }

        // Button Panggil Ulang Antrian
        $btnRecall = '<button class="btn btn-block btn-success" style="border-radius: 10px;" '.$dataNext['button_disabled'].' type="button" onclick="recallManual('.$dataNext['id'].')" title="Panggil Ulang"><i class="si si-refresh pull-left"></i> PANGGIL ULANG</button>';

        // Membuat bagian HTML untuk antrian yang sedang dilayani
        $divTabel .= '
            <div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-danger" style="border-bottom: 2px solid white; padding:10; border-radius: 10px;">
						<h3 class="block-title text-center">ANTRIAN DILAYANI</h3>
					</div>
					<div class="content-mini content-mini-full bg-danger" style="border-radius: 10px;">
						<div style="padding: 13px 0 13px 0;">
							<div class="h1 font-w700 text-center text-white">'.$dataNext['kode_antrian'].'</div>
							<div class="h4 text-center text-white">'.$dataNext['nomor_medrec'].'</div>
							<div class="h4 text-center text-white push-10">'.$dataNext['nama_pasien'].'</div>
						</div>
					</div>
				</div>
			</div>
        ';

        // Query untuk mendapatkan antrian selanjutnya di tujuan laboratorium
        $this->db->select('term_laboratorium_patologi_anatomi.id,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.kode_antrian,
            term_laboratorium_patologi_anatomi.nomor_antrian');
        $this->db->from('term_laboratorium_patologi_anatomi');
        $this->db->join('trawatinap_pendaftaran', 'term_laboratorium_patologi_anatomi.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4)', 'LEFT');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2)', 'LEFT');
        $this->db->join('trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT');
        $this->db->where_in('term_laboratorium_patologi_anatomi.tujuan_pelayanan', $tujuanPelayanan);
        $this->db->where_in('term_laboratorium_patologi_anatomi.tujuan_laboratorium', $tujuanLaboratorium);
        $this->db->where('term_laboratorium_patologi_anatomi.status_panggil', '0');
        $this->db->order_by('term_laboratorium_patologi_anatomi.rencana_pemeriksaan', 'ASC');
        $this->db->limit(1);
        $queryNext = $this->db->get();

        $dataNext = $queryNext->row_array();

        if ($dataNext) {
            $dataNext['button_disabled'] = '';
            $idNext = $dataNext['id'];
        } else {
            $dataNext = array(
                'id' => '',
                'nama_pasien' => '-',
                'nomor_medrec' => '-',
                'kode_antrian' => '-',
                'nomor_antrian' => '',
                'button_disabled' => 'disabled',
            );
        }

        // Membuat bagian HTML untuk antrian selanjutnya
        $divTabel .= '
            <div class="col-sm-3">
                <div class="block block-themed">
                    <div class="block-header bg-success" style="border-bottom: 2px solid white; padding: 10; border-radius: 10px;">
                        <h3 class="block-title text-center" >ANTRIAN SELANJUTNYA</h3>
                    </div>
                    <div class="content-mini content-mini-full bg-success" style="border-radius: 10px;">
                        '. ($dataNext['kode_antrian'] == '-' ? '<div style="padding: 21px 0 21px 0;">
                            <div class="h1 font-w700 text-center text-white">-</div>
                        </div>' : '<div>
                            <div class="h2 text-center text-white">'.$dataNext['kode_antrian'].'</div>
                            <div class="h5 text-center text-white">'.$dataNext['nomor_medrec'].'</div>
                            <div class="h5 text-center text-white push-10">'.$dataNext['nama_pasien'].'</div>
                        </div>') . '
                    </div>
                    <div class="text-center" style="margin-top: 10px">
                        <button class="btn btn-block btn-danger" type="button" style="border-radius: 10px;" onclick="callAntrianManual('.$dataNext['id'].')" '.$dataNext['button_disabled'].' title="Lanjut Panggil" data-toggle="modal" data-target="#modal-call-antrian"><i class="glyphicon glyphicon-volume-up pull-left"></i> '.($idLayani=='0'?'PANGGIL':'ANTRIAN SELANJUTNYA').'</button>
                    </div>
                </div>
            </div>
        ';

        // Query untuk mendapatkan total antrian dan sisa antrian di tujuan laboratorium
        $this->db->select("SUM(1) as total_antrian,
            SUM(CASE WHEN term_laboratorium_patologi_anatomi.status_panggil = '0' THEN 1 ELSE 0 END) as sisa_antrian");
        $this->db->from('term_laboratorium_patologi_anatomi');
        $this->db->join('trawatinap_pendaftaran', 'term_laboratorium_patologi_anatomi.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4)', 'LEFT');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2)', 'LEFT');
        $this->db->join('trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT');
        $this->db->where_in('term_laboratorium_patologi_anatomi.tujuan_pelayanan', $tujuanPelayanan);
        $this->db->where_in('term_laboratorium_patologi_anatomi.tujuan_laboratorium', $tujuanLaboratorium);
        $this->db->order_by('term_laboratorium_patologi_anatomi.rencana_pemeriksaan', 'ASC');
        $queryTotal = $this->db->get();

        $dataNext = $queryTotal->row_array();

        // Menentukan apakah button harus dinonaktifkan
        $buttonDisable = ($idLayani == '0' || $dataNext['sisa_antrian'] == '0') ? 'disabled' : '';

        // Membuat bagian HTML untuk sisa antrian
        $divTabel .= '<div class="col-sm-3">
            <div class="block block-themed">
                <div class="block-header bg-success" style="border-bottom: 2px solid white; padding: 10; border-radius: 10px;">
                    <h3 class="block-title text-center">SISA ANTRIAN</h3>
                </div>
                <div class="content-mini content-mini-full bg-success" style="border-radius: 10px;">
                    <div style="padding: 21px 0 21px 0;">
                        <div class="h1 font-w700 text-center text-white">'.$dataNext['sisa_antrian'].'</div>
                    </div>
                </div>
                <div class="text-center" style="margin-top: 10px">
                    '. $btnRecall .'
                </div>
            </div>
        </div>';

        // Menambahkan bagian HTML untuk total antrian
        $divTabel .= '<div class="col-sm-3">
            <div class="block block-themed">
                <div class="block-header bg-success" style="border-bottom: 2px solid white; padding: 10; border-radius: 10px;">
                    <h3 class="block-title text-center">TOTAL ANTRIAN</h3>
                </div>
                <div class="content-mini content-mini-full bg-success" style="border-radius: 10px;">
                    <div style="padding: 21px 0 21px 0;">
                        <div class="h1 font-w700 text-center text-white">'.$dataNext['total_antrian'].'</div>
                    </div>
                </div>
                <div class="text-center" style="margin-top: 10px">
                    <button class="btn btn-block btn-warning" style="border-radius: 10px;" type="button" '.$buttonDisable.' onclick="callLewati('.$idLayani.','.$idNext.')"  title="LEWATI"><i class="fa fa-forward pull-left"></i> LEWATI ANTRIAN</button>
                </div>
            </div>
        </div>';

        // Mengirimkan output dalam format JSON
        $this->output->set_output(json_encode($divTabel));
    }

    public function getInformasiAntrian()
    {
        $antrianId = $this->input->post('antrian_id');
        $tujuanPelayanan = $this->input->post('tujuan_pelayanan');
        $tujuanLaboratorium = $this->input->post('tujuan_laboratorium');

        $data = $this->db
            ->select('term_laboratorium_patologi_anatomi.id AS antrian_id,
                tpoliklinik_pendaftaran.kode_antrian,
                tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                tpoliklinik_pendaftaran.title AS title_pasien,
                tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                term_laboratorium_patologi_anatomi.nomor_laboratorium,
                merm_referensi.ref AS jenis_kelamin,
                tpoliklinik_pendaftaran.tanggal_lahir,
                tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
                tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
                tpoliklinik_pendaftaran.umurhari AS umur_hari,
                tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                (CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2) THEN
                        "rawat_jalan"
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4) THEN
                        "rawat_inap"
                END) AS asal_rujukan,
                (CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2) THEN
                        tpoliklinik_pendaftaran.nopendaftaran
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4) THEN
                        trawatinap_pendaftaran.nopendaftaran
                END) AS nomor_pendaftaran,
                (CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2) THEN
                        tpoliklinik_pendaftaran.id
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4) THEN
                        trawatinap_pendaftaran.id
                END) AS pendaftaran_id,
                "" AS antrian_id_next')
            ->from('term_laboratorium_patologi_anatomi')
            ->join('trawatinap_pendaftaran', 'term_laboratorium_patologi_anatomi.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4)', 'LEFT')
            ->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2)', 'LEFT')
            ->join('trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT')
            ->join('merm_referensi', 'merm_referensi.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND merm_referensi.ref_head_id = 1')
            ->where('term_laboratorium_patologi_anatomi.id', $antrianId)
            ->get()
            ->row();

        // Query untuk mendapatkan antrian selanjutnya di tujuan laboratorium
        $this->db->select('term_laboratorium_patologi_anatomi.id,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.kode_antrian,
            term_laboratorium_patologi_anatomi.nomor_antrian');
        $this->db->from('term_laboratorium_patologi_anatomi');
        $this->db->join('trawatinap_pendaftaran', 'term_laboratorium_patologi_anatomi.pendaftaran_id = trawatinap_pendaftaran.id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (3, 4)', 'LEFT');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id AND term_laboratorium_patologi_anatomi.asal_rujukan IN (1, 2)', 'LEFT');
        $this->db->join('trawatinap_pendaftaran trawatinap', 'trawatinap.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT');
        $this->db->where_in('term_laboratorium_patologi_anatomi.tujuan_pelayanan', $tujuanPelayanan);
        $this->db->where_in('term_laboratorium_patologi_anatomi.tujuan_laboratorium', $tujuanLaboratorium);
        $this->db->where('term_laboratorium_patologi_anatomi.status_panggil', '0');
        $this->db->order_by('term_laboratorium_patologi_anatomi.rencana_pemeriksaan', 'ASC');
        $this->db->limit(1);
        $queryNext = $this->db->get();

        $dataNext = $queryNext->row_array();

        $result = [
            'data' => $data,
            'antrian_id_next' => $dataNext ? $dataNext['id'] : 0,
        ];

        $this->output->set_output(json_encode($result));
    }

    public function getNotif()
    {
        $tujuanLaboratorium = $this->input->post('tujuan_laboratorium');
        
        $this->db->select('GROUP_CONCAT(term_laboratorium_patologi_anatomi.id) AS id,
            GROUP_CONCAT(term_laboratorium_patologi_anatomi.sound_play SEPARATOR ":") AS sound_play');
        $this->db->from('term_laboratorium_patologi_anatomi');
        $this->db->where('term_laboratorium_patologi_anatomi.status_pemeriksaan', '2');
        $this->db->where('term_laboratorium_patologi_anatomi.status_notif_sound', '0');
        $this->db->where('term_laboratorium_patologi_anatomi.sound_play !=', '');
        $this->db->where_in('tujuan_laboratorium', $tujuanLaboratorium);
        
        $result = $this->db->get()->row_array();

        $this->output->set_output(json_encode($result));
    }

    public function updateNotif()
    {
        $antrianId = $this->input->post('antrian_id');

        $this->db->set('term_laboratorium_patologi_anatomi.status_notif_sound', '1');
        $this->db->where_in('term_laboratorium_patologi_anatomi.id', $antrianId);
        $result = $this->db->update('term_laboratorium_patologi_anatomi');

        $this->output->set_output(json_encode($result));
    }


    public function getIndex(): void
    {
        $nomor_medrec = $this->input->post('nomor_medrec');
        $nama_pasien = $this->input->post('nama_pasien');
        $tujuan_laboratorium = $this->input->post('tujuan_laboratorium');
        $asal_pasien = $this->input->post('asal_pasien');
        $dokter_peminta = $this->input->post('dokter_peminta');
        $kelompok_pasien = $this->input->post('kelompok_pasien');
        $tanggal_dari = YMDFormat($this->input->post('tanggal_dari'));
        $tanggal_sampai = YMDFormat($this->input->post('tanggal_sampai'));
        $nomor_pendaftaran = $this->input->post('nomor_pendaftaran');
        $nomor_permintaan = $this->input->post('nomor_permintaan');
        $status_antrian = $this->input->post('status_antrian');
        $status_tindakan = $this->input->post('status_tindakan');
        
        $where = '';
        if ('' != $nomor_medrec) {
            $where .= " AND ((tpoliklinik_pendaftaran.no_medrec) = '{$nomor_medrec}' OR (poliklinik_ranap.nomor_medrec) = '{$nomor_medrec}')";
        }
        if ('' != $nama_pasien) {
            $where .= " AND ((tpoliklinik_pendaftaran.namapasien) LIKE '%".$nama_pasien."%' OR (poliklinik_ranap.nama_pasien) LIKE '%".$nama_pasien."%')";
        }
        if ('0' != $tujuan_laboratorium) {
            $where .= " AND (term_laboratorium_patologi_anatomi.tujuan_laboratorium) = '{$tujuan_laboratorium}'";
        }
        if ('0' != $asal_pasien) {
            $where .= " AND (tpoliklinik_pendaftaran.idtipe) = '{$asal_pasien}'";
        }
        if ('0' != $kelompok_pasien) {
            $where .= " AND (tpoliklinik_pendaftaran.idkelompokpasien) = '{$kelompok_pasien}'";
        }
        if ('0' != $dokter_peminta) {
            $where .= " AND (term_laboratorium_patologi_anatomi.dokter_peminta_id) = '{$dokter_peminta}'";
        }
        if ('' != $tanggal_dari) {
            $where .= " AND DATE(term_laboratorium_patologi_anatomi.rencana_pemeriksaan) >= '{$tanggal_dari}'";
        }
        if ('' != $tanggal_sampai) {
            $where .= " AND DATE(term_laboratorium_patologi_anatomi.rencana_pemeriksaan) <= '{$tanggal_sampai}'";
        }
        if ('' != $nomor_pendaftaran) {
            $where .= " AND (tpoliklinik_pendaftaran.nopendaftaran) = '{$nomor_pendaftaran}'";
        }
        if ('' != $nomor_permintaan) {
            $where .= " AND (term_laboratorium_patologi_anatomi.nomor_permintaan) = '{$nomor_permintaan}'";
        }
        if ('#' != $status_antrian) {
            $where .= " AND (term_laboratorium_patologi_anatomi.status_panggil) = '{$status_antrian}'";
        }
        if ('#' != $status_tindakan) {
            $where .= " AND (term_laboratorium_patologi_anatomi.status_pemeriksaan) = '{$status_tindakan}'";
        }

        $this->select = [];
        $from = "
            ( SELECT
            term_laboratorium_patologi_anatomi.id,
            term_laboratorium_patologi_anatomi.asal_rujukan,
            (
                CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 1 THEN
                    'Poliklinik' 
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 2 THEN
                    'Instalasi Gawat Darurat (IGD)' 
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 3 THEN
                    'Rawat Inap' 
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 4 THEN
                    'One Day Surgery (ODS)' 
                END 
            ) AS asal_pasien,
            (
                CASE
                
                WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 ) THEN
                tpoliklinik_pendaftaran.nopendaftaran 
                WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 ) THEN
                poliklinik_ranap.nopendaftaran 
            END 
            ) AS nomor_pendaftaran,
            (
                CASE    
                WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 ) THEN
                tpoliklinik_pendaftaran.id 
                WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 ) THEN
                poliklinik_ranap.id 
            END 
            ) AS pendaftaran_id,
            term_laboratorium_patologi_anatomi.nomor_permintaan,
            term_laboratorium_patologi_anatomi.rencana_pemeriksaan,
            term_laboratorium_patologi_anatomi.nomor_antrian,
            COALESCE ( tpoliklinik_pendaftaran.kode_antrian, poliklinik_ranap.kode_antrian) AS kode_antrian,
            COALESCE ( tpoliklinik_pendaftaran.no_medrec, poliklinik_ranap.nomor_medrec) AS nomor_medrec,
            COALESCE ( tpoliklinik_pendaftaran.idpasien, poliklinik_ranap.pasien_id) AS pasien_id,
            COALESCE ( tpoliklinik_pendaftaran.namapasien, poliklinik_ranap.nama_pasien) AS nama_pasien,
            COALESCE ( mpasien_kelompok.nama, poliklinik_ranap.kelompok_pasien) AS kelompok_pasien,
            term_laboratorium_patologi_anatomi.status_panggil,
            term_laboratorium_patologi_anatomi.status_pemeriksaan,
            term_laboratorium_patologi_anatomi.jumlah_edit,
            SUM( term_laboratorium_patologi_anatomi_pemeriksaan.totalkeseluruhan ) AS total_tarif,
            tkasir.status AS statuskasir,
            poliklinik_ranap.id AS idpendaftaran_ranap,
            COALESCE ( tpoliklinik_pendaftaran.id, poliklinik_ranap.idpendaftaran_rajal ) AS idpendaftaran_rajal
        FROM
            term_laboratorium_patologi_anatomi
            LEFT JOIN (
                SELECT
                    trawatinap_pendaftaran.id,
                    trawatinap_pendaftaran.nopendaftaran,
                    tpoliklinik_pendaftaran.kode_antrian,
                    tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                    tpoliklinik_pendaftaran.idpasien AS pasien_id,
                    tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                    mpasien_kelompok.nama AS kelompok_pasien,
                    tpoliklinik_pendaftaran.id AS idpendaftaran_rajal
                FROM
                    trawatinap_pendaftaran
                    INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
                    INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
            ) AS poliklinik_ranap ON term_laboratorium_patologi_anatomi.pendaftaran_id = poliklinik_ranap.id 
            AND term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 )
            LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id 
            AND term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 )
            LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
            LEFT JOIN merm_pengaturan_tujuan_laboratorium ON merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_patologi_anatomi.tujuan_laboratorium
            LEFT JOIN term_laboratorium_patologi_anatomi_pemeriksaan ON term_laboratorium_patologi_anatomi_pemeriksaan.transaksi_id = term_laboratorium_patologi_anatomi.id 
            LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
            LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN (1,2)
        WHERE
            term_laboratorium_patologi_anatomi.jenis_pemeriksaan = '1' 
            AND term_laboratorium_patologi_anatomi.status_pemeriksaan != '1' 
            AND merm_pengaturan_tujuan_laboratorium.tipe_layanan = '2' ".$where.'
        GROUP BY
            term_laboratorium_patologi_anatomi.id 
        ) AS tbl 
        WHERE
        tbl.id IS NOT NULL
        ';
        
        $this->from = $from;
        $this->join = [];

        $this->order = [
			'tbl.nomor_antrian' => 'ASC'
        ];
        $this->group = [];
        $this->column_search = ['nomor_permintaan'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];
            
            $asal_rujukan = (in_array($r->asal_rujukan, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');

            $result[] = $no;
            $result[] = $r->asal_pasien;
            $result[] = $r->nomor_pendaftaran;
            $result[] = $r->nomor_permintaan;
            $result[] = HumanDateShort($r->rencana_pemeriksaan);
            $result[] = $r->nomor_antrian;
            $result[] = $r->kode_antrian;
            $result[] = $r->nomor_medrec;
            $result[] = $r->nama_pasien;
            $result[] = $r->kelompok_pasien;
            $result[] = StatusERMAntrian($r->status_panggil);
            $result[] = StatusERMTindakan($r->status_pemeriksaan);
            $result[] = '<div class="btn-group">
                            <a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs" onclick="prosesTransaksi(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')"><i class="fa fa-check-circle"></i></a>
                            <a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editPermintaan(\'' . $asal_rujukan . '\',' . $r->pendaftaran_id . ', ' . $r->id . ')"><i class="fa fa-pencil"></i></a>
                            <a href="#" data-toggle="tooltip" title="Batal Transaksi" class="btn btn-danger btn-xs" onclick="batalTransaksi(' . $r->id . ')"><i class="fa fa-trash"></i></a>
                            <a href="#" data-toggle="tooltip" title="Panggil Antrian" class="btn btn-primary btn-xs" onclick="callAntrianManual('.$r->id.')"><i class="glyphicon glyphicon-volume-up"></i></a>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						        <span class="fa fa-ellipsis-v"></span>
					        </button>
					        <ul class="dropdown-menu pull-right">
                                <li class=""><a href="#" onclick="prosesTransaksi(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')">Proses Transaksi</a></li>
                                <li class=""><a href="#" onclick="editPermintaan(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')">Tambah Permintaan</a></li>
                                <li class=""><a href="#" onclick="lihatDataPermintaan(' . $r->id . ')" data-toggle="modal" data-target="#modal-data-permintaan">Data Permintaan</a></li>
                                <li class=""><a href="#" onclick="riwayatLaboratorium(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ')">Riwayat Laboratorium</a></li>
                                <li class=""><a href="#" onclick="splitPermintaan(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')">Split Permintaan</a></li>
                                <li class=""><a href="#" onclick="gabungPermintaan(' . $r->pendaftaran_id . ', ' . $r->id . ', ' . $r->pasien_id . ')">Gabung Permintaan</a></li>
                                <li class=""><a href="#" onclick="dataLaboratorium(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')">Data Laboratorium</a></li>
                                <li class=""><a href="#" onclick="stikerIdentitas(' . $r->id .')">Stiker Identitas</a></li>
                                <li class=""><a href="#" onclick="labelTabungDarah(' . $r->id .')">Label Tabung Darah</a></li>
                                '.($r->statuskasir != 2 ? '<li class=""><a href="#" data-toggle="modal" data-target="#modal-ubah-kelas-tarif" data-transaksi-id="' . $r->id . '" data-total-tarif="' . $r->total_tarif . '" id="ubah-kelas-tarif">Ubah Kelas Tarif</a></li>' : '').'
                                <li class=""><a href="#" onclick="inputBMHPTagihan(' . $r->pendaftaran_id .')">Input BMHP Tagihan</a></li>
                                <li class=""><a href="#" onclick="inputBMHPNonTagihan(' . $r->pendaftaran_id .')">Input BMHP Non Tagihan</a></li>
                                <li class=""><a href="#" data-toggle="modal" data-target="#modal-ubah-tujuan-laboratorium" data-transaksi-id="' . $r->id . '" id="ubah-tujuan-laboratorium">Ubah Tujuan Laboratorium</a></li>
                                '.($r->idpendaftaran_rajal ? '<li class=""><a href="' . site_url().'tpendaftaran_poli_ttv/tindakan/' . $r->idpendaftaran_rajal . '/erm_rj" target="_blank">ERM Rawat Jalan</a></li>' : ''). '
                                '.($r->idpendaftaran_ranap ? '<li class=""><a href="' . site_url().'tpendaftaran_ranap_erm/tindakan/' . $r->idpendaftaran_ranap . '/erm_ri" target="_blank">ERM Rawat Inap</a></li>' : ''). '
                            </ul>
				        </div>';

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexTambahBaru(): void
    {
        $jenis = $this->input->post('jenis');
        $asal_pasien = $this->input->post('asal_pasien');
        $dokter_id = $this->input->post('dokter');
        $nomor_medrec = $this->input->post('medrec');
        $tanggal_dari = YMDFormat($this->input->post('tanggal_dari'));
        $tanggal_sampai = YMDFormat($this->input->post('tanggal_sampai'));
        $status_permintaan = $this->input->post('status_permintaan');
        $klinik_kelas = $this->input->post('klinik_kelas');
        $kelompok_pasien = $this->input->post('kelompok_pasien');
        $nama_pasien = $this->input->post('nama_pasien');
        
        $where = '';
        if ('0' != $jenis) {
            $where .= " AND (table_pendaftaran.jenis) = '{$jenis}'";
        }
        if ('0' != $asal_pasien) {
            $where .= " AND (table_pendaftaran.asal_pasien) = '{$asal_pasien}'";
        }
        if ('0' != $dokter_id) {
            $where .= " AND (table_pendaftaran.dokter_id) = '{$dokter_id}'";
        }
        if ('' != $nomor_medrec) {
            $where .= " AND (table_pendaftaran.nomor_medrec) = '{$nomor_medrec}'";
        }
        if ('' != $tanggal_dari) {
            $where .= " AND DATE(table_pendaftaran.tanggal_daftar) >= '{$tanggal_dari}'";
        }
        if ('' != $tanggal_sampai) {
            $where .= " AND DATE(table_pendaftaran.tanggal_daftar) <= '{$tanggal_sampai}'";
        }
        if ('0' != $status_permintaan) {
            $where .= " AND (table_pendaftaran.status_permintaan) = '{$status_permintaan}'";
        }
        if ('0' != $klinik_kelas) {
            $where .= " AND (table_pendaftaran.klinik_kelas) = '{$klinik_kelas}'";
        }
        if ('0' != $kelompok_pasien) {
            $where .= " AND (table_pendaftaran.kelompok_pasien_id) = '{$kelompok_pasien}'";
        }
        if ('' != $nama_pasien) {
            $where .= " AND (table_pendaftaran.nama_pasien) LIKE '%".$nama_pasien."%'";
        }

        $this->select = [];
        
        $from = "
                (
                SELECT
                    tpoliklinik_pendaftaran.id AS pendaftaran_id,
                    IF( term_laboratorium_patologi_anatomi.id, 1, 2 ) AS status_permintaan,
                    IF(tpoliklinik_tindakan.statusredirect, 1, 2) AS jenis,
                    tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_daftar,
                    ( CASE WHEN tpoliklinik_pendaftaran.idtipe = 1 THEN '1' WHEN tpoliklinik_pendaftaran.idtipe = 2 THEN '2' END ) AS asal_pasien,
                    tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
                    tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                    tpoliklinik_pendaftaran.idpasien AS pasien_id,
                    tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                    mpasien_kelompok.id AS kelompok_pasien_id,
                    mpasien_kelompok.nama AS kelompok_pasien,
                    mdokter.id AS dokter_id,
                    mpoliklinik.id AS klinik_kelas,
                    CONCAT( mpoliklinik.nama, ' - ', mdokter.nama ) AS detail,
                    tkasir.STATUS AS status_kasir 
                FROM
                    tpoliklinik_pendaftaran
                    LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
                    LEFT JOIN term_laboratorium_patologi_anatomi ON tpoliklinik_pendaftaran.idpasien = term_laboratorium_patologi_anatomi.pasien_id AND term_laboratorium_patologi_anatomi.status_pemeriksaan = 2
                    LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
                    LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
                    LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
                    LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND (tkasir.STATUS IS NULL OR tkasir.STATUS = 1)
                    AND tkasir.idtipe IN ( 1, 2 ) 
                WHERE tkasir.status != '2'
                    
                UNION ALL

                SELECT
                    trawatinap_pendaftaran.id AS pendaftaran_id,
                    IF( term_laboratorium_patologi_anatomi.id, 1, 2 ) AS status_permintaan,
                    2 AS jenis,
                    trawatinap_pendaftaran.tanggaldaftar AS tanggal_daftar,
                    ( CASE WHEN trawatinap_pendaftaran.idtipe = 1 THEN '3' WHEN trawatinap_pendaftaran.idtipe = 2 THEN '4' END ) AS asal_pasien,
                    trawatinap_pendaftaran.nopendaftaran AS nomor_pendaftaran,
                    tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                    tpoliklinik_pendaftaran.idpasien AS pasien_id,
                    tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                    mpasien_kelompok.id AS kelompok_pasien_id,
                    mpasien_kelompok.nama AS kelompok_pasien,
                    mdokter.id AS dokter_id,
                    trawatinap_pendaftaran.idkelas AS klinik_kelas,
                    CONCAT( mpoliklinik.nama, ' - ', mdokter.nama ) AS detail,
                    tkasir.STATUS AS status_kasir 
                FROM
                    trawatinap_pendaftaran
                    LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
                    LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
                    LEFT JOIN term_laboratorium_patologi_anatomi ON trawatinap_pendaftaran.idpasien = term_laboratorium_patologi_anatomi.pasien_id AND term_laboratorium_patologi_anatomi.status_pemeriksaan = 2
                    LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
                    LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
                    LEFT JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
                    LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND (tkasir.STATUS IS NULL OR tkasir.STATUS = 1)
                    AND tkasir.idtipe IN ( 1, 2 ) 
                WHERE
                    (trawatinap_pendaftaran.statuskasir != 1 OR trawatinap_pendaftaran.statuspembayaran != 1)
                ) AS table_pendaftaran
                WHERE table_pendaftaran.pendaftaran_id IS NOT NULL ".$where."
            ";
        
        $this->from = $from;
        $this->join = [];

        $this->order = [
			'table_pendaftaran.tanggal_daftar' => 'DESC'
		];
		$this->group = ['table_pendaftaran.pendaftaran_id', 'table_pendaftaran.asal_pasien'];
        $this->column_search = ['nomor_permintaan'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];
            $result[] = '<div class="btn-group">
                            <a href="#" data-toggle="tooltip" title="Tambah Permintaan" class="btn btn-success btn-xs" onclick="buatDraftPermintaan(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ', ' . $r->asal_pasien . ')"><i class="fa fa-plus"></i></a>
                            '. ($r->status_permintaan == 1 ? '<a href="#" data-toggle="tooltip" title="List" class="btn btn-warning btn-xs" onclick="planOrder(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ', ' . $r->asal_pasien . ')"><i class="fa fa-list"></i></a>' : '') .
                        '</div>';
            $result[] = GetStatusPermintaan($r->status_permintaan);
            $result[] = GetJenisDirect($r->jenis);
            $result[] = HumanDateLong($r->tanggal_daftar);
            $result[] = GetAsalPasien($r->asal_pasien);
            $result[] = $r->nomor_pendaftaran;
            $result[] = $r->nomor_medrec;
            $result[] = $r->nama_pasien;
            $result[] = $r->kelompok_pasien;
            $result[] = $r->detail;
            
            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexTambahRanap(): void
    {
        $jenis = $this->input->post('jenis');
        $asal_pasien = $this->input->post('asal_pasien');
        $dokter_id = $this->input->post('dokter');
        $nomor_medrec = $this->input->post('medrec');
        $tanggal_dari = YMDFormat($this->input->post('tanggal_dari'));
        $tanggal_sampai = YMDFormat($this->input->post('tanggal_sampai'));
        $status_permintaan = $this->input->post('status_permintaan');
        $klinik_kelas = $this->input->post('klinik_kelas');
        $kelompok_pasien = $this->input->post('kelompok_pasien');
        $nama_pasien = $this->input->post('nama_pasien');
        
        $where = '';
        if ('0' != $jenis) {
            $where .= " AND (table_pendaftaran.jenis) = '{$jenis}'";
        }
        if ('0' != $asal_pasien) {
            $where .= " AND (table_pendaftaran.asal_pasien) = '{$asal_pasien}'";
        }
        if ('0' != $dokter_id) {
            $where .= " AND (table_pendaftaran.dokter_id) = '{$dokter_id}'";
        }
        if ('' != $nomor_medrec) {
            $where .= " AND (table_pendaftaran.nomor_medrec) = '{$nomor_medrec}'";
        }
        if ('' != $this->input->post('tanggal_dari')) {
            $where .= " AND DATE(table_pendaftaran.tanggal_daftar) >= '{$tanggal_dari}'";
        }
        if ('' != $this->input->post('tanggal_sampai')) {
            $where .= " AND DATE(table_pendaftaran.tanggal_daftar) <= '{$tanggal_sampai}'";
        }
        if ('0' != $status_permintaan) {
            $where .= " AND (table_pendaftaran.status_permintaan) = '{$status_permintaan}'";
        }
        if ('0' != $klinik_kelas) {
            $where .= " AND (table_pendaftaran.klinik_kelas) = '{$klinik_kelas}'";
        }
        if ('0' != $kelompok_pasien) {
            $where .= " AND (table_pendaftaran.kelompok_pasien_id) = '{$kelompok_pasien}'";
        }
        if ('' != $nama_pasien) {
            $where .= " AND (table_pendaftaran.nama_pasien) LIKE '%".$nama_pasien."%'";
        }

        $this->select = [];
        
        $from = "
                (
                SELECT
                    trawatinap_pendaftaran.id AS pendaftaran_id,
                    IF ( term_laboratorium_patologi_anatomi.id, 1, 2 ) AS status_permintaan,
                    2 AS jenis,
                    trawatinap_pendaftaran.tanggaldaftar AS tanggal_daftar,
                    ( CASE WHEN trawatinap_pendaftaran.idtipe = 1 THEN '3' WHEN trawatinap_pendaftaran.idtipe = 2 THEN '4' END ) AS asal_pasien,
                    trawatinap_pendaftaran.nopendaftaran AS nomor_pendaftaran,
                    tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                    tpoliklinik_pendaftaran.idpasien AS pasien_id,
                    tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                    mpasien_kelompok.id AS kelompok_pasien_id,
                    mpasien_kelompok.nama AS kelompok_pasien,
                    mdokter.id AS dokter_id,
                    trawatinap_pendaftaran.idkelas AS klinik_kelas,
                    CONCAT( mpoliklinik.nama, ' - ', mdokter.nama ) AS detail,
                    tkasir.STATUS AS status_kasir 
                FROM
                    trawatinap_pendaftaran
                    LEFT JOIN term_laboratorium_patologi_anatomi ON trawatinap_pendaftaran.idpasien = term_laboratorium_patologi_anatomi.pasien_id AND term_laboratorium_patologi_anatomi.status_pemeriksaan = 2
                    LEFT JOIN trawatinap_pendaftaran_dpjp ON trawatinap_pendaftaran_dpjp.pendaftaran_id = trawatinap_pendaftaran.id
                    LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
                    LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
                    LEFT JOIN mruangan ON mruangan.id = trawatinap_pendaftaran.idruangan
                    LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
                    LEFT JOIN antrian_harian ON antrian_harian.id = tpoliklinik_pendaftaran.antrian_id
                    LEFT JOIN mdokter ON mdokter.id = iddokterpenanggungjawab
                    LEFT JOIN mpoliklinik ON mpoliklinik.id = trawatinap_pendaftaran.idpoliklinik_asal
                    LEFT JOIN merm_referensi ON merm_referensi.ref_head_id = '1' 
                    AND merm_referensi.nilai = trawatinap_pendaftaran.jenis_kelamin
                    LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
                    LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
                    LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
                    LEFT JOIN musers ON musers.id = trawatinap_pendaftaran.deleted_by
                    LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND ( tkasir.STATUS IS NULL OR tkasir.STATUS = 1 ) AND tkasir.idtipe IN ( 1, 2 )
                    LEFT JOIN (
                    SELECT
                        trawatinap_tindakan_pembayaran.idtindakan AS pendaftaran_id,
                        trawatinap_tindakan_pembayaran.status_verifikasi AS status_verifikasi_bayar,
                        trawatinap_tindakan_pembayaran_detail.st_verifikasi_piutang 
                    FROM
                        trawatinap_tindakan_pembayaran
                        LEFT JOIN trawatinap_tindakan_pembayaran_detail ON trawatinap_tindakan_pembayaran_detail.idtindakan = trawatinap_tindakan_pembayaran.id 
                        AND trawatinap_tindakan_pembayaran_detail.st_verifikasi_piutang = 1 
                        AND trawatinap_tindakan_pembayaran_detail.idmetode IN ( 6, 8 ) 
                    WHERE
                        trawatinap_tindakan_pembayaran.statusbatal = '0' 
                    GROUP BY
                        trawatinap_tindakan_pembayaran.id 
                    ) tindakan_pembayaran ON tindakan_pembayaran.pendaftaran_id = trawatinap_pendaftaran.id 
                WHERE
                    trawatinap_pendaftaran.status = 1 AND
                    (
                            (trawatinap_pendaftaran.idtipe = 1 AND trawatinap_pendaftaran.statuscheckout = 0) OR
                            (trawatinap_pendaftaran.idtipe = 2)
                    ) AND
                    trawatinap_pendaftaran.statuskasir = 0
                GROUP BY
                    trawatinap_pendaftaran.id 
                ORDER BY
                    trawatinap_pendaftaran.tanggaldaftar DESC
                ) AS table_pendaftaran
                WHERE table_pendaftaran.pendaftaran_id IS NOT NULL ".$where."
            ";
        
        $this->from = $from;
        $this->join = [];

        $this->order = [
			'table_pendaftaran.tanggal_daftar' => 'DESC'
		];
		$this->group = ['table_pendaftaran.pendaftaran_id', 'table_pendaftaran.asal_pasien'];
        $this->column_search = ['nomor_permintaan'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];
            $result[] = '<div class="btn-group">
                            <a href="#" data-toggle="tooltip" title="Tambah Permintaan" class="btn btn-success btn-xs" onclick="buatDraftPermintaan(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ', ' . $r->asal_pasien . ')"><i class="fa fa-plus"></i></a>
                            '. ($r->status_permintaan == 1 ? '<a href="#" data-toggle="tooltip" title="List" class="btn btn-warning btn-xs" onclick="planOrder(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ', ' . $r->asal_pasien . ')"><i class="fa fa-list"></i></a>' : '') .
                        '</div>';
            $result[] = GetStatusPermintaan($r->status_permintaan);
            $result[] = GetJenisDirect($r->jenis);
            $result[] = HumanDateLong($r->tanggal_daftar);
            $result[] = GetAsalPasien($r->asal_pasien);
            $result[] = $r->nomor_pendaftaran;
            $result[] = $r->nomor_medrec;
            $result[] = $r->nama_pasien;
            $result[] = $r->kelompok_pasien;
            $result[] = $r->detail;
            
            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexDaftarRencana(): void
    {
        $tanggal_dari = $this->input->post('tanggal_dari');
        $tanggal_sampai = $this->input->post('tanggal_sampai');
        $asal_pasien = $this->input->post('asal_pasien');
        $dokter_peminta = $this->input->post('dokter_peminta');
        $nomor_permintaan = $this->input->post('nomor_permintaan');
        $tujuan_laboratorium = $this->input->post('tujuan_laboratorium');
        $medrec = $this->input->post('medrec');
        $rencana_pemeriksaan_dari = $this->input->post('rencana_pemeriksaan_dari');
        $rencana_pemeriksaan_sampai = $this->input->post('rencana_pemeriksaan_sampai');
        $waktu_permintaan_dari = $this->input->post('waktu_permintaan_dari');
        $waktu_permintaan_sampai = $this->input->post('waktu_permintaan_sampai');
        $klinik_kelas = $this->input->post('klinik_kelas');
        $kelompok_pasien = $this->input->post('kelompok_pasien');
        $nomor_pendaftaran = $this->input->post('nomor_pendaftaran');
        $prioritas = $this->input->post('prioritas');
        $nama_pasien = $this->input->post('nama_pasien');
        
        $where = '';
        if ('' != $tanggal_dari) {
            $where .= " AND DATE(tbl.tanggal_pendaftaran) >= '" . YMDFormat($tanggal_dari) . "'";
        }
        if ('' != $tanggal_sampai) {
            $where .= " AND DATE(tbl.tanggal_pendaftaran) <= '" . YMDFormat($tanggal_sampai) . "'";
        }
        if ('0' != $asal_pasien) {
            $where .= " AND (tbl.asal_pasien_id) = '{$asal_pasien}'";
        }
        if ('0' != $dokter_peminta) {
            $where .= " AND (tbl.dokter_peminta_id) = '{$dokter_peminta}'";
        }
        if ('' != $nomor_permintaan) {
            $where .= " AND (tbl.nomor_permintaan) = '{$nomor_permintaan}'";
        }
        if ('0' != $tujuan_laboratorium) {
            $where .= " AND (tbl.tujuan_laboratorium_id) = '{$tujuan_laboratorium}'";
        }
        if ('' != $medrec) {
            $where .= " AND (tbl.nomor_medrec) = '{$medrec}'";
        }
        if ('' != $rencana_pemeriksaan_dari) {
            $where .= " AND DATE(tbl.rencana_pemeriksaan) >= '" . YMDFormat($rencana_pemeriksaan_dari) . "'";
        }
        if ('' != $rencana_pemeriksaan_sampai) {
            $where .= " AND DATE(tbl.rencana_pemeriksaan) <= '" . YMDFormat($rencana_pemeriksaan_sampai) . "'";
        }
        if ('' != $waktu_permintaan_dari) {
            $where .= " AND DATE(tbl.waktu_permintaan) >= '" . YMDFormat($waktu_permintaan_dari) . "'";
        }
        if ('' != $waktu_permintaan_sampai) {
            $where .= " AND DATE(tbl.waktu_permintaan) <= '" . YMDFormat($waktu_permintaan_sampai) . "'";
        }
        if ('0' != $klinik_kelas) {
            $where .= " AND (tbl.klinik_kelas) = '{$klinik_kelas}'";
        }
        if ('0' != $kelompok_pasien) {
            $where .= " AND (tbl.kelompok_pasien_id) = '{$kelompok_pasien}'";
        }
        if ('' != $nomor_pendaftaran) {
            $where .= " AND (tbl.nomor_pendaftaran) LIKE '%".$nomor_pendaftaran."%'";
        }
        if ('0' != $prioritas) {
            $where .= " AND (tbl.prioritas) = '{$prioritas}'";
        }
        if ('' != $nama_pasien) {
            $where .= " AND (tbl.nama_pasien) LIKE '%".$nama_pasien."%'";
        }

        $this->select = [];
        $from = "
        (
            SELECT
                term_laboratorium_patologi_anatomi.id,
                (
                CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 ) THEN
                    tpoliklinik_pendaftaran.id
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 ) THEN
                    poliklinik_ranap.id
                END
                ) AS pendaftaran_id,
                term_laboratorium_patologi_anatomi.pasien_id,
                term_laboratorium_patologi_anatomi.waktu_permintaan,
                term_laboratorium_patologi_anatomi.nomor_permintaan,
                (
                CASE
					WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 ) THEN
					tpoliklinik_pendaftaran.tanggaldaftar
					WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 ) THEN
					poliklinik_ranap.tanggaldaftar
				END
				) AS tanggal_pendaftaran,
				term_laboratorium_patologi_anatomi.asal_rujukan AS asal_pasien_id,
				(
				CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 1 THEN
                    'Poliklinik'
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 2 THEN
                    'Instalasi Gawat Darurat (IGD)'
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 3 THEN
                    'Rawat Inap'
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan = 4 THEN
                    'One Day Surgery (ODS)'
                END
                ) AS asal_pasien,
                (
                CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 ) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 ) THEN
                    poliklinik_ranap.nopendaftaran
                END
                ) AS nomor_pendaftaran,
                tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                CONCAT( mpoliklinik.nama, ' - ', mdokter_penanggung_jawab.nama ) AS detail,
                term_laboratorium_patologi_anatomi.rencana_pemeriksaan,
                term_laboratorium_patologi_anatomi.dokter_peminta_id,
                mdokter_peminta.nama AS dokter_peminta,
                term_laboratorium_patologi_anatomi.tujuan_laboratorium AS tujuan_laboratorium_id,
                merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium,
                term_laboratorium_patologi_anatomi.prioritas,
                referensi_prioritas.ref AS prioritas_label,
                mppa.nama AS created_ppa,
                term_laboratorium_patologi_anatomi.created_at,
                mpasien_kelompok.id AS kelompok_pasien_id,
                (
                CASE
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 ) THEN
                    tpoliklinik_pendaftaran.idpoliklinik
                    WHEN term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 ) THEN
                    poliklinik_ranap.idkelas
                END
                ) AS klinik_kelas
            FROM
                term_laboratorium_patologi_anatomi
            LEFT JOIN (
            SELECT
                trawatinap_pendaftaran.id,
                trawatinap_pendaftaran.tanggaldaftar,
                trawatinap_pendaftaran.nopendaftaran,
                trawatinap_pendaftaran.idkelas,
                tpoliklinik_pendaftaran.kode_antrian,
                tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                tpoliklinik_pendaftaran.idpasien AS pasien_id,
                tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                mpasien_kelompok.nama AS kelompok_pasien
            FROM
                trawatinap_pendaftaran
                INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
                INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
            ) AS poliklinik_ranap ON term_laboratorium_patologi_anatomi.pendaftaran_id = poliklinik_ranap.id
            AND term_laboratorium_patologi_anatomi.asal_rujukan IN ( 3, 4 )
            LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id
            AND term_laboratorium_patologi_anatomi.asal_rujukan IN ( 1, 2 )
            LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
            LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
            LEFT JOIN mdokter mdokter_penanggung_jawab ON mdokter_penanggung_jawab.id = tpoliklinik_pendaftaran.iddokter
            LEFT JOIN mdokter mdokter_peminta ON mdokter_peminta.id = term_laboratorium_patologi_anatomi.dokter_peminta_id
            LEFT JOIN merm_pengaturan_tujuan_laboratorium ON merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_patologi_anatomi.tujuan_laboratorium
            LEFT JOIN merm_referensi referensi_prioritas ON referensi_prioritas.ref_head_id = 85
            AND referensi_prioritas.nilai = term_laboratorium_patologi_anatomi.prioritas
            LEFT JOIN mppa ON mppa.id = term_laboratorium_patologi_anatomi.created_ppa
            WHERE
                term_laboratorium_patologi_anatomi.jenis_pemeriksaan = '2'
                AND term_laboratorium_patologi_anatomi.status_pemeriksaan != '1'
                AND merm_pengaturan_tujuan_laboratorium.tipe_layanan = '2'
        ) AS tbl
        WHERE
            tbl.id IS NOT NULL ".$where.'
        ';

        
        $this->from = $from;
        $this->join = [];

        $this->order = [];
        $this->group = [];
        $this->column_search = ['nomor_permintaan'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];
            
            $asal_rujukan = (in_array($r->asal_pasien_id, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');

            $result[] = '<div class="btn-group">
                            <a href="#" data-toggle="tooltip" title="Transaksi" class="btn btn-primary btn-xs" onclick="switchOrder(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ', ' . $r->id . ')"><i class="fa fa-plus"></i></a>
                            <a href="#" data-toggle="tooltip" title="Cetak Bukti Permintaan" class="btn btn-xs btn-success" onclick="cetakBuktiPermintaan(' . $r->id . ')"><i class="fa fa-print"></i></a>
                            <a href="#" data-toggle="tooltip" title="Lihat Permintaan" class="btn btn-xs btn-default" onclick="lihatPermintaan(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')"><i class="fa fa-eye"></i></a>
                        </div>';
            $result[] = HumanDateLong($r->waktu_permintaan);
            $result[] = $r->nomor_permintaan;
            $result[] = HumanDateLong($r->tanggal_pendaftaran);
            $result[] = GetAsalPasien($r->asal_pasien_id);
            $result[] = $r->nomor_pendaftaran;
            $result[] = $r->nomor_medrec;
            $result[] = $r->nama_pasien;
            $result[] = $r->detail;
            $result[] = HumanDateLong($r->rencana_pemeriksaan);
            $result[] = $r->dokter_peminta;
            $result[] = $r->tujuan_laboratorium;
            $result[] = '<span class="label label-md label-danger" style="font-size: 11px;">'.$r->prioritas_label.'</span>';
            $result[] = $r->created_ppa.' - '.HumanDateLong($r->created_at);
            
            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }
}
