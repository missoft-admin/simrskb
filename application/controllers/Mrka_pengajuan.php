<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_pengajuan extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_pengajuan_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array(
			'tanggal_pengajuan2'=>'',
			'tanggal_pengajuan1'=>'',
			'tanggal_dibutuhkan1'=>'',
			'tanggal_dibutuhkan2'=>'',
			'idjenis'=>'#',
			'status'=>'1',
		);
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis_all();
		$data['error'] 			= '';
		$data['title'] 			= 'Pengajuan';
		$data['content'] 		= 'Mrka_pengajuan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("List RKA",'#'),
									    			array("List",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create($tipe_rka='1',$disabel=''){
		
		$data = array(
			'id' 						=> '',
			'catatan' 			=> '',
			'idvendor' 					=> '#',
			'cara_pembayaran' 					=> '#',
			'nama' 					=> '',
			'periode' 					=> date('Y'),
			'idunit' 				=> '#',
			// 'idunit_pengaju' 				=> '#',
			'nama_pemohon' 				=> $this->session->userdata('user_name'),
			'tanggal_pengajuan' 	=> date('d-m-Y'),
			'tanggal_dibutuhkan' 	=> date('d-m-Y'),
			'tipe_rka' 				=> $tipe_rka,
			'url_asal' 				=> 'mrka_pengajuan',
			'idrka_kegiatan' 		=> '',
			'idrka_kegiatan' 		=> '',
			'nama_kegiatan' 		=> '',
			'grand_total' 		=> '0',
			'total_jenis_barang' 		=> '0',
			'idjenis' 				=> '#',
			'total_item' 		=> '0',
			'total_dp_cicilan' 		=> '0',
			'sisa_cicilan' 		=> '0',
			'jml_kali_cicilan' 		=> '0',
			'per_bulan_cicilan' 		=> '0',
			'xtanggal_cicilan_pertama' 		=> '',
			'total_dp_termin' 		=> '0',
			'sisa_termin' 		=> '0',
			'tanggal_kontrabon' 		=> '',
			'jenis_cicilan' 		=> '#',
			'cara_pembayaran' 		=> '#',
			'jenis_pembayaran' 		=> '#',
			'idklasifikasi' 		=> '#',
			'norek' 		=> '',
			'atasnama' 		=> '',
			'bank' 		=> '',
			'bulan' 		=> '',
			'keterangan_bank' 		=> '',
			'idpengajuan_asal' 		=> '',
		);

		$data['idunit_pengaju']=$this->Mrka_pengajuan_model->get_default_limit();
		$data['list_dp_cicilan'] 		= array();
		$data['list_cicilan'] 		= array();
		$data['list_dp_termin'] 		= array();
		$data['list_detail'] 		= array();
		$data['list_klasifikasi'] 		= $this->Mrka_pengajuan_model->list_klasifikasi();
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		// print_r($data['list_unit']);exit();
		$data['list_jenis'] 	= $this->Mrka_pengajuan_model->list_jenis($tipe_rka);
		$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
		$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
		$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengajuan Kegiatan';
		$data['content'] 		= 'Mrka_pengajuan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("Tambah",'mrka_pengajuan')
													);
		// print_r($data['idunit_pengaju']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function create_rka($id='1',$disabel=''){
		$row=$this->Mrka_pengajuan_model->getSpecified_idrka_kegiatan($id);
		$data = array(
			'id' 						=> '',
			'catatan' 			=> '',
			'idvendor' 					=> '#',
			'cara_pembayaran' 					=> '#',
			'nama' 					=> '',
			'periode' 					=> date('Y'),
			'idunit' 				=> '#',
			'idunit_pengaju' 				=> $row->idunit,
			'nama_pemohon' 				=> $this->session->userdata('user_name'),
			'tanggal_pengajuan' 	=> date('d-m-Y'),
			'tanggal_dibutuhkan' 	=> date('d-m-Y'),
			'tipe_rka' 				=> '1',
			'idrka_kegiatan' 		=> $id,
			'nama_kegiatan' 		=> $row->nama_kegiatan,
			'grand_total' 		=> '0',
			'total_jenis_barang' 		=> '0',
			'idjenis' 				=> '#',
			'total_item' 		=> '0',
			'total_dp_cicilan' 		=> '0',
			'sisa_cicilan' 		=> '0',
			'jml_kali_cicilan' 		=> '0',
			'per_bulan_cicilan' 		=> '0',
			'xtanggal_cicilan_pertama' 		=> '',
			'total_dp_termin' 		=> '0',
			'sisa_termin' 		=> '0',
			'tanggal_kontrabon' 		=> '',
			'jenis_cicilan' 		=> '#',
			'cara_pembayaran' 		=> '#',
			'jenis_pembayaran' 		=> '#',
			'norek' 		=> '',
			'bank' 		=> '',
			'keterangan_bank' 		=> '',
		);


		$data['list_dp_cicilan'] 		= array();
		$data['list_cicilan'] 		= array();
		$data['list_dp_termin'] 		= array();
		$data['list_detail'] 		= array();
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 	= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
		$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
		$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
		$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengajuan Kegiatan';
		$data['content'] 		= 'Mrka_pengajuan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("Tambah",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_detail_setting($idrka_kegiatan,$bulan,$tanggal_bayar=''){
		$row=$this->Mrka_pengajuan_model->load_detail_setting($idrka_kegiatan,$bulan,$tanggal_bayar);
		// print_r($row);exit();
		$data = array(
			'id' 						=> '',
			'catatan' 			=> '',
			'idvendor' 					=> '#',
			'cara_pembayaran' 					=> '#',
			'nama' 					=> '',
			'periode' 					=> date('Y'),
			'idunit' 				=> $row->idunit,
			'idunit_pengaju' 				=> $row->idunit,
			'nama_pemohon' 				=> $this->session->userdata('user_name'),
			'tanggal_pengajuan' 	=> ($row->tanggal_bayar? HumanDateShort($row->tanggal_bayar):date('d-m-Y')),
			'tanggal_dibutuhkan' 	=> ($row->tanggal_bayar? HumanDateShort($row->tanggal_bayar):date('d-m-Y')),
			'tipe_rka' 				=> '1',
			'idrka_kegiatan' 		=> $idrka_kegiatan,
			'nama_kegiatan' 		=> $row->nama,
			'grand_total' 		=> '0',
			'total_jenis_barang' 		=> '0',
			'idjenis' 				=> '#',
			'total_item' 		=> 0,
			'bulan' 		=> $bulan,
			'total_dp_cicilan' 		=> '0',
			'sisa_cicilan' 		=> '0',
			'jml_kali_cicilan' 		=> '0',
			'per_bulan_cicilan' 		=> '0',
			'xtanggal_cicilan_pertama' 		=> '',
			'total_dp_termin' 		=> '0',
			'sisa_termin' 		=> '0',
			'tanggal_kontrabon' 		=> '',
			'jenis_cicilan' 		=> '#',
			'cara_pembayaran' 		=> '#',
			'idklasifikasi' 		=> '#',
			'jenis_pembayaran' 		=> '#',
			'norek' 		=> '',
			'bank' 		=> '',
			'url_asal' 		=> 'mrka_pengajuan',
		);

		$data['list_klasifikasi'] 		= $this->Mrka_pengajuan_model->list_klasifikasi();
		$data['list_dp_cicilan'] 		= array();
		$data['list_cicilan'] 		= array();
		$data['list_dp_termin'] 		= array();
		$data['list_detail'] 		= $this->Mrka_pengajuan_model->load_detail($idrka_kegiatan,$bulan,$tanggal_bayar);
		// print_r($data['list_detail']);exit();
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 	= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
		$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
		$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
		$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
		$data['disabel'] 			= '';
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengajuan Kegiatan';
		$data['content'] 		= 'Mrka_pengajuan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("Tambah",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id,$disabel='',$url_asal='mrka_pengajuan'){
		
			$data = $this->Mrka_pengajuan_model->getSpecified($id);
			// print_r($data);exit;
			$data['tanggal_dibutuhkan']=HumanDateShort($data['tanggal_dibutuhkan']);
			$data['tanggal_pengajuan']=HumanDateShort($data['tanggal_pengajuan']);
			$data['list_klasifikasi'] 		= $this->Mrka_pengajuan_model->list_klasifikasi();
			$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
			$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
			// $data['list_detail'] 		= $this->Mrka_pengajuan_model->list_detail($id);
			$data['list_detail'] 		= array();
			$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
			$data['list_dp_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,1);
			$data['list_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,2);
			// print_r($data['list_cicilan'] );exit();
			$data['list_dp_termin'] 	=  $this->Mrka_pengajuan_model->list_dp_termin($id);
			$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
			$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
			$data['disabel'] 			= $disabel;
			$data['url_asal'] 			= $url_asal;
			$data['error'] 			= '';
			$data['title'] 			= 'Tambah Pengajuan Kegiatan';
			$data['content'] 		= 'Mrka_pengajuan/manage';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
														array("Tambah",'mrka_pengajuan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	function load_item_barang($id,$disabel=''){
		$list_detail= $this->Mrka_pengajuan_model->list_detail($id);
		
		$gt=0;
		$item=0;
		$tabel='';
		foreach($list_detail as $row){ 
			$gt=$gt + $row->total_harga;
			$btn_harga='';
			if ($row->total_harga > $row->total_harga_asal && $row->total_harga_asal > 0){
				$btn_harga="<br><br><button type='button' onclick='lihat_harga_asal(".$row->id.")' class='btn btn-danger btn-xs'><i class='fa fa-eye'></i> Harga Berbeda</button>";
			}
			$item=$item + 1;		
			$tabel .='<tr>';
				$tabel .='<td>'.$row->nama_barang.'</td>';
				$tabel .='<td>'.$row->merk_barang.'</td>';
				$tabel .='<td>'.$row->kuantitas.'</td>';
				$tabel .='<td>'.$row->satuan.'</td>';
				$tabel .='<td>'.number_format($row->harga_satuan).'<br>└─ Hpp. '.number_format($row->harga_pokok).'<br>└─ Disc. '.number_format($row->harga_diskon).'<br>└─ PPN. '.number_format($row->harga_ppn).'</td>';
				$tabel .='<td>'.number_format($row->total_harga).'</td>';
				$tabel .='<td>'.$row->keterangan.$btn_harga.'</td>';
				$tabel .='<td><div class="btn-group"><button type="button" '.$disabel.' class="btn btn-xs btn-info edit kunci"><i class="glyphicon glyphicon-pencil"></i></button><button type="button" '.$disabel.' class="btn btn-xs btn-danger hapus kunci"><i class="glyphicon glyphicon-remove"></i></button></div></td>';
				$tabel .='<td style="display:none"><input type="text" name=xiddet[]" value="'.$row->id.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xnama_barang[]" value="'.$row->nama_barang.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xmerk_barang[]" value="'.$row->merk_barang.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xkuantitas[]" value="'.$row->kuantitas.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xsatuan[]" value="'.$row->satuan.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xharga_satuan[]" value="'.$row->harga_satuan.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xtotal_harga[]" value="'.$row->total_harga.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xketerangan[]" value="'.$row->keterangan.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xstatus[]" value="'.$row->status.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xharga_pokok[]" value="'.$row->harga_pokok.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xharga_diskon[]" value="'.$row->harga_diskon.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xharga_ppn[]" value="'.$row->harga_ppn.'"></td>';
				$tabel .='<td style="display:none"><input type="text" name="xidklasifikasi_det[]" value="'.$row->idklasifikasi_det.'"></td>';
			$tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		$arr['grand_total']=$gt;
		$q="UPDATE rka_pengajuan set grand_total='$gt' where id='$id'";
		$this->db->query($q);
		$arr['item']=$item;
		$this->output->set_output(json_encode($arr));
	}
	function lihat_harga_asal($id){
		$list_detail= $this->db->query("SELECT *FROM rka_pengajuan_detail_asal where id='$id'")->result();
		
		$tabel='';
		foreach($list_detail as $row){ 
					
			$tabel .='<tr>';
				$tabel .='<td>'.$row->nama_barang.'</td>';
				$tabel .='<td>'.$row->merk_barang.'</td>';
				$tabel .='<td>'.$row->kuantitas.'</td>';
				$tabel .='<td>'.$row->satuan.'</td>';
				$tabel .='<td class="text-right">'.number_format($row->harga_pokok).'</td>';
				$tabel .='<td class="text-right">'.number_format($row->harga_diskon).'</td>';
				$tabel .='<td class="text-right">'.number_format($row->harga_ppn).'</td>';
				$tabel .='<td class="text-right">'.number_format($row->harga_satuan).'</td>';
				$tabel .='<td class="text-right">'.number_format($row->total_harga).'</td>';
				
			$tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function list_detail_cicilan(){
		$id=$this->input->post('id');
		$tanggal_cicilan=$this->input->post('tanggal_cicilan');
		$jenis_cicilan=$this->input->post('jenis_cicilan');
		$q="SELECT D.nama_barang,D.merk_barang,D.satuan 
			,H.nominal_cicilan,H.harga_pokok,H.harga_diskon,H.harga_ppn,H.keterangan_cicilan
			FROM `rka_pengajuan_cicilan` H
			LEFT JOIN rka_pengajuan_detail D ON D.id=H.idpengajuan_detail

			WHERE H.tanggal_cicilan='$tanggal_cicilan' AND H.jenis_cicilan='$jenis_cicilan' AND H.idpengajuan='$id'";
		$list_detail= $this->db->query($q)->result();
		
		
		$gt=0;
		$tabel='';
		foreach($list_detail as $row){ 
			$gt=$gt + $row->nominal_cicilan;
			$tabel .='<tr>';
				$tabel .='<td>'.$row->nama_barang.'</td>';
				$tabel .='<td>'.$row->merk_barang.'</td>';
				$tabel .='<td>'.$row->satuan.'</td>';
				$tabel .='<td>'.number_format($row->nominal_cicilan).'<br>└─ Hpp. '.number_format($row->harga_pokok).'<br>└─ Disc. '.number_format($row->harga_diskon).' (-)<br>└─ PPN. '.number_format($row->harga_ppn).'</td>';
				$tabel .='<td>'.$row->keterangan_cicilan.'</td>';
				
			$tabel .='</tr>';
		}
		$tabel .='<tr>';
			$tabel .='<td colspan="3"><strong>TOTAL </strong></td>';
			$tabel .='<td>'.number_format($gt).'</td>';
			$tabel .='<td></td>';			
		$tabel .='</tr>';
		
		$arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function load_item_dp($id,$disabel=''){
		$q="SELECT C.id,D.nama_barang,C.tanggal_cicilan,C.nominal_cicilan,C.keterangan_cicilan from rka_pengajuan_cicilan C 
			LEFT JOIN rka_pengajuan_detail D ON D.id=C.idpengajuan_detail

			WHERE C.idpengajuan='$id' AND C.jenis_cicilan='1'
			ORDER BY C.tanggal_cicilan";
		$list_detail= $this->db->query($q)->result();
		
		$tabel='';
		foreach($list_detail as $row){ 
			$tabel .='<tr>';
				$tabel .='<td>'.$row->nama_barang.'</td>';
				$tabel .='<td>'.number_format($row->nominal_cicilan).'</td>';
				$tabel .='<td>'.HumanDateShort($row->tanggal_cicilan).'</td>';
				$tabel .='<td>'.$row->keterangan_cicilan.'</td>';
				
				$tabel .='<td><button type="button" '.$disabel.' class="btn btn-sm btn-danger" onclick="hapus_dp_cicilan('.$row->id.')" tabindex="9" title="Hapus"><i class="fa fa-times"></i></button></td>';
				
			$tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function load_item_termin($id,$disabel=''){
		$q="SELECT * FROM `rka_pengajuan_termin` H WHERE H.idpengajuan='$id'
			ORDER BY H.tanggal_termin ASC";
		$list_detail= $this->db->query($q)->result();
		
		$tabel='';
		$total_dp_termin=0;
		foreach($list_detail as $row){ 
			$total_dp_termin=$total_dp_termin + $row->nominal_termin;
			if ($row->st_proses_bendahara=='1'){
				$chk="checked";
			}else{
				$chk="";
			}
			$aksi='<label class="css-input switch switch-sm switch-success">
                         <input '.$disabel.' type="checkbox" '.$chk.' class="pil" onclick="handleClickTermin(this,'.$row->id.');"><span></span> 
					  </label>&nbsp;&nbsp;&nbsp;';
					  
			$tabel .='<tr>';
				$tabel .='<td>'.$aksi.($row->jenis_termin=='1'?'Down Payment':'Pembayaran').'</td>';
				$tabel .='<td>'.$row->judul_termin.'</td>';
				$tabel .='<td>'.$row->deskripsi_termin.'</td>';
				$tabel .='<td>'.number_format($row->nominal_termin).'</td>';
				$tabel .='<td>'.HumanDateShort($row->tanggal_termin).'</td>';
				
				
				$tabel .='<td><button type="button" '.$disabel.' class="btn btn-sm btn-danger" onclick="hapus_termin('.$row->id.')" tabindex="9" title="Hapus"><i class="fa fa-times"></i></button></td>';
				
			$tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		$arr['total_dp_termin']=$total_dp_termin;
		$this->output->set_output(json_encode($arr));
	}
	function load_item_barang_select2($id){
		$list_detail= $this->Mrka_pengajuan_model->list_detail($id);
		
		
		$tabel='';
		$tabel .='<option value="#" >-Pilih Item-</option>';
		foreach($list_detail as $row){ 
			$tabel .='<option value="'.$row->id.'" >'.$row->nama_barang.'</option>';
				
		}
		$arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function delete($id){
		$this->Mrka_pengajuan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mrka_pengajuan','location');
	}
	function update_status($id,$status){
		$result=$this->Mrka_pengajuan_model->update_status($id,$status);
		echo json_encode($result);
	}
	function get_mak_dp($id){
		$q="SELECT (((H.harga_pokok - H.harga_diskon) * H.kuantitas) - H.nominal_dp) maks_dp FROM `rka_pengajuan_detail` H WHERE H.id='$id'";
		$result=$this->db->query($q)->row('maks_dp');
		echo json_encode($result);
	}
	function get_total_dp($id){
		$q="SELECT SUM(H.nominal_dp) nominal_dp FROM `rka_pengajuan_detail` H WHERE H.idpengajuan='$id'";
		// print_r($q);exit;
		$result=$this->db->query($q)->row('nominal_dp');
		echo json_encode($result);
	}
	function simpan_proses_peretujuan($id){
		
		$result=$this->Mrka_pengajuan_model->simpan_proses_peretujuan($id);
		echo json_encode($result);
	}

	function save(){
		// print_r($this->input->post());exit();
		$btn_simpan=$this->input->post('btn_simpan');
		$url_asal=$this->input->post('url_asal');
		if($this->input->post('id') == '' ) {
			$id=$this->Mrka_pengajuan_model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				if ($btn_simpan=='1'){
					redirect($url_asal,'location');
				}else{
					redirect('mrka_pengajuan/update/'.$id.'/0/'.$url_asal,'location');
					
				}
			}
		} else {
			$id=$this->input->post('id');
			if($this->Mrka_pengajuan_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				if ($btn_simpan=='1'){
					redirect($url_asal,'location');
				}else{
					redirect('mrka_pengajuan/update/'.$id.'/0/'.$url_asal,'location');
					
				}
				
				// redirect('mrka_pengajuan','location');
			}
		}

	}
	function save_detail(){
		$idpengajuan=$this->input->post('id');
		$iddet=$this->input->post('iddet');
		$nama_barang=$this->input->post('nama_barang');
		$merk_barang=$this->input->post('merk_barang');
		$kuantitas=$this->input->post('kuantitas');
		$satuan=$this->input->post('satuan');
		$keterangan=$this->input->post('keterangan');
		$status=$this->input->post('status');
		$idklasifikasi_det=$this->input->post('idklasifikasi_det');
		$harga_satuan=RemoveComma($this->input->post('harga_satuan'));
		$total_harga=RemoveComma($this->input->post('total_harga'));
		$harga_pokok=RemoveComma($this->input->post('harga_pokok'));
		$harga_diskon=RemoveComma($this->input->post('harga_diskon'));
		$harga_ppn=RemoveComma($this->input->post('harga_ppn'));
		$data_detail=array(
			'idpengajuan'=>$idpengajuan,
			'nama_barang'=>$nama_barang,
			'merk_barang'=>$merk_barang,
			'kuantitas'=>$kuantitas,
			'satuan'=>$satuan,
			'harga_satuan'=>$harga_satuan,
			'total_harga'=>$total_harga,
			'keterangan'=>$keterangan,
			'status'=>$status,
			
			'idklasifikasi_det'=>$idklasifikasi_det,
			'harga_pokok'=>$harga_pokok,
			'harga_diskon'=>$harga_diskon,
			'harga_ppn'=>$harga_ppn,
			
			'created_by'=> $this->session->userdata('user_id'),
			'created_nama'=> $this->session->userdata('user_name'),
			'created_date'=>  date('Y-m-d H:i:s'),
		); 
		// print_r($data_detail);exit;
		if ($iddet){
			if ($status=='0'){
				$data_detail['deleted_by']=$this->session->userdata('user_id');
				$data_detail['deleted_nama']=$this->session->userdata('user_name');
				$data_detail['deleted_date']=date('Y-m-d H:i:s');
			}else{
				$data_detail['edited_by']=$this->session->userdata('user_id');
				$data_detail['edited_nama']=$this->session->userdata('user_name');
				$data_detail['edited_date']=date('Y-m-d H:i:s');
			}
			
		    $this->db->where('id', $iddet);		
		    $arr=$this->db->update('rka_pengajuan_detail', $data_detail);		
		}else{			
		    $data_detail['created_by']=$this->session->userdata('user_id');
		    $data_detail['created_nama']=$this->session->userdata('user_name');
		    $data_detail['created_date']=date('Y-m-d H:i:s');
		    $arr=$this->db->insert('rka_pengajuan_detail', $data_detail);			
		}
		
		$this->output->set_output(json_encode($arr));
	}
	function save_detail_termin(){
		$idpengajuan=$this->input->post('id');
		
		$jenis_termin=$this->input->post('jenis_termin');
		$tanggal_termin=YMDFormat($this->input->post('tanggal_termin'));
		$judul_termin=$this->input->post('judul_termin');
		$deskripsi_termin=$this->input->post('deskripsi_termin');
		$nominal_termin=RemoveComma($this->input->post('nominal_termin'));
		
		$data_detail=array(
			'idpengajuan'=>$idpengajuan,
			'jenis_termin'=>$jenis_termin,
			'tanggal_termin'=>$tanggal_termin,
			'judul_termin'=>$judul_termin,
			'deskripsi_termin'=>$deskripsi_termin,
			'nominal_termin'=>$nominal_termin,
			
		); 
	
		$arr=$this->db->insert('rka_pengajuan_termin', $data_detail);	
		$q="UPDATE rka_pengajuan_termin set st_proses_bendahara='0' WHERE idpengajuan='$idpengajuan'";
		$this->db->query($q);
		$tgl=$this->db->query("SELECT MIN(tanggal_termin) as tgl FROM rka_pengajuan_termin WHERE idpengajuan='$idpengajuan'")->row('tgl');
		$this->db->query($q);
		$q="UPDATE rka_pengajuan_termin set st_proses_bendahara='1' WHERE idpengajuan='$idpengajuan' AND tanggal_termin='$tgl'";
		$this->db->query($q);
		
		$this->output->set_output(json_encode($arr));
	}
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mrka_pengajuan/manage';

		if($id==''){
			$data['title'] = 'Tambah Create RKA';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
															array("Tambah",'mrka_pengajuan')
													);
		}else{
			$data['title'] = 'Ubah Create RKA';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
															array("Ubah",'mrka_pengajuan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  		
	  $no_pengajuan=$this->input->post('no_pengajuan');
	  $nama_kegiatan=$this->input->post('nama_kegiatan');
	  $tipe_rka=$this->input->post('tipe_rka');
	  $idunit_pengaju=$this->input->post('idunit_pengaju');
	  $idjenis=$this->input->post('idjenis');
	  $tanggal_pengajuan1= ($this->input->post('tanggal_pengajuan1'));
	  $tanggal_pengajuan2= ($this->input->post('tanggal_pengajuan2'));
	  $tanggal_dibutuhkan1= ($this->input->post('tanggal_dibutuhkan1'));
	  $tanggal_dibutuhkan2= ($this->input->post('tanggal_dibutuhkan2'));
	  
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  $status= ($this->input->post('status'));
	  if ($status !='#'){
		  $where .=" AND H.status='$status'";
	  }
	  if ($no_pengajuan !=''){
		  $where .=" AND H.no_pengajuan='$no_pengajuan'";
	  }
	  if ($nama_kegiatan !=''){
		  $where .=" AND H.nama_kegiatan='$nama_kegiatan'";
	  }
	  if ($tipe_rka !='#'){
		  $where .=" AND H.tipe_rka='$tipe_rka'";
	  }
	  if ($idunit_pengaju !='#'){
		  $where .=" AND H.idunit_pengaju='$idunit_pengaju'";
	  }
	  if ($idjenis !='#'){
		  $where .=" AND H.idjenis='$idjenis'";
	  }
	  if ($tanggal_pengajuan1 !='' || $tanggal_pengajuan2 !=''){
		  $where .=" AND (H.tanggal_pengajuan >='".YMDFormat($tanggal_pengajuan1)."' AND H.tanggal_pengajuan <='".YMDFormat($tanggal_pengajuan2)."' )";
	  }
	  if ($tanggal_dibutuhkan1 !='' || $tanggal_dibutuhkan2 !=''){
		  $where .=" AND (H.tanggal_dibutuhkan >='".YMDFormat($tanggal_dibutuhkan1)."' AND H.tanggal_dibutuhkan <='".YMDFormat($tanggal_dibutuhkan2)."' )";
	  }
	  
	  $where .="AND (H.idunit_pengaju IN (SELECT idunit from munitpelayanan_user_setting US WHERE US.userid='$iduser'))";
	  $from="(
				SELECT H.idpengajuan_asal,H.id,H.no_pengajuan,H.created_date, H.tanggal_pengajuan,H.tipe_rka,H.nama_kegiatan,H.catatan 
				,U.nama as unit_pengaju,H.idunit_pengaju,H.grand_total,H.`status`,H.idjenis
				,H.tanggal_dibutuhkan,H.idunit,cek_logic(H.id) as nama_logic
				,J.nama as jenis,GROUP_CONCAT(AP.user_nama) as nama_user,GROUP_CONCAT(AP.approve) as st_approve,MAX(AP.step) as step
				from rka_pengajuan H
				LEFT JOIN rka_pengajuan_approval AP ON AP.idrka=H.id AND AP.st_aktif='1'
				LEFT JOIN munitpelayanan U ON U.id=H.idunit_pengaju
				LEFT JOIN mjenis_pengajuan_rka J ON J.id=H.idjenis
				WHERE H.status != '99' ".$where."
				GROUP BY H.id
				ORDER BY H.tanggal_dibutuhkan,H.id DESC
				) as tbl";
	// print_r($tanggal_dibutuhkan1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_kegiatan','no_pengajuan','catatan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
		  $button_asal='';
		  if ($r->idpengajuan_asal){
			  $button_asal='<br><a href="'.site_url().'mrka_pengajuan/update/'.$r->idpengajuan_asal.'/disabled" target="_blank" title="Lihat Pengajau Asal"><i class="fa fa-folder-open-o"></i></a>';
		  }
		  $nama_logic=$r->nama_logic;//$r->unit_pengaju;;

					$aksi = '<div class="btn-group">';
					if ($r->status !='0'){
						if (UserAccesForm($user_acces_form,array('1398'))){
							$aksi .= '<a href="'.site_url().'mrka_pengajuan/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Detail" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
						}
						if (UserAccesForm($user_acces_form,array('1399'))){
							$aksi .= '<button title="Upload Dokument" class="btn btn-warning btn-sm" onclick="upload_image('.$r->id.')"><i class="fa fa-file-image-o"></i></button>';
						}
						if ($r->status=='1'){//Draft
						if (UserAccesForm($user_acces_form,array('1399'))){
							$aksi .= '<a href="'.site_url().'mrka_pengajuan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
						}
						if (UserAccesForm($user_acces_form,array('1400'))){
							if ($nama_logic==''){
								$aksi .= '<button disabled title="Actived" class="btn btn-primary btn-sm persetujuan"><i class="fa fa-check"></i></button>';
								$nama_logic='<span class="label label-danger" data-toggle="tooltip" title="BELUM DISETTING">LOGIC BELUM DISETTING</span>';
							}else{
								$aksi .= '<button title="Actived" class="btn btn-primary btn-sm persetujuan"><i class="fa fa-check"></i></button>';
								$nama_logic='<span class="label label-success" data-toggle="tooltip" title="LOGI">'.$nama_logic.'</span>';
							}
						}
							if (UserAccesForm($user_acces_form,array('1401'))){
							$aksi .= '<button title="Hapus" class="btn btn-danger btn-sm hapus"><i class="fa fa-trash-o"></i></button>';
							}
						}
							
					
							if (UserAccesForm($user_acces_form,array('1402'))){
								$aksi .= '<a href="#" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
							}
					}
			        $aksi .= '</div>';
		  if ($r->status!='1'){
			  // if ($r->status=='2'){
				$nama_logic='<button title="Lihat User" class="btn btn-danger btn-xs user"><i class="si si-user"></i></button>';
			  // }else{
				// $nama_logic='';
			  // }
			 
		  }
		  $catatan='';
		  if ($r->catatan){
			  $catatan='<br>'.'<p class="text-primary"><i class="fa fa-sticky-note-o"></i> '.$r->catatan.'</p>';
		  }
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $no;//3
          $row[] = $r->no_pengajuan.'<br>'.HumanDateLong($r->created_date);
          $row[] = HumanDateShort($r->tanggal_pengajuan).$button_asal;
          $row[] = tipe_rka($r->tipe_rka);
          $row[] = $r->nama_kegiatan.$catatan;
          $row[] = $r->unit_pengaju;
          $row[] = HumanDateShort($r->tanggal_dibutuhkan);
          $row[] = number_format($r->grand_total,0);
          $row[] = $r->jenis;
          $row[] = status_pengajuan($r->status,$r->step).' '.$nama_logic;//.($status=='1'?$nama_logic:'');
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_list_rekening()
    {
	  // $data_user=get_acces();
	  // $user_acces_form=$data_user['user_acces_form'];
	  		
	  $idvendor=$this->input->post('idvendor');
	  
	  $from="(
				SELECT H.id,H.bankid,M.bank,H.atasnama,H.cabang,H.norekening
				,H.staktif
				FROM mdistributor_bank H
				LEFT JOIN ref_bank M ON M.id=H.bankid
				WHERE H.iddistributor='$idvendor' AND H.tipe_distributor='3'
				) as tbl";
	// print_r($tanggal_dibutuhkan1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('norekening','atasnama','bank');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
		 

					$aksi = '<div class="btn-group">';
					$aksi .= '<button data-toggle="tooltip" title="Pilih" class="btn btn-danger btn-xs pilih_rekening"><i class="fa fa-check"></i> Pilih</button>';
					$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-info btn-xs edit_rekening"><i class="fa fa-pencil"></i></button>';
					// $aksi .= '<a href="#btabs-animated-slideleft-add"><i class="fa fa-pencil"></i> Add / Edit Rekening</a>';
					$aksi .= '</div>';
		
          $row[] = $r->id;
          $row[] = $r->bankid;
          $row[] = $r->bank;
          $row[] = $r->norekening;
          $row[] = $r->atasnama;
          $row[] = $r->cabang;
         
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  public function save_bank()
    {
		
        $idrekening = $this->input->post('idrekening');
        $tipe_distributor = $this->input->post('tipe_distributor');
        $iddistributor = $this->input->post('iddistributor');
        $bankid = $this->input->post('bankid');
        $cabang = $this->input->post('cabang');
        $norekening = $this->input->post('norekening');
        $atasnama = $this->input->post('atasnama');
		// if ($tipe_distributor){}
        // $sql="UPDATE mdistributor_bank SET staktif='0' WHERE iddistributor='$iddistributor' AND tipe_distributor='$tipe_distributor'";
		// $this->db->query($sql);
        $data =array(
            'iddistributor'=>$iddistributor,
            'tipe_distributor'=>$tipe_distributor,
            'bankid'=>$bankid,
            'cabang'=>$cabang,
            'norekening'=>$norekening,
            'atasnama'=>$atasnama,
        );
		if ($idrekening){
			$this->db->where('id', $idrekening);
			$result=$this->db->update('mdistributor_bank', $data);
				
		}else{
			$data['staktif']=1;
			$result=$this->db->insert('mdistributor_bank', $data);
			
		}
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function save_dp()
    {
		
        $det_cicilan=array(
				'idpengajuan'=> $_POST['idpengajuan'],
				'idpengajuan_detail'=> $_POST['idpengajuan_detail'],
				'jenis_cicilan'=> $_POST['jenis_cicilan'],
				'keterangan_cicilan'=> $_POST['keterangan_cicilan'],
				'tanggal_cicilan'=> YMDFormat($_POST['tanggal_cicilan']),
				'nominal_cicilan'=> RemoveComma($_POST['nominal_cicilan']),
				'harga_pokok'=> RemoveComma($_POST['nominal_cicilan']),
				'status'=>1,
			);
			// print_r($det_cicilan);exit;
			$result=$this->db->insert('rka_pengajuan_cicilan',$det_cicilan);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function savePilih()
    {
		
        $det_cicilan=array(
				'st_proses_bendahara'=> $_POST['pilih'],
				
			);
			$this->db->where('tanggal_cicilan',$_POST['tanggal_cicilan']);
			$this->db->where('idpengajuan',$_POST['id']);
			$this->db->where('jenis_cicilan',$_POST['jenis_cicilan']);
			// print_r($det_cicilan);exit;
			$result=$this->db->update('rka_pengajuan_cicilan',$det_cicilan);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function savePilihTermin()
    {
		
        $det_cicilan=array(
				'st_proses_bendahara'=> $_POST['pilih'],
				
			);
			$this->db->where('id',$_POST['idtermin']);
			$this->db->where('idpengajuan',$_POST['id']);
			// print_r($det_cicilan);exit;
			$result=$this->db->update('rka_pengajuan_termin',$det_cicilan);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function hapus_dp_cicilan($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('rka_pengajuan_cicilan');
		$this->output->set_output(json_encode($result));
	}
	function hapus_termin($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('rka_pengajuan_termin');
		$this->output->set_output(json_encode($result));
	}
	function hapus_item($id){
		$this->db->where('id',$id);
		$result=$this->db->delete('rka_pengajuan_detail');
		
		$this->db->where('idpengajuan_detail',$id);
		$result=$this->db->delete('rka_pengajuan_cicilan');
		$this->output->set_output(json_encode($result));
	}
  function load_kegiatan()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $user_id=$this->session->userdata('user_id');	
	  
	  $idrka=$this->input->post('idrka');
	  $idprespektif=$this->input->post('idprespektif');
	  $idprogram=$this->input->post('idprogram');
	  
	  $where='';
	
	  if ($idrka !='#'){
		  $where .=" AND K.idrka='$idrka'";
	  }
	  if ($idprespektif !='#'){
		  $where .=" AND K.idprespektif='$idprespektif'";
	  }
	  if ($idprogram !='#'){
		  $where .=" AND K.idprogram='$idprogram'";
	  }
	  
		
	  $from="(
				SELECT H.nama as rka,K.id,K.idprespektif,K.idkegiatan,M.nama as kegiatan 
				,U.nama as unit,K.idunit,P.nama as presprektif,PR.nama as program
				FROM mrka H
				LEFT JOIN mrka_kegiatan K ON K.idrka=H.id
				LEFT JOIN mkegiatan_rka M ON M.id=K.idkegiatan
				LEFT JOIN munitpelayanan U ON U.id=K.idunit
				LEFT JOIN mprespektif_rka P ON P.id=K.idprespektif
				LEFT JOIN mprogram_rka PR ON PR.id=K.idprogram
				WHERE H.`status`='3' AND K.`status`='1' AND K.idunit IN (SELECT idunit from munitpelayanan_user_setting US WHERE US.userid='$user_id')
				".$where."
				) as tbl";
	// print_r($tanggal_dibutuhkan1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('kegiatan','presprektif','program','rka');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

			$aksi = '<div class="btn-group">';
			$aksi .= '<button title="Pilih" class="btn btn-success btn-sm pilih"><i class="fa fa-check"></i> Pilih</button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $r->rka;
          $row[] = $r->presprektif;
          $row[] = $r->program;
          $row[] = $r->kegiatan;
          $row[] = $r->unit;         
          $row[] = $aksi;
          $row[] = $r->idunit;         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_kegiatan_reminder()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $user_id=$this->session->userdata('user_id');	
	  
	  $idrka=$this->input->post('idrka');
	  $idprespektif=$this->input->post('idprespektif');
	  $idprogram=$this->input->post('idprogram');
	  
	  $where='';
	
	  if ($idrka !='#'){
		  $where .=" AND K.idrka='$idrka'";
	  }
	  if ($idprespektif !='#'){
		  $where .=" AND K.idprespektif='$idprespektif'";
	  }
	  if ($idprogram !='#'){
		  $where .=" AND K.idprogram='$idprogram'";
	  }
	  
		
	  $from="(
				SELECT M.periode,M.nama as rka_nama,K.id as idrka_kegiatan, K.idrka,K.idkegiatan, K.idprogram,K.idprespektif,K.idunit
				,MK.nama as kegiatan,prog.nama as program,MP.nama as prespektif
				,KN.bulan,KN.nominal,KN.st_diajukan,KN.st_setting,KD.tanggal_bayar,KD.st_diajukan as st_diajukan2,KD.total_harga	
				 from mrka_kegiatan K
				 INNER JOIN mrka_kegiatan_nominal KN ON KN.idrka_kegiatan=K.id AND KN.nominal > 0
				 LEFT JOIN mrka_kegiatan_detail KD ON KD.idrka_kegiatan=KN.idrka_kegiatan AND KD.bulan=KN.bulan
				LEFT JOIN mrka M ON M.id=K.idrka
				LEFT JOIN mkegiatan_rka MK ON MK.id=K.idkegiatan
				LEFT JOIN mprogram_rka prog ON prog.id=K.idprogram
				LEFT JOIN mprespektif_rka MP ON MP.id=K.idprespektif
				LEFT JOIN mrka_kegiatan_unit_kolab KL ON KL.idrka_kegiatan=K.id
				WHERE M.`status`='3' ".$where."
				
				AND (K.idunit IN (SELECT idunit from munitpelayanan_user_setting US WHERE US.userid='$user_id') OR KL.idunit IN (SELECT idunit from munitpelayanan_user_setting US WHERE US.userid='$user_id')) 
				GROUP BY K.id,KN.bulan,KD.tanggal_bayar
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('kegiatan','prespektif','program','rka_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$row[] = $r->idrka_kegiatan;
			$row[] = $r->bulan;
			$row[] = $r->idprespektif;
			$row[] = $r->idprogram;
			$row[] = $r->idkegiatan;
			$row[] = $no;
			if ($r->st_diajukan2 =='1'){
				$status_setting='<span class="label label-primary">SUDAH DIAJUKAN</span>';
			}else{
				$status_setting=status_setting($r->st_setting);
			}
			$row[] = $r->rka_nama;
			if ($r->tanggal_bayar){
				$tanggal_bayar=HumanDateShort($r->tanggal_bayar);
			}else{
				$tanggal_bayar='';
			}
			$row[] = $r->kegiatan.' '.$tanggal_bayar;
			$row[] = $r->program;
			$row[] = $r->prespektif;
			$row[] = MONTHFormat($r->bulan).' '.$r->periode.'<br>'.$status_setting;
			if ($r->st_setting=='0'){
				$row[] = number_format($r->nominal,0);
			}else{
				$row[] = number_format($r->total_harga,0);				
			}
			  
			$aksi = '<div class="btn-group">';
			if ($r->st_diajukan2 =='0'){
				if ($r->st_setting == '1' || $r->st_setting == '2'){
					$aksi .= '<a href="'.site_url().'mrka_pengajuan/load_detail_setting/'.$r->idrka_kegiatan.'/'.$r->bulan.'/'.$r->tanggal_bayar.'" data-toggle="tooltip" title="Load Detail" class="btn btn-success btn-sm"><i class="si si-arrow-right"></i></a>';			
				
				}else{
					$aksi .= '<button title="Pilih" class="btn btn-danger btn-sm pilih"><i class="fa fa-check"></i> Pilih</button>';
				}
			}else{
				if ($r->st_diajukan2 !='1'){
					$aksi .= '<button title="Pilih" class="btn btn-danger btn-sm pilih"><i class="fa fa-check"></i> Pilih</button>';
				}
			}
				// $aksi .= $r->st_setting;
			$aksi .= '</div>';

			$row[] = $aksi;
			$data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_user_approval()
    {
	  $idrka=$this->input->post('idrka');	 
	  $where='';
	  $from="(
				SELECT rka_pengajuan.grand_total,U.`name` as user_nama, mlogic_detail.*  FROM rka_pengajuan 
				LEFT JOIN mlogic_unit ON  rka_pengajuan.idunit_pengaju = mlogic_unit.idunit
				LEFT JOIN mlogic_detail ON  mlogic_unit.idlogic = mlogic_detail.idlogic AND rka_pengajuan.tipe_rka = mlogic_detail.tipe_rka AND rka_pengajuan.idjenis = mlogic_detail.idjenis AND mlogic_detail.`status` = 1
				LEFT JOIN  musers U ON U.id=mlogic_detail.iduser
				WHERE rka_pengajuan.id ='$idrka' AND  calculate_logic(mlogic_detail.operand,rka_pengajuan.grand_total,mlogic_detail.nominal) = 1
				ORDER BY mlogic_detail.step,mlogic_detail.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){		 
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function refresh_vendor(){
		
		$q="SELECT * FROM mvendor M
			WHERE M.`status`='1'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function refresh_program($idrka){
		
		$q="SELECT P.idprogram as id,M.nama FROM mrka_program P
			LEFT JOIN mprogram_rka M ON M.id=P.idprogram
			WHERE P.idrka='$idrka' AND P.`status`='1'";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	
	function generate_cicilan(){
		$id=$this->input->post('id');
		$jml_kali_cicilan=$this->input->post('jml_kali_cicilan');
		$xtanggal_cicilan=YMDFormat($this->input->post('xtanggal_cicilan'));
		$per_bulan_cicilan=RemoveComma($this->input->post('per_bulan_cicilan'));
		$sisa_cicilan=$this->input->post('sisa_cicilan');
		$total_dp_cicilan=RemoveComma($this->input->post('total_dp_cicilan'));
		
		$data_header=array(
			'cara_pembayaran' =>2,
			'jml_kali_cicilan' =>$jml_kali_cicilan,
			'per_bulan_cicilan' =>$per_bulan_cicilan,
			'xtanggal_cicilan_pertama' =>$xtanggal_cicilan,
			'total_dp_cicilan' =>$total_dp_cicilan,
		);
		$this->db->where('id',$id);
		$this->db->update('rka_pengajuan',$data_header);
		$q="SELECT D.idpengajuan,D.id,D.nama_barang,D.merk_barang,D.idklasifikasi_det,1 as kuantitas,D.satuan
			,(D.harga_pokok*D.kuantitas) - D.nominal_dp as pokok
			,(D.harga_diskon * D.kuantitas) as diskon
			,(D.harga_ppn * D.kuantitas) as ppn
			,(SELECT pokok) - (SELECT diskon) + (SELECT ppn) as total_satuan
			,(SELECT pokok) - (SELECT diskon) + (SELECT ppn) as total_harga
			FROM rka_pengajuan_detail D
			WHERE D.idpengajuan='$id'";
		$list_detail=$this->db->query($q)->result();
		
		
		$sql_delete="DELETE FROM rka_pengajuan_cicilan WHERE idpengajuan='$id' AND jenis_cicilan='2'";
		$this->db->query($sql_delete);	
		$sql="INSERT INTO rka_pengajuan_cicilan 
			(idpengajuan,idpengajuan_detail,jenis_cicilan,tanggal_cicilan,keterangan_cicilan
			,nominal_cicilan,status,harga_pokok,harga_diskon,harga_ppn) VALUES ";
		
		foreach($list_detail as $r){	
			$total_pokok=$r->pokok;
			$total_diskon=$r->diskon;
			$total_ppn=$r->ppn;
			for ($i=1;$i<=$jml_kali_cicilan;$i++){
				$bulan=($i-1);
				$effectiveDate = date('d-m-Y', strtotime($xtanggal_cicilan . "+$bulan months") );
				// $tang`
				$cicilan='Cicilan Ke-'.($i);
				// $nominal_cicilan=$r->total_satuan/$jml_kali_cicilan;
				
				if ($i==$jml_kali_cicilan){
					$harga_pokok=$total_pokok;
					$harga_diskon=$total_diskon;
					$harga_ppn=$total_ppn;
				}else{
					$harga_pokok=round($r->pokok/$jml_kali_cicilan,0);
					$harga_diskon=round($r->diskon/$jml_kali_cicilan,0);
					$harga_ppn=round($r->ppn/$jml_kali_cicilan,0);
					
					
				}
					$total_pokok=$total_pokok - $harga_pokok;
					$total_diskon=$total_diskon - $harga_diskon;
					$total_ppn=$total_ppn - $harga_ppn;
					$nominal_cicilan=$harga_pokok-$harga_diskon+$harga_ppn;
				
				$sql .= "(".$r->idpengajuan.",".$r->id.",2,'".YMDFormat($effectiveDate)."','".$cicilan."'
						,".$nominal_cicilan.",1,".$harga_pokok.",".$harga_diskon.",".$harga_ppn."),";
				
			}
		}
		$sql = rtrim($sql, ", ");
		$arr=$this->db->query($sql);	
		// print_r($sql);exit;
		$q="SELECT MIN(H.tanggal_cicilan) as tgl FROM rka_pengajuan_cicilan H WHERE H.idpengajuan='$id'";
		$tgl=$this->db->query($q)->row('tgl');
		$q="UPDATE rka_pengajuan_cicilan SET st_proses_bendahara='1' WHERE idpengajuan='$id' AND tanggal_cicilan='$tgl'";
		$tgl=$this->db->query($q);
		// $arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_cicilan($id,$disabel=''){
		$opsi='';
		$total=0;
		$q="SELECT H.idpengajuan,H.tanggal_cicilan,SUM(H.nominal_cicilan) as nominal_cicilan
			,GROUP_CONCAT(DISTINCT H.keterangan_cicilan) as keterangan_cicilan,H.jenis_cicilan 
			,MAX(H.st_proses_bendahara) as st_proses_bendahara
			FROM `rka_pengajuan_cicilan` H
			WHERE H.idpengajuan='$id'
			GROUP BY tanggal_cicilan,H.jenis_cicilan
			ORDER BY H.tanggal_cicilan";
		
		$hasil=$this->db->query($q)->result();
		foreach($hasil as $r){	
			$total=$total +  $r->nominal_cicilan;
			if ($r->st_proses_bendahara=='1'){
				$chk="checked";
			}else{
				$chk="";
			}
			$btn='&nbsp;&nbsp;&nbsp;   <button class="btn btn-xs" type="button" title="Lihat Rincian" onclick="show_detail(\''.$r->tanggal_cicilan.'\','.$r->jenis_cicilan.');"><li class="fa fa-location-arrow text-primary"></li> </button>';
			$aksi='<label class="css-input switch switch-sm switch-success">
                         <input '.$disabel.' type="checkbox" '.$chk.' class="pil" onclick="handleClick(this,\''.$r->tanggal_cicilan.'\','.$r->jenis_cicilan.');"><span></span> 
					  </label>';
			$opsi .='<tr>';
			$opsi .='<td class="text-left">'.$r->keterangan_cicilan.$btn.'</td>';
			$opsi .='<td class="text-center">'.HumanDateShort($r->tanggal_cicilan).'</td>';
			$opsi .='<td class="text-right">'.number_format($r->nominal_cicilan,2).'</td>';
			$opsi .='<td class="text-center">'.$aksi.'</td>';
			$opsi .='</tr>';
		}
		$opsi .='<tr>';
		$opsi .='<td colspan="2" class="text-center"><strong>TOTAL</strong></td>';
		$opsi .='<td class="text-right"><strong>'.number_format($total,2).'</strong></td>';
		$opsi .='<td><input class="form-control input-sm number" type="hidden" id="total_generate_cicilan" name="total_generate_cicilan" value="'.$total.'" /></td>';
		$opsi .='</tr>';
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function get_pilih_kegiatan($id,$bulan){
		$q="SELECT K.id,K.idrka,N.bulan,K.idkegiatan,M.nama,K.idunit from mrka_kegiatan K
		LEFT JOIN mrka_kegiatan_nominal N ON N.idrka_kegiatan=K.id AND N.bulan='$bulan'
		LEFT JOIN mkegiatan_rka M ON M.id=K.idkegiatan
		WHERE K.id='$id'";
		$arr=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function update_rekening(){
		$idvendor=$this->input->post('idvendor');
		$rekening_id=$this->input->post('rekening_id');
		$q="UPDATE mdistributor_bank SET staktif='0' WHERE tipe_distributor='3' AND iddistributor='$idvendor'";
		$this->db->query($q);
		$q="UPDATE mdistributor_bank SET staktif='1' WHERE id='$rekening_id' AND iddistributor='$idvendor'";
		$arr=$this->db->query($q);
		$this->output->set_output(json_encode($arr));
		
	}
	function get_default_rekening(){
		$idvendor=$this->input->post('idvendor');
		
		
		$q="SELECT H.id,H.bankid,M.bank,H.atasnama,H.cabang,H.norekening
			,H.staktif
			FROM mdistributor_bank H
			LEFT JOIN ref_bank M ON M.id=H.bankid
			WHERE H.iddistributor='$idvendor' AND H.tipe_distributor='3'
			ORDER BY H.staktif DESC
			LIMIT 1";
		$arr=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function list_user($id){
		$q="SELECT *FROM rka_pengajuan_approval H WHERE H.idrka='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';			
			$content .='</tr>';
			
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}

}
