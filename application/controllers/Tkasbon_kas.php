<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;

class Tkasbon_kas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Tkasbon_kas_model', 'model');
        $this->load->model('Tpoliklinik_tindakan_model');
        $this->load->model('Mrka_bendahara_model');
        $this->load->model('Thonor_dokter_model');
    }

    public function index($status='#')
    {
        $data = array(
            'idtipe'      => "#",
            'idpegawai'    => "#",
            'tiperefund'    => "#",
            'tanggalawal'   => '',
            'status'   => $status,
            'tanggalakhir'  => ''
        );

        $this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']      = 'Kasbon Proses';
        $data['content']    = 'Tkasbon_kas/index';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Kasbon",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function refresh_pegawai($idtipe){
		if ($idtipe=='1'){
			$q="SELECT M.id,M.nama from mdokter M WHERE M.`status`='1' ORDER BY M.nama asc";
		}elseif ($idtipe=='2'){
			$q="SELECT M.id,M.nama from mpegawai M WHERE M.`status`='1' ORDER BY M.nama asc";
		}

		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}

	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];

		// status:status,
		// idtipe:idtipe,
		// idpegawai:idpegawai,
		// tanggal1:tanggal1,
		// tanggal2:tanggal2,

	  $idtipe=$this->input->post('idtipe');
	  $status=$this->input->post('status');
	  $idpegawai=$this->input->post('idpegawai');
	  $tanggal1=$this->input->post('tanggal1');
	  $tanggal2=$this->input->post('tanggal2');

	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($idtipe !='#'){
		  $where .=" AND H.idtipe='$idtipe'";
	  }
	  if ($idpegawai !='#'){
		  $where .=" AND H.idpegawai='$idpegawai'";
	  }
	  if ($status !='#'){
		 if ($status=='1'){
			  $where .=" AND H.st_transaksi='0'";
		 }
		 if ($status=='2'){
			  $where .=" AND H.st_transaksi='1' AND H.st_approval='0'";
		 }
		 if ($status=='3'){
			  $where .=" AND H.st_transaksi='1' AND H.st_approval='1'";
		 }
		 if ($status=='4'){
			  $where .=" AND H.st_transaksi='1' AND H.st_approval='2'";
		 }
		 if ($status=='5'){
			  $where .=" AND H.st_transaksi='1' AND H.stpencairan='1'";
		 }
	  }
	  if ($tanggal1 !=''){
		  $where .=" AND H.tanggal >='".YMDFormat($tanggal1)."'";
	  }
	  if ($tanggal2 !=''){
		  $where .=" AND H.tanggal <='".YMDFormat($tanggal2)."'";
	  }

	  $from="(
				SELECT H.id,H.idtipe,H.idpegawai,H.tanggal,
				CASE WHEN H.idtipe='1' THEN 'DOKTER' ELSE 'PEGAWAI' END as tipe_nama
				,CASE WHEN H.idtipe='1' THEN MD.nama ELSE MP.nama END as nama
				,H.catatan,H.alokasidana
				,CASE WHEN H.alokasidana='1' THEN 'KASIR RAJAL'
				 WHEN H.alokasidana='2' THEN 'KASIR RANAP'
				ELSE 'BENDAHARA' END as alokasi_nama,H.stpencairan
				,H.st_approval,H.st_transaksi,MAX(AP.step) as step,H.st_verifikasi,H.nominal,H.status,H.notransaksi,COUNT(DISTINCT Doc.id) as doc
				from tkasbon H
				LEFT JOIN mpegawai MP ON MP.id=H.idpegawai AND H.idtipe='2'
				LEFT JOIN mdokter MD ON MD.id=H.idpegawai AND H.idtipe='1'
				LEFT JOIN tkasbon_approval AP ON AP.idkasbon=H.id AND AP.st_aktif='1'
				LEFT JOIN tkasbon_dokumen Doc ON Doc.idkasbon=H.id
				WHERE H.`status`!=0 ".$where."
				GROUP BY H.id
				ORDER BY H.id DESC
				) as tbl";
	// print_r($tiperefund);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama','jenis_kas','bank');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $iduser=$this->session->userdata('user_id');
      foreach ($list as $r) {
            $no++;
            $row = array();
			$query=$this->get_respon($r->id,$r->step);
            $action = '';
            $status = '';
			if ($r->st_transaksi=='1'){
				if ($r->st_approval=='1'){
					$status='<span class="label label-primary">DISETUJUI</span>';
				}
				if ($r->st_approval=='2'){
					$status='<span class="label label-danger">DITOLAK</span>';
				}
				if ($r->st_approval=='0'){
					$status='<span class="label label-success">PROSES PERSETUJUAN</span> <button title="Lihat User" class="btn btn-danger btn-xs user"><i class="si si-user"></i></button>';
				}
				if ($r->stpencairan=='1'){
					$status='<span class="label label-success">TELAH DITRANSAKSIKAN</span>';
				}

			}elseif($r->st_transaksi=='0'){
				$status='<span class="label label-danger">BELUM DIPROSES</span>';
			}
			$action .= '<a href="'.base_url().'tkasbon/update/'.$r->id.'/disabled" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>';
			
			if ($r->st_transaksi=='0'){
				$action  .= '<a href="'.base_url().'tkasbon/update/'.$r->id.'" class="btn btn-xs btn-primary" ><i class="fa fa-pencil"></i></a>&nbsp;';
			}

			$action .= '<a href="'.base_url()."tkasbon/printout/".$r->id.'" class="btn btn-xs btn-success print-refund" target="_blank"><i class="fa fa-print"></i></a>';
			if ($r->st_transaksi=='0' || $r->st_approval=='2'){
				$action .= '<button class="btn btn-xs btn-danger approval" title="Proses Approval" data-idtipe="'.$r->idtipe.'" data-nominal="'.$r->nominal.'" data-id="'.$r->id.'"><i class="fa fa-send"></i></button>';
			}
			if ($r->st_approval=='1' && $r->stpencairan=='0'){
				$action .= '<a href="'.base_url().'tkasbon_kas/pembayaran/'.$r->id.'" class="btn btn-xs btn-primary bayar" title="Proses Pembayaran" data-idtipe="'.$r->idtipe.'" data-nominal="'.$r->nominal.'" data-id="'.$r->id.'"><i class="fa fa-credit-card"></i></a>';
			}
			if ($r->st_verifikasi=='0' && $r->stpencairan=='1'){
				$action  .= '<a href="'.base_url().'tkasbon_kas/pembayaran/'.$r->id.'" class="btn btn-xs btn-success" ><i class="fa fa-credit-card"></i></a>&nbsp;';
				$action .= '<button class="btn btn-xs btn-primary verif" data-tanggal="'.$r->tanggal.'" data-idtipe="'.$r->idtipe.'" data-idpegawai="'.$r->idpegawai.'" data-catatan="'.$r->catatan.'" data-nominal="'.$r->nominal.'" data-id="'.$r->id.'" title="Proses Verifikasi"><i class="fa fa-check"></i> Verifikasi</button>';
			}
			if ($r->st_verifikasi=='1' && $r->stpencairan=='1'){
				$action  .= '<a href="'.base_url().'tkasbon_kas/pembayaran/'.$r->id.'/disabled" class="btn btn-xs btn-default" ><i class="fa fa-credit-card"></i></a>&nbsp;';
			}	
			$respon='';
			if ($query){
			foreach ($query as $res){
				if ($res->iduser==$iduser && $res->step==$r->step){
					if ($res->approve=='0'){
					  $action .= '<button title="Setuju" class="btn btn-primary btn-xs setuju" data-id="'.$res->id.'"><i class="fa fa-check"></i> Setuju</button>';
					  $action .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" data-id="'.$res->id.'"><i class="si si-ban"></i></button>';
					}else{

					  // $action .= '<button title="Batalkan Pesetujuan / Tolak" class="btn btn-primary btn-xs batal" data-id="'.$res->id.'"><i class="fa fa-refresh"></i></button>';
					}
				}
				if ($res->iduser==$iduser){
					$respon .='<span class="label label-warning" data-toggle="tooltip">STEP '.$res->step.'</span> : '.status_approval($res->approve).'<br>';
				}
			}


		  }else{
			   $respon='';
		  }
		  if ($r->stpencairan=='1'){
			  if ($r->st_verifikasi=='1'){
				  $respon ='<span class="label label-success" data-toggle="tooltip">SUDAH DIVERIFIKASI</span>';
			  }else{
				  $respon ='<span class="label label-danger" data-toggle="tooltip">BELUM DIVERIFIKASI</span>';
			  }
		  }
		  if ($r->doc){
			$action .= '<a href="'.base_url().'tkasbon_kas/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-warning" ><i class="fa fa-file-image-o"></i></a>';
		  }else{
			$action .= '<a href="'.base_url().'tkasbon_kas/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-danger" ><i class="fa fa-upload"></i></a>';
			  
		  }
			// $query=$this->get_respon($r->id,$r->step);
            $row[] = $r->id;
            $row[] = $no;
  			$row[] = HumanDateShort($r->tanggal);
  			$row[] = $r->tipe_nama;
  			$row[] = $r->nama;
  			$row[] = $r->catatan;
            $row[] = number_format($r->nominal);
  			$row[] = $r->alokasi_nama;
  			// $row[] = metode_pembayaran_label($r->metode);
  			$row[] = ($r->stpencairan=='0'?'<span class="label label-danger" data-toggle="tooltip">BELUM DIBAYAR</span>':'<span class="label label-success" data-toggle="tooltip">SUDAH DIBAYARKAN</span>');
  			$row[] = $status.'<br><br>'.$respon;
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  public function upload_document($idkasbon)
    {
       
        $data = $this->model->get_kasbon($idkasbon);
		// print_r($data);exit();
        // $data = array(
          // 'idkasbon' => $row->idkasbon,
          // 'nopendaftaran' => $row->nopendaftaran,
          // 'tipependaftaran' => $row->tipe,
          // 'nomedrec' => $row->nomedrec,
          // 'namapasien' => $row->namapasien,
        // );

        $data['error']      = '';
        $data['title'] = 'Upload Dokumen KASBON';
        $data['content'] = 'Tkasbon_kas/upload_dokumen';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Kasbon", 'Tkasbon_kas/index'),
            array("Upload Dokumen", '#')
        );

        // $data['listFiles'] = array();
        $data['listFiles'] = $this->model->getListUploadedDocument($idkasbon);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/kasbon/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'tkasbon_dokumen';

                    $data = array();
                    $data['idkasbon']  = $this->input->post('idkasbon');
                    $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	 public function delete_file($idfile)
    {
        $this->db->where('id', $idfile);
        $query = $this->db->get('tkasbon_dokumen');
        $row = $query->row();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $idfile);
            if ($this->db->delete('tkasbon_dokumen')) {
                if (file_exists('./assets/upload/kasbon/'.$row->filename) && $row->filename !='') {
                    unlink('./assets/upload/kasbon/'.$row->filename);
                }
            }
        }

        return true;
    }
  function get_respon($idkasbon,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from tkasbon_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idkasbon='$idkasbon'";
	  return $this->db->query($q)->result();
  }
  function setuju_batal($id,$status,$idkasbon=''){
		// $arr=array();
		$q="call update_kasbon('$id', $status) ";
		$result=$this->db->query($q);
		// $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM tkasbon H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}

	function verifikasi()
  {
      $idtransaksi = $this->input->post('id');
      $tanggal = $this->input->post('tanggal');
      $idtipe = $this->input->post('idtipe');
      $iddokter = $this->input->post('idpegawai');
      $catatan = $this->input->post('catatan');
      $nominal = $this->input->post('nominal');

      if ($idtipe == '1') {
        $tanggal_pembayaran = YMDFormat(getPembayaranHonorDokter($iddokter));
        $tanggal_jatuhtempo = YMDFormat(getJatuhTempoHonorDokter($iddokter));

        $this->db->set('tanggal_pembayaran', $tanggal_pembayaran);
        $this->db->set('tanggal_jatuhtempo', $tanggal_jatuhtempo);
        $this->db->set('st_verifikasi', '1');
        $this->db->where('id', $idtransaksi);
        $result = $this->db->update('tkasbon');
		$this->model->insert_validasi_kasbon($idtransaksi);
        if ($result) {

          // Proses Honor Dokter
          $isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($iddokter, $tanggal_pembayaran);
          if ($isStopPeriode == null) {
            $idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($iddokter, $tanggal_pembayaran);

            $dokter = $this->db->query("SELECT mdokter.id,
                mdokter.nama,
                mdokter_kategori.id AS idkategori,
                mdokter_kategori.nama AS namakategori
              FROM
                mdokter
              JOIN mdokter_kategori ON mdokter_kategori.id=mdokter.idkategori
              WHERE
                mdokter.id = $iddokter AND
                mdokter.status = 1")->row();

            if ($idhonor == null) {
                $dataHonorDokter = array(
                  'iddokter' => $iddokter,
                  'namadokter' => $dokter->nama,
                  'tanggal_pembayaran' => $tanggal_pembayaran,
                  'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
                  'nominal' => 0,
                  'created_at' => date('Y-m-d H:i:s'),
                  'created_by' => $this->session->userdata('user_id')
                );

                if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
                    $idhonor = $this->db->insert_id();
                }
            }
          }

          $dataDetailHonorDokter = array(
            'idhonor' => $idhonor,
            'idtransaksi' => $idtransaksi,
            'jenis_transaksi' => 'pengeluaran',
            'jenis_tindakan' => 'POTONGAN KASBON',
            'reference_table' => 'tkasbon',
            'namatarif' => $catatan,
            'idkategori' => $dokter->idkategori,
            'namakategori' => $dokter->namakategori,
            'iddokter' => $iddokter,
            'namadokter' => $dokter->nama,
            'jasamedis' => $nominal,
            'potongan_rs' => 0,
            'nominal_potongan_rs' => 0,
            'pajak_dokter' => 0,
            'nominal_pajak_dokter' => 0,
            'jasamedis_netto' => $nominal,
            'tanggal_pemeriksaan' => $tanggal,
            'tanggal_pembayaran' => $tanggal_pembayaran,
            'tanggal_jatuhtempo' => $tanggal_jatuhtempo
          );

          $this->db->insert('thonor_dokter_detail', $dataDetailHonorDokter);

      		$this->output->set_output(json_encode($result));
        }
      } else {
        $this->db->set('st_verifikasi', '1');
        $this->db->where('id', $idtransaksi);
        $result = $this->db->update('tkasbon');

        $this->output->set_output(json_encode($result));
      }
	}

	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_kasbon('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('tkasbon_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
	function load_user_approval()
    {
	  $idtipe=$this->input->post('idtipe');
	  $nominal_trx=$this->input->post('nominal_trx');
	  $where='';
	  $from="(
				SELECT S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak FROM mlogic_kasbon S
				LEFT JOIN musers U ON U.id=S.iduser
				WHERE S.idtipe='$idtipe' AND calculate_logic(S.operand, ".$nominal_trx.", S.nominal)='1' AND S.status='1'
				ORDER BY S.step,S.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_user($id){
		$q="SELECT *FROM tkasbon_approval H WHERE H.idkasbon='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';
			$content .='</tr>';

		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
  function simpan_proses_peretujuan($id){

		$result=$this->model->simpan_proses_peretujuan($id);
		echo json_encode($result);
	}
    public function pembayaran($id,$disabel='')
    {
		$data=$this->model->get_kasbon($id);
		// print_r($data);exit();
        $data['list_pembayaran']      = $this->model->list_pembayaran($id);
		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
        $data['st_edit']      = '0';
        $data['error']      = '';
        $data['title']      = 'Pembayaran';
		if ($data['st_verifikasi']=='1'){
        $data['disabel']      = 'disabled';
			
		}else{
        $data['disabel']      = $disabel;
			
		}
        $data['content']    = 'Tkasbon_kas/manage';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Kasbon",'#'), array("Tambah",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'nomedrec'      => $this->input->post('nomedrec'),
            'namapasien'    => $this->input->post('namapasien'),
            'tiperefund'    => $this->input->post('tiperefund'),
            'tanggalawal'   => $this->input->post('tanggal_refund_awal'),
            'tanggalakhir'  => $this->input->post('tanggal_refund_akhir')
        );

        $this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']      = 'Kasbon';
        $data['content']    = 'Tkasbon_kas/index';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Kasbon",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }


    public function save()
    {
		$id=$this->model->saveData();
		if($id){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tkasbon_kas','location');
		}
	}
    public function saveKasbon()
    {
        $data = array(
            'tanggal' => date("Y-m-d h:i:s", strtotime(YMDFormat($this->input->post('tanggal')).' '.$this->input->post('jam'))),
            'tipe' => $this->input->post('tiperefund'),
            'tipetransaksi' => $this->input->post('tipetransaksi'),
            'idtransaksi' => $this->input->post('idtransaksi'),
            'notransaksi' => $this->input->post('notransaksi'),
            'idpasien' => $this->input->post('idpasien'),
            'idkelompokpasien' => $this->input->post('idkelompokpasien'),
            'metode'     => $this->input->post('metode'),
            'totaltransaksi'  => RemoveComma($this->input->post('totaltransaksi')),
            'totalpembayaran'  => RemoveComma($this->input->post('totalpembayaran')),
            'totaldeposit' => RemoveComma($this->input->post('totaldeposit')),
            'nominal'  => RemoveComma($this->input->post('nominal')),
            'bank' => $this->input->post('bank'),
            'norekening' => $this->input->post('norekening'),
            'nominal'  => RemoveComma($this->input->post('nominal')),
            'alasan' => $this->input->post('alasan'),
            'created_by' => $this->session->userdata('user_id')
        );

        if ($this->db->insert('tkasbon', $data)) {
            $detail = json_decode($this->input->post('detail'));
            $idkasbon = $this->db->insert_id();

            foreach ($detail as $row) {
                $data = array();
                $data['idkasbon'] = $idkasbon;
                if ($this->input->post('tiperefund') != '2') {
                    $data['reference'] = $row[0];
                    $data['idtransaksi'] = $row[1];
                } else {
                    $data['reference'] = $row->reference;
                    $data['idtransaksi'] = $row->iddetail;
                    $data['tanggal'] = $row->tanggal;
                    $data['tipe'] = $row->tipe;
                    $data['tindakan'] = $row->tindakan;
                    $data['kuantitas'] = $row->kuantitas;
                    $data['harga'] = $row->harga;
                    $data['totalharga'] = $row->total;
                }

                $this->db->insert('tkasbon_detail', $data);
            }

            $status	= ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    public function saveKasbonTransaksi()
    {
        $detail = $this->input->post('details');

        $data = array(
            'idtransaksi' => $this->input->post('idtransaksi'),
            'tanggal' => date("Y-m-d", strtotime($this->input->post('tanggal_refund'))),
            'noregistrasi' => $this->input->post('no_registrasi'),
            'nomedrec' => $this->input->post('no_medrec'),
            'idtipe' => $this->input->post('idtipe'),
            'metode_refund' => $this->input->post('metode_refund'),
            'bank_refund' => $this->input->post('bank_refund'),
            'norek_refund' => $this->input->post('norek_refund'),
            'nominal_refund' => $this->input->post('nominal_refund'),
            'total' => 0,
            'total_transaksi' => 0,
            'alasan' => $this->input->post('alasan'),
            'user_created' => $this->session->userdata('user_id'),
            'status' => 1
        );

        $result = $this->model->saveKasbonTransaksi($data, $detail);

        if ($result) {
            $status = ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    public function removeKasbon($id)
    {
        $alasan = $this->input->post('alasan');
        $result = $this->model->removeKasbon($id, $alasan);

        if ($result) {
            $status	= ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    function updatePembayaranKasbon()
    {
        $this->metode = $this->input->post('metode');
        $this->bank = $this->input->post('bank');
        $this->norekening = $this->input->post('norekening');
        $this->alasan = $this->input->post('alasan');

        if ($this->db->update('tkasbon', $this, array('id' =>$this->input->post('idtransaksi')))) {
            $status	= ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    public function searchList()
    {
        $tiperefund = $this->input->post('tiperefund');
        $tanggalawal = YMDFormat($this->input->post('tanggalawal'));
        $tanggalakhir = YMDFormat($this->input->post('tanggalakhir'));
        $idtipetransaksi = $this->input->post('idtipetransaksi');

        $data = array();

        if ($tiperefund == '0') {
            $result = $this->model->getKasbonDeposit($tanggalawal, $tanggalakhir, $idtipetransaksi);
        } elseif ($tiperefund == '1') {
            $result = $this->model->getKasbonObat($tanggalawal, $tanggalakhir, $idtipetransaksi);
        } elseif ($tiperefund == '2') {
            $result = $this->model->getKasbonTransaksi($tanggalawal, $tanggalakhir, $idtipetransaksi);
        }

        foreach ($result as $row) {
            $rows = array();

            $rows[] = '<label class="label label-primary text-uppercase">' . $row->tipetransaksi. '</label>';
            ;
            $rows[] = $row->notransaksi;
            $rows[] = $row->nomedrec;
            $rows[] = $row->namapasien;
            $rows[] = $row->namakelompokpasien;
            $rows[] = $row->namadokter;

            if ($tiperefund == '2') {
                $rows[] = '<button class="btn btn-success btn-xs text-uppercase selectTransaksi" style="font-size:10px;" data-reference="'.$row->reference.'" data-idtransaksi="'.$row->id.'">Pilih Transaksi</button>';
            } else {
                $rows[] = '<button class="btn btn-success btn-xs text-uppercase selectTransaksi" style="font-size:10px;" data-idtransaksi="'.$row->id.'">Pilih Transaksi</button>';
            }

            $data[] = $rows;
        }

        $output = array(
          "recordsTotal" => count($result),
          "recordsFiltered" => count($result),
          "data" => $data
        );

        echo json_encode($output);
    }

    public function getInfoKasbon($id)
    {
        $row = $this->model->getInfoKasbon($id);

        $this->output->set_output(json_encode($row));
    }

    public function fakturKasbonDeposit($id)
    {
        $dompdf = new Dompdf();
        $row = $this->model->getInfoKasbon($id);

        $data = array(
            'notransaksi' 		 => $row->notransaksi,
            'tanggaltransaksi' => $row->tanggaltransaksi,
            'notransaksi' 		     => $row->notransaksi,
            'tanggalrefund'	   => $row->tanggalrefund,
            'tiperefund' 		   => $row->tiperefund,
            'nomedrec' 		     => $row->nomedrec,
            'namapasien' 		   => $row->namapasien,
            'kelompokpasien'   => $row->kelompokpasien,
            'alasan'           => $row->alasan,
            'idmetode'         => $row->idmetode,
            'metode'           => $row->metode,
            'bank'             => $row->bank,
            'norekening'       => $row->norekening,
            'totaltransaksi'   => $row->totaltransaksi,
            'totaldeposit'     => $row->totaldeposit,
            'nominal'      => $row->nominal,
        );

        $data['list_deposit'] = $this->model->getFakturKasbonDeposit($id);

        $html = $this->load->view('Tkasbon_kas/print/faktur_refund_deposit', $data, true);

        $dompdf->loadHtml($html);

        $customPaper = array(0,0,480,600);
        $dompdf->setPaper($customPaper);

        // $dompdf->setPaper('A3', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Kasbon Deposit.pdf', array("Attachment"=>0));
    }

    public function fakturKasbonObat($id)
    {
        $dompdf = new Dompdf();
        $row = $this->model->getInfoKasbon($id);

        $data = array(
            'notransaksi' 		 => $row->notransaksi,
            'tanggaltransaksi' => $row->tanggaltransaksi,
            'notransaksi' 		     => $row->notransaksi,
            'tanggalrefund'	   => $row->tanggalrefund,
            'tiperefund' 		   => $row->tiperefund,
            'nomedrec' 		     => $row->nomedrec,
            'namapasien' 		   => $row->namapasien,
            'kelompokpasien'   => $row->kelompokpasien,
            'alasan'           => $row->alasan,
            'idmetode'         => $row->idmetode,
            'metode'           => $row->metode,
            'bank'             => $row->bank,
            'norekening'       => $row->norekening,
            'totaltransaksi'   => $row->totaltransaksi,
            'totaldeposit'     => $row->totaldeposit,
            'nominal'      => $row->nominal,
        );

        $data['list_obat'] = $this->model->getFakturKasbonObat($id);

        $html = $this->load->view('Tkasbon_kas/print/faktur_refund_obat', $data, true);

        $dompdf->loadHtml($html);

        $customPaper = array(0,0,480,600);
        $dompdf->setPaper($customPaper);

        // $dompdf->setPaper('A3', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Kasbon Deposit.pdf', array("Attachment"=>0));
    }

    public function fakturKasbonTransaksi($id)
    {
        $dompdf = new Dompdf();
        $row = $this->model->getInfoKasbon($id);

        $data = array(
            'notransaksi' 		 => $row->notransaksi,
            'tanggaltransaksi' => $row->tanggaltransaksi,
            'notransaksi' 		     => $row->notransaksi,
            'tanggalrefund'	   => $row->tanggalrefund,
            'tiperefund' 		   => $row->tiperefund,
            'nomedrec' 		     => $row->nomedrec,
            'namapasien' 		   => $row->namapasien,
            'kelompokpasien'   => $row->kelompokpasien,
            'alasan'           => $row->alasan,
            'idmetode'         => $row->idmetode,
            'metode'           => $row->metode,
            'bank'             => $row->bank,
            'norekening'       => $row->norekening,
            'totaltransaksi'   => $row->totaltransaksi,
            'totaldeposit'     => $row->totaldeposit,
            'nominal'      => $row->nominal,
        );

        $data['list_transaksi'] = $this->model->getFakturKasbonTransaksi($id);

        $html = $this->load->view('Tkasbon_kas/print/faktur_refund_transaksi', $data, true);

        $dompdf->loadHtml($html);

        $customPaper = array(0,0,480,600);
        $dompdf->setPaper($customPaper);

        // $dompdf->setPaper('A3', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Kasbon Deposit.pdf', array("Attachment"=>0));
    }

    // Kasbon Deposit
    public function getKasbonDeposit()
    {
        $empty  = (object)array('id' => '', 'text' => '');
        $result = $this->model->getKasbonDeposit();
        array_unshift($result, $empty);

        $this->output->set_output(json_encode($result));
    }

    public function getInfoKasbonDeposit($id)
    {
        $row = $this->model->getInfoKasbonDeposit($id);

        $this->output->set_output(json_encode($row));
    }

    // Kasbon Retur Obat Rawat Jalan
    public function getKasbonObat()
    {
        $empty  = (object)array('id' => '', 'text' => '');
        $result = $this->model->getKasbonObat();
        array_unshift($result, $empty);

        $this->output->set_output(json_encode($result));
    }

    public function getInfoKasbonObat($id)
    {
        $row = $this->model->getInfoKasbonObat($id);

        $this->output->set_output(json_encode($row));
    }

    public function getDetailKasbonObat($id)
    {
        $row = $this->model->getDetailKasbonObat($id);

        $this->output->set_output(json_encode($row));
    }

    // Kasbon Transaksi Rawat Jalan & Rawat Inap
    public function getKasbonTransaksi()
    {
        $empty  = (object)array('id' => '', 'text' => '');
        $result = $this->model->getKasbonTransaksi();
        array_unshift($result, $empty);

        $this->output->set_output(json_encode($result));
    }

    public function getInfoKasbonTransaksi($reference, $id)
    {
        $row = $this->model->getInfoKasbonTransaksi($reference, $id);

        $this->output->set_output(json_encode($row));
    }

    public function getDetailKasbonTransaksiRawatJalan($id)
    {
        $rowTindakan = $this->Tpoliklinik_tindakan_model->getSpecifiedTindakan($id);

        $data = array();
        $data['idpendaftaran'] = $id;
        $data['listPelayanan']	= $this->Tpoliklinik_tindakan_model->getListPelayanan($rowTindakan->id);
        $data['listObat']	= $this->Tpoliklinik_tindakan_model->getListObat($rowTindakan->id);
        $data['listAlkes']	= $this->Tpoliklinik_tindakan_model->getListAlkes($rowTindakan->id);
        $data['listLaboratorium']	= $this->model->getListLaboratorium($rowTindakan->idpendaftaran);
        $data['listRadiologi']	= $this->model->getListRadiologi($rowTindakan->idpendaftaran);
        $data['listFisioterapi']	= $this->model->getListFisioterapi($rowTindakan->idpendaftaran);
        $data['listFarmasi']	= $this->model->getListFarmasi($rowTindakan->idpendaftaran);
        $data['listAdministrasi']	= $this->model->getListAdministrasi($rowTindakan->idpendaftaran);

        $this->load->view('Tkasbon_kas/transaksi_rawatjalan', $data);
    }

    public function getDetailKasbonTransaksiRawatInap($id)
    {
        $data = array();
        $data['idpendaftaran'] = $id;

        $this->load->view('Tkasbon_kas/transaksi_rawatinap', $data);
    }
	public function insert_validasi_kasbon($id){
		$this->model->insert_validasi_kasbon($id);
	}
}
