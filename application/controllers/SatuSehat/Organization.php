<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Organization extends CI_Controller
{
    /**
     * Organization controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('SatuSehat/Organization_model', 'model');
    }

    public function index(): void
    {
        $this->load->library('SatuSehat/OrganizationLib', null, 'SatuSehatOrganization');
        $result = $this->SatuSehatOrganization->getData();
        $data_satu_sehat = json_decode($result);
        
        $data = [
            'organization_id' => isset($data_satu_sehat) ? $data_satu_sehat->id : '',
            'organization_name' => isset($data_satu_sehat) ? $data_satu_sehat->name : '',
            'type_code' => 'dept',
            'type_name' => 'Hospital Department',
            'telecom_phone' => isset($data_satu_sehat->telecom[0]) ? $data_satu_sehat->telecom[0]->value : '',
            'telecom_email' => isset($data_satu_sehat->telecom[1]) ? $data_satu_sehat->telecom[1]->value : '',
            'telecom_url' => isset($data_satu_sehat->telecom[2]) ? $data_satu_sehat->telecom[2]->value : '',
            'address_line' => isset($data_satu_sehat->address[0]->line[0]) ? $data_satu_sehat->address[0]->line[0] : '',
            'address_city' => isset($data_satu_sehat->address[0]) ? $data_satu_sehat->address[0]->city : '',
            'address_postal_code' => isset($data_satu_sehat->address[0]) ? $data_satu_sehat->address[0]->postalCode : '',
            'address_country' => isset($data_satu_sehat->address[0]) ? $data_satu_sehat->address[0]->country : 'ID',
            'address_province_id' => isset($data_satu_sehat->address[0]->extension[0]->extension[0]) ? $data_satu_sehat->address[0]->extension[0]->extension[0]->valueCode : '',
            'address_city_id' => isset($data_satu_sehat->address[0]->extension[0]->extension[1]) ? $data_satu_sehat->address[0]->extension[0]->extension[1]->valueCode : '',
            'address_district_id' => isset($data_satu_sehat->address[0]->extension[0]->extension[2]) ? $data_satu_sehat->address[0]->extension[0]->extension[2]->valueCode : '',
            'address_village_id' => isset($data_satu_sehat->address[0]->extension[0]->extension[3]) ? $data_satu_sehat->address[0]->extension[0]->extension[3]->valueCode : '',
            'satu_sehat_response' => $result,
        ];

        $data['error'] = '';
        $data['title'] = 'Organization';
        $data['content'] = '_SatuSehat/Organization/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Organization', '#'],
            ['Update', 'SatuSehat/organization'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function save(): void
    {
        $this->load->library('SatuSehat/OrganizationLib', null, 'SatuSehatOrganization');

        $organizationData = [
            'organization_id' => $this->input->post('organization_id'),
            'organization_name' => $this->input->post('organization_name'),
            'type_code' => $this->input->post('type_code'),
            'type_name' => $this->input->post('type_name'),
            'telecom_phone' => $this->input->post('telecom_phone'),
            'telecom_email' => $this->input->post('telecom_email'),
            'telecom_url' => $this->input->post('telecom_url'),
            'address_line' => $this->input->post('address_line'),
            'address_city' => $this->input->post('address_city'),
            'address_postal_code' => $this->input->post('address_postal_code'),
            'address_country' => $this->input->post('address_country'),
            'address_province_id' => $this->input->post('address_province_id'),
            'address_city_id' => $this->input->post('address_city_id'),
            'address_district_id' => $this->input->post('address_district_id'),
            'address_village_id' => $this->input->post('address_village_id'),
        ];

        $response = $this->SatuSehatOrganization->update($organizationData);
        if (json_decode($response)->id) {
            $this->model->saveData($organizationData);
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('SatuSehat/organization', 'location');
        } else {
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output($response)
                ->_display()
            ;
    
            exit;
        }
    }
}
