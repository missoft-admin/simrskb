<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Condition extends CI_Controller
{
    /**
     * Condition controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('SatuSehat/Condition_model', 'model');
    }

    public function index(): void
    {
        $data = [
			'registration_number' => '',
			'medical_record_number' => '',
			'patient_name' => '',
			'start_date' => date('d/m/Y'),
			'end_date' => date('d/m/Y'),
			'patient_origin' => '',
			'polyclinic_id' => '',
			'doctor_id' => '',
		];

        $data['error'] = '';
        $data['title'] = 'Condition';
        $data['content'] = '_SatuSehat/Condition/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Condition', '#'],
            ['List', 'SatuSehat/encounter'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->model->getSpecified($id);

            $encounter_id = $row->encounter_id;
            
            // Satu Sehat Condition
            $this->load->library('SatuSehat/ConditionLib', null, 'SatuSehatCondition');
            $result = $this->SatuSehatCondition->searchByEncounter($encounter_id);
						
            if (isset($row->transaction_id)) {
                $data = [
                    'id' => $row->id,
                    'condition_id' => $row->condition_id,
                    'encounter_id' => $row->encounter_id,
                    'transaction_id' => $row->transaction_id,
                    'category_code' => $row->category_code,
                    'category_name' => $row->category_name,
                    'condition_code' => $row->condition_code,
                    'condition_name' => $row->condition_name,
                    'patient_id' => $row->patient_id,
                    'patient_name' => $row->patient_name,
                    'encounter_display' => $row->encounter_display,
                    'satu_sehat_response' => $result,
                ];

                $data['error'] = '';
                $data['title'] = 'Condition';
                $data['content'] = '_SatuSehat/Condition/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Condition', '#'],
                    ['Update', 'SatuSehat/encounter'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('SatuSehat/encounter', 'encounter');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('SatuSehat/encounter');
        }
    }

    public function save(): void
    {
        $this->load->library('SatuSehat/ConditionLib', null, 'SatuSehatCondition');

        $encounterData = [
            'condition_id' => $this->input->post('condition_id'),
            'encounter_id' => $this->input->post('encounter_id'),
            'transaction_id' => $this->input->post('transaction_id'),
            'category_code' => $this->input->post('category_code'),
            'category_name' => $this->input->post('category_name'),
            'condition_code' => $this->input->post('condition_code'),
            'condition_name' => $this->input->post('condition_name'),
            'patient_id' => $this->input->post('patient_id'),
            'patient_name' => $this->input->post('patient_name'),
            'encounter_display' => $this->input->post('encounter_display'),
        ];

        if ($this->input->post('id') == '') {
            $response = $this->SatuSehatCondition->diagnosis($encounterData);
        } else {
            $encounterId = $this->input->post('encounter_id');
            $response = $this->SatuSehatCondition->update($encounterId, $encounterData);
        }

        $responseDecode = json_decode($response);
        if (isset($responseDecode->id) && $responseDecode->id) {
            $this->model->saveData(array_merge($encounterData, ['condition_id' => $responseDecode->id]));
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('SatuSehat/encounter', 'encounter');
        } else {
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output($response)
                ->_display()
            ;
    
            exit;
        }
    }

    public function getIndex(): void
    {
        $this->select = [];

		$where = '';

		if ($this->input->post('start_date') == '') {
			$where = " WHERE DATE(tpoliklinik_pendaftaran.tanggaldaftar) = '" . date('Y-m-d') . "'";
		} else {
			$where = " WHERE DATE(tpoliklinik_pendaftaran.tanggaldaftar) >= '" . YMDFormat($this->input->post('start_date')) . "' AND DATE(tpoliklinik_pendaftaran.tanggaldaftar) <= '" . YMDFormat($this->input->post('end_date')) . "'";
		}
		if ($this->input->post('patient_origin') != '') {
			$where .= " AND tpoliklinik_pendaftaran.idtipe = '" . $this->input->post('patient_origin') . "'";
		}
		if ($this->input->post('doctor_id') != '') {
			$where .= " AND tpoliklinik_pendaftaran.iddokter='" . $this->input->post('doctor_id') . "'";
		}
		if ($this->input->post('polyclinic_id') != '') {
			$where .= " AND tpoliklinik_pendaftaran.idpoliklinik = '" . $this->input->post('polyclinic_id') . "'";
		}
		if ($this->input->post('registration_number') != '') {
            $where .= " AND tpoliklinik_pendaftaran.nopendaftaran = '" . $this->input->post('registration_number') . "'";
		}
		if ($this->input->post('medical_record_number') != '') {
            $where .= " AND tpoliklinik_pendaftaran.no_medrec = '" . $this->input->post('medical_record_number') . "'";
		}
		if ($this->input->post('patient_name') != '') {
            $where .= " AND tpoliklinik_pendaftaran.namapasien LIKE '%" . $this->input->post('patient_name') . "%'";
        }
        if ($this->input->post('status_condition') != '') {
            $where .= " AND IF ( satu_sehat_condition.condition_id <> '', 1, 0 ) = '" . $this->input->post('status_condition') . "'";
        }

        $this->from = '(
            SELECT
                satu_sehat_encounter.id,
                satu_sehat_encounter.encounter_id,
                satu_sehat_encounter.transaction_id,
                satu_sehat_encounter.status_encounter,
                satu_sehat_encounter.patient_name,
                satu_sehat_encounter.practitioner_name,
                satu_sehat_encounter.location_name,
                satu_sehat_encounter.diagnosis_condition_name,
                tpoliklinik_pendaftaran.nopendaftaran AS reference_transacation,
                tpoliklinik_pendaftaran.no_medrec AS reference_medical_record,
                tpoliklinik_pendaftaran.namapasien AS reference_patient_name,
                mdokter.nama AS reference_doctor_name,
                IF
                    ( satu_sehat_condition.condition_id <> "", 1, 0 ) AS status_connect
            FROM
                satu_sehat_encounter
            LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = satu_sehat_encounter.transaction_id
            LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
            LEFT JOIN satu_sehat_condition ON satu_sehat_condition.encounter_id = satu_sehat_encounter.encounter_id
            ' . $where . '
        ) as result';

        $this->join = [];

        $this->where = [];

        $this->order = [
            'id' => 'DESC',
        ];

        $this->group = [];

        $this->column_search = ['patient_name'];
        $this->column_order = ['patient_name'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->encounter_id;
            $row[] = $r->patient_name;
            $row[] = $r->practitioner_name;
            $row[] = $r->location_name;
            $row[] = $r->diagnosis_condition_name;
            $row[] = 'No. Transaksi: ' . $r->reference_transacation . '<br>No. Medrec: ' . $r->reference_medical_record . '<br>Nama Pasien: '  . $r->reference_patient_name . '<br>Dokter: '  . $r->reference_doctor_name;
            $row[] = StatusConnect($r->status_connect);
            $row[] = '<a href="'.site_url().'SatuSehat/condition/update/'.$r->id.'" data-toggle="tooltip" title="Update" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }
}
