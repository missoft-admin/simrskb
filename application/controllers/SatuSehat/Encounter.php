<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Encounter extends CI_Controller
{
    /**
     * Encounter controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('SatuSehat/Encounter_model', 'model');
    }

    public function index(): void
    {
        $data = [
			'registration_number' => '',
			'medical_record_number' => '',
			'patient_name' => '',
			'start_date' => date('d/m/Y'),
			'end_date' => date('d/m/Y'),
			'patient_origin' => '',
			'polyclinic_id' => '',
			'doctor_id' => '',
		];

        $data['error'] = '';
        $data['title'] = 'Encounter';
        $data['content'] = '_SatuSehat/Encounter/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Encounter', '#'],
            ['List', 'SatuSehat/encounter'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->model->getSpecified($id);

            $encounter_id = $row->encounter_id;
            
            // Satu Sehat Encounter
            $this->load->library('SatuSehat/EncounterLib', null, 'SatuSehatEncounter');
            $result = $this->SatuSehatEncounter->getById($encounter_id);
						
            if (isset($row->transaction_id)) {
                $data = [
                    'id' => $row->id,
                    'encounter_id' => $row->encounter_id,
                    'transaction_id' => $row->transaction_id,
                    'resource_type' => $row->resource_type,
                    'status_encounter' => $row->status_encounter,
                    'class_code' => $row->class_code,
                    'class_name' => $row->class_name,
                    'patient_id' => $row->patient_id,
                    'patient_name' => $row->patient_name,
                    'participant_type_code' => $row->participant_type_code,
                    'participant_type_name' => $row->participant_type_name,
                    'practitioner_id' => $row->practitioner_id,
                    'practitioner_name' => $row->practitioner_name,
                    'arrived_start_date' => $row->arrived_start_date,
                    'arrived_end_date' => $row->arrived_start_date,
                    'inprogress_start_date' => $row->arrived_start_date,
                    'inprogress_end_date' => $row->finished_start_date,
                    'finished_start_date' => $row->finished_start_date,
                    'finished_end_date' => $row->finished_start_date,
                    'location_id' => $row->location_id,
                    'location_name' => $row->location_name,
                    'diagnosis_condition_id' => $row->diagnosis_condition_id,
                    'diagnosis_condition_name' => $row->diagnosis_condition_name,
                    'diagnosis_code' => $row->diagnosis_code,
                    'diagnosis_name' => $row->diagnosis_name,
                    'organization_id' => $row->organization_id,
                    'satu_sehat_response' => $result,
                ];

                // Konversi string tanggal menjadi objek DateTime
                $inprogress_start = convertToDateTime($row->arrived_start_date);
                $inprogress_end = convertToDateTime($row->finished_start_date);
                $finished_start = convertToDateTime($row->finished_start_date);
                $finished_end = convertToDateTime($row->finished_start_date);
            

                $errors = [];
                if ($inprogress_end < $inprogress_start) {
                    $errors[] = 'Tanggal selesai proses in-progress harus lebih besar dari atau sama dengan tanggal mulai proses in-progress.';
                }
                if ($finished_start < $inprogress_end) {
                    $errors[] = 'Tanggal mulai proses finished harus lebih besar dari atau sama dengan tanggal selesai proses in-progress.';
                }
                if ($finished_end < $finished_start) {
                    $errors[] = 'Tanggal selesai proses finished harus lebih besar dari atau sama dengan tanggal mulai proses finished.';
                }

                if (empty($errors)) {
                    $data['is_error'] = false;
                } else {
                    $data['is_error'] = true;
                    $data['error_messages'] = '<div class="error-messages">';
                    foreach ($errors as $error) {
                        $data['error_messages'] .= '<p>' . htmlspecialchars($error) . '</p>';
                    }
                    $data['error_messages'] .= '</div>';
                }

                $data['error'] = '';
                $data['title'] = 'Encounter';
                $data['content'] = '_SatuSehat/Encounter/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Encounter', '#'],
                    ['Update', 'SatuSehat/encounter'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('SatuSehat/encounter', 'encounter');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('SatuSehat/encounter');
        }
    }

    public function save(): void
    {
        $this->load->library('SatuSehat/EncounterLib', null, 'SatuSehatEncounter');

        $encounterData = [
            'transaction_id' => $this->input->post('transaction_id'),
            'resource_type' => $this->input->post('resource_type'),
            'status_encounter' => $this->input->post('status_encounter'),
            'class_code' => $this->input->post('class_code'),
            'class_name' => $this->input->post('class_name'),
            'patient_id' => $this->input->post('patient_id'),
            'patient_name' => $this->input->post('patient_name'),
            'participant_type_code' => $this->input->post('participant_type_code'),
            'participant_type_name' => $this->input->post('participant_type_name'),
            'practitioner_id' => $this->input->post('practitioner_id'),
            'practitioner_name' => $this->input->post('practitioner_name'),
            'arrived_start_date' => $this->input->post('arrived_start_date'),
            'arrived_end_date' => $this->input->post('arrived_end_date'),
            'inprogress_start_date' => $this->input->post('inprogress_start_date'),
            'inprogress_end_date' => $this->input->post('inprogress_end_date'),
            'finished_start_date' => $this->input->post('finished_start_date'),
            'finished_end_date' => $this->input->post('finished_end_date'),
            'location_id' => $this->input->post('location_id'),
            'location_name' => $this->input->post('location_name'),
            'diagnosis_condition_id' => $this->input->post('diagnosis_condition_id'),
            'diagnosis_condition_name' => $this->input->post('diagnosis_condition_name'),
            'diagnosis_code' => $this->input->post('diagnosis_code'),
            'diagnosis_name' => $this->input->post('diagnosis_name'),
            'organization_id' => $this->input->post('organization_id'),
        ];

        if ($this->input->post('id') == '') {
            $response = $this->SatuSehatEncounter->create($encounterData);
        } else {
            $encounterId = $this->input->post('encounter_id');
            $response = $this->SatuSehatEncounter->updateFinished($encounterId, $encounterData);
        }

        $responseDecode = json_decode($response);
        if (isset($responseDecode->id) && $responseDecode->id) {
            $this->model->saveData(array_merge($encounterData, ['encounter_id' => $responseDecode->id]));
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('SatuSehat/encounter', 'encounter');
        } else {
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output($response)
                ->_display()
            ;
    
            exit;
        }
    }

    public function getIndex(): void
    {
        $this->select = [];

		$where = '';

		if ($this->input->post('start_date') == '') {
			$where = " WHERE DATE(tpoliklinik_pendaftaran.tanggal) = '" . date('Y-m-d') . "'";
		} else {
			$where = " WHERE DATE(tpoliklinik_pendaftaran.tanggal) >= '" . YMDFormat($this->input->post('start_date')) . "' AND DATE(tpoliklinik_pendaftaran.tanggal) <= '" . YMDFormat($this->input->post('end_date')) . "'";
		}
		if ($this->input->post('patient_origin') != '') {
			$where .= " AND tpoliklinik_pendaftaran.idtipe = '" . $this->input->post('patient_origin') . "'";
		}
		if ($this->input->post('doctor_id') != '') {
			$where .= " AND tpoliklinik_pendaftaran.iddokter='" . $this->input->post('doctor_id') . "'";
		}
		if ($this->input->post('polyclinic_id') != '') {
			$where .= " AND tpoliklinik_pendaftaran.idpoliklinik = '" . $this->input->post('polyclinic_id') . "'";
		}
		if ($this->input->post('registration_number') != '') {
            $where .= " AND tpoliklinik_pendaftaran.nopendaftaran = '" . $this->input->post('registration_number') . "'";
		}
		if ($this->input->post('medical_record_number') != '') {
            $where .= " AND tpoliklinik_pendaftaran.no_medrec = '" . $this->input->post('medical_record_number') . "'";
		}
		if ($this->input->post('patient_name') != '') {
			$where .= " AND tpoliklinik_pendaftaran.namapasien LIKE '%" . $this->input->post('patient_name') . "%'";
		}
        if ($this->input->post('status_encounter') != '-') {
            if ($this->input->post('status_encounter') == 'request-ihs') {
                $where .= " AND satu_sehat_patient.patient_id IS NULL";
            } else {
                $where .= " AND IF( satu_sehat_encounter.transaction_id, satu_sehat_encounter.status_encounter, 'not-connect') = '" . $this->input->post('status_encounter') . "'";
            }
        }
        if ($this->input->post('status_nik') == '1') {
            $where .= " AND COALESCE(mfpasien.ktp, mfpasien.nik) != ''";
        } else if ($this->input->post('status_nik') == '0') {
            $where .= " AND COALESCE(mfpasien.ktp, mfpasien.nik) = ''";
        } else if ($this->input->post('status_nik') == '9999999999999999') {
            $where .= " AND COALESCE(mfpasien.ktp, mfpasien.nik) = '9999999999999999'";
        }
        $where .= " AND tpoliklinik_pendaftaran.status = 1 AND tpoliklinik_pendaftaran.status_reservasi = 3";
        $where .= " AND satu_sehat_location.location_status = 'active'";
        
        $this->from = '(
            SELECT
                satu_sehat_encounter.id,
                satu_sehat_condition.condition_id,
                tpoliklinik_pendaftaran.id AS transaction_id,
                "Encounter" AS resource_type,
                IF( satu_sehat_encounter.transaction_id, satu_sehat_encounter.status_encounter, "not-connect") AS status_encounter,
                "AMB" AS class_code,
                "ambulatory" AS class_name,
                satu_sehat_patient.patient_id AS patient_id,
                satu_sehat_patient.name AS patient_name,
                "ATND" AS participant_type_code,
                "attender" AS participant_type_name,
                satu_sehat_practitioner.practitioner_id AS practitioner_id,
                satu_sehat_practitioner.name AS practitioner_name,
                IF (
                    tpoliklinik_pendaftaran.reservasi_cara = 0, 
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(app_reservasi_tanggal_pasien.checkin_date, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS arrived_start_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(poli_ttv.created_date, "%Y-%m-%dT%H:%i:%s"),"+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS arrived_end_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(poli_ttv.created_date, "%Y-%m-%dT%H:%i:%s"),"+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS inprogress_start_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(tkasir.tanggal, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS inprogress_end_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(tkasir.tanggal, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS finished_start_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(tkasir.tanggal, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS finished_end_date,
                satu_sehat_location.location_id AS location_id,
                satu_sehat_location.location_description AS location_name,
                icd_10.kode AS diagnosis_condition_id,
                icd_10.deskripsi AS diagnosis_condition_name,
                "DD" AS diagnosis_code,
                "Discharge diagnosis" AS diagnosis_name,
                (SELECT organization_id FROM satu_sehat_organization LIMIT 1) AS organization_id,
                tpoliklinik_pendaftaran.nopendaftaran AS reference_transacation,
                tpoliklinik_pendaftaran.no_medrec AS reference_medical_record,
                tpoliklinik_pendaftaran.namapasien AS reference_patient_name,
                mdokter.nama AS reference_doctor_name,
                COALESCE(
                    CASE 
                        WHEN mfpasien.ktp IS NULL OR mfpasien.ktp = "" OR mfpasien.ktp = "-" THEN mfpasien.nik 
                        ELSE mfpasien.ktp 
                    END, 
                    mfpasien.nik
                ) AS identifier,
                tpoliklinik_pendaftaran.idpasien AS reference_patient_id
            FROM
                tpoliklinik_pendaftaran
            LEFT JOIN mfpasien ON mfpasien.id = tpoliklinik_pendaftaran.idpasien
            LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
            LEFT JOIN mppa ON mppa.pegawai_id = mdokter.id AND mppa.tipepegawai = 2
            LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
            LEFT JOIN tkasir ON tkasir.idtipe IN (1, 2) AND tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.status = 2
            LEFT JOIN mlokasi_ruangan ON mlokasi_ruangan.poliklinik_id = tpoliklinik_pendaftaran.idpoliklinik
            LEFT JOIN satu_sehat_location ON satu_sehat_location.reference_location_id = mlokasi_ruangan.id
            LEFT JOIN satu_sehat_patient ON satu_sehat_patient.pasien_id = tpoliklinik_pendaftaran.idpasien
            LEFT JOIN satu_sehat_practitioner ON satu_sehat_practitioner.ppa_id = mppa.id
            LEFT JOIN satu_sehat_encounter ON satu_sehat_encounter.transaction_id = tpoliklinik_pendaftaran.id
            LEFT JOIN satu_sehat_condition ON satu_sehat_condition.encounter_id = satu_sehat_encounter.encounter_id
            LEFT JOIN trm_layanan_berkas ON trm_layanan_berkas.id_trx = tpoliklinik_pendaftaran.id
            LEFT JOIN trm_berkas_icd10 ON trm_berkas_icd10.idberkas = trm_layanan_berkas.id AND trm_berkas_icd10.jenis_id = 1 AND trm_berkas_icd10.STATUS = 1 
            LEFT JOIN icd_10 ON icd_10.id = trm_berkas_icd10.icd_id 
            LEFT JOIN app_reservasi_tanggal_pasien ON app_reservasi_tanggal_pasien.id = tpoliklinik_pendaftaran.reservasi_id
            LEFT JOIN (SELECT pendaftaran_id, created_date FROM tpoliklinik_ttv WHERE status_ttv = 2 ORDER BY id DESC LIMIT 1) AS poli_ttv ON poli_ttv.pendaftaran_id = tpoliklinik_pendaftaran.id
            ' . $where . '
            GROUP BY
                tpoliklinik_pendaftaran.id
        ) as result';

        $this->join = [];

        $this->where = [];

        $this->order = [
            'transaction_id' => 'DESC',
        ];

        $this->group = [];

        $this->column_search = ['reference_transacation', 'reference_medical_record'];
        $this->column_order = ['reference_transacation'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->class_code;
            $row[] = $r->patient_name;
            $row[] = $r->practitioner_name;
            $row[] = $r->location_name;
            $row[] = $r->diagnosis_condition_name;
            $row[] = 'No. Transaksi: ' . $r->reference_transacation . '<br>No. Medrec: ' . $r->reference_medical_record . '<br>Nama Pasien: '  . $r->reference_patient_name . '<br>NIK Pasien: '  . $r->identifier . '<br>Dokter: '  . $r->reference_doctor_name;
            $row[] = StatusEncounter($r->status_encounter);
            $row[] = '
                '.($r->status_encounter != 'finished' && $r->patient_id != '' ? '<a target="_blank" href="'.site_url().'SatuSehat/encounter/update/'.$r->transaction_id.'" data-toggle="tooltip" title="Update" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> '.($r->condition_id == '' ? 'Update to In Progress' : 'Update to Finished').'</a>' : '').'
                '.($r->patient_id == '' ? '<a target="_blank" href="'.site_url().'SatuSehat/patient/update/'.$r->reference_patient_id.'" data-toggle="tooltip" title="Request IHS Number" class="btn btn-primary btn-xs"><i class="fa fa-user"></i> Request IHS Number</a>' : '').'
                '.($r->id != '' && $r->condition_id == '' ? '<a target="_blank" href="'.site_url().'SatuSehat/condition/update/'.$r->id.'" data-toggle="tooltip" title="Sync Diagnosis Condition" class="btn btn-primary btn-xs"><i class="fa fa-stethoscope"></i> Sync Diagnosis Condition</a>' : '').'
            ';

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }
}
