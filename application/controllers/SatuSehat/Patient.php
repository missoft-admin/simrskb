<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Patient extends CI_Controller
{
    /**
     * Patient controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('SatuSehat/Patient_model', 'model');
    }

    public function index(): void
    {
        $data = [
			'medical_record_number' => '',
			'patient_name' => '',
			'ihs_number' => '',
			'status_integration' => '',
			'filter_date' => '',
			'arrived_date_start' => '',
			'arrived_date_end' => '',
		];

        $data['error'] = '';
        $data['title'] = 'Patient';
        $data['content'] = '_SatuSehat/Patient/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Patient', '#'],
            ['List', 'SatuSehat/patient'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->model->getSpecified($id);
            
            $nik = $row->identifier;
            $name = $row->name;
            $birthday = $row->birth_date;
            $gender = $row->gender;
            
            // Satu Sehat Patient
            $this->load->library('SatuSehat/PatientLib', null, 'SatuSehatPatient');
            
            // Jika NIK tidak valid, langsung cari berdasarkan nama, gender, dan tanggal lahir
            if ($nik == '' || $nik == '-' || $nik == '9999999999999999') {
                $result = $this->SatuSehatPatient->searchByNameGenderBirthdate($name, $birthday, $gender);
            } else {
                // Cari berdasarkan NIK
                $result = $this->SatuSehatPatient->searchByNIK($nik);
                
                // Jika hasil pencarian berdasarkan NIK tidak ditemukan, cari berdasarkan nama, gender, dan tanggal lahir
                if (empty($result) || empty(json_decode($result)->entry)) {
                    $result = $this->SatuSehatPatient->searchByNameGenderBirthdate($name, $birthday, $gender);
                }
            }

            $data_satu_sehat = $result ? json_decode($result)->entry[0]->resource : [];
            
            if (!isset($data_satu_sehat->id)) {
                $this->SatuSehatPatient->createByNIK($row);
            }

            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'patient_nik' => $row->identifier,
                    'patient_nama' => $row->name,
                    'patient_id' => isset($data_satu_sehat->id) ? $data_satu_sehat->id : null,
                    'patient_name' => isset($data_satu_sehat->name[0]->text) ? $data_satu_sehat->name[0]->text : null,
                    'patient_gender' => isset($data_satu_sehat->gender) ? $data_satu_sehat->gender : null,
                    'patient_birthdate' => isset($data_satu_sehat->birthDate) ? $data_satu_sehat->birthDate : null,
                    'satu_sehat_response' => $result,
                ];

                $data['error'] = '';
                $data['title'] = 'Patient';
                $data['content'] = '_SatuSehat/Patient/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Patient', '#'],
                    ['Update', 'SatuSehat/patient'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('SatuSehat/patient', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('SatuSehat/patient');
        }
    }

    public function save(): void
    {
        $this->form_validation->set_rules('patient_id', 'Patient ID', 'trim|required');

        if (true === $this->form_validation->run()) {
            if ($this->model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('SatuSehat/patient', 'location');
            }
        } else {
            $this->failed_save();
        }
    }

    public function failed_save(): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = '_SatuSehat/Patient/manage';

        $data['title'] = 'Patient';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Patient', '#'],
            ['Update', 'SatuSehat/patient'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function process_by_nik($id) {
        $patient = $this->model->getSpecified($id);

        if ($patient) {
            $dataPatient = [
                'pasien_id' => $patient->id,
                'patient_nama' => $row->name,
                'patient_id' => isset($data_satu_sehat->id) ? $data_satu_sehat->id : null,
                'patient_name' => isset($data_satu_sehat->name[0]->text) ? $data_satu_sehat->name[0]->text : null,
                'patient_gender' => isset($data_satu_sehat->gender) ? $data_satu_sehat->gender : null,
                'patient_birthdate' => isset($data_satu_sehat->birthDate) ? $data_satu_sehat->birthDate : null,
                'satu_sehat_response' => $result,
            ];

            $nik = $patient->nik;
            if (!empty($nik)) {
                // Satu Sehat Patient
                $this->load->library('SatuSehat/PatientLib', null, 'SatuSehatPatient');
                $result = $this->SatuSehatPatient->searchByNIK($nik);
                $data_satu_sehat = $result ? json_decode($result)->entry[0]->resource : [];

                if (isset($data_satu_sehat->id)) {
                    $status = 'SUCCESS';
                } else {
                    $status = 'FAILED';
                    $result = 'NIK not found in SatuSehat';
                }
            } else {
                $result = 'NIK is empty';
                $status = 'FAILED';
            }

            $this->model->saveHistory($id, 'BY NIK', $nik, $status);

            if ($status === 'SUCCESS') {
                $dataPatient = array(
                    'pasien_id' => $patient->id,
                    'patient_id' => isset($data_satu_sehat->id) ? $data_satu_sehat->id : null,
                    'name' => isset($data_satu_sehat->name[0]->text) ? $data_satu_sehat->name[0]->text : null,
                    'gender' => isset($data_satu_sehat->gender) ? $data_satu_sehat->gender : null,
                    'birthdate' => isset($data_satu_sehat->birthDate) ? $data_satu_sehat->birthDate : null,
                );
                
                $this->_handleResult($status, 'Proses by NIK', $dataPatient);
            } else {
                $this->session->set_flashdata('message_flash', 'NIK is empty or not found in SatuSehat');
                redirect('SatuSehat/patient');
            }
        } else {
            $this->session->set_flashdata('message_flash', 'Patient not found');
            redirect('SatuSehat/patient');
        }
    }

    public function process_by_ktp($id) {
        $patient = $this->model->getSpecified($id);

        if ($patient) {
            $ktp = $patient->ktp;

            if (!empty($ktp)) {
                // Satu Sehat Patient
                $this->load->library('SatuSehat/PatientLib', null, 'SatuSehatPatient');
                $result = $this->SatuSehatPatient->searchByNIK($ktp);
                $data_satu_sehat = $result ? json_decode($result)->entry[0]->resource : [];

                if (isset($data_satu_sehat->id)) {
                    $status = 'SUCCESS';
                } else {
                    $status = 'FAILED';
                    $result = 'KTP not found in SatuSehat';
                }
            } else {
                $result = 'KTP is empty';
                $status = 'FAILED';
            }

            $this->model->saveHistory($id, 'BY KTP', $ktp, $status);

            if ($status === 'SUCCESS') {
                $dataPatient = array(
                    'pasien_id' => $patient->id,
                    'patient_id' => isset($data_satu_sehat->id) ? $data_satu_sehat->id : null,
                    'name' => isset($data_satu_sehat->name[0]->text) ? $data_satu_sehat->name[0]->text : null,
                    'gender' => isset($data_satu_sehat->gender) ? $data_satu_sehat->gender : null,
                    'birthdate' => isset($data_satu_sehat->birthDate) ? $data_satu_sehat->birthDate : null,
                );

                $this->_handleResult($status, 'Proses by KTP', $dataPatient);
            } else {
                $this->session->set_flashdata('message_flash', 'KTP is empty or not found in SatuSehat');
                redirect('SatuSehat/patient');
            }
        } else {
            $this->session->set_flashdata('message_flash', 'Patient not found');
            redirect('SatuSehat/patient');
        }
    }

    public function process_by_data($id) {
        $patient = $this->model->getSpecified($id);

        if ($patient) {
            $name = $patient->name;
            $birthdate = $patient->birth_date;
            $gender = $patient->gender;

            if ($name != '' && $birthdate != '' && $gender != '') {
                // Satu Sehat Patient
                $this->load->library('SatuSehat/PatientLib', null, 'SatuSehatPatient');
                $result = $this->SatuSehatPatient->searchByNameGenderBirthdate($name, $birthdate, $gender);

                $data_satu_sehat = $result ? json_decode($result)->entry[0]->resource : [];

                if (isset($data_satu_sehat->id)) {
                    $status = 'SUCCESS';
                } else {
                    $status = 'FAILED';
                    $result = 'Data not found in SatuSehat';
                }
            } else {
                $result = 'Incomplete data: name, birthdate or gender is missing';
                $status = 'FAILED';
            }

            $this->model->saveHistory($id, 'BY DATA', $name . ', ' . $birthdate . ', ' . $gender, $status);

            if ($status === 'SUCCESS') {
                $dataPatient = array(
                    'pasien_id' => $patient->id,
                    'patient_id' => isset($data_satu_sehat->id) ? $data_satu_sehat->id : null,
                    'name' => isset($data_satu_sehat->name[0]->text) ? $data_satu_sehat->name[0]->text : null,
                    'gender' => isset($data_satu_sehat->gender) ? $data_satu_sehat->gender : null,
                    'birthdate' => isset($data_satu_sehat->birthDate) ? $data_satu_sehat->birthDate : null,
                );

                $this->_handleResult($status, 'Proses by Data', $dataPatient);
            } else {
                $this->session->set_flashdata('message_flash', 'Incomplete or invalid data');
                redirect('SatuSehat/patient');
            }
        } else {
            $this->session->set_flashdata('message_flash', 'Patient not found');
            redirect('SatuSehat/patient');
        }
    }

    private function _handleResult($status, $processType, $dataPatient) {
        if ($status == 'SUCCESS') {
            $this->db->on_duplicate('satu_sehat_patient', $dataPatient);

            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', "$processType berhasil.");
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', "$processType gagal.");
        }
        redirect('SatuSehat/patient');
    }

    public function getIndex(): void
    {
        $this->select = [];

        $where = 'WHERE mfpasien.status = 1';
        
		if ($this->input->post('medical_record_number') != '') {
            $where .= " AND mfpasien.no_medrec = '" . $this->input->post('medical_record_number') . "'";
		}
		if ($this->input->post('patient_name') != '') {
            $where .= " AND mfpasien.nama LIKE '%" . $this->input->post('patient_name') . "%'";
		}
		if ($this->input->post('ihs_number') != '') {
            $where .= " AND satu_sehat_patient.patient_id = '" . $this->input->post('ihs_number') . "'";
		}
		if ($this->input->post('status_integration') != '-') {
			$where .= " AND COALESCE( CASE 
                WHEN satu_sehat_patient.patient_id IS NOT NULL THEN 1
                WHEN satu_sehat_patient_history.status = 'FAILED' THEN 9
                ELSE 0
            END, 0) = '" . $this->input->post('status_integration') . "'";
		}
		if ($this->input->post('filter_date') == '1') {
            if ($this->input->post('arrived_date_start') != '') {
                $where .= " AND DATE ( tpoliklinik_pendaftaran.tanggal ) >= '" . YMDFormat($this->input->post('arrived_date_start')) . "'";
            }
            if ($this->input->post('arrived_date_end') != '') {
                $where .= " AND DATE ( tpoliklinik_pendaftaran.tanggal ) <= '" . YMDFormat($this->input->post('arrived_date_end')) . "'";
            }
		}
        if ($this->input->post('medical_record_number') == '' && $this->input->post('medical_record_number') == '') {
            $where .= " LIMIT 100";
        }

        $this->from = '(
				SELECT
					mfpasien.id,
					mfpasien.nama,
					satu_sehat_patient.patient_id,
                    mfpasien.no_medrec AS reference_medical_record,
                    mfpasien.nama AS reference_patient_name,
                    DATE(mfpasien.tanggal_lahir) AS reference_born_date,
                    mfpasien.ktp,
                    mfpasien.nik,
                    COALESCE(
                        CASE 
                            WHEN mfpasien.ktp IS NULL OR mfpasien.ktp = "" OR mfpasien.ktp = "-" THEN mfpasien.nik 
                            ELSE mfpasien.ktp 
                        END, 
                        mfpasien.nik
                    ) AS identifier,
                    COALESCE(
                        CASE 
                            WHEN satu_sehat_patient.patient_id IS NOT NULL THEN 1
                            WHEN satu_sehat_patient_history.status = "FAILED" THEN 9
                            ELSE 0
                        END, 
                        0
                    ) AS status_connect
				FROM
					mfpasien
					LEFT JOIN satu_sehat_patient ON satu_sehat_patient.pasien_id = mfpasien.id
					LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.idpasien = mfpasien.id
                    LEFT JOIN (
                        SELECT
                            patient_id,
                            status,
                            MAX(id) AS last_update
                        FROM 
                            satu_sehat_patient_history
                        GROUP BY patient_id
                    ) AS satu_sehat_patient_history 
                        ON satu_sehat_patient_history.patient_id = mfpasien.id
                '. $where .'
			) as result';

        $this->join = [];

        $this->where = [];

        $this->order = [
            'reference_patient_name' => 'ASC',
        ];

        $this->group = [];

        $this->column_search = ['reference_patient_name'];
        $this->column_order = ['reference_patient_name'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->reference_medical_record;
            $row[] = $r->nama;
            $row[] = $r->reference_born_date;
            $row[] = $r->nik;
            $row[] = $r->ktp;
            $row[] = $r->patient_id;
            $row[] = StatusConnect($r->status_connect) . ' <a href="#" onclick="loadHistory(' . $r->id . ')" data-toggle="modal" data-target="#HistoryIntegrasiModal" title="History" class="btn btn-primary btn-xs" style="font-size: 9px;"><i class="fa fa-file"></i> History</a>';
            $row[] = '
                <div class="btn-group">
                    <a href="'.site_url().'SatuSehat/patient/process_by_nik/'.$r->id.'" data-toggle="tooltip" title="Proses By NIK" class="btn btn-primary btn-xs" style="font-size: 9px;"><i class="fa fa-check-circle-o"></i> By NIK</a>
                    <a href="'.site_url().'SatuSehat/patient/process_by_ktp/'.$r->id.'" data-toggle="tooltip" title="Proses By KTP" class="btn btn-danger btn-xs" style="font-size: 9px;"><i class="fa fa-check-circle-o"></i> By KTP</a>
                    <a href="'.site_url().'SatuSehat/patient/process_by_data/'.$r->id.'" data-toggle="tooltip" title="Proses By Data" class="btn btn-warning btn-xs" style="font-size: 9px;"><i class="fa fa-check-circle-o"></i> By Data</a>
                    <a href="#" data-id="'.$r->id.'" data-toggle="modal" data-target="#UpdateDataModal" class="btn btn-success btn-xs" style="font-size: 9px;"><i class="fa fa-pencil"></i> Update</a>
                    <a href="'.site_url().'SatuSehat/patient/update/'.$r->id.'"target="_blank" data-toggle="tooltip" title="Lihat Rincian" class="btn btn-primary btn-xs" style="font-size: 9px;"><i class="fa fa-eye"></i> View</a>
                </div>
            ';

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }

    public function getHistoryIntegration() {
        $id = $this->input->post('id');
        $data = $this->model->getHistoryById($id);
        
        if ($data) {
            echo json_encode(['status' => true, 'data' => $data]);
        } else {
            echo json_encode(['status' => false, 'message' => 'Data history tidak ditemukan']);
        }
    }

    public function getPatientById() {
        $patient_id = $this->input->post('patient_id');

        $this->db->select('no_medrec, nama, DATE_FORMAT(tanggal_lahir, "%d/%m/%Y") AS tanggal_lahir, nik, ktp');
        $this->db->where('id', $patient_id);
        $query = $this->db->get('mfpasien');

        if ($query->num_rows() > 0) {
            $data = $query->row();
            echo json_encode($data);
        } else {
            echo json_encode(['error' => 'Data tidak ditemukan']);
        }
    }

    public function updatePatientData() {
        $patient_id = $this->input->post('patient_id');
        $tanggal_lahir = YMDFormat($this->input->post('tanggal_lahir'));
        $ktp = $this->input->post('ktp');
        $nik = $this->input->post('nik');

        $data = array(
            'tanggal_lahir' => $tanggal_lahir,
            'ktp' => $ktp,
            'nik' => $nik
        );

        $this->db->where('id', $patient_id);

        if($this->db->update('mfpasien', $data)) {
            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false]);
        }
    }

    public function process_all_by_nik() {
        $startDate = YMDFormat($this->input->post('start_date'));
        $endDate = YMDFormat($this->input->post('end_date'));

        // Ambil semua pasien dalam rentang tanggal
        $patients = $this->model->getPatientsByDateRange($startDate, $endDate);
        $results = []; // Menyimpan hasil proses

        foreach ($patients as $patient) {
            $nik = $patient->nik; // Ambil NIK

            if (!empty($nik)) {
                $this->load->library('SatuSehat/PatientLib', null, 'SatuSehatPatient');
                $result = $this->SatuSehatPatient->searchByNIK($nik);
                $data_satu_sehat = $result ? json_decode($result)->entry[0]->resource : [];

                if (isset($data_satu_sehat->id)) {
                    $status = 'SUCCESS';
                } else {
                    $status = 'FAILED';
                    $results[] = "NIK $nik not found in SatuSehat"; // Simpan pesan gagal
                }
            } else {
                $results[] = "NIK for patient ID {$patient->id} is empty"; // Simpan pesan NIK kosong
                $status = 'FAILED';
            }

            $this->model->saveHistory($patient->id, 'BY NIK', $nik, $status);
        }

        // Menyimpan pesan sukses atau gagal setelah semua proses
        if (!empty($results)) {
            $this->session->set_flashdata('message_flash', implode('<br>', $results));
        } else {
            $this->session->set_flashdata('message_flash', 'All patients processed successfully by NIK');
        }

        redirect('SatuSehat/patient'); // Redirect setelah selesai memproses semua pasien
    }

    public function process_all_by_ktp() {
        $startDate = YMDFormat($this->input->post('start_date'));
        $endDate = YMDFormat($this->input->post('end_date'));

        // Ambil semua pasien dalam rentang tanggal
        $patients = $this->model->getPatientsByDateRange($startDate, $endDate);
        $results = []; // Menyimpan hasil proses

        foreach ($patients as $patient) {
            $ktp = $patient->ktp; // Ambil KTP
            
            if (!empty($ktp)) {
                $this->load->library('SatuSehat/PatientLib', null, 'SatuSehatPatient');
                $result = $this->SatuSehatPatient->searchByNIK($ktp);
                $data_satu_sehat = $result ? json_decode($result)->entry[0]->resource : [];

                if (isset($data_satu_sehat->id)) {
                    $status = 'SUCCESS';
                } else {
                    $status = 'FAILED';
                    $results[] = "KTP $ktp not found in SatuSehat"; // Simpan pesan gagal
                }
            } else {
                $results[] = "KTP for patient ID {$patient->id} is empty"; // Simpan pesan KTP kosong
                $status = 'FAILED';
            }

            $this->model->saveHistory($patient->id, 'BY KTP', $ktp, $status);
        }

        // Menyimpan pesan sukses atau gagal setelah semua proses
        if (!empty($results)) {
            $this->session->set_flashdata('message_flash', implode('<br>', $results));
        } else {
            $this->session->set_flashdata('message_flash', 'All patients processed successfully by KTP');
        }

        redirect('SatuSehat/patient'); // Redirect setelah selesai memproses semua pasien
    }

    public function process_all_by_data() {
        $startDate = YMDFormat($this->input->post('start_date'));
        $endDate = YMDFormat($this->input->post('end_date'));

        // Ambil semua pasien dalam rentang tanggal
        $patients = $this->model->getPatientsByDateRange($startDate, $endDate);
        $results = []; // Menyimpan hasil proses

        foreach ($patients as $patient) {
            $name = $patient->name; // Ambil nama
            $birthdate = $patient->birth_date; // Ambil tanggal lahir
            $gender = $patient->gender; // Ambil jenis kelamin

            if ($name != '' && $birthdate != '' && $gender != '') {
                $this->load->library('SatuSehat/PatientLib', null, 'SatuSehatPatient');
                $result = $this->SatuSehatPatient->searchByNameGenderBirthdate($name, $birthdate, $gender);
                $data_satu_sehat = $result ? json_decode($result)->entry[0]->resource : [];

                if (isset($data_satu_sehat->id)) {
                    $status = 'SUCCESS';
                } else {
                    $status = 'FAILED';
                    $results[] = "Data for patient $name not found in SatuSehat"; // Simpan pesan gagal
                }
            } else {
                $results[] = "Incomplete data for patient ID {$patient->id}"; // Simpan pesan data tidak lengkap
                $status = 'FAILED';
            }

            $this->model->saveHistory($patient->id, 'BY DATA', "$name, $birthdate, $gender", $status);
        }

        // Menyimpan pesan sukses atau gagal setelah semua proses
        if (!empty($results)) {
            $this->session->set_flashdata('message_flash', implode('<br>', $results));
        } else {
            $this->session->set_flashdata('message_flash', 'All patients processed successfully by Data');
        }

        redirect('SatuSehat/patient'); // Redirect setelah selesai memproses semua pasien
    }
}
