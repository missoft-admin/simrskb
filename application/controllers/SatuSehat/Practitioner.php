<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Practitioner extends CI_Controller
{
    /**
     * Practitioner controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('SatuSehat/Practitioner_model', 'model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Practitioner';
        $data['content'] = '_SatuSehat/Practitioner/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Practitioner', '#'],
            ['List', 'SatuSehat/practitioner'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->model->getSpecified($id);

            $nik = $row->ppa_nik;
            
            // Satu Sehat Practitioner
            $this->load->library('SatuSehat/PractitionerLib', null, 'SatuSehatPractitioner');
            $result = $this->SatuSehatPractitioner->searchByNIK($nik);
            $data_satu_sehat = isset(json_decode($result)->entry[0]) ? json_decode($result)->entry[0]->resource : [];
						
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'ppa_nik' => $row->ppa_nik,
                    'ppa_nama' => $row->ppa_nama,
                    'practitioner_id' => isset($data_satu_sehat->id) ? $data_satu_sehat->id : null,
                    'practitioner_name' => isset($data_satu_sehat->name[0]->text) ? $data_satu_sehat->name[0]->text : null,
                    'practitioner_gender' => isset($data_satu_sehat->gender) ? $data_satu_sehat->gender : null,
                    'practitioner_birthdate' => isset($data_satu_sehat->birthDate) ? $data_satu_sehat->birthDate : null,
                    'satu_sehat_response' => $result,
                ];

                $data['error'] = '';
                $data['title'] = 'Practitioner';
                $data['content'] = '_SatuSehat/Practitioner/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Practitioner', '#'],
                    ['Update', 'SatuSehat/practitioner'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('SatuSehat/practitioner', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('SatuSehat/practitioner');
        }
    }

    public function save(): void
    {
        $this->form_validation->set_rules('practitioner_id', 'Practitioner ID', 'trim|required');

        if (true === $this->form_validation->run()) {
            if ($this->model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('SatuSehat/practitioner', 'location');
            }
        } else {
            $this->failed_save();
        }
    }

    public function failed_save(): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = '_SatuSehat/Practitioner/manage';

        $data['title'] = 'Practitioner';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Practitioner', '#'],
            ['Update', 'SatuSehat/practitioner'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex(): void
    {
        $this->select = [];

        $where = '';

		if ($this->input->post('status_practitioner') != '-') {
			$where = " WHERE IF ( satu_sehat_practitioner.practitioner_id, 1, 0 ) = '" . $this->input->post('status_practitioner') . "'";
		}

        $this->from = '(
				SELECT
					mppa.id,
					mppa.nama,
					satu_sehat_practitioner.practitioner_id,
                    mppa.nik AS reference_id,
                    mppa.nama AS reference_name,
                    DATE(mppa.tanggal_lahir) AS reference_born_date,
				IF
					( satu_sehat_practitioner.practitioner_id, 1, 0 ) AS status_connect
				FROM
					mppa
					LEFT JOIN satu_sehat_practitioner ON satu_sehat_practitioner.ppa_id = mppa.id
                '.$where.'
			) as result';

        $this->join = [];

        $this->where = [];

        $this->order = [
            'nama' => 'ASC',
        ];

        $this->group = [];

        $this->column_search = ['reference_name'];
        $this->column_order = ['reference_name'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = $r->practitioner_id;
            $row[] = 'NIK: ' . $r->reference_id . '<br>Nama: '  . $r->reference_name . '<br>Tanggal Lahir: '  . $r->reference_born_date;
            $row[] = StatusConnect($r->status_connect);
            $row[] = '<a href="'.site_url().'SatuSehat/practitioner/update/'.$r->id.'" data-toggle="tooltip" title="Update" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }
}
