<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Location extends CI_Controller
{
    /**
     * Location controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('SatuSehat/Location_model', 'model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Location';
        $data['content'] = '_SatuSehat/Location/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Location', '#'],
            ['List', 'SatuSehat/location'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'organization_id' => '',
            'location_id' => '',
            'location_status' => '',
            'location_identifier' => '',
            'location_name' => '',
            'location_description' => '',
            'telecom_phone' => '',
            'telecom_fax' => '',
            'telecom_email' => '',
            'telecom_url' => '',
            'address_line' => '',
            'address_city' => '',
            'address_postal_code' => '',
            'address_country' => '',
            'address_province_id' => '',
            'address_city_id' => '',
            'address_district_id' => '',
            'address_village_id' => '',
            'address_rt' => '0',
            'address_rw' => '0',
            'physical_type_code' => '',
            'physical_type_name' => '',
            'position_longitude' => '0',
            'position_latitude' => '0',
            'reference_location_id' => '',
            'satu_sehat_response' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Location';
        $data['content'] = '_SatuSehat/Location/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Location', '#'],
            ['Create New', 'SatuSehat/location'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->model->getSpecified($id);

            $location_id = $row->location_id;
            
            // Satu Sehat Location
            $this->load->library('SatuSehat/LocationLib', null, 'SatuSehatLocation');
            $result = $this->SatuSehatLocation->getById($location_id);
						
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'organization_id' => $row->organization_id,
                    'location_id' => $row->location_id,
                    'location_status' => $row->location_status,
                    'location_identifier' => $row->location_identifier,
                    'location_name' => $row->location_name,
                    'location_description' => $row->location_description,
                    'telecom_phone' => $row->telecom_phone,
                    'telecom_fax' => $row->telecom_fax,
                    'telecom_email' => $row->telecom_email,
                    'telecom_url' => $row->telecom_url,
                    'address_line' => $row->address_line,
                    'address_city' => $row->address_city,
                    'address_postal_code' => $row->address_postal_code,
                    'address_country' => $row->address_country,
                    'address_province_id' => $row->address_province_id,
                    'address_city_id' => $row->address_city_id,
                    'address_district_id' => $row->address_district_id,
                    'address_village_id' => $row->address_village_id,
                    'address_rt' => $row->address_rt,
                    'address_rw' => $row->address_rw,
                    'physical_type_code' => $row->physical_type_code,
                    'physical_type_name' => $row->physical_type_name,
                    'position_longitude' => $row->position_longitude,
                    'position_latitude' => $row->position_latitude,
                    'reference_location_id' => $row->reference_location_id,
                    'satu_sehat_response' => $result,
                ];

                $data['error'] = '';
                $data['title'] = 'Location';
                $data['content'] = '_SatuSehat/Location/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Location', '#'],
                    ['Update', 'SatuSehat/location'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('SatuSehat/location', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('SatuSehat/location');
        }
    }

    public function save(): void
    {
        $this->load->library('SatuSehat/LocationLib', null, 'SatuSehatLocation');

        $locationData = [
            'organization_id' => $this->input->post('organization_id'),
            'location_status' => $this->input->post('location_status'),
            'location_identifier' => $this->input->post('location_identifier'),
            'location_name' => $this->input->post('location_name'),
            'location_description' => $this->input->post('location_description'),
            'telecom_phone' => $this->input->post('telecom_phone'),
            'telecom_fax' => $this->input->post('telecom_fax'),
            'telecom_email' => $this->input->post('telecom_email'),
            'telecom_url' => $this->input->post('telecom_url'),
            'address_line' => $this->input->post('address_line'),
            'address_city' => $this->input->post('address_city'),
            'address_postal_code' => $this->input->post('address_postal_code'),
            'address_country' => $this->input->post('address_country'),
            'address_province_id' => $this->input->post('address_province_id'),
            'address_city_id' => $this->input->post('address_city_id'),
            'address_district_id' => $this->input->post('address_district_id'),
            'address_village_id' => $this->input->post('address_village_id'),
            'address_rt' => $this->input->post('address_rt'),
            'address_rw' => $this->input->post('address_rw'),
            'physical_type_code' => $this->input->post('physical_type_code'),
            'physical_type_name' => $this->input->post('physical_type_name'),
            'position_longitude' => $this->input->post('position_longitude'),
            'position_latitude' => $this->input->post('position_latitude'),
            'reference_location_id' => $this->input->post('reference_location_id'),
        ];

        if ($this->input->post('id') == '') {
            $response = $this->SatuSehatLocation->create($locationData);
        } else {
            $locationId = $this->input->post('location_id');
            $response = $this->SatuSehatLocation->update($locationId, $locationData);
        }

        $responseDecode = json_decode($response);
        if (isset($responseDecode->id) && $responseDecode->id) {
            $this->model->saveData(array_merge($locationData, ['location_id' => $responseDecode->id]));
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('SatuSehat/location', 'location');
        } else {
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output($response)
                ->_display()
            ;
    
            exit;
        }
    }

    public function getIndex(): void
    {
        $this->select = [];

        $where = '';

		if ($this->input->post('status_location') != '-') {
			$where = " WHERE satu_sehat_location.location_status = '" . $this->input->post('status_location') . "'";
		}

        $this->from = '(
				SELECT
                    *
				FROM
					satu_sehat_location
                '.$where.'
			) as result';

        $this->join = [];

        $this->where = [];

        $this->order = [
            'id' => 'DESC',
        ];

        $this->group = [];

        $this->column_search = ['nama'];
        $this->column_order = ['nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->organization_id;
            $row[] = $r->location_id;
            $row[] = $r->location_name;
            $row[] = $r->location_description;
            $row[] = StatusActive($r->location_status);
            $row[] = '<a href="'.site_url().'SatuSehat/location/update/'.$r->id.'" data-toggle="tooltip" title="Update" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }

    function getOrganization($organization_id){
	    $this->db->where('organization_id', $organization_id);
	    $query = $this->db->get('satu_sehat_organization');
        
        $data = $query->row();

        if ($data) {
            $response = [
                'status' => true,
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }
}
