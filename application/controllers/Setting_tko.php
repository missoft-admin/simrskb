<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_tko extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_tko_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('2285'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('2286'))){
				$tab='2';
			}
		}
		if (UserAccesForm($user_acces_form,array('2285','2286'))){
			$data = $this->Setting_tko_model->get_assesmen_setting();
			// $data_label = $this->Setting_tko_model->get_assesmen_label();
			// $data_user = $this->Setting_tko_model->get_assesmen_user();
			// print_r($data_spesifik);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Laporan Pembedahan';
			$data['content'] 		= 'Setting_tko/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Pengkajian Laporan Pembedahan Setting",'setting_tko/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_tarif()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
						,KP.nama as nama_kelompok_pasien,KO.nama as nama_kelompok_operasi
						,J.ref as nama_jenis_operasi,B.ref as nama_jenis_bedah,A.ref as nama_jenis_anestesi,MR.nama as nama_rekanan
						,JO.nama as tarif_1,FC.nama as tarif_2,SA.nama as tarif_3
							FROM `setting_tko_tarif_bedah` H
							LEFT JOIN mkelompok_operasi KO ON KO.id=H.kelompok_opr
							LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
							LEFT JOIN mjenis_operasi JO ON JO.id=H.tarif_jenis_opr
							LEFT JOIN mtarif_rawatinap FC ON FC.id=H.taif_full_care
							LEFT JOIN mtarif_operasi_sewaalat SA ON SA.id=H.tarif_sewa_alat
							LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
							LEFT JOIN merm_referensi J ON J.nilai=H.jenis_opr AND J.ref_head_id='124'
							LEFT JOIN merm_referensi B ON B.nilai=H.jenis_bedah AND B.ref_head_id='389'
							LEFT JOIN merm_referensi A ON A.nilai=H.jenis_anestesi AND A.ref_head_id='394'
							ORDER BY H.idkelompokpasien,H.idrekanan,H.kelompok_opr,H.jenis_opr,H.jenis_bedah,H.jenis_anestesi



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nama_kelompok_pasien);
          $result[] = ($r->idrekanan!='0'?$r->nama_rekanan:text_default('SEMUA'));
          $result[] = ($r->kelompok_opr!='0'?$r->nama_kelompok_operasi:text_default('SEMUA'));
          $result[] = ($r->jenis_opr!='0'?$r->nama_jenis_operasi:text_default('SEMUA'));
          $result[] = ($r->jenis_bedah!='0'?$r->nama_jenis_bedah:text_default('SEMUA'));
          $result[] = ($r->jenis_anestesi!='0'?$r->nama_jenis_anestesi:text_default('SEMUA'));
          $result[] = ($r->tarif_1);
          $result[] = ($r->tarif_2);
          $result[] = ($r->tarif_3);
		  // if (UserAccesForm($user_acces_form,array('1603'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_tarif('.$r->id.')" type="button" title="Edit Logic" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_tarif('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  // }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function edit_tarif(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM setting_tko_tarif_bedah H WHERE id='$id'";
	  $data=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($data));
  }
	function save_assesmen(){
		$data=array(
			'def_kel_opr' => $this->input->post('def_kel_opr'),
			'def_jenis_opr' => $this->input->post('def_jenis_opr'),
			'def_jenis_bedah' => $this->input->post('def_jenis_bedah'),
			'def_jenis_anes' => $this->input->post('def_jenis_anes'),
			'st_kelas_tarif' => $this->input->post('st_kelas_tarif'),
			'st_pilih_tarif_jenis_opr' => $this->input->post('st_pilih_tarif_jenis_opr'),
			'auto_input_sr' => $this->input->post('auto_input_sr'),
			'edit_jenis_opr_sr' => $this->input->post('edit_jenis_opr_sr'),
			'edit_kelas_tarif_sr' => $this->input->post('edit_kelas_tarif_sr'),
			'pilih_discount_sr' => $this->input->post('pilih_discount_sr'),
			'auto_input_fc' => $this->input->post('auto_input_fc'),
			'edit_jenis_opr_fc' => $this->input->post('edit_jenis_opr_fc'),
			'edit_kelas_tarif_fc' => $this->input->post('edit_kelas_tarif_fc'),
			'pilih_discount_fc' => $this->input->post('pilih_discount_fc'),
			'do_jenis_opr' => $this->input->post('do_jenis_opr'),
			'do_kelas_tarif' => $this->input->post('do_kelas_tarif'),
			'do_nama_tarif' => $this->input->post('do_nama_tarif'),
			'do_pilih_operator' => $this->input->post('do_pilih_operator'),
			'do_pilih_disc_operator' => $this->input->post('do_pilih_disc_operator'),
			'da_persen' => $this->input->post('da_persen'),
			'da_edit_persen' => $this->input->post('da_edit_persen'),
			'da_pilih_diskon' => $this->input->post('da_pilih_diskon'),
			'ao_persen' => $this->input->post('ao_persen'),
			'ao_edit_persen' => $this->input->post('ao_edit_persen'),
			'ao_pilih_diskon' => $this->input->post('ao_pilih_diskon'),
			'aa_persen' => $this->input->post('aa_persen'),
			'aa_edit_persen' => $this->input->post('aa_edit_persen'),
			'aa_pilih_diskon' => $this->input->post('aa_pilih_diskon'),
			'peggunaan_narcose' => $this->input->post('peggunaan_narcose'),
			'peggunaan_obat' => $this->input->post('peggunaan_obat'),
			'peggunaan_alkes' => $this->input->post('peggunaan_alkes'),
			'peggunaan_implan' => $this->input->post('peggunaan_implan'),
			'sa_jenis_opr' => $this->input->post('sa_jenis_opr'),
			'sa_kelas_tarif' => $this->input->post('sa_kelas_tarif'),
			'sa_nama_tarif' => $this->input->post('sa_nama_tarif'),
			'sa_pilih_operator' => $this->input->post('sa_pilih_operator'),
			'sa_pilih_disc_operator' => $this->input->post('sa_pilih_disc_operator'),
		);
		
		$hasil=$this->db->update('setting_tko',$data);
		
		$this->output->set_output(json_encode($data));
		
		// if ($this->Setting_tko_model->save_assesmen()==true){
			// $this->session->set_flashdata('confirm',true);
			// $this->session->set_flashdata('message_flash','data telah disimpan.');
			// redirect('setting_tko/index/1','location');
		// }
	}
	
	
  function simpan_tarif(){
		$hasil=null;
		$iddet = $this->input->post('iddet');
		$data=array(
			'idkelompokpasien' => $this->input->post('idkelompokpasien'),
			'idrekanan' => $this->input->post('idrekanan'),
			'kelompok_opr' => $this->input->post('kelompok_opr'),
			'jenis_opr' => $this->input->post('jenis_opr'),
			'jenis_bedah' => $this->input->post('jenis_bedah'),
			'jenis_anestesi' => $this->input->post('jenis_anestesi'),
			'tarif_jenis_opr' => $this->input->post('tarif_jenis_opr'),
			'taif_full_care' => $this->input->post('taif_full_care'),
			'tarif_sewa_alat' => $this->input->post('tarif_sewa_alat'),
		);
		
		$hasil=$this->cek_duplicate_tarif($data,$iddet);
		if ($hasil==null){	
			if ($iddet){
				$this->db->where('id',$iddet);
				$hasil=$this->db->update('setting_tko_tarif_bedah',$data);
			}else{
				$data['created_by'] = $this->session->userdata('user_id');
				$data['created_date']= date('Y-m-d H:i:s');
				$hasil=$this->db->insert('setting_tko_tarif_bedah',$data);
			}
			
		
			
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_tarif($data,$iddet){
		$q="SELECT *FROM setting_tko_tarif_bedah WHERE  
			idkelompokpasien = ".$data['idkelompokpasien']." AND 
			idrekanan = ".$data['idrekanan']." AND 
			kelompok_opr = ".$data['kelompok_opr']." AND 
			jenis_opr = ".$data['jenis_opr']." AND 
			jenis_bedah = ".$data['jenis_bedah']." AND 
			jenis_anestesi = ".$data['jenis_anestesi']." AND 
			tarif_jenis_opr = ".$data['tarif_jenis_opr']." AND 
			taif_full_care = ".$data['taif_full_care']." AND 
			tarif_sewa_alat = ".$data['tarif_sewa_alat']." AND id!='".$iddet."'
		";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
  function hapus_tarif(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_tko_tarif_bedah',$this);
	  
	  json_encode($hasil);
	  
  }
  
  
}
