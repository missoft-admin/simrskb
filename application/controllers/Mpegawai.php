<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpegawai extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpegawai_model');
		$this->load->helper('path');
		
  }

	function index($idkategori='0',$jeniskelamin='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['idkategori'] 			= $idkategori;
			$data['jeniskelamin'] 			= $jeniskelamin;
			$data['error'] 			= '';
			$data['title'] 			= 'Pegawai';
			$data['content'] 		= 'Mpegawai/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pegawai",'#'),
												  array("List",'mpegawai')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nip' 					=> '',
			'nip_hrms' 					=> '',
			'nama' 					=> '',
			'jeniskelamin' 	=> '',
			'alamat' 				=> '',
			'tempatlahir' 	=> '',
			'tanggallahir' 	=> '',
			'telepon' 	    => '',
			'idkategori' 		=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pegawai';
		$data['content'] 		= 'Mpegawai/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Pegawai",'#'),
								            array("Tambah",'mpegawai')
								        	);

		$data['statusAvailableApoteker'] = $this->Mpegawai_model->getStatusAvailableApoteker();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mpegawai_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nip' 					=> $row->nip,
					'nip_hrms' 					=> $row->nip_hrms,
					'nama' 					=> $row->nama,
					'jeniskelamin' 	=> $row->jeniskelamin,
					'alamat' 				=> $row->alamat,
					'tempatlahir' 	=> $row->tempatlahir,
					'tanggallahir' 	=> DMYFormat($row->tanggallahir),
					'telepon' 			=> $row->telepon,
					'idkategori' 		=> $row->idkategori,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Pegawai';
				$data['content']    = 'Mpegawai/manage';
				$data['breadcrum'] 	= array(
						                    array("RSKB Halmahera",'#'),
						                    array("Pegawai",'#'),
						                    array("Ubah",'mpegawai')
						                	);

				$data['statusAvailableApoteker'] = $this->Mpegawai_model->getStatusAvailableApoteker();
				
				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpegawai','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpegawai');
		}
	}

	function delete($id){
		
		$this->Mpegawai_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mpegawai','location');
	}

	function save(){
		$this->form_validation->set_rules('nip', 'NIP', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('jeniskelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('tempatlahir', 'Tempat Lahir', 'trim|required');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');
		$this->form_validation->set_rules('idkategori', 'Kategori', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mpegawai_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpegawai','location');
				}
			} else {
				if($this->Mpegawai_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpegawai','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpegawai/manage';

		if($id==''){
			$data['title'] = 'Tambah Pegawai';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pegawai",'#'),
							               array("Tambah",'mpegawai')
								           );
		}else{
			$data['title'] = 'Ubah Pegawai';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pegawai",'#'),
							               array("Ubah",'mpegawai')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function filter(){
			$idkategori = $this->input->post('idkategori');
			$jeniskelamin = $this->input->post('jeniskelamin');
			redirect("mpegawai/index/$idkategori/$jeniskelamin",'location');
	}
	function getIndex($idkategori='0',$jeniskelamin='0')
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			
			$this->select = array();
			$this->from   = 'mpegawai';
			$this->join 	= array();
			if ($idkategori !='0' && $jeniskelamin=='0'){
					$this->where  = array(
					'idkategori' => $idkategori,
					'status' => '1'
				);
			}
			if ($idkategori =='0' && $jeniskelamin!='0'){
					$this->where  = array(
					'jeniskelamin' => $jeniskelamin,
					'status' => '1'
					);
			}
			if ($idkategori !='0' && $jeniskelamin!='0'){
					$this->where  = array(
					'idkategori' => $idkategori,
					'jeniskelamin' => $jeniskelamin,
					'status' => '1'
					);
			}
			if ($idkategori =='0' && $jeniskelamin=='0'){
					$this->where  = array(
						'status' => '1'
					);
			}
			
			
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

			if (UserAccesForm($user_acces_form,array('249'))){
				$this->column_search = array('nip','nama');
			}else{
				$this->column_search = array();				
			}
			$this->column_order  = array('nip','nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $row) {
          $no++;
          $result = array();
		  $btn_nip='<br>';
		  if ($row->nip_hrms){
			  $btn_nip .='<p class="text-primary">'.$row->nip_hrms.'</p>';
		  }else{
			$btn_nip .= '<button type="button" data-toggle="tooltip" onclick="show_pegawai('.$row->id.')" title="Ubah" class="btn btn-success btn-xs"><i class="si si-link"></i> Belum Terhubung</button>';
			  
		  }
          $result[] = $no;
          $result[] = $row->nip.$btn_nip;
          $result[] = $row->nama.($row->nama_hrms?'<br><p class="text-primary">'.$row->nama_hrms.'</p>':'');
          $result[] = GetJenisKelamin($row->jeniskelamin);
          $result[] = GetKategoriPegawai($row->idkategori);
          $aksi = '<div class="btn-group">';
          if (UserAccesForm($user_acces_form,array('4'))){
          	$aksi .= '<a href="'.site_url().'mpegawai/update/'.$row->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          if (UserAccesForm($user_acces_form,array('5'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mpegawai" data-urlremove="'.site_url().'mpegawai/delete/'.$row->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_list_pegawai()
    {
	  // $idvendor=$this->input->post('idvendor');
	  
	  $from="(
				SELECT * FROM (
					SELECT H.nip,H.nama,H.namadepartemen,H.unit,H.namajabatan,M.nip_hrms FROM view_m03pegawai H
					LEFT JOIN mpegawai M ON M.nip_hrms=H.nip
				) T WHERE T.nip_hrms IS NULL
			) as tbl";
	// print_r($tanggal_dibutuhkan1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nip','nama','unit');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
		 

					$aksi = '<div class="btn-group">';
					$aksi .= '<button data-toggle="tooltip" title="Pilih" class="btn btn-danger btn-xs pilih_pegawai"><i class="fa fa-check"></i> Pilih</button>';
					$aksi .= '</div>';
		
          $row[] = $no;
          $row[] = $r->nip;
          $row[] = $r->nama;
          $row[] = $r->namadepartemen;
          $row[] = $r->unit;
          $row[] = $r->namajabatan;
         
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_data_pegawai($id){
	  $q="SELECT H.nama,H.nip,H.nip_hrms FROM `mpegawai` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  function get_data_pegawai_hrms(){
	  $nip=$this->input->post('nip');
	  
	  $q="SELECT *FROM view_m03pegawai WHERE nip='$nip'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  function save_nip(){
	  $id=$this->input->post('idpegawai');
	  $data=array(
		'nip_hrms' => $this->input->post('nip_hrms'),
		'nama_hrms' => $this->input->post('nama_hrms'),
	  );
	  // print_r($data);exit;
	  $this->db->where('id',$id);
	  $output=$this->db->update('mpegawai',$data);
	  echo json_encode($output);
  }
  
}
