<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_ews extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_ews_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('2377'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('2379'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('2381'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('2382'))){
				$tab='4';
			}
		}
		if (UserAccesForm($user_acces_form,array('2377','2379','2381','2382'))){
			$data = $this->Setting_ews_model->get_ews_setting();
			// print_r($data);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting EWS';
			$data['content'] 		= 'Setting_ews/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("EWS",'setting_ews/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_ews(){
		if ($this->Setting_ews_model->save_ews()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_ews/index/1','location');
		}
	}
	function save_ews_label(){
		if ($this->Setting_ews_model->save_ews_label()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_ews/index/3','location');
		}
	}

  function simpan_hak_akses(){
		$hasil=$this->cek_duplicate_hak_akses($this->input->post('profesi_id'),$this->input->post('spesialisasi_id'),$this->input->post('mppa_id'));
		if ($hasil==null){	
			$this->profesi_id=$this->input->post('profesi_id');
			$this->spesialisasi_id=$this->input->post('spesialisasi_id');
			$this->mppa_id=$this->input->post('mppa_id');
			$this->st_lihat=$this->input->post('st_lihat');
			$this->st_input=$this->input->post('st_input');
			$this->st_edit=$this->input->post('st_edit');
			$this->st_hapus=$this->input->post('st_hapus');
			$this->st_cetak=$this->input->post('st_cetak');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_ews_akses',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_hak_akses($profesi_id,$spesialisasi_id,$mppa_id){
		$q="SELECT *FROM setting_ews_akses WHERE profesi_id='$profesi_id' AND spesialisasi_id='$spesialisasi_id' AND mppa_id='$mppa_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
	function load_hak_akses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.profesi_id,P.ref as profesi
							,H.spesialisasi_id,CASE WHEN H.spesialisasi_id=0 THEN 'SEMUA' ELSE S.ref END as spesialisasi 
							,H.mppa_id,M.nama as nama_ppa,H.st_lihat,H.st_input,H.st_edit,H.st_hapus,H.st_cetak
							FROM `setting_ews_akses` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							LEFT JOIN merm_referensi S ON S.nilai=H.spesialisasi_id AND S.ref_head_id='22'  AND S.`status`='1'
							LEFT JOIN mppa M ON M.id=H.mppa_id AND M.staktif='1'
							ORDER BY H.profesi_id,H.spesialisasi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = ($r->spesialisasi_id=='0'?text_default($r->spesialisasi):$r->spesialisasi);
          $result[] = ($r->mppa_id=='0'?text_default('SEMUA'):$r->nama_ppa);
          $result[] = ($r->st_lihat?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_input?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_edit?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_hapus?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_cetak?'IZINKAN':text_danger('TIDAK'));
		  if (UserAccesForm($user_acces_form,array('1603'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_hak_akses('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_hak_akses(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_ews_akses',$this);
	  
	  json_encode($hasil);
	  
  }
	function simpan_parameter(){
		for ($i=1;$i<=10;$i++){
			
		  $data=array(
			'nourut' => $this->input->post('nourut_'.$i),
			'nama_ews' => $this->input->post('nama_'.$i),
			'staktif' => $this->input->post('staktif_'.$i),
		  );
			$hasil=$this->db->update('ews_master',$data,array('id'=>$i));
		}

		$this->output->set_output(json_encode($hasil));

	}

  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function load_skor()
 {
	$data_user=get_acces();
	$user_acces_form=$data_user['user_acces_form'];
	$ews_id=$this->input->post('ews_id');
	if ($ews_id=='1'){
	$list_opsi=list_variable_ref(421);
		
	}
	$this->select = array();
	$from="
			(
				SELECT H.*
					FROM `ews_master_skor` H
					where H.staktif='1'
					AND H.ews_id='$ews_id'
					ORDER BY H.nilai_1


			) as tbl
		";
	// print_r($from);exit();
	$this->from   = $from;
	$this->join 	= array();
	$this->order  = array();
	$this->group  = array();
	$this->column_search = array('nilai_1','nilai_2','skor');
	$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
			 $nilai=(int)$r->nilai_1;
		  if ($r->ews_id=='1'){
          $result[] = get_nama_ref($nilai,23);
		  }elseif ($r->ews_id=='10'){
          $result[] = get_nama_ref($nilai,421);
		  }else{
          $result[] = ($r->nilai_1.' - '.$r->nilai_2);
			  
		  }
          $result[] = $r->skor;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.','.$r->ews_id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.','.$r->ews_id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function simpan_skor(){
		$skor_id=$this->input->post('skor_id');
		$data=array(
			'ews_id'=>$this->input->post('ews_id'),
			'nilai_1'=>$this->input->post('nilai_1'),
			'nilai_2'=>$this->input->post('nilai_2'),
			'skor'=>$this->input->post('skor'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('ews_master_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('ews_master_skor',$data);
		}
		  
		json_encode($hasil);
	}
	
	function hapus_nilai(){
	  $id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$hasil=$this->db->update('ews_master_skor',$data);
	  
	  json_encode($hasil);
	  
  }
  
  function find_skor(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM ews_master_skor H WHERE H.id='$id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  function simpan_hasil_skor(){
		$skor_id=$this->input->post('id');
		$data=array(
			'hasil_skor'=>$this->input->post('hasil_skor'),
			'nilai_1'=>$this->input->post('nilai_1'),
			'nilai_2'=>$this->input->post('nilai_2'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('ews_master_hasil_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('ews_master_hasil_skor',$data);
		}
		  
		json_encode($hasil);
	}
	function load_hasil_skor()
 {
	$data_user=get_acces();
	$user_acces_form=$data_user['user_acces_form'];
	
	$this->select = array();
	$from="
			(
				SELECT H.*
					FROM `ews_master_hasil_skor` H
					where H.staktif='1'
					ORDER BY H.nilai_1


			) as tbl
		";
	// print_r($from);exit();
	$this->from   = $from;
	$this->join 	= array();
	$this->order  = array();
	$this->group  = array();
	$this->column_search = array('nilai_1','nilai_2','skor');
	$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		
		  $result[] = ($r->nilai_1.' - '.$r->nilai_2);
		  $result[] = get_nama_ref($r->hasil_skor,422);
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_hasil_skor('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_hasil_skor('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function find_hasil_skor(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM ews_master_hasil_skor H WHERE H.id='$id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_hasil_skor(){
	  $id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$hasil=$this->db->update('ews_master_hasil_skor',$data);
	  
	  json_encode($hasil);
	  
  }
  function simpan_hasil_parameter(){
		$parameter_id=$this->input->post('id');
		$data=array(
			'hasil_parameter'=>$this->input->post('hasil_parameter'),
			'jumlah_param'=>$this->input->post('jumlah_param'),
			'skor_param'=>$this->input->post('skor_param'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('ews_master_hasil_parameter',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('ews_master_hasil_parameter',$data);
		}
		  
		json_encode($hasil);
	}
	function load_hasil_parameter()
 {
	$data_user=get_acces();
	$user_acces_form=$data_user['user_acces_form'];
	
	$this->select = array();
	$from="
			(
				SELECT H.*
					FROM `ews_master_hasil_parameter` H
					where H.staktif='1'
					ORDER BY H.jumlah_param


			) as tbl
		";
	// print_r($from);exit();
	$this->from   = $from;
	$this->join 	= array();
	$this->order  = array();
	$this->group  = array();
	$this->column_search = array('hasil_skor','skor_param','parameter');
	$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		
		  $result[] = ($r->jumlah_param);
		  $result[] = ($r->skor_param);
		  $result[] = get_nama_ref($r->hasil_parameter,422);
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_hasil_parameter('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_hasil_parameter('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function find_hasil_parameter(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM ews_master_hasil_parameter H WHERE H.id='$id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_hasil_parameter(){
	  $id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$hasil=$this->db->update('ews_master_hasil_parameter',$data);
	  
	  json_encode($hasil);
	  
  }
}
