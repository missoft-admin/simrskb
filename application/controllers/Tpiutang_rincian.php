<?php defined('BASEPATH') OR exit('No direct script access allowed');
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use Dompdf\Dompdf;
class Tpiutang_rincian extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_rincian_model','model');
		$this->load->model('Thonor_dokter_model');
		$this->load->model('Tvalidasi_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Trawatinap_verifikasi_model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$date=date_create($hari_ini);
		date_add($date,date_interval_create_from_date_string("-30 days"));
		$tgl_pertama = date_format($date,"d-m-Y");

		$date=date_create($hari_ini);
		date_add($date,date_interval_create_from_date_string("30 days"));
		$tgl_terakhir = date_format($date,"d-m-Y");

		// $tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		// $tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tanggal_tagihan'=>$tgl_pertama,
			'tanggal_tagihan2'=>$tgl_terakhir,
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_deskripsi'] 	= $this->model->list_deskripsi();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Rincian Tagihan';
		$data['content'] 		= 'Tpiutang_rincian/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Rincian Tagihan",'tpiutang_rincian/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');

		$no_piutang=$this->input->post('no_piutang');
		$tanggal_id=$this->input->post('tanggal_id');
		$status=$this->input->post('status');
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');

		$where1='';
		$where2='';
		$where='';

		if ($no_piutang !=''){
			$where1 .=" AND H.no_piutang LIKE '%".$no_piutang."%' ";
		}

		if ($tanggal_id !='#'){
			$where1 .=" AND H.tanggal_id='$tanggal_id' ";
		}

		if ($status !='#'){
			$where1 .=" AND H.status_kirim='$status' ";
		}


		if ('' != $tanggal_tagihan) {
            $where1 .= " AND DATE(H.tanggal_tagihan) >='".YMDFormat($tanggal_tagihan)."' AND DATE(H.tanggal_tagihan) <='".YMDFormat($tanggal_tagihan2)."'";
        }

        $from = "(
					SELECT H.id,H.no_piutang,S.deskripsi,H.tanggal_id,H.idpengaturan,H.tanggal_tagihan,H.total_tagihan,H.`status`,H.jml_faktur,H.status_kirim from tpiutang H
					LEFT JOIN mpiutang_setting S ON S.id=H.tanggal_id
					WHERE H.`status`!='0' ".$where1."
				) as tbl  ORDER BY tanggal_tagihan ASC";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = $r->tanggal_id;
            $row[] = $no;
            $row[] = $r->no_piutang;
            $row[] = $r->deskripsi;
            $row[] = HumanDateShort($r->tanggal_tagihan);
            $row[] = number_format($r->total_tagihan,2);
            $row[] = ($r->status=='1' ? '<span class="label label-danger">BELUM DI PROSES</span>':'<span class="label label-success">TELAH DIPROSES</span>');


			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/0" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';

			if ($r->status_kirim=='0'){
			$aksi .= '<button title="Kirim"  class="btn btn-xs btn-primary kirim"><i class="fa fa-send"></i></button>';
			$aksi .= '<button  disabled title="Memajukan Tagihan"  class="btn btn-xs btn-danger ganti"><i class="si si-arrow-right"></i></button>';
			}

			// $aksi .= '<a title="Lihat Detail" href="'.$url.'print_rincian/'.$r->id.'" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
			$aksi .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Piutang Karyawan</li>
							<li><a tabindex="-1" href="'.$url.'print_rincian/'.$r->id.'" target="_blank"  >Rekapitulasi</a></li>
							<li><a tabindex="-1" href="'.$url.'print_rincian_all/'.$r->id.'" target="_blank">Rincian Per Orang</a></li>
							
						</ul>
					</div>';
			// $aksi .= '<button class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';



			$aksi.='</div>';
            $row[] = $aksi;
            $row[] = $r->idpengaturan;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_detail(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');



		$disabel=$this->input->post('disabel');
		$piutang_id=$this->input->post('piutang_id');
		$xtipepegawai=$this->input->post('xtipepegawai');
		$xidpeg_dok=$this->input->post('xidpeg_dok');
		$status=$this->input->post('status');

		$where1='';
		$group_by=' D.piutang_id,D.tipepegawai,D.idpegawai,D.iddokter';

		if ($xtipepegawai !='#'){
			$where1 .=" AND D.tipepegawai='$xtipepegawai' ";
		}
		if ($xidpeg_dok !=''){
			if ($xtipepegawai=='1'){
				$where1 .=" AND D.idpegawai='$xidpeg_dok' ";
				$group_by=" D.piutang_id,D.tipepegawai,D.idpegawai";
			}else{
				$where1 .=" AND D.iddokter='$xidpeg_dok' ";
				$group_by=" D.piutang_id,D.tipepegawai,D.iddokter";
			}
		}
		if ($status !='#'){
			$where1 .=" AND D.status_lunas='$status' ";
		}



        $from = "(
					SELECT D.id,D.piutang_id,D.tipepegawai,D.pembayaran_id,D.tanggal_tagihan,SUM(D.nominal) as nominal,COUNT(D.id) as jml_trx
					,CASE WHEN D.tipepegawai=1 THEN P.nama ELSE MD.nama END as atas_nama
					,CASE WHEN D.tipepegawai=1 THEN 'PEGAWAI' ELSE 'DOKTER' END as tipe_nama
					,D.iddokter,D.idpegawai,D.status_lunas,D.pendaftaran_id,D.kasir_id,D.tipe,SUM(CASE WHEN D.status_lunas=1 THEN 1 ELSE 0 END) as jml_lunas
					from tpiutang_detail D
					LEFT JOIN mpegawai P ON P.id=D.idpegawai AND D.tipepegawai='1'
					LEFT JOIN mdokter MD ON MD.id=D.iddokter AND D.tipepegawai='2'
					WHERE D.piutang_id='$piutang_id' ".$where1."
					GROUP BY ".$group_by."

				) as tbl ";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] =$r->tipe;
            $row[] = $no;
            $row[] = ($r->tipepegawai=='1' ? '<span class="label label-primary">'.$r->tipe_nama.'</span>':'<span class="label label-success">'.$r->tipe_nama.'</span>');
            $row[] = $r->atas_nama;
            $row[] = number_format($r->nominal,0);
            $row[] = $r->jml_trx.' Trx / '.$r->jml_lunas.' Lunas';
            // $row[] = $r->jml_trx.'-'.$r->jml_lunas;
            $row[] = ($r->jml_trx==$r->jml_lunas ? '<span class="label label-success">LUNAS</span>':'<span class="label label-danger">BELUM LUNAS</span>');
            $idpeg_dok='';
			if ($r->tipepegawai=='1'){
				$idpeg_dok=$r->idpegawai;
			}else{
				$idpeg_dok=$r->iddokter;
			}
			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_trx/'.$r->piutang_id.'/'.$r->tipepegawai.'/'.$idpeg_dok.'" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i></a>';
			if ($r->jml_trx!=$r->jml_lunas){
				if ($r->jml_trx=='1'){
				   $aksi .= '<button '.$disabel.' class="view btn btn-xs btn-primary lunaskan" type="button"  title="Lunaskan"><i class="fa fa-check"></i></button>';

				}
			}
			$aksi .= '<a title="Print Detail Tagihan" href="'.$url.'print_rincian_perorang/'.$r->piutang_id.'/'.$r->tipepegawai.'/'.$idpeg_dok.'" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
			// if ($r->tipe=='1'){
			// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			// }



			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_detail_trx(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');


		$tipe=$this->input->post('tipe');
		$disabel=$this->input->post('disabel');
		$piutang_id=$this->input->post('piutang_id');
		$tipepegawai=$this->input->post('tipepegawai');
		$iddokter=$this->input->post('iddokter');
		$idpegawai=$this->input->post('idpegawai');

		$where1='';

		$from = "(
					SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien
					,TD.nominal
					,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'RJ' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
					,TD.cicilan_ke
					from tpiutang_detail TD
					LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mpoliklinik M ON TP.idpoliklinik=M.id
					LEFT JOIN mdokter D ON D.id=TP.iddokter
					LEFT JOIN tkasir K ON K.id=TD.kasir_id
					WHERE TD.piutang_id='$piutang_id' AND TD.tipe='1' AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
					GROUP BY TP.id

					UNION ALL

					SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggal as tanggaldaftar,TP.nopenjualan as nopendaftaran,TP.nomedrec as no_medrec,TP.nama as namapasien
					,TD.nominal
					,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'Pembelian Obat' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
					,TD.cicilan_ke
					from tpiutang_detail TD
					LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
					LEFT JOIN tpasien_penjualan TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN tkasir K ON K.id=TD.kasir_id
					WHERE TD.piutang_id='$piutang_id' AND TD.tipe='3' AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
					GROUP BY TP.id

					UNION ALL

					SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien
					,TBD.nominal,TD.tanggal_tagihan,TD.tipe,TD.jml_cicilan,TB.tanggal as tanggal_kasir,'RI/ODS' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
					,TD.cicilan_ke
					from tpiutang_detail TD
					LEFT JOIN trawatinap_pendaftaran TP ON TP.id=TD.pendaftaran_id
				    LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.idtindakan=TP.id AND TB.statusbatal=0
				    LEFT JOIN trawatinap_tindakan_pembayaran_detail TBD ON TBD.idtindakan=TB.id
					WHERE TD.piutang_id='$piutang_id' AND TD.tipe='2'  AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
					GROUP BY TP.id

				) as tbl ";


        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
		// print_r($list);exit();


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = $r->tipe;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = HumanDateShort($r->tanggal_kasir);
			 $row[] = ($r->tipe=='1' ? '<span class="label label-primary">'.$r->tipe_nama.'</span>':'<span class="label label-success">'.$r->tipe_nama.'</span>');
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = ($r->cicilan_ke=='1' ? '<span class="label label-info">TAGIHAN BARU</span>':'<span class="label label-success">TAGIHAN LAMA</span>');
            $row[] = ($r->jml_cicilan=='1' ? '<span class="label label-danger">LANGSUNG</span>':'<span class="label label-primary">CICILAN '.$r->cicilan_ke.'/'.$r->jml_cicilan.'</span>');
            $row[] = number_format($r->nominal,0);
			$row[] = ($r->status_lunas=='1' ? '<span class="label label-success">LUNAS</span>':'<span class="label label-danger">BELUM LUNAS</span>');
            if ($r->status_lunas=='0'){
			// $aksi .= '<button title="Lunaskan Tagihan" '.$disabel.' class="btn btn-xs btn-primary lunaskan"><i class="fa fa-check"></i></button>';
			   $aksi .= '<button '.$disabel.' class="view btn btn-xs btn-primary lunaskan" type="button"  title="Lunaskan"><i class="fa fa-check"></i></button>';

			}
			$aksi .= '<button '.$disabel.' class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';

			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_trx/'.$r->piutang_id.'/'.$r->tipepegawai.'" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i></a>';
			// if ($r->tipe=='1'){
			// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			// }
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';

        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }

	function rincian_tagihan($id,$disabel='0'){

		$data=$this->model->detail($id);

		if ($disabel!='0'){
			$data['disabel'] 				= 'disabled';

		}else{
			$data['disabel'] 				= '';
		}
		$data['error'] 				= '';

			$data['title'] 				= 'Detail Piutang Karyawan';
			$data['content'] 			= 'Tpiutang_rincian/detail_rincian';
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'),
										array("piutang Rincian",'tpiutang_rincian/index'),
										array("List",'#')
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function rincian_trx($id,$tipe,$idpeg,$disabel='0'){

		$data=$this->model->detail_trx($id,$tipe,$idpeg);
		// print_r($data);exit();
		if ($disabel!='0'){
			$data['disabel'] 				= 'disabled';

		}else{
			$data['disabel'] 				= '';
		}
		$data['error'] 				= '';

			$data['title'] 				= 'Rincian Piutang Karyawan';
			$data['content'] 			= 'Tpiutang_rincian/rincian_trx';
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'),
										array("piutang Rincian",'tpiutang_rincian/index'),
										array("List",'#')
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function cek_date(){
		$tgl='2011-20-01';
		$tgl_asli=(int)substr($tgl,-2);
		print_r($tgl_asli);
	}

	public function verifikasi()
    {

		$where='';
        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $tipekontraktor = $this->input->post('tipekontraktor');
        $idkontraktor = $this->input->post('idkontraktor');
        $tanggal_jt =YMDFormat($this->input->post('tanggal_jt'));
		$tgl_angka=(int)substr($tanggal_jt,-2);
		// if ($tipe='1'){
			if ($tipekontraktor=='1'){
				$where .=" AND  K.idrekanan='$idkontraktor'";
			}
			$q="SELECT K.id from tpiutang K
				WHERE K.tipe='$tipe' AND K.idkelompokpasien='$tipekontraktor'  AND K.tanggal_tagihan='$tanggal_jt' AND status=1 ".$where;
			$piutang_id=$this->db->query($q)->row('id');
			if ($piutang_id==''){
				if ($tipekontraktor=='1'){
				$q_set="SELECT * FROM (
						SELECT '1' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mrekanan_setting M
						WHERE M.idrekanan='$idkontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

						UNION ALL

						SELECT '2' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mpasien_kelompok_setting M
						WHERE M.idkelompokpasien='$tipekontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

						) T ORDER BY T.tipe ASC LIMIT 1";
				}else{
					$q_set="SELECT * FROM (
						SELECT '2' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mpasien_kelompok_setting M
						WHERE M.idkelompokpasien='$tipekontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

						) T ORDER BY T.tipe ASC LIMIT 1";
				}
				$row=$this->db->query($q_set)->row();
				if ($row){
					$var_batas_kirim=$row->batas_kirim;
					$var_jml_hari=$row->jml_hari;

					$date=date_create($tanggal_jt);
					date_add($date,date_interval_create_from_date_string("$var_batas_kirim days"));
					$batas_kirim = date_format($date,"Y-m-d");

					$date2=date_create($tanggal_jt);
					date_add($date2,date_interval_create_from_date_string("$var_jml_hari days"));
					$jatuh_tempo_bayar = date_format($date2,"Y-m-d");


					// $batas_kirim
				}else{
					$batas_kirim='';
					$jatuh_tempo_bayar='';
				}

				$data_info=array(
					'tipe' =>$tipe,
					'idkelompokpasien' =>$tipekontraktor,
					'idrekanan' =>$idkontraktor,
					'idrekanan' =>$idkontraktor,
					'tanggal_tagihan' =>$tanggal_jt,
					'batas_kirim' =>$batas_kirim,
					'jatuh_tempo_bayar' =>$jatuh_tempo_bayar,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_user_id' =>$this->session->userdata('user_id'),
					'created_user' =>$this->session->userdata('user_name'),
					'status' =>'1',
				);
				// print_r($data_info);
				$this->db->insert('tpiutang', $data_info);
				$piutang_id=$this->db->insert_id();
			}
			$data_det=array(
					'piutang_id' =>$piutang_id,
					'tipe' =>$tipe,
					'pembayaran_id' =>$id,
					'user_verifikasi'=>$this->session->userdata('user_name'),
					'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					'tanggal_tagihan' =>$tanggal_jt,
				);
			$result=$this->db->insert('tpiutang_detail', $data_det);

		// }



        // $data =array(
            // 'st_verifikasi_piutang'=>'1',
			// 'user_verifikasi'=>$this->session->userdata('user_name'),
            // 'tanggal_verifikasi'=>date('Y-m-d H:i:s')
        // );
        // $this->db->where('id', $id);
		// if ($tipe=='1'){
			// $result=$this->db->update('tkasir_pembayaran', $data);
		// }else{
			// $result=$this->db->update('trawatinap_tindakan_pembayaran_detail', $data);
		// }
        if ($result) {
            $this->output->set_output(json_encode($data_info));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

	function list_tanggal_by_tangggal_id($tanggal_id){

		$q="SELECT tgl.* FROM (
				SELECT RS.idpengaturan,RS.id,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl
				,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH)  ELSE (SELECT tgl) END as tanggal_next
				from mpiutang_setting RS
				WHERE RS.`status`='1' AND RS.idpengaturan='3'
			) tgl
			LEFT JOIN tpiutang on tpiutang.tanggal_tagihan=tgl.tanggal_next  AND tpiutang.`status` > 1
			WHERE tpiutang.id is null
			ORDER BY tgl.tanggal_next ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.HumanDateShort($r->tanggal_next).'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function get_data_kirim($id){

		$arr['detail']=$this->db->query("SELECT noresi,keterangan,date_format(tanggal_kirim,'%d-%m-%Y')as tanggal_kirim FROM tpiutang WHERE id='$id'")->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function edit_tanggal_all(){
		$id=$this->input->post('piutang_id');
		$idpengaturan=$this->input->post('idpengaturan');
		$tanggal_id=$this->input->post('tanggal_id');
		$tanggal_tagihan=YMDFormat($this->input->post('tanggal_tagihan'));
		$tanggal_jt=YMDFormat($this->input->post('tanggal_tagihan'));

		$q_ada="SELECT K.id from tpiutang K
			WHERE K.tanggal_id='$tanggal_id'  AND K.tanggal_tagihan='$tanggal_jt' AND status>1 ";
		$piutang_id=$this->db->query($q_ada)->row('id');
		$detail_piutang=$this->db->query("SELECT tpiutang_detail.* from tpiutang_detail LEFT JOIN tpiutang ON tpiutang.id=tpiutang_detail.piutang_id where tpiutang_detail.piutang_id='$id'")->result();

		if ($piutang_id==''){//JIKA TIDAK ADA
			$data_info=array(
					'tanggal_id' =>$tanggal_id,
					'idpengaturan' =>$idpengaturan,
					'tanggal_tagihan' =>$tanggal_jt,
					'batas_kirim' =>$tanggal_jt,
					'jatuh_tempo_bayar' =>$tanggal_jt,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_user_id' =>$this->session->userdata('user_id'),
					'created_user' =>$this->session->userdata('user_name'),
					'status' =>'1',
				);
				// print_r($data_info);
				$this->db->insert('tpiutang', $data_info);
				$piutang_id=$this->db->insert_id();
		}
		foreach ($detail_piutang as $r){
			$this->db->query("DELETE FROM tpiutang_detail WHERE id='".$r->id."'");

			if ($r->tipe=='1'){
				$this->db->query("UPDATE tkasir_pembayaran set tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
			}else{
				$this->db->query("UPDATE trawatinap_tindakan_pembayaran_detail set tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");

			}
			$data_det=array(
					'piutang_id' =>$piutang_id,
					'tipe' =>$r->tipe,
					'pembayaran_id' =>$r->pembayaran_id,
					'user_verifikasi'=>$this->session->userdata('user_name'),
					'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					'tanggal_tagihan' =>$tanggal_tagihan,
				);
			$result=$this->db->insert('tpiutang_detail', $data_det);
		}
	}
	function edit_tanggal_satu(){
		$id=$this->input->post('piutang_id');
		$tanggal_tagihan=YMDFormat($this->input->post('tanggal_tagihan'));
		$tgl_asli=(int)substr($tanggal_tagihan,-2);
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$nama_asuransi='';
		if ($idkelompokpasien=='1'){
			$nama_asuransi=$this->db->query("SELECT nama from mrekanan WHERE id='$idrekanan'")->row('nama');
		}else{
			$nama_asuransi=$this->db->query("SELECT nama from mpasien_kelompok WHERE id='$idkelompokpasien'")->row('nama');
		}
		$where='';
		$id_setting='';
		if ($idkelompokpasien=='1'){
			$q_cari="SELECT *FROM mrekanan_setting S WHERE S.idrekanan='$idrekanan' AND S.`status`='1' AND tanggal_hari='$tgl_asli'";
			$hasil=$this->db->query($q_cari)->row();
			if ($hasil){
				$id_setting=$hasil->id;
				$var_batas_kirim=$hasil->batas_kirim;
				$var_jml_hari=$hasil->jml_hari;
			}
		}
		if ($id_setting==''){
			$q_cari="SELECT *FROM mpasien_kelompok_setting S WHERE S.idkelompokpasien='$idkelompokpasien' AND S.`status`='1' AND tanggal_hari='$tgl_asli'";
			$hasil=$this->db->query($q_cari)->row();
			if ($hasil){
				$id_setting=$hasil->id;
				$var_batas_kirim=$hasil->batas_kirim;
				$var_jml_hari=$hasil->jml_hari;
			}
		}

		if ($idkelompokpasien=='1'){
				$where .=" AND  K.idrekanan='$idrekanan'";
			}
		$q="SELECT K.id from tpiutang K
				WHERE K.tipe='$tipe' AND K.idkelompokpasien='$idkelompokpasien'  AND K.tanggal_tagihan='$tanggal_tagihan' AND status=1 ".$where;
		$piutang_id=$this->db->query($q)->row('id');
		$detail_piutang=$this->db->query("SELECT tpiutang_detail.*,tpiutang.idkelompokpasien,tpiutang.idrekanan from tpiutang_detail LEFT JOIN tpiutang ON tpiutang.id=tpiutang_detail.piutang_id where tpiutang_detail.id='$id'")->result();
		$date=date_create($tanggal_tagihan);
		date_add($date,date_interval_create_from_date_string("$var_batas_kirim days"));
		$batas_kirim = date_format($date,"Y-m-d");

		$date2=date_create($tanggal_tagihan);
		date_add($date2,date_interval_create_from_date_string("$var_jml_hari days"));
		$jatuh_tempo_bayar = date_format($date2,"Y-m-d");

		if ($piutang_id==''){//JIKA TIDAK ADA
				$data_info=array(
					'tipe' =>$tipe,
					'idkelompokpasien' =>$idkelompokpasien,
					'idrekanan' =>$idrekanan,
					'tanggal_tagihan' =>$tanggal_tagihan,
					'batas_kirim' =>$batas_kirim,
					'jatuh_tempo_bayar' =>$jatuh_tempo_bayar,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_user_id' =>$this->session->userdata('user_id'),
					'created_user' =>$this->session->userdata('user_name'),
					'status' =>'1',
				);
				// print_r($data_info);
				$this->db->insert('tpiutang', $data_info);
				$piutang_id=$this->db->insert_id();
		}
		foreach ($detail_piutang as $r){
			$this->db->query("DELETE FROM tpiutang_detail WHERE id='".$r->id."'");
			if ($idrekanan=='#'){
				$idrekanan='';
			}
			if ($r->tipe=='1'){
				$xnama_asuransi='Pembayaran Asuransi :'.$nama_asuransi.' [edited]';
				$this->db->query("UPDATE tkasir_pembayaran set tipekontraktor='$idkelompokpasien',idkontraktor='$idrekanan',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");
			}else{
				$xnama_asuransi='Perusahaan Asuransi :'.$nama_asuransi.' [edited]';
				$this->db->query("UPDATE trawatinap_tindakan_pembayaran_detail set tipekontraktor='$idkelompokpasien',idkontraktor='$idrekanan',keterangan='$xnama_asuransi',tanggal_jatuh_tempo='$tanggal_tagihan' WHERE id='".$r->pembayaran_id."'");

			}
			//INSERT HISTORY
			$data_his=array(
					'tpiutang_id' =>$r->piutang_id,
					'tpiutang_id_detail' =>$r->id,
					'pembayaran_id' =>$r->pembayaran_id,
					'tipe' =>$tipe,
					'idkelompokpasien_asal' =>$r->idkelompokpasien,
					'idrekanan_asal' =>$r->idrekanan,
					'idkelompokpasien' =>$idkelompokpasien,
					'idrekanan' =>$idrekanan,
					'user_nama' =>$this->session->userdata('user_name'),
					'tanggal_pindah' =>date('Y-m-d H:i:s'),
					'ket' =>'Pindah Satuan',
				);
				$this->db->insert('tpiutang_history', $data_his);

			$data_det=array(
					'piutang_id' =>$piutang_id,
					'tipe' =>$tipe,
					'pembayaran_id' =>$r->pembayaran_id,
					'user_verifikasi'=>$this->session->userdata('user_name'),
					'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
					'tanggal_tagihan' =>$tanggal_tagihan,
				);
			$result=$this->db->insert('tpiutang_detail', $data_det);
		}
	}
	function stop(){
		$id=$this->input->post('id');
		$result=$this->db->query("update tpiutang SET status='2' WHERE id='$id'");
		$this->output->set_output(json_encode($result));
	}
	function buka(){
		$id=$this->input->post('id');
		$result=$this->db->query("update tpiutang SET status='1' WHERE id='$id'");
		$this->output->set_output(json_encode($result));
	}

	function kirim(){
		$idtransaksi = $this->input->post('id');
		$q="SELECT H.pendaftaran_id,H.kasir_id,H.tipe FROM `tpiutang_detail` H WHERE H.piutang_id='$idtransaksi'";
		$row=$this->db->query($q)->result();
		foreach($row as $r){
			if ($r->tipe=='1'){
				$this->Tvalidasi_model->GenerateValidasiPendapatanRajal($r->pendaftaran_id,$r->kasir_id,'3',date('Y-m-d'));				
			}
			if ($r->tipe=='2'){
				$this->Tvalidasi_model->GenerateValidasiPendapatanRanap($r->pendaftaran_id,$r->kasir_id,'3',date('Y-m-d'));				
			}
		}
		
		$data = array(
			'status_kirim' =>'1',
			'status' =>'2',
			'tanggal_kirim' =>date('Y-m-d H:i:s'),
			'kirim_user' =>$this->session->userdata('user_name'),
			'kirim_date' =>date('Y-m-d H:i:s'),
		);
		$this->db->where('id', $idtransaksi);
		$result = $this->db->update('tpiutang', $data);

		if ($result) {
			$piutangRincian = $this->db->query("SELECT tpiutang_detail.id AS iddetail,tpiutang_detail.tanggal_tagihan AS tanggal,tpiutang.no_piutang AS notransaksi,mdokter.id AS iddokter,mdokter.nama AS namadokter,tpiutang_detail.nominal FROM tpiutang_detail JOIN tpiutang ON tpiutang.id=tpiutang_detail.piutang_id JOIN mdokter ON mdokter.id=tpiutang_detail.iddokter WHERE piutang_id=$idtransaksi AND tipepegawai=2")->result();
			foreach ($piutangRincian as $row) {
				$iddetail = $row->iddetail;
				$tanggal = $row->tanggal;
				$notransaksi = $row->notransaksi;
				$iddokter = $row->iddokter;
				$namadokter = $row->namadokter;
				$nominal = $row->nominal;

        $tanggal_pembayaran = YMDFormat(getPembayaranHonorDokter($iddokter));
        $tanggal_jatuhtempo = YMDFormat(getJatuhTempoHonorDokter($iddokter));

				$this->db->set('tanggal_pembayaran_honor', $tanggal_pembayaran);
        $this->db->set('tanggal_jatuhtempo_honor', $tanggal_jatuhtempo);
        $this->db->where('id', $iddetail);
        if ($this->db->update('tpiutang_detail')) {

					// Proses Honor Dokter
					$isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($iddokter, $tanggal_pembayaran);
					if ($isStopPeriode == null) {
						$idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($iddokter, $tanggal_pembayaran);

						if ($idhonor == null) {
								$dataHonorDokter = array(
									'iddokter' => $iddokter,
									'namadokter' => $namadokter,
									'tanggal_pembayaran' => $tanggal_pembayaran,
									'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
									'nominal' => 0,
									'created_at' => date('Y-m-d H:i:s'),
									'created_by' => $this->session->userdata('user_id')
								);

								if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
										$idhonor = $this->db->insert_id();
								}
						}
					}

					$dokter = $this->db->query("SELECT mdokter.id,
							mdokter.nama,
							mdokter_kategori.id AS idkategori,
							mdokter_kategori.nama AS namakategori
						FROM
							mdokter
						JOIN mdokter_kategori ON mdokter_kategori.id=mdokter.idkategori
						WHERE
							mdokter.id = $iddokter AND
							mdokter.status = 1")->row();

					$dataDetailHonorDokter = array(
						'idhonor' => $idhonor,
						'idtransaksi' => $idtransaksi,
						'jenis_transaksi' => 'pengeluaran',
						'jenis_tindakan' => 'POTONGAN BEROBAT',
						'reference_table' => 'tpiutang_detail',
						'iddetail' => $iddetail,
						'namatarif' => 'Potongan Berobat ('.$notransaksi.')',
						'idkategori' => $dokter->idkategori,
						'namakategori' => $dokter->namakategori,
						'iddokter' => $iddokter,
						'namadokter' => $namadokter,
						'jasamedis' => $nominal,
						'potongan_rs' => 0,
						'nominal_potongan_rs' => 0,
						'pajak_dokter' => 0,
						'nominal_pajak_dokter' => 0,
						'jasamedis_netto' => $nominal,
						'tanggal_pemeriksaan' => $tanggal,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo
					);

					$this->db->insert('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			$this->output->set_output(json_encode($result));
		}
	}

	function lunaskan_detail_trx(){
		$id=$this->input->post('id');
		$data=array(
			'status_lunas' =>'1',
			'date_pelunasan' =>date('Y-m-d H:i:s'),
			'tanggal_pembayaran' =>date('Y-m-d'),
			'user_pelunasan' =>$this->session->userdata('user_name'),
		);
		$this->db->where('id', $id);
		$result=$this->db->update('tpiutang_detail', $data);
		$this->output->set_output(json_encode($result));
	}
	public function lunaskansemua()
    {
		$arr_id = $this->input->post('arr_id');

		$result=false;
		foreach ($arr_id as $x => $val){
			$id = $arr_id[$x];

			$data=array(
			'status_lunas' =>'1',
			'date_pelunasan' =>date('Y-m-d H:i:s'),
			'tanggal_pembayaran' =>date('Y-m-d'),
			'user_pelunasan' =>$this->session->userdata('user_name'),
		);
		$this->db->where('id', $id);
		$result=$this->db->update('tpiutang_detail', $data);
		}
		$this->output->set_output(json_encode($result));


    }
	public function print_rincian($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
		$q="SELECT H.no_piutang, D.tipepegawai,D.idpegawai,D.iddokter,CASE WHEN D.tipepegawai='1' THEN MP.nama ELSE MD.nama END as nama
				,CASE WHEN D.tipepegawai='1' THEN 'PEGAWAI' ELSE 'DOKTER' END as nama_tipe
				,SUM(CASE WHEN D.tipe='1' THEN D.nominal ELSE 0 END) as rj
				,SUM(CASE WHEN D.tipe='2' THEN D.nominal ELSE 0 END) as ri
				,SUM(CASE WHEN D.tipe='3' THEN D.nominal ELSE 0 END) as ob
				,GROUP_CONCAT(DISTINCT D.pembayaran_id) as pembayaran_id_group,H.tanggal_tagihan
				from tpiutang H
				LEFT JOIN tpiutang_detail D ON D.piutang_id=H.id
				LEFT JOIN mpegawai MP ON MP.id=D.idpegawai AND D.tipepegawai='1'
				LEFT JOIN mdokter MD ON MD.id=D.iddokter AND D.tipepegawai='2'
				WHERE H.id='$id'
				GROUP BY D.tipepegawai,D.idpegawai,D.iddokter ORDER BY D.tipepegawai,MP.nama,MD.nama";
        $data=$this->model->detail($id);
        $data['error']          = '';
        $data['list_detail']    = $this->db->query($q)->result();//$this->model->list_pesanan_edit($id);
		// print_r($data['list_detail']);exit();
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tpiutang_rincian/lap_rincian', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
	public function print_rincian_perorang($piutang_id,$tipepegawai,$idpeg)
    {
		// $r->piutang_id.'/'.$r->tipepegawai.'/'.$idpeg_dok
        // instantiate and use the dompdf class
		if ($tipepegawai=='1'){
		   $idpegawai=$idpeg;
		   $iddokter=0;
	   }else{
		   $idpegawai=0;
		   $iddokter=$idpeg;
	   }
	   
        $dompdf = new Dompdf();
		$q="SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien
			,TD.nominal
			,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'RJ' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
			,TD.cicilan_ke
			from tpiutang_detail TD
			LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.pendaftaran_id
			LEFT JOIN mpoliklinik M ON TP.idpoliklinik=M.id
			LEFT JOIN mdokter D ON D.id=TP.iddokter
			LEFT JOIN tkasir K ON K.id=TD.kasir_id
			WHERE TD.piutang_id='$piutang_id' AND TD.tipe='1' AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
			GROUP BY TP.id

			UNION ALL

			SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggal as tanggaldaftar,TP.nopenjualan as nopendaftaran,TP.nomedrec as no_medrec,TP.nama as namapasien
			,TD.nominal
			,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'Pembelian Obat' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
			,TD.cicilan_ke
			from tpiutang_detail TD
			LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
			LEFT JOIN tpasien_penjualan TP ON TP.id=TD.pendaftaran_id
			LEFT JOIN tkasir K ON K.id=TD.kasir_id
			WHERE TD.piutang_id='$piutang_id' AND TD.tipe='3' AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
			GROUP BY TP.id

			UNION ALL

			SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien
			,TBD.nominal,TD.tanggal_tagihan,TD.tipe,TD.jml_cicilan,TB.tanggal as tanggal_kasir,'RI/ODS' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
			,TD.cicilan_ke
			from tpiutang_detail TD
			LEFT JOIN trawatinap_pendaftaran TP ON TP.id=TD.pendaftaran_id
			LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.idtindakan=TP.id AND TB.statusbatal=0
			LEFT JOIN trawatinap_tindakan_pembayaran_detail TBD ON TBD.idtindakan=TB.id
			WHERE TD.piutang_id='$piutang_id' AND TD.tipe='2'  AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
			GROUP BY TP.id";
        $data=$this->model->detail_trx($piutang_id,$tipepegawai,$idpeg);
        $data['error']          = '';
        $data['list_detail']    = $this->db->query($q)->result();//$this->model->list_pesanan_edit($id);
		// print_r($data['list_detail']);exit();
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tpiutang_rincian/lap_rincian_perorang', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
	public function print_rincian_all($id)
    {
		// $r->piutang_id.'/'.$r->tipepegawai.'/'.$idpeg_dok
        // instantiate and use the dompdf class
		
        $dompdf = new Dompdf();
		$q="SELECT H.id,H.no_piutang, D.tipepegawai,H.tanggal_tagihan,D.idpegawai,D.iddokter,CASE WHEN D.tipepegawai='1' THEN MP.nama ELSE MD.nama END as nama
				,CASE WHEN D.tipepegawai='1' THEN 'PEGAWAI' ELSE 'DOKTER' END as nama_tipe 
				FROM tpiutang H
				LEFT JOIN tpiutang_detail D ON D.piutang_id=H.id
				LEFT JOIN mpegawai MP ON MP.id=D.idpegawai AND D.tipepegawai='1'
				LEFT JOIN mdokter MD ON MD.id=D.iddokter AND D.tipepegawai='2'
				WHERE H.id='$id'
				GROUP BY H.id,D.tipepegawai,D.idpegawai,D.iddokter
				ORDER BY D.tipepegawai,MP.nama,MD.nama
";
        // $data=$this->model->detail_trx($piutang_id,$tipepegawai,$idpeg);
        $data['error']          = '';
        $data['list_detail']    = $this->db->query($q)->result();//$this->model->list_pesanan_edit($id);
		// print_r($data['list_detail']);exit();
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tpiutang_rincian/lap_rincian_all', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
}
