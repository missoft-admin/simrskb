<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mnilainormal extends CI_Controller
{
    /**
     * Nilai Normal controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mnilainormal_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Nilai Normal';
        $data['content'] = 'Mnilainormal/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Nilai Normal', '#'],
            ['List', 'mnilainormal'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'nama' => '',
            'idsatuan' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Nilai Normal';
        $data['content'] = 'Mnilainormal/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Nilai Normal', '#'],
            ['Tambah', 'mnilainormal'],
        ];

        $data['list_satuan'] = $this->Mnilainormal_model->getSatuanLab();
        $data['list_tariflab'] = $this->Mnilainormal_model->getTarifLab();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mnilainormal_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'nama' => $row->nama,
                    'idsatuan' => $row->idsatuan,
                    'status' => $row->status,
                ];
                $data['error'] = '';
                $data['title'] = 'Ubah Nilai Normal';
                $data['content'] = 'Mnilainormal/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Nilai Normal', '#'],
                    ['Ubah', 'mnilainormal'],
                ];

                $data['list_satuan'] = $this->Mnilainormal_model->getSatuanLab();
                $data['list_tariflab'] = $this->Mnilainormal_model->getTarifLab();
                $data['list_tariflab_selected'] = $this->Mnilainormal_model->getTarifLabSelected($id);
                $data['list_kriteria'] = $this->Mnilainormal_model->getListKriteria($id);
                $data['list_kritis'] = $this->Mnilainormal_model->getListKritis($id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mkelas', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mnilainormal');
        }
    }

    public function delete($id): void
    {
        $this->Mnilainormal_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mnilainormal', 'location');
    }

    public function save(): void
    {
        if ('' === $this->input->post('id')) {
            if ($this->Mnilainormal_model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('mnilainormal', 'location');
            }
        } else {
            if ($this->Mnilainormal_model->updateData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('mnilainormal', 'location');
            }
        }
    }

    public function getSatuanLab(): void
    {
        $data = get_all('msatuan', ['idkategori' => 2, 'status' => 1]);
        $this->output->set_output(json_encode($data));
    }

    public function getIndex(): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];
        $this->select = ['mnilainormal.*', 'msatuan.singkatan AS singkatansatuan'];
        $this->from = 'mnilainormal';
        $this->join = [
            ['msatuan', 'msatuan.id = mnilainormal.idsatuan', ''],
        ];
        $this->where = [
            'mnilainormal.status' => '1',
        ];
        $this->order = [
            'mnilainormal.id' => 'DESC',
        ];
        $this->group = [];

        $this->column_search = ['mnilainormal.nama'];
        $this->column_order = ['mnilainormal.nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = $r->singkatansatuan;
            $aksi = '<div class="btn-group">';
            if (UserAccesForm($user_acces_form, ['205'])) {
                $aksi .= '<a href="'.site_url().'mnilainormal/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            }
            if (UserAccesForm($user_acces_form, ['206'])) {
                $aksi .= '<a href="#" data-urlindex="'.site_url().'mrak" data-urlremove="'.site_url().'mnilainormal/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            }
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }
}

// End of file welcome.php
// Location: ./system/application/controllers/welcome.php
