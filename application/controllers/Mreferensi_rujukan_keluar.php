<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mreferensi_rujukan_keluar extends CI_Controller
{
    /**
     * Referensi Rujukan Keluar controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mreferensi_rujukan_keluar_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Referensi Rujukan Keluar';
        $data['content'] = 'Mreferensi_rujukan_keluar/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Referensi Rujukan Keluar', '#'],
            ['List', 'mreferensi_rujukan_keluar'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'nama_tujuan' => '',
            'nama_lengkap' => '',
            'alamat' => '',
            'nomor_telepon' => '',
            'email' => '',
            'nomor_perjanjian' => '',
            'tanggal_perjanjian_dari' => '',
            'tanggal_perjanjian_sampai' => '',
            'pic' => '',
            'status' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Referensi Rujukan Keluar';
        $data['content'] = 'Mreferensi_rujukan_keluar/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Referensi Rujukan Keluar', '#'],
            ['Tambah', 'mreferensi_rujukan_keluar'],
        ];

        $data['option_jenis_layanan'] = [];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mreferensi_rujukan_keluar_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,        
                    'nama_tujuan' => $row->nama_tujuan,
                    'nama_lengkap' => $row->nama_lengkap,
                    'alamat' => $row->alamat,
                    'nomor_telepon' => $row->nomor_telepon,
                    'email' => $row->email,
                    'nomor_perjanjian' => $row->nomor_perjanjian,
                    'tanggal_perjanjian_dari' => DMYFormat($row->tanggal_perjanjian_dari),
                    'tanggal_perjanjian_sampai' => DMYFormat($row->tanggal_perjanjian_sampai),
                    'pic' => $row->pic,
                ];
                $data['error'] = '';
                $data['title'] = 'Ubah Referensi Rujukan Keluar';
                $data['content'] = 'Mreferensi_rujukan_keluar/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Referensi Rujukan Keluar', '#'],
                    ['Ubah', 'mreferensi_rujukan_keluar'],
                ];

                $data['option_jenis_layanan'] = $this->Mreferensi_rujukan_keluar_model->getReferensiJenisLayanan($id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mreferensi_rujukan_keluar', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mreferensi_rujukan_keluar');
        }
    }

    public function delete($id): void
    {
        $this->Mreferensi_rujukan_keluar_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mreferensi_rujukan_keluar', 'location');
    }

    public function save(): void
    {
        $this->form_validation->set_rules('nama_tujuan', 'Nama Tujuan', 'trim|required');

        if (true === $this->form_validation->run()) {
            if ('' === $this->input->post('id')) {
                if ($this->Mreferensi_rujukan_keluar_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mreferensi_rujukan_keluar', 'location');
                }
            } else {
                if ($this->Mreferensi_rujukan_keluar_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mreferensi_rujukan_keluar', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = 'Mreferensi_rujukan_keluar/manage';

        $data['option_jenis_layanan'] = $this->Mreferensi_rujukan_keluar_model->getReferensiJenisLayanan($id);
        
        if ('' === $id) {
            $data['title'] = 'Tambah Referensi Rujukan Keluar';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Referensi Rujukan Keluar', '#'],
                ['Tambah', 'mreferensi_rujukan_keluar'],
            ];
        } else {
            $data['title'] = 'Ubah Referensi Rujukan Keluar';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Referensi Rujukan Keluar', '#'],
                ['Ubah', 'mreferensi_rujukan_keluar'],
            ];
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex(): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        $this->select = [];
        $this->from = 'merm_referensi_rujukan_keluar';
        $this->join = [];
        $this->where = [
            'status' => '1',
        ];
        $this->order = [
            'nama_tujuan' => 'ASC',
        ];
        $this->group = [];

        $this->column_search = ['nama_tujuan'];
        $this->column_order = ['nama_tujuan'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama_tujuan;
            $row[] = StatusRow($r->status);
            $aksi = '<div class="btn-group">';
						$aksi .= '<a href="'.site_url().'mreferensi_rujukan_keluar/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mreferensi_rujukan_keluar" data-urlremove="'.site_url().'mreferensi_rujukan_keluar/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }
}
