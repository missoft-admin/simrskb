<?php

function logicFeeRujukan($tipePasien=0, $asalPasien=0, $idPoliklinik=0, $tipeDokter=0, $idDokter=0, $hitungOperasi=0, $hitungVisite=0, $stKasirRajal=0)
{
    $row = get_row('msetting_feerujukan_dokter');

    if ($row) {
        $ztipePasien = json_decode(json_decode($row->tipe_pasien));
        $zasalPasien = json_decode(json_decode($row->asal_pasien));
        $zidPoliklinik = json_decode(json_decode($row->poliklinik));
        $ztipeDokter = json_decode(json_decode($row->tipe_dokter));
        $zidDokter = json_decode(json_decode($row->nama_dokter));
        $zhitungOperasi = json_decode(json_decode($row->hitung_operasi));
        $zhitungVisite = json_decode(json_decode($row->hitung_visite));

        $xTipePasien = isset($ztipePasien[0]) ? array_column($ztipePasien, 0) : array();
        $xAsalPasien = isset($zasalPasien[0]) ? array_column($zasalPasien, 0) : array();
        $xIdPoliklinik = isset($zidPoliklinik[0]) ? array_column($zidPoliklinik, 0) : array();
        $xTipeDokter = isset($ztipeDokter[0]) ? array_column($ztipeDokter, 0) : array();
        $xIdDokter = isset($zidDokter[0]) ? array_column($zidDokter, 0) : array();
        $xHitungOperasi = isset($zhitungOperasi[0]) ? array_column($zhitungOperasi, 0) : array();
        $xHitungVisite = isset($zhitungVisite[0]) ? array_column($zhitungVisite, 0) : array();

        // PASSED - print_r(in_array($tipePasien, $xTipePasien));exit();
        // PASSED - print_r(in_array($asalPasien, $xAsalPasien));exit();
        // PASSED - print_r((in_array($idPoliklinik, $xIdPoliklinik) || COUNT($xIdPoliklinik) == 0));exit();
        // PASSED - print_r((in_array($tipeDokter, $xTipeDokter) || COUNT($xTipeDokter) == 0));exit();
        // PASSED - print_r((in_array($idDokter, $xIdDokter) || COUNT($xIdDokter) == 0));exit();
        // PASSED - print_r(in_array($hitungOperasi, $xHitungOperasi));exit();
        // PASSED - print_r(in_array($hitungVisite, $xHitungVisite));exit();
        // PASSED - print_r($stKasirRajal));exit();

        if (
          in_array($tipePasien, $xTipePasien) &&
          in_array($asalPasien, $xAsalPasien) &&
          (in_array($idPoliklinik, $xIdPoliklinik) || COUNT($xIdPoliklinik) == 0) &&
          (in_array($tipeDokter, $xTipeDokter) || COUNT($xTipeDokter) == 0) &&
          (in_array($idDokter, $xIdDokter) || COUNT($xIdDokter) == 0) &&
          in_array($hitungOperasi, $xHitungOperasi) &&
          in_array($hitungVisite, $xHitungVisite) &&
          $stKasirRajal == 0
        ) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function logicAnalisaRekamMedis($settingAnalisa, $dataBerkas)
{
    if ($settingAnalisa) {
        $jenisKunjungan = jsonDecode($settingAnalisa->jenis);
        $tipeKunjungan = jsonDecode($settingAnalisa->tipe);
        $poliklinikKelas = jsonDecode($settingAnalisa->poliklinik);
        $listDokter = jsonDecode($settingAnalisa->dokter);
        $kelompokDiagnosa = jsonDecode($settingAnalisa->kelompok_diagnosa);
        $kelompokTindakan = jsonDecode($settingAnalisa->kelompok_tindakan);
        $kelompokTindakanOperasi = jsonDecode($settingAnalisa->kelompok_tindakan_operasi);

        $statusPoliklinikKelas = 0;
        foreach ((array) $poliklinikKelas as $row) {
            if ($row->jenis == $dataBerkas['jenisKunjungan'] && $row->id == $dataBerkas['poliklinikKelas']) {
                $statusPoliklinikKelas++;
            }
        }

        // print_r( in_array($dataBerkas['jenisKunjungan'], $jenisKunjungan) ); exit();
        // print_r( (in_array($dataBerkas['tipeKunjungan'], $tipeKunjungan) || COUNT($tipeKunjungan) == 0) ); exit();
        // print_r( ($statusPoliklinikKelas || COUNT($poliklinikKelas) == 0) ); exit();
        // print_r( (in_array($dataBerkas['listDokter'], $listDokter) || COUNT($listDokter) == 0) ); exit();
        // print_r( (in_array($dataBerkas['kelompokDiagnosa'], $kelompokDiagnosa) || COUNT($kelompokDiagnosa) == 0) ); exit();
        // print_r( (in_array($dataBerkas['kelompokDiagnosa'], $kelompokDiagnosa) || COUNT($kelompokDiagnosa) == 0) ); exit();
        // print_r( (in_array($dataBerkas['kelompokTindakan'], $kelompokTindakan) || COUNT($kelompokTindakan) == 0) ); exit();
        // print_r( (in_array($dataBerkas['kelompokTindakanOperasi'], $kelompokTindakanOperasi) || COUNT($kelompokTindakanOperasi) == 0) ); exit();

        if (
          in_array($dataBerkas['jenisKunjungan'], $jenisKunjungan) &&
          (in_array($dataBerkas['tipeKunjungan'], $tipeKunjungan) || COUNT($tipeKunjungan) == 0) &&
          ($statusPoliklinikKelas || COUNT($poliklinikKelas) == 0) &&
          (in_array($dataBerkas['listDokter'], $listDokter) || COUNT($listDokter) == 0) &&
          (in_array($dataBerkas['kelompokDiagnosa'], $kelompokDiagnosa) || COUNT($kelompokDiagnosa) == 0) &&
          (in_array($dataBerkas['kelompokTindakan'], $kelompokTindakan) || COUNT($kelompokTindakan) == 0) &&
          (in_array($dataBerkas['kelompokTindakanOperasi'], $kelompokTindakanOperasi) || COUNT($kelompokTindakanOperasi) == 0)
        ) {
            return 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}
