<?php
    function get_kode($kode = 'KODE', $namakode, $tbl, $str_length=null)
    {
        $CI =& get_instance();
        $CI->db->like($namakode, $kode, 'after');
        $CI->db->from($tbl);
        $query = $CI->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = $kode.str_pad($autono, $str_length, '0', STR_PAD_LEFT);
        } else {
            $left = "";
            for ($i=1; $i<=$str_length; $i++) {
                $left .= "0";
            }
            $left = substr($left, 0, $str_length-1)."1";
            $autono = $kode.$left;
        }
        return $autono;
    }

    function createKode($table, $id, $namakode)
    {
        $CI =& get_instance();
        $CI->db->select('RIGHT(t1.'.$id.',4) as kode', false)
                ->order_by($id, 'DESC')
                ->limit(1);
        $query = $CI->db->get($table.' t1');
        if ($query->num_rows() <> 0) {
            $data =$query->row();
            $kode =intval($data->kode) + 1;
        } else {
            $kode =1;
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodejadi = $namakode.date("ymd").$kodemax;
        return $kodejadi;
    }

    function getDoubleWhere($kondisi1, $where1, $kondisi2, $where2, $table)
    {
        $CI =& get_instance();
        return $CI->db->where($kondisi1, $where1)
                      ->where($kondisi2, $where2)
                      ->get($table.' t1');
    }

    function getTripleWhere($kondisi1, $where1, $kondisi2, $where2, $kondisi3, $where3, $table)
    {
        $CI =& get_instance();
        return $CI->db->where($kondisi1, $where1)
                      ->where($kondisi2, $where2)
                      ->where($kondisi3, $where3)
                      ->get($table.' t1');
    }

    function get_detail_pengganti($id)
    {
        $CI =& get_instance();
        $q="SELECT
				B.namatipe
				,nama as nama_barang,
				G.harga,
				G.kuantitas,
				CASE WHEN G.opsisatuan='1' THEN 'KECIL' ELSE 'BESAR' END as satuan,
				G.tot_harga,
				G.idtipe,
				G.idbarang,
				G.opsisatuan,
				G.idpengembalian,
				G.idpengembalian_det,
				G.id
			FROM
				tgudang_pengembalian_ganti G
				LEFT JOIN view_barang B ON B.id = G.idbarang AND B.idtipe = G.idtipe
			WHERE
				G.idpengembalian = '$id'";
        $query=$CI->db->query($q);
        $hasil=($query->result_array());
        $arr = array_map(function ($x) {
            return array_values($x);
        }, $hasil);
        // return $arr;
        return json_encode($arr);
    }
    function get_by_field($fieldkey, $fieldval, $tbl)
    {
        $CI =& get_instance();
        $CI->db->where($fieldkey, $fieldval);
        $query = $CI->db->get($tbl);
        return $query->row();
    }

    function getwhere($kondisi, $where, $table)
    {
        $CI =& get_instance();
        return $CI->db->where($kondisi, $where)
                        ->get($table.' t1');
    }

    function getwherejoin($kondisi, $where, $table, $table2, $join)
    {
        $CI =& get_instance();
        return $CI->db->select('*')
                        ->where($kondisi, $where)
                        ->join($table2, $join)
                        ->get($table);
    }

    function getlikejoin($kondisi, $like, $table, $table2, $join)
    {
        $CI =& get_instance();
        return $CI->db->select('*')
                        ->like($kondisi, $like)
                        ->join($table2, $join)
                        ->get($table);
    }

    function getData($table)
    {
        $CI =& get_instance();
        return $CI->db->get($table);
    }

    function get_all($tbl, $where = array(), $order_by = null,$urutan='DESC')
    {
        $CI =& get_instance();

        if (mysqli_more_results($CI->db->conn_id)) {
            mysqli_next_result($CI->db->conn_id);
        }

        $CI->db->where($where);
        if ($order_by!==null) {
            $CI->db->order_by($order_by, $urutan);
        }
        $query = $CI->db->get($tbl);
        return $query->result();
    }
	function get_saldo_awal_hutang($tipe,$iddistributor,$tanggal='')
    {
        $CI =& get_instance();

        if (mysqli_more_results($CI->db->conn_id)) {
            mysqli_next_result($CI->db->conn_id);
        }
		$q="SELECT IFNULL(SUM(H.kredit - H.debet),0) as saldo 
			FROM tbuku_besar_hutang H
			WHERE H.tipe_distributor='$tipe' AND H.iddistributor='$iddistributor' AND H.tanggal_transaksi < '$tanggal'";
        // $CI->db->where($where);
        // if ($order_by!==null) {
            // $CI->db->order_by($order_by, $urutan);
        // }
        $query = $CI->db->query($q);
        return $query->row('saldo');
    }
	function get_saldo_awal_piutang($idkelompok,$tanggal='')
    {
        $CI =& get_instance();

        if (mysqli_more_results($CI->db->conn_id)) {
            mysqli_next_result($CI->db->conn_id);
        }
		$q="SELECT IFNULL(SUM(H.debet - H.kredit),0) as saldo 
			FROM tbuku_besar_piutang H
			WHERE CONCAT(H.idkelompokpasien,'-',H.idrekanan)='$idkelompok' AND H.tanggal_transaksi < '$tanggal'";
       
		// print_r($q);exit();
        $query = $CI->db->query($q);
        return $query->row('saldo');
    }
	function get_saldo_awal_hutang_periode($tipe,$iddistributor,$periode='')
    {
        $CI =& get_instance();

        if (mysqli_more_results($CI->db->conn_id)) {
            mysqli_next_result($CI->db->conn_id);
        }
		$q="SELECT IFNULL(SUM(H.kredit - H.debet),0) as saldo 
			FROM tbuku_besar_hutang H
			WHERE H.tipe_distributor='$tipe' AND H.iddistributor='$iddistributor' AND H.periode < '$periode'";
        // $CI->db->where($where);
        // if ($order_by!==null) {
            // $CI->db->order_by($order_by, $urutan);
        // }
        $query = $CI->db->query($q);
        return $query->row('saldo');
    }
	function get_saldo_awal_piutang_periode($idkelompok,$periode='')
    {
        $CI =& get_instance();

        if (mysqli_more_results($CI->db->conn_id)) {
            mysqli_next_result($CI->db->conn_id);
        }
		$q="SELECT IFNULL(SUM(H.debet - H.kredit),0) as saldo 
			FROM tbuku_besar_piutang H
			WHERE CONCAT(H.idkelompokpasien,'-',H.idrekanan)='$idkelompok' AND H.periode < '$periode'";
        // $CI->db->where($where);
        // if ($order_by!==null) {
            // $CI->db->order_by($order_by, $urutan);
        // }
        $query = $CI->db->query($q);
        return $query->row('saldo');
    }

    function get_row($tbl)
    {
        $CI =& get_instance();
        if (mysqli_more_results($CI->db->conn_id)) {
            mysqli_next_result($CI->db->conn_id);
        }
        $query = $CI->db->get($tbl);
        return $query->row();
    }

    function getByQuery($query)
    {
        $CI =& get_instance();
        $CI->db->select(implode(',', $query['select']));

        foreach ($query['where'] as $row) {
          $CI->db->where($row[0], $row[1]);
        }

        $output = $CI->db->get($query['table']);

        if ($query['output_type'] == 'single') {
          return $output->row();
        } else {
          return $output->result();
        }
    }

    function get_detail_barang($idtipe, $idbarang)
    {
        $arr = array(null, 'mdata_alkes', 'mdata_implan', 'mdata_obat', 'mdata_logistik');
        return get_by_field('id', $idbarang, $arr[$idtipe]);
    }

    function detail_stok($idtipe, $idbarang, $idunit)
    {
        $CI =& get_instance();
        $query = $CI->db->query("SELECT H.stok from mgudang_stok H
			WHERE H.idbarang='$idbarang' AND H.idtipe='$idtipe' AND H.idunitpelayanan='$idunit'");
        return $query->row('stok');
    }
	function list_variable_ref($idhead){
		 $CI =& get_instance();
        $query = $CI->db->query("SELECT H.nilai as id,H.ref as nama,H.st_default FROM `merm_referensi` H
								WHERE H.ref_head_id='$idhead' AND H.status='1'
								ORDER BY H.nilai ASC");
        return $query->result();
	}
	function get_logic_pendaftaran_poli($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM app_pendaftaran_logic logic_0
			LEFT JOIN 
			(SELECT * FROM app_pendaftaran_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter'
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM app_pendaftaran_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
        $query = $CI->db->query($q);
        return $query->row();
	}
	function auto_insert_EWS($id,$tipe_input){
		$CI =& get_instance();
		$assesmen_id='';
		$q="SELECT *FROM tpoliklinik_ews WHERE tipe_input='$tipe_input' AND id_asal='$id'";
		$assesmen_id=$CI->db->query($q)->row('assesmen_id');
		
		if ($tipe_input=='1'){
			$q="SELECT  
				H.tanggal_input,H.pendaftaran_id,null as pendaftaran_id_ranap,0 as st_ranap,H.idpasien,H.created_date,H.created_ppa,H.status_ttv as status_assemen
				,H.deleted_ppa,H.deleted_date,H.alasan_id,H.keterangan_hapus,H.alasan_edit_id,H.keterangan_edit,H.jml_edit,H.edited_ppa,H.edited_date,H.tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.tinggi_badan,H.berat_badan,H.suplemen_oksigen,'1' as tipe_input,H.id as id_asal
				FROM tpoliklinik_ttv H 
				WHERE H.id='$id'";
		}
		if ($tipe_input=='2'){
			$q="SELECT  
				H.tanggal_input,null as pendaftaran_id, H.pendaftaran_id_ranap,1 as st_ranap,MP.idpasien,H.created_date,H.created_ppa,H.status_ttv as status_assemen
				,H.deleted_ppa,H.deleted_date,H.alasan_id,H.keterangan_hapus,H.alasan_edit_id,H.keterangan_edit,H.jml_edit,H.edited_ppa,H.edited_date,H.tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.tinggi_badan,H.berat_badan,H.suplemen_oksigen,'2' as tipe_input,H.id as id_asal
				FROM tranap_ttv H 
				INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap
				WHERE H.id='$id'";
		}
		$row=$CI->db->query($q)->row();
		$data=array(
			'tanggal_input' => $row->tanggal_input,
			'pendaftaran_id' => $row->pendaftaran_id,
			'pendaftaran_id_ranap' => $row->pendaftaran_id_ranap,
			'st_ranap' => $row->st_ranap,
			'idpasien' => $row->idpasien,
			'created_date' => $row->created_date,
			'created_ppa' => $row->created_ppa,
			'status_assemen' => $row->status_assemen,
			'deleted_ppa' => $row->deleted_ppa,
			'deleted_date' => $row->deleted_date,
			'alasan_id' => $row->alasan_id,
			'keterangan_hapus' => $row->keterangan_hapus,
			'alasan_edit_id' => $row->alasan_edit_id,
			'keterangan_edit' => $row->keterangan_edit,
			'jml_edit' => $row->jml_edit,
			'edited_ppa' => $row->edited_ppa,
			'edited_date' => $row->edited_date,
			'tingkat_kesadaran' => $row->tingkat_kesadaran,
			'nadi' => $row->nadi,
			'nafas' => $row->nafas,
			'spo2' => $row->spo2,
			'td_sistole' => $row->td_sistole,
			'td_diastole' => $row->td_diastole,
			'suhu' => $row->suhu,
			'tinggi_badan' => $row->tinggi_badan,
			'berat_badan' => $row->berat_badan,
			'suplemen_oksigen' => $row->suplemen_oksigen,
			'tipe_input' => $row->tipe_input,
			'id_asal' => $row->id_asal,
			);
		if ($assesmen_id){
			$CI->db->where('assesmen_id',$assesmen_id);
			$CI->db->update('tpoliklinik_ews',$data);
		}else{
			$CI->db->insert('tpoliklinik_ews',$data);
			$assesmen_id = $CI->db->insert_id();
		}
		update_logic_EWS($assesmen_id);
	}
	function auto_insert_Setoran_RJ($tanggal){
		$CI =& get_instance();
		$assesmen_id='';
		$q="INSERT INTO tsetoran_rj (tanggal_trx,tanggal_setoran)
			SELECT DT.tanggal as tanggal_trx,DT.tanggal as tanggal_setoran FROM date_row DT
			LEFT JOIN tsetoran_rj H ON H.tanggal_trx=DT.tanggal
			WHERE DT.tanggal='$tanggal' AND H.id IS NULL
			";
		$CI->db->query($q);
		
	}
	function update_logic_EWS($assesmen_id){
		 $CI =& get_instance();
		  $q="
				SELECT G.skor as skor_param,L.hasil_skor,L.warna,R.ref as hasil_ews FROM(
				SELECT SUM(G.skor) as skor FROM (
				SELECT H.*,COALESCE(M.skor,0) as skor  FROM( SELECT 
				M.id,M.nama_ews
				,CASE 
					WHEN M.id=1 THEN H.tingkat_kesadaran 
					WHEN M.id=2 THEN H.nadi
					WHEN M.id=3 THEN H.nafas
					WHEN M.id=4 THEN H.spo2
					WHEN M.id=5 THEN H.td_sistole
					WHEN M.id=6 THEN H.td_diastole
					WHEN M.id=7 THEN H.suhu
					WHEN M.id=8 THEN H.tinggi_badan
					WHEN M.id=9 THEN H.berat_badan
					WHEN M.id=10 THEN H.suplemen_oksigen
					
				END as nilai
				FROM tpoliklinik_ews H,ews_master M
				WHERE H.assesmen_id='$assesmen_id' AND M.staktif='1'
				) H
				LEFT JOIN ews_master_skor M ON M.ews_id=H.id AND (H.nilai BETWEEN M.nilai_1 AND M.nilai_2) AND M.staktif='1'
				) G 

				) G LEFT JOIN ews_master_hasil_skor L ON (G.skor BETWEEN L.nilai_1 AND L.nilai_2) AND L.staktif='1'
				LEFT JOIN merm_referensi R ON R.nilai=L.hasil_skor AND R.ref_head_id='422'

			";
			$query = $CI->db->query($q)->row();
			if ($query){
				$data=array(
					'hasil_ews' => $query->hasil_ews,
					'warna_ews' => $query->warna,
					'total_skor_ews' => $query->skor_param,
				);
				$CI->db->where('assesmen_id',$assesmen_id);
				$CI->db->update('tpoliklinik_ews',$data);
			}
		  $q="
			SELECT G.jumlah as jumlah_param,G.skor as skor_param,L.hasil_parameter,L.warna,R.ref as hasil_ews FROM(
			SELECT G.skor,COUNT(G.skor) as jumlah FROM (
			SELECT H.*,COALESCE(M.skor,0) as skor  FROM( SELECT 
			M.id,M.nama_ews
			,CASE 
				WHEN M.id=1 THEN H.tingkat_kesadaran 
				WHEN M.id=2 THEN H.nadi
				WHEN M.id=3 THEN H.nafas
				WHEN M.id=4 THEN H.spo2
				WHEN M.id=5 THEN H.td_sistole
				WHEN M.id=6 THEN H.td_diastole
				WHEN M.id=7 THEN H.suhu
				WHEN M.id=8 THEN H.tinggi_badan
				WHEN M.id=9 THEN H.berat_badan
				WHEN M.id=10 THEN H.suplemen_oksigen
				
			END as nilai
			FROM tpoliklinik_ews H,ews_master M
			WHERE H.assesmen_id='$assesmen_id' AND M.staktif='1'
			) H
			LEFT JOIN ews_master_skor M ON M.ews_id=H.id AND (H.nilai BETWEEN M.nilai_1 AND M.nilai_2) AND M.staktif='1'
			) G 
			GROUP BY G.skor
			) G LEFT JOIN ews_master_hasil_parameter L ON L.jumlah_param=G.jumlah AND L.skor_param=G.skor AND L.staktif='1'
			LEFT JOIN merm_referensi R ON R.nilai=L.hasil_parameter AND R.ref_head_id='422'
			WHERE L.hasil_parameter IS NOT NULL
		  ";
        $query = $CI->db->query($q)->row();
		if ($query){
			$data=array(
				'hasil_ews' => $query->hasil_ews,
				'warna_ews' => $query->warna,
			);
			$CI->db->where('assesmen_id',$assesmen_id);
			$CI->db->update('tpoliklinik_ews',$data);
		}
	}
	function get_logic_pendaftaran_rehab($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM app_pendaftaran_rm_logic logic_0
			LEFT JOIN 
			(SELECT * FROM app_pendaftaran_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter' AND iddokter<>0
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM app_pendaftaran_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_pendaftaran_apm($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM apm_pendaftaran_logic logic_0
			LEFT JOIN 
			(SELECT * FROM apm_pendaftaran_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter' AND iddokter<>0
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM apm_pendaftaran_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_checkin_apm($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM apm_checkin_logic logic_0
			LEFT JOIN 
			(SELECT * FROM apm_checkin_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter' AND iddokter<>0
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM apm_checkin_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_reservasi_poli_petugas($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM setting_reservasi_petugas_logic logic_0
			LEFT JOIN 
			(SELECT * FROM setting_reservasi_petugas_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter'
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM setting_reservasi_petugas_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_checkin_poli_petugas($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM setting_checkin_petugas_logic logic_0
			LEFT JOIN 
			(SELECT * FROM setting_checkin_petugas_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter'
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM setting_checkin_petugas_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
		// print_r($q);exit;
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_checkin_rehab_petugas($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM setting_checkin_petugas_rm_logic logic_0
			LEFT JOIN 
			(SELECT * FROM setting_checkin_petugas_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter' AND iddokter<>0
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM setting_checkin_petugas_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
		  // print_r($q);exit;
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_reservasi_rehab_petugas($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM setting_reservasi_petugas_rm_logic logic_0
			LEFT JOIN 
			(SELECT * FROM setting_reservasi_petugas_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter' AND iddokter<>0
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM setting_reservasi_petugas_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
		  // print_r($q);exit;
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_daftar_poli_petugas($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM setting_daftar_petugas_logic logic_0
			LEFT JOIN 
			(SELECT * FROM setting_daftar_petugas_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter'
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM setting_daftar_petugas_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
        $query = $CI->db->query($q);
        return $query->row();
	}
	function get_logic_daftar_rehab_petugas($jenis_pertemuan_id,$idpoliklinik,$iddokter){
		 $CI =& get_instance();
		 
		  $q="
			SELECT logic_0.* FROM setting_daftar_petugas_rm_logic logic_0
			LEFT JOIN 
			(SELECT * FROM setting_daftar_petugas_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id' 
			 AND idpoliklinik =  '$idpoliklinik' 
			 AND iddokter = '$iddokter' AND iddokter<>0
			) logic_1 ON logic_1.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id
			LEFT JOIN
			(SELECT * FROM setting_daftar_petugas_rm_logic 
			 WHERE jenis_pertemuan_id = '$jenis_pertemuan_id'  
			 AND idpoliklinik =  '$idpoliklinik'  
			 AND iddokter = 0
			) logic_2 ON logic_2.jenis_pertemuan_id =  logic_0.jenis_pertemuan_id 

			WHERE logic_0.jenis_pertemuan_id = '$jenis_pertemuan_id' 
			AND (logic_0.idpoliklinik =  '$idpoliklinik'  OR logic_0.idpoliklinik = 0)
			AND (logic_0.iddokter = '$iddokter' OR logic_0.iddokter = 0)
			AND ((logic_1.id IS NULL AND logic_2.id IS NULL AND logic_0.idpoliklinik = 0 AND logic_0.iddokter = 0) OR 
				 (logic_1.id IS NULL AND logic_2.id IS NOT NULL AND logic_0.iddokter = 0) OR
					 (logic_1.id IS NOT NULL AND logic_0.idpoliklinik <> 0 AND logic_0.iddokter <> 0)
				  )
		  ";
		  // print_r($q);exit;
        $query = $CI->db->query($q);
        return $query->row();
	}

	function get_ppa_login(){
		 $CI =& get_instance();
		 
		  // $q="SELECT H.id as login_ppa_id, H.nip as login_nip_ppa, H.nama as login_nama_ppa FROM mppa H WHERE H.user_id='00'";
		  $q="SELECT H.id as login_ppa_id,H.tipepegawai as login_tipepegawai, H.nip as login_nip_ppa, H.nama as login_nama_ppa,H.pegawai_id as login_pegawai_id 
				,H.spesialisasi_id as login_spesialisasi_id,H.jenis_profesi_id as login_profesi_id
				FROM mppa H WHERE H.user_id='".$CI->session->userdata('user_id')."'";
		  // print_r($q);exit;
        $query = $CI->db->query($q)->row_array();
		if ($query){
			
		}else{
			$query=array(
				'login_ppa_id'=>'',
				'login_pegawai_id'=>'',
				'login_nip_ppa'=>'',
				'login_nama_ppa'=>'',
				'login_spesialisasi_id'=>'',
				'login_profesi_id'=>'',
				'login_tipepegawai'=>'', 
			);
			
		}
		$CI->session->set_userdata($query);
        return $query;
	}

	function search_tarif_lab($id,$tarif_lab){
		$value = array_values(search_object_value($tarif_lab,'id',$id));
		return $value[0];
	}


	function search_object_value($array, $index, $value)
{
	if (count((array)$array) > 0) {
		return array_filter($array, function ($var) use ($index, $value) { return isset($var->$index) && $var->$index == $value; });
	} else {
		return false;
	}
}