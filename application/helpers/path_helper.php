<?php
function backend_info()
{
    $CI =& get_instance();
    $data = array(
        'site_url'             => site_url(),
        'base_url'             => base_url(),

        'assets_path'          => base_url().'assets/',
        'css_path'             => base_url().'assets/css/',
        'fonts_path'           => base_url().'assets/fonts/',
        'img_path'             => base_url().'assets/img/',
        'js_path'              => base_url().'assets/js/',
        'plugins_path'         => base_url().'assets/js/plugins/',
        'upload_path'          => base_url().'assets/upload/',
        'ajax'                 => base_url().'assets/ajax/',
        'toastr_css'           => base_url().'assets/toastr/toastr.min.css',
        'toastr_js'            => base_url().'assets/toastr/toastr.min.js',

        'user_id'              => $CI->session->userdata('user_id'),
        'user_avatar'          => $CI->session->userdata('user_avatar'),
        'user_name'            => $CI->session->userdata('user_name'),
        'user_permission'      => $CI->session->userdata('user_permission'),
        'user_username'         => $CI->session->userdata('user_username'),
        'user_defaultunitpelayanan'     => $CI->session->userdata('user_defaultunitpelayanan'),
        'user_last_login'      => HumanDate($CI->session->userdata('user_last_login')),
    );

    $iduser = $CI->session->userdata('user_id');
    $query = $CI->db->query("SELECT L1.id from musers U
							INNER JOIN mroles_akses A ON A.role_id=U.roleid AND A.pilih='1'
							INNER JOIN mmenu_action L3 ON L3.id3=A.id3
							INNER JOIN mmenu_sub L2 ON L2.id2=L3.id_menu_sub
							INNER JOIN mmenu L1 ON L2.id_menu=L1.id
							WHERE U.id='$iduser'
							GROUP BY L1.id");

    $data['user_acces_menu']=(array_column($query->result_array(), 'id'));
//     $CI->db->query("UPDATE app_reservasi_tanggal_pasien INNER JOIN (
// SELECT H.id,H.tanggal,H.kode_antrian,K.kodeantrian_poli
// ,CONCAT(COALESCE(K.kodeantrian_poli,''),COALESCE(KD.kodeantrian_dokter,''),LPAD(H.noantrian,3,'0')) as kode_baru
// FROM app_reservasi_tanggal_pasien H
// LEFT JOIN antrian_poli_kode K ON K.idpoli=H.idpoli
// LEFT JOIN antrian_poli_kode_dokter KD ON KD.idpoli=H.idpoli AND KD.iddokter=H.iddokter
// WHERE H.kode_antrian IS NULL
// ) T ON T.id=app_reservasi_tanggal_pasien.id
// SET app_reservasi_tanggal_pasien.kode_antrian=T.kode_baru");
							
	$query = $CI->db->query("SELECT U.roleid,A.id3 from musers U
							INNER JOIN mroles_akses A ON A.role_id=U.roleid AND A.pilih='1'
							WHERE U.id='$iduser' ");

    $data['user_acces_form']=(array_column($query->result_array(), 'id3'));

    $query = $CI->db->query("SELECT * FROM mconfig_rs");
    $row = $query->row();
    if ($row) {
        $data['nama_rs']=$row->nama_rs;
        $data['alamat_rs1']=$row->alamat_rs1;
        $data['alamat_rs2']=$row->alamat_rs2;
        $data['telepon_rs']=$row->telepon_rs;
        $data['fax_rs']=$row->fax_rs;
        $data['email_rs']=$row->email_rs;
        $data['kota_rs']=$row->kota_rs;
        $data['kodepos_rs']=$row->kodepos_rs;
        $data['logo1_rs']=$row->logo1_rs;
        $data['logo2_rs']=$row->logo2_rs;
        $data['web_rs']=$row->web_rs;
    }

    $q="SELECT SUM( CASE WHEN STATUS = '2' THEN 1 ELSE 0 END) AS jml_keuangan,
				SUM( CASE WHEN STATUS = '3' THEN 1 ELSE 0 END) AS jml_pesanan
			FROM tgudang_pemesanan WHERE (`status` = '2' OR `status` = '3')
			AND tgudang_pemesanan.tipepemesanan IN (SELECT G.gudangtipe from musers_tipegudang G WHERE G.iduser='".$CI->session->userdata('user_id')."')";
    $query = $CI->db->query($q);
    $row = $query->row();

    
    $data['notif_pesanan']='';
    
    $data['jml_pesanan']='';

    if ($row) {
        // $jml_keuangan=$row->jml_keuangan;
        // $data['jml_keuangan']=$jml_keuangan;
        $jml_pesanan=$row->jml_pesanan;
        $data['jml_pesanan']=$jml_pesanan;
        // if ($jml_keuangan) {
            // $data['notif_keuangan']='<span class="label label-danger">'.$jml_keuangan.'</span>';
        // }
        if ($jml_pesanan) {
            $data['notif_pesanan']='<span class="label label-danger">'.$jml_pesanan.'</span>';
        }
    }
	$data['notif_keuangan']='';
	$q="SELECT  COUNT(A.id) as jml FROM tgudang_pemesanan_approval A
		WHERE A.iduser='".$CI->session->userdata('user_id')."' AND A.approve='0' AND A.st_aktif='1'";
	$query = $CI->db->query($q);
	$jml_keuangan = $query->row('jml');
	$data['jml_keuangan']=$jml_keuangan;
	if ($jml_keuangan){
		$data['notif_keuangan']='<span class="label label-danger">'.$jml_keuangan.'</span>';
	}
	
    $q="SELECT SUM(CASE WHEN H.jenis_pelayanan='1' THEN 1 ELSE 0 END) as jml_layanan_berkas,
		SUM(CASE WHEN H.jenis_pelayanan='2' THEN 1 ELSE 0 END) as jml_pinjam_berkas
		from trm_layanan_berkas H
		WHERE H.`status`='1' AND H.status_kirim='0'";

    $query = $CI->db->query($q);
    $row = $query->row();

    $data['notif_berkas']='';
    $data['notif_pinjam']='';
    if ($row) {
        $jml_berkas=$row->jml_layanan_berkas;
        $jml_pinjam=$row->jml_pinjam_berkas;
        if ($jml_berkas) {
            $data['notif_berkas']='<span class="label label-danger">'.$jml_berkas.'</span>';
        }
        if ($jml_pinjam) {
            $data['notif_pinjam']='<span class="label label-success">'.$jml_pinjam.'</span>';
        }
    }
	$q="SELECT COUNT(U.id) as jml_appvore FROM tkontrabon_info H
			INNER JOIN tkontrabon_user U ON H.id=U.kontrabon_info_id
			WHERE H.`status`='3' AND U.user_id='$iduser' AND U.approve = 0";
	$jml_appvore=$CI->db->query($q)->row('jml_appvore');
	if ($jml_appvore){
		$data['jml_appvore']=$jml_appvore;
	}else{
		$data['jml_appvore']='0';
	}
	
	$q="SELECT COUNT(AP.id) as jml_app_pengajuan from rka_pengajuan_approval AP
		LEFT JOIN rka_pengajuan H ON H.id=AP.idrka AND H.`status`='2'
		WHERE AP.iduser='$iduser' AND AP.st_aktif='1' AND AP.approve='0'";
	$jml_app_pengajuan=$CI->db->query($q)->row('jml_app_pengajuan');
	if ($jml_app_pengajuan){
		$data['jml_app_pengajuan']=$jml_app_pengajuan;
	}else{
		$data['jml_app_pengajuan']='0';
	}
	//Approval Lanjutan
	$q="SELECT COUNT(AP.id) as jml_app_pengajuan from rka_pengajuan_approval_lanjutan AP
		LEFT JOIN rka_pengajuan H ON H.id=AP.idrka AND H.`st_approval_lanjutan`='1' AND H.`status_approval_lanjutan`='0'
		WHERE AP.iduser='$iduser' AND AP.st_aktif='1' AND AP.approve='0'";
	$jml_app_pengajuan_lanjutan=$CI->db->query($q)->row('jml_app_pengajuan');
	if ($jml_app_pengajuan_lanjutan){
		$data['jml_app_pengajuan_lanjutan']=$jml_app_pengajuan_lanjutan;
	}else{
		$data['jml_app_pengajuan_lanjutan']='0';
	}
	
	//Approval Mutasi Kas
	$q="SELECT COUNT(AP.id) as jml_app_mutasi from tmutasi_kas_approval AP
		LEFT JOIN tmutasi_kas H ON H.id=AP.idtransaksi AND H.`st_approval`='1' AND H.`status_approval`='0'
		WHERE AP.iduser='$iduser' AND AP.st_aktif='1' AND AP.approve='0'";
	$jml_app_mutasi=$CI->db->query($q)->row('jml_app_mutasi');
	if ($jml_app_mutasi){
		$data['jml_app_mutasi']=$jml_app_mutasi;
	}else{
		$data['jml_app_mutasi']='0';
	}
	
	
	$q="SELECT COUNT(DISTINCT H.id) as jml  from rka_pengajuan H
		INNER JOIN rka_pengajuan_bendahara U ON U.idrka=H.id AND U.iduser='$iduser'
		WHERE H.`status`='3'
		GROUP BY H.id";
	$jml_bendahara_app=$CI->db->query($q)->row('jml');
	if ($jml_bendahara_app){
		$data['jml_bendahara_app']=$jml_bendahara_app;
	}else{
		$data['jml_bendahara_app']='0';
	}
	
	$q="SELECT COUNT(AP.id) as jml_app_pengajuan from tbagi_hasil_bayar_approval AP
		LEFT JOIN tbagi_hasil_bayar_head H ON H.id=AP.tbagi_hasil_bayar_id AND H.`status`='2'
		WHERE AP.iduser='$iduser' AND AP.st_aktif='1' AND AP.approve='0'";
	$jml_BH_approval=$CI->db->query($q)->row('jml_app_pengajuan');
	if ($jml_BH_approval){
		$data['jml_BH_approval']=$jml_BH_approval;
	}else{
		$data['jml_BH_approval']='0';
	}
	$q="SELECT COUNT(*) as jml_approval_honor from thonor_approval H
		WHERE H.st_aktif='1' AND H.iduser='$iduser' AND H.approve='0'";
	$jml_approval_honor=$CI->db->query($q)->row('jml_approval_honor');
// print_r($jml_approval_honor);exit();
	if ($jml_approval_honor){
		$data['jml_approval_honor']=$jml_approval_honor;
	}else{
		$data['jml_approval_honor']='0';
	}
	$q="SELECT COUNT(*) as jml_approval_refund from trefund_approval H
		WHERE H.st_aktif='1' AND H.iduser='$iduser' AND H.approve='0'";
	$jml_approval_refund=$CI->db->query($q)->row('jml_approval_refund');
	if ($jml_approval_refund){
		$data['jml_approval_refund']=$jml_approval_refund;
	}else{
		$data['jml_approval_refund']='0';
	}
	// print_r($jml_approval_refund);exit();
	$q="SELECT COUNT(*) as jml_approval_kasbon from tkasbon_approval H
		WHERE H.st_aktif='1' AND H.iduser='$iduser' AND H.approve='0'";
	$jml_approval_kasbon=$CI->db->query($q)->row('jml_approval_kasbon');
// print_r($jml_approval_honor);exit();
	if ($jml_approval_kasbon){
		$data['jml_approval_kasbon']=$jml_approval_kasbon;
	}else{
		$data['jml_approval_kasbon']='0';
	}
	$q="SELECT count(H.id) as jml_pembayaran_hd FROM thonor_dokter H
		WHERE H.status_approval='2' AND H.status_pembayaran='0'";
	$jml_pembayaran_hd=$CI->db->query($q)->row('jml_pembayaran_hd');
	if ($jml_pembayaran_hd){
		$data['jml_pembayaran_hd']=$jml_pembayaran_hd;
	}else{
		$data['jml_pembayaran_hd']='0';
	}
	
	$q="SELECT H.id,H.tanggal_saldo,H.periode from mperiode_akun H WHERE H.st_aktif='1' AND H.st_tutup='0'";
	$row_periode=$CI->db->query($q)->row();
	if ($row_periode){
		$data['idperiode_akuntansi']=$row_periode->id;
		$data['tanggal_awal_akuntansi']=($row_periode->tanggal_saldo);
		$data['periode_akuntansi']=get_periode($row_periode->periode);
	
	}
	
	// $q="SELECT *FROM user_sessions H
// WHERE H.`data` like '%logged_in|b:1%' AND CAST(UNIX_TIMESTAMP() AS UNSIGNED) - `timestamp` < 300";
	// $rows_session=$CI->db->query($q)->result();
	// foreach($rows_session as $row){	
		// $online = extract_user_data($row->data);
		// if(count($online) == 0){
			// $online = unserialize($row->data);
		// }   
	
		// // print_r($online['user_name']);exit();
	// }
	$data['rows_session']=array();
	// print_r($data['rows_session']);exit();
	// if ($row_periode){
		// $data['idperiode_akuntansi']=$row_periode->id;
		// $data['tanggal_awal_akuntansi']=($row_periode->tanggal_saldo);
		// $data['periode_akuntansi']=get_periode($row_periode->periode);
	
	// }
	
	// print_r();exit();
    return $data;
}

function get_acces()
{
    $CI =& get_instance();
    $data = array();
    $iduser=$CI->session->userdata('user_id');
    $query = $CI->db->query("SELECT L1.id from musers U
							INNER JOIN mroles_akses A ON A.role_id=U.roleid AND A.pilih='1'
							INNER JOIN mmenu_action L3 ON L3.id3=A.id3
							INNER JOIN mmenu_sub L2 ON L2.id2=L3.id_menu_sub
							INNER JOIN mmenu L1 ON L2.id_menu=L1.id
							WHERE U.id='$iduser'
							GROUP BY L1.id");
    $data['user_acces_menu']=(array_column($query->result_array(), 'id'));
    $query = $CI->db->query("SELECT U.roleid,A.id3 from musers U
							INNER JOIN mroles_akses A ON A.role_id=U.roleid AND A.pilih='1'
							WHERE U.id='$iduser' ");
    $data['user_acces_form']=(array_column($query->result_array(), 'id3'));
    return $data;
}
