<?php
function insert_direct_poli()
{
    $CI =& get_instance();
	$q="
		INSERT INTO tpoliklinik_e_resep (pendaftaran_id,idpasien,iddokter_resep,status_assemen,st_direct,tujuan_farmasi_id,tujuan_id,tanggal_kirim,tanggal_permintaan,created_date)
		SELECT H.id as pendaftaran_id,H.idpasien,M.id as iddokter_resep,'2' as status_assemen,1 as st_direct,L.tujuan_id,TF.mtujuan_antrian_id 
		,NOW(),NOW(),NOW()
		FROM `tpoliklinik_pendaftaran` H
		LEFT JOIN tpoliklinik_e_resep D ON D.pendaftaran_id=H.id
		LEFT JOIN mppa M ON M.pegawai_id=H.iddokter AND M.tipepegawai='2'
		LEFT JOIN mtujuan_farmasi_logic L ON L.idpoli_kelas=H.idpoliklinik
		LEFT JOIN mtujuan_farmasi TF ON TF.id=L.tujuan_id
		WHERE H.status !='0' AND H.idpoliklinik IN (SELECT idpoliklinik from mpoliklinik_direct) AND (H.tanggal IS NOT NULL OR H.tanggal >= '2024-03-05') AND D.assesmen_id IS NULL 
	";

    $CI->db->query($q);
}
function insert_awal_history_bed()
{
    $CI =& get_instance();
	$user_id=$CI->session->userdata('user_id');
	$q="
		INSERT INTO trawatinap_history_bed(idrawatinap,tanggaldari,tanggalsampai,idpegawai,idruangan,idkelas,idbed,status,created_by,created_date)
		SELECT H.id,DATE_FORMAT(H.tanggaldaftar,'%Y-%m-%d') as tanggaldari,'0000-00-00' as tanggalsampai,H.created_by as idpegawai 
		,H.idruangan,H.idkelas,H.idbed,1 as `status`,H.created_by as created_by,H.created_date created_date 
		FROM trawatinap_pendaftaran H
		LEFT JOIN trawatinap_history_bed M ON M.idrawatinap=H.id
		WHERE H.statuscheckout='0' AND H.st_terima_ranap='1' AND M.id IS NULL
	";

    $CI->db->query($q);
}
function insert_awal_history_bed_manual($id)
{
    $CI =& get_instance();
	$user_id=$CI->session->userdata('user_id');
	$q="
		INSERT INTO trawatinap_history_bed(idrawatinap,tanggaldari,tanggalsampai,idpegawai,idruangan,idkelas,idbed,status,created_by,created_date)
		SELECT H.id,DATE_FORMAT(H.tanggaldaftar,'%Y-%m-%d') as tanggaldari,'0000-00-00' as tanggalsampai,H.created_by as idpegawai 
		,H.idruangan,H.idkelas,H.idbed,1 as `status`,H.created_by as created_by,H.created_date created_date 
		FROM trawatinap_pendaftaran H
		LEFT JOIN trawatinap_history_bed M ON M.idrawatinap=H.id
		WHERE H.id='$id' AND M.id IS NULL
	";

    $CI->db->query($q);
}

function insertPoliklinikTindakan($id, $pendaftaran)
{
    $CI =& get_instance();
    
    $poliklinikId = $pendaftaran['idpoliklinik'];
    $dataDirectLaboratorium = logicDirectLaboratorium($poliklinikId);

    $data = [];
    $data['idpendaftaran'] = $id;
    $data['iddokter1'] = $pendaftaran['iddokter'];
    $data['iduser_tindakan'] = $CI->session->userdata('user_id');
    $data['tanggal_tindakan'] = date('Y-m-d H:i:s');

    if ($dataDirectLaboratorium) {
        $data['rujuklaboratorium'] = 1;
        $data['iduser_rujuk_laboratorium'] = $CI->session->userdata('user_id');
        $data['tanggal_rujuk_laboratorium'] = date('Y-m-d H:i:s');
        
        // $tujuanLaboratorium = $dataDirectLaboratorium->idtipe;
        // insertDirectPermintaanLaboratorium($id, $pendaftaran, $tujuanLaboratorium);
    }

    $CI->db->insert('tpoliklinik_tindakan', $data);
}
function insertPoliklinikTindakan_All()
{
    $CI =& get_instance();
    $q="INSERT INTO tpoliklinik_tindakan (idpendaftaran,iddokter1,iduser_tindakan,tanggal_tindakan)
	SELECT H.id,H.iddokter,COALESCE(H.created_by,R.created_by,1),COALESCE(H.created_date,R.created_date,NOW()) 
	FROM `tpoliklinik_pendaftaran` H
	LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=H.id
	LEFT JOIN app_reservasi_tanggal_pasien R ON R.id=H.reservasi_id
	WHERE H.tanggal = CURRENT_DATE() AND T.id IS NULL AND H.`status`='1'";

    
    $CI->db->query($q);
}

function insertDirectPermintaanLaboratorium($id, $pendaftaran, $tujuanLaboratorium)
{
    $CI =& get_instance();
    
    $data = [
        'pendaftaran_id' => $id,
        'pasien_id' => $pendaftaran['idpasien'],
        'asal_rujukan' => $pendaftaran['idtipe'],
        'jenis_pemeriksaan' => '1',
        'rencana_pemeriksaan' => YMDFormat($pendaftaran['tanggaldaftar']),
        'status_pemeriksaan' => '2',
        'tujuan_laboratorium' => $tujuanLaboratorium,
        'waktu_permintaan' => date('Y-m-d H:i:s'),
        'created_ppa' => $CI->session->userdata('login_ppa_id'),
        'created_at' => date("Y-m-d H:i:s"),
    ];

    // Insert data into term_laboratorium table
    $CI->db->insert('term_laboratorium_umum', $data);
}

function logicDirectLaboratorium($poliklinikId)
{
    $CI =& get_instance();
    
    $CI->db->select('idpoliklinik, idtipe');
    $CI->db->from('merm_pengaturan_direct_poliklinik_laboratorium');
    $CI->db->where('idpoliklinik', $poliklinikId);
    $query = $CI->db->get();

    if ($query->num_rows() > 0) {
        return $query->row_array();
    } else {
        return array();
    }
}

function getPembayaranHonorDokter($iddokter)
{
    $CI =& get_instance();

    if ($iddokter) {
        return DMYFormat($CI->db->query("SELECT GetPembayaranHonorDokter($iddokter) AS tanggal_pembayaran")->row()->tanggal_pembayaran);
    }
}
function status_estimasi_biaya($status_proses,$status_persetujuan_pasien,$alasan_menolak,$nama_kelas){
	  $btn='';
	  
	  $btn .=($status_proses==1?text_primary('TELAH DIPROSES'):text_default('MENUNGGU DIPROSES'));
	  if ($status_persetujuan_pasien=='0'){
		  $btn .='<br><br><span class="push 10-t">'.text_default('Menunggu Konfirmasi Pasien').'</span>';
	  }
	  if ($status_persetujuan_pasien=='1'){
		  $btn .='<br><br><span class="push 10-t">'.text_warning('Pasien Berunding').'</span>';
	  }
	  if ($status_persetujuan_pasien=='2'){
		  $btn .='<br><br><span class="push 10-t">'.text_success('Pasien Setuju, Kelas : '.$nama_kelas).'</span>';
	  }
	  if ($status_persetujuan_pasien=='3'){
		  $btn .='<br><br><span class="push 10-t">'.text_danger('Pasien Menolak').'</span>';
	  }
	
	  return $btn;
  }
function getJatuhTempoHonorDokter($iddokter)
{
	$CI = &get_instance();

	if ($iddokter) {
		return DMYFormat($CI->db->query("SELECT GetJatuhTempoHonorDokter($iddokter) AS tanggal_jatuhtempo")->row()->tanggal_jatuhtempo);
	}
}

function getPembayaranFeeRujukanRS($idasalpasien, $idrujukan)
{
    $CI =& get_instance();

    if ($idasalpasien && $idrujukan) {
        $row = $CI->db->query("SELECT GetPembayaranFeeRujukanRS($idasalpasien, $idrujukan) AS tanggal_pembayaran")->row();
        if (isset($row->tanggal_pembayaran)) {
            return YMDFormat($row->tanggal_pembayaran);
        } else {
            return 'null';
        }
    } else {
        return '-';
    }
}
function ReplaceKutip($str){
	return str_replace("'", "''", $str);
	// return $str.eplace("'", "''");
}
function extract_user_data($user_data)
{
    $session_data = $user_data ;
    $return_data = array();  
             
    $offset = 0;
    while ($offset < strlen($session_data)) 
    {
       if (!strstr(substr($session_data, $offset), "|")) 
        {
          return array();
        }
          $pos = strpos($session_data, "|", $offset);
          $num = $pos - $offset;
          $varname = substr($session_data, $offset, $num);
          $offset += $num + 1;
          $data = unserialize(substr($session_data, $offset));
          $return_data[$varname] = $data;  
          $offset += strlen(serialize($data));
    }
    // print_r($return_data);exit;
    return $return_data;
}
function getJatuhTempoFeeRujukanRS($idasalpasien, $idrujukan)
{
	$CI = &get_instance();

	if ($idasalpasien && $idrujukan) {
		$row = $CI->db->query("SELECT GetJatuhTempoFeeRujukanRS($idasalpasien, $idrujukan) AS tanggal_jatuhtempo")->row();
        if (isset($row->tanggal_jatuhtempo)) {
            return YMDFormat($row->tanggal_jatuhtempo);
        } else {
            return 'null';
        }
	} else {
        return '-';
    }
}

function getRekeningPS($id)
{
    $CI =& get_instance();

    if ($id) {
        $opsi='';
        $result=$CI->db->query("SELECT M.id,R.bank,M.atas_nama,M.norek,M.pilih_default FROM `mpemilik_saham_rek` M
			LEFT JOIN ref_bank R ON R.id=M.idbank WHERE M.idpemilik='$id'")->result();
        foreach ($result as $r) {
            $opsi .='<option value="'.$r->id.'" '.($r->pilih_default=='1'?"selected":"").'>'.$r->bank.' - '.$r->norek.'</option>';
        }
        return $opsi;
    }
}
function getRekeningPSEdit($id, $idrek)
{
    $CI =& get_instance();

    if ($id) {
        $opsi='';
        $result=$CI->db->query("SELECT M.id,R.bank,M.atas_nama,M.norek,M.pilih_default FROM `mpemilik_saham_rek` M
			LEFT JOIN ref_bank R ON R.id=M.idbank WHERE M.idpemilik='$id'")->result();
        foreach ($result as $r) {
            $opsi .='<option value="'.$r->id.'" '.($r->id==$idrek?"selected":"").'>'.$r->bank.' - '.$r->norek.'</option>';
        }
        return $opsi;
    }
}
function getRekeningPS_ID($id)
{
    $CI =& get_instance();

    if ($id) {
        $result=$CI->db->query("SELECT M.id,R.bank,M.atas_nama,M.norek,M.idbank FROM `mpemilik_saham_rek` M
			LEFT JOIN ref_bank R ON R.id=M.idbank WHERE M.id='$id'")->row();

        return $result;
    }
}
function getMetode()
{
    $CI =& get_instance();


    $opsi='';
    $result=$CI->db->query("SELECT * FROM `ref_metode` WHERE `status`='1' ORDER BY urutan ")->result();
    // $opsi .='<option value="#" selected>- Pilih -</option>';
    foreach ($result as $r) {
        $opsi .='<option value="'.$r->id.'">'.$r->metode_bayar.'</option>';
    }
    return $opsi;
}
function getMetodeEdit($id='')
{
    $CI =& get_instance();


    $opsi='';
    $result=$CI->db->query("SELECT * FROM `ref_metode` WHERE `status`='1' ORDER BY urutan ")->result();
    $opsi .='<option value="#">- Pilih -</option>';
    foreach ($result as $r) {
        $opsi .='<option value="'.$r->id.'" '.($id==$r->id?"selected":"").'>'.$r->metode_bayar.'</option>';
    }
    return $opsi;
}

function menuIsActive($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(1) == $menu) ? "class=\"active\"" : "";
}
function menuIsActiveTindakan($menu)
{
    $CI =& get_instance();
	$path_tindakan=$CI->session->userdata('path_tindakan');
    return ($menu == $path_tindakan) ? "class=\"active\"" : "";
}

function menuIsActive2($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(2) == $menu) ? "class=\"active\"" : "";
}

function menuIsActive3($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(3) == $menu) ? "class=\"active\"" : "";
}

function cekMenu_Active_2($menu1,$menu2)
{
    $CI =& get_instance();
    return ($CI->uri->segment(1) == $menu1 && $CI->uri->segment(2) == $menu2) ? "class=\"active\"" : "";
}

function BackendBreadcrum($data)
{
    $result = array();
    foreach ($data as $row) {
        if ($row[1] == '#') {
            $url = '<li><a class="text-muted" href="#">' . $row[0] . '</a></li>';
        } elseif ($row[1] == '') {
            $url = '<li><a class="link-effect" href="#">' . $row[0] . '</a></li>';
        } else {
            $url = '<li><a class="link-effect" href="' . site_url($row[1]) . '">' . $row[0] . '</a></li>';
        }
        array_push($result, $url);
    }

    return implode('', $result);
}

function TreeView($level, $name)
{
    $indent = '';
    for ($i = 0; $i < $level; $i++) {
        if ($i > 0) {
            $indent .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        } else {
            $indent .= '';
        }
    }

    if ($i > 0) {
        $indent .= '└─';
    } else {
        $indent .= '';
    }

    return $indent . ' ' . $name;
}

function TreeView2($level, $name)
{
    $indent = '';
    for ($i = 0; $i < $level; $i++) {
        if ($i > 0) {
            $indent .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        } else {
            $indent .= '';
        }
    }

    if ($i > 0) {
        $indent .=' ';
    } else {
        $indent .= '';
    }

    return $indent . ' ' . $name;
}
function TreeView_excel($level, $name)
{
    $indent = '';
    for ($i = 0; $i < $level; $i++) {
        if ($i > 0) {
            $indent .= '      ';
        } else {
            $indent .= '';
        }
    }

    if ($i > 0) {
        $indent .= '└─';
    } else {
        $indent .= '';
    }

    return $indent . ' ' . $name;
}

function WarningStok($stok, $stokreorder, $stokminimum)
{
    if ($stok <= $stokminimum) {
        return '<span class="label label-md label-danger" style="font-size: 11px;" data-toggle="tooltip" title="Stok Reoder (' . number_format($stokreorder) . ')">' . number_format($stok) . '</span>';
    } elseif ($stok <= $stokreorder) {
        return '<span class="label label-md label-warning" style="font-size: 11px;"data-toggle="tooltip" title="Stok Minimun (' . number_format($stokminimum) . ')">' . number_format($stok) . '</span>';
    } else {
        return '<span class="label label-md label-success" style="font-size: 11px;"data-toggle="tooltip" title="Stok Normal">' . number_format($stok) . '</span>';
    }
}

function LabelWarningStok($stok, $stokreorder, $stokminimum)
{
    if ($stok <= $stokminimum) {
        return '<span class="label label-md label-danger" style="font-size: 11px;">Back To Order</span>';
    } elseif ($stok <= $stokreorder) {
        return '<span class="label label-md label-warning" style="font-size: 11px;">Minimum Stock</span>';
    } else {
        return '<span class="label label-md label-success" style="font-size: 11px;">Available</span>';
    }
}

function LabelSettingAnalisa($status)
{
    if ($status) {
        return '<span class="label label-md label-success" style="font-size: 11px;">Active</span>';
    } else {
        return '<span class="label label-md label-danger" style="font-size: 11px;">Not Active</span>';
    }
}

function getFieldTable($table)
{
    $ci =& get_instance();
    $fields = $ci->db->list_fields($table);
    foreach ($fields as $r) {
        $data[$r] = '';
    }
    return $data;
}

// Function Format Date
function MONTHFormat($date)
{
    switch ($date) {
        case 1:
            $date = "Januari";
            break;
        case 2:
            $date = "Februari";
            break;
        case 3:
            $date = "Maret";
            break;
        case 4:
            $date = "April";
            break;
        case 5:
            $date = "Mei";
            break;
        case 6:
            $date = "Juni";
            break;
        case 7:
            $date = "Juli";
            break;
        case 8:
            $date = "Agustus";
            break;
        case 9:
            $date = "September";
            break;
        case 10:
            $date = "Oktober";
            break;
        case 11:
            $date = "November";
            break;
        case 12:
            $date = "Desember";
            break;
    }
    return $date;
}

function DMYFormat($date)
{
    if ($date) {
        return date('d/m/Y', strtotime($date));
    } else {
        return '-';
    }
}
function YFormat($date)
{
    if ($date) {
        return date('Y', strtotime($date));
    } else {
        return '-';
    }
}
function DFormat($date)
{
    if ($date) {
        return date('d', strtotime($date));
    } else {
        return '-';
    }
}
function MFormat($date)
{
    if ($date) {
        return date('m', strtotime($date));
    } else {
        return '-';
    }
}
function tanggal_indo($tanggal)
{
    $bulan = array(1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split = explode('-', $tanggal);
    return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}
function tanggal_indo_DM($tanggal)
{
    $bulan = array(1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split = explode('-', $tanggal);
    return $split[2] . '  ' . $bulan[ (int)$split[1] ];
}
function tanggal_indo_MY($tanggal)
{
    $bulan = array(1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split = explode('-', $tanggal);
    return  $bulan[ (int)$split[1] ].'  '.$split[0];
}
function tanggal_indo_DMY($tanggal)
{
	$tanggal=YMDFormat($tanggal);
    $bulan = array(1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split = explode('-', $tanggal);
    return  $split[2].' '.$bulan[ (int)$split[1] ].'  '.$split[0];
}
function DMYFormat2($date)
{
    return date('d-m-Y', strtotime($date));
}

function DMYTimeFormat($date)
{
    return date('d/m/Y H:i:s', strtotime($date));
}

function HISTimeFormat($date)
{
    return date('H:i:s', strtotime($date));
}
// function YMDTimeFormat($date)
// {
    // return date('Y/m/d H:i:s', strtotime($date));
// }

function YMDFormat($date)
{
    $dateConv = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($dateConv));
}
function YMFormat($date)
{
    $dateConv = str_replace('/', '-', $date);
    return date('Ym', strtotime($dateConv));
}
function tanggal_format($date)
{
    $dateConv = str_replace('/', '-', $date);
    return date('d', strtotime($dateConv));
}

function YMDTimeFormat($date)
{
    $dateConv = str_replace('/', '-', $date);
    return date('Y-m-d H:i:s', strtotime($dateConv));
}
function YMDMenitFormat($date)
{
    $dateConv = str_replace('/', '-', $date);
    return date('Y-m-d H:i', strtotime($dateConv));
}

function HumanDate($date)
{
    $date = date('j F Y', strtotime($date));

    return $date;
}

function HumanDateMy($date)
{
    $date = date('F Y', strtotime($date));

    return $date;
}
function convert_month($num){
    switch($num){
        case "01":
            return "Januari";
            break;
        case "02":
            return "Februari";
            break;
        case "03":
            return "Maret";
            break;
        case "04":
            return "April";
            break;
        case "05":
            return "Mei";
            break;
        case "06":
            return "Juni";
            break;
        case "07":
            return "Juli";
            break;
        case "08":
            return "Agustus";
            break;
        case "09":
            return "September";
            break;
        case "10":
            return "Oktober";
            break;
        case "11":
            return "November";
            break;
        case "12":
            return "Desember";
            break;
    }
}
function get_nama_periode($bulan,$tahun){
	$bulan=convert_month($bulan);
	return $bulan .' '.$tahun;
}
function DayMonthYear($date)
{
    $day = date('D', strtotime($date));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );

    $date = $dayList[$day] . ', ' . date('d-m-Y', strtotime($date));

    return $date;
}
function GetDateNow()
{
	$date=date('Y-m-d');
    $day = date('D', strtotime($date));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );

    $date = $dayList[$day] . ', ' . date('d-m-Y', strtotime($date));

    return $date;
}

function HumanDateShort($date)
{
    $date = date('d-m-Y', strtotime($date));

    return $date;
}
function HumanDateShort2($date)
{
    $date = date('d/m/Y', strtotime($date));

    return $date;
}
// function HumanDateShort_exp($date)
// {
    // $date = date('d-m-Y', strtotime($date));
    // if ($date=='01-01-1970') {
        // return '-';
    // } else {
        // return $date;
    // }
// }
function HumanDateLong($date)
{
    $date = date('d-m-Y H:i:s', strtotime($date));

    return $date;
}
function HumanDateLong2($date)
{
    $date = date('d-m-Y H:i', strtotime($date));

    return $date;
}
function HumanTime($date)
{
    $date = date('H:i:s', strtotime($date));

    return $date;
}
function HumanTimeShort($date)
{
    $date = date('H:i', strtotime($date));

    return $date;
}

function HumanDateTime($date)
{
    $date = date('j F Y h:i:s', strtotime($date));

    return $date;
}

function LastLoginDate($date)
{
    if ($date != '' && $date != '0000-00-00 00:00:00') {
        $date = date('j F Y h:i:s', strtotime($date));
    } else {
        $date = 'First Login';
    }

    return $date;
}

function GetDay($date)
{
    $date = date('D, j F Y', strtotime($date));

    return $date;
}

function GetDayIndonesia($date, $printDay = false, $style = 'text')
{
    $day = array(
    1 => 'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jumat',
        'Sabtu',
        'Minggu'
    );

    $month = array(
    1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );

    $split 	  = explode('-', $date);
    $format = $month[ (int)$split[1] ] . ' ' . $split[0];

    if ($printDay) {
        if ($style == 'number') {
            return $split[2] . ' ' . $month[ (int)$split[1] ] . ' ' . $split[0];
        } else {
            $num = date('N', strtotime($date));
            return $day[$num] . ', ' . $split[2] . ' ' . $format;
        }
    }

    return $format;
}

function GetMonthIndo($date)
{
    $month = array(
      1 => 'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
    );

    $split 	  = explode('-', $date);
    $format = $month[ (int)$split[1] ] . ' ' . $split[0];

    return $format;
}

function UserAccesForm($user_acces_form, $arr)
{
    $res=0;
    foreach ($arr as $row) {
        if (in_array($row, $user_acces_form, false)) {
            $res=1;
        }
    }
    return $res;
}
// EOF Function Format Date

function button_roles($link_navigation)
{
    $ci =& get_instance();
    $items = array();
    if ($ci->session->userdata('user_id')) {
        $ci->db->select('mresources.nama as controller');
        $ci->db->from('musers');
        $ci->db->join('mroles_detail', 'musers.roleid = mroles_detail.idrole', 'left');
        $ci->db->join('mresources', 'mroles_detail.idresource = mresources.id', 'left');
        $ci->db->where('musers.id', $ci->session->userdata('user_id'));
        $ci->db->where('mroles_detail.status', 2);
        $controllers = $ci->db->get()->result_array();

        foreach ($controllers as $control) {
            $item     = array();
            $item     = strtolower($control['controller']);
            $items[]  = $item;
        }

        if (in_array(strtolower($link_navigation), $items)) {
            return true;
        }
    }
    return true;
}
function get_logic($idunit, $tipe_rka, $idjenis)
{
    $ci =& get_instance();

    $q="SELECT MU.idlogic,M.nama as logic, MD.id as iddet,MD.operand,MD.nominal,MD.iduser,MD.proses_setuju,MD.proses_tolak,MD.step
		from mlogic_unit MU
		INNER JOIN mlogic_detail MD ON MD.idlogic=MU.idlogic AND MD.tipe_rka='$tipe_rka' AND MD.idjenis='$idjenis' AND MD.`status`='1'
		LEFT JOIN mlogic M ON M.id=MU.idlogic
		WHERE MU.idunit='$idunit'
		GROUP BY MU.idlogic
		";

    return $ci->db->query($q)->row('logic');
}

function navigation_roles($link_navigation)
{
    // $ci =& get_instance();
    // $items = array();
    // if($ci->session->userdata('user_id')) {

    // $ci->db->select('mresources.nama as controller');
    // $ci->db->from('musers');
    // $ci->db->join('mroles_detail', 'musers.roleid = mroles_detail.idrole', 'left');
    // $ci->db->join('mresources', 'mroles_detail.idresource = mresources.id', 'left');
    // $ci->db->where('musers.id', $ci->session->userdata('user_id') );
    // $ci->db->where('mroles_detail.status', 2);
    // $controllers = $ci->db->get()->result_array();

    // foreach($controllers as $control) {
    // $item     = array();
    // $item     = strtolower($control['controller']);
    // $items[]  = $item;
    // }

    // if(in_array( strtolower($link_navigation), $items)) {
    // return true;
    // }
    // }
    return true;
}


function roles()
{
    // $ci =& get_instance();
    // if($ci->session->userdata('user_id')) {

    // $items = array();

    // $ci->db->select('mresources.nama as controller');
    // $ci->db->from('musers');
    // $ci->db->join('mroles_detail', 'musers.roleid = mroles_detail.idrole', 'left');
    // $ci->db->join('mresources', 'mroles_detail.idresource = mresources.id', 'left');
    // $ci->db->where('musers.id', $ci->session->userdata('user_id') );
    // $ci->db->where('mroles_detail.status', 2);
    // $controllers = $ci->db->get()->result_array();

    // foreach($controllers as $control) {
    // $item     = array();
    // $item     = strtolower($control['controller']);
    // $items[]  = $item;
    // }

    // $uri_string = strtolower($ci->uri->segment(1));
    // if($ci->uri->segment(2)) {
    // $uri_string = strtolower( $ci->uri->segment(1). '/' .$ci->uri->segment(2) );
    // }
    // // $uri_string = uri_string();
    // if(in_array($uri_string, $items) || uri_string() == 'musers/sign_out') {
    // return true;
    // } else {
    // redirect('forbidden','refresh');
    // }
    // }
    return true;
}

// Start Function Permission Access
function PermissionUserLoggedIn($session)
{
	// print_r('GO');exit;
    // roles();
    if (!$session->userdata('logged_in')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
		$session->set_userdata(array('referer' => current_url()));
        redirect('sign_in');
    }
}

function PermissionUserLogin($session)
{
    if ($session->userdata('logged_in')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
        redirect('dashboard');
    }
}

// EOF Function Permission Access

// Start Function Notify Message
function ErrorSuccess($session)
{
    if ($session->flashdata('error')) {
        return ErrorMessage($session->flashdata('message_flash'));
    } elseif ($session->flashdata('confirm')) {
        return SuccesMessage($session->flashdata('message_flash'));
    } else {
        return '';
    }
}

function ErrorMessage($message)
{
    return '<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h3 class="font-w300 push-15">Error!</h3>
        <p>' . $message . '</p>
      </div>';
}

function SuccesMessage($message)
{
    return '<div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h3 class="font-w300 push-15">Success!</h3>
      <p>' . $message . '</p>
    </div>';
}

// EOF Function Notify Message

// Start Function Format Number
function number($number)
{
    return number_format($number, 0);
}
function status_pesan_gudang($id, $label='')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>',
            '1' => '<span class="label label-default" data-toggle="tooltip" title="PENDING">PENDING</span>',
            '2' => '<span class="label label-warning" data-toggle="tooltip" title="PENDING">PERSETUJUAN KEUANGAN</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="PENDING">DISETUJUI KEUANGAN</span> <span class="label label-warning" data-toggle="tooltip" title="PENDING">PERSETUJUAN PEMESANAN</span>',
            '4' => '<span class="label label-success" data-toggle="tooltip" title="PENDING">DISETUJUI - PROSES PEMESANAN</span>',
            '5' => '<span class="label label-success" data-toggle="tooltip" title="SELESAI">SELESAI</span>',
            '6' => '<span class="label label-success" data-toggle="tooltip" title="SELESAI">SELESAI DIALIHKAN</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function status_bagi_hasil_bayar($id, $setuju='')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">DIBATALKAN</span>',
            '1' => '<span class="label label-default">DRAFT</span>',
            '2' => '<span class="label label-primary">PROSES PERSETUJUAN '.($setuju!=''?"STEP ".$setuju:"").'</span>',
            '3' => '<span class="label label-warning">PROSE PEMBAYARAN</span>',
            '4' => '<span class="label label-success">SELESAI</span>',
        );
        // if ($setuju !=''){
        // return $data[$id];
        // }else{
        // if ($setuju=='0'){
        // return '<span class="label label-primary">PROSES PERSETUJUAN</span>';
        // }else{

        // }
        // }
        return $data[$id];
    } else {
        return '';
    }
}
function status_approval_HD($id, $setuju='')
{
    if ($id != '') {
        $data = array(
            '0' => '',
            '1' => '<span class="label label-default">MENUNGGU APPROVAL '.($setuju!=''?"STEP ".$setuju:"").'</span>',
            '2' => '<span class="label label-primary">DISETUJUI</span>',
            '3' => '<span class="label label-danger">DITOLAK</span>',
            '4' => '<span class="label label-success">SELESAI</span>',
        );
        // if ($setuju !=''){
        // return $data[$id];
        // }else{
        // if ($setuju=='0'){
        // return '<span class="label label-primary">PROSES PERSETUJUAN</span>';
        // }else{

        // }
        // }
        return $data[$id];
    } else {
        return '';
    }
}
function tipe_jurnal($id, $nama='')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">DIBATALKAN</span>',
            '1' => '<span class="label label-primary">'.$nama.'</span>',
            '2' => '<span class="label label-danger">'.$nama.'</span>',
            '3' => '<span class="label label-success">'.$nama.'</span>',
        );
       
        return $data[$id];
    } else {
        return '';
    }
}
function asal_rujukan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-warning">Non Rujukan</span>',
            '1' => '<span class="label label-info">Poliklinik</span>',
            '2' => '<span class="label label-primary">IGD</span>',
            '3' => '<span class="label label-success">Rawat Inap</span>',
        );
       
        return $data[$id];
    } else {
        return '';
    }
}
function idpoli_kelas($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-default">Penjualan Bebas</span>',
            '1' => '<span class="label label-info">Pasien RS</span>',
            '2' => '<span class="label label-info">Pegawai</span>',
            '3' => '<span class="label label-info">Dokter</span>',
        );
       
        return $data[$id];
    } else {
        return '';
    }
}
function get_tipe_trx($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-success">TRANSAKSI</span>',
            '2' => '<span class="label label-primary">PIUTANG</span>',
            '3' => '<span class="label label-danger">TAGIHAN KARYAWAN</span>',
            '4' => '<span class="label label-default">TIDAK TERTAGIH</span>',
            '5' => '<span class="label label-info">REFUND</span>',
            '6' => '<span class="label label-warning">INFORMASI MEDIS</span>',
        );
       
        return $data[$id];
    } else {
        return '';
    }
}
function GetTipeTransaksiDetail($id,$tipe_refund='')
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-primary">Poliklinik</span>',
            '2' => '<span class="label label-primary">IGD</span>',
            '3' => '<span class="label label-warning">Obat Bebas</span>',
            '4' => '<span class="label label-default">Refund '.($tipe_refund=='1'?' Obat':' Transaksi').'</span>',
        );
       
        return $data[$id];
    } else {
        return '';
    }
}
function status_bagi_hasil($id, $label='')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>',
            '1' => '<span class="label label-default" data-toggle="tooltip" title="Sedang Diproses">Sedang Diproses</span>',
            '2' => '<span class="label label-success" data-toggle="tooltip" title="Sudah Diproses">Sudah Diproses</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="Sudah Diproses">Sudah Diproses Pembayaran</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function status_pemeriksaan_laboratorium($id, $label='')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
            '1' => '<span class="label label-warning" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
            '2' => '<span class="label label-info" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
            '4' => '<span class="label label-success" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
            '5' => '<span class="label label-success" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
            '6' => '<span class="label label-success" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
            '7' => '<span class="label label-success" data-toggle="tooltip" title="' . $label . '">' . $label . '</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function StatusInfo($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>',
            '1' => '<span class="label label-default" data-toggle="tooltip" title="Sedang Diproses">DRAFT</span>',
            '2' => '<span class="label label-success" data-toggle="tooltip" title="Sudah Diproses">PUBLISH</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="Sudah Diproses">SELESAI</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function status_pengajuan($id, $step='')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>',
            '1' => '<span class="label label-default" data-toggle="tooltip" title="DRAFT">DRAFT</span>',
            '2' => '<span class="label label-warning" data-toggle="tooltip" title="STEP PROSES">PERSETUJUAN STEP '.$step.'</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="PROSES BENDAHARA">PROSES BENDAHARA</span>',
            '4' => '<span class="label label-info" data-toggle="tooltip" title="PROSES BELANJA">PROSES BELANJA</span>',
            '5' => '<span class="label label-success" data-toggle="tooltip" title="SELESAI">SELESAI</span>',
            '99' => '<span class="label label-default" data-toggle="tooltip" title="OVER">GANTI CICILAN</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function status_approval($id, $step='')
{
    if ($id != '') {
        $data = array(
            '0' => ' <span class="label label-default" data-toggle="tooltip"><i class="si si-clock"></i>    Menunggu</span>',
            '1' => ' <span class="label label-success" data-toggle="tooltip"><i class="fa fa-check"></i>    Setuju</span>',
            '2' => ' <span class="label label-danger" data-toggle="tooltip"><i class="fa fa-times"></i>    Menolak</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function status_feerujukan($id)
{
    if ($id != '') {
        $data = array(
            '0' => ' <span class="label label-danger" data-toggle="tooltip"><i class="si si-clock"></i> On Proses</span>',
            '1' => ' <span class="label label-default" data-toggle="tooltip"><i class="si si-clock"></i> Selesai Perhitungan</span>',
            '2' => ' <span class="label label-default" data-toggle="tooltip"><i class="si si-clock"></i> Menunggu Approval</span>',
            '3' => ' <span class="label label-success" data-toggle="tooltip"><i class="fa fa-check"></i> Disetujui</span>',
            '4' => ' <span class="label label-danger" data-toggle="tooltip"><i class="fa fa-times"></i> Ditolak</span>',
            '5' => ' <span class="label label-warning" data-toggle="tooltip"><i class="fa fa-check"></i> Pembayaran</span>',
            '6' => ' <span class="label label-success" data-toggle="tooltip"><i class="fa fa-times"></i> Telah Dibayarkan</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function status_rka($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIHAPUS">DIHAPUS</span>',
            '1' => '<span class="label label-default" data-toggle="tooltip" title="DRAFT">DRAFT</span>',
            '2' => '<span class="label label-info" data-toggle="tooltip" title="PUBLISH">PUBLISH</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="ACTIVE">ACTIVE</span>',
            '4' => '<span class="label label-success" data-toggle="tooltip" title="SELESAI">SELESAI</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function get_table_id($id)
{
    if ($id != '') {
        $data = array(
            'mtarif_administrasi' => '1',
            'mtarif_rawatjalan' => '2',
            'mtarif_rawatinap' => '3',
            'mtarif_visitedokter' => '4',
            'mtarif_radiologi' => '5',
            'mtarif_laboratorium' => '6',
            'mtarif_fisioterapi' => '7',
            'mtarif_operasi_sewaalat' => '8',
            'mtarif_operasi' => '9',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function status_setting($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip">BELUM DIATUR</span>',
            '1' => '<span class="label label-success" data-toggle="tooltip">SUDAH DIATUR</span>',
            '2' => '<span class="label label-success" data-toggle="tooltip">SUDAH DIATUR</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function tipe_rka($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIHAPUS">DIHAPUS</span>',
            '1' => '<span class="label label-success" data-toggle="tooltip" title="RKA">RKA</span>',
            '2' => '<span class="label label-primary" data-toggle="tooltip" title="NON RKA">NON RKA</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="ACTIVE">ACTIVE</span>',
            '4' => '<span class="label label-success" data-toggle="tooltip" title="SELESAI">SELESAI</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function get_status($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIHAPUS">NOT ACTIVE</span>',
            '1' => '<span class="label label-success" data-toggle="tooltip" title="RKA">ACTIVE</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function get_status_setoran_rj($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">DIBATALKAN</span>',
            '1' => '<span class="label label-default">ON PROSES</span>',
            '2' => '<span class="label label-success">SELESAI</span>',
            '3' => '<span class="label label-warning">PROSES PEMERIKSAAN</span>',
            '4' => '<span class="label label-primary">SELESAI PEMERIKSAAN</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function status_retur_gudang($id, $label='')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>',
            '1' => '<span class="label label-default" data-toggle="tooltip" title="PENDING">PENDING</span>',
            '2' => '<span class="label label-warning" data-toggle="tooltip" title="PENDING">SUDAH DIPROSES</span>',
            '3' => '<span class="label label-primary" data-toggle="tooltip" title="PENDING">SELESAI</span>',
            '4' => '<span class="label label-success" data-toggle="tooltip" title="PENDING">DISETUJUI - PROSES PEMESANAN</span>',
            '5' => '<span class="label label-success" data-toggle="tooltip" title="SELESAI">SELESAI</span>',
            '6' => '<span class="label label-success" data-toggle="tooltip" title="SELESAI">SELESAI DIALIHKAN</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function get_bulan($bulan)
{
    switch ($bulan) {
        case '01':
            $date = "Januari";
            break;
        case '02':
            $date = "Februari";
            break;
        case '03':
            $date = "Maret";
            break;
        case '04':
            $date = "April";
            break;
        case '05':
            $date = "Mei";
            break;
        case '06':
            $date = "Juni";
            break;
        case '07':
            $date = "Juli";
            break;
        case '08':
            $date = "Agustus";
            break;
        case '09':
            $date = "September";
            break;
        case '10':
            $date = "Oktober";
            break;
        case '11':
            $date = "November";
            break;
        case '12':
            $date = "Desember";
            break;
    }
    return $date;
}
function get_periode($periode)
{
	$bulan=substr($periode,-2);
	$tahun=substr($periode,0,4);
    switch ($bulan) {
        case '01':
            $date = "Januari";
            break;
        case '02':
            $date = "Februari";
            break;
        case '03':
            $date = "Maret";
            break;
        case '04':
            $date = "April";
            break;
        case '05':
            $date = "Mei";
            break;
        case '06':
            $date = "Juni";
            break;
        case '07':
            $date = "Juli";
            break;
        case '08':
            $date = "Agustus";
            break;
        case '09':
            $date = "September";
            break;
        case '10':
            $date = "Oktober";
            break;
        case '11':
            $date = "November";
            break;
        case '12':
            $date = "Desember";
            break;
    }
    return $date.' '.$tahun;
}
function get_periode_2($periode)
{
	$bulan=substr($periode,-2);
	$tahun=substr($periode,0,4);
    switch ($bulan) {
        case '01':
            $date = "Jan";
            break;
        case '02':
            $date = "Feb";
            break;
        case '03':
            $date = "Mar";
            break;
        case '04':
            $date = "Apr";
            break;
        case '05':
            $date = "Mei";
            break;
        case '06':
            $date = "Jun";
            break;
        case '07':
            $date = "Jul";
            break;
        case '08':
            $date = "Ags";
            break;
        case '09':
            $date = "Sep";
            break;
        case '10':
            $date = "Okt";
            break;
        case '11':
            $date = "Nov";
            break;
        case '12':
            $date = "Des";
            break;
    }
    return $date.' '.$tahun;
}
function RemoveComma($number, $delimiter = ',')
{
    return str_replace($delimiter, '', $number);
}
function replace_string($string, $delimiter = ',', $ganti='')
{
    return str_replace($delimiter, $ganti, $string);
}
function RemoveBintang($string)
{
    return str_replace('*', '', $string);
}
// EOF Function Format Number

// Start Function Get Label
// Get Label Kategori
function GetKategoriPegawai($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Pegawai Umum',
            '2' => 'Pegawai Kamar Operasi',
            '3' => 'Pegawai Fisioterapi',
            '4' => 'Pegawai Anesthesi',
            '5' => 'Pegawai Rawat Inap',
            '6' => 'Pegawai Apoteker',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetTipeBayarGaji($idtipe,$st_pendapatan)
{
   $tipe=0;
   if ($idtipe=='1' && $st_pendapatan=='0'){
	   $tipe='1';
   }
   if ($idtipe=='2' && $st_pendapatan=='0'){
	   $tipe='2';
   }
   if ($idtipe=='2' && $st_pendapatan=='1'){
	   $tipe='3';
   }
   return $tipe;
}
function GetTipePemilikSaham($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Pegawai',
            '2' => 'Dokter',
            '3' => 'Non Pegawai',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetKategoriDokter($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Umum',
            '2' => 'Spesialis Radiologi',
            '3' => 'Spesialis Patologi Anatomi',
            '4' => 'Spesialis Anesthesi',
            '5' => 'Spesialis Lainnya'
        );

        return $data[$id];
    } else {
        return '';
    }
}
function list_reservasi_cara(){
	$tree = [
			['id' => 1, 'nama' => 'Mobile Apps'],
			['id' => 2, 'nama' => 'Web Apps'],
			['id' => 3, 'nama' => 'FO Whatsaap'],
			['id' => 4, 'nama' => 'FO Telephone'],
			['id' => 5, 'nama' => 'FO Datang Langsung'],
			['id' => 6, 'nama' => 'APM']
		];
	return $tree;
}
function GetAsalRujukan($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Pasien Non Rujukan',
            '1' => 'Poliklinik',
            '2' => 'Instalasi Gawat Darurat',
            '3' => 'Rawat Inap',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetJenisKunjungan($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Pasien Baru',
            '2' => 'Kontrol Kasus Lama',
            '3' => 'Kontrol Kasus Baru',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetTipePasienValidasiRajal($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Poliklinik',
            '2' => 'Instalasi Gawat Darurat',
            '3' => 'Non Rujukan',
            '4' => 'Refund',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetAsalRujukanR($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Pasien Non Rujukan',
            '1' => 'Poliklinik',
            '2' => 'Instalasi Gawat Darurat (IGD)',
            '3' => 'Rawat Inap'
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetAsalRujukanRI($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Rawat Inap',
            '2' => 'ODS',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetAsalRujukanKwitansi($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Pasien Non Rujukan',
            '1' => 'Rawat Jalan',
            '2' => 'IGD',
            '3' => 'Rawat Inap'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetAsalPasienRujukan($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Pasien Non Rujukan',
            '1' => 'Poliklinik',
            '2' => 'Instalasi Gawat Darurat',
            '3' => 'Rawat Inap'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function JenisTransaksi($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'POLI',
            '2' => 'IGD',
            '3' => 'O. BEBAS'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusRekanan($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-success">Berekanan</span>',
            '2' => '<span class="label label-danger">Tidak Berekanan</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}


function GetTipeRujukanRawatInap($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-primary">Rawat Inap</span>',
            '2' => '<span class="label label-danger">ODS</span>'
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetTipeTarifTindakan($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Administrasi Rawat Inap',
            '2' => 'Ruang Kamar Operasi',
            '3' => 'Sewa Alat Kamar Operasi',
            '4' => 'Tindakan Rawat Inap Full Care',
            '5' => 'Tindakan Rawat Inap ECG',
            '6' => 'Tindakan Rawat Inap Ambulance',
            '7' => 'Tindakan Rawat Inap Sewa Alat',
            '8' => 'Tindakan Rawat Inap Lain-Lain',
            '9' => 'Tindakan Laboratorium Umum',
            '10' => 'Tindakan Laboratorium Phatologi Anatomi',
            '11' => 'Tindakan Laboratorium PMI',
            '12' => 'Tindakan Radiologi X-Ray',
            '13' => 'Tindakan Radiologi USG',
            '14' => 'Tindakan Radiologi CT-Scan',
            '15' => 'Tindakan Radiologi MRI',
            '16' => 'Tindakan Radiologi BMD',
            '17' => 'Tindakan Fisioterapi',
            '18' => 'Tindakan Rawat Inap Visite',
            '19' => 'Tindakan Operasi Dokter Operator',
            '20' => 'Tindakan Rawat Jalan',
            '21' => 'Administrasi Rawat Jalan',
            '22' => 'Tarif Ruangan',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function TipeTarifRanap($id)
{
    if ($id != '') {
        $data = array(
            '0' => text_default('SEMUA'),
            '1' => 'Full Care',
            '2' => 'ECG',
            '4' => 'Sewa Alat',
            '5' => 'Ambulance',
            '6' => 'Lain-Lain',
            
        );

        return $data[$id];
    } else {
        return '';
    }
}
function TipeDiskon($tipe,$angka)
{
    if ($tipe != '') {
        $data = array(
            '1' => number_format($angka).' %',
            '2' => 'Rp. '.number_format($angka),
            
        );

        return $data[$tipe];
    } else {
        return '';
    }
}
function opsi_iter($max_iter,$id,$nama,$nama_class){
	$opsi='<select tabindex="8"  width="100%" id="'.$nama.'" class="js-select2 form-control '.$nama_class.'" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
	for($i=0;$i<=$max_iter;$i++){
		$opsi .='<option value="'.$i.'" '.($i==$id?'selected':'').'>'.$i.'</option>';
	}
	$opsi .='</select>';
	return $opsi;
}
function opsi_iter_record($max_iter,$id,$nama,$nama_class){
	$opsi='<select tabindex="8"  width="100%" id="'.$nama.'" class="js-select2 form-control opsi_change_nr '.$nama_class.'" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
	for($i=0;$i<=$max_iter;$i++){
		$opsi .='<option value="'.$i.'" '.($i==$id?'selected':'').'>'.$i.'</option>';
	}
	$opsi .='</select>';
	return $opsi;
}
function opsi_iter_record_paket($max_iter,$id,$nama,$nama_class){
	$opsi='<select tabindex="8"  width="100%" id="'.$nama.'" class="js-select2 form-control opsi_change_paket '.$nama_class.'" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
	for($i=0;$i<=$max_iter;$i++){
		$opsi .='<option value="'.$i.'" '.($i==$id?'selected':'').'>'.$i.'</option>';
	}
	$opsi .='</select>';
	return $opsi;
}
function GetTipePasienPiutang($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-default">BELUM DITENTUKAN</span>',
            '1' => '<span class="label label-warning">Poliklinik</span>',
            '2' => '<span class="label label-danger">IGD</span>',
            '3' => '<span class="label label-success">Rawat Inap</span>',
            '4' => '<span class="label label-primary">ODS</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetAsalPasien($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-warning">Poliklinik</span>',
            '2' => '<span class="label label-danger">IGD</span>',
            '3' => '<span class="label label-success">Rawat Inap</span>',
            '4' => '<span class="label label-primary">ODS</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetTipeOrder($pendaftaran_id, $id,$program_rm_id,$jml)
{
    if ($id != '') {
        $data = array(
            '1' => '<a class="btn btn-xs btn-block btn-primary push-10" href="'.base_url().'tpoliklinik_rm_order/tindakan/'.$pendaftaran_id.'/1/erm_rm/input_order/'.$program_rm_id.'" target="_blank" type="button">ORDER</a><button class="btn btn-xs btn-block btn-primary push-10" type="button">'.$jml.' KUNJUNGAN</button>',
            '2' => '<a class="btn btn-xs btn-block btn-danger push-10" href="'.base_url().'tpoliklinik_rm_order/tindakan/'.$pendaftaran_id.'/1/erm_rm/input_perencanaan/'.$program_rm_id.'" target="_blank"  type="button">PERENCANAAN</a><button class="btn btn-xs btn-block btn-primary push-10" type="button">'.$jml.' KUNJUNGAN</button>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function Status_Reservasi_Bed($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-default">MENUNGGU DIPROSES</span>',
            '2' => '<span class="label label-success">SELESAI DIPROSES</span>',
            '0' => '<span class="label label-danger">DIBATALKAN</span>',
			);

        return $data[$id];
    } else {
        return '';
    }
}
function GetAsalPasienLabel($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Poliklinik',
            '2' => 'Instalasi Gawat Darurat (IGD)',
            '3' => 'Rawat Inap',
            '4' => 'One Day Surgery (ODS)',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetJenisDirect($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-primary">Direct</span>',
            '2' => '<span class="label label-danger">Non Direct</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetStatusPermintaan($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-primary">Memiliki Permintaan</span>',
            '2' => '<span class="label label-danger">Tidak Memiliki Permintaan</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function status_cover($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-warning"><i class="fa fa-exclamation-circle"></i> TIDAK DICOVER</span>',
            '2' => '<span class="label label-danger"><i class="fa fa-times-circle"></i> TIDAK DAPAT DIPILIH</span>',
            null => '',
            '0' => '',
            '' => '',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function status_stok($id,$stok_kuantitas=0)
{
	$label='';
	if ($id<1){
		$label='<br><br><span class="label label-danger"><i class="fa fa-exclamation-circle"></i> STOK OBAT KOSONG ('.$id.')</span>';
	}else{
		if ($stok_kuantitas > $id){
			
		$label='<br><br><span class="label label-warning"><i class="fa fa-exclamation-circle"></i> STOK OBAT KURANG ('.$id.')</span>';
		}
	}
    return $label;
}
function GetFormulir($id)
{
    if ($id != '') {
        $data = array(
            '1' => text_danger('TAMPIL (Wajib diisi)'),
            '2' => text_warning('TAMPIL (Tidak Wajib diisi)'),
            '3' => text_default('TIDAK TAMPIL'),
            '0' => '<span class="label label-primary">SEMUA</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function status_tindakan_farmasi($id)
{
    if ($id != '') {
        $data = array(
            '0' => text_default('MENUNGGU TELAAH'),
            '1' => text_info('TELAH DITELAAH'),
            '2' => text_warning('SELESAI TRANSAKSI'),
            '3' => text_primary('SIAP DISERAHKAN'),
            '4' => text_success('SUDAH DISERAHKAN'),
            '5' => text_danger('TIDAK DIAMBIL'),
            '6' => text_success('SUDAH DISERAHKAN'),
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetPrioritasCito($id)
{
    if ($id != '') {
        $data = array(
            '2' => text_danger('CITO'),
            '1' => '',
            '0' => '',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetPertemuan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-success">PASEIAN LAMA</span>',
            '1' => '<span class="label label-danger">PASIEN BARU</span>',
            '2' => '<span class="label label-default">ALL</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetTipePasienRanap($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-success">PASIEN LAMA</span>',
            '1' => '<span class="label label-danger">PASIEN RS</span>',
            '2' => '<span class="label label-default">PASIEN PRIBADI</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetTipeDistributor($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-primary">DISTRIBUTOR</span>',
            '3' => '<span class="label label-success">VENDOR</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetStatusBedah($statussetuju,$statusdatang)
{
	
	if ($statussetuju=='1') {
		$status_acc='<span class="label label-success">Terjadwal</span>';
	} else {
		$status_acc='<span class="label label-default">Menunggu Diproses</span>';
	}
	if ($statusdatang=='1') {
		$status_datang='<span class="label label-primary">Pasien Sudah Datang</span>';
	} else {
		$status_datang='<span class="label label-warning">Pasien Belum Datang</span>';
	}
	$status='<div class="h5 text-uppercase push-5-t">'.$status_acc.'<div><div class="h5 text-uppercase push-5-t">'.$status_datang.'</div>';
	return $status;
}
function GetTipePasienPiutangBiasa($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Poliklinik',
            '2' => 'IGD',
            '3' => 'Rawat Inap',
            '4' => 'ODS',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeAdministrasi($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Rawat Jalan',
            '2' => 'Rawat Inap'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetJenisAdministrasi($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Administrasi',
            '2' => 'Cetak Kartu'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetKelas($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'I',
            '2' => 'II',
            '3' => 'III',
            '4' => 'UTAMA',
            '5' => 'VIP',
            '0' => 'POLIKLINIK / IGD',
        );

        return $data[$id];
    } else {
        return '';
    }
}


function GetJenisKelamin($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Laki-laki',
            '2' => 'Perempuan',
            '3' => 'Tidak Mengisi',
            '4' => 'Tidak Mengisi',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetGolonganDarah($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-',
            '1' => 'A',
            '2' => 'B',
            '3' => 'AB',
            '4' => 'O'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetStatusKawin($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-',
            '1' => 'Belum Menikah',
            '2' => 'Sudah Menikah',
            '3' => 'Janda',
            '4' => 'Duda'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeKategoriBarang($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Alat Kesehatan',
            '2' => 'Implan',
            '3' => 'Obat',
            '4' => 'Logistik'
        );

        return $data[$id];
    } else {
        return '';
    }
}
function GetKategoriJualNonRujukan($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Penjualan Bebas',
            '1' => 'Pasien RS',
            '2' => 'Pegawai',
            '3' => 'Dokter'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeKategoriSatuan($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Umum',
            '2' => 'Laboratorium',
            '3' => 'Farmasi & Gudang',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeLaboratorium($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Laboratorium Umum',
            '2' => 'Patologi Anatomi',
            '3' => 'Bank Darah',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeLayananLaboratorium($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Patologi Klinik',
            '2' => 'Patologi Anatomi',
            '3' => 'Bank Darah',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeLayananRadiologi($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'X-Ray',
            '2' => 'USG',
            '3' => 'CT-Scan',
            '4' => 'MRI',
            '5' => 'Lain-lain',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function getNamaHari($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Minggu',
            '2' => 'Senin',
            '3' => 'Selasa',
            '4' => 'Rabu',
            '5' => 'Kamis',
            '6' => 'Jumat',
            '7' => 'Sabtu',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeRuangan($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Ruang Umum',
            '2' => 'Ruang Operasi',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeRak($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Rak Gudang',
            '2' => 'Rak Farmasi',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetTipeRadiologi($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'X-Ray',
            '2' => 'USG',
            '3' => 'CT Scan',
            '4' => 'MRI',
            '5' => 'BMD',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function getJenisRacikan($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Kapsul',
            '2' => 'Sirup',
            '3' => 'Salep',
            '99' => 'Lainnya'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetJenisPembayaranPengajuan($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Tunai',
            '2' => 'Kontrabon',
            '3' => 'Transfer',
            '4' => 'Termin By Progress',
            '5' => 'Termin Fix (Cicilan)'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetStatusTermin($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Dihapus</span>',
            '1' => '<span class="label label-primary">Belum diaktivasi</span>',
            '2' => '<span class="label label-success">Diaktivasi</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function get_agama($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-',
            '1' => 'Islam',
            '2' => 'Kristen',
            '3' => 'Katolik',
            '4' => 'Budha',
            '5' => 'Hindu',
            '6' => 'Kong Hu Cu',
            '7' => 'Lain-lain',
            '8' => 'Protestan',
            '9' => 'Kristen',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function get_golongan_darah($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'A',
            '2' => 'AB',
            '3' => 'Hindu',
            '4' => 'Budha',
            '5' => 'Konghucu',
            '6' => 'Lain-lain',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function text_group($text)
{

    return '<div class="push-5"><span class="label label-primary"><i class="glyphicon glyphicon-time"></i> '.$text.'</span></div>';
}
function text_primary($text)
{
    return '<span class="label label-primary">'.$text.'</span>';
}
function text_danger($text)
{
    return '<span class="label label-danger">'.$text.'</span>';
}
function text_warning($text)
{
    return '<span class="label label-warning">'.$text.'</span>';
}
function text_success($text)
{
    return '<span class="label label-success">'.$text.'</span>';
}
function text_info($text)
{
    return '<span class="label label-info">'.$text.'</span>';
}
function text_default($text)
{
    return '<span class="label label-default">'.$text.'</span>';
}

function GetStatusRencana($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-',
            '1' => '<span class="label label-primary">Rawat Inap</span>',
            '2' => '<span class="label label-danger">One Day Surgery (ODS)</span>'
        );

        return $data[$id];
    } else {
        return '-';
    }
}
function get_tipe_layanan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-',
            '1' => '<span class="label label-primary">Rawat Jalan</span>',
            '2' => '<span class="label label-success">Rawat Inap</span>'
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function GetTujuanBerkas($id)
{
    if ($id=='1') {
        return '<span class="label label-success">POLIKLINIK</span>';
    } elseif ($id=='2') {
        return '<span class="label label-danger">IGD</span>';
    } elseif ($id=='3') {
        return '<span class="label label-primary">RAWAT INAP</span>';
    } elseif ($id=='4') {
        return '<span class="label label-default">ODS</span>';
    } elseif ($id=='5') {
        return '<span class="label label-warning">PINJAM</span>';
    }
}

function GetStatusAlokasiDana($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Kasir Rawat Jalan',
            '2' => 'Kasir Rawat Inap',
            '3' => 'Bendahara',
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function GetJenisLayananBerkas($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Rawat Jalan',
            '2' => 'Instalasi Gawat Darurat (IGD)',
            '3' => 'Rawat Inap',
            '4' => 'One Day Surgery (ODS)',
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function GetStatusPenyerahanHonor($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Diberikan',
            '2' => 'Menunggu',
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function GetStatusPengajuan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger text-uppercase">Draft</span>',
            '1' => '<span class="label label-warning text-uppercase">Telah Diajukan</span>',
            '2' => '<span class="label label-success text-uppercase">Disetujui</span>',
            '3' => '<span class="label label-danger text-uppercase">Ditolak</span>',
            '4' => '<span class="label label-success text-uppercase">Aktivasi Bendahara</span>',
            '5' => '<span class="label label-success text-uppercase">Proses Belanja</span>',
            '6' => '<span class="label label-danger text-uppercase">Ditolak</span>'
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function GetStatusAssigment($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<i class="si si-ban fa-2x" data-toggle="tooltip" title="Belum Memberikan Ulasan"></i>',
            '1' => '<i class="si si-check fa-2x text-success" data-toggle="tooltip" title="Menyetujui"></i>',
            '2' => '<i class="si si-close fa-2x text-danger" data-toggle="tooltip" title="Menolak"></i>',
        );

        return $data[$id];
    } else {
        return '-';
    }
}

// EOF Function Get Label

function StatusTransaksiKasir($status)
{
    if ($status != '') {
        $data = array(
            '0' => '<span class="label label-danger text-uppercase" data-toggle="tooltip" title="Menunggu Transaksi">Menunggu Transaksi</span>',
            '1' => '<span class="label label-success text-uppercase" data-toggle="tooltip" title="Sudah Terverifikasi">Telah Transaksi</span>'
        );

        return $data[$status];
    } else {
        return '';
    }
}

function StatusVerifTransaksi($status)
{
    if ($status != '') {
        $data = array(
            '0' => '<span class="label label-danger text-uppercase" data-toggle="tooltip" title="Belum Terverifikasi">Belum Terverifikasi</span>',
            '1' => '<span class="label label-success text-uppercase" data-toggle="tooltip" title="Sudah Terverifikasi">Sudah Terverifikasi</span>',
            '2' => '<span class="label label-warning text-uppercase" data-toggle="tooltip" title="Unverifikasi">Unverifikasi</span>'
        );

        return $data[$status];
    } else {
        return '';
    }
}

function StatusOperasiTransaksi($status)
{
    if ($status != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="Non Operasi">Non Operasi</span>',
            '1' => '<span class="label label-primary" data-toggle="tooltip" title="Operasi">Operasi</span>',
        );

        return $data[$status];
    } else {
        return '';
    }
}

function StatusIndexRujukan($id, $label)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger" data-toggle="tooltip" title="Tidak Dirujuk">' . $label . '</span>',
            '1' => '<span class="label label-success" data-toggle="tooltip" title="Dirujuk">' . $label . '</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function StatusIndexRujukan2($id, $label, $status)
{
    $hasil='';
    if ($id=='0') {
        $hasil='<span class="label label-default" data-toggle="tooltip" title="TIDAK DIRUJUK">' . $label . '</span>';
    } else {
        if ($status=='0') {
            $hasil='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">' . $label . '</span>';
        }
        if ($status=='1') {
            $hasil='<span class="label label-warning" data-toggle="tooltip" title="MENUNGGU DITINDAK">' . $label . '</span>';
        }
        if ($status=='2' || $status=='3') {
            $hasil='<span class="label label-success" data-toggle="tooltip" title="SUDAH DITINDAK">' . $label . '</span>';
        }
    }


    return $hasil;
}

function StatusRujukan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<div style="margin-top:7px"><span class="label label-danger" style="font-size:9pt; padding:6px;">Dibatalkan</span></div>',
            '1' => '<div style="margin-top:7px"><span class="label label-default" style="font-size:9pt; padding:6px;">Menunggu Tindakan</span></div>',
            '2' => '<div style="margin-top:7px"><span class="label label-primary" style="font-size:9pt; padding:6px;">Menunggu Hasil</span></div>',
            '3' => '<div style="margin-top:7px"><span class="label label-success" style="font-size:9pt; padding:6px;">Selesai</span></div>'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusRujukanKasir($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-default">Tdk. Rujuk</span>',
            '1' => '<span class="label label-info">Dirujuk</span>',
            '2' => '<span class="label label-primary">Batal</span>',
            '3' => '<span class="label label-success">Selesai</span>'
        );

        return $data[$id];
    } else {
        return '';
    }
}
function StatusKelompokNR($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Penjualan Bebas',
            '1' => 'Pasien RS',
            '2' => 'Pegawai',
            '3' => 'Dokter'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusKasir($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Dibatalkan</span>',
            '1' => '<span class="label label-default">Menunggu Diproses</span>',
            '2' => '<span class="label label-success">Telah Diproses</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusPembelianFarmasi($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Dibatalkan</span>',
            '1' => '<span class="label label-default">Menunggu Tindakan</span>',
            '2' => '<span class="label label-success">Sudah Ditindak</span>',
            '3' => '<span class="label label-primary">Sudah Diserahkan</span>'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusRujukanRadiologi($status, $statuspasien, $style = true)
{
    if ($status == 0) {
        if ($style) {
            return '<span class="label label-danger">Dibatalkan</span>';
        } else {
            return 'Dibatalkan';
        }
    } elseif ($status == 1) {
        if ($style) {
            return '<span class="label label-default">Menunggu Tindakan</span>';
        } else {
            return 'Menunggu Tindakan';
        }
    }

    if ($status == 2 && $statuspasien == 0) {
        if ($style) {
            return '<span class="label label-primary">Telah Ditindak</span>';
        } else {
            return 'Telah Ditindak';
        }
    } elseif ($status == 2 && $statuspasien == 1) {
        if ($style) {
            return '<span class="label label-success">Langsung</span>';
        } else {
            return 'Langsung';
        }
    } elseif ($status == 2 && $statuspasien == 2) {
        if ($style) {
            return '<span class="label label-danger">Menolak</span>';
        } else {
            return 'Menolak';
        }
    } elseif ($status == 2 && $statuspasien == 3) {
        if ($style) {
            return '<span class="label label-info">Kembali</span>';
        } else {
            return 'Kembali';
        }
    } else {
        return '';
    }
}

function StatusTransaction($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Rejected</span>',
            '1' => '<span class="label label-success">Approved</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusPemesananGudang($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Dibatalkan</span>',
            '1' => '<span class="label label-default">Pending</span>',
            '2' => '<span class="label label-info">Sudah Dipesan</span>',
            '3' => '<span class="label label-warning">Penerimaan</span>',
            '4' => '<span class="label label-success">Selesai</span>'
        );
        return $data[$id];
    } else {
        return '';
    }
}
function status_jawab_perubahan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-default">BELUM DIJAWAB</span>',
            '1' => '<span class="label label-success">TELAH DISETUJUI</span>',
            '2' => '<span class="label label-warning">DISETUJUI SEBAGIAN</span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function StatusPenerimaanGudang($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-danger">Belum Selesai Penerimaan</span>',
            '2' => '<span class="label label-success">Sudah Selesai</span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function StatusPengajuanInformasi($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Belum Selesai</span>',
            '1' => '<span class="label label-success">Sudah Selesai</span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function StatusKasbon($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Belum Dibayar</span>',
            '1' => '<span class="label label-default">Sudah Dibayar</span>'
        );
        return $data[$id];
    } else {
        return '';
    }
}

function StatusDeleteDeposit($status, $statusverifikasi)
{
    if ($status == 1 && $statusverifikasi == '') {
        $notif = '<span class="label label-primary">Tidak Dihapus</span>';
    } elseif ($status == 0 && $statusverifikasi == '') {
        $notif = '<span class="label label-danger">Dihapus</span>';
    } elseif ($statusverifikasi == 1) {
        $notif = '<span class="label label-success">Verifikasi Sesuai</span>';
    } elseif ($statusverifikasi == 0) {
        $notif = '<span class="label label-danger">Verifikasi Tidak Sesuai</span>';
    }

    return $notif;
}

function warna_stok($stok_asli, $stok_minimum)
{
    if ($stok_asli > $stok_minimum) {
        return $stok_asli;
    } else {
        return '<span class="label label-danger"> ' . $stok_asli . '</span>';
    }
}

function StatusTindakan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span data-toggle="tooltip" title="Belum ditindak" class="label label-warning"><i class="fa fa-exclamation-circle"></i> Belum ditindak</span>',
            '1' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Sudah ditindak</span>',
            '2' => '<span data-toggle="tooltip" title="Tindakan dibatalkan" class="label label-danger"><i class="fa fa-info-circle"></i> Tindakan dibatalkan</span>'
        );

        return $data[$id];
    } else {
        $data = array(
            '' => '<span data-toggle="tooltip" title="Belum ditindak" class="label label-warning"><i class="fa fa-exclamation-circle"></i> Belum ditindak</span>'
        );
        return $data[$id];
    }
}
function StatusPanggil($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span data-toggle="tooltip" title="Belum ditindak" class="label label-default"><i class="fa fa-exclamation-circle"></i> Menunggu</span>',
            '1' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Sudah Dilayani</span>',
            '2' => '<span data-toggle="tooltip" title="Tindakan dibatalkan" class="label label-warning"><i class="fa fa-info-circle"></i> Dilewati</span>'
        );

        return $data[$id];
    } else {
        $data = array(
            '' => '<span data-toggle="tooltip" title="Belum ditindak" class="label label-warning"><i class="fa fa-exclamation-circle"></i> Belum ditindak</span>'
        );
        return $data[$id];
    }
}
function StatusPanggilFarmasi($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span data-toggle="tooltip" title="Belum ditindak" class="label label-default"><i class="fa fa-exclamation-circle"></i> MENUNGGU PANGGILAN</span>',
            '1' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> SUDAH DIPANGGIL</span>',
            '2' => '<span data-toggle="tooltip" title="Tindakan dibatalkan" class="label label-warning"><i class="fa fa-info-circle"></i> DILEWATI</span>'
        );

        return $data[$id];
    } else {
        $data = array(
            '' => '<span data-toggle="tooltip" title="Belum ditindak" class="label label-warning"><i class="fa fa-exclamation-circle"></i> Belum ditindak</span>'
        );
        return $data[$id];
    }
}
function StatusRawatInap($id)
{
    if ($id != '') {
        $data = array(
            '11' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Dirawat</span>',
            '12' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Pembayaran</span>',
            '13' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Pulang</span>',
            '21' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-info"><i class="fa fa-check"></i> Menunggu Tindakan</span>',
            '22' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Sudah Ditindak</span>',
            '23' => '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-danger"><i class="fa fa-check"></i> Dibatalkan</span>'
        );

        return $data[$id];
    } else {
        $data = array(
            '' => '<span data-toggle="tooltip" title="Belum ditindak" class="label label-warning"><i class="fa fa-exclamation-circle"></i> Belum ditindak</span>'
        );
        return $data[$id];
    }
}

function StatusPoP($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Pasien Non-RS',
            '1' => 'Pasien RS'
        );
        return $data[$id];
    } else {
        return '';
    }
}

function StatusOK($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Dibatalkan</span>',
            '1' => '<span class="label label-default">Pasien Belum Datang</span>',
            '2' => '<span class="label label-primary">Pasien Sudah Datang</span>',
            '3' => '<span class="label label-success">Siap Di Operasi</span>',
            '4' => '<span class="label label-info">Sudah Di Operasi</span>'
        );

        return $data[$id];
    } else {
        return '';
    }
}
function StatusBarang($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Tidak Aktif</span>',
            '1' => '<span class="label label-success">Aktif</span>',
            '2' => '<span class="label label-primary">Pasien Sudah Datang</span>',
            '3' => '<span class="label label-success">Siap Di Operasi</span>',
            '4' => '<span class="label label-info">Sudah Di Operasi</span>'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusJatuhTempo($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Tidak Aktif</span>',
            '1' => '<span class="label label-success">Aktif</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusRow($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Tidak Aktif</span>',
            '1' => '<span class="label label-success">Aktif</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusTarifGroupTest($id)
{
    if ('' !== $id) {
        $data = [
            '0' => '<span class="label label-black">Tidak Dapat Dipilih</span>',
            '1' => '<span class="label label-danger">Telah Terpilih</span>',
            '2' => '<span class="label label-primary">Belum Terpilih</span>',
        ];

        return $data[$id];
    }

    return '';
}

function StatusPembayaranKlinik($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Belum Dibayarkan</span>',
            '1' => '<span class="label label-primary">Telah Dibayarkan</span>'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusPembayaranFeeRujukan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">On Proses</span>',
            '1' => '<span class="label label-primary">Selesai Perhitungan</span>',
            '2' => '<span class="label label-primary">Approval</span>',
            '3' => '<span class="label label-primary">Disetujui / tolak</span>',
            '4' => '<span class="label label-primary">Pembayaran</span>',
            '5' => '<span class="label label-primary">Telah DIbayarkan</span>'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function JenisBerekanan($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Persentase',
            '2' => 'Flat Rate',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusHonorDokter($idkelompokpasien, $namaasuransi, $statusasuransi)
{
    if ($idkelompokpasien != 0) {
        if ($statusasuransi) {
            return '<span class="label label-danger">' . $namaasuransi . '</span>';
        } else {
            return '<span class="label label-success">' . $namaasuransi . '</span>';
        }
    } else {
        return '<span class="label label-default">Non Asuransi</span>';
    }
}

function StatusTindakanDPJP($id)
{
    if ($id != '') {
        $data = array(
            '0' => 'Umum (Rawat Jalan)',
            '1' => 'Jasa Dokter Bedah (Operator)',
            '2' => 'Jasa Dokter Visite',
            '3' => 'Jasa Tindakan'
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusTransaksiPencairan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Sedang Diproses</span>',
            '1' => '<span class="label label-success">Telah Diproses</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusPencairan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-primary">Belum Diproses</span>',
            '1' => '<span class="label label-danger">Telah Dicairakan</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusERMAntrian($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-warning">Menunggu Dipanggil</span>',
            '1' => '<span class="label label-primary">Sedang Dilayani</span>',
            '2' => '<span class="label label-danger">Dilewati</span>',
            '3' => '<span class="label label-success">Telah Dilayani</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusERMTindakan($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-warning" style="width: 100%; display: block;">Draft</span>',
            '2' => '<span class="label label-default" style="width: 100%; display: block;">Menunggu Pemeriksaan</span>',
            '3' => '<span class="label label-primary" style="width: 100%; display: block;">Proses Sampling</span>',
            '4' => '<span class="label label-primary" style="width: 100%; display: block;">Menunggu Hasil</span>',
            '5' => '<span class="label label-primary" style="width: 100%; display: block;">Menunggu Validasi</span>',
            '6' => '<span class="label label-success" style="width: 100%; display: block;">Selesai Validasi</span>',
            '7' => '<span class="label label-success" style="width: 100%; display: block;">Telah Dikirim</span>',
            '0' => '<span class="label label-danger" style="width: 100%; display: block;">Dibatalkan</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusERMTindakanRadiologi($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-warning" style="width: 100%; display: block;">Draft</span>',
            '2' => '<span class="label label-default" style="width: 100%; display: block;">Menunggu Pemeriksaan</span>',
            '3' => '<span class="label label-primary" style="width: 100%; display: block;">Proses Sampling</span>',
            '4' => '<span class="label label-primary" style="width: 100%; display: block;">Menunggu Upload</span>',
            '5' => '<span class="label label-primary" style="width: 100%; display: block;">Telah Upload</span>',
            '6' => '<span class="label label-success" style="width: 100%; display: block;">Selesai Expertise</span>',
            '7' => '<span class="label label-success" style="width: 100%; display: block;">Telah Dikirim</span>',
            '0' => '<span class="label label-danger" style="width: 100%; display: block;">Dibatalkan</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusExpertiseRadiologi($id, $size = '')
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-warning" style="width: 100%; display: block; ' . ($size ? "font-size: $size;" : "") . '">-</span>',
            '1' => '<span class="label label-success" style="width: 100%; display: block; ' . ($size ? "font-size: $size;" : "") . '">Langsung</span>',
            '2' => '<span class="label label-danger" style="width: 100%; display: block; ' . ($size ? "font-size: $size;" : "") . '">Menolak</span>',
            '3' => '<span class="label label-info" style="width: 100%; display: block; ' . ($size ? "font-size: $size;" : "") . '">Kembali</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusPemeriksaanRadiologi($id, $size = '')
{
    if ($id >= 6) {
        return '<span class="label label-success" style="width: 100%; display: block; ' . ($size ? "font-size: $size;" : "") . '">Telah Expertise</span>';
    } else {
        return '<span class="label label-danger" style="width: 100%; display: block; ' . ($size ? "font-size: $size;" : "") . '">Belum Expertise</span>';
    }
}

function StatusUploadFotoRadiologi($id)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-success" style="width: 100%; display: block;">Telah Diupload</span>',
            '0' => '<span class="label label-default" style="width: 100%; display: block;">Belum Diupload</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusPengaturanPenginputanExpertise($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Tampil Wajib',
            '2' => 'Tampil Saja',
            '3' => 'Tidak Tampil',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function checkvalue($value)
{
    if ($value == null) {
        return '-';
    } else {
        return $value;
    }
}

function defaultKelasOds($value)
{
    if ($value == null) {
        return 'III';
    } else {
        if ($value == 1) {
            return 'I';
        } elseif ($value == 2) {
            return 'II';
        } elseif ($value == 3) {
            return 'III';
        } elseif ($value == 4) {
            return 'UTAMA';
        }
    }
}

// function statusEstimasi($id)
// {
//     if ($id != '') {
//         $data = array(
//           '1' => '-',
//           '2' => '<span class="label label-success">Estimasi Di Acc</span>',
//         );

//         return $data[$id];
//     } else {
//         return '';
//     }
// }

function btnPrint($d)
{
    $conn = '<div class="btn-group cols-sm-3" data-toggle="buttons">
            <div class="btn-group" role="group">
                        <button class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><i class="fa fa-print"></i>
                            Print <span class="caret"></span>
                        </button>&nbsp;
                        <ul style="font-size:12px" class="dropdown-menu fade in">
                            <li>
                            <a tabindex="-1" class="link-effect kw-besar" data-id="' . $d . '" href="javascript:void(0)"><i class="fa fa-print"></i> Kw. Besar</a>
                           </li>
                           <li class="divider"></li>
                           <li>
                            <a tabindex="-1" class="link-effect kw-kecil" data-id="' . $d . '" href="javascript:void(0)"><i class="fa fa-print"></i> Kw. Kecil</a>
                           </li>
                       </ul>
                   </div>
            </div>';
    return $conn;
}

function getStatusEstimasiPengajuanInformasi($xtanggalPengajuan, $xtanggalEstimasiSelesai)
{
    $tanggalPengajuan  = new DateTime($xtanggalPengajuan);
    $tanggalEstimasiSelesai = new DateTime($xtanggalEstimasiSelesai);

    $diff = $tanggalEstimasiSelesai->diff($tanggalPengajuan);
    if ($diff->d >= 5 && $diff->d < 10) {
        return '<span class="label label-warning">' . $diff->d . ' Hari Lagi</span>';
    } else if ($diff->d < 5) {
        return '<span class="label label-danger">' . $diff->d . ' Hari Lagi</span>';
    } else {
        return '<span class="label label-success">' . $diff->d . ' Hari Lagi</span>';
    }
}

function getUmur($tgl)
{
    $birthDt = new DateTime($tgl);
    $today = new DateTime('today');
    $y = $today->diff($birthDt)->y;
    $m = $today->diff($birthDt)->m;
    $d = $today->diff($birthDt)->d;
    return $y . " tahun " . $m . " bulan " . $d . " hari";
}
function getUmurArray($tgl)
{
    $birthDt = new DateTime($tgl);
    $today = new DateTime('today');
    $y = $today->diff($birthDt)->y;
    $m = $today->diff($birthDt)->m;
    $d = $today->diff($birthDt)->d;
	$data=array(
		'tahun' => $y,
		'bulan' => $m,
		'hari' => $d,
	);
    return $data;
}
function getDurasi($tgl1, $tgl2)
{
    $awal  = new DateTime($tgl1); //Waktu awal

    $akhir =new DateTime($tgl2); // Waktu sekarang atau akhir

    $diff  = $awal->diff($akhir);

    $y=($diff->y)? $diff->y.' th ':'';
    $m=($diff->m)? $diff->m.' bln ':'';
    $d=($diff->d)? $diff->d.' hr ':'';
    $h=($diff->h)? $diff->h.' jam ':'';
    $i=($diff->i)? $diff->i.' menit ':'';
    $s=($diff->s)? $diff->s.' detik ':'';
    return $y.$m.$d.$h.$i.($i?'':$s);
}
function getLamaDirawat($tgl1, $tgl2)
{
    $awal  = strtotime($tgl1); //Waktu awal

    $akhir =strtotime($tgl2); // Waktu sekarang atau akhir
	
    $jarak = $akhir - $awal;

	$hari = $jarak / 60 / 60 / 24;
	// print_r($hari);exit;
    $d=$hari.' Hari';
    return $d;
}
function selisih_tanggal($tgl1, $tgl2)
{
    $awal  = strtotime($tgl1); //Waktu awal

    $akhir =strtotime($tgl2); // Waktu sekarang atau akhir
	
    $jarak = $akhir - $awal;

	$hari = $jarak / 60 / 60 / 24;
	// print_r($hari);exit;
    $d=$hari+1;
    return $d;
}
function getUmur2($tgl, $format)
{
    $birthDt = new DateTime($tgl);
    $today = new DateTime('today');
    $y = $today->diff($birthDt)->y;
    $m = $today->diff($birthDt)->m;
    $d = $today->diff($birthDt)->d;
    if ($format == 'y') {
        return $y;
    }
    if ($format == 'm') {
        return $m;
    }
    if ($format == 'd') {
        return $d;
    }
    // return $y." tahun ".$m." bulan ".$d." hari";
}

function sql_connect()
{
    $conn = array(
        'user' => 'rskbhalmahera',
        'pass' => 'kurvasoft@2017',
        'db' => 'rskbhalm_simrs',
        'host' => 'rskbhalmahera.ga');
    return $conn;
}

function encriptURL($url)
{
    $CI =& get_instance();
    $base64 = $CI->encrypt->encode($url);
    $urisafe = strtr($base64, '+/', '-_');
    return $urisafe;
}

function decriptURL($url)
{
    $CI =& get_instance();
    $urisafe = strtr($url, '-_', '+/');
    $base64 = $CI->encrypt->decode($urisafe);
    return $base64;
}

function metodePembayaran($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Tunai',
            '2' => 'Debit',
            '3' => 'Kartu Kredit',
            '4' => 'Transfer',
            '5' => 'Tagihan Karyawan (Pegawai)',
            '6' => 'Tagihan Karyawan (Dokter)',
            '7' => 'Tidak Tertagihkan',
            '8' => 'Kontraktor'
        );

        return $data[$id];
    } else {
        return '-';
    }
}
function metodePembayaran_bendahara($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'CHEQ',
            '2' => 'TUNAI',
            '3' => 'TRANSFER BEDA BANK',
            '4' => 'TRANSFER SAMA BANK',
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function metodePembayaranKasir($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Tunai',
            '2' => 'Debit',
            '3' => 'Kartu Kredit',
            '4' => 'Transfer',
            '5' => 'Tagihan Karyawan (Pegawai)',
            '6' => 'Tagihan Karyawan (Dokter)',
            '7' => 'Tidak Tertagihkan',
            '8' => 'Kontraktor'
        );

        return $data[$id];
    } else {
        return '-';
    }
}
function metodePembayaranKasirRajal($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Tunai',
            '2' => 'Debit',
            '3' => 'Kartu Kredit',
            '4' => 'Transfer',
            '5' => 'Tagihan Karyawan / Dokter',
            '6' => 'Tidak Tertagihkan',
            '7' => 'Kontraktor'
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function tipeRujukanRS($id)
{
    if ($id != '') {
        $data = array(
            '1' => 'Rumah Sakit',
            '2' => 'Klinik'
        );

        return $data[$id];
    } else {
        return '-';
    }
}

function jenis_pengembalian($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-',
            '1' => 'Retur',
            '2' => 'Pemusnahan',
        );

        return $data[$id];
    } else {
        return "-";
    }
}

function jenis_retur($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-',
            '1' => 'Ganti Uang',
            '2' => 'Ganti Barang',
            '3' => 'Ganti Barang Lain',
        );

        return $data[$id];
    } else {
        return "-";
    }
}

function cara_minum($id)
{
    if ($id != '') {
        $data = array(
            'SBM' => 'Sebelum Makan',
            'SSM' => 'Sesudah Makan',
            'BM' => 'Bersama Makan',
            '-' => '-',
        );

        return $data[$id];
    } else {
        return "-";
    }
}
function cara_bayar($tipe)
{
    if ($tipe=='1') {
        $tipe='<span class="label label-warning">CHEQ</span>';
    } elseif ($tipe=='2') {
        $tipe='<span class="label label-default">TUNAI</span>';
    } elseif ($tipe=='4') {
        $tipe='<span class="label label-primary">TRANSFER SAMA BANK</span>';
    } elseif ($tipe=='3') {
        $tipe='<span class="label label-success">TRANSFER BEDA BANK</span>';
    } else {
        $tipe='<span class="label label-danger">HARUS DI HAPUS</span>';
    }

    return $tipe;
}
function cara_bayar2($tipe)
{
    if ($tipe=='1') {
        $tipe='<span class="label label-primary">CHEQ</span>';
    } elseif ($tipe=='2') {
        $tipe='<span class="label label-default">TUNAI</span>';
    } elseif ($tipe=='4') {
        $tipe='<span class="label label-primary">TRANSFER</span>';
    } elseif ($tipe=='3') {
        $tipe='<span class="label label-success">TRANSFER BEDA BANK</span>';
    } else {
        $tipe='<span class="label label-danger">HARUS DI HAPUS</span>';
    }

    return $tipe;
}
function status_persetujuan($tipe)
{
    if ($tipe=='0') {
        $tipe='<span class="label label-warning"> Belum Menyetujui</span>';
    } elseif ($tipe=='1') {
        $tipe='<span class="label label-success"> Telah Menyetujui</span>';
    } elseif ($tipe=='2') {
        $tipe='<span class="label label-danger"> Tolak</span>';
    } else {
        $tipe='<span class="label label-danger"> HARUS DI HAPUS</span>';
    }

    return $tipe;
}
function status_bayar($tipe)
{
    if ($tipe=='0') {
        $tipe='<span class="label label-warning">UNPAID</span>';
    } elseif ($tipe=='1') {
        $tipe='<span class="label label-success">PAID</span>';
    }

    return $tipe;
}
function format_kenaikan($nilai)
{
	$hasil='';
    if ($nilai>0) {
        $hasil='<span class="label label-success">+ '.number_format($nilai,2).'</span>';
    } elseif ($nilai<0) {
        $hasil='<span class="label label-danger">'.number_format($nilai,2).'</span>';
    }else{
		$hasil=0;
	}

    return $hasil;
}
function format_kenaikan_persen($nilai)
{
	$hasil='';
    if ($nilai>0) {
        $hasil='<span class="label label-primary">+ '.number_format($nilai,2).'%</span>';
    } elseif ($nilai<0) {
        $hasil='<span class="label label-warning">'.number_format($nilai,2).'%</span>';
    }else{
		$hasil='0%';
	}

    return $hasil;
}
function BerkasTipeKunjungan($tujuan)
{
    if ($tujuan == '1') {
        $tipeKunjungan = 'poliklinik';
    } elseif ($tujuan == '2') {
        $tipeKunjungan = 'igd';
    } elseif ($tujuan == '3') {
        $tipeKunjungan = 'rawat_inap';
    } elseif ($tujuan == '4') {
        $tipeKunjungan = 'ods';
    }

    return $tipeKunjungan;
}
function waktu_minum($id)
{
    $waktu_minum='';
    if ($id) {
        $arr=json_decode($id);
        foreach ($arr as list($a, $b, $c)) {
            if ($waktu_minum !='') {
                $waktu_minum=$waktu_minum.','.$b.' '.$c;
            } else {
                $waktu_minum=$b.' '.$c;
            }
        }
        if ($waktu_minum !='') {
            $waktu_minum=' ('.$waktu_minum.')';
        }
    }
    return $waktu_minum;
}
function tipe_pemesanan_kbo($tipe='1', $jenis_retur)
{
    if ($tipe=='1') {
        if ($jenis_retur=='0') {
            $tipe='<span class="label label-primary">PEMESANAN</span>';
        } elseif ($jenis_retur=='1') {
            $tipe='<span class="label label-danger">RETUR UANG</span>';
        } elseif ($jenis_retur=='2') {
            $tipe='<span class="label label-default">RETUR GANTI BARANG</span>';
        } elseif ($jenis_retur=='3') {
            $tipe='<span class="label label-success">RETUR BEDA</span>';
        }
    } else {
        if ($jenis_retur=='1') {
            $tipe='<span class="label label-danger">RETUR UANG</span>';
        } elseif ($jenis_retur=='2') {
            $tipe='<span class="label label-default">RETUR GANTI BARANG</span>';
        } elseif ($jenis_retur=='3') {
            $tipe='<span class="label label-success">RETUR BEDA</span>';
        }
    }

    return $tipe;
}
function jenis_reservasi($tipe='1')
{
    
	if ($tipe=='0') {
		$tipe='<span class="label label-default">PENDAFTARAN LANGSUNG</span>';
	}elseif ($tipe=='1') {
		$tipe='<span class="label label-info">ONLINE</span>';
	} elseif ($tipe=='2') {
		$tipe='<span class="label label-warning">WEB</span>';
	} elseif ($tipe=='3') {
		$tipe='<span class="label label-success">BY WHATSAPP</span>';
	} elseif ($tipe=='4') {
		$tipe='<span class="label label-primary">BY PHONE</span>';
	} elseif ($tipe=='5') {
		$tipe='<span class="label label-danger">DATANG LANGSUNG</span>';
	} elseif ($tipe=='6') {
		$tipe='<span class="label label-default">APM</span>';
	}
    

    return $tipe;
}
function label_skrining($st_gc,$st_general,$st_sp,$st_skrining,$st_sc,$st_covid){
	$label='';
	if ($st_gc=='1'){
		if ($st_general=='1'){
			$label.=' <span class="label label-primary"><i class="fa fa-check"></i> GC</span>';
		}else{
			$label.=' <span class="label label-danger"><i class="fa fa-times"></i> GC</span>';
		}
	}
	if ($st_sp=='1'){
		if ($st_skrining=='1'){
			$label.=' <span class="label label-primary"><i class="fa fa-check"></i> SP</span>';
		}else{
			$label.=' <span class="label label-danger"><i class="fa fa-times"></i> SP</span>';
		}
	}
	if ($st_sc=='1'){
		if ($st_covid=='1'){
			$label.=' <span class="label label-primary"><i class="fa fa-check"></i> SC</span>';
		}else{
			$label.=' <span class="label label-danger"><i class="fa fa-times"></i> SC</span>';
		}
	}
	if ($label==''){
		$label='<span class="label label-default"><i class="si si-ban"></i> TIDAK ADA SKRINING</span>';
	}
	return $label;
}
function label_skrining_ranap($st_lm,$st_lembar,$st_sp,$st_pernyataan,$st_gc,$st_general,$st_hk,$st_hak){
	$label='';
	if ($st_lm=='1'){
		if ($st_lembar=='1'){
			$label.=' <span class="label label-primary"><i class="fa fa-check"></i> Lembar Masuk</span>';
		}else{
			$label.=' <span class="label label-danger"><i class="fa fa-times"></i> Lembar Masuk</span>';
		}
	}
	if ($st_sp=='1'){
		if ($st_pernyataan=='1'){
			$label.=' <span class="label label-primary"><i class="fa fa-check"></i> Surat Pernyataan</span>';
		}else{
			$label.=' <span class="label label-danger"><i class="fa fa-times"></i> Surat Pernyataan</span>';
		}
	}
	if ($st_gc=='1'){
		if ($st_general=='1'){
			$label.=' <span class="label label-primary"><i class="fa fa-check"></i> GC</span>';
		}else{
			$label.=' <span class="label label-danger"><i class="fa fa-times"></i> GC</span>';
		}
	}
	if ($st_hk=='1'){
		if ($st_hak=='1'){
			$label.=' <span class="label label-primary"><i class="fa fa-check"></i> Hak & Kewajiban</span>';
		}else{
			$label.=' <span class="label label-danger"><i class="fa fa-times"></i> Hak & Kewajiban</span>';
		}
	}
	
	
	if ($label==''){
		$label='<span class="label label-default"><i class="si si-ban"></i> TIDAK ADA SKRINING</span>';
	}
	return $label;
}
function status_reservasi($tipe='1')
{
    
	if ($tipe=='1') {
		$tipe='<span class="label label-default">MENUNGGU DIPROSES</span>';
	} elseif ($tipe=='2') {
		$tipe='<span class="label label-primary">TELAH DIPROSES</span>';
	} elseif ($tipe=='3') {
		$tipe='<span class="label label-success">TELAH HADIR</span>';
	} elseif ($tipe=='4') {
		$tipe='<span class="label label-warning">SEDANG DI PERIKSA</span>';
	} elseif ($tipe=='5') {
		$tipe='<span class="label label-primary">SELESAI</span>';
	} elseif ($tipe=='0') {
		$tipe='<span class="label label-danger">DIBATALKAN</span>';
	
	}
    

    return $tipe;
}
function status_penerimaan($jenis_retur)
{
    if ($jenis_retur=='1') {
        $tipe='<span class="label label-success">DIBAYAR DISTRIBUTOR</span>';
    } elseif ($jenis_retur=='2') {
        $tipe='<span class="label label-danger">RUMAH SAKIT BAYAR</span>';
    } elseif ($jenis_retur=='0') {
        $tipe='<span class="label label-default">TIDAK ADA PEMBAYARAN</span>';
    }

    return $tipe;
}
function jenis_isi($jenis_isi)
{
    if ($jenis_isi=='1') {
        $tipe='Option';
    } elseif ($jenis_isi=='2') {
        $tipe='Free Text';
    } elseif ($jenis_isi=='3') {
        $tipe='Tandatangan';
	} elseif ($jenis_isi=='0') {
        $tipe='Label';
    }

    return $tipe;
}
function asal_transaksi_retur($jenis_retur)
{
    if ($jenis_retur=='1') {
        $tipe='<span class="label label-default">TUNAI</span>';
    } elseif ($jenis_retur=='2') {
        $tipe='<span class="label label-info">KBO</span>';
    } elseif ($jenis_retur=='0') {
        $tipe='<span class="label label-default">-</span>';
    }

    return $tipe;
}
function jenis_racikan($id)
{
    if ($id != '') {
        $data = array(
            '0' => '-- Pilih Jenis --',
            '1' => 'Kapsul',
            '2' => 'Sirup',
            '3' => 'Salep',
            '99' => 'Lainnya',
        );

        return $data[$id];
    } else {
        return "-";
    }
}

function kekata($x)
{
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } elseif ($x < 20) {
        $temp = kekata($x - 10) . " belas";
    } elseif ($x < 100) {
        $temp = kekata($x / 10) . " puluh" . kekata($x % 10);
    } elseif ($x < 200) {
        $temp = " seratus" . kekata($x - 100);
    } elseif ($x < 1000) {
        $temp = kekata($x / 100) . " ratus" . kekata($x % 100);
    } elseif ($x < 2000) {
        $temp = " seribu" . kekata($x - 1000);
    } elseif ($x < 1000000) {
        $temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
    } elseif ($x < 1000000000) {
        $temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
    } elseif ($x < 1000000000000) {
        $temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
    } elseif ($x < 1000000000000000) {
        $temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
    }
    return $temp;
}

function terbilang($x, $style = 4)
{
    if ($x < 0) {
        $hasil = "minus " . trim(kekata($x));
    } else {
        $hasil = trim(kekata($x));
    }
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }
    return $hasil;
}
function terbilang_sound($x){
	$hasil = trim(angka_to_sound($x));
	if (substr($hasil,0,1)==':'){
		$hasil=substr($hasil,1,strlen($hasil));
	}
	return $hasil;
}
function angka_to_sound($x)
{
	$angka_kosong=$x;
    $x = abs($x);
    $angka = array("kosong.mp3", "1.mp3", "2.mp3", "3.mp3", "4.mp3", "5.mp3",
        "6.mp3", "7.mp3", "8.mp3", "9.mp3", "10.mp3", "11.mp3");
    $temp = "";
	$st_ketemu=false;
	for($i=0;$i<strlen($angka_kosong);$i++){
		$z=substr($angka_kosong,$i,1);
		if ($st_ketemu==false){
			if ($z=='0'){
				$temp .= ":".$angka[$z];
			// print_r($z);
			}else{
				$st_ketemu=true;
				
			}
		}
	}
	// print_r($temp);exit;
    if ($x < 12) {
        $temp .= ":" . $angka[$x];
    } elseif ($x < 20) {
        $temp .= angka_to_sound($x - 10) . ":belas.mp3";
    } elseif ($x < 100) {
        $temp .= angka_to_sound($x / 10) . ":puluh.mp3" . angka_to_sound($x % 10);
    } elseif ($x < 200) {
        $temp .= ":100.mp3" . angka_to_sound($x - 100);
    } elseif ($x < 1000) {
        $temp .= angka_to_sound($x / 100) . ":ratus.mp3" . angka_to_sound($x % 100);
    } elseif ($x < 2000) {
        $temp .= ":1000.mp3" . angka_to_sound($x - 1000);
    } elseif ($x < 1000000) {
        $temp .= angka_to_sound($x / 1000) . ":ribu.mp3" . angka_to_sound($x % 1000);
    } elseif ($x < 1000000000) {
        $temp .= angka_to_sound($x / 1000000) . " juta" . angka_to_sound($x % 1000000);
    } elseif ($x < 1000000000000) {
        $temp .= angka_to_sound($x / 1000000000) . " milyar" . angka_to_sound(fmod($x, 1000000000));
    } elseif ($x < 1000000000000000) {
        $temp.= angka_to_sound($x / 1000000000000) . " trilyun" . angka_to_sound(fmod($x, 1000000000000));
    }
	// print_r(substr($temp,0,1));exit;
	// if (substr($temp,0,1)==':'){
		// $temp=substr($temp,1,strlen($temp));
	// }
	return $temp;
}
function jenis_retur_gudang($retur, $jenis)
{
    if ($retur == 1) {
        if ($jenis == 1) {
            return "Retur Ganti Uang";
        } elseif ($jenis == 2) {
            return "Retur Ganti Barang";
        } else {
            return "Retur Ganti Barang Beda";
        }
    } else {
        return "Pemusnahan";
    }
}

function metode_pembayaran($id)
{
    switch ($id) {
        case 1:
            $result = "Tunai";
            break;
        case 2:
            $result = "Debit";
            break;
        case 3:
            $result = "Kredit";
            break;
        case 4:
            $result = "Transfer";
            break;
    }

    return $result;
}
function metode_pembayaran_label($id)
{
    switch ($id) {
        case 1:
            $result = '<span class="label label-default">TUNAI</span>';
            break;
        case 2:
            $result = '<span class="label label-default">DEBIT</span>';
            break;
        case 3:
            $result = '<span class="label label-success">KREDIT</span>';
            break;
        case 4:
            $result = '<span class="label label-primary">TRANSFER</span>';
            break;
    }

    return $result;
}
function metode_pembayaran_kasir($id)
{
    switch ($id) {
        case 1:
            $result = "Tunai";
            break;
        case 2:
            $result = "Debit";
            break;
        case 3:
            $result = "Kartu Kredit";
            break;
        case 4:
            $result = "Transfer";
            break;
            case 5:
            $result = "Tagihan Karyawan";
            break;
            case 6:
            $result = "";
            break;
            case 7:
            $result = "Kontraktor";
            break;
        default:
            $result = "";
            break;
    }

    return $result;
}

function numbers_to_words($number)
{
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'minus ';
    $decimal     = ' koma ';
    $dictionary  = array(
            0				=> 'nol',
            1				=> 'satu',
            2				=> 'dua',
            3				=> 'tiga',
            4				=> 'empat',
            5				=> 'lima',
            6				=> 'enam',
            7				=> 'tujuh',
            8				=> 'delapan',
            9				=> 'sembilan',
            10				=> 'sepuluh',
            11				=> 'sebelas',
            19				=> 'belas',
            90				=> 'puluh',
            100				=> 'seratus',
            999				=> 'ratus',
            1000			=> 'seribu',
            999999			=> 'ribu',
            999999999		=> 'juta',
            999999999999	=> 'milyar',
            999999999999999	=> 'trilyun'
        );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . numbers_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
        if (intval($fraction) === 0) {
            $fraction = null;
        }
    }

    switch (true) {
            case $number < 12:
                $string = $dictionary[$number];
                break;
            case $number < 20:
                $tens   = 19;
                $units  = $number % 10;
                $string = $dictionary[$units] . ' ' . $dictionary[$tens];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10));
                $units  = $number % 10;
                $string = $dictionary[$tens] . ' ' . $dictionary[90];
                if ($units) {
                    $string .= ' '.$dictionary[$units];
                }
                break;
            case $number == 100:
                $string = $dictionary[$number];
                break;
            case $number < 200:
                $remainder = $number % 100;
                $string = $dictionary[100];
                if ($remainder) {
                    $string .= ' ' . numbers_to_words($remainder);
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[999];
                if ($remainder) {
                    $string .= ' ' . numbers_to_words($remainder);
                }
                break;
            case $number == 1000:
                $string = $dictionary[$number];
                break;
            case $number < 2000:
                $remainder = $number % 1000;
                $string = $dictionary[1000];
                if ($remainder) {
                    $string .= ' ' . numbers_to_words($remainder);
                }
                break;
            case $number < 1000000:
                $thousands  = (int) ($number / 1000);
                $remainder = $number % ($thousands * 1000);
                $string = numbers_to_words($thousands) . ' ' . $dictionary[999999];
                if ($remainder) {
                    $string .= ' ' . numbers_to_words($remainder);
                }
                break;
            case $number < 1000000000:
                $millions  = (int) ($number / 1000000);
                $remainder = $number % ($millions * 1000000);
                $string = numbers_to_words($millions) . ' ' . $dictionary[999999999];
                if ($remainder) {
                    $string .= ' ' . numbers_to_words($remainder);
                }
                break;
            case $number < 1000000000000:
                $billions  = (int) ($number / 1000000000);
                $remainder = $number % ($billions * 1000000000);
                $string = numbers_to_words($billions) . ' ' . $dictionary[999999999999];
                if ($remainder) {
                    $string .= ' ' . numbers_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = numbers_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= numbers_to_words($remainder);
                }
                break;
        }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

function stAkuntansi($id)
{
    if ($id !='') {
        $permission = array(
                    "-" => "-",
                    "D" => "Debit",
                    "K" => "Kredit",
                    );
        return $permission[$id];
    } else {
        return '';
    }
}

function stBayar($id)
{
    if ($id !='') {
        $permission = array(
                    "0" => "Tidak",
                    "1" => "Ya",
                    );
        return $permission[$id];
    } else {
        return '';
    }
}
function status_piutang($jenis)
{
    if ($jenis=='0') {
        $tipe='<span class="label label-default">MENUNGGU DIPROSES</span>';
    } elseif ($jenis=='1') {
        $tipe='<span class="label label-primary">TELAH DIVERIFIKASI</span>';
    } elseif ($jenis=='2') {
        $tipe='<span class="label label-success">TELAH DIPROSES</span>';
    }
    return $tipe;
}
function status_selisih($selisih)
{
    if ($selisih==0) {
        $tipe='<span class="label label-success">TEPAT WAKTU</span>';
    } elseif ($selisih < 0) {
        $tipe='<span class="label label-primary">LEBIH CEPAT: '.$selisih * (-1).' HARI</span>';
    } elseif ($selisih > 1) {
        $tipe='<span class="label label-danger">TERLAMBAT: '.$selisih.' HARI</span>';
    } else {
        $tipe='';
    }


    return $tipe;
}
function nama_tabel($nama)
{
    if ($nama=='trefund') {
        $tipe='<span class="label label-danger">REFUND</span>';
    } elseif ($nama=='tklaim_detail') {
        $tipe='<span class="label label-primary">PEND ASURANSI</span>';
    } elseif ($nama=='tkontrabon') {
        $tipe='<span class="label label-warning">KONTRABON</span>';
    } elseif ($nama=='tbendahara_pendapatan') {
        $tipe='<span class="label label-success">PEND LAIN-LAIN</span>';
    } elseif ($nama=='tkasbon') {
        $tipe='<span class="label label-danger">KASBON</span>';
    } elseif ($nama=='tmutasi_kas') {
        $tipe='<span class="label label-primary">MUTASI KAS</span>';
    } elseif ($nama=='tpenyesuaian_kas') {
        $tipe='<span class="label label-primary">PENYESUAIAN</span>';
	} elseif ($nama=='trekap') {
        $tipe='<span class="label label-success">GAJI</span>';
	} elseif ($nama=='thonor_dokter') {
        $tipe='<span class="label label-success">HONOR DOKTER</span>';
	} elseif ($nama=='tbagi_hasil') {
        $tipe='<span class="label label-default">BAGI HASIL</span>';
	} elseif ($nama=='tkontrabon') {
        $tipe='<span class="label label-default">KONTRABON</span>';
	} elseif ($nama=='rka_pengajuan') {
        $tipe='<span class="label label-danger">PENGAJUAN</span>';
	} elseif ($nama=='tpengelolaan') {
        $tipe='<span class="label label-default">SELISIH HUTANG & PIUTANG</span>';
	} elseif ($nama=='tgudang_penerimaan') {
        $tipe='<span class="label label-success">PEMBELIAN TUNAI</span>';
    } else {
        $tipe=$nama;
    }


    return $tipe;
}
function nama_tabel_biasa($nama)
{
    if ($nama=='trefund') {
        $tipe='REFUND';
    } elseif ($nama=='tklaim_detail') {
        $tipe='PEND ASURANSI';
    } elseif ($nama=='tkontrabon') {
        $tipe='KONTRABON';
    } elseif ($nama=='tbendahara_pendapatan') {
        $tipe='PEND LAIN-LAIN';
    } elseif ($nama=='tkasbon') {
        $tipe='KASBON';
    } elseif ($nama=='tmutasi_kas') {
        $tipe='MUTASI KAS';
    } elseif ($nama=='tpenyesuaian_kas') {
        $tipe='PENYESUAIAN';
	} elseif ($nama=='trekap') {
        $tipe='GAJI';
	} elseif ($nama=='thonor_dokter') {
        $tipe='HONOR DOKTER';
	} elseif ($nama=='tbagi_hasil') {
        $tipe='BAGI HASIL';
	} elseif ($nama=='tkontrabon') {
        $tipe='KONTRABON';
	} elseif ($nama=='rka_pengajuan') {
        $tipe='PENGAJUAN';
	} elseif ($nama=='tpengelolaan') {
        $tipe='SELISIH HUTANG & PIUTANG';
	} elseif ($nama=='tgudang_penerimaan') {
        $tipe='PEMBELIAN TUNAI';
    } else {
        $tipe=$nama;
    }


    return $tipe;
}
function cek_akhir_row($akhir)
{
    if ($akhir <= 10) {
        return 10;
    } elseif ($akhir <= 20) {
        return 20;
    } elseif ($akhir <= 30) {
        return 30;
    }
}
function cek_c1($akhir)
{
    if ($akhir <=5) {
        return $akhir;
    } elseif ($akhir <= 10) {
        return $akhir+5;
    } elseif ($akhir <= 15) {
        return $akhir+10;
    }
}
function ceiling($number, $significance = 1)
{
    return (is_numeric($number) && is_numeric($significance)) ? (ceil($number/$significance)*$significance) : false;
}
function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}
function encodeURIComponent($string)
{
    $result = "";
    for ($i = 0; $i < strlen($string); $i++) {
        $result .= encodeURIComponentbycharacter(urlencode($string[$i]));
    }
    return $result;
}


function encodeURIComponentbycharacter($char)
{
    if ($char == "+") {
        return "%20";
    }
    if ($char == "%21") {
        return "!";
    }
    if ($char == "%27") {
        return '"';
    }
    if ($char == "%28") {
        return "(";
    }
    if ($char == "%29") {
        return ")";
    }
    if ($char == "%2A") {
        return "*";
    }
    if ($char == "%7E") {
        return "~";
    }
    if ($char == "%80") {
        return "%E2%82%AC";
    }
    if ($char == "%81") {
        return "%C2%81";
    }
    if ($char == "%82") {
        return "%E2%80%9A";
    }
    if ($char == "%83") {
        return "%C6%92";
    }
    if ($char == "%84") {
        return "%E2%80%9E";
    }
    if ($char == "%85") {
        return "%E2%80%A6";
    }
    if ($char == "%86") {
        return "%E2%80%A0";
    }
    if ($char == "%87") {
        return "%E2%80%A1";
    }
    if ($char == "%88") {
        return "%CB%86";
    }
    if ($char == "%89") {
        return "%E2%80%B0";
    }
    if ($char == "%8A") {
        return "%C5%A0";
    }
    if ($char == "%8B") {
        return "%E2%80%B9";
    }
    if ($char == "%8C") {
        return "%C5%92";
    }
    if ($char == "%8D") {
        return "%C2%8D";
    }
    if ($char == "%8E") {
        return "%C5%BD";
    }
    if ($char == "%8F") {
        return "%C2%8F";
    }
    if ($char == "%90") {
        return "%C2%90";
    }
    if ($char == "%91") {
        return "%E2%80%98";
    }
    if ($char == "%92") {
        return "%E2%80%99";
    }
    if ($char == "%93") {
        return "%E2%80%9C";
    }
    if ($char == "%94") {
        return "%E2%80%9D";
    }
    if ($char == "%95") {
        return "%E2%80%A2";
    }
    if ($char == "%96") {
        return "%E2%80%93";
    }
    if ($char == "%97") {
        return "%E2%80%94";
    }
    if ($char == "%98") {
        return "%CB%9C";
    }
    if ($char == "%99") {
        return "%E2%84%A2";
    }
    if ($char == "%9A") {
        return "%C5%A1";
    }
    if ($char == "%9B") {
        return "%E2%80%BA";
    }
    if ($char == "%9C") {
        return "%C5%93";
    }
    if ($char == "%9D") {
        return "%C2%9D";
    }
    if ($char == "%9E") {
        return "%C5%BE";
    }
    if ($char == "%9F") {
        return "%C5%B8";
    }
    if ($char == "%A0") {
        return "%C2%A0";
    }
    if ($char == "%A1") {
        return "%C2%A1";
    }
    if ($char == "%A2") {
        return "%C2%A2";
    }
    if ($char == "%A3") {
        return "%C2%A3";
    }
    if ($char == "%A4") {
        return "%C2%A4";
    }
    if ($char == "%A5") {
        return "%C2%A5";
    }
    if ($char == "%A6") {
        return "%C2%A6";
    }
    if ($char == "%A7") {
        return "%C2%A7";
    }
    if ($char == "%A8") {
        return "%C2%A8";
    }
    if ($char == "%A9") {
        return "%C2%A9";
    }
    if ($char == "%AA") {
        return "%C2%AA";
    }
    if ($char == "%AB") {
        return "%C2%AB";
    }
    if ($char == "%AC") {
        return "%C2%AC";
    }
    if ($char == "%AD") {
        return "%C2%AD";
    }
    if ($char == "%AE") {
        return "%C2%AE";
    }
    if ($char == "%AF") {
        return "%C2%AF";
    }
    if ($char == "%B0") {
        return "%C2%B0";
    }
    if ($char == "%B1") {
        return "%C2%B1";
    }
    if ($char == "%B2") {
        return "%C2%B2";
    }
    if ($char == "%B3") {
        return "%C2%B3";
    }
    if ($char == "%B4") {
        return "%C2%B4";
    }
    if ($char == "%B5") {
        return "%C2%B5";
    }
    if ($char == "%B6") {
        return "%C2%B6";
    }
    if ($char == "%B7") {
        return "%C2%B7";
    }
    if ($char == "%B8") {
        return "%C2%B8";
    }
    if ($char == "%B9") {
        return "%C2%B9";
    }
    if ($char == "%BA") {
        return "%C2%BA";
    }
    if ($char == "%BB") {
        return "%C2%BB";
    }
    if ($char == "%BC") {
        return "%C2%BC";
    }
    if ($char == "%BD") {
        return "%C2%BD";
    }
    if ($char == "%BE") {
        return "%C2%BE";
    }
    if ($char == "%BF") {
        return "%C2%BF";
    }
    if ($char == "%C0") {
        return "%C3%80";
    }
    if ($char == "%C1") {
        return "%C3%81";
    }
    if ($char == "%C2") {
        return "%C3%82";
    }
    if ($char == "%C3") {
        return "%C3%83";
    }
    if ($char == "%C4") {
        return "%C3%84";
    }
    if ($char == "%C5") {
        return "%C3%85";
    }
    if ($char == "%C6") {
        return "%C3%86";
    }
    if ($char == "%C7") {
        return "%C3%87";
    }
    if ($char == "%C8") {
        return "%C3%88";
    }
    if ($char == "%C9") {
        return "%C3%89";
    }
    if ($char == "%CA") {
        return "%C3%8A";
    }
    if ($char == "%CB") {
        return "%C3%8B";
    }
    if ($char == "%CC") {
        return "%C3%8C";
    }
    if ($char == "%CD") {
        return "%C3%8D";
    }
    if ($char == "%CE") {
        return "%C3%8E";
    }
    if ($char == "%CF") {
        return "%C3%8F";
    }
    if ($char == "%D0") {
        return "%C3%90";
    }
    if ($char == "%D1") {
        return "%C3%91";
    }
    if ($char == "%D2") {
        return "%C3%92";
    }
    if ($char == "%D3") {
        return "%C3%93";
    }
    if ($char == "%D4") {
        return "%C3%94";
    }
    if ($char == "%D5") {
        return "%C3%95";
    }
    if ($char == "%D6") {
        return "%C3%96";
    }
    if ($char == "%D7") {
        return "%C3%97";
    }
    if ($char == "%D8") {
        return "%C3%98";
    }
    if ($char == "%D9") {
        return "%C3%99";
    }
    if ($char == "%DA") {
        return "%C3%9A";
    }
    if ($char == "%DB") {
        return "%C3%9B";
    }
    if ($char == "%DC") {
        return "%C3%9C";
    }
    if ($char == "%DD") {
        return "%C3%9D";
    }
    if ($char == "%DE") {
        return "%C3%9E";
    }
    if ($char == "%DF") {
        return "%C3%9F";
    }
    if ($char == "%E0") {
        return "%C3%A0";
    }
    if ($char == "%E1") {
        return "%C3%A1";
    }
    if ($char == "%E2") {
        return "%C3%A2";
    }
    if ($char == "%E3") {
        return "%C3%A3";
    }
    if ($char == "%E4") {
        return "%C3%A4";
    }
    if ($char == "%E5") {
        return "%C3%A5";
    }
    if ($char == "%E6") {
        return "%C3%A6";
    }
    if ($char == "%E7") {
        return "%C3%A7";
    }
    if ($char == "%E8") {
        return "%C3%A8";
    }
    if ($char == "%E9") {
        return "%C3%A9";
    }
    if ($char == "%EA") {
        return "%C3%AA";
    }
    if ($char == "%EB") {
        return "%C3%AB";
    }
    if ($char == "%EC") {
        return "%C3%AC";
    }
    if ($char == "%ED") {
        return "%C3%AD";
    }
    if ($char == "%EE") {
        return "%C3%AE";
    }
    if ($char == "%EF") {
        return "%C3%AF";
    }
    if ($char == "%F0") {
        return "%C3%B0";
    }
    if ($char == "%F1") {
        return "%C3%B1";
    }
    if ($char == "%F2") {
        return "%C3%B2";
    }
    if ($char == "%F3") {
        return "%C3%B3";
    }
    if ($char == "%F4") {
        return "%C3%B4";
    }
    if ($char == "%F5") {
        return "%C3%B5";
    }
    if ($char == "%F6") {
        return "%C3%B6";
    }
    if ($char == "%F7") {
        return "%C3%B7";
    }
    if ($char == "%F8") {
        return "%C3%B8";
    }
    if ($char == "%F9") {
        return "%C3%B9";
    }
    if ($char == "%FA") {
        return "%C3%BA";
    }
    if ($char == "%FB") {
        return "%C3%BB";
    }
    if ($char == "%FC") {
        return "%C3%BC";
    }
    if ($char == "%FD") {
        return "%C3%BD";
    }
    if ($char == "%FE") {
        return "%C3%BE";
    }
    if ($char == "%FF") {
        return "%C3%BF";
    }
    return $char;
}

function convertToDateTime($dateString) {
    return new DateTime($dateString);
}

function TipeTransaksiVerifikasi($id, $label)
{
    if ($id != '') {
        $data = array(
            '1' => '<span class="label label-danger">'.$label.'</span>',
            '2' => '<span class="label label-primary">'.$label.'</span>',
            '3' => '<span class="label label-warning">'.$label.'</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusEditVerifikasi($id)
{
    if ($id != '') {
        $data = array(
            '' => '<span class="label label-danger"><i class="fa fa-times"></i> Tidak</span>',
            '0' => '<span class="label label-danger"><i class="fa fa-times"></i> Tidak</span>',
            '1' => '<span class="label label-success"><i class="fa fa-check"></i> Ya</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusConnect($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger">Not Connect</span>',
            '1' => '<span class="label label-success">Connected</span>',
            '9' => '<span class="label label-warning">Failed</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusActive($id)
{
    if ($id != '') {
        $data = array(
            'inactive' => '<span class="label label-danger">Inactive</span>',
            'active' => '<span class="label label-success">Active</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusEncounter($id)
{
    if ($id != '') {
        $data = array(
            'not-connect' => '<span class="label label-danger">Not Connect</span>',
            'arrived' => '<span class="label label-warning">Arrived</span>',
            'in-progress' => '<span class="label label-primary">In Progress</span>',
            'finished' => '<span class="label label-success">Finished</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusRujukanRS($id)
{
    if ($id != '') {
        $data = array(
            '0' => '<span class="label label-danger"><i class="fa fa-times"></i></span>',
            '1' => '<span class="label label-success"><i class="fa fa-check"></i></span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}
function tipe_komunikasi_sbar($id,$jenis)
{
    if ($id != '') {
        $data = array(
            '1' => '<strong>S B A R</strong>',
            '2' => '<strong>NILAI KRITIS ('.$jenis.')</strong>',
            '3' => '<strong>NOTES</strong>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function StatusHoldHonor($id)
{
    if ($id == 0) {
        return '<span class="label label-danger"><i class="fa fa-times"></i> Tidak</span>';
    } else {
        return '<span class="label label-success"><i class="fa fa-check"></i> Ya</span>';
    }
}

function StatusPengelolaanHold($status)
{
    if ($status) {
        return '<span class="label label-success"><i class="fa fa-check"></i></span>';
    } else {
        return '<span class="label label-danger"><i class="fa fa-times"></i></span>';
    }
}

function StatusKasirVerifikasi($id)
{
    if ($id != '') {
        $data = array(
            '' => '<span class="label label-danger">Unverified</span>',
            '0' => '<span class="label label-danger">Unverified</span>',
            '1' => '<span class="label label-success">Verified</span>',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function styleNilaiLembaranRM($value)
{
    $color = '';
    $icon = '';

    if ($value == '+') {
        $color = 'label-success';
        $icon = '<i class="fa fa-plus"></i>';
    } elseif ($value == '-') {
        $color = 'label-danger';
        $icon = '<i class="fa fa-minus"></i>';
    } else {
        $color = 'label-warning';
        $icon = '<svg height="8" width="8" aria-hidden="true" style="padding: 2px 0 0 0;" focusable="false" data-prefix="fas" data-icon="equals" class="svg-inline--fa fa-equals fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
      <path fill="currentColor" d="M416 304H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32zm0-192H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
     </svg>';
    }

    return '<span class="label label-xl ' . $color . '">' . $icon . '</span>';
};

function balik_spasi_header($kata, $pengganti=" ")
{
    $kata= str_replace("_", $pengganti, $kata);
    $result = preg_replace("/[^a-zA-Z ]/", "", $kata);
    return $result;
}

function base64url_decode($data)
{
    return base64_decode(strtr($data, '-_', '+/') . str_repeat('=', 3 - (3 + strlen($data)) % 4));
}

function base64url_encode($data)
{
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function jsonDecode($value)
{
    return $value && $value != '' ? json_decode($value) : array();
}

function jsonEncode($value)
{
    return isset($value) && $value != '[]' ? json_encode($value) : '';
}

function getDateWithDay($day, $format) {
    $days = array(
        'Minggu' => 'Sunday', 
        'Senin' => 'Monday', 
        'Selasa' => 'Tuesday', 
        'Rabu' => 'Wednesday',
        'Kamis' => 'Thursday',
        'Jumat' => 'Friday', 
        'Sabtu' => 'Saturday'
    );
    return date($format, strtotime($days[$day], strtotime('this week')));
}

function getDayWithDate($day) {
    $days = array(
        'Minggu' => 'Sunday', 
        'Senin' => 'Monday', 
        'Selasa' => 'Tuesday', 
        'Rabu' => 'Wednesday',
        'Kamis' => 'Thursday',
        'Jumat' => 'Friday', 
        'Sabtu' => 'Saturday'
    );

    $kodehari = date('N', strtotime($days[$day], strtotime('this week'))) + 1;

    if ($kodehari == 8) {
        return 1;
    } else {
        return $kodehari;
    }
}

function fileExists($filePath)
{
    return is_file(getcwd() . $filePath) && file_exists(getcwd() . $filePath);
}

function addDayToDate($date, $increment)
{
    return date('Y-m-d', strtotime($date . ' + ' .$increment . 'day'));
}
function addMonthToDate($date, $increment)
{
    return date('Y-m-d', strtotime($date . ' + ' .$increment . 'months'));
}
function MinMonthToDate($date, $increment)
{
    return date('Y-m-d', strtotime($date . ' - ' .$increment . 'months'));
}

function StatusCheckoutRanap($status)
{
    if ($status != '') {
        $data = array(
            '0' => '<span data-toggle="tooltip" class="label label-danger"><i class="fa fa-check"></i> Masih Dirawat</span>',
            '1' => '<span data-toggle="tooltip" class="label label-success"><i class="fa fa-check"></i> Sudah Pulang</span>'
        );

        return $data[$status];
    }
}

function StatusPembayaranInformasi($status)
{
    if ($status != '') {
        $data = array(
            '0' => '<span data-toggle="tooltip" class="label label-danger">Menunggu Diproses</span>',
            '1' => '<span data-toggle="tooltip" class="label label-primary">Telah Diproses</span>',
            '2' => '<span data-toggle="tooltip" class="label label-success">Telah Diverifikasi</span>'
        );

        return $data[$status];
    }
}

function calcDiscountPercent($discount, $subtotal)
{
    if ($discount > 0) {
        return number_format(($discount * 100) / $subtotal, 2);
    } else {
        return number_format(0, 2);
    }
}
function addTime($time, $days, $months, $years)
{
    // Convert unix time to date format
    if (is_numeric($time))
    $time = date('Y-m-d', $time);

    try
    {
        $date_time = new DateTime($time);
    }
    catch (Exception $e)
    {
        echo $e->getMessage();
        exit;
    }

    if ($days)
    $date_time->add(new DateInterval('P'.$days.'D'));

    // Preserve day number
    if ($months or $years)
    $old_day = $date_time->format('d');

    if ($months)
    $date_time->add(new DateInterval('P'.$months.'M'));

    if ($years)
    $date_time->add(new DateInterval('P'.$years.'Y'));

    // Patch for adding months or years    
    if ($months or $years)
    {
        $new_day = $date_time->format("d");

        // The day is changed - set the last day of the previous month
        if ($old_day != $new_day)
        $date_time->sub(new DateInterval('P'.$new_day.'D'));
    }
    // You can chage returned format here
    return $date_time->format('Y-m-d');
}

function GetPotonganDokterRanap($idrawatinap, $iddokter)
{
    $CI =& get_instance();
    $queryRanap = $CI->db->query("SELECT idtipe, idtipepasien FROM trawatinap_pendaftaran WHERE id = $idrawatinap");
    $ranap = $queryRanap->row();

    if ($ranap->idtipe = 1) {
        // RANAP
        if ($ranap->idtipepasien == 1) {
            // Pasien RS
            $queryDokter = $CI->db->query("SELECT potonganrsranap AS potongan_rs, pajak AS pajak_dokter, potonganfeers AS potonganfee_rs FROM mdokter WHERE id = $iddokter");
            $dokter = $queryDokter->row();
        } else {
            // Pasien Pribadi
            $queryDokter = $CI->db->query("SELECT potonganrsranapsiang AS potongan_rs, pajak AS pajak_dokter, potonganfeerspribadi AS potonganfee_rs FROM mdokter WHERE id = $iddokter");
            $dokter = $queryDokter->row();
        }
    } else {
        // ODS
        if ($ranap->idtipepasien == 1) {
            // Pasien RS
            $queryDokter = $CI->db->query("SELECT potonganrsods AS potongan_rs, pajak AS pajak_dokter, potonganfeers AS potonganfee_rs FROM mdokter WHERE id = $iddokter");
            $dokter = $queryDokter->row();
        } else {
            // Pasien Pribadi
            $queryDokter = $CI->db->query("SELECT potonganrsodssiang AS potongan_rs, pajak AS pajak_dokter, potonganfeerspribadi AS potonganfee_rs FROM mdokter WHERE id = $iddokter");
            $dokter = $queryDokter->row();
        }
    }

    return $dokter;
}
function get_nama_kota($id){
	$CI =& get_instance();
	$kota='';
	$q="SELECT nama FROM mfwilayah WHERE id='$id'";
	$kota=$CI->db->query($q)->row('nama');
	return $kota;
}
function get_nama_user($id){
	$CI =& get_instance();
	$kota='';
	$q="SELECT nama FROM mppa WHERE user_id='$id'";
	$kota=$CI->db->query($q)->row('nama');
	return $kota;
}
function get_nama_user_nama($id){
	$CI =& get_instance();
	$kota='';
	$q="SELECT name as nama FROM musers WHERE id='$id'";
	// print_r($q);exit;
	$kota=$CI->db->query($q)->row('nama');
	return $kota;
}
function get_nama_ppa($id){
	$CI =& get_instance();
	$kota='';
	$q="SELECT nama FROM mppa WHERE id='$id'";
	$kota=$CI->db->query($q)->row('nama');
	return $kota;
}
function get_parameter_ews($id){
	$CI =& get_instance();
	$kota='';
	$q="SELECT id,nourut,nama_ews,staktif FROM ews_master WHERE id='$id'";
	$kota=$CI->db->query($q)->row();
	return $kota;
}
function get_nama_dokter_ttd($id){
	$CI =& get_instance();
	$kota='';
	// $q="SELECT nip,nama FROM mdokter WHERE id='$id'";
	$q="SELECT H.nama,H.nip from mppa H WHERE H.pegawai_id='$id' AND H.tipepegawai='2'";
	$kota=$CI->db->query($q)->row();
	$ttd='( '.$kota->nama.' )<br>'.$kota->nip;
	return $ttd;
}
function get_nama_ppa_array($id){
	$CI =& get_instance();
	$kota='';
	$q="SELECT * FROM mppa WHERE id='$id'";
	$kota=$CI->db->query($q)->row_array();
	return $kota;
}
function get_nama_ref($id,$ref_head_id){
	$CI =& get_instance();
	$ref='';
	$q="SELECT ref FROM merm_referensi WHERE nilai='$id' AND ref_head_id='$ref_head_id'";
	$ref=$CI->db->query($q)->row('ref');
	return $ref;
}
function get_where_kunjungan_ranap(){
	$CI =& get_instance();
	$where='';
	$q="SELECT * FROM setting_ranap_kunjungan";
	$ref=$CI->db->query($q)->result();
	
	foreach ($ref as $r){
		$sub='';
		if ($r->asal_pasien!='0'){
			$sub .= "H.idtipe ='".$r->asal_pasien."' ";
		}else{
			$sub .="H.idtipe IN (1,2) ";
		}
		if ($r->idpoli!='0'){
			$sub .=" AND  H.idpoliklinik ='".$r->idpoli."' ";
		}
		if ($r->iddokter!='0'){
			$sub .=" AND  H.iddokter ='".$r->iddokter."' ";
		}
		
		
		$where .=($where?' OR ': '' )."(".$sub.")";
	}
	return ($where?' AND (': '' ).$where.($where?' )': '' );
}
function build_html_calendar_detail($year, $month, $events = null, $css = '')
{
	
	// if ($data_event){
		// print_r($data_event);exit;
	// }

	// CSS classes
	$css_cal = 'kalendar';
	$css_cal_row = 'kalendar-row';
	$css_cal_day_head = 'kalendar-day-head';
	$css_cal_day = 'kalendar-day';
	$css_cal_day_number = 'day-number' . $css;
	$css_cal_day_blank = 'kalendar-day-np';
	$css_cal_day_last = 'kalendar-day-last';
	$css_cal_day_event = 'kalendar-day-event  bg-danger-lighter';
	$css_cal_event = 'kalendar-event';

	// Table headings
	$headings = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

	// Start: draw table
	$kalendar =
		"<table cellpadding='0' cellspacing='0' class='{$css_cal}'>" .
		"<tr class='{$css_cal_row}'>" .
		"<td class='{$css_cal_day_head}'>" .
		implode("</td><td class='{$css_cal_day_head}'>", $headings) .
		'</td>' .
		'</tr>';

	// Days and weeks
	$running_day = date('N', mktime(0, 0, 0, $month, 1, $year));
	$days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));

	// Row for week one
	$kalendar .= "<tr class='{$css_cal_row}'>";

	// Print "blank" days until the first of the current week
	for ($x = 1; $x < $running_day; $x++) {
		$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
	}

	// Keep going with days...
	for ($day = 1; $day <= $days_in_month; $day++) {
		// Check if there is an event today
		
		
		$cur_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
		$draw_event = false;
		
		
		// if (isset($events) && isset($events[$cur_date])) {
			// $draw_event = true;
		// }
		
		$data_event = array_filter($events, function($var) use ($cur_date) { 
			return ($var['tanggal'] == $cur_date);
		});
		if ($data_event){
			$draw_event = true;
			
		}
		
		// Day cell
		$kalendar .= $draw_event ?
		"<td class='{$css_cal_day} {$css_cal_day_event}'>" :
		"<td class='{$css_cal_day}'>";

		// Add the day number
		$kalendar .= "<div class='{$css_cal_day_number}'>" . $day;

		// Insert an event for this day
		if ($draw_event) {
			foreach($data_event as $row){
			$kalendar .=
			"<div class='{$css_cal_event}'>" .
				"<a href='{$row['href']}' class='{$row['status']}' target='".($row['href']=='#'?'_self':'_blank')."' title='{$row['judul']}'>" .
				$row['text'] .
				'</a>' .
			'</div>';
			}
		}

		// Close day cell
		$kalendar .= '</div></td>';

		// New row
		if ($running_day == 7) {
			$kalendar .= '</tr>';
			if (($day + 1) <= $days_in_month) {
				$kalendar .= "<tr class='{$css_cal_row}'>";
			}
			$running_day = 1;
		}

		// Increment the running day
		else {
			$running_day++;
		}
	} // for $day

	// Finish the rest of the days in the week
	if ($running_day != 1) {
		for ($x = $running_day; $x <= 7; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
	}

	// Final row
	$kalendar .= '</tr>';

	// End the table
	$kalendar .= '</table>';

	// All done, return result
	return $kalendar;
}
function build_html_calendar_view($year, $month, $events = null, $css = '')
{
	
	// if ($data_event){
		// print_r($data_event);exit;
	// }

	// CSS classes
	$css_cal = 'kalendar';
	$css_cal_row = 'kalendar-row';
	$css_cal_day_head = 'kalendar-day-head';
	$css_cal_day = 'kalendar-day';
	$css_cal_day_number = 'day-number' . $css;
	$css_cal_day_blank = 'kalendar-day-np';
	$css_cal_day_last = 'kalendar-day-last';
	$css_cal_day_event = 'kalendar-day-event  bg-danger-lighter';
	$css_cal_event = 'kalendar-event';

	// Table headings
	$headings = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

	// Start: draw table
	$kalendar =
		"<table cellpadding='0' cellspacing='0' class='{$css_cal}'>" .
		"<tr class='{$css_cal_row}'>" .
		"<td class='{$css_cal_day_head}'>" .
		implode("</td><td class='{$css_cal_day_head}'>", $headings) .
		'</td>' .
		'</tr>';

	// Days and weeks
	$running_day = date('N', mktime(0, 0, 0, $month, 1, $year));
	$days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));

	// Row for week one
	$kalendar .= "<tr class='{$css_cal_row}'>";

	// Print "blank" days until the first of the current week
	for ($x = 1; $x < $running_day; $x++) {
		$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
	}

	// Keep going with days...
	for ($day = 1; $day <= $days_in_month; $day++) {
		// Check if there is an event today
		
		
		$cur_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
		$draw_event = false;
		
		
		// if (isset($events) && isset($events[$cur_date])) {
			// $draw_event = true;
		// }
		
		$data_event = array_filter($events, function($var) use ($cur_date) { 
			return ($var['tanggal'] == $cur_date);
		});
		if ($data_event){
			$draw_event = true;
			
		}
		
		// Day cell
		$kalendar .= $draw_event ?
		"<td class='{$css_cal_day} {$css_cal_day_event}'>" :
		"<td class='{$css_cal_day}'>";

		// Add the day number
		$kalendar .= "<div class='{$css_cal_day_number}'>" . $day;

		// Insert an event for this day
		if ($draw_event) {
			foreach($data_event as $row){
			$kalendar .=
			"<div class='{$css_cal_event}'>" .
				"<a href='javascript:void(0)' ".$row['onclick']." class='{$row['status']}' title='{$row['judul']}'>" .
				$row['text'] .
				'</a>' .
			'</div>';
			}
		}

		// Close day cell
		$kalendar .= '</div></td>';

		// New row
		if ($running_day == 7) {
			$kalendar .= '</tr>';
			if (($day + 1) <= $days_in_month) {
				$kalendar .= "<tr class='{$css_cal_row}'>";
			}
			$running_day = 1;
		}

		// Increment the running day
		else {
			$running_day++;
		}
	} // for $day

	// Finish the rest of the days in the week
	if ($running_day != 1) {
		for ($x = $running_day; $x <= 7; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
	}

	// Final row
	$kalendar .= '</tr>';

	// End the table
	$kalendar .= '</table>';

	// All done, return result
	return $kalendar;
}
function build_html_calendar_detail_week($tanggal,$list_tanggal,$events = null, $css = '')
{
	
	$running_day=$list_tanggal[0]->hari;
	// print_r($running_day);exit;
	
	// CSS classes
	$css_cal = 'kalendar';
	$css_cal_row = 'kalendar-row';
	$css_cal_day_head = 'kalendar-day-head';
	$css_cal_day = 'kalendar-day';
	$css_cal_day_number = 'day-number' . $css;
	$css_cal_day_blank = 'kalendar-day-np';
	$css_cal_day_event = 'kalendar-day-event  bg-danger-lighter';
	$css_cal_event = 'kalendar-event';

	// Table headings
	$headings = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

	// Start: draw table
	$kalendar =
		"<table cellpadding='0' cellspacing='0' class='{$css_cal}'>" .
		"<tr class='{$css_cal_row}'>" .
		"<td class='{$css_cal_day_head}'>" .
		implode("</td><td class='{$css_cal_day_head}'>", $headings) .
		'</td>' .
		'</tr>';
	// $kalendar .= "<tr class='{$css_cal_row}'>";

	// // Print "blank" days until the first of the current week
	// for ($x = 1; $x < $running_day; $x++) {
		// $kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
	// }
	if ($running_day!='1'){
			
		$kalendar .= "<tr class='{$css_cal_row}'>";
		for ($x = 1; $x < $running_day-1; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
	}else{
		$kalendar .= "<tr class='{$css_cal_row}'>";
		for ($x = 1; $x <= 6; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
		// $kalendar .= '</tr>';
	}
	foreach($list_tanggal as $r){
		$cur_date = $r->tanggal;
		$draw_event = false;
		
		
		$running_day=$r->hari;
		$data_event = array_filter($events, function($var) use ($cur_date) { 
			return ($var['tanggal'] == $cur_date);
		});
		if ($data_event){
			$draw_event = true;
			
		}
		// Day cell
		$kalendar .= $draw_event ?
		"<td class='{$css_cal_day} {$css_cal_day_event}'>" :
		"<td class='{$css_cal_day}'>";

		// Add the day number
		$kalendar .= "<div class='{$css_cal_day_number}'>" .tanggal_indo_DM($r->tanggal);

		// Insert an event for this day
		if ($draw_event) {
			foreach($data_event as $row){
			$kalendar .=
			"<div class='{$css_cal_event}'>" .
				"<a href='{$row['href']}' class='{$row['status']}' target='".($row['href']=='#'?'_self':'_blank')."' title='{$row['judul']}'>" .
				$row['text'] .
				'</a>' .
			'</div>';
			}
		}
		$kalendar .= '</div></td>';
		if ($running_day==1){
			$kalendar .= '</tr>';
		}
		
	}
	if ($running_day>1){
		for ($x = $running_day; $x <= 7; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
	}
	// Final row
	$kalendar .= '</tr>';

	// End the table
	$kalendar .= '</table>';

	// All done, return result
	return $kalendar;
}
function build_html_calendar_view_week($tanggal,$list_tanggal,$events = null, $css = '')
{
	
	$running_day=$list_tanggal[0]->hari;
	// print_r($running_day);exit;
	
	// CSS classes
	$css_cal = 'kalendar';
	$css_cal_row = 'kalendar-row';
	$css_cal_day_head = 'kalendar-day-head';
	$css_cal_day = 'kalendar-day';
	$css_cal_day_number = 'day-number' . $css;
	$css_cal_day_blank = 'kalendar-day-np';
	$css_cal_day_event = 'kalendar-day-event  bg-danger-lighter';
	$css_cal_event = 'kalendar-event';

	// Table headings
	$headings = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

	// Start: draw table
	$kalendar =
		"<table cellpadding='0' cellspacing='0' class='{$css_cal}'>" .
		"<tr class='{$css_cal_row}'>" .
		"<td class='{$css_cal_day_head}'>" .
		implode("</td><td class='{$css_cal_day_head}'>", $headings) .
		'</td>' .
		'</tr>';
	// $kalendar .= "<tr class='{$css_cal_row}'>";

	// // Print "blank" days until the first of the current week
	// for ($x = 1; $x < $running_day; $x++) {
		// $kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
	// }
	if ($running_day!='1'){
			
		$kalendar .= "<tr class='{$css_cal_row}'>";
		for ($x = 1; $x < $running_day-1; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
	}else{
		$kalendar .= "<tr class='{$css_cal_row}'>";
		for ($x = 1; $x <= 6; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
		// $kalendar .= '</tr>';
	}
	foreach($list_tanggal as $r){
		$cur_date = $r->tanggal;
		$draw_event = false;
		
		
		$running_day=$r->hari;
		$data_event = array_filter($events, function($var) use ($cur_date) { 
			return ($var['tanggal'] == $cur_date);
		});
		if ($data_event){
			$draw_event = true;
			
		}
		// Day cell
		$kalendar .= $draw_event ?
		"<td class='{$css_cal_day} {$css_cal_day_event}'>" :
		"<td class='{$css_cal_day}'>";

		// Add the day number
		$kalendar .= "<div class='{$css_cal_day_number}'>" .tanggal_indo_DM($r->tanggal);

		// Insert an event for this day
		if ($draw_event) {
			foreach($data_event as $row){
			$kalendar .=
			"<div class='{$css_cal_event}'>" .
				"<a href='javascript:void(0)' ".$row['onclick']." class='{$row['status']}' title='{$row['judul']}'>" .
				$row['text'] .
				'</a>' .
			'</div>';
			}
		}
		$kalendar .= '</div></td>';
		if ($running_day==1){
			$kalendar .= '</tr>';
		}
		
	}
	if ($running_day>1){
		for ($x = $running_day; $x <= 7; $x++) {
			$kalendar .= "<td class='{$css_cal_day_blank}'> </td>";
		}
	}
	// Final row
	$kalendar .= '</tr>';

	// End the table
	$kalendar .= '</table>';

	// All done, return result
	return $kalendar;
}
function build_html_calendar_detail_day($tanggal,$css='',$no_hari,$events)
{
	$tanggal= date_format(date_create($tanggal),"Y-m-d");
	$day= date_format(date_create($tanggal),"d");
	$css_cal = 'kalendar';
	$css_cal_row = 'kalendar-row';
	$css_cal_day_head = 'kalendar-day-head';
	$css_cal_day = 'kalendar-day';
	$css_cal_day_number = 'day-number' . $css;
	$css_cal_day_blank = 'kalendar-day-np';
	$css_cal_day_event = 'kalendar-day-event  bg-danger-lighter';
	$css_cal_event = 'kalendar-event';

	// Table headings
	$headings = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

	// Start: draw table
	$kalendar =
		"<table cellpadding='0' cellspacing='0' class='{$css_cal}'>" .
		"<tr class='{$css_cal_row}'>" .
		"<td class='{$css_cal_day_head}'>".$headings[$no_hari-1] .
		'</td>' .
		'</tr>';



	// Row for week one
	$kalendar .= "<tr class='{$css_cal_row}'>";
	// $kalendar .= "<div>";
	
		$draw_event = false;
		$data_event = array_filter($events, function($var) use ($tanggal) { 
			return ($var['tanggal'] == $tanggal);
		});
		if ($data_event){
			$draw_event = true;
			
		}
		
		// Day cell
		$kalendar .= $draw_event ? "<td class='{$css_cal_day} {$css_cal_day_event}'>" : "<td class='{$css_cal_day}'>";
	$kalendar .= "<div class='{$css_cal_day_number}'>" . $day;
		
		if ($draw_event) {
			foreach($data_event as $row){
			$kalendar .=
			"<div class='{$css_cal_event}'>" .
				"<a href='{$row['href']}' class='{$row['status']}' target='".($row['href']=='#'?'_self':'_blank')."' title='{$row['judul']}'>" .
				$row['text'] .
				'</a>' .
			'</div>';
			}
		}
		$kalendar .= '</div></td></tr>';
		

	// Final row
	$kalendar .= '</tr>';

	// End the table
	$kalendar .= '</table>';

	// All done, return result
	return $kalendar;
}
function build_html_calendar_view_day($tanggal,$css='',$no_hari,$events)
{
	$tanggal= date_format(date_create($tanggal),"Y-m-d");
	$day= date_format(date_create($tanggal),"d");
	$css_cal = 'kalendar';
	$css_cal_row = 'kalendar-row';
	$css_cal_day_head = 'kalendar-day-head';
	$css_cal_day = 'kalendar-day';
	$css_cal_day_number = 'day-number' . $css;
	$css_cal_day_blank = 'kalendar-day-np';
	$css_cal_day_event = 'kalendar-day-event  bg-danger-lighter';
	$css_cal_event = 'kalendar-event';

	// Table headings
	$headings = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

	// Start: draw table
	$kalendar =
		"<table cellpadding='0' cellspacing='0' class='{$css_cal}'>" .
		"<tr class='{$css_cal_row}'>" .
		"<td class='{$css_cal_day_head}'>".$headings[$no_hari-1] .
		'</td>' .
		'</tr>';



	// Row for week one
	$kalendar .= "<tr class='{$css_cal_row}'>";
	// $kalendar .= "<div>";
	
		$draw_event = false;
		$data_event = array_filter($events, function($var) use ($tanggal) { 
			return ($var['tanggal'] == $tanggal);
		});
		if ($data_event){
			$draw_event = true;
			
		}
		
		// Day cell
		$kalendar .= $draw_event ? "<td class='{$css_cal_day} {$css_cal_day_event}'>" : "<td class='{$css_cal_day}'>";
	$kalendar .= "<div class='{$css_cal_day_number}'>" . $day;
		
		if ($draw_event) {
			foreach($data_event as $row){
			$kalendar .=
			"<div class='{$css_cal_event}'>" .
				"<a href='javascript:void(0)' ".$row['onclick']." class='{$row['status']}' title='{$row['judul']}'>" .
				$row['text'] .
				'</a>' .
			'</div>';
			}
		}
		$kalendar .= '</div></td></tr>';
		

	// Final row
	$kalendar .= '</tr>';

	// End the table
	$kalendar .= '</table>';

	// All done, return result
	return $kalendar;
}

function convert_month_year($num,$year)
{
	switch ($num) {
		case '01':
			return 'Januari'.' '.$year;
			break;
		case '02':
			return 'Februari'.' '.$year;
			break;
		case '03':
			return 'Maret'.' '.$year;
			break;
		case '04':
			return 'April'.' '.$year;
			break;
		case '05':
			return 'Mei'.' '.$year;
			break;
		case '06':
			return 'Juni'.' '.$year;
			break;
		case '07':
			return 'Juli'.' '.$year;
			break;
		case '08':
			return 'Agustus'.' '.$year;
			break;
		case '09':
			return 'September'.' '.$year;
			break;
		case '10':
			return 'Oktober'.' '.$year;
			break;
		case '11':
			return 'November'.' '.$year;
			break;
		case '12':
			return 'Desember'.' '.$year;
			break;
	}
}
function text_alergi($id){
	$hasil='';
	if ($id=='2'){
		$hasil='<span class="text-danger">Ada Alergi</span>';
	}elseif ($id=='1'){
		$hasil='<span>Tidak Diketahui</span>';
		
	}elseif ($id=='0'){
		$hasil='<span>Tidak Ada Alegi</span>';
	}else{
		$hasil='<span>Tidak Ada Alegi</span>';
		
	}
	
	return $hasil;
}
function text_status_proses_farmasi($id){
	$hasil='';
	if ($id=='0'){
		$hasil=text_danger('Dibatalkan');
	}elseif ($id=='1'){
		$hasil=text_default('Menunggu Diproses');
		
	}elseif ($id=='2'){
		$hasil=text_warning('Sedang Diproses');
	}else{
		$hasil=text_success('Telah Diserahkan');
		
	}
	
	return $hasil;
}
function format_tanggal_short($tanggal){
    $hasil='';
    if ($tanggal=='0000-00-00'){
        $hasil='';
    }elseif($tanggal=='1970-01-01'){
        $hasil='';
	}elseif($tanggal==null){
        $hasil='';
    }else{
        $hasil=HumanDateShort($tanggal);
    }
    return $hasil;
}
function HumanDateShort_exp($tanggal){
    $hasil='';
    if ($tanggal=='0000-00-00'){
        $hasil='-';
    }elseif($tanggal=='1970-01-01'){
        $hasil='-';
	}elseif($tanggal==null){
        $hasil='-';
    }else{
        $hasil=HumanDateShort($tanggal);
    }
    return $hasil;
}
function setting_diskon_tujuan_poliklinik($status, $tujuan) {
    if ($status == '0') {
        return 'Tidak Ditentukan';
    } else {
        return $tujuan;
    }
}

function div_panel_kendali($user_acces_form, $pendaftaran_id, $custom = '')
{
    // $data_user=get_acces();
    // $user_acces_form=$data_user['user_acces_form'];
    $panel = '
				<div class="btn-group push-5-t">
					<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						'.($custom ?: "<span class='fa fa-caret-down'></span> PANEL KENDALI").'
					</button>
					<ul class="dropdown-menu">';

    if (UserAccesForm($user_acces_form, ['1625'])) {
        $panel .= '<li class=""><a href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_rj" data-category="erm_rj"><i class="fa fa-fw fa-folder-open-o push-5-r"></i> ERM RJ</a></li>';
    }
   
    // if (UserAccesForm($user_acces_form, ['1627'])) {
    //     $panel .= '<li class=""><a href="javascript:void(0)" data-category="erm_penunjang"><i class="fa fa-fw fa-file-photo-o push-5-r"></i> PENUNJANG</a></li>';
    // }
    if (UserAccesForm($user_acces_form, ['1628'])) {
        $panel .= '<li class=""><a href="'.site_url().'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/erm_e_resep/input_eresep" data-category="e_resep"><i class="fa fa-fw fa-file-audio-o push-5-r"></i> E-RESEP</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_laboratorium_umum/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_lab/laboratorium_permintaan" data-category="erm_laboratorium"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> LABORATORIUM</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_laboratorium_patologi_anatomi/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_lab/patologi_anatomi_permintaan" data-category="erm_laboratorium"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> PATALOGI ANATOMI</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_laboratorium_bank_darah/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_lab/bank_darah_permintaan" data-category="erm_laboratorium"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> BANK DARAH</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_radiologi_xray/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_rad/xray_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> X-RAY</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_radiologi_usg/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_rad/usg_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> USG</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_radiologi_ctscan/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_rad/ctscan_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> CT-SCAN</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_radiologi_mri/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_rad/mri_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> MRI</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1629'])) {
        $panel .= '<li class=""><a href="'.site_url().'term_radiologi_lainnya/tindakan/rawat_jalan/'.$pendaftaran_id.'/erm_rad/lainnya_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> RADIOLOGI LAIN-LAIN</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1631'])) {
        $panel .= '<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/erm_trx" data-category="erm_trx"><i class="fa fa-fw fa-file-text-o push-5-r"></i> TRANSAKSI</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1632'])) {
        $panel .= '<li class=""><a href="'.site_url().'tpoliklinik_ranap/tindakan/'.$pendaftaran_id.'/erm_perencanaan" data-category="erm_perencanaan" data-category="erm_perencanaan"><i class="fa fa-fw fa-file-text-o push-5-r"></i> PERENCANAAN</a></li>';
    }
    // if (UserAccesForm($user_acces_form, ['1633'])) {
    //     $panel .= '<li class=""><a href="javascript:void(0)" data-category="surat"><i class="fa fa-fw fa-file-text-o push-5-r"></i> PENERBITAN SURAT</a></li>';
    // }
    if (UserAccesForm($user_acces_form, ['1634'])) {
        $panel .= '<li class=""><a href="javascript:void(0)" data-category="rujukan"><i class="fa fa-fw fa-file-text-o push-5-r"></i> RUJUKAN</a></li>';
    }
    if (UserAccesForm($user_acces_form, ['1635'])) {
        $panel .= '<li class=""><a href="'.site_url().'tkonsul/tindakan/'.$pendaftaran_id.'/erm_konsul" data-category="erm_konsul"><i class="fa fa-fw fa-file-text-o push-5-r"></i> KONSULTASI</a></li>';
    }
    $panel .= '		</ul>
				</div>
	';

    return $panel;
}
function get_ttv_ranap_sql_pendaftaran($pendaftaran_id){
    $q="
        SELECT H.tanggal_input,H.pendaftaran_id_ranap,
		H.nadi as header_nadi
		,mnadi.warna as warna_nadi
		,H.nafas as  header_nafas,mnafas.warna as warna_nafas
		,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
		,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
		,H.suhu as header_suhu,msuhu.warna as warna_suhu
		,H.tinggi_badan as header_tinggi_badan
		,H.berat_badan as header_berat_badan
		,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
		,H.spo2 as header_spo2,mspo2.warna as warna_spo2,H.tingkat_kesadaran as header_tingkat_kesadaran
		FROM (
			SELECT tanggal_input,pendaftaran_id_ranap, tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan FROM tranap_ttv WHERE pendaftaran_id_ranap='$pendaftaran_id' AND status_ttv='2'
			UNION ALL								
			SELECT tanggal_input,pendaftaran_id_ranap, 0  as tingkat_kesadaran,nadi,nafas,'' as spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan FROM trawatinap_fisio WHERE pendaftaran_id_ranap='$pendaftaran_id' AND status_assemen='2'
		) H
		LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
		LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
		LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
		LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
		LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
		LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
		LEFT JOIN mspo2 ON H.spo2 BETWEEN mspo2.spo2_1 AND mspo2.spo2_2 AND mspo2.staktif='1'
        
    ";
    return $q;
}
function get_ttv_ranap_sql_all(){
    $q="
        SELECT H.tanggal_input,H.pendaftaran_id_ranap,
		H.nadi as header_nadi
		,mnadi.warna as warna_nadi
		,H.nafas as  header_nafas,mnafas.warna as warna_nafas
		,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
		,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
		,H.suhu as header_suhu,msuhu.warna as warna_suhu
		,H.tinggi_badan as header_tinggi_badan
		,H.berat_badan as header_berat_badan
		,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
		,H.spo2 as header_spo2,mspo2.warna as warna_spo2,H.tingkat_kesadaran as header_tingkat_kesadaran
		FROM (
			SELECT tanggal_input,pendaftaran_id_ranap, tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan FROM tranap_ttv WHERE status_ttv='2'
			UNION ALL								
			SELECT tanggal_input,pendaftaran_id_ranap, 0  as tingkat_kesadaran,nadi,nafas,'' as spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan FROM trawatinap_fisio WHERE status_assemen='2'
		) H
		LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
		LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
		LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
		LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
		LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
		LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
		LEFT JOIN mspo2 ON H.spo2 BETWEEN mspo2.spo2_1 AND mspo2.spo2_2 AND mspo2.staktif='1'
        
    ";
    return $q;
}
function get_alergi_sql(){
    $q="
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi FROM `tpoliklinik_assesmen_igd_alergi`
        WHERE  status_assemen='2'  AND staktif='1'
        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi FROM `tpoliklinik_assesmen_alergi`
        WHERE status_assemen='2'  AND staktif='1'
        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi FROM `tranap_assesmen_alergi`
        WHERE status_assemen IN (1,2)  AND staktif='1'
        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi FROM `tranap_gizi_lanjutan_alergi`
        WHERE status_assemen='2'  AND staktif='1'
        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi FROM `tranap_intra_hospital_alergi`
        WHERE status_assemen='2'  AND staktif='1'
        UNION ALL
        SELECT M.id,M.idpasien,1 as input_jenis_alergi,M.nama_obat as input_detail_alergi,M.bentuk_reaksi as input_reaksi_alergi
        FROM `tranap_rekon_obat_alergi` M
        INNER JOIN tranap_rekon_obat H ON H.assesmen_id=M.assesmen_id
        WHERE  status='1' AND H.status_assemen='2'
        
    ";
    return $q;
}
function get_alergi_sql_pasien($idpasien){
    $q="
       SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,created_by,created_date FROM `tpoliklinik_assesmen_igd_alergi`
        WHERE  status_assemen='2' AND idpasien='$idpasien' AND staktif='1'

        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,created_by,created_date FROM `tpoliklinik_assesmen_alergi`
        WHERE  status_assemen='2' AND idpasien='$idpasien' AND staktif='1'
        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,created_by,created_date FROM `tranap_assesmen_alergi`
        WHERE  status_assemen IN (1,2) AND idpasien='$idpasien' AND staktif='1'
        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,created_by,created_date FROM `tranap_gizi_lanjutan_alergi`
        WHERE  status_assemen='2' AND idpasien='$idpasien' AND staktif='1'
        UNION ALL
        SELECT id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,created_by,created_date FROM `tranap_intra_hospital_alergi`
        WHERE  status_assemen='2' AND idpasien='$idpasien' AND staktif='1' 
        UNION ALL
        SELECT M.id,M.idpasien,1 as input_jenis_alergi,M.nama_obat as input_detail_alergi,M.bentuk_reaksi as input_reaksi_alergi,M.created_ppa as created_by,M.created_date 
        FROM `tranap_rekon_obat_alergi` M
        INNER JOIN tranap_rekon_obat H ON H.assesmen_id=M.assesmen_id
        WHERE  status='1' AND M.idpasien='$idpasien' AND H.status_assemen='2' 
        UNION ALL
        SELECT M.assesmen_id,M.idpasien,1 as input_jenis_alergi,M.alergi as input_detail_alergi,'' as input_reaksi_alergi,M.created_ppa as created_by,M.created_date 
        FROM `tpoliklinik_e_resep` M
        WHERE  M.idpasien='$idpasien' AND M.status_assemen='2'  AND M.st_alergi='1'

    ";
    return $q;
}
function get_tindakan_persetujuan(){
    $q="
       SELECT *FROM (
			SELECT H.id,H.deskripsi,H.idrawatinap,CONCAT('ACC TINDAKAN <br>',H.nama_tindakan)  AS nama_tindakan_setuju
			,H.setuju_date 
			FROM trawatinap_deposit_head H
			INNER JOIN musers MU ON MU.id=H.setuju_user_id
			WHERE H.st_persetujuan='1' 
			ORDER BY H.deskripsi DESC LIMIT 100000000
		) T GROUP BY T.idrawatinap

    ";
    return $q;
}
function div_panel_kendali_ranap($user_acces_form,$pendaftaran_id, $custom = ''){
	// $data_user=get_acces();
	// $user_acces_form=$data_user['user_acces_form'];
	$panel='
				<div class="btn-group push-5-t">
					<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						'.($custom ? $custom : "<span class='fa fa-caret-down'></span> PANEL KENDALI").'
					</button>
					<ul class="dropdown-menu">';
					
				if (UserAccesForm($user_acces_form,array('1625'))){ 
				$panel .='<li class=""><a href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_rj" data-category="erm_rj"><i class="fa fa-fw fa-folder-open-o push-5-r"></i> ERM RJ</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1626'))){ 
				$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri" data-category="erm_ri"><i class="fa fa-fw fa-file-movie-o push-5-r"></i> ERM RI</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1627'))){ 
				$panel .='<li class=""><a href="javascript:void(0)" data-category="erm_penunjang"><i class="fa fa-fw fa-file-photo-o push-5-r"></i> PENUNJANG</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1628'))){ 
				$panel .='<li class=""><a href="'.site_url().'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/erm_e_resep" data-category="e_resep"><i class="fa fa-fw fa-file-audio-o push-5-r"></i> E-RESEP</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1629'))){ 
				$panel .='<li class=""><a href="'.site_url().'term_laboratorium_umum/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_lab/laboratorium_permintaan" data-category="erm_laboratorium"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> LABORATORIUM</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1629'))){ 
				$panel .='<li class=""><a href="'.site_url().'term_laboratorium_patologi_anatomi/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_lab/patologi_anatomi_permintaan" data-category="erm_laboratorium"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> PATALOGI ANATOMI</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1629'))){ 
				$panel .='<li class=""><a href="'.site_url().'term_laboratorium_bank_darah/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_lab/bank_darah_permintaan" data-category="erm_laboratorium"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> BANK DARAH</a></li>';
				}
				if (UserAccesForm($user_acces_form, ['1629'])) {
                    $panel .= '<li class=""><a href="'.site_url().'term_radiologi_xray/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_rad/xray_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> X-RAY</a></li>';
                }
                if (UserAccesForm($user_acces_form, ['1629'])) {
                    $panel .= '<li class=""><a href="'.site_url().'term_radiologi_usg/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_rad/usg_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> USG</a></li>';
                }
                if (UserAccesForm($user_acces_form, ['1629'])) {
                    $panel .= '<li class=""><a href="'.site_url().'term_radiologi_ctscan/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_rad/ctscan_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> CT-SCAN</a></li>';
                }
                if (UserAccesForm($user_acces_form, ['1629'])) {
                    $panel .= '<li class=""><a href="'.site_url().'term_radiologi_mri/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_rad/mri_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> MRI</a></li>';
                }
                if (UserAccesForm($user_acces_form, ['1629'])) {
                    $panel .= '<li class=""><a href="'.site_url().'term_radiologi_lainnya/tindakan/rawat_inap/'.$pendaftaran_id.'/erm_rad/lainnya_permintaan" data-category="erm_radiologi"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> RADIOLOGI LAIN-LAIN</a></li>';
                }
				if (UserAccesForm($user_acces_form,array('1631'))){ 
				$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/erm_trx" data-category="erm_trx"><i class="fa fa-fw fa-file-text-o push-5-r"></i> TRANSAKSI</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1632'))){ 
				$panel .='<li class=""><a href="javascript:void(0)" data-category="rencana"><i class="fa fa-fw fa-file-text-o push-5-r"></i> PERENCANAAN</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1633'))){ 
				$panel .='<li class=""><a href="javascript:void(0)" data-category="surat"><i class="fa fa-fw fa-file-text-o push-5-r"></i> PENERBITAN SURAT</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1634'))){ 
				$panel .='<li class=""><a href="javascript:void(0)" data-category="rujukan"><i class="fa fa-fw fa-file-text-o push-5-r"></i> RUJUKAN</a></li>';
				}
				if (UserAccesForm($user_acces_form,array('1635'))){ 
				$panel .='<li class=""><a href="'.site_url().'tkonsul/tindakan/'.$pendaftaran_id.'/erm_konsul" data-category="konsul"><i class="fa fa-fw fa-file-text-o push-5-r"></i> KONSULTASI</a></li>';
				}
	$panel .='		</ul>
				</div>
	';
	return $panel;
}
function panel_persetujuan_pulang($status_proses_card_pass,$nopendaftaran){
	$label='';
	if ($status_proses_card_pass=='1'){
		$label='
				<div class="text-danger font-s13 push-5-t text-bold">
				<a href="'.base_url().'assets/upload/kwitansi/CP_'.$nopendaftaran.'.pdf" target="_blank" class="btn btn-sm btn-warning">PERSETUJUAN PULANG</a>
				</div>
			  ';
	}
	
	return $label;
}
function div_panel_kendali_ri($user_acces_form,$pendaftaran_id, $custom = ''){
	// $data_user=get_acces();
	// $user_acces_form=$data_user['user_acces_form'];
	$CI =& get_instance();
	$q="
		SELECT perencanaan_id,idpoliklinik,idpasien FROM `trawatinap_pendaftaran` WHERE id='$pendaftaran_id'
	";

    $perencanaan_id=$CI->db->query($q)->row('perencanaan_id');
    $idpoliklinik=$CI->db->query($q)->row('idpoliklinik');
    $idpasien=$CI->db->query($q)->row('idpasien');
	$panel='
				<div class="btn-group push-5-t">
					<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						'.($custom ? $custom : "<span class='fa fa-caret-down'></span> PANEL KENDALI").'
					</button>
					<ul class="dropdown-menu">';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_ttv_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Input TTV RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/erm_e_resep/input_eresep_ri" data-category="erm_e_resep"><i class="fa fa-caret-right push-5-r"></i> E-Resep</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_asmed_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Medis RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_assesmen_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Keperawatan RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_risiko_jatuh" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Risiko Jatuh</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_dpjp/input_dpjp" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Pencatatan DPJP</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_dpjp/input_pindah_dpjp" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Pindah DPJP</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_site/input_lokasi_operasi" data-category="erm_site"><i class="fa fa-caret-right push-5-r"></i> Penandaan Lokasi Operasi</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_sga" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Penilaian SGA</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_gizi_anak" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Yorkhill Malnutrition Score</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_gizi_lanjutan" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Gizi Lanjutan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_discarge" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Discarge Planing</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_rekon/input_rekon_obat" data-category="erm_rekon"><i class="fa fa-caret-right push-5-r"></i> Rekonsiliasi Obat</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_tf/input_intra_hospital" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Transfer Intra Hospital</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_tf/input_tf_hospital" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Transfer Keluar RS</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_implementasi_kep" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Implementasi Keperawatan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_timbang" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Timbang Terima</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_per/input_tindakan_anestesi" data-category="erm_per"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Tindakan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_per/input_risiko_tinggi" data-category="erm_per"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Risiko Tinggi</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_bmhp_ri" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Input BMHP RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_tf/input_pindah_bed" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Pindah Ruangan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_layanan_ri" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Input Tindakan Pelayanan RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_visit" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Input Visite Dokter</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_layanan_fisio_ri" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Tindakan Fisioterapi RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$idpoliklinik.'/0/erm_trx/input_layanan_fisio" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Tindakan Fisioterapi RJ</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_ri/input_cppt_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> CPPT RI</a></li>';
						if ($perencanaan_id){
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_ranap/cetak_rencana_ranap/'.$perencanaan_id.'" target="_blank"><i class="fa fa-caret-right push-5-r"></i> Perencanaan Rawat Inap</a></li>';
							
						}
						// $panel .='<li class=""><a href="#" onclick="show_modal_pendaftaran('.$idpasien.','.$pendaftaran_id.','.$idpoliklinik.')"><i class="fa fa-caret-right push-5-r"></i> Ganti Kunjungan RJ</a></li>';
                        
	$panel .='		</ul>
				</div>
	';
	return $panel;
}
function div_panel_kendali_ri_admin($user_acces_form,$pendaftaran_id, $custom = ''){
	// $data_user=get_acces();
	// $user_acces_form=$data_user['user_acces_form'];
	$CI =& get_instance();
	$q="
		SELECT perencanaan_id,idpoliklinik,idpasien FROM `trawatinap_pendaftaran` WHERE id='$pendaftaran_id'
	";

    $perencanaan_id=$CI->db->query($q)->row('perencanaan_id');
    $idpoliklinik=$CI->db->query($q)->row('idpoliklinik');
    $idpasien=$CI->db->query($q)->row('idpasien');
	$panel='
				<div class="btn-group push-5-t">
					<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						'.($custom ? $custom : "<span class='fa fa-caret-down'></span> PANEL KENDALI").'
					</button>
					<ul class="dropdown-menu">';
						$panel .='<li class=""><a href="#" onclick="show_modal_pendaftaran('.$idpasien.','.$pendaftaran_id.','.$idpoliklinik.')"><i class="fa fa-caret-right push-5-r"></i> Ganti Kunjungan RJ</a></li>';
						$panel .='<li class=""><a href="#" onclick="show_modal_kasir('.$idpasien.')"><i class="fa fa-caret-right push-5-r"></i> Data Kasir</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_ttv_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Input TTV RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/erm_e_resep/input_eresep_ri" data-category="erm_e_resep"><i class="fa fa-caret-right push-5-r"></i> E-Resep</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_asmed_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Medis RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_assesmen_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Keperawatan RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_risiko_jatuh" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Risiko Jatuh</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_dpjp/input_dpjp" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Pencatatan DPJP</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_dpjp/input_pindah_dpjp" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Pindah DPJP</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_site/input_lokasi_operasi" data-category="erm_site"><i class="fa fa-caret-right push-5-r"></i> Penandaan Lokasi Operasi</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_sga" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Penilaian SGA</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_gizi_anak" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Yorkhill Malnutrition Score</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_gizi_lanjutan" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Assesmen Gizi Lanjutan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_discarge" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Discarge Planing</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_rekon/input_rekon_obat" data-category="erm_rekon"><i class="fa fa-caret-right push-5-r"></i> Rekonsiliasi Obat</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_tf/input_intra_hospital" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Transfer Intra Hospital</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_tf/input_tf_hospital" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Transfer Keluar RS</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_implementasi_kep" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Implementasi Keperawatan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_ri/input_timbang" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> Timbang Terima</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_per/input_tindakan_anestesi" data-category="erm_per"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Tindakan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_per/input_risiko_tinggi" data-category="erm_per"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Risiko Tinggi</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_bmhp_ri" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Input BMHP RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/erm_tf/input_pindah_bed" data-category="erm_tf"><i class="fa fa-caret-right push-5-r"></i> Pindah Ruangan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_layanan_ri" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Input Tindakan Pelayanan RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_visit" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Input Visite Dokter</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_layanan_fisio_ri" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Tindakan Fisioterapi RI</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$idpoliklinik.'/0/erm_trx/input_layanan_fisio" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Tindakan Fisioterapi RJ</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_ri/input_cppt_ri" data-category="erm_ri"><i class="fa fa-caret-right push-5-r"></i> CPPT RI</a></li>';
						if ($perencanaan_id){
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_ranap/cetak_rencana_ranap/'.$perencanaan_id.'" target="_blank"><i class="fa fa-caret-right push-5-r"></i> Perencanaan Rawat Inap</a></li>';
							
						}
						
                        
	$panel .='		</ul>
				</div>
	';
	return $panel;
}
function div_panel_kendali_rencana_bedah($user_acces_form,$row, $custom = ''){
	// $data_user=get_acces();
	// $user_acces_form=$data_user['user_acces_form'];
	
	$panel='
				<div class="btn-group push-5-t">
					<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						'.($custom ? $custom : "<span class='fa fa-caret-down'></span> PANEL KENDALI").'
					</button>
					<ul class="dropdown-menu">';
						if ($row->id==''){
							if (UserAccesForm($user_acces_form,array('2296'))){
								$panel .='<li class=""><a href="javascript:void(0)" onclick="insert_jadwal_rencana('.$row->assesmen_id.')"><i class="fa fa-trash text-danger push-5-r"></i> Create Jadwal</a></li>';
							}
						}
						if ($row->id){
							if (UserAccesForm($user_acces_form,array('2294'))){
								$panel .='<li class=""><a href="javascript:void(0)" onclick="batalkan_jadwal('.$row->id.')"><i class="fa fa-trash text-danger push-5-r"></i> Batalkan Jadwal</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2295'))){
								$panel .='<li class=""><a href="javascript:void(0)" onclick="gabung_proses('.$row->id.')" title="Gabungkan Jadwal"><i class="fa fa fa-external-link-square text-primary push-5-r"></i> Gabung Jadwal</a></li>';
							}
						}
						
                        
	$panel .='		</ul>
				</div>
	';
	return $panel;
}
function div_panel_kendali_jadwal_bedah($user_acces_form,$row, $custom = ''){
	// $data_user=get_acces();
	// $user_acces_form=$data_user['user_acces_form'];
	
	$panel='
				<div class="btn-group push-5-t">
					<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						'.($custom ? $custom : "<span class='fa fa-caret-down'></span> PANEL KENDALI").'
					</button>
					<ul class="dropdown-menu">';
						if ($row->statussetuju=='1' && $row->statusdatang=='1'){
							if (UserAccesForm($user_acces_form,array('2297'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_trx" ><i class="fa fa-caret-right push-5-r"></i> Transaksi Pembedahan</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2298'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_sewa_ruangan" ><i class="fa fa-caret-right push-5-r"></i> Sewa Ruangan</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2299'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_full_care" ><i class="fa fa-caret-right push-5-r"></i> Full Care OK</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2300'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_do" ><i class="fa fa-caret-right push-5-r"></i> Dokter Operator</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2301'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_da" ><i class="fa fa-caret-right push-5-r"></i> Dokter Anestesi</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2302'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_daa" ><i class="fa fa-caret-right push-5-r"></i> Asisten Anestesi</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2303'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_dao" ><i class="fa fa-caret-right push-5-r"></i> Asisten Operator</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2304'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_narcose" ><i class="fa fa-caret-right push-5-r"></i> Narcose</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2305'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_obat" ><i class="fa fa-caret-right push-5-r"></i> Obat</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2306'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_alkes" ><i class="fa fa-caret-right push-5-r"></i> Alkes</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2307'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_implan" ><i class="fa fa-caret-right push-5-r"></i> Implant</a></li>';
							}
							if (UserAccesForm($user_acces_form,array('2308'))){
								$panel .='<li class=""><a href="'.site_url().'tkamar_bedah/tindakan/'.$row->id.'/erm_bedah/input_bedah_sewa_alat" ><i class="fa fa-caret-right push-5-r"></i> Sewa Alat</a></li>';
							}
						}
						if (UserAccesForm($user_acces_form,array('2294'))){
							$panel .='<li class=""><a href="javascript:void(0)" onclick="batalkan_jadwal('.$row->id.')"><i class="fa fa-trash text-danger push-5-r"></i> Batalkan Jadwal</a></li>';
						}
						if (UserAccesForm($user_acces_form,array('2295'))){
							$panel .='<li class=""><a href="javascript:void(0)" onclick="gabung_proses('.$row->id.')" title="Gabungkan Jadwal"><i class="fa fa fa-external-link-square text-primary push-5-r"></i> Gabung Jadwal</a></li>';
						}
                        
	$panel .='		</ul>
				</div>
	';
	return $panel;
}
function div_panel_kendali_RM($user_acces_form,$pendaftaran_id, $custom = ''){
	// $data_user=get_acces();
	// $user_acces_form=$data_user['user_acces_form'];
	$panel='
				<div class="btn-group push-5-t">
					<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
						'.($custom ? $custom : "<span class='fa fa-caret-down'></span> PANEL KENDALI").'
					</button>
					<ul class="dropdown-menu">';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/1/erm_trx/input_layanan_fisio_ri" data-category="erm_trx"><i class="fa fa-caret-right push-5-r"></i> Tindakan Fisioterapi</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$pendaftaran_id.'/1/erm_rm/input_order" data-category="erm_rm"><i class="fa fa-caret-right push-5-r"></i> Input Order</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$pendaftaran_id.'/1/erm_rm/input_perencanaan" data-category="erm_rm"><i class="fa fa-caret-right push-5-r"></i> Input Perencanaan</a></li>';
						$panel .='<li class=""><a href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$pendaftaran_id.'/1/erm_rm/pilih_program" data-category="erm_rm"><i class="fa fa-caret-right push-5-r"></i> Pilih Program</a></li>';
                        
	$panel .='		</ul>
				</div>
	';
	return $panel;
}

function getLevelZero($input) {
    $parts = explode('.', $input);
    $levelZero = $parts[0];
    return $levelZero;
}

function generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, $type) {
    $CI =& get_instance();

    $tables = [
        'umum' => 'term_laboratorium_umum',
        'patologi_anatomi' => 'term_laboratorium_patologi_anatomi',
        'bank_darah' => 'term_laboratorium_bank_darah',
        'xray' => 'term_radiologi_xray',
        'usg' => 'term_radiologi_usg',
        'ctscan' => 'term_radiologi_ctscan',
        'mri' => 'term_radiologi_mri',
        'lainnya' => 'term_radiologi_lainnya'
    ];

    $titles = [
        'umum' => 'Laboratorium',
        'patologi_anatomi' => 'Patologi Anatomi',
        'bank_darah' => 'Bank Darah',
        'xray' => 'X-Ray',
        'usg' => 'USG',
        'ctscan' => 'CT-Scan',
        'mri' => 'MRI',
        'lainnya' => 'Lainnya'
    ];

    $url_paths = [
        'umum' => 'term_laboratorium_umum_hasil',
        'patologi_anatomi' => 'term_laboratorium_patologi_anatomi_hasil',
        'bank_darah' => 'term_laboratorium_bank_darah_hasil',
        'xray' => 'term_radiologi_xray_hasil',
        'usg' => 'term_radiologi_usg_hasil',
        'ctscan' => 'term_radiologi_ctscan_hasil',
        'mri' => 'term_radiologi_mri_hasil',
        'lainnya' => 'term_radiologi_lainnya_hasil'
    ];

    if (!array_key_exists($type, $tables)) {
        return '';
    }

    $table = $tables[$type];
    $status_pemeriksaan = ($type === 'umum' || $type === 'patologi_anatomi' || $type === 'bank_darah') ? '>= 6' : '>= 5';
    $query = "SELECT id, nomor_permintaan FROM $table WHERE pendaftaran_id = ? AND asal_rujukan = ? AND status_pemeriksaan $status_pemeriksaan";

    $data = $CI->db->query($query, [$pendaftaran_id, $asal_rujukan])->result();
    
    if (empty($data)) {
        return '';
    }

    $title = $titles[$type];
    $url_path = $url_paths[$type];
    $content = '<div class="btn-group dropup print-order">';
    $content .= '<button class="btn btn-success btn-xs btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
    $content .= '<i class="fa fa-list"></i> ' . $title;
    $content .= '</button>';
    $content .= '<ul class="dropdown-menu pull-left">';

    foreach ($data as $row) {
        $content .= '<li><a href="' . base_url() . $url_path . '/proses/' . $row->id . '/lihat_hasil_pemeriksaan" target="_blank">' . $row->nomor_permintaan . '</a></li>';
    }

    $content .= '</ul></div>';

    return $content;
}

function generate_erm_pemeriksaan_dropdown($pendaftaran_id, $asal_rujukan) {
    $contentHasilLaboratoriumUmum = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'umum');
    $contentHasilLaboratoriumPatologiAnatomi = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'patologi_anatomi');
    $contentHasilLaboratoriumBankDarah = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'bank_darah');
    $contentHasilRadiologiXRay = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'xray');
    $contentHasilRadiologiUSG = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'usg');
    $contentHasilRadiologiCTScan = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'ctscan');
    $contentHasilRadiologiMRI = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'mri');
    $contentHasilRadiologiLainnya = generate_erm_pemeriksaan($pendaftaran_id, $asal_rujukan, 'lainnya');

    return $contentHasilLaboratoriumUmum . ' ' .
        $contentHasilLaboratoriumPatologiAnatomi . ' ' .
        $contentHasilLaboratoriumBankDarah . ' ' .
        $contentHasilRadiologiXRay . ' ' .
        $contentHasilRadiologiUSG . ' ' .
        $contentHasilRadiologiCTScan . ' ' .
        $contentHasilRadiologiMRI . ' ' .
        $contentHasilRadiologiLainnya;
}
function opsi_nilai_pulang_pdf($arr_isi,$param_id,$jenis_id){
		$data = array_filter($arr_isi, function($var) use ($param_id,$jenis_id) { 
			return ($var['param_id']==$param_id && $var['jenis']==$jenis_id);
		});	
		// print_r($arr_jadwal);exit;
		$data = reset($data);
		$opsi_data='';
		if ($data){
			
			$opsi_data=$data['jawaban_nama'];
			
		}else{
			$opsi_data='';
		}
		return $opsi_data;
	 
  }
  function opsi_nilai_pulang_his($arr_isi,$param_id,$jenis_id,$arr_isi_last){
		$data = array_filter($arr_isi, function($var) use ($param_id,$jenis_id) { 
			return ($var['param_id']==$param_id && $var['jenis']==$jenis_id);
		});	
		// print_r($arr_jadwal);exit;
		$data = reset($data);
		$data_2 = array_filter($arr_isi_last, function($var) use ($param_id,$jenis_id) { 
			return ($var['param_id']==$param_id && $var['jenis']==$jenis_id);
		});	
		// print_r($arr_jadwal);exit;
		$data_2 = reset($data_2);
		$opsi_data='';
		if ($data){
			$nama_class='';
			if ($data['jawaban_nama'] != $data_2['jawaban_nama']){
				$nama_class='text-danger';
			}
			$opsi_data='<span class="'.$nama_class.'"><strong>'.$data['jawaban_nama'].'</strong></span>';
			
		}else{
			$opsi_data='';
		}
		return $opsi_data;
	 
  }
  function increment_single_string($last) {
		 $string = $last;
		 $string++;		
		 return $string;
	}
	function increment_banyak_string($last,$bil) {
		 $string = $last;
		 for($i=0;$i<$bil;$i++){
			 $string++;		
		 }
		 
		 return $string;
	}
	function kurangin_abjad($last){
		$string = $last;
		 $string=$string+1;
		
		 return $string;
	}
	function toAlpha($number){
		$number--;
		$number=$number+1;
		$alphabet=range('A','Z');
		$alpha="";
		$count = count($alphabet);
		if($number <= $count)
			return $alphabet[$number-1];
		while($number > 0){
			$modulo     = ($number - 1) % $count;
			$alpha      = $alphabet[$modulo].$alpha;
			$number     = floor((($number - $modulo) / $count));
		}
		return $alpha;
	}
