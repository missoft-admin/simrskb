<div class="modal fade in black-overlay" id="modal_upload" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title"><i class="fa fa-file-image-o"></i> Upload Document Pendukung</h3>
				</div>
				<div class="block-content">
					<div class="col-md-12" style="margin-top: 10px;">
						<div class="block" id="box_file2">					
							 <table class="table table-bordered" id="list_file">
								<thead>
									<tr>
										<th width="50%" class="text-center">File</th>
										<th width="15%" class="text-center">Size</th>
										<th width="35%" class="text-center">User</th>
										<th width="35%" class="text-center">X</th>
										
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
						<div class="block">
							<form action="{base_url}mrka_bendahara/upload" enctype="multipart/form-data" class="dropzone" id="image-upload">
								<input type="hidden" class="form-control" name="idrka" id="idrka2" value="" readonly>
								<div>
								  <h5>Bukti Upload</h5>
								</div>
							</form>
						</div>
						
					</div> 
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>

<script type="text/javascript">
	var myDropzone ;
	
	$(document).ready(function(){
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
	})	
	function upload_image($id){
		$("#idrka2").val($id);
		$("#modal_upload").modal('show');
		refresh_image();
	}
	
	$(document).on("click",".hapus_file",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
	
	function refresh_image(){
		var id=$("#idrka2").val();
		$('#list_file tbody').empty();
		// alert(id);
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				if (data.detail!=''){
					$('#list_file tbody').empty();
					$("#box_file2").attr("hidden",false);
					$("#list_file tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	
</script>