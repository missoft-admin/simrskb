<div class="modal fade in black-overlay" id="modal_vendor" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:40%">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Rekening Vendor</h3>
				</div>
				<div class="block-content">
					<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_list">
								<a href="#btabs-animated-slideleft-list"><i class="fa fa-list-ol"></i> List Rekening</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-add"><i class="fa fa-pencil"></i> Add / Edit Rekening</a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-list">
								<h5 class="font-w300 push-15" id="lbl_list_rekening">Dafar Rekening </h5>
								<table id="tabel_list_rekening" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 15%;">id</th>
											<th style="width: 15%;">id</th>
											<th style="width: 15%;">Bank</th>
											<th style="width: 15%;">No Rekening</th>
											<th style="width: 5%;">Atas Nama</th>
											<th style="width: 5%;">Cabang</th>
											<th style="width: 8%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>									
								</table>
							</div>
							<div class="tab-pane fade fade-left" id="btabs-animated-slideleft-add">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Vendor</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="edit_vendor" name="edit_vendor" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="idrekening" name="idrekening" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Bank</label>
											<div class="col-md-10">
												<select name="edit_bankid" id="edit_bankid" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="#" selected>-Bank-</option>
													<? foreach(get_all('ref_bank') as $row){ ?>										
														<option value="<?=$row->id?>"><?=$row->bank?></option>
													<?}?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_norekening">No. Rekening</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="edit_norekening" name="edit_norekening" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_atasnama">Atas Nama</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="edit_atasnama" name="edit_atasnama" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_cabang">Cabang</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="edit_cabang" name="edit_cabang" value=""/>	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase" type="button" id="btn_save_rekening" name="btn_save_rekening" ><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning text-uppercase" type="button" id="btn_clear_rekening" name="btn_clear_rekening" ><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on("click",".edit_rekening",function(){
		var table = $('#tabel_list_rekening').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		$("#idrekening").val(table.cell(tr,0).data());
		$("#edit_norekening").val(table.cell(tr,3).data());
		$("#edit_atasnama").val(table.cell(tr,4).data());
		$("#edit_cabang").val(table.cell(tr,5).data());
		$("#edit_bankid").val(table.cell(tr,1).data()).trigger('change');
		$('#tab_add > a').tab('show');
		
    });
	
	$(document).on("click","#btn_add_rekening",function(){
		var idvendor=$("#idvendor").val();
		if (idvendor=='#' || idvendor=='0'){
			sweetAlert("Maaf...", "Vendor Belum dipilih!", "error");
			return false;
		}
		$('#modal_vendor').modal('show');
		var nama_vendor=$("#idvendor option:selected").text();	
		$('#lbl_list_rekening').text('List Rekening : '+nama_vendor);
		$('#edit_vendor').val(nama_vendor);
		clear_rekening();
		load_list_rekening();
	});
	$(document).on("click","#btn_clear_rekening",function(){		
		clear_rekening();
	});
	$(document).on("click",".pilih_rekening",function(){
		var table = $('#tabel_list_rekening').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// alert(table.cell(tr,1).data());
		
		$("#rekening_id").val(table.cell(tr,0).data());
		$("#norek").val(table.cell(tr,3).data());
		$("#bank").val(table.cell(tr,2).data());
		$("#atasnama").val(table.cell(tr,4).data());
		// $("#keterangan_bank").val(table.cell(tr,5).data());
		$('#modal_vendor').modal('hide');
		
		var idvendor=$("#idvendor").val();		
		var rekening_id=$("#rekening_id").val();		
		$.ajax({
			url: '{site_url}mrka_pengajuan/update_rekening',
			dataType: "JSON",
			method: "POST",
			data : {idvendor:idvendor,rekening_id:rekening_id},
			success: function(data) {
				console.log(data);
				
			}
		});
			
    });
	
	function validate_detail_rekening() {
		// alert('sini');
		if ($("#edit_bankid").val() == "#") {
			sweetAlert("Maaf...", "Bank Harus Diisi!", "error");
			$("#edit_bankid").focus();
			return false;
		}
		if ($("#edit_norekening").val() == "") {
			sweetAlert("Maaf...", "No Rekening Harus Diisi!", "error");
			$("#edit_norekening").focus();
			return false;
		}
		if ($("#edit_atasnama").val() == "") {
			sweetAlert("Maaf...", "Atas nama Harus Diisi!", "error");
			$("#edit_atasnama").focus();
			return false;
		}
		
		if ($("#edit_cabang").val() == "") {
			sweetAlert("Maaf...", "Cabang Harus Diisi!", "error");
			$("#edit_cabang").focus();
			return false;
		}
		
		
		return true;
	}
	function clear_rekening(){
		$("#idrekening").val('');
		$("#edit_norekening").val('');
		$("#edit_atasnama").val('');
		$("#edit_cabang").val('');
		$("#edit_bankid").val('#').trigger('change');
	}
	$(document).on("click","#btn_save_rekening",function(){
		if (!validate_detail_rekening()) return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Simpan Rekening vendor ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			save_bank();
		});

	});
	function save_bank(){
		var tipe_distributor=3;		
		var idrekening=$("#idrekening").val();		
		var iddistributor=$("#idvendor").val();		
		var bankid=$("#edit_bankid").val();		
		var cabang=$("#edit_cabang").val();		
		var norekening=$("#edit_norekening").val();		
		var atasnama=$("#edit_atasnama").val();		
		// table = $('#index_rekening').DataTable()	
		$.ajax({
			url: '{site_url}mrka_pengajuan/save_bank',
			type: 'POST',
			data: {
				iddistributor: iddistributor,
				bankid: bankid,
				cabang: cabang,
				norekening: norekening,
				atasnama: atasnama,
				tipe_distributor: tipe_distributor,
				idrekening: idrekening,
			},
			complete: function() {
				sweetAlert("Berhasil...", "Data Bank disimpan!", "success");
				$('#tabel_list_rekening').DataTable().ajax.reload( null, false );
				clear_rekening();
				$('#tab_list > a').tab('show');
			}
		});
	
	}
	function load_list_rekening(){
		var idvendor=$("#idvendor").val();
		$('#tabel_list_rekening').DataTable().destroy();
		table=$('#tabel_list_rekening').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mrka_pengajuan/load_list_rekening',
				type: "POST",
				dataType: 'json',
				data : {
					idvendor:idvendor,
				   }
			},
			"columnDefs": [
				{"targets": [0,1], "visible": false },
				 // {  className: "text-right", targets:[3,10] },
				 // {  className: "text-center", targets:[4,5,,6,7,8,9,11,12] },
				 // { "width": "5%", "targets": [3] },
				 // { "width": "8%", "targets": [4,5,6,9] },
				 // { "width": "10%", "targets": [7,8,11] },
				 // { "width": "12%", "targets": [12] },
				 // { "width": "15%", "targets": [13] },

			]
		});
	}
	$(document).on("change", "#idvendor", function() {
		var idvendor=$("#idvendor").val();
		
		
			// var values = $(this).val();
			// var title=jQuery('#namastokies').val();
			$.ajax({
			url: '{site_url}mrka_pengajuan/get_default_rekening',
			dataType: "JSON",
			method: "POST",
			data : {idvendor:idvendor},
			success: function(data) {
				console.log(data);
				if (data){
					$("#rekening_id").val(data.id);
					$("#norek").val(data.norekening);
					$("#bank").val(data.bank);
					$("#atasnama").val(data.atasnama);
				}else{
					$("#rekening_id").val('');
					$("#norek").val('');
					$("#bank").val('');
					$("#atasnama").val('');
				}
				
			}
			});
		
	});
</script>