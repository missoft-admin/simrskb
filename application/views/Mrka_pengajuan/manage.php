<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <button onclick="goBack()" class="btn"><i class="fa fa-reply"></i></button>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_pengajuan/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Pemohon</label>
				<div class="col-md-4">
					<input type="hidden" class="form-control" id="id" name="id" value="{id}" required="" aria-required="true">
					<input type="text" readonly class="form-control" id="nama_pemohon" <?=$disabel?> placeholder="Nama Pemohon" name="nama_pemohon" value="{nama_pemohon}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" name="disabel" value="{disabel}">
					<input type="hidden" class="form-control" id="idrka_kegiatan" name="idrka_kegiatan" value="{idrka_kegiatan}" required="" aria-required="true">
					<input type="hidden" class="form-control" name="bulan" id="bulan" value="<?=$bulan?>" required="" aria-required="true">
					<input type="hidden" class="form-control" name="url_asal" id="url_asal" value="<?=$url_asal?>">
					
				</div>
				<label class="col-md-2 control-label" for="nama">Unit Yang Mengajukan </label>
				<div class="col-md-4">
					<select id="idunit_pengaju" <?=$disabel?>  name="idunit_pengaju" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit_pengaju==$row->idunit?'selected':'')?> <?=$row->akses?>><?=$row->nama.' '.$row->akses?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Pengajuan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_pengajuan" name="tanggal_pengajuan" value="<?=$tanggal_pengajuan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama"></label>
				<label class="col-md-2 control-label" for="nama">Tanggal Dibutuhkan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_dibutuhkan" name="tanggal_dibutuhkan" value="<?=$tanggal_dibutuhkan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Untuk  Unit</label>
				<div class="col-md-4">
					<select id="idunit" name="idunit" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach(get_all('munitpelayanan',array('status'=>1)) as $row){ ?>
							<option value="<?=$row->id?>" <?=($idunit==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Jenis Pengajuan</label>
				<div class="col-md-4">
					<select id="idjenis" name="idjenis"  <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Jenis -</option>
						<? foreach($list_jenis as $row){ ?>
							<option value="<?=$row->id?>" <?=($idjenis==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tipe Pengajuan</label>
				<div class="col-md-4">
					<select id="tipe_rka" disabled name="tipe_rka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="1" <?=$tipe_rka=='1'?'selected':''?>>RKA</option>
						<option value="2" <?=$tipe_rka=='2'?'selected':''?>>NON RKA</option>						
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Nama Kegiatan</label>
				<div class="col-md-4">
					<? if ($tipe_rka=='1'){ ?>
					<div class="input-group date">
						<input type="text" class="form-control" <?=$disabel?> readonly id="nama_kegiatan" <?=$disabel?> placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
						<span class="input-group-btn">
							<button class="btn btn-primary" <?=$disabel?> type="button" id="btn_open_modal_kegiatan" title="Terapkan sama rata"><i class="fa fa-search"></i></button>
						</span>
					</div>
					<?}else{?>
					<input type="text" class="form-control" <?=$disabel?> <?=$disabel?> placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
					<?}?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Vendor</label>
				<div class="col-md-4">
					<div class="input-group">
						<select id="idvendor" name="idvendor" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idvendor=='#'?'selected':'')?>>- Pilih Vendor -</option>
							<option value="0" <?=($idvendor=='0'?'selected':'')?>>Tentukan Vendor Nanti</option>
							<? foreach($list_vendor as $row){ ?>
								<option value="<?=$row->id?>" <?=($idvendor==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" <?=$disabel?> type="button" id="btn_refresh_vendor" title="Refresh"><i class="fa fa-refresh"></i></button>
							<a href="{base_url}mvendor" target="_blank" class="btn btn-primary" <?=$disabel?> type="button" id="btn_add_vendor" title="Tambah Vendor"><i class="fa fa-plus"></i></a>
						</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama">Klasifikasi</label>
				<div class="col-md-4">
					<select id="idklasifikasi" name="idklasifikasi" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($idklasifikasi=='#'?'selected':'')?>>- Pilih Klasifikasi -</option>
						<? foreach($list_klasifikasi as $row){ ?>
							<option value="<?=$row->id?>" <?=($idklasifikasi==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Catatan</label>
				<div class="col-md-10">
						<textarea class="form-control" rows="2" name="catatan" id="catatan"><?=$catatan?></textarea>
						
				</div>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<h5 style="margin-bottom: 10px;">DETAIL PENGAJUAN</h5>
			
			<div class="form-group">
				<table id="tabel_add" class="table table-striped table-bordered" style="margin-bottom: 0;">
					<thead>
						<tr>
							<th style="width: 15%;">Nama Barang</th>
							<th style="width: 15%;">Merk Barang</th>
							<th style="width: 5%;">QTY</th>
							<th style="width: 5%;">Satuan</th>
							<th style="width: 10%;">Harga</th>
							<th style="width: 10%;">Total</th>
							<th style="width: 15%;">Keterangan</th>
							<th style="width: 8%;">Actions</th>
						</tr>
						<tr>							
							<td><input class="form-control input-sm" tabindex="2" type="text" id="nama_barang" /></td>
							<td><input class="form-control input-sm" tabindex="3" type="text" id="merk_barang" /></td>
							<td><input class="form-control input-sm number" tabindex="4" type="text" id="kuantitas" /></td>
							<td><input class="form-control input-sm" tabindex="5" type="text" id="satuan" /></td>
							<td>
								<div class="input-group date">
								<input class="form-control input-sm decimal" readonly tabindex="6" type="text" id="harga_satuan" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_pokok" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_diskon" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_ppn" />
								<input class="form-control input-sm " readonly tabindex="6" type="hidden" id="idklasifikasi_det" />
									<span class="input-group-btn">
										<button class="btn btn-primary btn-sm" <?=$disabel?> type="button" id="btn_open_modal_harga" title="Edit Harga"><i class="fa fa-pencil"></i></button>
									</span>
								</div>
							</td>
							<td><input class="form-control input-sm decimal" readonly  type="text" id="total_harga" /></td>
							<td><input class="form-control input-sm" tabindex="7" type="text" id="keterangan" /></td>
							<td>
								<button type="button" <?=$disabel?> class="btn btn-sm btn-primary kunci" tabindex="8" id="addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
								<button type='button' <?=$disabel?> class='btn btn-sm btn-danger kunci' tabindex="9" id="canceljual" title="Refresh"><i class='fa fa-times'></i></button>
							</td>
						</tr>
					</thead>
					<tbody>
						<? 
						$gt=0;
						$item=0;
						foreach($list_detail as $row){ 
							$gt=$gt + $row->total_harga;
							$item=$item + 1;
						?>
							<tr>
								<td><?=$row->nama_barang?></td>
								<td><?=$row->merk_barang?></td>
								<td><?=$row->kuantitas?></td>
								<td><?=$row->satuan?></td>
								<td><?=number_format($row->harga_satuan).'<br>└─ Hpp. '.number_format($row->harga_pokok).'<br>└─ Disc. '.number_format($row->harga_diskon).'<br>└─ PPN. '.number_format($row->harga_ppn)?></td>
								<td><?=number_format($row->total_harga)?></td>
								<td><?=$row->keterangan?></td>
								<td><? echo "<div class='btn-group'><button type='button' ".$disabel." class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' ".$disabel." class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div>"?></td>
								<td style='display:none'><input type='text' name='xiddet[]' value="<?=$row->id?>"></td>
								<td style='display:none'><input type='text' name='xnama_barang[]' value="<?=$row->nama_barang?>"></td>
								<td style='display:none'><input type='text' name='xmerk_barang[]' value="<?=$row->merk_barang?>"></td>
								<td style='display:none'><input type='text' name='xkuantitas[]' value="<?=$row->kuantitas?>"></td>
								<td style='display:none'><input type='text' name='xsatuan[]' value="<?=$row->satuan?>"></td>
								<td style='display:none'><input type='text' name='xharga_satuan[]' value="<?=$row->harga_satuan?>"></td>
								<td style='display:none'><input type='text' name='xtotal_harga[]' value="<?=$row->total_harga?>"></td>
								<td style='display:none'><input type='text' name='xketerangan[]' value="<?=$row->keterangan?>"></td>
								<td style='display:none'><input type='text' name='xstatus[]' value="<?=$row->status?>"></td>
								<td style='display:none'><input type='text' name='xharga_pokok[]' value="<?=$row->harga_pokok?>"></td>
								<td style='display:none'><input type='text' name='xharga_diskon[]' value="<?=$row->harga_diskon?>"></td>
								<td style='display:none'><input type='text' name='xharga_ppn[]' value="<?=$row->harga_ppn?>"></td>
								<td style='display:none'><input type='text' name='xidklasifikasi_det[]' value="<?=$row->idklasifikasi_det?>"></td>
							</tr>
						<?}?>
					</tbody>
					<tfoot id="foot-total-nonracikan">
						<tr>
							<th colspan="5" class="hidden-phone">

								<span class="pull-right"><b>TOTAL</b></span></th>
							<input type="hidden" id="rowindex" name="rowindex" value="">
							<input type="hidden" id="iddet" name="iddet" value="">
							<th><input class="form-control input-sm number" readonly type="text" id="grand_total" name="grand_total" value="<?=$gt?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_item" name="total_item" value="<?=$item?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_jenis_barang" name="total_jenis_barang" value="<?=$item?>"/></th>
							
						</tr>
					</tfoot>
				</table>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Cara Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="cara_pembayaran" name="cara_pembayaran" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($cara_pembayaran=='#'?'selected':'')?>>- Pilih Cara Pembayaran -</option>
						<option value="0" <?=($cara_pembayaran=='0'?'selected':'')?>>Tentukan Cara Pembayaran Nanti</option>
						<option value="1" <?=($cara_pembayaran=='1'?'selected':'')?>>Satu Kali Bayar</option>	
						<?if (!$idpengajuan_asal){?>
						<option <?=($id==''?'disabled':'')?> value="2" <?=($cara_pembayaran=='2'?'selected':'')?>>Cicilan <?=($id==''?' (dapat diakses jika sudah disimpan)':'')?></option>
						<option <?=($id==''?'disabled':'')?> value="3" <?=($cara_pembayaran=='3'?'selected':'')?>>By Termin <?=($id==''?' (dapat diakses jika sudah disimpan)':'')?></option>
						<?}?>
					</select>						
				</div>				
			</div>
			<div class="form-group" style="display:block" id="div_satu">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Jenis Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="jenis_pembayaran" name="jenis_pembayaran" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($jenis_pembayaran=='#'?'selected':'')?>>- Pilih Jenis Pembayaran -</option>
						<option value="0" <?=($jenis_pembayaran=='0'?'selected':'')?>>Tentukan Jenis Pembayaran Nanti</option>
						<option value="1" <?=($jenis_pembayaran=='1'?'selected':'')?>>Tunai</option>
						<option value="2" <?=($jenis_pembayaran=='2'?'selected':'')?>>Transfer</option>
						<option value="3" <?=($jenis_pembayaran=='3'?'selected':'')?>>Kartu Kredit</option>
						<option value="4" <?=($jenis_pembayaran=='4'?'selected':'')?>>Kontrabon</option>
						
					</select>						
				</div>				
				<div class="col-md-1">
				</div>				
			</div>
			<div id="div_tf">
				<div class="form-group" style="display:block" style="margin-bottom: 10px;">
					<label class="col-md-2 control-label" for="nama">No Rekening</label>
					<div class="col-md-4">
						<div class="input-group">
							<input class="form-control input-sm "  type="hidden" readonly id="rekening_id" name="rekening_id" value="{rekening_id}"/>
							<input class="form-control input-sm " <?=$disabel?> type="text" id="norek" name="norek" value="{norek}"/>	
							<span class="input-group-btn">
								<button class="btn btn-primary btn-sm" <?=$disabel?> type="button" id="btn_add_rekening" title="Rekening Vendor"><i class="fa fa-credit-card"></i></button>
							</span>
						</div>
					</div>
					<label class="col-md-2 control-label" for="nama">Bank</label>
					<div class="col-md-4">
						<input class="form-control input-sm " <?=$disabel?> type="text" id="bank" name="bank" value="{bank}"/>			
								
					</div>				
				</div>
				<div class="form-group" style="display:block" style="margin-bottom: 10px;">
					<label class="col-md-2 control-label" for="nama">Atas Nama</label>
					<div class="col-md-4">
						<input class="form-control input-sm " <?=$disabel?> type="text" id="atasnama" name="atasnama" value="{atasnama}"/>			
					</div>
								
				</div>
				<div class="form-group" style="display:block" style="margin-bottom: 10px;">
					<label class="col-md-2 control-label" for="nama">Keterangan</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="2" <?=$disabel?>  name="keterangan_bank" id="keterangan_bank"><?=$keterangan_bank?></textarea>	
					</div>
								
				</div>
			</div>
			<div id="div_kbo">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Tanggal Kontrabon</label>
					<div class="col-md-4">
						<div class="input-group date">
							<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_kontrabon" name="tanggal_kontrabon" value="<?=($tanggal_kontrabon?HumanDateShort($tanggal_kontrabon):'')?>" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
								
				</div>
				
			</div>
			
			<?$this->load->view('Mrka_pengajuan/div_cicilan')?>
			<?$this->load->view('Mrka_pengajuan/div_termin')?>
			
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<? if ($disabel!='disabled'){ ?>
			<div class="form-group">
				<div class="text-right bg-light lter">
					<button class="btn btn-info" type="submit" id="btn_simpan" name="btn_simpan" value="1">Simpan<img style="display: none;" id="loading-button-ajax" src="http://localhost/simrskb/assets/ajax/img/ajax-loader.gif"></button>
					<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan" value="2">Simpan & Edit<img style="display: none;" id="loading-button-ajax" src="http://localhost/simrskb/assets/ajax/img/ajax-loader.gif"></button>
					<a href="{base_url}mrka_pengajuan" class="btn btn-default" type="button">Kembali Ke Index</a>
				</div>
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_kegiatan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:65%">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Kegiatan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">RKA</label>
								<div class="col-md-8">
									<select name="idrka" id="idrka" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih RKA-</option>
										<? foreach($list_rka as $row){ ?>										
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idprespektif">Prespektif</label>
								<div class="col-md-8">
									<select name="idprespektif" id="idprespektif" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
										<? foreach($list_prespektif as $row){ ?>										
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idprespektif">Program</label>
								<div class="col-md-8">
									<select name="idprogram" id="idprogram" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_rka" name="btn_filter_rka" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="tabel_kegiatan" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th hidden>idrka_kegiatan</th>
								<th hidden>idrka</th>
								<th hidden>idpresprekti</th>
								<th hidden>idprogram</th>
								<th hidden>idkegiatan</th>
								<th>#</th>
								<th>RKA</th>
								<th>Kegiatan</th>
								<th>Program</th>
								<th>Prespektif</th>
								<th align="center">Target</th>
								<th>Nominal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Mrka_pengajuan/modal_harga')?>
<?$this->load->view('Mrka_pengajuan/modal_vendor')?>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		$id=$("#id").val();
		$(".decimal").number(true,0,'.',',');
		$('.number').number(true, 0, '.', ',');
		clear_input();
		show_hide_pembayaran();
		if ($id){
			load_item_barang($id);
			
			setTimeout(function() {
			load_list_cicilan();
			load_item_termin();
			}, 1000);


		}
	})	
	
	function goBack() {
	  window.history.back();
	}
	function show_hide_pembayaran(){
		$('#div_cicilan').hide();
		$('#div_satu').hide();
		$('#div_kbo').hide();
		$('#div_termin').hide();
		$('#div_tf').hide();
		$(':button[type=submit]').removeAttr("disabled");
		if ($("#cara_pembayaran").val()=='1'){//Satu Kali			
			$('#div_satu').show();
			if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
				$('#div_tf').show();
				
			}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
				$('#div_kbo').show();
			}
		}else if ($("#cara_pembayaran").val()=='2'){//Cicilan
			$('#div_tf').show();
			$('#div_cicilan').show();
			
			// hitung_total_cicilan();
		}else if ($("#cara_pembayaran").val()=='3'){//Fix
			$('#div_tf').show();
			$('#div_termin').show();
			hitung_total_termin();
		}
		
	}
	function clear_input(){
		$("#iddet").val('')
		$("#nama_barang").val('')
		$("#merk_barang").val('')
		$("#kuantitas").val('0')
		$("#satuan").val('')
		$("#harga_satuan").val('0')
		$("#harga_pokok").val('0')
		$("#harga_diskon").val('0')
		$("#harga_ppn").val('0')
		$("#total_harga").val('')
		$("#keterangan").val('')
		$("#idklasifikasi_det").val($("#idklasifikasi").val());
		$("#rowindex").val('')
		
		if ($("#disabel").val()==''){
			$(".edit").attr('disabled', false);
			$(".hapus").attr('disabled', false);
		}
		// hitung_total();
	}
	$(document).on("change","#jenis_pembayaran,#cara_pembayaran",function(){
		// console.log('sini');
		show_hide_pembayaran();
		// alert($("#jenis_pembayaran").val());
		if ($("#id").val()!='' && $("#cara_pembayaran").val()=='2'){
			load_item_barang_select2();
			load_item_dp();
		}
	});
	$(document).on("change","#idklasifikasi",function(){
		// console.log('sini');
		show_hide_pembayaran();
		
	});
	$(document).on("keyup","#kuantitas,#harga_satuan",function(){
		// console.log('sini');
		perkalian();
		
	});
	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	function perkalian(){
		var total=0;
		total=parseFloat($("#kuantitas").val()) * parseFloat($("#harga_satuan").val());
		$("#total_harga").val(total)
	}
	
	function validate_final() {
		var rowCount = $('#tabel_add tbody tr').length;
		if (rowCount < 1){
			sweetAlert("Maaf...", "Tidak ada barang yang diajukan!", "error");
			return false;
		}
		if ($("#idunit").val() == "#") {
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit").focus();
			return false;
		}
		if ($("#idunit_pengaju").val() == "#") {
			sweetAlert("Maaf...", "Unit Pengaju Harus Diisi!", "error");
			$("#idunit_pengaju").focus();
			return false;
		}
		if ($("#idjenis").val() == "#") {
			sweetAlert("Maaf...", "Jenis Pengajuan Harus Diisi!", "error");
			$("#idjenis").focus();
			return false;
		}
		if ($("#nama_kegiatan").val() == "") {
			sweetAlert("Maaf...", "Kegiatan Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		if ($("#idvendor").val() == "#") {
			sweetAlert("Maaf...", "Teruntukan Vendor!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($("#idklasifikasi").val() == "#") {
			sweetAlert("Maaf...", "Teruntukan Klasifikasi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($("#cara_pembayaran").val() == "#") {
			sweetAlert("Maaf...", "Cara Pembayaran Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		
		// if ($("#cara_pembayaran").val() != "#" && $("#cara_pembayaran").val() != "0") {
			// if ($("#jenis_pembayaran").val() == "#") {
				// sweetAlert("Maaf...", "Jenis Pembayaran Diisi!", "error");
				// $("#jenis_pembayaran").focus();
				// return false;
			// }	
		// }
		if ($("#cara_pembayaran").val() == "1") {//1 kali
				
				if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
					if ($("#norek").val()=='' || $("#bank").val()=='' || $("#atasnama").val()==''){
						sweetAlert("Maaf...", "Data Bank harus Diisi!", "error");
						return false;
					}
				}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
					if ($("#tanggal_kontrabon").val()==''){
						sweetAlert("Maaf...", "tanggal Kontrabon harus Diisi!", "error");
						return false;
					}
				}else if ($("#jenis_pembayaran").val()=='#'){//Konta	
					sweetAlert("Maaf...", "Jenis Pembayaran Harus Diisi!", "error");
					return false;
				}
		}
		// alert($("#cara_pembayaran").val() + ' : '+ $("#sisa_termin").val());
		if ($("#cara_pembayaran").val() == "2") {//CICILAN
			// if ($("#total_dp_cicilan").val()=='0'){
				// sweetAlert("Maaf...", "DP Belum diinput Harus Diisi!", "error");
				// $("#cara_pembayaran").focus();
				// return false;
			// }else{
				var rowCount = $('#tabel_cicilan tbody tr').length;
				if (rowCount==0){
					sweetAlert("Maaf...", "Cicilan Belum diinput Harus Diisi!", "error");
					return false;
				}
			// }
			
			// return false;
			
		}
		if ($("#cara_pembayaran").val() == "3" && $("#sisa_termin").val()>0) {
			sweetAlert("Maaf...", "Termin Belum selesai Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		
		$("#cover-spin").show();
		$("*[disabled]").not(true).removeAttr("disabled");
		return true;
	}
	$(document).on("click", ".edit", function() {
			$(".edit").attr('disabled', true);
			$(".hapus").attr('disabled', true);
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);			
			$("#nama_barang").val($(this).closest('tr').find("td:eq(9) input").val());
			$("#merk_barang").val($(this).closest('tr').find("td:eq(10) input").val());
			$("#kuantitas").val($(this).closest('tr').find("td:eq(11) input").val());
			$("#satuan").val($(this).closest('tr').find("td:eq(12) input").val());
			$("#harga_satuan").val($(this).closest('tr').find("td:eq(13) input").val());
			$("#total_harga").val($(this).closest('tr').find("td:eq(14) input").val());
			$("#keterangan").val($(this).closest('tr').find("td:eq(15) input").val());
			$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
			// alert($(this).closest('tr').find("td:eq(8) input").val());
			$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
			
			$("#harga_pokok").val($(this).closest('tr').find("td:eq(17) input").val());
			$("#harga_diskon").val($(this).closest('tr').find("td:eq(18) input").val());
			$("#harga_ppn").val($(this).closest('tr').find("td:eq(19) input").val());
			$("#idklasifikasi_det").val($(this).closest('tr').find("td:eq(20) input").val());
			
			
			$("#kuantitas").focus();
	});
	
	function hitung_total(){
		var total_grand = 0;
		var total_item = 0;
		var total_jenis_barang = 0;
		$('#tabel_add tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(16) input").val()=='1'){
				total_jenis_barang +=1;
				total_grand += parseFloat($(this).find('td:eq(14) input').val());
				total_item += parseFloat($(this).find('td:eq(11) input').val());
			}
		});
		// alert(total_grand);
		$("#grand_total").val(total_grand);
		$("#total_item").val(total_item);
		$("#total_jenis_barang").val(total_jenis_barang);
	}
	$(document).on("click", "#canceljual", function() {
		clear_input();
	});
	$(document).on("change", "#idrka", function() {
		refresh_program();
	});
	
	$(document).on("click", "#btn_refresh_vendor", function() {
		refresh_vendor();
	});
	
	function refresh_vendor(){
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_vendor/',
			dataType: "json",
			success: function(data) {
				$("#idvendor").empty();
				$('#idvendor').append('<option value="#">- Pilih Vendor -</option><option value="0">Tentukan Vendor Nanti</option>');
				$('#idvendor').append(data.detail);
			}
		});
	}
	function refresh_program(){
		var idrka=$("#idrka").val();
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_program/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#idprogram").empty();
				$('#idprogram').append('<option value="#">- Pilih Program -</option>');
				$('#idprogram').append(data.detail);
			}
		});
	}
	
	
	$(document).on("click", "#btn_open_modal_kegiatan", function() {
		$("#modal_kegiatan").modal('show');
		
	});
	function load_kegiatan(){
		var idrka=$("#idrka").val();
		var idprespektif=$("#idprespektif").val();
		var idprogram=$("#idprogram").val();
		
		$('#tabel_kegiatan').DataTable().destroy();
		table=$('#tabel_kegiatan').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_pengajuan/load_kegiatan_reminder',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
						idprespektif:idprespektif,
						idprogram:idprogram,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,3,4], "visible": false },
					{"targets": [10], "class": 'text-center' },
					
				]
			});
	}
	$(document).on("click",".pilih",function(){
		var table = $('#tabel_kegiatan').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var bulan = table.cell(tr,1).data()
		$('#tabel_add tbody').empty();
		hitung_total();
		$.ajax({
			url: '{site_url}mrka_pengajuan/get_pilih_kegiatan/'+id+'/'+bulan,
			dataType: "json",
			success: function(data) {
				$("#nama_kegiatan").val(data.nama);
				$("#bulan").val(bulan);
		// alert(id+' '+bulan);
				$("#idrka_kegiatan").val(id);
				// $("#idunit_pengaju").val(data.idunit).trigger('change');
				// $("#idunit").val(data.idunit).trigger('change');
				$("#modal_kegiatan").modal('hide');
			}
		});
		
		
	});
	$(document).on("click","#btn_filter_rka",function(){
		load_kegiatan();
		
	});
	
</script>