<div id="div_termin">
	<div class="form-group" style="display:block">
		<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Jenis Termin Fix</span></h4></label>
		<div class="col-md-10">
			<table id="tabel_dp_termin" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th style="width: 15%;">Jenis</th>
						<th style="width: 20%;">Judul Termin</th>
						<th style="width: 20%;">Deskrpsi</th>
						<th style="width: 15%;">Nominal</th>
						<th style="width: 15%;">Tanggal</th>
						<th style="width: 10%;">Actions</th>
					</tr>
					<tr>							
						<td>
							<select id="xjenis_termin" name="xjenis_termin" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>Pilih Jenis Pembayaran</option>
								<option value="1" >Down Payment</option>
								<option value="2" >Pembayaran</option>							
							</select>
						</td>
						<td><input class="form-control input-sm" type="text" id="xjudul_termin" /></td>
						<td><input class="form-control input-sm" type="text" id="xdeskripsi_termin" /></td>
						<td><input class="form-control input-sm number" type="text" id="xnominal_termin" /></td>
						<td>
							<div class="input-group date">
								<input class="js-datepicker form-control input-sm"  <?=$disabel?> type="text" id="xtanggal_termin" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</td>
						<td>
							<button type="button" <?=$disabel?> class="btn btn-sm btn-primary kunci" tabindex="5" id="add_dp_termin" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
							<button type='button' <?=$disabel?> class='btn btn-sm btn-danger kunci' tabindex="9" id="cancel_dp_termin" title="Refresh"><i class='fa fa-times'></i></button>
						</td>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
				
			</table>
		</div>
					
	</div>
	<div class="form-group" style="display:block">
		<label class="col-md-2 control-label" for="nama">Total Pembayaran</label>
		<div class="col-md-4">
			<input readonly class="form-control input-sm number" type="text" id="total_dp_termin" name="total_dp_termin" value="{total_dp_termin}"/>
		</div>
		<label class="col-md-2 control-label" for="nama">Sisa Untuk dicicil</label>
		<div class="col-md-4">
			<input readonly class="form-control input-sm number" type="text" id="sisa_termin" name="sisa_termin" value="{sisa_termin}"/>
			<input readonly class="form-control input-sm" type="hidden" id="rowindex_dp_termin" />
		</div>
					
	</div>
	
</div>
<script type="text/javascript">
//Termin
	$("#add_dp_termin").click(function() {
		var content = "";
		var id=$("#id").val();
		if (!validate_detail_termin()) return false;
		
		if (id){
			add_detail_termin_live(id);
		}else{		
			if ($("#rowindex_dp_cicilan").val() == '') {
				content += "<tr>";
			}
			content+='<td><input class="form-control input-sm" readonly type="text" value="'+$("#xjenis_termin option:selected").text()+'"/></td>';	
			content+='<td><input class="form-control input-sm" readonly type="text" name="judul_termin[]" value="'+$("#xjudul_termin").val()+'"</td>';
			content+='<td><input class="form-control input-sm" readonly type="text" name="deskripsi_termin[]" value="'+$("#xdeskripsi_termin").val()+'"/></td>';
			content+='<td><input class="form-control input-sm number" readonly type="text" name="nominal_termin[]" value="'+$("#xnominal_termin").val()+'"/></td>';
			content+='<td>';
				content+='<div class="input-group date">';
					content+='<input class="js-datepicker form-control input-sm"  disabled type="text" name="tanggal_termin[]" value="'+$("#xtanggal_termin").val()+'" data-date-format="dd-mm-yyyy"/>';
					content+='<span class="input-group-addon">';
						content+='<span class="fa fa-calendar"></span>';
					content+='</span>';
				content+='</div>';
			content+='</td>';
			content+='<td><button type="button" class="btn btn-sm btn-danger hapus_termin" tabindex="9" id="cancel_dp_termin" title="Refresh"><i class="fa fa-times"></i></button></td>';
			content+='<td style="display:none"><input class="form-control input-sm" type="text" name="jenis_termin[]" value="'+$("#xjenis_termin").val()+'"/></td>';
			content+='<td style="display:none"><input class="form-control input-sm" type="text" name="status_dibayar_termin[]" value="0"/></td>';
				
				if ($("#rowindex").val() != '') {
					$('#tabel_dp_termin tbody tr:eq(' + $("#rowindex_dp_cicilan").val() + ')').html(content);
				} else {
					content += "</tr>";
					$('#tabel_dp_termin tbody').append(content);
				}
				hitung_total_termin();
				clear_input_termin();
		}
	});
	function load_item_termin(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$("#cover-spin").show();
		$("#tabel_dp_termin tbody").empty();
		
		$.ajax({
			url: '{site_url}mrka_pengajuan/load_item_termin/'+id+'/'+disabel,
			dataType: "json",
			success: function(data) {
				$("#tabel_dp_termin tbody").empty();
				$("#tabel_dp_termin tbody").append(data.tabel);
				$("#total_dp_termin").val(data.total_dp_termin);
				$("#cover-spin").hide();
				hitung_total_termin();
			}
		});
	}
	function add_detail_termin_live($id){
		if ($id){
			var id=$("#id").val();		
			var jenis_termin=$("#xjenis_termin").val();		
			var judul_termin=$("#xjudul_termin").val();		
			var deskripsi_termin=$("#xdeskripsi_termin").val();		
			var nominal_termin=$("#xnominal_termin").val();		
			var tanggal_termin=$("#xtanggal_termin").val();		
			
			$.ajax({
				url: '{site_url}mrka_pengajuan/save_detail_termin',
				type: 'POST',
				data: {
					id: id,jenis_termin: jenis_termin,
					judul_termin: judul_termin,deskripsi_termin: deskripsi_termin,
					nominal_termin: nominal_termin,tanggal_termin: tanggal_termin,
					
				},
				complete: function() {
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					load_item_termin();
					// load_item_barang(id);
					hitung_total();
					clear_input_termin();
				}
			});
		}
	}
	function handleClickTermin(cb,idtermin) {
		// alert(cb.checked);
		var pilih;
		var id = $('#id').val();
		if (cb.checked==true){
			pilih='1';
		}else{
			pilih='0';
		}
		// alert(jenis_cicilan);
		$.ajax({
				url: '{site_url}mrka_pengajuan/savePilihTermin',
				type: 'POST',
				data: {id: id, idtermin: idtermin, pilih: pilih},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil diubah'});
					
				}
			});
	}
	function hapus_termin($id){
		$.ajax({
			url: '{site_url}mrka_pengajuan/hapus_termin/'+$id,
			dataType: "json",
			success: function(data) {
				load_item_termin();
				hitung_total();
				// load_list_cicilan();
				// reset_cicilan();
			}
		});
	}
	function validate_detail_termin() {
		// alert('sini');
		if ($("#xjenis_termin").val() == "#") {
			sweetAlert("Maaf...", "Tipe Transaksi Harus Diisi!", "error");
			$("#xjenis_termin").focus();
			return false;
		}
		if ($("#xjudul_termin").val() == "") {
			sweetAlert("Maaf...", "Judul Harus Diisi!", "error");
			$("#xjudul_termin").focus();
			return false;
		}
		if ($("#xdeskripsi_termin").val() == "") {
			sweetAlert("Maaf...", "Deskripsi Harus Diisi!", "error");
			$("#xdeskripsi_termin").focus();
			return false;
		}
		if ($("#xnominal_termin").val() == "" || $("#xnominal_termin").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#xnominal_termin").focus();
			return false;
		}
		if ($("#xtanggal_termin").val() == "") {
			sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			$("#xtanggal_termin").focus();
			return false;
		}
		var sisa=0;
		sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_termin").val()) + parseFloat($("#total_dp_termin").val()));
		if (sisa < 0){
			sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			return false;
		}
		
		
		return true;
	}
	$(document).on("click", ".hapus_termin", function() {
		var baris=$(this).closest('td').parent();		
		baris.remove();
		hitung_total_termin();
		clear_input_termin();
	});
	
	
	function hitung_total_termin(){
		var total_dp = $("#total_dp_termin").val();;
		// $('#tabel_dp_termin tbody tr').each(function() {
				// console.log($(this).find('td:eq(3) input').val());
				// total_dp += parseFloat($(this).find('td:eq(3) input').val());
		// });
		// $("#total_dp_termin").val(total_dp);
		$("#sisa_termin").val(parseFloat($("#grand_total").val()) - total_dp);
		if ($("#cara_pembayaran").val()=='3'){
			if ($("#sisa_termin").val()>0){
				$(':button[type=submit]').prop('disabled', true);
				$.toaster({priority : 'danger', title : 'Warning!', message : 'Pembayaran Termin Belum Sesuai'});
			}else{
				$(':button[type=submit]').removeAttr("disabled");
			}
		}
		// alert(parseFloat($("#grand_total").val()));
		
	}
	function clear_input_termin(){
		$("#xjenis_termin").val('#').trigger('change')
		$("#xjudul_termin").val('')
		$("#xdeskripsi_termin").val('')
		$("#xnominal_termin").val('0')
		$("#xtanggal_termin").val('')
		$('.number').number(true, 0, '.', ',');
	}
	
	// END TERMIN
</script>