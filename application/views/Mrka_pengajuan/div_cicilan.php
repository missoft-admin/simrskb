<input type="hidden" class="form-control" placeholder="Nama RKA" name="status_pengajuan" id="status_pengajuan" value="{status}">
<input type="hidden" class="form-control" placeholder="Nama RKA" name="st_validasi" id="st_validasi" value="{st_validasi}">
<div id="div_cicilan">
	<div class="form-group" style="display:block">
		<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Pembayaran Cicilan</span></h4></label>
		<div class="col-md-10">
			<table id="tabel_dp_cicilan" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th style="width: 30%;">Item DP</th>
						<th style="width: 15%;">Nominal DP</th>
						<th style="width: 15%;">Tanggal</th>
						<th style="width: 30%;">Keterangan</th>
						<th style="width: 10%;">Actions</th>
					</tr>
					<tr>							
						<td>
							<select id="item_id" name="item_id" <?=$disabel?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								
							</select>
						</td>
						<td><input class="form-control input-sm number" type="text"  <?=$disabel?> id="xnominal_cicilan" /><input class="form-control input-sm number" type="hidden" id="xmax_dp" /></td>
						<td>
							<div class="input-group date">
								<input class="js-datepicker form-control input-sm"  <?=$disabel?> autocomplete="off"  <?=$disabel?> type="text" id="xtanggal_cicilan" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</td>
						<td><input class="form-control input-sm" type="text"  <?=$disabel?> id="xketerangan_cicilan" /></td>
						<td>
							<button type="button" <?=$disabel?> class="btn btn-sm btn-primary kunci" tabindex="5" id="add_dp_cicilan" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
							<button type='button' <?=$disabel?> class='btn btn-sm btn-danger kunci' tabindex="9" id="cancel_dp_cicilan" title="Refresh"><i class='fa fa-times'></i></button>
						</td>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
				
			</table>
		</div>
					
	</div>
	<div class="form-group" style="display:block">
		<label class="col-md-2 control-label" for="nama">Total DP</label>
		<div class="col-md-4">
			<input readonly class="form-control input-sm number" type="text" id="total_dp_cicilan" name="total_dp_cicilan" value="{total_dp_cicilan}"/>
		</div>
		<label class="col-md-2 control-label" for="nama">Sisa Untuk dicicil</label>
		<div class="col-md-4">
			<input readonly class="form-control input-sm number" type="text" id="sisa_cicilan" name="sisa_cicilan" value="{sisa_cicilan}"/>
			<input readonly class="form-control input-sm" type="hidden" id="rowindex_dp_cicilan" />
		</div>
					
	</div>
	<div class="form-group" style="display:block">
		<label class="col-md-2 control-label" for="nama">Jumlah cicilan</label>
		<div class="col-md-4">
			<input class="form-control input-sm number" <?=$disabel?> type="text" id="jml_kali_cicilan" name="jml_kali_cicilan" value="{jml_kali_cicilan}" />
		</div>
		<label class="col-md-2 control-label" for="nama">Cicilan Per bulan</label>
		<div class="col-md-4">
			<input readonly class="form-control input-sm number" type="text" id="per_bulan_cicilan" name="per_bulan_cicilan" value="{per_bulan_cicilan}"/>
		</div>
		
					
	</div>
	<div class="form-group" style="display:block">
		<label class="col-md-2 control-label" for="nama">Cicilan Ke 1</label>
		<div class="col-md-4">
			<div class="input-group date">
				<input class="js-datepicker form-control input-sm" autocomplete="off" <?=$disabel?> type="text" name="xtanggal_cicilan_pertama" id="xtanggal_cicilan_pertama" value="<?=($xtanggal_cicilan_pertama?HumanDateShort($xtanggal_cicilan_pertama):'')?>" data-date-format="dd-mm-yyyy"/>
				<span class="input-group-addon">
					<span class="fa fa-calendar"></span>
				</span>
			</div>
		</div>
		
		<div class="col-md-4">
			<button type="button" <?=$disabel?> class="btn btn-sm btn-success kunci" tabindex="5" id="generate_cicilan" title="Masukan Item"><i class="fa fa-level-down"></i> Generate Cicilan</button>
		</div>
	</div>
	<div class="form-group" style="display:block">
		<label class="col-md-2 control-label" for="nama"></label>
		<div class="col-md-10">
			<table id="tabel_cicilan" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th style="width: 30%;">Keterangan</th>
						<th style="width: 15%;">Tanggal</th>
						<th style="width: 15%;">Nominal</th>
						<th style="width: 15%;">Proses Langsung</th>
					</tr>								
				</thead>
				<tbody>
					
				</tbody>
				
			</table>
		</div>
					
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_detail_cicilan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title"><i class="fa fa-file-image-o"></i> Detail Cicilan</h3>
				</div>
				<div class="block-content">
					<div class="col-md-12" style="margin-top: 10px;">
						<div class="block" id="box_file2">					
							 <table class="table table-striped table-bordered" id="tabel_detail_cicilan">
								<thead>
									<tr>
										<th width="20%" class="text-center">Nama</th>
										<th width="25%" class="text-center">Merk</th>
										<th width="10%" class="text-center">Satuan</th>
										<th width="15%" class="text-center">Nominal</th>
										<th width="35%" class="text-center">Keterangan</th>
										
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
						
					</div> 
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_harga_asal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title"><i class="si si-bar-chart"></i> Harga Asal</h3>
				</div>
				<div class="block-content">
					<div class="col-md-12" style="margin-top: 10px;">
						<div class="block">					
							 <table class="table table-striped table-bordered" id="tabel_harga_asal">
								<thead>
									<tr>
										<th width="15%" class="text-center">Nama</th>
										<th width="15%" class="text-center">Merk</th>
										<th width="5%" class="text-center">Qty</th>
										<th width="5%" class="text-center">Satuan</th>
										<th width="15%" class="text-center">Harga Pokok</th>
										<th width="10%" class="text-center">Diskon</th>
										<th width="10%" class="text-center">PPN</th>
										<th width="15%" class="text-center">Harga Satuan</th>
										<th width="15%" class="text-center">Total Harga</th>
										
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
						
					</div> 
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript">
//HARGA Asal
function lihat_harga_asal(id){
	$("#modal_harga_asal").modal('show');
	$.ajax({
		url: '{site_url}mrka_pengajuan/lihat_harga_asal/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_harga_asal tbody").empty();
			$("#tabel_harga_asal tbody").append(data.tabel);
			
		}
	});
}
//TITIPAN REVISIAN 
	$("#addjual").click(function() {
		var id=$("#id").val();
		if (id){
			if (!validate_detail()) return false;
			add_detail_live(id);
		}else{
			
			var content = "";
			if (!validate_detail()) return false;
			if ($("#rowindex").val() == '') {
				content += "<tr>";
				// alert('Baru');
			}else{
				// alert('update');
			}
			content += "<td>" + $("#nama_barang").val() + "</td>"; //0 No Urut
			content += "<td>" + $("#merk_barang").val() + "</td>"; //1 
			content += "<td>" + $("#kuantitas").val() + "</td>"; //2 
			content += "<td>" + $("#satuan").val() + "</td>"; //3 
			content += "<td>" + formatNumber($("#harga_satuan").val()) + "</td>"; //4 
			content += "<td>" + formatNumber($("#total_harga").val()) + "</td>"; //5 
			content += "<td>" + $("#keterangan").val() + "</td>"; //6			
			content +="<td ><div class='btn-group'><button type='button' class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div></td>";
			content += "<td style='display:none'><input type='text' name='xiddet[]' value='"+$("#iddet").val()+"'></td>"; //8 nomor
			content += "<td style='display:none'><input type='text' name='xnama_barang[]' value='"+$("#nama_barang").val()+"'></td>"; //9 
			content += "<td style='display:none'><input type='text' name='xmerk_barang[]' value='"+$("#merk_barang").val()+"'></td>"; //10
			content += "<td style='display:none'><input type='text' name='xkuantitas[]' value='"+$("#kuantitas").val()+"'></td>"; //11
			content += "<td style='display:none'><input type='text' name='xsatuan[]' value='"+$("#satuan").val()+"'></td>"; //12
			content += "<td style='display:none'><input type='text' name='xharga_satuan[]' value='"+$("#harga_satuan").val()+"'></td>"; //13
			content += "<td style='display:none'><input type='text' name='xtotal_harga[]' value='"+$("#total_harga").val()+"'></td>"; //14
			content += "<td style='display:none'><input type='text' name='xketerangan[]' value='"+$("#keterangan").val()+"'></td>"; //15
			content += "<td style='display:none'><input type='text' name='xstatus[]' value='1'></td>"; //16
			content += "<td style='display:none'><input type='text' name='xharga_pokok[]' value='"+$("#harga_pokok").val()+"'></td>"; //17
			content += "<td style='display:none'><input type='text' name='xharga_diskon[]' value='"+$("#harga_diskon").val()+"'></td>"; //18
			content += "<td style='display:none'><input type='text' name='xharga_ppn[]' value='"+$("#harga_ppn").val()+"'></td>"; //19
			content += "<td style='display:none'><input type='text' name='xidklasifikasi_det[]' value='"+$("#idklasifikasi_det").val()+"'></td>"; //20
			
			if ($("#rowindex").val() != '') {
				$('#tabel_add tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#tabel_add tbody').append(content);
			}
			hitung_total();
			clear_input();
		}
	});
	function add_detail_live($id){
		if ($id){
			var id=$("#id").val();		
			var iddet=$("#iddet").val();		
			var nama_barang=$("#nama_barang").val();		
			var merk_barang=$("#merk_barang").val();		
			var kuantitas=$("#kuantitas").val();		
			var satuan=$("#satuan").val();		
			var harga_satuan=$("#harga_satuan").val();		
			var total_harga=$("#total_harga").val();		
			var keterangan=$("#keterangan").val();		
			var status=1;		
			var harga_pokok=$("#harga_pokok").val();		
			var harga_diskon=$("#harga_diskon").val();		
			var harga_ppn=$("#harga_ppn").val();		
			var idklasifikasi_det=$("#idklasifikasi_det").val();		
			// table = $('#harga_pokok').DataTable()	
			$.ajax({
				url: '{site_url}mrka_pengajuan/save_detail',
				type: 'POST',
				data: {
					id: id,iddet: iddet,
					nama_barang: nama_barang,merk_barang: merk_barang,
					kuantitas: kuantitas,satuan: satuan,
					harga_satuan: harga_satuan,total_harga: total_harga,
					keterangan: keterangan,status: status,
					harga_pokok: harga_pokok,harga_diskon: harga_diskon,
					harga_ppn: harga_ppn,idklasifikasi_det: idklasifikasi_det,
				},
				complete: function() {
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});

					load_item_barang(id);
					hitung_total();
					clear_input();
				}
			});
		}
	}
	function validate_detail() {
		if ($("#nama_barang").val() == "") {
			sweetAlert("Maaf...", "Nama barang Harus Diisi!", "error");
			$("#nama_barang").focus();
			return false;
		}
		if ($("#merk_barang").val() == "") {
			sweetAlert("Maaf...", "Merk barang Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		
		if ($("#kuantitas").val() < 1) {
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#kuantitas").focus();
			return false;
		}
		if ($("#satuan").val() == "") {
			sweetAlert("Maaf...", "Satuan barang Harus Diisi!", "error");
			$("#satuan").focus();
			return false;
		}
		if ($("#harga_satuan").val() < 1) {
			sweetAlert("Maaf...", "Harga Harus Diisi!", "error");
			$("#harga_satuan").focus();
			return false;
		}
		if ($("#total_harga").val() < 1) {
			sweetAlert("Maaf...", "Total Harga Harus Diisi!", "error");
			$("#total_harga").focus();
			return false;
		}
		return true;
	}
	$(document).on("click", ".hapus", function() {
		var baris=$(this).closest('td').parent();
		var id;
		// alert($(this).closest('tr').find("td:eq(8) input").val());
			if ($("#id").val()==''){
				// alert($("#id").val());
				baris.remove();
				hitung_total();
			}else{
				id=$(this).closest('tr').find("td:eq(8) input").val();
				hapus_item(id);
				// $(this).closest('tr').find("td:eq(16) input").val(0)
				// $(this).closest('tr').find("td:eq(6)").html('<span class="label label-danger">DELETED</span>')
				// $(this).closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
			}
		
		 hitung_total();
	});
	function hapus_item($id){
		var idpengajuan=$("#id").val();
		$.ajax({
			url: '{site_url}mrka_pengajuan/hapus_item/'+$id,
			dataType: "json",
			success: function(data) {
				load_item_barang(idpengajuan);
			}
		});
	}
	function show_detail(tanggal_cicilan,jenis_cicilan){
		$("#modal_detail_cicilan").modal('show');
		var id=$("#id").val();
		$("#tabel_detail_cicilan tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mrka_pengajuan/list_detail_cicilan',
			type: 'POST',
			dataType: "json",
			data: {
				id: id,
				jenis_cicilan: jenis_cicilan,
				tanggal_cicilan: tanggal_cicilan,
			},
			success: function(data) {
				$("#tabel_detail_cicilan tbody").empty();
				$("#tabel_detail_cicilan tbody").append(data.tabel);
				$("#cover-spin").hide();
				// alert(data.tabel);
			}
		});
	}
	function load_item_barang($id){
		var disabel=$("#disabel").val();
		if ($("#st_validasi").val()=='1'){
			disabel='disabled';
		}
		$("#cover-spin").show();
		$("#tabel_add tbody").empty();
		
		$.ajax({
			url: '{site_url}mrka_pengajuan/load_item_barang/'+$id+'/'+disabel,
			dataType: "json",
			success: function(data) {
				$("#tabel_add tbody").empty();
				$("#tabel_add tbody").append(data.tabel);
				// $("#grand_total").val(data.grand_total);
				// $("#total_item").val(data.item);
				if ($("#cara_pembayaran").val()=='2'){
					load_item_dp();
					load_item_barang_select2();
				}
				if ($("#cara_pembayaran").val()=='3'){
					load_item_termin();
					hitung_total_termin();
				}
				if ($("#cara_pembayaran").val()=='1'){
					if ($("#status_pengajuan").val()=='3'){
						refresh_pembayaran();
					}
				}
				hitung_total();
				$("#cover-spin").hide();
				// $('#idvendor').append('<option value="#">- Pilih Vendor -</option><option value="0">Tentukan Vendor Nanti</option>');
				// $('#idvendor').append(data.detail);
			}
		});
	}
	function load_item_barang_select2(){
		var id=$("#id").val();
		// $("#cover-spin").show();
		$("#item_id").empty();
		
		$.ajax({
			url: '{site_url}mrka_pengajuan/load_item_barang_select2/'+id,
			dataType: "json",
			success: function(data) {
				$("#item_id").empty();
				$("#item_id").append(data.tabel);
				
			}
		});
	}
	function handleClick(cb,tanggal_cicilan,jenis_cicilan) {
		// alert(cb.checked);
		// var dt = $('#mrole').DataTable();
		var pilih;
		var id = $('#id').val();
		if (cb.checked==true){
			pilih='1';
		}else{
			pilih='0';
		}
		// alert(jenis_cicilan);
		$.ajax({
				url: '{site_url}mrka_pengajuan/savePilih',
				type: 'POST',
				data: {id: id, tanggal_cicilan: tanggal_cicilan, pilih: pilih, jenis_cicilan: jenis_cicilan},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil diubah'});
					
				}
			});
	}
	
	function load_item_dp(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$("#cover-spin").show();
		$("#tabel_dp_cicilan tbody").empty();
		
		$.ajax({
			url: '{site_url}mrka_pengajuan/load_item_dp/'+id+'/'+disabel,
			dataType: "json",
			success: function(data) {
				$("#tabel_dp_cicilan tbody").empty();
				$("#tabel_dp_cicilan tbody").append(data.tabel);
				$("#cover-spin").hide();
				hitung_total_cicilan();
			}
		});
	}
	//Cicilan
	$("#add_dp_cicilan").click(function() {
			if (!validate_detail_cicilan()) return false;
			var idpengajuan=$("#id").val();		
			var idpengajuan_detail=$("#item_id").val();		
			var jenis_cicilan=1;		
			var tanggal_cicilan=$("#xtanggal_cicilan").val();		
			var keterangan_cicilan=$("#xketerangan_cicilan").val();		
			var nominal_cicilan=$("#xnominal_cicilan").val();		
			$.ajax({
				url: '{site_url}mrka_pengajuan/save_dp',
				type: 'POST',
				data: {
					idpengajuan: idpengajuan,
					idpengajuan_detail: idpengajuan_detail,
					jenis_cicilan: jenis_cicilan,
					tanggal_cicilan: tanggal_cicilan,
					keterangan_cicilan: keterangan_cicilan,
					nominal_cicilan: nominal_cicilan,
				},
				complete: function() {
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});

					clear_input_cicilan();
					load_item_dp();
					load_list_cicilan();
					reset_cicilan();
				}
			});
			
	});
	function clear_input_cicilan(){
		$("#xmax_dp").val('0')
		$("#xnominal_cicilan").val('0')
		$("#xtanggal_cicilan").val('')
		$("#xketerangan_cicilan").val('')
		$("#rowindex_dp_cicilan").val('')
		$('.number').number(true, 0, '.', ',');
		$("#item_id").val("#").trigger('change');
	}
	function validate_detail_cicilan() {
		var cicilan=parseFloat($("#xnominal_cicilan").val());
		var dp=parseFloat($("#xmax_dp").val());
		// alert('sini');
		if ($("#item_id").val() == "#") {
			sweetAlert("Maaf...", "Item DP Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		
		if (cicilan > dp){
			sweetAlert("Maaf...", "Jumlah DP Maksimal "+formatNumber(dp)+" !", "error");
			$("#xnominal_cicilan").val(dp);
			return false;
		}
		
		if ($("#xnominal_cicilan").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		
		if ($("#xtanggal_cicilan").val() == "") {
			sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		if ($("#xketerangan_cicilan").val() == "") {
			sweetAlert("Maaf...", "Keterangan DP Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		var sisa=0;
		sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_cicilan").val()) + parseFloat($("#total_dp_cicilan").val()));
		if (sisa < 0){
			sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			return false;
		}
		
		
		return true;
	}
	function hitung_total_cicilan(){
		var total_dp = 0;
		$('#tabel_dp_cicilan tbody tr').each(function() {
				// console.log($(this).find('td:eq(0) input').val());
				total_dp += parseFloat($(this).find('td:eq(0) input').val());
		});
		let total_pembayaran=0;
		var id=$("#id").val();
		$.ajax({
			url: '{site_url}mrka_pengajuan/get_total_dp/'+id,
			dataType: "json",
			success: function(data) {
				$("#total_dp_cicilan").val(data);
				$("#sisa_cicilan").val(parseFloat($("#grand_total").val()) - parseFloat($("#total_dp_cicilan").val()));
				
			}
		});
	}
	$(document).on("click", "#generate_cicilan", function() {
		generate_cicilan();
	});
	$(document).on("click", "#cancel_dp_cicilan", function() {
		clear_input_cicilan();
	});
	$(document).on("keyup", "#jml_kali_cicilan", function() {
		var perbulan=0;
		if ($("#jml_kali_cicilan").val()==''){
			$("#jml_kali_cicilan").val(0);
		}
		if (parseFloat($("#jml_kali_cicilan").val())>180){
			$("#jml_kali_cicilan").val(180)
			sweetAlert("Maaf...", "Jumlah Cicilan tidak boleh lebih dari 15 tahun!", "error");
			return false;
		}
		if ($("#sisa_cicilan").val()=='0' || $("#sisa_cicilan").val()=='0'){
			return false;
		}
		
		perbulan=parseFloat($("#sisa_cicilan").val()) / parseFloat($("#jml_kali_cicilan").val());
		$("#per_bulan_cicilan").val(perbulan)
	});
	function generate_cicilan(){
		perbulan=parseFloat($("#sisa_cicilan").val()) / parseFloat($("#jml_kali_cicilan").val());
		$("#per_bulan_cicilan").val(perbulan)
		if ($("#sisa_cicilan").val()=='0' || $("#sisa_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Sisa cicilan harus ada!", "error");
			return false;
		}
		if ($("#xtanggal_cicilan_pertama").val()==null || $("#xtanggal_cicilan_pertama").val()==''){
			sweetAlert("Maaf...", "Tanggal pertama harus ada!", "error");
			return false;
		}
		if ($("#jml_kali_cicilan").val()=='0' || $("#jml_kali_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Jumlah cicilan harus ada!", "error");
			return false;
		}
		if ($("#per_bulan_cicilan").val()=='0' || $("#per_bulan_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Jumlah cicilan per bulan harus ada!", "error");
			return false;
		}
		$("tabel_cicilan tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mrka_pengajuan/generate_cicilan/',
			type: "POST",
			dataType: "json",
			data : {
					xtanggal_cicilan:$("#xtanggal_cicilan_pertama").val(),
					jml_kali_cicilan:$("#jml_kali_cicilan").val(),
					per_bulan_cicilan:$("#per_bulan_cicilan").val(),					
					sisa_cicilan:$("#sisa_cicilan").val(),					
					total_dp_cicilan:$("#total_dp_cicilan").val(),					
					id:$("#id").val(),					
					
				   },
			success: function(data) {
				// $("#tabel_cicilan tbody").empty();
				// $('#tabel_cicilan tbody').append(data.detail);
				// $('.number').number(true, 0, '.', ',');
				$("#cover-spin").hide();
				load_list_cicilan();
			}
		});
	}
	function load_list_cicilan(){
		// alert('cicilan');
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$("#cover-spin").show();
		$("#tabel_cicilan tbody").empty();
		
		$.ajax({
			url: '{site_url}mrka_pengajuan/list_cicilan/'+id+'/'+disabel,
			dataType: "json",
			success: function(data) {
				$("#tabel_cicilan tbody").empty();
				$('#tabel_cicilan tbody').append(data.detail);
				$("#cover-spin").hide();
				if ($("#cara_pembayaran").val()=='2'){					
					if (parseFloat($("#grand_total").val()) != parseFloat($("#total_generate_cicilan").val())){
						// alert(parseFloat($("#grand_total").val()) +' - '+parseFloat($("#total_generate_cicilan").val()));
						$(':button[type=submit]').prop('disabled', true);
						$.toaster({priority : 'danger', title : 'Warning!', message : 'Jumlah Cicilan Beda'});
						if ($("#status_pengajuan").val()=='3'){
							$('#btn_generate').prop('disabled', true);
						}
					}else{
						$(':button[type=submit]').removeAttr("disabled");
						if ($("#status_pengajuan").val()=='3'){
							$('#btn_generate').prop('disabled', false);
						}
					}
				}
			}
		});
	}
	function reset_cicilan(){
		$("#tabel_cicilan tbody").empty();
		
	}
	$(document).on("click", ".hapus_dp_cicilan", function() {
		var baris=$(this).closest('td').parent();		
		baris.remove();
		hitung_total_cicilan();
		reset_cicilan();
	});
	$(document).on("change", "#item_id", function() {
		var id=$("#item_id").val();
		$("#xnominal_cicilan").val(0);
		if (id!='#'){			
			$.ajax({
				url: '{site_url}mrka_pengajuan/get_mak_dp/'+id,
				dataType: "json",
				success: function(data) {
					$("#xmax_dp").val(data);
				}
			});
		}else{
			$("#xmax_dp").val(0);
		}
	});
	$(document).on("keyup", "#xnominal_cicilan", function() {
		var cicilan=parseFloat($("#xnominal_cicilan").val());
		var dp=parseFloat($("#xmax_dp").val());
		if ($("#item_id").val()=='#'){
			$("#xnominal_cicilan").val(0);
			return false;
		}
		if (cicilan > dp){
			sweetAlert("Maaf...", "Jumlah DP Maksimal "+formatNumber(dp)+" !", "error");
			$("#xnominal_cicilan").val(dp);
			return false;
		}
		
	});
	function hapus_dp_cicilan($id){
		$.ajax({
			url: '{site_url}mrka_pengajuan/hapus_dp_cicilan/'+$id,
			dataType: "json",
			success: function(data) {
				load_item_dp();
				load_list_cicilan();
				reset_cicilan();
			}
		});
	}
	function clear_tabel_cicilan(){
		$("#tabel_cicilan tbody").empty();
	}
	// function formatNumber (num) {
	// return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	// }

	//END CICILAN
</script>