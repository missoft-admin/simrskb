<div class="modal fade in black-overlay" id="modal_harga" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:40%">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Menentukan Harga</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Klasifikasi</label>
								<div class="col-md-8">
									<select id="tidklasifikasi" name="tidklasifikasi" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Klasifikasi -</option>
										<? foreach($list_klasifikasi as $row){ ?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Harga Pokok</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal harga" tabindex="4" type="text" id="tharga_pokok" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Nominal Diskon</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal harga" tabindex="4" type="text" id="tharga_diskon" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Nominal PPN</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal harga" tabindex="4" type="text" id="tharga_ppn" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Harga Final</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal" readonly tabindex="4" type="text" id="tharga_satuan" />
								</div>
							</div>
							
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
					<button class="btn btn-sm btn-primary" type="button" onclick="set_harga()"><i class="fa fa-check"></i> Ok</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on("click","#btn_open_modal_harga",function(){
		$('#modal_harga').modal('show');
		
		load_harga_pokok();
		if ($("#rowindex").val()==''){
			$("#tidklasifikasi").val($("#idklasifikasi").val()).trigger('change');			
		}else{
			$("#tidklasifikasi").val($("#idklasifikasi_det").val()).trigger('change');
		}
		generate_harga_final();
	});
	function load_harga_pokok(){
		$("#tharga_satuan").val($("#harga_satuan").val());
		$("#tharga_pokok").val($("#harga_pokok").val());
		$("#tharga_diskon").val($("#harga_diskon").val());
		$("#tharga_ppn").val($("#harga_ppn").val());
	}
	$(document).on("keyup",".harga",function(){
		if ($(this).val()==''){
			$(this).val(0);
		}
		generate_harga_final();
	});
	function generate_harga_final(){
		var harga_final=0;
		var hpp=parseFloat($("#tharga_pokok").val());
		var hd=parseFloat($("#tharga_diskon").val());
		var hp=parseFloat($("#tharga_ppn").val());
		harga_final=hpp-hd+hp;
		$("#tharga_satuan").val(harga_final);
	}
	function set_harga(){
		if ($("#tidklasifikasi").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Klasifikasi!", "error");
			return false;
		}
		$("#harga_satuan").val($("#tharga_satuan").val());
		$("#harga_pokok").val($("#tharga_pokok").val());
		$("#harga_diskon").val($("#tharga_diskon").val());
		$("#harga_ppn").val($("#tharga_ppn").val());
		$("#idklasifikasi_det").val($("#tidklasifikasi").val());
		$("#modal_harga").modal('hide')
		perkalian();
		// alert('sini');
	}
</script>