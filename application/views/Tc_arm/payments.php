<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="<?= '{site_url}Tc_arm/periodyears/'.$this->uri->segment(3);?>" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
    <hr>
	</div>

	<div class="block-content" style="text-transform:uppercase;">
    <div class="row" style="margin-top:-33px;">
      <div class="col-md-12">
        <button id="create-payments" class="btn btn-success text-uppercase" type="button" name="button" data-toggle="modal" data-target="#add-payments" data-value="0"><i class="fa fa-plus"></i> Buat Transaksi Bagi Hasil</button>
      </div>
    </div>

    <div class="row" style="margin-top:20px;">
      <div class="col-md-12">

        <table id="payments" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No Transaksi</th>
              <th>Periode</th>
              <th>Bulan</th>
              <th>Tahun</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <!-- {{ @action }} -->
          </tbody>
        </table>
      </div>
    </div>

	</div>
</div>

<div class="modal in" id="add-payments" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Transaksi Bagi Hasil</h3>
				</div>
				<div class="block-content" style="text-transform:uppercase;">
          <!-- @ -->

          <div class="row">
            <div class="col-md-12">

              <form id="form-payments" class="form-horizontal push-10-t" action="#">

								<div class="col-md-12">

									<div class="form-group">
										<label for="" class="control-label col-md-4">Periode Tahun</label>
										<div class="col-md-7">
											<input id="periodyears" class="form-control" multiple type="text" name="periodyears" readonly>
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-4">Nama Pembayaran</label>
										<div class="col-md-7">
											<input id="name-payments" class="form-control" type="text" name="name-payments" readonly>
										</div>
									</div>

									<div class="form-group">
										<label for="" class="control-label col-md-4">Bulan Dibayarkan</label>
										<div class="col-md-7">
											<select id="months" class="form-control js-select2" multiple type="text" name="months[]" style="width:100%;">
												<!-- @action -->
											</select>
										</div>
									</div>

									<div class="form-group">
										<label for="" class="control-label col-md-4">Catatan</label>
										<div class="col-md-7">
											<input id="note" class="form-control" type="text" name="note">
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-4"></div>
										<div class="col-md-7">
											<button id="save-payments" class="btn btn-success btn-sm text-uppercase" type="button" style="width:100px;">Save</button>
										</div>
									</div>
									<input id="id-carm" type="text" name="id-carm" hidden="true" readonly>

								</div>
									<input id="status" type="text" name="state" hidden="true" readonly>
              </form>

            </div>
          </div>
          <!-- @ -->
				</div>
				<div class="modal-footer">
					<div style="float:right;">
						<button type="button" class="btn btn-sm btn-default text-uppercase" data-dismiss="modal" style="width:100px;font-size:11px;">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/url.js"></script>

<script type="text/javascript">

  $(document).ready(function () {

		// @common &start
		$('#periodyears').val('<?=$this->uri->segment(4)?>');
		$('#id-carm').val('<?=$this->uri->segment(3)?>');

		function showSelect(status) {
			if (status) {
				$.ajax({
					url: "{site_url}Tc_arm/showPaymentsMonths/"+'<?=$this->uri->segment(3)?>'+"/"+'<?=$this->uri->segment(4)?>',
					dataType: 'json',
					success: function(data) {
						$('#months').select2('destroy');

						for (var iter = 0; iter < data.length; iter++) {
							if (data[iter].statuspayment == 1) {
								$('#months').append('<option value="'+data[iter].periodebulan+'" disabled>'+data[iter].periodebulan+'</option>');
							} else {
								$('#months').append('<option value="'+data[iter].periodebulan+'">'+data[iter].periodebulan+'</option>');
							}
						}

						$('#months').select2();
					}
				});
			} else {
				$('#months option').remove();
			}
		}

		function showSelected(months, status) {
			var arr = months;

			if (status) {
				$.ajax({
					url: "{site_url}Tc_arm/showPaymentsMonths/"+'<?=$this->uri->segment(3)?>'+"/"+'<?=$this->uri->segment(4)?>',
					dataType: 'json',
					success: function(data) {
						$('#months').select2('destroy');

						for (var iter = 0; iter < data.length; iter++) {
							if (data[iter].statuspayment == 1) {
								for (var i = 0; i < arr.length; i++) {
									if (data[iter].periodebulan == arr[i]) {
										$('#months').append('<option value="'+data[iter].periodebulan+'" selected>'+data[iter].periodebulan+'</option>');
									}
								}
							} else {
								$('#months').append('<option value="'+data[iter].periodebulan+'">'+data[iter].periodebulan+'</option>');
							}
						}

						$('#months').select2();
					}
				});
			} else {
				$('#months option').remove();
			}
		}

		$('#months').change(function() {
			var months = $(this).val();
			$('#name-payments').val("Pembayaran "+months);
		});

		$('#save-payments').click(function() {
			$.ajax({
				url: "{site_url}Tc_arm/savePayments/"+'<?=$this->uri->segment(3)?>'+"/"+'<?=$this->uri->segment(4)?>',
				type: 'POST',
				data: $('#form-payments').serialize(),
				complete: function(data) {
					swal("Berhasil!", "Membuat Transaksi Bagi Hasil", "success").then(function() {
						window.location.assign('{site_url}Tc_arm/payments/'+'<?=$this->uri->segment(3)?>'+"/"+'<?=$this->uri->segment(4)?>');
					});
					payments.ajax.reload();
				}
			});
		});
		// @common &end


    var payments = $('#payments').DataTable({
      "oLanguage":{
	      "sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
	    },
			"ajax": {
				"url":"{site_url}Tc_arm/showPayments",
			},
      "autoWidth": false,
      "lengthChange": true,
      "ordering": true,
      "searching": true,
      "processing": true,
      "bInfo": true,
      "bPaginate": true,
      "order": []
    });

		$('#create-payments').click(function() {
			id = $(this).attr('data-value');

			$('#status').val(id);

			$('#name-payments').val('');
			$('#note').val('');

			if (id == 0) {
				showSelect(true);
				showSelected(null, false);
			}
		});

		$(document).on('click', '.edit-payments', function() {
			// periodeyears name-payments months note
			var id = $(this).attr('data-value');

			$('#status').val(id);

			$.ajax({
				url: '{site_url}tc_arm/showPaymentsD/'+id,
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					var months = data[0].bulan.split(',');

					$('#periodeyears').val(data[0].tahun);
					$('#name-payments').val(data[0].nama_transaksi);
					$('#note').val(data[0].catatan);

					if (id != 0) {
						showSelected(months, true);
						showSelect(false);
					}
				}
			});

		});

  });
</script>
