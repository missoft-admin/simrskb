<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="<?='{site_url}Tc_arm/payments/'.$this->uri->segment(3).'/'.$this->uri->segment(4);?>" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
    <hr>
	</div>

	<div class="block-content" style="text-transform:uppercase;">
    <div class="row" style="margin-top:-33px;">
      <div class="col-md-12">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="" class="control-label col-md-2">Nama C - ARM</label>
            <div class="col-md-4">
              <input id="name-carm" class="form-control" type="text" name="name-carm" readonly>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="control-label col-md-2">Potongan RS</label>
            <div class="col-md-4">
              <input id="potongan-rs" class="form-control" type="text" name="potongan-rs" readonly>
            </div>
          </div>

          <div class="form-group">
            <label for="" class="control-label col-md-2">Periode Tahun</label>
            <div class="col-md-4">
              <input id="periodyears" class="form-control" type="text" name="periodyears" readonly>
            </div>
          </div>

          <div class="form-group">
            <label for="" class="control-label col-md-2">Nama Pembayaran</label>
            <div class="col-md-4">
              <input id="name-payments" class="form-control" type="text" name="name-payments" readonly>
            </div>
          </div>

          <div class="form-group">
            <label for="" class="control-label col-md-2">Bulan Dibayarkan</label>
            <div class="col-md-4">
              <select id="payment-months" class="form-control" multiple type="text" name="payment-months" disabled>
                <!-- @action -->
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="row" style="margin-top:20px;">
      <div class="col-md-12">

        <div class="row" style="padding-bottom:10px">
          <div class="col-md-6">
            <h5 style="padding-bottom:9px;" style="float:left;">Perhitungan Pendapatan</h5>
          </div>
          <div class="col-md-6">
            <button id="oo-paymentsdetail" class="btn btn-xs btn-primary text-uppercase" type="button" style="float:right;">Sembuyikan <i class="fa fa-arrow-up"></i></button>
          </div>
        </div>

        <table id="paymentsdetail" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Periode</th>
							<th>Omset</th>
              <th>Potongan Rs</th>
              <th>Operasional</th>
              <th>Deviden Pemilik Saham</th>
							<th hidden>Harga Perlembar</th>
            </tr>
          </thead>
          <tbody>
            <!-- {{ @action }} -->
          </tbody>
					<tr>
						<td colspan="4" style="text-align:right;font-size:10px;"><b>Total Deviden</b></td>
						<td id="total-deviden"></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:right;font-size:10px;"><b>Harga Perlembar</b></td>
						<td id="total-lembar"></td>
					</tr>
        </table>
      </div>
    </div>

    <div class="row" style="margin-top:20px;">
      <div class="col-md-12">

        <div class="row" style="padding-bottom:10px">
          <div class="col-md-6">
            <h5 style="padding-bottom:9px;" style="float:left;">Transaksi & Perhitungan</h5>
          </div>
          <div class="col-md-6">
            <button id="oo-payments-ps" class="btn btn-xs btn-primary text-uppercase" type="button" style="float:right;">Sembuyikan <i class="fa fa-arrow-up"></i></button>
          </div>
        </div>

        <table id="payments-ps" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="350">Nama Dokter</th>
              <th width="30">Lembar</th>
              <th width="150">Total</th>
              <th width="160">Jenis Pembayaran</th>
              <th width="200">No Rekening</th>
              <th width="200">Nama Bank</th>
              <th width="200">Tanggal</th>
              <th width="100">Action</th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
            </tr>
          </thead>
          <tbody>
            <!-- {{ @action }} -->
          </tbody>
        </table>

				<table class="table table-bordered table-striped" style="width:400px;float:right;margin-top:20px;">
					<tr>
						<td colspan="2"><b>Summary</b></td>
					</tr>
					<tr>
						<th>Transfer</th>
						<td id="payments-transfer"></td>
					</tr>
					<tr>
						<th>Cash</th>
						<td id="payments-cash"></td>
					</tr>
				</table>
      </div>
    </div>

    <hr>

    <div class="row" style="padding: 0 0 10px 0">
      <div class="col-md-12">
        <div style="float:right;">
          <button id="save-payments-detail" class="btn btn-success btn-sm text-uppercase" type="button" style="width:100px;">Simpan</button>
          &nbsp;<a href="<?='{site_url}Tc_arm/payments/'.$this->uri->segment(3).'/'.$this->uri->segment(4);?>" class="btn btn-danger btn-sm text-uppercase" type="button" style="width:100px;">Batal</a>
        </div>
      </div>
    </div>
	</div>

	<input id="total-lembaran" type="text" hidden="true" readonly>
	<input id="detailps" type="text" hidden="true" readonly>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/url.js"></script>
<script src="{js_path}custom/number_format.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
		$('#save-payments-detail').click(function() {
			var detailps = $('#detailps').val();
					id 			 = '<?=$this->uri->segment(3)?>';

			$.ajax({
				url: '{site_url}tc_arm/savePaymentsDetail',
				type: 'POST',
				data: {detailps: detailps, id:id},
				complete: function() {
					swal("Berhasil!", "Melakukan Pembayaran kepada Pemilik Saham", "success").then(function() {
						window.location.assign('{site_url}Tc_arm/payments/'+'<?=$this->uri->segment(3)?>'+"/"+'<?=$this->uri->segment(4)?>');
					});
				}
			});

		});
    // @common &start
    var con1 = 0;
    $('#oo-paymentsdetail').click(function() {
      if (con1++ % 2 === 0) {
        $('#oo-paymentsdetail i').removeClass('fa-arrow-up');
        $('#oo-paymentsdetail i').addClass('fa-arrow-down');
      } else {
        $('#oo-paymentsdetail i').removeClass('fa-arrow-down');
        $('#oo-paymentsdetail i').addClass('fa-arrow-up');
      }

      $('#paymentsdetail').toggle('slow');
    });

    var con2 = 0;
    $('#oo-payments-ps').click(function() {
      if (con2++ % 2 === 0) {
        $('#oo-payments-ps i').removeClass('fa-arrow-up');
        $('#oo-payments-ps i').addClass('fa-arrow-down');
      } else {
        $('#oo-payments-ps i').removeClass('fa-arrow-down');
        $('#oo-payments-ps i').addClass('fa-arrow-up');
      }

      $('#payments-ps').toggle('slow');
    });

		var year = $('#periodyears').val();
    // @common &end

		// @here
		$.ajax({
			url: '{site_url}Tc_arm/showPaymentsB/'+'<?=$this->uri->segment(3)?>',
			dataType: 'json',
			success: function(data) {
				$('#name-carm').val(data[0].nama_carm);
				$('#potongan-rs').val(data[0].potongan_rs);
				$('#periodyears').val(data[0].tahun);
				$('#name-payments').val(data[0].nama_transaksi);
			}
		}).done(function(data) {
			var months 		 = data[0].bulan.split(','),
					new_months = "";

			for (var i = 0; i < months.length; i++) {
				if (months.length - 1 == i) {
					new_months += months[i];
				} else {
					new_months += months[i]+" ";
				}
			}

			paymentsDetail(String(new_months), String(data[0].tahun));

			$.ajax({
				url: "{site_url}Tc_arm/showPaymentsMonths/"+data[0].id_carm+"/"+'<?=$this->uri->segment(4)?>'+"/"+1,
				dataType: 'json',
				success: function(data) {
					for (var iter = 0; iter < data.length; iter++) {
						$('#payment-months').append('<option id="'+data[iter].periodebulan+data[iter].periodetahun+'" value="'+data[iter].periodebulan+'">'+data[iter].periodebulan+'</option>');

						if (data[iter].periodebulan == months[iter]) {
							var selector = '#'+data[iter].periodebulan+data[iter].periodetahun;
							$(selector).attr('selected', 'selected');
						}
					}

					$('#payment-months').select2();
				}
			});
		});

		// @here

		function paymentsDetail(months, year) {
			var total1 = 0,
					total2 = 0;

			var paymentsdetail = $('#paymentsdetail').DataTable({
				"oLanguage":{
					"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
				},
				"ajax": {
					"url":"{site_url}Tc_arm/showPaymentsDetail/"+year+"/"+months,
				},
				"scrollX": true,
				"autoWidth": false,
				"lengthChange": false,
				"ordering": false,
				"searching": false,
				"processing": true,
				"bInfo": false,
				"bPaginate": false,
				"order": [],
				"rowCallback": function(row, data, index) {
					$('td:eq(5)', row).attr('hidden', '');
					total1 = total1 + parseInt(data[5].replace(/,/g, ''));
					total2 = total2 + parseInt(data[4].replace(/,/g, ''));

					$('td:eq(2)', row).html(number_format(parseInt(data[1].replace(/,/g, '') * parseInt(data[2]) / 100)));

					$('#total-lembaran').val(total1);
					$('#total-lembar').text(number_format(total1))

					$('#total-deviden').text(number_format(total2));
				},
				"initComplete": function(settings, json) {
					payments_Ps(total1);
  			}
			});

		}

		function payments_Ps(total) {
			var transfer = 0,
					cash = 0;

			var payments_ps = $('#payments-ps').DataTable({
				"oLanguage":{
					"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
				},
				"ajax": {
					"url":"{site_url}Tc_arm/showPaymentsPS/"+'<?=$this->uri->segment(3)?>',
				},
				"scrollX": true,
				"autoWidth": true,
				"lengthChange": false,
				"ordering": false,
				"searching": false,
				"processing": true,
				"bInfo": false,
				"bPaginate": false,
				"order": [],
				"rowCallback": function(row, data, index) {
					$('td:eq(2)', row).html(number_format(data[1] * total));
					$('td:eq(8)', row).attr('hidden', '');
					$('td:eq(9)', row).attr('hidden', '');
					$('td:eq(10)', row).attr('hidden', '');
					$('td:eq(11)', row).attr('hidden', '');
					$('td:eq(12)', row).attr('hidden', '');
					$('td:eq(13)', row).attr('hidden', '');
					$('td:eq(14)', row).attr('hidden', '');
					$('td:eq(15)', row).attr('hidden', '');
					$('td:eq(16)', row).attr('hidden', '');
					$('td:eq(17)', row).attr('hidden', '');

					if (data[8] == 1) {
						transfer = (transfer + data[1] * total);
					}

					if (data[8] == 0) {
						cash = (cash + data[1] * total);
					}

					$('#payments-transfer').text(number_format(transfer));
					$('#payments-cash').text(number_format(cash));
				},
				"initComplete": function(settings, json) {
					$('.datepayments').datepicker({
						format: "yyyy-mm-dd"
					});
					getDetailTable();
				}
			});
		}

		$(document).on('click', '.edit-method', function() {
			$(event.target).closest('tr').each(function() {
				$(this).find('td:eq(3) .method-transfer').removeAttr('disabled');
				$(this).find('td:eq(6) .datepayments').removeAttr('disabled');
				$(this).find('td:eq(7) .check-method').removeAttr('disabled');

				$(this).find('td:eq(17)').text('').append(($(this).find('td:eq(8)').text()))
			});
		});

		$(document).on('click', '.check-method', function() {
			$(event.target).closest('tr').each(function() {
				$(this).find('td:eq(3) .method-transfer').attr('disabled', 'disabled');
				$(this).find('td:eq(6) .datepayments').attr('disabled', 'disabled');
				$(this).find('td:eq(7) .check-method').attr('disabled', 'disabled');

				date = $(this).find('td:eq(6) .datepayments').val();
				$(this).find('td:eq(16)').text('').append(date);

				var condition = parseInt($(this).find('td:eq(8)').text());

				cash  	= parseInt($('#payments-cash').text().replace(/,/g, ''));
				transfer = parseInt($('#payments-transfer').text().replace(/,/g, ''));

				price = parseInt($(this).find('td:eq(2)').text().replace(/,/g, ''));

				if (condition !== parseInt($(this).find('td:eq(17)').text())) {
					if (condition == 1) {
						plus 	= transfer + price;
						minus = cash - price;

						$('#payments-transfer').text(number_format(plus));
						$('#payments-cash').text(number_format(minus));
					} else {
						plus 	= cash + price;
						minus = transfer - price;

						$('#payments-transfer').text(number_format(minus));
						$('#payments-cash').text(number_format(plus));
					}
				}

			});
		});

		function getDetailTable() {
			var detail_table = $('#payments-ps tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});

			$("#detailps").val(JSON.stringify(detail_table));
		}


		$(document).on('change', '.method-transfer', function() {
			var method = $(this).val();

			$(event.target).closest('tr').each(function() {
				var id 			= parseInt($(this).find('td:eq(11)').text()),
						bank 		= $(this).find('td:eq(14)').text(),
						rekening = $(this).find('td:eq(15)').text();

				if (parseInt(method) == 1) {
					$(this).find('td:eq(4) .bank').val(bank);
					$(this).find('td:eq(5) .number_rek').val(rekening);

					$(this).find('td:eq(8)').text('').append(1);
					$(this).find('td:eq(9)').text('').append(bank);
					$(this).find('td:eq(10)').text('').append(rekening);
					getDetailTable();
				} else {
					$(this).find('td:eq(8)').text('').append(0);
					$(this).find('td:eq(9)').text('').append("-");
					$(this).find('td:eq(10)').text('').append("-");

					$(this).find('td:eq(4) .bank').val("-");
					$(this).find('td:eq(5) .number_rek').val("-");
					getDetailTable();
				}

			});

		});


  });
</script>
