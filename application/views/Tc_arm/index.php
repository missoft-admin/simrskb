<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
    <hr>
	</div>
	<div class="block-content">
		<table id="tcarm" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No Registrasi</th>
					<th>Nama</th>
					<th>Total Lembaran</th>
          <th>Status</th>
          <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<!-- {{ @action }} -->
			</tbody>
		</table>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    var showCarm = $('#tcarm').DataTable({
      "oLanguage":{
	      "sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
	    },
			"ajax": {
				"url":"{site_url}Tc_arm/showCarm",
				"type": "POST"
			},
      "autoWidth": false,
      "lengthChange": true,
      "ordering": true,
      "searching": true,
      "processing": true,
      "bInfo": true,
      "bPaginate": true,
      "order": []
    });

		// @function:status-carm &start
		$(document).on('click', '.status-carm', function() {
			var status = $(this).attr('data-value');
			$(event.target).closest('tr').each(function () {
				var id = $(this).find('td:eq(0)').text();
				if (parseInt(status) == 1) {
					status = 0;
				} else {
					status = 1;
				}

				$.ajax({
					url: '{site_url}Mc_arm/changeStatusCarm',
					type: 'POST',
					dataType: 'json',
					data: {id:id, status:status},
					complete: function() {
						showCarm.ajax.reload();
						swal("Berhasil!", "Berhasil memperbaharui status!", "success");
					}
				});
			});
		});
		// @function:status-carm &end
  });
</script>
