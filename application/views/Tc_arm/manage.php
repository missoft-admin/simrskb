<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">

    </ul>
		<h3 class="block-title">{title}</h3>
    <hr>
	</div>
	<div class="block-content">
    <form method="post" action="{site_url}Tc_arm/saveTransaction" id="form-mcarm" class="form-horizontal" style="margin-top:-15px;">
			<div class="row">

				<div class="col-md-5">

					<div class="form-group">
						<label class="col-md-4 control-label text-uppercase" for="name-tarif" style="float:left;">Nama</label>
						<div class="col-md-8">
							<input type="text" id="name-tarif" class="form-control" name="name-tarif" required="" aria-required="true" readonly>
						</div>
					</div>

          <div class="form-group">
						<label class="col-md-4 control-label text-uppercase" for="name-period" style="float:left;">Periode</label>
						<div class="col-md-8">
							<input type="text" id="name-period" class="form-control" name="name-period" required="" aria-required="true" readonly>
						</div>
					</div>

          <div class="form-group">
						<label class="col-md-4 control-label text-uppercase" for="total-pemilik-saham" style="float:left;">Jumlah PS</label>
						<div class="col-md-8">
							<input type="text" id="total-pemilik-saham" class="form-control" name="total-pemilik-saham" required="" aria-required="true" readonly>
						</div>
					</div>

				</div>

        <div class="col-md-7">
          <div class="form-group">
						<label class="control-label text-uppercase col-md-3" for="note-carm" style="float:left;">Catatan</label>
						<div class="col-md-8">
							<textarea id="note-carm" class="form-control" name="note-carm" rows="6" cols="80" readonly></textarea>
						</div>
					</div>
        </div>

				<!-- @here &start -->
				<input id="id-tarif" type="text" name="id-tarif" hidden="true" readonly>
				<input id="id-transaksi" type="text" name="id-transaksi" hidden="true" readonly>
				<!-- @here &end -->

				<input id="total-pendapatan" type="text" name="total-pendapatan" hidden="true" readonly>
				<input id="total-operasional" type="text" name="total-operasional" hidden="true" readonly>
				<input id="total-potongan" type="text" name="total-potongan" hidden="true" readonly>
				<input id="total-deviden" type="text" name="total-deviden" hidden="true" readonly>
				<input id="total-perlembar" type="text" name="total-perlembar" hidden="true" readonly>
				<input id="detail-biaya-operasional" type="text" name="detail-biaya-operasional" hidden="true" readonly>

				<input id="id-sewaalat" type="text" name="id-sewaalat" hidden="true" readonly>

				<input id="action" type="text" name="action" hidden="true" readonly>
		</form>

    <div class="col-md-12">
      <hr>
    </div>

    <div class="col-md-12">

      <div class="row" style="padding-bottom:10px">
        <div class="col-md-6">
          <h5 style="text-transform:uppercase;padding-bottom:9px;float:left;">Detail Transaksi C - ARM</h5>
        </div>
        <div class="col-md-6">
          <button id="oo-carm-trancasction" class="btn btn-xs btn-primary text-uppercase" type="button" style="float:right;">Sembuyikan <i class="fa fa-arrow-up"></i></button>
        </div>
      </div>

      <div id="carm-transaction">
        <table id="carm-transaction-detail" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No Registrasi</th>
              <th>No Medrec</th>
              <th>Nama Pasien</th>
              <th>Kelompok Pasien</th>
              <th>Asuransi</th>
              <th>Kelas</th>
              <th>Jenis Operasi</th>
              <th>Tanggal Operasi</th>
              <th>Status</th>
              <th>Nominal</th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
							<th hidden></th>
            </tr>
          </thead>
          <tbody>
            <!-- {{ @action }} -->
          </tbody>
        </table>
      </div>
    </div>

    <div class="col-md-12">
      <hr>
    </div>

    <div class="col-md-12">

      <div class="row" style="padding-bottom:10px">
        <div class="col-md-6">
          <h5 style="text-transform:uppercase;padding-bottom:9px;float:left;">Biaya Operasional</h5>
        </div>
        <div class="col-md-6">
          <button id="oo-tcarm-operasional" class="btn btn-xs btn-primary text-uppercase" type="button" style="float:right;">Sembuyikan <i class="fa fa-arrow-up"></i></button>
        </div>
      </div>

      <div id="tcarm-operasional">
        <table id="tcarm-operasional-detail" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tanggal</th>
              <th>Deskripsi Biaya</th>
              <th>Jumlah</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody id="operasional-detail">
						<!-- {{ @action }} -->
          </tbody>
					<tr>
						<td><input type="text" id="date-operasional" class="form-control"></td>
						<td><input type="text" id="desc-operasional" class="form-control"></td>
						<td><input type="number" id="nominal-operasional" class="form-control"></td>
						<td><button id="save-operasional" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i></button></td>
					</tr>
        </table>
      </div>

    </div>

		<div class="col-md-12">
			<hr>
		</div>

		<div class="col-md-12" style="padding:3px 20px 40px 30px;">
			<div class="col-md-12" style="padding-top:20px;padding-bottom:10px">
				<table id="tcarm-deviden-detail" class="table table-bordered">
					<tr>
						<th class="text-center active" colspan="2" style="font-size:15px;">Perhitungan Pendapatan & Pengeluaran</th>
					</tr>
					<tr>
						<th class="text-right" width="20%">Total Pendapatan</th>
						<td width="15%"><span id="show-total-pendapatan" style="float:right;">
							<!-- {{ @action }} -->
						</span></td>
					</tr>
					<tr>
						<th class="text-right active">Total Biaya Operasional</th>
						<td><span id="show-total-biaya" style="float:right;">
							<!-- {{ @action }} -->
						</span></td>
					</tr>
					<tr>
						<th class="text-right">Potongan Rumah Sakit</th>
						<td><span id="show-total-potongan" style="float:right;">
							<!-- {{ @action }} -->
						</span></td>
						<td class="text-center info" width="8%"><span id="show-percent">20</span> %</td>
						<td width="0.5%"><a class="mcarm btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a></td>
					</tr>
					<tr>
						<th class="text-right active">Pendapatan Bersih (Deviden)</th>
						<td><span id="show-total-deviden" style="float:right;">
							<!-- {{ @action }} -->
						</span></td>
					</tr>
					<tr>
						<th class="text-right">Pendapatan Perlembar</th>
						<td><span id="show-total-perlembar" style="float:right;">
							<!-- {{ @action }} -->
						</span></td>
						<td class="text-center info"><span id="show-lembar">20</span> Lbr</td>
						<td><a class="mcarm btn btn-xs btn-primary"><i class="fa fa-eye"></i></a></td>
						<td width="54%" style="border:1px solid white;"></td>
					</tr>
				</table>
			</div>
			<div class="col-md-6">
				<button id="save-carm-transaction" class="btn btn-sm btn-success text-uppercase" type="submit" name="save-carm-transaction" style="width:110px;" value="0">Simpan</button>&nbsp;
				<a href="<?='{site_url}Tc_arm/periodMonths/'.$this->uri->segment(3)?>" id="cancel-carm-transaction" class="btn btn-sm btn-warning text-uppercase" type="button" name="button" style="width:110px;">Batal</a>&nbsp;
				<button id="print-carm-transcation" class="btn btn-sm btn-primary text-uppercase" type="button" name="button" style="width:110px;"><i class="fa fa-print"></i>&nbsp;Rincian</button>
				<button id="force-close" class="btn btn-sm btn-danger text-uppercase" type="button" name="force-close" value="1" style="width:110px;"><i class="fa fa-close"></i>&nbsp;Force Close</button>
			</div>
		</div>

	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}custom/url.js"></script>

<script type="text/javascript">
// @common &start
	var ids = new Array();
	var i = 0;

	var eoperasionalbtn = "<button class='btn btn-sm btn-primary edit-operasional-biaya' type='button'><i class='fa fa-pencil'></i></button>",
			doperasionalbtn = "<button class='btn btn-sm btn-danger del-operasional-biaya' type='button'><i class='fa fa-close'></i></button>";

		$('#id-tarif').val('<?=$this->uri->segment(3)?>');
		$('#id-transaksi').val('<?=$this->uri->segment(5)?>');

		function clearValue() {
			$('#date-operasional, #desc-operasional, #nominal-operasional').val('');
		}

		function getDetailTable() {
			var detail = $('#tcarm-operasional-detail #operasional-detail tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});
			$("#detail-biaya-operasional").val(JSON.stringify(detail));
		}

		function getDetailDeviden() {
			var detail = $('#tcarm-deviden-detail tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});
			$("#detail-carm-transaction").val(JSON.stringify(detail));
		}
	// @common &end

  $(document).ready(function () {
    // @common &start

		$('.mcarm').attr('href', '{site_url}Mc_arm/create/'+'<?=$this->uri->segment(3)?>');
	// $('td:eq(8)', row).attr('hidden', '');
					// $('td:eq(9)', row).attr('hidden', '');
    var con1 = 0;
    $('#oo-carm-trancasction').click(function() {
      if (con1++ % 2 === 0) {
        $('#oo-carm-trancasction i').removeClass('fa-arrow-up');
        $('#oo-carm-trancasction i').addClass('fa-arrow-down');
      } else {
        $('#oo-carm-trancasction i').removeClass('fa-arrow-down');
        $('#oo-carm-trancasction i').addClass('fa-arrow-up');
      }

      $('#carm-transaction').toggle('slow');
    });

    var con2 = 0;
    $('#oo-tcarm-operasional').click(function() {
      if (con2++ % 2 === 0) {
        $('#oo-tcarm-operasional i').removeClass('fa-arrow-up');
        $('#oo-tcarm-operasional i').addClass('fa-arrow-down');
      } else {
        $('#oo-tcarm-operasional i').removeClass('fa-arrow-down');
        $('#oo-tcarm-operasional i').addClass('fa-arrow-up');
      }

      $('#tcarm-operasional').toggle('slow');
    });
    // @common &end

    arr  = '<?=$this->uri->segment(4)?>'.split("%");

    $.ajax({
  		url: '{site_url}Tc_arm/showCarmDetail/'+'<?=$this->uri->segment(3)?>',
  		dataType: 'json',
  		success: function(data) {
  			$('#name-tarif').val(data[0].nama_carm);
        $('#name-period').val(arr[0]+" "+arr[1]);
        $('#total-pemilik-saham').val(data[0].totallembaran);
        $('#note-carm').val(data[0].catatan);

				$('#show-percent').text(data[0].potongan_rs);
				$('#show-lembar').text(data[0].totallembaran);
  		}
  	});

		if ('<?=$this->uri->segment(5)?>' != '') {
			var url = "{site_url}Tc_arm/showDetailInMonths/"+'<?=$this->uri->segment(3)?>'+"/"+arr[1]+"/"+arr[0]+"/"+'<?=$this->uri->segment(5)?>';

			$.ajax({
				url: '{site_url}Tc_arm/showTransactionId/'+'<?=$this->uri->segment(5)?>',
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					if (data.status == 2 || data.status == 1) {
						if (data.statuspayment == 1) {
							$('#save-carm-transaction').attr('disabled', 'disabled');
						}
						$('#force-close').attr('disabled', 'disabled');
					}
				}
			});

		} else {
			var url = "{site_url}Tc_arm/showDetailInMonths/"+'<?=$this->uri->segment(3)?>'+"/"+arr[1]+"/"+arr[0];
		}

		var nominal = 0;
    var showPeriodInMonths = $('#carm-transaction-detail').DataTable({
      "oLanguage":{
	      "sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
	    },
			"ajax": {
				"url":url,
				"type": "POST"
			},
      "autoWidth": false,
      "lengthChange": true,
      "ordering": true,
      "searching": true,
      "processing": true,
      "bInfo": true,
      "bPaginate": true,
      "order": [],
			 "rowCallback": function(row, data, index) {
				 	$('td:eq(10)', row).attr('hidden', '');
					$('td:eq(11)', row).attr('hidden', '');
					$('td:eq(12)', row).attr('hidden', '');
					$('td:eq(13)', row).attr('hidden', '');

					ids[i++] = parseInt(data[11]);

					nominal += parseInt(data[10]);
					$('#show-total-pendapatan').attr('data-value', nominal);
					$('#show-total-pendapatan').text(number_format(nominal));
					$('#id-sewaalat').val(ids);
					if ('<?=$this->uri->segment(5)?>' != '') {
						$.ajax({
							url: '{site_url}Tc_arm/showTransactionId/'+'<?=$this->uri->segment(5)?>',
							dataType: 'json',
							success: function(data) {
								$('#show-total-pendapatan').text(number_format(data.total_pendapatan));
								$('#total-pendapatan').val(data.total_pendapatan);

								$('#show-total-biaya').text(number_format(data.total_operasional));
								$('#total-operasional').val(data.total_operasional);

								$('#show-total-potongan').text(number_format(data.total_potongan));
								$('#total-potongan').val(data.total_potongan);

								$('#show-total-deviden').text(number_format(data.total_deviden));
								$('#total-deviden').val(data.total_deviden);

								$('#show-total-perlembar').text(number_format(data.total_perlembar));
								$('#total-perlembar').val(data.total_perlembar);
							}
						});
					} else {
						calculateDeviden(nominal, 0);
					}
			 }
    });

		function calculateDeviden(total_pendapatan, biaya_operasional) {
			var total_perlembar, total_deviden, total_potongan;

			$.ajax({
				url: '{site_url}Tc_arm/showCarmDetail/'+'<?=$this->uri->segment(3)?>',
				dataType: 'json',
				success: function(data) {
					total_potongan  = (total_pendapatan * (data[0].potongan_rs / 100));
					total_deviden = total_pendapatan - total_potongan - biaya_operasional;
					total_perlembar = total_deviden / data[0].totallembaran;
				}
			}).done(function(data) {
				$('#show-total-pendapatan').text(number_format(total_pendapatan));
				$('#total-pendapatan').val(total_pendapatan);

				$('#show-total-biaya').text(number_format(biaya_operasional));
				$('#total-operasional').val(biaya_operasional);

				$('#show-total-potongan').text(number_format(total_potongan));
				$('#total-potongan').val(total_potongan);

				$('#show-total-deviden').text(number_format(total_deviden));
				$('#total-deviden').val(total_deviden);

				$('#show-total-perlembar').text(number_format(total_perlembar));
				$('#total-perlembar').val(total_perlembar);

				getDetailDeviden();
			});

		}

		function showBiayaOperasional() {
			var table;

			$.ajax({
				url: '{site_url}Tc_arm/showTransactionDetail/'+'<?=$this->uri->segment(5)?>',
				dataType: 'json',
				success: function(data) {
					$('#tcarm-operasional-detail #operasional-detail').empty();
					for (var iter = 0; iter < data.length; iter++) {
						table = '<tr>';
						table += '<td>'+data[iter].tanggal+'</td>';
						table += '<td>'+data[iter].deskripsi+'</td>';
						table += '<td>'+number_format(data[iter].total)+'</td>';
						table += '<td>'+eoperasionalbtn+'&nbsp;'+doperasionalbtn+'</td>';
						table += '</tr>';

						$('#tcarm-operasional-detail #operasional-detail').append(table);
					}
				}
			});
		}


		if ('<?=$this->uri->segment(5)?>' != '') {
			showBiayaOperasional();
		}

		$('#date-operasional').datepicker({
			format: "yyyy-mm-dd",
			todayHighlight: true,
		});

		$('#save-operasional').click(function() {
			var date 		= $('#date-operasional').val(),
					desc 		= $('#desc-operasional').val(),
					nominal = $('#nominal-operasional').val();

			if (date !== "" && desc !== "" && nominal !== "") {
				el = '<tr>';
				el += '<td>'+date+'</td>';
				el += '<td>'+desc+'</td>';
				el += '<td>'+number_format(nominal)+'</td>';
				el += '<td>'+eoperasionalbtn+" "+doperasionalbtn+'</td>';
				el += '</tr>'
				$('#tcarm-operasional-detail #operasional-detail').append(el);

				// @change to be function &start
				var total_pendapatan = parseInt($('#show-total-pendapatan').attr('data-value')),
				temp_biaya  = parseInt($('#show-total-biaya').text().replace(/,/g, ''));
				total_biaya = parseInt(nominal) + temp_biaya;

				calculateDeviden(total_pendapatan, total_biaya);
				// @change to be function &end

				getDetailTable();
				clearValue();
			}

			getDetailDeviden();
		});

		$(document).on('click', '.del-operasional-biaya', function() {
			// 		tipetransaksi = $(this).find('td:eq(0)').text(),
			// 		idpelayanan   = $(this).find('td:eq(0)').text(),
			// 		idkelas 			= $(this).find('td:eq(0)').text(),
			// 		tarif 				= $(this).find('td:eq(0)').text(),
			// 		kuantitas 		= $(this).find('td:eq(0)').text();on () {
			var total = parseInt($(this).closest('tr').find('td:eq(2)').text().replace(/,/g, ''));
			var biaya_operasional = parseInt($('#show-total-biaya').text().replace(/,/g, '')) - total;

			calculateDeviden(parseInt($('#show-total-pendapatan').text().replace(/,/g, '')), biaya_operasional);
			$(this).closest('tr').remove();
			getDetailTable();

			getDetailDeviden();
		});

		$(document).on('click', '.edit-operasional-biaya', function() {
			$(event.target).closest('tr').each(function() {
				var date 		 = $(this).find('td:eq(0)').text(),
						describe = $(this).find('td:eq(1)').text(),
						total		 = $(this).find('td:eq(2)').text();

				var in1 = '<input class="form-control date cdate-operasional " type="text" value="'+date+'">',
						in2 = '<input class="form-control cdesc-operasional" type="text" value="'+describe+'">',
						in3 = '<input class="form-control cnominal-operasional" type="number" value="'+total.replace(/,/g, '')+'">';

				var act = '<button type="button" class="btn btn-sm btn-info sedit-operasional"><i class="fa fa-check"></i></button>&nbsp;';
						act += '<button type="button" class="btn btn-sm btn-warning uedit-operasional" data-value="'+date+'|'+describe+'|'+total.replace(/,/g, '')+'"><i class="fa fa-undo"></i></button>';


				$(this).find('td:eq(0)').text('').append(in1);
				$(this).find('td:eq(1)').text('').append(in2);
				$(this).find('td:eq(2)').text('').append(in3);
				$(this).find('td:eq(3)').text('').append(act);

				$(this).append('<td hidden>'+total+'</td>');

				$('.date').datepicker({
					format: "yyyy-mm-dd"
				});
			});
		});

		$(document).on('click', '.sedit-operasional', function() {
			$(event.target).closest('tr').each(function() {
				var date_temp = $(this).find('td:eq(0) .cdate-operasional').val(),
						desc_temp = $(this).find('td:eq(1) .cdesc-operasional').val(),
						nomi_temp = $(this).find('td:eq(2) .cnominal-operasional').val();

						total_temp = parseInt($(this).find('td:eq(4)').text().replace(/,/g, ''));

				var biaya_operasional = parseInt($('#show-total-biaya').text().replace(/,/g, '')) - total_temp;
						result = biaya_operasional + parseInt(nomi_temp.replace(/,/g, ''));

				calculateDeviden(parseInt($('#show-total-pendapatan').text().replace(/,/g, '')), result);

				$(this).find('td:eq(0)').text('').append(date_temp);
				$(this).find('td:eq(1)').text('').append(desc_temp);
				$(this).find('td:eq(2)').text('').append(nomi_temp);
				$(this).find('td:eq(3)').text('').append(eoperasionalbtn+"&nbsp"+doperasionalbtn);

				getDetailTable();
				getDetailDeviden();
			});
		});

		$(document).on('click', '.uedit-operasional', function() {
			var val = $(this).attr('data-value').split('|');

			$(event.target).closest('tr').each(function () {
				$(this).find('td:eq(0)').text('').append(val[0]);
				$(this).find('td:eq(1)').text('').append(val[1]);
				$(this).find('td:eq(2)').text('').append(val[2]);
				$(this).find('td:eq(3)').text('').append(eoperasionalbtn+"&nbsp"+doperasionalbtn);
			})
		});

		$(document).on('click', '#force-close, #save-carm-transaction', function () {
			$('#action').val($(this).val());
		});

		// @ --
		$('#force-close').click(function() {
			swal({
		    title: 'Apakah anda yakin akan <span style="font-weight:bold;">Closing Transaksi</span> ini ?',
		    text : "Seluruh Transaksi bulan ini akan otomatis berhenti dan akan dimasukan pada bulan berikutnya.",
		    type : "success",
		    showCancelButton: true,
		    confirmButtonText: "Ya",
		    confirmButtonColor: "#34a263",
		    cancelButtonText: "Batalkan",
		  }).then(function() {
		    $.ajax({
		      url: '{site_url}Tc_arm/saveTransaction',
		      type: 'POST',
		      dataType: 'json',
		      data: $('#form-mcarm').serialize(),
		      complete: function() {
		        swal("Berhasil!", "Transaksi C-ARM sudah di Closing .", "success").then(function() {
							window.location.assign('{site_url}Tc_arm');
						});
		      },
		      error: function(data) {
		        swal("Oops", "We couldn't connect to the server!", "error");
					}
		    });
		  });
		});
		// @ --
	});
</script>
