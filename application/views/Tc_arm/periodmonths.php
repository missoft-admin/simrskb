<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="<?='{site_url}Tc_arm/periodYears/'.$this->uri->segment(3)?>" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
    <hr>
	</div>
	<div class="block-content" style="text-transform:uppercase;">
		<table id="periode-tcarm-years" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Periode</th>
					<th>Total Pendapatan</th>
          <th>Status</th>
          <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<!-- {{ @action }} -->
			</tbody>
		</table>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>

<script src="{js_path}custom/url.js"></script>
<script type="text/javascript">

  $(document).ready(function () {
    var showPeriodInMonths = $('#periode-tcarm-years').DataTable({
      "oLanguage":{
	      "sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
	    },
			"ajax": {
				"url":"{site_url}Tc_arm/showPeriodInMonths/" + '<?=$this->uri->segment(3)?>',
				"type": "POST"
			},
      "autoWidth": false,
      "lengthChange": true,
      "ordering": true,
      "searching": true,
      "processing": true,
      "bInfo": true,
      "bPaginate": true,
      "order": []
    });

  });
</script>
