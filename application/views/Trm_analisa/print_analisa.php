<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Berkas Analisa</title>
    <style>
    body {
        -webkit-print-color-adjust: exact;
    }

    @media screen {
        * {
            font-size: 14px !important;
        }

        table {
            font-size: 14px !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        td {
            padding: 3px;
        }

        .content td {
            padding: 0px;
        }

        .content-2 td {
            margin: 3px;
        }

        /* border-normal */
        .border-full {
            border: 1px solid #000 !important;
        }

        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }

        /* border-thick */
        .border-thick-top {
            border-top: 2px solid #000 !important;
        }

        .border-thick-bottom {
            border-bottom: 2px solid #000 !important;
        }

        .border-dotted {
            border-width: 1px;
            border-bottom-style: dotted;
        }

        /* text-position */
        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        /* text-style */
        .text-italic {
            font-style: italic;
        }

        .text-bold {
            font-weight: bold;
        }
    }

    </style>
    <script type="text/javascript">
    try {
        this.print();
    } catch (e) {
        window.onload = window.print;
    }

    </script>
</head>

<body>
    <table class="content-2">
        <tr>
            <td rowspan="6" style="width:10%"><img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"></td>
        </tr>
        <tr>
            <td style="width:150px">NOMOR REGISTER</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $notransaksi; ?></td>

            <td style="width:150px">TANGGAL LAHIR</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $tanggal_lahir; ?></td>
        </tr>
        <tr>
            <td style="width:150px">NO REKAM MEDIS</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $nomedrec; ?></td>

            <td style="width:150px">DPJP</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $dokter_pj; ?></td>
        </tr>
        <tr>
            <td style="width:150px">NAMA PASIEN</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $namapasien; ?></td>

            <td style="width:150px">R PERAWATAN</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $ruangan_perawatan; ?></td>
        </tr>
        <tr>
            <td style="width:150px">PETUGAS ANALISA</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $petugas_analisa; ?></td>

            <td style="width:150px">NAMA ANALISA</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?= $nama_analisa; ?></td>
        </tr>
    </table>

    <table>
        <tr>
            <td class="text-center border-full"><b>NO</b></td>
            <td class="text-center border-full"><b>NAMA LEMBARAN</b></td>
            <td class="text-center border-full"><b>KRITERIA</b></td>
            <td class="text-center border-full"><b>DETAIL</b></td>
            <td class="text-center border-full"><b>DOKTER</b></td>
            <td class="text-center border-full"><b>CATATAN</b></td>
        </tr>
        <?php $number = 1; ?>

        <?php foreach ($this->model->getSettingAnalisa($lembaran) as $index => $setting) { ?>

            <?php
            $berkasOnDB = getByQuery(
                array(
                    'output_type' => 'single',
                    'table' => 'trm_berkas_analisa',
                    'select' => array('*'),
                    'where' => array(
                    ['idberkas', $idberkas],
                    ['idanalisa', $idanalisa],
                    ['idlembaran', $setting['id']],
                    )
                )
            );

            if ($berkasOnDB) {
                $valKriteria = $berkasOnDB->kriteria_nilai;
                $valDetail = json_decode($berkasOnDB->detail);
                $valDokter = isset($berkasOnDB->dokter) ? array_column(json_decode($berkasOnDB->dokter), 'id') : array();
                $valCatatan = $berkasOnDB->catatan;
            } else {
                $valKriteria = '';
                $valDetail = array();
                $valDokter = array();
                $valCatatan = '';
            }
            ?>

            <tr>
                <td class="text-center border-full"><?= $index+1; ?></td>
                <td class="text-left border-full"><?= $setting['nama']; ?></td>
                <td class="text-center border-full">
                    <?php if ($setting['kriteria']) { ?>
                        <?php foreach ($setting['kriteria'] as $kriteria) { ?>
                            <?= ($valKriteria == $kriteria->nilai ? $kriteria->keterangan : ''); ?>
                        <?php } ?>
                    <?php } ?>
                </td>
                <td class="text-center border-full">
                    <?php if ($setting['detail']) { ?>
                        <?php foreach ($setting['detail'] as $detail) { ?>
                            <?= (in_array($detail->keterangan, $valDetail) ? $detail->keterangan : ''); ?>
                        <?php } ?>
                    <?php } ?>
                </td>
                <td class="text-center border-full">
                    <?php if ($setting['dokter']) { ?>
                        <?php foreach ($setting['dokter'] as $dokter) { ?>
                            <?= (in_array($dokter->id, $valDokter) ? $dokter->nama : ''); ?>
                        <?php } ?>
                    <?php } ?>
                </td>
                <td class="text-center border-full"><?= $valCatatan; ?></td>
            </tr>
        <?php } ?>
    </table>
</body>

</html>
