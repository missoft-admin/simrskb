<div class="block block-bordered">
  <div class="block-header">
    <ul class="block-options">
      <li>
        <a href="{site_url}trm_berkas" type="button"><i class="si si-action-undo"></i></a>
      </li>
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">Berkas Pasien</h3>
  </div>
  <div class="block-content">
    <form class="form-horizontal push-10-t" action="base_forms_elements.html" method="post" onsubmit="return false;">
    <div class="pull-r-l pull-t push">
      <table class="block-table text-center bg-primary border-b">
        <tbody>
          <tr>
            <td class="border-r bg-success" style="width: 20%;">
              <div class="push-30 push-30-t">
                <i class="si si-user fa-3x text-white-op"></i>
              </div>
            </td>
            <td>
              <div class="h4 text-left text-white font-w500"><?=$pasien['no_medrec']?></div>
              <div class="h4 text-left text-white font-w500"><?=$pasien['nama'].', '.$pasien['title']?></div>
              <div class="h4 text-left text-white text-muted text-uppercase push-5-t"><?=HumanDateShort($pasien['tanggal_lahir'])?></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="push-10-t">
      <div class="form-group" style="margin-bottom: 15px;margin-top: 5px;">
        <label class="col-md-2 control-label" for="filter_idjenis">Filter</label>
        <div class="col-md-10">
          <select id="filter_idjenis" name="filter_idjenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
            <option value="#" selected>- Semua Tujuan -</option>
            <option value="1">Poliklinik</option>
            <option value="2">Instalasi Gawat Darurat (IGD)</option>
            <option value="3">Rawat Inap</option>
            <option value="4">One Day Surgery (ODS)</option>
          </select>
        </div>
      </div>
      <table class="table table-bordered table-striped" id="tabel_berkas" style="width: 100%;">
        <thead>
          <tr>
            <th style="width:50%">Tanggal / Dokter</th>
            <th style="width:30%">Status</th>
            <th style="width:20%">Aksi</th>
            <th hidden>ID Berkas</th>
            <th hidden>ID Tujuan</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    </form>
  </div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<script type="text/javascript">
  var table;
  $(document).ready(function() {
      $(".disable").attr('disabled', true);
      loadBerkasPasien();

      var tipeAction = '{tipe}';
      var idberkas = '{idberkas}';
      var idpendaftaran = '{idpendaftaran}';
      var asalpendaftaran = '{asalpendaftaran}';

      if (tipeAction == 'analisa' && idberkas) {
        startAnalisa(idberkas, idpendaftaran, asalpendaftaran);
      }
      if (tipeAction == 'verifikasi' && idberkas) {
        startVerifikasi(idberkas, idpendaftaran, asalpendaftaran);
      }

      if (tipeAction == 'lihat' && idberkas) {
        startLihatBerkas(idberkas, idpendaftaran, asalpendaftaran);
      }
  });

  $(document).on("click", ".kembali", function() {
    var table = $('#tabel_berkas').DataTable();
    var row = table.row($(this).parents('tr')).index();
    var idberkas = table.cell(row, 3).data();

    swal({
      title: "Anda Yakin ?",
      text : "Akan Mengembalikan Berkas ?",
      type : "success",
      showCancelButton: true,
      confirmButtonText: "Ya",
      confirmButtonColor: "#34a263",
      cancelButtonText: "Batalkan",
    }).then(function() {

      table = $('#tabel_berkas').DataTable();
      $.ajax({
        url: '{site_url}trm_layanan_berkas/set_kembali',
        type: 'POST',
        data: { id: idberkas },
        complete: function() {
          $.toaster({ priority : 'success', title : 'Succes!', message : ' Selesai Dikirim' });
          table.ajax.reload( null, false );
        }
      });
    });
  });

  $(document).on("change", "#filter_idjenis", function() {
	   loadBerkasPasien();
		 clearForm();
	});

  function loadBerkasPasien() {
    var idpasien = $("#idpasien").val();
    var filter_idjenis = $("#filter_idjenis").val();

    $('#tabel_berkas').DataTable().destroy();

    var table = $('#tabel_berkas').DataTable({
      "pageLength": 10,
      "searching": false,
      "ordering": false,
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "fixedHeader": true,
      "order": [],
      "ajax": {
        url: '{site_url}trm_analisa/get_berkas_pasien/',
        type: "POST",
        dataType: 'json',
        data: {
          idpasien: idpasien,
          filter_idjenis: filter_idjenis
        }
      },
      columnDefs: [
        { "width": "60%", "targets": [0] },
        { "width": "30%", "targets": [1] },
        { "width": "10%", "targets": [2] },
        {  className: "text-center", targets:[0,1,2] },
        { "visible": false, "targets": [3,4] }
      ],
      rowCallback: function(row, data, index) {
        if (data[3] == '{idberkas}') {
          $(row).addClass("success");
        }
      }
    });
  }

  function startAnalisa(idberkas, idpendaftaran, asalpendaftaran) {
    closeHeaderGroupSection();

		$("#text_header").html('<span class="label label-primary">ANALISA BERKAS</span>');
		$("#btn_save").show();
		$("#btn_verifikasi").hide();

    $("#idberkas").val(idberkas);
		$("#idpendaftaran").val(idpendaftaran);
		$("#asalpendaftaran").val(asalpendaftaran);

		$("#isReadonly").val('0');

		var idpasien = $("#idpasien").val();

		$.ajax({
		  url: '{site_url}trm_berkas/get_header_kodetifikasi/',
		  type: "POST",
		  dataType: 'json',
		  data: {
        idpasien: idpasien,
        idberkas: idberkas
      },
		  success: function(data) {
			  if (data) {
  				$("#kod_user_daftar").val(data.user_daftar);
  				$("#kod_tanggalkunjungan").val(data.tanggal_trx);
  				$("#kod_poli").val(data.namapoliklinik);
  				$("#idtransaksi").val(data.id_trx);
  				$("#kod_kunjungan").val(data.tujuan);
  				$("#kod_dokter").val(data.namadokter);
  				$("#kod_tanggalkirim").val(data.tanggal_kirim);
  				$("#kod_user_kirim").val(data.user_kirim_nama);
  				$("#kod_tanggalkembali").val(data.tanggal_kembali);
  				$("#kod_lama_kirim").val(data.durasi_kirim);
  				$("#kod_lama_kembali").val(data.durasi_kembali);
  				$("#kod_status_pasien").val(data.status_pasien);
  				$("#kod_kasus").val(data.statuskasus_nama);
  				$("#idtujuan").val(data.tujuan_id);

  				if (data.tujuan_id < 3){
  					$("#div_kodefikasi_dokter").hide();
  					$("#div_kodefikasi_operasi").hide();
  				}else{
  					$("#div_kodefikasi_dokter").show();
  					$("#div_kodefikasi_operasi").show();
  				}

          $(".disable").attr('disabled', false);
          setDokter();
			  }
		  }
		});
	}

	function startVerifikasi(idberkas, idpendaftaran, asalpendaftaran) {
    closeHeaderGroupSection();

		$("#text_header").html('<span class="label label-success">VERIFIKASI BERKAS</span>');
		$("#btn_save_kodefikasi").hide();
		$("#btn_verifikasi").show();

    $("#idberkas").val(idberkas);
		$("#idpendaftaran").val(idpendaftaran);
		$("#asalpendaftaran").val(asalpendaftaran);

		$("#isReadonly").val('0');

		var idpasien = $("#idpasien").val();

		$.ajax({
		  url: '{site_url}trm_berkas/get_header_verifikasi/',
		  type: "POST",
		  dataType: 'json',
		  data: {
        idpasien: idpasien,
        idberkas:idberkas
      },
		  success: function(data) {
			  if (data){
  				$("#kod_user_daftar").val(data.user_daftar);
  				$("#kod_tanggalkunjungan").val(data.tanggal_trx);
  				$("#kod_poli").val(data.namapoliklinik);
  				$("#idtransaksi").val(data.id_trx);
  				$("#kod_kunjungan").val(data.tujuan);
  				$("#kod_dokter").val(data.namadokter);
  				$("#kod_tanggalkirim").val(data.tanggal_kirim);
  				$("#kod_user_kirim").val(data.user_kirim_nama);
  				$("#kod_tanggalkembali").val(data.tanggal_kembali);
  				$("#kod_lama_kirim").val(data.durasi_kirim);
  				$("#kod_lama_kembali").val(data.durasi_kembali);
  				$("#kod_status_pasien").val(data.status_pasien);
  				$("#kod_kasus").val(data.statuskasus_nama);
  				$("#idtujuan").val(data.tujuan_id);

  				if (data.tujuan_id < 3){
  					$("#div_kodefikasi_dokter").hide();
  					$("#div_kodefikasi_operasi").hide();
  				}else{
  					$("#div_kodefikasi_dokter").show();
  					$("#div_kodefikasi_operasi").show();
  				}
				 $(".disable").attr('disabled', false);

				 loadDokter();
				 loadDokterAnesthesi();
			  }
		  }
		});
	}

  function startLihatBerkas(idberkas, idpendaftaran, asalpendaftaran) {
    closeHeaderGroupSection();

		$("#text_header").html('<span class="label label-default">VIEW ANALISA</span>');
		$("#btn_save_kodefikasi").hide();
		$("#btn_verifikasi").hide();

    $("#idberkas").val(idberkas);
		$("#idpendaftaran").val(idpendaftaran);
		$("#asalpendaftaran").val(asalpendaftaran);

		$("#isReadonly").val('1');

		var idpasien = $("#idpasien").val();

		$.ajax({
		  url: '{site_url}trm_berkas/get_header_verifikasi/',
		  type: "POST",
		  dataType: 'json',
		  data: {
        idpasien: idpasien,
        idberkas:idberkas
      },
		  success: function(data) {
			  if (data) {
          $("#kod_user_daftar").val(data.user_daftar);
          $("#kod_tanggalkunjungan").val(data.tanggal_trx);
          $("#kod_poli").val(data.namapoliklinik);
          $("#idtransaksi").val(data.id_trx);
          $("#kod_kunjungan").val(data.tujuan);
          $("#kod_dokter").val(data.namadokter);
          $("#kod_tanggalkirim").val(data.tanggal_kirim);
          $("#kod_user_kirim").val(data.user_kirim_nama);
          $("#kod_tanggalkembali").val(data.tanggal_kembali);
          $("#kod_lama_kirim").val(data.durasi_kirim);
          $("#kod_lama_kembali").val(data.durasi_kembali);
          $("#kod_status_pasien").val(data.status_pasien);
          $("#kod_kasus").val(data.statuskasus_nama);
          $("#idtujuan").val(data.tujuan_id);

          if (data.tujuan_id < 3){
          	$("#div_kodefikasi_dokter").hide();
          	$("#div_kodefikasi_operasi").hide();
          }else{
          	$("#div_kodefikasi_dokter").show();
          	$("#div_kodefikasi_operasi").show();
          }

          setViewAuth();

          loadDokter();
          loadDokterAnesthesi();
			  }
		  }
		});
	}

  function setDokter() {
		if ($("#idtujuan").val() > '2') {
			var idberkas = $("#idberkas").val();

			$.ajax({
			  url: '{site_url}trm_berkas/get_dokter/',
			  type: "POST",
			  dataType: 'json',
			  data: {
          idberkas: idberkas
        },
			  success: function(data) {
				  if (data){
  					$("#iddokter").val(data.iddokter).trigger('change');
  					$("#iddokter_perujuk").val(data.iddokter_perujuk).trigger('change');
				  }
			  }
			});

			setTimeout(function() {
				loadDokter();
				loadDokterAnesthesi();
			}, 1000);
		}
	}

  function loadDokter() {
		var idberkas = $("#idberkas").val();
		var iddokter = $("#iddokter").val();
		var iddokter_perujuk = $("#iddokter_perujuk").val();

		$('#tabel_dokter tbody').empty();
		$.ajax({
		  url: '{site_url}trm_berkas/load_konsulen/',
		  type: "POST",
		  dataType: 'json',
		  data: {
				idberkas: idberkas,iddokter_perujuk:iddokter_perujuk,iddokter:iddokter
			},
		  success: function(data) {

        var content='';
        $.each(data, function (a, row) {
    			 var label = '';
    			 if (row.tabel_asal=='Manual'){
    				 label = '<span class="label label-danger"> Manual </span>';
    			 }else{
    				 label = '<span class="label label-primary"> Otomatis </span>';
    			 }

    			 content ='<tr>';
    			 content +=' <td><i class="fa fa-user-md fa-2x"></i> ' + row.nama + '</td>';
    			 content +=' <td class="text-center"><span class="label label-success">' + row.detail + '</span> ' + label + '</td>';
    			 content +=' <td class="text-center"><div class="btn-group"><button class="btn btn-xs btn-primary edit_dokter disable" title="Edit"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-danger hapus_dokter disable" title="Hapus"><i class="fa fa-close"></i></button></div></td>';
    			 content +=' <td style="display:none">' + row.id + '</td>';
    			 content +=' <td style="display:none">' + row.iddokter + '</td>';
    			 content +="<\tr>";

    			 $('#tabel_dokter tbody').append(content);
			  });
		  }
		});
	}

	function loadDokterAnesthesi(){
    $('#tabel_dokter_anestesi tbody').empty();

    var idTransaksi = $("#idtransaksi").val();
		$.ajax({
		  url: '{site_url}trm_berkas/load_anasthesi/',
		  type: "POST",
		  dataType: 'json',
		  data: {
				id_trx: idTransaksi
			},
		  success: function(data) {
			 $.each(data, function (a,row) {
				 $('#tabel_dokter_anestesi tbody').append("<tr><td><i class='fa fa-user-md fa-2x'></i> " + row.nama + "</td><\tr>");
			 });
		  }
		});
	}

  function setViewAuth() {
		if ($("#isReadonly").val()=='1') {
			$(".disable").attr('disabled', true);
			$(".read").attr('disabled', false);
		}else{
			$(".disable").attr('disabled', false);
			$(".read").attr('disabled', false);
		}
	}

  function closeHeaderGroupSection() {
		$("#div_kodefikasi_fisio").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_fisio").val('0');
		$("#div_kodefikasi_rad").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_rad").val('0');
		$("#div_kodefikasi_lab").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_lab").val('0');
		$("#div_kodefikasi_dokter").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_dokter").val('0');
		$("#div_kodefikasi_operasi").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_operasi").val('0');

		$(".section_analisa").removeClass("block-opt-hidden").addClass("block-opt-hidden");
	}

  function clearForm(){
    $(".clear").val('');
  }
</script>
