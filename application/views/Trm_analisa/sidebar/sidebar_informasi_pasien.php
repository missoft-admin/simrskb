<div class="block block-bordered">
  <div class="block-header">
    <ul class="block-options">
      <?php if (UserAccesForm($user_acces_form,array('1184'))){?>
      <a href="{site_url}trm_berkas/profile/<?= $pasien['id']; ?>" target="_blank" class="btn btn-xs btn-primary" type="button"><i class="si si-user"></i> Profile</a>
      <?}?>
      <?php if (UserAccesForm($user_acces_form,array('1185'))){?>
      <button class="btn btn-xs btn-success" type="submit"><i class="si si-printer"></i> Cetak Dokumen</button>
      <?}?>
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">Informasi Detail Pasien</h3>
  </div>
  <div class="block-content">
    <h3 id="text_header"></h3>
    <? $this->load->view('Trm_analisa/header'); ?>
    <? $this->load->view('Trm_berkas/kodetifikasi_radiologi'); ?>
    <? $this->load->view('Trm_berkas/kodetifikasi_lab'); ?>
    <? $this->load->view('Trm_berkas/kodetifikasi_fisio'); ?>
    <? $this->load->view('Trm_berkas/kodetifikasi_dokter'); ?>
    <? $this->load->view('Trm_berkas/kodetifikasi_operasi'); ?>
    <hr style="margin-top: 0;">
    <? $this->load->view('Trm_analisa/analisa'); ?>
  </div>

  <div class="modal-footer">
    <div class="buton">
    <?php if (UserAccesForm($user_acces_form,array('1187'))){?>
      <button type="button" id="btn_save" class="btn btn-success" style="display: none;"><i class="fa fa-save"></i> Simpan Analisa</button>
    <?}?>
    <?php if (UserAccesForm($user_acces_form,array('1189'))){?>
      <button type="button" id="btn_verifikasi" class="btn btn-danger" style="display: none;"><i class="fa fa-check"></i> Simpan Verifikasi</button>
    <?}?>
      <a href="{base_url}trm_analisa/proses/<?= $pasien['id']; ?>" type="button" class="btn btn-default"><i class="si si-refresh"></i> Refresh</a>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).on("click", "#btn_save", function(e) {
    e.preventDefault();
		var idberkas = $("#idberkas").val();
		var idpendaftaran = $("#idpendaftaran").val();
		var asalpendaftaran = $("#asalpendaftaran").val();

    var tableAnalisa = $('.tableAnalisa tbody tr').get().map(function(row) {
      return $(row).find('td').closest('tr').get().map(function(cell, index) {
        return {
          'idberkas': $(cell).find("td:eq(8)").html(),
          'idanalisa': $(cell).find("td:eq(9)").html(),
          'idlembaran': $(cell).find("td:eq(10)").html(),
          'kriteria': $(cell).find("td:eq(2) select option:selected").get().map(function(item) { return { 'nilai': $(item).val(), 'keterangan': $(item).text() } }),
          'detail': $(cell).find("td:eq(3) select option:selected").get().map(function(item) { return $(item).val(); }),
          'dokter': $(cell).find("td:eq(4) select option:selected").get().map(function(item) { return { 'id': $(item).val(), 'value': $(item).text(), 'tipe': $(item).data('tipe') } }),
          'catatan': $(cell).find("td:eq(5) input").val(),
          'hold': $(cell).find("td:eq(11)").html(),
        }
      })[0];
    }).filter(function (item) {
      return item
    });

    swal({
      title: "Anda Yakin ?",
      text : "Akan Menyelesaikan Analisa ?",
      type : "success",
      showCancelButton: true,
      confirmButtonText: "Ya",
      confirmButtonColor: "#34a263",
      cancelButtonText: "Batalkan",
    }).then(function() {
      $.ajax({
  			url: '{site_url}trm_analisa/save',
  			type: 'POST',
  			data: {
          idberkas: idberkas,
          idpendaftaran: idpendaftaran,
          asalpendaftaran: asalpendaftaran,
          analisa: JSON.stringify(tableAnalisa),
        },
  			complete: function() {
  				swal({
						title: "Berhasil!",
						text: "Proses Penyimpanan Transaksi Analisa.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

  				setTimeout(function() {
  					window.location = '{base_url}trm_analisa/proses/<?= $pasien['id']; ?>'
  				}, 2000);
  			}
  		});
    });
  });

  $(document).on("click", "#btn_verifikasi", function() {
		var idberkas = $("#idberkas").val();
		var idpendaftaran = $("#idpendaftaran").val();
		var asalpendaftaran = $("#asalpendaftaran").val();

    var tableAnalisa = $('.tableAnalisa tbody tr').get().map(function(row) {
      return $(row).find('td').closest('tr').get().map(function(cell, index) {
        return {
          'idberkas': $(cell).find("td:eq(8)").html(),
          'idanalisa': $(cell).find("td:eq(9)").html(),
          'idlembaran': $(cell).find("td:eq(10)").html(),
          'kriteria': $(cell).find("td:eq(2) select option:selected").get().map(function(item) { return { 'nilai': $(item).val(), 'keterangan': $(item).text() } }),
          'detail': $(cell).find("td:eq(3) select option:selected").get().map(function(item) { return $(item).val(); }),
          'dokter': $(cell).find("td:eq(4) select option:selected").get().map(function(item) { return { 'id': $(item).val(), 'value': $(item).text(), 'tipe': $(item).data('tipe') } }),
          'catatan': $(cell).find("td:eq(5) input").val(),
          'hold': $(cell).find("td:eq(11)").html(),
        }
      })[0];
    }).filter(function (item) {
      return item
    });

		swal({
			title: "Anda Yakin ?",
			text : "Akan Menyelesaikan Verifikasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
      $.ajax({
        url: '{site_url}trm_analisa/save_verifikasi',
        type: 'POST',
  			data: {
          idberkas: idberkas,
          idpendaftaran: idpendaftaran,
          asalpendaftaran: asalpendaftaran,
          analisa: JSON.stringify(tableAnalisa),
        },
        complete: function() {
          swal({
            title: "Berhasil!",
            text: "Proses Penyimpanan Verifikasi.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
          });
          setTimeout(function() {
            window.location = '{base_url}trm_analisa/proses/<?= $pasien['id']; ?>'
          }, 2000);
        }
      });
		});
	});
</script>
