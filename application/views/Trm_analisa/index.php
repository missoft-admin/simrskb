<style media="screen">
	.v-align-middle {
		vertical-align: middle;
	}
</style>

<div class="row">
	<div class="col-lg-4">
		<?php $this->load->view('Trm_analisa/sidebar/sidebar_berkas'); ?>
	</div>

	<div class="col-lg-8">
		<?php $this->load->view('Trm_analisa/sidebar/sidebar_informasi_pasien'); ?>
	</div>
</div>

<input type="hidden" readonly id="idpasien" value="<?= $pasien['id']; ?>">
<input type="hidden" readonly id="idberkas" class="clear" value="">
<input type="hidden" readonly id="idpendaftaran" class="clear" value="">
<input type="hidden" readonly id="asalpendaftaran" class="clear" value="">
<input type="hidden" readonly id="idtransaksi" class="clear" value="">
<input type="hidden" readonly id="idtujuan" class="clear" value="">
<input type="hidden" readonly id="isReadonly" value="0">

<script type="text/javascript">
	function set_view() {
		if ($("#isReadonly").val()=='1') {
			$(".disable").attr('disabled', true);
			$(".read").attr('disabled', false);
		}else{
			$(".disable").attr('disabled', false);
			$(".read").attr('disabled', false);
		}
	}
</script>
