<?php foreach (get_all('msetting_analisa', array('status' => '1')) as $row) { ?>
<?php if ($row->nama !== 'REVIEW RANAP PASIEN DIRAWAT') { ?>

  <?php
    if (isset($_GET['idberkas'])) {
      $idberkas = $_GET['idberkas'];
      $logicStatus = $this->model->runLogic($row, $idberkas);
    } else {
      $logicStatus = 0;
    }
  ?>

  <?php if ($logicStatus) { ?>
    <div class="row">
      <div class="col-md-12">
        <div class="block block-themed block-opt-hidden section_analisa">
          <div class="block-header bg-success">
            <ul class="block-options ">
              <li>
                <a class="btn btn-xs btn-warning" target="_blank" href="{base_url}trm_analisa/print_analisa/<?= $idberkas; ?>/<?= $row->id; ?>"><i class="fa fa-print"></i> Cetak Dokumen Analisa</a>
              </li>
              <li>
                <button class="disable read" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
              </li>
            </ul>
            <h3 class="block-title"><i class="fa fa-bed"></i> <?= $row->nama; ?></h3>
          </div>
          <div class="block-content block-content">
            <form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
              <div class="form-group">
                <div class="col-sm-12">
          		    <div class="table-responsive">
                    <table class="table table-bordered table-striped tableAnalisa" id="tabel_analisa" style="width: 100%;">
                      <thead>
                        <tr>
                          <th style="width:5%" class="text-center">No</th>
                          <th style="width:10%" class="text-center">Nama Lembaran</th>
                          <th style="width:15%" class="text-center">Kriteria</th>
                          <th style="width:15%" class="text-center">Detail</th>
                          <th style="width:15%" class="text-center">Dokter / Pegawai</th>
                          <th style="width:20%" class="text-center">Catatan</th>
                          <th style="width:5%" class="text-center">Status Hold</th>
                          <th style="width:5%" class="text-center">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($this->model->getSettingAnalisa($row->lembaran) as $index => $setting) { ?>

                          <?php
                            $berkasOnDB = getByQuery(
                                array(
                                  'output_type' => 'single',
                                  'table' => 'trm_berkas_analisa',
                                  'select' => array('*'),
                                  'where' => array(
                                    ['idberkas', $idberkas],
                                    ['idanalisa', $row->id],
                                    ['idlembaran', $setting['id']],
                                  )
                                )
                            );

                            if ($berkasOnDB) {
                              $valKriteria = $berkasOnDB->kriteria_nilai;
                              $valDetail = json_decode($berkasOnDB->detail);
                              $valDokter = isset($berkasOnDB->dokter) ? array_column(json_decode($berkasOnDB->dokter), 'id') : array();
                              $valCatatan = $berkasOnDB->catatan;
                            } else {
                              $valKriteria = '';
                              $valDetail = array();
                              $valDokter = array();
                              $valCatatan = '';
                            }
                          ?>

                          <tr>
                            <td style="width:5%" class="text-center v-align-middle"><?= $index+1; ?></td>
                            <td style="width:10%" class="v-align-middle"><?= $setting['nama']; ?></td>
                            <td style="width:15%" class="v-align-middle">
                              <select class="js-select2 form-control disable" style="width: 100%;" data-placeholder="Pilih Kriteria">
                                <?php foreach ($setting['kriteria'] as $kriteria) { ?>
                                  <option value="<?= $kriteria->nilai; ?>" <?= ($valKriteria == $kriteria->nilai ? 'selected':'') ?>>( <?= $kriteria->nilai; ?> ) <?= $kriteria->keterangan; ?></option>
                                <?php } ?>
                              </select>
                            </td>
                            <td style="width:15%" class="v-align-middle">
                              <select class="js-select2 form-control disable" style="width: 100%;" data-placeholder="Pilih Detail" multiple>
                                <?php foreach ($setting['detail'] as $detail) { ?>
                                  <option value="<?= $detail->keterangan; ?>" <?= (in_array($detail->keterangan, $valDetail) ? 'selected':'') ?>><?= $detail->keterangan; ?></option>
                                <?php } ?>
                              </select>
                            </td>
                            <td style="width:15%" class="v-align-middle">
                              <select class="js-select2 form-control disable" style="width: 100%;" data-placeholder="Pilih Dokter / Pegawai" multiple>
                                <?php foreach ($setting['dokter'] as $dokter) { ?>
                                  <option value="<?= $dokter->id; ?>" data-tipe="<?= $dokter->tipe; ?>" <?= (in_array($dokter->id, $valDokter) ? 'selected':'') ?>><?= $dokter->nama; ?></option>
                                <?php } ?>
                              </select>
                            </td>
                            <td style="width:20%" class="v-align-middle">
                              <input type="text" class="form-control disable" placeholder="Catatan" value="<?= $valCatatan; ?>">
                            </td>
                            <td style="width:5%" class="text-center v-align-middle"><?= ($setting['variable_hold'] ? 'Ya' : 'Tidak'); ?></td></td>
                            <td style="width:5%" class="text-center v-align-middle">
                              <button class="btn btn-sm btn-danger disable reset_lembaran">
                                <i class="fa fa-undo"></i>
                              </button>
                            </td>
                            <td hidden><?= $idberkas; ?></td>
                            <td hidden><?= $row->id; ?></td>
                            <td hidden><?= $setting['id']; ?></td>
                            <td hidden><?= $setting['variable_hold']; ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
<?php } ?>
<?php } ?>

<script type="text/javascript">
  $(document).on("click", ".reset_lembaran", function() {
    $(this).closest('tr').find('td:eq(2) select').val('').trigger('change');
    $(this).closest('tr').find('td:eq(3) select').val('').trigger('change');
    $(this).closest('tr').find('td:eq(4) select').val('').trigger('change');
    $(this).closest('tr').find('td:eq(5) input').val('');
  });
</script>
