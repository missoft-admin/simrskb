<div class="block-content block-content">
	<div class="form-horizontal push-10-t">
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_tanggalkunjungan" name="kod_tanggalkunjungan" disabled="">
					<label for="">Tanggal Kunjungan</label>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_poli" name="kod_poli" disabled="">
					<label for="">Poliklinik</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_status_pasien" name="kod_status_pasien" disabled="">
					<label for="">Status Pasien</label>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_kasus" name="kod_kasus" disabled="">
					<label for="">Kasus</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_kunjungan" name="kod_kunjungan" disabled="">
					<label for="">Status Kunjungan</label>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_dokter" name="kod_dokter" disabled="">
					<label for="">Dokter</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_user_daftar" name="kod_user_daftar" disabled="">
					<label for="">Petugas Pendaftaran</label>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_tanggalkirim" name="kod_tanggalkirim" disabled="">
					<label for="">Tanggal Kirim</label>
				</div>
			</div>
			<div class="col-sm-3"></div>
		</div>

		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_user_kirim" name="kod_user_kirim" disabled="">
					<label for="">Petugas Kirim Berkas</label>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_tanggalkembali" name="kod_tanggalkembali" disabled="">
					<label for="">Tanggal Kembali</label>
				</div>
			</div>
			<div class="col-sm-3"></div>
		</div>

		<div class="form-group">
			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_lama_kirim" name="kod_lama_kirim" disabled="">
					<label for="">Lama Kirim Berkas</label>
				</div>
			</div>
			<div class="col-sm-3"></div>

			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_lama_kembali" name="kod_lama_kembali" disabled="">
					<label for="">Lama Kembali</label>
				</div>
			</div>
			<div class="col-sm-3"></div>
		</div>
	</div>
</div>
