<!-- Modal Filter Advance -->
<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                </ul>
                <h5 class="modal-title" id="exampleModalLabel">Filter Mode Advance</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="selectTujuanLab">Tujuan Laboratorium</label>
                                <select id="selectTujuanLab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <?php foreach ($tujuan_laboratorium as $r) { ?>
                                    <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputTanggalPermintaanDari">Tanggal Permintaan</label>
                                <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                    <input class="form-control" type="text" id="inputTanggalPermintaanDari" name="inputTanggalPermintaanDari" value="{tanggal}">
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input class="form-control" type="text" id="inputTanggalPermintaanSampai" name="inputTanggalPermintaanSampai" value="{tanggal}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNomorMedrec">Nomor Medrec</label>
                                <input type="text" class="form-control" id="inputNomorMedrec" placeholder="Masukkan Nomor Medrec">
                            </div>
                            <div class="form-group">
                                <label for="inputNomorPendaftaran">Nomor Pendaftaran</label>
                                <input type="text" class="form-control" id="inputNomorPendaftaran" placeholder="Masukkan Nomor Pendaftaran">
                            </div>
                            <div class="form-group">
                                <label for="selectStatusAntrian">Status Antrian</label>
                                <select id="selectStatusAntrian" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="#" selected>Semua</option>
                                    <option value="0">Menunggu Dipanggil</option>
                                    <option value="1">Sedang Dilayani</option>
                                    <option value="2">Dilewati</option>
                                    <option value="3">Telah Dilayani</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="selectAsalPasien">Asal Pasien</label>
                                <select id="selectAsalPasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <option value="1">Poliklinik</option>
                                    <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                    <option value="3">Rawat Inap</option>
                                    <option value="4">One Day Surgery (ODS)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="selectKelompokPasien">Kelompok Pasien</label>
                                <select id="selectKelompokPasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <?php foreach (get_all('mpasien_kelompok', ['status' => 1]) as $row) { ?>
                                    <option value="<?=$row->id?>"><?=$row->nama?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputNamaPasien">Nama Pasien</label>
                                <input type="text" class="form-control" id="inputNamaPasien" placeholder="Masukkan Nama Pasien">
                            </div>
                            <div class="form-group">
                                <label for="inputNomorPermintaan">Nomor Permintaan</label>
                                <input type="text" class="form-control" id="inputNomorPermintaan" placeholder="Masukkan Nomor Permintaan">
                            </div>
                            <div class="form-group">
                                <label for="selectStatusTindakan">Status Tindakan</label>
                                <select id="selectStatusTindakan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="#" selected>Semua</option>
                                    <option value="2">Menunggu Pemeriksaan</option>
                                    <option value="3">Proses Sampling</option>
                                    <option value="4">Menunggu Hasil</option>
                                    <option value="5">Menunggu Validasi</option>
                                    <option value="6">Selesai Validasi</option>
                                    <option value="7">Telah DIkirim</option>
                                    <option value="0">Dibatalkan</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btn-filter-advance" class="btn btn-primary" data-dismiss="modal">Terapkan Filter</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    function bindChangeEvent(source, target) {
        $(document).on("change", source, function() {
            let value = $(this).val();
            $(target).val(value).trigger('change');
        });
    }

    bindChangeEvent("#nomor_medrec", "#inputNomorMedrec");
    bindChangeEvent("#nama_pasien", "#inputNamaPasien");
    bindChangeEvent("#tujuan_laboratorium", "#selectTujuanLab");
    bindChangeEvent("#asal_pasien", "#selectAsalPasien");
    bindChangeEvent("#status_antrian", "#selectStatusAntrian");
    bindChangeEvent("#tanggal_dari", "#inputTanggalPermintaanDari");
    bindChangeEvent("#tanggal_sampai", "#inputTanggalPermintaanSampai");

    $('#modal-filter').on('hidden.bs.modal', function () {
        const elements = [
            { source: '#inputNomorMedrec', target: '#nomor_medrec' },
            { source: '#inputNamaPasien', target: '#nama_pasien' },
            { source: '#selectTujuanLab option:selected', target: '#tujuan_laboratorium' },
            { source: '#selectAsalPasien option:selected', target: '#asal_pasien' },
            { source: '#selectStatusAntrian option:selected', target: '#status_antrian' },
            { source: '#inputTanggalPermintaanDari', target: '#tanggal_dari' },
            { source: '#inputTanggalPermintaanSampai', target: '#tanggal_sampai' },
        ];

        elements.forEach(element => {
            let value = $(element.source).val();
            $(element.target).val(value).trigger('change');
        });

        setTabStatusPemeriksaan($('#selectStatusAntrian option:selected').val());
    });
});

</script>
