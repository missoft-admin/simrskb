<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <form action="javascript:filter()" class="form-horizontal" id="frm1" method="post" accept-charset="utf-8">
                <div class="col-md-6"> 
                    <div class="form-group"> 
                        <label class="col-md-3 control-label">Nomor Bukti</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{nokontrabon}" disabled>
                        </div> 
                    </div>
                    <div class="form-group"> 
                        <label class="col-md-3 control-label">Nama Vendor</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{vendor}" disabled>
                        </div> 
                    </div>
                    <div class="form-group"> 
                        <div class="col-md-3"></div> 
                        <div class="col-md-9">
                            <button class="btn btn-success btn-block text-uppercase" name="button">
                                <i class="fa fa-print"></i> Cetak Detail KBO
                            </button>
                        </div>
                    </div>                    
                </div>
            </form>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="index_table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NO</th>
                    <th>TANGGAL FAKTUR</th>
                    <th>NOMOR FAKTUR</th>
                    <th>NO PENERIMAAN</th>
                    <th>NOMINAL</th>
                    <th>AKSI</th>
                </tr>
            </thead>
            <tbody> </tbody>
            <tfoot> 
                <tr> 
                    <th colspan="5" class="text-right">Total:</th> 
                    <th></th> 
                    <th></th> 
                </tr> 
            </tfoot>            
        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td width="70%" class="bg-info text-white">Total Tagihan</td>
                <td class="bg-success text-white text-right"><?php echo number_format($totalnominal) ?></td>
            </tr>
            <?php if ($carabayar == 1): ?>
                <tr>
                    <td width="70%" class="bg-info text-white">Biaya Check</td>
                    <td class="bg-danger text-white text-right"><?php echo number_format($biayacek) ?></td>
                </tr>
                <tr>
                    <td width="70%" class="bg-info text-white">Biaya Materai</td>
                    <td class="bg-danger text-white text-right"><?php echo number_format($biayamaterai) ?></td>
                </tr>
            <?php endif ?>
            <tr>
                <td width="70%" class="bg-info text-white">Grand Total Pembayaran</td>
                <td class="bg-success text-white text-right"><?php echo number_format($grandtotalnominal) ?></td>
            </tr>
            <tr>
                <td width="70%" class="bg-gray text-black" colspan="2">
                    TERBILANG: <?php echo strtoupper( terbilang($grandtotalnominal) ) ?> RUPIAH
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="text-left push-20">
    <a href="{site_url}tkontrabon" class="btn btn-danger btn-md">Kembali</a> 
</div>

<div class="modal fade" id="history" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">HISTORY TRANSAKSI</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
                                <label>NOMOR PEMESANAN</label> 
                                <input class="form-control" type="text" id="nopemesanan" disabled>
                            </div> 
                            <div class="col-md-6">
                                <label>USER PEMESANAN</label> 
                                <input class="form-control" type="text" id="userpemesanan" disabled>
                            </div> 
                            <div class="col-md-6">
                                <label>NOMOR PENERIMAAN</label> 
                                <input class="form-control" type="text" id="nopenerimaan" disabled>
                            </div> 
                            <div class="col-md-6">
                                <label>USER PENERIMAAN</label> 
                                <input class="form-control" type="text" id="userpenerimaan" disabled>
                            </div>
                            <div class="col-md-6">
                                <label>NOMOR FAKTUR</label> 
                                <input class="form-control" type="text" id="nofakturexternal" disabled>
                            </div> 
                            <div class="col-md-6">
                                <label>TOTAL BARANG</label> 
                                <input class="form-control" type="text" id="totalbarang" disabled>
                            </div> 
                            <div class="col-md-6">
                                <label>VERIFIKASI AWAL</label> 
                                <input class="form-control" type="text" id="pegawai_verif" disabled>
                            </div> 
                            <div class="col-md-6">
                                <label>AKTIFASI FAKTUR ASLI</label> 
                                <input class="form-control" type="text" id="pegawai_aktivasifaktur" disabled>
                            </div> 
                        </div>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-12">
								<div class="table-responsive">
                                <table class="table table-bordered table-striped" id="detailbarang">
                                    <thead>
                                        <th>NO</th>
                                        <th>TIPE BARANG</th>
                                        <th>NAMA</th>
                                        <th>NOBATCH</th>
                                        <th>QTY</th>
                                        <th>PPN</th>
                                        <th>DISC</th>
                                        <th>HARGA</th>
                                        <th>TOTAL</th>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot> 
                                        <tr> 
                                            <th colspan="8" class="text-right">Total:</th> 
                                            <th></th> 
                                        </tr> 
                                    </tfoot>
                                </table>
                            </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-12">
                                <div class="text-left">
                                    <div class="btn-group">
                                        <a href="#" class="btn btn-success btn-xs btnphoto" title="Lihat Attachment"><i class="fa fa-image"></i> Attachment</a>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="view_photofaktur" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">LIHAT BUKTI FAKTUR</h3>
                </div>
                <div class="block-content">
                    <img src="#" class="img-responsive" id="img_photobukti">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

    function view_photofaktur(photofaktur) {
        $('#img_photobukti').attr('src','{base_url}assets/upload/faktur/'+photofaktur);
        $('#view_photofaktur').modal('show')
    }    

    var table;

    $(document).ready(function(){

        $.ajax({
            url: '{site_url}tkontrabon/get_detail_pembayaran/{id}',
            dataType: 'json',
            method: 'post',
            success: function(res) {
                
            }
        })

        $('select[name=nokontrabon]').select2({
            placeholder: 'Cari Nomor KBO',
            allowClear: true,
            ajax: {
                url: '{site_url}tkontrabon/get_nokbo/',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })

        $('select[name=stkontrabon]').select2({
            placeholder: 'Plih Status KBO',
            allowClear: true,
            data: [
                {id: '0', text: 'BELUM DIBAYAR'},
                {id: '1', text: 'SUDAH DIBAYAR'},
            ]
        }).val(null).trigger('change')

        index_table();
    })

    function filter() {
        index_table();
    }


    function index_table() {

        table = $('#index_table').DataTable({
            pageLength: 50, 
            paging: false,
            searching: false,
            info: false,
            autoWidth: true, 
            order: [[4,'desc']],
            destroy: true,
            ajax: { 
                url: '{site_url}tkontrabon/index_table_detail_kontrabon/{id}', 
                type: 'POST',
            }, 
            columns: [
                {data: 'tipeid', visible: false}, 
                {data: 'nomor', orderable: false}, 
                {data: 'tanggal'}, 
                {data: 'nofakturexternal'}, 
                {data: 'kodenomor'},
                {
                    data: 'totalnominal', className: 'text-right',
                    render: $.fn.dataTable.render.number( ',', '.', 2 )
                },
                {
                    className: 'text-right',
                    render: function(data,type,row) {
                        $aksi = '<div class="btn-group">';
                        $aksi += '<a class="btn btn-danger btn-xs get_history" title="History"><i class="fa fa-eye"></i> History</a>';
                        $aksi += '</div>';
                        return $aksi;
                    }
                },
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var total = api .column( 5 ) .data() .reduce( function (a, b) {
                    return parseFloat(a) + parseFloat(b); 
                }, 0 );

                $( api.column( 5 ).footer() ).html('('+ formatMoney(total) +')');
            }            
        });
    }

    $('#index_table tbody').on('click', '.get_history', function() {
        var row = $(this).parents('tr');
        var dataArray = table.rows(row).data().toArray()[0];

        var res = dataArray; 

        $('#history').modal('show');
        $('#nopemesanan').val( res.nopemesanan )
        $('#nopenerimaan').val( res.kodenomor )
        $('#nofakturexternal').val( res.nofakturexternal )
        $('#totalbarang').val( res.totalbarang )
        $('#userpemesanan').val( res.userpemesanan )
        $('#userpenerimaan').val( res.userpenerimaan )
        $('#pegawai_verif').val( res.pegawai_verif )
        $('#pegawai_aktivasifaktur').val( res.pegawai_aktivasifaktur )
        $('.btnphoto').attr( 'href', 'javascript:view_photofaktur(''+res.photoaktivasifaktur+'')' )
        detailbarang(res.tipe, res.tipeid);
        index_table()
    })    

    $('#index_table tbody').on('click', '.editjatuhtempo', function() {
        var row = $(this).parents('tr');
        var id = table.cell(row,0).data()
        var tanggaljatuhtempo = table.cell(row,5).data()
        var nopemesanan = table.cell(row,2).data()
        var nopenerimaan = table.cell(row,3).data()
        var vendor = table.cell(row,6).data()
        var iddistributor = table.cell(row,12).data()
        $('input[name=tanggaljatuhtempo]').val( tanggaljatuhtempo )
        $('input[name=id]').val( id )
        $('input[name=nopemesanan]').val( nopemesanan )
        $('input[name=nopenerimaan]').val( nopenerimaan )
        $('input[name=vendor]').val( vendor )
        $('input[name=iddistributor]').val( iddistributor )
        $('#modal_editjatuhtempo').modal('show')
    })

    function view_photofaktur(photofaktur) {
        $('#img_photobukti').attr('src','{base_url}assets/upload/faktur/'+photofaktur);
        $('#view_photofaktur').modal('show')
    }

    function simpanjatuhtempo() {
        var form = $('#frm1')[0];
        var formData = new FormData(form);
        console.log(JSON.stringify(formData))
        $.ajax({
            url: '{site_url}tkontrabon_verifikasi/simpanjatuhtempo',
            method: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(res) {
                if(res.status == 200) {
                    index_table()
                    $('#modal_editjatuhtempo').modal('hide');
                    swal('Sukses','Data berhasil diverifikasi','success');
                } else {
                    swal({title: 'Kesalahan!', text: res.msg, type: 'warning'});
                }
            }
        })
    }

    function history(id) {
        $.ajax({
            url: '{site_url}tkontrabon/history/'+id,
            dataType: 'json',
            success: function(res) {
                $('#nopemesanan').val( res.nopemesanan )
                $('#nopenerimaan').val( res.nopenerimaan )
                $('#nofakturexternal').val( res.nofakturexternal )
                $('#totalbarang').val( res.totalbarang )
                $('#userpemesanan').val( res.userpemesanan )
                $('#userpenerimaan').val( res.userpenerimaan )
                $('#pegawai_verif').val( res.pegawai_verif )
                $('#pegawai_aktivasifaktur').val( res.pegawai_aktivasifaktur )
                $('.btnphoto').attr( 'href', 'javascript:view_photofaktur('+res.photoaktivasifaktur+')' )
                detailbarang(res.id);
            }
        })
        $('#history').modal('show')
    }

    function editjatuhtempo(id,tanggaljatuhtempo) {
        $('#modal_editjatuhtempo').modal('show');
        $('input[name=tanggaljatuhtempo]').val(id)
    }

    function verifikasi(id) {
        swal({
            text: 'Apakah yakin akan verifikasi data ini?',
            showCancelButton: true,
            confirmButtonColor: '#46c37b',
            confirmButtonText: 'Ya, verifikasi data!',
            html: false,
            preConfirm: function() {
              return new Promise(function (resolve) {
                setTimeout(function() { resolve() }, 50);
              })
            }
        }).then(
            function (result) {
                $.ajax({url: '{site_url}tkontrabon_verifikasi/confirm_verif/'+id})
                swal('Berhasil!', 'Data telah berhasil diverifikasi.', 'success');
                index_table();
            }
        );
    }

    function image_exists(image_url){
        var http = new XMLHttpRequest(); 
        http.open('HEAD', image_url, false); 
        http.send();
        return http.status;
    }

    function detailbarang(tipe,tipeid) {

        table = $('#detailbarang').DataTable({
            pageLength: 50, 
            paging: false,
            searching: false,
            info: false,
            sort: false,
            autoWidth: false, 
            order: [[4,'asc']],
            destroy: true,
            ajax: { 
                url: '{site_url}tkontrabon/index_table_detailbarang/',
                data: {
                    tipe: tipe,
                    tipeid: tipeid
                },
                type: 'POST',
            }, 
            columns: [
                {data: 'nomor', orderable: false}, 
                {data: 'namatipe'}, 
                {data: 'namabarang'}, 
                {data: 'nobatch'}, 
                {data: 'kuantitas'}, 
                {data: 'ppn'}, 
                {data: 'diskon'}, 
                {
                    data: 'harga', className: 'text-right',
                    render: $.fn.dataTable.render.number( ',', '.', 2 )
                }, 
                {
                    data: 'totalharga', className: 'text-right',
                    render: $.fn.dataTable.render.number( ',', '.', 2 )
                },
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                total = api .column( 8 ) .data() .reduce( function (a, b) {
                    return parseFloat(a) + parseFloat(b); 
                }, 0 );

                $( api.column( 8 ).footer() ).html('('+ formatMoney(total) +')');
            }
        });
    }

    function formatMoney(n, c, d, t) {
      var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

      return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };    


</script>