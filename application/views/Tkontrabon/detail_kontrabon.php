<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<style media="screen">
	.text-bold {
		font-weight: bold;
	}

	.fixed-header {
		padding-top: 1rem;
		position: fixed;
		top: 6rem;
		width: calc(100% - 16rem);
		z-index: 100;
		background: #fff;
	}

	.fixed-header-full {
		padding-top: 1rem;
		position: fixed;
		top: 6rem;
		width: calc(100% - 33rem);
		z-index: 100;
		background: #fff;
		left: 28rem;
	}

	.separator-header {
		margin-top: 450px;
	}

	.modal {
		overflow: auto !important;
	}
</style>
<div class="block header-freeze">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content ">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nomor KBO</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{nokontrabon}" disabled>
                            <input class="form-control" type="hidden" name="rekening_id_asal" id="rekening_id_asal" value="{rekening_id_asal}" >
                            <input class="form-control" type="hidden" name="tipe_distributor" id="tipe_distributor" value="{tipe_distributor}" >
                            <input class="form-control" type="hidden" name="tkontrabon_id" id="tkontrabon_id" value="{id}" >
                            <input class="form-control" type="hidden" name="iddistributor" id="iddistributor" value="{iddistributor}" >
                            <input class="form-control" type="hidden" name="tanggalkontrabon" id="tanggalkontrabon" value="{tanggalkontrabon}" >
                            <input class="form-control" type="hidden" name="kontrabon_info_id" id="kontrabon_info_id" value="{kontrabon_info_id}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                            <input class="form-control" type="hidden" name="st_verifikasi" id="st_verifikasi" value="{st_verifikasi}" >
                            <input class="form-control" type="hidden" name="st_kbo" id="st_kbo" value="{st_kbo}" >
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;"> 
                        <label class="col-md-3 control-label"><?=($tipe_distributor=='3'?'Vendor':'Distributor')?></label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{nama_dist}" disabled>
                        </div> 
                    </div>
                                      
                </div>
				<div class="col-md-6" style="margin-bottom: 5px;"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Alamat</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{alamat}" disabled>
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;"> 
                        <label class="col-md-3 control-label">No Rekening</label> 
                        <div class="col-md-9"> 
                           <select id="rekening_id" name="rekening_id" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one.." <?=($st_kbo!='4'?$disabel:'')?>>
								<option value="#" selected>-Tidak Ada Rekening-</option>
								<?foreach($list_bank_dist as $row){?>
									<option value="<?=$row->id?>" selected><?=$row->bank.' ['.$row->norekening.']'?></option>
								<?}?>
							</select>
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <div class="col-md-3">
							
						</div> 
                        <div class="col-md-9">
							<?if ($tipe_distributor !='3'){?>
							<button class="btn btn-primary  text-uppercase" type="button"  name="btn_add_tagihan" id="btn_add_tagihan" <?=$disabel?>>
                                <i class="fa fa-plus"></i> Tambah Tagihan
                            </button>
							<?}?>
                            <button class="btn btn-success  text-uppercase" type="button"  name="btn_cetak" id="btn_cetak" <?=$disabel?>>
                                <i class="fa fa-print"></i> Cetak Detail KBO
                            </button>
							
							<button class="btn btn-danger  text-uppercase" type="button" name="btn_add_bank" id="btn_add_bank" <?=($st_kbo!='4'?$disabel:'')?>>
                                <i class="fa fa-plus"></i> Add Rekening
                            </button>
							
                        </div>
                    </div>           
                </div>
            <?php echo form_close(); ?>
        </div>
        <div class="row">
			<button id="pinheader" class="btn" style="float:right; color:red"><i class="fa fa-thumb-tack"></i>Freeze</button>
		</div>
        <div class="form-group" style="margin-top: 20px;">
		</div>
		
    </div>
</div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">DETAIL TAGIHAN</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Tipe Transaksi</label>
                    <div class="col-md-8">
                        <select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>PEMESANAN GUDANG</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>RETUR</option>
							<option value="3" <?=("3" == $tipe ? 'selected' : ''); ?>>MODUL PENGAJUAN</option>
							
						</select>
                    </div>
                </div>
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Pemesanan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_po" placeholder="No. PO" name="no_po" value="{no_po}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Penerimaan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="{no_terima}">
                    </div>
                </div>
                
            </div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Faktur</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_fakur" placeholder="No. Faktur" name="no_fakur" value="{no_fakur}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx" name="tgl_trx" placeholder="From" value="{tgl_trx}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value="{tgl_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="index_list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>TIPE</th>
                    <th><?=($tipe_distributor=='3'?'NO PENGAJUAN':'NO PENERIMAAN')?></th>
                    <th>NOMOR PEMESANAN</th>
                    <th><?=($tipe_distributor=='3'?'TGL PENGAJUAN':'TGL TRX')?></th>
                    <th><?=($tipe_distributor=='3'?'TGL DIBUTUHKAN':'TGL TERIMA')?></th>
                    <th>NO FAKTUR</th>
                    <th>NOMINAL</th>
                    <th>AKSI</th>
                    <th>X</th>
                </tr>
            </thead>
            <tbody> </tbody>
            <tfoot> 
                <tr> 
                    <th colspan="7" class="text-right">Total:</th> 
                    <th><?= number_format($totalnominal,2)?></th> 
                    <th></th> 
                </tr> 
            </tfoot>            
        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="block">
    
    <div class="block-content">
        <div class="row">
			<div class="col-md-6">
				<h3><span class="label label-success">ESTIMASI PEMBAYARAN</span></h3><br>
				<table class="table table-condensed" >
					<tr>
						<td width="50%" class="text-right"><strong>Total Tagihan </strong></td>
						<td ><input type="text" class="form-control decimal" id="est_tagihan" name="tot_tagihan" value="{totalnominal}" readonly></td>
					</tr>
					
					<tr>
						<td width="50%" class="text-right"><strong>Estimasi Pembayaran Cheq </strong></td>
						<td ><input type="text" class="form-control decimal" id="est_cheq" name="est_cheq" value="{est_cheq}" readonly></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>Estimasi Pembayaran Tunai / Transfer </strong></td>
						<td ><input type="text" class="form-control decimal" id="est_tunai" name="est_tunai" value="{est_tunai}" readonly></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>Estimasi Pembayaran Transfer Beda Bank </strong></td>
						<td ><input type="text" class="form-control decimal" id="est_tf" name="est_tf" value="{est_tf}" readonly></td>
					</tr>
					
					
					
				</table>
				<h3><span class="label label-success">SETTING KONTRABON</span></h3><br>
				<table class="table table-condensed" >
					<tr>
						<td width="50%" class="text-right"><strong>Biaya Transfer </strong></td>
						<td ><input type="text" class="form-control number" id="b_tf" name="b_tf" value="{b_tf}" readonly></td>
					</tr>
					
					<tr>
						<td width="50%" class="text-right"><strong>Biaya Materai </strong></td>
						<td ><input type="text" class="form-control number" id="b_materai" name="b_materai" value="{b_materai}" readonly></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>Biaya Cheq </strong></td>
						<td ><input type="text" class="form-control number" id="b_cheq" name="b_cheq" value="{b_cheq}" readonly></td>
					</tr>
					
					
				</table>
			</div>
			<div class="col-md-6">
				<h3><span class="label label-success">RINGKASAN PEMBAYARAN</span></h3><br>
				<table class="table">
					<tr>
						<td width="50%" class="bg-info text-white text-right">TOTAL PEMBAYARAN </td>
						<td ><input type="text" class="form-control decimal" id="ringkasan_total" name="ringkasan_total" value="{totalnominal}" readonly></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>CARA PEMBAYARAN </strong></td>
						<td ><select id="carabayar" name="carabayar" <?=$lock?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." <?=($st_kbo!='4'?$disabel:'')?>>
							<option value="#">- Semua Cara Bayar -</option>
							<option value="1" <?=("1" == $carabayar ? 'selected' : ''); ?>>CHEQ</option>
							<option value="2" <?=("2" == $carabayar ? 'selected' : ''); ?>>TUNAI</option>
							<option value="4" <?=("4" == $carabayar ? 'selected' : ''); ?>>TRANSFER</option>
							<option value="3" <?=("3" == $carabayar ? 'selected' : ''); ?>>TRANSFER BEDA BANK</option>
							
						</select></td>
					</tr>
					
					<tr>
						<td width="50%" class="text-right"><strong>REKENING</strong></td>
						<td ><label id="rek_dipake"></label></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>BIAYA TRANSFER</strong></td>
						<td ><input type="text" class="form-control number hitung" id="biaya_tf" <?=$disabel?> name="biaya_tf" value="{biaya_tf}"></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>BIAYA MATERAI</strong></td>
						<td ><input type="text" class="form-control number hitung" id="biayamaterai" <?=$disabel?> name="biayamaterai" value="{biayamaterai}"></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>BIAYA CEQ</strong></td>
						<td ><input type="text" class="form-control number hitung" id="biayacek" <?=$disabel?> name="biayacek" value="{biayacek}"></td>
					</tr>
					<tr>
						<td width="50%" class="bg-info text-white text-right">GRAND TOTAL PEMBAYARAN</td>
						<td ><input type="text" class="form-control decimal" id="grandtotalnominal" <?=$disabel?> name="grandtotalnominal" value="{grandtotalnominal}" readonly></td>
					</tr>
					<tr>
						<td width="50%" class="bg-gray text-black" colspan="2">
							TERBILANG: <?php echo strtoupper( terbilang($grandtotalnominal) ) ?> RUPIAH
							
						</td>
					</tr>
					<tr>
						<td></td>
						<td width="50%">
							<button class="btn btn-success text-uppercase" <?=$lock?>  <?=($st_kbo!='4'?$disabel:'')?> type="button" name="btn_update" id="btn_update" style="font-size:13px;width:100%;float:right;"><i class="fa fa-save"></i> UPDATE</button>
						</td>
					</tr>
				</table>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-top: 20px;"></div>
			</div>
		</div>
    </div>
</div>


<div class="modal" id="modal_rekening" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">ADD Rekening</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_list">
								<a href="#btabs-animated-slideleft-list"><i class="fa fa-list-ol"></i> List Rekening</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-add"><i class="fa fa-pencil"></i> Add / Edit Rekening</a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-list">
								<h5 class="font-w300 push-15" id="lbl_list_rekening">Dafar Rekening <?=$nama_dist?></h5>
								<table class="table table-bordered table-striped" id="index_rekening">
                                    <thead>
                                        <th>NO</th>
                                        <th>Kode Bank</th>
                                        <th>Nama Bank</th>
                                        <th>Cabang</th>
                                        <th>Atas Nama</th>
                                        <th>Rekening</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </thead>
                                    <tbody></tbody>
                                  
                                </table>
							</div>
							<div class="tab-pane fade fade-left" id="btabs-animated-slideleft-add">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Distributor</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="edit_vendor" name="edit_vendor" value="{nama_dist}"/>	
												<input class="form-control input-sm " readonly type="hidden" id="idrekening" name="idrekening" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Bank</label>
											<div class="col-md-10">
												<select id="bankid" name="bankid" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>-Pilih Bank-</option>
													<?foreach($list_bank as $row){?>
														<option value="<?=$row->id?>"><?=$row->bank?></option>
													<?}?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="norekening">No. Rekening</label>
											<div class="col-md-10">
												<input class="form-control" type="text" id="norekening" >
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="atasnama">Atas Nama</label>
											<div class="col-md-10">
												<input class="form-control" type="text" id="atasnama" >	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="cabang">Cabang</label>
											<div class="col-md-10">
												<input class="form-control" type="text" id="cabang" >	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase" <?=$disabel?> type="button" name="btn_save_bank" id="btn_save_bank"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning text-uppercase" type="button" id="btn_clear_rekening" name="btn_clear_rekening" ><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							
						</div>
					</div>
                        
                                     
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal" id="modal_tambah_faktur" role="dialog" aria-hidden="true" >
    <div class="modal-dialog modal-lg" style="width:80%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Tambah Faktur</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="distributor">Tipe Bayar</label>
									<div class="col-md-8">
										<select id="xtipe" name="xtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#">- Semua Tipe -</option>
											<option value="1">PEMESANAN GUDANG</option>
											<option value="2">RETUR</option>
											<option value="3">MODUL PENGAJUAN</option>
											
										</select>
									</div>
								</div>
								
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="status">No Pemesanan</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="xno_po" placeholder="No. PO" name="xno_po" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="status">No Penerimaan</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="xno_terima" placeholder="No. Penerimaan" name="xno_terima" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="status">No Faktur</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="xno_fakur" placeholder="No. Faktur" name="xno_fakur" value="">
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tanggal">Cara Bayar</label>
									<div class="col-md-8">
										<select id="xcara_bayar" name="xcara_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#">- Semua Cara Bayar -</option>
											<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>TUNAI</option>
											<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>KREDIT</option>
											
										</select>
									</div>
								</div>
								
							
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tanggal">Tanggal Penerimaan</label>
									<div class="col-md-8">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="xtanggalterima1" name="xtanggalterima1" placeholder="From" value=""/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="xtanggalterima2" name="xtanggalterima2" placeholder="To" value=""/>
										</div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-4 control-label" for="tanggal">Tanggal Jatuh Tempo</label>
									<div class="col-md-8">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="xtanggaljt1" name="xtanggaljt1" placeholder="From" value=""/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="xtanggaljt2" name="xtanggaljt2" placeholder="To" value=""/>
										</div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for=""></label>
									<div class="col-md-8">
										<button class="btn btn-success text-uppercase" type="button" name="btn_cari_tagihan" id="btn_cari_tagihan" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
									</div>
								</div>
							</div>		
                            
                        </div>
						
                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-12">
								<div class="table-responsive">
                                <table class="table table-bordered table-striped" id="index_faktur">
									<thead>
										<tr>
											<th hidden width="5%">TIPE</th>
											<th hidden width="2%">ID</th>
											<th width="10%">NO</th>
											<th width="10%">TIPE</th>
											<th width="10%">NO PENERIMAAN</th>
											<th width="10%">NO PEMESANAN</th>
											<th width="10%">TGL TRX</th>
											<th width="10%">TGL TERIMA</th>
											<th width="10%">NO FAKTUR</th>
											<th width="15%">DISTRIBUTOR</th>
											<th width="15%">NOMINAL</th>
											<th width="15%">CARA BAYAR</th>
											<th width="15%">J. TEMPO</th>
											<th width="15%">STATUS</th>
											<th width="15%" class="text-center">AKSI</th>
										</tr>										
									</thead>
									<tbody></tbody>
								</table>
                            </div>
                            </div>
                        </div>
                                     
                    </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="view_photofaktur" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">LIHAT BUKTI LAMPIRAN</h3>
                </div>
                <div class="block-content">
					<div class="table-responsive">
                    <table class="table table-bordered" id="list_index_lampiran">
						<thead>
							<tr>
								<th width="50%" class="text-center">File</th>
								<th width="15%" class="text-center">Size</th>
								<th width="35%" class="text-center">User</th>
								<th width="35%" class="text-center">X</th>
								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

    function view_photofaktur(photofaktur) {
        $('#img_photobukti').attr('src','{base_url}assets/upload/faktur/'+photofaktur);
        $('#view_photofaktur').modal('show')
    }    

    var table;
	var rekening_id_asal;
    $(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
      loadKbo_detail();
      load_rekening();
		var pinStatus = true;
		$("#pinheader").click(function () {
			pinStatus = !pinStatus;
			if (pinStatus) {
				$("#pinheader").html('<i class="fa fa-close"></i> Close');
			} else {
				$("#pinheader").html('<i class="fa fa-thumb-tack"></i> Freeze');

				$('.header-freeze').removeClass('fixed-header');
				$('.header-freeze').removeClass('fixed-header-full');
				$('.content-verification').removeClass('separator-header');
			}
		});

		$(window).scroll(function () {
			if (pinStatus) {
				$("#pinheader").html('<i class="fa fa-close"></i> Close');
				if ($(window).scrollTop() >= 300) {
					if (localStorage.getItem("sidebar") == 'full') {
						$('.header-freeze').addClass('fixed-header-full');
					} else {
						$('.header-freeze').addClass('fixed-header');
					}

					$('.content-verification').addClass('separator-header');
				} else {
					if (localStorage.getItem("sidebar") == 'full') {
						$('.header-freeze').removeClass('fixed-header-full');
					} else {
						$('.header-freeze').removeClass('fixed-header');
					}

					$('.content-verification').removeClass('separator-header');
				}
			} else {
				$("#pinheader").html('<i class="fa fa-thumb-tack"></i> Freeze');
			}
		});
		rekening_id_asal=$("#rekening_id_asal").val();
    });
	function load_rekening(){
		var rek='';
		if ($("#rekening_id").val()=='#'){
			rek=': TIDAK DIPILIH';
		}else{
			rek=$("#rekening_id option:selected").text();
		}
		$("#rek_dipake").text(rek);
	}
	$(document).on("click","#btn_add_bank",function(){	
		$("#modal_rekening").modal('show');
		LoadBank();
	});
	$(".hitung").keyup(function() {
		if ($(this).val()==''){
			$(this).val(0)
		}
		hitung_total();
	});
	$("#carabayar").change(function() {
		
		if ($(this).val()=='1'){
			$("#biaya_tf").val(0);
			$("#biayamaterai").val($("#b_materai").val());
			$("#biayacek").val($("#b_cheq").val());
			$("#rekening_id").val('#').trigger('change');
		}else if ($(this).val()=='2' || $(this).val()=='4'){
			$("#biaya_tf").val(0);
			$("#biayamaterai").val(0);
			$("#biayacek").val(0);
			$("#rekening_id").val('#').trigger('change');
		}else if ($(this).val()=='3'){
			$("#biaya_tf").val($("#b_tf").val());
			$("#biayamaterai").val(0);
			$("#biayacek").val(0);
		}
		if ($(this).val()=='3' || $(this).val()=='4'){		
			// alert(rekening_id_asal);
			$("#rekening_id").val(rekening_id_asal).trigger('change');
		}
		hitung_total();
	});
	$("#rekening_id").change(function() {
		if ($("#rekening_id").val() !='#'){
			rekening_id_asal=$("#rekening_id").val();			
		}
		load_rekening();
	});

	
	function hitung_total(){
		var ringkasan_total=$("#ringkasan_total").val();
		var grandtotalnominal=ringkasan_total - (parseFloat($("#biaya_tf").val()) + parseFloat($("#biayamaterai").val()) + parseFloat($("#biayacek").val()));
		$("#grandtotalnominal").val(grandtotalnominal);
		// $("#lbl_gt").text($("#grandtotalnominal").val());
		
	}
    function loadKbo_detail() {
		var tipe_distributor=$("#tipe_distributor").val();
		var tkontrabon_id=$("#tkontrabon_id").val();
		var tipe=$("#tipe").val();
		var no_po=$("#no_po").val();
		var no_terima=$("#no_terima").val();
		var no_fakur=$("#no_fakur").val();
		var tgl_trx=$("#tgl_trx").val();
		var tgl_trx2=$("#tgl_trx2").val();
		var disabel=$("#disabel").val();
		var st_verifikasi=$("#st_verifikasi").val();
		var tanggalkontrabon=$("#tanggalkontrabon").val();
		var iddistributor=$("#iddistributor").val();
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tkontrabon/loadKbo_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				tkontrabon_id: tkontrabon_id,
				tipe_distributor: tipe_distributor,
				tipe:tipe,
				no_po:no_po,no_terima:no_terima,
						no_fakur:no_fakur,
						tgl_trx:tgl_trx,tgl_trx2:tgl_trx2,disabel:disabel,st_verifikasi:st_verifikasi
						,tanggalkontrabon:tanggalkontrabon,iddistributor:iddistributor
			}
		},
		columnDefs: [
					{"targets": [9], "visible": false },
					 {  className: "text-right", targets:[7] },
					 {  className: "text-center", targets:[1,2,3,6,8] },
					 { "width": "5%", "targets": [0] },
					 // { "width": "8%", "targets": [5,6,7,8] },
					 { "width": "10%", "targets": [1,2,3,4,5,6,7] },
					 { "width": "15%", "targets": [8] }

					]
		});
	}
	function LoadBank() {
		var iddistributor=$("#iddistributor").val();
		
		$('#index_rekening').DataTable().destroy();
		var table = $('#index_rekening').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tkontrabon/LoadBank/',
			type: "POST",
			dataType: 'json',
			data: {
				iddistributor: iddistributor,
				
			}
		},
		columnDefs: [
					 {  className: "text-center", targets:[6,7] },
					 {  className: "text-left", targets:[2,3,4,5] },
					 { "width": "5%", "targets": [0,1,6] },
					 { "width": "10%", "targets": [3,5] },
					 { "width": "15%", "targets": [7,4,2] },
					 { "visible": false, "targets": [0,1] },

					]
		});
	}
	$(document).on("click",".edit_rekening",function(){
		var table = $('#index_rekening').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		$("#idrekening").val(table.cell(tr,0).data());
		$("#norekening").val(table.cell(tr,5).data());
		$("#atasnama").val(table.cell(tr,4).data());
		$("#cabang").val(table.cell(tr,3).data());
		$("#bankid").val(table.cell(tr,1).data()).trigger('change');
		$('#tab_add > a').tab('show');
		
    });
	$(document).on("click","#btn_clear_rekening",function(){		
		clear_rekening();
	});
	function clear_rekening(){
		$("#idrekening").val('');
		$("#norekening").val('');
		$("#atasnama").val('');
		$("#cabang").val('');
		$("#bankid").val('#').trigger('change');
	}
	$("#btn_filter").click(function() {
		loadKbo_detail();
	});
	$("#btn_update").click(function() {
		update_pembayaran();
	});
	
	
	function update_pembayaran(){
		var id=$("#tkontrabon_id").val();		
		var rekening_id=$("#rekening_id").val();		
		var carabayar=$("#carabayar").val();		
		var biaya_tf=$("#biaya_tf").val();		
		var biayamaterai=$("#biayamaterai").val();		
		var biayacek=$("#biayacek").val();		
		var grandtotalnominal=$("#grandtotalnominal").val();		
		var ringkasan_total=$("#ringkasan_total").val();	
			
		// alert(rekening_id+' '+carabayar);
		// return false;
		if (rekening_id=='#' && carabayar=='3'){
			sweetAlert("Maaf...", "Rekening Harus diisi!", "error");
			// $("#btn_serahkan").attr("disabled",true);
			return false;
		}
		if (rekening_id=='#' && carabayar=='4'){
			sweetAlert("Maaf...", "Rekening Harus diisi!", "error");
			// $("#btn_serahkan").attr("disabled",true);
			return false;
		}
		
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tkontrabon/update_pembayaran',
			type: 'POST',
			data: {
				id: id,
				carabayar: carabayar,
				biaya_tf: biaya_tf,
				biayamaterai: biayamaterai,
				biayacek: biayacek,
				grandtotalnominal: grandtotalnominal,		
				ringkasan_total: ringkasan_total,		
				rekening_id: rekening_id,		
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				sweetAlert("Berhasil...", "Data telah diubah!", "success");
				table.ajax.reload( null, false ); 
			}
		});
	
	}
	$("#btn_save_bank").click(function() {
		save_bank();
	});
	function aktivasi_bank(idrekening,tipe_distributor,iddistributor){
		// alert(idrekening+'-'+tipe_distributor+' '+iddistributor);
		// return false;
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkontrabon/aktivasi_bank',
			type: 'POST',
			data: {
				iddistributor: iddistributor,
				idrekening: idrekening,
				tipe_distributor: tipe_distributor,
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				sweetAlert("Berhasil...", "Data Bank disimpan!", "success");
				location.reload();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
	}
	function save_bank(){
		var tipe_distributor=$("#tipe_distributor").val();		
		var iddistributor=$("#iddistributor").val();		
		var bankid=$("#bankid").val();		
		var idrekening=$("#idrekening").val();		
		var cabang=$("#cabang").val();		
		var norekening=$("#norekening").val();		
		var atasnama=$("#atasnama").val();		
		// table = $('#index_rekening').DataTable()	
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkontrabon/save_bank',
			type: 'POST',
			data: {
				iddistributor: iddistributor,
				idrekening: idrekening,
				bankid: bankid,
				cabang: cabang,
				norekening: norekening,
				atasnama: atasnama,
				tipe_distributor: tipe_distributor,
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				sweetAlert("Berhasil...", "Data Bank disimpan!", "success");
				location.reload();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
	
	}
	$(document).on("click","#btn_add_tagihan",function(){	
		$("#modal_tambah_faktur").modal('show');
		
	});
	$(document).on("click","#btn_cari_tagihan",function(){	
		load_index_faktur();
	});
	
	function load_index_faktur(){
		var tipe=$("#xtipe").val();
		var iddistributor=$("#iddistributor").val();
		var no_po=$("#xno_po").val();
		var no_terima=$("#xno_terima").val();
		var no_fakur=$("#xno_fakur").val();
		var status='';
		var cara_bayar=$("#xcara_bayar").val();
		var tanggalterima1=$("#xtanggalterima1").val();
		var tanggalterima2=$("#xtanggalterima2").val();
		var tanggaljt1=$("#xtanggaljt1").val();
		var tanggaljt2=$("#xtanggaljt2").val();
		// alert(tanggalterima1);
		$('#index_faktur').DataTable().destroy();
		table = $('#index_faktur').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [{ "targets": [0,1], "visible": false },
								{ "width": "3%", "targets": [2] },
								{ "width": "8%", "targets": [4,5,6,7,10,12] },
								{ "width": "5%", "targets": [8,11,13] },
								{ "width": "10%", "targets": [3,9,14] },
								// { "width": "15%", "targets": [14] },
							 {"targets": [2], className: "text-left" },
							 {"targets": [10], className: "text-right" },
							 {"targets": [3,4,5,6,7,11,12,14], className: "text-center" }
							 ],
				ajax: { 
					url: '{site_url}tkontrabon/get_index_faktur', 
					type: "POST" ,
					dataType: 'json',
					data : {
							tipe:tipe,iddistributor:iddistributor,no_po:no_po,no_terima:no_terima,
							no_fakur:no_fakur,cara_bayar:cara_bayar,status:status,
							tanggalterima1:tanggalterima1,tanggalterima2:tanggalterima2,tanggaljt1:tanggaljt1,tanggaljt2:tanggaljt2
						   }
				}
			});
	}
	$(document).on("click",".pilih_trx",function(){	
		var table = $('#index_faktur').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idpenerimaan=table.cell(tr,1).data();
		var tanggalkontrabon=$("#tanggalkontrabon").val();
		var kontrabon_info_id=$("#kontrabon_info_id").val();
		var tkontrabon_id=$("#tkontrabon_id").val();
		$.ajax({
			url: '{site_url}tkontrabon/pilih_trx',
			type: 'POST',
			data: {
				idpenerimaan: idpenerimaan,
				tanggalkontrabon: tanggalkontrabon,
				kontrabon_info_id: kontrabon_info_id,
				tkontrabon_id: tkontrabon_id,
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				sweetAlert("Berhasil...", "Data Faktur sudah ditambahkan!", "success");
				location.reload();
				// table.ajax.reload( null, false ); 
			}
		});
	
	
	});
	$(document).on("click",".pilih_retur",function(){	
		var table = $('#index_faktur').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idpenerimaan=table.cell(tr,1).data();
		var tanggalkontrabon=$("#tanggalkontrabon").val();
		var kontrabon_info_id=$("#kontrabon_info_id").val();
		var tkontrabon_id=$("#tkontrabon_id").val();
		$.ajax({
			url: '{site_url}tkontrabon/pilih_retur',
			type: 'POST',
			data: {
				idpenerimaan: idpenerimaan,
				tanggalkontrabon: tanggalkontrabon,
				kontrabon_info_id: kontrabon_info_id,
				tkontrabon_id: tkontrabon_id,
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				sweetAlert("Berhasil...", "Data Telah ditambahkan!", "success");
				location.reload();
				// table.ajax.reload( null, false ); 
			}
		});	
	});
	$(document).on("click",".lihat_lampiran",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idpenerimaan=table.cell(tr,9).data();
		$("#view_photofaktur").modal('show');
		show_lampiran(idpenerimaan);
	});
	function show_lampiran($id){
		//list_index_lampiran
		var id=$id;
		$('#list_index_lampiran').DataTable().destroy();
		var table = $('#list_index_lampiran').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tkontrabon/list_index_lampiran/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,
				
			}
		},
		columnDefs: [
					 {  className: "text-center", targets:[0,1,2] },

					]
		});
	}
</script>