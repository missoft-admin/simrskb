<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Kontrabon Rekap</title>
    <style>
	@page {
			margin-top: 190px;
			margin-left:50px;
			margin-right:50px;
			margin-bottom:100px;
            }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
		 
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
		  padding-left:5px;
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .noborder-full {
		  padding-left:5px;
			border-bottom:0px solid #000 !important;
			border-top:0px solid #000 !important;
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
		
		header {
			position: fixed;
			top: -150px;
			left: 0px;
			right: 0px;
			height: 50px;

			
		}

		footer {
			position: fixed; 
			bottom: -260px; 
			left: 0px; 
			right: 0px;
			height: 50px; 
		}
		/*main {
			page-break-inside: auto;
		}
		*/
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
	<header>
		<table class="content">
		  <tr>
			<td width="20%" class="text-left"><img src="assets/upload/logo/logomenu.jpg" alt="" ></td>
			<td width="30%" class="text-bold text-judul text-center text-top" style="height:50px"><u>REKAP KONTRABON DETAIL</u><br>TANGGAL : <?=HumanDateShort($tanggal_kbo)?></td>	
			<td width="20%" class="text-bold text-judul text-center text-top" style="height:50px"></td>	
		  </tr>
		  	 
		</table>
	</header>
	<footer>
		<p style="position:fixed; bottom:40px; font-size: 18px; color: #3abad8;">
        JL. L.L.RE Martadinata No. 28 40115 Jawa Barat, Indonesia<br>T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com
      </p>
	</footer>
	<main>
		<table class="content">
		 
		  <tr>
			
		  </tr>		 
		</table>
		<?
			$header='<tr style="background-color:#eadccb">
			<th width="10%" class="border-full text-center"><strong>TANGGAL</strong></th>
			<th width="10%" class="border-full text-center"><strong>NO-SUPPLIER</strong></th>
			<th width="25%" class="border-full text-center"><strong>SUPPLIER</strong></th>
			<th width="15%" class="border-full text-center"><strong>NO. PENERIMAAN </strong></th>
			<th width="20%" class="border-full text-center"><strong>NO FAKTUR EXTERNAL</strong></th>
			<th width="15%" class="border-full text-center"><strong>JUMLAH</strong></th>
			<th width="15%" class="border-full text-center"><strong>PARAF</strong></th>
		  </tr>';
		
		?>
		<table id="customers">
		 
      <?php 
	  $nodistributor='';
	  $nama_dist='';
	  $total_bayar='0';
	  $all_bayar='0';
	  $nokbo='';
	  
	  ?>
      <?php foreach ($list_detail as $row){ ?>
        <?php 
		if ($nodistributor !=$row->iddistributor){			
			if ($nodistributor !=''){
				echo '
					<tr>
				<td colspan="3"  class="border-full text-center text-bold" style="height:20px">SUB TOTAL '.strtoupper($nama_dist).'</td>
				<td colspan="2"  class="border-full text-center text-bold"> '.$nokbo.'</td>
				<td class="border-full text-right text-bold">'.number_format($total_bayar,2).'&nbsp;</td>			 
				<td class="noborder-full text-center text-bold"></td>			 
				</tr>';
				
			}
			echo $header;
			$total_bayar=0;
			$nokbo=$row->nokontrabon;
			$nodistributor=$row->iddistributor;
			$nama_dist=$row->nama_dist;
		}
		$total_bayar=$total_bayar + $row->nominal_bayar;
		$all_bayar=$all_bayar + $row->nominal_bayar;
		?>
        <tr>
		  <td class="border-full text-center"><?=HumanDateShort($row->tanggal_kbo)?></td>
		  <td class="border-full text-center"><?=strtoupper($row->kode)?></td>
		  <td class="border-full text-left"><?=strtoupper($row->nama_dist)?></td>
		  <td class="border-full text-center"><?=($row->nopenerimaan)?></td>
		  <td class="border-full text-center"><?=($row->nofakturexternal)?></td>
		  <td class="border-full text-right"><?php echo number_format($row->nominal_bayar,2)?> &nbsp; </td>
		  <td class="noborder-full text-right"></td>
        </tr>
      <? 
		
	  } ?>
	  <?
	  echo '
			<tr>
				<td colspan="3"  class="border-full text-center text-bold" style="height:20px">SUB TOTAL '.strtoupper($nama_dist).'</td>
				<td colspan="2"  class="border-full text-center text-bold"> '.$nokbo.'</td>
				<td class="border-full text-right text-bold">'.number_format($total_bayar,2).'&nbsp;</td>			 
				<td class="noborder-full text-center text-bold"></td>			 
			</tr>';
	  
	  ?>
		<tr style="height:30px;background-color:#ececec">
			<td colspan="5"  class="border-full text-center text-bold"style="height:30px" >GRAND TOTAL</td>
			<td  class="border-full text-right text-bold"><?=number_format($all_bayar,2)?>&nbsp; </td>
			<td class="border-full text-center text-bold"></td>
			 
		</tr>
    </table>
	<br>
	<table id="customers" >
		 <tr>
			<td>Generate By SIMRS RSKB Halmahera Siaga <?=(date('d-m-Y H:i:s'))?></td>
			<td style="width:20%" class="text-center text-bold">KEPALA BAGIAN KEUANGAN</td>
			<td style="width:30%"  class="text-center text-bold">BENDAHAARA</td>
        </tr>
		<tr>
			<td></td>
			<td style="width:20%" class="text-center text-bold"></td>
			<td style="width:30%"  class="text-center text-bold"></td>
        </tr>
		<tr>
			<td></td>
			<td style="height:55px" class="text-center text-bold"></td>
			<td style="height:55px"  class="text-center text-bold"></td>
        </tr>
		<tr>
			<td></td>
			<td style="width:20%" class="text-center text-bold">( _____________________ )</td>
			<td style="width:30%"  class="text-center text-bold">( _____________________ )</td>
        </tr>
		
	</table>	
	
	</main>
	
	<br>
    
  </body>
</html>
