<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Kontrabon Detail</title>
    <style>
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
		  
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 18px !important;
      }
	  .text-detail{
		font-size: 15px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td rowspan="3" width="30%" class="text-center"><img src="{logo1_rs}" alt="" width="60" height="60"></td>
        <td rowspan="2" colspan="2" width="50%"  class="text-center text-bold text-judul"><u><?=$ket?></u></td>
		<td rowspan="2" width="20%"></td>		
      </tr>
	  <tr></tr>
	  <tr>
		<td width="14%" class="text-left text-bold text-top">NO BUKTI</td>
		<td width="60%" colspan="" class="text-top">: <?=$nokontrabon?></td>		
	  </tr>
	  <tr>
	    <td  class="text-center">{alamat_rs1}</td>
		<td  class="text-left text-bold text-top">TANGGAL KEMBALI</td>
		<td width="60%" colspan="2" class="text-top">: <?=HumanDateShort($tanggalkontrabon)?></td>
	  </tr>
	  <tr>
	    <td  class="text-center"><?=$telepon_rs.' '.$fax_rs?></td>
		<td class="text-left text-bold text-top">DISTRIBUTOR</td>
		<td class=" text-top" colspan="2" >: <?=strtoupper($nama_dist)?></td>
	  </tr>
	  
    </table>
    
	<br>
    <table id="customers">
      <tr>
        <th width="5%" class="border-full text-right text-top"><strong>NO</strong></th>
        <th width="25%" class="border-full text-center text-top"><strong>TANGGAL FAKTUR</strong></th>
        <th width="35%" class="border-full text-center text-top"><strong>NO PENERIMAAN</strong></th>
        <th width="15%" class="border-full text-center text-top"><strong>NOMINAL</strong></th>
        <th width="25%" class="border-full text-center text-top"><strong>NO FAKTUR </strong></th>
      </tr>
      <?php $number = 0; $total_bayar=0;$harga=0;?>
      <?php foreach ($list_detail as $row){ ?>
        <?php 
		$number = $number + 1; 
		
		// if ($row->status=='1'){
		$total_bayar = $total_bayar + $row->totalharga ;
		// }
		// $total=0;
		?>
        <tr>
          <td class="border-full text-right text-detail"> <?=$number?>&nbsp;&nbsp;</td>
		  <td class="border-full text-center text-detail">&nbsp;&nbsp;<?=HumanDateShort($row->tgl_terima)?></td>
		  <td class="border-full text-center text-detail"><?=strtoupper($row->nopenerimaan)?></td>
		  <td class="border-full text-right text-detail"><?php echo number_format($row->totalharga,2)?> &nbsp;&nbsp;</td>
          <td class="border-full text-center text-detail">&nbsp;<?=$row->nofakturexternal?></td>
        </tr>
      <? } ?>
		<tr>
			<td colspan="3"  class="border-full text-center text-bold text-detail">TOTAL</td>
			<td  class="border-full text-right text-bold text-detail"><?=number_format($total_bayar,2)?>&nbsp;&nbsp;&nbsp;</td>
			<td  class="border-full text-right text-bold text-detail"></td>
			
		</tr>
    </table><br>
   <table id="customers">
      <tr>
        <th width="100%" colspan="3" class="text-left text-italic text-detail">TERBILANG : <?=terbilang($total_bayar)?></th>
        
      </tr>
	  <tr>
        <th width="60%" class="text-left text-detail">Kembali / Tanggal Kontrabon : </th>
        <th width="20%" class="text-center"><strong></strong></th>
        <th width="20%" class="text-right text-detail"><strong>Bandung, <?=HumanDateShort(date('Y-m-d'))?></strong><br></th>
      </tr>
	  <tr>
        <th width="60%" class="text-right"></th>
        <th width="20%" class="border-full text-center text-detail">Yang Menerima</th>
        <th width="20%" class="border-full text-center text-detail">Yang Menyerahkan</th>
      </tr>
	  <tr>
        <th width="60%" class="text-right"><strong></strong></th>
        <th width="20%" class="border-full text-center"><strong></strong></th>
        <th width="20%" class="border-full text-right"><br>&ensp; <br>&ensp; <br>&ensp; <br>&ensp; <br>&ensp; </th>
      </tr>
      
    </table><br>
  </body>
</html>
