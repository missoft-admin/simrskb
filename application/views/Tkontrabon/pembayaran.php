<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php echo form_open('tkontrabon/save_pembayaran', 'class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">

                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Nomor KBO</label>
                        <div class="col-md-9">
                            <input class="form-control" value="{nokontrabon}" disabled>
                            <input class="form-control" type="hidden" name="tkontrabon_id" id="tkontrabon_id" value="{id}" >
                            <input class="form-control" type="hidden" name="iddistributor" id="iddistributor" value="{iddistributor}" >
                            <input class="form-control" type="hidden" name="tanggalkontrabon" id="tanggalkontrabon" value="{tanggalkontrabon}" >
                            <input class="form-control" type="hidden" name="kontrabon_info_id" id="kontrabon_info_id" value="{kontrabon_info_id}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                            <input class="form-control" type="hidden" name="st_verifikasi" id="st_verifikasi" value="{st_verifikasi}" >
                            <input class="form-control" type="hidden" name="st_kbo" id="st_kbo" value="{st_kbo}" >
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Distributor</label>
                        <div class="col-md-9">
                            <input class="form-control" value="{nama_dist}" disabled>
                        </div>
                    </div>
					<div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Bank</label>
                        <div class="col-md-9">
                            <input class="form-control" id="bank" value="" disabled>
                        </div>
                    </div>
					<div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">No Rekening</label>
                        <div class="col-md-9">
                            <input class="form-control" id="norekening" value="" disabled>
                        </div>
                    </div>
					

                </div>
				<div class="col-md-6" style="margin-bottom: 5px;">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Alamat</label>
                        <div class="col-md-9">
                            <input class="form-control" value="{alamat}" disabled>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">No Rekening</label>
                        <div class="col-md-9">
                           <select id="rekening_id" name="rekening_id" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one.." <?=($st_kbo!='4'?$disabel:'')?>>
								<option value="#" selected>-Tidak Ada Rekening-</option>
								<?foreach($list_bank_dist as $row){?>
									<option value="<?=$row->id?>" selected><?=$row->bank.' ['.$row->norekening.']'?></option>
								<?}?>
							</select>
                        </div>
                    </div>
					<div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Atas Nama</label>
                        <div class="col-md-9">
                            <input class="form-control" id="atasnama" value="" disabled>
                        </div>
                    </div>
					<div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Cabang</label>
                        <div class="col-md-9">
                            <input class="form-control" id="cabang" value="" disabled>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <div class="col-md-3">

						</div>
                        <div class="col-md-9">
							<a href="{base_url}tkontrabon/detail_kontrabon/<?=$id?>/1" class="btn btn-primary  text-uppercase" type="button"  name="btn_add_tagihan" id="btn_add_tagihan" <?=$disabel?>>
                                <i class="fa fa-eye"></i> Lihat
                            </a>
                        </div>
                    </div>
                </div>

        </div>

        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>

<div class="block">

    <div class="block-content">
        <div class="row">

			<div class="col-md-12">
				<h3><span class="label label-success">RINGKASAN PEMBAYARAN </span></h3><br>
				<table class="table">
					<tr>
						<td width="50%" class="bg-info text-white text-right">TOTAL PEMBAYARAN </td>
						<td ><input type="text" class="form-control decimal" id="ringkasan_total" name="ringkasan_total" value="{totalnominal}" readonly></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>CARA PEMBAYARAN </strong></td>
						<td ><select disabled id="carabayar" name="carabayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." <?=($st_kbo!='4'?$disabel:'')?>>
							<option value="#">- Semua Cara Bayar -</option>
							<option value="1" <?=("1" == $carabayar ? 'selected' : ''); ?>>CHEQ</option>
							<option value="2" <?=("2" == $carabayar ? 'selected' : ''); ?>>TUNAI</option>
							<option value="4" <?=("4" == $carabayar ? 'selected' : ''); ?>>TRANSFER</option>
							<option value="3" <?=("3" == $carabayar ? 'selected' : ''); ?>>TRANSFER BEDA BANK</option>

						</select></td>
					</tr>

					<tr>
						<td width="50%" class="text-right"><strong>REKENING</strong></td>
						<td ><label id="rek_dipake"></label></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>BIAYA TRANSFER</strong></td>
						<td ><input type="text" disabled class="form-control number hitung" id="biaya_tf" <?=$disabel?> name="biaya_tf" value="{biaya_tf}"></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>BIAYA MATERAI</strong></td>
						<td ><input type="text"  disabled class="form-control number hitung" id="biayamaterai" <?=$disabel?> name="biayamaterai" value="{biayamaterai}"></td>
					</tr>
					<tr>
						<td width="50%" class="text-right"><strong>BIAYA CEQ</strong></td>
						<td ><input type="text" disabled class="form-control number hitung" id="biayacek" <?=$disabel?> name="biayacek" value="{biayacek}"></td>
					</tr>
					<tr>
						<td width="50%" class="bg-info text-white text-right">GRAND TOTAL PEMBAYARAN</td>
						<td ><input type="text" disabled class="form-control decimal" id="grandtotalnominal" <?=$disabel?> name="grandtotalnominal" value="{grandtotalnominal}" readonly></td>
					</tr>

					<tr>
						<td></td>
						<td width="50%">
							<input type="hidden" class="form-control decimal" id="nominal" <?=$disabel?> name="nominal" value="{grandtotalnominal}" readonly>
							<button class="btn btn-success text-uppercase" <?=($st_kbo!='4'?$disabel:'')?> type="button" name="btn_add_pembayaran" id="btn_add_pembayaran" style="font-size:13px;width:100%;float:right;"><i class="fa fa-plus"></i> PEMBAYARAN</button>
						</td>
					</tr>
				</table>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">

                    </div>
                </div>
				<div class="form-group" style="margin-top: 20px;"></div>
			</div>
		</div>
		<div class="row">
			<div class="block-content block-content">
				<input type="hidden" class="form-control" id="rowindex">
				<input type="hidden" class="form-control" id="iddet">
				<div class="form-group">
					 <table class="table table-bordered" id="list_pembayaran">
						<thead>
							<tr>
								<th width="10%" class="text-center">Jenis Kas</th>
								<th width="15%" class="text-center">Sumber Kas</th>
								<th width="10%" class="text-center">Metode</th>
								<th width="10%" class="text-center" >Tgl Pencairan</th>
								<th width="15%" class="text-center">Keterangan</th>
								<th width="10%" class="text-center">Nominal</th>
								<th width="10%" class="text-center">Biaya Transfer</th>
								<th width="15%" class="text-center">Actions</th>

							</tr>
						</thead>
						</thead>
						<tbody>
							<?
								$no=1;
								if ($list_pembayaran){
									foreach($list_pembayaran as $row){ ?>
										<tr>
										<td><?=$row->jenis_kas?></td>
										<td><?=$row->sumber_kas?></td>
										<td><?=metodePembayaran_bendahara($row->idmetode)?></td>
										<td><?=HumanDateShort($row->tanggal_pencairan)?></td>
										<td><?=$row->ket_pembayaran?></td>
										<td align="right"><?=number_format($row->nominal_bayar,2)?></td>
										<td align="right"><?=number_format($row->biaya_tf,2)?></td>
										<td><button type='button'  <?=$disabel?>  class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button'  <?=$disabel?>  class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='<?=$row->id?>'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='<?=$row->jenis_kas_id?>'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='<?=$row->sumber_kas_id?>'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='<?=$row->idmetode?>'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='<?=$row->ket_pembayaran?>'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='<?=$row->nominal_bayar?>'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xtanggal_pencairan[]' value='<?=HumanDateShort($row->tanggal_pencairan)?>'></td>
										<td style='display:none'><input readonly type='text' class='form-control' name='xbiaya_transfer[]' value='<?=$row->biaya_tf?>'></td>
										</tr>
									<?
									$no=$no+1;
									}
								}
							?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5" class="text-right"> <strong>Total Bayar </strong></td>
								<td  colspan="2"> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="0"/></td>
							</tr>
							<tr>
								<td colspan="5" class="text-right"> <strong>Sisa</strong></td>
								<td  colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
							</tr>
							<tr>
								<td colspan="5" class="text-right"> <strong>Sisa Biaya Transfer</strong></td>
								<td  colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa_biaya_tf" name="total_sisa_biaya_tf" value="0"/> </td>
							</tr>
						</tfoot>
					</table>
				</div>
				<? if ($disabel==''){ ?>
				<div class="form-group hiden btn_hide">
					<div class="text-right bg-light lter">
						<!--<button class="btn btn-success" type="submit"  name="btn_simpan" value="2" hidden>Simpan & Verifikasi</button> -->
						<button class="btn btn-primary" type="submit"  name="btn_simpan" value="1">Simpan</button>

					</div>
				</div>
				<?}?>

			</div>
		</div>
    </div>
</div>
<?php echo form_close(); ?>
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>

				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Rekening. </label>
							<div class="col-md-7">
								<input readonly type="text" class="form-control" id="xnorekening" placeholder="No Rekening" name="xnorekening">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
							<div class="col-md-7">
								<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
							<div class="col-md-7">
								<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">- Pilih Opsi -</option>
									<? foreach($list_jenis_kas as $row){ ?>
										<option value="<?=$row->id?>" <?=$row->st_default=='1'?'selected':''?>><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="sumber_kas_id">Sumber Kas</label>
							<div class="col-md-7">
								<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="idmetode">Metode</label>
							<div class="col-md-7">
								<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">Pilih Opsi</option>
									<option value="1">Cheq</option>
									<option value="2">Tunai</option>
									<option value="4">Transfer Antar Bank</option>
									<option value="3">Transfer Beda Bank</option>
								</select>
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
							<div class="col-md-7">
								<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
								<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
								<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" >
							</div>
						</div>
						<div class="form-group" id="div_tf">
							<label class="col-md-3 control-label" for="nama">Biaya Transfer Rp. </label>
							<div class="col-md-7">
								<input  type="text" class="form-control number" id="biaya_transfer" placeholder="Biaya Transfer" name="biaya_transfer" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Tanggal Pencairan</label>
							<div class="col-md-7">
								<div class="input-group date">
								<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Keterangan </label>
							<div class="col-md-7">
								<textarea class="form-control" name="ket_pembayaran" id="ket_pembayaran"></textarea>
							</div>
						</div>

						<div class="modal-footer">
							<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


    var table;

    $(document).ready(function(){
		$(".number").number(true,2,'.',',');
		$(".decimal").number(true,2,'.',',');
		checkTotal();
      get_data_rek();

    });
	$(document).on("click","#btn_add_pembayaran",function(){
		// if ($("#nominal").val()=='' || $("#nominal").val()=='0'){
			// return false;
		// }
		$("#idmetode").val($("#carabayar").val()).trigger('change');
		$("#modal_pembayaran").modal('show');
		if ($("#idmetode").val()=='3'){
			$("#biaya_transfer").val($("#total_sisa_biaya_tf").val());
		}
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat($("#total_bayar").val()));
		$("#sisa_modal").val($("#total_sisa").val());
		$("#nominal_bayar").val($("#total_sisa").val());
		get_data_rek();
		
		var sumber_kas_id='';
		var jenis_kas_id=$("#jenis_kas_id").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {
				$("#sumber_kas_id").empty();
				$('#sumber_kas_id').append(data.detail);
			}
		});

	});
	function get_data_rek(){
		$("#xnorekening").val($("#rekening_id option:selected").text());
		var rekening_id=$("#rekening_id").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/get_data_rek',
			type: 'POST',
			dataType: "json",
			data: {rekening_id: rekening_id},
			success : function(data) {
				if (data){
					$("#norekening").val(data.norekening)
					$("#atasnama").val(data.atasnama)
					$("#bank").val(data.bank)
					$("#cabang").val(data.cabang)
				}else{
					$("#norekening").val('')
					$("#atasnama").val('')
					$("#bank").val('')
					$("#cabang").val('')
				}
				// alert(data.norekening);
			}
		});
	}
	$(document).on("change","#rekening_id",function(){
		get_data_rek();
	});
	$(document).on("change","#idmetode",function(){
		if ($(this).val()=='3'){
			$("#div_tf").show();
		}else{
			$("#div_tf").hide();
			$("#biaya_transfer").val(0);
		}
	});
	$(document).on("change","#jenis_kas_id",function(){
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		// alert(sumber_kas_id);
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {
				$("#sumber_kas_id").empty();
				console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	$(document).on("keyup","#nominal_bayar",function(){
		if (parseFloat($("#sisa_modal").val()) < parseFloat($("#nominal_bayar").val())){
			$("#nominal_bayar").val($("#sisa_modal").val());
		}
	});
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function add_pembayaran(){

		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#tanggal_pencairan").val() == "") {
			sweetAlert("Maaf...", "Tanggal Pembayaran  Harus Diisi!", "error");
			$("#tanggal_pencairan").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		var content = "";

		var row_index;
		var duplicate = false;

		if($("#rowindex").val() != ''){
			var number = $("#number").val();
			var content = "";
			row_index = $("#rowindex").val();
		}else{
			var number = $('#list_pembayaran tr').length;
			var content = "<tr>";
			// alert($("#idmetode option:selected").text());
			$('#list_pembayaran tbody tr').filter(function (){
				var $cells = $(this).children('td');
				console.log($cells.eq(1).text());
				if($cells.eq(1).text() === $("#idmetode option:selected").text()){
					sweetAlert("Maaf...", $("#pembayaran_jenis option:selected").text() + " sudah ditambahkan.", "error");
					duplicate = true;

					pembayaran_clear();
					return false;
				}
			});
		}

		if(duplicate == false){
			var ket=$('#ket_pembayaran').summernote('code');
			content += "<td>" + $("#jenis_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#sumber_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#idmetode option:selected").text() + "</td>";
			content += "<td>"+$("#tanggal_pencairan").val()+"</td>";
			content += "<td>" + ket + "</td>";
			content += "<td align='right'>" + formatNumber($("#nominal_bayar").val()) + "</td>";
			content += "<td align='right'>" + formatNumber($("#biaya_transfer").val()) + "</td>";
			content += "<td><button type='button' class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>";
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='"+$("#iddet").val()+"'></td>";//8
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='"+$("#jenis_kas_id").val()+"'></td>";//9
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='"+$("#sumber_kas_id").val()+"'></td>";//10
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='"+$("#idmetode").val()+"'></td>";//11
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='"+ket+"'></td>";//12
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='"+$("#nominal_bayar").val()+"'></td>";//13
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>";//14
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xtanggal_pencairan[]' value='"+$("#tanggal_pencairan").val()+"'></td>";//15
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xbiaya_transfer[]' value='"+$("#biaya_transfer").val()+"'></td>";//16
			if($("#rowindex").val() != ''){
				$('#list_pembayaran tbody tr:eq(' + row_index + ')').html(content);
			}else{
				content += "</tr>";
				$('#list_pembayaran tbody').append(content);
			}

			pembayaran_clear();
			checkTotal();
		}


	}
	$(document).on("click",".pembayaran_edit",function(){
		$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
		$("#modal_pembayaran").modal('show');
		$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
		$("#tanggal_pencairan").val($(this).closest('tr').find("td:eq(15) input").val());
		$("#biaya_transfer").val($(this).closest('tr').find("td:eq(16) input").val());
		$("#sumber_kas_id_tmp").val($(this).closest('tr').find("td:eq(10) input").val());
		$("#jenis_kas_id").val($(this).closest('tr').find("td:eq(9) input").val()).trigger('change');
		$("#nominal_bayar").val($(this).closest('tr').find("td:eq(13) input").val());
		$("#ket_pembayaran").summernote('code',$(this).closest('tr').find("td:eq(12) input").val());
		$("#idmetode").val($(this).closest('tr').find("td:eq(11) input").val()).trigger('change');
		$("#sisa_modal").val(parseFloat($("#total_sisa").val()) + parseFloat($("#nominal_bayar").val()));
		// sisa_modal($();
		// $("#pembayaran_id").val($(this).closest('tr').find("td:eq(0)").val());


		return false;
	});
	$(document).on("click", ".pembayaran_remove", function() {
		var tr=$(this);
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menghapus Pembayaran ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				var iddet=tr.closest('tr').find("td:eq(8) input").val()
				if (iddet==''){
					tr.closest('td').parent().remove();

				}else{
					tr.closest('tr').find("td:eq(14) input").val(0)
					tr.closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
				}
				checkTotal();
			});

			// if (confirm("Hapus data ?") == true) {

			// }
		// return false;
	});
	function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function checkTotal(){
		var total_pembayaran = 0;
		var total_biaya_transfer = 0;
		$('#list_pembayaran tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(14) input").val()=='1'){
				total_pembayaran += parseFloat($(this).closest('tr').find("td:eq(13) input").val());
				total_biaya_transfer += parseFloat($(this).closest('tr').find("td:eq(16) input").val());
			}
		});
		console.log(total_biaya_transfer);
		$("#total_bayar").val(total_pembayaran);
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat(total_pembayaran));
		$("#total_sisa_biaya_tf").val(parseFloat($("#biaya_tf").val()) - parseFloat(total_biaya_transfer));
	}
	function pembayaran_clear(){
		$("#sumber_kas_id_tmp").val('');
		$("#number").val('');
		$("#rowindex").val('');
		$("#modal_pembayaran").modal('hide');
		clear_input_pembayaran();
	}
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
		$("#iddet").val('');
	}
	function validate_final(){
		// if ($("#idpendapatan").val() == "#") {
			// sweetAlert("Maaf...", "Pendapatan tidak boleh kosong!", "error");
			// return false;
		// }
		// if ($("#terimadari").val() == "#") {
			// sweetAlert("Maaf...", "Sumber Pendapatan tidak boleh kosong!", "error");
			// return false;
		// }
		if ($("#total_sisa_biaya_tf").val() != "0") {
			sweetAlert("Maaf...", "Biaya Transfer Belum Valid!", "error");
			return false;
		}
		if ($("#total_bayar").val() == "0") {
			sweetAlert("Maaf...", "Pembayaran Belum Dipilih!", "error");
			return false;
		}
		if ($("#total_sisa").val() != "0") {
			sweetAlert("Maaf...", "Masih Ada Sisa!", "error");
			return false;
		}
	}
</script>
