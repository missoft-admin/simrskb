<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Kontrabon Rekap</title>
    <style>
	@page {
			margin-top: 190px;
			margin-left:50px;
			margin-right:50px;
			margin-bottom:100px;
            }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
		 
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
		  padding-left:5px;
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
		
		header {
			position: fixed;
			top: -150px;
			left: 0px;
			right: 0px;
			height: 50px;

			
		}

		footer {
			position: fixed; 
			bottom: -260px; 
			left: 0px; 
			right: 0px;
			height: 50px; 
		}
		/*main {
			page-break-inside: auto;
		}
		*/
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
	<header>
		<table class="content">
		  <tr>
			<td width="20%" class="text-left"><img src="assets/upload/logo/logomenu.jpg" alt="" ></td>
			<td width="30%" class="text-bold text-judul text-center text-top" style="height:50px"><u>REKAP KONTRABON</u><br>TANGGAL : <?=HumanDateShort($tanggal_kbo)?></td>	
			<td width="20%" class="text-bold text-judul text-center text-top" style="height:50px"></td>	
		  </tr>
		  	 
		</table>
	</header>
	<footer>
		<p style="position:fixed; bottom:40px; font-size: 18px; color: #3abad8;">
        JL. L.L.RE Martadinata No. 28 40115 Jawa Barat, Indonesia<br>T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com
      </p>
	</footer>
	<main>
		<table class="content">
		 
		  <tr>
			
		  </tr>		 
		</table>
		<table id="customers">
		  <tr>
			<th width="5%" class="border-full text-right"><strong>NO</strong></th>
			<th width="10%" class="border-full text-center"><strong>NO-SUPPLIER</strong></th>
			<th width="25%" class="border-full text-center"><strong>SUPPLIER</strong></th>
			<th width="10%" class="border-full text-center"><strong>TANGGAL</strong></th>
			<th width="15%" class="border-full text-center"><strong>NO. KONTRABON </strong></th>
			<th width="15%" class="border-full text-center"><strong>JUMLAH</strong></th>
			<th width="15%" class="border-full text-center"><strong>PEMBAYARAN</strong></th>
			<th width="20%" class="border-full text-center"><strong>NO REKENING</strong></th>
			<th width="15%" class="border-full text-center"><strong>PARAF</strong></th>
		  </tr>
      <?php $number = 0; $total_bayar=0;$harga=0;
		$tunai=0;
		$tunai_tf=0;
		$tf=0;
		$cheq=0;
	  ?>
      <?php foreach ($list_detail as $row){ ?>
        <?php 
		$number = $number + 1; 
		
		// if ($row->status=='1'){
		$total_bayar = $total_bayar + $row->grandtotalnominal ;
		if ($row->carabayar=='2'){
			$tunai=$tunai + $row->grandtotalnominal;
		}
		if ($row->carabayar=='4'){
			$tunai_tf=$tunai_tf + $row->grandtotalnominal;
		}
		if ($row->carabayar=='3'){
			$tf=$tf + $row->grandtotalnominal;
		}
		if ($row->carabayar=='1'){
			$cheq=$cheq + $row->grandtotalnominal;
		}
		// }
		// $total=0;
		?>
        <tr>
          <td class="border-full text-right" style="height:45px"> <?=$number?>&nbsp;&nbsp;</td>
		  <td class="border-full text-center"><?=strtoupper($row->kode)?></td>
		  <td class="border-full text-left"><?=strtoupper($row->nama_dist)?></td>
		  <td class="border-full text-center"><?=HumanDateShort($row->tanggal_kbo)?></td>
		  <td class="border-full text-center"><?=($row->nokontrabon)?></td>
		  <td class="border-full text-right"><?php echo number_format($row->grandtotalnominal,2)?> &nbsp; </td>
		  <td class="border-full text-center"><?=cara_bayar2($row->carabayar)?></td>
          <td class="border-full text-center"><?=$row->nama_bank?><?=($row->nama_bank?' - '.$row->norekening:'')?></td>
		  <td class="border-full text-right"></td>
        </tr>
      <? 
		
	  } ?>
		<tr>
			<td colspan="5"  class="border-full text-center text-bold" style="height:30px">TOTAL</td>
			<td  class="border-full text-right text-bold"><?=number_format($total_bayar,2)?>&nbsp; </td>
			<td class="border-full text-center text-bold"></td>
			<td class="border-full text-center text-bold"></td>
			<td class="border-full text-center text-bold"></td>
			 
		</tr>
    </table>
	<br>
	<table id="customers" >
		 <tr>
			<td>Generate By SIMRS RSKB Halmahera Siaga <?=(date('d-m-Y H:i:s'))?></td>
			<td style="width:20%" class="text-center text-bold">KEPALA BAGIAN KEUANGAN</td>
			<td style="width:30%"  class="text-center text-bold">BENDAHAARA</td>
        </tr>
		<tr>
			<td></td>
			<td style="width:20%" class="text-center text-bold"></td>
			<td style="width:30%"  class="text-center text-bold"></td>
        </tr>
		<tr>
			<td></td>
			<td style="height:55px" class="text-center text-bold"></td>
			<td style="height:55px"  class="text-center text-bold"></td>
        </tr>
		<tr>
			<td></td>
			<td style="width:20%" class="text-center text-bold">( _____________________ )</td>
			<td style="width:30%"  class="text-center text-bold">( _____________________ )</td>
        </tr>
		
	</table>	
	<table id="customers" style="display:none">
		 <tr>
			<td class="border-full text-center text-judul" colspan="3">DETAIL</td>
			<td style="width:60%"></td>
        </tr>
		<tr>
			<td class="border-full text-right" style="width:5%">1</td>
			<td class="border-full text-left" style="width:20%">TUNAI</td>
			<td class="border-full text-right" style="width:15%"><?=number_format($tunai,2)?></td>
			<td style="width:55%"></td>
        </tr>
		<tr>
			<td class="border-full text-right" style="width:5%">2</td>
			<td class="border-full text-left" style="width:20%">TRANSFER</td>
			<td class="border-full text-right" style="width:15%"><?=number_format($tunai_tf,2)?></td>
			<td style="width:60%"></td>
        </tr>
		<tr>
			<td class="border-full text-right" style="width:5%">3</td>
			<td class="border-full text-left" style="width:20%">TRANSFER BEDA BANK</td>
			<td class="border-full text-right" style="width:15%"><?=number_format($tf,2)?></td>
			<td style="width:60%"></td>
        </tr>
		<tr>
			<td class="border-full text-right" style="width:5%">4</td>
			<td class="border-full text-left" style="width:20%">CHEQ</td>
			<td class="border-full text-right" style="width:15%"><?=number_format($cheq,2)?></td>
			<td style="width:60%"></td>
        </tr>
		<tr>
			<td style="height:30px" class="text-left" colspan="3"></td>
			<td style="width:60%"></td>
        </tr>
	</table>
	</main>
	
	<br>
    
  </body>
</html>
