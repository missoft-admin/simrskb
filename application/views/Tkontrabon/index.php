<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="row">
    <div class="col-md-4">
        <div class="block">
            <div class="block-header bg-gray-lighter">
                <h3 class="block-title">Filter Kontrabon</h3>
            </div>
            <div class="block-content">
                <form action="{site_url}tkontrabon/index" class="form-horizontal" onsubmit="return validate_final()" id="frm1" method="POST" accept-charset="utf-8">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label">Tanggal KBO</label>
                        <div class="col-md-8">
							<div class="input-group date">
								<input class="js-datepicker form-control input"  type="text" id="tanggalkontrabon" name="tanggalkontrabon" value="{tanggalkontrabon}" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							<input class="form-control" type="hidden" name="tanggal_pilih" id="tanggal_pilih" value="{tanggalkontrabon}" >
							<input type="hidden" class="form-control detail" id="idkbo" name="idkbo" value="{idkbo}">
							<input type="hidden" class="form-control detail" id="st_kbo" name="st_kbo" value="{st_kbo}">
						</div>
                    </div>
                    <div class="form-group" style="margin-bottom: 10x;">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-8">
                            <button class="btn btn-success btn-block text-uppercase btn-sm" type="submit" value="1" id="btn_reload" name="btn_post">
                                <i class="fa fa-refresh"></i> Load Data
                            </button>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label">Status KBO</label>
                        <div class="col-md-8">
							<? if ($st_kbo=='0') {?>
								<? if ($jml_list=='0') {?>							
									<span class="label label-danger" style="font-size:12px">TIDAK ADA LIST VERIFIKASI</span>
								<?}else{?>
									<span class="label label-danger" style="font-size:12px">Belum Verifikasi</span>
								<?}?>
							<?}?>
							<? if ($st_kbo=='1') {?>
								<span class="label label-info" style="font-size:12px">Proses Verifikasi</span>
							<?}?>
							<? if ($st_kbo=='2') {?>
								<span class="label label-primary" style="font-size:12px">Telah diverifikasi -  Menunggu Proses Validasi</span>
							<?}?>
							<? if ($st_kbo=='3') {?>
								<span class="label label-primary" style="font-size:12px">Telah diaktifasi -  Menunggu Persetujuan</span>
							<?}?>
							<? if ($st_kbo=='4') {?>
								<span class="label label-success" style="font-size:12px">Telah Selesai Persetujuan</span>
							<?}?>
                        </div>						
                    </div>
					<? if ($st_kbo=='3') {?>
					<div class="form-group" id="div_user">
						<label class="col-md-4 control-label"></label>
						<div class="col-md-8">
							<table class="table table-bordered" id="data_user">
								<thead>
									<tr>
										<th width="60%" class="text-left">User Aktifasi</th>
										<th width="40%" class="text-left">Status</th>
										
									</tr>
								</thead>
								<tbody>
									<?foreach($list_user_aktifasi  as $r){?>
									<tr>
										
										<td class='text-left'><?=$r->name?></td>
										<td  class='text-left'>
											
											<?
												if ($r->user_id==$this->session->userdata('user_id') && $r->approve=='0'){
													echo '<button  class="btn btn-success btn-block  btn-xs" type="button" id="btn_setuju" name="btn_setuju"><i class="fa fa-check"></i> Setujui</button>';
												}else{
													echo status_persetujuan($r->approve);
												}
											?>
											
										</td>
									<tr>
									<?}?>
									
								</tbody>
							</table>
						</div>
						
                    </div>
					<?}?>
                    <div class="form-group" id="div_button">
						<label class="col-md-4 control-label"></label>
                        <div class="col-md-8">
							<div class="text-right push-20">
								<button style="display:<?=($st_kbo =='0' && $jml_list > 0)?'block':'none'?>" <?=($jml_list =='0'?'disabled':'')?> class="btn btn-primary btn-block  btn-sm" type="submit" id="btn_post" name="btn_post" value="2"><i class="fa fa-check"></i> Verifikasi </button>
								<button style="display:<?=($st_kbo =='1')?'block':'none'?>" class="btn btn-warning btn-block  btn-sm" type="submit" id="btn_batal_verifikasi" name="btn_post" value="3"><i class="fa fa-times"></i> Batal Verifikasi </button>
								<button style="display:<?=($st_kbo =='1')?'block':'none'?>" <?=($jml_list > 0?'disabled':'')?> <?=($jml_minus > 0?'disabled':'')?> class="btn btn-danger btn-block  btn-sm" type="button" id="btn_validasi" name="btn_validasi"><i class="fa fa-gavel"></i> Selesai Verifikasi</button>                        
								<button style="display:<?=($st_kbo =='2')?'block':'none'?>" class="btn btn-danger btn-block  btn-sm" type="button" id="btn_batal_validasi" name="btn_batal_validasi"><i class="fa fa-chevron-left"></i>  Batal Validasi</button>                        
								<?if ($st_kbo=='2'){?>
								<button style="display:<?=($st_kbo =='2')?'block':'none'?>" class="btn btn-info btn-block  btn-sm" type="button" id="btn_aktivasi" name="btn_aktivasi"><i class="fa fa-send"></i> Selesai Validasi</button>                        
								<?}?>
								
								<a type="button" href="{base_url}tkontrabon/print_rekap/<?=$idkbo?>/1" target="_blank" style="display:<?=($st_kbo !='0')?'block':'none'?>" class="btn btn-primary btn-block  btn-sm" type="button" id="btn_cetak" name="btn_cetak" hidden><i class="fa fa-print"></i> Rekap</a>
								<a type="button" href="{base_url}tkontrabon/print_rekap_detail/<?=$idkbo?>/1" target="_blank" style="display:<?=($st_kbo !='0')?'block':'none'?>" class="btn btn-info btn-block  btn-sm" type="button" id="btn_cetak" name="btn_cetak" hidden><i class="fa fa-print"></i> Rekap Detail</a>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="block">
            <table class="table table-bordered">
                <tr class="active">
                    <td colspan="3">Informasi Cara Pembayaran </td>
                </tr>
                <tr>
                    <td colspan="3" id="cara_pembayaran" class="text-left">
                       <button class="btn btn-primary btn-xs btn-block">
                            Rp. <?php echo number_format($total_pembayaran,2) ?>
                        </button>                   
                    </td>
                </tr>
                
                <tr>
                    <td class="text-center" width="33%"><button class="btn btn-info btn-block btn-xs"><?php echo number_format($count_cash) ?> Cash / TF</button>
					<button class="btn btn-success btn-block btn-xs"><?php echo number_format($tot_cash,2) ?></button></td>
                    <td class="text-center" width="33%"><button class="btn btn-info btn-block btn-xs"><?php echo number_format($count_tf) ?> TF Beda Bank</button>
					<button class="btn btn-success btn-block btn-xs"><?php echo number_format($tot_tf,2) ?></td>
                    <td class="text-center" width="33%"><button class="btn btn-info btn-block btn-xs"><?php echo number_format($count_cheq) ?> Cheq</button>
					<button class="btn btn-success btn-block btn-xs"><?php echo number_format($tot_cheq,2) ?></td>
                </tr>
				
				
            </table>
			<table class="table table-bordered">
				<tr class="active">
                    <td colspan="3">Informasi Vendor</td>
                </tr>
                <tr >
                    <td class="bg-success text-white text-right" colspan="2"><strong><?php echo number_format($total_distributor) ?> Distributor</strong></td>
                    <td class="bg-success text-white text-right" width="50%"><strong><?php echo number_format($total_vendor) ?> Vendor</strong></td>
                </tr>
			</table>
        </div>
    </div>
    <div class="col-md-4">
        <div class="block">
            <table class="table table-bordered">
                
                <tr class="active">
                    <td colspan="2">Detail Barang</td>
                </tr>
                <tr >
                    <td  class="bg-success text-white text-right">Obat</td>
                    <td width="50%" id="obat">
                        <strong><?php echo number_format($total_detail_barang_obat,2) ?></strong>
                    </td>
                </tr>
                <tr >
                    <td class="bg-success text-white text-right">Alat Kesehatan</td>
                    <td width="50%" id="alkes">
                        <strong><?php echo number_format($total_detail_barang_alkes,2) ?></strong>
                    </td>
                </tr>
                <tr >
                    <td class="bg-success text-white text-right">Implant</td>
                    <td width="50%" id="implant">
                        <strong><?php echo number_format($total_detail_barang_implant,2) ?></strong>
                    </td>
                </tr>
                <tr >
                    <td class="bg-success text-white text-right">Logistik</td>
                    <td width="50%" id="logistik">
                        <strong><?php echo number_format($total_detail_barang_logistik,2) ?></strong>
                    </td>
                </tr>
                <tr >
                    <td class="bg-danger text-white text-right">Retur Uang</td>
                    <td width="50%" id="logistik">
                        <strong><?php echo number_format($total_retur_uang_kbo,2) ?></strong>
                    </td>
                </tr>
				<tr >
                    <td class="bg-primary text-white text-right">Pengajuan</td>
                    <td width="50%" id="logistik">
                        <strong><?php echo number_format($nominal_pengajuan,2) ?></strong>
                    </td>
                </tr>
				<tr >
                    <td colspan="2">
						<button class="btn btn-primary btn-xs btn-block">
                            Rp. <?php echo number_format($nominal_barang,2) ?>
                        </button>      
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
</div>
<? if($st_kbo >=2 && $sisa_belum_trx_kas > 0) {?>
<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h3 class="font-w300 push-15">Informasi</h3>
	<p>Opps, ada <?=$sisa_belum_trx_kas?> Data Transaksi  Yang belum Transaksi Kas <a class="alert-link" href="javascript:void(0)"></a>!</p>
</div>
<?}?>
<? if($st_kbo !='0' && $jml_list > 0) {?>
<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h3 class="font-w300 push-15">Informasi</h3>
	<p>Opps, ada <?=$jml_list?> data verifikasi susulan <a class="alert-link" href="javascript:void(0)">Segera Periksa</a>!</p>
</div>
<?}?>
<? if($jml_minus > 0) {?>
<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h3 class="font-w300 push-15">Warning</h3>
	<p>Opps, ada <?=$jml_minus?> data kontrabon yang total tagihan minus <a class="alert-link" href="javascript:void(0)">Segera Periksa</a>!</p>
</div>
<?}?>
<? if ($st_kbo!='3' || $sisa_belum_trx_kas>0) {?>
<div class="block" id="div_proses">
    <div class="block-header">
		
        <h3 class="block-title">DATA KONTRABON</h3>
		<hr style="margin-top:15px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form"'); ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Jenis Distributor</label>
                    <div class="col-md-8">
                        <select id="tipe_distributor" name="tipe_distributor" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-All-</option>
							<option value="1">Distributor</option>
							<option value="3">Vendor</option>
							
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Vendor / Distributor</label>
                    <div class="col-md-8">
                        <select id="iddistributor" name="iddistributor" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-All-</option>
							
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">No Kontrabon</label>
                    <div class="col-md-8">
						<input class="form-control" placeholder="No Kontrabon" type="text" id="nokontrabon"  value="">
                    </div>
                </div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Cara Bayar</label>
                    <div class="col-md-8">
                        <select id="cara_bayar" name="cara_bayar" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-All-</option>
							<option value="2">Tunai Cash</option>
							<option value="4">Tunai Transfer</option>
							<option value="3">Transfer Beda Bank</option>
							<option value="1">Cheq</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Status</label>
                    <div class="col-md-8">
						<select id="status" name="status" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-All-</option>
							<option value="0">UNPAID</option>
							<option value="1">PAID</option>
                        </select>
                    </div>
                </div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="distributor"></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="button" id="btn_filter_kontrabon" name="btn_filter_kontrabon" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
						
					</div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
    </div>

    <div class="block-content" id="div_tabel">
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th style="width:5%">#</th>
                    <th style="width:5%">No Kontrabon</th>
                    <th style="width:15%">Distributor / Vendor</th>
                    <th style="width:5%">Jml Faktur</th>
                    <th style="width:5%">Last Update</th>
                    <th style="width:5%">Nominal Tagihan</th>
                    <th style="width:5%">Cheq</th>
                    <th style="width:5%">Tunai</th>
                    <th style="width:15%">Transfer</th>
                    <th style="width:10%">Cara Bayar</th>
                    <th style="width:10%">Status</th>
                    <th style="width:10%">
						<?php if ($st_kbo =='4'){?>
						
						<div class="btn-group" role="group"> 
							<button class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false"> Aksi Button
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li class="dropdown-header">Pilih Action</li>
								<li>
									<a tabindex="-1" class="serahkan_semua" href="javascript:void(0)">Serahkan All</a>
								</li>
								
							</ul>
						</div>
						<?}else{?>
							Aksi
						<?}?></th>                    
                    <th style="width:0%">id</th>
                    <th style="width:0%">iddistributor</th>
                    <th style="width:0%">iddistributor</th>
                    
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    </div>


</div>
<?}?>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Ubah Tanggal Kontrabon</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal Kontrabon</label>
							<div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
								<input class="js-datepicker form-control" type="text" id="tgl_edit_kbo" name="tgl_edit_kbo" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 
						
						
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="tko_id" placeholder="No. PO" name="tko_id" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_aktifasi" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:60%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">PROSES AKTIFASI</h3>
                </div>
                <div class="block-content">
					<label><h3>Anda akan melakukan proses aktifasi pastikan data diabawah ini.</h3></label>
					<div class="row">
						<div class="col-md-6">
						
								<table class="table table-bordered">
									<tr class="active">
										<td colspan="3">Informasi Cara Pembayaran</td>
									</tr>
									<tr>
										<td colspan="3" id="cara_pembayaran" class="text-left">
										   <button class="btn btn-primary btn-xs btn-block">
												Rp. <?php echo number_format($total_pembayaran,2) ?>
											</button>                   
										</td>
									</tr>
									
									<tr>
										<td class="text-center" width="33%"><button class="btn btn-info btn-block btn-xs"><?php echo number_format($count_cash) ?> Cash</button>
										<button class="btn btn-success btn-block btn-xs"><?php echo number_format($tot_cash) ?></button></td>
										<td class="text-center" width="33%"><button class="btn btn-info btn-block btn-xs"><?php echo number_format($count_tf) ?> Transfer</button>
										<button class="btn btn-success btn-block btn-xs"><?php echo number_format($tot_tf) ?></td>
										<td class="text-center" width="33%"><button class="btn btn-info btn-block btn-xs"><?php echo number_format($count_cheq) ?> Cheq</button>
										<button class="btn btn-success btn-block btn-xs"><?php echo number_format($tot_cheq) ?></td>
									</tr>
									
									
								</table>
								<table class="table table-bordered">
									<tr class="active">
										<td colspan="3">Informasi Vendor</td>
									</tr>
									<tr >
										<td class="bg-success text-white text-right" colspan="2"><strong><?php echo number_format($total_distributor) ?> Distributor</strong></td>
										<td class="bg-success text-white text-right" width="50%"><strong><?php echo number_format($total_vendor) ?> Vendor</strong></td>
									</tr>
								</table>
						</div>
						<div class="col-md-6">
								<table class="table table-bordered">
									
									<tr class="active">
										<td colspan="2">Detail Barang</td>
									</tr>
									<tr >
										<td  class="bg-success text-white text-right">Obat</td>
										<td width="50%" id="obat">
											<strong><?php echo number_format($total_detail_barang_obat) ?></strong>
										</td>
									</tr>
									<tr >
										<td class="bg-success text-white text-right">Alat Kesehatan</td>
										<td width="50%" id="alkes">
											<strong><?php echo number_format($total_detail_barang_alkes) ?></strong>
										</td>
									</tr>
									<tr >
										<td class="bg-success text-white text-right">Implant</td>
										<td width="50%" id="implant">
											<strong><?php echo number_format($total_detail_barang_implant) ?></strong>
										</td>
									</tr>
									<tr >
										<td class="bg-success text-white text-right">Logistik</td>
										<td width="50%" id="logistik">
											<strong><?php echo number_format($total_detail_barang_logistik) ?></strong>
										</td>
									</tr>
									<tr >
										<td class="bg-danger text-white text-right">Retur Uang</td>
										<td width="50%" id="logistik">
											<strong><?php echo number_format($total_retur_uang_kbo) ?></strong>
										</td>
									</tr>
									<tr >
										<td colspan="2">
											<button class="btn btn-primary btn-xs btn-block">
												Rp. <?php echo number_format($nominal_barang,2) ?>
											</button>      
											
										</td>
									</tr>
								</table>
						</div>
					</div>
					<label><h5>User Aktifasi</h5></label>
					<div class="row">
						<div class="col-md-12">
							<?foreach($list_user as $r){ ?>
							<button class="btn btn-default push-5-r push-10" type="button"><i class="si si-user-following"></i> <?=$r->name?></button>
							<?}?>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="tko_id" placeholder="No. PO" name="tko_id" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success" type="button" name="btn_start_aktifasi" id="btn_start_aktifasi"><i class="fa fa-send"></i> Start Aktifasi</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_serahkan" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Bukti Penyerahan</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal </label>
							<div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
								<input class="js-datepicker form-control" type="text" id="tgl_serahkan" name="tgl_serahkan" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 
						<hr>
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Nama Penerima </label>
							<div class="col-md-8">
							<input type="text" class="form-control" id="penerima" placeholder="Nama Penerima" name="penerima" value="">
							<input type="hidden" class="form-control" id="st_edit" placeholder="" name="st_edit" value="">
							</div>
						</div> 
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="block" id="box_file2">					
								 <table class="table table-bordered" id="list_file">
									<thead>
										<tr>
											<th width="50%" class="text-center">File</th>
											<th width="15%" class="text-center">Size</th>
											<th width="35%" class="text-center">User</th>
											<th width="35%" class="text-center">X</th>
											
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							<div class="block">
								<form action="{base_url}tkontrabon/upload_bukti" enctype="multipart/form-data" class="dropzone" id="image-upload">
									<input type="hidden" class="form-control" name="idkontrabon" id="idkontrabon" value="" readonly>
									<div>
									  <h5>Bukti Penyerahan</h5>
									</div>
								  </form>
							</div>
							
						</div> 
						
						
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="id_penyerahan" placeholder="No. PO" name="id_penyerahan" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_serahkan" id="btn_serahkan"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_serahkan_semua" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
		
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Bukti Penyerahan Semua</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal </label>
							<div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
								<input class="js-datepicker form-control" type="text" id="tgl_serahkan_semua" name="tgl_serahkan_semua" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 
						<hr>
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Nama Penerima </label>
							<div class="col-md-8">
							<input type="text" class="form-control" id="penerima_semua" placeholder="Nama Penerima" name="penerima_semua" value="">
							
							</div>
						</div> 
						<div class="col-md-12" style="margin-top: 10px;">
							
							<div class="block">
								<form action="{base_url}tkontrabon/upload_bukti_semua" enctype="multipart/form-data" class="dropzone" id="dropzone_semua">
									<input type="hidden" class="form-control" id="idkbo_all" placeholder="Nama Penerima" name="idkbo_all" value="<?=$idkbo?>">
									<div>
									  <h5>Bukti Penyerahan</h5>
									</div>
								</form>
							</div>
							
						</div> 
						
						
					</div>
					
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_serahkan_semua" id="btn_serahkan_semua"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
			
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	
		
	var myDropzone 
	var focused = true;

	window.onfocus = function() {
		if (focused==false){
			if ($("#st_kbo").val()!='4'){
				// filter_form();
			}
		}
		focused = true;
		console.log(focused);
	};
	window.onblur = function() {
		focused = false;
		console.log(focused);
	};
	
    $(document).ready(function(){
       var tanggalkontrabon=$("#tanggalkontrabon").val();
       var idkbo=$("#idkbo").val();
	   // alert(tanggalkontrabon);
		if (tanggalkontrabon){
			if (idkbo !='0'){
				loadKbo_verifikasi();
			}else{
				loadKbo();
				
	   // alert(idkbo);
			}
		}else{
			clear_data();
		}
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
		myDropzone2 = new Dropzone("#dropzone_semua", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		
    })
	$(document).on("click","#btn_filter_kontrabon",function(){	
		var tanggalkontrabon=$("#tanggalkontrabon").val();
		var idkbo=$("#idkbo").val();
		if (tanggalkontrabon){
			if (idkbo !='0'){
				loadKbo_verifikasi();
			}else{
				loadKbo();
				
			}
		}else{
			clear_data();
		}
		
	});
	function loadKbo() {
		// alert('sini');
		var tanggalkontrabon=$("#tanggalkontrabon").val();
		var iddistributor=$("#iddistributor").val();
		var nokontrabon=$("#nokontrabon").val();
		var cara_bayar=$("#cara_bayar").val();
		var status=$("#status").val();
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tkontrabon/loadKbo/',
			type: "POST",
			dataType: 'json',
			data: {
				tanggalkontrabon: tanggalkontrabon,
				iddistributor: iddistributor,
				nokontrabon: nokontrabon,
				cara_bayar: cara_bayar,
				status: status,
			}
		},
		columnDefs: [{"targets": [12,13,14], "visible": false },
					 {  className: "text-right", targets:[0,5,6,7,8] },
					 {  className: "text-center", targets:[1,3,4] },
					 { "width": "5%", "targets": [0,1,3] },
					 { "width": "8%", "targets": [5,6,7,8,9,10] },
					 { "width": "10%", "targets": [11] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	function loadKbo_verifikasi() {
		var idkbo=$("#idkbo").val();
		var tipe_distributor=$("#tipe_distributor").val();
		var tanggalkontrabon=$("#tanggalkontrabon").val();
		var iddistributor=$("#iddistributor").val();
		var nokontrabon=$("#nokontrabon").val();
		var cara_bayar=$("#cara_bayar").val();
		var status=$("#status").val();
		var st_kbo=$("#st_kbo").val();
		// alert(tipe_distributor);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tkontrabon/loadKbo_verifikasi/',
			type: "POST",
			dataType: 'json',
			data: {
				tanggalkontrabon: tanggalkontrabon,
				idkbo: idkbo,
				iddistributor: iddistributor,
				nokontrabon: nokontrabon,
				cara_bayar: cara_bayar,
				status: status,
				st_kbo: st_kbo,
				tipe_distributor: tipe_distributor,
			}
		},
		columnDefs: [{"targets": [12,13,14], "visible": false },
					 {  className: "text-right", targets:[0,5,6,7,8] },
					 {  className: "text-center", targets:[1,3,4,11] },
					 { "width": "3%", "targets": [0] },
					 { "width": "5%", "targets": [3,9] },
					 { "width": "8%", "targets": [5,6,7,8,10] },
					 { "width": "10%", "targets": [11] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	function clear_data(){

		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable( {
			"searching": false,
			columnDefs: [{ targets: [12,13], visible: false},
				 {  className: "text-right", targets:[0,5,6,7,8] },
				 {  className: "text-center", targets:[1,2,3,4,9,10,11] },
				 { "width": "3%", "targets": [0] },
				 { "width": "5%", "targets": [1,3,9] },
				 { "width": "8%", "targets": [5,6,7,8] },
				 { "width": "10%", "targets": [10,11] }
			]
		} );
	}
	$(document).on("click",".edit_tanggal",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,12).data();
		$("#tko_id").val(id);
		$("#modal_edit").modal('show');
		$("#btn_ubah").attr('disabled',true);
	
	});
	$(document).on("click","#btn_ubah",function(){	
		var tgl_1=$("#tanggalkontrabon").val();
		var tgl_edit_kbo=$("#tgl_edit_kbo").val();
		if (tgl_edit_kbo==''){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			$("#btn_ubah").attr("disabled",true);
			return false;
		}
		if (tgl_edit_kbo==tgl_1){
			sweetAlert("Maaf...", "Tanggal masih sama!", "error");
			$("#btn_ubah").attr("disabled",true);
			return false;
		}
		var tgl_edit_kbo=$("#tgl_edit_kbo").val();		
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan Tanggal Kontrabon ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal();
		});
	});
	$(document).on("change","#tgl_edit_kbo",function(){	
		var tgl_edit_kbo=$("#tgl_edit_kbo").val();
		cek_tanggal(tgl_edit_kbo);		
	});
	function cek_tanggal($tgl_edit_kbo){
		$.ajax({
			url: '{site_url}tkontrabon/cek_tanggal',
			type: 'POST',
			data: {tgl_edit_kbo:$tgl_edit_kbo},
			success : function(data) {
				if (data=='error'){
					sweetAlert("Maaf...", "Tanggal Sudah ada Kontrabon!", "error");
					$("#btn_ubah").attr("disabled",true);
					return false;
				}else{
					$("#btn_ubah").attr("disabled",false);
					return true;
				}
				
			}
		});
	}
	function edit_tanggal(){
		var id=$("#tko_id").val();		
		var tgl_edit_kbo=$("#tgl_edit_kbo").val();		
		
		$.ajax({
			url: '{site_url}tkontrabon/edit_tanggal',
			type: 'POST',
			data: {id: id,tgl_edit_kbo:tgl_edit_kbo},
			complete: function() {

				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Kontrabon'});
				// location.reload();
				filter_form();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
	}
	function validate_final()
	{
		$("#cover-spin").show();

	}
	$(document).on("change","#tanggalkontrabon",function(){	
		var tgl_kb=$(this).val();
		var tanggal_pilih=$("#tanggal_pilih").val();
		if (tgl_kb != tanggal_pilih){
			$("#div_button").hide();
		}else{
			$("#div_button").show();
		}
		
	});
	function filter_form(){
		$("#btn_reload").val(1);
		document.getElementById("frm1").submit();

	}
	$(document).on("click","#btn_validasi",function(){	
			swal({
				title: "Anda Yakin ?",
				text : "Akan Lanjut ke Proses validasi ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$("#cover-spin").hide();
				start_validasi();
			});
	});
	
	function start_validasi(){
		var idkbo=$("#idkbo").val();
		var st_kbo='2';
		$.ajax({
			url: '{site_url}tkontrabon/start_validasi',
			type: 'POST',
			data: {idkbo: idkbo,st_kbo:st_kbo},
			complete: function() {
				$("#cover-spin").show();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Kontrabon'});
				$("#cover-spin").hide();
				filter_form();
			}
		});
		
	}
	$(document).on("click","#btn_aktivasi",function(){	
		$("#modal_aktifasi").modal('show');
	});
	$(document).on("click","#btn_start_aktifasi",function(){	
		swal({
				title: "Anda Yakin ?",
				text : "Akan Lanjut ke Proses Aktifasi ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				start_aktifasi();
			});
	});
	
	function start_aktifasi(){
		var idkbo=$("#idkbo").val();
		var st_kbo='3';
		$.ajax({
			url: '{site_url}tkontrabon/start_aktifasi',
			type: 'POST',
			data: {idkbo: idkbo,st_kbo:st_kbo},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Start Aktifasi'});
				$("#cover-spin").hide();
				filter_form();
			}
		});
		
	}
	$(document).on("click","#btn_batal_validasi",function(){	
		swal({
				title: "Anda Yakin ?",
				text : "Akan Kembali ke Proses Verifikasi ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				batal_validasi();
			});
	});
	
	function batal_validasi(){
		var idkbo=$("#idkbo").val();
		// alert(idkbo);return false;
		var st_kbo='1';
		$.ajax({
			url: '{site_url}tkontrabon/batal_validasi',
			type: 'POST',
			data: {idkbo: idkbo,st_kbo:st_kbo},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Validasi'});
				$("#cover-spin").hide();
				filter_form();
			}
		});
		
	}
	$(document).on("click","#btn_setuju",function(){	
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Setuju Kontrabon ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				start_setuju();
			});
	});
	
	function start_setuju(){
		var idkbo=$("#idkbo").val();
		var st_kbo='1'
		$.ajax({
			url: '{site_url}tkontrabon/start_setuju',
			type: 'POST',
			data: {idkbo: idkbo,st_kbo:st_kbo},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Kontrabon'});
				$("#cover-spin").hide();
				filter_form();
			}
		});
		
	}
	$(document).on("click",".serahkan",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,12).data();
		$("#st_edit").val('0');
		load_file_serahkan(id);		
	});
	
	$(document).on("click",".serahkan_edit",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,12).data();
		$("#st_edit").val('1');
		load_file_serahkan(id);
	});
	function load_file_serahkan($id){
		var id=$id;
		 // $('#image-upload').removeAllFiles(true)
		 // $('.dz-preview').empty();
		// $('.dz-message').show();
		
		$('#modal_serahkan').modal('show');
		$("#penerima").val('');
				
		$("#id_penyerahan").val(id);
		$("#idkontrabon").val(id);
		$('#list_file tbody').empty();
		$.ajax({
			url: '{site_url}tkontrabon/get_data_serahkan/'+id,
			dataType: "json",
			success: function(data) {
				$("#penerima").val(data.header.nama_penerima);
				$("#tgl_serahkan").val(data.header.tanggal_penyerahan);
				if (data.detail!=''){
					$("#box_file2").attr("hidden",false);
					$("#list_file tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	function refresh_image(){
		var id=$("#idkontrabon").val();
		
		$.ajax({
			url: '{site_url}tkontrabon/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				if (data.detail!=''){
					$('#list_file tbody').empty();
					$("#box_file2").attr("hidden",false);
					$("#list_file tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	$(document).on("click","#btn_serahkan",function(){	
		var tgl_serahkan=$("#tgl_serahkan").val();
		var penerima=$("#penerima").val();
		var id=$("#id_penyerahan").val();
		var st_edit=$("#st_edit").val();
		if (tgl_serahkan==''){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			// $("#btn_serahkan").attr("disabled",true);
			return false;
		}
		if (penerima==''){
			sweetAlert("Maaf...", "Penerima Harus diisi!", "error");
			// $("#btn_ubah").attr("disabled",true);
			return false;
		}
		$('#modal_serahkan').modal('hide');
		$("#cover-spin").show();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tkontrabon/serahkan',
			type: 'POST',
			data: {id: id,tgl_serahkan:tgl_serahkan,penerima:penerima,st_edit:st_edit},
			success : function(data) {	
					$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Penyerahan Kontrabon'});
				table.ajax.reload( null, false );				
			}
		});
		
		// alert(id);
	});
	$(document).on("click","#btn_serahkan_semua",function(){	
		var tgl_serahkan=$("#tgl_serahkan_semua").val();
		var penerima=$("#penerima_semua").val();
		var id=$("#idkbo").val();
		if (tgl_serahkan==''){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			// $("#btn_serahkan").attr("disabled",true);
			return false;
		}
		if (penerima==''){
			sweetAlert("Maaf...", "Penerima Harus diisi!", "error");
			// $("#btn_ubah").attr("disabled",true);
			return false;
		}
		$('#modal_serahkan_semua').modal('hide');
		$("#cover-spin").show();
		var arr_id=[];
		var table = $('#index_list').DataTable();		
		 var data = table.rows().data();
		 var idkbo;
		 data.each(function (value, index) {
			 if (table.cell(index,14).data()=='0'){
				 idkbo=table.cell(index,12).data();
					arr_id.push(idkbo);
			 }
			
			// console.log(id);
			
		 });
		 // alert(arr_id);
		// var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tkontrabon/serahkan_all',
			type: 'POST',
			data: {id: id,tgl_serahkan:tgl_serahkan,penerima:penerima,arr_id:arr_id},
			complete : function(data) {
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Penyerahan Kontrabon'});
				filter_form();			
			}
		});
		
		// alert(id);
	});
	$(document).on("click",".hapus_file",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		$.ajax({
			url: '{site_url}tkontrabon/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
	$(document).on("click",".serahkan_semua",function(){	
		$('#modal_serahkan_semua').modal('show');
	});
	$(document).on("change","#tipe_distributor",function(){	
		refresh_vendor();
	});
	function refresh_vendor(){
		var tipe_distributor=$("#tipe_distributor").val();
		$.ajax({
			url: '{site_url}tkontrabon/refresh_vendor/'+tipe_distributor,
			dataType: "json",
			success: function(data) {
				$("#iddistributor").empty();
				$('#iddistributor').append('<option value="#" selected>- ALL -</option>');
				$('#iddistributor').append(data.detail);
			}
		});
	}

</script>
