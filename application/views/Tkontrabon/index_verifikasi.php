<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="row">

	<div class="block" id="div_proses">
		<div class="block-header">
			
			<h3 class="block-title"><?=$title?></h3>
			<hr style="margin-top:15px">
			
		</div>

		<div class="block-content" id="div_tabel">
			<div class="table-responsive">
			<table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
				<thead>
					<tr>
						<th style="width:5%">#</th>
						<th style="width:5%">Tanggal</th>
						<th style="width:15%">Jumlah Distributor</th>
						<th style="width:5%">Nominal Cheq</th>
						<th style="width:5%">Nominal Tunai</th>
						<th style="width:5%">Nominal Transfer</th>
						<th style="width:5%">Total Pembayaran</th>						
						<th style="width:20%">Action</th>
						
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			</div>
		</div>


	</div>


</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	
    $(document).ready(function(){
       loadKbo_approve();
    })
	
	function loadKbo_approve() {		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tkontrabon/loadKbo_approve/',
			type: "POST",
			dataType: 'json',
			data: {
				
			}
		},
		columnDefs: [
					 {  className: "text-right", targets:[0,2,3,4,5,6] },
					 {  className: "text-center", targets:[7] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [1,2,3,4,5,6] },
					 { "width": "10%", "targets": [7] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	$(document).on("click",".approve_kontrabon",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var tanggalkontrabon=table.cell(tr,1).data();
		var btn_post='1';
		window.location.replace("{site_url}tkontrabon/index/"+tanggalkontrabon);
		// $.ajax({
			// url: '{site_url}tkontrabon/index',
			// type: 'POST',
			// data: {
				// tanggalkontrabon: tanggalkontrabon,
				// btn_post: btn_post,
				
			// },
			// complete: function() {
				// // $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				// // sweetAlert("Berhasil...", "Data Bank disimpan!", "success");
				// // location.reload();
				// // table.ajax.reload( null, false ); 
			// }
		// });	
	});
	
</script>
