<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_kas/save_detail','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No. Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi" placeholder="No. Jurnal" name="notransaksi" value="{notransaksi}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="idvalidasi" placeholder="No. Jurnal" name="idvalidasi" value="{idvalidasi}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                        <input type="hidden" readonly class="form-control" id="id_reff" placeholder="No. Jurnal" name="id_reff" value="{id_reff}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=HumanDateShort($tanggal_transaksi)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Mulai</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=HumanDateShort($tanggal_mulai)?>">
                        
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Nominal</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($nominal)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Keterangan</label>
                    <div class="col-md-8">
                       <input type="text"  readonly class="form-control" id="distributor"  name="distributor" value="{keterangan}">
                    </div>
                </div>
                
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Pembayaran Asuransi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{status_langsung}">
                    </div>
                </div>
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Bagi Hasil</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{nama_bagi_hasil}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Bagian RS</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="<?=number_format($bagian_rs,2)?> %">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Bagian Pemilik Saham</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="<?=number_format($bagian_ps,2)?> %">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Posting</label>
                    <div class="col-md-8">
                       <h2 class="block-title"><?=($st_posting=='1'?text_primary('SUDAH DIPOSTING'):text_default('MENUNGGU DIPOSTING'))?></h2>
                    </div>
                </div>
               
			</div>
            
        </div>
	</div>
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_default('Informasi Pembayaran')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			<?
				$q="SELECT *FROM tbagi_hasil H WHERE H.id IN (".$list_tbagi_hasil_id.")";
				$no=1;
				$row=$this->db->query($q)->result();
			?>
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_hasil" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-right">X</th>
								<th style="width:10%"  class="text-center">NO TRANSAKSI</th>
								<th style="width:20%"  class="text-center">DESKRIPSI</th>
								<th style="width:10%"  class="text-center">TANGGAL</th>
								<th style="width:10%"  class="text-right">TOTAL</th>
								<th style="width:10%"  class="text-right">BIAYA</th>
								<th style="width:10%"  class="text-right">NOMINAL RUMAH SAKIT</th>
								<th style="width:15%"  class="text-right">NOMINAL PEMILIK SAHAM</th>
							</tr>
						</thead>
						<tbody>
							<?foreach ($row as $r){?>
								<tr>
								<td class="text-right"><?=$no?></td>
								<td class="text-center"><?=$r->notransaksi?></td>
								<td class="text-center"><?=$r->nama_bagi_hasil?></td>
								<td class="text-center"><?=HumanDateShort($r->tanggal_tagihan)?></td>
								<td class="text-right"><?=number_format($r->total_pendapatan,2)?></td>
								<td class="text-right"><?=number_format($r->total_biaya,2)?></td>
								<td class="text-right"><?=number_format($r->pendapatan_bersih_rs,2)?></td>
								<td class="text-right"><strong><?=number_format($r->pendapatan_bersih_ps,2)?></strong></td>
								</tr>
							<?
							$no=$no+1;
							}?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_default('Pemilik Saham')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_ps" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:10%"  class="text-center">NAMA PEMILIK SAHAM</th>								
								<th style="width:8%"  class="text-center">TIPE</th>								
								<th style="width:3%"  class="text-center">JML LEMBAR</th>								
								<th style="width:8%"  class="text-center">NOMINAL</th>								
								<th style="width:10%"  class="text-center">REKENING</th>
								<th style="width:8%"  class="text-center">METODE</th>
								<th style="width:8%"  class="text-center">TANGGAL BAYAR</th>
								<th style="width:20%"  class="text-center">AKUN</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('Detail Pembayaran')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_bayar" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">JENIS KAS</th>								
								<th style="width:8%"  class="text-center">SUMBER KAS</th>								
								<th style="width:8%"  class="text-center">BANK</th>								
								<th style="width:9%"  class="text-center">METODE</th>								
								<th style="width:10%"  class="text-center">NOMINAL</th>
								<th style="width:20%"  class="text-center">AKUN</th>								
								<th style="width:5%"  class="text-center">AKSI</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_danger('Ringkasan Akun')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">NO AKUN</th>								
								<th style="width:30%"  class="text-center">AKUN</th>								
								<th style="width:20%"  class="text-center">DEBIT</th>								
								<th style="width:20%"  class="text-center">KREDIT</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_kas" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
						<button class="btn btn-success" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Update & Posting</button>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update Akun</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   load_bayar();
   load_ps();
   load_ringkasan();
});
function get_transaksi_detail($id){
	var id=$id;
	$("#modal_list_bayar").modal('show');
	get_pembayaran(id);
}
$(document).on("click", ".list_bayar", function() {
	
});

function load_bayar(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	var id_reff=$("#id_reff").val();
	// alert(idvalidasi);
	$('#index_bayar tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_bayar/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_bayar').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function load_ps(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	// alert(idvalidasi);
	$('#index_ps tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_ps/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_ps').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function load_ringkasan(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	var idvalidasi=$("#idvalidasi").val();
	var id_reff=$("#id_reff").val();
	$('#index_ringkasan tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_ringkasan').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}

function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

</script>