<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_kas/save_detail','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No. Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi" placeholder="No. Jurnal" name="notransaksi" value="{notransaksi}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="idvalidasi" placeholder="No. Jurnal" name="idvalidasi" value="{idvalidasi}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                        <input type="hidden" readonly class="form-control" id="id_reff" placeholder="No. Jurnal" name="id_reff" value="{id_reff}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Unit</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{nama_unit}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Unit Pengaju</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control " id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($nama_pengaju)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">TIPE </label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($tipe_nama)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Kegiatan</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control " id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($nama_kegiatan)?>">
                        
                    </div>
                </div>
               
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Klasifikasi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($klasifikasi)?>">
                        
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=HumanDateShort($tanggal_transaksi)?>">
                        
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Vendor</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{nama_vendor}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Jenis Pengajuan </label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($nama_jenis)?>">
                        
                    </div>
                </div>
				
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Nominal</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($nominal)?>">
                        
                    </div>
                </div>
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Catatan</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($catatan)?>">
                        
                    </div>
                </div>
				
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Posting</label>
                    <div class="col-md-8">
                       <h2 class="block-title"><?=($st_posting=='1'?text_primary('SUDAH DIPOSTING'):text_default('MENUNGGU DIPOSTING'))?></h2>
                    </div>
                </div>
               
			</div>
            
        </div>
	</div>
	
	<div class="block-content">
		   <hr style="margin-top:10px">
		<div class="row">
			<div class="col-md-12">
            <div class="progress progress-mini">
			<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<h5 style="margin-bottom: 10px;">DETAIL PENGAJUAN</h5>
			
				<table id="tabel_add" class="table table-striped table-bordered" style="margin-bottom: 0;">
					<thead>
						<tr>
							<th style="width: 15%;">Nama Barang</th>
							<th style="width: 15%;">Merk Barang</th>
							<th style="width: 5%;">QTY</th>
							<th style="width: 5%;">Satuan</th>
							<th style="width: 10%;">Harga</th>
							<th style="width: 10%;">Total</th>
							<th style="width: 15%;">Keterangan</th>
							<th style="width: 8%;">Actions</th>
						</tr>
						<tr hidden>							
							<td><input class="form-control input-sm" tabindex="2" type="text" id="nama_barang" /></td>
							<td><input class="form-control input-sm" tabindex="3" type="text" id="merk_barang" /></td>
							<td><input class="form-control input-sm number" tabindex="4" type="text" id="kuantitas" /></td>
							<td><input class="form-control input-sm" tabindex="5" type="text" id="satuan" /></td>
							<td><input class="form-control input-sm number" tabindex="6" type="text" id="harga_satuan" /></td>
							<td><input class="form-control input-sm number" readonly  type="text" id="total_harga" /></td>
							<td><input class="form-control input-sm" tabindex="7" type="text" id="keterangan" /></td>
							<td>
								<button type="button"  disabled  class="btn btn-sm btn-primary kunci" tabindex="8" id="addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
								<button type='button'  disabled  class='btn btn-sm btn-danger kunci' tabindex="9" id="canceljual" title="Refresh"><i class='fa fa-times'></i></button>
							</td>
						</tr>
					</thead>
					<tbody>
						<? 
						// $list_detail=array();
						$gt=0;
						$item=0;
						foreach($list_detail as $row){ 
							$gt=$gt + $row->total_harga;
							$item=$item + 1;
						?>
							<tr>
								<td><?=$row->nama_barang?></td>
								<td><?=$row->merk_barang?></td>
								<td><?=$row->kuantitas?></td>
								<td><?=$row->satuan?></td>
								<td><?=number_format($row->harga_satuan).'<br>└─ Hpp. '.number_format($row->harga_pokok).'<br>└─ Disc. '.number_format($row->harga_diskon).'<br>└─ PPN. '.number_format($row->harga_ppn)?></td>
								<td><?=number_format($row->total_harga)?></td>
								<td><?=$row->keterangan?></td>
								<td><? echo "<div class='btn-group'><button type='button' disabled class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' disabled class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div>"?></td>
								<td style='display:none'><input type='text' name='xiddet[]' value="<?=$row->id?>"></td>
								<td style='display:none'><input type='text' name='xnama_barang[]' value="<?=$row->nama_barang?>"></td>
								<td style='display:none'><input type='text' name='xmerk_barang[]' value="<?=$row->merk_barang?>"></td>
								<td style='display:none'><input type='text' name='xkuantitas[]' value="<?=$row->kuantitas?>"></td>
								<td style='display:none'><input type='text' name='xsatuan[]' value="<?=$row->satuan?>"></td>
								<td style='display:none'><input type='text' name='xharga_satuan[]' value="<?=$row->harga_satuan?>"></td>
								<td style='display:none'><input type='text' name='xtotal_harga[]' value="<?=$row->total_harga?>"></td>
								<td style='display:none'><input type='text' name='xketerangan[]' value="<?=$row->keterangan?>"></td>
								<td style='display:none'><input type='text' name='xstatus[]' value="<?=$row->status?>"></td>
							</tr>
						<?}?>
					</tbody>
					<tfoot id="foot-total-nonracikan">
						<tr>
							<th colspan="5" class="hidden-phone">

								<span class="pull-right"><b>TOTAL</b></span></th>
							<input type="hidden" id="rowindex" name="rowindex" value="">
							<input type="hidden" id="iddet" name="iddet" value="">
							<th><input class="form-control input-sm number" readonly type="text" id="grand_total" name="grand_total" value="<?=$gt?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_item" name="total_item" value="<?=$item?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_jenis_barang" name="total_jenis_barang" value="<?=$item?>"/></th>
							
						</tr>
					</tfoot>
				</table>
				
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
		</div>
	</div>
	<div class="block-content" id="detail_akun_barang">
		<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:3%"  class="text-center">NO</th>
					<th style="width:10%"  class="text-center">BARANG</th>
					<th style="width:10%"  class="text-center">MERK</th>
					<th style="width:5%"  class="text-center">HARGA BELI</th>
					<th style="width:15%"  class="text-center">NO AKUN BELI</th>
					<th  style="width:5%" class="text-center">PPN</th>
					<th style="width:15%" class="text-center">NO AKUN PPN</th>
					<th  style="width:5%" class="text-center">DISKON</th>
					<th  style="width:15%" class="text-center">NO AKUN DISKON</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<?
		$q="SELECT H.*,A.noakun,A.namaakun from tvalidasi_kas_06_pengajuan_piutang H
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			WHERE H.idvalidasi='$idvalidasi'";
		$list_piutang=$this->db->query($q)->result();
	?>
	<?if ($list_piutang){?>
		<div class="block-content">
			<h3 class="block-title"> <?=text_success('Detail Piutang / Penampung')?></h3>
			   <hr style="margin-top:10px">
			<div class="row">
				
				<div class="col-md-12">
						<table class="table table-bordered table-striped fixed" id="index_penampung_piutang" style="width: 100%;">
							<thead>
								<tr>
									<th style="width:5%"  class="text-center">NO </th>
									<th style="width:75%"  class="text-center">NO AKUN</th>								
									<th style="width:20%"  class="text-center">NOMINAL</th>								
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
				</div>
			</div>
		</div>
	<?}?>
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('Detail Pengembalian')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_pengembalian" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">JENIS KAS</th>								
								<th style="width:8%"  class="text-center">SUMBER KAS</th>								
								<th style="width:8%"  class="text-center">BANK</th>								
								<th style="width:9%"  class="text-center">METODE</th>								
								<th style="width:10%"  class="text-center">NOMINAL</th>
								<th style="width:20%"  class="text-center">AKUN</th>								
								<th style="width:5%"  class="text-center">AKSI</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('Detail Pembayaran')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_bayar" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">JENIS KAS</th>								
								<th style="width:8%"  class="text-center">SUMBER KAS</th>								
								<th style="width:8%"  class="text-center">BANK</th>								
								<th style="width:9%"  class="text-center">METODE</th>								
								<th style="width:10%"  class="text-center">NOMINAL</th>
								<th style="width:20%"  class="text-center">AKUN</th>								
								<th style="width:5%"  class="text-center">AKSI</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_danger('Ringkasan Akun')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">NO AKUN</th>								
								<th style="width:30%"  class="text-center">AKUN</th>								
								<th style="width:20%"  class="text-center">DEBIT</th>								
								<th style="width:20%"  class="text-center">KREDIT</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_kas" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
						<button class="btn btn-success" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Update & Posting</button>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update Akun</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   load_piutang_pengajuan();
   load_bayar();
   load_ringkasan();
   load_detail();
   load_pengembalian();
});
function load_detail(){
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	$('#index_list tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_detail_pengajuan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_list').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function get_transaksi_detail($id){
	var id=$id;
	$("#modal_list_bayar").modal('show');
	get_pembayaran(id);
}
function load_bayar(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	var id_reff=$("#id_reff").val();
	// alert(idvalidasi);
	$('#index_bayar tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_bayar/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_bayar').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function load_pengembalian(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	var id_reff=$("#id_reff").val();
	// alert(idvalidasi);
	$('#index_pengembalian tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_pengembalian/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_pengembalian').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function load_piutang_pengajuan(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	var id_reff=$("#id_reff").val();
	// alert(idvalidasi);
	$('#index_penampung_piutang tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_piutang_pengajuan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_penampung_piutang').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
$(document).on("click", ".list_bayar", function() {
	
});


function load_ringkasan(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	var idvalidasi=$("#idvalidasi").val();
	var id_reff=$("#id_reff").val();
	$('#index_ringkasan tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_ringkasan').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}

function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

</script>