<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_kas/save_detail','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Penerimaan</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="nojurnal" placeholder="No. Jurnal" name="nojurnal" value="{notransaksi}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
						<input type="hidden" readonly class="form-control" id="idvalidasi" placeholder="No. Jurnal" name="idvalidasi" value="{idvalidasi}">
						<input type="hidden" readonly class="form-control" id="id_reff" placeholder="No. Jurnal" name="id_reff" value="{id_reff}">
                    </div>
                </div>
                
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Distributor</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{distributor}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Cara Bayar</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tipe_bayar" placeholder="No. Faktur" name="tipe_bayar" value="<?=($tipe_bayar=='1'?'TUNAI':'KREDIT')?>">
                    </div>
                </div>
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">No Faktur</label>
                    <div class="col-md-8">
						<input type="text"  readonly class="form-control" id="nofakturexternal" placeholder="No. Faktur" name="nofakturexternal" value="<?=$nofakturexternal?>">
					</div>
                </div>
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Penerimaan</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_terima" placeholder="No. Faktur" name="tanggal_terima" value="<?=HumanDateShort($tanggal_terima)?>">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=HumanDateShort($tanggal_transaksi)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Posting</label>
                    <div class="col-md-8">
                       <h2 class="block-title"><?=($st_posting=='1'?text_primary('SUDAH DIPOSTING'):text_default('MENUNGGU DIPOSTING'))?></h2>
                    </div>
                </div>
               
			</div>
            
        </div>
	</div>
	<div class="block-content">
		<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:3%"  class="text-center">NO</th>
					<th style="width:5%"  class="text-center">TIPE</th>
					<th style="width:12%"  class="text-center">BARANG</th>
					<th style="width:8%"  class="text-center">HARGA BELI</th>
					<th style="width:10%"  class="text-center">NO AKUN BELI</th>
					<th  style="width:8%" class="text-center">PPN</th>
					<th style="width:10%" class="text-center">NO AKUN PPN</th>
					<th  style="width:8%" class="text-center">DISKON</th>
					<th  style="width:10%" class="text-center">NO AKUN DISKON</th>
					<th  style="width:5%" class="text-center">AKSI</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('Detail Pembayaran')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_bayar" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">JENIS KAS</th>								
								<th style="width:8%"  class="text-center">SUMBER KAS</th>								
								<th style="width:8%"  class="text-center">BANK</th>								
								<th style="width:9%"  class="text-center">METODE</th>								
								<th style="width:10%"  class="text-center">NOMINAL</th>
								<th style="width:20%"  class="text-center">AKUN</th>								
								<th style="width:5%"  class="text-center">AKSI</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		 <h3 class="block-title"><?=text_danger('Ringkasan')?></h3>
		 <hr style="margin-top:10px">
		<table class="table table-bordered table-striped fixed" id="ringkasan_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:10%"  class="text-center">NO</th>
					<th style="width:20%"  class="text-center">NO AKUN</th>
					<th style="width:25%"  class="text-center">NAMA AKUN</th>
					<th style="width:15%"  class="text-center">DEBIT</th>
					<th style="width:15%"  class="text-center">KREDIT</th>
					
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_kas" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
						<button class="btn btn-success" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Update & Posting</button>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update Akun</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='title_barang'>Verifikasi Penerimaan BARANG</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="rowIndex">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Barang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" readonly id="xnamabarang">
                                    </div>
									
                                </div>
                                <div id="div_harga" class="form-group div_harga" >
                                    <label class="col-md-3 control-label">Harga</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga">
										<div id="val_harga" class="help-block animated fadeInDown"></div>
                                        										
                                    </div>
									
                                </div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Diskon %.</label>
									<div class="col-md-9">
										<div class="input-group">
											<input class="form-control diskon" type="text" readonly id="xdiskon_persen"  placeholder="Disc %">
											<span class="input-group-addon">Diskon Rp</span>
											<input class="form-control decimal" type="text" readonly id="xdiskon_rp"  placeholder="Disc Rp">
										</div>
									</div>
								</div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Harga Before PPN</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga_after_diskon">
										
                                    </div>
									
                                </div>
								<div class="form-group">
									<label class="col-md-3 control-label">PPN %.</label>
									<div class="col-md-9">
										<div class="input-group">
											<input type="text" readonly class="form-control diskon" id="xppn">
											<span class="input-group-addon">PPN Rp</span>
											<input type="text" class="form-control decimal" readonly id="xppn_nominal">
										</div>
									</div>
								</div>
								
								
								<div class="form-group div_harga">
                                    <label class="col-md-3 control-label">Harga Final Rp.</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga_after_ppn">										
                                    </div>									
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kuantitas</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control number" readonly id="xkuantitas">										
                                        <input type="hidden" readonly class="form-control number" id="xkuantitas_sisa">										
                                    </div>		
									<label class="col-md-2 control-label">Total Rp.</label>
                                    <div class="col-md-4">
                                        <input type="text" readonly class="form-control decimal" id="xharga_total_fix">
                                    </div>
                                </div>
                                
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tgl Kadaluarsa</label>
									<div class="col-md-4">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="js-datepicker form-control" readonly type="text" id="tanggalkadaluarsa" name="tanggalkadaluarsa" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">No Batch</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" readonly id="nobatch">
                                    </div>
                                </div>
                            </div>
                            
                                
                        </div>
                    </form>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
                </div>
            </div>
        </div>
    </div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$("#cover-spin").show();
	load_detail();
	load_bayar();
	load_ringkasan();
   
});
function load_bayar(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	var id_reff=$("#id_reff").val();
	// alert(idvalidasi);
	$('#index_bayar tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_bayar/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_bayar').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function get_transaksi_detail($id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tvalidasi_kas/get_transaksi_detail/'+$id,
		dataType: "json",
		success: function(data) {
			console.log(data);
			$('#xnamabarang').val(data.nama_barang);
			$('#xharga').val(data.harga_asli);
			$('#xdiskon_persen').val(data.diskon);
			$('#xdiskon_rp').val(data.nominaldiskon);
			$('#xharga_after_diskon').val(data.harga_after_diskon);
			$('#xppn').val(data.ppn);
			$('#xppn_nominal').val(data.nominalppn);
			$('#xharga_after_ppn').val(data.harga_after_ppn);
			$('#xharga_total_fix').val(data.harga_after_ppn);
			
			$('#xkuantitas').val(data.kuantitas);
			$('#tanggalkadaluarsa').val(data.tanggalkadaluarsa);
			$('#nobatch').val(data.nobatch);
			$("#modal_edit").modal('show');
			$("#cover-spin").hide();
		}
	});
}

function load_detail(){
	var id=$("#id").val();
	var idvalidasi=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	$('#index_list tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_detail_gudang/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_list').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function load_ringkasan(){
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	var idvalidasi=$("#idvalidasi").val();
	$('#ringkasan_list tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi},
		success: function(data) {
			$('#ringkasan_list').append(data.tabel);
			
		}
	});
}

</script>