<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_kas/save_detail','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tanggal</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi" placeholder="No. Jurnal" name="notransaksi" value="<?=HumanDateShort($tanggal_transaksi)?>">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="idvalidasi" placeholder="No. Jurnal" name="idvalidasi" value="{idvalidasi}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                        <input type="hidden" readonly class="form-control" id="id_reff" placeholder="No. Jurnal" name="id_reff" value="{id_reff}">
                    </div>
                </div>
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Jenis Kas</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($jenis_nama)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Saldo Awal <?=$sumber_nama?></label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($saldo_awal)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Keterangan</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="User" name="distributor" value="<?=strip_tags($keterangan)?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Akun <?=$posisi_akun1?></label>
                    <div class="col-md-8">
                        <select id="idakun1" name="idakun1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Pilih Akun -</option>
							<?foreach($list_akun as $row){?>
								<option value="<?=$row->id?>" <?=($row->id==$idakun1?'selected':'')?>><?=$row->noakun.' - '.$row->namaakun?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				
                
                
            </div>
			<div class="col-md-6">
				
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama User</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="User" name="distributor" value="{nama_user}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Sumber Kas</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{sumber_nama}">
                    </div>
                </div>
				
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Saldo Akhir <?=$sumber_nama?></label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($saldo_akhir)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Posting</label>
                    <div class="col-md-8">
                       <h2 class="block-title"><?=($st_posting=='1'?text_primary('SUDAH DIPOSTING'):text_default('MENUNGGU DIPOSTING'))?></h2>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Akun <?=$posisi_akun2?></label>
                    <div class="col-md-8">
                        <select id="idakun2" name="idakun2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Pilih Akun -</option>
							<?foreach($list_akun as $row){?>
								<option value="<?=$row->id?>" <?=($row->id==$idakun2?'selected':'')?>><?=$row->noakun.' - '.$row->namaakun?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				
               
			</div>
            
			
        </div>
	</div>
	
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_danger('Ringkasan Akun')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">NO AKUN</th>								
								<th style="width:30%"  class="text-center">AKUN</th>								
								<th style="width:20%"  class="text-center">DEBIT</th>								
								<th style="width:20%"  class="text-center">KREDIT</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_kas" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
						<button class="btn btn-success" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Update & Posting</button>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update Akun</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   load_ringkasan();
});
function get_transaksi_detail($id){
	var id=$id;
	$("#modal_list_bayar").modal('show');
	get_pembayaran(id);
}
$(document).on("click", ".list_bayar", function() {
	
});


function load_ringkasan(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	var idvalidasi=$("#idvalidasi").val();
	var id_reff=$("#id_reff").val();
	$('#index_ringkasan tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_kas/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idvalidasi: idvalidasi,id_reff: id_reff},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_ringkasan').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}

function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

</script>