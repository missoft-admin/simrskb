<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nomor Tagihan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{notransaksi}" disabled>
                            <input class="form-control" type="hidden" name="id" id="id" value="{id}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                            <input class="form-control" type="hidden" name="disabel_edit" id="disabel_edit" value="{disabel_edit}" >
                            <input class="form-control" type="hidden" name="mbagi_hasil_id" id="mbagi_hasil_id" value="{mbagi_hasil_id}" >
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;"> 
                        <label class="col-md-3 control-label">Nama Rekanan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{nama_bagi_hasil}" disabled>
                        </div> 
                    </div>
                                      
                </div>
				<div class="col-md-6" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Deskripsi</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="<?=($deskripsi)?>" disabled>
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tanggal Tagihan</label> 
                        <div class="col-md-4"> 
                            <input class="form-control" value="<?=HumanDateShort($tanggal_tagihan)?>" disabled>
                        </div> 
                    </div>
                    
                    
                    
                </div>
            <?php echo form_close(); ?>
        </div>
        
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>

<div class="block">
    <div class="block-header">
        <h3 class="block-title"><h3><span class="label label-success">Data Transaksi Tindakan</span></h3></h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Layanan</label>
                    <div class="col-md-8">
                        <select name="jenis_transaksi_id" id="jenis_transaksi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">- Pilih -</option>
							<option value="1">Rawat Jalan</option>
							<option value="2">Rawat Inap</option>
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
                
            </div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Tarif</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="namatarif" placeholder="Nama Tarif" name="namatarif" value="">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Tindakan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx" name="tgl_trx" placeholder="From" value=""/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value=""/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="index_list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NO</th>
                    <th>NO. REG</th>
                    <th>LAYANAN</th>
                    <th>TANGGAL TINDAKAN</th>
                    <th>PASIEN</th>
                    <th>NAMA TARIF</th>
                    <th>QTY</th>
                    <th>NOMINAL</th>
                    <th>KEL. PASIEN	</th>
                    <th>TANGGAL TAGIHAN</th>
                    <th>KET</th>
                </tr>
            </thead>
            <tbody> </tbody>
              
        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title"><h3><span class="label label-danger">Biaya Operasional & Rekapitulasi</span></h3></h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-12"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-2 control-label">Biaya Operasional</label> 
                        <div class="col-md-10"> 
							<div class="table-responsive">
							<table class="table table-bordered table-striped table-responsive" id="index_biaya">
								<thead>
									<tr>
										<th hidden>Actions</th>
										<th style="width: 3%;">#</th>
										<th style="width: 3%;">#</th>
										<th style="width: 12%;">Tanggal Pembelian</th>
										<th style="width: 25%;">Biaya</th>
										<th style="width: 12%;">Nominal</th>								
										<th style="width: 12%;">Keterangan</th>								
										<th style="width: 13%;">Actions</th>
									</tr>
									<tr>
										<td hidden></td>
										<td></th>
										<td></th>
										<td>
											<div class="input-group date" style=" width: 100%;">
											<input class="js-datepicker form-control input"  type="text" id="tanggal_beli" name="tanggal_beli" value="" data-date-format="dd-mm-yyyy"/>
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
											</div>										
										</td>
										<td>
											<div class="input-group"  style=" width: 100%;">
												<select id="idbiaya" name="idbiaya"   class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one.."  width="100%">
													<option value="#" selected>- Pilih Biaya -</option>
													
												</select>
												<span class="input-group-btn"  style=" width: 20%;">
													<button class="btn btn-success" <?=$disabel?>  type="button" onclick="refresh_biaya()" title="Refresh"><i class="fa fa-refresh"></i></button>
													<a href="{base_url}mbiaya_operasional/create" <?=$disabel?> target="_blank" class="btn btn-primary"   type="button" title="Tambah Biaya"><i class="fa fa-plus"></i></a>
												</span>
											</div>											
										</td>
										<td>
											<input type="text" class="form-control number" id="nominal" placeholder="nominal" name="nominal" value=""  style=" width: 100%;">							
										</td>
																
										<td>									
											<input type="text" class="form-control" id="keterangan" placeholder="keterangan" name="keterangan" value=""  style=" width: 100%;">
											<input type="hidden" class="form-control" id="id_edit" placeholder="id_edit" name="id_edit" value="">
										</td>
										
										<td>									
											<button type="button" class="btn btn-sm btn-primary" tabindex="8" <?=$disabel?> id="btn_simpan_tanggal" onclick="simpan_biaya()" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
											<button type="button" class="btn btn-sm btn-success" tabindex="8" <?=$disabel?> id="btn_update_tanggal" onclick="simpan_biaya()" title="Update"><i class="fa fa-save"></i> Update</button>
											<button type="button" class="btn btn-sm btn-warning" tabindex="8" <?=$disabel?> onclick="clear_all()" title="Batal"><i class="fa fa-refresh"></i></button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>		
								  
							</table>
						   
                        </div> 
                        </div> 
                    </div>                          
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-2 control-label">Total Pendapatan Rp.</label> 
                        <div class="col-md-4"> 
                            <input class="form-control number" readonly id="total_pendapatan" name="total_pendapatan" value="">
                        </div> 

                    </div>
                </div>
				
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="tanggal"><span class="label label-danger">Pendapatan Kotor</span></label>
						<div class="col-md-8">
							<div class="input-group">
								
							</div>
						</div>
					</div>
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="tanggal">Bagian Rumah Sakit Rp.</label>
						<div class="col-md-8">
							<div class="input-group">
								<input class="form-control number" readonly type="text" id="nominal_rs" name="nominal_rs" placeholder="Rp" value="">
								<span class="input-group-addon"><i class="fa fa-percent"></i></span>
								<input class="form-control decimal" <?=$disabel?> type="text" id="bagian_rs_persen" name="bagian_rs_persen" placeholder="%" value="" style="width:50%;">
							</div>
						</div>
					</div>
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group">
						<label class="col-md-2 control-label" for="tanggal">Bagian Pemilik Saham Rp.</label>
						<div class="col-md-8">
							<div class="input-group">
								<input class="form-control number" readonly type="text" id="nominal_ps" name="nominal_ps" placeholder="Rp" value="">
								<span class="input-group-addon"><i class="fa fa-percent"></i></span>
								<input class="form-control decimal" type="text" <?=$disabel?> id="bagian_ps_persen" name="bagian_ps_persen" placeholder="%" value="" style="width:50%;">
							</div>
						</div>
					</div>
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-2 control-label">Biaya Operasional Rp.</label> 
                        <div class="col-md-4"> 
                            <input class="form-control number" type="text" readonly id="total_biaya" name="total_biaya" value="">
                            <input class="form-control number" type="hidden" readonly id="total_biaya_saved" name="total_biaya_saved" value="">
                        </div> 
						<label class="col-md-2 control-label">Tanggungan Biaya Operasional</label> 
                        <div class="col-md-4"> 
                            <select name="tanggungan_biaya" <?=$disabel?> id="tanggungan_biaya" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="1">Rumah Sakit</option>
								<option value="2">Pemilik Saham</option>
								<option value="3">Ditanggung Bersama Sama Rata</option>
								<option value="4">Ditanggung Bersama Sesuai Persentase</option>
							</select>
                        </div> 
                    </div>
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
                        
                    </div>
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" for="tanggal"><span class="label label-success">Pendapatan Bersih</span></label>
					<div class="col-md-8">
						<div class="input-group">
							
						</div>
					</div>
				</div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-2 control-label">Rumah Sakit Rp.</label> 
                        <div class="col-md-4"> 
                            <input class="form-control number" type="text" readonly id="pendapatan_bersih_rs" name="pendapatan_bersih_rs" value="">
                        </div> 
						<label class="col-md-2 control-label">Pemilik Saham Rp.</label> 
                        <div class="col-md-4"> 
                            <input class="form-control number" type="text" readonly id="pendapatan_bersih_ps" name="pendapatan_bersih_ps" value="">
                        </div> 
                    </div>					
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-2 control-label">Pendapatan Per Lembar Rp.</label> 
                        <div class="col-md-4"> 
                            <input class="form-control number" type="text" readonly id="pendapatan_per_lembar" name="pendapatan_per_lembar" value="">
                            <input class="form-control" type="hidden" id="pendapatan_per_lembar_saved" name="pendapatan_per_lembar_saved" value="">
                        </div> 
						<label class="col-md-2 control-label"></label> 
                        <div class="col-md-4" hidden> 
                            <input class="form-control number" type="text" readonly id="pembagian_bersih" name="pembagian_bersih" value="">
                            <input class="form-control number" type="text" readonly id="jumlah_lembar" name="jumlah_lembar" value="">
                            
                        </div> 
                    </div>					
                </div>
				<div class="col-md-12" style="margin-top: 15px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
						<label class="col-md-2 control-label"></label> 
						<?if ($disabel==''){?>
                        <div class="col-md-4"> 
							<button type="button" class="btn btn-sm btn-success" tabindex="8" id="simpan_rekap_" onclick="simpan_rekap()" title="Simpan"><i class="fa fa-save"></i> Simpan</button>
							<button type="button" class="btn btn-sm btn-default" tabindex="8" onclick="load_info()" title="Batal"><i class="fa fa-refresh"></i> Batal</button>
							
						</div>	
						<?}?>
                    </div>				
                </div>
				<div class="col-md-12" style="margin-top: 15px;"> 
					<div class="form-group" style="margin-bottom: 5px;"> 
						<label class="col-md-2 control-label"></label> 
                        <div class="col-md-10"> 
							
							<div id="msg_simpan" hidden>
								<div class="alert alert-danger alert-dismissable">
									<h3 class="font-w300 push-15">Warning</h3>
									<p>Data Belum disimpan!</p>
								</div>
							</div>
						</div>	
									
                    </div>				
                </div>
				
				
            <?php echo form_close(); ?>
        </div>
        
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Edit Tanggal Perhitungan </h3>
                </div>
                <div class="block-content">
					<div class="row">
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal Perhitungan</label>
								<div class="col-md-8">
									<select id="xtgl_pilih" name="xtgl_pilih" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" readonly class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


    var table;

    $(document).ready(function(){
		load_biaya();
		refresh_biaya();
		clear_all();
		load_detail();
		load_info();
		
		$('.number').number(true, 0, '.', ',');
		$('.decimal').number(true, 2, '.', ',');
    });
	function edit_tanggal($id){
		$("#xid").val($id);
		load_list_tanggal();
		$("#modal_edit").modal('show');
		
	}
	function load_list_tanggal(){
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		if (mbagi_hasil_id){
			$.ajax({
				url: '{site_url}tbagi_hasil/list_tanggal_by_tangggal_id/'+mbagi_hasil_id,
				dataType: "json",
				success: function(data) {
					$("#xtgl_pilih").empty();
					$('#xtgl_pilih').append(data.detail);
				}
			});
		}
	}
	function load_info(){
		var id=$("#id").val();
		$.ajax({
			url: '{site_url}tbagi_hasil/get_info/',
			dataType: "JSON",
			method: "POST",
			data : {id:id},
			success: function(data) {
				console.log(data.detail);
				$("#total_pendapatan").val(data.total_pendapatan);
				$("#total_biaya_saved").val(data.total_biaya);
				$("#total_biaya").val(data.total_biaya_detail);
				$("#bagian_rs_persen").val(data.bagian_rs_persen);
				$("#bagian_ps_persen").val(data.bagian_ps_persen);
				$("#nominal_rs").val(data.nominal_rs);
				$("#nominal_ps").val(data.nominal_ps);
				$("#jumlah_lembar").val(data.jumlah_lembar);
				$("#pendapatan_per_lembar_saved").val(data.pendapatan_per_lembar);
				$("#tanggungan_biaya").val(data.tanggungan_biaya).trigger('change');
				hitung_bagian_kotor();
				// hitung_bagian_bersih();
			}
		});
	}
	function simpan_rekap(){
		var tbagi_hasil_id=$("#id").val();
		var bagian_rs_persen=$("#bagian_rs_persen").val();
		var bagian_ps_persen=$("#bagian_ps_persen").val();
		var nominal_ps=$("#nominal_ps").val();
		var nominal_rs=$("#nominal_rs").val();
		var total_biaya=$("#total_biaya").val();
		var tanggungan_biaya=$("#tanggungan_biaya").val();
		var pembagian_bersih=$("#pembagian_bersih").val();
		var pendapatan_bersih_rs=$("#pendapatan_bersih_rs").val();
		var pendapatan_bersih_ps=$("#pendapatan_bersih_ps").val();
		var jumlah_lembar=$("#jumlah_lembar").val();
		var pendapatan_per_lembar=$("#pendapatan_per_lembar").val();
		// var table = $('#index_biaya').DataTable();
		 swal({
			title: "Anda Yakin ?",
			text : "Untuk Simpan Rekapitulasi?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {	
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tbagi_hasil/simpan_rekap',
				type: 'POST',
				data: {
					tbagi_hasil_id: tbagi_hasil_id,bagian_ps_persen: bagian_ps_persen,nominal_ps: nominal_ps,nominal_rs: nominal_rs,total_biaya: total_biaya,bagian_rs_persen: bagian_rs_persen
					,tanggungan_biaya: tanggungan_biaya
					,pembagian_bersih: pembagian_bersih
					,pendapatan_bersih_rs: pendapatan_bersih_rs
					,pendapatan_bersih_ps: pendapatan_bersih_ps
					,jumlah_lembar: jumlah_lembar
					,pendapatan_per_lembar: pendapatan_per_lembar				
				},
				complete: function() {
					$("#cover-spin").hide();
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
					sweetAlert("Berhsil...", "Menyimpan Data!", "success");
					load_info();
				}
			});
		});
		
		
	}
	$("#bagian_ps_persen").keyup(function(){
		var bagian_ps_persen=$("#bagian_ps_persen").val();
		var sisa=100 - parseFloat(bagian_ps_persen);		
		$("#bagian_rs_persen").val(sisa);
		hitung_bagian_kotor();
	});
	
	$("#bagian_rs_persen").keyup(function(){
		var bagian_rs_persen=$("#bagian_rs_persen").val();
		var sisa=100 - parseFloat(bagian_rs_persen);		
		$("#bagian_ps_persen").val(sisa);
		hitung_bagian_kotor();
	});
	function hitung_bagian_kotor(){
		var pendapatan=$("#total_pendapatan").val();
		var persen_rs=$("#bagian_rs_persen").val();
		var persen_ps=$("#bagian_ps_persen").val();
		var bagian_rs=parseFloat(pendapatan) * parseFloat(persen_rs)/100;
		var bagian_ps=parseFloat(pendapatan) * parseFloat(persen_ps)/100;
		$("#nominal_rs").val(bagian_rs);
		$("#nominal_ps").val(bagian_ps);
		hitung_bagian_bersih();
	}
	function hitung_bagian_bersih(){
		var pendapatan=$("#total_pendapatan").val();
		var nominal_rs=$("#nominal_rs").val();
		var nominal_ps=$("#nominal_ps").val();
		var total_biaya=$("#total_biaya").val();
		var bagian_rs_persen=$("#bagian_rs_persen").val();
		var bagian_ps_persen=$("#bagian_ps_persen").val();
		var biaya_ps=0;
		var biaya_rs=0;
		
		var bagian_rs=0;
		var bagian_ps=0;
		var pembagian_bersih=pendapatan -  total_biaya;
		if ($('#tanggungan_biaya').val()=='1'){//Tanggungan RS
			bagian_rs=nominal_rs - total_biaya;
			bagian_ps=nominal_ps;
		}
		if ($('#tanggungan_biaya').val()=='2'){//Tanggungan PS
			bagian_rs=nominal_rs;
			bagian_ps=nominal_ps - total_biaya;
		}
		if ($('#tanggungan_biaya').val()=='3'){//Tanggungan PS
			total_biaya=total_biaya/2;
			bagian_rs=nominal_rs - total_biaya;
			bagian_ps=nominal_ps - total_biaya;
		}
		if ($('#tanggungan_biaya').val()=='4'){//Bersama Persentase
			// total_biaya=total_biaya/2;
			biaya_rs=bagian_rs_persen * total_biaya /100;
			biaya_ps=bagian_ps_persen * total_biaya /100;
			// console.log('TOTAL BIAYA :' + biaya_rs);
			console.log('Biaya RS :' + biaya_rs);
			console.log('Biaya PS :' + biaya_ps);
			bagian_rs=nominal_rs - biaya_rs;
			bagian_ps=nominal_ps - biaya_ps;
		}
		$("#pendapatan_bersih_rs").val(bagian_rs);
		$("#pendapatan_bersih_ps").val(bagian_ps);
		$("#pembagian_bersih").val(pembagian_bersih);
		var pendapatan_per_lembar;
		pendapatan_per_lembar=bagian_ps/$("#jumlah_lembar").val();
		$("#pendapatan_per_lembar").val(pendapatan_per_lembar);
		// alert(parseFloat($("#pendapatan_per_lembar_saved").val()) + '-' + parseFloat($("#pendapatan_per_lembar").val()));
		if (parseFloat($("#pendapatan_per_lembar").val())!=parseFloat($("#pendapatan_per_lembar_saved").val())){
			$('#msg_simpan').show();
		}else{
			$('#msg_simpan').hide();
		}
	}
	function clear_all(){
		$('#idbiaya').val('#').trigger('change');
		$('#keterangan').val('');
		$('#nominal').val('');
		$('#tanggal_beli').val('');
		$('#id_edit').val('');
		
		$("#btn_simpan_tanggal").show()
		$("#btn_update_tanggal").hide()
		$("#btn_batal_tanggal").hide()
	}
	function simpan_biaya(){
		var tbagi_hasil_id=$("#id").val();
		var id_edit=$("#id_edit").val();
		var tanggal_beli=$("#tanggal_beli").val();
		var idbiaya=$("#idbiaya").val();
		var nominal=$("#nominal").val();
		var keterangan=$("#keterangan").val();
		var table = $('#index_biaya').DataTable();
		if (!validate_detail()) return false;
		$.ajax({
			url: '{site_url}tbagi_hasil/simpan_biaya',
			type: 'POST',
			data: {tbagi_hasil_id: tbagi_hasil_id,tanggal_beli: tanggal_beli,idbiaya: idbiaya,nominal: nominal,keterangan: keterangan,id_edit: id_edit},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				table.ajax.reload( null, false );
				clear_all();
				load_info();
			}
		});
		
	}
	
	function validate_detail() {
		if ($("#idbiaya").val() == "#") {
			sweetAlert("Maaf...", "Biaya Harus Diisi!", "error");
			return false;
		}
		if ($("#tanggal_beli").val() == "") {
			sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			return false;
		}
		if ($("#nominal").val() == "" || $("#nominal").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			return false;
		}	
		
		return true;
	}
	function refresh_biaya(){
		$.ajax({
			url: '{site_url}tbagi_hasil/refresh_biaya/',
			dataType: "json",
			success: function(data) {
				$("#idbiaya").empty();
				$('#idbiaya').append('<option value="#">- Pilih Biaya -</option>');
				$('#idbiaya').append(data.detail);
			}
		});
	}
    function load_detail() {
		var id=$("#id").val();
		var no_reg=$("#no_reg").val();
		var disabel_edit=$("#disabel_edit").val();
		var jenis_transaksi_id=$("#jenis_transaksi_id").val();
		var no_medrec=$("#no_medrec").val();
		var nama_pasien=$("#nama_pasien").val();
		var tgl_trx=$("#tgl_trx").val();
		var tgl_trx2=$("#tgl_trx2").val();
		var namatarif=$("#namatarif").val();
		var disabel=$("#disabel").val();
		// alert(id);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tbagi_hasil/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,no_reg: no_reg,jenis_transaksi_id: jenis_transaksi_id,namatarif: namatarif,
				no_medrec: no_medrec,nama_pasien: nama_pasien,
				tgl_trx:tgl_trx,tgl_trx2:tgl_trx2,disabel:disabel,
			}
		},
		columnDefs: [
					{"targets": [0], "visible": false },
					 {  className: "text-right", targets:[1,7,8] },
					 {  className: "text-center", targets:[2,3,4,9,10] },
					 // {  className: "text-left", targets:[6] },
					 { "width": "3%", "targets": [1] },
					 { "width": "5%", "targets": [3,7] },
					 { "width": "8%", "targets": [2,4,8,9,10] },
					 { "width": "15%", "targets": [5,6] },
					 // { "width": "10%", "targets": [12,6] },
					 // { "width": "15%", "targets": [8] }

					]
		});
	}
	function load_biaya() {
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		
		$('#index_biaya').DataTable().destroy();
		var table = $('#index_biaya').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": false,
		"order": [],
		"ajax": {
			url: '{site_url}tbagi_hasil/load_biaya/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,disabel: disabel
			}
		},
		columnDefs: [
					{"targets": [0,1], "visible": false },
					 {  className: "text-right", targets:[5] },
					 // {  className: "text-center", targets:[2,3,4,8,9] },
					 // // {  className: "text-left", targets:[6] },
					 { "width": "3%", "targets": [2] },
					 { "width": "20%", "targets": [3] },
					 { "width": "8%", "targets": [7] },
					 { "width": "10%", "targets": [5,6] },
					 // { "width": "15%", "targets": [5,6] },
					 // // { "width": "10%", "targets": [12,6] },
					 // // { "width": "15%", "targets": [8] }

					]
		});
	}
	function hapus_biaya($id) {
		var id=$id;
		 swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus data?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			var table = $('#index_biaya').DataTable();
			$.ajax({
				url: '{site_url}tbagi_hasil/hapus_biaya',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					table.ajax.reload( null, false );
					clear_all();
					load_info();
					// $("#cover-spin").hide();
					// filter_form();
				}
			});
		});
	}
	$(document).on("click",".edit_biaya",function(){	
		var table = $('#index_biaya').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var id_edit=table.cell(tr,0).data();
		var idbiaya=table.cell(tr,1).data();
		var tanggal_beli=table.cell(tr,3).data();
		var nominal=table.cell(tr,5).data();
		var keterangan=table.cell(tr,6).data();
		// alert(nominal);
		$('#id_edit').val(id_edit);
		// alert(id_pemilik_saham);
		$('#idbiaya').val(idbiaya).trigger('change');
		$('#tanggal_beli').val(tanggal_beli);
		$('#keterangan').val(keterangan);
		$('#nominal').val(nominal);
		$("#btn_simpan_pemilik_saham").hide()
		$("#btn_update_pemilik_saham").show()
		$("#btn_batal_pemilik_saham").show()
	});
	$(document).on("click","#btn_ubah",function(){	
		var id=$("#xid").val();
		var tanggal=$("#xtgl_pilih").val();
		// alert(tanggal);return false;
		var table = $('#index_list').DataTable();
		swal({
			title: "Anda Yakin ?",
			text : "Untuk Pindah Tanggal?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tbagi_hasil/pindah_tanggal',
				type: 'POST',
				data: {id: id,tanggal: tanggal},
				complete: function() {
					$("#modal_edit").modal('hide');
					sweetAlert("Berhsil...", "Pindah Tanggal!", "success");
					table.ajax.reload( null, false );
					load_info();
					
				}
			});
		});
	});
	$("#idbiaya").change(function(){
		if ($("#id_edit").val()==''){
			$.ajax({
				url: '{site_url}tbagi_hasil/get_estimasi/',
				dataType: "JSON",
				method: "POST",
				data : {idbiaya:$("#idbiaya").val()},
				success: function(data) {
					console.log(data);
					$("#nominal").val(data.detail);
					
				}
			});
			
		}
	});
	$("#tanggungan_biaya").change(function(){
		hitung_bagian_bersih();
	});
	
	$("#btn_filter").click(function() {
		load_detail();
	});
	
	
</script>