<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="notransaksi" placeholder="No Registrasi" name="notransaksi" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Bagi Hasil</label>
                    <div class="col-md-8">
                        <select id="mbagi_hasil_id" name="mbagi_hasil_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua -</option>
							<?foreach($list_mbagi_hasil as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Deskripsi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="deskripsi" placeholder="Deskripsi" name="deskripsi" value="">
                    </div>
                </div>
				       
            </div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="1">Sedang Diproses</option>
							<option value="2">Telah Diproses</option>
							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Perhitungan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_tagihan1" name="tanggal_tagihan1" placeholder="From" value="{tanggal_tagihan1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_tagihan2" name="tanggal_tagihan2" placeholder="To" value="{tanggal_tagihan2}"/>
                        </div>
                    </div>
                </div>
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">NO REGISTER</th>
                    <th width="10%">BAGI HASIL NAMA</th>
                    <th width="10%">DESKRIPSI</th>
                    <th width="10%">TANGGAL PERHITUNGAN</th>
                    <th width="10%">NOMINAL</th>
                    <th width="10%">STATUS</th>                   
                    <th width="15%" class="text-center">AKSI</th>
                    
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var notransaksi=$("#notransaksi").val();
	var deskripsi=$("#deskripsi").val();
	var status=$("#status").val();
	var mbagi_hasil_id=$("#mbagi_hasil_id").val();
	var tanggal_tagihan1=$("#tanggal_tagihan1").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
							{ "width": "3%", "targets": [1] },
							{ "width": "8%", "targets": [2,5,6,7] },
							{ "width": "10%", "targets": [4] },
							{ "width": "15%", "targets": [3,8] },
						 {"targets": [8,3,4], className: "text-left" },
						 {"targets": [1,6], className: "text-right" },
						 {"targets": [2,5,7], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tbagi_hasil/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						notransaksi:notransaksi,deskripsi:deskripsi,status:status,
						mbagi_hasil_id:mbagi_hasil_id,tanggal_tagihan1:tanggal_tagihan1,
						tanggal_tagihan2:tanggal_tagihan2
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});
function stop($id,$val){
	swal({
		title: "Anda Yakin ?",
		text : "Untuk Stop Transaksi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tbagi_hasil/update_start_stop',
			type: 'POST',
			data: {id: $id,val:$val},
			complete: function(data) {
				// console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Stop'});
				table.ajax.reload( null, false ); 
			}
		});
		// update_start_stop($id);
	});
}
function start($id,$val){
	swal({
		title: "Anda Yakin ?",
		text : "Untuk Start Transaksi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tbagi_hasil/update_start_stop',
			type: 'POST',
			data: {id: $id,val:$val},
			complete: function(data) {
				// console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Start'});
				table.ajax.reload( null, false ); 
			}
		});
		// update_start_stop($id);
	});
}
</script>