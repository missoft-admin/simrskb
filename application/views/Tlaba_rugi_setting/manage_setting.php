<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tlaba_rugi_setting" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('tlaba_rugi_setting/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3" style="margin-bottom:5px">
				<label class="col-md-2 control-label" for="nama">Nama Layout / Template</label>
				<div class="col-md-8">
					<input type="hidden" class="form-control" id="disabel" name="disabel" value="{disabel}">
					<input type="hidden" class="form-control" id="id" name="id" value="{id}">
					<input type="hidden" class="form-control" id="template_id" name="template_id" value="{id}">
					<input type="text" <?=$disabel?> class="form-control" disabled id="nama" placeholder="Nama Layout / Template" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group" style="margin-bottom:5px">
				<label class="col-md-2 control-label" for="nama">Tahun</label>
				<div class="col-md-2">
					<select name="tahun" id="tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="#" selected>-Pilih Tahun-</option>
						<?			
						$awal_tahun=date('Y');
						for($x=2021;$x<($awal_tahun+5);$x++){
						?>
						<option value="<?=$x?>" <?=($x==date('Y')?'selected':'')?>><?=$x?></option>
						
						<?}?>
					</select>	
				</div>
			</div>
			<div class="form-group" style="margin-bottom:15px">
				<label class="col-md-2 control-label" for="nama">Periode</label>
				<div class="col-md-8">
					<select name="periode[]" id="periode" class="js-select2 form-control" style="width: 100%;" multiple></select>
				</div>
			</div>
			
			
			<?if ($disabel==''){?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-2">
					<button class="btn btn-success" type="button" id="btn_filter" style="width:100%"><i class="fa fa-filter"></i> Tampilkan</button>	
					
				</div>
				<div class="col-md-2" class="text-right">
					<button class="wizard-next btn btn-danger" style="width:100%" type="button" id="btn_add_periode"><i class="fa fa-plus"></i> Create Periode</button>					
				</div>
				
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>

</div>


<div class="block">
	<div class="block-header bg-primary">
		<ul class="block-options">
       
    </ul>
		<h3 class="block-title">Detail Template</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive" id="div_tabel">
										
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>
<div class="modal fade in black-overlay" id="modal_add_periode" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Tambah Periode</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
								
								
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Tahun</label>
									<div class="col-md-9">
										<select name="xtahun" id="xtahun" class="js-select2 form-control" style="width: 100%;">
											<?			
											$awal_tahun=date('Y');
											for($x=2021;$x<($awal_tahun+5);$x++){
											?>
											<option value="<?=$x?>" <?=($x==$awal_tahun?'selected':'')?>><?=$x?></option>
											<?}?>
										</select>									
									</div>
								</div>								
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Periode</label>
									<div class="col-md-9">
										<select name="xperiode[]" id="xperiode" class="js-select2 form-control" style="width: 100%;" multiple></select>
									</div>
								</div>
								
							</div>							
						</form>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-success" type="button" id="btn_simpan_periode"><i class="fa fa-plus"></i> Create</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_copy_field" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Penerapan Formula</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
															
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tipe_adm">Decision Bulan Berikutnya</label>
									<div class="col-md-8">
										<select name="decision_copy" id="decision_copy" class="js-select2 form-control" style="width: 100%;">
											<option value="1">Terapkan angka yang sama</option>
											<option value="2">Sesuaikan kenaikan dengan nominal</option>
											<option value="3">Sesuaikan kenaikan dengan persentase</option>
											<option value="4">Hapus Anggaran</option>
										</select>
										<input class="form-control input-sm" readonly id="section_akun_id_copy" type="hidden" placeholder="0" value="0"> 
										<input class="form-control input-sm" readonly id="periode_awal_copy" type="hidden" placeholder="0" value="0"> 
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;" id="div_nominal_asal">
									<label class="col-md-4 control-label" for="tipe_adm">Nominal Periode ini</label>
									<div class="col-md-8">
										<input class="form-control decimal input-sm" readonly id="nominal_copy" type="text" placeholder="0" value="0"> 
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;" id="div_nominal">
									<label class="col-md-4 control-label" for="tipe_adm">Nominal Kenaikan</label>
									<div class="col-md-8">
										<input class="form-control decimal input-sm" id="nominal_kenaikan_copy" type="text" placeholder="0" value="0"> 
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;" id="div_persentase">
									<label class="col-md-4 control-label" for="tipe_adm">Persentase Kenaikan</label>
									<div class="col-md-8">
										<div class="input-group">
											<input class="form-control decimal input-sm" id="persentase_kenaikan_copy" type="text" placeholder="0" value="0"> 
											<span class="input-group-addon"><i class="fa fa-percent"></i></span>
										</div>
										
									</div>
								</div>
								
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tipe_adm">Penerapan Ke-Periode</label>
									<div class="col-md-8">
										<select name="periode_copy[]" id="periode_copy" class="js-select2 form-control" style="width: 100%;" multiple></select>
									</div>
								</div>
								
							</div>							
						</form>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-success" type="button" id="btn_copy_periode"><i class="fa fa-save"></i> Terapkan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}pages/dataTables.fixedColumns.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var rTipe='';
var nAwal=0;
var nAkhir=0;
var table;
	window.onfocus = function() {
			$(".decimal").number(true,0,'.',',');
	};
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,0,'.',',');
		load_periode();
		// var str="Hallow Aord";
		// console.log(str.replaceAll('a',''));
		
	})	
	$(document).on("click","#btn_filter",function(){		
		load_anggaran();
	});
	function clear_table(){
		$tabel='<table class="table table-striped" id="index_list">';
		$tabel +='<thead></thead><tbody></tbody></table>';
		$("#div_tabel").html($tabel);
	}
	function load_anggaran() {
		
		var template_id=$("#template_id").val();
		var periode=$("#periode").val();
		var data = [];
		data=$('#periode').select2('data');
		clear_table();
		if (periode){
		// alert(unit);
		var content='';
		content +='<tr>';
		content +='<th></th>';
		content +='<th></th>';
		content +='<th class="text-center" style="background-color:#f5f5f5">Daftar Akun</th>';
		$('#index_list thead').empty();
		for (i = 0; i < data.length; i++) {
		  // console.log(data[i].text+' '+data[i].id)
		 
		  content +='<th class="text-right">'+data[i].text+'&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" onclick="copy_ke_periode_all('+data[i].id+')" title="Terapkan Ke Periode Selanjutnya" type="button"><i class="fa fa-paper-plane"></i></button></th>';
		}
		content +='</tr>';
		$('#index_list thead').append(content);
		$('#index_list tbody').empty();
		$('#index_list').DataTable().destroy();
		// $('#index_list').DataTable().clear().draw();
		var table = $('#index_list').removeAttr('width').DataTable({
			 scrollY:        "450px",
			scrollX:        true,
			scrollCollapse: true,
			paging:         false,
			serverSide: true,
			"processing": true,
			"ordering": false,
			fixedColumns:   {
				left: 3,
				// right: 1
			},
			"ajax": {
				url: '{site_url}tlaba_rugi_setting/load_anggaran/',
				type: "POST",
				dataType: 'json',
				data: {
					periode: periode,template_id:template_id
				}
			},
			// fixedColumns:   {
				// left: 4,
				// // right: 1
			// },
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData[0]=='0' && aData[1]=='1'){
					$('td', nRow).css('background-color', '#e9e9e9');					
				}
				if (aData[0]=='1'  && aData[1]=='1'){
					$('td', nRow).css('background-color', '#d1e2ef');					
					$(".decimal").number(true,0,'.',',');
				}
				if (aData[1]==''){
					 $(nRow).find('td:eq(0)').css('background-color', '#f5f5f5');
				}
				
				console.log(iDisplayIndex);
			},
			
			columnDefs: [
				{ "width": "4%", "targets": [0,1],  className: "text-left","visible":false },
				{ "width": "450px", "targets": [2]},
				// { "width": "200px", "targets": [kolom]},
				// { "width": "100%", "targets": 2,  className: "text-left" },
			]
			});
		}
		
	}
	function show_hide_decision(){
		if ($("#section_akun_id_copy").val()!='all'){
			$("#div_nominal_asal").show();			
		}
		$("#div_nominal").hide();
		$("#div_persentase").hide();
		if ($("#decision_copy").val()=='2'){
			$("#div_nominal").show();
		}
		if ($("#decision_copy").val()=='3'){
			$("#div_persentase").show();
		}
	}
	
	$(document).on("change","#decision_copy",function(){		
		show_hide_decision();
	});
	function copy_ke_periode($periode,$id){
		$("#periode_awal_copy").val($periode);
		$("#section_akun_id_copy").val($id);
		$("#cover-spin").show();
		$('#periode_copy').empty();
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/date_akun_copy',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val(),
				periode: $periode,
				section_akun_id: $id,
				},
			success: function(data) {
				$("#cover-spin").hide();
				// alert(data.formula);
				$('#periode_copy').append(data.periode);
				$("#nominal_copy").val(data.nominal);
				$("#modal_copy_field").modal('show');
			}
		});
		show_hide_decision()
	}
	function copy_ke_periode_all($periode){
		show_hide_decision()
		$("#periode_awal_copy").val($periode);
		$("#section_akun_id_copy").val('all');
		$("#div_nominal_asal").hide();
		$("#cover-spin").show();
		$('#periode_copy').empty();
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/date_akun_copy_all',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val(),
				periode: $periode,
				},
			success: function(data) {
				$("#cover-spin").hide();
				// alert(data.formula);
				$('#periode_copy').append(data.periode);
				$("#nominal_copy").val(data.nominal);
				$("#modal_copy_field").modal('show');
			}
		});
	}
	$(window).focus(function() {
	  $(".decimal").number(true,0,'.',',');
	});
	function focusFunction(input){
		$(".decimal").number(true,0,'.',',');
		nAwal=removeComa(input.value);
		
	}
	function blurFunction(input,$periode,$id){
		var tmp=(input.value);
		nAkhir=removeComa(tmp);
		if (nAwal !=nAkhir){
			// console.log('Nominal Awal : '+nAwal+' Akhir :'+nAkhir+' Akun :'+$id);		
			var template_id=$("#template_id").val();	
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tlaba_rugi_setting/update_nominal',
				type: 'POST',
				data: {
					periode: $periode,template_id: template_id,nominal:nAkhir,section_akun_id:$id
					},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
					$("#cover-spin").hide();
				}
			});
		}
		
	}
	function removeComa($num){
		// alert(($num.replaceAll(',','')));
		return parseFloat(($num.replaceAll(',','')))
	}
	
	
	
	$(document).on("click","#btn_clear",function(){		
		clear_table();
	});
	
	$(document).on("click","#btn_add_periode",function(){		
		$("#modal_add_periode").modal('show');
		refresh_periode();
	});
	$(document).on("click","#btn_simpan_periode",function(){			
		simpan_periode();
	});
	function simpan_periode(){
		var periode=$("#xperiode").val();
		var template_id=$("#template_id").val();
		if (periode){
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tlaba_rugi_setting/simpan_periode',
				type: 'POST',
				data: {
					periode: periode,template_id: template_id
					},
				complete: function() {
					$("#modal_add_periode").modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
					$("#cover-spin").hide();
					$("#tahun").val("#").trigger('change');
				}
			});
			
		}else{
			sweetAlert("Gagal...", "Pilih Periode!", "error");
		}
	}
	$(document).on("click","#btn_list_variable",function(){			
		$("#modal_list_variable").modal('show');
	});
	$(document).on("change","#xtahun",function(){		
		refresh_periode();
	});
	$(document).on("change","#tahun",function(){		
		clear_table();
		load_periode();
	});
	$(document).on("click","#btn_copy_periode",function(){		
		save_copy_periode();
	});
	function save_copy_periode(){
		if ($("#decision_copy").val()=='2' && ($("#nominal_kenaikan_copy").val()=='0' || $("#nominal_kenaikan_copy").val()=='')){
			sweetAlert("Maaf...", "Tentukan Nominal kenaikan", "error");
			return false;
		}
		if ($("#decision_copy").val()=='3' && ($("#persentase_kenaikan_copy").val()=='0' || $("#persentase_kenaikan_copy").val()=='')){
			sweetAlert("Maaf...", "Tentukan Persentase kenaikan", "error");
			return false;
		}
		if ($("#periode_copy").val()==null){
			sweetAlert("Maaf...", "Tentukan Periode", "error");
			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menentukan Nominal Untuk Periode Selanjutnya ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			var template_id=$("#template_id").val();	
			$("#cover-spin").show();
			var table = $('#index_list').DataTable();
			$.ajax({
				url: '{site_url}tlaba_rugi_setting/update_copy_periode',
				type: 'POST',
				data: {
					 periode: $("#periode_copy").val()
					,template_id: template_id
					,section_akun_id:$("#section_akun_id_copy").val()
					,nominal:$("#nominal_copy").val()
					,nominal_kenaikan:$("#nominal_kenaikan_copy").val()
					,persentase_kenaikan:$("#persentase_kenaikan_copy").val()
					,decision:$("#decision_copy").val()
					,periode_awal:$("#periode_awal_copy").val()
					},
				complete: function() {
					$("#modal_copy_field").modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
					table.ajax.reload( null, false );
					$("#cover-spin").hide();
				}
			});
		});

		
			
	}
	function refresh_periode(){
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/refresh_periode/'+$("#xtahun").val(),
			dataType: "json",
			success: function(data) {
				$("#xperiode").empty();
				$('#xperiode').append(data.detail);
			}
		});
	}
	function load_periode(){
		$("#periode").empty();
		if ($("#tahun").val() !='#'){
			$.ajax({
				url: '{site_url}tlaba_rugi_setting/load_periode/'+$("#tahun").val()+'/'+$("#template_id").val(),
				dataType: "json",
				success: function(data) {					
					$('#periode').append(data.detail);
					// load_anggaran();
				}
			});
		}
		
	}
	
	
	//BATAS HAPUS
	
</script>