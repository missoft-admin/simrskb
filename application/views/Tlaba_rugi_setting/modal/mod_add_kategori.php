<div class="modal fade in black-overlay" id="modal_add_kategori" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Tambah Kategori</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Nama Kategori</label>
									<div class="col-md-9">
										<input type="hidden" class="form-control" id="id_edit_kategori" value="">	
										<input type="hidden" class="form-control" id="header_id_kategori" value="">	
										<input type="text" autocomplete="off" class="form-control" id="nama_kategori" placeholder="Nama Kategori" name="nama_kategori" value="">									
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Urutan</label>
									<div class="col-md-9">
										<input type="text" class="form-control number" id="urutan_kategori"  name="urutan_kategori" placeholder="Urutan" value="">									
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Summary</label>
									<div class="col-md-9">
										<select name="st_jumlah_kategori" style="width: 100%" id="st_jumlah_kategori" class="js-select2 form-control input-sm">										
											<option value="1" selected> YA </option>
											<option value="0"> TIDAK </option>										
										</select>
									</div>
								</div>															
							</div>							
						</form>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
				<button class="btn btn-sm btn-success" type="button" id="btn_simpan_kategori">Simpan</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).on('click', '#btn_simpan_kategori', function() {	
		if (validate_simpan_kategori()==false){return false}
		var id_edit_kategori=$("#id_edit_kategori").val();
		var header_id=$("#header_id_kategori").val();
		var template_id=$("#template_id").val();
		var nama=$("#nama_kategori").val();
		var urutan=$("#urutan_kategori").val();
		var st_jumlah=$("#st_jumlah_kategori").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/save_kategori',
			type: 'POST',
			data: {
				id_edit_kategori: id_edit_kategori,template_id: template_id,nama: nama,urutan: urutan,st_jumlah: st_jumlah,header_id: header_id
				},
			complete: function() {
				$("#modal_add_kategori").modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				
				table.ajax.reload( null, false );
			}
		});
		
	});
	function add_kategori($id){		
		clear_kategori();
		$('#header_id_kategori').val($id);
		$("#modal_add_kategori").modal('show');
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/max_kategori',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val(),header_id:$("#header_id_kategori").val()
				},
			success: function(data) {
				// alert(data.nama);
				$("#urutan_kategori").val(data.urutan)
				$("#nama_kategori").focus();
			}
		});
	}
	
	function clear_kategori(){
		$("#header_id_kategori").val('');
		$("#id_edit_kategori").val('');
		$("#nama_kategori").val('');
		$("#urutan_kategori").val('');
	}
	function validate_simpan_kategori(){
		if ($("#nama_kategori").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Kategori", "error");
			return false;
		}
		if ($("#urutan_kategori").val()==''){
			sweetAlert("Maaf...", "Tentukan Urutan Kategori", "error");
			return false;
		}
		
		return true;
	}
	function edit_kategori($id,$header_id){
		$("#id_edit_kategori").val($id);
		$("#header_id_kategori").val($header_id);
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/get_info_section',
			type: 'POST',
			dataType: 'json',
			data: {
				id: $id
				},
			success: function(data) {
				// alert(data.nama);
				$("#nama_kategori").val(data.nama)
				$("#urutan_kategori").val(data.urutan)
				$("#st_jumlah_kategori").val(data.st_jumlah).trigger('change');
				$("#modal_add_kategori").modal('show');
			}
		});
	}
	function hapus_kategori($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Kategori ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			var table = $('#index_list').DataTable();
			$.ajax({
				url: '{site_url}tlaba_rugi_setting/hapus_section',
				type: 'POST',
				dataType: 'json',
				data: {
					id: $id
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});						
					table.ajax.reload( null, false );
				}
			});
		});

		
	}
</script>
