<div class="modal fade in black-overlay" id="modal_add_perhitungan" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Tambah Perhitungan</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Urutan</label>
									<div class="col-md-9">
										<input type="text" class="form-control number" id="urutan_perhitungan"  name="urutan_perhitungan" placeholder="Urutan" value="">									
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Nama Perhitungan</label>
									<div class="col-md-9">
										<input type="hidden" class="form-control" id="lev_perhitungan" value="">	
										<input type="hidden" class="form-control" id="id_edit_perhitungan" value="">	
										<input type="hidden" class="form-control" id="header_id_perhitungan" value="">	
										<input type="text" autocomplete="off" class="form-control" id="nama_perhitungan" placeholder="Nama Perhitungan" name="nama_perhitungan" value="">									
									</div>
								</div>								
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Formula</label>
									<div class="col-md-9">
										<textarea class="form-control" id="formula" name="formula" rows="3" placeholder="Formula"></textarea>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm"></label>
									<div class="col-md-9">
										<button class="btn btn-xs btn-default push-5-r push-10" type="button" id="btn_list_variable"><i class="si si-bulb"></i> Variable</button>
										<button class="btn btn-xs btn-default push-5-r push-10" type="button" id="btn_check"><i class="si si-magic-wand"></i> View Formula</button>
									</div>
								</div>	
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">View Formula</label>
									<div class="col-md-9">
										<textarea class="form-control" id="xformula" name="xformula" rows="5" placeholder="View Formula" disabled> </textarea>
									</div>
								</div>
							</div>							
						</form>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
				<button class="btn btn-sm btn-success" type="button" id="btn_simpan_perhitungan">Simpan</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_list_variable" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List Variable</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="table-responsive">
										<table class="table table-condensed" id="index_list_variable" style="width: 100%;">
											<thead>
												<tr>
													<th hidden>Actions</th>
													<th hidden>Actions</th>
													<th style="width: 10%;"></th>
													<th style="width: 10%;">Varibale</th>
												</tr>							
											</thead>
											<tbody></tbody>							
										</table>										
									</div>
								</div>																						
							</div>							
						</form>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-primary" type="button" id="btn_refresh_var"><i class="fa fa-refresh"></i> Refresh</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).on('click', '#btn_simpan_perhitungan', function() {	
		if (validate_simpan_perhitungan()==false){return false}
		var id_edit_perhitungan=$("#id_edit_perhitungan").val();
		var header_id=$("#header_id_perhitungan").val();
		var template_id=$("#template_id").val();
		var nama=$("#nama_perhitungan").val();
		var urutan=$("#urutan_perhitungan").val();
		var formula=$("#formula").val();
		var lev=$("#lev_perhitungan").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/save_perhitungan',
			type: 'POST',
			data: {
				id_edit_perhitungan: id_edit_perhitungan,template_id: template_id,nama: nama,urutan: urutan,header_id: header_id,lev: lev,formula: formula
				},
			complete: function() {
				$("#modal_add_perhitungan").modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				
				table.ajax.reload( null, false );
			}
		});
		
	});
	$(document).on('click', '#btn_refresh_var', function() {	
		list_variable();
	});
	function list_variable(){
		var template_id=$("#template_id").val();
		var disabel=$("#disabel").val();
		
		$('#index_list_variable').DataTable().destroy();
		table = $('#index_list_variable').DataTable({
				autoWidth: false,
				searching: true,
				paging: true,
				"pageLength": 10,
				serverSide: true,
				"processing": true,
				"order": [],
				"ordering": false,
				"columnDefs": [
						{ "width": "4%", "targets": [0,1,3],  className: "text-left","visible":false },
						{ "width": "100%", "targets": 2,  className: "text-left" },
						
					],
				ajax: { 
					url: '{site_url}tlaba_rugi_setting/list_variable', 
					type: "POST" ,
					dataType: 'json',
					data : {
							template_id:template_id,
							disabel:disabel,
						   }
				},
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					
					if (aData[0]=='0' && aData[1]=='1'){
						$('td', nRow).css('background-color', '#f0f0f0');					
					}
					if (aData[0]=='1'  && aData[1]=='1'){
						$('td', nRow).css('background-color', '#f9f9f9');					
						// $(nRow).find('td:eq(0)').css('background-color', 'white');
					}
					if (aData[1]=='2'){
						// alert('ada');
						$('td', nRow).css('background-color', '#fdf3e5');	
							// if (aData[0]=='1'){
								// $(nRow).find('td:eq(0)').css('background-color', 'white');
								
							// }
						// $(nRow).find('td:eq(0)').css('background-color', 'white');
					}
            },
			});
	}
	function pilih_variable($nama_variable){
		// alert($("#formula").val());
		if ($("#formula").val()==''){
		var new_value=$nama_variable;
			
		}else{
		var new_value=" + "+$nama_variable;
			
		}
		$("#formula").val($("#formula").val()+new_value);
		conver_formula();
		$("#modal_list_variable").modal('hide');
		// alert(new_value);
	}
	$(document).on("click","#btn_list_variable",function(){	
		
		$("#modal_list_variable").modal('show');
	});
	$(document).on("click","#btn_check",function(){	
		conver_formula();
		
	});
	function conver_formula(){
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/check_convert_formula',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val(),
				formula: $("#formula").val(),
				},
			success: function(data) {
				// alert(data.formula);
				
				$("#xformula").val(data.formula);
			}
		});
	}
	$(document).on("click","#btn_refresh_list",function(){	
		list_variable();
	});
	$(document).on("click","#btn_add_perhitugan_section",function(){	
		clear_perhitungan();
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/max_section',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val()
				},
			success: function(data) {
				// alert(data.nama);
				$("#urutan_perhitungan").val(data.urutan)
				$("#lev_perhitungan").val(0)
				$("#header_id_perhitungan").val(0)
				$("#nama_perhitungan").focus();
			}
		});
		$("#modal_add_perhitungan").modal('show');
	});
	function add_perhitugan_kategori($id){		
		clear_perhitungan();
		$('#header_id_perhitungan').val($id);
		$("#modal_add_perhitungan").modal('show');
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/max_perhitungan',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val(),header_id:$id
				},
			success: function(data) {
				// alert(data.nama);
				$("#nama_perhitungan").focus();
				$("#urutan_perhitungan").val(data.urutan)
				$("#lev_perhitungan").val(1)
			}
		});
	}
	
	function clear_perhitungan(){
		$("#header_id_perhitungan").val('');
		$("#id_edit_perhitungan").val('');
		$("#nama_perhitungan").val('');
		$("#urutan_perhitungan").val('');
		$("#formula").val('');
	}
	function validate_simpan_perhitungan(){
		if ($("#nama_perhitungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Perhitungan", "error");
			return false;
		}
		if ($("#urutan_perhitungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Urutan Perhitungan", "error");
			return false;
		}
		
		return true;
	}
	function edit_perhitungan($id,$header_id){
		$("#id_edit_perhitungan").val($id);
		$("#header_id_perhitungan").val($header_id);
		$.ajax({
			url: '{site_url}tlaba_rugi_setting/get_info_perhitungan',
			type: 'POST',
			dataType: 'json',
			data: {
				id: $id
				},
			success: function(data) {
				// alert(data.nama);
				$("#nama_perhitungan").val(data.nama)
				$("#urutan_perhitungan").val(data.urutan)
				$("#lev_perhitungan").val(data.lev);
				$("#formula").val(data.formula);
				$("#modal_add_perhitungan").modal('show');
				conver_formula();
			}
		});
	}
	function hapus_perhitungan($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Perhitungan ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			var table = $('#index_list').DataTable();
			$.ajax({
				url: '{site_url}tlaba_rugi_setting/hapus_perhitungan',
				type: 'POST',
				dataType: 'json',
				data: {
					id: $id
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});						
					table.ajax.reload( null, false );
				}
			});
		});

		
	}
</script>
