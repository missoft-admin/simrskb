<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tlaba_rugi_setting" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('tlaba_rugi_setting/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="nama">Nama Layout / Template</label>
				<div class="col-md-7">
					<input type="hidden" class="form-control" id="disabel" name="disabel" value="{disabel}">
					<input type="hidden" class="form-control" id="id" name="id" value="{id}">
					<input type="hidden" class="form-control" id="template_id" name="template_id" value="{id}">
					<input type="text" <?=$disabel?> class="form-control" id="nama" placeholder="Nama Layout / Template" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="nama">User Akses</label>
				<div class="col-md-7">
					<select name="iduser[]" <?=$disabel?> id="iduser" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih User Acces" multiple>		
						<?foreach($list_user_akses as $row){?>
						<option value="<?=$row->id?>" <?=$row->pilih?>><?=$row->name?></option>
						<?}?>
					</select>
				</div>
			</div>
			<?if ($disabel==''){?>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}tlaba_rugi_setting" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>

</div>

<? if ($id !=''){ ?>
<div class="block">
	<div class="block-header bg-primary">
		<ul class="block-options">
       
    </ul>
		<h3 class="block-title">DETAIL VARIABLE</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<?if ($disabel==''){?>
			<div class="form-group" style="margin-bottom: 5px;">
				<div class="col-md-12">
				<button class="btn btn-primary btn-xs" id="btn_add_section" type="button"><i class="fa fa-plus"></i> Section</button>
				<button class="btn btn-danger btn-xs" id="btn_add_perhitugan_section" type="button"><i class="fa fa-plus"></i> Perhitungan Section</button>
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th hidden>Actions</th>
								<th hidden>Actions</th>
								<th style="width: 10%;">No</th>
								<th style="width: 75%;">Nama</th>
								<th style="width: 75%;"></th>
								<th style="width: 15%;">Actions</th>
							</tr>							
						</thead>
						<tbody></tbody>							
					</table>
					
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>

<?}?>
<?$this->load->view('Tlaba_rugi_setting/modal/mod_add_section')?>
<?$this->load->view('Tlaba_rugi_setting/modal/mod_add_kategori')?>
<?$this->load->view('Tlaba_rugi_setting/modal/mod_add_akun')?>
<?$this->load->view('Tlaba_rugi_setting/modal/mod_add_perhitungan')?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var rTipe='';
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
		load_index();
		list_variable();
	})	
	function load_index(){
		var template_id=$("#template_id").val();
		var disabel=$("#disabel").val();
		
		$('#index_list').DataTable().destroy();
		table = $('#index_list').DataTable({
				autoWidth: false,
				searching: false,
				serverSide: true,
				"processing": true,
				"order": [],
				"paging": false,
				"ordering": false,
				"columnDefs": [
						{ "width": "4%", "targets": [0,1],  className: "text-left","visible":false },
						{ "width": "5%", "targets": 2,  className: "text-right" },
						{ "width": "45%", "targets": 3,  className: "text-left" },
						{ "width": "40%", "targets": 4,  className: "text-left" },
						{ "width": "10%", "targets": 5,  className: "text-center" },
						
					],
				ajax: { 
					url: '{site_url}tlaba_rugi_setting/load_variable', 
					type: "POST" ,
					dataType: 'json',
					data : {
							template_id:template_id,
							disabel:disabel,
						   }
				},
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					
					if (aData[0]=='0' && aData[1]=='1'){
						$('td', nRow).css('background-color', '#e9e9e9');					
					}
					if (aData[0]=='1'  && aData[1]=='1'){
						$('td', nRow).css('background-color', '#d1e2ef');					
						$(nRow).find('td:eq(0)').css('background-color', 'white');
					}
					if (aData[1]=='2'){
						// alert('ada');
						$('td', nRow).css('background-color', '#fdf3e5');	
							if (aData[0]=='1'){
								$(nRow).find('td:eq(0)').css('background-color', 'white');
								
							}
						// $(nRow).find('td:eq(0)').css('background-color', 'white');
					}
            },
			});
	}
	
	function validate_final(){
		var nama=$("#nama").val();
		var iduser=$("#iduser").val();
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Template", "error");
			return false;
		}
		if (iduser==null){
			sweetAlert("Maaf...", "Tentukan User Akses", "error");
			return false;
		}
		
		// alert(iduser);return false;
		return true;
	}
	
	//BATAS HAPUS
	
</script>