<?php 'Naha'?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Print Barang <?=$namaunit?></title>
  <style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    
  </style>
  <script type="text/javascript">
    try {
      this.print();
    } catch (e) {
      window.onload = window.print;
    }
  </script>
</head>

<body>
  <table class="content">
      <tr>
        <td rowspan="3" width="30%" class="text-center"><img src="{logo1_rs}" alt="" width="60" height="60"></td>
        <td rowspan="2" colspan="3" width="50%"  class="text-center text-bold text-judul"><u>LAPORAN STOK <?=strtoupper($namaunit)?></u></td>
		<td rowspan="2" width="20%"></td>		
      </tr>
	  <tr></tr>
	  <tr>
		<td width="14%" class="text-left text-bold text-top">Tanggal Cetak</td>
		<td width="20%" colspan="" class="text-top">: <?=HumanDateLong(date('Y-m-d H:i:s'))?></td>		
		<td width="10%" colspan="" class="text-top text-bold text-top">Periode Cetak</td>		
		<td width="20%" colspan="" class="text-top">: <?=$nama_periode?></td>		
	  </tr>
	  <tr>
	    <td  class="text-center">{alamat_rs1}</td>
		<td  class="text-left text-bold text-top">Unit Pelayanan</td>
		<td width="20%" colspan="" class="text-top">: {namaunit}</td>
		<td width="10%" colspan="" class="text-top text-bold text-top">Tipe Barang</td>		
		<td width="20%" colspan="" class="text-top">: {namatipe}</td>
	  </tr>
	  <tr>
	    <td  class="text-center"><?=$telepon_rs.' '.$fax_rs?></td>
		<td class="text-left text-bold text-top">Kategori</td>
		<td class=" text-top" colspan="" >: {namakategori}</td>
		<td width="20%" colspan="" class="text-top text-bold text-top">Status Stock</td>		
		<td width="20%" colspan="" class="text-top">: {namastok}</td>
	  </tr>
	  <tr>
	    <td  class="text-center"></td>
		<td class="text-left text-bold text-top">User Cetak</td>
		<td class=" text-top">: <?=$this->session->userdata('user_name')?></td>
		<td width="20%" colspan="" class="text-top text-bold text-top"></td>		
		<td width="20%" colspan="" class="text-top"> </td>		
	  </tr>
    </table>
  <br/>
  <br/>
  <table width="100%">
    <thead>
      <tr>
        <th class="border-full text-center">NO</th>
        <th class="border-full text-center">NAMA BARANG</th>
        <th class="border-full text-center">TIPE </th>
        <th class="border-full text-center">SATUAN</th>
        <th class="border-full text-center">HARGA SATUAN</th>
        <th class="border-full text-center">CURRENT STOCK</th>
        <th class="border-full text-center">STATUS</th>
        <th class="border-full text-center">TOTAL HARGA</th>
      </tr>
    </thead>
    <tbody>
      <?php $i=1;
	  $idtipe='';
	  $namatipe='';
	  $idkategori='';
	  $jml_all=0;
	  $jml=0;
	  $jml_harga=0;
	  $jml_harga_all=0;
	  if ($group=='namakategori'){
		$cari='KATEGORI';		  
	  }elseif ($group=='namatipe'){
		$cari='TIPE';		  
	  }else{
		  $cari='';
	  }
	  // $cari='TIPE';
	  $cari_id="";
	  $cari_nama="";
	  foreach ($details as $row):?>
	  <?
	  if ($cari!=''){
		if ($cari=='TIPE'){
			$cari_id=$row->idtipe;
			$cari_nama=$row->namatipe;
		}elseif ($cari=='KATEGORI'){
			$cari_id=$row->idkategori;
			$cari_nama=$row->namakategori;
		}
			if ($idtipe==''){
				echo '<tr><td colspan="8" class="border-full text-left text-bold"> '.$cari.' BARANG : '.$cari_nama.'</td></tr>';
				$idtipe=$cari_id;
			}else{
				if ($idtipe !=$cari_id){
					echo '<tr>
							<td colspan="5" class="border-full text-left text-bold"> TOTAL '.$cari.' BARANG : '.$namatipe.'</td>
							<td class="border-full text-right text-bold">'.number_format($jml,0).'</td>
							<td class="border-full text-right text-bold"></td>
							<td class="border-full text-right text-bold">'.number_format($jml_harga,0).'</td>
					  </tr>';
					echo '<tr><td colspan="8" class="border-full text-left text-bold"> '.$cari.' BARANG : '.$cari_nama.'</td></tr>';
					$idtipe=$cari_id;
					$jml=0;
					$jml_harga=0;
					  
				}
			}
	 }	
			$namatipe=$cari_nama;
			$jml=$jml+$row->stok;
			$jml_all=$jml_all+$row->stok;
			$jml_harga=$jml_harga+($row->stok*$row->hpp);
			$jml_harga_all=$jml_harga_all+($row->stok*$row->hpp);
	  ?>
      <tr>
		
        <td class="border-full text-right"><?=$i?></td>
        <td class="border-full"><?=$row->namabarang?></td>
        <td class="border-full"><?=$row->namatipe?></td>
        <td class="border-full"><?=$row->namasatuan?></td>
        <td class="border-full text-right"><?=number_format($row->hpp,0)?></td>
        <td class="border-full text-right"><?=$row->stok?></td>
		<?
			$status='';
			if ($row->stok > $row->stokreorder){
				$status='Available';
			}elseif (($row->stok <= $row->stokreorder) && ($row->stok > $row->stokminimum)){
				$status='Back To Order';
			}else{
				$status='Stok Minimum';
			}
		
		?>
        <td class="border-full text-center"><?=$status?></td>
        <td class="border-full text-right"><?=number_format($row->stok*$row->hpp,0)?></td>
      </tr>
      <?php $i++;endforeach;?>
	  <?
		echo '<tr>
							<td colspan="5" class="border-full text-left text-bold"> TOTAL '.$cari.' BARANG : '.$namatipe.'</td>
							<td class="border-full text-right text-bold">'.number_format($jml,0).'</td>
							<td class="border-full text-right text-bold"></td>
							<td class="border-full text-right text-bold">'.number_format($jml_harga,0).'</td>
			 </tr>';
		echo '<tr>
					<td colspan="5" class="border-full text-left text-bold"> TOTAL ALL :</td>
					<td class="border-full text-right text-bold">'.number_format($jml_all,0).'</td>
					<td class="border-full text-right text-bold"></td>
					<td class="border-full text-right text-bold">'.number_format($jml_harga_all,0).'</td>
			  </tr>';
	  
	  ?>
    </tbody>
  </table>
  <br/>
  
</body>

</html>