<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Kartu Stok</title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>

	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }


  </style>
</head>

<body>
	<table class="content">
		<tr>
			<td rowspan="3" width="30%" class="text-center"><img src="<?=$logo1_rs?>" alt="" width="60" height="60"></td>
			<td rowspan="2" colspan="3" width="50%" class="text-center text-bold text-judul"><u>KARTU STOK</u></td>
			<td rowspan="2" width="20%"></td>
		</tr>
		<tr></tr>
		<tr>
			<td width="14%" class="text-left text-bold text-top">Unit Pelayanan</td>
			<td width="20%" colspan="" class="text-top">: <?=$namaunit?></td>
			<td width="10%" colspan="" class="text-top text-bold text-top">Nama Barang</td>
			<td width="20%" colspan="" class="text-top">: <?=$namabarang?></td>
		</tr>
		<tr>
			<td class="text-center"><?=$alamat_rs1?></td>
			<td class="text-left text-bold text-top">Kode Barang</td>
			<td width="20%" colspan="" class="text-top">: <?=$kode?></td>
			<td width="10%" colspan="" class="text-top text-bold text-top">Tipe Barang</td>
			<td width="20%" colspan="" class="text-top">: <?=$namatipe?></td>
		</tr>
		<tr>
			<td class="text-center"><?=$telepon_rs.' '.$fax_rs?></td>
			<td class="text-left text-bold text-top">Satuan Kecil</td>
			<td class=" text-top" colspan="">: <?=($namasatuan)?></td>
			<td width="20%" colspan="" class="text-top text-bold text-top">Kategori</td>
			<td width="20%" colspan="" class="text-top">: <?=$namakategori?></td>
		</tr>
	</table>

	<br>

	<table class="content-2">
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td class="border-full text-center" width="5%">Tanggal</td>
			<td class="border-full text-center" width="10%">User</td>
			<td class="border-full text-center" width="10%">Jenis Transaksi</td>
			<td class="border-full text-center" width="5%">No. Transaksi</td>
			<td class="border-full text-center" width="5%">Tujuan</td>
			<td class="border-full text-center" width="20%">Keterangan</td>
			<td class="border-full text-center" width="5%">Penerimaan</td>
			<td class="border-full text-center" width="5%">Pengeluaran</td>
			<td class="border-full text-center" width="5%">Sisa Stok</td>
		</tr>
		<?php $sisaStok = 0; ?>
		<?php foreach ($kartustok as $row) { ?>
		<?php $sisaStok = $sisaStok + $row->penerimaan - $row->pengeluaran; ?>
		<tr>
			<td class="border-full text-center"><?=$row->tanggal?></td>
			<td class="border-full text-center"><?=$row->user?></td>
			<td class="border-full text-center"><?=$row->jenis_trx?></td>
			<td class="border-full text-center"><?=$row->notransaksi?></td>
			<td class="border-full text-center"><?=$row->tujuan?></td>
			<td class="border-full"><?=$row->keterangan?></td>
			<td class="border-full text-center"><?=number_format($row->penerimaan)?></td>
			<td class="border-full text-center"><?=number_format($row->pengeluaran)?></td>
			<td class="border-full text-center"><?=number_format($sisaStok)?></td>
		</tr>
		<?php } ?>
	</table>
</body>

</html>
