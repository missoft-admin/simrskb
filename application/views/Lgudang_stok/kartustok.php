<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" for="asalpasien">Unit Pelayanan</label>
					<div class="col-md-2">
						: {namaunit}
					</div>
					<label class="col-md-2 control-label" for="asalpasien">Nama Barang</label>
					<div class="col-md-2">
						: {namabarang}
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" for="asalpasien">Kode Barang</label>
					<div class="col-md-2">
						: {kode}
					</div>
					<label class="col-md-2 control-label" for="asalpasien">Tipe Barang</label>
					<div class="col-md-2">
						: {namatipe}
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" for="asalpasien">Satuan Kecil</label>
					<div class="col-md-2">
						: {namasatuan}
					</div>
					<label class="col-md-2 control-label" for="asalpasien">Kategori</label>
					<div class="col-md-2">
						: {namakategori}
					</div>
				</div>
			</div>
		</div>
		<hr>
		<?php echo form_open('lgudang_stok/print_barang','class="form-horizontal" id="form-work" target="_blank"') ?>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" for="asalpasien">Periode</label>
					<div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" for="asalpasien">Jenis Transaksi</label>
					<div class="col-md-4">
						<select name="jenis_trx" id="jenis_trx" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>-Semua Transaksi-</option>
							<?php foreach  ($list_jenis as $row) { ?>
							<option value="<?=$row->id?>" <?=($jenis_trx == $row->id) ? "selected" : "" ?>><?=$row->nama_trx?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-2">
						<button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
					<div class="col-md-2">
						<a href="{base_url}lgudang_stok/print_kartustok/<?=$idtipe?>/<?=$idbarang?>/<?=$idunit?>" target="_blank" class="btn btn-primary text-uppercase" type="button" id="btn_print" name="btn_print" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Print / Pdf</a>
					</div>
					<div class="col-md-2">
						<a href="{base_url}lgudang_stok/export_excel/<?=$idtipe?>/<?=$idbarang?>/<?=$idunit?>" class="btn btn-danger text-uppercase" type="button" id="btn_excel"  target="_blank" name="btn_excel" style="font-size:13px;width:100%;float:right;"><i class="fa fa-file-excel-o"></i> Export Excel</a>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
		<hr>
		<input type="hidden" id="idtipe" value="{idtipe}">
		<input type="hidden" id="idbarang" value="{idbarang}">
		<input type="hidden" id="idunit" value="{idunitpelayanan}">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th style="width:10%">Tanggal</th>
					<th style="width:5%">User</th>
					<th style="width:12%">Jenis Transaksi</th>
					<th style="width:5%">No. Transaksi</th>
					<th style="width:10%">Tujuan</th>
					<th style="width:20%">Keterangan</th>
					<th style="width:5%">Penerimaan</th>
					<th style="width:5%">Pengeluaran</th>
					<th style="width:5%">Sisa Stok</th>
					<th style="width:5%">Aksi</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		loadBarang();

	});
	$(document).on("click", "#btn_filter", function() {
		// alert($("#idunitpelayanan").val());
		if ($("#idunitpelayanan").val() == '#') {
			sweetAlert("Maaf...", "Silahkan pilih Unit Pelayanan Untuk mempercepat pencarian", "error");
			return false;
		} else {
			loadBarang();
		}
	});

	function loadBarang() {
		var idunit = $("#idunit").val();
		var idtipe = $("#idtipe").val();
		var idbarang = $("#idbarang").val();
		var jenis_trx = $("#jenis_trx").val();
		var tanggaldari = $("#tanggaldari").val();
		var tanggalsampai = $("#tanggalsampai").val();

		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}lgudang_stok/getKS/',
				type: "POST",
				dataType: 'json',
				data: {
					tanggaldari: tanggaldari,
					tanggalsampai: tanggalsampai,
					idtipe: idtipe,
					idbarang: idbarang,
					idunit: idunit,
					jenis_trx: jenis_trx,

				}
			},
			columnDefs: [
						 {"targets": [1], "visible": false },
							{  className: "text-right", targets:[6,7,8] },
							{ "width": "10%", "targets": [0,2,3,4] },
							// { "width": "5%", "targets": [9] },
							{ "width": "7%", "targets": [6,7,8,9] },
							 {  className: "text-center", targets:[3] },
						]
					
		});
	}

	function clear_data() {

		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable({
			"searching": true,
			columnDefs: [
						 {"targets": [1], "visible": false },
							{  className: "text-right", targets:[6,7,8] },
							{ "width": "10%", "targets": [0,2,3,4] },
							// { "width": "5%", "targets": [9] },
							{ "width": "7%", "targets": [6,7,8,9] },
							 {  className: "text-center", targets:[3] },
						]
			
		});
	}
</script>
