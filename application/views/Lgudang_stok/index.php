<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
				<?php echo form_open('lgudang_stok/print_barang','class="form-horizontal" id="form-work" target="_blank"') ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idunitpelayanan">Unit Pelayanan</label>
					<div class="col-md-8">
						<select name="idunitpelayanan" id="idunitpelayanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>-Semua Unit-</option>
							<?php foreach  ($list_unitpelayanan as $row) { ?>
								<option value="<?=$row->id?>" <?=($idunitpelayanan == $row->id) ? "selected" : "" ?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idtipe">Tipe Barang</label>
					<div class="col-md-8">
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>-Pilih Semua-</option>
							<?php foreach  ($list_tipe as $row) { ?>
								<option value="<?=$row->id?>"><?=$row->text?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idkategori">Kategori Barang</label>
					<div class="col-md-8">
						<select name="idkategori" id="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>-Pilih Semua-</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="statusstok">Status Stok</label>
					<div class="col-md-8">
						<select name="statusstok" id="statusstok" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>-Pilih Semua-</option>
							<option value="1" <?=($statusstok == 1) ? "selected" : "" ?>>Available</option>
							<option value="2" <?=($statusstok == 2) ? "selected" : "" ?>>Back To Order</option>
							<option value="3" <?=($statusstok == 3) ? "selected" : "" ?>>Stok Minimum</option>
						</select>
					</div>
				</div>
				
			</div>
			<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="statusstok">Order By</label>
						<div class="col-md-10">
							<select class="js-select2 form-control" id="order_by" name="order_by[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
								<option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
								<option value="namatipe" selected>Tipe Barang</option>
								<option value="namakategori">Kategori</option>
								<option value="namabarang" selected>Nama Barang</option>
								<option value="stok">Stok</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-10">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-10">
							<button class="btn btn-danger text-uppercase" type="submit" id="btn_print" name="btn_print" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Print</button>
						</div>
					</div>
			</div>
		</div>
			<?php echo form_close() ?>
		<hr>

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>No</th>
					<th>Kode Barang</th>
					<th>Nama Barang</th>
					<th>Tipe Barang</th>
					<th>Kategori</th>
					<th>Satuan</th>
					<th>Stok</th>
					<th>HPP</th>
					<th>Nilai</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

$(document).ready(function(){
        loadBarang();
    })
	$("#idunitpelayanan").change(function(){
		// alert($(this).val());
		if ($(this).val()!='#'){
			load_tipe();
		}
		
	});
	$(document).on("click", "#btn_filter", function() {
		// // alert($("#idunitpelayanan").val());
		// if ($("#idunitpelayanan").val()=='#'){
			// sweetAlert("Maaf...", "Silahkan pilih Unit Pelayanan Untuk mempercepat pencarian", "error");
			// return false;
		// }else{
			loadBarang();
		// }
	});
	function loadBarang() {
		// alert($("#order_by").val());
		var order_by=$("#order_by").val();
		var idunit=$("#idunitpelayanan").val();
		var idtipe=$("#idtipe").val();
		var idkategori=$("#idkategori").val();
		var statusstok=$("#statusstok").val();
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}lgudang_stok/getBarang/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				idtipe: idtipe,
				idkategori: idkategori,
				statusstok: statusstok,
				order_by: order_by,
			}
		},
		columnDefs: [
			{  className: "text-right", targets:[0,6,7,8] },
			{  className: "text-center", targets:[1,3,4,5,9,10] },
			 { "width": "5%", "targets": [0,1,3,5,6] },
			 { "width": "10%", "targets": [7,8,9] },
			 { "width": "15%", "targets": [1,4,10] },
			 { "width": "20%", "targets": [2] }
		]
		});
	}
	// $(document).on("click", "#btn_print", function() {
		
			// print_barang();
	// });
	// function print_barang() {
		// $.ajax({        
           // url: '{site_url}lgudang_stok/print_barang/',
            // type: "POST",
            // data: {
				// idunit: idunit,
				// idtipe: idtipe,
				// idkategori: idkategori,
				// statusstok: statusstok,
				// order_by: order_by,
			// }
                   
    // }); 
	// }
	function clear_data(){
		
		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable( {
			"searching": true,
			columnDefs: []
		} );
	}
	function load_tipe(){
		clear_data();
		var idunit=$("#idunitpelayanan").val();
		$('#idtipe').find('option').remove();
		
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		var def='';
		if (idunit !='#'){
				$('#idtipe').append('<option value="#" selected>- Pilih Semua -</option>');
						
				$.ajax({
					url: '{site_url}tstockopname/list_tipe/'+idunit,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
						$('#idtipe')
						.append('<option value="' + row.id + '">' + row.text + '</option>');
						});
					}
				});

		}
	}
	$("#idtipe").change(function(){
		load_kategori();
	});
	function load_kategori(){
		var idtipe=$("#idtipe").val();
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		if (idtipe !='#'){
						
				$.ajax({
					url: '{site_url}tstockopname/list_kategori/'+idtipe,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
							
						$('#idkategori')
						.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
						});
					}
				});

		}
	}
	function TreeView($level, $name)
	{
		// console.log($name);
		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			if ($i > 0) {
				$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$indent += '';
			}
		}

		if ($i > 0) {
			$indent += '└─';
		} else {
			$indent += '';
		}

		return $indent + ' ' +$name;
	}
</script>
