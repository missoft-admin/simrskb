<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}medukasi" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content ">
		<?php echo form_open('medukasi/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="judul">Judul Edukasi</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="judul" placeholder="Judul Edukasi" name="judul" value="{judul}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom:5px">
				<label class="col-md-2 control-label" for="isi">Content</label>
				<div class="col-md-9">
					<textarea class="form-control js-summernote" name="isi" id="isi" required><?=$isi?></textarea>
				</div>
			</div>
			
		
			<div class="form-group">
				<div class="col-md-9 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}medukasi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_close() ?>
			
			
			
	</div>
	<?if ($id){?>
	<div class="block-content ">
		<div class="">
			<div class="row">
				
				<div class="form-group" style="margin-bottom:10px">
					<label class="col-md-2 control-label"></label>
					<div class="col-md-9">
						 <form class="dropzone" class="form-horizontal push-10-t" action="{base_url}medukasi/upload_files" method="post" enctype="multipart/form-data">
							<textarea id="keterangan" name="keterangan" class="form-control" style="display:none;"></textarea>
							<input name="idtransaksi" type="hidden" value="{id}">
						  </form>
					</div>
				</div>
				<hr>
				
			</div>
			<div class="row">
				<div class="form-group">
					<div class="col-md-9 col-md-offset-2">
						<div class="table-responsive">
						  <table class="table table-bordered table-striped table-responsive" id="listFile">
								<thead>
									<tr>
										<th width="1%">No</th>
										<th width="20%">File</th>
										<th width="20%">Keterangan</th>
										<th width="15%">Upload By</th>
										<th width="5%">Size</th>
										<th width="5%">Aksi</th>
									</tr>
								</thead>
								<tbody>
							 
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?}?>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var myDropzone 
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let mrisiko_id=$("#id").val();
	Dropzone.autoDiscover = false;
	myDropzone = new Dropzone(".dropzone", { 
	   autoProcessQueue: true,
	   maxFilesize: 30,
	});
	myDropzone.on("complete", function(file) {
	  
	  refresh_image();
	  myDropzone.removeFile(file);
	  
	});
	if (mrisiko_id){
		refresh_image();
	}
})	
function refresh_image(){
	var id=$("#id").val();
	
	$.ajax({
		url: '{site_url}medukasi/refresh_image/'+id,
		dataType: "json",
		success: function(data) {
			// if (data.detail!=''){
				$('#listFile tbody').empty();
				// $("#box_file2").attr("hidden",false);
				$("#listFile tbody").append(data.detail);
			// }
			// console.log();
			
		}
	});
}
function hapus_file(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus File?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}medukasi/delete_file',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					refresh_image();
					
				}
			});
	});
}


</script>