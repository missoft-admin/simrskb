<hr>

<div class="form-horizontal">
    <div class="row pull-10">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="tanggal_tab3">Tanggal Daftar</label>
                <div class="col-md-8">
                    <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                        <input class="form-control" type="text" id="tanggal_dari_tab3" placeholder="Tanggal Dari" value="">
                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                        <input class="form-control" type="text" id="tanggal_sampai_tab3" placeholder="Tanggal Sampai" value="">
                    </div>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="asal_pasien_tab3">Asal Pasien</label>
                <div class="col-md-8">
                    <select id="asal_pasien_tab3" name="asal_pasien_tab3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Tipe">
                        <option value="0">Semua</option>
                        <option value="1">Poliklinik</option>
                        <option value="2">Instalasi Gawat Darurat (IGD)</option>
                        <option value="3">Rawat Inap</option>
                        <option value="4">One Day Surgery (ODS)</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="dokter_peminta_tab3">Dokter Peminta</label>
                <div class="col-md-8">
                    <select id="dokter_peminta_tab3" name="dokter_peminta_tab3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mdokter') as $dokter) { ?>
                            <option value="<?=$dokter->id?>"><?=$dokter->nama?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="nomor_permintaan_tab3">Nomor Permintaan</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="nomor_permintaan_tab3" placeholder="Nomor Permintaan" name="nomor_permintaan_tab3" value="">
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="tujuan_radiologi_tab3">Tujuan Radiologi</label>
                <div class="col-md-8">
                    <select id="tujuan_radiologi_tab3" name="tujuan_radiologi_tab3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                        <option value="0" selected>Semua</option>
                        <?php foreach ($tujuan_radiologi as $r) { ?>
                        <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="medrec_tab3">No. Medrec</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="medrec_tab3" placeholder="No. Medrec" name="medrec_tab3" value="">
                </div>
            </div>
            
            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="rencana_pemeriksaan_tab3">Rencana Pemeriksaan</label>
                <div class="col-md-8">
                    <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                        <input class="form-control" type="text" id="rencana_pemeriksaan_dari_tab3" placeholder="Tanggal Dari" value="{tanggal}">
                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                        <input class="form-control" type="text" id="rencana_pemeriksaan_sampai_tab3" placeholder="Tanggal Sampai" value="{tanggal}">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="waktu_permintaan_tab3">Waktu Permintaan</label>
                <div class="col-md-8">
                    <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                        <input class="form-control" type="text" id="waktu_permintaan_dari_tab3" placeholder="Tanggal Dari" value="">
                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                        <input class="form-control" type="text" id="waktu_permintaan_sampai_tab3" placeholder="Tanggal Sampai" value="">
                    </div>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="klinik_kelas_tab3">Klinik / Kelas</label>
                <div class="col-md-8">
                    <select id="klinik_kelas_tab3" name="klinik_kelas_tab3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Klinik / Kelas">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mpoliklinik', ['status' => 1]) as $poliklinik) { ?>
                            <option value="<?=$poliklinik->id?>"><?=$poliklinik->nama?></option>
                        <?php } ?>
                        <?php foreach (get_all('mkelas') as $kelas) { ?>
                            <option value="<?=$kelas->id?>"><?=$kelas->nama?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="kelompok_pasien_tab3">Kelompok Pasien</label>
                <div class="col-md-8">
                    <select id="kelompok_pasien_tab3" name="kelompok_pasien_tab3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Kelompok Pasien">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mpasien_kelompok') as $kelompok) { ?>
                            <option value="<?=$kelompok->id?>"><?=$kelompok->nama?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="nomor_pendaftaran_tab3">Nomor Pendaftaran</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="nomor_pendaftaran_tab3" placeholder="Nomor Pendaftaran" name="nomor_pendaftaran_tab3" value="">
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="prioritas_tab3">Prioritas</label>
                <div class="col-md-8">
                    <select id="prioritas_tab3" name="prioritas_tab3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Kelompok Pasien">
                        <option value="0">Semua</option>
                        <?php foreach (list_variable_ref(85) as $row) { ?>
                        <option value="<?php echo $row->id; ?>" <?php '1' == $row->st_default ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="nama_pasien_tab3">Nama Pasien</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="nama_pasien_tab3" placeholder="Nama Pasien" name="nama_pasien_tab3" value="">
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 5px;">
                <label class="col-md-4 control-label" for=""></label>
                <div class="col-md-8">
                    <button class="btn btn-success text-uppercase" type="button" id="btn-filter-transaksi-daftar-rencana" style="font-size:13px; width:100%; float:right;"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="transaksi-tambah-daftar-rencana">
                <thead>
                    <tr>
                        <th width="5%">Action</th>
                        <th width="5%">Waktu Permintaan</th>
                        <th width="5%">Nomor Permintaan</th>
                        <th width="5%">Tanggal Daftar</th>
                        <th width="5%">Asal Pasien</th>
                        <th width="5%">Nomor Pendaftaran</th>
                        <th width="5%">Nomor Medrec</th>
                        <th width="5%">Nama Pasien</th>
                        <th width="5%">Detail</th>
                        <th width="5%">Rencana Pemeriksaan</th>
                        <th width="5%">Dokter Peminta</th>
                        <th width="5%">Tujuan Radiologi</th>
                        <th width="5%">Prioritas</th>
                        <th width="5%">Dibuat Oleh</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", "#btn-filter-transaksi-daftar-rencana", function() {
        $("#cover-spin").show();
        
        loadDataTableTransaksiDaftarRencana();
    });
});

function loadDataTableTransaksiDaftarRencana() {
    let tanggal_dari = $("#tanggal_dari_tab3").val();
    let tanggal_sampai = $("#tanggal_sampai_tab3").val();
    let asal_pasien = $("#asal_pasien_tab3 option:selected").val();
    let dokter_peminta = $("#dokter_peminta_tab3 option:selected").val();
    let nomor_permintaan = $("#nomor_permintaan_tab3").val();
    let tujuan_radiologi = $("#tujuan_radiologi_tab3 option:selected").val();
    let medrec = $("#medrec_tab3").val();
    let rencana_pemeriksaan_dari = $("#rencana_pemeriksaan_dari_tab3").val();
    let rencana_pemeriksaan_sampai = $("#rencana_pemeriksaan_sampai_tab3").val();
    let waktu_permintaan_dari = $("#waktu_permintaan_dari_tab3").val();
    let waktu_permintaan_sampai = $("#waktu_permintaan_sampai_tab3").val();
    let klinik_kelas = $("#klinik_kelas_tab3 option:selected").val();
    let kelompok_pasien = $("#kelompok_pasien_tab3 option:selected").val();
    let nomor_pendaftaran = $("#nomor_pendaftaran_tab3").val();
    let prioritas = $("#prioritas_tab3 option:selected").val();
    let nama_pasien = $("#nama_pasien_tab3").val();

    $('#transaksi-tambah-daftar-rencana').DataTable().destroy();
    $('#transaksi-tambah-daftar-rencana').DataTable({
        "autoWidth": false,
        "searching": true,
        "pageLength": 50,
        "serverSide": true,
        "processing": false,
        "order": [],
        "pageLength": 10,
        "ordering": false,
        "columnDefs": [
            {
                "width": "5%",
                "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13],
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": [11],
                "className": "text-left"
            },
        ],
        ajax: {
            url: '{site_url}term_radiologi_ctscan_permintaan/getIndexDaftarRencana',
            type: "POST",
            dataType: 'json',
            data: {
                tanggal_dari: tanggal_dari,
                tanggal_sampai: tanggal_sampai,
                asal_pasien: asal_pasien,
                dokter_peminta: dokter_peminta,
                nomor_permintaan: nomor_permintaan,
                tujuan_radiologi: tujuan_radiologi,
                medrec: medrec,
                rencana_pemeriksaan_dari: rencana_pemeriksaan_dari,
                rencana_pemeriksaan_sampai: rencana_pemeriksaan_sampai,
                waktu_permintaan_dari: waktu_permintaan_dari,
                waktu_permintaan_sampai: waktu_permintaan_sampai,
                klinik_kelas: klinik_kelas,
                kelompok_pasien: kelompok_pasien,
                nomor_pendaftaran: nomor_pendaftaran,
                prioritas: prioritas,
                nama_pasien: nama_pasien,
            }
        }
    });

    $("#cover-spin").hide();
}
</script>