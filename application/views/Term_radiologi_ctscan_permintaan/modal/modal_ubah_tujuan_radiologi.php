<!-- Modal Ubah Tujuan Radiologi -->
<div class="modal fade" id="modal-ubah-tujuan-radiologi" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-danger">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Ubah Tujuan Radiologi</h3>
				</div>
                <div class="block-content">
                    <h5 class="text-center">Apakah anda yakin akan memindahkan tujuan pemeriksaan? memindahkan tujuan akan otomatis mengikuti urutan antrian.</h5>
                    <br><br>
                    <div class="form-group">
                        <label for="selectUbahTujuanLab">Tujuan Baru</label>
                        <select id="selectUbahTujuanLab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <?php foreach ($tujuan_radiologi as $r) { ?>
                            <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btn-ubah-tujuan-radiologi" class="btn btn-success" data-dismiss="modal">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", "#ubah-tujuan-radiologi", function() {
        transaksiId = $(this).data('transaksi-id');
    });

    $(document).on("click", "#btn-ubah-tujuan-radiologi", function() {
        let tujuanRadiologi = $("#selectUbahTujuanLab option:selected").val();
        
        ubahTujuanRadiologi(transaksiId, tujuanRadiologi);
    });
});

function ubahTujuanRadiologi(transaksiId, tujuanRadiologi) {
    $.ajax({
        url: '{site_url}term_radiologi_ctscan/ubah_tujuan_radiologi/' + transaksiId + '/' + tujuanRadiologi,
        success: function (result) {
            // If deletion is successful, reload the DataTable
            loadDataTableTransaksiPermintaan();
        },
        error: function (error) {
            // Handle error if deletion fails
            console.error('Error changing order:', error);
            alert('Error changing order. Please try again.');
        }
    });
}
</script>
