<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2001'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2003'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2004'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_label" onclick="load_tab_3()"><i class="si si-note"></i> Isi Pegkajian</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2005'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="load_tab_4()"><i class="fa fa-check-square-o"></i> Logic Hasil Pengkajian</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2001'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_sga/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_ina">Judul Header <span style="color:red;"> INA</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_ina" name="judul_header_ina" value="{judul_header_ina}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_ina">Judul Footer <span style="color:red;"> INA </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_ina" name="judul_footer_ina" value="{judul_footer_ina}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_eng">Judul Footer <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_eng" name="judul_footer_eng" value="{judul_footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2003'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_hak_akses">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Profesi</th>
											<th width="15%">Spesialisasi</th>
											<th width="15%">PPA</th>
											<th width="15%">Melihat</th>
											<th width="10%">Input</th>
											<th width="10%">Edit</th>
											<th width="10%">Hapus</th>
											<th width="10%">Cetak</th>
											<th width="10%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													
													<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
													<?foreach(list_variable_ref(22) as $r){?>
														<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</th>
											<th>
												<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
													
												</select>
											</th>
											
											<th>
												<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<?php if (UserAccesForm($user_acces_form,array('1761'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
												<?}?>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2004'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_label">
			
			<?php echo form_open('setting_sga/save_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Pengkajian</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="header_pengkajian">Header</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="header_pengkajian" name="header_pengkajian" value="{header_pengkajian}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="footer_pengkajian">Footer<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="footer_pengkajian" name="footer_pengkajian" value="{footer_pengkajian}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="btn_save_label"></label>
						<div class="col-md-10">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="h5"><?=text_primary('PARAMETER')?></div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_param">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="50%">Parameter</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="param_id" value="" placeholder="id"></th>
											<th>
												<input id="parameter" class="form-control" type="text" style="width:100%"  placeholder="Parameter" value="" >
											</th>
											
											<th>
												<div class="btn-group">
												<button class="btn btn-primary btn-sm simpan_param" type="button" onclick="simpan_param()"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning btn-sm clear_param" type="button" onclick="clear_param()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>	
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="h5"><?=text_primary('SKOR AKHIR')?></div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_nilai">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="30%">Nilai Parameter</th>
											<th width="50%">Tindakan</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="nilai_id" value="" placeholder="id"></th>
											<th>
												<select id="nilai_parameter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<?foreach(get_all('merm_referensi',array('ref_head_id'=>255,'status'=>1)) as $r){?>
														<option value="<?=$r->nilai?>" ><?=$r->ref?></option>
													<?}?>
												</select>
											</th>
											<th>
												<input id="tindakan" class="form-control" type="text" style="width:100%"  placeholder="Parameter" value="" >
											</th>
											
											
											<th>
												<div class="btn-group">
												<button class="btn btn-primary btn-sm simpan_nilai" type="button" onclick="simpan_nilai()"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning btn-sm clear_nilai" type="button" onclick="clear_nilai()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>	
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2005'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PERHITUNGAN HASIL ASSESMEN')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_hasil">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="25%">Hasil IMT</th>
											<th width="25%">Hasil Skor SGA</th>
											<th width="30%">Ref Nilai SGA / GIZI</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="hasil_id" value="" placeholder="id"></th>
											<th>
												<select id="nilai_imt" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="#" selected >Pilih</option>
													<?foreach(get_all('mberat',array('staktif'=>1)) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->kategori_berat?></option>
													<?}?>
												</select>
												
											</th>
											<th>
												<select id="nilai_param" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected >Pilih</option>
													<?foreach(get_all('merm_referensi',array('ref_head_id'=>255,'status'=>1)) as $r){?>
														<option value="<?=$r->nilai?>" ><?=$r->ref?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="nilai_gizi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected >Pilih</option>
													<?foreach(get_all('merm_referensi',array('ref_head_id'=>256,'status'=>1)) as $r){?>
														<option value="<?=$r->nilai?>" ><?=$r->ref?></option>
													<?}?>
												</select>
												
											</th>
											
											<th>
												<div class="btn-group">
												<button class="btn btn-primary btn-sm simpan_hasil" type="button" onclick="simpan_hasil()"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning btn-sm clear_hasil" type="button" onclick="clear_hasil()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>	
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
			
		
	</div>
</div>
<div class="modal in" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tambah value</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-horizontal">
									<label for="parameter_add">Parameter :</label><br>
									<label id="parameter_add"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_jawaban" style=" vertical-align: baseline;">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="25%">Hasil SGA</th>
												<th width="55%">Deskripsi Parameter</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th  style=" vertical-align: top;">#
													
												</th>
												<th  style=" vertical-align: top;">
													<select tabindex="8" id="skor" name="skor" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Referensi Nilai" required>
														<option value="" selected></option>
														<?foreach(get_all('merm_referensi',array('ref_head_id'=>255,'status'=>1)) as $r){?>
														<option value="<?=$r->nilai?>"><?=$r->ref?></option>
														<?}?>
														
													</select>
												</th>
												<th  style=" vertical-align: top;">
													<input type="text" class="form-control" id="deskripsi_nama"  style="width:100%"  placeholder="Deskripsi Parameter" name="deskripsi_nama" value="" >
													<input type="hidden" class="form-control" id="skor_id" value="" >
												</th>
												
												<th style=" vertical-align: top;">
													<div class="btn-group">
													<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_jawaban" name="btn_tambah_jawaban"><i class="fa fa-save"></i> Simpan</button>
													<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_jawaban()"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>	
								
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	if (tab=='3'){
		load_tab_3();
		
	}
	if (tab=='4'){
		load_tab_4();
	}
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

})	
function load_tab_3(){
	clear_param();
	clear_nilai();
	load_param();
	load_nilai();
}
function load_tab_4(){
	clear_hasil();
	load_hasil();
}
function clear_param(){
	$("#ref_nilai").val('255').trigger('change.select2');
	$("#param_id").val('');
	$("#parameter").val('');
	$(".simpan_param").html('<i class="fa fa-save"></i> Save');
}
function simpan_param(){
	let param_id=$("#param_id").val();
	let parameter=$("#parameter").val();
	let ref_nilai=$("#ref_nilai").val();
	let kategori_param=$("#kategori_param").val();
	let warna=$("#warna").val();
	
	if (parameter==''){
		sweetAlert("Maaf...", "Tentukan Parameter", "error");
		return false;
	}
	if (ref_nilai==''){
		sweetAlert("Maaf...", "Tentukan Referensi", "error");
		return false;
	}

	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_sga/simpan_param', 
		dataType: "JSON",
		method: "POST",
		data : {
				param_id:param_id,
				parameter:parameter,
				ref_nilai:ref_nilai,
				
			},
		complete: function(data) {
			clear_param();
			$('#index_param').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});
}
function load_param(){
	$('#index_param').DataTable().destroy();	
	table = $('#index_param').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_sga/load_param', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function edit_param(param_id){
	$("#param_id").val(param_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_sga/find_param',
		type: 'POST',
		dataType: "JSON",
		data: {id: param_id},
		success: function(data) {
			$(".simpan_param").html('<i class="fa fa-save"></i> Edit');
			$("#param_id").val(data.id);
			$("#parameter").val(data.parameter);
			$("#ref_nilai").val(data.ref_nilai).trigger('change.select2');
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_param(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Parameter?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_sga/hapus_param',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_param').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
//Nilai
function clear_nilai(){
	$("#nilai_parameter").val('#').trigger('change.select2');
	$("#nilai_id").val('');
	$("#tindakan").val('');
	$(".simpan_nilai").html('<i class="fa fa-save"></i> Save');
}
function simpan_nilai(){
	let nilai_id=$("#nilai_id").val();
	let tindakan=$("#tindakan").val();
	let nilai_parameter=$("#nilai_parameter").val();
	let kategori_nilai=$("#kategori_nilai").val();
	let warna=$("#warna").val();
	
	if (tindakan==''){
		sweetAlert("Maaf...", "Tentukan Tindakan", "error");
		return false;
	}
	if (nilai_parameter==''){
		sweetAlert("Maaf...", "Tentukan Referensi Nilai", "error");
		return false;
	}

	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_sga/simpan_nilai', 
		dataType: "JSON",
		method: "POST",
		data : {
				nilai_id:nilai_id,
				tindakan:tindakan,
				nilai_parameter:nilai_parameter,
				
			},
		complete: function(data) {
			clear_nilai();
			$('#index_nilai').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});
}
function load_nilai(){
	$('#index_nilai').DataTable().destroy();	
	table = $('#index_nilai').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_sga/load_nilai', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function edit_nilai(nilai_id){
	$("#nilai_id").val(nilai_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_sga/find_nilai',
		type: 'POST',
		dataType: "JSON",
		data: {id: nilai_id},
		success: function(data) {
			$(".simpan_nilai").html('<i class="fa fa-save"></i> Edit');
			$("#nilai_id").val(data.id);
			$("#tindakan").val(data.tindakan);
			$("#nilai_parameter").val(data.nilai_parameter).trigger('change.select2');
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_nilai(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Nilai?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_sga/hapus_nilai',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_nilai').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
//HASIL
function clear_hasil(){
	$("#nilai_param").val('#').trigger('change.select2');
	$("#nilai_gizi").val('#').trigger('change.select2');
	$("#nilai_imt").val('#').trigger('change.select2');
	$("#hasil_id").val('');
	$(".simpan_hasil").html('<i class="fa fa-save"></i> Save');
}
function simpan_hasil(){
	let hasil_id=$("#hasil_id").val();
	let nilai_gizi=$("#nilai_gizi").val();
	let nilai_param=$("#nilai_param").val();
	let nilai_imt=$("#nilai_imt").val();
	let warna=$("#warna").val();
	
	if (nilai_imt=='#'){
		sweetAlert("Maaf...", "Tentukan Nilai IMT", "error");
		return false;
	}
	if (nilai_gizi=='#'){
		sweetAlert("Maaf...", "Tentukan Nilai SGA", "error");
		return false;
	}
	if (nilai_param=='#'){
		sweetAlert("Maaf...", "Tentukan Referensi Nilai", "error");
		return false;
	}

	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_sga/simpan_hasil', 
		dataType: "JSON",
		method: "POST",
		data : {
				hasil_id:hasil_id,
				nilai_gizi:nilai_gizi,
				nilai_param:nilai_param,
				nilai_imt:nilai_imt,
				
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==false){
				sweetAlert("Maaf...", "DATA DUPLICATE", "error");
				return false;
			}else{
				clear_hasil();
				$('#index_hasil').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
			
		}
	});
}
function load_hasil(){
	$('#index_hasil').DataTable().destroy();	
	table = $('#index_hasil').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_sga/load_hasil', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function edit_hasil(hasil_id){
	$("#hasil_id").val(hasil_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_sga/find_hasil',
		type: 'POST',
		dataType: "JSON",
		data: {id: hasil_id},
		success: function(data) {
			$(".simpan_hasil").html('<i class="fa fa-save"></i> Edit');
			$("#hasil_id").val(data.id);
			$("#nilai_gizi").val(data.nilai_gizi).trigger('change.select2');
			$("#nilai_param").val(data.nilai_parameter).trigger('change.select2');
			$("#nilai_imt").val(data.nilai_imt).trigger('change.select2');
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_hasil(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Nilai?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_sga/hapus_hasil',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hasil').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_tab4(){
	load_dokter();
	load_pemberi();
	load_pendamping();
	set_user();
}

function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
	// load_formulir();	
}

// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_sga/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_sga/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_sga/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_default(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_sga/hapus_default',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil_default').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_sga/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$("#operand_tahun").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_bulan").val($(this).val()).trigger('change');
		$("#operand_hari").val($(this).val()).trigger('change');
		$("#umur_bulan").val($(this).val()).trigger('change');
		$("#umur_hari").val($(this).val()).trigger('change');
		$(".bulan").attr('disabled','disabled');
		$(".hari").attr('disabled','disabled');
	}else{
		$(".bulan").removeAttr('disabled');
		// $(".hari").removeAttr('disabled');
	}

});
$("#operand_bulan").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_hari").val($(this).val()).trigger('change');
		$(".hari").attr('disabled','disabled');
		$("#umur_hari").val($(this).val()).trigger('change');
	}else{
		$(".hari").removeAttr('disabled');
	}

});
$("#st_spesifik_sga").change(function(){
	if ($(this).val()=='1'){
		$(".setting_tidak").show();
	}else{
		$(".setting_tidak").hide();
	}

});
$("#btn_tambah_formulir").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let pertemuan_id=$("#pertemuan_id").val();
	let st_tujuan_terakhir=$("#st_tujuan_terakhir").val();
	let operand=$("#operand").val();
	// alert(operand);
	let lama_terakhir_tujan=$("#lama_terakhir_tujan").val();
	let st_formulir=$("#st_formulir").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tujuan", "error");
		return false;
	}
	
	if (st_formulir=='0'){
		sweetAlert("Maaf...", "Tentukan Status Formulir", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_sga/simpan_formulir', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe:idtipe,
				idpoli:idpoli,
				statuspasienbaru:statuspasienbaru,
				pertemuan_id:pertemuan_id,
				st_tujuan_terakhir:st_tujuan_terakhir,
				operand:operand,
				lama_terakhir_tujan:lama_terakhir_tujan,
				st_formulir:st_formulir,

			
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				$("#idtipe").val('#').trigger('change');
				$("#idpoli").val('#').trigger('change');
				$("#statuspasienbaru").val('#').trigger('change');
				$("#pertemuan_id").val('#').trigger('change');
				$("#operand").val('0').trigger('change');
				$("#st_tujuan_terakhir").val('0').trigger('change');
				$("#lama_terakhir_tujan").val('0');
				$("#st_formulir").val('0').trigger('change');
				$('#index_tampil').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});

});
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}setting_sga/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
$("#idtipe_default").change(function(){
		$.ajax({
			url: '{site_url}setting_sga/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli_default").empty();
				$("#idpoli_default").append(data);
			}
		});

});
function add_jawaban(parameter_id){
	$("#param_id").val(parameter_id);
	$("#modal_tambah").modal('show');
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_sga/find_param',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			// $("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			// $("#parameter_id").val(data.id);
			$("#parameter_add").html(data.parameter);
			// $('#parameter_add').summernote('code',data.parameter_nama);
			$("#cover-spin").hide();
			load_jawaban();
		}
	});
}
function hapus_jawaban(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Skor?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_sga/hapus_jawaban',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_jawaban').DataTable().ajax.reload( null, false ); 
					load_param();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function clear_jawaban(){
	$("#skor_id").val('');
	$('#deskripsi_nama').val('');
	$('#skor').val('');
	
	$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Save');
	
}
function edit_jawaban(skor_id){
	$("#skor_id").val(skor_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_sga/find_jawaban',
		type: 'POST',
		dataType: "JSON",
		data: {id: skor_id},
		success: function(data) {
			$("#modal_tambah").modal('show');
			$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Edit');
			$("#skor").val(data.skor).trigger('change.select2');
			$("#skor_id").val(data.id);
			$("#deskripsi_nama").val(data.deskripsi_nama);
			$("#cover-spin").hide();
			
		}
	});
}
$("#btn_tambah_jawaban").click(function() {
	let parameter_id=$("#param_id").val();
	let skor_id=$("#skor_id").val();
	let skor=$("#skor").val();
	let deskripsi_nama=$("#deskripsi_nama").val();
	
	if (skor==''){
		sweetAlert("Maaf...", "Tentukan Skor", "error");
		return false;
	}
	if (deskripsi_nama==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_sga/simpan_jawaban', 
		dataType: "JSON",
		method: "POST",
		data : {
				parameter_id:parameter_id,
				deskripsi_nama:deskripsi_nama,
				skor:skor,
				skor_id:skor_id,
				
			},
		complete: function(data) {
			clear_jawaban();
			load_param(); 
			$('#index_jawaban').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function load_jawaban(){
	let parameter_id=$("#param_id").val();
	$('#index_jawaban').DataTable().destroy();	
	table = $('#index_jawaban').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_sga/load_jawaban', 
                type: "POST" ,
                dataType: 'json',
				data : {
							parameter_id:parameter_id
					   }
            }
        });
}
</script>