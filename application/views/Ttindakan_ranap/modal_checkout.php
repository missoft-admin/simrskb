<div class="modal in" id="modal_checkout"  tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-c">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Checkout</h3>
                </div>
                <div class="block-content block-content-narrow">
					<div class="form-horizontal">
					<div class="row">
                    <div class="form-group remove-margin-t" hidden>
                        <label class="col-md-4 control-label" for="" >Tanggal Daftar</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <input type="text" id="tanggaldaftar" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" placeholder="Tanggal Keluar" value="">
                        </div>
                    </div>
					<input type="hidden" id="tempIdRawatInap" value="">
					<div class="form-group remove-margin-t">
                        <label class="col-md-4 control-label" for="" >Tanggal Keluar</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <input type="text" id="checkoutTanggalKeluar" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" placeholder="Tanggal Keluar" value="<?=date("d-m-Y")?>">
                        </div>
                    </div>
                    <div class="form-group remove-margin-t">
                        <label class="col-md-4 control-label" for="" >Tanggal Kontrol</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <input type="text" id="checkoutTanggalKontrol" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" placeholder="Tanggal Kontrol" value="<?=date("d-m-Y")?>">
                        </div>
                    </div>
                    <div class="form-group remove-margin-t">
                        <label class="col-md-4 control-label" for="" >Lama Dirawat</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <input type="text" id="checkoutLamaDirawat" class="form-control" placeholder="Lama Dirawat" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="" >No. Medrec</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <input type="text" id="checkoutNoMedrec" class="form-control" placeholder="No. Medrec" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="" >Nama Pasien</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <input type="text" id="checkoutNamapasien" class="form-control" placeholder="Nama Pasien" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="" >Sebab Luar</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <select id="checkoutSebabLuar" class="js-select2 form-control input-sm" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (get_all('msebab_luar',array('status'=>1)) as $row) { ?>
                                <option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="" >Keadaan Pasien Keluar</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <select id="checkoutKeadaanPasienKeluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="1" selected>Hidup</option>
                                <option value="2">Mati</option>
                            </select>
                        </div>
                    </div>
					
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="" >Kondisi Pasien Keluar</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <select id="checkoutKondisiPasienKeluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                                <option value="1">Sembuh</option>
                                <option value="2">Membaik</option>
                                <option value="3">Belum Sembuh</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label class="col-md-4 control-label" for="" >Kondisi Pasien Saat Ini</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <input type="text" id="checkoutKondisiPasienSaatIni" class="form-control" placeholder="Kondisi pasien saat ini" value="">
                        </div>
                    </div>
                    <div class="form-group " id="f_checkoutCaraPasienKeluar">
                        <label class="col-md-4 control-label" for="" >Cara Pasien Keluar</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <select id="checkoutCaraPasienKeluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                                <option value="1">Pulang</option>
                                <option value="2">Dirujuk</option>
                                <option value="3">Pindah ke RS Lain</option>
                                <option value="4">Pulang Paksa</option>
                                <option value="5">Lari</option>
                            </select>
                        </div>
                    </div>
					
                    <div class="form-group div_all" id="f_checkoutCaraPasienKeluarRsKlinik" >
                        <label class="col-md-4 control-label" for="" >Dirujuk Ke</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <select id="checkoutCaraPasienKeluarRsKlinik" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group div_all" id="f_checkoutCaraPasienKeluarAlasanBatal" >
                        <label class="col-md-4 control-label" for="" >Alasan Batal</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <select id="checkoutCaraPasienKeluarAlasanBatal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (get_all('malasan_batal',array('status'=>1)) as $row) { ?>
                                	<option value="<?=$row->id?>"><?=$row->keterangan?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group " id="f_checkoutCaraPasienKeluarKeterangan" >
                        <label class="col-md-4 control-label" for="" >Keterangan</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <textarea id="checkoutCaraPasienKeluarKeterangan" class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="" >Diagnosa</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <textarea id="checkoutDiagnosa" class="form-control" placeholder="Diagnosa"></textarea>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="" >Kelompok Diagnosa</label>
                        <div class="col-md-8" style="margin-bottom: -10px;">
                            <select id="checkoutKelompokDiagnosa" class="js-select2 form-control input-sm" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                <?php foreach (get_all('mkelompok_diagnosa',array('status'=>1)) as $row) { ?>
                                	<option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button id="saveCheckout" class="btn btn-sm btn-primary" type="button" style="width: 20%;">Checkout</button>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	function show_modal_checkout(idpendaftaran){
		
		$("#tempIdRawatInap").val(idpendaftaran);
		$("#modal_checkout").modal('show');
		set_select2_modal();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}ttindakan_ranap/get_data_pasien_checkout/',
			dataType: 'json',
			method: 'post',
			data:{
				pendaftaran_id:idpendaftaran
				
			},
			success: function(data) {
				$("#tanggaldaftar").val(data.tanggaldaftar);
				$("#cover-spin").hide();
				$("#checkoutNoMedrec").val(data.no_medrec);
				$("#checkoutNamapasien").val(data.namapasien);
				$("#checkoutTanggalKeluar").val(data.tanggal_keluar);
				$("#checkoutTanggalKontrol").val(data.tanggal_kontrol);
				let newOption = new Option(data.nama_rs, data.idrumahsakit, true, true);
					
				$("#checkoutCaraPasienKeluarRsKlinik").append(newOption).select2();

				// $("#checkoutCaraPasienKeluarRsKlinik").select2("trigger", "select", {
					// data: {
						// id: data.idrumahsakit,
						// text: data.nama_rs
					// }
				// });
				// $("#checkoutSebabLuar").val(data.sebabluar).trigger('change.select2');
				// $("#checkoutKeadaanPasienKeluar").val(data.keadaanpasienkeluar).trigger('change');
				// $("#checkoutKondisiPasienKeluar").val(data.keadaanpasienkeluar).trigger('change');
				// $("#checkoutKondisiPasienSaatIni").val(data.kondisipasiensaatini);
				$("#checkoutCaraPasienKeluar").val(data.carapasienkeluar).trigger('change');
				// $("#checkoutCaraPasienKeluarKeterangan").val(data.keterangancarapasienkeluar);
				// $("#checkoutDiagnosa").val(data.diagnosa);
				$("#checkoutCaraPasienKeluarAlasanBatal").val(data.idalasanbatal).trigger('change.select2');
				$("#checkoutKelompokDiagnosa").empty().append(data.opsi);
				get_lama_dirawat();
			}
		});
		// $.ajax({
			// url: '{site_url}trawatinap_tindakan/getDiagnosa/' + idpendaftaran,
			// dataType: 'json',
			// method: 'post',
			// success: function(data) {
				// $("#checkoutDiagnosa").val(data.diagnosa);
			// }
		// });
		$.ajax({
                url: '{site_url}trawatinap_tindakan/getDataCheckout/' + idpendaftaran,
                dataType: 'json',
                success: function(data) {
                    $("#checkoutSebabLuar").select2("trigger", "select", {
                        data: {
                            id: data.sebabluar,
                            text: data.sebabluar
                        }
                    });
                    $("#checkoutKeadaanPasienKeluar").select2("trigger", "select", {
                        data: {
                            id: data.keadaanpasienkeluar,
                            text: data.keadaanpasienkeluar
                        }
                    });
                    $("#checkoutKondisiPasienKeluar").select2("trigger", "select", {
                        data: {
                            id: data.kondisipasienkeluar,
                            text: data.kondisipasienkeluar
                        }
                    });
                    $("#checkoutKondisiPasienSaatIni").val(data.kondisipasiensaatini);
                    // $("#checkoutCaraPasienKeluar").select2("trigger", "select", {
                        // data: {
                            // id: data.carapasienkeluar,
                            // text: data.carapasienkeluar
                        // }
                    // });
                    
                    
                    $("#checkoutCaraPasienKeluarKeterangan").val(data.keterangancarapasienkeluar);
                    $("#checkoutDiagnosa").val(data.diagnosa);

                    if (data.keadaanpasienkeluar == 1) {
                        $("#f_checkoutKondisiPasienSaatIni").show();
                        $("#f_checkoutCaraPasienKeluar").show();
                    } else {
                        $("#f_checkoutKondisiPasienSaatIni").hide();
                        $("#f_checkoutCaraPasienKeluar").hide();
                    }

                    $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan Batal');
                    if (data.carapasienkeluar == '1' || data.carapasienkeluar == '5') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                    } else if (data.carapasienkeluar == '2') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                    } else if (data.carapasienkeluar == '3') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                        $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan');
                    } else if (data.carapasienkeluar == '4') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                    } else {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', true);
                    }
                }
            })
	}
	function set_checkout(idpendaftaran){
		$("#tempIdRawatInap").val(idpendaftaran);
		// alert(pendaftaran_id);
		$("#modal_checkout").modal('show');
		set_select2_modal();
		$("#checkoutCaraPasienKeluar").val("").trigger('change');
		$("#checkoutKeadaanPasienKeluar").val("1").trigger('change');
			$.ajax({
                url: '{site_url}trawatinap_tindakan/getDiagnosa/' + idpendaftaran,
                dataType: 'json',
                method: 'post',
                success: function(data) {
                    $("#checkoutDiagnosa").val(data.diagnosa);
                }
            });
			$.ajax({
                url: '{site_url}ttindakan_ranap/get_data_pasien/',
                dataType: 'json',
                method: 'post',
				data:{
					pendaftaran_id:idpendaftaran
					
				},
                success: function(data) {
                    $("#checkoutNoMedrec").val(data.no_medrec);
                    $("#checkoutNamapasien").val(data.namapasien);
                    $("#tanggaldaftar").val(data.tanggaldaftar);
					get_lama_dirawat();
                }
            });
			
	}
	
	$(document).on('change', '#checkoutTanggalKeluar', function() {
        get_lama_dirawat();
    });
	function get_lama_dirawat(){
		$.ajax({
			url: '{site_url}ttindakan_ranap/get_lama_dirawat/',
			dataType: 'json',
			method: 'post',
			data:{
				tanggaldaftar:$("#tanggaldaftar").val(),
				tanggalkeluar:$("#checkoutTanggalKeluar").val(),
				
			},
			success: function(data) {
				$("#checkoutLamaDirawat").val(data.lama);
			}
		});
	}
	$(document).on('click', '#saveCheckout', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var tanggalkeluar = $("#checkoutTanggalKeluar").val();
        var tanggalkontrol = $("#checkoutTanggalKontrol").val();
        var sebabluar = $("#checkoutSebabLuar").val();
        var kelompokDiagnosa = $("#checkoutKelompokDiagnosa").val();
        var keadaanpasienkeluar = $("#checkoutKeadaanPasienKeluar").val();
        var kondisipasienkeluar = $("#checkoutKondisiPasienKeluar").val();
        var checkoutKondisiPasienSaatIni = $("#checkoutKondisiPasienSaatIni").val();
        var carapasienkeluar = $("#checkoutCaraPasienKeluar").val();
        var diagnosa = $("#checkoutDiagnosa").val();
        var checkoutCaraPasienKeluarRsKlinik = $("#checkoutCaraPasienKeluarRsKlinik").val();
        var checkoutCaraPasienKeluarAlasanBatal = $("#checkoutCaraPasienKeluarAlasanBatal").val();
        var checkoutCaraPasienKeluarKeterangan = $("#checkoutCaraPasienKeluarKeterangan").val();

        if (tanggalkeluar == '') {
            sweetAlert("Maaf...", "Tanggal Keluar tidak boleh kosong !", "error").then((value) => {
                $("#checkoutTanggalKeluar").focus();
            });
            return false;
        }

        if (tanggalkontrol == '') {
            sweetAlert("Maaf...", "Tanggal Kontrol tidak boleh kosong !", "error").then((value) => {
                $("#checkoutTanggalKontrol").focus();
            });
            return false;
        }

        if (sebabluar == '') {
            sweetAlert("Maaf...", "Sebab Luar belum dipilih !", "error").then((value) => {
                $("#checkoutSebabLuar").select2('open');
            });
            return false;
        }

        if (kelompokDiagnosa == '') {
            sweetAlert("Maaf...", "Kelompok Diagnosa belum dipilih !", "error").then((value) => {
                $("#checkoutKelompokDiagnosa").select2('open');
            });
            return false;
        }

        if (keadaanpasienkeluar == '') {
            sweetAlert("Maaf...", "Keadaan Pasien Keluar belum dipilih !", "error").then((value) => {
                $("#checkoutKeadaanPasienKeluar").select2('open');
            });
            return false;
        }

        if (kondisipasienkeluar == '') {
            sweetAlert("Maaf...", "Kondisi Pasien Keluar belum dipilih !", "error").then((value) => {
                $("#checkoutKondisiPasienKeluar").select2('open');
            });
            return false;
        }

        if (carapasienkeluar == '') {
            sweetAlert("Maaf...", "Cara Pasien Keluar belum dipilih !", "error").then((value) => {
                $("#checkoutCaraPasienKeluar").select2('open');
            });
            return false;
        }

        if (diagnosa == '') {
            sweetAlert("Maaf...", "Diagnosa tidak boleh kosong !", "error").then((value) => {
                $("#checkoutDiagnosa").select2('open');
            });
            return false;
        }
		$("#cover-spin").show();
        $.ajax({
            url: "{site_url}ttindakan_ranap/saveCheckout",
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                tanggalkeluar: tanggalkeluar,
                tanggalkontrol: tanggalkontrol,
                sebabluar: sebabluar,
                kelompokdiagnosa: kelompokDiagnosa,
                keadaanpasienkeluar: keadaanpasienkeluar,
                kondisipasienkeluar: kondisipasienkeluar,
                checkoutKondisiPasienSaatIni: checkoutKondisiPasienSaatIni,
                carapasienkeluar: carapasienkeluar,
                diagnosa: diagnosa,
                checkoutCaraPasienKeluarRsKlinik: checkoutCaraPasienKeluarRsKlinik,
                checkoutCaraPasienKeluarAlasanBatal: checkoutCaraPasienKeluarAlasanBatal,
                checkoutCaraPasienKeluarKeterangan: checkoutCaraPasienKeluarKeterangan,
            },
            success: function(data) {
				$("#cover-spin").hide();
				$("#modal_checkout").modal('hide');
                swal({
                    title: "Berhasil!",
                    text: "Pasien Telah Checkout.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
				load_index_all();
                // location.reload();
            }
        });
    });
	function set_select2_modal(){
			$("#checkoutSebabLuar,#checkoutKelompokDiagnosa,#checkoutKeadaanPasienKeluar,#checkoutKondisiPasienKeluar,#checkoutCaraPasienKeluar,#checkoutCaraPasienKeluarAlasanBatal,#checkoutCaraPasienKeluarRsKlinik").select2({
				dropdownParent: $("#modal_checkout")
			});
	}
	$("#checkoutKeadaanPasienKeluar").change(function() {
        if ($(this).val() == 1) {
            // Kondisi Hidup
            $("#checkoutKondisiPasienKeluar").html('<option value="">Pilih Opsi</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="1">Sembuh</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="2">Membaik</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="3">Belum Sembuh</option>');

            $("#checkoutKondisiPasienKeluar").select2("destroy");
            $("#checkoutKondisiPasienKeluar").select2();

            // Show
            $("#f_checkoutKondisiPasienSaatIni").show();
            $("#f_checkoutCaraPasienKeluar").show();
        } else {
            // Kondisi Mati
            $("#checkoutKondisiPasienKeluar").html('<option value="">Pilih Opsi</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="4">< 48 Jam</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="5">>= 48 Jam</option>');

            $("#checkoutKondisiPasienKeluar").select2("destroy");
            $("#checkoutKondisiPasienKeluar").select2();

            // Hide
            $("#f_checkoutKondisiPasienSaatIni").hide();
            $("#f_checkoutCaraPasienKeluar").hide();
        }
    });
	$(document).on('change', '#checkoutCaraPasienKeluar', function() {
        var val = $(this).val();

        $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan Batal');

        if (val == '1' || val == '5') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
        } else if (val == '2') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);

            $("#checkoutCaraPasienKeluarRsKlinik").select2({
				minimumInputLength: 2,
				noResults: 'Data Tidak Ditemukan.',
				// allowClear: true
				// tags: [],
				ajax: {
					url: '{site_url}ttindakan_ranap/getRsKlinik/',
					dataType: 'json',
					type: "POST",
					quietMillis: 50,
				 data: function (params) {
					  var query = {
						search: params.term,
					  }
					  return query;
					},
					processResults: function (data) {
						return {
							results: $.map(data, function (item) {
								return {
									text: item.nama,
									id: item.id,
								}
							})
						};
					}
				},
				dropdownParent: $("#modal_checkout")
			});
        } else if (val == '3') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);

            $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan');

            // $('#checkoutCaraPasienKeluarRsKlinik').select2({
                // ajax: {
                    // url: '{site_url}trawatinap_tindakan/getRsKlinik',
                    // dataType: 'json',
                    // processResults: function(data) {
                        // var res = data.results.map(function(item) {
                            // return {
                                // id: item.id,
                                // text: item.nama
                            // }
                        // })
                        // return {
                            // results: res
                        // }
                    // },
                // }
				
            // })
			$("#checkoutCaraPasienKeluarRsKlinik").select2({
				minimumInputLength: 2,
				noResults: 'Data Tidak Ditemukan.',
				// allowClear: true
				// tags: [],
				ajax: {
					url: '{site_url}ttindakan_ranap/getRsKlinik/',
					dataType: 'json',
					type: "POST",
					quietMillis: 50,
				 data: function (params) {
					  var query = {
						search: params.term,
					  }
					  return query;
					},
					processResults: function (data) {
						return {
							results: $.map(data, function (item) {
								return {
									text: item.nama,
									id: item.id,
								}
							})
						};
					}
				},
				dropdownParent: $("#modal_checkout")
			});

			// set_select2_modal();
            
        } else if (val == '4') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
            
			set_select2_modal();
        } else {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', true);
        }
    });
</script>
