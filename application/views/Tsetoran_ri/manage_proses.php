<style>
	.form_sm {
		border-radius: 3px;
		background-color:#fdffe2;
		height:28px;
	}
</style>

<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tsetoran_ri/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tsetoran_ri" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">TRANSAKSI SETORAN KASIR</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<div class="row">
			<div class="col-md-2">
				<a class="block block-link-hover2 text-center" href="javascript:void(0)">
					<div class="block-content block-content-full bg-modern">
						<i class="fa fa-calendar fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">TANGGAL TRANSAKSI</div>
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<strong><?=tanggal_indo_DMY($tanggal_trx)?></strong>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a class="block block-link-hover2 text-center" href="javascript:void(0)">
					<div class="block-content block-content-full bg-success">
						<i class="fa fa-user-o fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">DIBUAT OLEH</div>
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<?=($user_buat?$user_buat:'AUTOMATIC SYSTEM')?> - <?=($user_buat?HumanDateLong($created_at):'')?>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a class="block block-link-hover2 text-center" onclick="show_modal_last(<?=$id?>)" href="javascript:void(0)">
					<div class="block-content block-content-full bg-warning">
						<i class="fa fa-pencil fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">LAST UPDATE</div>
						
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<span id="user_update"><?=($user_update?$user_update:'')?> - <?=($user_update?HumanDateLong($edited_date):'')?></span>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a class="block block-link-hover2 text-center" href="javascript:void(0)">
					<div class="block-content block-content-full bg-default">
						<i class="fa fa-star-o fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">HASIL</div>
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<span id="hasil"></span>
					</div>
				</a>
			</div>
			
				<input type="hidden" class="form-control" id="idtransaksi" placeholder="ID" value="{id}" required="" aria-required="true">
				<input type="hidden" readonly class="form-control" id="status_setoran" value="{status}" required="" aria-required="true">
				<input type="hidden" readonly class="form-control" id="hasil_pemeriksaan" value="{hasil_pemeriksaan}" required="" aria-required="true">
			
			<div class="col-md-12" >
				<div class="form-group">
					<div class="col-md-12">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 2%;">NO</th>
										<th style="width: 8%;">NAMA USER</th>
										<th style="width: 5%;">JUMLAH TRX</th>
										<th style="width: 7%;">TOTAL TRX</th>
										<th style="width: 7%;">TOTAL NON TUNAI</th>
										<th style="width: 7%;">TOTAL DEBIT</th>
										<th style="width: 7%;">TOTAL KREDIT</th>
										<th style="width: 7%;">TOTAL TRANSFER</th>
										<th style="width: 7%;">TOTAL KONTRAKTOR</th>
										<th style="width: 10%;">TOTAL TUNAI</th>
										<th style="width: 7%;">TOTAL PENGELUARAN</th>
										<th style="width: 9%;">FINAL TUNAI</th>
										<th style="width: 9%;">REAL CASH USER</th>
										<th style="width: 9%;">REAL CASH PEMERIKSA</th>
										<th style="width: 8%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
								
								
						</table>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 ">
					<div class="pull-right">
						<button class="btn btn-success push-5-r push-10" id="btn_selesai" type="button"><i class="fa fa-save"></i> SELESAI PENDAPATAN</button>
						<button class="btn btn-warning push-5-r push-10" id="btn_selesai_periksa" type="button"><i class="fa fa-save"></i> SELESAI PERIKSA</button>
					</div>
					</div>
				</div>
			</div>
			<?if ($status=='4'){?>
			<?if ($hasil_pemeriksaan!='0'){?>
			<div class="col-md-12">
				<!-- Static Labels block-opt-hidden -->
				<div class="block block-themed" >
					<div class="block-header bg-success">
						<ul class="block-options">
							<li>
								<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title"><i class="fa fa-money"></i>  PEMBAYARAN</h3>
					</div>
					<div class="block-content block-content">
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-2 control-label" for="nama">Nominal</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="nominal" placeholder="Selisih" name="nominal" value="(<?=($selisih<0?$selisih*-1:$selisih)?>)">
								</div>
								<label class="col-md-2 control-label" for="nama">Tanggal Setoran</label>
								<div class="col-md-2">
									<div class="input-group date">
									<input class="js-datepicker form-control input" <?=(UserAccesForm($user_acces_form,array('2602'))?'':'disabled')?> type="text" id="tanggal_setoran" name="tanggal_setoran" value="<?=($tanggal_setoran?HumanDateShort($tanggal_setoran) : HumanDateShort($tanggal_trx))?>" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-2 control-label" for="nama">JENIS <?=($hasil_pemeriksaan=='1'?'PENDAPATAN':'OTHER LOSS')?></label>
								<div class="col-md-8">
									 <select id="jenis_id" name="jenis_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($jenis_id=='0'?'selected':'')?>>-Silahkan Pilih-</option>
										<?foreach($list_jenis as $r){?>
										<option value="<?=$r->id?>" <?=($jenis_id==$r->id?'selected':'')?>><?=$r->nama_jenis?></option>
										<?}?>
													
									</select>
								</div>
								
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-2 control-label" for="nama">Keterangan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="keterangan" placeholder="Description" name="keterangan" value="{keterangan}">
								</div>
							</div>
							<? if ($st_verifikasi=='0'){ ?>
							<div class="form-group hiden btn_hide">
								<div class="text-right bg-light lter">
									<button class="btn btn-primary" type="button" onclick="simpan_verifikasi(0)" ><i class="fa fa-save"></i> Simpan</button>
								</div>
							</div>
							<?}?>
							
						</div> 
				</div>
				<!-- END Static Labels -->
			</div>
			<?}?>
			<?}?>
		</div>
		
	</div>
	<?php echo form_close() ?>
</div>
<div class="modal in" id="Modal_Rincian_trx" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title titleRincian">List Transaksi</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<table id="index_trx" class="table table-bordered table-striped" style="margin-top: 10px;">
						<thead>
							<tr>
								<th style="width:10%" class="text-center">Tanggal</th>
								<th style="width:10%" class="text-center">No Transaksi</th>
								<th style="width:10%" class="text-center">No Medrec</th>
								<th style="width:20%" class="text-center">Nama</th>
								<th style="width:20%" class="text-center">Tujuan</th>
								<th style="width:15%" class="text-center">Total Transaksi</th>
								<th style="width:15%" class="text-center">Cara Bayar</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						
					</table>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tsetoran_ri/modal_user')?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// st_show_rajal_tunai=0;
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		load_index();
		
	})
	function simpan_verifikasi(st_verifikasi){
		let ket='';
		let id=$("#idtransaksi").val();
		if ($("#jenis_id").val() == "0") {
			sweetAlert("Maaf...", "Jenis Belum Dipilih!", "error");
			return false;
		}
		if ($("#keterangan").val() == "") {
			sweetAlert("Maaf...", "Keterangan Belum Dipilih!", "error");
			return false;
		}
		if (st_verifikasi=='0'){
			ket=' Simpan Data';
		}else{
			
			ket=' Simpan & Verifikasi ';
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : ket,
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsetoran_ri/simpan_verifikasi/',
				dataType: "json",
				type: "POST",
				data : {
					id:id,
					st_verifikasi:st_verifikasi,
					idtransaksi:$("#idtransaksi").val(),
					tanggal_setoran:$("#tanggal_setoran").val(),
					jenis_id:$("#jenis_id").val(),
					keterangan:$("#keterangan").val(),
					nominal:$("#nominal").val(),
					hasil_pemeriksaan:$("#hasil_pemeriksaan").val(),
				   },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Setoran Selesai'});
					location.reload();
				}
			});
		});
	}
	function load_index(){
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tsetoran_ri/getIndex_RJ/',
			dataType: "json",
			type: "POST",
			data : {
					id:$("#idtransaksi").val(),
				   },
			success: function(data) {
				$("#tabel_rajal tbody").empty();
				$("#tabel_rajal tbody").append(data.tabel);
				$("#hasil").html(data.hasil);
				$(".decimal").number(true,2,'.',',');
					$('.number').number(true, 0, '.', ',');
					$("#cover-spin").hide();
				if ($("#status_setoran").val()=='1'){
					$("#btn_selesai").show();
					if (data.status_user_terkecil=='2'){
						$("#btn_selesai").removeAttr("disabled");
					}else{
						$("#btn_selesai").prop("disabled", true);
					}
				}else{
					$("#btn_selesai").hide();
					
				}
				if ($("#status_setoran").val()=='3'){
					$("#btn_selesai_periksa").show();
					if (data.status_pemeriksa_terkecil=='3'){
						$("#btn_selesai_periksa").removeAttr("disabled");
					}else{
						$("#btn_selesai_periksa").prop("disabled", true);
					}
				}else{
					$("#btn_selesai_periksa").hide();
					
				}
				$("#user_update").text(data.user_update);
			}
		});
	
	}
	function getIndex_detail_trx(tanggal,user_id){
		$("#Modal_Rincian_trx").modal('show');
		$('#index_trx').DataTable().destroy();
		table=$('#index_trx').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_ri/getIndex_detail_trx',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal:tanggal,
						user_id:user_id,
					   }
				},
				"columnDefs": [
					
					{  className: "text-center", targets:[0,1] },
					{  className: "text-right", targets:[5] },
				]
			});
	}
	$(".auto_blur_racikan").blur(function(){
		// alert('sini');
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_heaad_racikan();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
		
	});
	$(document).on("click",".btn_selesai_input",function(){
		let tr=$(this).closest('tr');
		let id=tr.find(".id_detail").val();
		let real_cash_user=tr.find(".real_cash_user").val();
		let final_total=tr.find(".final_total").val();
		console.log('Real :'+real_cash_user+' Total :'+final_total);
		let ket='';
		if (real_cash_user > final_total){
			ket ='Ada Kelebihan ?';
		}
		
		if (real_cash_user < final_total){
			ket ='Ada Kekurangan ?';
		}
		if (real_cash_user == final_total){
			ket ='Uang Sesuai ?';
		}
		swal({
			title: "Apakah Anda Yakin Simpan Data?",
			text : ket,
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsetoran_ri/update_real_user/',
				dataType: "json",
				type: "POST",
				data : {
					id:id,
					real_cash_user:real_cash_user,
					idtransaksi:$("#idtransaksi").val(),
				   },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Setoran Selesai'});
					load_index();
				}
			});
		});
		console.log(id);
	});
	$(document).on("click",".btn_selesai_input_periksa",function(){
		let tr=$(this).closest('tr');
		let id=tr.find(".id_detail").val();
		let real_cash_pemeriksa=tr.find(".real_cash_pemeriksa").val();
		let final_total=tr.find(".final_total").val();
		console.log('Real :'+real_cash_pemeriksa+' Total :'+final_total);
		let ket='';
		if (real_cash_pemeriksa > final_total){
			ket ='Ada Kelebihan ?';
		}
		
		if (real_cash_pemeriksa < final_total){
			ket ='Ada Kekurangan ?';
		}
		if (real_cash_pemeriksa == final_total){
			ket ='Uang Sesuai ?';
		}
		swal({
			title: "Apakah Anda Yakin Simpan Data?",
			text : ket,
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsetoran_ri/update_real_pemeriksa/',
				dataType: "json",
				type: "POST",
				data : {
					id:id,
					real_cash_pemeriksa:real_cash_pemeriksa,
					idtransaksi:$("#idtransaksi").val(),
				   },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Setoran Selesai'});
					load_index();
				}
			});
		});
		console.log(id);
	});
	$(document).on("click","#btn_selesai",function(){
		let id=$("#idtransaksi").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Selesai Transaksi",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsetoran_ri/update_selesai/',
				dataType: "json",
				type: "POST",
				data : {
					id:id,
				   },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai'});
					window.location.href = "<?php echo site_url('tsetoran_ri'); ?>";
				}
			});
		});
		console.log(id);
	});
	$(document).on("click","#btn_selesai_periksa",function(){
		let id=$("#idtransaksi").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Selesai Transaksi Periksa",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsetoran_ri/update_selesai_periksa/',
				dataType: "json",
				type: "POST",
				data : {
					id:id,
				   },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai'});
					window.location.href = "<?php echo site_url('tsetoran_ri'); ?>";
				}
			});
		});
		console.log(id);
	});
	
	function batalkan_selesai(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Setoran",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsetoran_ri/batalkan_selesai/',
				dataType: "json",
				type: "POST",
				data : {
					id:id,
					idtransaksi:$("#idtransaksi").val(),
				   },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Batal Selesai'});
					load_index();
				}
			});
		});
	}
	function batalkan_selesai_periksa(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Setoran",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsetoran_ri/batalkan_selesai_periksa/',
				dataType: "json",
				type: "POST",
				data : {
					id:id,
					idtransaksi:$("#idtransaksi").val(),
				   },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Batal Selesai'});
					load_index();
				}
			});
		});
	}
</script>