<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('2593'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('2594'))){ ?>
		<ul class="block-options">       
            <a href="{base_url}tsetoran_ri/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> TAMBAH TRANSAKSI</a>       
		</ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="notransaksi" placeholder="No Transaksi" name="notransaksi" value="">
                    </div>
                </div>
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                       <select id="status" name="status" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua -</option>
							<option value="1" >SUDAH VERIFIKASI</option>
							<option value="2" >BELUM DIVERIFIKASI</option>							
							<option value="3" >DIHAPUS</option>							
						</select>
                    </div>
                </div>
				
            </div>
			<div class="col-md-6">
				
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Setoran</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_setoran1" name="tanggal_setoran1" placeholder="From" value="{tanggal_setoran1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_setoran2" name="tanggal_setoran2" placeholder="To" value="{tanggal_setoran2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>No</th>					
					<th>Tanggal Pendapatan</th>
					<th>Total Transaksi</th>
					<th>Total Tunai</th>
					<th>Real Cash User</th>
					<th>Real Cash Pemeriksa</th>
					<th>Status</th>
					<th>Hasil Pemeriksa</th>
					<th>Status Pemeriksaan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>


<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	function verif(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Verifikasi Setoran Kas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tsetoran_ri/verif/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Setoran diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
	}
	// $(document).on("click",".verif",function(){
		
		// var table = $('#table_index').DataTable()
		// var tr = $(this).parents('tr')
		// var id = table.cell(tr,0).data()
		// // alert(id); return false;
		
		
		// return false;
	// });
	$(document).on("click",".hapus",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Hapus Setoran?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tsetoran_ri/hapus/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Setoran diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	function load_index(){
		var notransaksi=$("#notransaksi").val();
		var status=$("#status").val();		
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var tanggal_setoran1=$("#tanggal_setoran1").val();
		var tanggal_setoran2=$("#tanggal_setoran2").val();
		// alert(tanggal_setoran1);
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_ri/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						notransaksi:notransaksi,
						status:status,
						tanggal_trx1:tanggal_trx1,
						tanggal_trx2:tanggal_trx2,
						tanggal_setoran1:tanggal_setoran1,
						tanggal_setoran2:tanggal_setoran2,
						
					   }
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "visible": true },
					{ "width": "10%", "targets": 1, "visible": true },
					{ "width": "10%", "targets": 2, "visible": true },
					{ "width": "10%", "targets": 3, "visible": true },
					{ "width": "10%", "targets": 4, "visible": true },
					{ "width": "10%", "targets": 5, "visible": true },
					{ "width": "10%", "targets": 6, "visible": true },
					{ "width": "10%", "targets": 7, "visible": true },
					{ "width": "10%", "targets": 8, "visible": true },
					{ "width": "15%", "targets": 9, "visible": true },
					{  className: "text-right", targets:[2, 3,4,5] },
					{  className: "text-center", targets:[6,1,7,8,9] },
				]
			});
	}
	function update_periksa(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mulai Transaksi Periksa",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			
			$.ajax({
				url: '{site_url}tsetoran_ri/update_periksa/',
				dataType: "json",
				type: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tsetoran_ri/update/')?>"+id;
				}
			});
		});
		
	}
</script>
