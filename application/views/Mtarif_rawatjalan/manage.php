<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
      <li>
          <a href="{base_url}mtarif_rawatjalan/index/1" class="btn"><i class="fa fa-reply"></i></a>
      </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_rawatjalan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="headerpath">Parent</label>
				<div class="col-md-7">
					<select name="headerpath" id="headerpath" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="" selected>Pilih Opsi</option>
						<?=$list_parent?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idkelompok">Jenis</label>
				<div class="col-md-7">
					<label class="radio-inline" for="kelompok">
              <input type="radio" id="kelompok" checked name="idkelompok" onclick="toggleJenis('kelompok')" <?=($idkelompok == 1 ? 'checked':'')?> value="1"> Kelompok
          </label>
          <label class="radio-inline" for="rincian">
              <input type="radio" id="rincian" name="idkelompok" onclick="toggleJenis('rincian')" <?=($idkelompok == 0 ? 'checked':'')?> value="0"> Rincian
          </label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div id="form-detail" style="display:none">
				<div class="form-group">
					<label class="col-md-3 control-label" for="jasasarana">Jasa Sarana</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="jasasarana" onkeyup="sumTotal()" placeholder="Jasa Sarana" name="jasasarana" value="{jasasarana}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="jasapelayanan">Jasa Pelayanan</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="jasapelayanan" onkeyup="sumTotal()" placeholder="Jasa Pelayanan" name="jasapelayanan" value="{jasapelayanan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="bhp">BHP</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="bhp" onkeyup="sumTotal()" placeholder="BHP" name="bhp" value="{bhp}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="biayaperawatan">Biaya Perawatan</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="biayaperawatan" onkeyup="sumTotal()" placeholder="Biaya Perawatan" name="biayaperawatan" value="{biayaperawatan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="total">Total Tarif</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="total" placeholder="Total Tarif" name="total" value="{total}" readonly>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_rawatjalan/index/1" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<input type="hidden" id="old_headerpath" name="old_headerpath" value="{headerpath}" readonly>
			<input type="hidden" id="path" name="path" value="{path}" readonly>
			<input type="hidden" id="level" name="level" value="{level}" readonly>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	var idkelompok = <?=$idkelompok?>;

  $(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		if(idkelompok == 1){
			$("#form-detail").hide();
		}else{
			$("#form-detail").show();
		}

		$(document).on("change","#headerpath",function(){
			$.ajax({
				url: '{base_url}mtarif_rawatjalan/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					if($('#headerpath option:selected').text() === 'Root'){
						$("#path").val($('#headerpath option:selected').val());
					}else{
						$("#path").val(data.path);
					}
					$("#level").val(data.level);
				}
			});
		});
	});

	function toggleJenis(label) {
		if(label == 'kelompok'){
			$("#form-detail").hide();
		}else{
			$("#form-detail").show();
		}
	}

	function sumTotal() {
		var jasasarana 			= ($("#jasasarana").val() ? parseFloat($("#jasasarana").val().replace(/,/g, '')) : 0);
		var jasapelayanan 	= ($("#jasapelayanan").val() ? parseFloat($("#jasapelayanan").val().replace(/,/g, '')) : 0);
		var bhp 						= ($("#bhp").val() ? parseFloat($("#bhp").val().replace(/,/g, '')) : 0);
		var biayaperawatan 	= ($("#biayaperawatan").val() ? parseFloat($("#biayaperawatan").val().replace(/,/g, '')) : 0);

		var totaltarif = jasasarana + jasapelayanan + bhp + biayaperawatan;
		$("#total").val(totaltarif.toLocaleString(undefined, {
			minimumFractionDigits: 0
		}));
	}
</script>
