<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mvariable" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mvariable/save','class="form-horizontal push-10-t" id="form-work" onsubmit="return validate_final()"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Urutan</label>
			<div class="col-md-7">
				<input type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut" value="{nourut}">
				<input type="hidden" class="form-control" id="id" placeholder="" name="id" value="{id}">
				<input type="hidden" class="form-control" id="tedit" placeholder="" name="tedit" value="0">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idtipe">Tipe</label>
			<div class="col-md-7">
				<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Pendapatan</option>
					<option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>Potongan</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idsub">Status</label>
			<div class="col-md-7">
				<select name="idsub" id="idsub" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="0" <?=($idsub == 0 ? 'selected' : '')?>>Non Sub</option>
					<option value="1" <?=($idsub == 1 ? 'selected' : '')?>>Sub</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row" style="margin-top:-25px">
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mvariable" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<div class="block-content">
		<div class="block">

			<div class="block-content" id="groupSub" style="<?=($idsub == 0 ? 'display: none;' :'')?> border-radius: 2px; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
				<h6>Sub Data</h6>
				<hr>
				<table class="table table-bordered table-striped" id="subDataList">
					<thead>
						<tr>
							<th style="display:none"></th>
							<th width="20%">No Urut</th>
							<th width="50%">Nama</th>
							<th width="30%">Aksi</th>
						</tr>
						<tr>
							<th>
								<input type="text" class="form-control number" id="xnourut" placeholder="No Urut" value="">
							</th>
							<th>
								<input type="text" class="form-control" id="namasub" placeholder="Nama Sub" value="">
							</th>
							<th>
								<button id="addSub" class="btn btn-sm btn-success" type="button"><i class="fa fa-save"></i> Simpan</button>
								<button id="btn_clear" class="btn btn-sm btn-danger" type="button"><i class="fa fa-refresh"></i></button>
							</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>

		</div>
	</div>

	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');
			clear();
			get_list_variable();
		});
		
		$(document).on("change", "#idtipe", function() {
			if($(this).val() == 1){
				$("#groupPendapatan").hide();
			}else{
				$("#groupPendapatan").show();
			}
		});

		// $(document).on("change", "#idsub", function() {
			// if($(this).val() == 0){
				// $("#groupSub").hide();
			// }else{
				// $("#groupSub").show();
			// }
		// });

		// Sub Content Group
		$(document).on("click", "#addSub", function() {
			if (validate_detail()==false)return false;
			simpan();
			
		});
		function simpan(){
			var tedit=$("#tedit").val();		
			var idheader=$("#id").val();		
			var idtipe=$("#idtipe").val();		
			var nourut=$("#xnourut").val();		
			var nama=$("#namasub").val();
		// alert('sini');
			if (tedit =='0'){
				// alert('AD');
				$.ajax({
					url: '{site_url}mvariable/simpan_add',
					type: 'POST',
					data: {
						idheader: idheader,idtipe: idtipe,
						nourut: nourut,nama: nama,		
					},
					complete: function() {
						get_list_variable();
						swal({
							title: "Berhasil!",
							text: "Proses penyimpanan data.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
					}
				});
			}else{
				$.ajax({
					url: '{site_url}mvariable/simpan_edit',
					type: 'POST',
					data: {
						idheader: idheader,idtipe: idtipe,
						nourut: nourut,nama: nama,tedit: tedit		
					},
					complete: function() {
						get_list_variable();
						swal({
							title: "Berhasil!",
							text: "Proses penyimpanan data.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
					}
				});
			}
			$('#tedit').val(0);	
		}
		function get_list_variable(){

			var id=$("#id").val();
			$.ajax({
				url: '{site_url}mvariable/get_list_variable/' + id,
				method: 'GET',
				success: function(data) {
					$("#subDataList tbody").html(data);
				}
			});
			clear();
		}
		function validate_detail()
		{
			if ($("#xnourut").val()=='' || $("#xnourut").val()=='0'){
				sweetAlert("Maaf...", "No Urut Harus diisi", "error");
				return false;
			}
			
			if ($("#namasub").val()==''){
				sweetAlert("Maaf...", " Nama Variaable Harus diisi", "error");
				return false;
			}
			
			return true;
		}
		function validate_final()
		{
			if ($("#nourut").val()=='' || $("#nourut").val()=='0'){
				sweetAlert("Maaf...", "No Urut Harus diisi", "error");
				return false;
			}
			
			if ($("#nama").val()==''){
				sweetAlert("Maaf...", " Nama Variaable Harus diisi", "error");
				return false;
			}
			
			return true;
		}
		function clear(){
			$("#tedit").val(0);
			$("#xnourut").val('');
			$("#namasub").val('');
			// $("#btn_clear").attr('hide',true);
			
		}
		$(document).on("click", "#btn_clear", function() {
			clear();
		});
		$(document).on("click", ".editSub", function() {
			$("#tedit").val($(this).closest('tr').find("td:eq(0)").html());
			$("#namasub").val($(this).closest('tr').find("td:eq(2)").html());
			$("#xnourut").val($(this).closest('tr').find("td:eq(1)").html());

			return false;
		});

		$(document).on("click", ".hapus", function() {
			var id=$(this).closest('tr').find("td:eq(0)").html();
			$("#tedit").val($(this).closest('tr').find("td:eq(0)").html());
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menghapus Sub Variaable ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}mvariable/hapus',
					type: 'POST',
					data: {
						id: id
					},
					complete: function() {
						get_list_variable();
						swal({
							title: "Berhasil!",
							text: "Proses Penghapusan data.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
					}
				});
			});


			return false;
		});

</script>
