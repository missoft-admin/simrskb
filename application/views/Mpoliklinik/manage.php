<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpoliklinik/index/1" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mpoliklinik/save','class="form-horizontal push-10-t" id="form-work"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Poliklinik</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" <?=($statuslock ? 'readonly' : '')?> required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Tipe</label>
				<div class="col-md-7">
					<?php if($statuslock){ ?>
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" disabled data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($idtipe == 1 ? 'selected="selected"':'')?>>Poliklinik</option>
							<option value="2" <?=($idtipe == 2 ? 'selected="selected"':'')?>>Instalasi Gawat Darurat</option>
						</select>
						<input type="hidden" name="idtipe" value="{idtipe}">
					<?php }else{ ?>
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($idtipe == 1 ? 'selected="selected"':'')?>>Poliklinik</option>
							<option value="2" <?=($idtipe == 2 ? 'selected="selected"':'')?>>Instalasi Gawat Darurat</option>
						</select>
					<?php } ?>
				</div>
			</div>
	</div>
	<div class="block-content">
			<input type="hidden" class="form-control" id="rowindex"/>
			<input type="hidden" class="form-control" id="number"/>
			<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Dokter</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<select class="js-select2 form-control" id="iddokter" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach ($list_dokter as $row){ ?>
									<option value="<? echo $row->id ?>"><?php echo $row->nama; ?></option>
								<?php } ?>
							</select>
						</td>
						<td>
							<a id="detail_add" class="btn btn-success">Tambahkan</a>
						</td>
					</tr>
				</tbody>
			</table>
			</div>
			<div class="table-responsive">
			<table class="table table-bordered table-striped" id="detail_list">
				<thead>
					<tr>
						<th>No</th>
						<th>Dokter</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($this->uri->segment(2) == 'update') { ?>
						<?php $number = 0; ?>
						<?php foreach  ($list_detail as $row) { ?>
						<?php $number = $number + 1; ?>
						<tr>
							<td><?=$number;?></td>
							<td style="display:none;"><?=$row->iddokter;?></td>
							<td><?=$row->namadokter;?></td>
							<td><a href="#" class="detail_edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href='#' class='detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></a></td>
						</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
				</div>
			<div class="row">
				<div class="col-sm-3">
				</div>
				<div class="col-sm-9">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpoliklinik" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<input type="hidden" id="statuslock" name="statuslock" value="{statuslock}">
			<input type="hidden" id="detail_value" name="detail_value">
			<br><br>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
		$("#detail_add").click(function() {
      var valid = validate_detail();
			if(!valid) return false;

			var row_index;
			var number;
			var duplicate = false;

			if($("#number").val() != ''){
				var content = "";
				number = $("#number").val();
				row_index = $("#rowindex").val();
			}else{
				var content = "<tr>";
				number = $('#detail_list tr').length - 1;
				$('#detail_list tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(1).text() === $("#iddokter").val()){
						sweetAlert("Maaf...", "Dokter " + $("#iddokter option:selected").text() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}else{
						no = $("#number").val();
					}
				});
			}

			if(duplicate == false){
				number = $('#detail_list tr').length;
				content += "<td width='3%'>" + number + "</td>";
				content += "<td width='10%' style='display:none;'>" + $("#iddokter option:selected").val(); + "</td>";
				content += "<td width='10%'>" + $("#iddokter option:selected").text(); + "</td>";
				content += "<td width='10%'><a href='#' class='detail_edit' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></a></td>";

				if($("#rowindex").val() != ''){
					$('#detail_list tbody tr:eq(' + row_index + ')').html(content);
				}else{
					content += "</tr>";
					$('#detail_list tbody').append(content);
				}

				detail_clear();
			}
		});

		$(document).on("click", ".detail_edit", function() {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#number").val($(this).closest('tr').find("td:eq(0)").html());
			$("#iddokter").val($(this).closest('tr').find("td:eq(1)").html()).trigger("change");

			return false;
		});

		$(document).on("click", ".detail_remove", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});

			$("#detail_value").val(JSON.stringify(detail_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});

	});

	function detail_clear() {
		$("#rowindex").val('');
		$("#number").val('');
    $("#iddokter").select2("trigger", "select", {data : {id: '',text: ''}});
		$(".detail").val('');
	}

	function validate_detail() {
		if ($("#iddokter").val() == "") {
			sweetAlert("Maaf...", "Dokter Belum Dipilih!", "error").then((value) => {
				$("#iddokter").select2('open');
			});
			return false;
		}

		return true;
	}
</script>
