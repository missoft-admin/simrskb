<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_assesmen_pulang'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_assesmen_pulang' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_assesmen_pulang=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >							
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
								<h5 class="font-w400 push-5 text-center"><strong><i>{judul_header_eng}</i></strong></h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="pull-right push-10-r">
								<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_gizi_anak" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="alasan">Alasan Pulang</label>
									<select id="alasan" class=" form-control <?=($alasan!=$alasan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($alasan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(317) as $row){?>
										<option value="<?=$row->id?>" <?=($alasan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($alasan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-8 ">
									<label for="mobilisasi">Mobilisasi Saat Pulang Alat Bantu</label>
									<select id="mobilisasi" class=" form-control <?=($mobilisasi!=$mobilisasi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($mobilisasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(318) as $row){?>
										<option value="<?=$row->id?>" <?=($mobilisasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($mobilisasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="mobilisasi_lain">Mobilisasi Lainnya</label>
									<input id="mobilisasi_lain" class="form-control <?=($mobilisasi_lain!=$mobilisasi_lain_asal?'edited':'')?>" type="text"  value="{mobilisasi_lain}" name="mobilisasi_lain" placeholder="Mobilisasi Lainnya" >
								</div>
								<div class="col-md-6 ">
									<label for="alkes">Alat Kesehatan Yang Terpasang</label>
									<select id="alkes" class=" form-control <?=($alkes!=$alkes_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($alkes == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(319) as $row){?>
										<option value="<?=$row->id?>" <?=($alkes == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($alkes == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="perawatan">Perawatan Khusus Dirumah</label>
									<select id="perawatan" class=" form-control <?=($perawatan!=$perawatan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($perawatan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(320) as $row){?>
										<option value="<?=$row->id?>" <?=($perawatan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($perawatan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-8 ">
									<label for="perawatan_lain">Perawatan Khusus Dirumah Lainnya</label>
									<input id="perawatan_lain" class="form-control <?=($perawatan_lain!=$perawatan_lain_asal?'edited':'')?>" type="text"  value="{perawatan_lain}" name="perawatan_lain" placeholder="Perawatan Khusus Dirumah Lainnya" >
								</div>
								
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Keadaan Saat Pulang </h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="jam_tv">Tanda Vital Jam</label>
									<div class="input-group">
										<input  type="text"  class="time-datepicker form-control <?=($jam_tv!=$jam_tv_asal?'edited':'')?>" id="jam_tv" name="jam_tv" value="<?= $jam_tv ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-8 ">
									<label for="tingkat_kesadaran">Tingkat Kesadaran</label>
									<select id="tingkat_kesadaran"   name="tingkat_kesadaran" class="<?=($tingkat_kesadaran!=$tingkat_kesadaran_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4 push-10-t">
									<label for="nadi">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nadi!=$nadi_asal?'edited':'')?>"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-4 push-10-t">
									<label for="nafas">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nafas!=$nafas_asal?'edited':'')?>"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-4 push-10-t">
									<label for="spo2">SpO2</label>
									<div class="input-group">
										<input class="form-control decimal <?=($spo2!=$spo2_asal?'edited':'')?>"   type="text" id="spo2" name="spo2" value="{spo2}"  placeholder="Frekuensi spo2" required>
										<span class="input-group-addon"><?=$satuan_spo2?></span>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal <?=($td_sistole!=$td_sistole_asal?'edited':'')?>"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal <?=($td_diastole!=$td_diastole_asal?'edited':'')?>"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal <?=($suhu!=$suhu_asal?'edited':'')?>"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Tinggi Badan</label>
									<div class="input-group">
										<input class="form-control decimal <?=($tinggi_badan!=$tinggi_badan_asal?'edited':'')?>"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
										<span class="input-group-addon">Cm</span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Berat Badan</label>
									<div class="input-group">
										<input class="form-control decimal <?=($berat_badan!=$berat_badan_asal?'edited':'')?>"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >DIAGNOSA KEPERAWATAN</h5>
							</div>
							</div>
							<div class="col-md-6 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >RENCANA ASUHAN KEPERAWATAN</h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<?if($status_assemen !='2'){?>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="index_header">
											<thead>
												<tr>
													<th width="55%">
														Diagnosa
													</th>
													<th width="25%">
														Priority
													</th>
													<th width="20%">
														
													</th>
												</tr>
												<tr>
													<th width="55%">
														<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
														<select id="mdiagnosa_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
															<?}?>
														</select>
													</th>
													<th width="25%">
														<select id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															
															<?for($i=1;$i<20;$i++){?>
															<option value="<?=$i?>"><?=$i?></option>
															<?}?>
														</select>
													</th>
													<th width="20%">
														<span class="input-group-btn">
															<button class="btn btn-info" onclick="simpan_diagnosa_pulang()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
															<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<?}?>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Diagnosa Keperawatan</th>
													<th width="10%">Priority</th>
													<th width="25%">User</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa</label>
									<select id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
								<div class="col-md-12 push-10-t">
									<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
										<thead></thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="perawatan">Tindakan Keperawatan  Selamat Dirarawat</label>
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_tindakan">
											<thead>
												<?if($status_assemen !='2'){?>
												<tr>
													<th width="70%" colspan="2">
														<input id="trx_nama" class="form-control" type="text"  value="" placeholder="Nama Tindakan" >
													</th>
													<th width="30%">
														<div class="input-group date">
															<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="trx_tanggal" placeholder="HH/BB/TTTT" value="<?=HumanDateShort(date('Y-m-d'))?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
													</th>
													<th width="10%">
														<button class="btn btn-info" onclick="simpan_tindakan()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
													</th>
												</tr>
												<?}?>
												<tr>
													<th width="5%">No</th>
													<th width="55%">Nama Tindakan</th>
													<th width="30%">Tanggal</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="obat">Manajemen Nyeri Obat Yang Diminum /<br> Anti Nyeri</label>
									<input id="obat" class="form-control <?=($obat!=$obat_asal?'edited':'')?>" type="text"  value="{obat}" name="obat" placeholder="" >
								</div>
								<div class="col-md-6 ">
									<label for="efek">Efek Samping Mungkin Timbul<br>&nbsp;</label>
									<input id="efek" class="form-control <?=($efek!=$efek_asal?'edited':'')?>" type="text"  value="{efek}" name="efek" placeholder="" >
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="nyeri">Bila Nyeri bertambah berat segera ke RS</label>
									<input id="nyeri" class="form-control <?=($nyeri!=$nyeri_asal?'edited':'')?>" type="text"  value="{nyeri}" name="nyeri" placeholder="" >
								</div>
								<div class="col-md-6 ">
									<label for="efek">Batasan cairan</label>
									<select id="batasan_cairan" class="js-select2 form-control <?=($batasan_cairan!=$batasan_cairan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($batasan_cairan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(321) as $row){?>
										<option value="<?=$row->id?>" <?=($batasan_cairan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($batasan_cairan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="evaluasi">Evaluasi</label>
									<input id="evaluasi" class="form-control <?=($evaluasi!=$evaluasi_asal?'edited':'')?>" type="text"  value="{evaluasi}" name="evaluasi" placeholder="Evaluasi" >
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-bottom:-10px">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >EDUKASI </h5>
							</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
											<thead>
												<tr>
													<th width="5%" class="text-center">NO</th>
													<th width="45%" class="text-center">JENIS EDUKASI</th>
													<?
													$q="SELECT H.jenis as id,M.ref as nama 
													FROM `tranap_assesmen_pulang_pengkajian` H 
													INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='325'
													WHERE H.assesmen_id='$assesmen_id'
													GROUP BY H.jenis
													ORDER BY H.jenis";
													$list_header=$this->db->query($q)->result();
													$jml_kolom=count($list_header);
													$lebar_kolom=50 / $jml_kolom;
													foreach($list_header as $r){
													?>
													<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
													
													<?}?>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:-10px">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Hasil Pemeriksaan Yang Diserahkan </h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_hasil">
											<thead>
											<?if ($status_assemen!='2'){?>
												<tr>
													<th colspan="2">
														<select id="trx_jenis_pemeriksaan" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="0" selected>-Pilih-</option>
															<?foreach(list_variable_ref(322) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th >
														<input id="trx_hasil" class="form-control" type="text"  value="" placeholder="Hasil Pemeriksaan" >
													</th>
													<th >
														<div class="input-group date">
															<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="trx_tanggal_hasil" placeholder="HH/BB/TTTT" value="<?=HumanDateShort(date('Y-m-d'))?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
													</th>
													<th >
														<input id="trx_jumlah_lembar" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text"  value="" placeholder="Jumlah Lembar" >
													</th>
													<th width="10%">
														<button class="btn btn-info" onclick="simpan_hasil()" id="btn_add_hasil" type="button"><i class="fa fa-plus"></i> Add</button>
													</th>
												</tr>
											<?}?>
												<tr>
													<th width="5%">No</th>
													<th width="20%">Jenis Pemeriksaan</th>
													<th width="30%">Hasil Pemeriksaan</th>
													<th width="15%">Tanggal</th>
													<th width="15%">Jumlah Lembar</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-bottom:-10px">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Rencana Kontrol </h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_kontrol">
											<thead>
												<?if($status_assemen !='2'){?>
												<tr>
													<th colspan="2">
														<div class="input-group date">
															<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="trx_tanggal_kontrol" placeholder="HH/BB/TTTT" value="<?=HumanDateShort(date('Y-m-d'))?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
														
													</th>
													<th >
														<select id="trx_poli" class="js-select2 form-control " style="width: 100%;" data-placeholder="Poliklinik" required>
															<option value="0" selected>-Pilih Poliklinik-</option>
															<?foreach(get_all('mpoliklinik',array('idtipe'=>1,'status'=>1)) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th >
														<select id="trx_iddokter" class="js-select2 form-control " style="width: 100%;" data-placeholder="Dokter" required>
															
														</select>
													</th>
													
													<th >
														<select id="trx_jam" class="js-select2 form-control " style="width: 100%;" data-placeholder="Jam Praktek" required>
															
														</select>
													</th>
													<th >
														<input id="trx_keterangan" class="form-control" type="text"  value="" placeholder="Keterangan" >
													</th>
													<th width="10%">
														<button class="btn btn-info" onclick="simpan_kontrol()" id="btn_add_kontrol" type="button"><i class="fa fa-plus"></i> Add</button>
													</th>
												</tr>
												<?}?>
												<tr>
													<th width="5%">No</th>
													<th width="15%">Tanggal Kontrol</th>
													<th width="20%">Poliklinik</th>
													<th width="25%">Dokter</th>
													<th width="15%">Jam Prakter</th>
													<th width="25%">Keterangan</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="skr">Surat Keterangan Rawat</label>
									<select id="skr" class="<?=($skr!=$skr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($skr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(323) as $row){?>
										<option value="<?=$row->id?>" <?=($skr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($skr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-6 ">
									<label for="asuransi">Asuransi</label>
									<select id="asuransi" class="<?=($asuransi!=$asuransi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($asuransi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(324) as $row){?>
										<option value="<?=$row->id?>" <?=($asuransi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($asuransi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<table width="100%">
										<tr>
											<td width="25" class="text-center"><strong>DISERAHKAN OLEH<br>PERAWAT PENANGGUNG JAWAB PASIEN</strong></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center"><strong>DITERIMA OLEH<br>PASIEN / KELUARGA</strong></td>
										</tr>
										<tr>
											<td width="25" class="text-center"><strong>
												<img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $diserahkan; ?>" width="100px">
											</strong></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<?if ($diterima_pasien_ttd){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$diterima_pasien_ttd?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_pernyataan()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
											</td>
										</tr>
										<tr>
											<td width="25" class="text-center">
											<strong>( <?=get_nama_ppa($diserahkan)?> )</strong> 
											<?if ($status_assemen!='2'){?>
											<br>
											<button class="btn btn-xs btn-default" onclick="modal_ttd_petugas()"  type="button"><i class="fa fa-paint-brush"></i></button>
											<?}?>
											</td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<strong>
													<?if ($diterima_pasien_nama){?>
														(<?=($diterima_pasien_nama)?>)
													<?}?>
												</strong>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>

						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_pulang();
		load_kontrol_assesmen_pulang();
		load_hasil_assesmen_pulang();
		load_tindakan_assesmen_pulang();
		load_diagnosa_pulang();
		
		load_awal_assesmen=false;
	}
	list_index_history_edit();
	// load_data_rencana_asuhan_pulang(1);
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_pulang', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

function load_skrining_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_pulang', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				versi_edit:$("#versi_edit").val(),
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			// console.LOG(data);
			// $(".nilai").select2();
			// get_skor_pengkajian_pulang();
			$("#cover-spin").hide();
		}
	});
}
function load_kontrol_assesmen_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_kontrol_assesmen_pulang', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:$("#assesmen_id").val(),
				versi_edit:$("#versi_edit").val(),
				
			},
		success: function(data) {
			$("#tabel_kontrol tbody").empty();
			$("#tabel_kontrol tbody").append(data.tabel);
		}
	});
}	
	
function load_tindakan_assesmen_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	$.ajax({
			url: '{site_url}thistory_tindakan/load_tindakan_assesmen_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					versi_edit:$("#versi_edit").val(),
					
				},
			success: function(data) {
				$("#tabel_tindakan tbody").empty();
				$("#tabel_tindakan tbody").append(data.tabel);
			}
		});
}
function load_hasil_assesmen_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}thistory_tindakan/load_hasil_assesmen_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					versi_edit:$("#versi_edit").val(),
					
				},
			success: function(data) {
				$("#tabel_hasil tbody").empty();
				$("#tabel_hasil tbody").append(data.tabel);
			}
		});
}
function load_diagnosa_pulang(){
	
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	$('#tabel_diagnosa').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#tabel_diagnosa').DataTable({
			autoWidth: false,
			serverSide: true,
			"searching": false,
			"processing": false,
			"bPaginate": false,
			"bFilter": false,
			"bInfo": false,
			"order": [],
			// "pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}thistory_tindakan/load_diagnosa_pulang', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
	refresh_diagnosa_pulang();
}
	
function refresh_diagnosa_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	$("#diagnosa_id_list").empty();
	$.ajax({
		url: '{site_url}thistory_tindakan/refresh_diagnosa_pulang', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				versi_edit:versi_edit,
				
			},
		success: function(data) {
			// alert(data);
			$("#diagnosa_id_list").append(data);
			load_data_rencana_asuhan_pulang();
		}
	});
}
function load_data_rencana_asuhan_pulang(diagnosa_id){
	if (diagnosa_id){
		
	}else{
		diagnosa_id=$("#diagnosa_id_list").val()
	}
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_data_rencana_asuhan_pulang', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				diagnosa_id:diagnosa_id,
				versi_edit:versi_edit,
				
			},
		success: function(data) {
			tabel=data;
			$("#tabel_rencana_asuhan tbody").empty().html(tabel);
			$("#cover-spin").hide();
			disabel_edit();
			// console.log(data)
			// $("#tabel_rencana_asuhan tbody").append(data);
		}
	});
}
function set_rencana_asuhan(id){
	 $("#diagnosa_id_list").val(id).trigger('change');
}
$("#diagnosa_id_list").on("change", function(){
	load_data_rencana_asuhan_pulang($(this).val());
});
</script>