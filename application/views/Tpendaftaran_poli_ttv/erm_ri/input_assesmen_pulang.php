<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_assesmen_pulang'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_assesmen_pulang' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_assesmen_pulang=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_pulang()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
								<h5 class="font-w400 push-5 text-center"><strong><i>{judul_header_eng}</i></strong></h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_assesmen_pulang=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen_pulang" onclick="create_assesmen_pulang()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_pulang/<?=$assesmen_id?>" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_rj/input_assesmen_pulang" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_pulang" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_pulang()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="alasan">Alasan Pulang</label>
									<select id="alasan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($alasan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(317) as $row){?>
										<option value="<?=$row->id?>" <?=($alasan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($alasan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-8 ">
									<label for="mobilisasi">Mobilisasi Saat Pulang Alat Bantu</label>
									<select id="mobilisasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($mobilisasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(318) as $row){?>
										<option value="<?=$row->id?>" <?=($mobilisasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($mobilisasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="mobilisasi_lain">Mobilisasi Lainnya</label>
									<input id="mobilisasi_lain" class="form-control auto_blur" type="text"  value="{mobilisasi_lain}" name="mobilisasi_lain" placeholder="Mobilisasi Lainnya" >
								</div>
								<div class="col-md-6 ">
									<label for="alkes">Alat Kesehatan Yang Terpasang</label>
									<select id="alkes" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($alkes == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(319) as $row){?>
										<option value="<?=$row->id?>" <?=($alkes == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($alkes == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="perawatan">Perawatan Khusus Dirumah</label>
									<select id="perawatan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($perawatan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(320) as $row){?>
										<option value="<?=$row->id?>" <?=($perawatan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($perawatan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-8 ">
									<label for="perawatan_lain">Perawatan Khusus Dirumah Lainnya</label>
									<input id="perawatan_lain" class="form-control auto_blur" type="text"  value="{perawatan_lain}" name="perawatan_lain" placeholder="Perawatan Khusus Dirumah Lainnya" >
								</div>
								
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Keadaan Saat Pulang </h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="jam_tv">Tanda Vital Jam</label>
									<div class="input-group">
										<input  type="text"  class="time-datepicker form-control auto_blur" id="jam_tv" name="jam_tv" value="<?= $jam_tv ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-8 ">
									<label for="tingkat_kesadaran">Tingkat Kesadaran</label>
									<select id="tingkat_kesadaran"   name="tingkat_kesadaran" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4 push-10-t">
									<label for="nadi">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-4 push-10-t">
									<label for="nafas">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-4 push-10-t">
									<label for="spo2">SpO2</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="spo2" name="spo2" value="{spo2}"  placeholder="Frekuensi spo2" required>
										<span class="input-group-addon"><?=$satuan_spo2?></span>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal auto_blur"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Tinggi Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
										<span class="input-group-addon">Cm</span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Berat Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >DIAGNOSA KEPERAWATAN</h5>
							</div>
							</div>
							<div class="col-md-6 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >RENCANA ASUHAN KEPERAWATAN</h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<?if($status_assemen !='2'){?>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="index_header">
											<thead>
												<tr>
													<th width="55%">
														Diagnosa
													</th>
													<th width="25%">
														Priority
													</th>
													<th width="20%">
														
													</th>
												</tr>
												<tr>
													<th width="55%">
														<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
														<select id="mdiagnosa_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
															<?}?>
														</select>
													</th>
													<th width="25%">
														<select id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															
															<?for($i=1;$i<20;$i++){?>
															<option value="<?=$i?>"><?=$i?></option>
															<?}?>
														</select>
													</th>
													<th width="20%">
														<span class="input-group-btn">
															<button class="btn btn-info" onclick="simpan_diagnosa_pulang()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
															<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<?}?>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Diagnosa Keperawatan</th>
													<th width="10%">Priority</th>
													<th width="25%">User</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa</label>
									<select id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
								<div class="col-md-12 push-10-t">
									<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
										<thead></thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="perawatan">Tindakan Keperawatan  Selama Dirarawat</label>
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_tindakan">
											<thead>
												<?if($status_assemen !='2'){?>
												<tr>
													<th width="70%" colspan="2">
														<input id="trx_nama" class="form-control" type="text"  value="" placeholder="Nama Tindakan" >
													</th>
													<th width="30%">
														<div class="input-group date">
															<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="trx_tanggal" placeholder="HH/BB/TTTT" value="<?=HumanDateShort(date('Y-m-d'))?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
													</th>
													<th width="10%">
														<button class="btn btn-info" onclick="simpan_tindakan()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
													</th>
												</tr>
												<?}?>
												<tr>
													<th width="5%">No</th>
													<th width="55%">Nama Tindakan</th>
													<th width="30%">Tanggal</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="obat">Manajemen Nyeri Obat Yang Diminum /<br> Anti Nyeri</label>
									<input id="obat" class="form-control auto_blur" type="text"  value="{obat}" name="obat" placeholder="" >
								</div>
								<div class="col-md-6 ">
									<label for="efek">Efek Samping Mungkin Timbul<br>&nbsp;</label>
									<input id="efek" class="form-control auto_blur" type="text"  value="{efek}" name="efek" placeholder="" >
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="nyeri">Bila Nyeri bertambah berat segera ke RS</label>
									<input id="nyeri" class="form-control auto_blur" type="text"  value="{nyeri}" name="nyeri" placeholder="" >
								</div>
								<div class="col-md-6 ">
									<label for="efek">Batasan cairan</label>
									<select id="batasan_cairan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($batasan_cairan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(321) as $row){?>
										<option value="<?=$row->id?>" <?=($batasan_cairan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($batasan_cairan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="evaluasi">Evaluasi</label>
									<input id="evaluasi" class="form-control auto_blur" type="text"  value="{evaluasi}" name="evaluasi" placeholder="Evaluasi" >
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-bottom:-10px">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >EDUKASI </h5>
							</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
											<thead>
												<tr>
													<th width="5%" class="text-center">NO</th>
													<th width="45%" class="text-center">JENIS EDUKASI</th>
													<?
													$q="SELECT H.jenis as id,M.ref as nama 
													FROM `tranap_assesmen_pulang_pengkajian` H 
													INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='325'
													WHERE H.assesmen_id='$assesmen_id'
													GROUP BY H.jenis
													ORDER BY H.jenis";
													$list_header=$this->db->query($q)->result();
													$jml_kolom=count($list_header);
													$lebar_kolom=50 / $jml_kolom;
													foreach($list_header as $r){
													?>
													<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
													
													<?}?>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:-10px">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Hasil Pemeriksaan Yang Diserahkan </h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_hasil">
											<thead>
											<?if ($status_assemen!='2'){?>
												<tr>
													<th colspan="2">
														<select id="trx_jenis_pemeriksaan" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="0" selected>-Pilih-</option>
															<?foreach(list_variable_ref(322) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th >
														<input id="trx_hasil" class="form-control" type="text"  value="" placeholder="Hasil Pemeriksaan" >
													</th>
													<th >
														<div class="input-group date">
															<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="trx_tanggal_hasil" placeholder="HH/BB/TTTT" value="<?=HumanDateShort(date('Y-m-d'))?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
													</th>
													<th >
														<input id="trx_jumlah_lembar" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text"  value="" placeholder="Jumlah Lembar" >
													</th>
													<th width="10%">
														<button class="btn btn-info" onclick="simpan_hasil()" id="btn_add_hasil" type="button"><i class="fa fa-plus"></i> Add</button>
													</th>
												</tr>
											<?}?>
												<tr>
													<th width="5%">No</th>
													<th width="20%">Jenis Pemeriksaan</th>
													<th width="30%">Hasil Pemeriksaan</th>
													<th width="15%">Tanggal</th>
													<th width="15%">Jumlah Lembar</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-bottom:-10px">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Rencana Kontrol </h5>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="tabel_kontrol">
											<thead>
												<?if($status_assemen !='2'){?>
												<tr>
													<th colspan="2">
														<div class="input-group date">
															<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="trx_tanggal_kontrol" placeholder="HH/BB/TTTT" value="<?=HumanDateShort(date('Y-m-d'))?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
														
													</th>
													<th >
														<select id="trx_poli" class="js-select2 form-control " style="width: 100%;" data-placeholder="Poliklinik" required>
															<option value="0" selected>-Pilih Poliklinik-</option>
															<?foreach(get_all('mpoliklinik',array('idtipe'=>1,'status'=>1)) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th >
														<select id="trx_iddokter" class="js-select2 form-control " style="width: 100%;" data-placeholder="Dokter" required>
															
														</select>
													</th>
													
													<th >
														<select id="trx_jam" class="js-select2 form-control " style="width: 100%;" data-placeholder="Jam Praktek" required>
															
														</select>
													</th>
													<th >
														<input id="trx_keterangan" class="form-control" type="text"  value="" placeholder="Keterangan" >
													</th>
													<th width="10%">
														<button class="btn btn-info" onclick="simpan_kontrol()" id="btn_add_kontrol" type="button"><i class="fa fa-plus"></i> Add</button>
													</th>
												</tr>
												<?}?>
												<tr>
													<th width="5%">No</th>
													<th width="15%">Tanggal Kontrol</th>
													<th width="20%">Poliklinik</th>
													<th width="25%">Dokter</th>
													<th width="15%">Jam Prakter</th>
													<th width="25%">Keterangan</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="skr">Surat Keterangan Rawat</label>
									<select id="skr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($skr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(323) as $row){?>
										<option value="<?=$row->id?>" <?=($skr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($skr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-6 ">
									<label for="asuransi">Asuransi</label>
									<select id="asuransi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($asuransi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(324) as $row){?>
										<option value="<?=$row->id?>" <?=($asuransi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($asuransi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<table width="100%">
										<tr>
											<td width="25" class="text-center"><strong>DISERAHKAN OLEH<br>PERAWAT PENANGGUNG JAWAB PASIEN</strong></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center"><strong>DITERIMA OLEH<br>PASIEN / KELUARGA</strong></td>
										</tr>
										<tr>
											<td width="25" class="text-center"><strong>
												<img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $diserahkan; ?>" width="100px">
											</strong></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<?if ($diterima_pasien_ttd){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$diterima_pasien_ttd?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_pernyataan()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
											</td>
										</tr>
										<tr>
											<td width="25" class="text-center">
											<strong>( <?=get_nama_ppa($diserahkan)?> )</strong> 
											<?if ($status_assemen!='2'){?>
											<br>
											<button class="btn btn-xs btn-default" onclick="modal_ttd_petugas()"  type="button"><i class="fa fa-paint-brush"></i></button>
											<?}?>
											</td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<strong>
													<?if ($diterima_pasien_nama){?>
														(<?=($diterima_pasien_nama)?>)
													<?}?>
												</strong>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>

						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-right push-20-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_assesmen_pulang=='1'){?>
										<button class="btn btn-primary" id="btn_create_assesmen_pulang" onclick="create_assesmen_pulang()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										<?if ($assesmen_id!=''){?>
											<?if ($status_assemen=='3'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
											<?}?>
											<?if ($status_assemen=='1'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
											<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
											<?}?>
											<?if ($assesmen_id!=''){?>
											<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_pulang/<?=$assesmen_id?>" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
											<?}?>
											<?if ($status_assemen=='3'){?>
												<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
											<?}?>
											<?if ($status_assemen=='2'){?>
												<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
												<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_rj/input_assesmen_pulang" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
											<?}?>
										<?}?>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template_pulang()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen_pulang()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen_pulang()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan </h3>
				</div>
				<div class="block-content">
					
					
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$diterima_pasien_ttd?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="diterima_pasien_nama" placeholder="Nama" value="{diterima_pasien_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_petugas" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PERAWAT PENANGGUNG JAWAB PASIEN</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="diserahkan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										
										<?foreach(get_all('mppa',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>" <?=($diserahkan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_petugas()"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/modal_email')?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});

function modal_ttd_petugas(){
	$("#modal_ttd_petugas").modal('show');
}
function modal_ttd_pernyataan(){
	let ttd=$("#signature64").val();
	$("#modal_ttd").modal('show');
	$('#sig_ttd_pernyataan').signature('enable').signature('draw', $("#signature64_ttd_pernyataan").val());
}
function clear_ttd_pernyataan(){
	sig_ttd_pernyataan.signature('clear');
	$("#signature64").val('');
}
function simpan_ttd(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var diterima_pasien_nama=$("#diterima_pasien_nama").val();
	if ($("#diterima_pasien_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama ", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				diterima_pasien_nama:diterima_pasien_nama,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  location.reload();
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
function simpan_ttd_petugas(){
	var assesmen_id=$("#assesmen_id").val();
	var diserahkan=$("#diserahkan").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_petugas_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				diserahkan:diserahkan,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  location.reload();
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
function hapus_ttd_pernyataan(){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64").val('')
	var signature64=$("#signature64").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				location.reload();
			}
		});
}
$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	// set_ttd_assesmen();
	// $(".js-datetimepicker").datetimepicker({
		// format: "dd-mm-yyyy HH:mm"
	// });
	$('.number').number(true, 0);
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_pulang();
		load_kontrol_assesmen_pulang();
		load_hasil_assesmen_pulang();
		load_tindakan_assesmen_pulang();
		load_diagnosa_pulang();
		if ($("#signature64").val()){
			set_img_canvas();
		}
		
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian()
	}
	
	// load_data_rencana_asuhan_pulang(1);
});
function load_skrining_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		$("#tabel_skrining tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
					},
			success: function(data) {
				$("#tabel_skrining tbody").append(data.opsi);
				// console.LOG(data);
				$(".nilai").select2();
				// get_skor_pengkajian_pulang();
				$("#cover-spin").hide();
			}
		});
	}
	
	$(document).on("change",".nilai",function(){
		let assesmen_id=$("#assesmen_id").val();
		// let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		let row_id=$(this).find("option:selected").attr('data-row_id');
		let nilai_nama=$(this).find("option:selected").attr('data-nama');
		var nilai_id=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_pulang', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					row_id:row_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					nilai_nama:nilai_nama,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
					// $("#st_tindakan").val(data.st_tindakan);
					 // $("#nama_tindakan").val(data.nama_tindakan);
					 // $("#skor_pengkajian").val(data.skor);
					 // $("#risiko_jatuh").val(data.ref_nilai);
				}
			});
		
	});
	$(document).on("change",".nilai_mengerti",function(){
		let assesmen_id=$("#assesmen_id").val();
		// let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		var risiko_nilai_id=tr.find(".risiko_edukasi_id").val();
		let nilai_nama=$(this).find("option:selected").attr('data-nama');
		var nilai_id=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_mengerti_pulang', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					risiko_nilai_id:risiko_nilai_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					nilai_nama:nilai_nama,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
					// $("#st_tindakan").val(data.st_tindakan);
					 // $("#nama_tindakan").val(data.nama_tindakan);
					 // $("#skor_pengkajian").val(data.skor);
					 // $("#risiko_jatuh").val(data.ref_nilai);
				}
			});
		
	});
	function get_skor_pengkajian_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#template_id").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_risiko_jatuh_pulang', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },
			  success: function(data) {
				 $("#st_tindakan").val(data.st_tindakan);
				 $("#nama_tindakan").val(data.nama_tindakan);
				 $("#skor_pengkajian").val(data.skor);
				 $("#risiko_jatuh").val(data.ref_nilai);
			  }
		});
		
	}
	$(".opsi_change").change(function(){
		console.log('OPSI CHANTE')
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});

	$(".auto_blur").blur(function(){
		console.log('BLUR')
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
		
	});
	function simpan_assesmen(){
	// alert(load_awal_assesmen);
	if (status_assemen !='2'){
		
		if (load_awal_assesmen==false){
		let assesmen_id=$("#assesmen_id").val();
		// alert($("#tanggal_keluar").val());return false;
		let tanggaldatang=$("#tanggaldatang").val();
		let waktudatang=$("#waktudatang").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/save_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						// nama_template:nama_template,
						tanggaldatang:tanggaldatang,
						waktudatang:waktudatang,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						alasan : $("#alasan").val(),
						mobilisasi : $("#mobilisasi").val(),
						mobilisasi_lain : $("#mobilisasi_lain").val(),
						alkes : $("#alkes").val(),
						perawatan : $("#perawatan").val(),
						perawatan_lain : $("#perawatan_lain").val(),
						jam_tv : $("#jam_tv").val(),
						tingkat_kesadaran : $("#tingkat_kesadaran").val(),
						nadi : $("#nadi").val(),
						nafas : $("#nafas").val(),
						spo2 : $("#spo2").val(),
						td_sistole : $("#td_sistole").val(),
						td_diastole : $("#td_diastole").val(),
						suhu : $("#suhu").val(),
						tinggi_badan : $("#tinggi_badan").val(),
						berat_badan : $("#berat_badan").val(),
						obat : $("#obat").val(),
						efek : $("#efek").val(),
						nyeri : $("#nyeri").val(),
						batasan_cairan : $("#batasan_cairan").val(),
						evaluasi : $("#evaluasi").val(),
						skr : $("#skr").val(),
						asuransi : $("#asuransi").val(),
						diserahkan : $("#diserahkan").val(),

						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
			}
		 }
		}
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
function create_assesmen_pulang(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_assesmen_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
$("#trx_poli").on("change", function(){
	get_dokter();
});
$("#trx_iddokter").on("change", function(){
	get_jam_dokter();
});
function get_dokter(){
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:$("#trx_poli").val(),
				
			},
		success: function(data) {
			$("#trx_iddokter").empty().append(data)
		}
	});
}
function get_jam_dokter(){
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_jam_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				idpoli:$("#trx_poli").val(),
				iddokter:$("#trx_iddokter").val(),
				
			},
		success: function(data) {
			$("#trx_jam").empty().append(data)
		}
	});
}
function simpan_kontrol(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tanggal=$("#trx_tanggal_kontrol").val();
		let idpoli=$("#trx_poli").val();
		let iddokter=$("#trx_iddokter").val();
		let jam=$("#trx_jam").val();
		let keterangan=$("#trx_keterangan").val();
		
		let idpasien=$("#idpasien").val();
		if (tanggal==''){
			sweetAlert("Maaf...", "Isi Nama Tanggal!", "error");
			return false;

		}
		
		if (idpoli=='0'){
			sweetAlert("Maaf...", "Isi Poliklinik!", "error");
			return false;

		}
		if (iddokter=='0'){
			sweetAlert("Maaf...", "Isi Dokter!", "error");
			return false;

		}
		if (jam=='0'){
			sweetAlert("Maaf...", "Isi Jadwal!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_kontrol_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						tanggal:tanggal,
						idpoli:idpoli,
						iddokter:iddokter,
						jam:jam,
						keterangan:keterangan,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Tindkan.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_kontrol_assesmen_pulang();
						// $("#trx_").val('')
					}
				}
			});
		}
		
	}
	function load_kontrol_assesmen_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/load_kontrol_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#tabel_kontrol tbody").empty();
					$("#tabel_kontrol tbody").append(data.tabel);
				}
			});
	}
	function hapus_kontrol_assesmen_pulang(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Rencana Kontrol?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_kontrol_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
					load_kontrol_assesmen_pulang();		
				}
			});
		});			
	}
function simpan_tindakan(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let nama=$("#trx_nama").val();
		let tanggal=$("#trx_tanggal").val();
		
		let idpasien=$("#idpasien").val();
		if (nama==''){
			sweetAlert("Maaf...", "Isi Nama Tindakan!", "error");
			return false;

		}
		
		if (tanggal==''){
			sweetAlert("Maaf...", "Isi Tanggal!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_tindakan_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						nama:nama,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						tanggal:tanggal,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Tindkan.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_tindakan_assesmen_pulang();
						$("#trx_nama").val('')
					}
				}
			});
		}
		
	}
	function load_tindakan_assesmen_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/load_tindakan_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#tabel_tindakan tbody").empty();
					$("#tabel_tindakan tbody").append(data.tabel);
				}
			});
	}
	function hapus_tindakan_assesmen_pulang(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Tindakan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_tindakan_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
					load_tindakan_assesmen_pulang();		
				}
			});
		});			
	}
	function simpan_hasil(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let jenis_pemeriksaan=$("#trx_jenis_pemeriksaan").val();
		let tanggal=$("#trx_tanggal_hasil").val();
		let jumlah_lembar=$("#trx_jumlah_lembar").val();
		let hasil=$("#trx_hasil").val();
		
		let idpasien=$("#idpasien").val();
		if (jenis_pemeriksaan=='0'){
			sweetAlert("Maaf...", "Isi Nama Jenis!", "error");
			return false;

		}
		if (hasil==''){
			sweetAlert("Maaf...", "Isi Nama Jenis!", "error");
			return false;

		}
		if (jumlah_lembar==''){
			sweetAlert("Maaf...", "Isi Nama Jumlah Lembar!", "error");
			return false;

		}
		
		if (tanggal==''){
			sweetAlert("Maaf...", "Isi Tanggal!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_hasil_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						jenis_pemeriksaan:jenis_pemeriksaan,
						hasil:hasil,
						jumlah_lembar:jumlah_lembar,
						tanggal:tanggal,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Tindkan.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_hasil_assesmen_pulang();
						$("#trx_jenis_pemeriksaan").val('0').trigger('change');
						$("#trx_hasil").val('');
						$("#trx_jumlah_lembar").val('');
					}
				}
			});
		}
		
	}
	function load_hasil_assesmen_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/load_hasil_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#tabel_hasil tbody").empty();
					$("#tabel_hasil tbody").append(data.tabel);
				}
			});
	}
	function hapus_hasil_assesmen_pulang(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Hasil Pemeriksaan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_hasil_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_hasil_assesmen_pulang();		
				}
			});
		});			
	}
	function simpan_diagnosa_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let mdiagnosa_id=$("#mdiagnosa_id").val();
		let diagnosa_id=$("#diagnosa_id").val();
		let prioritas=$("#prioritas").val();
		
		let idpasien=$("#idpasien").val();
		if (mdiagnosa_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mdiagnosa_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_diagnosa_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						diagnosa_id:diagnosa_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						mdiagnosa_id:mdiagnosa_id,
						prioritas:prioritas,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Diagnosa.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_diagnosa_pulang();
					}
				}
			});
		}
		
	}
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function edit_diagnosa(id){
		$("#diagnosa_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_diagnosa_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
				$("#mdiagnosa_id").val(data.mdiagnosa_id).trigger('change');
				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	function set_rencana_asuhan(id){
		 $("#diagnosa_id_list").val(id).trigger('change');
	}
	function load_diagnosa_pulang(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa_pulang();
	}
	
	function refresh_diagnosa_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				load_data_rencana_asuhan_pulang();
			}
		});
	}
	function hapus_diagnosa_pulang(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_diagnosa_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_diagnosa_pulang();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	$("#diagnosa_id_list").on("change", function(){
		load_data_rencana_asuhan_pulang($(this).val());
	});
	function load_data_rencana_asuhan_pulang(diagnosa_id){
		if (diagnosa_id){
			
		}else{
			diagnosa_id=$("#diagnosa_id_list").val()
		}
		let assesmen_id=$("#assesmen_id").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_data_rencana_asuhan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_id,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan tbody").append(data);
			}
		});
	}
$("#template_id").on("change", function(){
	let template_id=$(this).val();
	if (template_id!=template_id_last){
			swal({
			  title: 'Anda Yakain?',
			  text: "Mengganti Mode Penilaian!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Proses!'
			}).then(function(json_data) {
					$("#cover-spin").show();
					let assesmen_id=$("#assesmen_id").val();
					template_id_last=template_id;
					$.ajax({
						url: '{site_url}Tpendaftaran_ranap_erm/simpan_risiko_jatuh_template_pulang', 
						dataType: "JSON",
						method: "POST",
						data : {
								assesmen_id:assesmen_id,
								template_id:template_id,
								
							},
						success: function(data) {
							
									console.log(data);
							if (data==null){
								swal({
									title: "Gagal!",
									text: "Simpan TTV.",
									type: "error",
									timer: 1500,
									showConfirmButton: false
								});

							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Ganti Mode Penilaian '});
								location.reload();			
							}
						}
					});
			}, function(dismiss) {
			  if (dismiss === 'cancel' || dismiss === 'close') {
					$("#template_id").val(template_id_last).trigger('change.select2');
			  } 
			})
		}else{
			save_all_detail(kategori_id_last);
		}
		
	
	
});
function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd_pulang(){
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}

	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function load_ttd_pulang(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_ttd_pulang', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;
			
			// $("#sig").html('<img src="'+src+'"/>')
			// console.log(data.ttd);
		// set_img_canvas();
			$('#sig').signature('enable'). signature('draw', data.ttd);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd_pulang();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_pulang();
}
function create_with_template_pulang(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_pulang(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_template_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_assesmen_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_template_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$("#medukasi_id").change(function(){
	if ($("#st_edited").val()=='0'){
	simpan_edukasi_pulang();
	}
});

// $(".auto_blur").blur(function(){
	// if ($("#st_edited").val()=='0'){
		// $(this).removeClass('input_edited');
		// simpan_assesmen_ttv();
	// }else{
		// if ($("#st_edited").val()=='1'){
			// if ($(this).val()!=before_edit){
				// console.log('Ada Peruabahan');
				// // $(this).attr('input_edited');
				 // $(this).addClass('input_edited');
			// }
		// }
	// }
	
// });
$("#tingkat_kesadaran").change(function(){
		if ($("#st_edited").val()=='0'){
			// simpan_assesmen();
			simpan_assesmen_ttv();
		}
});

function simpan_assesmen_ttv(){
	// let assesmen_id=$("#assesmen_id").val();
	// let tingkat_kesadaran=$("#tingkat_kesadaran").val();
	// let nadi=$("#nadi").val();
	// let nafas=$("#nafas").val();
	// let td_sistole=$("#td_sistole").val();
	// let td_diastole=$("#td_diastole").val();
	// let suhu=$("#suhu").val();
	// let tinggi_badan=$("#tinggi_badan").val();
	// let berat_badan=$("#berat_badan").val();
	// console.log('SIMPAN TTV');
	// // $("#cover-spin").show();
	// $.ajax({
		// url: '{site_url}Tpendaftaran_ranap_erm/simpan_assesmen_ttv', 
		// dataType: "JSON",
		// method: "POST",
		// data : {
				// assesmen_id:assesmen_id,
				// tingkat_kesadaran:tingkat_kesadaran,
				// nadi:nadi,
				// nafas:nafas,
				// td_sistole:td_sistole,
				// td_diastole:td_diastole,
				// suhu:suhu,
				// tinggi_badan:tinggi_badan,
				// berat_badan:berat_badan,
				
			// },
		// success: function(data) {
			
					// console.log(data);
			// if (data==null){
				// swal({
					// title: "Gagal!",
					// text: "Simpan TTV.",
					// type: "error",
					// timer: 1500,
					// showConfirmButton: false
				// });

			// }else{
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan TTV Draft'});
				
			// }
		// }
	// });
}
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			// simpan_assesmen_ttv();
			// simpan_edukasi_pulang();
			// simpan_riwayat_penyakit_pulang();
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}

	
	function simpan_alergi_pulang(){// alert($("#keluhan_utama").val());
		let alergi_id=$("#alergi_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		let input_jenis_alergi=$("#input_jenis_alergi").val();
		let input_detail_alergi=$("#input_detail_alergi").val();
		let input_reaksi_alergi=$("#input_reaksi_alergi").val();
		if (input_detail_alergi==''){
			sweetAlert("Maaf...", "Isi Detail Alergi!", "error");
			return false;

		}
		if (input_reaksi_alergi==''){
			sweetAlert("Maaf...", "Isi Reaksi Alergi!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_alergi_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						alergi_id:alergi_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						assesmen_id:assesmen_id,
						input_jenis_alergi:input_jenis_alergi,
						input_detail_alergi:input_detail_alergi,
						input_reaksi_alergi:input_reaksi_alergi,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_alergi();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_alergi_pulang();
					}
				}
			});
		}
		
	}
	function simpan_obat_pulang(){// alert($("#keluhan_utama").val());
		let obat_id=$("#obat_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		let nama_obat=$("#nama_obat").val();
		let dosis=$("#dosis").val();
		let waktu=$("#waktu").val();
		if (nama_obat==''){
			sweetAlert("Maaf...", "Isi Nama Obat!", "error");
			return false;

		}
		if (dosis==''){
			sweetAlert("Maaf...", "Isi Dosis Obat!", "error");
			return false;

		}
		if (waktu==''){
			sweetAlert("Maaf...", "Isi Waktu Obat!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_obat_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						obat_id:obat_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						assesmen_id:assesmen_id,
						nama_obat:nama_obat,
						dosis:dosis,
						waktu:waktu,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_obat();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_obat_pulang();
					}
				}
			});
		}
		
	}
	function simpan_obat_infus_pulang(){
		let obat_infus_id=$("#obat_infus_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		
		let obat_id_select2=$("#obat_id_select2").val();
		let tanggalinfus=$("#tanggalinfus").val();
		let waktuinfus=$("#waktuinfus").val();
		let kuantitas_infus=$("#kuantitas_infus").val();
		let dosis_infus=$("#dosis_infus").val();
		let ppa_pemberi=$("#ppa_pemberi").val();
		let ppa_periksa=$("#ppa_periksa").val();
		let rute=$("#rute").val();
		let idtipe=$("#idtipe").val();
		
		if (obat_id_select2==''){
			sweetAlert("Maaf...", "Isi Nama Obat!", "error");
			return false;
		}
		if (tanggalinfus==''){
			sweetAlert("Maaf...", "Isi Tanggal!", "error");
			return false;
		}
		if (waktuinfus==''){
			sweetAlert("Maaf...", "Isi Waktu Obat!", "error");
			return false;
		}
		if (kuantitas_infus==''){
			sweetAlert("Maaf...", "Isi Kuantitas!", "error");
			return false;
		}
		if (dosis_infus==''){
			sweetAlert("Maaf...", "Isi Dosis!", "error");
			return false;
		}
		if (ppa_pemberi==''){
			sweetAlert("Maaf...", "Isi Pemberi!", "error");
			return false;
		}
		if (ppa_periksa==''){
			sweetAlert("Maaf...", "Isi Pemeriksa!", "error");
			return false;
		}
		if (rute==''){
			sweetAlert("Maaf...", "Isi Rute!", "error");
			return false;
		}
		
		
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_obat_infus_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						obat_infus_id:obat_infus_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						assesmen_id:assesmen_id,
						
						obat_id_select2:obat_id_select2,
						tanggalinfus:tanggalinfus,
						waktuinfus:waktuinfus,
						kuantitas_infus:kuantitas_infus,
						dosis_infus:dosis_infus,
						ppa_pemberi:ppa_pemberi,
						ppa_periksa:ppa_periksa,
						rute:rute,
						idtipe:idtipe,
					},
				success: function(data) {
					clear_input_obat_infus();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Pemberiann Obat.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_obat_infus_pulang();
					}
				}
			});
		}
		
	}
	function clear_input_obat_infus(){
		$("#obat_infus_id").val('');
		$("#btn_add_obat_infus").html('<i class="fa fa-plus"></i> Add');
		$("#obat_id_select2").val('').trigger('change');
		$("#ppa_pemberi").val('').trigger('change');
		$("#ppa_periksa").val('').trigger('change');
		$("#kuantitas_infus").val('');
		$("#dosis_infus").val('');
	}
	
	
	
	function clear_input_tindakan(){
		$("#tindakan_id").val('');
		$("#btn_add_tindakan").html('<i class="fa fa-plus"></i> Add');
		$("#mtindakan_id").val('').trigger('change');
		$("#ppa_pelaksana").val('').trigger('change');
		// $("#waktu_mulai").val('').trigger('change');
		$("#waktu_mulai").val('');
		$("#waktu_selesai").val('');
	}
	
	
	function clear_input_alergi(){
		$("#alergi_id").val('');
		$("#btn_add_alergi").html('<i class="fa fa-plus"></i> Add');
		$("#input_jenis_alergi").val('');
		$("#input_detail_alergi").val('');
		$("#input_reaksi_alergi").val('');
	}
	function clear_input_obat(){
		$("#obat_id").val('');
		$("#btn_add_obat").html('<i class="fa fa-plus"></i> Add');
		$("#nama_obat").val('');
		$("#dosis").val('');
		$("#waktu").val('');
	}
	function load_alergi_pulang(){
		$('#alergi_2 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	function load_obat_pulang(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#index_obat').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_obat').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_obat_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	// function load_obat_infus_pulang(){
		
		// let assesmen_id=$("#assesmen_id").val();
		// $('#index_obat_infus').DataTable().destroy();	
		// // $("#cover-spin").show();
		// // alert(ruangan_id);
		// table = $('#index_obat_infus').DataTable({
				// autoWidth: false,
				// searching: true,
				// serverSide: true,
				// "processing": false,
				// "order": [],
				// "pageLength": 10,
				// "ordering": false,
				// ajax: { 
					// url: '{site_url}Tpendaftaran_ranap_erm/load_obat_infus_pulang', 
					// type: "POST" ,
					// dataType: 'json',
					// data : {
							// assesmen_id:assesmen_id,
						   // }
				// },
				// columnDefs: [
					// {  className: "text-right", targets:[3] },
					// {  className: "text-center", targets:[8] },
					 // { "width": "5%", "targets": [1,3] },
					 // { "width": "10%", "targets": [0,4] },
					 // { "width": "20%", "targets": [2] },
					 // { "width": "10%", "targets": [5,6,7,8] }
				// ],
				// "drawCallback": function( settings ) {
					 // $("#cover-spin").hide();
					 // disabel_edit();
				 // }  
			// });
		
	// }
	
	function load_obat_infus_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/load_obat_infus_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#index_obat_infus tbody").empty();
					$("#index_obat_infus tbody").append(data.tabel);
				}
			});
	}
	function load_alergi_pulang_his(){
		$('#alergi_1 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_alergi_his').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi_his').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_his', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_alergi_pulang_pasien(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function hapus_alergi_pulang(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Alergi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_alergi_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_alergi_pulang();		
				}
			});
		});			
	}
	function hapus_obat_pulang(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Penggunaan Obat ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_obat_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_obat_pulang();		
				}
			});
		});			
	}
	function hapus_obat_infus_pulang(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Penggunaan Obat Infus ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_obat_infus_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_obat_infus_pulang();		
				}
			});
		});			
	}
	
	
	function simpan_edukasi_pulang(){// alert($("#keluhan_utama").val());
		let assesmen_id=$("#assesmen_id").val();
		let medukasi_id=$("#medukasi_id").val();
		console.log('SIMPAN EDUKASI');
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_edukasi_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						medukasi_id:medukasi_id,
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}
				}
			});
		}
		
	}
	 
	$(document).on('click','.data_asuhan',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/update_data_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa_pulang();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template_pulang").removeAttr('disabled');
		}else{
			$("#btn_create_with_template_pulang").attr('disabled','disabled');
		}
	});
	function list_index_template_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_index_template_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id_ranap);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_template_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen_pulang(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		// alert(assesmen_detail_id);return false;
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_edit_assesmen_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id=data.pendaftaran_id;
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_ri/input_assesmen_pulang'); ?>";
				}
			}
		});
	}
	function hapus_record_assesmen_pulang(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template_pulang();
				}
			});
		});

	}
	$(document).on('change','#rencana_kontrol',function(){
		set_rencana_kontrol();
	});
	function set_rencana_kontrol(){
		let rencana_kontrol=$("#rencana_kontrol").val();
		let tanggal=$("#tanggal").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/set_rencana_kontrol', 
			dataType: "JSON",
			method: "POST",
			data : {
					tanggal:tanggal,
					rencana_kontrol:rencana_kontrol,
				   },
			success: function(data) {
				// alert(data);
				$("#tanggal_kontrol").val(data);
				simpan_assesmen();
			}
		});
	}
	$("#idasalpasien").change(function() {
		// set_asal_rujukan();
		load_faskes_1();
	});
	function load_faskes_1(){
		$("#cover-spin").show();
		var asal = $("#idasalpasien").val();
		$.ajax({
			url: '{site_url}tpoliklinik_pendaftaran/getRujukan',
			method: "POST",
			dataType: "json",
			data: {
				"asal": asal
			},
			success: function(data) {
				$("#idrujukan").empty();
				$("#idrujukan").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#idrujukan").append("<option value='" + data[i].id + "' >" + data[i].nama + "</option>");
				}
				$("#idrujukan").selectpicker("refresh");
				$("#cover-spin").hide();
			}
		});
	}
	function show_modal_obat(){
		$("#obat_non_racikan-modal").modal('show');
		$("#tipeid,#idkat").select2({
			dropdownParent: $("#obat_non_racikan-modal")
		  });
	}
	$("#btn_filter_barang").click(function() {
		loadBarang();
	});
	
	function loadBarang() {
		var idunit = '1';
		var idtipe = $("#tipeid").val();
		var idkategori = $("#idkat").val();
		$('#table-Obat').DataTable().destroy();
		var table = $('#table-Obat').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}Tpendaftaran_ranap_erm/get_obat_rujukan/',
				type: "POST",
				dataType: 'json',
				data: {
					idunit: idunit,
					idtipe: idtipe,
					idkategori: idkategori,
				}
			},
			columnDefs: [{
					"width": "5%",
					"targets": [0],
					"orderable": true
				},
				{
					"width": "10%",
					"targets": [1, 3],
					"orderable": true
				},
				// {
					// "width": "15%",
					// "targets": [5],
					// "orderable": true
				// },
				{
					"width": "40%",
					"targets": [2],
					"orderable": true
				},
			]
		});
	}
	$("#tipeid").change(function() {
		load_kategori();
	});
	
	function load_kategori() {
		var idtipe = $("#tipeid").val();
		$('#idkat').find('option').remove();
		$('#idkat').append('<option value="#" selected>- Silahkan Pilih -</option>');
		if (idtipe != '#') {

			$.ajax({
				url: '{site_url}tstockopname/list_kategori/' + idtipe,
				dataType: "json",
				success: function(data) {
					$.each(data, function(i, row) {

						$('#idkat')
							.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
					});
				}
			});

		}
	}
	function TreeView($level, $name) {
		// console.log($name);
		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			if ($i > 0) {
				$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$indent += '';
			}
		}

		if ($i > 0) {
			$indent += '└─';
		} else {
			$indent += '';
		}

		return $indent + ' ' + $name;
	}
	function pilih_obat(obat_id,tipe_id,nama_obat){
		// $("#obat_infus_id").val(data.id);
		let newOption = new Option(nama_obat, obat_id, true, true);
			
		$("#obat_id_select2").append(newOption);

		$("#idtipe").val(tipe_id);
		$("obat_non_racikan-modal").modal('hide');
	}
	$(document).on("click", ".selectObat", function() {
		
		let idobat = ($(this).data('idobat'));
		let idtipe = ($(this).data('idtipe'));
		// alert(idtipe);
		$("#idtipe").val(idtipe);
		$.ajax({
			url: '{site_url}tpasien_penjualan/get_obat_detail/' + idobat + '/5/' + idtipe,
			dataType: "json",
			success: function(data) {
				

				var isi = {
					id: idobat,
					text: data.nama,
					idtipe: data.idtipe
				};
				// alert(isi.idtipe);
				var newOption = new Option(isi.text, isi.id, true, true);
				$("#obat_id_select2").append(newOption);
				$("obat_non_racikan-modal").modal('hide');
			}

		});

	});
</script>