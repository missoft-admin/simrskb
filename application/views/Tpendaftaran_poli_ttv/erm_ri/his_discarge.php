<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.border-bottom{
        border-bottom:1px solid #e9e9e9 !important;
		
      }
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	 <?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='his_discarge'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_discarge' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						// $tanggal_masuk_rs=HumanDateLong($tanggal_masuk_rs);
						$tanggalmasukrs=HumanDateShort($tanggal_masuk_rs);
						$tanggalmasukrs_asal=HumanDateShort($tanggal_masuk_rs_asal);
						$waktumasukrs=HumanTime($tanggal_masuk_rs);
						$waktumasukrs_asal=HumanTime($tanggal_masuk_rs_asal);
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktumasukrs=date('H:i:s');
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggalmasukrs=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_discarge=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_discarge=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_discarge=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >SAAT MASUK RUMAH SAKIT</h5>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-4">
								<div class="col-md-6">
									<label for="tanggalmasukrs">Waktu Masuk RS</label>
									<div class="input-group date">
										<input id="tanggalmasukrs"  class="<?=($tanggalmasukrs!=$tanggalmasukrs_asal?'edited':'')?> js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalmasukrs ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="tanggalmasukrs">&nbsp;</label>
									<div class="input-group">
										<input id="waktumasukrs" class="<?=($waktumasukrs!=$waktumasukrs_asal?'edited':'')?> time-datepicker form-control " type="text"   value="<?= $waktumasukrs ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="col-md-4">
									<label for="rencana_pemulangan">Rencana Pemulangan</label>
									<input id="rencana_pemulangan" class="<?=($rencana_pemulangan!=$rencana_pemulangan_asal?'edited':'')?> form-control "  type="text"  value="{rencana_pemulangan}"  placeholder="Rencana Pemulangan" required>
								</div>
								
								<div class="col-md-4">
									<label for="alasan_masuk_rs">Alasan Masuk RS</label>
									<input id="alasan_masuk_rs" class="<?=($alasan_masuk_rs!=$alasan_masuk_rs_asal?'edited':'')?> form-control "  type="text"  value="{alasan_masuk_rs}"  placeholder="Alasan Masuk RS" required>
								</div>
								<div class="col-md-4">
									<label for="diagnosa_medis">Diagnosa Medis</label>
									<input id="diagnosa_medis" class="<?=($diagnosa_medis!=$diagnosa_medis_asal?'edited':'')?> form-control "  type="text"  value="{diagnosa_medis}"  placeholder="Diagnosa Medis" required>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >KETERANGAN RENCANA PEMULANGAN</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="pengaruh_ranap_pasien">Pengaruh Rawat Inap Terhadap Pasien / Keluarga</label>
									<select id="pengaruh_ranap_pasien" name="pengaruh_ranap_pasien" class="<?=($pengaruh_ranap_pasien!=$pengaruh_ranap_pasien_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pengaruh_ranap_pasien == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(270) as $row){?>
										<option value="<?=$row->id?>"  <?=($pengaruh_ranap_pasien == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaruh_ranap_pasien == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="pengaruh_ranap_pasien_detail">Detail Pengaruh Rawat Inap Terhadap Pasien / Keluarga</label>
									<input id="pengaruh_ranap_pasien_detail" class="<?=($pengaruh_ranap_pasien_detail!=$pengaruh_ranap_pasien_detail_asal?'edited':'')?> form-control "  type="text"  value="{pengaruh_ranap_pasien_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="pengaruh_pekerjaan">Pengaruh Pengaruh Rawat inap terhadap pekerjaan / sekolah</label>
									<select id="pengaruh_pekerjaan" name="pengaruh_pekerjaan" class="<?=($pengaruh_pekerjaan!=$pengaruh_pekerjaan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pengaruh_pekerjaan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(271) as $row){?>
										<option value="<?=$row->id?>"  <?=($pengaruh_pekerjaan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaruh_pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="pengaruh_pekerjaan_detail">Detail Pengaruh Pengaruh Rawat inap terhadap pekerjaan / sekolah</label>
									<input id="pengaruh_pekerjaan_detail" class="<?=($pengaruh_pekerjaan_detail!=$pengaruh_pekerjaan_detail_asal?'edited':'')?> form-control "  type="text"  value="{pengaruh_pekerjaan_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="pengaruh_keuangan">Pengaruh Rawat Inap Terhadap Keuangan</label>
									<select id="pengaruh_keuangan" name="pengaruh_keuangan" class="<?=($pengaruh_keuangan!=$pengaruh_keuangan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pengaruh_keuangan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(272) as $row){?>
										<option value="<?=$row->id?>"  <?=($pengaruh_keuangan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaruh_keuangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="pengaruh_keuangan_detail">Detail Pengaruh Rawat Inap Terhadap Keuangan</label>
									<input id="pengaruh_keuangan_detail" class="<?=($pengaruh_keuangan_detail!=$pengaruh_keuangan_detail_asal?'edited':'')?> form-control "  type="text"  value="{pengaruh_keuangan_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="antisipasi_masalah_pulang">Antisipasi Terhadap Masalah Saat Pulang</label>
									<select id="antisipasi_masalah_pulang" name="antisipasi_masalah_pulang" class="<?=($antisipasi_masalah_pulang!=$antisipasi_masalah_pulang_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($antisipasi_masalah_pulang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(273) as $row){?>
										<option value="<?=$row->id?>"  <?=($antisipasi_masalah_pulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($antisipasi_masalah_pulang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="antisipasi_masalah_pulang_detail">Detail Antisipasi Terhadap Masalah Saat Pulang</label>
									<input id="antisipasi_masalah_pulang_detail" class="<?=($antisipasi_masalah_pulang_detail!=$antisipasi_masalah_pulang_detail_asal?'edited':'')?> form-control "  type="text"  value="{antisipasi_masalah_pulang_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>		
						<?
							$arr=explode(',',$bantuan_diperlukan_hal);
						?>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="bantuan_diperlukan_hal" class="<?=($bantuan_diperlukan_hal!=$bantuan_diperlukan_hal_asal?'edited':'')?>">Bantuan Diperlukan Dalam hal</label>
									<select id="bantuan_diperlukan_hal" name="bantuan_diperlukan_hal" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(274) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="bantuan_diperlukan_hal_detail">Detail Bantuan Diperlukan Dalam hal</label>
									<input id="bantuan_diperlukan_hal_detail" class="<?=($bantuan_diperlukan_hal_detail!=$bantuan_diperlukan_hal_detail_asal?'edited':'')?> form-control "  type="text"  value="{bantuan_diperlukan_hal_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="adakah_yg_membantu">Adakah Yang Membantu Keperluan Pasien</label>
									<select id="adakah_yg_membantu" name="adakah_yg_membantu" class="<?=($adakah_yg_membantu!=$adakah_yg_membantu_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($adakah_yg_membantu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(275) as $row){?>
										<option value="<?=$row->id?>"  <?=($adakah_yg_membantu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($adakah_yg_membantu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="adakah_yg_membantu_detail">Detail Adakah Yang Membantu Keperluan Pasien</label>
									<input id="adakah_yg_membantu_detail" class="<?=($adakah_yg_membantu_detail!=$adakah_yg_membantu_detail_asal?'edited':'')?> form-control "  type="text"  value="{adakah_yg_membantu_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="tinggal_sendiri">Apakah Pasien tinggal sendiri setelah keluar dari Rumah Sakit</label>
									<select id="tinggal_sendiri" name="tinggal_sendiri" class="<?=($tinggal_sendiri!=$tinggal_sendiri_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($tinggal_sendiri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(276) as $row){?>
										<option value="<?=$row->id?>"  <?=($tinggal_sendiri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tinggal_sendiri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tinggal_sendiri_detail">Detail Apakah Pasien tinggal sendiri setelah keluar dari Rumah Sakit</label>
									<input id="tinggal_sendiri_detail" class="<?=($tinggal_sendiri_detail!=$tinggal_sendiri_detail_asal?'edited':'')?> form-control "  type="text"  value="{tinggal_sendiri_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="menggunakan_peralatan_medis">Apakah Pasien menggunakan peralatan medis dirumah setelah keluar dari RS (Cateter, NGT, Oksiten)</label>
									<select id="menggunakan_peralatan_medis" name="menggunakan_peralatan_medis" class="<?=($menggunakan_peralatan_medis!=$menggunakan_peralatan_medis_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($menggunakan_peralatan_medis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(283) as $row){?>
										<option value="<?=$row->id?>"  <?=($menggunakan_peralatan_medis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($menggunakan_peralatan_medis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="menggunakan_peralatan_medis_detail">Detail Apakah Pasien menggunakan peralatan medis dirumah setelah keluar dari RS (Cateter, NGT, Oksiten)</label>
									<input id="menggunakan_peralatan_medis_detail" class="<?=($menggunakan_peralatan_medis_detail!=$menggunakan_peralatan_medis_detail_asal?'edited':'')?> form-control "  type="text"  value="{menggunakan_peralatan_medis_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="memerlukan_alat_bantu">Apakah pasien memerlukan Alat Bantu Setelah Keluar RS (Tongkat, Kursi Roda, Walker, dll)</label>
									<select id="memerlukan_alat_bantu" name="memerlukan_alat_bantu" class="<?=($memerlukan_alat_bantu!=$memerlukan_alat_bantu_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_alat_bantu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(277) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_alat_bantu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_alat_bantu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_alat_bantu_detail">Detail Apakah pasien memerlukan Alat Bantu Setelah Keluar RS (Tongkat, Kursi Roda, Walker, dll)</label>
									<input id="memerlukan_alat_bantu_detail" class="<?=($memerlukan_alat_bantu_detail!=$memerlukan_alat_bantu_detail_asal?'edited':'')?> form-control "  type="text"  value="{memerlukan_alat_bantu_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="memerlukan_perawatn_khusus">Apakah Pasien Memerlukan Bantuan / Perawatan Khusus dirumah Setelah Keluar dari RS</label>
									<select id="memerlukan_perawatn_khusus" name="memerlukan_perawatn_khusus" class="<?=($memerlukan_perawatn_khusus!=$memerlukan_perawatn_khusus_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_perawatn_khusus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(278) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_perawatn_khusus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_perawatn_khusus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_perawatn_khusus_detail">Detail Apakah Pasien Memerlukan Bantuan / Perawatan Khusus dirumah Setelah Keluar dari RS</label>
									<input id="memerlukan_perawatn_khusus_detail" class="<?=($memerlukan_perawatn_khusus_detail!=$memerlukan_perawatn_khusus_detail_asal?'edited':'')?> form-control "  type="text"  value="{memerlukan_perawatn_khusus_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="memerlukan_kebutuhan_pribadi">Apakah pasien Bermasalah Dalam Memenuhi kebutuhan Pribadinya seteleh Keluar dari RS (Makan, minum, BAB, BAK dll)</label>
									<select id="memerlukan_kebutuhan_pribadi" name="memerlukan_kebutuhan_pribadi" class="<?=($memerlukan_kebutuhan_pribadi!=$memerlukan_kebutuhan_pribadi_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_kebutuhan_pribadi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(279) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_kebutuhan_pribadi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_kebutuhan_pribadi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_kebutuhan_pribadi_detail">Detail Apakah pasien Bermasalah Dalam Memenuhi kebutuhan Pribadinya seteleh Keluar dari RS (Makan, minum, BAB, BAK dll)</label>
									<input id="memerlukan_kebutuhan_pribadi_detail" class="<?=($memerlukan_kebutuhan_pribadi_detail!=$memerlukan_kebutuhan_pribadi_detail_asal?'edited':'')?> form-control "  type="text"  value="{memerlukan_kebutuhan_pribadi_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="memiliki_nyeri_kronis">Apakah Pasien Memiliki Nyeri Kkronis dan Kelelahan Setelah Keluar dari RS</label>
									<select id="memiliki_nyeri_kronis" name="memiliki_nyeri_kronis" class="<?=($memiliki_nyeri_kronis!=$memiliki_nyeri_kronis_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memiliki_nyeri_kronis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(280) as $row){?>
										<option value="<?=$row->id?>"  <?=($memiliki_nyeri_kronis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memiliki_nyeri_kronis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memiliki_nyeri_kronis_detail">Detail Apakah Pasien Memiliki Nyeri Kkronis dan Kelelahan Setelah Keluar dari RS</label>
									<input id="memiliki_nyeri_kronis_detail" class="<?=($memiliki_nyeri_kronis_detail!=$memiliki_nyeri_kronis_detail_asal?'edited':'')?> form-control "  type="text"  value="{memiliki_nyeri_kronis_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="memerlukan_edukasi">Apakah pasien dan Keluarga memerlukan edukasi kesehatan setelah keluar dari RS ? (Obat-obatan, efek samping obat, nyeri dll)</label>
									<select id="memerlukan_edukasi" name="memerlukan_edukasi" class="<?=($memerlukan_edukasi!=$memerlukan_edukasi_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_edukasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(281) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_edukasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_edukasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_edukasi_detail">Detail Apakah pasien dan Keluarga memerlukan edukasi kesehatan setelah keluar dari RS ? (Obat-obatan, efek samping obat, nyeri dll)</label>
									<input id="memerlukan_edukasi_detail" class="<?=($memerlukan_edukasi_detail!=$memerlukan_edukasi_detail_asal?'edited':'')?> form-control "  type="text"  value="{memerlukan_edukasi_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px">
								<div class="col-md-6">
									<label for="memerlukan_ket_khusus">Apakah Pasien Memerlukan keterampilan khusus  Setelah Keluar dari RS ? (Perawatan Luka, Injeksi, Perawatan Bayi dll)</label>
									<select id="memerlukan_ket_khusus" name="memerlukan_ket_khusus" class="<?=($memerlukan_ket_khusus!=$memerlukan_ket_khusus_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_ket_khusus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(282) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_ket_khusus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_ket_khusus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_ket_khusus_detail">Detail Apakah Pasien Memerlukan keterampilan khusus  Setelah Keluar dari RS ? (Perawatan Luka, Injeksi, Perawatan Bayi dll)</label>
									<input id="memerlukan_ket_khusus_detail" class="<?=($memerlukan_ket_khusus_detail!=$memerlukan_ket_khusus_detail_asal?'edited':'')?> form-control "  type="text"  value="{memerlukan_ket_khusus_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >CATATAN TAMBAHAN APABILA ADA PERUBAHAN PERENCANAAN PULANG</h5>
							</div>
						</div>						
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<table style="width:100%" class="table table-bordered" id="tabel_catatan">
										<thead>
											<tr>
												<th width="20%" colspan="4"></th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	
	$("#tanggal_masuk_rs").datetimepicker({
            format: "DD-MM-YYYY HH:mm",
            // stepping: 30
        });
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		list_index_history_edit();
		load_catatan_discarge();
		load_awal_assesmen=false;
	}
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_discarge', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function load_catatan_discarge(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_catatan tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_catatan_discarge', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				st_sedang_edit:$("#st_sedang_edit").val(),
				
				},
		success: function(data) {
			$("#tabel_catatan tbody").append(data.opsi);
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#gambar_id").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".his_filter").removeAttr('disabled');
	}
}

</script>