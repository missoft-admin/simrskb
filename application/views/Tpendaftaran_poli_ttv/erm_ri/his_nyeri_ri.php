<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1;
	}
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='his_nyeri_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_nyeri_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_nyeri=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_nyeri=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="total_skor_nyeri" value="<?=$total_skor_nyeri?>" >		
						<input type="hidden" id="mnyeri_id" value="<?=$mnyeri_id?>" >	
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >							
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
										<button class="btn btn-default btn_back"  type="button" onclick="goBack()"><i class="fa fa-reply"></i> Kembali</button>
									<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id?>/erm_ri/input_nyeri_ri" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-12">
								<div class="form-group" >
									<table class="block-table text-center ">
										<tbody>
											<tr>
												<td class="border-r" style="width:70%">
													<div>
														<div class="input-group">
															<input class="form-control" disabled type="text" readonly value="<?=$nama_kajian?>" id="nama_kajian" placeholder="nama_kajian" >
															<span class="input-group-addon"><i class="fa fa-user"></i></span>
														</div>
													</div>
													<div class="h5 font-w700 text-left text-primary push-5-t"><?=$isi_header?></div>
												</td>
												<td class="border-r"  style="width:30%">
													<div class="h1 font-w700 <?=($total_skor_nyeri!=$total_skor_nyeri_asal?'text-danger':'text-primary')?>" id="div_total_skor_nyeri"><?=$total_skor_nyeri?></div>
													<div class="h5 text-uppercase push-5-t">Total Skor Skala Nyeri</div>
												</td>
											</tr>
										</tbody>
									</table>
									
								</div>
								
							</div>
						</div>
						<?if ($mnyeri_id=='1'){?>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-12">
									<div id="div_tabel_nrs" class="form-group">
										
									</div>
								</div>
							</div>
						</div>
						<?}else{?>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<table class="table table-bordered" id="tabel_non_nrs">
											<thead>
												<tr>
													<th width="10%">No</th>
													<th width="40%">Pengkajian</th>
													<th width="30%">Jawaban</th>
													<th width="20%">Skor</th>
												</tr>
											</thead>
											<tbody>
												<!-- {{ @action }} -->
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						
						<?}?>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Lokasi Nyeri</label>
									<select tabindex="8" id="lokasi_nyeri" name="lokasi_nyeri" class="<?=($lokasi_nyeri!=$lokasi_nyeri_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($lokasi_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(44) as $row){?>
										<option value="<?=$row->id?>" <?=($lokasi_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($lokasi_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penyebab Nyeri</label>
									<select tabindex="8" id="penyebab_nyeri" name="penyebab_nyeri" class="<?=($penyebab_nyeri!=$penyebab_nyeri_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($penyebab_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(45) as $row){?>
										<option value="<?=$row->id?>" <?=($penyebab_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($penyebab_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Durasi Nyeri</label>
									<select tabindex="8" id="durasi_nyeri" name="durasi_nyeri" class="<?=($durasi_nyeri!=$durasi_nyeri_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($durasi_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(46) as $row){?>
										<option value="<?=$row->id?>" <?=($durasi_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($durasi_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Frekuensi Nyeri</label>
									<select tabindex="8" id="frek_nyeri" name="frek_nyeri" class="<?=($frek_nyeri!=$frek_nyeri_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($frek_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(47) as $row){?>
										<option value="<?=$row->id?>" <?=($frek_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($frek_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Jenis Nyeri</label>
									<select tabindex="8" id="jenis_nyeri" name="jenis_nyeri" class="<?=($jenis_nyeri!=$jenis_nyeri_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(48) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Skala Nyeri</label>
									<select tabindex="8" id="skala_nyeri" name="skala_nyeri" class="<?=($skala_nyeri!=$skala_nyeri_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($skala_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(49) as $row){?>
										<option value="<?=$row->id?>" <?=($skala_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($skala_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Karakteristik</label>
									<select tabindex="8" id="karakteristik_nyeri" name="karakteristik_nyeri" class="<?=($karakteristik_nyeri!=$karakteristik_nyeri_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($karakteristik_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(50) as $row){?>
										<option value="<?=$row->id?>" <?=($karakteristik_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($karakteristik_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Nyeri Hilang Bila</label>
									<select tabindex="8" id="nyeri_hilang" name="nyeri_hilang" class="<?=($nyeri_hilang!=$nyeri_hilang_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($nyeri_hilang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(51) as $row){?>
										<option value="<?=$row->id?>" <?=($nyeri_hilang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($nyeri_hilang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label class="text-primary" ><?=$isi_footer?></h5>
								</div>
								
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;

$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		if ($("#mnyeri_id").val()=='1'){
			load_nrs();
			
		}else{
			load_non_nrs();
		}
		
		load_awal_assesmen=false;
	}
	list_index_history_edit();
});
$(document).on('click','.data_asuhan',function(){
	 event.preventDefault();
// alert('Break');
  
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "20%", "targets": [1] },
					 // { "width": "15%", "targets": [3] },
					 // { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_nyeri_ri', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}

function disabel_edit(){
	// if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".his_filter").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $('.edited').css('background', '#fff2f1');
		 $(".btn_back").removeAttr('disabled');
		 // $(".nilai").removeAttr('disabled');
	// }
}
function load_non_nrs(){
	assesmen_id=$("#assesmen_id").val();
	versi_edit=$("#versi_edit").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_nilai_param_nyeri_ri', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				versi_edit:versi_edit,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#tabel_non_nrs tbody").empty();
			$("#tabel_non_nrs tbody").append(data.tabel);
			// $(".nilai_nyeri").select2();
			$("#total_skor_nyeri").val(data.total_skor);
			$("#div_total_skor_nyeri").text(data.total_skor);
		}
	});
}
$(document).on("change",".nilai_nyeri",function(){
	assesmen_id=$("#assesmen_id").val();
	var tr=$(this).closest('tr');
	var parameter_id=tr.find(".parameter_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/update_nilai_nyeri/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				parameter_id:parameter_id,nilai_id:nilai_id,assesmen_id:assesmen_id
		  },success: function(data) {
			  load_non_nrs();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
		  }
		});
	
});
function load_nrs(){
	assesmen_id=$("#assesmen_id").val();
	versi_edit=$("#versi_edit").val();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_nrs_ri', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				versi_edit:versi_edit,
			   },
		success: function(data) {
			$("#div_tabel_nrs").append(data);
		}
	});
}
	function goBack() {
	  window.history.back();
	}

</script>