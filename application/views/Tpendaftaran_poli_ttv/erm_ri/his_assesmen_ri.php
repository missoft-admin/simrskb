<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	 .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }
	  <?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>

<?if ($menu_kiri=='his_assesmen_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_assesmen_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
						$disabel_input='';
						$disabel_input_ttv='';
						// if ($status_ttv!='1'){
							// $disabel_input_ttv='disabled';
						// }
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_assesmen=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$jml_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_assesmen_ri" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-xs-12" for="st_owned">Tanggal</label>
											<div class="col-xs-12">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="st_owned">Owned By Me</label>
											<div class="col-xs-12">
												<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0">TIDAK</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
											<div class="col-xs-12">
												<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
											<div class="col-xs-12">
												
												<div class="input-group">
                                                    <select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
                                                    </span>
                                                </div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_pencarian_history">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="10%">Action</th>
													<th width="20%">Versi</th>
													<th width="20%">Created</th>
													<th width="20%">Edited</th>
													<th width="25%">Alasan</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >PENGKAJIAN </h5>
							</div>
						</div>
						<input class="form-control"  type="hidden" id="st_anamnesa" name="st_anamnesa" value="<?=$st_anamnesa?>" >				
								
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="col-xs-12">Keluhan Utama </label>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" value="1" id="auto_anamnesa" name="anamnesa" onclick="set_auto()" <?=($st_anamnesa=='1'?'checked':'')?>><span></span> Auto Anamnesis
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  value="1" id="allo_anamnesa" name="anamnesa" onclick="set_allo()" <?=($st_anamnesa=='2'?'checked':'')?>><span></span> Allo Anamnesis
									</label>
									
								</div>
								<div class="col-md-4">
										<input class="allo_anamnesa form-control  push-5-r <?=($nama_anamnesa!=$nama_anamnesa_asal?'edited':'')?>" id="nama_anamnesa"  type="text"  name="nama_anamnesa" value="{nama_anamnesa}" placeholder="Tuliskan Nama" >
								</div>
								<div class="col-md-4">
										<input class="allo_anamnesa form-control <?=($hubungan_anamnesa!=$hubungan_anamnesa_asal?'edited':'')?>" id="hubungan_anamnesa"  type="text"  name="hubungan_anamnesa" value="{hubungan_anamnesa}" placeholder="Hubungan" >
								</div>
								<div class="col-md-12 push-10-t">
									<textarea  class="form-control <?=($keluhan_utama!=$keluhan_utama_asal?'edited':'')?>" id="keluhan_utama" name="story" rows="2" style="width:100%"><?=$keluhan_utama?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit" name="st_riwayat_penyakit" value="<?=$st_riwayat_penyakit?>" >
							
						</div>
						<?
						$riwayat_penyakit=($riwayat_penyakit?explode(',',$riwayat_penyakit):array());
						$keluhan_utama_list=($keluhan_utama_list?explode(',',$keluhan_utama_list):array());
						$riwayat_penyakit_pernah_list=($riwayat_penyakit_pernah_list?explode(',',$riwayat_penyakit_pernah_list):array());
						$pengobatan_list=($pengobatan_list?explode(',',$pengobatan_list):array());
						?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >RIWAYAT PENYAKIT SEKARANG DAN PENGOBATAN </h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit Sekarang </label>
								
								<div class="col-md-12">
									<select id="riwayat_penyakit" name="riwayat_penyakit" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach(list_variable_ref(132) as $r){?>
										<option value="<?=$r->id?>" <?=(in_array($r->id,$riwayat_penyakit)?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_lainnya" class="form-control <?=($riwayat_penyakit_lainnya!=$riwayat_penyakit_lainnya_asal?'edited':'')?>" name="riwayat_penyakit_lainnya" rows="2" style="width:100%"><?=$riwayat_penyakit_lainnya?></textarea>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<label class="col-xs-12">Keluhan Utama </label>
								
								<div class="col-md-12">
									<select id="keluhan_utama_list" name="keluhan_utama_list" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach(list_variable_ref(133) as $r){?>
										<option value="<?=$r->id?>" <?=(in_array($r->id,$keluhan_utama_list)?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="keluhan_utama_text" class="form-control <?=($keluhan_utama_text!=$keluhan_utama_text_asal?'edited':'')?>" name="keluhan_utama_text" rows="2" style="width:100%"><?=$keluhan_utama_text?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >RIWAYAT PENYAKIT YANG PERNAH DIALAMI </h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Sakit Yang Dialami </label>
								<div class="col-md-12">
									<select id="riwayat_penyakit_pernah_list" name="riwayat_penyakit_pernah_list" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach(list_variable_ref(134) as $r){?>
										<option value="<?=$r->id?>" <?=(in_array($r->id,$riwayat_penyakit_pernah_list)?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_pernah" class="form-control <?=($riwayat_penyakit_pernah!=$riwayat_penyakit_pernah_asal?'edited':'')?>" name="riwayat_penyakit_pernah" rows="2" style="width:100%"><?=$riwayat_penyakit_pernah?></textarea>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<label class="col-xs-12">Pengobatan / Perawatan yang didapat </label>
								
								<div class="col-md-12">
									<select id="pengobatan_list" name="pengobatan_list" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach(list_variable_ref(135) as $r){?>
										<option value="<?=$r->id?>" <?=(in_array($r->id,$pengobatan_list)?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="pengobatan_keterangan" class="form-control <?=($pengobatan_keterangan!=$pengobatan_keterangan_asal?'edited':'')?>" name="pengobatan_keterangan" rows="2" style="width:100%"><?=$pengobatan_keterangan?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Alergi </label>
								<div class="col-md-12">
									<select id="riwayat_alergi" name="riwayat_alergi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_alergi=='0'?'selected':'')?>>Tidak Ada Alergi</option>
										<option value="1" <?=($riwayat_alergi=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_alergi=='2'?'selected':'')?>>Ada Alergi</option>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
							</div>
						</div>
						<div class="form-group div_alergi_all">
							<div class="col-md-12 div_input_alergi">
								<div class="col-md-3 ">
									<input type="hidden" readonly class="form-control input-sm" id="alergi_id" value=""> 
									<label for="example-input-normal">Jenis Alergi</label>
									<select id="input_jenis_alergi" name="input_jenis_alergi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
										<?foreach(get_all('merm_referensi',array('ref_head_id'=>24,'status'=>1)) as $r){?>
										<option value="<?=$r->nilai?>"><?=$r->ref?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="example-input-normal">Detail Alergi</label>
									<input class="form-control" type="text" id="input_detail_alergi" name="input_detail_alergi" placeholder="Detail Alergi">
								</div>
								<div class="col-md-5 ">
									<label for="example-input-normal">Reaksi</label>
									<div class="input-group">
										<input class="form-control" type="text" id="input_reaksi_alergi" name="input_reaksi_alergi" placeholder="Reaksi Alergi">
										<span class="input-group-btn">
											<button class="btn btn-info" onclick="simpan_alergi()" id="btn_add_alergi" type="button"><i class="fa fa-plus"></i> Add</button>
											<button class="btn btn-warning" onclick="clear_input_alergi()" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
									
								</div>
							</div>
							
						</div>
						<div class="form-group div_alergi_all">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="block">
										<ul class="nav nav-tabs nav-tabs-alt nav-justified" data-toggle="tabs">
											<li class="" id="li_alergi_1">
												<a href="#alergi_1"  onclick="load_alergi()"><i class="fa fa-pencil "></i> Data Alergi</a>
											</li>
											<li class="" id="li_alergi_2">
												<a href=" #alergi_2" onclick="load_alergi_his()"><i class="fa fa-history"></i> History Alergi</a>
											</li>
											
										</ul>
										<div class="block-content tab-content ">
											<div class="tab-pane " id="alergi_1">
												<div class="table-responsive div_data_alergi">
													<table class="table" id="index_alergi">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="tab-pane " id="alergi_2">
												<div class="table-responsive">
													<table class="table" id="index_alergi_his">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							
							</div>
							<div class="col-md-6 ">
									
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >RIWAYAT PENYAKIT KELUARGA DAN HUBUNGAN DENGAN PASIEN </h5>
								<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Riwayat Penyakit Menurun</label>
									<select id="st_riwayat_penyakit_menurun" name="st_riwayat_penyakit_menurun" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($st_riwayat_penyakit_menurun=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($st_riwayat_penyakit_menurun=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($st_riwayat_penyakit_menurun=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_menurun_all push-10-t">
									<div class="form-group div_input_menurun">
										<div class="col-md-12 ">
											<div class="table-responsive">
												<table class="table table-condensed table-bordered"" id="index_menurun">
													<thead>
														<tr>
															<th width="30%" class="text-center">Penyakit<input type="hidden" id="penyakit_menurun_id" value="{penyakit_menurun_id}"><input type="hidden" id="penyakit_menurun_id_nama" value="{penyakit_menurun_id_nama}"></th>
															<th width="30%" class="text-center">Penderita<input  type="hidden" id="nama_orang_penyakit_menurun" value="{nama_orang_penyakit_menurun}"></th>
															<th width="25%" class="text-center">Status<input type="hidden" id="status_penyakit_menurun" value="{status_penyakit_menurun}"><input type="hidden" id="status_penyakit_menurun_nama" value="{status_penyakit_menurun_nama}"></th>
															<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_menurun"></th>
														</tr>
														<tr>
															<th width="30%" class="text-center">
																<select id="penyakit_menurun_id_var" class="js-select2 form-control text-center" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="#" selected>-Penyakit-</option>
																	<?foreach(list_variable_ref(136) as $r){?>
																		<option value="<?=$r->id?>"><?=$r->nama?></option>
																	<?}?>
																</select>
															</th>
															<th width="30%" class="text-center">
																<input class="form-control text-center" type="text" id="nama_orang_penyakit_menurun_var" style="width: 100%;" placeholder="Siapa">
															</th>
															<th width="20%" class="text-center">
																<select id="status_penyakit_menurun_var" class="js-select2 form-control text-center" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="#" selected>-Status-</option>
																	<?foreach(list_variable_ref(138) as $r){?>
																		<option value="<?=$r->id?>"><?=$r->nama?></option>
																	<?}?>
																</select>
															</th>
															<th width="20%" class="text-center">
																<span class="input-group-btn">
																	<button class="btn btn-info btn-sm" onclick="simpan_menurun()" id="btn_add_menurun" type="button"><i class="fa fa-plus"></i></button>
																	<button class="btn btn-warning  btn-sm" onclick="clear_input_menurun()" type="button"><i class="fa fa-refresh"></i></button>
																</span>
															</th>
														</tr>
														
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
									
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Riwayat Penyakit Menular</label>
									<select id="st_riwayat_penyakit_menular" name="st_riwayat_penyakit_menular" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($st_riwayat_penyakit_menular=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($st_riwayat_penyakit_menular=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($st_riwayat_penyakit_menular=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_menular_all push-10-t">
									<div class="form-group div_input_menular">
										<div class="col-md-12 ">
											<div class="table-responsive">
												<table class="table table-condensed table-bordered"" id="index_menular">
													<thead>
														<tr>
															<th width="30%" class="text-center">Penyakit<input type="hidden" id="penyakit_menular_id" value="{penyakit_menular_id}"><input type="hidden" id="penyakit_menular_id_nama" value="{penyakit_menular_id_nama}"></th>
															<th width="30%" class="text-center">Penderita<input  type="hidden" id="nama_orang_penyakit_menular" value="{nama_orang_penyakit_menular}"></th>
															<th width="25%" class="text-center">Status<input type="hidden" id="status_penyakit_menular" value="{status_penyakit_menular}"><input type="hidden" id="status_penyakit_menular_nama" value="{status_penyakit_menular_nama}"></th>
															<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_menular"></th>
														</tr>
														<tr>
															<th width="30%" class="text-center">
																<select id="penyakit_menular_id_var" class="js-select2 form-control text-center" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="#" selected>-Penyakit-</option>
																	<?foreach(list_variable_ref(137) as $r){?>
																		<option value="<?=$r->id?>"><?=$r->nama?></option>
																	<?}?>
																</select>
															</th>
															<th width="30%" class="text-center">
																<input class="form-control text-center" type="text" id="nama_orang_penyakit_menular_var" style="width: 100%;" placeholder="Siapa">
															</th>
															<th width="20%" class="text-center">
																<select id="status_penyakit_menular_var" class="js-select2 form-control text-center" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="#" selected>-Status-</option>
																	<?foreach(list_variable_ref(139) as $r){?>
																		<option value="<?=$r->id?>"><?=$r->nama?></option>
																	<?}?>
																</select>
															</th>
															<th width="20%" class="text-center">
																<span class="input-group-btn">
																	<button class="btn btn-info btn-sm" onclick="simpan_menular()" id="btn_add_menular" type="button"><i class="fa fa-plus"></i></button>
																	<button class="btn btn-warning  btn-sm" onclick="clear_input_menular()" type="button"><i class="fa fa-refresh"></i></button>
																</span>
															</th>
														</tr>
														
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >PROTEKSI / PERLINDUNGAN KESEHATAN</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-15px;!important">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Pengetahuan & Kemampuan yang dimiliki pasien dan keluarganya tentang sakitnya dan upaya menjaga kesehatan diri</label>
									<select  id="proteksi_kesehatan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($proteksi_kesehatan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(140) as $row){?>
										<option value="<?=$row->id?>"  <?=($proteksi_kesehatan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($proteksi_kesehatan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Keterangan<br>&nbsp;</label>
									<input class="form-control auto_blur" type="text" id="proteksi_keterangan" style="width: 100%;" placeholder="Keterangan" value="{proteksi_keterangan}">
								</div>
								
							</div>
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								<label class="text-primary" >TANDA VITAL </h5>
								<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Tingkat Kesadaran</label>
									<select  id="tingkat_kesadaran" style="background-color:#d0f3df;" name="tingkat_kesadaran" class="<?=($tingkat_kesadaran!=$tingkat_kesadaran_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-8 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal <?=($td_sistole !=$td_sistole_asal?'edited':'') ?>" type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal <?=($td_diastole !=$td_diastole_asal?'edited':'') ?>" type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal <?=($suhu !=$suhu_asal?'edited':'') ?>"  type="text" id="suhu"  value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								
								
								
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nadi !=$nadi_asal?'edited':'') ?>"  type="text" id="nadi" name="nadi" value="{nadi}"  placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nafas !=$nafas_asal?'edited':'') ?>"  type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Nafas" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Keadaan Umum</label>
									<select  id="keadaan_umum" name="keadaan_umum" class=" form-control <?=($keadaan_umum!=$keadaan_umum_asal?'edited':'')?>" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(130) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_umum == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_umum == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Keadaan Gizi</label>
									<select  id="keadaan_gizi" name="keadaan_gizi" class=" form-control <?=($keadaan_gizi!=$keadaan_gizi_asal?'edited':'')?>" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(131) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_gizi == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_gizi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >SYSTEM PERNAPASAN</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="batuk">Batuk</label>
									<select  id="batuk"  class="form-control <?=($batuk!=$batuk_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($batuk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(141) as $row){?>
										<option value="<?=$row->id?>" <?=($batuk == $row->id ? 'selected="selected"' : '')?> <?=($batuk == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="warna_sputum">Warna Sputum</label>
									<select  id="warna_sputum"  class="form-control <?=($warna_sputum!=$warna_sputum_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($warna_sputum == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(142) as $row){?>
										<option value="<?=$row->id?>" <?=($warna_sputum == $row->id ? 'selected="selected"' : '')?> <?=($warna_sputum == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="bunyi_nafas">Bunyi Nafas</label>
									<select  id="bunyi_nafas"  class="form-control <?=($bunyi_nafas!=$bunyi_nafas_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bunyi_nafas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(143) as $row){?>
										<option value="<?=$row->id?>" <?=($bunyi_nafas == $row->id ? 'selected="selected"' : '')?> <?=($bunyi_nafas == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="sesak_nafas_saat">Sesak nafas pada Saat</label>
									<select  id="sesak_nafas_saat"  class="form-control <?=($sesak_nafas_saat!=$sesak_nafas_saat_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sesak_nafas_saat == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(144) as $row){?>
										<option value="<?=$row->id?>" <?=($sesak_nafas_saat == $row->id ? 'selected="selected"' : '')?> <?=($sesak_nafas_saat == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="bentuk_dada">Bentuk Dada</label>
									<select  id="bentuk_dada"  class="form-control <?=($bentuk_dada!=$bentuk_dada_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bentuk_dada == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(145) as $row){?>
										<option value="<?=$row->id?>" <?=($bentuk_dada == $row->id ? 'selected="selected"' : '')?> <?=($bentuk_dada == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="tipe_pernafasan_hidung">Type Pernapasan Hidung</label>
									<select  id="tipe_pernafasan_hidung"  class="form-control <?=($tipe_pernafasan_hidung!=$tipe_pernafasan_hidung_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($tipe_pernafasan_hidung == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(146) as $row){?>
										<option value="<?=$row->id?>" <?=($tipe_pernafasan_hidung == $row->id ? 'selected="selected"' : '')?> <?=($tipe_pernafasan_hidung == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
								
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="alat_bantu_nafas">Alat Bantu Pernapasan</label>
									<select  id="alat_bantu_nafas"  class="form-control <?=($alat_bantu_nafas!=$alat_bantu_nafas_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($alat_bantu_nafas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(147) as $row){?>
										<option value="<?=$row->id?>" <?=($alat_bantu_nafas == $row->id ? 'selected="selected"' : '')?> <?=($alat_bantu_nafas == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="merokok">Kebiasan Merokok</label>
									<select  id="merokok"  class="form-control <?=($merokok!=$merokok_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($merokok == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(148) as $row){?>
										<option value="<?=$row->id?>" <?=($merokok == $row->id ? 'selected="selected"' : '')?> <?=($merokok == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="merokok_sejak">Sejak</label>
									<input class="form-control <?=($merokok_sejak!=$merokok_sejak_asal?'edited':'')?>" type="text" id="merokok_sejak" style="width: 100%;" placeholder="Keterangan" value="{merokok_sejak}">
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="merokok_per_hari">Banyak Rokok / Hari</label>
									<input class="form-control number <?=($merokok_per_hari!=$merokok_per_hari_asal?'edited':'')?>" type="text" id="merokok_per_hari" style="width: 100%;" placeholder="Banyak Rokok / Hari" value="{merokok_per_hari}">
								</div>
								<div class="col-md-4 ">
									<label for="jenis_rokok">Jenis Rokok</label>
									<input class="form-control <?=($jenis_rokok!=$jenis_rokok_asal?'edited':'')?>" type="text" id="jenis_rokok" style="width: 100%;" placeholder="Jenis Rokok" value="{jenis_rokok}">
								</div>
								<div class="col-md-4 ">
									<label for="keluhan_lain">Keluhan lain</label>
									<input class="form-control <?=($keluhan_lain!=$keluhan_lain_asal?'edited':'')?>" type="text" id="keluhan_lain" style="width: 100%;" placeholder="Keluhan lain" value="{keluhan_lain}">
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >SYSTEM JANTUNG, PEREDARAN DARAH DAN KELENJAR LIMFE</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="nyeri_dada">Nyeri Dada</label>
									<select  id="nyeri_dada"  class="form-control <?=($nyeri_dada!=$nyeri_dada_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($nyeri_dada == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(149) as $row){?>
										<option value="<?=$row->id?>" <?=($nyeri_dada == $row->id ? 'selected="selected"' : '')?> <?=($nyeri_dada == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="palpitasi">Palpitasi</label>
									<select  id="palpitasi"  class="form-control <?=($palpitasi!=$palpitasi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($palpitasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(150) as $row){?>
										<option value="<?=$row->id?>" <?=($palpitasi == $row->id ? 'selected="selected"' : '')?> <?=($palpitasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="pusing">Pusing</label>
									<select  id="pusing"  class="form-control <?=($pusing!=$pusing_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pusing == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(151) as $row){?>
										<option value="<?=$row->id?>" <?=($pusing == $row->id ? 'selected="selected"' : '')?> <?=($pusing == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="pingsan">Pingsan</label>
									<select  id="pingsan"  class="form-control <?=($pingsan!=$pingsan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pingsan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(152) as $row){?>
										<option value="<?=$row->id?>" <?=($pingsan == $row->id ? 'selected="selected"' : '')?> <?=($pingsan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="pendarahan_pada">Pendarahan Pada</label>
									<select  id="pendarahan_pada"  class="form-control <?=($pendarahan_pada!=$pendarahan_pada_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pendarahan_pada == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(153) as $row){?>
										<option value="<?=$row->id?>" <?=($pendarahan_pada == $row->id ? 'selected="selected"' : '')?> <?=($pendarahan_pada == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="ket_pendarahan">Keterangan Pendarahan</label>
									<input class="form-control <?=($ket_pendarahan!=$ket_pendarahan_asal?'edited':'') ?>" type="text" id="ket_pendarahan" style="width: 100%;" placeholder="Ket. Pendarahan" value="{ket_pendarahan}">
								</div>
								
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="oedama">Oedama Pada</label>
									<select  id="oedama"  class="form-control <?=($oedama!=$oedama_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($oedama == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(154) as $row){?>
										<option value="<?=$row->id?>" <?=($oedama == $row->id ? 'selected="selected"' : '')?> <?=($oedama == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="palpitasi">Keterangan Oedama</label>
									<input class="form-control <?=($oedama_ket!=$oedama_ket_asal?'edited':'') ?>" type="text" id="oedama_ket" style="width: 100%;" placeholder="Oedama" value="{oedama_ket}">
								</div>
								<div class="col-md-4 ">
									<label for="pusing">Hematoma Pada</label>
									<select  id="hematoma"  class="form-control <?=($hematoma!=$hematoma_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($hematoma == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(155) as $row){?>
										<option value="<?=$row->id?>" <?=($hematoma == $row->id ? 'selected="selected"' : '')?> <?=($hematoma == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="pingsan">Keterangan Hematoma</label>
									<input class="form-control <?=($hematoma_ket!=$hematoma_ket_asal?'edited':'') ?>" type="text" id="hematoma_ket" style="width: 100%;" placeholder="Hematoma" value="{hematoma_ket}">
								</div>
								<div class="col-md-4 ">
									<label for="piechi">Piechi Pada</label>
									<select  id="piechi"  class="form-control <?=($piechi!=$piechi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($piechi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(156) as $row){?>
										<option value="<?=$row->id?>" <?=($piechi == $row->id ? 'selected="selected"' : '')?> <?=($piechi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="piechi_ket">Sebutkan</label>
									<input class="form-control <?=($piechi_ket!=$piechi_ket_asal?'edited':'') ?>" type="text" id="piechi_ket" style="width: 100%;" placeholder="Sebutkan" value="{piechi_ket}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="ics">Tampak Di ICS</label>
									<select  id="ics"  class="form-control <?=($ics!=$ics_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($ics == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(157) as $row){?>
										<option value="<?=$row->id?>" <?=($ics == $row->id ? 'selected="selected"' : '')?> <?=($ics == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="palpitasi">Keterangan ICS</label>
									<input class="form-control <?=($ics_ket!=$ics_ket_asal?'edited':'') ?>" type="text" id="ics_ket" style="width: 100%;" placeholder="Keterangan ICS" value="{ics_ket}">
								</div>
								<div class="col-md-4 ">
									<label for="capitary">Capilary RefillPada</label>
									<select  id="capitary"  class="form-control <?=($capitary!=$capitary_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($capitary == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(158) as $row){?>
										<option value="<?=$row->id?>" <?=($capitary == $row->id ? 'selected="selected"' : '')?> <?=($capitary == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="capitary_ket">Keterangan Capilary Reffil</label>
									<input class="form-control <?=($capitary_ket!=$capitary_ket_asal?'edited':'') ?>" type="text" id="capitary_ket" style="width: 100%;" placeholder="Capilary" value="{capitary_ket}">
								</div>
								<div class="col-md-4 ">
									<label for="limfe">Pembesaran Kelenjar Limfe</label>
									<select  id="limfe"  class="form-control <?=($limfe!=$limfe_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($limfe == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(159) as $row){?>
										<option value="<?=$row->id?>" <?=($limfe == $row->id ? 'selected="selected"' : '')?> <?=($limfe == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="limfe_lokasi">Lokasi</label>
									<input class="form-control <?=($limfe_lokasi!=$limfe_lokasi_asal?'edited':'') ?>" type="text" id="limfe_lokasi" style="width: 100%;" placeholder="Lokasi" value="{limfe_lokasi}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="cordis">Ikuts Cordis, Alat Bantu</label>
									<select  id="cordis"  class="form-control <?=($cordis!=$cordis_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($cordis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(160) as $row){?>
										<option value="<?=$row->id?>" <?=($cordis == $row->id ? 'selected="selected"' : '')?> <?=($cordis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="palpitasi">Jenis Alat Bantu</label>
									<input class="form-control <?=($jenis_alat_bantu!=$jenis_alat_bantu_asal?'edited':'') ?>" type="text" id="jenis_alat_bantu" style="width: 100%;" placeholder="Jenis Alat Bantu" value="{jenis_alat_bantu}">
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="kelainan_cordis">Kelainan / Keluhan Lain</label>
									<input class="form-control <?=($kelainan_cordis!=$kelainan_cordis_asal?'edited':'') ?>" type="text" id="kelainan_cordis" style="width: 100%;" placeholder="Keluhan Lain" value="{kelainan_cordis}">
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >SKRINING NUTRISI</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Pengkajian Nutrisi</label>
									<select  id="skrining_nutrisi_id"  class="form-control <?=($skrining_nutrisi_id!=$skrining_nutrisi_id_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($skrining_nutrisi_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(get_all('mskrining_nutrisi',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>" <?=($skrining_nutrisi_id == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Skor Pengkajian</label>
									<input class="form-control <?=($total_skor_nutrisi != $total_skor_nutrisi_asal?'edited':'')?>" readonly type="text" id="total_skor_nutrisi"  value="{total_skor_nutrisi}" placeholder="Skor Pengkajian" >
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Hasil Pengkajian</label>
									<input class="form-control <?=($nama_hasil_pengkajian != $nama_hasil_pengkajian_asal?'edited':'')?>" readonly type="text" id="nama_hasil_pengkajian"  value="{nama_hasil_pengkajian}" placeholder="Hasil Pengkajian" >
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Tindakan</label>
									<input class="form-control <?=($tindakan_nutrisi != $tindakan_nutrisi_asal?'edited':'')?>" readonly type="text" id="tindakan_nutrisi"  value="{tindakan_nutrisi}" placeholder="Tindakan" >
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
											<thead>
												<tr>
													<th width="60%">Skrining</th>
													<th width="30%">Jawaban</th>
													<th width="10%" class="text-center">Skor</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary">SYSTEM PERSYARAFAN</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-7 ">
									<label for="syaraf_sadar">Kesadaran</label>
									<select  id="syaraf_sadar"  class="form-control <?=($syaraf_sadar!=$syaraf_sadar_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($syaraf_sadar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(161) as $row){?>
										<option value="<?=$row->id?>" <?=($syaraf_sadar == $row->id ? 'selected="selected"' : '')?> <?=($syaraf_sadar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-5 ">
									<label for="syaraf_gcs">GCS</label>
									<select  id="syaraf_gcs"  class="form-control <?=($syaraf_gcs!=$syaraf_gcs_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($syaraf_gcs == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(162) as $row){?>
										<option value="<?=$row->id?>" <?=($syaraf_gcs == $row->id ? 'selected="selected"' : '')?> <?=($syaraf_gcs == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="syaraf_motorik">Motorik</label>
									<select  id="syaraf_motorik"  class="form-control <?=($syaraf_motorik!=$syaraf_motorik_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($syaraf_motorik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(163) as $row){?>
										<option value="<?=$row->id?>" <?=($syaraf_motorik == $row->id ? 'selected="selected"' : '')?> <?=($syaraf_motorik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="syaraf_verbal">Verbal</label>
									<select  id="syaraf_verbal"  class="form-control <?=($syaraf_verbal!=$syaraf_verbal_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($syaraf_verbal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(164) as $row){?>
										<option value="<?=$row->id?>" <?=($syaraf_verbal == $row->id ? 'selected="selected"' : '')?> <?=($syaraf_verbal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="syaraf_buka_mata">Reaksi Membuka Mata</label>
									<select  id="syaraf_buka_mata"  class="form-control <?=($syaraf_buka_mata!=$syaraf_buka_mata_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($syaraf_buka_mata == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(165) as $row){?>
										<option value="<?=$row->id?>" <?=($syaraf_buka_mata == $row->id ? 'selected="selected"' : '')?> <?=($syaraf_buka_mata == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								<div class="col-md-12 ">
								<input type="hidden" id="pupil_kiri" value="{pupil_kiri}">
								<input type="hidden" id="pupil_kiri_cahaya" value="{pupil_kiri_cahaya}">
								<input type="hidden" id="pupil_kanan" value="{pupil_kanan}">
								<input type="hidden" id="pupil_kanan_cahaya" value="{pupil_kanan_cahaya}">
								
								<input type="hidden" id="pupil_kiri_asal" value="{pupil_kiri_asal}">
								<input type="hidden" id="pupil_kiri_cahaya_asal" value="{pupil_kiri_cahaya_asal}">
								<input type="hidden" id="pupil_kanan_asal" value="{pupil_kanan_asal}">
								<input type="hidden" id="pupil_kanan_cahaya_asal" value="{pupil_kanan_cahaya_asal}">
								<div id="div_sayaraf">
									
								</div>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="kesemutan">Kesemutan Pada</label>
									<select  id="kesemutan"  class="form-control <?=($kesemutan!=$kesemutan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kesemutan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(167) as $row){?>
										<option value="<?=$row->id?>" <?=($kesemutan == $row->id ? 'selected="selected"' : '')?> <?=($kesemutan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="kejang">Kejang pada</label>
									<select  id="kejang"  class="form-control <?=($kejang!=$kejang_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kejang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(168) as $row){?>
										<option value="<?=$row->id?>" <?=($kejang == $row->id ? 'selected="selected"' : '')?> <?=($kejang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="parase">Parase Pada</label>
									<select  id="parase"  class="form-control <?=($parase!=$parase_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($parase == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(169) as $row){?>
										<option value="<?=$row->id?>" <?=($parase == $row->id ? 'selected="selected"' : '')?> <?=($parase == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="paralise">Paralise Pada</label>
									<select  id="paralise"  class="form-control <?=($paralise!=$paralise_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($paralise == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(170) as $row){?>
										<option value="<?=$row->id?>" <?=($paralise == $row->id ? 'selected="selected"' : '')?> <?=($paralise == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="bingung">Bingung</label>
									<select  id="bingung"  class="form-control <?=($bingung!=$bingung_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bingung == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(171) as $row){?>
										<option value="<?=$row->id?>" <?=($bingung == $row->id ? 'selected="selected"' : '')?> <?=($bingung == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="koordinasi">Tak Ada Koordinasi</label>
									<select  id="koordinasi"  class="form-control <?=($koordinasi!=$koordinasi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($koordinasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(172) as $row){?>
										<option value="<?=$row->id?>" <?=($koordinasi == $row->id ? 'selected="selected"' : '')?> <?=($koordinasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="kaku_duduk">Kaku Duduk</label>
									<select  id="kaku_duduk"  class="form-control <?=($kaku_duduk!=$kaku_duduk_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kaku_duduk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(173) as $row){?>
										<option value="<?=$row->id?>" <?=($kaku_duduk == $row->id ? 'selected="selected"' : '')?> <?=($kaku_duduk == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="trimus">Trimus</label>
									<select  id="trimus"  class="form-control <?=($trimus!=$trimus_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($trimus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(174) as $row){?>
										<option value="<?=$row->id?>" <?=($trimus == $row->id ? 'selected="selected"' : '')?> <?=($trimus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="nyeri_kepala">Nyeri Kepala</label>
									<select  id="nyeri_kepala"  class="form-control <?=($nyeri_kepala!=$nyeri_kepala_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($nyeri_kepala == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(175) as $row){?>
										<option value="<?=$row->id?>" <?=($nyeri_kepala == $row->id ? 'selected="selected"' : '')?> <?=($nyeri_kepala == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-5 ">
									<label for="paralise">Lokasi Nyeri Kepala</label>
									<input class="form-control <?=($nyeri_kepala_lokasi!=$nyeri_kepala_lokasi_asal?'edited':'')?>" type="text" id="nyeri_kepala_lokasi" style="width: 100%;" placeholder="Keterangan" value="{nyeri_kepala_lokasi}">
								</div>
								<div class="col-md-7 ">
									<label for="bingung">Kelainan / Keluhan lain</label>
									<input class="form-control <?=($nyari_kepala_kelainan!=$nyari_kepala_kelainan_asal?'edited':'')?>" type="text" id="nyari_kepala_kelainan" style="width: 100%;" placeholder="Keterangan" value="{nyari_kepala_kelainan}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary">SYSTEM PANCA INDERA [PENGLIHATAN]</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="penglihatan">Penglihatan</label>
									<select  id="penglihatan"  class="form-control <?=($penglihatan!=$penglihatan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($penglihatan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(176) as $row){?>
										<option value="<?=$row->id?>" <?=($penglihatan == $row->id ? 'selected="selected"' : '')?> <?=($penglihatan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="conjugtiva">Conjungtiva</label>
									<select  id="conjugtiva"  class="form-control <?=($conjugtiva!=$conjugtiva_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($conjugtiva == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(177) as $row){?>
										<option value="<?=$row->id?>" <?=($conjugtiva == $row->id ? 'selected="selected"' : '')?> <?=($conjugtiva == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="sklera">Sklera</label>
									<select  id="sklera"  class="form-control <?=($sklera!=$sklera_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sklera == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(178) as $row){?>
										<option value="<?=$row->id?>" <?=($sklera == $row->id ? 'selected="selected"' : '')?> <?=($sklera == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="keadaan_mata">Keadaan Mata</label>
									<select  id="keadaan_mata"  class="form-control <?=($keadaan_mata!=$keadaan_mata_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($keadaan_mata == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(179) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_mata == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_mata == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="kornea">Kornea</label>
									<select  id="kornea"  class="form-control <?=($kornea!=$kornea_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kornea == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(180) as $row){?>
										<option value="<?=$row->id?>" <?=($kornea == $row->id ? 'selected="selected"' : '')?> <?=($kornea == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="alat_bantu_mata">Alat Bantu</label>
									<select  id="alat_bantu_mata"  class="form-control <?=($alat_bantu_mata!=$alat_bantu_mata_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($alat_bantu_mata == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(181) as $row){?>
										<option value="<?=$row->id?>" <?=($alat_bantu_mata == $row->id ? 'selected="selected"' : '')?> <?=($alat_bantu_mata == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary">SYSTEM PANCA INDERA [PENDENGARAN]</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="berdengung">Berdengung</label>
									<select  id="berdengung"  class="form-control <?=($berdengung!=$berdengung_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($berdengung == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(182) as $row){?>
										<option value="<?=$row->id?>" <?=($berdengung == $row->id ? 'selected="selected"' : '')?> <?=($berdengung == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="nyeri_telinga">Nyeri / Sakit</label>
									<select  id="nyeri_telinga"  class="form-control <?=($nyeri_telinga!=$nyeri_telinga_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($nyeri_telinga == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(183) as $row){?>
										<option value="<?=$row->id?>" <?=($nyeri_telinga == $row->id ? 'selected="selected"' : '')?> <?=($nyeri_telinga == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="cairan_telinga">Cairan</label>
									<input class="form-control <?=($cairan_telinga!=$cairan_telinga_asal?'edited':'')?>" type="text" id="cairan_telinga" style="width: 100%;" placeholder="Keterangan" value="{cairan_telinga}">
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="plulurent">Bening / Plulurent</label>
									<select  id="plulurent"  class="form-control <?=($plulurent!=$plulurent_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($plulurent == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(184) as $row){?>
										<option value="<?=$row->id?>" <?=($plulurent == $row->id ? 'selected="selected"' : '')?> <?=($plulurent == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="keadaan_pendengaran">Keadaan Pendengaran</label>
									<select  id="keadaan_pendengaran"  class="form-control <?=($keadaan_pendengaran!=$keadaan_pendengaran_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($keadaan_pendengaran == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(185) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_pendengaran == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_pendengaran == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="keluhan_lain_pendengaran">Kelainan / Keluhan lain</label>
									<input class="form-control <?=($keluhan_lain_pendengaran!=$keluhan_lain_pendengaran_asal?'edited':'')?>" type="text" id="keluhan_lain_pendengaran" style="width: 100%;" placeholder="Kelainan / Keluhan lain" value="{keluhan_lain_pendengaran}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary">SYSTEM PANCA INDERA [PENCIUMAN / PENGHIDU]</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="septum">Septum</label>
									<select  id="septum"  class="form-control <?=($septum!=$septum_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($septum == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(186) as $row){?>
										<option value="<?=$row->id?>" <?=($septum == $row->id ? 'selected="selected"' : '')?> <?=($septum == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="mucosa">Mucosa</label>
									<select  id="mucosa"  class="form-control <?=($mucosa!=$mucosa_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($mucosa == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(187) as $row){?>
										<option value="<?=$row->id?>" <?=($mucosa == $row->id ? 'selected="selected"' : '')?> <?=($mucosa == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="polip">Polip</label>
									<select  id="polip"  class="form-control <?=($polip!=$polip_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($polip == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(244) as $row){?>
										<option value="<?=$row->id?>" <?=($polip == $row->id ? 'selected="selected"' : '')?> <?=($polip == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="epistaxis">Epistaxis</label>
									<select  id="epistaxis"  class="form-control <?=($epistaxis!=$epistaxis_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($epistaxis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(188) as $row){?>
										<option value="<?=$row->id?>" <?=($epistaxis == $row->id ? 'selected="selected"' : '')?> <?=($epistaxis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="sekret">Sekret</label>
									<select  id="sekret"  class="form-control <?=($sekret!=$sekret_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sekret == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(189) as $row){?>
										<option value="<?=$row->id?>" <?=($sekret == $row->id ? 'selected="selected"' : '')?> <?=($sekret == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="kelainan_penciuman">Kelainan / Keluhan lain</label>
									<input class="form-control <?=($kelainan_penciuman!=$kelainan_penciuman_asal?'edited':'')?>" type="text" id="kelainan_penciuman" style="width: 100%;" placeholder="Kelainan / Keluhan lain" value="{keluhan_lain_pendengaran}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary">ELIMINASI</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-12">
							<div class="col-md-4 ">
								<label class="">POLA KEBIASAN</label>
							</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="bak_x">Frekuensi Buang Air Kecil</label>
									<div class="input-group">
										<input class="form-control <?=($bak_x!=$bak_x_asal?'edited':'')?>"  type="text" id="bak_x" value="{bak_x}"  placeholder="">
										<span class="input-group-addon">x/hari</span>
									</div>
								</div>
								<div class="col-md-4 ">
									<label for="bak_warna">Warna Buang Air Kecil</label>
									<input class="form-control <?=($bak_warna!=$bak_warna_asal?'edited':'')?>" type="text" id="bak_warna" style="width: 100%;" placeholder="" value="{bak_warna}">
								</div>
								<div class="col-md-4 ">
									<label for="bak_jumlah">Jumlah Buang Air Kecil</label>
									<input class="form-control <?=($bak_jumlah!=$bak_jumlah_asal?'edited':'')?>" type="text" id="bak_jumlah" style="width: 100%;" placeholder="" value="{bak_jumlah}">
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="bab_freq">Frekuensi BAB</label>
									<div class="input-group">
										<input class="form-control <?=($bab_freq!=$bab_freq_asal?'edited':'')?>"  type="text" id="bab_freq" value="{bab_freq}"  placeholder="">
										<span class="input-group-addon">x/hari</span>
									</div>
								</div>
								<div class="col-md-4 ">
									<label for="bab_konsisten">Konsistensi Buang Air Besar</label>
									<input class="form-control <?=($bab_konsisten!=$bab_konsisten_asal?'edited':'')?>" type="text" id="bab_konsisten" style="width: 100%;" placeholder="" value="{bab_konsisten}">
								</div>
								<div class="col-md-4 ">
									<label for="bab_warna">Warna Buang Air Besar</label>
									<input class="form-control <?=($bab_warna!=$bab_warna_asal?'edited':'')?>" type="text" id="bab_warna" style="width: 100%;" placeholder="" value="{bab_warna}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="obat_pencahar">Obat Pencahar / Makanan </label>
									<input class="form-control <?=($obat_pencahar!=$obat_pencahar_asal?'edited':'')?>" type="text" id="obat_pencahar" style="width: 100%;" placeholder="" value="{obat_pencahar}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:0px;!important">
							<div class="col-md-12">
							<div class="col-md-4 ">
								<label class="">KEADAAN SAAT INI</label>
							</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-15px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="bak_saat_ini">Buang Air Kecil</label>
									<select  id="bak_saat_ini"  class="form-control <?=($bak_saat_ini!=$bak_saat_ini_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bak_saat_ini == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(190) as $row){?>
										<option value="<?=$row->id?>" <?=($bak_saat_ini == $row->id ? 'selected="selected"' : '')?> <?=($bak_saat_ini == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="alat_saat_ini">Alat Bantu</label>
									<input class="form-control <?=($alat_saat_ini!=$alat_saat_ini_asal?'edited':'')?>" type="text" id="alat_saat_ini" style="width: 100%;" placeholder="" value="{alat_saat_ini}">
								</div>
								<div class="col-md-4 ">
									<label for="sejak_saat_ini">Sejak</label>
									<input class="form-control <?=($sejak_saat_ini!=$sejak_saat_ini_asal?'edited':'')?>" type="text" id="sejak_saat_ini" style="width: 100%;" placeholder="" value="{sejak_saat_ini}">
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="bak_x_saat_ini">Frekuensi Buang Air Kecil</label>
									<div class="input-group">
										<input class="form-control <?=($bak_x_saat_ini!=$bak_x_saat_ini_asal?'edited':'')?>"  type="text" id="bak_x_saat_ini" value="{bak_x_saat_ini}"  placeholder="">
										<span class="input-group-addon">x/hari</span>
									</div>
								</div>
								<div class="col-md-4 ">
									<label for="bak_jumlah_saat_ini">Buang Air Kecil</label>
									<input class="form-control <?=($bak_jumlah_saat_ini!=$bak_jumlah_saat_ini_asal?'edited':'')?>" type="text" id="bak_jumlah_saat_ini" style="width: 100%;" placeholder="" value="{bak_jumlah_saat_ini}">
								</div>
								<div class="col-md-4 ">
									<label for="bak_warna_saat_ini">Warna Buang Air Kecil</label>
									<input class="form-control <?=($bak_warna_saat_ini!=$bak_warna_saat_ini_asal?'edited':'')?>" type="text" id="bak_warna_saat_ini" style="width: 100%;" placeholder="" value="{bak_warna_saat_ini}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="bab_saat_ini">Buang Air Besar</label>
									<select  id="bab_saat_ini"  class="form-control <?=($bab_saat_ini!=$bab_saat_ini_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bab_saat_ini == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(191) as $row){?>
										<option value="<?=$row->id?>" <?=($bab_saat_ini == $row->id ? 'selected="selected"' : '')?> <?=($bab_saat_ini == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="ket_saat_ini">Keterangan</label>
									<input class="form-control <?=($ket_saat_ini!=$ket_saat_ini_asal?'edited':'')?>" type="text" id="ket_saat_ini" style="width: 100%;" placeholder="" value="{ket_saat_ini}">
								</div>
								<div class="col-md-4 ">
									<label for="bab_x_saat_ini">Frekuensi BAB</label>
									<div class="input-group">
										<input class="form-control <?=($bab_x_saat_ini!=$bab_x_saat_ini_asal?'edited':'')?>"  type="text" id="bab_x_saat_ini" value="{bab_x_saat_ini}"  placeholder="">
										<span class="input-group-addon">x/hari</span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="bab_warna_saat_ini">Warna</label>
									<input class="form-control <?=($bab_warna_saat_ini!=$bab_warna_saat_ini_asal?'edited':'')?>" type="text" id="bab_warna_saat_ini" style="width: 100%;" placeholder="" value="{bab_warna_saat_ini}">
								</div>
								<div class="col-md-4 ">
									<label for="bab_bau_saat_ini">Bau</label>
									<input class="form-control <?=($bab_bau_saat_ini!=$bab_bau_saat_ini_asal?'edited':'')?>" type="text" id="bab_bau_saat_ini" style="width: 100%;" placeholder="" value="{bab_bau_saat_ini}">
								</div>
								<div class="col-md-4 ">
									<label for="bab_keringat_saat_ini">Keringat</label>
									<select  id="bab_keringat_saat_ini"  class="form-control <?=($bab_keringat_saat_ini!=$bab_keringat_saat_ini_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bab_keringat_saat_ini == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(192) as $row){?>
										<option value="<?=$row->id?>" <?=($bab_keringat_saat_ini == $row->id ? 'selected="selected"' : '')?> <?=($bab_keringat_saat_ini == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="keluhan_saat_ini">KELAINAN / KELUHAN LAIN </label>
									<input class="form-control <?=($keluhan_saat_ini!=$keluhan_saat_ini_asal?'edited':'')?>" type="text" id="keluhan_saat_ini" style="width: 100%;" placeholder="" value="{keluhan_saat_ini}">
								</div>
							</div>
						</div>
						
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary">SYSTEM MUSCULLO SCELETAL / OTOT DAN TULANG</label>
							</div>
						</div>
						<div class="form-group pull-5-b" style="margin-top:-10px;!important">
							<div class="col-md-12">
								<label class="text-black">KEADAAN ANATOMIS DAN FISIOLOGIS</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="bentuk_bahu">Bentuk Bahu</label>
									<select  id="bentuk_bahu"  class="form-control <?=($bentuk_bahu!=$bentuk_bahu_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bentuk_bahu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(193) as $row){?>
										<option value="<?=$row->id?>" <?=($bentuk_bahu == $row->id ? 'selected="selected"' : '')?> <?=($bentuk_bahu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="bentuk_tulang">Bentuk Tulang Belakang</label>
									<select  id="bentuk_tulang"  class="form-control <?=($bentuk_tulang!=$bentuk_tulang_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($bentuk_tulang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(194) as $row){?>
										<option value="<?=$row->id?>" <?=($bentuk_tulang == $row->id ? 'selected="selected"' : '')?> <?=($bentuk_tulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="rentan_gerak_atas">Rentang Gerak Extemitas Atas</label>
									<select  id="rentan_gerak_atas"  class="form-control <?=($rentan_gerak_atas!=$rentan_gerak_atas_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($rentan_gerak_atas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(195) as $row){?>
										<option value="<?=$row->id?>" <?=($rentan_gerak_atas == $row->id ? 'selected="selected"' : '')?> <?=($rentan_gerak_atas == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="rentan_gerak_bawah">Rentang Gerak Extemitas Bawah</label>
									<select  id="rentan_gerak_bawah"  class="form-control <?=($rentan_gerak_bawah!=$rentan_gerak_bawah_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($rentan_gerak_bawah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(196) as $row){?>
										<option value="<?=$row->id?>" <?=($rentan_gerak_bawah == $row->id ? 'selected="selected"' : '')?> <?=($rentan_gerak_bawah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="gerak_motorik">Gerak Motorik</label>
									<select  id="gerak_motorik"  class="form-control <?=($gerak_motorik!=$gerak_motorik_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($gerak_motorik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(197) as $row){?>
										<option value="<?=$row->id?>" <?=($gerak_motorik == $row->id ? 'selected="selected"' : '')?> <?=($gerak_motorik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group pull-5-b" style="margin-top:0px;!important">
							<div class="col-md-12">
								<label class="text-black">UJI KEKUATAN OTOT</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-6 ">
									<label for="uji_otot_id">Uji Kekuatan Otot</label>
									<select  id="uji_otot_id"  class="form-control <?=($uji_otot_id!=$uji_otot_id_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($uji_otot_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(get_all('muji_otot',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>" <?=($uji_otot_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<input type="hidden" id="nilai_otot_1" value="{nilai_otot_1}">
								<input type="hidden" id="nilai_otot_2" value="{nilai_otot_2}">
								<input type="hidden" id="nilai_otot_3" value="{nilai_otot_3}">
								<input type="hidden" id="nilai_otot_4" value="{nilai_otot_4}">

								<input type="hidden" id="nilai_otot_1_asal" value="{nilai_otot_1_asal}">
								<input type="hidden" id="nilai_otot_2_asal" value="{nilai_otot_2_asal}">
								<input type="hidden" id="nilai_otot_3_asal" value="{nilai_otot_3_asal}">
								<input type="hidden" id="nilai_otot_4_asal" value="{nilai_otot_4_asal}">
								<div id="div_ujji_otot">
									
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" >
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="keseimbangan">Keseimbangan & Posisi Berdiri</label>
									<select  id="keseimbangan"  class="form-control <?=($keseimbangan!=$keseimbangan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($keseimbangan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(198) as $row){?>
										<option value="<?=$row->id?>" <?=($keseimbangan == $row->id ? 'selected="selected"' : '')?> <?=($keseimbangan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="cara_berjalan">Cara Berjalan</label>
									<select  id="cara_berjalan"  class="form-control <?=($cara_berjalan!=$cara_berjalan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($cara_berjalan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(199) as $row){?>
										<option value="<?=$row->id?>" <?=($cara_berjalan == $row->id ? 'selected="selected"' : '')?> <?=($cara_berjalan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="alat_bantu">Alat Bantu</label>
									<input class="form-control <?=($alat_bantu!=$alat_bantu_asal?'edited':'')?>" type="text" id="alat_bantu" style="width: 100%;" placeholder="Alat Bantu" value="{alat_bantu}">
								</div>
								<div class="col-md-4 ">
									<label for="dimana">Dimana</label>
									<input class="form-control <?=($dimana!=$dimana_asal?'edited':'')?>" type="text" id="dimana" style="width: 100%;" placeholder="Dimana" value="{dimana}">
								</div>
								<div class="col-md-4 ">
									<label for="sejak">Sejak</label>
									<input class="form-control <?=($sejak!=$sejak_asal?'edited':'')?>" type="text" id="sejak" style="width: 100%;" placeholder="Sejak" value="{sejak}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-b" style="margin-top:0px;!important">
							<div class="col-md-12">
								<label class="text-black">POLA AKTIVITAS DAN ISTIRAHAT</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="waktu_tidur_siang">Waktu Tidur Siang</label>
									<input class="form-control <?=($waktu_tidur_siang!=$waktu_tidur_siang_asal?'edited':'')?>" type="text" id="waktu_tidur_siang" style="width: 100%;" placeholder="Tidur Siang" value="{waktu_tidur_siang}">
								</div>
								<div class="col-md-4 ">
									<label for="waktu_tidur_malam">Waktu Tidur Malam</label>
									<input class="form-control <?=($waktu_tidur_malam!=$waktu_tidur_malam_asal?'edited':'')?>" type="text" id="waktu_tidur_malam" style="width: 100%;" placeholder="Tidur Malam" value="{waktu_tidur_malam}">
								</div>
								<div class="col-md-4 ">
									<label for="penghantar">Penghantar</label>
									<input class="form-control <?=($penghantar!=$penghantar_asal?'edited':'')?>" type="text" id="penghantar" style="width: 100%;" placeholder="Penghantar" value="{penghantar}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="obat_tidur">Obat Tidur</label>
									<select  id="obat_tidur"  class="form-control <?=($obat_tidur!=$obat_tidur_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($obat_tidur == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(200) as $row){?>
										<option value="<?=$row->id?>" <?=($obat_tidur == $row->id ? 'selected="selected"' : '')?> <?=($obat_tidur == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="jenis_obat">Jenis Obat</label>
									<input class="form-control <?=($jenis_obat!=$jenis_obat_asal?'edited':'')?>" type="text" id="jenis_obat" style="width: 100%;" placeholder="Jenis Obat" value="{jenis_obat}">
								</div>
								<div class="col-md-4 ">
									<label for="dosis">Dosis</label>
									<input class="form-control <?=($dosis!=$dosis_asal?'edited':'')?>" type="text" id="dosis" style="width: 100%;" placeholder="Dosis" value="{dosis}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="sejak_obat_tidur">Sejak</label>
									<input class="form-control <?=($sejak_obat_tidur!=$sejak_obat_tidur_asal?'edited':'')?>" type="text" id="sejak_obat_tidur" style="width: 100%;" placeholder="Sejak" value="{sejak_obat_tidur}">
								</div>
								<div class="col-md-4 ">
									<label for="insomnia">Insomnia</label>
									<select  id="insomnia"  class="form-control <?=($insomnia!=$insomnia_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($insomnia == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(201) as $row){?>
										<option value="<?=$row->id?>" <?=($insomnia == $row->id ? 'selected="selected"' : '')?> <?=($insomnia == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="hobby">Hobby</label>
									<input class="form-control <?=($hobby!=$hobby_asal?'edited':'')?>" type="text" id="hobby" style="width: 100%;" placeholder="Hobby" value="{hobby}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="rutinitas">Kegiatan Rutin</label>
									<input class="form-control <?=($rutinitas!=$rutinitas_asal?'edited':'')?>" type="text" id="rutinitas" style="width: 100%;" placeholder="Rutinitas" value="{rutinitas}">
								</div>
								<div class="col-md-8 ">
									<label for="waktu_luang">Penggunaan Waktu Luang Yang Diisi Dengan</label>
									<input class="form-control <?=($waktu_luang!=$waktu_luang_asal?'edited':'')?>" type="text" id="waktu_luang" style="width: 100%;" placeholder="Penggunaan Waktu Luang Yang Diisi Dengan" value="{waktu_luang}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="kelainan_pola">Kelainan / Keluhan lain</label>
									<input class="form-control <?=($kelainan_pola!=$kelainan_pola_asal?'edited':'')?>" type="text" id="kelainan_pola" style="width: 100%;" placeholder="Keluhan Lain" value="{kelainan_pola}">
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary H5">SYTEM INTUGUMEN DAN POLA MERAWAT DIRI</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="warna_kulit">Warna Kulit</label>
									<select  id="warna_kulit"  class="form-control <?=($warna_kulit!=$warna_kulit_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($warna_kulit == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(202) as $row){?>
										<option value="<?=$row->id?>" <?=($warna_kulit == $row->id ? 'selected="selected"' : '')?> <?=($warna_kulit == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="bergelembung">Bergelembung Pada</label>
									<input class="form-control <?=($bergelembung!=$bergelembung_asal?'edited':'')?>" type="text" id="bergelembung" style="width: 100%;" placeholder="Bergelembung Pada" value="{bergelembung}">
									
								</div>
								<div class="col-md-4 ">
									<label for="lecet">Lecet Pada</label>
									<input class="form-control <?=($lecet!=$lecet_asal?'edited':'')?>" type="text" id="lecet" style="width: 100%;" placeholder="Lecet" value="{lecet}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="luka">Luka Pada</label>
									<input class="form-control <?=($luka!=$luka_asal?'edited':'')?>" type="text" id="luka" style="width: 100%;" placeholder="Luka" value="{luka}">
								</div>
								<div class="col-md-4 ">
									<label for="decubitus">Decubitus Pada</label>
									<input class="form-control <?=($decubitus!=$decubitus_asal?'edited':'')?>" type="text" id="decubitus" style="width: 100%;" placeholder="Decubitus" value="{decubitus}">
								</div>
								<div class="col-md-4 ">
									<label for="luka_bakar">Luka Bakar</label>
									<select  id="luka_bakar"  class="form-control <?=($luka_bakar!=$luka_bakar_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($luka_bakar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(203) as $row){?>
										<option value="<?=$row->id?>" <?=($luka_bakar == $row->id ? 'selected="selected"' : '')?> <?=($luka_bakar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="grade_luka_bakar">Grade Luka Bakar</label>
									<input class="form-control <?=($grade_luka_bakar!=$grade_luka_bakar_asal?'edited':'')?>" type="text" id="grade_luka_bakar" style="width: 100%;" placeholder="Grade" value="{grade_luka_bakar}">
								</div>
								<div class="col-md-4 ">
									<label for="persen_luka_bakar">Persentase Luka Bakar</label>
									<input class="form-control <?=($persen_luka_bakar!=$persen_luka_bakar_asal?'edited':'')?>" type="text" id="persen_luka_bakar" style="width: 100%;" placeholder="Persentase Luka " value="{persen_luka_bakar}">
									
								</div>
								<div class="col-md-4 ">
									<label for="turgor">Turgor Kulit</label>
									<select  id="turgor"  class="form-control <?=($turgor!=$turgor_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($turgor == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(204) as $row){?>
										<option value="<?=$row->id?>" <?=($turgor == $row->id ? 'selected="selected"' : '')?> <?=($turgor == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="keadaan_kulit">Keadaan Kulit Tubuh</label>
									<select  id="keadaan_kulit"  class="form-control <?=($keadaan_kulit!=$keadaan_kulit_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($keadaan_kulit == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(205) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_kulit == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_kulit == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="keadaan_kulit_kepala">Keadaan Kulit Kepala dan Rambut</label>
									<select  id="keadaan_kulit_kepala"  class="form-control <?=($keadaan_kulit_kepala!=$keadaan_kulit_kepala_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($keadaan_kulit_kepala == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(206) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_kulit_kepala == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_kulit_kepala == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="mandi_x">Mandi</label>
									<div class="input-group">
										<input class="form-control <?=($mandi_x!=$mandi_x_asal?'edited':'')?>"  type="text" id="mandi_x" name="mandi_x" value="{mandi_x}"  placeholder="Mandi" required>
										<span class="input-group-addon">x/hari</span>
									</div>
								</div>
								<div class="col-md-4 ">
									<label for="gosok_gigi_x">Gosok Gigi</label>
									<div class="input-group">
										<input class="form-control <?=($gosok_gigi_x!=$gosok_gigi_x_asal?'edited':'')?>"  type="text" id="gosok_gigi_x"  value="{gosok_gigi_x}"  placeholder="X" required>
										<span class="input-group-addon">x/hari</span>
									</div>
									
								</div>
								<div class="col-md-4 ">
									<label for="ganti_baju_x">Ganti Pakaian</label>
									<div class="input-group">
										<input class="form-control <?=($ganti_baju_x!=$ganti_baju_x_asal?'edited':'')?>"  type="text" id="ganti_baju_x"  value="{ganti_baju_x}"  placeholder="X" required>
										<span class="input-group-addon">x/hari</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="cuci_rambut_x">Cuci Rambut</label>
									<div class="input-group">
										<input class="form-control <?=($cuci_rambut_x!=$cuci_rambut_x_asal?'edited':'')?>"  type="text" id="cuci_rambut_x"  value="{cuci_rambut_x}"  placeholder="X" required>
										<span class="input-group-addon">x/hari</span>
									</div>
								</div>
								<div class="col-md-8 ">
									<label for="dilakukan_secara">Dilakukan Secara</label>
									<select  id="dilakukan_secara"  class="form-control <?=($dilakukan_secara!=$dilakukan_secara_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($dilakukan_secara == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(207) as $row){?>
										<option value="<?=$row->id?>" <?=($dilakukan_secara == $row->id ? 'selected="selected"' : '')?> <?=($dilakukan_secara == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="kelainan_kulit">Kelainan / Keluhan</label>
									<input class="form-control <?=($kelainan_kulit!=$kelainan_kulit_asal?'edited':'')?>" type="text" id="kelainan_kulit" style="width: 100%;" placeholder="Keluhan" value="{kelainan_kulit}">
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-6">
							<div class="col-md-6">
								<label class="text-primary H5">SYSTEM REPRODUKSI / SEXUALITAS</label>
								<select  id="jk_pasien"  class="form-control <?=($jk_pasien!=$jk_pasien_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($jk_pasien == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach(list_variable_ref(1) as $row){?>
									<option value="<?=$row->id?>" <?=($jk_pasien == $row->id ? 'selected="selected"' : '')?> <?=($jk_pasien == $jenis_kelamin && $jk_pasien=='' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="payudara">Payudara</label>
									<select  id="payudara"  class="form-control <?=($payudara!=$payudara_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($payudara == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(208) as $row){?>
										<option value="<?=$row->id?>" <?=($payudara == $row->id ? 'selected="selected"' : '')?> <?=($payudara == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="kelainan_payudara">Lokasi Kelainan Payudara</label>
									<input class="form-control <?=($kelainan_payudara!=$kelainan_payudara_asal?'edited':'')?>" type="text" id="kelainan_payudara" style="width: 100%;" placeholder="Lokasi Kelainan" value="{kelainan_payudara}">
									
								</div>
								<div class="col-md-4 ">
									<label for="ukuran_payudara">Ukuran Kelainan Payudara</label>
									<input class="form-control <?=($ukuran_payudara!=$ukuran_payudara_asal?'edited':'')?>" type="text" id="ukuran_payudara" style="width: 100%;" placeholder="Ukuran Kelainan" value="{ukuran_payudara}">
								</div>
							</div>
							<div class="col-md-6 div_laki">
								<div class="col-md-4 ">
									<label for="testis">Testis</label>
									<select  id="testis"  class="form-control <?=($testis!=$testis_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($testis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(209) as $row){?>
										<option value="<?=$row->id?>" <?=($testis == $row->id ? 'selected="selected"' : '')?> <?=($testis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="lokasi_kelainan_testis">Lokasi Kelainan</label>
									<input class="form-control <?=($lokasi_kelainan_testis!=$lokasi_kelainan_testis_asal?'edited':'')?>" type="text" id="lokasi_kelainan_testis" style="width: 100%;" placeholder="Lokasi Kelainan" value="{lokasi_kelainan_testis}">
								</div>
								<div class="col-md-4 ">
									<label for="ukuranan_kelainan_testis">Ukuran Kelainan Testis</label>
									<input class="form-control <?=($ukuranan_kelainan_testis!=$ukuranan_kelainan_testis_asal?'edited':'')?>" type="text" id="ukuranan_kelainan_testis" style="width: 100%;" placeholder="Ukuran Kelainan" value="{ukuranan_kelainan_testis}">
									
								</div>
							</div>
							<div class="col-md-6 div_wanita">
								<div class="col-md-4 ">
									<label for="genitalia">Genitalia</label>
									<select  id="genitalia"  class="form-control <?=($genitalia!=$genitalia_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($genitalia == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(213) as $row){?>
										<option value="<?=$row->id?>" <?=($genitalia == $row->id ? 'selected="selected"' : '')?> <?=($genitalia == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="lokasi_kelainan_genitalia">Lokasi Kelainan</label>
									<input class="form-control <?=($lokasi_kelainan_genitalia!=$lokasi_kelainan_genitalia_asal?'edited':'')?>" type="text" id="lokasi_kelainan_genitalia" style="width: 100%;" placeholder="Lokasi Kelainan" value="{lokasi_kelainan_genitalia}">
								</div>
								<div class="col-md-4 ">
									<label for="ukuran_kelainan_genitalia">Ukuran Kelainan</label>
									<input class="form-control <?=($ukuran_kelainan_genitalia!=$ukuran_kelainan_genitalia_asal?'edited':'')?>" type="text" id="ukuran_kelainan_genitalia" style="width: 100%;" placeholder="Ukuran Kelainan" value="{ukuran_kelainan_genitalia}">
									
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t div_laki" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="jenis_kelainan_testis">Jenis Kelainan Testis</label>
									<input class="form-control <?=($jenis_kelainan_testis!=$jenis_kelainan_testis_asal?'edited':'')?>" type="text" id="jenis_kelainan_testis" style="width: 100%;" placeholder="Jenis Kelainan" value="{jenis_kelainan_testis}">
								</div>
								<div class="col-md-4 ">
									<label for="penis">Penis</label>
									<input class="form-control <?=($penis!=$penis_asal?'edited':'')?>" type="text" id="penis" style="width: 100%;" placeholder=" " value="{penis}">
									
								</div>
								<div class="col-md-4 ">
									<label for="lokasi_kelainan_penis">Lokasi Kelainan Penis</label>
									<input class="form-control <?=($lokasi_kelainan_penis!=$lokasi_kelainan_penis_asal?'edited':'')?>" type="text" id="lokasi_kelainan_penis" style="width: 100%;" placeholder="Lokasi Kelainan" value="{lokasi_kelainan_penis}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="jenis_kelainan_penis">Jenis Kelainan Penis</label>
									<select  id="jenis_kelainan_penis"  class="form-control <?=($jenis_kelainan_penis!=$jenis_kelainan_penis_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_kelainan_penis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(210) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kelainan_penis == $row->id ? 'selected="selected"' : '')?> <?=($jenis_kelainan_penis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="cairan_keluar_penis">Ada Cairan Yang Keluar</label>
									<select  id="cairan_keluar_penis"  class="form-control <?=($cairan_keluar_penis!=$cairan_keluar_penis_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($cairan_keluar_penis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(211) as $row){?>
										<option value="<?=$row->id?>" <?=($cairan_keluar_penis == $row->id ? 'selected="selected"' : '')?> <?=($cairan_keluar_penis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="jenis_cairan_penis">Sebutkan Jenis Cairan</label>
									<input class="form-control <?=($jenis_cairan_penis!=$jenis_cairan_penis_asal?'edited':'')?>" type="text" id="jenis_cairan_penis" style="width: 100%;" placeholder="Cairan" value="{jenis_cairan_penis}">
									
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t div_wanita" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="jenis_kelainan_genitalia">Jenis Kelainan Genitalia</label>
									<input class="form-control <?=($jenis_kelainan_genitalia!=$jenis_kelainan_genitalia_asal?'edited':'')?>" type="text" id="jenis_kelainan_genitalia" style="width: 100%;" placeholder="Jenis Kelainan" value="{jenis_kelainan_genitalia}">
								</div>
								<div class="col-md-4 ">
									<label for="penis">Prolaps Uteri</label>
									<select  id="prolaps"  class="form-control <?=($prolaps!=$prolaps_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($prolaps == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(214) as $row){?>
										<option value="<?=$row->id?>" <?=($prolaps == $row->id ? 'selected="selected"' : '')?> <?=($prolaps == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="ukuran_kelainan_prolaps">Ukuran Kelainan Prolaps</label>
									<input class="form-control <?=($ukuran_kelainan_prolaps!=$ukuran_kelainan_prolaps_asal?'edited':'')?>" type="text" id="ukuran_kelainan_prolaps" style="width: 100%;" placeholder="Ukuran" value="{ukuran_kelainan_prolaps}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-8 ">
									<label for="jenis_kelainan_prolaps">Jenis Kelainan Prolaps Uteri</label>
									<input class="form-control <?=($jenis_kelainan_prolaps!=$jenis_kelainan_prolaps_asal?'edited':'')?>" type="text" id="jenis_kelainan_prolaps" style="width: 100%;" placeholder="Jenis Kelainan" value="{jenis_kelainan_prolaps}">
								</div>
								<div class="col-md-4 ">
									<label for="fluor">Fluor Albas</label>
									<select  id="fluor"  class="form-control <?=($fluor!=$fluor_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($fluor == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(215) as $row){?>
										<option value="<?=$row->id?>" <?=($fluor == $row->id ? 'selected="selected"' : '')?> <?=($fluor == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-t div_wanita" style="margin-top:-10px;!important">
							<div class="col-md-5 ">
								<div class="col-md-6 ">
									<label for="infertil">Infertil</label>
									<select  id="infertil"  class="form-control <?=($infertil!=$infertil_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($infertil == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(216) as $row){?>
										<option value="<?=$row->id?>" <?=($infertil == $row->id ? 'selected="selected"' : '')?> <?=($infertil == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-6 ">
									<label for="penis">Sebutkan</label>
									<input class="form-control <?=($infertil_sebutkan!=$infertil_sebutkan_asal?'edited':'')?>" type="text" id="infertil_sebutkan" style="width: 100%;" placeholder="Sebutkan" value="{infertil_sebutkan}">
									
								</div>
								
							</div>
							<div class="col-md-7 ">
								
								<div class="col-md-2 ">
									<label for="fertile_g">Fetile</label>
									<div class="input-group">
										<input class="form-control <?=($fertile_g!=$fertile_g_asal?'edited':'')?>"  type="text" id="fertile_g" value="{fertile_g}"  placeholder="">
										<span class="input-group-addon">G</span>
									</div>
								</div>
								<div class="col-md-2 ">
									<label for="fertile_p">&nbsp;</label>
									<div class="input-group">
										<input class="form-control <?=($fertile_p!=$fertile_p_asal?'edited':'')?>"  type="text" id="fertile_p" value="{fertile_p}"  placeholder="">
										<span class="input-group-addon">P</span>
									</div>
								</div>
								<div class="col-md-2 ">
									<label for="fertile_ah">&nbsp;</label>
									<div class="input-group">
										<input class="form-control <?=($fertile_ah!=$fertile_ah_asal?'edited':'')?>"  type="text" id="fertile_ah" value="{fertile_ah}"  placeholder="">
										<span class="input-group-addon">AH</span>
									</div>
								</div>
								<div class="col-md-2 ">
									<label for="fertile_am">&nbsp;</label>
									<div class="input-group">
										<input class="form-control <?=($fertile_am!=$fertile_am_asal?'edited':'')?>"  type="text" id="fertile_am" value="{fertile_am}"  placeholder="">
										<span class="input-group-addon">AM</span>
									</div>
								</div>
								<div class="col-md-2 ">
									<label for="fertile_ab">&nbsp;</label>
									<div class="input-group">
										<input class="form-control <?=($fertile_ab!=$fertile_ab_asal?'edited':'')?>"  type="text" id="fertile_ab" value="{fertile_ab}"  placeholder="">
										<span class="input-group-addon">AB</span>
									</div>
								</div>
								
								
							</div>
							
						</div>
						<div class="form-group pull-5-t div_wanita" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="umur_kehamilan">Umur Kemahilan</label>
									<div class="input-group">
										<input class="form-control <?=($umur_kehamilan!=$umur_kehamilan_asal?'edited':'')?>"  type="text" id="umur_kehamilan" value="{umur_kehamilan}"  placeholder="">
										<span class="input-group-addon">mg/bulan</span>
									</div>
								</div>
								<div class="col-md-4 ">
									<label for="hpl">HPL</label>
									<input class="form-control <?=($hpl!=$hpl_asal?'edited':'')?>" type="text" id="hpl" style="width: 100%;" placeholder="HPL" value="{hpl}">
									
								</div>
								<div class="col-md-4 ">
									<label for="siklus">Siklus</label>
									<div class="input-group">
										<input class="form-control <?=($siklus!=$siklus_asal?'edited':'')?>"  type="text" id="siklus" value="{siklus}"  placeholder="">
										<span class="input-group-addon">Hari</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="lama_siklus">Lamanya</label>
									<div class="input-group">
										<input class="form-control <?=($lama_siklus!=$lama_siklus_asal?'edited':'')?>"  type="text" id="lama_siklus" value="{lama_siklus}"  placeholder="">
										<span class="input-group-addon">Hari</span>
									</div>
								</div>
								<div class="col-md-4 ">
									<label for="teratur_siklus">Teratur</label>
									<select  id="teratur_siklus"  class="form-control <?=($teratur_siklus!=$teratur_siklus_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($teratur_siklus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(217) as $row){?>
										<option value="<?=$row->id?>" <?=($teratur_siklus == $row->id ? 'selected="selected"' : '')?> <?=($teratur_siklus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="sakit_haid">Sakit Haid</label>
									<select  id="sakit_haid"  class="form-control <?=($sakit_haid!=$sakit_haid_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sakit_haid == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(218) as $row){?>
										<option value="<?=$row->id?>" <?=($sakit_haid == $row->id ? 'selected="selected"' : '')?> <?=($sakit_haid == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t div_wanita" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="haid_terakhir">Haid Terakhir</label>
									<input class="form-control <?=($haid_terakhir!=$haid_terakhir_asal?'edited':'')?>" type="text" id="haid_terakhir" style="width: 100%;" placeholder="" value="{haid_terakhir}">
								</div>
								<div class="col-md-4 ">
									<label for="jenis_pendarahan">Jenis Pendarahan</label>
									<input class="form-control <?=($jenis_pendarahan!=$jenis_pendarahan_asal?'edited':'')?>" type="text" id="jenis_pendarahan" style="width: 100%;" placeholder="Jenis" value="{jenis_pendarahan}">
									
								</div>
								<div class="col-md-4 ">
									<label for="jumlah_pendarahan">Jumlah Pendarahan</label>
									<select  id="jumlah_pendarahan"  class="form-control <?=($jumlah_pendarahan!=$jumlah_pendarahan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jumlah_pendarahan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(219) as $row){?>
										<option value="<?=$row->id?>" <?=($jumlah_pendarahan == $row->id ? 'selected="selected"' : '')?> <?=($jumlah_pendarahan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="laktasi">Laktasi</label>
									<select  id="laktasi"  class="form-control <?=($laktasi!=$laktasi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($laktasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(220) as $row){?>
										<option value="<?=$row->id?>" <?=($laktasi == $row->id ? 'selected="selected"' : '')?> <?=($laktasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="menyusui">Menyusui</label>
									<select  id="menyusui"  class="form-control <?=($menyusui!=$menyusui_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($menyusui == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(221) as $row){?>
										<option value="<?=$row->id?>" <?=($menyusui == $row->id ? 'selected="selected"' : '')?> <?=($menyusui == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="menyusui_lama">Sebutkan lama Waktu</label>
									<div class="input-group">
										<input class="form-control <?=($menyusui_lama!=$menyusui_lama_asal?'edited':'')?>"  type="text" id="menyusui_lama" value="{menyusui_lama}"  placeholder="">
										<span class="input-group-addon">Bln/tahun</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t div_wanita" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="alasan_tiak_menyusui">Alasan Tidak Menyusui</label>
									<input class="form-control <?=($alasan_tiak_menyusui!=$alasan_tiak_menyusui_asal?'edited':'')?>" type="text" id="alasan_tiak_menyusui" style="width: 100%;" placeholder="" value="{alasan_tiak_menyusui}">
								</div>
								<div class="col-md-4 ">
									<label for="kb">KB</label>
									<select  id="kb"  class="form-control <?=($kb!=$kb_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kb == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(222) as $row){?>
										<option value="<?=$row->id?>" <?=($kb == $row->id ? 'selected="selected"' : '')?> <?=($kb == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="kb_lama">Lama KB</label>
									<input class="form-control <?=($kb_lama!=$kb_lama_asal?'edited':'')?>" type="text" id="kb_lama" style="width: 100%;" placeholder="Lama KB" value="{kb_lama}">
									
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="kb_keluhan">Keluhan KB</label>
									<input class="form-control <?=($kb_keluhan!=$kb_keluhan_asal?'edited':'')?>" type="text" id="kb_keluhan" style="width: 100%;" placeholder="Keluhan" value="{kb_keluhan}">
								</div>
								<div class="col-md-8 ">
									<label for="alasan_tidak_kb">Alasan Tidak KB</label>
									<input class="form-control <?=($alasan_tidak_kb!=$alasan_tidak_kb_asal?'edited':'')?>" type="text" id="alasan_tidak_kb" style="width: 100%;" placeholder="Alasan" value="{alasan_tidak_kb}">
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-t div_wanita" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								
								<div class="col-md-4 ">
									<label for="seksualitas">Seksualitas</label>
									<select  id="seksualitas"  class="form-control <?=($seksualitas!=$seksualitas_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($seksualitas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(223) as $row){?>
										<option value="<?=$row->id?>" <?=($seksualitas == $row->id ? 'selected="selected"' : '')?> <?=($seksualitas == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-8 ">
									<label for="kesulitan_seksualitas">Sebutkan Kesulitan Seksualitas</label>
									<div class="input-group">
										<input class="form-control <?=($kesulitan_seksualitas!=$kesulitan_seksualitas_asal?'edited':'')?>"  type="text" id="kesulitan_seksualitas" value="{kesulitan_seksualitas}"  placeholder="">
										<span class="input-group-addon">Bln/tahun</span>
									</div>
									
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="kelain_seksualitas">Kelainan / Keluhan lain</label>
									<input class="form-control <?=($kelain_seksualitas!=$kelain_seksualitas_asal?'edited':'')?>" type="text" id="kelain_seksualitas" style="width: 100%;" placeholder="Keluhan Seksualitas" value="{kelain_seksualitas}">
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary H5">PSIKO-SOSIAL</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="komunikasi">Komunikasi</label>
									<select  id="komunikasi"  class="form-control <?=($komunikasi!=$komunikasi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($komunikasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(224) as $row){?>
										<option value="<?=$row->id?>" <?=($komunikasi == $row->id ? 'selected="selected"' : '')?> <?=($komunikasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="kemampuan_bicara">Kemampuan Bicara</label>
									<select  id="kemampuan_bicara"  class="form-control <?=($kemampuan_bicara!=$kemampuan_bicara_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kemampuan_bicara == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(225) as $row){?>
										<option value="<?=$row->id?>" <?=($kemampuan_bicara == $row->id ? 'selected="selected"' : '')?> <?=($kemampuan_bicara == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="bahasa">Bahasa Yang Dipakai</label>
									<input class="form-control <?=($bahasa!=$bahasa_asal?'edited':'')?>" type="text" id="bahasa" style="width: 100%;" placeholder="Bahasa" value="{bahasa}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="ekpresi">Ekspresi</label>
									<select  id="ekpresi"  class="form-control <?=($ekpresi!=$ekpresi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($ekpresi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(226) as $row){?>
										<option value="<?=$row->id?>" <?=($ekpresi == $row->id ? 'selected="selected"' : '')?> <?=($ekpresi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="pasien_tinggal">Pasien Tinggal Dengan</label>
									<input class="form-control <?=($pasien_tinggal!=$pasien_tinggal_asal?'edited':'')?>" type="text" id="pasien_tinggal" style="width: 100%;" placeholder="Pasien Tinggal Dengan" value="{pasien_tinggal}">
									
								</div>
								<div class="col-md-4 ">
									<label for="orang_terdekat">Orang Yang Paling Dekat</label>
									<input class="form-control <?=($orang_terdekat!=$orang_terdekat_asal?'edited':'')?>" type="text" id="orang_terdekat" style="width: 100%;" placeholder="Orang Yang Paling Dekat" value="{orang_terdekat}">
									
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="pengambil_keputusan">Pengambil Keputusan</label>
									<input class="form-control <?=($pengambil_keputusan!=$pengambil_keputusan_asal?'edited':'')?>" type="text" id="pengambil_keputusan" style="width: 100%;" placeholder="Pengambil Keputusan" value="{pengambil_keputusan}">
								</div>
								
								<div class="col-md-6 ">
									<label for="hungan_keluarga">Hubungan Keluarga</label>
									<input class="form-control <?=($hungan_keluarga!=$hungan_keluarga_asal?'edited':'')?>" type="text" id="hungan_keluarga" style="width: 100%;" placeholder="Hubungan" value="{hungan_keluarga}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="kedudukan_dalam_masyarakat">Kedudukan Didalam Masyarakat</label>
									<input class="form-control <?=($kedudukan_dalam_masyarakat!=$kedudukan_dalam_masyarakat_asal?'edited':'')?>" type="text" id="kedudukan_dalam_masyarakat" style="width: 100%;" placeholder="Kedudukan" value="{kedudukan_dalam_masyarakat}">
								</div>
								
								<div class="col-md-6 ">
									<label for="kelainan_komunikasi">Kelainan & Keluhan lain</label>
									<input class="form-control <?=($kelainan_komunikasi!=$kelainan_komunikasi_asal?'edited':'')?>" type="text" id="kelainan_komunikasi" style="width: 100%;" placeholder="Kelainan & Keluhan Lain" value="{kelainan_komunikasi}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary H5">SPIRITUAL</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="ibadah">Ibadah</label>
									<select  id="ibadah"  class="form-control <?=($ibadah!=$ibadah_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($ibadah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(227) as $row){?>
										<option value="<?=$row->id?>" <?=($ibadah == $row->id ? 'selected="selected"' : '')?> <?=($ibadah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="tempat_ibadah">Tempat Ibadah</label>
									<input class="form-control <?=($tempat_ibadah!=$tempat_ibadah_asal?'edited':'')?>" type="text" id="tempat_ibadah" style="width: 100%;" placeholder="Tempat Ibadah" value="{tempat_ibadah}">
									
								</div>
								<div class="col-md-4 ">
									<label for="berdoa">Berdoa</label>
									<select  id="berdoa"  class="form-control <?=($berdoa!=$berdoa_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($berdoa == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(228) as $row){?>
										<option value="<?=$row->id?>" <?=($berdoa == $row->id ? 'selected="selected"' : '')?> <?=($berdoa == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="bantuan_yang_dibutuhkan">Bantuan Yang Dibutuhkan</label>
									<input class="form-control <?=($bantuan_yang_dibutuhkan!=$bantuan_yang_dibutuhkan_asal?'edited':'')?>" type="text" id="bantuan_yang_dibutuhkan" style="width: 100%;" placeholder="" value="{bantuan_yang_dibutuhkan}">
								</div>
								<div class="col-md-6 ">
									<label for="persepsi_sehat">Persepsi Terhadap Sehat / sakit</label>
									<input class="form-control <?=($persepsi_sehat!=$persepsi_sehat_asal?'edited':'')?>" type="text" id="persepsi_sehat" style="width: 100%;" placeholder="Mengenai Sehat" value="{persepsi_sehat}">
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="harapan">Keluhan / Harapan</label>
									<input class="form-control <?=($harapan!=$harapan_asal?'edited':'')?>" type="text" id="harapan" style="width: 100%;" placeholder="Harapan" value="{harapan}">
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-12">
								<label class="text-primary H5">SOSIAL EKONOMI</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="pekerjaan_utama">Pekerjaan Utama</label>
									<input class="form-control <?=($pekerjaan_utama!=$pekerjaan_utama_asal?'edited':'')?>" type="text" id="pekerjaan_utama" style="width: 100%;" placeholder="Pekerjaan" value="{pekerjaan_utama}">
								</div>
								<div class="col-md-4 ">
									<label for="tempat_kerja">Tempat kerja</label>
									<input class="form-control <?=($tempat_kerja!=$tempat_kerja_asal?'edited':'')?>" type="text" id="tempat_kerja" style="width: 100%;" placeholder="Tempat Kerja" value="{tempat_kerja}">
									
								</div>
								<div class="col-md-4 ">
									<label for="jabatan">Posisi / Jabatan</label>
									<input class="form-control <?=($jabatan!=$jabatan_asal?'edited':'')?>" type="text" id="jabatan" style="width: 100%;" placeholder="Jabatan" value="{jabatan}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="pekerjaan_tambahan">Pekerjaan Tambahan</label>
									<select  id="pekerjaan_tambahan"  class="form-control <?=($pekerjaan_tambahan!=$pekerjaan_tambahan_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pekerjaan_tambahan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(229) as $row){?>
										<option value="<?=$row->id?>" <?=($pekerjaan_tambahan == $row->id ? 'selected="selected"' : '')?> <?=($pekerjaan_tambahan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-6 ">
									<label for="dibantu_pekerjaan">Dibantu Oleh</label>
									<input class="form-control <?=($dibantu_pekerjaan!=$dibantu_pekerjaan_asal?'edited':'')?>" type="text" id="dibantu_pekerjaan" style="width: 100%;" placeholder="Dibantu Oleh" value="{dibantu_pekerjaan}">
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="biaya_hidup">Biaya Hidup</label>
									<select  id="biaya_hidup"  class="form-control <?=($biaya_hidup!=$biaya_hidup_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($biaya_hidup == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(230) as $row){?>
										<option value="<?=$row->id?>" <?=($biaya_hidup == $row->id ? 'selected="selected"' : '')?> <?=($biaya_hidup == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="lain_pekerjaan">Lain Lain</label>
									<input class="form-control <?=($lain_pekerjaan!=$lain_pekerjaan_asal?'edited':'')?>" type="text" id="lain_pekerjaan" style="width: 100%;" placeholder="Lain Lain" value="{lain_pekerjaan}">
									
								</div>
								<div class="col-md-4 ">
									<label for="penanggung_biaya_rs">Biaya RS Ditanggung Oleh</label>
									<select  id="penanggung_biaya_rs"  class="form-control <?=($penanggung_biaya_rs!=$penanggung_biaya_rs_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($penanggung_biaya_rs == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(232) as $row){?>
										<option value="<?=$row->id?>" <?=($penanggung_biaya_rs == $row->id ? 'selected="selected"' : '')?> <?=($penanggung_biaya_rs == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="penanggung_nama_kelaurga">Jika Keluarga Sebtukan</label>
									<input class="form-control <?=($penanggung_nama_kelaurga!=$penanggung_nama_kelaurga_asal?'edited':'')?>" type="text" id="penanggung_nama_kelaurga" style="width: 100%;" placeholder="Keluarga" value="{penanggung_nama_kelaurga}">
									
								</div>
								<div class="col-md-6 ">
									<label for="keluhan_sosial">Keluhan / Harapan</label>
									<input class="form-control <?=($keluhan_sosial!=$keluhan_sosial_asal?'edited':'')?>" type="text" id="keluhan_sosial" style="width: 100%;" placeholder="Keluhan" value="{keluhan_sosial}">
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-b" >
							<div class="col-md-12">
								<label class="text-primary H5">LINGKUNGAN</label>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="rumah">Rumah</label>
									<select  id="rumah"  class="form-control <?=($rumah!=$rumah_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($rumah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(233) as $row){?>
										<option value="<?=$row->id?>" <?=($rumah == $row->id ? 'selected="selected"' : '')?> <?=($rumah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="ventilasi">Ventilasi</label>
									<select  id="ventilasi"  class="form-control <?=($ventilasi!=$ventilasi_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($ventilasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(234) as $row){?>
										<option value="<?=$row->id?>" <?=($ventilasi == $row->id ? 'selected="selected"' : '')?> <?=($ventilasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="sinar_matahari">Sinar Matahari</label>
									<select  id="sinar_matahari"  class="form-control <?=($sinar_matahari!=$sinar_matahari_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sinar_matahari == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(235) as $row){?>
										<option value="<?=$row->id?>" <?=($sinar_matahari == $row->id ? 'selected="selected"' : '')?> <?=($sinar_matahari == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="jendela">Jendela</label>
									<select  id="jendela"  class="form-control <?=($jendela!=$jendela_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jendela == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(236) as $row){?>
										<option value="<?=$row->id?>" <?=($jendela == $row->id ? 'selected="selected"' : '')?> <?=($jendela == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="wc">WC</label>
									<select  id="wc"  class="form-control <?=($wc!=$wc_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($wc == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(237) as $row){?>
										<option value="<?=$row->id?>" <?=($wc == $row->id ? 'selected="selected"' : '')?> <?=($wc == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="bab_tempat">BAB Dimana</label>
									<input class="form-control <?=($bab_tempat!=$bab_tempat_asal?'edited':'')?>" type="text" id="bab_tempat" style="width: 100%;" placeholder="Tempat BAB" value="{bab_tempat}">
								</div>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="air_cucian">Air Cucian</label>
									<select  id="air_cucian"  class="form-control <?=($air_cucian!=$air_cucian_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($air_cucian == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(238) as $row){?>
										<option value="<?=$row->id?>" <?=($air_cucian == $row->id ? 'selected="selected"' : '')?> <?=($air_cucian == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="tempat_kerja">Tempat Sampah</label>
									<select  id="tempat_sampah"  class="form-control <?=($tempat_sampah!=$tempat_sampah_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($tempat_sampah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(239) as $row){?>
										<option value="<?=$row->id?>" <?=($tempat_sampah == $row->id ? 'selected="selected"' : '')?> <?=($tempat_sampah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="tempat_pembuangan">Dimana Tempat Sampah</label>
									<input class="form-control <?=($tempat_pembuangan!=$tempat_pembuangan_asal?'edited':'')?>" type="text" id="tempat_pembuangan" style="width: 100%;" placeholder="Buang Sampah" value="{tempat_pembuangan}">
								</div>
								<div class="col-md-4 ">
									<label for="sumber_air_minum">Sumber Air Minum</label>
									<select  id="sumber_air_minum"  class="form-control <?=($sumber_air_minum!=$sumber_air_minum_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sumber_air_minum == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(240) as $row){?>
										<option value="<?=$row->id?>" <?=($sumber_air_minum == $row->id ? 'selected="selected"' : '')?> <?=($sumber_air_minum == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="sumber_air_minum_tempat">Setbukan Sumber Air Minum</label>
									<input class="form-control <?=($sumber_air_minum_tempat!=$sumber_air_minum_tempat_asal?'edited':'')?>" type="text" id="sumber_air_minum_tempat" style="width: 100%;" placeholder="Air Minum" value="{sumber_air_minum_tempat}">
									
								</div>
								
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-10px;!important">
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="lokasi_tinggal">Lokasi Tempat Tinggal</label>
									<select  id="lokasi_tinggal"  class="form-control <?=($lokasi_tinggal!=$lokasi_tinggal_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($lokasi_tinggal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(241) as $row){?>
										<option value="<?=$row->id?>" <?=($lokasi_tinggal == $row->id ? 'selected="selected"' : '')?> <?=($lokasi_tinggal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="wabah_terjangkit">Wabah Yang Sedang Terjangkit</label>
									<select  id="wabah_terjangkit"  class="form-control <?=($wabah_terjangkit!=$wabah_terjangkit_asal?'edited':'')?> " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($wabah_terjangkit == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(242) as $row){?>
										<option value="<?=$row->id?>" <?=($wabah_terjangkit == $row->id ? 'selected="selected"' : '')?> <?=($wabah_terjangkit == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="sebutkan_tempat_wabah">Sebutkan</label>
									<input class="form-control <?=($sebutkan_tempat_wabah!=$sebutkan_tempat_wabah_asal?'edited':'')?>" type="text" id="sebutkan_tempat_wabah" style="width: 100%;" placeholder="Sebutkan Tempat Wabah" value="{sebutkan_tempat_wabah}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="text-primary" >DIAGNOSA KEPERAWATAN</h5>
							</div>
							<div class="col-md-6 ">
								<label class="text-primary" >RENCANA ASUHAN KEPERAWATAN</h5>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="index_header">
											<thead>
												<tr>
													<th width="55%">
														Diagnosa
													</th>
													<th width="25%">
														Priority
													</th>
													<th width="20%">
														
													</th>
												</tr>
												<tr>
													<th width="55%">
														<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
														<select  id="mdiagnosa_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(get_all('mdiagnosa_ranap',array('staktif'=>1)) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
															<?}?>
														</select>
													</th>
													<th width="25%">
														<select  id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															
															<?for($i=1;$i<20;$i++){?>
															<option value="<?=$i?>"><?=$i?></option>
															<?}?>
														</select>
													</th>
													<th width="20%">
														<span class="input-group-btn">
															<button class="btn btn-info" onclick="simpan_diagnosa()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
															<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</th>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Diagnosa Keperawatan</th>
													<th width="10%">Priority</th>
													<th width="25%">User</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa</label>
									<select  id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
								<div class="col-md-12 push-10-t">
									<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
										<thead></thead>
										<tbody>
											<tr>
												<td colspan="2" style="width:100%"><label class="text-primary">Section</label></td>
											</tr>
											<tr>
												<td colspan="2" style="width:100%"><label class="text-success">Kategori</label></td>
											</tr>
											<tr>
												<td style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
												<td  style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" style="width:100%"><label class="text-success">Kategori</label></td>
											</tr>
											<tr>
												<td style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
												<td  style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_menurun(){
	let tabel='';
	if ($("#penyakit_menurun_id_var").val()=="#"){
		sweetAlert("Maaf...", "Pilih Nama Penyakit!", "error");
		return false;
	}
	if ($("#nama_orang_penyakit_menurun_var").val()==""){
		sweetAlert("Maaf...", "Tentukan Penderita!", "error");
		return false;
	}
	if ($("#status_penyakit_menurun_var").val()=="#"){
		sweetAlert("Maaf...", "Tentukan Status Penderita!", "error");
		return false;
	}
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_menurun"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_menurun"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center"><input type="hidden" class="penyakit_menurun_id_var" value="'+$("#penyakit_menurun_id_var").val()+'">'+$("#penyakit_menurun_id_var option:selected").text()+'</td>';
	tabel +='<td class="text-center"><input type="hidden" class="nama_orang_penyakit_menurun_var" value="'+$("#nama_orang_penyakit_menurun_var").val()+'">'+$("#nama_orang_penyakit_menurun_var").val()+'</td>';
	tabel +='<td class="text-center"><input type="hidden" class="status_penyakit_menurun_var" value="'+$("#status_penyakit_menurun_var").val()+'">'+$("#status_penyakit_menurun_var option:selected").text()+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	
	if ($("#rowindex_menurun").val() != '') {
		$('#index_menurun tbody tr:eq(' + $("#rowindex_menurun").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_menurun tbody').append(tabel);
	}
	// $("#index_menurun").append(tabel);
	clear_input_menurun();
	generate_array_penyakit();
}
function clear_input_menurun(){
	$("#penyakit_menurun_id_var").val("#").trigger('change');
	$("#nama_orang_penyakit_menurun_var").val("");
	$("#status_penyakit_menurun_var").val("#").trigger('change');
	
	$("#rowindex_menurun").val("");
	$(".btn_edit_menurun").attr('disabled', false);
	$(".btn_hapus_menurun").attr('disabled', false);
}
$(document).on("click", ".btn_edit_menurun", function() {
	$("#rowindex_menurun").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_menurun").attr('disabled', true);
	$(".btn_hapus_menurun").attr('disabled', true);
	
	$("#penyakit_menurun_id_var").val($(this).closest('tr').find("td:eq(0) input").val()).trigger('change');
	$("#nama_orang_penyakit_menurun_var").val($(this).closest('tr').find("td:eq(1) input").val());
	$("#status_penyakit_menurun_var").val($(this).closest('tr').find("td:eq(2) input").val()).trigger('change');
	
	$("#nama_orang_penyakit_menurun_var").focus();
});
$(document).on("click", ".btn_hapus_menurun", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function simpan_menular(){
	let tabel='';
	if ($("#penyakit_menular_id_var").val()=="#"){
		sweetAlert("Maaf...", "Pilih Nama Penyakit!", "error");
		return false;
	}
	if ($("#nama_orang_penyakit_menular_var").val()==""){
		sweetAlert("Maaf...", "Tentukan Penderita!", "error");
		return false;
	}
	if ($("#status_penyakit_menular_var").val()=="#"){
		sweetAlert("Maaf...", "Tentukan Status Penderita!", "error");
		return false;
	}
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_menular"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_menular"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center"><input type="hidden" class="penyakit_menular_id_var" value="'+$("#penyakit_menular_id_var").val()+'">'+$("#penyakit_menular_id_var option:selected").text()+'</td>';
	tabel +='<td class="text-center"><input type="hidden" class="nama_orang_penyakit_menular_var" value="'+$("#nama_orang_penyakit_menular_var").val()+'">'+$("#nama_orang_penyakit_menular_var").val()+'</td>';
	tabel +='<td class="text-center"><input type="hidden" class="status_penyakit_menular_var" value="'+$("#status_penyakit_menular_var").val()+'">'+$("#status_penyakit_menular_var option:selected").text()+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	
	if ($("#rowindex_menular").val() != '') {
		$('#index_menular tbody tr:eq(' + $("#rowindex_menular").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_menular tbody').append(tabel);
	}
	// $("#index_menular").append(tabel);
	clear_input_menular();
	generate_array_penyakit();
}
function clear_input_menular(){
	$("#penyakit_menular_id_var").val("#").trigger('change');
	$("#nama_orang_penyakit_menular_var").val("");
	$("#status_penyakit_menular_var").val("#").trigger('change');
	
	$("#rowindex_menular").val("");
	$(".btn_edit_menular").attr('disabled', false);
	$(".btn_hapus_menular").attr('disabled', false);
}
$(document).on("click", ".btn_edit_menular", function() {
	$("#rowindex_menular").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_menular").attr('disabled', true);
	$(".btn_hapus_menular").attr('disabled', true);
	
	$("#penyakit_menular_id_var").val($(this).closest('tr').find("td:eq(0) input").val()).trigger('change');
	$("#nama_orang_penyakit_menular_var").val($(this).closest('tr').find("td:eq(1) input").val());
	$("#status_penyakit_menular_var").val($(this).closest('tr').find("td:eq(2) input").val()).trigger('change');
	
	$("#nama_orang_penyakit_menular_var").focus();
});
$(document).on("click", ".btn_hapus_menular", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function generate_array_penyakit(){
	let penyakit_menurun_id='';
	let penyakit_menurun_id_nama='';
	let nama_orang_penyakit_menurun='';
	let status_penyakit_menurun='';
	let status_penyakit_menurun_nama='';
	$('#index_menurun tbody tr').each(function() {
		if (penyakit_menurun_id!=''){penyakit_menurun_id +=","+$(this).find('td:eq(0) input').val()}else{penyakit_menurun_id +=$(this).find('td:eq(0) input').val()};
		if (penyakit_menurun_id_nama!=''){penyakit_menurun_id_nama +="#"+$(this).find('td:eq(0)').text()}else{penyakit_menurun_id_nama +=$(this).find('td:eq(0)').text()};
		if (nama_orang_penyakit_menurun!=''){nama_orang_penyakit_menurun +="#"+$(this).find('td:eq(1) input').val()}else{nama_orang_penyakit_menurun +=$(this).find('td:eq(1) input').val()};
		if (status_penyakit_menurun!=''){status_penyakit_menurun +=","+$(this).find('td:eq(2) input').val()}else{status_penyakit_menurun +=$(this).find('td:eq(2) input').val()};
		if (status_penyakit_menurun_nama!=''){status_penyakit_menurun_nama +="#"+$(this).find('td:eq(2)').text()}else{status_penyakit_menurun_nama +=$(this).find('td:eq(2)').text()};
		
	});
	$("#penyakit_menurun_id").val(penyakit_menurun_id);
	$("#penyakit_menurun_id_nama").val(penyakit_menurun_id_nama);
	$("#nama_orang_penyakit_menurun").val(nama_orang_penyakit_menurun);
	$("#status_penyakit_menurun").val(status_penyakit_menurun);
	$("#status_penyakit_menurun_nama").val(status_penyakit_menurun_nama);
	
	//Menular
	let penyakit_menular_id='';
	let penyakit_menular_id_nama='';
	let nama_orang_penyakit_menular='';
	let status_penyakit_menular='';
	let status_penyakit_menular_nama='';
	$('#index_menular tbody tr').each(function() {
		if (penyakit_menular_id!=''){penyakit_menular_id +=","+$(this).find('td:eq(0) input').val()}else{penyakit_menular_id +=$(this).find('td:eq(0) input').val()};
		if (penyakit_menular_id_nama!=''){penyakit_menular_id_nama +="#"+$(this).find('td:eq(0)').text()}else{penyakit_menular_id_nama +=$(this).find('td:eq(0)').text()};
		if (nama_orang_penyakit_menular!=''){nama_orang_penyakit_menular +="#"+$(this).find('td:eq(1) input').val()}else{nama_orang_penyakit_menular +=$(this).find('td:eq(1) input').val()};
		if (status_penyakit_menular!=''){status_penyakit_menular +=","+$(this).find('td:eq(2) input').val()}else{status_penyakit_menular +=$(this).find('td:eq(2) input').val()};
		if (status_penyakit_menular_nama!=''){status_penyakit_menular_nama +="#"+$(this).find('td:eq(2)').text()}else{status_penyakit_menular_nama +=$(this).find('td:eq(2)').text()};
		
	});
	$("#penyakit_menular_id").val(penyakit_menular_id);
	$("#penyakit_menular_id_nama").val(penyakit_menular_id_nama);
	$("#nama_orang_penyakit_menular").val(nama_orang_penyakit_menular);
	$("#status_penyakit_menular").val(status_penyakit_menular);
	$("#status_penyakit_menular_nama").val(status_penyakit_menular_nama);
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
}
function generate_tabel_penyakit(){
	
	let penyakit_menurun_id_nama = $("#penyakit_menurun_id_nama").val().split("#");
	let penyakit_menurun_id = $("#penyakit_menurun_id").val().split(",");
	let nama_orang_penyakit_menurun = $("#nama_orang_penyakit_menurun").val().split("#");
	let status_penyakit_menurun = $("#status_penyakit_menurun").val().split(",");
	let status_penyakit_menurun_nama = $("#status_penyakit_menurun_nama").val().split("#");
	let tabel='';
	if (penyakit_menurun_id){
	console.log(penyakit_menurun_id);
		
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_menurun"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_menurun"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	penyakit_menurun_id.forEach(myFunction)
	function myFunction(item, index, arr) {
		if (item!==''){
			
		tabel +="<tr>";
		tabel +='<td class="text-center"><input type="hidden" class="penyakit_menurun_id_var" value="'+item+'">'+penyakit_menurun_id_nama[index]+'</td>';
		tabel +='<td class="text-center"><input type="hidden" class="nama_orang_penyakit_menurun_var" value="'+nama_orang_penyakit_menurun[index]+'">'+nama_orang_penyakit_menurun[index]+'</td>';
		tabel +='<td class="text-center"><input type="hidden" class="status_penyakit_menurun_var" value="'+status_penyakit_menurun[index]+'">'+status_penyakit_menurun_nama[index]+'</td>';
		tabel +='<td class="text-center">'+btn+'</td>';
		tabel +="</tr>";
		}
		
	}
	$('#index_menurun tbody').append(tabel);
	}
		
	let penyakit_menular_id_nama = $("#penyakit_menular_id_nama").val().split("#");
	let penyakit_menular_id = $("#penyakit_menular_id").val().split(",");
	let nama_orang_penyakit_menular = $("#nama_orang_penyakit_menular").val().split("#");
	let status_penyakit_menular = $("#status_penyakit_menular").val().split(",");
	let status_penyakit_menular_nama = $("#status_penyakit_menular_nama").val().split("#");
	 tabel='';
	if (penyakit_menular_id){
	let btn2 ='<span class="input-group-btn">';
		btn2 +='<button class="btn btn-info btn-xs btn_edit_menular"   type="button"><i class="fa fa-pencil"></i></button>';
		btn2 +='<button class="btn btn-danger  btn-xs btn_hapus_menular"  type="button"><i class="fa fa-times"></i></button>';
		btn2 +='</span>';
	penyakit_menular_id.forEach(myFunction2)
	function myFunction2(item, index, arr) {
		if (item!==''){
		tabel +="<tr>";
		tabel +='<td class="text-center"><input type="hidden" class="penyakit_menular_id_var" value="'+item+'">'+penyakit_menular_id_nama[index]+'</td>';
		tabel +='<td class="text-center"><input type="hidden" class="nama_orang_penyakit_menular_var" value="'+nama_orang_penyakit_menular[index]+'">'+nama_orang_penyakit_menular[index]+'</td>';
		tabel +='<td class="text-center"><input type="hidden" class="status_penyakit_menular_var" value="'+status_penyakit_menular[index]+'">'+status_penyakit_menular_nama[index]+'</td>';
		tabel +='<td class="text-center">'+btn2+'</td>';
		tabel +="</tr>";
		
		}
	}
	$('#index_menular tbody').append(tabel);
	}
		
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
						// { "width": "5%", "targets": [0] },
						// { "width": "20%", "targets": [1] },
						// { "width": "15%", "targets": [3] },
						// { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_assesmen_ri', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
$(document).ready(function() {
	$(".number").number(true,0,'.',',');
	disabel_edit();
	set_jenis_kelamin();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		list_index_history_edit();
		refresh_diagnosa();
		
		set_anamnesa();
		set_riwayat_alergi();
		load_awal_assesmen=false;
		load_uji_otot();
		set_riwayat_penyakit();
		generate_tabel_penyakit();
		load_syaraf_mata();
		load_alergi();
		load_alergi_his();
		load_skrining_nutrisi();
		load_diagnosa();
		load_data_rencana_asuhan();
		
		
	}
	// load_data_rencana_asuhan(1);
});
function load_syaraf_mata(){
	let assesmen_id=$("#assesmen_id").val();
	let pupil_kiri=$("#pupil_kiri").val();
	let pupil_kiri_cahaya=$("#pupil_kiri_cahaya").val();
	let pupil_kanan=$("#pupil_kanan").val();
	let pupil_kanan_cahaya=$("#pupil_kanan_cahaya").val();
	let pupil_kiri_asal=$("#pupil_kiri_asal").val();
	let pupil_kiri_cahaya_asal=$("#pupil_kiri_cahaya_asal").val();
	let pupil_kanan_asal=$("#pupil_kanan_asal").val();
	let pupil_kanan_cahaya_asal=$("#pupil_kanan_cahaya_asal").val();
	$.ajax({
	  url: '{site_url}thistory_tindakan/load_syaraf_mata_ri/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			assesmen_id:assesmen_id,
			pupil_kiri:pupil_kiri,
			pupil_kiri_cahaya:pupil_kiri_cahaya,
			pupil_kanan:pupil_kanan,
			pupil_kanan_cahaya:pupil_kanan_cahaya,
			pupil_kiri_asal:pupil_kiri_asal,
			pupil_kiri_cahaya_asal:pupil_kiri_cahaya_asal,
			pupil_kanan_asal:pupil_kanan_asal,
			pupil_kanan_cahaya_asal:pupil_kanan_cahaya_asal,
			
	  },success: function(data) {
			$("#div_sayaraf").html(data);
			
			$(".opsi_change_pupil_1").select2();
			$(".opsi_change_pupil_2").select2();
		}
	});
}
$(document).on("change","#jk_pasien",function(){
	set_jenis_kelamin();
});
$(document).on("change","#uji_otot_id",function(){
	$("#nilai_otot_1").val('0');
	$("#nilai_otot_2").val('0');
	$("#nilai_otot_3").val('0');
	$("#nilai_otot_4").val('0');
	load_uji_otot();
});
$(document).on("change",".opsi_change_pupil_1",function(){
	$("#pupil_kiri_cahaya").val($(this).val());
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});
$(document).on("change",".opsi_change_pupil_2",function(){
	$("#pupil_kanan_cahaya").val($(this).val());
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});
// $(".opsi_change_pupil_1").change(function(){
	// alert('sini');
	// if ($("#st_edited").val()=='0'){
		// simpan_assesmen();
	// }
// });
function set_syaraf(id,nilai,tabel){
	// alert(tabel);
	if (tabel=='1'){
		$("#pupil_kiri").val(nilai);
	}
	if (tabel=='2'){
		$("#pupil_kanan").val(nilai);
	}
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
}

function disabel_edit(){
	 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".his_filter").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $('.edited').css('background', '#fff2f1');
}
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let idpasien=$("#idpasien").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_assesmen_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

$("#riwayat_penyakit,#keluhan_utama_list,#riwayat_penyakit_pernah_list,#pengobatan_list").change(function(){
	generate_riwayat_penyakit();
});

//riwayat_penyakit
function generate_riwayat_penyakit(){
	if ($("#status_assemen").val()!='2'){
	var select_button_text
	let str_penyakit='';
	select_button_text = $('#riwayat_penyakit option:selected')
                .toArray().map(item => item.text).join();
	
	$("#riwayat_penyakit_lainnya").val(select_button_text);
	
	select_button_text = $('#keluhan_utama_list option:selected')
                .toArray().map(item => item.text).join();
	
	$("#keluhan_utama_text").val(select_button_text);
	
	select_button_text = $('#riwayat_penyakit_pernah_list option:selected')
                .toArray().map(item => item.text).join();
	
	$("#riwayat_penyakit_pernah").val(select_button_text);
	select_button_text = $('#pengobatan_list option:selected')
                .toArray().map(item => item.text).join();
	
	$("#pengobatan_keterangan").val(select_button_text);
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
}
}
function set_jenis_kelamin(){
	if ($("#jk_pasien").val()=='1'){
		
		$(".div_wanita").hide();
		$(".div_laki").show();
	}else if($("#jk_pasien").val()=='2'){
		$(".div_wanita").show();
		$(".div_laki").hide();
	}else{
		$(".div_wanita").hide();
		$(".div_laki").hide();
	}
}
function set_allo(){
	$("#st_anamnesa").val('2');
	set_anamnesa();
}
function set_auto(){
	$("#st_anamnesa").val('1');
	set_anamnesa();
}
function set_riwayat_alergi(){
	let riwayat=$("#riwayat_alergi").val();
	if (riwayat=='2'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").show();
		$(".div_input_alergi").show();
		$("#li_alergi_1").addClass('active');
		$("#li_alergi_2").removeClass('active');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else if (riwayat=='1'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").hide();
		$(".div_input_alergi").hide();
		// // $('#alergi_2 > a').tab('show');
		// $('#alergi_1 > a').tab('hide');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else{
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
		$(".div_alergi_all").hide();
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	
		
	}
	
}
$("#riwayat_alergi").on("change", function(){
	set_riwayat_alergi();
});
function set_riwayat_penyakit(){
	let st_riwayat_penyakit_menurun=$("#st_riwayat_penyakit_menurun").val();
	if (st_riwayat_penyakit_menurun=='2'){
		$(".div_menurun_all").show();
	}else{
		$(".div_menurun_all").hide();
	}
	let st_riwayat_penyakit_menular=$("#st_riwayat_penyakit_menular").val();
	if (st_riwayat_penyakit_menular=='2'){
		$(".div_menular_all").show();
	}else{
		$(".div_menular_all").hide();
	}
	
}
$("#st_riwayat_penyakit_menurun,#st_riwayat_penyakit_menular").on("change", function(){
	set_riwayat_penyakit();
});
function set_anamnesa(){
	if ($("#st_anamnesa").val()=='1'){
		$(".allo_anamnesa").attr('disabled','disabled');
		$(".allo_anamnesa").removeAttr('required');
		$("#nama_anamnesa").val('');
		$("#hubungan_anamnesa").val('');
	}else{
		$(".allo_anamnesa").removeAttr('disabled');
		$(".allo_anamnesa").attr('required');
		
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
$("#medukasi_id").change(function(){
	if ($("#st_edited").val()=='0'){
	simpan_edukasi();
	}
});

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
$(".auto_blur_ttv").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});

$(".auto_blur_ttv").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
	
	function simpan_alergi(){// alert($("#keluhan_utama").val());
		let alergi_id=$("#alergi_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		let input_jenis_alergi=$("#input_jenis_alergi").val();
		let input_detail_alergi=$("#input_detail_alergi").val();
		let input_reaksi_alergi=$("#input_reaksi_alergi").val();
		if (input_detail_alergi==''){
			sweetAlert("Maaf...", "Isi Detail Alergi!", "error");
			return false;

		}
		if (input_reaksi_alergi==''){
			sweetAlert("Maaf...", "Isi Reaksi Alergi!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_alergi_ri', 
				dataType: "JSON",
				method: "POST",
				data : {
						alergi_id:alergi_id,
						// pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						assesmen_id:assesmen_id,
						input_jenis_alergi:input_jenis_alergi,
						input_detail_alergi:input_detail_alergi,
						input_reaksi_alergi:input_reaksi_alergi,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_alergi();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_alergi();
					}
				}
			});
		}
		
	}
	
	function clear_input_alergi(){
		$("#alergi_id").val('');
		$("#btn_add_alergi").html('<i class="fa fa-plus"></i> Add');
		$("#input_jenis_alergi").val('');
		$("#input_detail_alergi").val('');
		$("#input_reaksi_alergi").val('');
	}
	function load_alergi(){
		$('#alergi_2 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	
	function load_alergi_his(){
		$('#alergi_1 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_alergi_his').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi_his').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi_his', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_alergi_pasien(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function hapus_alergi(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Alergi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_alergi', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_alergi();		
				}
			});
		});			
	}
	function edit_alergi(id){
		$("#alergi_id").val(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_alergi', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_alergi").html('<i class="fa fa-save"></i> Simpan');
				$("#input_jenis_alergi").val(data.input_jenis_alergi).trigger('change');
				$("#input_detail_alergi").val(data.input_detail_alergi);
				$("#input_reaksi_alergi").val(data.input_reaksi_alergi);
			}
		});
	}
	
	function simpan_edukasi(){// alert($("#keluhan_utama").val());
		let assesmen_id=$("#assesmen_id").val();
		let medukasi_id=$("#medukasi_id").val();
		console.log('SIMPAN EDUKASI');
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						medukasi_id:medukasi_id,
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}
				}
			});
		}
		
	}
	
	function load_skrining_nutrisi(){
		let template_id=$("#skrining_nutrisi_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$("#tabel_skrining tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}thistory_tindakan/load_skrining_nutrisi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					template_id:template_id,
					versi_edit:versi_edit,
					
					},
			success: function(data) {
				$("#tabel_skrining tbody").append(data.opsi);
				$(".nilai").select2();
				get_skor_pengkajian();
				$("#cover-spin").hide();
			}
		});
	}
	function set_uji(id,nilai){
		$("#nilai_otot_"+id).val(nilai);
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	}
	function load_uji_otot(){
		let uji_otot_id=$("#uji_otot_id").val();
		let assesmen_id=$("#assesmen_id").val();
		$("#tabel_skrining tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}thistory_tindakan/get_uji_otot_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					uji_otot_id:uji_otot_id,
					nilai_otot_1 : $("#nilai_otot_1").val(),
					nilai_otot_2 : $("#nilai_otot_2").val(),
					nilai_otot_3 : $("#nilai_otot_3").val(),
					nilai_otot_4 : $("#nilai_otot_4").val(),

					nilai_otot_1_asal : $("#nilai_otot_1_asal").val(),
					nilai_otot_2_asal : $("#nilai_otot_2_asal").val(),
					nilai_otot_3_asal : $("#nilai_otot_3_asal").val(),
					nilai_otot_4_asal : $("#nilai_otot_4_asal").val(),
					
					},
			success: function(data) {
				$('#div_ujji_otot').html(data);
				$("#cover-spin").hide();
			}
		});
	}
	
	$(document).on("change",".nilai",function(){
		let data = $(this).select2('data')[0];
		let skor=$(this).find("option:selected").attr('data-nilai');
		// console.log(data.dataset.nilai);
		// alert($(this).find("option:selected").attr('data-nilai'))
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#skrining_nutrisi_id").val();
		var tr=$(this).closest('tr');
		tr.find("td:eq(2)").html(skor)
		
		var risiko_nilai_id=tr.find(".risiko_nilai_id").val();
		var nilai_id=$(this).val();
		
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_nutrisi', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					risiko_nilai_id:risiko_nilai_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					$("#st_tindakan").val(data.st_tindakan);
					 $("#nama_hasil_pengkajian").val(data.ref_nilai);
					 $("#total_skor_nutrisi").val(data.skor);
					 $("#risiko_jatuh").val(data.ref_nilai);
					 $("#tindakan_nutrisi").val(data.nama_tindakan);
					 $(".label_total_nutrisi").text(data.skor);
					 // nama_tindakan
					 // nama_hasil_pengkajian
					 // tindakan_nutrisi
				}
			});
		
	});
	$(document).on("change","#skrining_nutrisi_id",function(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#skrining_nutrisi_id").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/ganti_nutrisi_id', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },success: function(data) {
				 load_skrining_nutrisi();
			 }
		});
		
	});
	function get_skor_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#skrining_nutrisi_id").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_nutrisi', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },
			  success: function(data) {
				 $("#st_tindakan").val(data.st_tindakan);
				 $("#nama_tindakan").val(data.nama_tindakan);
				 $("#total_skor_nutrisi").val(data.skor);
				 $("#risiko_jatuh").val(data.ref_nilai);
			  }
		});
		
	}
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function load_diagnosa(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,list_index_history_edit
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Thistory_tindakan/load_diagnosa_ri', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
					 // alert('sono');
				 }  
			});
		refresh_diagnosa();
	}
	
	function refresh_diagnosa(){
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Thistory_tindakan/refresh_diagnosa_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					versi_edit:versi_edit,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				load_data_rencana_asuhan();
			}
		});
	}
	
	$("#diagnosa_id_list").on("change", function(){
		load_data_rencana_asuhan($(this).val());
	});
	function load_data_rencana_asuhan(diagnosa_id){
		var diagnosa_id=$("#diagnosa_id_list").val();
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Thistory_tindakan/load_data_rencana_asuhan_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_id,
					versi_edit:versi_edit,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan tbody").append(data);
			}
		});
	}
	function set_rencana_asuhan(id){
		 $("#diagnosa_id_list").val(id).trigger('change');
	}
	// $(".data_asuhan").on("click", function(){
		// let id_det;
		 // check = $(this).is(":checked");
		// alert(check);
			// // if(check) {
				// // $(this).closest('tr').find(".isiField").removeAttr("disabled");
			// // } else {
				// // $(this).closest('tr').find(".isiField").attr('disabled', 'disabled');
			// // }
	// }); 
	$(document).on('click','.data_asuhan',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/update_data', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
</script>