<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.peng_keterangan{
		background-color:#fdffe2;
	}
	.cls_tabel_kasssa{
		background-color:#fdffe2;
	}
	.cls_tabel_instrumen{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_assesmen_askep'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_assesmen_askep' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						if ($tiba_dikamar_beda){
							$tgl_tiba_dikamar_beda=HumanDateShort($tiba_dikamar_beda);
							$waktu_tiba_dikamar_beda=HumanTime($tiba_dikamar_beda);
						}else{
							$tgl_tiba_dikamar_beda=date('d-m-Y');
							$waktu_tiba_dikamar_beda=date('H:i:s');
						}
						if ($masuk_jam){
							$tgl_masuk_jam=HumanDateShort($masuk_jam);
							$waktu_masuk_jam=HumanTime($masuk_jam);
						}else{
							$tgl_masuk_jam=date('d-m-Y');
							$waktu_masuk_jam=date('H:i:s');
						}
						if ($keluar_jam){
							$tgl_keluar_jam=HumanDateShort($keluar_jam);
							$waktu_keluar_jam=HumanTime($keluar_jam);
						}else{
							$tgl_keluar_jam=date('d-m-Y');
							$waktu_keluar_jam=date('H:i:s');
						}
						if ($memanggil_perawat){
							$tgl_memanggil_perawat=HumanDateShort($memanggil_perawat);
							$waktu_memanggil_perawat=HumanTime($memanggil_perawat);
						}else{
							$tgl_memanggil_perawat=date('d-m-Y');
							$waktu_memanggil_perawat=date('H:i:s');
						}
						if ($perawat_datang){
							$tgl_perawat_datang=HumanDateShort($perawat_datang);
							$waktu_perawat_datang=HumanTime($perawat_datang);
						}else{
							$tgl_perawat_datang=date('d-m-Y');
							$waktu_perawat_datang=date('H:i:s');
						}
						if ($pemberitahuan){
							$tgl_pemberitahuan=HumanDateShort($pemberitahuan);
							$waktu_pemberitahuan=HumanTime($pemberitahuan);
						}else{
							$tgl_pemberitahuan=date('d-m-Y');
							$waktu_pemberitahuan=date('H:i:s');
						}
						
					}else{
						$tanggaldaftar=date('d-m-Y');
						$waktudaftar=date('H:i:s');
						$tgl_tiba_dikamar_beda=date('d-m-Y');
						$waktu_tiba_dikamar_beda=date('H:i:s');
						
						$tgl_masuk_jam=date('d-m-Y');
						$waktu_masuk_jam=date('H:i:s');
						
						$tgl_keluar_jam=date('d-m-Y');
						$waktu_keluar_jam=date('H:i:s');
						$tgl_memanggil_perawat=date('d-m-Y');
						$waktu_memanggil_perawat=date('H:i:s');
						$tgl_perawat_datang=date('d-m-Y');
						$waktu_perawat_datang=date('H:i:s');
						$tgl_pemberitahuan=date('d-m-Y');
						$waktu_pemberitahuan=date('H:i:s');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_assesmen_askep=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_askep()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="idtipe" value="<?=$idtipe?>" >		
						<input type="hidden" id="st_tornique" value="<?=$st_tornique?>" >		
						<input type="hidden" id="tab_wizard" value="<?=$tab_wizard?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_assesmen_askep=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen_askep" onclick="create_assesmen_askep()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_askep()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen_askep()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										
										<?}?>
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_askep" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_askep()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id !=''){?>
						<div class="form-group">
							<div class="col-md-12">
								 <!-- Simple Classic Progress Wizard (.js-wizard-simple class is initialized in js/pages/base_forms_wizard.js) -->
                            <!-- For more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/ -->
                            <div class="js-wizard-simple block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="<?=($tab_wizard=='1'?'active':'')?>">
                                        <a href="#simple-classic-progress-step1" onclick="set_tab_wizard(1)" data-toggle="tab">STEP 1</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='2'?'active':'')?>">
                                        <a href="#simple-classic-progress-step2" onclick="set_tab_wizard(2)" data-toggle="tab">STEP 2</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='3'?'active':'')?>">
                                        <a href="#simple-classic-progress-step3" onclick="set_tab_wizard(3)" data-toggle="tab">STEP 3</a>
                                    </li>
									<li class="<?=($tab_wizard=='4'?'active':'')?>">
                                        <a href="#simple-classic-progress-step4" onclick="set_tab_wizard(4)" data-toggle="tab">STEP 4</a>
                                    </li>
									<li class="<?=($tab_wizard=='5'?'active':'')?>">
                                        <a href="#simple-classic-progress-step5" onclick="set_tab_wizard(5)" data-toggle="tab">STEP 5</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="form-horizontal" action="base_forms_wizard.html" method="post">
                                    <!-- Steps Progress -->
                                    <div class="block-content block-content-mini block-content-full border-b">
                                        <div class="wizard-progress progress progress-mini remove-margin-b">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
                                        </div>
                                    </div>
                                    <!-- END Steps Progress -->

                                    <!-- Steps Content -->
                                    <div class="block-content tab-content">
                                        <!-- Step 1 -->
                                        <div class="tab-pane fade fade-up   push-50  <?=($tab_wizard=='1'?'in active':'')?>" id="simple-classic-progress-step1">
											
                                           <div class="form-group">
												<div class="col-md-6 ">
													<label for="diagnosa">Diagnosa</label>
													<input id="diagnosa" class="form-control auto_blur" type="text"  value="{diagnosa}" name="diagnosa" placeholder="Diagnosa" >
												</div>
												
												<div class="col-md-4 ">
													<label for="tindakan">Tindakan</label>
													<input id="tindakan" class="form-control auto_blur" type="text"  value="{tindakan}" name="tindakan" placeholder="Tindakan" >
												</div>
												<div class="col-md-2 ">
													<label for="tindakan">Tanggal Pembedahan</label>
													<div class="input-group date">
														<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="tanggal_bedah" placeholder="HH/BB/TTTT" value="<?=($tanggal_bedah?HumanDateShort($tanggal_bedah):HumanDateShort(date('Y-m-d')))?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN OPERASI (Disi Oleh Perawat Ruangan)</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_awal">Keadaan Umum Pra Operasi</label>
													<select id="tingkat_kesadaran_awal"   name="tingkat_kesadaran_awal" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_awal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="gcs_e">GCS</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_e" name="gcs_e" value="{gcs_e}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_m" name="gcs_m" value="{gcs_m}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_v" name="gcs_v" value="{gcs_v}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1">Pupil</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="pupil_1" name="pupil_1" value="{pupil_1}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  auto_blur"  type="text" id="pupil_2" name="pupil_2" value="{pupil_2}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pernafasan">Pernafasan</label>
													<select id="pernafasan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pernafasan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(327) as $row){?>
														<option value="<?=$row->id?>" <?=($pernafasan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pernafasan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2 ">
													<label for="pernafasan_lain">Keterangan Lain Pernafasan</label>
													<input id="pernafasan_lain" class="form-control auto_blur" type="text"  value="{pernafasan_lain}" name="pernafasan_lain" placeholder="" >
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5" >TANDA VITAL</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv">Tanda Vital Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control auto_blur" id="jam_tv" name="jam_tv" value="<?= ($jam_tv?$jam_tv:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran"   name="tingkat_kesadaran" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input class="form-control decimal auto_blur"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi">Frekuensi Nadi</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas">Frekuensi Nafas</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2">SpO2</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="spo2" name="spo2" value="{spo2}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Tinggi Badan</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
														<span class="input-group-addon">Cm</span>
														
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Berat Badan</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
														<span class="input-group-addon">Kg</span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-3">
													<label for="visite_do">Visite Dokter Operator Sebelum Operasi</label>
													<select id="visite_do" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($visite_do == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(328) as $row){?>
														<option value="<?=$row->id?>" <?=($visite_do == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($visite_do == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="visite_da">Visite Dokter Anestesi Sebelum Operasi</label>
													<select id="visite_da" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($visite_da == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(328) as $row){?>
														<option value="<?=$row->id?>" <?=($visite_da == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($visite_da == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<?
												$riwayat_penyakit=($riwayat_penyakit_id?explode(',',$riwayat_penyakit_id):array());
											?>
											<div class="form-group">
												<div class="col-md-6">
													<label for="riwayat_penyakit_id">Riwayat Penyakit</label>
													<select id="riwayat_penyakit_id" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<?foreach(list_variable_ref(329) as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$riwayat_penyakit)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
													</select>
													<textarea id="riwayat_penyakit_nama" style="margin-top:10px" class="form-control auto_blur" name="riwayat_penyakit_nama" rows="3" style="width:100%"><?=$riwayat_penyakit_nama?></textarea>
												</div>
												<div class="col-md-6">
														<label for="perawatan">Pengobatan Sekarang / Paramedikasi</label>
														<div class="table-responsive">
															<table class="table table table-bordered table-striped" id="tabel_tindakan">
																<thead>
																	<?if($status_assemen !='2'){?>
																	<tr>
																		<th width="40%" colspan="2">
																			<input id="trx_nama" class="form-control" type="text"  value="" placeholder="Nama Pengobatan" >
																		</th>
																		<th width="30%">
																			
																				<input id="trx_dosis" class="form-control" type="text"  value="" placeholder="Dosis" >
																				
																		</th>
																		<?
																		$trx_jam=HumanTime(date('H:i:s'));
																		?>
																		<th width="20%">
																			<div class="input-group">
																				<input  type="text" class="time-datepicker form-control" id="trx_jam" name="trx_jam" value="<?= $trx_jam ?>" required>
																				<span class="input-group-addon"><i class="si si-clock"></i></span>
																			</div>
																		</th>
																		<th width="10%">
																			<button class="btn btn-info" onclick="simpan_pengobatan()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
																		</th>
																	</tr>
																	<?}?>
																	<tr>
																		<th width="5%">No</th>
																		<th width="35%">Nama Pengobatan</th>
																		<th width="30%">Dosis</th>
																		<th width="20%">Jam</th>
																		<th width="10%">Action</th>
																	</tr>
																</thead>
																<tbody></tbody>
															</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-3">
													<label for="riwayat_operasi">Riwayat Operasi</label>
													<select id="riwayat_operasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($riwayat_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(330) as $row){?>
														<option value="<?=$row->id?>" <?=($riwayat_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($riwayat_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="riwayat_operasi_ket">Sebutkan Jika Ya</label>
													<input id="riwayat_operasi_ket" class="form-control auto_blur" type="text"  value="{riwayat_operasi_ket}" name="riwayat_operasi_ket" placeholder="" >
												</div>
												<div class="col-md-3">
													<label for="pem_lab">Pemeriksaan Labolatorium</label>
													<select id="pem_lab" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pem_lab == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(331) as $row){?>
														<option value="<?=$row->id?>" <?=($pem_lab == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pem_lab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="hasil_lab">Hasil Terlampir</label>
													<select id="hasil_lab" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($hasil_lab == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(332) as $row){?>
														<option value="<?=$row->id?>" <?=($hasil_lab == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($hasil_lab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="persiapan_darah">Persiapan Darah</label>
													<select id="persiapan_darah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($persiapan_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(333) as $row){?>
														<option value="<?=$row->id?>" <?=($persiapan_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($persiapan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_darah">Pilih Jenis Darah Jika Ya</label>
													<select id="jenis_darah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(334) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jumlah_darah">Jumlah</label>
													<input id="jumlah_darah" class="form-control auto_blur number" type="text"  value="{jumlah_darah}" name="jumlah_darah" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="alat_invasi">Alat Invasif Terpasang</label>
													<select id="alat_invasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($alat_invasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(335) as $row){?>
														<option value="<?=$row->id?>" <?=($alat_invasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($alat_invasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="minum_jamu">Riw Minum Jamu/Tradisional</label>
													<select id="minum_jamu" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($minum_jamu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(336) as $row){?>
														<option value="<?=$row->id?>" <?=($minum_jamu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($minum_jamu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_jamu">Jenis Jamu</label>
													<input id="jenis_jamu" class="form-control auto_blur " type="text"  value="{jenis_jamu}" name="jenis_jamu" placeholder="" >
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="hasil_rad">Hasil Radiologi</label>
													<select id="hasil_rad" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($hasil_rad == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(337) as $row){?>
														<option value="<?=$row->id?>" <?=($hasil_rad == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($hasil_rad == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_rad">Pilih Jenis  Jika Ya</label>
													<input id="jenis_rad" class="form-control auto_blur " type="text"  value="{jenis_rad}" name="jenis_rad" placeholder="" >
													
												</div>
												<div class="col-md-2">
													<label for="lembar_rad">Jumlah Lembar</label>
													<input id="lembar_rad" class="form-control auto_blur number" type="text"  value="{lembar_rad}" name="lembar_rad" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="st_emosional">Status Emosional</label>
													<select id="st_emosional" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($st_emosional == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(338) as $row){?>
														<option value="<?=$row->id?>" <?=($st_emosional == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_emosional == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="icu">Persiapan ICU</label>
													<select id="icu" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($icu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(339) as $row){?>
														<option value="<?=$row->id?>" <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($icu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="icu_acc">Surat Persetujuan ICU, Bila Ya</label>
													<select id="icu_acc" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($icu_acc == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(340) as $row){?>
														<option value="<?=$row->id?>" <?=($icu_acc == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($icu_acc == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-3">
													<label for="izin_operasi">Surat Izin Operasi</label>
													<select id="izin_operasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($izin_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(341) as $row){?>
														<option value="<?=$row->id?>" <?=($izin_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($izin_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="izin_anestesi">Surat Izin Anestesi</label>
													<select id="izin_anestesi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($izin_anestesi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(342) as $row){?>
														<option value="<?=$row->id?>" <?=($izin_anestesi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($izin_anestesi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN PRA OPERASI (Disi Oleh Perawat Ruangan & Kamar Operasi)</h5>
												</div>
											</div>
											<div class="form-group">
												
												<div class="col-md-2 ">
													<label for="izin_anestesi">Tiba Dikamar Bedah</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_tiba_dikamar_beda" placeholder="HH/BB/TTTT" name="tgl_tiba_dikamar_beda" value="<?= $tgl_tiba_dikamar_beda ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="izin_anestesi">Waktu</label>
													<div class=" input-group">
														<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_tiba_dikamar_beda" name="waktu_tiba_dikamar_beda" value="<?= $waktu_tiba_dikamar_beda ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_awal_opr">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_awal_opr"   name="tingkat_kesadaran_awal_opr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_awal_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="gcs_e">GCS</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_e_opr" name="gcs_e_opr" value="{gcs_e_opr}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_m_opr" name="gcs_m_opr" value="{gcs_m_opr}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_v_opr" name="gcs_v_opr" value="{gcs_v_opr}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1">Pupil</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="pupil_1_opr" name="pupil_1_opr" value="{pupil_1_opr}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  auto_blur"  type="text" id="pupil_2_opr" name="pupil_2_opr" value="{pupil_2_opr}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label for="pernafasan_opr">Pernafasan</label>
													<select id="pernafasan_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pernafasan_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(327) as $row){?>
														<option value="<?=$row->id?>" <?=($pernafasan_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pernafasan_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="pernafasan_lain_opr">Keterangan Lain Pernafasan</label>
													<input id="pernafasan_lain_opr" class="form-control auto_blur" type="text"  value="{pernafasan_lain_opr}" name="pernafasan_lain_opr" placeholder="" >
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5" >TANDA VITAL</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv_opr">Tanda Vital Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control auto_blur" id="jam_tv_opr" name="jam_tv_opr" value="<?= ($jam_tv_opr?$jam_tv_opr:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran_opr">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_opr"   name="tingkat_kesadaran_opr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="td_sistole_opr"  value="{td_sistole_opr}" name="td_sistole_opr" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input class="form-control decimal auto_blur"  type="text" id="td_diastole_opr" value="{td_diastole_opr}"  name="td_diastole_opr" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="suhu_opr" name="suhu_opr" value="{suhu_opr}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi_opr">Frekuensi Nadi</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="nadi_opr" name="nadi_opr" value="{nadi_opr}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas_opr">Frekuensi Nafas</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="nafas_opr" name="nafas_opr" value="{nafas_opr}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_opr">SpO2</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="spo2_opr" name="spo2_opr" value="{spo2_opr}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Tinggi Badan</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="tinggi_badan_opr"  value="{tinggi_badan_opr}" name="tinggi_badan_opr" placeholder="cm" required>
														<span class="input-group-addon">Cm</span>
														
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Berat Badan</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="berat_badan_opr"  value="{berat_badan_opr}" name="berat_badan_opr" placeholder="Kg" required>
														<span class="input-group-addon">Kg</span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_ic">
															<thead>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<select id="trx_ic" class="js-select2 form-control " style="width: 100%;" data-placeholder="Line" required>
																			<option value="0" selected>-Pilih Line-</option>
																			<?foreach(list_variable_ref(343) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama?></option>
																			<?}?>
																		</select>
																	</th>
																	
																	<th >
																		<input id="trx_lokasi" class="form-control" type="text"  value="" placeholder="Lokasi" >
																	</th>
																	
																	<th >
																		<input id="trx_kondisi" class="form-control" type="text"  value="" placeholder="Kondisi" >
																	</th>
																	<th >
																		<button class="btn btn-info" onclick="simpan_ic()" id="btn_add_ic" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																<tr>
																	<th width="5%">NO</th>
																	<th width="45%">LINE</th>
																	<th width="20%">LOKASI</th>
																	<th width="20%">KONDISI</th>
																	<th width="10%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top:-10px">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
															<thead>
																<tr>
																	<th width="5%" class="text-center">NO</th>
																	<th width="30%" class="text-center">JENIS PARAMETER</th>
																	<?
																	$q="SELECT H.jenis as id,M.ref as nama 
																	FROM `tranap_assesmen_askep_pengkajian` H 
																	INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='386'
																	WHERE H.assesmen_id='$assesmen_id'
																	GROUP BY H.jenis
																	ORDER BY H.jenis";
																	$list_header=$this->db->query($q)->result();
																	$jml_kolom=count($list_header);
																	$lebar_kolom=50 / $jml_kolom;
																	foreach($list_header as $r){
																	?>
																	<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
																	
																	<?}?>
																	<th width="15%" class="text-center">KETERANGAN</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<table width="100%">
														<tr>
															<td width="25" class="text-center"><strong>PERAWAT RUANGAN</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center"><strong>PERAWAT KAMAR BEDAH</strong></td>
														</tr>
														<tr>
															<td width="25" class="text-center"><strong>
																<?if ($perawat_ruangan_ttd){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_ruangan_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_ruangan_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT RUANGAN</button>
																<?}?>
															</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_ruangan_bedah_ttd){?>
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_ruangan_bedah_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan_bedah()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_ruangan_bedah_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_bedah()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT KAMAR BEDAH</button>
																<?}?>
															</td>
														</tr>
														<tr>
															<td width="25" class="text-center">
															<?if ($perawat_ruangan_id){?>
															<strong>( <?=get_nama_ppa($perawat_ruangan_id)?> )</strong> 
															<?}?>
															</td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_ruangan_bedah_id){?>
																<strong>( <?=get_nama_ppa($perawat_ruangan_bedah_id)?> )</strong> 
															<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>
											
											
										</div>
                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='2'?'in active':'')?>" id="simple-classic-progress-step2">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN INTRA OPERASI (Disi Oleh Perawat Kamar Bedah)</h5>
												</div>
											</div>
											<div class="form-group" style="margin-bottom=-10px">
												<div class="col-md-6 ">
													<label class="h5 " ><strong>DIAGNOSA KEPERAWATAN</strong></h5>
												</div>
												<div class="col-md-6 ">
													<label class="h5 " ><strong>RENCANA ASUHAN KEPERAWATAN</strong></h5>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-6 ">
													<?if($status_assemen !='2'){?>
													<div class="table-responsive">
														<table class="table" id="index_header">
															<thead>
																<tr>
																	<th width="55%">
																		Diagnosa
																	</th>
																	<th width="25%">
																		Priority
																	</th>
																	<th width="20%">
																		
																	</th>
																</tr>
																<tr>
																	<th width="55%">
																		<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
																		<select id="mdiagnosa_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																			<option value="" selected>Pilih Opsi</option>
																			<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="25%">
																		<select id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																			
																			<?for($i=1;$i<20;$i++){?>
																			<option value="<?=$i?>"><?=$i?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="20%">
																		<span class="input-group-btn">
																			<button class="btn btn-info" onclick="simpan_diagnosa_askep()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
																			<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
																		</span>
																	</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
													<?}?>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_diagnosa">
															<thead>
																<tr>
																	<th width="5%">No</th>
																	<th width="40%">Diagnosa Keperawatan</th>
																	<th width="10%">Priority</th>
																	<th width="25%">User</th>
																	<th width="20%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<div class="col-md-12 ">
														<label for="example-input-normal">Diagnosa</label>
														<select id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
															
														</select>
													</div>
													<div class="col-md-12 push-10-t">
														<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
															<thead></thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-4">
													<label for="catatan_diagnosa">Catatan</label>
													<input id="catatan_diagnosa" class="form-control auto_blur " type="text"  value="{catatan_diagnosa}" name="catatan_diagnosa" placeholder="" >
													
												</div>
												<div class="col-md-2">
													<label for="respon_mata">Respon Membuka Mata</label>
													<select id="respon_mata" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($respon_mata == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(344) as $row){?>
														<option value="<?=$row->id?>" <?=($respon_mata == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($respon_mata == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-3">
													<label for="respon_verbal">Respon Verbal</label>
													<select id="respon_verbal" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($respon_verbal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(345) as $row){?>
														<option value="<?=$row->id?>" <?=($respon_verbal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($respon_verbal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="respon_motorik">Respon Motorik</label>
													<select id="respon_motorik" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($respon_motorik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(346) as $row){?>
														<option value="<?=$row->id?>" <?=($respon_motorik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($respon_motorik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												
											</div>
											<div class="form-group">
												<div class="col-md-6 ">
													<label for="tabel_anestesi">Anestesi</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_anestesi">
															<thead>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<div class=" input-group">
																			<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_jam_anestesi" placeholder="HH/BB/TTTT" name="tgl_jam_anestesi" value="<?= $tgl_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_jam_anestesi" name="waktu_jam_anestesi" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	
																	
																	<th >
																		<button class="btn btn-info" onclick="simpan_anestesi()" id="btn_add_anestesi" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																<tr>
																	<th width="5%">NO</th>
																	<th width="45%">ANESTESI</th>
																	<th width="10%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<label for="tabel_anestesi">Pembedahan</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_pembedahan">
															<thead>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<div class=" input-group">
																			<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_jam_pembedahan" placeholder="HH/BB/TTTT" name="tgl_jam_pembedahan" value="<?= $tgl_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_jam_pembedahan" name="waktu_jam_pembedahan" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	
																	
																	<th >
																		<button class="btn btn-info" onclick="simpan_pembedahan()" id="btn_add_pembedahan" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																<tr>
																	<th width="5%">NO</th>
																	<th width="45%">PEMBEDAHAN</th>
																	<th width="10%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tipe_opr">Tipe Operasi</label>
													<select id="tipe_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($tipe_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(347) as $row){?>
														<option value="<?=$row->id?>" <?=($tipe_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tipe_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_bius">Jenis Pembiusan</label>
													<select id="jenis_bius" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_bius == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(348) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_bius == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bius == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="kesadaran_opr">Kesadaran</label>
													<select id="kesadaran_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kesadaran_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(349) as $row){?>
														<option value="<?=$row->id?>" <?=($kesadaran_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kesadaran_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="st_emosi_opr">Status Emosi</label>
													<select id="st_emosi_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($st_emosi_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(350) as $row){?>
														<option value="<?=$row->id?>" <?=($st_emosi_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_emosi_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-2">
													<label for="canul">Posisi Canul Intra Vena</label>
													<select id="canul" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($canul == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(351) as $row){?>
														<option value="<?=$row->id?>" <?=($canul == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($canul == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_operasi">Jenis Operasi</label>
													<select id="jenis_operasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(352) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="posisi_operasi">Posisi Operasi</label>
													<select id="posisi_operasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(353) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<?
												$arr_diawasi=($diawasi?explode(',',$diawasi):array());
												$arr_dipasang_oleh=($dipasang_oleh?explode(',',$dipasang_oleh):array());
												$arr_dipasang_netral=($dipasang_netral?explode(',',$dipasang_netral):array());
												?>
												<div class="col-md-2">
													<label for="diawasi">Diawasi Oleh</label>
													<select id="diawasi" name="diawasi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$arr_diawasi)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
														
													</select>
													
												</div>
												<div class="col-md-2">
													<label for="posisi_lengan">Posisi Lengan Tangan</label>
													<select id="posisi_lengan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_lengan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(354) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_lengan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_lengan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="kulit">Kulit Dibersihkan Menggunakan</label>
													<select id="kulit" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kulit == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(355) as $row){?>
														<option value="<?=$row->id?>" <?=($kulit == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kulit == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="urine">Urine Caterer</label>
													<select id="urine" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($urine == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(356) as $row){?>
														<option value="<?=$row->id?>" <?=($urine == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($urine == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-2">
													<label for="dipasang_oleh">Dipasang Oleh</label>
													<select id="dipasang_oleh" name="dipasang_oleh" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$arr_dipasang_oleh)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
														
													</select>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="disinfeksi">Disinfeksi Kulit</label>
													<select id="disinfeksi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($disinfeksi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(357) as $row){?>
														<option value="<?=$row->id?>" <?=($disinfeksi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($disinfeksi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-2">
													<label for="disinfeksi_lain">Disinfeksi Kulit Lainnya</label>
													<input id="disinfeksi_lain" class="form-control auto_blur " type="text"  value="{disinfeksi_lain}" name="disinfeksi_lain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="posisi_alat">Posisi Alat Bantu Yg Digunakan</label>
													<select id="posisi_alat" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_alat == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(358) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_alat == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_alat == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="posisi_alat_lain">Posisi Alat Bantu Lainnya</label>
													<input id="posisi_alat_lain" class="form-control auto_blur " type="text"  value="{posisi_alat_lain}" name="posisi_alat_lain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="diatermi">Pemakaian Diatermi</label>
													<select id="diatermi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($diatermi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(359) as $row){?>
														<option value="<?=$row->id?>" <?=($diatermi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($diatermi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="lokasi_netral">Lokasi Netral Elektrode</label>
													<select id="lokasi_netral" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($lokasi_netral == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(360) as $row){?>
														<option value="<?=$row->id?>" <?=($lokasi_netral == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($lokasi_netral == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="dipasang_netral">Dipasang Oleh</label>
													<select id="dipasang_netral" name="dipasang_netral" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$arr_dipasang_netral)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pemeriksaan_kulit">Pemeriksaaan Kulit Sebelum Operasi</label>
													<select id="pemeriksaan_kulit" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemeriksaan_kulit == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(361) as $row){?>
														<option value="<?=$row->id?>" <?=($pemeriksaan_kulit == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemeriksaan_kulit == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pemeriksaan_kulit_setelah">Pemeriksaaan Kulit Setelah Operasi</label>
													<select id="pemeriksaan_kulit_setelah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemeriksaan_kulit_setelah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(362) as $row){?>
														<option value="<?=$row->id?>" <?=($pemeriksaan_kulit_setelah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemeriksaan_kulit_setelah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pemasangan_warm">Pemasangan Warm Blanket</label>
													<select id="pemasangan_warm" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemasangan_warm == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(363) as $row){?>
														<option value="<?=$row->id?>" <?=($pemasangan_warm == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemasangan_warm == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_warm">Jenis </label>
													<input id="jenis_warm" class="form-control auto_blur " type="text"  value="{jenis_warm}" name="jenis_warm" placeholder="" >
												</div>
												<?
													$jam_mulai=($jam_mulai?HumanTime($jam_mulai):HumanTime(date('H:i:s')));
													$jam_selesai=($jam_selesai?HumanTime($jam_selesai):HumanTime(date('H:i:s')));
												?>
												<div class="col-md-1">
													<label for="jam_mulai">Mulai </label>
														<input  type="text" class="time-datepicker form-control auto_blur" id="jam_mulai" name="jam_mulai" value="<?= $jam_mulai ?>" required>
												</div>
												<div class="col-md-1">
													<label for="jam_selesai">Selesai </label>
														<input  type="text" class="time-datepicker form-control auto_blur" id="jam_selesai" name="jam_selesai" value="<?= $jam_selesai ?>" required>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label class="css-input switch switch-sm switch-primary">
														<input type="checkbox" id="chck_st_tornique" <?=($st_tornique=='0'?'':'checked')?>><span></span> <strong>Pemakaian Tornique</strong>
													</label>
												</div>
												
											</div>
											<div class="form-group div_tornique" style="margin-top: -10px;">
												<div class="col-md-12">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_tornique">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="35%">LOKASI</th>
																	<th width="10%" class="text-center">JAM MULAI</th>
																	<th width="10%" class="text-center">JAM SELESAI</th>
																	<th width="30%" class="text-center">DIPASANG OLEH</th>
																	<th width="10%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<div class="input-group">
																			<select id="lokasi_tornique" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																				<option value="0" selected>-Tentukan Lokasi-</option>
																				<?foreach(list_variable_ref(364) as $row){?>
																				<option value="<?=$row->id?>"><?=$row->nama?></option>
																				<?}?>
																				
																			</select>
																			<span class="input-group-btn">
																				<button class="btn btn-primary" title="Tambah Lokasi" onclick="add_referensi('lokasi_tornique','364','Lokasi Tornique')" type="button"><i class="fa fa-plus"></i></button>
																			</span>
																		</div>
																		
																	</th>
																	<th>
																		<div class=" input-group">
																			<input  type="text" class="time-datepicker form-control" id="mulai_tornique" name="mulai_tornique" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	
																	
																	<th >
																		<div class=" input-group">
																			<input  type="text" class="time-datepicker form-control" id="selesai_tornique" name="selesai_tornique" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	<th >
																		<select id="dipasang_tornique" name="dipasang_tornique" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
																			<option value="0" selected>- Pilih Petugas -</option>
																			<?foreach($list_ppa as $r){?>
																			<option value="<?=$r->id?>"><?=$r->nama?></option>
																			<?}?>
																			
																			
																		</select>
																	</th>
																	<th >
																		<button class="btn btn-info" onclick="simpan_tornique()" id="btn_add_tornique" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												
											</div>	
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="pemakaian_implan">Pemakaian Implant</label>
													<select id="pemakaian_implan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemakaian_implan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(365) as $row){?>
														<option value="<?=$row->id?>" <?=($pemakaian_implan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemakaian_implan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_implan">Jenis  Imiplant</label>
													<input id="jenis_implan" class="form-control auto_blur " type="text"  value="{jenis_implan}" name="jenis_implan" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="lokasi_implan">Lokasi Implan</label>
													<input id="lokasi_implan" class="form-control auto_blur " type="text"  value="{lokasi_implan}" name="lokasi_implan" placeholder="" >
												</div>
												
												<div class="col-md-2">
													<label for="drain">Pemakaian Drain</label>
													<select id="drain" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($drain == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(366) as $row){?>
														<option value="<?=$row->id?>" <?=($drain == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($drain == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_drain">Jenis  Drain</label>
													<input id="jenis_drain" class="form-control auto_blur " type="text"  value="{jenis_drain}" name="jenis_drain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="lokasi_drain">Lokasi Drain</label>
													<input id="lokasi_drain" class="form-control auto_blur " type="text"  value="{lokasi_drain}" name="lokasi_drain" placeholder="" >
												</div>
											</div>						
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="irigasi_luka">Irigasi Luka</label>
													<select id="irigasi_luka" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($irigasi_luka == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(367) as $row){?>
														<option value="<?=$row->id?>" <?=($irigasi_luka == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($irigasi_luka == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="irigasi_luka_dengan">Irigasi Luka Dengan</label>
													<select id="irigasi_luka_dengan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($irigasi_luka_dengan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(368) as $row){?>
														<option value="<?=$row->id?>" <?=($irigasi_luka_dengan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($irigasi_luka_dengan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="irigasi_luka_lain">Irigasi Lainnya</label>
													<input id="irigasi_luka_lain" class="form-control auto_blur " type="text"  value="{irigasi_luka_lain}" name="irigasi_luka_lain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="tampon">Tampon</label>
													<select id="tampon" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($tampon == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(369) as $row){?>
														<option value="<?=$row->id?>" <?=($tampon == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tampon == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<textarea id="riwayat_penyakit_nama" style="margin-top:10px" class="form-control auto_blur" name="riwayat_penyakit_nama" rows="3" style="width:100%"><?=$riwayat_penyakit_nama?></textarea>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="tabel_cairan">Pemakaian Cairan Inpus</label> 
													
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_cairan">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="20%" class="text-center">INPUT</th>
																	<th width="20%" class="text-center">JML (CC)</th>
																	<th width="20%" class="text-center">OUTPUT</th>
																	<th width="20%" class="text-center">JML (CC)</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<input  type="text" class="form-control" id="input_cairan" name="input_cairan" value="" required>
																	</th>
																	<th>
																		<div class=" input-group">
																			<input  type="text" class="form-control " onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="input_jml_cairan" name="input_jml_cairan" value="" required>
																			<span class="input-group-addon">CC</span>
																		</div>
																	</th>
																	<th>
																		<input  type="text" class="form-control" id="output_cairan" name="output_cairan" value="" required>
																	</th>
																	<th>
																		<div class=" input-group">
																			<input  type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="output_jml_cairan" name="output_jml_cairan" value="" required>
																			<span class="input-group-addon">CC</span>
																		</div>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_cairan()" id="btn_add_cairan" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
															<tfoot>
															<tr>
															  <th colspan="3">Keseimbangan Cairan dan Input dan Outpupt = Balance</th>
															  <th colspan="3"><input id="keseimbangan_cairan" class="form-control auto_blur " type="text"  value="{keseimbangan_cairan}" name="keseimbangan_cairan" placeholder="" ></th>
															</tr>
														  </tfoot>
														</table>
													</div>
												</div>
												<div class="col-md-6">
													<label for="tabel_spesimen">Spesimen</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_spesimen">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="40%" class="text-center">SPESIMEN</th>
																	<th width="40%" class="text-center">JENIS</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<select id="spesimen_id" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="0" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(370) as $row){?>
																			<option value="<?=$row->id?>" ><?=$row->nama?></option>
																			<?}?>
																			
																		</select>
																	</th>
																	
																	<th>
																		<input  type="text" class="form-control" id="spesimen_jenis" name="spesimen_jenis" value="" required>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_spesimen()" id="btn_add_spesimen" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="tabel_cairan">Perhitungan Kassa, Jarum, Bisturi DLL</label> 
													<div class="btn-group">
													<button class="btn btn-primary btn-xs" title="Tambah Referensi" onclick="add_referensi('','384','Perhitungan Kassa Dll')" type="button"><i class="fa fa-plus"></i> Tambah Referensi</button>
													<button class="btn btn-success btn-xs" title="Tambah Referensi" onclick="refresh_kassa()" type="button"><i class="fa fa-refresh"></i> Sync Referensi</button>
													</div>
													<br>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_kassa">
															<thead>
																<tr>
																	<th width="40%" class="text-center">PARAMETER</th>
																	<th width="15%" class="text-center">SEBELUM OPERASI</th>
																	<th width="15%" class="text-center">PENAMBAHAN SELAMA OPERASI</th>
																	<th width="15%" class="text-center">JUMLAH SETELAH OPERASI</th>
																	<th width="15%" class="text-center">JUMLAH YANG DIGUNAKAN</th>
																</tr>
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-12">
													<label for="tabel_cairan">Perhitungan Cairan Instrument</label> 
													<div class="btn-group">
													<button class="btn btn-primary btn-xs" title="Tambah Referensi" onclick="add_referensi('','385','Perhitungan Cairan Instrument')" type="button"><i class="fa fa-plus"></i> Tambah Referensi</button>
													<button class="btn btn-success btn-xs" title="Sync Referensi" onclick="refresh_instrumen()" type="button"><i class="fa fa-refresh"></i> Sync Referensi</button>
													</div>
													<br>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_instrumen">
															<thead>
																<tr>
																	<th width="40%" class="text-center">PARAMETER</th>
																	<th width="15%" class="text-center">JUMLAH PRE OPERASI</th>
																	<th width="15%" class="text-center">TAMBAHAN DURANTE OPERASI</th>
																	<th width="15%" class="text-center">JUMLAH POST OPERASI</th>
																	<th width="15%" class="text-center">SELISIH</th>
																</tr>
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
											</div>
                                        </div>
                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='3'?'in active':'')?>" id="simple-classic-progress-step3">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DURANTE OPERASI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-bottom=-10px">
												<div class="col-md-6 ">
													<label class="h5 " ><strong>DIAGNOSA KEPERAWATAN</strong></h5>
												</div>
												<div class="col-md-6 ">
													<label class="h5 " ><strong>RENCANA ASUHAN KEPERAWATAN</strong></h5>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 ">
													<?if($status_assemen !='2'){?>
													<div class="table-responsive">
														<table class="table" id="index_header_durante">
															<thead>
																<tr>
																	<th width="55%">
																		Diagnosa
																	</th>
																	<th width="25%">
																		Priority
																	</th>
																	<th width="20%">
																		
																	</th>
																</tr>
																<tr>
																	<th width="55%">
																		<input class="form-control " type="hidden" id="diagnosa_durante_id"  value="" placeholder="" >
																		<select id="mdiagnosa_durante_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																			<option value="" selected>Pilih Opsi</option>
																			<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="25%">
																		<select id="prioritas_durante" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Prioritas" required>
																			
																			<?for($i=1;$i<20;$i++){?>
																			<option value="<?=$i?>"><?=$i?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="20%">
																		<span class="input-group-btn">
																			<button class="btn btn-info" onclick="simpan_diagnosa_durante_askep()" id="btn_add_diagnosa_durante" type="button"><i class="fa fa-plus"></i> Add</button>
																			<button class="btn btn-warning" onclick="clear_input_diagnosa_durante()" type="button"><i class="fa fa-refresh"></i></button>
																		</span>
																	</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
													<?}?>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_diagnosa_durante">
															<thead>
																<tr>
																	<th width="5%">No</th>
																	<th width="40%">Diagnosa Keperawatan</th>
																	<th width="10%">Priority</th>
																	<th width="25%">User</th>
																	<th width="20%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<div class="col-md-12 ">
														<label for="diagnosa_durante_id_list">Diagnosa</label>
														<select id="diagnosa_durante_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
															
														</select>
													</div>
													<div class="col-md-12 push-10-t">
														<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan_durante">
															<thead></thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="catatan_all">Catatan</label>
													<textarea class="form-control auto_blur" name="catatan_all" rows="4" id="catatan_all"><?=$catatan_all?></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<table width="100%">
														<tr>
															<td width="33" class="text-center"><strong>PERAWAT ASSISTEN / INSTRUMEN</strong></td>
															<td width="33" class="text-center"><strong>PERAWAT SIRKULER</strong></td>
															<td width="33" class="text-center"><strong>PERAWAT ANESTESI</strong></td>
														</tr>
														<tr>
															<td width="33" class="text-center">
																<strong>
																<?if ($perawat_assisten_id){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_assisten_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_asisten()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_assisten_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_asisten()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT ASSISTEN / INSTRUMEN</button>
																<?}?>
																</strong>
															</td>
															<td width="33" class="text-center">
																<strong>
																<?if ($perawat_sirkuler_id){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_sirkuler_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_sirkuler()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_sirkuler_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_sirkuler()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT SIRKULER</button>
																<?}?>
																</strong>
															</td>
															
															<td width="33" class="text-center">
																<?if ($perawat_anestesi_id){?>
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_anestesi_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_anestesi()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_anestesi_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_anestesi()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT ANESTESI</button>
																<?}?>
															</td>
														</tr>
														<tr>
															<td width="33%" class="text-center">
															<?if ($perawat_assisten_id){?>
															<strong>( <?=get_nama_ppa($perawat_assisten_id)?> )</strong> 
															<?}?>
															</td>
															<td width="33%" class="text-center">
															<?if ($perawat_sirkuler_id){?>
															<strong>( <?=get_nama_ppa($perawat_sirkuler_id)?> )</strong> 
															<?}?>
															</td>
															<td width="33%" class="text-center">
																<?if ($perawat_anestesi_id){?>
																<strong>( <?=get_nama_ppa($perawat_anestesi_id)?> )</strong> 
															<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>
											
                                        </div>
                                        <!-- END Step 3 -->
										<!-- Step 4 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='4'?'in active':'')?>" id="simple-classic-progress-step4">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN PASKA OPERASI</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="ruang_pemulihan">Ruang Pemulihan</label>
													<select id="ruang_pemulihan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($ruang_pemulihan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(369) as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_pemulihan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($ruang_pemulihan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="tgl_masuk_jam">Masuk Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_masuk_jam" placeholder="HH/BB/TTTT" name="tgl_masuk_jam" value="<?= $tgl_masuk_jam ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_masuk_jam" name="waktu_masuk_jam" value="<?= $waktu_masuk_jam ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="izin_anestesi">Keluar Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_keluar_jam" placeholder="HH/BB/TTTT" name="tgl_keluar_jam" value="<?= $tgl_keluar_jam ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_keluar_jam" name="waktu_keluar_jam" value="<?= $waktu_keluar_jam ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												
												<div class="col-md-2 ">
													<label for="kembali_ke">Kembali Ke</label>
													<select id="kembali_ke" name="kembali_ke" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
														<option value="0" <?=($kembali_ke=='' || $kembali_ke=='0'?'selected':'')?>>- Pilih Ruangan -</option>
														<?foreach(get_all('mruangan',array('status'=>1,'st_tampil'=>1,'idtipe'=>1)) as $r){?>
														<option value="<?=$r->id?>" <?=($kembali_ke==$r->id?'selected':'')?>><?=$r->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-2 ">
													<label for="ruangan_lainnya">Ruangan lainnya</label>
													<input id="ruangan_lainnya" class="form-control auto_blur" type="text"  value="{ruangan_lainnya}" name="ruangan_lainnya" placeholder="" >
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="keadaan_paska">Keadaan Umum Paska Operasi</label>
													<select id="keadaan_paska"   name="keadaan_paska" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($keadaan_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_paska">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_paska"   name="tingkat_kesadaran_paska" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="gcs_e_paska">GCS</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_e_paska" name="gcs_e_paska" value="{gcs_e_paska}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m_paska">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_m_paska" name="gcs_m_paska" value="{gcs_m_paska}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v_paska">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_v_paska" name="gcs_v_paska" value="{gcs_v_paska}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1_paska">Pupil</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="pupil_1_paska" name="pupil_1_paska" value="{pupil_1_paska}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  auto_blur"  type="text" id="pupil_2_paska" name="pupil_2_paska" value="{pupil_2_paska}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="kulit_datang">Keadaan Kulit Waktu Datang</label>
													<select id="kulit_datang" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kulit_datang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(372) as $row){?>
														<option value="<?=$row->id?>" <?=($kulit_datang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kulit_datang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="kulit_keluar">Keadaan Kulit Waktu Keluar</label>
													<select id="kulit_keluar" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kulit_keluar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(373) as $row){?>
														<option value="<?=$row->id?>" <?=($kulit_keluar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kulit_keluar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="sirkulasi_paska">Sirkulasi Anggota Badan</label>
													<select id="sirkulasi_paska" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($sirkulasi_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(374) as $row){?>
														<option value="<?=$row->id?>" <?=($sirkulasi_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($sirkulasi_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="posisi_paska">Posisi Pasien</label>
													<select id="posisi_paska" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(375) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pendarahan_paska">Pendarahan</label>
													<select id="pendarahan_paska" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pendarahan_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(376) as $row){?>
														<option value="<?=$row->id?>" <?=($pendarahan_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pendarahan_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="lokasi_pendarahan_paska">Lokasi Pendarahan</label>
													<input id="lokasi_pendarahan_paska" class="form-control auto_blur" type="text"  value="{lokasi_pendarahan_paska}" name="lokasi_pendarahan_paska" placeholder="" >
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="muntah_paska">Muntah</label>
													<select id="muntah_paska" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($muntah_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(377) as $row){?>
														<option value="<?=$row->id?>" <?=($muntah_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($muntah_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="mukosa_mulut">Mukosa Mulut</label>
													<select id="mukosa_mulut" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($mukosa_mulut == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(378) as $row){?>
														<option value="<?=$row->id?>" <?=($mukosa_mulut == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($mukosa_mulut == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jaringan_pa">Jaringan PA & Formulir</label>
													<select id="jaringan_pa" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jaringan_pa == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(379) as $row){?>
														<option value="<?=$row->id?>" <?=($jaringan_pa == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jaringan_pa == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jaringan_pa_dari">Jika YA PA Dikirim dari</label>
													<input id="jaringan_pa_dari" class="form-control auto_blur" type="text"  value="{jaringan_pa_dari}" name="jaringan_pa_dari" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="skrining_nyeri">Skrining Nyeri</label>
													<select id="skrining_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($skrining_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(380) as $row){?>
														<option value="<?=$row->id?>" <?=($skrining_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($skrining_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="risko_jatuh_paska">Risiko Jatuh</label>
													<select id="risko_jatuh_paska" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($risko_jatuh_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(381) as $row){?>
														<option value="<?=$row->id?>" <?=($risko_jatuh_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($risko_jatuh_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
											</div>

											<div class="form-group" >
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining_opr">
															<thead>
																<tr>
																	<th width="5%" class="text-center">NO</th>
																	<th width="30%" class="text-center">JENIS PARAMETER</th>
																	<?
																	$q="SELECT H.jenis as id,M.ref as nama 
																	FROM `tranap_assesmen_askep_pengkajian_opr` H 
																	INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='387'
																	WHERE H.assesmen_id='$assesmen_id'
																	GROUP BY H.jenis
																	ORDER BY H.jenis";
																	$list_header=$this->db->query($q)->result();
																	$jml_kolom=count($list_header);
																	$lebar_kolom=65 / $jml_kolom;
																	foreach($list_header as $r){
																	?>
																	<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
																	
																	<?}?>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-bottom=-10px">
												<div class="col-md-6 ">
													<label class="h5 " ><strong>DIAGNOSA KEPERAWATAN</strong></h5>
												</div>
												<div class="col-md-6 ">
													<label class="h5 " ><strong>RENCANA ASUHAN KEPERAWATAN</strong></h5>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 ">
													<?if($status_assemen !='2'){?>
													<div class="table-responsive">
														<table class="table" id="index_header_paska">
															<thead>
																<tr>
																	<th width="55%">
																		Diagnosa
																	</th>
																	<th width="25%">
																		Priority
																	</th>
																	<th width="20%">
																		
																	</th>
																</tr>
																<tr>
																	<th width="55%">
																		<input class="form-control " type="hidden" id="diagnosa_paska_id"  value="" placeholder="" >
																		<select id="mdiagnosa_paska_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																			<option value="" selected>Pilih Opsi</option>
																			<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="25%">
																		<select id="prioritas_paska" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Prioritas" required>
																			
																			<?for($i=1;$i<20;$i++){?>
																			<option value="<?=$i?>"><?=$i?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="20%">
																		<span class="input-group-btn">
																			<button class="btn btn-info" onclick="simpan_diagnosa_paska_askep()" id="btn_add_diagnosa_paska" type="button"><i class="fa fa-plus"></i> Add</button>
																			<button class="btn btn-warning" onclick="clear_input_diagnosa_paska()" type="button"><i class="fa fa-refresh"></i></button>
																		</span>
																	</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
													<?}?>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_diagnosa_paska">
															<thead>
																<tr>
																	<th width="5%">No</th>
																	<th width="40%">Diagnosa Keperawatan</th>
																	<th width="10%">Priority</th>
																	<th width="25%">User</th>
																	<th width="20%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<div class="col-md-12 ">
														<label for="diagnosa_paska_id_list">Diagnosa</label>
														<select id="diagnosa_paska_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
															
														</select>
													</div>
													<div class="col-md-12 push-10-t">
														<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan_paska">
															<thead></thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
												</div>
											</div>
											
                                        </div>
                                        <!-- END Step 4 -->
										<!-- Step 4 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='5'?'in active':'')?>" id="simple-classic-progress-step5">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >SERAH TERIMA DENGAN RUANGAN</h5>
												</div>
											</div>
											<div class="form-group" >
												
												<div class="col-md-3 ">
													<label for="tgl_memanggil_perawat">Memanggil Perawat Ruangan Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_memanggil_perawat" placeholder="HH/BB/TTTT" name="tgl_memanggil_perawat" value="<?= $tgl_memanggil_perawat ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_memanggil_perawat" name="waktu_memanggil_perawat" value="<?= $waktu_memanggil_perawat ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="perawat">Nama Perawat Ruangan </label>
													<select id="perawat" name="perawat" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
														<option value="#" selected>- Pilih -</option>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=($perawat==$r->id?'selected':'')?>><?=$r->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="tgl_perawat_datang">Perawat Ruangan Datang Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_perawat_datang" placeholder="HH/BB/TTTT" name="tgl_perawat_datang" value="<?= $tgl_perawat_datang ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_perawat_datang" name="waktu_perawat_datang" value="<?= $waktu_perawat_datang ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6">
													<label for="tabel_post">Dokumentasi Post Operasi</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_post">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="40%" class="text-center">NAMA DOKUMENTASI</th>
																	<th width="40%" class="text-center">KETERANGAN</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<select id="dokumen_id" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="0" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(382) as $row){?>
																			<option value="<?=$row->id?>" ><?=$row->nama?></option>
																			<?}?>
																			
																		</select>
																	</th>
																	
																	<th>
																		<input  type="text" class="form-control" id="keterangan_pos" name="keterangan_pos" value="" required>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_post()" id="btn_add_post" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6">
													<label for="tabel_barang">Barang / Dokumen Yang Dikembalikan</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_barang">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="40%" class="text-center">NAMA </th>
																	<th width="40%" class="text-center">KETERANGAN</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<select id="dokumen_barang_id" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="0" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(383) as $row){?>
																			<option value="<?=$row->id?>" ><?=$row->nama?></option>
																			<?}?>
																			
																		</select>
																	</th>
																	
																	<th>
																		<input  type="text" class="form-control" id="keterangan_barang" name="keterangan_barang" value="" required>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_barang()" id="btn_add_barang" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" >
												
												<div class="col-md-3 ">
													<label for="tgl_pemberitahuan">Pemberitahuan Kepada Keluarga Pasien</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tgl_pemberitahuan" placeholder="HH/BB/TTTT" name="tgl_pemberitahuan" value="<?= $tgl_pemberitahuan ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control auto_blur" id="waktu_pemberitahuan" name="waktu_pemberitahuan" value="<?= $waktu_pemberitahuan ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-2 ">
													<label for="keterangan_pemberitahuan">Keterangan</label>
													<input id="keterangan_pemberitahuan" class="form-control auto_blur" type="text"  value="{keterangan_pemberitahuan}" name="keterangan_pemberitahuan" placeholder="" >
												</div>
												<div class="col-md-2 ">
													<label for="keadaan_ruangan">Keadaan Umum</label>
													<select id="keadaan_ruangan"   name="keadaan_ruangan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($keadaan_ruangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-1">
													<label for="gcs_e_ruangan">GCS</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_e_ruangan" name="gcs_e_ruangan" value="{gcs_e_ruangan}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m_ruangan">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_m_ruangan" name="gcs_m_ruangan" value="{gcs_m_ruangan}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v_ruangan">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="gcs_v_ruangan" name="gcs_v_ruangan" value="{gcs_v_ruangan}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1_ruangan">Pupil</label>
													<div class="input-group">
														<input class="form-control  auto_blur"  type="text" id="pupil_1_ruangan" name="pupil_1_ruangan" value="{pupil_1_ruangan}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  auto_blur"  type="text" id="pupil_2_ruangan" name="pupil_2_ruangan" value="{pupil_2_ruangan}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5" >TANDA VITAL</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv_ruangan">Tanda Vital Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control auto_blur" id="jam_tv_ruangan" name="jam_tv_ruangan" value="<?= ($jam_tv_ruangan?$jam_tv_ruangan:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran_ruangan">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_ruangan"   name="tingkat_kesadaran_ruangan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_ruangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="td_sistole_ruangan"  value="{td_sistole_ruangan}" name="td_sistole_ruangan" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input class="form-control decimal auto_blur"  type="text" id="td_diastole_ruangan" value="{td_diastole_ruangan}"  name="td_diastole_ruangan" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"  type="text" id="suhu_ruangan" name="suhu_ruangan" value="{suhu_ruangan}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi_ruangan">Frekuensi Nadi</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="nadi_ruangan" name="nadi_ruangan" value="{nadi_ruangan}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas_ruangan">Frekuensi Nafas</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="nafas_ruangan" name="nafas_ruangan" value="{nafas_ruangan}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_ruangan">SpO2</label>
													<div class="input-group">
														<input class="form-control decimal auto_blur"   type="text" id="spo2_ruangan" name="spo2_ruangan" value="{spo2_ruangan}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="catatan_ruangan">Catatan</label>
													<textarea class="form-control auto_blur" name="catatan_ruangan" rows="4" id="catatan_ruangan"><?=$catatan_ruangan?></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<table width="100%">
														<tr>
															<td width="25" class="text-center"><strong>PERAWAT RUANGAN</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center"><strong>PERAWAT RUANGAN PEMULIHAN</strong></td>
														</tr>
														<tr>
															<td width="25" class="text-center"><strong>
																<?if ($perawat_ruangan_5_ttd){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_ruangan_5_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan_5()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_ruangan_5_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_5()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT RUANGAN</button>
																<?}?>
															</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_pemullihan_5_ttd){?>
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_pemullihan_5_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan_pemulihan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_pemullihan_5_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_pemulihan()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT RUANG PEMULIHAN</button>
																<?}?>
															</td>
														</tr>
														<tr>
															<td width="25" class="text-center">
															<?if ($perawat_ruangan_5_id){?>
															<strong>( <?=get_nama_ppa($perawat_ruangan_5_id)?> )</strong> 
															<?}?>
															</td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_pemullihan_5_id){?>
																<strong>( <?=get_nama_ppa($perawat_pemullihan_5_id)?> )</strong> 
															<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>					
                                        </div>
                                        <!-- END Step 4 -->
									</div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-mini block-content-full border-t">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button class="wizard-prev btn btn-default" type="button"><i class="fa fa-arrow-left"></i> Kembali</button>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="wizard-next btn btn-default" type="button">Step Selanjutnya <i class="fa fa-arrow-right"></i></button>
												<?if ($status_assemen=='1'){?>
												
                                                <button class="wizard-finish btn btn-primary" onclick="close_assesmen()" type="button"><i class="fa fa-save"></i> SIMPAN</button>
												<?}else{?>
                                                <button class="wizard-finish btn btn-warning" onclick="simpan_template()" type="button"><i class="fa fa-save"></i> SIMPAN TEMPLATE</button>
												
												<?}?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                           
						   </div>
                            <!-- END Simple Classic Progress Wizard -->
							</div>
						</div>
						<?}?>
					
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-right push-20-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_assesmen_askep=='1'){?>
										<button class="btn btn-primary" id="btn_create_assesmen_askep" onclick="create_assesmen_askep()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										<?if ($assesmen_id!=''){?>
											<?if ($status_assemen=='3'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_askep()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
											<?}?>
											<?if ($status_assemen=='1'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen_askep()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
											<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
											<?}?>
											<?if ($assesmen_id!=''){?>
											<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_askep/<?=$assesmen_id?>" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
											<?}?>
											<?if ($status_assemen=='3'){?>
												<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
											<?}?>
											<?if ($status_assemen=='2'){?>
												<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_rj/input_assesmen_askep" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
											<?}?>
										<?}?>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Pilih -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'st_tampil'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template_askep()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen_askep()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen_askep()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?if ($assesmen_id!=''){?>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"></textarea>
								<input type="hidden" id="jenis_ttd" value="" >		
								<input type="hidden" id="perawat_ruangan_id" value="<?=$perawat_ruangan_id?>" >		
								<textarea id="perawat_ruangan_ttd" name="perawat_ruangan_ttd" style="display: none"><?=$perawat_ruangan_ttd?></textarea>
								<input type="hidden" id="perawat_ruangan_bedah_id" value="<?=$perawat_ruangan_bedah_id?>" >		
								<textarea id="perawat_ruangan_bedah_ttd" name="perawat_ruangan_bedah_ttd" style="display: none"><?=$perawat_ruangan_bedah_ttd?></textarea>
								<input type="hidden" id="perawat_assisten_id" value="<?=$perawat_assisten_id?>" >		
								<textarea id="perawat_assisten_ttd" name="perawat_assisten_ttd" style="display: none"><?=$perawat_assisten_ttd?></textarea>
								<input type="hidden" id="perawat_sirkuler_id" value="<?=$perawat_sirkuler_id?>" >		
								<textarea id="perawat_sirkuler_ttd" name="perawat_sirkuler_ttd" style="display: none"><?=$perawat_sirkuler_ttd?></textarea>
								<input type="hidden" id="perawat_anestesi_id" value="<?=$perawat_anestesi_id?>" >		
								<textarea id="perawat_anestesi_ttd" name="perawat_anestesi_ttd" style="display: none"><?=$perawat_anestesi_ttd?></textarea>
								
								<input type="hidden" id="perawat_ruangan_5_id" value="<?=$perawat_ruangan_5_id?>" >		
								<textarea id="perawat_ruangan_5_ttd" name="perawat_ruangan_5_ttd" style="display: none"><?=$perawat_ruangan_5_ttd?></textarea>
								<input type="hidden" id="perawat_pemullihan_5_id" value="<?=$perawat_pemullihan_5_id?>" >		
								<textarea id="perawat_pemullihan_5_ttd" name="perawat_pemullihan_5_ttd" style="display: none"><?=$perawat_pemullihan_5_ttd?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="petugas_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Perugas" required>
										<option value="0">-Pilih Perawat-</option>
										<?foreach(get_all('mppa',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
									<label for="petugas_id">NAMA PERAWAT</label>
								</div>
							</div>
							
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
		
<script type="text/javascript" src="{js_path}pages/base_forms_wizard.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Merm_referensi/modal_ref'); ?>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});

$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var petugas_id=$("#petugas_id").val();
	if ($("#petugas_id").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Nama ", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_askep/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				jenis_ttd:jenis_ttd,
				petugas_id:petugas_id,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  location.reload();
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}

function hapus_ttd(jenis_ttd){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64").val('');
	$("#jenis_ttd").val(jenis_ttd);
	var signature64=$("#signature64").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_askep/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				jenis_ttd:jenis_ttd,
				petugas_id:0,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").show();
				location.reload();
			}
		});
}

function modal_ttd_asisten(){
	$("#jenis_ttd").val('perawat_assisten_id');
	$("#signature64").val($("#perawat_assisten_ttd").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	$("#petugas_id").val($("#perawat_assisten_id").val()).trigger('change');
	

}
function modal_ttd_sirkuler(){
	$("#jenis_ttd").val('perawat_sirkuler_id');
	$("#signature64").val($("#perawat_sirkuler_ttd").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	$("#petugas_id").val($("#perawat_sirkuler_id").val()).trigger('change');
	

}
function modal_ttd_anestesi(){
	$("#jenis_ttd").val('perawat_anestesi_id');
	$("#signature64").val($("#perawat_anestesi_ttd").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	$("#petugas_id").val($("#perawat_anestesi_id").val()).trigger('change');
	

}
function modal_ttd_ruangan(){
	$("#jenis_ttd").val('perawat_ruangan_id');
	$("#signature64").val($("#perawat_ruangan_ttd").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	$("#petugas_id").val($("#perawat_ruangan_id").val()).trigger('change');
	

}
function modal_ttd_ruangan_5(){
	$("#jenis_ttd").val('perawat_ruangan_5_id');
	$("#signature64").val($("#perawat_ruangan_5_ttd").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	
	$("#petugas_id").val($("#perawat_ruangan_5_id").val()).trigger('change');
	

}
function modal_ttd_ruangan_pemulihan(){
	$("#jenis_ttd").val('perawat_pemullihan_5_id');
	$("#signature64").val($("#perawat_pemullihan_5_ttd").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	$("#petugas_id").val($("#perawat_pemullihan_5_id").val()).trigger('change');
	

}
function modal_ttd_ruangan_bedah(){
	$("#jenis_ttd").val('perawat_ruangan_bedah_id');
	$("#signature64").val($("#perawat_ruangan_bedah_ttd").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	$("#petugas_id").val($("#perawat_ruangan_bedah_id").val()).trigger('change');
	

}

$(".wizard-next").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)+1;
	set_tab_wizard(tab_wizard);
});
$(".wizard-prev").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)-1;
	set_tab_wizard(tab_wizard);
});
function set_tab_wizard($id){
	var assesmen_id=$("#assesmen_id").val();
	$("#tab_wizard").val($id);
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/set_tab_wizard_askep/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			assesmen_id:assesmen_id,
			tab_wizard:$("#tab_wizard").val(),
	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Succes!', message : ' Wizard'});
		  
		}
	});
}
$(document).ready(function() {
	$('.angka').number(true, 0);
	// document.getElementById("nadi").style.color = "#eb0404";
	$(".btn_close_left").click(); 
	// add_referensi('lokasi_tornique','364','Lokasi Tornique');
	disabel_edit();
	// $('#catatan_all').summernote({
	  // height: 100,   //set editable area's height
	  // codemirror: { // codemirror options
		// theme: 'monokai'
	  // }	
	// })	


	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_askep();
		load_skrining_opr_askep();
		// load_hasil_assesmen_askep();
		load_ic_assesmen_askep();
		load_pengobatan_assesmen_askep();
		load_diagnosa_askep();
		load_diagnosa_durante_askep();
		load_diagnosa_paska_askep();
		load_anestesi_assesmen_askep();
		load_pembedahan_assesmen_askep();
		load_status_tornique();
		load_cairan_assesmen_askep();
		load_spesimen_assesmen_askep();
		load_kassa_assesmen_askep();
		load_instrumen_assesmen_askep();
		load_barang_assesmen_askep();
		load_post_assesmen_askep();
		
		if ($("#signature64").val()){
			set_img_canvas();
		}
		
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian()
	}
	
	// load_data_rencana_asuhan_askep(1);
});
 $('#chck_st_tornique').click(function (event) {
	if (this.checked) {
		$("#st_tornique").val(1);
	} else {
		$("#st_tornique").val(0);
		// alert('tidak chekded');
	}
	load_status_tornique();
	// if ($("#st_edited").val()=='0'){
		// $(this).removeClass('input_edited');
		simpan_assesmen();
	// }else{
		// if ($("#st_edited").val()=='1'){
			// if ($(this).val()!=before_edit){
				// console.log('Ada Peruabahan');
				// // $(this).attr('input_edited');
				 // $(this).addClass('input_edited');
			// }
		// }
	// }
});
function load_status_tornique(){
	let st_tornique=$("#st_tornique").val();
	if (st_tornique=='1'){
		$(".div_tornique").show();
		load_tornique_assesmen_askep();
		
	}else{
		$(".div_tornique").hide();
		
	}
}

function load_skrining_opr_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_opr_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_skrining_opr tbody").append(data.opsi);
			// console.LOG(data);
			$(".nilai_opr").select2();
			// get_skor_pengkajian_askep();
			$("#cover-spin").hide();
		}
	});
}
function load_skrining_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			// console.LOG(data);
			$(".nilai").select2();
			// get_skor_pengkajian_askep();
			$("#cover-spin").hide();
		}
	});
}
function load_kassa_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_kassa_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_kassa tbody").empty();
			$("#tabel_kassa tbody").append(data.tabel);
			// console.LOG(data);
			$('.angka').number(true, 0);
			$("#cover-spin").hide();
		}
	});
}
function refresh_kassa(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_kassa_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#cover-spin").hide();
			load_kassa_assesmen_askep();
		}
	});
}
$(document).on("blur",".cls_tabel_kasssa",function(){
	let assesmen_id=$("#assesmen_id").val();
	// let template_id=$("#template_id").val();
	var tr=$(this).closest('tr');
	let row_id=$(this).data("id");
	let sebelum=tr.find(".cls_sebelum").val();
	let selama=tr.find(".cls_selama").val();
	let setelah=tr.find(".cls_setelah").val();
	
	let total=parseFloat(sebelum)+parseFloat(selama)-parseFloat(setelah);
	tr.find(".cls_total").val(total)
	// console.log(sebelum+' '+selama);
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/update_data_kassa_askep', 
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				id:row_id,
				selama:selama,
				sebelum:sebelum,
				setelah:setelah,
		  },success: function(data) {
			  console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
			}
		});
	
});

function load_instrumen_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_instrumen_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_instrumen tbody").empty();
			$("#tabel_instrumen tbody").append(data.tabel);
			// console.LOG(data);
			$('.angka').number(true, 0);
			$("#cover-spin").hide();
		}
	});
}
function refresh_instrumen(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_instrumen_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#cover-spin").hide();
			load_instrumen_assesmen_askep();
		}
	});
}
$(document).on("blur",".cls_tabel_instrumen",function(){
		let assesmen_id=$("#assesmen_id").val();
		var tr=$(this).closest('tr');
		let row_id=$(this).data("id");
		let sebelum=tr.find(".cls_sebelum").val();
		let selama=tr.find(".cls_selama").val();
		let setelah=tr.find(".cls_setelah").val();
		
		let total=parseFloat(sebelum)+parseFloat(selama)-parseFloat(setelah);
		tr.find(".cls_total").val(total)
		// console.log(sebelum+' '+selama);
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_data_instrumen_askep', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					id:row_id,
					selama:selama,
					sebelum:sebelum,
					setelah:setelah,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
			});
		
	});
	
	$(document).on("change",".nilai",function(){
		let assesmen_id=$("#assesmen_id").val();
		// let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		let row_id=$(this).find("option:selected").attr('data-row_id');
		let nilai_nama=$(this).find("option:selected").attr('data-nama');
		var nilai_id=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_askep', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					row_id:row_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					nilai_nama:nilai_nama,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
			});
		
	});
	$(document).on("change",".nilai_opr",function(){
		let assesmen_id=$("#assesmen_id").val();
		// let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		let row_id=$(this).find("option:selected").attr('data-row_id');
		let nilai_nama=$(this).find("option:selected").attr('data-nama');
		var nilai_id=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_opr_askep', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					row_id:row_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					nilai_nama:nilai_nama,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
			});
		
	});
	$(document).on("blur",".peng_keterangan",function(){
		let assesmen_id=$("#assesmen_id").val();
		// let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		let row_id=$(this).data("id");
		console.log(row_id);
		var keterangan=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_ket_askep', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					row_id:row_id,
					assesmen_id:assesmen_id,
					keterangan:keterangan,
			  },success: function(data) {
				  // console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
					// $("#st_tindakan").val(data.st_tindakan);
					 // $("#nama_tindakan").val(data.nama_tindakan);
					 // $("#skor_pengkajian").val(data.skor);
					 // $("#risiko_jatuh").val(data.ref_nilai);
				}
			});
		
	});
	$(document).on("change",".nilai_mengerti",function(){
		let assesmen_id=$("#assesmen_id").val();
		// let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		var risiko_nilai_id=tr.find(".risiko_edukasi_id").val();
		let nilai_nama=$(this).find("option:selected").attr('data-nama');
		var nilai_id=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_mengerti_askep', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					risiko_nilai_id:risiko_nilai_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					nilai_nama:nilai_nama,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
					// $("#st_tindakan").val(data.st_tindakan);
					 // $("#nama_tindakan").val(data.nama_tindakan);
					 // $("#skor_pengkajian").val(data.skor);
					 // $("#risiko_jatuh").val(data.ref_nilai);
				}
			});
		
	});
	function get_skor_pengkajian_askep(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#template_id").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_risiko_jatuh_askep', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },
			  success: function(data) {
				 $("#st_tindakan").val(data.st_tindakan);
				 $("#nama_tindakan").val(data.nama_tindakan);
				 $("#skor_pengkajian").val(data.skor);
				 $("#risiko_jatuh").val(data.ref_nilai);
			  }
		});
		
	}
	$(".opsi_change").change(function(){
		console.log('OPSI CHANTE')
		// if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		// }
	});

	$(".auto_blur").blur(function(){
		// console.log('BLUR')
		// if ($("#st_edited").val()=='0'){
			// $(this).removeClass('input_edited');
			simpan_assesmen();
		// }else{
			// if ($("#st_edited").val()=='1'){
				// if ($(this).val()!=before_edit){
					// console.log('Ada Peruabahan');
					// // $(this).attr('input_edited');
					 // $(this).addClass('input_edited');
				// }
			// }
		// }
		
	});
	function simpan_assesmen(){
	// alert(load_awal_assesmen);
	if (status_assemen !='2'){
		
		if (load_awal_assesmen==false){
		let assesmen_id=$("#assesmen_id").val();
		// alert($("#tanggal_keluar").val());return false;
		let tanggaldatang=$("#tanggaldatang").val();
		let waktudatang=$("#waktudatang").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/save_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						// nama_template:nama_template,
						tanggaldatang:tanggaldatang,
						waktudatang:waktudatang,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						
						diagnosa	: $("#diagnosa").val(),
						tindakan	: $("#tindakan").val(),
						tanggal_bedah	: $("#tanggal_bedah").val(),
						tingkat_kesadaran_awal	: $("#tingkat_kesadaran_awal").val(),
						gcs_e	: $("#gcs_e").val(),
						gcs_m	: $("#gcs_m").val(),
						gcs_v	: $("#gcs_v").val(),
						pupil_1	: $("#pupil_1").val(),
						pupil_2	: $("#pupil_2").val(),
						pernafasan	: $("#pernafasan").val(),
						pernafasan_lain	: $("#pernafasan_lain").val(),
						jam_tv	: $("#jam_tv").val(),
						tingkat_kesadaran	: $("#tingkat_kesadaran").val(),
						nadi	: $("#nadi").val(),
						nafas	: $("#nafas").val(),
						spo2	: $("#spo2").val(),
						td_sistole	: $("#td_sistole").val(),
						td_diastole	: $("#td_diastole").val(),
						suhu	: $("#suhu").val(),
						tinggi_badan	: $("#tinggi_badan").val(),
						berat_badan	: $("#berat_badan").val(),
						visite_do	: $("#visite_do").val(),
						visite_da	: $("#visite_da").val(),
						riwayat_penyakit_id	: $("#riwayat_penyakit_id").val(),
						riwayat_penyakit_nama	: $("#riwayat_penyakit_nama").val(),
						riwayat_operasi	: $("#riwayat_operasi").val(),
						riwayat_operasi_ket	: $("#riwayat_operasi_ket").val(),
						pem_lab	: $("#pem_lab").val(),
						hasil_lab	: $("#hasil_lab").val(),
						persiapan_darah	: $("#persiapan_darah").val(),
						jenis_darah	: $("#jenis_darah").val(),
						jumlah_darah	: $("#jumlah_darah").val(),
						alat_invasi	: $("#alat_invasi").val(),
						minum_jamu	: $("#minum_jamu").val(),
						jenis_jamu	: $("#jenis_jamu").val(),
						hasil_rad	: $("#hasil_rad").val(),
						jenis_rad	: $("#jenis_rad").val(),
						lembar_rad	: $("#lembar_rad").val(),
						st_emosional	: $("#st_emosional").val(),
						icu	: $("#icu").val(),
						icu_acc	: $("#icu_acc").val(),
						izin_operasi	: $("#izin_operasi").val(),
						izin_anestesi	: $("#izin_anestesi").val(),
						tgl_tiba_dikamar_beda	: $("#tgl_tiba_dikamar_beda").val(),
						waktu_tiba_dikamar_beda	: $("#waktu_tiba_dikamar_beda").val(),
						tingkat_kesadaran_awal_opr	: $("#tingkat_kesadaran_awal_opr").val(),
						gcs_e_opr	: $("#gcs_e_opr").val(),
						gcs_m_opr	: $("#gcs_m_opr").val(),
						gcs_v_opr	: $("#gcs_v_opr").val(),
						pupil_1_opr	: $("#pupil_1_opr").val(),
						pupil_2_opr	: $("#pupil_2_opr").val(),
						pernafasan_opr	: $("#pernafasan_opr").val(),
						pernafasan_lain_opr	: $("#pernafasan_lain_opr").val(),
						perawat_ruangan_id	: $("#perawat_ruangan_id").val(),
						perawat_ruangan_ttd	: $("#perawat_ruangan_ttd").val(),
						perawat_ruangan_bedah_id	: $("#perawat_ruangan_bedah_id").val(),
						perawat_ruangan_bedah_ttd	: $("#perawat_ruangan_bedah_ttd").val(),
						catatan_diagnosa	: $("#catatan_diagnosa").val(),
						respon_mata	: $("#respon_mata").val(),
						respon_verbal	: $("#respon_verbal").val(),
						respon_motorik	: $("#respon_motorik").val(),
						tipe_opr	: $("#tipe_opr").val(),
						jenis_bius	: $("#jenis_bius").val(),
						kesadaran_opr	: $("#kesadaran_opr").val(),
						st_emosi_opr	: $("#st_emosi_opr").val(),
						canul	: $("#canul").val(),
						jenis_operasi	: $("#jenis_operasi").val(),
						posisi_operasi	: $("#posisi_operasi").val(),
						diawasi	: $("#diawasi").val(),
						posisi_lengan	: $("#posisi_lengan").val(),
						kulit	: $("#kulit").val(),
						urine	: $("#urine").val(),
						dipasang_oleh	: $("#dipasang_oleh").val(),
						disinfeksi	: $("#disinfeksi").val(),
						disinfeksi_lain	: $("#disinfeksi_lain").val(),
						posisi_alat	: $("#posisi_alat").val(),
						posisi_alat_lain	: $("#posisi_alat_lain").val(),
						diatermi	: $("#diatermi").val(),
						lokasi_netral	: $("#lokasi_netral").val(),
						dipasang_netral	: $("#dipasang_netral").val(),
						pemeriksaan_kulit	: $("#pemeriksaan_kulit").val(),
						pemasangan_warm	: $("#pemasangan_warm").val(),
						jenis_warm	: $("#jenis_warm").val(),
						jam_mulai	: $("#jam_mulai").val(),
						jam_selesai	: $("#jam_selesai").val(),
						pemakaian_implan	: $("#pemakaian_implan").val(),
						jenis_implan	: $("#jenis_implan").val(),
						lokasi_implan	: $("#lokasi_implan").val(),
						drain	: $("#drain").val(),
						jenis_drain	: $("#jenis_drain").val(),
						lokasi_drain	: $("#lokasi_drain").val(),
						irigasi_luka	: $("#irigasi_luka").val(),
						irigasi_luka_dengan	: $("#irigasi_luka_dengan").val(),
						irigasi_luka_lain	: $("#irigasi_luka_lain").val(),
						tampon	: $("#tampon").val(),
						tampon_lokasi	: $("#tampon_lokasi").val(),
						catatan_all	: $("#catatan_all").val(),
						perawat_assisten_id	: $("#perawat_assisten_id").val(),
						perawat_assisten_ttd	: $("#perawat_assisten_ttd").val(),
						perawat_sirkuler_id	: $("#perawat_sirkuler_id").val(),
						perawat_sirkuler_ttd	: $("#perawat_sirkuler_ttd").val(),
						perawat_anestesi_id	: $("#perawat_anestesi_id").val(),
						perawat_anestesi_ttd	: $("#perawat_anestesi_ttd").val(),
						// tab_wizard	: $("#tab_wizard").val(),
						jam_tv_opr	: $("#jam_tv_opr").val(),
						tingkat_kesadaran_opr	: $("#tingkat_kesadaran_opr").val(),
						nadi_opr	: $("#nadi_opr").val(),
						nafas_opr	: $("#nafas_opr").val(),
						spo2_opr	: $("#spo2_opr").val(),
						td_sistole_opr	: $("#td_sistole_opr").val(),
						td_diastole_opr	: $("#td_diastole_opr").val(),
						suhu_opr	: $("#suhu_opr").val(),
						tinggi_badan_opr	: $("#tinggi_badan_opr").val(),
						berat_badan_opr	: $("#berat_badan_opr").val(),
						st_tornique	: $("#st_tornique").val(),
						keseimbangan_cairan	: $("#keseimbangan_cairan").val(),
						ruang_pemulihan : $("#ruang_pemulihan").val(),
						tgl_masuk_jam : $("#tgl_masuk_jam").val(),
						waktu_masuk_jam : $("#waktu_masuk_jam").val(),
						tgl_keluar_jam : $("#tgl_keluar_jam").val(),
						waktu_keluar_jam : $("#waktu_keluar_jam").val(),
						kembali_ke : $("#kembali_ke").val(),
						ruangan_lainnya : $("#ruangan_lainnya").val(),
						keadaan_paska : $("#keadaan_paska").val(),
						tingkat_kesadaran_paska : $("#tingkat_kesadaran_paska").val(),
						gcs_e_paska : $("#gcs_e_paska").val(),
						gcs_m_paska : $("#gcs_m_paska").val(),
						gcs_v_paska : $("#gcs_v_paska").val(),
						pupil_1_paska : $("#pupil_1_paska").val(),
						pupil_2_paska : $("#pupil_2_paska").val(),
						kulit_datang : $("#kulit_datang").val(),
						kulit_keluar : $("#kulit_keluar").val(),
						sirkulasi_paska : $("#sirkulasi_paska").val(),
						posisi_paska : $("#posisi_paska").val(),
						pendarahan_paska : $("#pendarahan_paska").val(),
						lokasi_pendarahan_paska : $("#lokasi_pendarahan_paska").val(),
						muntah_paska : $("#muntah_paska").val(),
						mukosa_mulut : $("#mukosa_mulut").val(),
						jaringan_pa : $("#jaringan_pa").val(),
						jaringan_pa_dari : $("#jaringan_pa_dari").val(),
						skrining_nyeri : $("#skrining_nyeri").val(),
						risko_jatuh_paska : $("#risko_jatuh_paska").val(),
						
						tgl_memanggil_perawat : $("#tgl_memanggil_perawat").val(),
						waktu_memanggil_perawat : $("#waktu_memanggil_perawat").val(),
						perawat : $("#perawat").val(),
						tgl_perawat_datang : $("#tgl_perawat_datang").val(),
						waktu_perawat_datang : $("#waktu_perawat_datang").val(),
						tgl_pemberitahuan : $("#tgl_pemberitahuan").val(),
						waktu_pemberitahuan : $("#waktu_pemberitahuan").val(),
						keterangan_pemberitahuan : $("#keterangan_pemberitahuan").val(),
						keadaan_ruangan : $("#keadaan_ruangan").val(),
						gcs_e_ruangan : $("#gcs_e_ruangan").val(),
						gcs_m_ruangan : $("#gcs_m_ruangan").val(),
						gcs_v_ruangan : $("#gcs_v_ruangan").val(),
						pupil_1_ruangan : $("#pupil_1_ruangan").val(),
						pupil_2_ruangan : $("#pupil_2_ruangan").val(),
						jam_tv_ruangan : $("#jam_tv_ruangan").val(),
						tingkat_kesadaran_ruangan : $("#tingkat_kesadaran_ruangan").val(),
						nadi_ruangan : $("#nadi_ruangan").val(),
						nafas_ruangan : $("#nafas_ruangan").val(),
						spo2_ruangan : $("#spo2_ruangan").val(),
						td_sistole_ruangan : $("#td_sistole_ruangan").val(),
						td_diastole_ruangan : $("#td_diastole_ruangan").val(),
						suhu_ruangan : $("#suhu_ruangan").val(),
						catatan_ruangan : $("#catatan_ruangan").val(),
						pemeriksaan_kulit_setelah : $("#pemeriksaan_kulit_setelah").val(),


					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
			}
		 }
		}
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_askep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
function create_assesmen_askep(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
$("#trx_ic").on("change", function(){
	get_dokter();
});
$("#trx_iddokter").on("change", function(){
	get_jam_dokter();
});
function get_dokter(){
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:$("#trx_ic").val(),
				
			},
		success: function(data) {
			$("#trx_iddokter").empty().append(data)
		}
	});
}
function get_jam_dokter(){
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_jam_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				idpoli:$("#trx_ic").val(),
				iddokter:$("#trx_iddokter").val(),
				
			},
		success: function(data) {
			$("#trx_jam").empty().append(data)
		}
	});
}
function simpan_ic(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tanggal=$("#trx_jam_ic").val();
	let line_id=$("#trx_ic").val();
	let lokasi=$("#trx_lokasi").val();
	let kondisi=$("#trx_kondisi").val();
	
	let idpasien=$("#idpasien").val();
	if (line_id=='0'){
		sweetAlert("Maaf...", "Isi Line!", "error");
		return false;

	}
	if (lokasi==''){
		sweetAlert("Maaf...", "Isi Lokasi!", "error");
		return false;

	}
	if (kondisi==''){
		sweetAlert("Maaf...", "Isi Kondisi!", "error");
		return false;

	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_ic_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					line_id:line_id,
					lokasi:lokasi,
					kondisi:kondisi,
				},
			success: function(data) {
				$("#kondisi").val('');
				$("#lokasi").val('');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat Line.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_ic_assesmen_askep();
					// $("#trx_").val('')
				}
			}
		});
	}
	
}
function load_ic_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_ic_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_ic tbody").empty();
				$("#tabel_ic tbody").append(data.tabel);
			}
		});
}
function hapus_ic_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Line?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_ic_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_ic_assesmen_askep();		
			}
		});
	});			
}
function simpan_tornique(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let lokasi=$("#lokasi_tornique").val();
	let jam_mulai=$("#mulai_tornique").val();
	let jam_selesai=$("#selesai_tornique").val();
	let dipasang=$("#dipasang_tornique").val();
	
	let idpasien=$("#idpasien").val();
	if (lokasi=='0'){
		sweetAlert("Maaf...", "Isi Lokasi Tornique!", "error");
		return false;
	}
	if (jam_mulai==''){
		sweetAlert("Maaf...", "Isi Jam Mulai!", "error");
		return false;
	}
	if (jam_selesai==''){
		sweetAlert("Maaf...", "Isi Jam Selesai!", "error");
		return false;
	}
	if (dipasang=='0'){
		sweetAlert("Maaf...", "Isi Petugas!", "error");
		return false;
	}
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_tornique_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					lokasi : lokasi,
					jam_mulai : jam_mulai,
					jam_selesai : jam_selesai,
					dipasang : dipasang,

				},
			success: function(data) {
				// $("#kondisi").val('');
				$("#lokasi").val('0').trigger('change');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_tornique_assesmen_askep();
					// $("#trx_").val('')
				}
			}
		});
	}
	
}
function load_tornique_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_tornique_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_tornique tbody").empty();
				$("#tabel_tornique tbody").append(data.tabel);
			}
		});
}
function hapus_tornique_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Tornique?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_tornique_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_tornique_assesmen_askep();		
			}
		});
	});			
}
function simpan_cairan(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let input=$("#input_cairan").val();
	let jml_input=$("#input_jml_cairan").val();
	let output=$("#output_cairan").val();
	let jml_output=$("#output_jml_cairan").val();
	
	
	let idpasien=$("#idpasien").val();
	if (input==''){
		sweetAlert("Maaf...", "Isi Input!", "error");
		return false;
	}
	if (jml_input==''){
		sweetAlert("Maaf...", "Isi Jumlah Input!", "error");
		return false;
	}
	if (output==''){
		sweetAlert("Maaf...", "Isi output!", "error");
		return false;
	}
	if (jml_output==''){
		sweetAlert("Maaf...", "Isi Jumlah Output!", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_cairan_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					input : input,
					jml_input : jml_input,
					output : output,
					jml_output : jml_output,
				},
			success: function(data) {
				// $("#kondisi").val('');
				$("#lokasi").val('0').trigger('change');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_cairan_assesmen_askep();
					$("#input_cairan").val('');
					$("#input_jml_cairan").val('');
					$("#output_cairan").val('');
					$("#output_jml_cairan").val('');
					// $("#trx_").val('')
				}
			}
		});
	}
	
}
function load_cairan_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_cairan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_cairan tbody").empty();
				$("#tabel_cairan tbody").append(data.tabel);
			}
		});
}
function hapus_cairan_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Tornique?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_cairan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_cairan_assesmen_askep();		
			}
		});
	});			
}
function simpan_spesimen(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let spesimen_id=$("#spesimen_id").val();
	let spesimen_jenis=$("#spesimen_jenis").val();
	
	
	let idpasien=$("#idpasien").val();
	if (spesimen_id=='0'){
		sweetAlert("Maaf...", "Isi SPESIMEN!", "error");
		return false;
	}
	if (spesimen_jenis==''){
		sweetAlert("Maaf...", "Isi Jenis!", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_spesimen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					spesimen_id : spesimen_id,
					jenis	 : spesimen_jenis,
				},
			success: function(data) {
				// $("#kondisi").val('');
				$("#spesimen_id").val('0').trigger('change');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_spesimen_assesmen_askep();
					$("#spesimen_jenis").val('');
				}
			}
		});
	}
	
}
function load_spesimen_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_spesimen_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_spesimen tbody").empty();
				$("#tabel_spesimen tbody").append(data.tabel);
			}
		});
}
function hapus_spesimen_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Tornique?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_spesimen_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_spesimen_assesmen_askep();		
			}
		});
	});			
}
function simpan_post(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let dokumen_id=$("#dokumen_id").val();
	let keterangan_pos=$("#keterangan_pos").val();
	
	
	let idpasien=$("#idpasien").val();
	if (dokumen_id=='0'){
		sweetAlert("Maaf...", "Isi Dokumen!", "error");
		return false;
	}
	if (keterangan_pos==''){
		sweetAlert("Maaf...", "Isi Keterangan!", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_post_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					dokumen_id : dokumen_id,
					keterangan	 : keterangan_pos,
				},
			success: function(data) {
				// $("#kondisi").val('');
				$("#post_id").val('0').trigger('change');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_post_assesmen_askep();
					$("#keterangan_pos").val('');
					$("#dokumen_id").val('0').trigger('change');
				}
			}
		});
	}
	
}
function load_post_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_post_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_post tbody").empty();
				$("#tabel_post tbody").append(data.tabel);
			}
		});
}
function hapus_post_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Dokumen?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_post_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_post_assesmen_askep();		
			}
		});
	});			
}
function simpan_barang(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let dokumen_id=$("#dokumen_barang_id").val();
	let keterangan_pos=$("#keterangan_barang").val();
	
	
	let idpasien=$("#idpasien").val();
	if (dokumen_id=='0'){
		sweetAlert("Maaf...", "Isi Dokumen!", "error");
		return false;
	}
	if (keterangan_pos==''){
		sweetAlert("Maaf...", "Isi Keterangan!", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_barang_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					dokumen_id : dokumen_id,
					keterangan	 : keterangan_pos,
				},
			success: function(data) {
				// $("#kondisi").val('');
				$("#barang_id").val('0').trigger('change');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_barang_assesmen_askep();
					$("#keterangan_barang").val('');
					$("#dokumen_barang_id").val('0').trigger('change');
				}
			}
		});
	}
	
}
function load_barang_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_barang_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_barang tbody").empty();
				$("#tabel_barang tbody").append(data.tabel);
			}
		});
}
function hapus_barang_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Barang / Dokumen?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_barang_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_barang_assesmen_askep();		
			}
		});
	});			
}
function simpan_anestesi(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tgl=$("#tgl_jam_anestesi").val();
	let jam=$("#waktu_jam_anestesi").val();
	
	let idpasien=$("#idpasien").val();
	if (tgl==''){
		sweetAlert("Maaf...", "Isi Tanggal!", "error");
		return false;

	}
	if (jam==''){
		sweetAlert("Maaf...", "Isi Jam!", "error");
		return false;

	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_anestesi_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tgl:tgl,
					jam:jam,
				},
			success: function(data) {
				$("#kondisi").val('');
				$("#lokasi").val('');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_anestesi_assesmen_askep();
					// $("#trx_").val('')
				}
			}
		});
	}
	
}
function load_anestesi_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_anestesi_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_anestesi tbody").empty();
				$("#tabel_anestesi tbody").append(data.tabel);
			}
		});
}
function hapus_anestesi_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Anestesi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_anestesi_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_anestesi_assesmen_askep();		
			}
		});
	});			
}
function simpan_pembedahan(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tgl=$("#tgl_jam_pembedahan").val();
	let jam=$("#waktu_jam_pembedahan").val();
	
	let idpasien=$("#idpasien").val();
	if (tgl==''){
		sweetAlert("Maaf...", "Isi Tanggal!", "error");
		return false;

	}
	if (jam==''){
		sweetAlert("Maaf...", "Isi Jam!", "error");
		return false;

	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_pembedahan_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tgl:tgl,
					jam:jam,
				},
			success: function(data) {
				$("#kondisi").val('');
				$("#lokasi").val('');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_pembedahan_assesmen_askep();
					// $("#trx_").val('')
				}
			}
		});
	}
	
}
function load_pembedahan_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_pembedahan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_pembedahan tbody").empty();
				$("#tabel_pembedahan tbody").append(data.tabel);
			}
		});
}
function hapus_pembedahan_assesmen_askep(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Anestesi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_pembedahan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_pembedahan_assesmen_askep();		
			}
		});
	});			
}
function simpan_pengobatan(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let nama=$("#trx_nama").val();
		let dosis=$("#trx_dosis").val();
		let jam=$("#trx_jam").val();
		
		let idpasien=$("#idpasien").val();
		if (nama==''){
			sweetAlert("Maaf...", "Isi Pengobatan!", "error");
			return false;

		}
		if (nama==''){
			sweetAlert("Maaf...", "Isi Pengobatan!", "error");
			return false;

		}
		if (dosis==''){
			sweetAlert("Maaf...", "Isi Dosis!", "error");
			return false;

		}
		
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_pengobatan_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						idpasien:idpasien,
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						jam:jam,
						dosis:dosis,
						nama:nama,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Tindkan.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_pengobatan_assesmen_askep();
						$("#trx_nama").val('')
					}
				}
			});
		}
		
	}
	function load_pengobatan_assesmen_askep(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/load_pengobatan_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#tabel_tindakan tbody").empty();
					$("#tabel_tindakan tbody").append(data.tabel);
				}
			});
	}
	function hapus_pengobatan_assesmen_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Tindakan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_pengobatan_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
					load_pengobatan_assesmen_askep();		
				}
			});
		});			
	}
	function simpan_hasil(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let jenis_pemeriksaan=$("#trx_jenis_pemeriksaan").val();
		let tanggal=$("#trx_jam_hasil").val();
		let jumlah_lembar=$("#trx_jumlah_lembar").val();
		let hasil=$("#trx_hasil").val();
		
		let idpasien=$("#idpasien").val();
		if (jenis_pemeriksaan=='0'){
			sweetAlert("Maaf...", "Isi Nama Jenis!", "error");
			return false;

		}
		if (hasil==''){
			sweetAlert("Maaf...", "Isi Nama Jenis!", "error");
			return false;

		}
		if (jumlah_lembar==''){
			sweetAlert("Maaf...", "Isi Nama Jumlah Lembar!", "error");
			return false;

		}
		
		if (tanggal==''){
			sweetAlert("Maaf...", "Isi Tanggal!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_hasil_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						jenis_pemeriksaan:jenis_pemeriksaan,
						hasil:hasil,
						jumlah_lembar:jumlah_lembar,
						tanggal:tanggal,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Tindkan.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_hasil_assesmen_askep();
						$("#trx_jenis_pemeriksaan").val('0').trigger('change');
						$("#trx_hasil").val('');
						$("#trx_jumlah_lembar").val('');
					}
				}
			});
		}
		
	}
	function load_hasil_assesmen_askep(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/load_hasil_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#tabel_hasil tbody").empty();
					$("#tabel_hasil tbody").append(data.tabel);
				}
			});
	}
	function hapus_hasil_assesmen_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Hasil Pemeriksaan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_hasil_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_hasil_assesmen_askep();		
				}
			});
		});			
	}
	function simpan_diagnosa_askep(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let mdiagnosa_id=$("#mdiagnosa_id").val();
		let diagnosa_id=$("#diagnosa_id").val();
		let prioritas=$("#prioritas").val();
		
		let idpasien=$("#idpasien").val();
		if (mdiagnosa_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mdiagnosa_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_diagnosa_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						diagnosa_id:diagnosa_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						mdiagnosa_id:mdiagnosa_id,
						prioritas:prioritas,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Diagnosa.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_diagnosa_askep();
					}
				}
			});
		}
		
	}
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function edit_diagnosa(id){
		$("#diagnosa_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_diagnosa_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
				$("#mdiagnosa_id").val(data.mdiagnosa_id).trigger('change');
				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function set_rencana_asuhan(id){
		 $("#diagnosa_id_list").val(id).trigger('change');
	}
	function load_diagnosa_askep(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_askep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa_askep();
	}
	
	function refresh_diagnosa_askep(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				load_data_rencana_asuhan_askep();
			}
		});
	}
	function hapus_diagnosa_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_diagnosa_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_diagnosa_askep();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	$("#diagnosa_id_list").on("change", function(){
		load_data_rencana_asuhan_askep($(this).val());
	});
	function load_data_rencana_asuhan_askep(diagnosa_id){
		if (diagnosa_id){
			
		}else{
			diagnosa_id=$("#diagnosa_id_list").val()
		}
		let assesmen_id=$("#assesmen_id").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_data_rencana_asuhan_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_id,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan tbody").append(data);
			}
		});
	}
	
	//DURANTE
	function simpan_diagnosa_durante_askep(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let mdiagnosa_durante_id=$("#mdiagnosa_durante_id").val();
		let diagnosa_durante_id=$("#diagnosa_durante_id").val();
		let prioritas=$("#prioritas_durante").val();
		
		let idpasien=$("#idpasien").val();
		if (mdiagnosa_durante_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mdiagnosa_durante_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_diagnosa_durante_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						diagnosa_id:diagnosa_durante_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						mdiagnosa_id:mdiagnosa_durante_id,
						prioritas:prioritas,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa_durante();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Diagnosa.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_diagnosa_durante_askep();
					}
				}
			});
		}
		
	}
	function clear_input_diagnosa_durante(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_durante_id").val('');
		$("#mdiagnosa_durante_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa_durante").html('<i class="fa fa-plus"></i> Add');
	}
	function edit_diagnosa_durante(id){
		$("#diagnosa_durante_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_diagnosa_durante_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_diagnosa_durante").html('<i class="fa fa-save"></i> Simpan');
				$("#mdiagnosa_durante_id").val(data.mdiagnosa_durante_id).trigger('change');
				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function set_rencana_asuhan_durante(id){
		 $("#diagnosa_durante_id_list").val(id).trigger('change');
	}
	function load_diagnosa_durante_askep(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa_durante').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa_durante').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_durante_askep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa_durante_askep();
	}
	
	function refresh_diagnosa_durante_askep(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_durante_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_durante_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_durante_id_list").append(data);
				load_data_rencana_asuhan_durante_askep();
			}
		});
	}
	function hapus_diagnosa_durante_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_diagnosa_durante_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_diagnosa_durante_askep();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	$("#diagnosa_durante_id_list").on("change", function(){
		load_data_rencana_asuhan_durante_askep($(this).val());
	});
	function load_data_rencana_asuhan_durante_askep(diagnosa_durante_id){
		if (diagnosa_durante_id){
			
		}else{
			diagnosa_durante_id=$("#diagnosa_durante_id_list").val()
		}
		let assesmen_id=$("#assesmen_id").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_data_rencana_asuhan_durante_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_durante_id,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan_durante tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan_durante tbody").append(data);
			}
		});
	}
	//DURANTE
	function simpan_diagnosa_paska_askep(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let mdiagnosa_paska_id=$("#mdiagnosa_paska_id").val();
		let diagnosa_paska_id=$("#diagnosa_paska_id").val();
		let prioritas=$("#prioritas_paska").val();
		
		let idpasien=$("#idpasien").val();
		if (mdiagnosa_paska_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mdiagnosa_paska_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_diagnosa_paska_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						diagnosa_id:diagnosa_paska_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						mdiagnosa_id:mdiagnosa_paska_id,
						prioritas:prioritas,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa_paska();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Diagnosa.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
						load_diagnosa_paska_askep();
					}
				}
			});
		}
		
	}
	function clear_input_diagnosa_paska(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_paska_id").val('');
		$("#mdiagnosa_paska_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa_paska").html('<i class="fa fa-plus"></i> Add');
	}
	function edit_diagnosa_paska(id){
		$("#diagnosa_paska_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_diagnosa_paska_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_diagnosa_paska").html('<i class="fa fa-save"></i> Simpan');
				$("#mdiagnosa_paska_id").val(data.mdiagnosa_paska_id).trigger('change');
				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function set_rencana_asuhan_paska(id){
		 $("#diagnosa_paska_id_list").val(id).trigger('change');
	}
	function load_diagnosa_paska_askep(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa_paska').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa_paska').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_paska_askep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa_paska_askep();
	}
	
	function refresh_diagnosa_paska_askep(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_paska_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_paska_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_paska_id_list").append(data);
				load_data_rencana_asuhan_paska_askep();
			}
		});
	}
	function hapus_diagnosa_paska_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_diagnosa_paska_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_diagnosa_paska_askep();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	$("#diagnosa_paska_id_list").on("change", function(){
		load_data_rencana_asuhan_paska_askep($(this).val());
	});
	function load_data_rencana_asuhan_paska_askep(diagnosa_paska_id){
		if (diagnosa_paska_id){
			
		}else{
			diagnosa_paska_id=$("#diagnosa_paska_id_list").val()
		}
		let assesmen_id=$("#assesmen_id").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_data_rencana_asuhan_paska_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_paska_id,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan_paska tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan_paska tbody").append(data);
			}
		});
	}
$("#template_id").on("change", function(){
	let template_id=$(this).val();
	if (template_id!=template_id_last){
			swal({
			  title: 'Anda Yakain?',
			  text: "Mengganti Mode Penilaian!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Proses!'
			}).then(function(json_data) {
					$("#cover-spin").show();
					let assesmen_id=$("#assesmen_id").val();
					template_id_last=template_id;
					$.ajax({
						url: '{site_url}Tpendaftaran_ranap_erm/simpan_risiko_jatuh_template_askep', 
						dataType: "JSON",
						method: "POST",
						data : {
								assesmen_id:assesmen_id,
								template_id:template_id,
								
							},
						success: function(data) {
							
									console.log(data);
							if (data==null){
								swal({
									title: "Gagal!",
									text: "Simpan TTV.",
									type: "error",
									timer: 1500,
									showConfirmButton: false
								});

							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Ganti Mode Penilaian '});
								location.reload();			
							}
						}
					});
			}, function(dismiss) {
			  if (dismiss === 'cancel' || dismiss === 'close') {
					$("#template_id").val(template_id_last).trigger('change.select2');
			  } 
			})
		}else{
			save_all_detail(kategori_id_last);
		}
		
	
	
});
function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd_askep(){
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}

	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_askep/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function load_ttd_askep(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_ttd_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;
			
			// $("#sig").html('<img src="'+src+'"/>')
			// console.log(data.ttd);
		// set_img_canvas();
			$('#sig').signature('enable'). signature('draw', data.ttd);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd_askep();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_durante_id_list").removeAttr('disabled');
		 $("#diagnosa_paska_id_list").removeAttr('disabled');
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $(".data_asuhan_durante").removeAttr('disabled');
		 $(".data_asuhan_paska").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
		 $(".wizard-prev").removeAttr('disabled');
		 $(".wizard-next").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_askep();
}
function create_with_template_askep(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_askep(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_template_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_askep(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_template_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$("#medukasi_id").change(function(){
	if ($("#st_edited").val()=='0'){
	simpan_edukasi_askep();
	}
});



// $(".auto_blur").blur(function(){
	// if ($("#st_edited").val()=='0'){
		// $(this).removeClass('input_edited');
		// simpan_assesmen_ttv();
	// }else{
		// if ($("#st_edited").val()=='1'){
			// if ($(this).val()!=before_edit){
				// console.log('Ada Peruabahan');
				// // $(this).attr('input_edited');
				 // $(this).addClass('input_edited');
			// }
		// }
	// }
	
// });
$("#tingkat_kesadaran").change(function(){
		// if ($("#st_edited").val()=='0'){
			simpan_assesmen();
			// simpan_assesmen_ttv();
		// }
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			// simpan_edukasi_askep();
			// simpan_riwayat_penyakit_askep();
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}

	
	function simpan_alergi_askep(){// alert($("#keluhan_utama").val());
		let alergi_id=$("#alergi_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		let input_jenis_alergi=$("#input_jenis_alergi").val();
		let input_detail_alergi=$("#input_detail_alergi").val();
		let input_reaksi_alergi=$("#input_reaksi_alergi").val();
		if (input_detail_alergi==''){
			sweetAlert("Maaf...", "Isi Detail Alergi!", "error");
			return false;

		}
		if (input_reaksi_alergi==''){
			sweetAlert("Maaf...", "Isi Reaksi Alergi!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_alergi_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						alergi_id:alergi_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						assesmen_id:assesmen_id,
						input_jenis_alergi:input_jenis_alergi,
						input_detail_alergi:input_detail_alergi,
						input_reaksi_alergi:input_reaksi_alergi,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_alergi();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_alergi_askep();
					}
				}
			});
		}
		
	}
	function simpan_obat_askep(){// alert($("#keluhan_utama").val());
		let obat_id=$("#obat_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		let nama_obat=$("#nama_obat").val();
		let dosis=$("#dosis").val();
		let waktu=$("#waktu").val();
		if (nama_obat==''){
			sweetAlert("Maaf...", "Isi Nama Obat!", "error");
			return false;

		}
		if (dosis==''){
			sweetAlert("Maaf...", "Isi Dosis Obat!", "error");
			return false;

		}
		if (waktu==''){
			sweetAlert("Maaf...", "Isi Waktu Obat!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_obat_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						obat_id:obat_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						assesmen_id:assesmen_id,
						nama_obat:nama_obat,
						dosis:dosis,
						waktu:waktu,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_obat();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_obat_askep();
					}
				}
			});
		}
		
	}
	function simpan_obat_infus_askep(){
		let obat_infus_id=$("#obat_infus_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		
		let obat_id_select2=$("#obat_id_select2").val();
		let tanggalinfus=$("#tanggalinfus").val();
		let waktuinfus=$("#waktuinfus").val();
		let kuantitas_infus=$("#kuantitas_infus").val();
		let dosis_infus=$("#dosis_infus").val();
		let ppa_pemberi=$("#ppa_pemberi").val();
		let ppa_periksa=$("#ppa_periksa").val();
		let rute=$("#rute").val();
		let idtipe=$("#idtipe").val();
		
		if (obat_id_select2==''){
			sweetAlert("Maaf...", "Isi Nama Obat!", "error");
			return false;
		}
		if (tanggalinfus==''){
			sweetAlert("Maaf...", "Isi Tanggal!", "error");
			return false;
		}
		if (waktuinfus==''){
			sweetAlert("Maaf...", "Isi Waktu Obat!", "error");
			return false;
		}
		if (kuantitas_infus==''){
			sweetAlert("Maaf...", "Isi Kuantitas!", "error");
			return false;
		}
		if (dosis_infus==''){
			sweetAlert("Maaf...", "Isi Dosis!", "error");
			return false;
		}
		if (ppa_pemberi==''){
			sweetAlert("Maaf...", "Isi Pemberi!", "error");
			return false;
		}
		if (ppa_periksa==''){
			sweetAlert("Maaf...", "Isi Pemeriksa!", "error");
			return false;
		}
		if (rute==''){
			sweetAlert("Maaf...", "Isi Rute!", "error");
			return false;
		}
		
		
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_obat_infus_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						obat_infus_id:obat_infus_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						assesmen_id:assesmen_id,
						
						obat_id_select2:obat_id_select2,
						tanggalinfus:tanggalinfus,
						waktuinfus:waktuinfus,
						kuantitas_infus:kuantitas_infus,
						dosis_infus:dosis_infus,
						ppa_pemberi:ppa_pemberi,
						ppa_periksa:ppa_periksa,
						rute:rute,
						idtipe:idtipe,
					},
				success: function(data) {
					clear_input_obat_infus();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Pemberiann Obat.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_obat_infus_askep();
					}
				}
			});
		}
		
	}
	function clear_input_obat_infus(){
		$("#obat_infus_id").val('');
		$("#btn_add_obat_infus").html('<i class="fa fa-plus"></i> Add');
		$("#obat_id_select2").val('').trigger('change');
		$("#ppa_pemberi").val('').trigger('change');
		$("#ppa_periksa").val('').trigger('change');
		$("#kuantitas_infus").val('');
		$("#dosis_infus").val('');
	}
	
	
	
	function clear_input_tindakan(){
		$("#tindakan_id").val('');
		$("#btn_add_tindakan").html('<i class="fa fa-plus"></i> Add');
		$("#mtindakan_id").val('').trigger('change');
		$("#ppa_pelaksana").val('').trigger('change');
		// $("#waktu_mulai").val('').trigger('change');
		$("#waktu_mulai").val('');
		$("#waktu_selesai").val('');
	}
	
	
	function clear_input_alergi(){
		$("#alergi_id").val('');
		$("#btn_add_alergi").html('<i class="fa fa-plus"></i> Add');
		$("#input_jenis_alergi").val('');
		$("#input_detail_alergi").val('');
		$("#input_reaksi_alergi").val('');
	}
	function clear_input_obat(){
		$("#obat_id").val('');
		$("#btn_add_obat").html('<i class="fa fa-plus"></i> Add');
		$("#nama_obat").val('');
		$("#dosis").val('');
		$("#waktu").val('');
	}
	function load_alergi_askep(){
		$('#alergi_2 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_askep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	
	// function load_obat_infus_askep(){
		
		// let assesmen_id=$("#assesmen_id").val();
		// $('#index_obat_infus').DataTable().destroy();	
		// // $("#cover-spin").show();
		// // alert(ruangan_id);
		// table = $('#index_obat_infus').DataTable({
				// autoWidth: false,
				// searching: true,
				// serverSide: true,
				// "processing": false,
				// "order": [],
				// "pageLength": 10,
				// "ordering": false,
				// ajax: { 
					// url: '{site_url}Tpendaftaran_ranap_erm/load_obat_infus_askep', 
					// type: "POST" ,
					// dataType: 'json',
					// data : {
							// assesmen_id:assesmen_id,
						   // }
				// },
				// columnDefs: [
					// {  className: "text-right", targets:[3] },
					// {  className: "text-center", targets:[8] },
					 // { "width": "5%", "targets": [1,3] },
					 // { "width": "10%", "targets": [0,4] },
					 // { "width": "20%", "targets": [2] },
					 // { "width": "10%", "targets": [5,6,7,8] }
				// ],
				// "drawCallback": function( settings ) {
					 // $("#cover-spin").hide();
					 // disabel_edit();
				 // }  
			// });
		
	// }
	
	function load_obat_infus_askep(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/load_obat_infus_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#index_obat_infus tbody").empty();
					$("#index_obat_infus tbody").append(data.tabel);
				}
			});
	}
	function load_alergi_askep_his(){
		$('#alergi_1 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_alergi_his').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi_his').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_his', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_alergi_askep_pasien(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_askep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function hapus_alergi_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Alergi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_alergi_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_alergi_askep();		
				}
			});
		});			
	}
	function hapus_obat_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Penggunaan Obat ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_obat_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_obat_askep();		
				}
			});
		});			
	}
	function hapus_obat_infus_askep(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Penggunaan Obat Infus ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_obat_infus_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_obat_infus_askep();		
				}
			});
		});			
	}
	
	
	function simpan_edukasi_askep(){// alert($("#keluhan_utama").val());
		let assesmen_id=$("#assesmen_id").val();
		let medukasi_id=$("#medukasi_id").val();
		console.log('SIMPAN EDUKASI');
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_edukasi_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						medukasi_id:medukasi_id,
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}
				}
			});
		}
		
	}
	 
	$(document).on('click','.data_asuhan',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/update_data_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa_askep();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
	$(document).on('click','.data_asuhan_durante',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/update_data_durante_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa_askep();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
	$(document).on('click','.data_asuhan_paska',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/update_data_paska_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa_askep();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template_askep").removeAttr('disabled');
		}else{
			$("#btn_create_with_template_askep").attr('disabled','disabled');
		}
	});
	$(document).on('change','#riwayat_penyakit_id',function(){
		var select_button_text
			select_button_text = $('#riwayat_penyakit_id option:selected')
					.toArray().map(item => item.text).join();
		$("#riwayat_penyakit_nama").val(select_button_text);
	});
	function list_index_template_askep(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_index_template_askep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id_ranap);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_template_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen_askep(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		// alert(assesmen_detail_id);return false;
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_edit_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id=data.pendaftaran_id;
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_ri/input_assesmen_askep'); ?>";
				}
			}
		});
	}
	function hapus_record_assesmen_askep(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_askep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template_askep();
				}
			});
		});

	}
	
	
</script>