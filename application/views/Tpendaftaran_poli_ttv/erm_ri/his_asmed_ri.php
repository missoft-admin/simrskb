<style>
	.edited_final{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited_final {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='his_asmed_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_asmed_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_asmed_ri=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_asmed_ri=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >				
						<div class="col-md-12">
							<?if($st_lihat_asmed_ri=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/his_asmed_ri" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) <?=$st_input_assesmen?></label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >ANAMNESIS </h5>
							</div>
						</div>
						<input class="form-control"  type="hidden" id="st_anamnesa" name="st_anamnesa" value="<?=$st_anamnesa?>" >				
								
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Keluhan Utama </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" value="1" id="auto_anamnesa" name="anamnesa" onclick="set_auto()" <?=($st_anamnesa=='1'?'checked':'')?>><span></span> Auto Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  value="1" id="allo_anamnesa" name="anamnesa" onclick="set_allo()" <?=($st_anamnesa=='2'?'checked':'')?>><span></span> Allo Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control  <?=($nama_anamnesa!=$nama_anamnesa_asal?'edited':'')?> push-5-r"  type="text"  id="nama_anamnesa" name="nama_anamnesa" value="{nama_anamnesa}" placeholder="Tuliskan Nama" >
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control <?=($hubungan_anamnesa!=$hubungan_anamnesa_asal?'edited':'')?>"  type="text" id="hubungan_anamnesa" name="hubungan_anamnesa" value="{hubungan_anamnesa}" placeholder="Hubungan" >
								</div>
								<div class="col-md-12 push-10-t">
									<textarea id="keluhan_utama" class="form-control <?=($keluhan_utama!=$keluhan_utama_asal?'edited':'')?>" name="story" rows="2" style="width:100%"><?=$keluhan_utama?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit" name="st_riwayat_penyakit" value="<?=$st_riwayat_penyakit?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(0)" <?=($st_riwayat_penyakit=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(1)" <?=($st_riwayat_penyakit=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<?
								$riwayat_penyakit=($riwayat_penyakit?explode(',',$riwayat_penyakit):array());
								$riwayat_penyakit_dahulu_list=($riwayat_penyakit_dahulu_list?explode(',',$riwayat_penyakit_dahulu_list):array());
								$riwayat_penyakit_keluarga_list=($riwayat_penyakit_keluarga_list?explode(',',$riwayat_penyakit_keluarga_list):array());
								?>
								<div class="col-md-7">
									
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_lainnya" class="form-control <?=($riwayat_penyakit_lainnya!=$riwayat_penyakit_lainnya_asal?'edited':'')?>" name="riwayat_penyakit_lainnya" rows="2" style="width:100%"><?=$riwayat_penyakit_lainnya?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit_dahulu" name="st_riwayat_penyakit_dahulu" value="<?=$st_riwayat_penyakit_dahulu?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit Dahulu</label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  name="opsi_rp_dahulu" onclick="set_riwayat_1(0)" <?=($st_riwayat_penyakit_dahulu=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_rp_dahulu" onclick="set_riwayat_1(1)" <?=($st_riwayat_penyakit_dahulu=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_dahulu" class="form-control  <?=($riwayat_penyakit_dahulu!=$riwayat_penyakit_dahulu_asal?'edited':'')?>" name="riwayat_penyakit_dahulu" rows="2" style="width:100%"><?=$riwayat_penyakit_dahulu?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit_keluarga" value="<?=$st_riwayat_penyakit_keluarga?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit Keluarga</label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_rp_keluarga" onclick="set_riwayat_2(0)" <?=($st_riwayat_penyakit_keluarga=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_rp_keluarga" onclick="set_riwayat_2(1)" <?=($st_riwayat_penyakit_keluarga=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_keluarga" class="form-control <?=($riwayat_penyakit_keluarga!=$riwayat_penyakit_keluarga_asal?'edited':'')?>" rows="2" style="width:100%"><?=$riwayat_penyakit_keluarga?></textarea>
								</div>
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >PEMERIKSAAN UMUM</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Tingkat Kesadaran</label>
									<select tabindex="8" id="tingkat_kesadaran"  style="background-color:#d0f3df;" name="tingkat_kesadaran" class="<?=($tingkat_kesadaran!=$tingkat_kesadaran_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-8 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal <?=($td_sistole!=$td_sistole_asal?'edited':'')?>" type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal <?=($td_diastole!=$td_diastole_asal?'edited':'')?>" type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal <?=($suhu!=$suhu_asal?'edited':'')?>"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								
								
								
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nadi!=$nadi_asal?'edited':'')?>"  type="text" id="nadi" name="nadi" value="{nadi}"  placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nafas!=$nafas_asal?'edited':'')?>"  type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Nafas" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Keadaan Umum</label>
									<select tabindex="8" id="keadaan_umum" name="keadaan_umum" class="<?=($keadaan_umum!=$keadaan_umum_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(130) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_umum == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_umum == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Keadaan Gizi</label>
									<select tabindex="8" id="keadaan_gizi" name="keadaan_gizi" class="<?=($keadaan_gizi!=$keadaan_gizi_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(131) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_gizi == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_gizi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<label class=" text-primary" >STATUS LOKALIS</label>
							</div>
							<div class="col-md-6 ">
								<label class="text-primary" >KETERANGAN</label>
							</div>
						</div>
						<input class="form-control " readonly type="hidden" id="src_gambar"  value="{upload_path}anatomi/{gambar_tubuh}" placeholder="" >
						<input class="form-control " readonly type="hidden" id="base_url"  value="{upload_path}anatomi/" placeholder="" >
						<div class="form-group">
							<div class="col-md-6 ">
								<canvas id="canvas" width="650" height="500" ></canvas>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="simbol-list">
										<thead>
											<tr>
												<th style="width:5%">No.</th>
												<th style="width:10%">Simbol</th>
												<th style="width:75%">Keterangan</th>
												<th style="width:10%">Aksi</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
								</div>
								<div class="col-md-12">
									<label>Keterangan</label>
									<input class="form-control <?=($txt_keterangan!=$txt_keterangan_asal?'edited':'')?>" type="text" id="txt_keterangan"  value="{txt_keterangan}" placeholder="Keterangan" >
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-sm-4">
									<label>Template</label>
									<select id="gambar_id"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(get_all('tranap_asmed_ri_lokalis_gambar',array('assesmen_id'=>$assesmen_id))as $r){?>
										<option value="<?=$r->id?>" <?=($r->st_default=='1'?'selected':'')?>><?=$r->nama_template_lokasi?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-4">
									<label>Jenis Drawing</label>
									<select id="jenis_drawing"  class="js-select2 form-control input-default" style="width: 100%;" data-placeholder="Choose one..">
										<option value="free_draw" <?=($jenis_drawing=='free_draw'?'selected':'')?>>Free Draw</option>
										<option value="segitiga" <?=($jenis_drawing=='segitiga'?'selected':'')?>>Triangle</option>
										<option value="kotak" <?=($jenis_drawing=='kotak'?'selected':'')?>>Box</option>
										<option value="bulat" <?=($jenis_drawing=='bulat'?'selected':'')?>>Round</option>
										<option value="silang" <?=($jenis_drawing=='silang'?'selected':'')?>>Cross</option>
										<option value="diamond" <?=($jenis_drawing=='diamond'?'selected':'')?>>Diamond</option>
										<option value="bintang" <?=($jenis_drawing=='bintang'?'selected':'')?>>Star</option>
										<option value="target" <?=($jenis_drawing=='target'?'selected':'')?>>Crosshairs</option>
										<option value="hati" <?=($jenis_drawing=='hati'?'selected':'')?>>Heart</option>
										<option value="bendera" <?=($jenis_drawing=='bendera'?'selected':'')?>>Flag</option>
										<option value="tempe" <?=($jenis_drawing=='tempe'?'selected':'')?>>Bookmark</option>
									</select>
								</div>
								<div class="col-md-4">
									<label>Warna</label>
									<div class="js-colorpicker input-group colorpicker-element">
										<input class="form-control input-default" readonly type="text" id="warna" name="warna" value="<?=$warna?>">
										<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12">
									
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary">PEMERIKSAAN FISIK</label>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-striped" id="tabel_anatomi">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="25%">Anatomi Tubuh</th>
													<th width="40%">Deskripsi Tidak Normal</th>
													<th width="15%">User</th>
													<th width="15%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
									<label for="example-input-normal">Pemeriksaan Penunjang Pre Rawat Inap</label>
									<textarea id="pemeriksaan_penunjang" class="form-control <?=($pemeriksaan_penunjang!=$pemeriksaan_penunjang_asal?'edited':'')?>" name="pemeriksaan_penunjang" rows="2" style="width:100%"><?=$pemeriksaan_penunjang?></textarea>
								
							</div>
							<div class="col-md-6 ">
									<label for="example-input-normal">Diagnosa Kerja</label>
									<textarea id="diagnosa_kerja" class="form-control <?=($diagnosa_kerja!=$diagnosa_kerja_asal?'edited':'')?>" name="diagnosa_kerja" rows="2" style="width:100%"><?=$diagnosa_kerja?></textarea>
								
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-6 ">
									<label for="example-input-normal">Diagnosa Banding</label>
									<textarea id="diagnosa_banding" class="form-control <?=($diagnosa_banding!=$diagnosa_banding_asal?'edited':'')?>" rows="2" style="width:100%"><?=$diagnosa_banding?></textarea>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Pengobatan</label>
									<textarea id="pengobatan" class="form-control <?=($pengobatan!=$pengobatan_asal?'edited':'')?>" rows="2" style="width:100%"><?=$pengobatan?></textarea>
								</div>
								
						</div>
						<div class="form-group">
								<div class="col-md-12 ">
									<label>Rencana</label>
									<textarea id="rencana" class="form-control <?=($rencana!=$rencana_asal?'edited':'')?>" rows="2" style="width:100%"><?=$rencana?></textarea>
								</div>
							
						</div>
						
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
const font      = '400 30px "Font Awesome 6 Free"';
const font_2    = '900 40px "Font Awesome 6 Free"';
const font_3    = '900 30px "Font Awesome 6 Free"';
const marker_type = [{key:'bulat',content:'\uf10c',class_name:'fa-circle',font:font},
                     {key:'segitiga',content:'\uf0d8',class_name:'fa-caret-up',font:font_2},
                     {key:'kotak',content:'\uf096',class_name:'fa-square',font:font},
                     {key:'silang',content:'\uf00d',class_name:'fa-times',font:font_3},
                     {key:'diamond',content:'\uf219',class_name:'fa-diamond',font:font_3},
                     {key:'tempe',content:'\uf097',class_name:'fa-bookmark',font:font},
                     {key:'bintang',content:'\uf006',class_name:'fa-star',font:font},
                     {key:'target',content:'\uf05b',class_name:'fa-crosshairs',font:font_3},
                     {key:'hati',content:'\uf08a',class_name:'fa-heart',font:font},
                     {key:'bendera',content:'\uf11d',class_name:'fa-flag',font:font},
                     {key:'free_draw',content:'',class_name:'fa-pencil',font:font_3},
                    ];

var selected_marker = null;
<?if ($assesmen_id){?>
	

var canvas      = document.getElementById('canvas');
var context     = canvas.getContext("2d");

context.lineWidth=2;
var st_drawing  = false;
var path     = [];
// const change_marker = document.querySelectorAll('.change-marker');
// change_marker.forEach(el => el.addEventListener('click', event => {
    // selected_marker = marker_type.find(o => o.key === el.id);
    
// }));
$("#jenis_drawing").on("change", function(){
	set_marker();
}); 


function set_marker(){
	 // $("#jenis_drawing").val();
	 selected_marker = marker_type.find(({ key }) => key === $("#jenis_drawing").val());
	 // console.log(Markers);
	 // selected_marker = marker_type.find(o => o.key === el.id);
}

document.fonts.load(font).then((_) => {
    
    // Map sprite
    var mapSprite   = new Image();
    // mapSprite.src   = "human_body.jpeg";
    mapSprite.src   = $("#src_gambar").val();
    
    // selected_marker=$("#jenis_drawing").val();
	set_marker();
    // console.log(selected_marker);
    
    var Marker = function () {
        this.Width = 20;
        this.Height = 20;
        this.XPos = 0;
        this.YPos = 0;
    }

    var Markers = new Array();
	var str_key='';
	var str_path='';
	var selected_marker2=null;
	<? foreach($list_lokalis as $r){?>
	
	str_path='<?=$r->path?>';
	str_key='<?=$r->key?>';
	selected_marker2 = marker_type.find(({ key }) => key === str_key);
		 var marker      = new Marker();
		marker.XPos     = '<?=$r->XPos?>';
		marker.YPos     = '<?=$r->YPos?>';
		marker.key      = '<?=$r->key?>';
		marker.content  = selected_marker2.content;
		marker.warna    = '<?=$r->warna?>';
		if (str_path){
			marker.path  = JSON.parse("[" + str_path + "]");
		}
		// marker.path    = <?=($r->path)?>;
			 
		marker.keterangan    = '<?=$r->keterangan?>';
		marker.class_name    = '<?=$r->class_name?>';
		marker.font    = selected_marker2.font;
		marker.path    = <?= ($r->path)? "JSON.parse('".$r->path."')" : $r->path?>;

		Markers.push(marker);
		load_data();
		console.log(Markers);
	<?}?>
    var mouseClicked = function (mouse) {
		if (status_assemen!='2'){
			if(selected_marker != null){
				var rect        = canvas.getBoundingClientRect();
				if(selected_marker.key == 'free_draw'){
					st_drawing = true;
					context.beginPath();
					context.moveTo(mouse.x - rect.left, mouse.y - rect.top);
					
				}else{
					// Get corrent mouse coords
					
					var mouseXPos = (mouse.x - rect.left)+10;
					var mouseYPos = (mouse.y - rect.top) +30;

					
					// Move the marker when placed to a better location
					const warna      = document.getElementById('warna');
					var marker      = new Marker();
					marker.XPos     = mouseXPos - (marker.Width / 2);
					marker.YPos     = mouseYPos - marker.Height;
					marker.key      = selected_marker.key;
					marker.content  = selected_marker.content;
					marker.warna    = warna.value;
					marker.keterangan    = '';
					marker.class_name    = selected_marker.class_name;
					marker.font    = selected_marker.font;
					marker.path    = '';
					

					Markers.push(marker);
				}
			}
        }
    }

    // Add mouse click event listener to canvas
    canvas.addEventListener("mousedown", mouseClicked, false);
    
    canvas.onmousemove = draw_line;
    canvas.onmouseup = stopDrawing;

    var firstLoad = function () {
        context.font = "15px Georgia";
        context.textAlign = "center";
    }

    firstLoad();

    var main = function () {
        draw();
    };
    
    //\uf10c bulat
    //\uf0d8 segitiga
    //\uf096 kotak
    //\uf00d silang
    //\uf219 diamond

    var draw = function () {
        if(st_drawing == false){
			
            // Clear Canvas
            context.fillStyle = "#000";
            context.fillRect(0, 0, canvas.width, canvas.height);

            // Draw map
            // Sprite, X location, Y location, Image width, Image height
            // You can leave the image height and width off, if you do it will draw the image at default size
          
				// console.log(Markers.length);
            context.drawImage(mapSprite, 0, 0, 680, 500);
            
            // Draw markers
            for (var i = 0; i < Markers.length; i++) {
              // note that I wasn't able to find \uF006 defined in the provided CSS
              // falling back to fa-bug for demo
                
                var tempMarker = Markers[i];
                // console.log(tempMarker);
                context.font = tempMarker.font;
                context.fillStyle = tempMarker.warna;
                // Draw marker
                //context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);
                let markerText = i + 1;
                let textMeasurements = context.measureText(markerText);
                
                
                if(tempMarker.key != 'free_draw'){
                    context.fillText(tempMarker.content, tempMarker.XPos, tempMarker.YPos);
                    // Calculate postion text
                    
                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 50, textMeasurements.width, 20);
                    
                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, tempMarker.XPos, tempMarker.YPos - 35);
                }else{
                    context.beginPath();
                    const begin_path =  tempMarker.path[0];
                    context.moveTo(begin_path.x, begin_path.y);
                    context.strokeStyle  = tempMarker.warna;
                    tempMarker.path.map( (p)=> {
                         context.lineTo(p.x, p.y);
                         context.stroke();
                    })
                    
                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(begin_path.x - (textMeasurements.width / 2), begin_path.y - 25, textMeasurements.width, 20);
                    
                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, begin_path.x, begin_path.y - 10);
                    
                }
            } 
        }
       
    };
    
    function draw_line(e){
        if (st_drawing == true && selected_marker.key == 'free_draw'){
             const rect        = canvas.getBoundingClientRect();
             const x = e.x - rect.left;
             const y = e.y - rect.top;
             path.push({x:x,y:y});
             const warna      = document.getElementById('warna');
             context.strokeStyle  = warna.value;
             console.log(context);
             console.log(x+' : '+y);
             context.lineTo(x, y);
             context.stroke();
        }
    }
    
    function stopDrawing() {
        if(selected_marker != null && selected_marker.key == 'free_draw'){
            if(path.length > 0){
                const warna      = document.getElementById('warna');
                const marker        = new Marker();
                marker.key          = selected_marker.key;
                marker.content      = '';
                marker.path         = path;
                marker.warna        = warna.value;
                marker.keterangan   = '';
                marker.class_name    = selected_marker.class_name;
                Markers.push(marker);
            }
            st_drawing = false;
            path = [];
        }
		// console.log('STIP');
        load_data();
   }
   $(document).on("blur",".input-keterangan",function(){	
		let index=$(this).attr("data-id");
		Markers[index].keterangan=$(this).val();
		simpan_status_lokalis();
		// $(this).closest('tr').find(".isiField").
	   
   });
   // $(".input-keterangan").blur(function(){
	// });

   function load_data(){
    const table = document.getElementById('simbol-list');
    let table_content = '';
    table.querySelector('tbody').innerHTML = '';
    for (var i = 0; i < Markers.length; i++) {
        const row = Markers[i];
        table_content += '<tr>';
        table_content +='<td>'+(i+1)+'</td>';
        table_content +='<td class="text-center"><i class="fa '+row.class_name+'"></i></td>';
        table_content +='<td><input data-id="'+ i +'" class="input-keterangan form-control input-sm"  type="text" value="'+row.keterangan+'"/></td>';
        table_content +='<td><button class="hapus-data btn btn-sm btn-danger" data-id="'+ i +'"><i class="fa fa-trash"></i></button></td>';
        table_content +='</tr>';
    }
    table.querySelector('tbody').innerHTML = table_content;
    // console.log(Markers);\
	disabel_edit();
    simpan_status_lokalis();
    const hapus_data = document.querySelectorAll('.hapus-data');
    hapus_data.forEach(el => el.addEventListener('click', event => {
        Markers.splice(el.dataset.id, 1);
        load_data();
    }));
   }



function convertCanvasToImage(aaa) {

    something = aaa[0].toDataURL("image/png");

}
   function simpan_status_lokalis(){// alert($("#keluhan_utama").val());
		   // var ctx  = document.createElement("canvas");

// var ctx1 = ctx.getContext("2d");    

// var asd  = convertCanvasToImage(ctx1)
		let canvas = $("#canvas")[0];
		let gambar_final = canvas.toDataURL("image/png");

		// $("#elememt-to-write-to").html('<img src="'+gambar_final+'"/>');
		// console.log(gambar_final);
		let assesmen_id=$("#assesmen_id").val();
		let gambar_id=$("#gambar_id").val();
		const jsonString = JSON.stringify(Object.assign({}, Markers));
		const json_obj = JSON.parse(jsonString);
		console.log(json_obj);
		// $("#cover-spin").show();
		// if (Markers.length>0){
			if (assesmen_id){
				$.ajax({
					url: '{site_url}tpendaftaran_ranap_erm/simpan_status_lokalis_ri', 
					dataType: "JSON",
					method: "POST",
					data : {
							assesmen_id:assesmen_id,
							gambar_id:gambar_id,
							gambar_final:gambar_final,
							jsonString:json_obj,
							
						},
					success: function(data) {
						// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Default Warna'});
					}
				});
			}
		// }
		
	}
    setInterval(main, (1000 / 60)); // Refresh 60 times a second
    $("#gambar_id").on("change", function(){
		load_gambar();
	}); 
    function load_gambar(){
		let assesmen_id=$("#assesmen_id").val();
		let gambar_id=$("#gambar_id").val();
		let status_assemen=$("#status_assemen").val();
		let versi_edit=$("#versi_edit").val();
		let base_url=$("#base_url").val();
		
		var canvas      = document.getElementById('canvas');
		var context     = canvas.getContext("2d");
			$("#cover-spin").show();	
		$.ajax({
			url: '{site_url}Thistory_tindakan/load_gambar_asmed_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					gambar_id:gambar_id,
					status_assemen:status_assemen,
					versi_edit:versi_edit,
					
				},
			success: function(data) {
				$("#src_gambar").val(base_url+data.gambar_tubuh);
				
				st_drawing  = false;
				// Map sprite
				mapSprite   = new Image();
				// mapSprite.src   = "human_body.jpeg";
				mapSprite.src   = $("#src_gambar").val();
				 Markers = new Array();
				var str_key='';
				var selected_marker2=null;
				
				$.each(data.list_lokalis, function (i,r) {
					str_key=r.key;
					selected_marker2 = marker_type.find(({ key }) => key === str_key);
					 var marker      = new Marker();
					marker.XPos     = r.XPos;
					marker.YPos     = r.YPos;
					marker.key      = r.key;
					marker.content  = selected_marker2.content;
					marker.warna    = r.warna;
					marker.keterangan    = r.keterangan;
					marker.class_name    = r.class_name;
					marker.font     = selected_marker.font;
					marker.path     = JSON.parse(r.path);

					Markers.push(marker);
					// console.log(Markers);
					// $('#nojabatan')
					// .append('<option value="' + pegawai_item.nojabatan + '">' + pegawai_item.namajabatan + '</option>');
				});
				 const table = document.getElementById('simbol-list');
					let table_content = '';
					table.querySelector('tbody').innerHTML = '';
					table.querySelector('tbody').innerHTML = table_content;
					load_data();
					$("#cover-spin").hide();
				draw();
				// alert('sini');
			}
		});
		}
})
<?}?>
// $(".input-keterangan").keyup(function(){
	// console.log($(this).val()));		
// });




// $('#clear_2').click(function(e) {
	// alert('sini');
	// // e.preventDefault();
	
// });
function load_anatomi(){
		
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	$('#tabel_anatomi').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#tabel_anatomi').DataTable({
			autoWidth: false,
			serverSide: true,
			"searching": false,
			"processing": false,
			"bPaginate": false,
			"bFilter": false,
			"bInfo": false,
			"order": [],
			// "pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}Thistory_tindakan/load_anatomi_asmed_ri', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
}
$(document).ready(function() {
	// $str='[{"x":"515.9375","y":"378.5520820617676"},{"x":"517.9375","y":"376.5520820617676"},{"x":"520.9375","y":"373.5520820617676"},{"x":"527.9375","y":"370.5520820617676"},{"x":"533.9375","y":"368.5520820617676"},{"x":"540.9375","y":"367.5520820617676"},{"x":"546.9375","y":"365.5520820617676"},{"x":"551.9375","y":"364.5520820617676"},{"x":"552.9375","y":"363.5520820617676"},{"x":"553.9375","y":"363.5520820617676"},{"x":"556.9375","y":"363.5520820617676"},{"x":"557.9375","y":"363.5520820617676"},{"x":"558.9375","y":"365.5520820617676"},{"x":"560.9375","y":"366.5520820617676"},{"x":"561.9375","y":"368.5520820617676"},{"x":"563.9375","y":"369.5520820617676"},{"x":"565.9375","y":"372.5520820617676"},{"x":"566.9375","y":"375.5520820617676"},{"x":"567.9375","y":"378.5520820617676"},{"x":"567.9375","y":"380.5520820617676"},{"x":"568.9375","y":"383.5520820617676"},{"x":"568.9375","y":"385.5520820617676"},{"x":"569.9375","y":"388.5520820617676"},{"x":"569.9375","y":"390.5520820617676"},{"x":"569.9375","y":"394.5520820617676"},{"x":"569.9375","y":"397.5520820617676"},{"x":"568.9375","y":"400.5520820617676"},{"x":"560.9375","y":"408.5520820617676"},{"x":"559.9375","y":"409.5520820617676"},{"x":"557.9375","y":"410.5520820617676"},{"x":"556.9375","y":"412.5520820617676"},{"x":"555.9375","y":"412.5520820617676"},{"x":"552.9375","y":"412.5520820617676"},{"x":"550.9375","y":"409.5520820617676"},{"x":"545.9375","y":"406.5520820617676"},{"x":"541.9375","y":"403.5520820617676"},{"x":"539.9375","y":"402.5520820617676"},{"x":"538.9375","y":"400.5520820617676"},{"x":"537.9375","y":"398.5520820617676"},{"x":"536.9375","y":"397.5520820617676"},{"x":"535.9375","y":"396.5520820617676"},{"x":"532.9375","y":"395.5520820617676"},{"x":"532.9375","y":"394.5520820617676"},{"x":"531.9375","y":"394.5520820617676"},{"x":"530.9375","y":"393.5520820617676"},{"x":"530.9375","y":"392.5520820617676"},{"x":"530.9375","y":"390.5520820617676"},{"x":"529.9375","y":"390.5520820617676"},{"x":"529.9375","y":"388.5520820617676"},{"x":"529.9375","y":"387.5520820617676"},{"x":"529.9375","y":"386.5520820617676"},{"x":"530.9375","y":"385.5520820617676"},{"x":"531.9375","y":"384.5520820617676"},{"x":"535.9375","y":"382.5520820617676"},{"x":"537.9375","y":"379.5520820617676"},{"x":"538.9375","y":"378.5520820617676"},{"x":"542.9375","y":"376.5520820617676"},{"x":"545.9375","y":"375.5520820617676"},{"x":"548.9375","y":"373.5520820617676"},{"x":"550.9375","y":"372.5520820617676"},{"x":"553.9375","y":"372.5520820617676"},{"x":"556.9375","y":"372.5520820617676"},{"x":"558.9375","y":"372.5520820617676"},{"x":"560.9375","y":"372.5520820617676"},{"x":"561.9375","y":"372.5520820617676"},{"x":"565.9375","y":"373.5520820617676"},{"x":"566.9375","y":"373.5520820617676"},{"x":"566.9375","y":"374.5520820617676"},{"x":"567.9375","y":"375.5520820617676"},{"x":"568.9375","y":"376.5520820617676"},{"x":"570.9375","y":"377.5520820617676"},{"x":"571.9375","y":"379.5520820617676"},{"x":"573.9375","y":"380.5520820617676"},{"x":"573.9375","y":"382.5520820617676"},{"x":"575.9375","y":"384.5520820617676"},{"x":"576.9375","y":"385.5520820617676"},{"x":"576.9375","y":"387.5520820617676"},{"x":"577.9375","y":"387.5520820617676"}]	';
	// var array = JSON.parse("[" + $str + "]");
	// console.log(array);
	// alert($str);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		// set_anamnesa();
		// set_riwayat($("#st_riwayat_penyakit").val())
		// set_riwayat_1($("#st_riwayat_penyakit_dahulu").val())
		// set_riwayat_2($("#st_riwayat_penyakit_keluarga").val())
		load_anatomi();
		set_anamnesa();
		list_index_history_edit();
		load_awal_assesmen=false;
	}
	// $("#mdiagnosa_id").select2({
        // minimumInputLength: 2,
		// tags: true,
		// noResults: 'Diagnosa Tidak Ditemukan.',
		// // allowClear: true
        // // tags: [],
        // ajax: {
            // url: '{site_url}tpendaftaran_ranap_erm/get_diagnosa_medis/',
            // dataType: 'json',
            // type: "POST",
            // quietMillis: 50,
		 // data: function (params) {
              // var query = {
                // search: params.term,
              // }
              // return query;
            // },
            // processResults: function (data) {
                // return {
                    // results: $.map(data, function (item) {
                        // return {
                            // text: item.nama,
                            // id: item.id,
                        // }
                    // })
                // };
            // }
        // }
    // });
	// load_data_rencana_asuhan(1);
});
function set_riwayat(st_riwayat_penyakit){
	$("#st_riwayat_penyakit").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit").removeAttr('disabled');
		$("#riwayat_penyakit_lainnya").removeAttr('disabled');
		$("#riwayat_penyakit").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit").val('').trigger('change');
		$("#riwayat_penyakit").removeAttr('required');
		$("#riwayat_penyakit").attr('disabled','disabled');
		$("#riwayat_penyakit_lainnya").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
function set_riwayat_1(st_riwayat_penyakit){
	$("#st_riwayat_penyakit_dahulu").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit_dahulu").removeAttr('disabled');
		$("#riwayat_penyakit_dahulu_list").removeAttr('disabled');
		
		$("#riwayat_penyakit_dahulu").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit_dahulu_list").val('').trigger('change');
		$("#riwayat_penyakit_dahulu").removeAttr('required');
		$("#riwayat_penyakit_dahulu_list").attr('disabled','disabled');
		$("#riwayat_penyakit_dahulu").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
function set_riwayat_2(st_riwayat_penyakit){
	$("#st_riwayat_penyakit_keluarga").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit_keluarga").removeAttr('disabled');
		$("#riwayat_penyakit_keluarga_list").removeAttr('disabled');
		
		$("#riwayat_penyakit_dahulu").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit_keluarga_list").val('').trigger('change');
		$("#riwayat_penyakit_keluarga").removeAttr('required');
		$("#riwayat_penyakit_keluarga").attr('disabled','disabled');
		$("#riwayat_penyakit_keluarga_list").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
function generate_riwayat_penyakit(){
	if ($("#status_assemen").val()!='2'){
	var select_button_text
	if ($("#st_riwayat_penyakit").val()=='1'){
		select_button_text = $('#riwayat_penyakit option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
	}
	
	$("#riwayat_penyakit_lainnya").val(select_button_text);
	
	var select_button_text
	if ($("#st_riwayat_penyakit_dahulu").val()=='1'){
		select_button_text = $('#riwayat_penyakit_dahulu_list option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
	}
	
	$("#riwayat_penyakit_dahulu").val(select_button_text);
	var select_button_text
	if ($("#st_riwayat_penyakit_keluarga").val()=='1'){
		select_button_text = $('#riwayat_penyakit_keluarga_list option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
	}
	
	$("#riwayat_penyakit_keluarga").val(select_button_text);
	}
}
function set_img_tubuh(){
	
	var src =$("#src_gambar").val();

	$('#sig_tubuh').signature('enable'). signature('draw', src);
}
function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#gambar_id").removeAttr('disabled');
		
		 $(".his_filter").removeAttr('disabled');
		 $(".edited").removeClass("edited").addClass("edited_final");
	}
}
function set_allo(){
	$("#st_anamnesa").val('2');
	set_anamnesa();
}
function set_auto(){
	$("#st_anamnesa").val('1');
	set_anamnesa();
}

function set_anamnesa(){
	if ($("#st_anamnesa").val()=='1'){
		$(".allo_anamnesa").attr('disabled','disabled');
		$(".allo_anamnesa").removeAttr('required');
		$("#nama_anamnesa").val('');
		$("#hubungan_anamnesa").val('');
	}else{
		$(".allo_anamnesa").removeAttr('disabled');
		$(".allo_anamnesa").attr('required');
		
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "20%", "targets": [1] },
					 // { "width": "15%", "targets": [3] },
					 // { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_asmed_ri', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
</script>