<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.bg-edited {
		background-color: #ffedf2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_assesmen_keselamatan' || $menu_kiri=='his_assesmen_keselamatan_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_assesmen_keselamatan' || $menu_kiri=='his_assesmen_keselamatan_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_keselamatan=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >							
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="pull-right push-10-r">
								<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_gizi_anak" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="block">
							<ul class="nav nav-tabs" data-toggle="tabs">
								<li class="<?=($step_wizard=='0'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(0)" >ALL</a>
								</li>
								<li class="<?=($step_wizard=='1'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(1)" ><?=$label_sig_in_ina?></a>
								</li>
								<li class="<?=($step_wizard=='2'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(2)" ><?=$label_time_out_ina?></a>
								</li>
								<li class="<?=($step_wizard=='3'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(3)" ><?=$label_sign_out_ina?></a>
								</li>
								
								
							</ul>
							<div class="block-content tab-content">
								<div class="tab-pane fade fade-left active in" id="tab_wizard_0">
									<div class="row">
										<div class="form-group">
											<div class="col-md-4 div_jenis_1">
												<div class="table-responsive">
													<table class="table table-bordered table-condensed" id="tabel_skrining_1">
														<thead>
															<tr>
																<th colspan="3" width="100%" class="text-center"><strong><span class="h4 text-primary"><?=$label_sig_in_ina?></span></strong></th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="col-md-4 div_jenis_2">
												<div class="table-responsive">
													<table class="table table-bordered table-condensed " id="tabel_skrining_2">
														<thead>
															<tr>
																<th colspan="3" width="100%" class="text-center"><strong><span class="h4 text-primary"><?=$label_time_out_ina?></span></strong></th>
																
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="col-md-4 div_jenis_3">
												<div class="table-responsive">
													<table class="table table-bordered table-condensed " id="tabel_skrining_3">
														<thead>
															<tr>
																<th colspan="3" width="100%" class="text-center"><strong><span class="h4 text-primary"><?=$label_sign_out_ina?></span></strong></th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?}?>
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_keselamatan();
		set_tab_wizard(0);
		load_awal_assesmen=false;
	}
	list_index_history_edit();
	// load_data_rencana_asuhan_keselamatan(1);
});
function set_tab_wizard($id){
	step_wizard=$id;
	$(".div_jenis_1").removeClass("col-md-4 col-md-12");
	$(".div_jenis_2").removeClass("col-md-4 col-md-12");
	$(".div_jenis_3").removeClass("col-md-4 col-md-12");
	$(".div_jenis_1").hide();
	$(".div_jenis_2").hide();
	$(".div_jenis_3").hide();
	if (step_wizard=='0'){
		$(".div_jenis_1").show();
		$(".div_jenis_2").show();
		$(".div_jenis_3").show();
		$(".div_jenis_1").addClass("col-md-4");
		$(".div_jenis_2").addClass("col-md-4");
		$(".div_jenis_3").addClass("col-md-4");
	}
	if (step_wizard=='1'){
		$(".div_jenis_1").show();
		$(".div_jenis_1").addClass("col-md-12");
	}
	if (step_wizard=='2'){
		$(".div_jenis_2").show();
		$(".div_jenis_2").addClass("col-md-12");
	}
	if (step_wizard=='3'){
		$(".div_jenis_3").show();
		$(".div_jenis_3").addClass("col-md-12");
	}
	
}
function load_skrining_keselamatan(){
	let assesmen_id=$("#assesmen_id").val();
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				jenis_header:1,
				versi_edit:$("#versi_edit").val(),
				},
		success: function(data) {
			$("#tabel_skrining_1 tbody").empty();
			$("#tabel_skrining_1 tbody").append(data.tabel);
			$(".js-select2").select2();
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				jenis_header:2,
				versi_edit:$("#versi_edit").val(),
				},
		success: function(data) {
			$("#tabel_skrining_2 tbody").empty();
			$("#tabel_skrining_2 tbody").append(data.tabel);
			$(".js-select2").select2();
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				jenis_header:3,
				versi_edit:$("#versi_edit").val(),
				},
		success: function(data) {
			$("#tabel_skrining_3 tbody").empty();
			$("#tabel_skrining_3 tbody").append(data.tabel);
			$(".js-select2").select2();
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_keselamatan', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

</script>