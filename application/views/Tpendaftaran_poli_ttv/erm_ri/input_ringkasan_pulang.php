<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_ringkasan_pulang'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_ringkasan_pulang' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
						$disabel_input='';
						
						if ($st_input_ringkasan_pulang=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_ringkasan_pulang=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_ringkasan_pulang=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_pulang()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="tanggal" value="<?=date('Y-m-d')?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
								<h5 class="font-w400 push-5 text-center"><strong><i>{judul_header_eng}</i></strong></h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_ringkasan_pulang=='1'){?>
									<button class="btn btn-primary" id="btn_create_ringkasan_pulang" onclick="create_ringkasan_pulang()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_ringkasan_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<?if ($disabel_cetak==''){?>
										<a href="{base_url}tpendaftaran_ranap_erm/cetak_assesmen_ringkasan_pulang/<?=$assesmen_id?>" target="_blank" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<button class="btn btn-default btn_ttd menu_click" type="button" onclick="goBack()"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_pulang" disabled <?=($st_input_ringkasan_pulang!='1'?'disabled':'')?> onclick="create_with_template_pulang()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<button class="btn btn-warning btn-xs" type="button" id="btn_generate_all" <?=($st_input_ringkasan_pulang!='1'?'disabled':'')?> onclick="generate_info_all()"><i class="glyphicon glyphicon-download-alt"></i> Generate Data ALL </button>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="indikasi_rawat"><?=$indikasi_rawat_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('indikasi_rawat')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<textarea class="form-control js-summernote auto_blur" id="indikasi_rawat"  rows="2"> <?=$indikasi_rawat?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="ringkasan"><?=$ringkasan_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('ringkasan')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<textarea class="form-control js-summernote auto_blur" id="ringkasan"  rows="2"> <?=$ringkasan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="pemeriksaan"><?=$pemeriksaan_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('pemeriksaan')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<textarea class="form-control js-summernote auto_blur" id="pemeriksaan"  rows="2"> <?=$pemeriksaan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="penunjang"><?=$penunjang_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('penunjang')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<textarea class="form-control js-summernote auto_blur" id="penunjang"  rows="2"> <?=$penunjang?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="konsultasi"><?=$konsultasi_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('konsultasi')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<textarea class="form-control js-summernote auto_blur" id="konsultasi"  rows="2"> <?=$konsultasi?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="therapi"><?=$therapi_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('therapi')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<textarea class="form-control js-summernote auto_blur" id="therapi"  rows="2"> <?=$therapi?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="therapi_pulang"><?=$therapi_pulang_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('therapi_pulang')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<textarea class="form-control js-summernote auto_blur" id="therapi_pulang"  rows="2"> <?=$therapi_pulang?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="komplikasi"><?=$komplikasi_ina?></label>
										<textarea class="form-control js-summernote auto_blur" id="komplikasi"  rows="2"> <?=$komplikasi?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-2">
										<label><?=$prognis_ina?></label>
										<select id="prognis" name="prognis" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(415) as $row){?>
											<option value="<?=$row->id?>" <?=($prognis == $row->id ? 'selected="selected"' : '')?> <?=(($prognis == '0' || $prognis == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-2">
										<label><?=$cara_pulang_ina?></label>
										<select id="cara_pulang" name="cara_pulang" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(416) as $row){?>
											<option value="<?=$row->id?>" <?=($cara_pulang == $row->id ? 'selected="selected"' : '')?> <?=(($cara_pulang == '0' || $cara_pulang == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-2">
										<label><?=$intuksi_fu_ina?></label>
										<select id="intuksi_fu" name="intuksi_fu" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(43) as $row){?>
											<option value="<?=$row->id?>" <?=($intuksi_fu == $row->id ? 'selected="selected"' : '')?> <?=(($intuksi_fu == '0' || $intuksi_fu == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-2">
										<label>TANGGAL KONTROL</label>
										<div class="input-group date">
											<input type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggal_kontrol" placeholder="dd-mm-yyyy" value="<?= $tanggal_kontrol ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-4">
										<label>KETERANGAN LAIN</label>
										<textarea class="form-control auto_blur" id="keterangan"  rows="1" placeholder=""><?=$keterangan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-12">
										<label><?=$diagnosa_utama_ina?></label>&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_info('diagnosa_utama')" type="button"><i class="glyphicon glyphicon-download-alt"></i></button>
										<input class="form-control " type="hidden" id="diagnosa_utama_id"  value="<?=$diagnosa_utama_id?>" placeholder="" >
										<div class="input-group">
											<select id="diagnosa_utama" class="form-control " style="width: 100%;" data-placeholder="Cari Diagnosa" required>
												<?if ($diagnosa_utama_id){?>
													<option value="<?=$diagnosa_utama_id?>" selected><?=$diagnosa_utama?></option>
												<?}?>
											</select>
											<span class="input-group-btn">
												<button class="btn btn-danger" type="button" onclick="hapus_diagnosa_utama()"><i class="fa fa-times"></i> clear</button>
											</span>
										</div>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-striped" id="tabel_diagnosa">
												<thead>
													<tr>
														<th width="5%">No</th>
														<th width="40%">Diagnosa Tambahan</th>
														<th width="15%">Priority</th>
														<th width="20%">User</th>
														<th width="10%">Action</th>
													</tr>
													<? if ($status_assemen!='2'){?>
													<tr>
														<th>#</th>
														<th>
														<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
															<select id="mdiagnosa_id" class="form-control " style="width: 100%;" data-placeholder="Cari Diagnosa" required>
																
															</select>
														</th>
														<th>
															<select  id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																<?for($i=1;$i<20;$i++){?>
																<option value="<?=$i?>"><?=$i?></option>
																<?}?>
															</select>
														</th>
														<th><span id="diagnosa_user"><?=get_nama_ppa($this->session->userdata('login_ppa_id'))?></label></th>
														<th>
															<span class="input-group-btn">
																<button class="btn btn-info" onclick="simpan_diagnosa()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
																<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
															</span>
														</th>
														
													</tr>
													<?}?>
												</thead>
												<tbody></tbody>
											</table>
										</div>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-striped" id="tabel_tindakan">
												<thead>
													<tr>
														<th width="5%">No</th>
														<th width="40%">Tindakan / Prosedur &nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" title="Generate Isi" onclick="generate_tindakan()" type="button"><i class="glyphicon glyphicon-download-alt"></i></button></th>
														<th width="15%">Priority</th>
														<th width="20%">User</th>
														<th width="10%">Action</th>
													</tr>
													<? if ($status_assemen!='2'){?>
													<tr>
														<th>#</th>
														<th>
														<input class="form-control " type="hidden" id="tindakan_id"  value="" placeholder="" >
															<select id="mtindakan_id" class="form-control " style="width: 100%;" data-placeholder="Cari Tindakan" required>
																
															</select>
														</th>
														<th>
															<select  id="prioritas_tindakan" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																<?for($i=1;$i<20;$i++){?>
																<option value="<?=$i?>"><?=$i?></option>
																<?}?>
															</select>
														</th>
														<th><span id="tindakan_user"><?=get_nama_ppa($this->session->userdata('login_ppa_id'))?></label></th>
														<th>
															<span class="input-group-btn">
																<button class="btn btn-info" onclick="simpan_tindakan()" id="btn_add_tindakan" type="button"><i class="fa fa-plus"></i> Add</button>
																<button class="btn btn-warning" onclick="clear_input_tindakan()" type="button"><i class="fa fa-refresh"></i></button>
															</span>
														</th>
														
													</tr>
													<?}?>
												</thead>
												<tbody></tbody>
											</table>
										</div>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<table width="100%">
										<tr>
											<td width="25" class="text-center"><strong><?=$dpjp_ina?></strong><?=($dpjp_eng?'<br><i>'.$dpjp_eng.'</i>':'')?></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center"><strong><?=$keluarga_ina?></strong><?=($keluarga_eng?'<br><i>'.$keluarga_eng.'</i>':'')?></td>
										</tr>
										<tr>
											<td width="25" class="text-center"><strong>
												<img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dpjp; ?>" width="100px">
											</strong></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<?if ($pasien_ttd){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img" style="width:60%;align:center" align="middle" src="<?=$pasien_ttd?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_pernyataan()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
											</td>
										</tr>
										<tr>
											<td width="25" class="text-center">
											<strong><?=get_nama_dokter_ttd($dpjp)?></strong> 
											<?if ($status_assemen!='2'){?>
											<br>
											<button class="btn btn-xs btn-default" onclick="modal_ttd_petugas()"  type="button"><i class="fa fa-paint-brush"></i></button>
											<?}?>
											</td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<strong>
													<?if ($pasien_nama){?>
														(<?=($pasien_nama)?>)
													<?}?>
												</strong>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>

						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-right push-20-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_ringkasan_pulang=='1'){?>
										<button class="btn btn-primary" id="btn_create_ringkasan_pulang" onclick="create_ringkasan_pulang()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										<?if ($assesmen_id!=''){?>
											<?if ($status_assemen=='3'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
											<?}?>
											<?if ($status_assemen=='1'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_ringkasan_pulang()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
											<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
											<?}?>
											<?if ($assesmen_id!=''){?>
											<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_pulang/<?=$assesmen_id?>" target="_blank" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
											<?}?>
											<?if ($status_assemen=='3'){?>
												<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
											<?}?>
											<?if ($status_assemen=='2'){?>
												<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
												<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_rj/input_ringkasan_pulang" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
											<?}?>
										<?}?>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_ringkasan_pulang!='1'?'disabled':'')?> onclick="create_template_pulang()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_ringkasan_pulang()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_ringkasan_pulang()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title"><?=$keluarga_ina?></h3>
				</div>
				<div class="block-content">
					
					
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$pasien_ttd?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="pasien_nama" placeholder="Nama" value="{pasien_nama}">
									<label for="jenis_ttd"><?=$keluarga_ina?></label>
								</div>
							</div>
							
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_petugas" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title"><?=$dpjp_ina?></h3>
				</div>
				<div class="block-content">
					
					<div class="row">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="diserahkan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										
										<?foreach($list_dokter as $row){?>
										<option value="<?=$row->id?>" <?=($dpjp == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_petugas()"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/modal_email')?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	$('.js-summernote').summernote({
		  height: 100,   //set editable area's height
			dialogsInBody: true,
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
	$("#diagnosa_utama").select2({
        minimumInputLength: 2,
		tags: true,
		noResults: 'Diagnosa Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}Tpendaftaran_poli_ttv/get_diagnosa_medis/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });
	$("#mdiagnosa_id").select2({
        minimumInputLength: 2,
		tags: true,
		noResults: 'Diagnosa Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}Tpendaftaran_poli_ttv/get_diagnosa_medis/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });
	$("#mtindakan_id").select2({
        minimumInputLength: 2,
		tags: true,
		noResults: 'Tindakan Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}Tpendaftaran_poli_ttv/get_tindakan_medis/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });
	// set_ttd_assesmen();
	// $(".js-datetimepicker").datetimepicker({
		// format: "dd-mm-yyyy HH:mm"
	// });
	$('.number').number(true, 0);
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_diagnosa();
		load_tindakan();
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian()
	}
	
	// load_data_rencana_asuhan_pulang(1);
});
$("#diagnosa_utama").change(function(){
	if ($(this).val()==$("#diagnosa_utama option:selected").text()){
		// alert('BARU');
		$("#diagnosa_utama_id").val('');
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_diagnosa_medis', 
			dataType: "JSON",
			method: "POST",
			data : {
					diagnosa_id:$("#diagnosa_utama_id").val(),
					nama:$("#diagnosa_utama option:selected").text(),
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#diagnosa_utama_id").val(data.id);
				simpan_assesmen();
			}
		});
	}else{
		$("#diagnosa_utama_id").val($(this).val());
	// alert($(this).val());
		
	}
});

function modal_ttd_petugas(){
	$("#modal_ttd_petugas").modal('show');
}
function modal_ttd_pernyataan(){
	let ttd=$("#signature64").val();
	$("#modal_ttd").modal('show');
	$('#sig_ttd_pernyataan').signature('enable').signature('draw', $("#signature64_ttd_pernyataan").val());
}
function clear_ttd_pernyataan(){
	sig_ttd_pernyataan.signature('clear');
	$("#signature64").val('');
}
 $(document).find('.js-summernote').on('summernote.blur', function() {
   if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
  });
  function hapus_diagnosa_utama(){
	$("#diagnosa_utama").val(null).trigger('change');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}
  }
  function simpan_diagnosa(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let mdiagnosa_id=$("#mdiagnosa_id").val();
		let diagnosa_id=$("#diagnosa_id").val();
		let prioritas=$("#prioritas").val();
		let mdiagnosa_nama=$("#mdiagnosa_id option:selected").text();	
		// alert($("#mdiagnosa_id").val());
		let idpasien=$("#idpasien").val();
		if (mdiagnosa_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mdiagnosa_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (load_awal_assesmen==false){
			
	
			if (assesmen_id){
				$.ajax({
					url: '{site_url}Tpendaftaran_ranap_erm/simpan_diagnosa_ringkasan_pulang', 
					dataType: "JSON",
					method: "POST",
					data : {
							diagnosa_id:diagnosa_id,
							pendaftaran_id:pendaftaran_id,
							mdiagnosa_id:mdiagnosa_id,
							mdiagnosa_nama:mdiagnosa_nama,
							prioritas:prioritas,
							assesmen_id:assesmen_id,
							idpasien:idpasien,
						},
					success: function(data) {
						
						$("#cover-spin").hide();
						if (data==false){
							swal({
								title: "Gagal!",
								text: "Duplikat Diagnosa / Prioritas.",
								type: "error",
								timer: 1500,
								showConfirmButton: false
							});

						}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
							load_diagnosa();
							clear_input_diagnosa();
						}
					}
				});
			}
		}
		
	}
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val(null).trigger('change');
		$("#prioritas").val(next).trigger('change');
		
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function load_diagnosa(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_ringkasan_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa();
	}
	function refresh_diagnosa(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/get_last_prioritas_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function edit_diagnosa(id){
		$("#diagnosa_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_diagnosa_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
				// $("#mdiagnosa_id").val(data.mdiagnosa_id).trigger('change');
				let newOption = new Option(data.nama, data.mdiagnosa_id, true, true);
					
					$("#mdiagnosa_id").append(newOption);

				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function hapus_diagnosa(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_diagnosa_ringkasan_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_diagnosa();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	
	//Tindakan
	function hapus_tindakan_utama(){
	$("#tindakan_utama").val(null).trigger('change');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}
  }
  function simpan_tindakan(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let mtindakan_id=$("#mtindakan_id").val();
		let tindakan_id=$("#tindakan_id").val();
		let prioritas=$("#prioritas_tindakan").val();
		let mtindakan_nama=$("#mtindakan_id option:selected").text();	
		// alert($("#mtindakan_id").val());
		let idpasien=$("#idpasien").val();
		if (mtindakan_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mtindakan_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (load_awal_assesmen==false){
			
	
			if (assesmen_id){
				$.ajax({
					url: '{site_url}Tpendaftaran_ranap_erm/simpan_tindakan_ringkasan_pulang', 
					dataType: "JSON",
					method: "POST",
					data : {
							tindakan_id:tindakan_id,
							pendaftaran_id:pendaftaran_id,
							mtindakan_id:mtindakan_id,
							mtindakan_nama:mtindakan_nama,
							prioritas:prioritas,
							assesmen_id:assesmen_id,
							idpasien:idpasien,
						},
					success: function(data) {
						
						$("#cover-spin").hide();
						if (data==false){
							swal({
								title: "Gagal!",
								text: "Duplikat Diagnosa / Prioritas.",
								type: "error",
								timer: 1500,
								showConfirmButton: false
							});

						}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
							load_tindakan();
							clear_input_tindakan();
						}
					}
				});
			}
		}
		
	}
	function clear_input_tindakan(){
		let next=parseFloat($("#prioritas_tindakan").val())+1;
		$("#tindakan_id").val('');
		$("#mtindakan_id").val(null).trigger('change');
		$("#prioritas_tindakan").val(next).trigger('change');
		
		$("#btn_add_tindakan").html('<i class="fa fa-plus"></i> Add');
	}
	function load_tindakan(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_tindakan').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_tindakan').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_tindakan_ringkasan_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_tindakan();
	}
	function refresh_tindakan(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#tindakan_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/get_last_prioritas_tindakan_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#prioritas_tindakan").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function edit_tindakan(id){
		$("#tindakan_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_tindakan_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_tindakan").html('<i class="fa fa-save"></i> Simpan');
				// $("#mtindakan_id").val(data.mtindakan_id).trigger('change');
				let newOption = new Option(data.nama, data.mtindakan_id, true, true);
					
					$("#mtindakan_id").append(newOption);

				$("#prioritas_tindakan").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function hapus_tindakan(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_tindakan_ringkasan_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_tindakan();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
function simpan_ttd(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var diterima_pasien_nama=$("#pasien_nama").val();
	if ($("#pasien_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama ", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_ringkasan_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				diterima_pasien_nama:diterima_pasien_nama,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  location.reload();
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
function simpan_ttd_petugas(){
	var assesmen_id=$("#assesmen_id").val();
	var diserahkan=$("#diserahkan").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_petugas_ringkasan_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				diserahkan:diserahkan,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  location.reload();
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
function hapus_ttd_pernyataan(){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64").val('')
	var signature64=$("#signature64").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_ringkasan_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").show();
				location.reload();
			}
		});
}

	$(".opsi_change").change(function(){
		console.log('OPSI CHANTE')
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});

	$(".auto_blur").blur(function(){
		console.log('BLUR')
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
		
	});
	function simpan_assesmen(){
	// alert(load_awal_assesmen);
	if (status_assemen !='2'){
		// alert($("#diagnosa_utama option:selected").text());
		if (load_awal_assesmen==false){
		let assesmen_id=$("#assesmen_id").val();
		// alert($("#tanggal_keluar").val());return false;
		let tanggaldatang=$("#tanggaldatang").val();
		let waktudatang=$("#waktudatang").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/save_ringkasan_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						// nama_template:nama_template,
						tanggaldatang:tanggaldatang,
						waktudatang:waktudatang,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						indikasi_rawat : $("#indikasi_rawat").val(),
						ringkasan : $("#ringkasan").val(),
						pemeriksaan : $("#pemeriksaan").val(),
						penunjang : $("#penunjang").val(),
						konsultasi : $("#konsultasi").val(),
						therapi : $("#therapi").val(),
						therapi_pulang : $("#therapi_pulang").val(),
						komplikasi : $("#komplikasi").val(),
						prognis : $("#prognis").val(),
						cara_pulang : $("#cara_pulang").val(),
						intuksi_fu : $("#intuksi_fu").val(),
						tanggal_kontrol : $("#tanggal_kontrol").val(),
						keterangan : $("#keterangan").val(),
						diagnosa_utama : $("#diagnosa_utama option:selected").text(),
						diagnosa_utama_id : $("#diagnosa_utama_id").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
			}
		 }
		}
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_history_ringkasan_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
function generate_info_all(){
	generate_info('indikasi_rawat');
	generate_info('ringkasan');
	generate_info('pemeriksaan');
	generate_info('penunjang');
	generate_info('konsultasi');
	generate_info('therapi');
	generate_info('therapi_pulang');
	generate_info('diagnosa_utama');
	generate_tindakan();
}
function generate_info(nama_id){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/generate_info_ringkasan_pulang', 
		dataType: "JSON",
		method: "POST",
		data : {
				pendaftaran_id:pendaftaran_id,
				pendaftaran_id_ranap:pendaftaran_id_ranap,
				nama_id:nama_id,
				
			   },
		success: function(data) {
			if (nama_id=='diagnosa_utama'){
				let newOption = new Option(data.hasil, data.hasil, true, true);
				$("#diagnosa_utama").append(newOption);
			}else{
				$("#"+nama_id).summernote('code',data.hasil);
			}
			if ($("#st_edited").val()=='0'){
				$(this).removeClass('input_edited');
				simpan_assesmen();
			}
		
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
		}
	});
}
function generate_tindakan(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let assesmen_id=$("#assesmen_id").val();
	let idpasien=$("#idpasien").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/generate_tindakan_auto', 
		dataType: "JSON",
		method: "POST",
		data : {
				pendaftaran_id:pendaftaran_id,
				pendaftaran_id_ranap:pendaftaran_id_ranap,
				assesmen_id:assesmen_id,
				idpasien:idpasien,
				
			   },
		success: function(data) {
			load_tindakan();
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
		}
	});
}
function create_ringkasan_pulang(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd_pulang(){
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}

	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_pulang/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function load_ttd_pulang(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_ttd_pulang', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;
			
			// $("#sig").html('<img src="'+src+'"/>')
			// console.log(data.ttd);
		// set_img_canvas();
			$('#sig').signature('enable'). signature('draw', data.ttd);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd_pulang();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
	}
}
$(document).on('change','#intuksi_fu',function(){
	set_rencana_kontrol();
});
$(document).on('change','#tglpendaftaran',function(){
	set_rencana_kontrol();
});
function set_rencana_kontrol(){
	let rencana_kontrol=$("#intuksi_fu").val();
	if (rencana_kontrol!='0'){
		let tanggal=$("#tglpendaftaran").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/set_rencana_kontrol', 
			dataType: "JSON",
			method: "POST",
			data : {
					tanggal:tanggal,
					rencana_kontrol:rencana_kontrol,
				   },
			success: function(data) {
				// alert(data);
				$("#tanggal_kontrol").val(data);
				simpan_assesmen();
			}
		});
	}else{
		$("#tanggal_kontrol").val('');
	}
}
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_pulang();
}
function create_with_template_pulang(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_pulang(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_template_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_ringkasan_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_pulang(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_template_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			// simpan_assesmen_ttv();
			// simpan_edukasi_pulang();
			// simpan_riwayat_penyakit_pulang();
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}

	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template_pulang").removeAttr('disabled');
		}else{
			$("#btn_create_with_template_pulang").attr('disabled','disabled');
		}
	});
	function list_index_template_pulang(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_index_template_ringkasan_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id_ranap);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_template_ringkasan_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_ringkasan_pulang(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		// alert(assesmen_detail_id);return false;
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_edit_ringkasan_pulang', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id=data.pendaftaran_id_ranap;
					// console.log(data);
					// return false;
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_ri/input_ringkasan_pulang'); ?>";
				}
			}
		});
	}
	function hapus_record_ringkasan_pulang(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_ringkasan_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_ringkasan_pulang', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template_pulang();
				}
			});
		});

	}
	
</script>