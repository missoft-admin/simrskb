<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	.input-keterangan{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.border-bottom{
        border-bottom:1px solid #e9e9e9 !important;
		
      }
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_discarge'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_discarge' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						// $tanggal_masuk_rs=HumanDateLong($tanggal_masuk_rs);
						$tanggalmasukrs=HumanDateShort($tanggal_masuk_rs);
						$waktumasukrs=HumanTime($tanggal_masuk_rs);
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktumasukrs=date('H:i:s');
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggalmasukrs=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_discarge=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_discarge=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian  <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'Baru')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_discarge=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_discarge=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-disk"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-save"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_discarge" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_discarge!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >SAAT MASUK RUMAH SAKIT</h5>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-4">
								<div class="col-md-6">
									<label for="tanggalmasukrs">Waktu Masuk RS</label>
									<div class="input-group date">
										<input id="tanggalmasukrs"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalmasukrs ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="tanggalmasukrs">&nbsp;</label>
									<div class="input-group">
										<input id="waktumasukrs" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktumasukrs ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="col-md-4">
									<label for="rencana_pemulangan">Rencana Pemulangan</label>
									<input id="rencana_pemulangan" class="auto_blur form-control "  type="text"  value="{rencana_pemulangan}"  placeholder="Rencana Pemulangan" required>
								</div>
								
								<div class="col-md-4">
									<label for="alasan_masuk_rs">Alasan Masuk RS</label>
									<input id="alasan_masuk_rs" class="auto_blur form-control "  type="text"  value="{alasan_masuk_rs}"  placeholder="Alasan Masuk RS" required>
								</div>
								<div class="col-md-4">
									<label for="diagnosa_medis">Diagnosa Medis</label>
									<input id="diagnosa_medis" class="auto_blur form-control "  type="text"  value="{diagnosa_medis}"  placeholder="Diagnosa Medis" required>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >KETERANGAN RENCANA PEMULANGAN</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="pengaruh_ranap_pasien">Pengaruh Rawat Inap Terhadap Pasien / Keluarga</label>
									<select id="pengaruh_ranap_pasien" name="pengaruh_ranap_pasien" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pengaruh_ranap_pasien == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(270) as $row){?>
										<option value="<?=$row->id?>"  <?=($pengaruh_ranap_pasien == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaruh_ranap_pasien == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="pengaruh_ranap_pasien_detail">Detail Pengaruh Rawat Inap Terhadap Pasien / Keluarga</label>
									<input id="pengaruh_ranap_pasien_detail" class="auto_blur form-control "  type="text"  value="{pengaruh_ranap_pasien_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="pengaruh_pekerjaan">Pengaruh Pengaruh Rawat inap terhadap pekerjaan / sekolah</label>
									<select id="pengaruh_pekerjaan" name="pengaruh_pekerjaan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pengaruh_pekerjaan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(271) as $row){?>
										<option value="<?=$row->id?>"  <?=($pengaruh_pekerjaan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaruh_pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="pengaruh_pekerjaan_detail">Detail Pengaruh Pengaruh Rawat inap terhadap pekerjaan / sekolah</label>
									<input id="pengaruh_pekerjaan_detail" class="auto_blur form-control "  type="text"  value="{pengaruh_pekerjaan_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="pengaruh_keuangan">Pengaruh Rawat Inap Terhadap Keuangan</label>
									<select id="pengaruh_keuangan" name="pengaruh_keuangan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pengaruh_keuangan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(272) as $row){?>
										<option value="<?=$row->id?>"  <?=($pengaruh_keuangan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaruh_keuangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="pengaruh_keuangan_detail">Detail Pengaruh Rawat Inap Terhadap Keuangan</label>
									<input id="pengaruh_keuangan_detail" class="auto_blur form-control "  type="text"  value="{pengaruh_keuangan_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="antisipasi_masalah_pulang">Antisipasi Terhadap Masalah Saat Pulang</label>
									<select id="antisipasi_masalah_pulang" name="antisipasi_masalah_pulang" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($antisipasi_masalah_pulang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(273) as $row){?>
										<option value="<?=$row->id?>"  <?=($antisipasi_masalah_pulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($antisipasi_masalah_pulang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="antisipasi_masalah_pulang_detail">Detail Antisipasi Terhadap Masalah Saat Pulang</label>
									<input id="antisipasi_masalah_pulang_detail" class="auto_blur form-control "  type="text"  value="{antisipasi_masalah_pulang_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>		
						<?
							$arr=explode(',',$bantuan_diperlukan_hal);
						?>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="bantuan_diperlukan_hal">Bantuan Diperlukan Dalam hal</label>
									<select id="bantuan_diperlukan_hal" name="bantuan_diperlukan_hal" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(274) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="bantuan_diperlukan_hal_detail">Detail Bantuan Diperlukan Dalam hal</label>
									<input id="bantuan_diperlukan_hal_detail" class="auto_blur form-control "  type="text"  value="{bantuan_diperlukan_hal_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="adakah_yg_membantu">Adakah Yang Membantu Keperluan Pasien</label>
									<select id="adakah_yg_membantu" name="adakah_yg_membantu" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($adakah_yg_membantu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(275) as $row){?>
										<option value="<?=$row->id?>"  <?=($adakah_yg_membantu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($adakah_yg_membantu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="adakah_yg_membantu_detail">Detail Adakah Yang Membantu Keperluan Pasien</label>
									<input id="adakah_yg_membantu_detail" class="auto_blur form-control "  type="text"  value="{adakah_yg_membantu_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="tinggal_sendiri">Apakah Pasien tinggal sendiri setelah keluar dari Rumah Sakit</label>
									<select id="tinggal_sendiri" name="tinggal_sendiri" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($tinggal_sendiri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(276) as $row){?>
										<option value="<?=$row->id?>"  <?=($tinggal_sendiri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tinggal_sendiri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tinggal_sendiri_detail">Detail Apakah Pasien tinggal sendiri setelah keluar dari Rumah Sakit</label>
									<input id="tinggal_sendiri_detail" class="auto_blur form-control "  type="text"  value="{tinggal_sendiri_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="menggunakan_peralatan_medis">Apakah Pasien menggunakan peralatan medis dirumah setelah keluar dari Rumah Sakit (Cateter, NGT, Oksigen)</label>
									<select id="menggunakan_peralatan_medis" name="menggunakan_peralatan_medis" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($menggunakan_peralatan_medis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(283) as $row){?>
										<option value="<?=$row->id?>"  <?=($menggunakan_peralatan_medis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($menggunakan_peralatan_medis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="menggunakan_peralatan_medis_detail">Detail Apakah Pasien menggunakan peralatan medis dirumah setelah keluar dari Rumah Sakit (Cateter, NGT, Oksigen)</label>
									<input id="menggunakan_peralatan_medis_detail" class="auto_blur form-control "  type="text"  value="{menggunakan_peralatan_medis_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="memerlukan_alat_bantu">Apakah pasien memerlukan Alat Bantu Setelah Keluar RS (Tongkat, Kursi Roda, Walker, dll)</label>
									<select id="memerlukan_alat_bantu" name="memerlukan_alat_bantu" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_alat_bantu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(277) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_alat_bantu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_alat_bantu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_alat_bantu_detail">Detail Apakah pasien memerlukan Alat Bantu Setelah Keluar RS (Tongkat, Kursi Roda, Walker, dll)</label>
									<input id="memerlukan_alat_bantu_detail" class="auto_blur form-control "  type="text"  value="{memerlukan_alat_bantu_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="memerlukan_perawatn_khusus">Apakah Pasien Memerlukan Bantuan / Perawatan Khusus dirumah Setelah Keluar dari Rumah Sakit</label>
									<select id="memerlukan_perawatn_khusus" name="memerlukan_perawatn_khusus" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_perawatn_khusus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(278) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_perawatn_khusus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_perawatn_khusus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_perawatn_khusus_detail">Detail Apakah Pasien Memerlukan Bantuan / Perawatan Khusus dirumah Setelah Keluar dari Rumah Sakit</label>
									<input id="memerlukan_perawatn_khusus_detail" class="auto_blur form-control "  type="text"  value="{memerlukan_perawatn_khusus_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="memerlukan_kebutuhan_pribadi">Apakah pasien Bermasalah Dalam Memenuhi kebutuhan Pribadinya seteleh Keluar dari Rumah Sakit (Makan, minum, BAB, BAK dll)</label>
									<select id="memerlukan_kebutuhan_pribadi" name="memerlukan_kebutuhan_pribadi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_kebutuhan_pribadi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(279) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_kebutuhan_pribadi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_kebutuhan_pribadi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_kebutuhan_pribadi_detail">Detail Apakah pasien Bermasalah Dalam Memenuhi kebutuhan Pribadinya seteleh Keluar dari Rumah Sakit (Makan, minum, BAB, BAK dll)</label>
									<input id="memerlukan_kebutuhan_pribadi_detail" class="auto_blur form-control "  type="text"  value="{memerlukan_kebutuhan_pribadi_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="memiliki_nyeri_kronis">Apakah Pasien Memiliki Nyeri Kkronis dan Kelelahan Setelah Keluar dari Rumah Sakit</label>
									<select id="memiliki_nyeri_kronis" name="memiliki_nyeri_kronis" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memiliki_nyeri_kronis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(280) as $row){?>
										<option value="<?=$row->id?>"  <?=($memiliki_nyeri_kronis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memiliki_nyeri_kronis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memiliki_nyeri_kronis_detail">Detail Apakah Pasien Memiliki Nyeri Kkronis dan Kelelahan Setelah Keluar dari Rumah Sakit</label>
									<input id="memiliki_nyeri_kronis_detail" class="auto_blur form-control "  type="text"  value="{memiliki_nyeri_kronis_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="memerlukan_edukasi">Apakah pasien dan Keluarga memerlukan edukasi kesehatan setelah keluar dari Rumah Sakit ? (Obat-obatan, efek samping obat, nyeri dll)</label>
									<select id="memerlukan_edukasi" name="memerlukan_edukasi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_edukasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(281) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_edukasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_edukasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_edukasi_detail">Detail Apakah pasien dan Keluarga memerlukan edukasi kesehatan setelah keluar dari Rumah Sakit ? (Obat-obatan, efek samping obat, nyeri dll)</label>
									<input id="memerlukan_edukasi_detail" class="auto_blur form-control "  type="text"  value="{memerlukan_edukasi_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="memerlukan_ket_khusus">Apakah Pasien Memerlukan keterampilan khusus  Setelah Keluar dari Rumah Sakit ? (Perawatan Luka, Injeksi, Perawatan Bayi dll)</label>
									<select id="memerlukan_ket_khusus" name="memerlukan_ket_khusus" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($memerlukan_ket_khusus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(282) as $row){?>
										<option value="<?=$row->id?>"  <?=($memerlukan_ket_khusus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($memerlukan_ket_khusus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="memerlukan_ket_khusus_detail">Detail Apakah Pasien Memerlukan keterampilan khusus  Setelah Keluar dari Rumah Sakit ? (Perawatan Luka, Injeksi, Perawatan Bayi dll)</label>
									<input id="memerlukan_ket_khusus_detail" class="auto_blur form-control "  type="text"  value="{memerlukan_ket_khusus_detail}"  placeholder="Detail" required>
								</div>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<label class="text-primary" >CATATAN TAMBAHAN APABILA ADA PERUBAHAN PERENCANAAN PULANG</h5>
							</div>
						</div>						
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<table style="width:100%" class="table table-bordered" id="tabel_catatan">
										<thead>
											<tr>
												<th width="20%">Tanggal & Jam</th>
												<th width="12%">Profesi</th>
												<th width="60%">Catatan</th>
												<th width="8%" clss="text-center">Action</th>
											</tr>
											<tr>
												<th >
													<div class="form-material input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpencatatan" placeholder="HH/BB/TTTT"  value="<?= $tanggaldaftar ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input tabindex="3" type="text" class="time-datepicker form-control " id="waktupencatatan" value="<?= $waktudaftar ?>" required>
													</div>
												</th>
												<th >
													<div class="form-material ">
													<input id="profesi" class="form-control " disabled type="text"  value="<?=get_nama_ref($login_profesi_id,21)?>"  placeholder="Profesi" >
													</div>
												</th>
												<th >
													<div class="form-material ">
													<input id="catatan" class="form-control " type="text"  value=""  placeholder="Catatan" >
													</div>
												</th>
												<th >
													<div class="input-group">
														<button class="btn btn-xs btn-primary" title="Simpan"  type="button" onclick="simpan_catatan()"><i class="fa fa-floppy-o"></i></button>
														<button class="btn btn-xs btn-warning" title="Batalkan Input" type="button" onclick="clear_catatan()"><i class="fa fa-refresh"></i></button>
														<input id="catatan_id" type="hidden"  value="" >
													</th>
												</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
									<h5 class="push-5 text-center text-primary">RIWAYAT {judul_header_eng}</h5>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<input type="hidden" id="pendaftaran_id_ranap_tmp" value="">
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="jenis_ttd" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_ttd==''?'selected':'')?>>Silahkan Pilih</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Pasien Sendiri Sendiri</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?> >Keluarga Terdekat / Wali</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?> >Penanggung Jawab</option>
										
									</select>
									<label for="jenis_ttd">Yang Tandatangan</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							<div class="col-md-6 push-10">
								<div class="form-material">
									<select tabindex="30" id="ttd_hubungan" name="ttd_hubungan" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($ttd_hubungan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<label for="ttd_hubungan">Hubungan</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_ttd()" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	
	$("#tanggal_masuk_rs").datetimepicker({
            format: "DD-MM-YYYY HH:mm",
            // stepping: 30
        });
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_catatan_discarge();
		load_awal_assesmen=false;
	}
});
function edit_catatan(id){
	$("#catatan_id").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/edit_catatan_discarge', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			// $("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
			$("#catatan").val(data.catatan);
			$("#waktupencatatan").val(data.waktupencatatan);
			$("#tglpencatatan").val(data.tglpencatatan);
		}
	});
}
function hapus_catatan(id){
	// $("#catatan_id").val(id);
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Catatan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_catatan_discarge', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus Data.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
							load_catatan_discarge();
					}
				}
			});
		});
		
	// $("#cover-spin").show();
	// $.ajax({
		// url: '{site_url}Tpendaftaran_ranap_erm/hapus_catatan_discarge', 
		// dataType: "JSON",
		// method: "POST",
		// data : {
				// id:id,
				
			// },
		// success: function(data) {
			// $("#cover-spin").hide();
			// // $("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
			// $("#catatan").val(data.catatan);
			// $("#waktupencatatan").val(data.waktupencatatan);
			// $("#tglpencatatan").val(data.tglpencatatan);
		// }
	// });
}
function clear_catatan(){
	$("#catatan").val('');
	$("#catatan_id").val('');
}
function simpan_catatan(){
	let assesmen_id=$("#assesmen_id").val();
	let catatan_id=$("#catatan_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let catatan=$("#catatan").val();
	let tglpencatatan=$("#tglpencatatan").val();
	let waktupencatatan=$("#waktupencatatan").val();
	
	let idpasien=$("#idpasien").val();
	if (catatan==''){
		sweetAlert("Maaf...", "Isi Catatan!", "error");
		return false;

	}
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_catatan_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					catatan_id:catatan_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					catatan:catatan,
					tglpencatatan:tglpencatatan,
					waktupencatatan:waktupencatatan,
				},
			success: function(data) {
				clear_catatan();
				$("#cover-spin").hide();
				if (data==false){
				

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Catatan Berhasil Disimpan'});
					load_catatan_discarge();
				}
			}
		});
	}
	
}
function load_catatan_discarge(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_catatan tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_catatan_discarge', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				st_sedang_edit:$("#st_sedang_edit").val(),
				
				},
		success: function(data) {
			$("#tabel_catatan tbody").append(data.opsi);
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#gambar_id").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_catatan(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Catatan Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/copy_catata_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					assesmen_id:assesmen_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				load_catatan_discarge();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_template_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_template_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
// $(".input-default").blur(function(){
	// simpan_default_warna();	
// });
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
function simpan_assesmen(){
	// alert($("#keluhan_utama").val());
	// return false;
	if (load_awal_assesmen==false){
	if (status_assemen !='2'){
		
	
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		// if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/save_discarge', 
				dataType: "JSON",
				method: "POST",
				data : {
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						
						tanggalmasukrs : $("#tanggalmasukrs").val(),
						waktumasukrs : $("#waktumasukrs").val(),
						rencana_pemulangan : $("#rencana_pemulangan").val(),
						alasan_masuk_rs : $("#alasan_masuk_rs").val(),
						diagnosa_medis : $("#diagnosa_medis").val(),
						pengaruh_ranap_pasien : $("#pengaruh_ranap_pasien").val(),
						pengaruh_ranap_pasien_detail : $("#pengaruh_ranap_pasien_detail").val(),
						pengaruh_pekerjaan : $("#pengaruh_pekerjaan").val(),
						pengaruh_pekerjaan_detail : $("#pengaruh_pekerjaan_detail").val(),
						pengaruh_keuangan : $("#pengaruh_keuangan").val(),
						pengaruh_keuangan_detail : $("#pengaruh_keuangan_detail").val(),
						antisipasi_masalah_pulang : $("#antisipasi_masalah_pulang").val(),
						antisipasi_masalah_pulang_detail : $("#antisipasi_masalah_pulang_detail").val(),
						bantuan_diperlukan_hal : $("#bantuan_diperlukan_hal").val(),
						bantuan_diperlukan_hal_detail : $("#bantuan_diperlukan_hal_detail").val(),
						adakah_yg_membantu : $("#adakah_yg_membantu").val(),
						adakah_yg_membantu_detail : $("#adakah_yg_membantu_detail").val(),
						tinggal_sendiri : $("#tinggal_sendiri").val(),
						tinggal_sendiri_detail : $("#tinggal_sendiri_detail").val(),
						menggunakan_peralatan_medis : $("#menggunakan_peralatan_medis").val(),
						menggunakan_peralatan_medis_detail : $("#menggunakan_peralatan_medis_detail").val(),
						memerlukan_alat_bantu : $("#memerlukan_alat_bantu").val(),
						memerlukan_alat_bantu_detail : $("#memerlukan_alat_bantu_detail").val(),
						memerlukan_perawatn_khusus : $("#memerlukan_perawatn_khusus").val(),
						memerlukan_perawatn_khusus_detail : $("#memerlukan_perawatn_khusus_detail").val(),
						memerlukan_kebutuhan_pribadi : $("#memerlukan_kebutuhan_pribadi").val(),
						memerlukan_kebutuhan_pribadi_detail : $("#memerlukan_kebutuhan_pribadi_detail").val(),
						memiliki_nyeri_kronis : $("#memiliki_nyeri_kronis").val(),
						memiliki_nyeri_kronis_detail : $("#memiliki_nyeri_kronis_detail").val(),
						memerlukan_edukasi : $("#memerlukan_edukasi").val(),
						memerlukan_edukasi_detail : $("#memerlukan_edukasi_detail").val(),
						memerlukan_ket_khusus : $("#memerlukan_ket_khusus").val(),
						memerlukan_ket_khusus_detail : $("#memerlukan_ket_khusus_detail").val(),



					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_index_template_discarge', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/edit_template_discarge', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_history_pengkajian_discarge', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function edit_history_assesmen(id,pendaftaran_id_ranap){
		$("#pendaftaran_id_ranap_tmp").val(pendaftaran_id_ranap);
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let pendaftaran_id_ranap_tmp=$("#pendaftaran_id_ranap_tmp").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/save_edit_discarge', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
						window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id_ranap_tmp+"/erm_ri/input_discarge'); ?>";
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_discarge', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_discarge', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	
</script>