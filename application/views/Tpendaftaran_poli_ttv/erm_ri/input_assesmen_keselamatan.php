<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_assesmen_keselamatan' || $menu_kiri=='input_assesmen_keselamatan_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_assesmen_keselamatan' || $menu_kiri=='input_assesmen_keselamatan_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_keselamatan=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_keselamatan()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="step_wizard" value="<?=$step_wizard?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
						<input type="hidden" id="pendaftaran_id_ranap_ini" value="<?=($st_ranap=='1'?$pendaftaran_id_ranap:$pendaftaran_id)?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_keselamatan=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen_keselamatan" onclick="create_assesmen_keselamatan()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_keselamatan()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen_keselamatan()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_keselamatan" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_keselamatan()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="block">
							<ul class="nav nav-tabs" data-toggle="tabs">
								<li class="<?=($step_wizard=='0'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(0)" >ALL</a>
								</li>
								<li class="<?=($step_wizard=='1'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(1)" ><?=$label_sig_in_ina?></a>
								</li>
								<li class="<?=($step_wizard=='2'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(2)" ><?=$label_time_out_ina?></a>
								</li>
								<li class="<?=($step_wizard=='3'?'active':'')?>">
									<a href="#tab_wizard_0" onclick="set_tab_wizard(3)" ><?=$label_sign_out_ina?></a>
								</li>
								
								
							</ul>
							<div class="block-content tab-content">
								<div class="tab-pane fade fade-left active in" id="tab_wizard_0">
									<div class="row">
										<div class="form-group">
											<div class="col-md-4 div_jenis_1">
												<div class="table-responsive">
													<table class="table table-bordered table-condensed" id="tabel_skrining_1">
														<thead>
															<tr>
																<th colspan="3" width="100%" class="text-center"><strong><span class="h4 text-primary"><?=$label_sig_in_ina?></span></strong></th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="col-md-4 div_jenis_2">
												<div class="table-responsive">
													<table class="table table-bordered table-condensed " id="tabel_skrining_2">
														<thead>
															<tr>
																<th colspan="3" width="100%" class="text-center"><strong><span class="h4 text-primary"><?=$label_time_out_ina?></span></strong></th>
																
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="col-md-4 div_jenis_3">
												<div class="table-responsive">
													<table class="table table-bordered table-condensed " id="tabel_skrining_3">
														<thead>
															<tr>
																<th colspan="3" width="100%" class="text-center"><strong><span class="h4 text-primary"><?=$label_sign_out_ina?></span></strong></th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?}?>
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-right push-20-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_keselamatan=='1'){?>
										<button class="btn btn-primary" id="btn_create_assesmen_keselamatan" onclick="create_assesmen_keselamatan()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										<?if ($assesmen_id!=''){?>
											<?if ($status_assemen=='3'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_keselamatan()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
											<?}?>
											<?if ($status_assemen=='1'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen_keselamatan()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
											<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
											<?}?>
											<?if ($assesmen_id!=''){?>
											<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_keselamatan/<?=$assesmen_id?>" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
											<?}?>
											<?if ($status_assemen=='3'){?>
												<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
											<?}?>
											<?if ($status_assemen=='2'){?>
												<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
												<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_rj/input_assesmen_keselamatan" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
											<?}?>
										<?}?>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?if ($st_ranap=='1'){?>
													<option value="<?=$pendaftaran_id_ranap?>" >Dirawat Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}else{?>
													<option value="<?=$pendaftaran_id?>" >Poliklinik Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>

												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template_keselamatan()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen_keselamatan()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen_keselamatan()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal in" id="modal_paraf" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<input type="hidden" readonly id="ttd_id" name="ttd_id" value="">
								<input type="hidden" readonly id="nama_tabel" name="nama_tabel" value="">
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
						
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<label for="spo2_opr">NAMA</label>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<select id="petugas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="0" selected>- Pilih -</option>
											<?foreach($list_ppa as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
											<?}?>
											
											
										</select>
									</div>
								</div>
								
							</div>
						
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<label for="spo2_opr">TANGGAL</label>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<div class="input-group date">
											<input type="text" class="js-datetimepicker form-control form-control"  id="jam_ttd" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?=date('d-m-Y H:i:s')?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Clear</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_paraf"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
var step_wizard=$("#step_wizard").val();
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function modal_faraf($pendaftaran_id,$nama_tabel){
	$("#ttd_id").val($pendaftaran_id);
	$("#nama_tabel").val($nama_tabel);
	$("#modal_paraf").modal('show');
	// petugas_id
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_paraf')
	});

	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_ttd_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:$pendaftaran_id,
				},
		success: function(data) {
			$("#petugas_id").val(data.petugas_id).trigger('change');
			$("#jam_ttd").val(data.date_isi);
			if (data.jawaban_ttd){
				$("#signature64").val(data.jawaban_ttd);
				$('#sig').signature('enable').signature('draw', data.jawaban_ttd);
			}else{
				sig.signature('clear');
			}
			$("#cover-spin").hide();
		}
	});
}
function hapus_paraf($pendaftaran_id,$nama_tabel){
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/hapus_paraf_keselematan/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				id:$pendaftaran_id,
				nama_tabel:$nama_tabel,
		  },success: function(data) {
				load_skrining_keselamatan();
				$("#cover-spin").hide();
			}
		});
}

function set_tab_wizard($id){
	step_wizard=$id;
	$(".div_jenis_1").removeClass("col-md-4 col-md-12");
	$(".div_jenis_2").removeClass("col-md-4 col-md-12");
	$(".div_jenis_3").removeClass("col-md-4 col-md-12");
	$(".div_jenis_1").hide();
	$(".div_jenis_2").hide();
	$(".div_jenis_3").hide();
	if (step_wizard=='0'){
		$(".div_jenis_1").show();
		$(".div_jenis_2").show();
		$(".div_jenis_3").show();
		$(".div_jenis_1").addClass("col-md-4");
		$(".div_jenis_2").addClass("col-md-4");
		$(".div_jenis_3").addClass("col-md-4");
	}
	if (step_wizard=='1'){
		$(".div_jenis_1").show();
		$(".div_jenis_1").addClass("col-md-12");
	}
	if (step_wizard=='2'){
		$(".div_jenis_2").show();
		$(".div_jenis_2").addClass("col-md-12");
	}
	if (step_wizard=='3'){
		$(".div_jenis_3").show();
		$(".div_jenis_3").addClass("col-md-12");
	}
	var assesmen_id=$("#assesmen_id").val();
	$("#step_wizard").val($id);
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/set_tab_wizard_keselamatan/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			assesmen_id:assesmen_id,
			step_wizard:$("#step_wizard").val(),
	  },success: function(data) {
		  // $.toaster({priority : 'success', title : 'Succes!', message : ' Wizard'});
		  
		}
	});
}

$(document).ready(function() {
	// alert(status_assemen);
	$("#jam_ttd").datetimepicker({
		format: "DD-MM-YYYY HH:mm",
		// stepping: 30
	});
	disabel_edit();
	set_tab_wizard(step_wizard);
	// set_ttd_assesmen();
	// $(".js-datetimepicker").datetimepicker({
		// format: "dd-mm-yyyy HH:mm"
	// });
	$('.number').number(true, 0);
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_keselamatan();
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian()
	}
	
	// load_data_rencana_asuhan_keselamatan(1);
});
function load_skrining_keselamatan(){
	let assesmen_id=$("#assesmen_id").val();
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				jenis_header:1,
				},
		success: function(data) {
			$("#tabel_skrining_1 tbody").empty();
			$("#tabel_skrining_1 tbody").append(data.tabel);
			$(".js-select2").select2();
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				jenis_header:2,
				},
		success: function(data) {
			$("#tabel_skrining_2 tbody").empty();
			$("#tabel_skrining_2 tbody").append(data.tabel);
			$(".js-select2").select2();
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				jenis_header:3,
				},
		success: function(data) {
			$("#tabel_skrining_3 tbody").empty();
			$("#tabel_skrining_3 tbody").append(data.tabel);
			$(".js-select2").select2();
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
}
function simpan_assesmen(){
// alert(load_awal_assesmen);
if (status_assemen !='2'){
	
	if (load_awal_assesmen==false){
	let assesmen_id=$("#assesmen_id").val();
	// alert($("#tanggal_keluar").val());return false;
	let tanggaldatang=$("#tanggaldatang").val();
	let waktudatang=$("#waktudatang").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	if (assesmen_id){
		console.log('SIMPAN');
		
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_assesmen_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					// nama_template:nama_template,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					st_edited:$("#st_edited").val(),
					assesmen_id:$("#assesmen_id").val(),
					nama_template:nama_template,
					status_assemen:$("#status_assemen").val(),
					
				},
			success: function(data) {
				
						console.log(data);
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					if (data.status_assemen=='1'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}else{
						if (data.status_assemen=='3'){
							
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
						}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
							$("#cover-spin").show();
							location.reload();			
						}
						
					}
				}
			}
		});
		}
	 }
	}
}
function list_history_pengkajian(){
	let assesmen_id=$("#assesmen_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let mppa_id=$("#mppa_id").val();
	let iddokter=$("#iddokter").val();
	let idrawat_ranap=$("#idrawat_ranap").val();
	let notransaksi=$("#notransaksi").val();
	let tanggal_input_1=$("#tanggal_input_1").val();
	let tanggal_input_2=$("#tanggal_input_2").val();
	let idtipe=$("#idtipe").val();
	let idruang=$("#idruang").val();
	let idbed=$("#idbed").val();
	let idkelas=$("#idkelas").val();
	let st_ranap=$("#st_ranap").val();
	$('#index_history_kajian').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_history_kajian').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					{  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,5,6,7] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [1,2,3,5] },
					 { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_keselamatan', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						iddokter:iddokter,
						mppa_id:mppa_id,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						idrawat_ranap:idrawat_ranap,
						idtipe:idtipe,
						idruang:idruang,
						idbed:idbed,
						idkelas:idkelas,
						st_ranap:st_ranap,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
function create_assesmen_keselamatan(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_assesmen_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $(".div_edit_ttd").hide();
		 $(".btn_back").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');

}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_keselamatan();
}
function create_with_template_keselamatan(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_keselamatan(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let st_ranap=$("#st_ranap").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_template_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					st_ranap:st_ranap,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen_keselamatan(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();

	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_assesmen_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_keselamatan(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_template_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$(".auto_blur").blur(function(){
	// console.log('BLUR')
	// if ($("#st_edited").val()=='0'){
		// $(this).removeClass('input_edited');
		simpan_assesmen();
	// }else{
		// if ($("#st_edited").val()=='1'){
			// if ($(this).val()!=before_edit){
				// console.log('Ada Peruabahan');
				// // $(this).attr('input_edited');
				 // $(this).addClass('input_edited');
			// }
		// }
	// }
	
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menyimpan Inputan Assesmen?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#status_assemen").val(2);
		nama_template='';
		// simpan_assesmen_ttv();
		// simpan_edukasi_keselamatan();
		// simpan_riwayat_penyakit_keselamatan();
		simpan_assesmen();
	});		

}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		

}


$(document).on('change','#template_assesmen_id',function(){
	if ($(this).val()!=''){
		$("#btn_create_with_template_keselamatan").removeAttr('disabled');
	}else{
		$("#btn_create_with_template_keselamatan").attr('disabled','disabled');
	}
});
function list_index_template_keselamatan(){
	let assesmen_id=$("#assesmen_id").val();
	$('#index_template_assemen').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_template_assemen').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					{  className: "text-right", targets:[0] },
					 { "width": "5%", "targets": [0] },
					 { "width": "20%", "targets": [1] },
					 { "width": "15%", "targets": [3] },
					 { "width": "60%", "targets": [2] }
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/list_index_template_keselamatan', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}

function edit_template_assesmen(assesmen_id){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let idpasien=$("#idpasien").val();
	// alert(pendaftaran_id_ranap);
	// return false;
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Edit Template?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_template_assesmen_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					
				},
			success: function(data) {
				
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
					$("#cover-spin").show();
					location.reload();	
				}
			}
		});
	});			
	
	
}

function edit_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_edit").modal('show');
	document.getElementById("modal_edit").style.zIndex = "1201";
	$("#alasan_id_edit").select2({
		dropdownParent: $("#modal_edit")
	  });
}
function hapus_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_hapus").modal('show');
	document.getElementById("modal_hapus").style.zIndex = "1201";
	$("#alasan_id").select2({
		dropdownParent: $("#modal_hapus")
	  });
	// document.getElementById("alasan_id").style.zIndex = "1202";
}
function save_edit_assesmen_keselamatan(){
	let jml_edit=$("#jml_edit").val();
	let keterangan_edit=$("#keterangan_edit").val();
	let alasan_id=$("#alasan_id_edit").val();
	let assesmen_detail_id=$("#assesmen_detail_id").val();
	// alert(assesmen_detail_id);return false;
	$("#st_sedang_edit").val(1);
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_edit").modal('hide');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/save_edit_assesmen_keselamatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				jml_edit:jml_edit,
				assesmen_id:assesmen_detail_id,
				alasan_id:alasan_id,
				keterangan_edit:keterangan_edit,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Gagal!",
					text: "Edit.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				$("#cover-spin").show();
				let st_ranap=data.st_ranap;
				let pendaftaran_id=data.pendaftaran_id;
				$("#cover-spin").show();
				if (st_ranap=='1'){
				window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_ri/input_assesmen_keselamatan'); ?>";
					
				}else{
				window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_rj/input_assesmen_keselamatan_rj'); ?>";
					
				}
			}
		}
	});
}
function hapus_record_assesmen_keselamatan(){
	let id=$("#assesmen_detail_id").val();
	let keterangan_hapus=$("#keterangan_hapus").val();
	let alasan_id=$("#alasan_id").val();
	
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_hapus").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Inputan Assesmen ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					keterangan_hapus:keterangan_hapus,
					alasan_id:alasan_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					list_history_pengkajian();
					$("#modal_hapus").modal('hide');
				}
			}
		});
	});

}
function hapus_template_assesmen(id){

	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Template Assesmen ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_keselamatan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					keterangan_hapus:'HAPUS TEMPLATE',
					alasan_id:null,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				list_index_template_keselamatan();
			}
		});
	});

}
$(document).on("change",".nilai_hk",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_keselamatan/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				approval_id:approval_id,nilai_id:nilai_id,
		  },success: function(data) {
				// validate_lm();
			}
		});
	
});
$(document).on("blur",".nilai_hk_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	// alert(approval_id);
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_keselamatan_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			// validate_hk();
		}
	});
});
$(document).on("click","#btn_save_paraf",function(){
	var pendaftaran_id=$("#ttd_id").val();
	var signature64=$("#signature64").val();
	var nama_tabel=$("#nama_tabel").val();
	var petugas_id=$("#petugas_id").val();
	var jam_ttd=$("#jam_ttd").val();
	$nama_tabel=$("#nama_tabel").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/save_ttd_keselamatan/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,
				signature64:signature64,
				nama_tabel:nama_tabel,
				petugas_id:petugas_id,
				jam_ttd:jam_ttd,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				load_skrining_keselamatan();
				$("#cover-spin").hide();
			}
		});
	
});
</script>