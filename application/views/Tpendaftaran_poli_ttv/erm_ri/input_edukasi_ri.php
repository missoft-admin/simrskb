<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	table.dataTable tbody td {
	  vertical-align: top;
	}
</style>
<style>
	
	#sig_ttd_petugas canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_sasaran canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_edukasi_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_edukasi_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
	
		// echo $tanggal_pernyataan;
					if ($assesmen_id){
						
						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
					}
					
					$disabel_input='';
					if ($st_input_edukasi=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_edukasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1" onclick="set_tab(1)"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<input class="form-control input-sm"  type="hidden" readonly id="edukasi_detail_id" value="" />
			<textarea id="signature64_ttd_sasaran" name="signed_ttd_sasaran" style="display: none"></textarea>
			<textarea id="signature64_ttd_petugas" name="signed_ttd_petugas" style="display: none"></textarea>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="tipe_rj_ri" value="3" >		
						<input type="hidden" id="st_sedang_edit" value="" >		
						<div class="col-md-12">
							<?if($st_lihat_edukasi=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_edukasi=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_edukasi_ri" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Pengkajian</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label class="text-primary">IDENTIFIKASI EDUKASI</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-12">
										<label for="example-input-normal">Bahasa</label>
										<select tabindex="8" id="bahasa_array" name="bahasa_array[]" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
											<?foreach(list_variable_ref(81) as $row){?>
											<option value="<?=$row->id?>" <?=(in_array($row->id, $bahasa_array)?'selected':'')?> ><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="example-input-normal">Dibutuhkan Penerjemaah ?</label>
										<select tabindex="8" id="st_penerjemaah" name="st_penerjemaah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($st_penerjemaah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(38) as $row){?>
											<option value="<?=$row->id?>" <?=($st_penerjemaah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_penerjemaah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">Jika Ya, Sebutkan</label>
										<input class="form-control auto_blur" type="text" id="penerjemaah"  value="{penerjemaah}" placeholder="Kebutuhan Penterjemaah" >
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-6">
										<label for="example-input-normal">Pendidikan</label>
										<select tabindex="8" id="pendidikan" name="pendidikan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
											<?foreach(list_variable_ref(13) as $row){?>
											<option value="<?=$row->id?>" <?=($pendidikan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">Baca dan Tulis</label>
										<select tabindex="8" id="baca_tulis" name="baca_tulis" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($baca_tulis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(82) as $row){?>
											<option value="<?=$row->id?>" <?=($baca_tulis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($baca_tulis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-4 ">
										<label for="example-input-normal">Pilih Cara Edukasi</label>
										<select tabindex="8" id="cara_edukasi" name="cara_edukasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($cara_edukasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(83) as $row){?>
											<option value="<?=$row->id?>" <?=($cara_edukasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($cara_edukasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-8 ">
										<label for="example-input-normal">Kesediaan Pasien / Keluarga untuk menerima informasi yang diberikan</label>
										<select tabindex="8" id="st_menerima_info" name="st_menerima_info" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($st_menerima_info == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(39) as $row){?>
											<option value="<?=$row->id?>" <?=($st_menerima_info == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_menerima_info == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-12">
										<label for="example-input-normal">Hambatan Edukasi Edukasi</label>
										<select tabindex="8" id="hambatan_edukasi_array" name="hambatan_edukasi_array[]" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
											<?foreach(list_variable_ref(40) as $row){?>
											<option value="<?=$row->id?>" <?=(in_array($row->id, $hambatan_edukasi_array)?'selected':'')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12">
										<label for="example-input-normal">Identifikasi Kebutuhan Edukasi</label>
										<select tabindex="8" id="medukasi_array" name="medukasi_array[]" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
											<?foreach(get_all('medukasi',array('staktif'=>1)) as $row){?>
											<option value="<?=$row->id?>" <?=(in_array($row->id, $medukasi_array)?'selected':'')?>><?=$row->judul?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
								<?if($status_assemen!='2'){?>
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered" id="index_header" >
												<thead>
													<tr>
														<th width="15%" class="text-center">Tanggal Dan Jam</th>
														<th width="40%" class="text-center">
															Penjelasan Materi Edukasi
															<div class="input-group">
																<select tabindex="8" id="medukasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Master Edukasi">
																	<option value="" selected>Terapkan Dari Master Edukasi</option>
																	<?foreach(get_all('medukasi',array('staktif'=>1)) as $row){?>
																		<option value="<?=$row->id?>" ><?=$row->judul?></option>
																	<?}?>
																</select>
																<span class="input-group-btn">
																	<button class="btn btn-default" onclick="terapkan_edukasi()" type="button">Terapkan</button>
																</span>
															</div>
															
														</th>
														<th width="10%" class="text-center">Tanda Tangan<br>Petugas & Profesi</th>
														<th width="10%" class="text-center">Tanda Tangan<br>Sasaran Edukasi</th>
														<th width="15%" class="text-center">Evaluasi</th>
														<th width="10%" class="text-center">Action</th>
													</tr>
													<tr>
														<td width="15%" class="text-center" style="vertical-align: top;">
																
															<div class="input-group date">
																<input class="js-datetimepicker form-control input-sm" type="text" id="input_tanggal" name="input_tanggal" value="<?=date('d-m-Y H:i:s')?>" />
																<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
															</div>
														</td>
														<td width="40%" class="text-left" style="vertical-align: top;">
															<textarea class="js-summernote form-control" id="materi_edukasi"  rows="3" placeholder="Isi keterangan"></textarea>
														</td>
														<td width="10%" class="text-center" style="vertical-align: top;">
															<div class="img-container fx-img-rotate-r ">
																<img class="img-responsive" id="ttd_view_petugas" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$created_ppa?>" alt="">
																<div class="text-muted text-center"><?=$ttd_petugas_nama?></div>
															
															</div>
														</td>
														<td width="10%" class="text-center" style="vertical-align: top;"></td>
														<td width="15%" class="text-left" style="vertical-align: top;">
															<select tabindex="8" id="evaluasi" name="evaluasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
																<option value="#" selected>Pilih Opsi</option>
																<?foreach(list_variable_ref(84) as $row){?>
																<option value="<?=$row->id?>"><?=$row->nama?></option>
																<?}?>
															</select>
														</td>
														<td width="10%" class="text-center" style="vertical-align: top;">
															<span class="input-group-btn">
																<button class="btn btn-info" onclick="simpan_materi_edukasi()" type="button"><i class="fa fa-plus"></i> Add</button>
																<button class="btn btn-warning" onclick="clear_edukasi()" type="button"><i class="fa fa-refresh"></i></button>
															</span>
														</td>
													</tr>
													
												</thead>
												<tbody></tbody>
											</table>
										</div>
								</div>
								<?}?>
								<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered" id="tabel_materi">
												<thead>
													<tr>
														<th width="15%" class="text-center">Tanggal Dan Jam</th>
														<th width="40%" class="text-center">
															Penjelasan Materi Edukasi
															
														</th>
														<th width="10%" class="text-center">Tanda Tangan<br>Petugas & Profesi</th>
														<th width="10%" class="text-center">Tanda Tangan<br>Sasaran Edukasi</th>
														<th width="15%" class="text-center">Evaluasi</th>
														<th width="10%" class="text-center">Action</th>
													</tr>
												
												</thead>
												<tbody></tbody>
											</table>
										</div>
								</div>
							</div>
						</div>
							
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="notransaksi">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tgl_daftar">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="mppa_id">Nama Petugas Profesi</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>'1')) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
									
								</div>
								
								<div class="col-md-6">
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="iddokter">Owned By Me</label>
										<div class="col-md-8">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>ALL</option>
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal_input_1">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												
												<th width="100%"></th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_edukasi!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="20%">Action</th>
												<th width="70%">Template</th>
												<th width="10%">Jumlah</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_ttd_petugas" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title"><?=$ttd_petugas_judul_ina.' / <i class="text-muted">'.$ttd_petugas_judul_eng.'</i>'?></h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ttd_petugas_ket_gambar_ina} / <i class="text-muted">{ttd_petugas_ket_gambar_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_petugas" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{ttd_petugas_nama_ina} / <i class="text-muted">{ttd_petugas_nama_eng}</i></label>
										<input tabindex="32" type="text" class="form-control " id="nama_petugas" value="">
									</div>
								</div>
								
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_petugas()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_petugas()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_sasaran" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title"><?=$ttd_sasaran_judul_ina.' / <i class="text-muted">'.$ttd_sasaran_judul_eng.'</i>'?></h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ttd_sasaran_ket_gambar_ina} / <i class="text-muted">{ttd_sasaran_ket_gambar_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_sasaran" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{ttd_sasaran_nama_ina} / <i class="text-muted">{ttd_sasaran_nama_eng}</i></label>
										<input tabindex="32" type="text" class="form-control " id="nama_sasaran" value="">
									</div>
								</div>
								
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_sasaran()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_sasaran()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var tab_active=1;
var sig_ttd_petugas = $('#sig_ttd_petugas').signature({syncField: '#signature64_ttd_petugas', syncFormat: 'PNG'});
var sig_ttd_sasaran = $('#sig_ttd_sasaran').signature({syncField: '#signature64_ttd_sasaran', syncFormat: 'PNG'});
	function terapkan_edukasi(){
		let medukasi_id=$("#medukasi_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/terapkan_edukasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					medukasi_id:medukasi_id,
				
				   },
			success: function(data) {
				$("#materi_edukasi").summernote("code", data);
				$("#cover-spin").hide();
				
			}
		});
	}
	function set_tab(tab){
		tab_active=tab;
	}
	function create_awal_template(){
		$("#modal_default_template").modal('show');
		$("#mnyeri_id_2").select2({
			dropdownParent: $("#modal_default_template")
		  });
	}
	
	$(document).ready(function() {
		$("#input_tanggal").datetimepicker({
			format: "DD-MM-YYYY HH:mm",
			// stepping: 30
		});
		$('.js-summernote').summernote({
			  height: 150,   //set editable area's height
			  
			  codemirror: { // codemirror options
				theme: 'monokai'
			  }	
			})
		
		disabel_edit();
		// set_ttd_assesmen();
		$(".time-datepicker").datetimepicker({
			format: "HH:mm:ss"
		});
		// $("#tabel_rencana_asuhan tbody").empty();
		let assesmen_id=$("#assesmen_id").val();
		
			if (assesmen_id){
				load_index_materi();
			}
		load_awal_assesmen=false;
		

	});

	
	function disabel_edit(){
		if (status_assemen=='2'){
			 $("#form1 :input").prop("disabled", true);
			 // $(".btn_rencana").removeAttr('disabled');
			 // $(".btn_ttd").removeAttr('disabled');
		}
	}
	function simpan_template(){
		$("#modal_savae_template_assesmen").modal('show');
		
	}
	function create_assesmen(){
		
		$("#modal_default").modal('hide');
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let tipe_rj_ri=$("#tipe_rj_ri").val();
		// let template_id=$("#template_id").val();
		let idpasien=$("#idpasien").val();
		
		let template='Baru';
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/create_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						idpasien:idpasien,
						tipe_rj_ri:tipe_rj_ri,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	function gunakan_template_assesmen(id){
		$("#template_assesmen_id").val(id).trigger('change');
		create_with_template();
	}
	function create_with_template(){
		let template_assesmen_id=$("#template_assesmen_id").val();
		// let template_assesmen_id=id;
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen Dari template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						template_assesmen_id:template_assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	function copy_history_assesmen(id){
		let template_assesmen_id=id;
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen Dari Duplikasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						template_assesmen_id:template_assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	function create_template(){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let idpasien=$("#idpasien").val();
		// alert(idpasien);
		// return false;
		let template='Baru';
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Template "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/create_template_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id:pendaftaran_id,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						idpasien:idpasien,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	$(".opsi_change").change(function(){
		// console.log('OPSI CHANTE')
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});

	

	$(".auto_blur_tanggal").change(function(){
		console.log($("#waktupernyataan").val())
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});
	function batal_assesmen(){
		let assesmen_id=$("#assesmen_id").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		
		let template=$("#template_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Assesment ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/batal_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						st_edited:st_edited,
						jml_edit:jml_edit,
					
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
					location.reload();
				}
			});
		});

	}
	function batal_template(){
		let assesmen_id=$("#assesmen_id").val();
		let nama_template=$("#nama_template").val();
		
		let template=$("#template_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Assesment ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/batal_template_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						nama_template:nama_template,
					
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
					location.reload();
				}
			});
		});

	}
	

	$(".auto_blur").blur(function(){
		// alert('sini');
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
		
	});
	$("#penerima_info").blur(function(){
		$("#penerima_info_paraf").val($("#penerima_info").val());
		$("#nama_kel_penerima_info").val($("#penerima_info").val());
		
	});
	$("#nama").blur(function(){
		$("#nama_pemberi_pernyataan").html($("#nama").val());
		
	});

	$(".auto_blur").focus(function(){
		before_edit=$(this).val();
		console.log(before_edit);
	});
	function close_assesmen(){
		swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menyimpan Inputan Assesmen?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$("#cover-spin").show();
				$("#status_assemen").val(2);
				
				simpan_assesmen();
			});		
		
	}
	function close_template(){
		if ($("#nama_template").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Template", "error");
			return false;
		}
		$("#modal_savae_template_assesmen").modal('hide');
		swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menyimpan Template ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$("#status_assemen").val(4);
				nama_template=$("#nama_template").val();
				simpan_assesmen();
			});		
		
	}
	function simpan_assesmen(){
		if (load_awal_assesmen==false){
			if (status_assemen !='2'){
			let assesmen_id=$("#assesmen_id").val();
			// alert(assesmen_id);return false;
			let tglpendaftaran=$("#tglpendaftaran").val();
			let waktupendaftaran=$("#waktupendaftaran").val();
			let tanggalinformasi=$("#tanggalinfo").val();
			let waktuinformasi=$("#waktuinfo").val();
			let tanggalpernyataan=$("#tanggalpernyataan").val();
			let waktupernyataan=$("#waktupernyataan").val();
			
			let st_edited=$("#st_edited").val();
			let jml_edit=$("#jml_edit").val();
			if (assesmen_id){
				// console.log('SIMPAN');
				
				$.ajax({
					url: '{site_url}Tpendaftaran_poli_ttv/save_edukasi', 
					dataType: "JSON",
					method: "POST",
					data : {
							tglpendaftaran:tglpendaftaran,
							waktupendaftaran:waktupendaftaran,
							assesmen_id:$("#assesmen_id").val(),
							nama_template:nama_template,
							status_assemen:$("#status_assemen").val(),
							bahasa_array: $("#bahasa_array").val(),
							st_penerjemaah: $("#st_penerjemaah").val(),
							penerjemaah: $("#penerjemaah").val(),
							pendidikan: $("#pendidikan").val(),
							baca_tulis: $("#baca_tulis").val(),
							cara_edukasi: $("#cara_edukasi").val(),
							st_menerima_info: $("#st_menerima_info").val(),
							hambatan_edukasi_array: $("#hambatan_edukasi_array").val(),
							medukasi_array: $("#medukasi_array").val(),
							st_edited:$("#st_edited").val(),
							jml_edit:$("#jml_edit").val(),
						},
					success: function(data) {
						
								console.log(data);
						if (data==null){
							swal({
								title: "Gagal!",
								text: "Simpan Assesmen.",
								type: "error",
								timer: 1500,
								showConfirmButton: false
							});

						}else{
							if (data.status_assemen=='1'){
								$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
							}else{
								if (data.status_assemen=='3'){
									
								$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
								}else{
									$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
									$("#cover-spin").show();
									// alert('sini');
									location.reload();			
								}
								
							}
						}
					}
				});
			}
			}
		}
	}
	
	
	
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		tab_active=3;
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_index_template_edukasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 // $("#index_template_assemen thead").remove();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/edit_template_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		tab_active=2;
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let profesi_id_filter=$("#profesi_id_filter").val();
		let st_verifikasi=$("#st_verifikasi").val();
		let st_owned=$("#st_owned").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let tipe_rj_ri=$("#tipe_rj_ri").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						// {  className: "text-center", targets:[1,2,3,5,6,7] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "10%", "targets": [1,2,3,5] },
						 // { "width": "15%", "targets": [4] },
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_history_pengkajian_edukasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							st_owned:st_owned,
							// st_verifikasi:st_verifikasi,
							// pemberi_info:profesi_id_filter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							tipe_rj_ri:tipe_rj_ri,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 $("#index_history_kajian thead").remove();
				 }  
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let materi_edukasi=$("#materi_edukasi").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		// $("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_edit_edukasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					materi_edukasi:materi_edukasi,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
						location.reload();
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		$("#modal_hapus").modal('hide');
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	function hapus_materi_edukasi(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Materi Edukasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_materi_edukasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					load_index_materi();
				}
			});
		});

	}
	function clear_edukasi(){
		$("#edukasi_detail_id").val('');
		$("#evaluasi").val('#').trigger('change');
		$("#materi_edukasi").summernote("code", '');
	}
	function simpan_materi_edukasi(){
		if (load_awal_assesmen==false){
			if (status_assemen !='2'){
			let assesmen_id=$("#assesmen_id").val();
			// alert(assesmen_id);return false;
			let input_tanggal=$("#input_tanggal").val();
			let materi_edukasi=$("#materi_edukasi").val();
			let evaluasi=$("#evaluasi").val();
			let edukasi_detail_id=$("#edukasi_detail_id").val();
			if (evaluasi=='#'){
				sweetAlert("Maaf...", "Tentukan Evaluasi", "error");
				return false;
			}
			

			if (assesmen_id){
				// console.log('SIMPAN');
				
				$.ajax({
					url: '{site_url}Tpendaftaran_poli_ttv/simpan_materi_edukasi', 
					dataType: "JSON",
					method: "POST",
					data : {
							assesmen_id:$("#assesmen_id").val(),
							input_tanggal:input_tanggal,
							materi_edukasi:materi_edukasi,
							evaluasi:evaluasi,
							edukasi_detail_id:edukasi_detail_id,
							
						},
					success: function(data) {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Save Edukasi'});
						load_index_materi();
						clear_edukasi();
					}
				});
			}
			}
		}
	}
	
	function load_index_materi(){
		$("#cover-spin").show();
		var assesmen_id=$("#assesmen_id").val();
		var status_assemen=$("#status_assemen").val();
		var tipe_rj_ri=$("#tipe_rj_ri").val();
		
		// alert(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/load_index_materi_edukasi/',
			dataType: "json",
			type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					status_assemen:status_assemen,
					tipe_rj_ri:tipe_rj_ri,
					
					
			  },
			success: function(data) {
				$("#tabel_materi tbody").empty();
				$("#tabel_materi tbody").append(data.tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// $('.auto_blur_tabel').summernote({
					// height: 100,
					// codemirror: { // codemirror options
						// theme: 'monokai'
					  // },	
				  // callbacks: {
					// onBlur: function(contents, $editable) {
							// assesmen_id=$("#assesmen_id").val();
							// var tr=$(this).closest('tr');
							// var informasi_id=tr.find(".informasi_id").val();
							// var isi=$(this).val();
							// $.ajax({
								// url: '{site_url}Tpendaftaran_poli_ttv/update_isi_informasi_edukasi/',
								// dataType: "json",
								// type: 'POST',
								// data: {
								// informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
								// },success: function(data) {

								// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
								// }
							// });
					// }
				  // }
				// });
					
			}
		});
	}
	function edit_materi_edukasi(id){
		$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/edit_materi_edukasi/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				id:id,
		  },success: function(data) {
				// alert(data.input_tanggal);
				$("#input_tanggal").val(data.input_tanggal);
				$("#evaluasi").val(data.evaluasi).trigger('change');
				$("#edukasi_detail_id").val(data.id);
				$("#nama_petugas").val(data.ttd_petugas_nama);
				$("#materi_edukasi").summernote("code", data.materi_edukasi);
				$("#signature64_ttd_petugas").val(data.ttd_petugas);
				// $("#modal_ttd_petugas").modal('show');
				$('#sig_ttd_petugas').signature('enable').signature('draw', data.ttd_petugas);
		  }
		});
	}
	function modal_ttd_petugas(id){
		$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/get_ttd_petugas_edukasi/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				id:id,
		  },success: function(data) {
				$("#edukasi_detail_id").val(data.id);
				$("#nama_petugas").val(data.ttd_petugas_nama);
				$("#signature64_ttd_petugas").val(data.ttd_petugas);
				$("#modal_ttd_petugas").modal('show');
				$('#sig_ttd_petugas').signature('enable').signature('draw', data.ttd_petugas);
		  }
		});
		
	}
	function clear_ttd_petugas(){
		sig_ttd_petugas.signature('clear');
		$("#signature64_ttd_petugas").val('');
	}
	function simpan_ttd_petugas(){
		let id=$("#edukasi_detail_id").val();
		let ttd_petugas=$("#signature64_ttd_petugas").val();
		let ttd_petugas_nama=$("#nama_petugas").val();
				$("#modal_ttd_petugas").modal('hide');
		if (ttd_petugas==''){
				swal({
					title: "Gagal!",
					text: "Tentukan Tanda Tangan Petugas.",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				$("#modal_ttd_petugas").modal('show');
				return false;
		}
		
		if (ttd_petugas_nama==''){
				swal({
					title: "Gagal!",
					text: "Tentukan Nama Tanda Tangan Petugas.",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				return false;
		}
		// ttd_view_petugas
		$("#ttd_view_petugas").attr("src", ttd_petugas);
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_petugas_edukasi/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_petugas:ttd_petugas,
					ttd_petugas_nama:ttd_petugas_nama,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Tanda Tangan'});
				  $("#cover-spin").hide();
					if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					$("#modal_ttd_petugas").modal('hide');
				}
			});
	}
	function hapus_ttd_petugas(id){
		var ttd_petugas='';
		let ttd_petugas_nama=$("#nama_petugas").val();
		
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_petugas_edukasi/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_petugas:ttd_petugas,
					ttd_petugas_nama:ttd_petugas_nama,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
				  $("#cover-spin").hide();
					if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					$("#modal_ttd_petugas").modal('hide');
				}
			});
	}
	
	function modal_ttd_sasaran(id){
		$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/get_ttd_sasaran_edukasi/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				id:id,
		  },success: function(data) {
				$("#edukasi_detail_id").val(data.id);
				$("#nama_sasaran").val(data.ttd_sasaran_nama);
				$("#signature64_ttd_sasaran").val(data.ttd_sasaran);
				$("#modal_ttd_sasaran").modal('show');
				$('#sig_ttd_sasaran').signature('enable').signature('draw', data.ttd_sasaran);
		  }
		});
		
	}
	function clear_ttd_sasaran(){
		sig_ttd_sasaran.signature('clear');
		$("#signature64_ttd_sasaran").val('');
	}
	function simpan_ttd_sasaran(){
		let id=$("#edukasi_detail_id").val();
		let ttd_sasaran=$("#signature64_ttd_sasaran").val();
		let ttd_sasaran_nama=$("#nama_sasaran").val();
				$("#modal_ttd_sasaran").modal('hide');
		if (ttd_sasaran==''){
				swal({
					title: "Gagal!",
					text: "Tentukan Tanda Tangan Petugas.",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				$("#modal_ttd_sasaran").modal('show');
				return false;
		}
		
		if (ttd_sasaran_nama==''){
				swal({
					title: "Gagal!",
					text: "Tentukan Nama Tanda Tangan Petugas.",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				return false;
		}
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_sasaran_edukasi/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_sasaran:ttd_sasaran,
					ttd_sasaran_nama:ttd_sasaran_nama,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Tanda Tangan'});
				  $("#cover-spin").hide();
				    if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					$("#modal_ttd_sasaran").modal('hide');
				}
			});
	}
	function hapus_ttd_sasaran(id){
		var ttd_sasaran='';
		let ttd_sasaran_nama=$("#nama_sasaran").val();
		
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_sasaran_edukasi/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_sasaran:ttd_sasaran,
					ttd_sasaran_nama:ttd_sasaran_nama,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
				  $("#cover-spin").hide();
					if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					$("#modal_ttd_sasaran").modal('hide');
				}
			});
	}
</script>