<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_gizi_anak'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_gizi_anak' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_gizi_anak=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_gizi_anak=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_gizi_anak=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
									<h5 class="font-w700 push-5 text-center text-primary"><i>RIWAYAT {judul_header_eng}</i></h5>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_gizi_anak" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-5 ">
								<div class="col-md-6">
									<label for="tanggal_pemeriksaan">Tanggal Pemeriksaan</label>
									<div class="input-group date">
										<input id="tanggal_pemeriksaan"  class="<?=($tanggal_pemeriksaan!=$tanggal_pemeriksaan_asal?'edited':'')?> auto_blur js-datepicker form-control " type="text" data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" name="tanggal_pemeriksaan" value="<?= $tanggal_pemeriksaan ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="berat_badan">Berat Badan / Panjang Badan</label>
									<div class="input-group">
										<input id="berat_badan" class="<?=($berat_badan!=$berat_badan_asal?'edited':'')?> auto_blur form-control decimal"  type="text"  value="{berat_badan}"  placeholder="BB Saat Ini" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
								
							</div>
							<div class="col-md-7 ">
								<div class="col-md-4">
									<label for="tinggi_badan">Umur</label>
									<div class="input-group">
										<input class="form-control" readonly type="text"  value="{umurtahun}"  placeholder="" >
										<span class="input-group-addon">Tahun</span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="tinggi_badan">&nbsp;</label>
									<div class="input-group">
										<input class="form-control" readonly type="text"  value="{umurbulan}"  placeholder="" >
										<span class="input-group-addon">Bulan</span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="tinggi_badan">&nbsp;</label>
									<div class="input-group">
										<input class="form-control" readonly type="text"  value="{umurhari}"  placeholder="" >
										<span class="input-group-addon">Hari</span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-5 ">
								<div class="col-md-6">
									<label for="bmi">BMI</label>
									<input id="bmi" class="<?=($bmi!=$bmi_asal?'edited':'')?> auto_blur form-control decimal"  type="text"  value="{bmi}"  placeholder="" required>
								</div>
								<div class="col-md-6">
									<label for="bb_tb">BB / TB</label>
									<input id="bb_tb" class="<?=($bb_tb!=$bb_tb_asal?'edited':'')?> auto_blur form-control decimal"  type="text"  value="{bb_tb}"  placeholder="" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="footer_pengkajian"><?=$header_pengkajian?></label>
									<input id="footer_pengkajian" class="form-control"  type="hidden"  value="{footer_pengkajian}"  placeholder="">
									
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-header-bg" id="tabel_skrining">
											<thead>
												<tr>
													<th width="5%" class="text-center">No</th>
													<th width="50%">Kriteria</th>
													<th width="30%">Pengkajian</th>
													<th width="15%" class="text-center">Skor</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot>
												<tr>
													<td colspan="2"><?=$footer_pengkajian?></td>
													<td class="text-right">Total Skor</td>
													<td ><input id="total_skor" class="<?=($total_skor!=$total_skor_asal?'edited':'')?> form-control text-center number"  type="text" readonly value="{total_skor}"  placeholder="" required></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-3">
									<label for="kesimpulan_gizi_text">KESIMPULAN</label>
									<input id="kesimpulan_gizi_text" class="<?=($kesimpulan_gizi_text!=$kesimpulan_gizi_text_asal?'edited':'')?> auto_blur form-control" readonly  type="text"  value="{kesimpulan_gizi_text}"  placeholder="" required>
									<input id="kesimpulan_gizi_id" readonly  type="hidden"  value="{kesimpulan_gizi_id}"  placeholder="" required>
								</div>
								<div class="col-md-5">
									<label for="tindakan_nama">TINDAK LANJUT</label>
									<input id="tindakan_nama" class="<?=($tindakan_nama!=$tindakan_nama_asal?'edited':'')?> auto_blur form-control" readonly type="text"  value="{tindakan_nama}"  placeholder="" required>
									<input id="tindakan_id" readonly type="hidden"  value="{tindakan_id}"  placeholder="" required>
								</div>
								<div class="col-md-4">
									<label for="keterangan">Keterangan</label>
									<input id="keterangan" class="<?=($keterangan!=$keterangan_asal?'edited':'')?> auto_blur form-control"  type="text"  value="{keterangan}"  placeholder="" required>
								</div>
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_gizi_anak();
		list_index_history_edit();
		
		load_awal_assesmen=false;
	}
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_gizi_anak', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function load_skrining_gizi_anak(){
	let assesmen_id=$("#assesmen_id").val();
	let footer_pengkajian=$("#footer_pengkajian").val();
	let versi_edit=$("#versi_edit").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_gizi_anak', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				footer_pengkajian:footer_pengkajian,
				versi_edit:versi_edit,
				
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			$(".nilai").select2();
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}
function get_perhitungan_skor(){

	let assesmen_id=$("#assesmen_id").val();
	let nilai_imt_id=$("#nilai_imt_id").val();
	let nilai_gizi_anak_id=$("#nilai_gizi_anak_id").val();
	// alert(nilai_gizi_anak_id);
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_perhitungan_skor', 
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				nilai_imt_id:nilai_imt_id,
				nilai_gizi_anak_id:nilai_gizi_anak_id,
		  }
		  ,success: function(data) {
			// $("#nilai_gizi_anak_id").val(data.nilai_gizi_anak_id);
			$("#penilaian_gizi_anak").val(data.penilaian_gizi_anak);
			$("#tindakan_gizi_anak").val(data.tindakan_gizi_anak);
				
			}
		});
	
}

function perhitungan_gizi(){
	let tinggi_badan=$("#tinggi_badan").val();
	let berat_badan=$("#berat_badan").val();
	  
	let masa_tubuh=0;
	// H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100))
	masa_tubuh=(parseFloat(berat_badan))/((parseFloat(tinggi_badan)/100)*(parseFloat(tinggi_badan)/100));
	$("#nilai_imt").val(masa_tubuh);
	$.ajax({
		url: '{site_url}tpendaftaran_ranap_erm/get_gizi', 
		dataType: "JSON",
		method: "POST",
		data : {
				masa_tubuh:masa_tubuh,
				
			   },
		success: function(data) {
			$("#nilai_imt_id").val(data.nilai_imt_id);
			$("#nilai_imt_text").val(data.nilai_imt_text);
			// $("#cover-spin").hide();
			get_perhitungan_skor();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
		}
	});
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}

</script>