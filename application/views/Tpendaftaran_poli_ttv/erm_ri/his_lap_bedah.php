<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.bg-edited {
		background-color: #ffedf2;
	}
	fieldset {
	  background-color: #eeeeee;
	  
	}

	legend {
	  background-color: gray;
	  color: white;
	  padding: 5px 5px;
	}
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_lap_bedah' || $menu_kiri=='his_lap_bedah_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_lap_bedah' || $menu_kiri=='his_lap_bedah_rj'? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_lap_bedah=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >							
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="pull-right push-10-r">
								<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_gizi_anak" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
							<?if ($assesmen_id!=''){?>
						<?
							if ($tanggal_opr){
								$tanggal_opr=HumanDateShort($tanggal_input);
								$tanggal_opr_asal=HumanDateShort($tanggal_input_asal);
							}else{
								$tanggal_opr=date('d-m-Y');
								$tanggal_opr_asal=date('d-m-Y');
								
							}
							if ($mulai){$mulai=HumanTime($mulai);}else{$mulai=date('H:i:s');}
							if ($selesai){$selesai=HumanTime($selesai);}else{$selesai=date('H:i:s');}
							
							
							$list_ruang_operasi=get_all('mruangan',array('idtipe'=>'2','status'=>1));
							$list_ppa_dokter=get_all('mppa',array('tipepegawai'=>'2','staktif'=>1));
							$list_ppa_all=get_all('mppa',array('staktif'=>1));
							$list_kelompok_operasi=get_all('mkelompok_operasi',array('status'=>1));
						?>
							<div class="form-group" style="margin-top: -10px;">
								
								<div class="col-md-2">
										<label for="jenis_bedah">Jenis Pembedahan</label>
										<select id="jenis_bedah" class="<?=($jenis_bedah!=$jenis_bedah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($jenis_bedah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(389) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_bedah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
								</div>
								<div class="col-md-2">
									<label for="tanggal_opr">Tanggal Operasi</label>
									<div class="input-group date">
										<input id="tanggal_opr" class="js-datepicker form-control <?=($tanggal_opr!=$tanggal_opr_asal?'edited':'')?>" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggal_opr ?>" >
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
								<div class="col-md-2">
									<label for="mulai">Mulai</label>
									<div class="input-group">
										<input id="mulai" class="time-datepicker form-control <?=($mulai!=$mulai_asal?'edited':'')?>" type="text" name="mulai" value="<?= $mulai ?>" >
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-2">
									<label for="selesai">Selesai</label>
									<div class="input-group">
										<input id="selesai" class="time-datepicker form-control <?=($selesai!=$selesai_asal?'edited':'')?>" type="text" name="selesai" value="<?= $selesai ?>" >
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="ruang_opr">Ruang Operasi</label>
									<select id="ruang_opr" class="<?=($ruang_opr!=$ruang_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($ruang_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach($list_ruang_operasi as $row){?>
										<option value="<?=$row->id?>" <?=($ruang_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="dokter_bedah">Dokter Bedah</label>
									<select id="dokter_bedah" class="<?=($dokter_bedah!=$dokter_bedah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($dokter_bedah == '' ? 'selected' : '')?>>Pilih Opsi</option>
										<?foreach($list_ppa_dokter as $row){?>
										<option value="<?=$row->id?>" <?=($dokter_bedah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<? $arr_assisten_bedah=($assisten_bedah?explode(',',$assisten_bedah):array()); ?>
								<div class="col-md-6">
									<label for="assisten_bedah">Asisten Bedah <?=($assisten_bedah!=$assisten_bedah_asal?text_danger('ADA PERUBAHAN'):'')?></label>
									<select id="assisten_bedah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_dokter as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_assisten_bedah)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="assisten_bedah_luar">Assisten Bedah (Jika dari luar)</label>
									<input id="assisten_bedah_luar" class="form-control <?=($assisten_bedah_luar!=$assisten_bedah_luar_asal?'edited':'')?> " type="text"  value="{assisten_bedah_luar}" name="assisten_bedah_luar" placeholder="" >
								</div>
								<? $arr_perawat_instrumen=($perawat_instrumen?explode(',',$perawat_instrumen):array()); ?>
								<div class="col-md-6">
									<label for="perawat_instrumen">Perawat Instrumen <?=($perawat_instrumen!=$perawat_instrumen_asal?text_danger('ADA PERUBAHAN'):'')?></label>
									<select id="perawat_instrumen" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_all as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_perawat_instrumen)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<? $arr_da=($da?explode(',',$da):array()); ?>
								<div class="col-md-6">
									<label for="da">Dokter Anestesi <?=($da!=$da_asal?text_danger('ADA PERUBAHAN'):'')?></label> 
									<select id="da" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_dokter as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_da)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<? $arr_perawat_anestesi=($perawat_anestesi?explode(',',$perawat_anestesi):array()); ?>
								<div class="col-md-6">
									<label for="perawat_anestesi">Perawat Anestesi <?=($perawat_anestesi!=$perawat_anestesi_asal?text_danger('ADA PERUBAHAN'):'')?></label>
									<select id="perawat_anestesi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_all as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_perawat_anestesi)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="diagnosa_pra">Diagnosa Pra Bedah</label>
									<input id="diagnosa_pra" class="form-control <?=($diagnosa_pra!=$diagnosa_pra_asal?'edited':'')?> " type="text"  value="{diagnosa_pra}" name="diagnosa_pra" placeholder="" required>
								</div>
								<div class="col-md-6">
									<label for="diagnosa_pasca">Diagnosa Pasca Bedah</label>
									<input id="diagnosa_pasca" class="form-control <?=($diagnosa_pasca!=$diagnosa_pasca_asal?'edited':'')?> " type="text"  value="{diagnosa_pasca}" name="diagnosa_pasca" placeholder="" required>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="tindakan">Tindakan</label>
									<input id="tindakan" class="form-control <?=($tindakan!=$tindakan_asal?'edited':'')?> " type="text"  value="{tindakan}" name="tindakan" placeholder="" >
								</div>
								<div class="col-md-6">
									<label for="disinfektan">Disinfektan Kulit</label>
									<input id="disinfektan" class="form-control <?=($disinfektan!=$disinfektan_asal?'edited':'')?> " type="text"  value="{disinfektan}" name="disinfektan" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="indikasi">Indikasi Operasi</label>
									<input id="indikasi" class="form-control <?=($indikasi!=$indikasi_asal?'edited':'')?> " type="text"  value="{indikasi}" name="indikasi" placeholder="" >
								</div>
								<div class="col-md-6">
									<label for="posisi">Posisi</label>
									<input id="posisi" class="form-control <?=($posisi!=$posisi_asal?'edited':'')?> " type="text"  value="{posisi}" name="posisi" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-3">
									<label for="pemasangan_implan">Pemasangan Implant</label>
									<select id="pemasangan_implan" class="<?=($pemasangan_implan!=$pemasangan_implan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($pemasangan_implan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(390) as $row){?>
										<option value="<?=$row->id?>" <?=($pemasangan_implan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemasangan_implan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-3">
									<label for="jenis_implan">Jenis Implant</label>
									<input id="jenis_implan" class="form-control <?=($jenis_implan!=$jenis_implan_asal?'edited':'')?> " type="text"  value="{jenis_implan}" name="jenis_implan" placeholder="" >
								</div>
								<div class="col-md-3">
									<label for="dikirim">Dikirim Pathologi Anatomi (PA)</label>
									<select id="dikirim" class="<?=($dikirim!=$dikirim_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($dikirim == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(391) as $row){?>
										<option value="<?=$row->id?>" <?=($dikirim == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($dikirim == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-3">
									<label for="asal_jaringan">Asal Jaringan</label>
									<input id="asal_jaringan" class="form-control <?=($asal_jaringan!=$asal_jaringan_asal?'edited':'')?> " type="text"  value="{asal_jaringan}" name="asal_jaringan" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-2">
									<label for="transfusi_darah">Transfusi Darah</label>
									<select id="transfusi_darah" class="<?=($transfusi_darah!=$transfusi_darah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($transfusi_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(392) as $row){?>
										<option value="<?=$row->id?>" <?=($transfusi_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($transfusi_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-2">
									<label for="jumlah_darah">Jumlah Darah Masuk</label>
									<div class=" input-group">
										<input id="jumlah_darah" class="form-control <?=($jumlah_darah!=$jumlah_darah_asal?'edited':'')?>"  type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?=$jumlah_darah?>" required>
										<span class="input-group-addon">CC</span>
									</div>
								</div>
								<div class="col-md-2">
									<label for="pendarahan">Pendarahan</label>
									<div class=" input-group">
										<input id="pendarahan" class="form-control <?=($pendarahan!=$pendarahan_asal?'edited':'')?>"  type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?=$pendarahan?>" required>
										<span class="input-group-addon">CC</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="klasifikasi_opr">Klarifikasi Operasi</label>
									<select id="klasifikasi_opr" class="<?=($klasifikasi_opr!=$klasifikasi_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($klasifikasi_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(124) as $row){?>
										<option value="<?=$row->id?>" <?=($klasifikasi_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($klasifikasi_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="jenis_opr">Jenis Operasi</label>
									<select id="jenis_opr" class="<?=($jenis_opr!=$jenis_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(393) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-3">
									<label for="jenis_anestesi">Jenis Anestesi</label>
									<select id="jenis_anestesi" class="<?=($jenis_anestesi!=$jenis_anestesi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_anestesi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(394) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_anestesi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_anestesi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-9">
									<label for="obat_anestesi">Obat Anestesi</label>
									<input id="obat_anestesi" class="form-control <?=($obat_anestesi!=$obat_anestesi_asal?'edited':'')?> " type="text"  value="{obat_anestesi}" name="obat_anestesi" placeholder="" >
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									
									<label for="macam_sayatan">Macan sayatan (bila perlu dengan gambar) <?=($macam_sayatan!=$macam_sayatan_asal?text_danger('ADA PERUBAHAN'):'')?></label>
									<textarea id="macam_sayatan" class="form-control auto_blur" name="macam_sayatan" ><?=$macam_sayatan?></textarea>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									
									<label for="laporan_pemedahan">Laporan Pembedahan <?=($laporan_pemedahan!=$laporan_pemedahan_asal?text_danger('ADA PERUBAHAN'):'')?><br><i><?=$catatan_ina?>
									</i></label>
									<textarea id="laporan_pemedahan" class="form-control auto_blur" name="laporan_pemedahan" ><?=$laporan_pemedahan?></textarea>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									
									<label for="komplikasi">Komplikasi <?=($komplikasi!=$komplikasi_asal?text_danger('ADA PERUBAHAN'):'')?></label>
									<textarea id="komplikasi" class="form-control auto_blur" name="komplikasi" ><?=$komplikasi?></textarea>
									
								</div>
								<div class="col-md-6">
									<label for="intruksi_post">Intruksi Post Operasi <?=($intruksi_post!=$intruksi_post_asal?text_danger('ADA PERUBAHAN'):'')?></label>
									<textarea id="intruksi_post" class="form-control auto_blur" name="intruksi_post" ><?=$intruksi_post?></textarea>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="pupil_1_ruangan">Upload Label Implan</label>
									<div id="div_label_implan" class="row items-push js-gallery-advanced">
										
									</div>
								</div>
							</div>
						<?}?>
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<table style="width:100%">
									<tr>
										<td style="width:33%" class="text-bold text-center"><strong>LAPORAN DIBUAT OLEH</strong></td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center"><strong>DOKTER BEDAH</strong></td>
									</tr>
									<tr>
										<td style="width:33%" class="text-bold text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($created_ppa)?>" alt="" title="">
										</td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center">
											<?if ($ttd_dokter){?>
												<div class="img-container fx-img-rotate-r">
													<img class="img-responsive" src="<?=$ttd_dokter?>" alt="">
													<?if ($status_assemen!='2'){?>
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="modal_ttd_ruangan_pemulihan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd('dokter_bedah')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
													<?}?>
												</div>
											<?}else{?>
												<?if ($dokter_bedah){?>
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($dokter_bedah)?>" alt="" title="">
												<?}else{?>
													<?if ($status_assemen=='2'){?>
													<button class="btn btn-sm btn-danger btn-menunggu" type="button"><i class="si si-user"></i> BELUM DITENTUKAN</button>
													<?}?>
												<?}?>
											<?}?>
													
											
											</td>
									</tr>
									<tr>
										<td style="width:33%" class="text-bold text-center text-bold"><i class="text-bold"><?=get_nama_ppa($created_ppa)?></i></td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center text-bold">
											<i class="text-bold"><?=($dokter_bedah?get_nama_ppa($dokter_bedah):'')?></i>
											<?if ($ttd_dokter==''){?>
											<?if ($status_assemen!='2'){?>
											<br>
											<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_pemulihan()"  type="button"><i class="fa fa-paint-brush"></i> TANDA TANGAN DOKTER</button>
											<?}?>
											<?}?>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		refresh_image_lap_bedah();
		load_awal_assesmen=false;
	}
	list_index_history_edit();
	$('#macam_sayatan,#komplikasi,#intruksi_post').summernote({
	  height: 100,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	})	
	$('#laporan_pemedahan').summernote({
	  height: 400,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	})	

	// load_data_rencana_asuhan_lap_bedah(1);
});
function refresh_image_lap_bedah(){
	var id=$("#assesmen_id").val();
	$('#div_label_implan').html('');
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_image_lap_bedah/'+id,
		dataType: "json",
		success: function(data) {
			// if (data.detail!=''){
				$("#div_label_implan").append(data.detail);
			// }
			// console.log();
			
		}
	});
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_lap_bedah', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

</script>