<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.bg-edited {
		background-color: #ffedf2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_pra_sedasi'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_pra_sedasi' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
		$waktudatang=date('H:i:s');
		$tanggaldatang=date('d-m-Y');
	if ($assesmen_id){
		$tanggaldaftar=HumanDateShort($tanggal_input);
		$waktudaftar=HumanTime($tanggal_input);
		
		if ($tanggal_step_1){$tanggal_step_1=HumanDateShort($tanggal_step_1);}else{$tanggal_step_1=date('d-m-Y');}
		if ($tanggal_step_2){$tanggal_step_2=HumanDateShort($tanggal_step_2);}else{$tanggal_step_2=date('d-m-Y');}
		if ($tanggal_step_3){$tanggal_step_3=HumanDateShort($tanggal_step_3);}else{$tanggal_step_3=date('d-m-Y');}
		if ($tanggal_step_1_asal){$tanggal_step_1_asal=HumanDateShort($tanggal_step_1_asal);}else{$tanggal_step_1_asal=date('d-m-Y');}
		if ($tanggal_step_2_asal){$tanggal_step_2_asal=HumanDateShort($tanggal_step_2_asal);}else{$tanggal_step_2_asal=date('d-m-Y');}
		if ($tanggal_step_3_asal){$tanggal_step_3_asal=HumanDateShort($tanggal_step_3_asal);}else{$tanggal_step_3_asal=date('d-m-Y');}
		if ($tiba_pemulihan){$tiba_pemulihan=HumanDateLong($tiba_pemulihan);}else{$tiba_pemulihan=date('d-m-Y H:i:s');}
		if ($pindah_jam){$pindah_jam=HumanDateLong($pindah_jam);}else{$pindah_jam=date('d-m-Y H:i:s');}
		if ($keruang_ranap){$keruang_ranap=HumanDateLong($keruang_ranap);}else{$keruang_ranap=date('d-m-Y H:i:s');}
		if ($jam_tv_2){$jam_tv_2=HumanTime($jam_tv_2);}else{$jam_tv_2=date('H:i:s');}
		
		
	}else{
		$tanggaldaftar=date('d-m-Y');
		$waktudaftar=date('H:i:s');
		$tgl_tiba_dikamar_beda=date('d-m-Y');
		
		
	}
	
		$disabel_input='';
		
		if ($st_input_assesmen=='0'){
			$disabel_input='disabled';
		}
		$disabel_cetak='';
		if ($st_cetak_assesmen=='0'){
			$disabel_cetak='disabled';
		}
	
	
?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_pra_sedasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >							
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="pull-right push-10-r">
								<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_gizi_anak" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id !=''){?>
						<div class="form-group">
							<div class="col-md-12">
								 <!-- Simple Classic Progress Wizard (.js-wizard-simple class is initialized in js/pages/base_forms_wizard.js) -->
                            <!-- For more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/ -->
                            <div class="js-wizard-simple block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="<?=($tab_wizard=='1'?'active':'')?>">
                                        <a href="#simple-classic-progress-step1" onclick="set_tab_wizard(1)" data-toggle="tab">PENGKAJIAN AWAL</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='2'?'active':'')?>">
                                        <a href="#simple-classic-progress-step2" onclick="set_tab_wizard(2)" data-toggle="tab">INTRA ANESTESI</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='3'?'active':'')?>">
                                        <a href="#simple-classic-progress-step3" onclick="set_tab_wizard(3)" data-toggle="tab">PASCA ANESTESI</a>
                                    </li>
									
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="form-horizontal" action="#" method="post">
                                    <!-- Steps Progress -->
                                    <div class="block-content block-content-mini block-content-full border-b">
                                        <div class="wizard-progress progress progress-mini remove-margin-b">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
                                        </div>
                                    </div>
                                    <!-- END Steps Progress -->

                                    <!-- Steps Content -->
                                    <div class="block-content tab-content">
                                        <!-- Step 1 -->
										<div class="tab-pane fade fade-up   push-50  <?=($tab_wizard=='1'?'in active':'')?>" id="simple-classic-progress-step1">
											<?
												$list_ruang_operasi=get_all('mruangan',array('idtipe'=>'2','status'=>1));
												$arr_subject_penyakit=($subject_penyakit?explode(',',$subject_penyakit):array());
												$list_ppa_dokter=get_all('mppa',array('tipepegawai'=>'2','staktif'=>1));
											?>
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-center text-primary">PENGKAJIAN AWAL PRA ANESTESI & PRA SEDASI</h4>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tanggal_step_1">Tanggal</label>
													<div class="input-group date">
														<input id="tanggal_step_1" class="js-datepicker form-control <?=($tanggal_step_1!=$tanggal_step_1_asal?'edited':'')?>" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggal_step_1 ?>" >
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="ruang_opr">Ruang Operasi</label>
													<select id="ruang_opr" class="<?=($ruang_opr !=$ruang_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($ruang_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach($list_ruang_operasi as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
														<label for="jenis_bedah">Jenis Pembedahan</label>
														<select id="jenis_bedah" name="jenis_bedah" class="<?=($jenis_bedah !=$jenis_bedah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" <?=($jenis_bedah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
															<?foreach(list_variable_ref(389) as $row){?>
															<option value="<?=$row->id?>" <?=($jenis_bedah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
															<?}?>
															
														</select>
												</div>
												<div class="col-md-3">
													<label for="diagnosa">Diagnosa</label>
													<input id="diagnosa" class="form-control <?=($diagnosa!=$diagnosa_asal?'edited':'')?> auto_blur " type="text"  value="{diagnosa}" name="diagnosa" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="jenis_opr">Jenis Operasi</label>
													<input id="jenis_opr" class="form-control <?=($jenis_opr!=$jenis_opr_asal?'edited':'')?> auto_blur " type="text"  value="{jenis_opr}" name="jenis_opr" placeholder="" required>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">A. SUBJECTIF</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
														<select id="subject_penyakit" name="subject_penyakit" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
															<option value="" >Pilih Opsi</option>
															<?foreach(list_variable_ref(395) as $row){?>
															<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_subject_penyakit)?'selected':'')?>><?=$row->nama?></option>
															<?}?>
															
														</select>
												</div>
												<div class="col-md-12">
													<textarea id="subject_penyakit_nama"class="form-control auto_blur <?=($subject_penyakit_nama!=$subject_penyakit_nama_asal?'edited':'')?>" name="subject_penyakit_nama" rows="2" style="width:100%"><?=$subject_penyakit_nama?></textarea>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label for="keterangan_step_1">Keterangan</label>
													<textarea id="keterangan_step_1"class="form-control auto_blur <?=($keterangan_step_1!=$keterangan_step_1_asal?'edited':'')?>" name="keterangan_step_1" rows="1" style="width:100%"><?=$keterangan_step_1?></textarea>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">B. OBJEKTIF</h5>
												</div>
												<div class="col-md-12 ">
													<label for="#">Keadaan Umum</label>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-2">
													<label for="gcs_e">GCS</label>
													<div class="input-group">
														<input id="gcs_e"  class="form-control <?=($gcs_e!=$gcs_e_asal?'edited':'')?> auto_blur"  type="text" name="gcs_e" value="{gcs_e}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="gcs_m">&nbsp;</label>
													<div class="input-group">
														<input id="gcs_m"  class="form-control <?=($gcs_m!=$gcs_m_asal?'edited':'')?> auto_blur"  type="text" name="gcs_m" value="{gcs_m}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="gcs_v">&nbsp;</label>
													<div class="input-group">
														<input id="gcs_v"  class="form-control <?=($gcs_v!=$gcs_v_asal?'edited':'')?> auto_blur"  type="text" name="gcs_v" value="{gcs_v}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input id="td_sistole"   class="form-control <?=($td_sistole!=$td_sistole_asal?'edited':'')?> auto_blur decimal"  type="text" value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input id="td_diastole"  class="form-control <?=($td_diastole!=$td_diastole_asal?'edited':'')?> auto_blur decimal"  type="text" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input id="suhu" class="form-control <?=($suhu!=$suhu_asal?'edited':'')?> auto_blur decimal"  type="text"  name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi">Frekuensi Nadi</label>
													<div class="input-group">
														<input id="nadi" class="form-control <?=($nadi!=$nadi_asal?'edited':'')?> auto_blur decimal"  type="text"  name="nadi" value="{nadi}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="respirasi">Respirasi</label>
													<div class="input-group">
														<input id="respirasi" class="form-control <?=($respirasi!=$respirasi_asal?'edited':'')?> auto_blur decimal" type="text"  name="respirasi" value="{respirasi}"  placeholder="Respirasi" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2">SpO2</label>
													<div class="input-group">
														<input id="spo2" class="form-control <?=($spo2!=$spo2_asal?'edited':'')?> auto_blur decimal" type="text"  name="spo2" value="{spo2}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="berat_badan">Berat Badan</label>
													<div class="input-group">
														<input id="berat_badan" class="form-control <?=($berat_badan!=$berat_badan_asal?'edited':'')?> auto_blur decimal"  type="text"   value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
														<span class="input-group-addon">Kg</span>
													</div>
												</div>

											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label for="mallampati">Mallampati</label>
													<select id="mallampati" class="<?=($mallampati !=$mallampati_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($mallampati == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(396) as $row){?>
														<option value="<?=$row->id?>" <?=($mallampati == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($mallampati == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="gigi">Gigi</label>
													<select id="gigi" class="<?=($gigi !=$gigi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($gigi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(397) as $row){?>
														<option value="<?=$row->id?>" <?=($gigi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($gigi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-2">
													<label for="gol_darah">Golongan Darah</label>
													<select id="gol_darah" class="<?=($gol_darah !=$gol_darah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($gol_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(11) as $row){?>
														<option value="<?=$row->id?>" <?=($gol_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($gol_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-2">
													<label for="leher">Leher</label>
													<select id="leher" class="<?=($leher !=$leher_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($leher == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(398) as $row){?>
														<option value="<?=$row->id?>" <?=($leher == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($leher == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="keterangan_obj">Keterangan</label>
													<input id="keterangan_obj" class="form-control <?=($keterangan_obj!=$keterangan_obj_asal?'edited':'')?> auto_blur" type="text"  value="{keterangan_obj}" name="keterangan_obj" placeholder="" >
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="kardio">Kardio Pulmonal</label>
													<textarea id="kardio" class="form-control <?=($kardio!=$kardio_asal?'edited':'')?> auto_blur summer100" name="kardio" ><?=$kardio?></textarea>
												</div>
												<div class="col-md-6">
													<label for="masalah_lain">Masalah lain</label>
													<textarea id="masalah_lain" class="form-control <?=($masalah_lain!=$masalah_lain_asal?'edited':'')?> auto_blur summer100" name="masalah_lain" ><?=$masalah_lain?></textarea>
													
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="lab">Laboratorium</label>
													<textarea id="lab" class="form-control <?=($lab!=$lab_asal?'edited':'')?> auto_blur summer100" name="lab" ><?=$lab?></textarea>
												</div>
												<div class="col-md-6">
													<label for="ekg">EKG/Rontgen/CT-Scan</label>
													<textarea id="ekg" class="form-control <?=($ekg!=$ekg_asal?'edited':'')?> auto_blur summer100" name="ekg" ><?=$ekg?></textarea>
												</div>
											</div>	
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6 ">
													<h5 class=" push-5 text-left text-primary">C. PENILAIAN</h5>
												</div>
												<div class="col-md-6 ">
													<h5 class=" push-5 text-left text-primary">D. PERENCANAAN ANESTESI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -15px;">
												<div class="col-md-6 ">
													<label for="status_fisik">Status Fisik</label>
													<select id="status_fisik" class="<?=($status_fisik !=$status_fisik_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($status_fisik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(399) as $row){?>
														<option value="<?=$row->id?>" <?=($status_fisik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($status_fisik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-6 ">
													<label for="perencanaan_anestesi">Status Fisik</label>
													<select id="perencanaan_anestesi" class="<?=($perencanaan_anestesi !=$perencanaan_anestesi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($perencanaan_anestesi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(400) as $row){?>
														<option value="<?=$row->id?>" <?=($perencanaan_anestesi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($perencanaan_anestesi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
											</div>		
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">D. PREMEDIKASI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_premedikasi">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="35%" class="text-center">NAMA OBAT</th>
																	<th width="20%" class="text-center">DOSIS</th>
																	<th width="15%" class="text-center">JAM</th>
																	<th width="15%" class="text-center">HASIL</th>
																	<th width="10%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th>#</th>
																	<th>
																		<input  type="hidden" class="form-control" id="premedikasi_id" value="" required>
																		<input  type="text" class="form-control" id="obat_premedikasi" name="obat_premedikasi" value="" required>
																	</th>
																	<th>
																		<input  type="text" class="form-control " id="dosis_premedikasi" name="dosis_premedikasi" value="" required>
																	</th>
																	<th>
																		<div class="input-group date">
																			<input type="text" class="js-datetimepicker form-control date_time"  id="jam_premedikasi" placeholder="HH/BB/TTTT" value="<?=date('d-m-Y H:i:s')?>" required>
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																		</div>
																	</th>
																	<th>
																		<select id="hasil_premedikasi" class="<?=($hasil_premedikasi !=$hasil_premedikasi_asal?'edited':'')?> form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(408) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama?></option>
																			<?}?>
																		</select>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_premedikasi()" id="btn_add_premedikasi" type="button"><i class="fa fa-plus"></i> Add</button>
																		<button class="btn btn-warning" onclick="clear_premedikasi_pra_sedasi()" type="button"><i class="fa fa-refresh"></i></button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<?
													$jml_ttd=count($list_ttd_1);
													$jml_kolom=($jml_ttd>0?$jml_ttd+1:2);
													$lebar_kolom=100/$jml_kolom;
													$jml_kolom_dokter=$jml_kolom-1;
												?>
												<div class="col-md-12 ">
													<table width="100%" >
														
														<tr>
															<td class="text-center" colspan="<?=$jml_kolom-1?>">
																<strong>DOKTER ANESTESI</strong>
																<br>
															</td>
															<td width="<?=$lebar_kolom?>%" class="text-center"><strong>PERAWAT / PENATA ANESTESI</strong></td>
														</tr>
														<tr>
															<?if($list_ttd_1){?>
																<?foreach($list_ttd_1 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<div class="img-container fx-img-rotate-r">
																			<img  class="" style="width:120px;height:120px; text-align: center;" src="<?=$r->ppa_dokter_ttd?>" alt="">
																			<?if ($status_assemen!='2'){?>
																			<div class="img-options">
																				<div class="img-options-content">
																					<div class="btn-group btn-group-sm">
																						<a class="btn btn-default" onclick="modal_faraf(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																						<a class="btn btn-default btn-danger" onclick="hapus_ttd(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																					</div>
																				</div>
																			</div>
																			<?}?>
																		</div>
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																<?if ($perawat_anes_id){?>
																<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($perawat_anes_id)?>" alt="" title="">
																<?}?>
															</td>
														</tr>
														<tr>
															<?if($list_ttd_1){?>
																<?foreach($list_ttd_1 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<strong>( <?=get_nama_ppa($r->ppa_dokter_id)?> )</strong>
																		<br>
																		(<?=HumanDateLong2($r->date_isi)?>)
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																
																<?if ($status_assemen!='2'){?>
																<select id="perawat_anes_id" name="perawat_anes_id" class="<?=($perawat_anes_id !=$perawat_anes_id_asal?'edited':'')?> form-control opsi_change_ttd" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="" <?=($perawat_anes_id==''?'selected':'')?>>- Pilih Perawat-</option>
																	<?foreach($list_ppa as $r){?>
																	<option value="<?=$r->id?>" <?=($perawat_anes_id==$r->id?'selected':'')?>><?=$r->nama?></option>
																	<?}?>
																	
																	
																</select>
																<?}else{?>
																	<?if ($perawat_anes_id){?>
																	<strong>( <?=get_nama_ppa($perawat_anes_id)?> )</strong> 
																	<?}?>
																
																<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>	
										</div>
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='2'?'in active':'')?>" id="simple-classic-progress-step2">
											
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-center text-primary">LAPORAN INTRA ANESTESI</h4>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tanggal_step_2">Tanggal</label>
													<div class="input-group date">
														<input id="tanggal_step_2" class="js-datepicker form-control <?=($tanggal_step_2!=$tanggal_step_2_asal?'edited':'')?>" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggal_step_2 ?>" >
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="ruang_opr_2">Ruang Operasi</label>
													<select id="ruang_opr_2" class="<?=($ruang_opr_2 !=$ruang_opr_2_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($ruang_opr_2 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach($list_ruang_operasi as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_opr_2 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_bedah_2">Jenis Pembedahan</label>
													<select id="jenis_bedah_2" name="jenis_bedah_2" class="<?=($jenis_bedah_2 !=$jenis_bedah_2_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_bedah_2 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(389) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_bedah_2 == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah_2 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<?
													$arr_da=($da?explode(',',$da):array());
													$arr_penata_anestesi=($penata_anestesi?explode(',',$penata_anestesi):array());
												?>
												<div class="col-md-3">
													<label for="da" >Dokter Anestesi <?=($da !=$da_asal?text_danger('edited'):'')?></label>
													<select id="da" name="da" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<?foreach($list_ppa_dokter as $row){?>
														<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_da)?'selected':'')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="penata_anestesi">Penaata Perawat Anestesi <?=($penata_anestesi !=$penata_anestesi_asal?text_danger('edited'):'')?></label>
													<select id="penata_anestesi" name="penata_anestesi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<?foreach($list_ppa as $row){?>
														<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_penata_anestesi)?'selected':'')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">F. PENILAIAN PRA INDUKSI</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv_2">Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control auto_blur " id="jam_tv_2" name="jam_tv_2" value="<?= ($jam_tv_2?$jam_tv_2:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran_2">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_2" class="<?=($tingkat_kesadaran_2 !=$tingkat_kesadaran_2_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_2 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="td_sistole_2">Tekanan Darah</label>
													<div class="input-group">
														<input id="td_sistole_2" class="form-control <?=($td_sistole_2!=$td_sistole_2_asal?'edited':'')?> auto_blur decimal"  type="text"   value="{td_sistole_2}" name="td_sistole_2" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input id="td_diastole_2" class="form-control <?=($td_diastole_2!=$td_diastole_2_asal?'edited':'')?> auto_blur decimal"  type="text"  value="{td_diastole_2}"  name="td_diastole_2" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="suhu_2">Suhu Tubuh</label>
													<div class="input-group">
														<input id="suhu_2" class="form-control <?=($suhu_2!=$suhu_2_asal?'edited':'')?> auto_blur decimal"  type="text"  name="suhu_2" value="{suhu_2}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi_2">Frekuensi Nadi</label>
													<div class="input-group">
														<input id="nadi_2" class="form-control <?=($nadi_2!=$nadi_2_asal?'edited':'')?> auto_blur decimal"   type="text"  name="nadi_2" value="{nadi_2}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas_2">Frekuensi Nafas</label>
													<div class="input-group">
														<input id="nafas_2" class="form-control <?=($nafas_2!=$nafas_2_asal?'edited':'')?> auto_blur decimal"   type="text"  name="nafas_2" value="{nafas_2}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_2">SpO2</label>
													<div class="input-group">
														<input id="spo2_2" class="form-control <?=($spo2_2!=$spo2_2_asal?'edited':'')?> auto_blur decimal"   type="text"  name="spo2_2" value="{spo2_2}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-3">
													<label for="cath">I.V Cath No</label>
													<input id="cath" class="form-control <?=($cath!=$cath_asal?'edited':'')?> auto_blur"   type="text"   value="{cath}" name="cath" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="keterangan_penilain_induksi">Keterangan </label>
													<input id="keterangan_penilain_induksi" class="form-control <?=($keterangan_penilain_induksi!=$keterangan_penilain_induksi_asal?'edited':'')?> auto_blur"   type="text"   value="{keterangan_penilain_induksi}" name="keterangan_penilain_induksi" placeholder="" required>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">G. ANESTESI UMUM</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-2">
													<label for="induksi">Induksi</label>
													<select id="induksi" class="<?=($induksi !=$induksi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($induksi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(401) as $row){?>
														<option value="<?=$row->id?>" <?=($induksi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($induksi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="induksi_lain">Keterangan lainnya Induksi</label>
													<input id="induksi_lain" class="form-control <?=($induksi_lain!=$induksi_lain_asal?'edited':'')?> auto_blur"   type="text"   value="{induksi_lain}" name="induksi_lain" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="ett">ETT/LMA No</label>
													<input id="ett" class="form-control <?=($ett!=$ett_asal?'edited':'')?> auto_blur"   type="text"   value="{ett}" name="ett" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="teknik">Teknik</label>
													<input id="teknik" class="form-control <?=($teknik!=$teknik_asal?'edited':'')?> auto_blur"   type="text"   value="{teknik}" name="teknik" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="teknik_ket">Keterangan Teknik</label>
													<input id="teknik_ket" class="form-control <?=($teknik_ket!=$teknik_ket_asal?'edited':'')?> auto_blur"   type="text"   value="{teknik_ket}" name="teknik_ket" placeholder="" required>
												</div>
												
											</div>					
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-2">
													<label for="pengaturan_nafas">Pengaturan Nafas</label>
													<select id="pengaturan_nafas" class="<?=($pengaturan_nafas !=$pengaturan_nafas_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pengaturan_nafas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(402) as $row){?>
														<option value="<?=$row->id?>" <?=($pengaturan_nafas == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaturan_nafas == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="pengaturan_nafas_ket">Keterangan</label>
													<input id="pengaturan_nafas_ket" class="form-control <?=($pengaturan_nafas_ket!=$pengaturan_nafas_ket_asal?'edited':'')?> auto_blur"   type="text"   value="{pengaturan_nafas_ket}" name="pengaturan_nafas_ket" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="ventilator">Ventilator Mode</label>
													<input id="ventilator" class="form-control <?=($ventilator!=$ventilator_asal?'edited':'')?> auto_blur"   type="text"   value="{ventilator}" name="ventilator" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="ventilator_volume">Ventilator Tidal Vollume</label>
													<input id="ventilator_volume" class="form-control <?=($ventilator_volume!=$ventilator_volume_asal?'edited':'')?> auto_blur"   type="text"   value="{ventilator_volume}" name="ventilator_volume" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="ventilator_rr">Ventilator RR</label>
													<input id="ventilator_rr" class="form-control <?=($ventilator_rr!=$ventilator_rr_asal?'edited':'')?> auto_blur"   type="text"   value="{ventilator_rr}" name="ventilator_rr" placeholder="" required>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">H. MONITORING INTRA ANESTESI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left">UPLOAD FILE</h5>
													<button class="btn btn-sm btn-warning" onclick="show_modal_upload(2)"  type="button"><i class="fa fa-upload"></i> Upload </button>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_upload_2">
															<thead>
																<tr>
																	<th width="50%" class="text-center">File</th>
																	<th width="15%" class="text-center">Size</th>
																	<th width="25%" class="text-center">User</th>
																	<th width="10%" class="text-center">X</th>
																	
																</tr>
															</thead>
															<tbody>
																
															</tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_medikasi">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="50%" class="text-center">NAMA OBAT</th>
																	<th width="25%" class="text-center">SATUAN</th>
																	<th width="20%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th>#</th>
																	<th>
																		<input  type="hidden" class="form-control" id="medikasi_id" value="" required>
																		<input  type="text" class="form-control" id="obat_medikasi" name="obat_medikasi" value="" required>
																	</th>
																	
																	<th>
																		<select id="satuan_medikasi" class="<?=($satuan_medikasi !=$satuan_medikasi_asal?'edited':'')?> form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="" selected>Pilih Satuan</option>
																			<?foreach(list_variable_ref(403) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama?></option>
																			<?}?>
																		</select>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info btn-xs" onclick="simpan_medikasi()" id="btn_add_medikasi" type="button"><i class="fa fa-plus"></i> Add</button>
																		<button class="btn btn-warning btn-xs" onclick="clear_medikasi_pra_sedasi()" type="button"><i class="fa fa-refresh"></i></button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
												<div class="col-md-3">
													<label for="masalah_durante">Masalah Durante Operasi / Anestesi</label>
													<textarea id="masalah_durante" class="form-control <?=($masalah_durante!=$masalah_durante_asal?'edited':'')?> auto_blur summer100" name="masalah_durante" ><?=$masalah_durante?></textarea>
													
												</div>
												<div class="col-md-3">
													<label for="tindakan_durante">Tindakan</label>
													<textarea id="tindakan_durante" class="form-control <?=($tindakan_durante!=$tindakan_durante_asal?'edited':'')?> auto_blur summer100" name="tindakan_durante" ><?=$tindakan_durante?></textarea>
													
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">I. BLOK REGIONAL</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-3">
													<label for="blok_teknik">Teknik</label>
													<select id="blok_teknik" class="<?=($blok_teknik !=$blok_teknik_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($blok_teknik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(404) as $row){?>
														<option value="<?=$row->id?>" <?=($blok_teknik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($blok_teknik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="blok_lokasi">Lokasi</label>
													<input id="blok_lokasi" class="form-control <?=($blok_lokasi!=$blok_lokasi_asal?'edited':'')?> auto_blur"   type="text"   value="{blok_lokasi}" name="blok_lokasi" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="jenis_obat">Jenis Obat</label>
													<input id="jenis_obat" class="form-control <?=($jenis_obat!=$jenis_obat_asal?'edited':'')?> auto_blur"   type="text"   value="{jenis_obat}" name="jenis_obat" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="dosis">Dosis</label>
													<input id="dosis" class="form-control <?=($dosis!=$dosis_asal?'edited':'')?> auto_blur"   type="text"   value="{dosis}" name="dosis" placeholder="" required>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: 25px;">
												<?
													$jml_ttd=count($list_ttd_2);
													$jml_kolom=($jml_ttd>0?$jml_ttd+1:2);
													$lebar_kolom=100/$jml_kolom;
													$jml_kolom_dokter=$jml_kolom-1;
												?>
												<div class="col-md-12 ">
													<table width="100%" >
														
														<tr>
															<td class="text-center" colspan="<?=$jml_kolom-1?>">
																<strong>DOKTER ANESTESI</strong>
																<br>
															</td>
															<td width="<?=$lebar_kolom?>%" class="text-center"><strong>PERAWAT / PENATA ANESTESI</strong></td>
														</tr>
														<tr>
															<?if($list_ttd_2){?>
																<?foreach($list_ttd_2 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<div class="img-container fx-img-rotate-r">
																			<img  class="" style="width:120px;height:120px; text-align: center;" src="<?=$r->ppa_dokter_ttd?>" alt="">
																			<?if ($status_assemen!='2'){?>
																			<div class="img-options">
																				<div class="img-options-content">
																					<div class="btn-group btn-group-sm">
																						<a class="btn btn-default" onclick="modal_faraf(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																						<a class="btn btn-default btn-danger" onclick="hapus_ttd(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																					</div>
																				</div>
																			</div>
																			<?}?>
																		</div>
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																<?if ($perawat_anes_id_2){?>
																<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($perawat_anes_id_2)?>" alt="" title="">
																<?}?>
															</td>
														</tr>
														<tr>
															<?if($list_ttd_2){?>
																<?foreach($list_ttd_2 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<strong>( <?=get_nama_ppa($r->ppa_dokter_id)?> )</strong>
																		<br>
																		(<?=HumanDateLong2($r->date_isi)?>)
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																
																<?if ($status_assemen!='2'){?>
																<select id="perawat_anes_id_2" name="perawat_anes_id_2" class="<?=($perawat_anes_id_2 !=$perawat_anes_id_2_asal?'edited':'')?> form-control opsi_change_ttd" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="" <?=($perawat_anes_id_2==''?'selected':'')?>>- Pilih Perawat-</option>
																	<?foreach($list_ppa as $r){?>
																	<option value="<?=$r->id?>" <?=($perawat_anes_id_2==$r->id?'selected':'')?>><?=$r->nama?></option>
																	<?}?>
																	
																	
																</select>
																<?}else{?>
																	<?if ($perawat_anes_id_2){?>
																	<strong>( <?=get_nama_ppa($perawat_anes_id_2)?> )</strong> 
																	<?}?>
																
																<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>	
										</div>
										<div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='3'?'in active':'')?>" id="simple-classic-progress-step3">
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-center text-primary">LAPORAN PASCA ANESTESI</h4>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tanggal_step_3">Tanggal</label>
													<div class="input-group date">
														<input id="tanggal_step_3" class="js-datepicker form-control <?=($tanggal_step_3!=$tanggal_step_3_asal?'edited':'')?>" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggal_step_3 ?>" >
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="ruang_opr_3">Ruang Operasi</label>
													<select id="ruang_opr_3" class="<?=($ruang_opr_3 !=$ruang_opr_3_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($ruang_opr_3 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach($list_ruang_operasi as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_opr_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="jenis_bedah_3">Jenis </label>
													<select id="jenis_bedah_3" name="jenis_bedah_3" class="<?=($jenis_bedah_3 !=$jenis_bedah_3_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_bedah_3 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(389) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_bedah_3 == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="tiba_pemulihan">Tiba Diruang Pemulihan</label>
													<div class="input-group date">
														<input type="text" class="js-datetimepicker form-control date_time"  id="tiba_pemulihan" placeholder="HH/BB/TTTT" value="<?=$tiba_pemulihan?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penyerah_id">Petugas Yang Menyerahkan</label>
														<div class="input-group">
														<select id="petugas_penyerah_id" name="petugas_penyerah_id" class="<?=($petugas_penyerah_id !=$petugas_penyerah_id_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penyerah_id==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penyerah_id==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penyerah_ttd?'default':'success')?>"  title="Tanda Tangan" onclick="modal_petugas('petugas_penyerah_id')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penerima_id">Petugas Yang Menerima</label>
														<div class="input-group">
														<select id="petugas_penerima_id" name="petugas_penerima_id" class="<?=($petugas_penerima_id !=$petugas_penerima_id_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penerima_id==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penerima_id==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penerima_ttd?'default':'success')?>"  title="Tanda Tangan" onclick="modal_petugas('petugas_penerima_id')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>	
																		
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label for="intruksi_pasca_anestesi">Intruksi Pasca Anestesi</label>
													<textarea id="intruksi_pasca_anestesi"class="form-control auto_blur <?=($intruksi_pasca_anestesi!=$intruksi_pasca_anestesi_asal?'edited':'')?>" name="intruksi_pasca_anestesi" rows="2" style="width:100%"><?=$intruksi_pasca_anestesi?></textarea>
												</div>
											</div>
											<div class="form-group" >
												
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_3">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_3" class="<?=($tingkat_kesadaran_3 !=$tingkat_kesadaran_3_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="td_sistole_3">Tekanan Darah</label>
													<div class="input-group">
														<input id="td_sistole_3" class="form-control <?=($td_sistole_3!=$td_sistole_3_asal?'edited':'')?> auto_blur decimal"  type="text"   value="{td_sistole_3}" name="td_sistole_3" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input id="td_diastole_3" class="form-control <?=($td_diastole_3!=$td_diastole_3_asal?'edited':'')?> auto_blur decimal"  type="text"  value="{td_diastole_3}"  name="td_diastole_3" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2 ">
													<label for="respirasi_id_3">Nadi</label>
													<select id="respirasi_id_3" class="<?=($respirasi_id_3 !=$respirasi_id_3_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(409) as $row){?>
														<option value="<?=$row->id?>"  <?=($respirasi_id_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="respirasi_3">Respirasi</label>
													<div class="input-group">
														<input id="respirasi_3" class="form-control <?=($respirasi_3!=$respirasi_3_asal?'edited':'')?> auto_blur decimal" type="text"  name="respirasi_3" value="{respirasi_3}"  placeholder="Respirasi" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_3">SpO2</label>
													<div class="input-group">
														<input id="spo2_3" class="form-control <?=($spo2_3!=$spo2_3_asal?'edited':'')?> auto_blur decimal"   type="text"  name="spo2_3" value="{spo2_3}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<div class="col-md-12">
														<table class="block-table text-center ">
															<tbody>
																<tr>
																	<td class="border-r" style="width:70%">
																		<div>
																			<div class="input-group">
																				<input class="form-control" disabled type="text" readonly value="<?=$nama_kajian?>" id="nama_kajian" placeholder="nama_kajian" >
																				<span class="input-group-addon"><i class="fa fa-user"></i></span>
																			</div>
																		</div>
																		<div class="h5 font-w700 text-left text-primary push-5-t">Penilaian Rating Scale</div>
																	</td>
																	<td class="border-r"  style="width:30%">
																		<div class="h1 font-w700 text-primary" id="div_total_skor_nyeri"><?=$total_skor_nyeri?></div>
																		<div class="h5 text-uppercase push-5-t">Total Skor Skala Nyeri</div>
																	</td>
																</tr>
															</tbody>
														</table>
														
													</div>
													
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<div class="col-md-12">
														<div id="div_tabel_nrs" class="form-group">
															
														</div>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6">
													<label for="komplikasi">Komplikasi / Penyakit Penyulit</label>
													<textarea id="komplikasi" class="form-control <?=($komplikasi!=$komplikasi_asal?'edited':'')?> auto_blur summer100" name="komplikasi" ><?=$komplikasi?></textarea>
													
												</div>
												<div class="col-md-6">
													<label for="komplikasi_tindakan">Tindakan</label>
													<textarea id="komplikasi_tindakan" class="form-control <?=($komplikasi_tindakan!=$komplikasi_tindakan_asal?'edited':'')?> auto_blur summer100" name="komplikasi_tindakan" ><?=$komplikasi_tindakan?></textarea>
													
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left">UPLOAD FILE</h5>
													<button class="btn btn-sm btn-warning" onclick="show_modal_upload(3)"  type="button"><i class="fa fa-upload"></i> Upload </button>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_upload_3">
															<thead>
																<tr>
																	<th width="50%" class="text-center">File</th>
																	<th width="15%" class="text-center">Size</th>
																	<th width="25%" class="text-center">User</th>
																	<th width="10%" class="text-center">X</th>
																	
																</tr>
															</thead>
															<tbody>
																
															</tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												
												<div class="col-md-12">
													<label for="metodi_penilain">Metode Penilaian</label>
													<select id="metodi_penilain" class="<?=($metodi_penilain !=$metodi_penilain_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($metodi_penilain == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(get_all('mpasca_anestesi',array('staktif'=>1)) as $row){?>
														<option value="<?=$row->id?>" <?=($metodi_penilain == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
																
											</div>
											<div class="form-group" style="margin-top:-10px">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
															<thead>
																<tr>
																	<th width="5%" class="text-center">NO</th>
																	<th width="15%" class="text-center">KRITERIA</th>
																	<?
																	$q="SELECT H.jenis as id,M.ref as nama 
																	FROM `tranap_pra_sedasi_skrining` H 
																	INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='407'
																	WHERE H.assesmen_id='$assesmen_id'
																	GROUP BY H.jenis
																	ORDER BY H.jenis";
																	$list_header=$this->db->query($q)->result();
																	$jml_kolom=count($list_header);
																	$lebar_kolom=80 / $jml_kolom;
																	foreach($list_header as $r){
																	?>
																	<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
																	
																	<?}?>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
													<label class="h4 text-primary" id="label_footer"></label>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label for="keterangan_penilaian">Keterangan Lain-Lain</label>
													<textarea id="keterangan_penilaian"class="form-control auto_blur <?=($keterangan_penilaian!=$keterangan_penilaian_asal?'edited':'')?>" name="keterangan_penilaian" rows="2" style="width:100%"><?=$keterangan_penilaian?></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-left">Pasca Anestesi Dirawat Inap</h4>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="lama_pasien">Lama Pasien Diruang Pemulihan</label>
													<input id="lama_pasien" class="form-control <?=($lama_pasien!=$lama_pasien_asal?'edited':'')?> auto_blur"   type="text"   value="{lama_pasien}" name="lama_pasien" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="pindah_jam">Pindah Dari Ruangan Pemulihan Jam</label>
													<div class="input-group date">
														<input type="text" class="js-datetimepicker form-control date_time"  id="pindah_jam" placeholder="HH/BB/TTTT" value="<?=$pindah_jam?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="keruang_ranap">Pindah Dari Ruangan Pemulihan Jam</label>
													<div class="input-group date">
														<input type="text" class="js-datetimepicker form-control date_time"  id="keruang_ranap" placeholder="HH/BB/TTTT" value="<?=$keruang_ranap?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penyerah_id_3">Petugas Yang Menyerahkan</label>
														<div class="input-group">
														<select id="petugas_penyerah_id_3" name="petugas_penyerah_id_3" class="<?=($petugas_penyerah_id_3 !=$petugas_penyerah_id_3_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penyerah_id_3==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penyerah_id_3==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penyerah_ttd_3?'default':'success')?>"  title="Tanda Tangan" onclick="modal_petugas('petugas_penyerah_id_3')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penerima_id_3">Petugas Yang Menerima</label>
														<div class="input-group">
														<select id="petugas_penerima_id_3" name="petugas_penerima_id_3" class="<?=($petugas_penerima_id_3 !=$petugas_penerima_id_3_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penerima_id_3==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penerima_id_3==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penerima_ttd_3?'default':'success')?>" title="Tanda Tangan" onclick="modal_petugas('petugas_penerima_id_3')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>	
																		
											</div>
											<?
												$arr_observasi_posisi=($observasi_posisi?explode(',',$observasi_posisi):array());
											?>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="observasi">Obeservasi Kesadaran, Tanda Vital (Tekanan Darah, Nadi, Pernapasan, Suhu) Setiap</label>
													<input id="observasi" class="form-control <?=($observasi!=$observasi_asal?'edited':'')?> auto_blur"   type="text"   value="{observasi}" name="observasi" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="observasi_posisi">Posisi <?=($observasi_posisi !=$observasi_posisi_asal?text_danger('edited'):'')?></label>
													<select id="observasi_posisi" name="observasi_posisi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<option value="" >Pilih Opsi</option>
														<?foreach(list_variable_ref(395) as $row){?>
														<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_observasi_posisi)?'selected':'')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="puasa">Puasa Selama</label>
													<input id="puasa" class="form-control <?=($puasa!=$puasa_asal?'edited':'')?> auto_blur"   type="text"   value="{puasa}" name="puasa" placeholder="" required>
												</div>
																
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-4">
													<label for="mual">Bila Timbul Mual / Muntah</label>
													<input id="mual" class="form-control <?=($mual!=$mual_asal?'edited':'')?> auto_blur"   type="text"   value="{mual}" name="mual" placeholder="" required>
												</div>
												<div class="col-md-4">
													<label for="manajemen_nyeri">Manajemen Nyeri</label>
													<input id="manajemen_nyeri" class="form-control <?=($manajemen_nyeri!=$manajemen_nyeri_asal?'edited':'')?> auto_blur"   type="text"   value="{manajemen_nyeri}" name="manajemen_nyeri" placeholder="" required>
												</div>
												<div class="col-md-4">
													<label for="penunjang">Pemeriksaan Penunjang & Peralatan Lainnya</label>
													<input id="penunjang" class="form-control <?=($penunjang!=$penunjang_asal?'edited':'')?> auto_blur"   type="text"   value="{penunjang}" name="penunjang" placeholder="" required>
												</div>
																
											</div>
											<div class="form-group" style="margin-top: 25px;">
												<?
													$jml_ttd=count($list_ttd_3);
													$jml_kolom=($jml_ttd>0?$jml_ttd+1:2);
													$lebar_kolom=100/$jml_kolom;
													$jml_kolom_dokter=$jml_kolom-1;
												?>
												<div class="col-md-12 ">
													<table width="100%" >
														
														<tr>
															<td class="text-center" colspan="<?=$jml_kolom-1?>">
																<strong>DOKTER ANESTESI</strong>
																<br>
															</td>
															<td width="<?=$lebar_kolom?>%" class="text-center"><strong>PERAWAT / PENATA ANESTESI</strong></td>
														</tr>
														<tr>
															<?if($list_ttd_3){?>
																<?foreach($list_ttd_3 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<div class="img-container fx-img-rotate-r">
																			<img  class="" style="width:120px;height:120px; text-align: center;" src="<?=$r->ppa_dokter_ttd?>" alt="">
																			<?if ($status_assemen!='2'){?>
																			<div class="img-options">
																				<div class="img-options-content">
																					<div class="btn-group btn-group-sm">
																						<a class="btn btn-default" onclick="modal_faraf(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																						<a class="btn btn-default btn-danger" onclick="hapus_ttd(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																					</div>
																				</div>
																			</div>
																			<?}?>
																		</div>
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																<?if ($perawat_anes_id_2){?>
																<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($perawat_anes_id_2)?>" alt="" title="">
																<?}?>
															</td>
														</tr>
														<tr>
															<?if($list_ttd_3){?>
																<?foreach($list_ttd_3 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<strong>( <?=get_nama_ppa($r->ppa_dokter_id)?> )</strong>
																		<br>
																		(<?=HumanDateLong2($r->date_isi)?>)
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																
																<?if ($status_assemen!='2'){?>
																<select id="perawat_anes_id_2" name="perawat_anes_id_2" class="<?=($perawat_anes_id_2 !=$perawat_anes_id_2_asal?'edited':'')?> form-control opsi_change_ttd" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="" <?=($perawat_anes_id_2==''?'selected':'')?>>- Pilih Perawat-</option>
																	<?foreach($list_ppa as $r){?>
																	<option value="<?=$r->id?>" <?=($perawat_anes_id_2==$r->id?'selected':'')?>><?=$r->nama?></option>
																	<?}?>
																	
																	
																</select>
																<?}else{?>
																	<?if ($perawat_anes_id_2){?>
																	<strong>( <?=get_nama_ppa($perawat_anes_id_2)?> )</strong> 
																	<?}?>
																
																<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>	
										</div>
									</div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-mini block-content-full border-t">
                                    <div class="block-content block-content-mini block-content-full border-t">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button class="wizard-prev btn btn-default" type="button"><i class="fa fa-arrow-left"></i> Kembali</button>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="wizard-next btn btn-default" type="button">Step Selanjutnya <i class="fa fa-arrow-right"></i></button>
												<?if ($status_assemen!='2'){?>
													<?if ($status_assemen=='1'){?>
													
													<button class="wizard-finish btn btn-primary" onclick="close_assesmen()" type="button"><i class="fa fa-save"></i> SIMPAN</button>
													<?}else{?>
													<button class="wizard-finish btn btn-warning" onclick="simpan_template()" type="button"><i class="fa fa-save"></i> SIMPAN TEMPLATE</button>
													
													<?}?>
												<?}?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                           
						   </div>
                            <!-- END Simple Classic Progress Wizard -->
							</div>
						</div>
						</div>
						<?}?>
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
		
<script type="text/javascript" src="{js_path}pages/base_forms_wizard.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	disabel_edit();
	$('.angka').number(true, 0);
	$(".btn_close_left").click(); 
	// add_referensi('lokasi_tornique','364','Lokasi Tornique');
	disabel_edit();
	$('.summer100').summernote({
	  height: 100,   //set editable area's height
	  dialogsInBody: true,
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	})	
	$(".date_time").datetimepicker({
		format: "DD-MM-YYYY HH:mm",
		// stepping: 30
	});

	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_premedikasi_pra_sedasi();
		load_medikasi_pra_sedasi();
		load_nrs();
		load_skrining_pra_sedasi();
		refresh_image();
		if ($("#status_assemen").val()!='2'){
			
			Dropzone.autoDiscover = false;
			myDropzone = new Dropzone(".dropzone", { 
			   autoProcessQueue: true,
			   maxFilesize: 30,
			});
			myDropzone.on("complete", function(file) {
			  refresh_image();
			  myDropzone.removeFile(file);
			});
		}
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian()
	}
	list_index_history_edit();
	// load_data_rencana_asuhan_pra_sedasi(1);
});

function load_nrs(){
	assesmen_id=$("#assesmen_id").val();
	status_assemen=$("#status_assemen").val();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_nrs_fisio_pra_sedasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				versi_edit:$("#versi_edit").val(),
			   },
		success: function(data) {
			$("#div_tabel_nrs").append(data);
		}
	});
}
function load_medikasi_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}thistory_tindakan/load_medikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					versi_edit:$("#versi_edit").val(),
					
				},
			success: function(data) {
				$("#tabel_medikasi tbody").empty();
				$("#tabel_medikasi tbody").append(data.tabel);
			}
		});
}
function load_premedikasi_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}thistory_tindakan/load_premedikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					versi_edit:$("#versi_edit").val(),
					
				},
			success: function(data) {
				$("#tabel_premedikasi tbody").empty();
				$("#tabel_premedikasi tbody").append(data.tabel);
			}
		});
}
function load_skrining_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	let metodi_penilain=$("#metodi_penilain").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_pra_sedasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				mrisiko_id:metodi_penilain,
				versi_edit:$("#versi_edit").val(),
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			$("#label_footer").html(data.isi_footer);
			// console.LOG(data);
			$(".nilai").select2();
			// get_skor_pengkajian_pra_sedasi();
			$("#cover-spin").hide();
		}
	});
}
function refresh_image(){
	var id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_image_pra_sedasi/'+id+'/2',
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#tabel_upload_2 tbody').empty();
				$("#tabel_upload_2 tbody").append(data.detail);
				disabel_edit();
			}
			// console.log();
			
		}
	});
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_image_pra_sedasi/'+id+'/3',
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#tabel_upload_3 tbody').empty();
				$("#tabel_upload_3 tbody").append(data.detail);
				disabel_edit();
			}
			// console.log();
			
		}
	});
}

function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_pra_sedasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
		  $(".data_asuhan").removeAttr('disabled');
		 $(".data_asuhan_durante").removeAttr('disabled');
		 $(".data_asuhan_paska").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
		 $(".wizard-prev").removeAttr('disabled');
		 $(".wizard-next").removeAttr('disabled');
		 $("#diagnosa_durante_id_list").removeAttr('disabled');
		 $("#diagnosa_paska_id_list").removeAttr('disabled');
		 $("#diagnosa_id_list").removeAttr('disabled');
	}
}
$(".wizard-next").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)+1;
	set_tab_wizard(tab_wizard);
});
$(".wizard-prev").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)-1;
	set_tab_wizard(tab_wizard);
});
function set_tab_wizard($id){
	var assesmen_id=$("#assesmen_id").val();
	$("#tab_wizard").val($id);
	// $.ajax({
	  // url: '{site_url}Tpendaftaran_ranap_erm/set_tab_wizard_pra_sedasi/',
	  // dataType: "json",
	  // type: 'POST',
	  // data: {
			// assesmen_id:assesmen_id,
			// tab_wizard:$("#tab_wizard").val(),
	  // },success: function(data) {
		  // $.toaster({priority : 'success', title : 'Succes!', message : ' Wizard'});
		  
		// }
	// });
}
</script>