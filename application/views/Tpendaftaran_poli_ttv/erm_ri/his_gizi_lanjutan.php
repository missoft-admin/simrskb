<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	 <?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	 .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='his_gizi_lanjutan'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_gizi_lanjutan' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
						$disabel_input='';
						$disabel_input_ttv='';
						// if ($status_ttv!='1'){
							// $disabel_input_ttv='disabled';
						// }
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_assesmen=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/his_gizi_lanjutan" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-xs-12" for="st_owned">Tanggal</label>
											<div class="col-xs-12">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="st_owned">Owned By Me</label>
											<div class="col-xs-12">
												<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0">TIDAK</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
											<div class="col-xs-12">
												<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
											<div class="col-xs-12">
												
												<div class="input-group">
                                                    <select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
                                                    </span>
                                                </div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_pencarian_history">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="10%">Action</th>
													<th width="20%">Versi</th>
													<th width="20%">Created</th>
													<th width="20%">Edited</th>
													<th width="25%">Alasan</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-2 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-7 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >RIWAYAT KLIEN</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-3">
									<label for="tinggi_badan">Umur</label>
									<div class="input-group">
										<input class="form-control" readonly type="text"  value="{umurtahun}"  placeholder="" >
										<span class="input-group-addon">Tahun</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="tinggi_badan">&nbsp;</label>
									<div class="input-group">
										<input class="form-control" readonly type="text"  value="{umurbulan}"  placeholder="" >
										<span class="input-group-addon">Bulan</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="tinggi_badan">&nbsp;</label>
									<div class="input-group">
										<input class="form-control" readonly type="text"  value="{umurhari}"  placeholder="" >
										<span class="input-group-addon">Hari</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="tinggi_badan">Jenis Kelamin</label>
									<input class="form-control" readonly type="text"  value="{jk}"  placeholder="" >
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4">
									<label for="tinggi_badan">Etnik / Agama</label>
									<input class="form-control" readonly type="text"  value="<?=get_nama_ref($agama_id,'3')?>"  placeholder="" >
								</div>
								<div class="col-md-4">
									<label for="tinggi_badan">Pekerjaan</label>
									<input class="form-control" readonly type="text"  value="<?=get_nama_ref($pekerjaan,'6')?>"  placeholder="" >
								</div>
								<div class="col-md-4">
									<label for="tinggi_badan">Pendidikan</label>
									<input class="form-control" readonly type="text"  value="<?=get_nama_ref($pendidikan,'13')?>"  placeholder="" >
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="mobilitas">Mobilitas</label>
									<select id="mobilitas" name="mobilitas" class="<?=($mobilitas!=$mobilitas_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($mobilitas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(257) as $row){?>
										<option value="<?=$row->id?>"  <?=($mobilitas == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($mobilitas == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="perokok">Perokok</label>
									<select id="perokok" name="perokok" class="<?=($perokok!=$perokok_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($perokok == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(259) as $row){?>
										<option value="<?=$row->id?>"  <?=($perokok == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($perokok == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="riwayat_medis">Riwayat Medis / Kesehatan Pasien / Keluarga</label>
									<input id="riwayat_medis" class="<?=($riwayat_medis!=$riwayat_medis_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{riwayat_medis}">
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="diagnosa_medis">Diagnosa Medis / Dokter</label>
									<input id="diagnosa_medis" class="<?=($diagnosa_medis!=$diagnosa_medis_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{diagnosa_medis}">
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >RIWAYAT DIET</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<label class="col-xs-12">Alergi Makanan </label>
								<div class="col-md-12">
									<select id="riwayat_alergi" name="riwayat_alergi" class="<?=($riwayat_alergi!=$riwayat_alergi_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_alergi=='0'?'selected':'')?>>Tidak Ada Alergi</option>
										<option value="1" <?=($riwayat_alergi=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_alergi=='2'?'selected':'')?>>Ada Alergi</option>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="pantangan_makan">Pantangan Makanan</label>
									<input id="pantangan_makan" class="<?=($pantangan_makan!=$pantangan_makan_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{pantangan_makan}">
								</div>
							</div>
						</div>
						<div class="form-group div_alergi_all">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="block">
										<ul class="nav nav-tabs nav-tabs-alt nav-justified" data-toggle="tabs">
											<li class="" id="li_alergi_1">
												<a href="#alergi_1"  onclick="load_alergi()"><i class="fa fa-pencil "></i> Data Alergi</a>
											</li>
											<li class="" id="li_alergi_2">
												<a href=" #alergi_2" onclick="load_alergi_his()"><i class="fa fa-history"></i> History Alergi</a>
											</li>
											
										</ul>
										<div class="block-content tab-content ">
											<div class="tab-pane " id="alergi_1">
												<div class="table-responsive div_data_alergi">
													<table class="table" id="index_alergi">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="tab-pane " id="alergi_2">
												<div class="table-responsive">
													<table class="table" id="index_alergi_his">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							
							</div>
							<div class="col-md-6 ">
									
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="tisak_suka_makanan">Ketidaksukaan Makanan</label>
									<input id="tisak_suka_makanan" class="<?=($tisak_suka_makanan!=$tisak_suka_makanan_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{tisak_suka_makanan}">
								</div>
							</div>
							<div class="col-md-6 ">
								<label class="col-xs-12">Pengalaman Konseling / Diet </label>
								<div class="col-md-12">
									<select id="pengalaman_konseling" name="pengalaman_konseling" class="<?=($pengalaman_konseling!=$pengalaman_konseling_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pengalaman_konseling == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(260) as $row){?>
										<option value="<?=$row->id?>"  <?=($pengalaman_konseling == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengalaman_konseling == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >ANTROPOMETRI</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-15px;!important">
							<div class="col-md-5 ">
								<div class="col-md-4">
									<label for="berat_badan_biasa">Berat Badan Biasanya</label>
									<div class="input-group">
										<input id="berat_badan_biasa" class="<?=($berat_badan_biasa!=$berat_badan_biasa_asal?'edited':'')?> form-control decimal"  type="text"  value="{berat_badan_biasa}"  placeholder="BB Biasanya" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="berat_badan">Berat Badan Saat Ini</label>
									<div class="input-group">
										<input id="berat_badan" class="<?=($berat_badan!=$berat_badan_asal?'edited':'')?> form-control decimal"  type="text"  value="{berat_badan}"  placeholder="BB Saat Ini" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="tinggi_badan">Tinggi Badan</label>
									<div class="input-group">
										<input id="tinggi_badan" class="<?=($tinggi_badan!=$tinggi_badan_asal?'edited':'')?> form-control decimal"  type="text"  value="{tinggi_badan}"  placeholder="Tinggi Badan" required>
										<span class="input-group-addon">Cm</span>
									</div>
								</div>
							</div>
							<div class="col-md-7 ">
								<div class="col-md-3">
									<label for="nilai_imt">IMT</label>
									<input id="nilai_imt" class="<?=($nilai_imt!=$nilai_imt_asal?'edited':'')?> form-control decimal" readonly type="text"  value="{nilai_imt}"  placeholder="IMT" required>
								</div>
								<div class="col-md-3">
									<label for="nilai_imt_text">Status Gizi</label>
									<input id="nilai_imt_text" class="<?=($nilai_imt_text!=$nilai_imt_text_asal?'edited':'')?> form-control"  type="text" readonly value="{nilai_imt_text}"  placeholder="" required>
									<input id="nilai_imt_id" class="<?=($nilai_imt_id!=$nilai_imt_id_asal?'edited':'')?> form-control"  type="hidden" readonly value="{nilai_imt_id}"  placeholder="" required>
								</div>
								<div class="col-md-3">
									<label for="penurunan_bb_persen">Penurunan BB (%)</label>
									<div class="input-group">
										<input id="penurunan_bb_persen" class="<?=($penurunan_bb_persen!=$penurunan_bb_persen_asal?'edited':'')?> form-control decimal"  type="text"  value="{penurunan_bb_persen}"  placeholder="" required>
										<span class="input-group-addon">%</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="penurunan_bb_bulan">Penurunan BB Mg / bln</label>
									<div class="input-group">
										<input id="penurunan_bb_bulan" class="<?=($penurunan_bb_bulan!=$penurunan_bb_bulan_asal?'edited':'')?> form-control number"  type="text"  value="{penurunan_bb_bulan}"  placeholder="" required>
										<span class="input-group-addon">Mg/Bln</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="pengukuran_lain">Pengukuran Lainnya</label>
									<input id="pengukuran_lain" class="<?=($pengukuran_lain!=$pengukuran_lain_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{pengukuran_lain}">
								</div>
							</div>
							
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								<label class="text-primary" >BIOKIMIA TERKAIT GIZI </h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="bioikimia">Biokimia</label>
									<input id="bioikimia" class="<?=($bioikimia!=$bioikimia_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{bioikimia}">
								</div>
							</div>
							
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >FISIK KLINIS GIZI</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="udem">Udem</label>
									<select  id="udem"  class="<?=($udem!=$udem_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($udem == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(261) as $row){?>
										<option value="<?=$row->id?>" <?=($udem == $row->id ? 'selected="selected"' : '')?> <?=($udem == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="artofi">Artofi Otot Lengan</label>
									<select  id="artofi"  class="<?=($artofi!=$artofi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($artofi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(262) as $row){?>
										<option value="<?=$row->id?>" <?=($artofi == $row->id ? 'selected="selected"' : '')?> <?=($artofi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="mual">Mual</label>
									<select  id="mual"  class="<?=($mual!=$mual_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($mual == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(31) as $row){?>
										<option value="<?=$row->id?>" <?=($mual == $row->id ? 'selected="selected"' : '')?> <?=($mual == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="nafsu_makan">Nafsu Makan</label>
									<select  id="nafsu_makan"  class="<?=($nafsu_makan!=$nafsu_makan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($nafsu_makan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(263) as $row){?>
										<option value="<?=$row->id?>" <?=($nafsu_makan == $row->id ? 'selected="selected"' : '')?> <?=($nafsu_makan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="muntah">Muntah</label>
									<select  id="muntah"  class="<?=($muntah!=$muntah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($muntah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(32) as $row){?>
										<option value="<?=$row->id?>" <?=($muntah == $row->id ? 'selected="selected"' : '')?> <?=($muntah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="hilang_lemak">Hilang Lemak Subukutan</label>
									<select  id="hilang_lemak"  class="<?=($hilang_lemak!=$hilang_lemak_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($hilang_lemak == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(146) as $row){?>
										<option value="<?=$row->id?>" <?=($hilang_lemak == $row->id ? 'selected="selected"' : '')?> <?=($hilang_lemak == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="kembung">Kembung</label>
									<select  id="kembung"  class="<?=($kembung!=$kembung_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kembung == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(265) as $row){?>
										<option value="<?=$row->id?>" <?=($kembung == $row->id ? 'selected="selected"' : '')?> <?=($kembung == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="diare">Diare</label>
									<select  id="diare"  class="<?=($diare!=$diare_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($diare == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(266) as $row){?>
										<option value="<?=$row->id?>" <?=($diare == $row->id ? 'selected="selected"' : '')?> <?=($diare == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="konstipasi">Konstipasi</label>
									<select  id="konstipasi"  class="<?=($konstipasi!=$konstipasi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($konstipasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(31) as $row){?>
										<option value="<?=$row->id?>" <?=($konstipasi == $row->id ? 'selected="selected"' : '')?> <?=($konstipasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="menelan">Gangguan Menelan</label>
									<select  id="menelan"  class="<?=($menelan!=$menelan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($menelan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(263) as $row){?>
										<option value="<?=$row->id?>" <?=($menelan == $row->id ? 'selected="selected"' : '')?> <?=($menelan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="mengunyah">Gangguan Mengunyah</label>
									<select  id="mengunyah"  class="<?=($mengunyah!=$mengunyah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($mengunyah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(32) as $row){?>
										<option value="<?=$row->id?>" <?=($mengunyah == $row->id ? 'selected="selected"' : '')?> <?=($mengunyah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="ttv">Tanda Tanda Vital</label>
									<input id="ttv" class="<?=($ttv!=$ttv_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{ttv}">
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="data_lain">Data Lain</label>
									<input id="data_lain" class="<?=($data_lain!=$data_lain_asal?'edited':'')?> form-control " type="text" style="width: 100%;" placeholder="" value="{data_lain}">
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="text-primary" >DIAGNOSA GIZI, INTERVENSI DAN RENCANA MONITORING EVALUASI GIZI</h5>
							</div>
							<div class="col-md-6 ">
								<label class="text-primary" >INTERVENSI DAN RENCANA MONITORING EVALUASI GIZI</h5>
							</div>
						</div>
						<div class="form-group pull-5-t" style="margin-top:-20px;!important">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Diagnosa Gizi</th>
													<th width="10%">Priority</th>
													<th width="25%">User</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa</label>
									<select  id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
								<div class="col-md-12 push-10-t">
									<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
										<thead></thead>
										<tbody>
											<tr>
												<td colspan="2" style="width:100%"><label class="text-primary">Section</label></td>
											</tr>
											<tr>
												<td colspan="2" style="width:100%"><label class="text-success">Kategori</label></td>
											</tr>
											<tr>
												<td style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
												<td  style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" style="width:100%"><label class="text-success">Kategori</label></td>
											</tr>
											<tr>
												<td style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
												<td  style="width:50%">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input type="checkbox" value="1"> Option 1
														</label>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	$(".number").number(true,0,'.',',');
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		// clear_input_alergi();
		list_index_history_edit();
		set_riwayat_alergi();
		load_alergi();
		load_alergi_his();
		refresh_diagnosa();
		load_diagnosa();
		load_data_rencana_asuhan();
	}
	// load_data_rencana_asuhan(1);
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_gizi_lanjutan', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
$("#riwayat_alergi").on("change", function(){
	set_riwayat_alergi();
});
function set_riwayat_alergi(){
	let riwayat=$("#riwayat_alergi").val();
	if (riwayat=='2'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").show();
		$(".div_input_alergi").show();
		$("#li_alergi_1").addClass('active');
		$("#li_alergi_2").removeClass('active');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else if (riwayat=='1'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").hide();
		$(".div_input_alergi").hide();
		// // $('#alergi_2 > a').tab('show');
		// $('#alergi_1 > a').tab('hide');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else{
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
		$(".div_alergi_all").hide();
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	
		
	}
	
}
// $(document).on("keyup","#berat_badan,#tinggi_badan",function(){
	// let berat_badan=parseFloat($("#berat_badan").val());
	// let tinggi_badan=parseFloat($("#tinggi_badan").val());
	// let imt=(parseFloat(berat_badan)) /((tinggi_badan/100)*(tinggi_badan/100));

	// $("#nilai_imt").val(imt);
	// let assesmen_id=$("#assesmen_id").val();
	// $.ajax({
	// url: '{site_url}Tpendaftaran_ranap_erm/get_gizi', 
	  // dataType: "json",
	  // type: 'POST',
	  // data: {
			// assesmen_id:assesmen_id,
			// masa_tubuh:imt,
	  // }
	  // ,success: function(data) {
		
		// $("#nilai_imt_id").val(data.nilai_imt_id);
		// $("#nilai_imt_text").val(data.nilai_imt_text);
			// simpan_assesmen();
		// }
	// });
	
// });

function disabel_edit(){
	$("#form1 :input").prop("disabled", true);
	 $("#diagnosa_id_list").removeAttr('disabled');
	 $(".btn_rencana").removeAttr('disabled');
	 $(".his_filter").removeAttr('disabled');
	 $(".data_asuhan").removeAttr('disabled');
	 $('.edited').css('background', '#fff2f1');
}
	function load_alergi(){
		$('#alergi_2 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_lanjutan', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							versi_edit:$("#versi_edit").val(),
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	
	function load_alergi_his(){
		$('#alergi_1 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_alergi_his').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi_his').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi_his', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_alergi_pasien(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function load_diagnosa(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}Thistory_tindakan/load_diagnosa_gizi_lanjutan', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
					 // alert('sono');
				 }  
			});
		refresh_diagnosa();
	}
	function refresh_diagnosa(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_gizi_lanjutan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				// alert($("#diagnosa_id_list").val());
				$("#diagnosa_id_list").val($("#diagnosa_id_list").val()).trigger('change');
				
			}
		});
	}
	$("#diagnosa_id_list").on("change", function(){
		load_data_rencana_asuhan($(this).val());
	});
	function load_data_rencana_asuhan(diagnosa_id){
		let assesmen_id=$("#assesmen_id").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Thistory_tindakan/load_data_rencana_asuhan_gizi_lanjutan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_id,
					versi_edit:$("#versi_edit").val(),
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan tbody").append(data);
			}
		});
	}
	function set_rencana_asuhan(id){
		 $("#diagnosa_id_list").val(id).trigger('change');
	}
	$(document).on('click','.data_asuhan',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/update_data_lanjutan', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	
</script>