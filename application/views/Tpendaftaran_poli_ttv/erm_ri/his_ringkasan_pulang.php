<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_ringkasan_pulang'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_ringkasan_pulang' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_sga=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_sga=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_sga=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
									<h5 class="font-w700 push-5 text-center text-primary"><i>RIWAYAT {judul_header_eng}</i></h5>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									<button class="btn btn-default btn_ttd menu_click" type="button" onclick="goBack()"><i class="fa fa-reply"></i> Kembali</button>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_sga" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="indikasi_rawat"><?=$indikasi_rawat_ina?></label> <?=($indikasi_rawat != $indikasi_rawat_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="indikasi_rawat"  rows="2"> <?=$indikasi_rawat?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="ringkasan"><?=$ringkasan_ina?></label> <?=($ringkasan != $ringkasan_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="ringkasan"  rows="2"> <?=$ringkasan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="pemeriksaan"><?=$pemeriksaan_ina?></label> <?=($pemeriksaan != $pemeriksaan_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="pemeriksaan"  rows="2"> <?=$pemeriksaan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="penunjang"><?=$penunjang_ina?></label> <?=($penunjang != $penunjang_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="penunjang"  rows="2"> <?=$penunjang?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="konsultasi"><?=$konsultasi_ina?></label> <?=($konsultasi != $konsultasi_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="konsultasi"  rows="2"> <?=$konsultasi?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="therapi"><?=$therapi_ina?></label> <?=($therapi != $therapi_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="therapi"  rows="2"> <?=$therapi?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="therapi_pulang"><?=$therapi_pulang_ina?></label> <?=($therapi_pulang != $therapi_pulang_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="therapi_pulang"  rows="2"> <?=$therapi_pulang?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="komplikasi"><?=$komplikasi_ina?></label> <?=($komplikasi != $komplikasi_asal?text_danger('EDITED'):'')?>
										<textarea class="form-control js-summernote auto_blur" id="komplikasi"  rows="2"> <?=$komplikasi?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-2">
										<label><?=$prognis_ina?></label>
										<select id="prognis" name="prognis" class="<?=($prognis!=$prognis_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(415) as $row){?>
											<option value="<?=$row->id?>" <?=($prognis == $row->id ? 'selected="selected"' : '')?> <?=(($prognis == '0' || $prognis == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-2">
										<label><?=$cara_pulang_ina?></label>
										<select id="cara_pulang" name="cara_pulang" class="<?=($cara_pulang!=$cara_pulang_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(416) as $row){?>
											<option value="<?=$row->id?>" <?=($cara_pulang == $row->id ? 'selected="selected"' : '')?> <?=(($cara_pulang == '0' || $cara_pulang == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-2">
										<label><?=$intuksi_fu_ina?></label>
										<select id="intuksi_fu" name="intuksi_fu" class="<?=($intuksi_fu!=$intuksi_fu_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(43) as $row){?>
											<option value="<?=$row->id?>" <?=($intuksi_fu == $row->id ? 'selected="selected"' : '')?> <?=(($intuksi_fu == '0' || $intuksi_fu == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-2">
										<label>TANGGAL KONTROL</label>
										<div class="input-group date">
											<input type="text" class="js-datepicker form-control <?=($tanggal_kontrol!=$tanggal_kontrol_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tanggal_kontrol" placeholder="dd-mm-yyyy" value="<?= $tanggal_kontrol ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-4">
										<label>KETERANGAN LAIN</label>
										<textarea class="form-control <?=($keterangan!=$keterangan_asal?'edited':'')?>" id="keterangan"  rows="1" placeholder=""><?=$keterangan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-12">
										<label><?=$diagnosa_utama_ina?></label>
										<input class="form-control <?=($diagnosa_utama!=$diagnosa_utama_asal?'edited':'')?>" type="text" id="diagnosa_utama"  value="<?=$diagnosa_utama?>" placeholder="" >
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-striped" id="tabel_diagnosa">
												<thead>
													<tr>
														<th width="5%">No</th>
														<th width="40%">Diagnosa Tambahan</th>
														<th width="15%">Priority</th>
														<th width="20%">User</th>
													</tr>
												</thead>
												<tbody></tbody>
											</table>
										</div>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-striped" id="tabel_tindakan">
												<thead>
													<tr>
														<th width="5%">No</th>
														<th width="40%">Tindakan / Prosedur</th>
														<th width="15%">Priority</th>
														<th width="20%">User</th>
													</tr>
													
												</thead>
												<tbody></tbody>
											</table>
										</div>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<table width="100%">
										<tr>
											<td width="25" class="text-center"><strong><?=$dpjp_ina?></strong><?=($dpjp_eng?'<br><i>'.$dpjp_eng.'</i>':'')?></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center"><strong><?=$keluarga_ina?></strong><?=($keluarga_eng?'<br><i>'.$keluarga_eng.'</i>':'')?></td>
										</tr>
										<tr>
											<td width="25" class="text-center"><strong>
												<img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dpjp; ?>" width="100px">
											</strong></td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<?if ($pasien_ttd){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img" style="width:60%;align:center" align="middle" src="<?=$pasien_ttd?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_pernyataan()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
											</td>
										</tr>
										<tr>
											<td width="25" class="text-center">
											<strong><?=get_nama_dokter_ttd($dpjp)?></strong> 
											<?if ($status_assemen!='2'){?>
											<br>
											<button class="btn btn-xs btn-default" onclick="modal_ttd_petugas()"  type="button"><i class="fa fa-paint-brush"></i></button>
											<?}?>
											</td>
											<td width="50" class="text-center"></td>
											<td width="25" class="text-center">
												<strong>
													<?if ($pasien_nama){?>
														(<?=($pasien_nama)?>)
													<?}?>
												</strong>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	$('.js-summernote').summernote({
		  height: 100,   //set editable area's height
			dialogsInBody: true,
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
		
	if (assesmen_id){
		load_diagnosa();
		load_tindakan();
		list_index_history_edit();
		load_awal_assesmen=false;
	}
});
function load_diagnosa(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_diagnosa_ringkasan_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		// refresh_diagnosa();
	}
	function load_tindakan(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_tindakan').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_tindakan').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_tindakan_ringkasan_pulang', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
	}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_ringkasan_pulang', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}

</script>