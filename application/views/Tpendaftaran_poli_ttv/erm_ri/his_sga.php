<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_sga'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_sga' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_sga=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_sga=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_sga=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
									<h5 class="font-w700 push-5 text-center text-primary"><i>RIWAYAT {judul_header_eng}</i></h5>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_sga" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4">
									<label for="berat_badan_biasa">Berat Badan Biasanya</label>
									<div class="input-group">
										<input id="berat_badan_biasa" class="<?=($berat_badan_biasa!=$berat_badan_biasa_asal?'edited':'')?> form-control decimal"  type="text"  value="{berat_badan_biasa}"  placeholder="BB Biasanya" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="berat_badan">Berat Badan Saat Ini</label>
									<div class="input-group">
										<input id="berat_badan" class="<?=($berat_badan!=$berat_badan_asal?'edited':'')?> form-control decimal"  type="text"  value="{berat_badan}"  placeholder="BB Saat Ini" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="waktu">Waktu</label>
									<input id="waktu" class="<?=($waktu!=$waktu_asal?'edited':'')?> form-control"  type="text"  value="{waktu}"  placeholder="" required>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4">
									<label for="tinggi_badan">Tinggi Badan</label>
									<div class="input-group">
										<input id="tinggi_badan" class="<?=($tinggi_badan!=$tinggi_badan_asal?'edited':'')?> form-control decimal"  type="text"  value="{tinggi_badan}"  placeholder="Tinggi Badan" required>
										<span class="input-group-addon">Cm</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="nilai_imt">IMT</label>
									<input id="nilai_imt" class="<?=($nilai_imt!=$nilai_imt_asal?'edited':'')?> form-control decimal" readonly type="text"  value="{nilai_imt}"  placeholder="IMT" required>
								</div>
								<div class="col-md-5">
									<label for="nilai_imt_text">Status Gizi</label>
									<input id="nilai_imt_text" class="<?=($nilai_imt_text!=$nilai_imt_text_asal?'edited':'')?> form-control"  type="text" readonly value="{nilai_imt_text}"  placeholder="" required>
									<input id="nilai_imt_id" class="form-control"  type="hidden" readonly value="{nilai_imt_id}"  placeholder="" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="diagnosa_medis">Diagnosa Medis</label>
									<input id="diagnosa_medis" class="<?=($diagnosa_medis!=$diagnosa_medis_asal?'edited':'')?> form-control"  type="text"  value="{diagnosa_medis}"  placeholder="" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="footer_pengkajian"><?=$header_pengkajian?></label>
									<input id="footer_pengkajian" class="form-control"  type="hidden"  value="{footer_pengkajian}"  placeholder="">
									
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-header-bg" id="tabel_skrining">
											<thead>
												<tr>
													<th width="5%" class="text-center">No</th>
													<th width="80%">Parameter</th>
													<th width="15%" class="text-center">Penilaian</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot>
												<tr>
													<td colspan="2"><?=$footer_pengkajian?></td>
													<td ><input id="nilai_sga" class="<?=($nilai_sga!=$nilai_sga_asal?'edited':'')?> form-control text-center"  type="text" readonly value="{nilai_sga}"  placeholder="" required></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-4">
									<label for="penilaian_sga">PENILAIAN SUBJECTIVE ASSESMEN (SGA)</label>
									<input id="penilaian_sga" class="<?=($penilaian_sga!=$penilaian_sga_asal?'edited':'')?> form-control" readonly  type="text"  value="{penilaian_sga}"  placeholder="" required>
								</div>
								<div class="col-md-4">
									<label for="tindakan_sga">TINDAK LANJUT</label>
									<input id="tindakan_sga" class="<?=($tindakan_sga!=$tindakan_sga_asal?'edited':'')?> form-control" readonly type="text"  value="{tindakan_sga}"  placeholder="" required>
								</div>
								<div class="col-md-4">
									<label for="keterangan">Keterangan</label>
									<input id="keterangan" class="<?=($keterangan!=$keterangan_asal?'edited':'')?> form-control"  type="text"  value="{keterangan}"  placeholder="" required>
								</div>
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_sga();
		list_index_history_edit();
		
		load_awal_assesmen=false;
	}
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_sga', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function load_skrining_sga(){
	let assesmen_id=$("#assesmen_id").val();
	let footer_pengkajian=$("#footer_pengkajian").val();
	let versi_edit=$("#versi_edit").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_sga', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				footer_pengkajian:footer_pengkajian,
				versi_edit:versi_edit,
				
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			$(".nilai").select2();
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}
function get_perhitungan_skor(){

	let assesmen_id=$("#assesmen_id").val();
	let nilai_imt_id=$("#nilai_imt_id").val();
	let nilai_sga_id=$("#nilai_sga_id").val();
	// alert(nilai_sga_id);
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_perhitungan_skor', 
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				nilai_imt_id:nilai_imt_id,
				nilai_sga_id:nilai_sga_id,
		  }
		  ,success: function(data) {
			// $("#nilai_sga_id").val(data.nilai_sga_id);
			$("#penilaian_sga").val(data.penilaian_sga);
			$("#tindakan_sga").val(data.tindakan_sga);
				
			}
		});
	
}

function perhitungan_gizi(){
	let tinggi_badan=$("#tinggi_badan").val();
	let berat_badan=$("#berat_badan").val();
	  
	let masa_tubuh=0;
	// H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100))
	masa_tubuh=(parseFloat(berat_badan))/((parseFloat(tinggi_badan)/100)*(parseFloat(tinggi_badan)/100));
	$("#nilai_imt").val(masa_tubuh);
	$.ajax({
		url: '{site_url}tpendaftaran_ranap_erm/get_gizi', 
		dataType: "JSON",
		method: "POST",
		data : {
				masa_tubuh:masa_tubuh,
				
			   },
		success: function(data) {
			$("#nilai_imt_id").val(data.nilai_imt_id);
			$("#nilai_imt_text").val(data.nilai_imt_text);
			// $("#cover-spin").hide();
			get_perhitungan_skor();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
		}
	});
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}

</script>