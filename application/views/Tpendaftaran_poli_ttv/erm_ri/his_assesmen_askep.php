<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_assesmen_askep'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_assesmen_askep' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$tanggaldaftar_asal=HumanDateShort($tanggal_input_asal);
						$waktudaftar=HumanTime($tanggal_input);
						$tanggaldaftar_asal=HumanDateShort($tanggal_input_asal);
						$waktudaftar_asal=HumanTime($tanggal_input_asal);
						if ($tiba_dikamar_beda){
							$tgl_tiba_dikamar_beda=HumanDateShort($tiba_dikamar_beda);
							$waktu_tiba_dikamar_beda=HumanTime($tiba_dikamar_beda);
						}else{
							$tgl_tiba_dikamar_beda=date('d-m-Y');
							$waktu_tiba_dikamar_beda=date('H:i:s');
						}
						if ($masuk_jam){
							$tgl_masuk_jam=HumanDateShort($masuk_jam);
							$waktu_masuk_jam=HumanTime($masuk_jam);
						}else{
							$tgl_masuk_jam=date('d-m-Y');
							$waktu_masuk_jam=date('H:i:s');
						}
						if ($keluar_jam){
							$tgl_keluar_jam=HumanDateShort($keluar_jam);
							$waktu_keluar_jam=HumanTime($keluar_jam);
						}else{
							$tgl_keluar_jam=date('d-m-Y');
							$waktu_keluar_jam=date('H:i:s');
						}
						if ($memanggil_perawat){
							$tgl_memanggil_perawat=HumanDateShort($memanggil_perawat);
							$waktu_memanggil_perawat=HumanTime($memanggil_perawat);
						}else{
							$tgl_memanggil_perawat=date('d-m-Y');
							$waktu_memanggil_perawat=date('H:i:s');
						}
						if ($perawat_datang){
							$tgl_perawat_datang=HumanDateShort($perawat_datang);
							$waktu_perawat_datang=HumanTime($perawat_datang);
						}else{
							$tgl_perawat_datang=date('d-m-Y');
							$waktu_perawat_datang=date('H:i:s');
						}
						if ($pemberitahuan){
							$tgl_pemberitahuan=HumanDateShort($pemberitahuan);
							$waktu_pemberitahuan=HumanTime($pemberitahuan);
						}else{
							$tgl_pemberitahuan=date('d-m-Y');
							$waktu_pemberitahuan=date('H:i:s');
						}

						//AWAL
						if ($tiba_dikamar_beda){
							$tgl_tiba_dikamar_beda_asal=HumanDateShort($tiba_dikamar_beda_asal);
							$waktu_tiba_dikamar_beda_asal=HumanTime($tiba_dikamar_beda_asal);
						}else{
							$tgl_tiba_dikamar_beda_asal=date('d-m-Y');
							$waktu_tiba_dikamar_beda_asal=date('H:i:s');
						}
						if ($masuk_jam){
							$tgl_masuk_jam_asal=HumanDateShort($masuk_jam_asal);
							$waktu_masuk_jam_asal=HumanTime($masuk_jam_asal);
						}else{
							$tgl_masuk_jam_asal=date('d-m-Y');
							$waktu_masuk_jam_asal=date('H:i:s');
						}
						if ($keluar_jam){
							$tgl_keluar_jam_asal=HumanDateShort($keluar_jam_asal);
							$waktu_keluar_jam_asal=HumanTime($keluar_jam_asal);
						}else{
							$tgl_keluar_jam_asal=date('d-m-Y');
							$waktu_keluar_jam_asal=date('H:i:s');
						}
						if ($memanggil_perawat){
							$tgl_memanggil_perawat_asal=HumanDateShort($memanggil_perawat_asal);
							$waktu_memanggil_perawat_asal=HumanTime($memanggil_perawat_asal);
						}else{
							$tgl_memanggil_perawat_asal=date('d-m-Y');
							$waktu_memanggil_perawat_asal=date('H:i:s');
						}
						if ($perawat_datang){
							$tgl_perawat_datang_asal=HumanDateShort($perawat_datang_asal);
							$waktu_perawat_datang_asal=HumanTime($perawat_datang_asal);
						}else{
							$tgl_perawat_datang_asal=date('d-m-Y');
							$waktu_perawat_datang_asal=date('H:i:s');
						}
						if ($pemberitahuan){
							$tgl_pemberitahuan_asal=HumanDateShort($pemberitahuan_asal);
							$waktu_pemberitahuan_asal=HumanTime($pemberitahuan_asal);
						}else{
							$tgl_pemberitahuan_asal=date('d-m-Y');
							$waktu_pemberitahuan_asal=date('H:i:s');
						}
						
					}else{
						$tanggaldaftar=date('d-m-Y');
						$waktudaftar=date('H:i:s');
						$tgl_tiba_dikamar_beda=date('d-m-Y');
						$waktu_tiba_dikamar_beda=date('H:i:s');
						
						$tgl_masuk_jam=date('d-m-Y');
						$waktu_masuk_jam=date('H:i:s');
						
						$tgl_keluar_jam=date('d-m-Y');
						$waktu_keluar_jam=date('H:i:s');
						$tgl_memanggil_perawat=date('d-m-Y');
						$waktu_memanggil_perawat=date('H:i:s');
						$tgl_perawat_datang=date('d-m-Y');
						$waktu_perawat_datang=date('H:i:s');
						$tgl_pemberitahuan=date('d-m-Y');
						$waktu_pemberitahuan=date('H:i:s');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_assesmen_askep=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >	
						<input type="hidden" id="st_tornique" value="<?=$st_tornique?>" >		
						<input type="hidden" id="tab_wizard" value="<?=($tab_wizard==''?'1':$tab_wizard)?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="pull-right push-10-r">
								<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_gizi_anak" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control <?=($tanggaldaftar!=$tanggaldaftar_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control <?=($waktudaftar!=$waktudaftar_asal?'edited':'')?>" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<!--BATS AKRHI -->
						<?if ($assesmen_id !=''){?>
						<div class="form-group">
							<div class="col-md-12">
								 <!-- Simple Classic Progress Wizard (.js-wizard-simple class is initialized in js/pages/base_forms_wizard.js) -->
                            <!-- For more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/ -->
                            <div class="js-wizard-simple block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="<?=($tab_wizard=='1'?'active':'')?>">
                                        <a href="#simple-classic-progress-step1" onclick="set_tab_wizard(1)" data-toggle="tab">STEP 1</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='2'?'active':'')?>">
                                        <a href="#simple-classic-progress-step2" onclick="set_tab_wizard(2)" data-toggle="tab">STEP 2</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='3'?'active':'')?>">
                                        <a href="#simple-classic-progress-step3" onclick="set_tab_wizard(3)" data-toggle="tab">STEP 3</a>
                                    </li>
									<li class="<?=($tab_wizard=='4'?'active':'')?>">
                                        <a href="#simple-classic-progress-step4" onclick="set_tab_wizard(4)" data-toggle="tab">STEP 4</a>
                                    </li>
									<li class="<?=($tab_wizard=='5'?'active':'')?>">
                                        <a href="#simple-classic-progress-step5" onclick="set_tab_wizard(5)" data-toggle="tab">STEP 5</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="form-horizontal" action="base_forms_wizard.html" method="post">
                                    <!-- Steps Progress -->
                                    <div class="block-content block-content-mini block-content-full border-b">
                                        <div class="wizard-progress progress progress-mini remove-margin-b">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
                                        </div>
                                    </div>
                                    <!-- END Steps Progress -->

                                    <!-- Steps Content -->
                                    <div class="block-content tab-content">
                                        <!-- Step 1 -->
                                        <div class="tab-pane fade fade-up   push-50  <?=($tab_wizard=='1'?'in active':'')?>" id="simple-classic-progress-step1">
											
                                           <div class="form-group">
												<div class="col-md-6 ">
													<label for="diagnosa">Diagnosa</label>
													<input id="diagnosa" class="form-control <?=($diagnosa!=$diagnosa_asal?'edited':'')?>" type="text"  value="{diagnosa}" name="diagnosa" placeholder="Diagnosa" >
												</div>
												
												<div class="col-md-4 ">
													<label for="tindakan">Tindakan</label>
													<input id="tindakan" class="form-control <?=($tindakan!=$tindakan_asal?'edited':'')?>" type="text"  value="{tindakan}" name="tindakan" placeholder="Tindakan" >
												</div>
												<div class="col-md-2 ">
													<label for="tindakan">Tanggal Pembedahan</label>
													<div class="input-group date">
														<input type="text" class="js-datepicker form-control <?=($tanggal_bedah!=$tanggal_bedah_asal?'edited':'')?>" data-date-format="dd-mm-yyyy" id="tanggal_bedah" placeholder="HH/BB/TTTT" value="<?=($tanggal_bedah?HumanDateShort($tanggal_bedah):HumanDateShort(date('Y-m-d')))?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN OPERASI (Disi Oleh Perawat Ruangan)</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_awal">Keadaan Umum Pra Operasi</label>
													<select id="tingkat_kesadaran_awal"   name="tingkat_kesadaran_awal" class="<?=($tingkat_kesadaran_awal!=$tingkat_kesadaran_awal_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_awal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="gcs_e">GCS</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_e!=$gcs_e_asal?'edited':'')?>"  type="text" id="gcs_e" name="gcs_e" value="{gcs_e}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_m!=$gcs_m_asal?'edited':'')?>"  type="text" id="gcs_m" name="gcs_m" value="{gcs_m}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_v!=$gcs_v_asal?'edited':'')?>"  type="text" id="gcs_v" name="gcs_v" value="{gcs_v}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1">Pupil</label>
													<div class="input-group">
														<input class="form-control  <?=($pupil_1!=$pupil_1_asal?'edited':'')?>"  type="text" id="pupil_1" name="pupil_1" value="{pupil_1}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  <?=($pupil_2!=$pupil_2_asal?'edited':'')?>"  type="text" id="pupil_2" name="pupil_2" value="{pupil_2}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pernafasan">Pernafasan</label>
													<select id="pernafasan" class="<?=($pernafasan!=$pernafasan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pernafasan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(327) as $row){?>
														<option value="<?=$row->id?>" <?=($pernafasan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pernafasan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2 ">
													<label for="pernafasan_lain">Keterangan Lain Pernafasan</label>
													<input id="pernafasan_lain" class="form-control auto_blur" type="text"  value="{pernafasan_lain}" name="pernafasan_lain" placeholder="" >
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5" >TANDA VITAL</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv">Tanda Vital Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control <?=($jam_tv!=$jam_tv_asal?'edited':'')?>" id="jam_tv" name="jam_tv" value="<?= ($jam_tv?$jam_tv:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran"   name="tingkat_kesadaran" class="<?=($tingkat_kesadaran!=$tingkat_kesadaran_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input class="form-control decimal <?=($td_sistole!=$td_sistole_asal?'edited':'')?>"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input class="form-control decimal <?=($td_diastole!=$td_diastole_asal?'edited':'')?>"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input class="form-control decimal <?=($suhu!=$suhu_asal?'edited':'')?>"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi">Frekuensi Nadi</label>
													<div class="input-group">
														<input class="form-control decimal <?=($nadi!=$nadi_asal?'edited':'')?>"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas">Frekuensi Nafas</label>
													<div class="input-group">
														<input class="form-control decimal <?=($nafas!=$nafas_asal?'edited':'')?>"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2">SpO2</label>
													<div class="input-group">
														<input class="form-control decimal <?=($spo2!=$spo2_asal?'edited':'')?>"   type="text" id="spo2" name="spo2" value="{spo2}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Tinggi Badan</label>
													<div class="input-group">
														<input class="form-control decimal <?=($tinggi_badan!=$tinggi_badan_asal?'edited':'')?>"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
														<span class="input-group-addon">Cm</span>
														
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Berat Badan</label>
													<div class="input-group">
														<input class="form-control decimal <?=($berat_badan!=$berat_badan_asal?'edited':'')?>"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
														<span class="input-group-addon">Kg</span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-3">
													<label for="visite_do">Visite Dokter Operator Sebelum Operasi</label>
													<select id="visite_do" class="<?=($visite_do!=$visite_do_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($visite_do == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(328) as $row){?>
														<option value="<?=$row->id?>" <?=($visite_do == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($visite_do == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="visite_da">Visite Dokter Anestesi Sebelum Operasi</label>
													<select id="visite_da" class="<?=($visite_da!=$visite_da_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($visite_da == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(328) as $row){?>
														<option value="<?=$row->id?>" <?=($visite_da == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($visite_da == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<?
												$riwayat_penyakit=($riwayat_penyakit_id?explode(',',$riwayat_penyakit_id):array());
											?>
											<div class="form-group">
												<div class="col-md-6">
													<label for="riwayat_penyakit_id">Riwayat Penyakit</label>
													<select id="riwayat_penyakit_id" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<?foreach(list_variable_ref(329) as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$riwayat_penyakit)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
													</select>
													<textarea id="riwayat_penyakit_nama" style="margin-top:10px" class="form-control <?=($riwayat_penyakit_nama!=$riwayat_penyakit_nama_asal?'edited':'')?>" name="riwayat_penyakit_nama" rows="3" style="width:100%"><?=$riwayat_penyakit_nama?></textarea>
												</div>
												<div class="col-md-6">
														<label for="perawatan">Pengobatan Sekarang / Paramedikasi</label>
														<div class="table-responsive">
															<table class="table table table-bordered table-striped" id="tabel_tindakan">
																<thead>
																	
																	<tr>
																		<th width="5%">No</th>
																		<th width="35%">Nama Pengobatan</th>
																		<th width="30%">Dosis</th>
																		<th width="20%">Jam</th>
																		<th width="10%">Action</th>
																	</tr>
																</thead>
																<tbody></tbody>
															</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-3">
													<label for="riwayat_operasi">Riwayat Operasi</label>
													<select id="riwayat_operasi" class="<?=($riwayat_operasi!=$riwayat_operasi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($riwayat_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(330) as $row){?>
														<option value="<?=$row->id?>" <?=($riwayat_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($riwayat_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="riwayat_operasi_ket">Sebutkan Jika Ya</label>
													<input id="riwayat_operasi_ket" class="form-control auto_blur" type="text"  value="{riwayat_operasi_ket}" name="riwayat_operasi_ket" placeholder="" >
												</div>
												<div class="col-md-3">
													<label for="pem_lab">Pemeriksaan Labolatorium</label>
													<select id="pem_lab" class="<?=($pem_lab!=$pem_lab_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pem_lab == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(331) as $row){?>
														<option value="<?=$row->id?>" <?=($pem_lab == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pem_lab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="hasil_lab">Hasil Terlampir</label>
													<select id="hasil_lab" class="<?=($hasil_lab!=$hasil_lab_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($hasil_lab == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(332) as $row){?>
														<option value="<?=$row->id?>" <?=($hasil_lab == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($hasil_lab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="persiapan_darah">Persiapan Darah</label>
													<select id="persiapan_darah" class="<?=($persiapan_darah!=$persiapan_darah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($persiapan_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(333) as $row){?>
														<option value="<?=$row->id?>" <?=($persiapan_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($persiapan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_darah">Pilih Jenis Darah Jika Ya</label>
													<select id="jenis_darah" class="<?=($jenis_darah!=$jenis_darah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(334) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jumlah_darah">Jumlah</label>
													<input id="jumlah_darah" class="form-control <?=($jumlah_darah!=$jumlah_darah_asal?'edited':'')?>" type="text"  value="{jumlah_darah}" name="jumlah_darah" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="alat_invasi">Alat Invasif Terpasang</label>
													<select id="alat_invasi" class="<?=($alat_invasi!=$alat_invasi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($alat_invasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(335) as $row){?>
														<option value="<?=$row->id?>" <?=($alat_invasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($alat_invasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="minum_jamu">Riw Minum Jamu/Tradisional</label>
													<select id="minum_jamu" class="<?=($minum_jamu!=$minum_jamu_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($minum_jamu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(336) as $row){?>
														<option value="<?=$row->id?>" <?=($minum_jamu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($minum_jamu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_jamu">Jenis Jamu</label>
													<input id="jenis_jamu" class="form-control <?=($jenis_jamu!=$jenis_jamu_asal?'edited':'')?>" type="text"  value="{jenis_jamu}" name="jenis_jamu" placeholder="" >
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="hasil_rad">Hasil Radiologi</label>
													<select id="hasil_rad" class="<?=($hasil_rad!=$hasil_rad_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($hasil_rad == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(337) as $row){?>
														<option value="<?=$row->id?>" <?=($hasil_rad == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($hasil_rad == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_rad">Pilih Jenis  Jika Ya</label>
													<input id="jenis_rad" class="form-control <?=($jenis_rad!=$jenis_rad_asal?'edited':'')?>" type="text"  value="{jenis_rad}" name="jenis_rad" placeholder="" >
													
												</div>
												<div class="col-md-2">
													<label for="lembar_rad">Jumlah Lembar</label>
													<input id="lembar_rad" class="form-control <?=($lembar_rad!=$lembar_rad_asal?'edited':'')?>" type="text"  value="{lembar_rad}" name="lembar_rad" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="st_emosional">Status Emosional</label>
													<select id="st_emosional" class="<?=($st_emosional!=$st_emosional_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($st_emosional == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(338) as $row){?>
														<option value="<?=$row->id?>" <?=($st_emosional == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_emosional == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="icu">Persiapan ICU</label>
													<select id="icu" class="<?=($icu!=$icu_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($icu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(339) as $row){?>
														<option value="<?=$row->id?>" <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($icu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="icu_acc">Surat Persetujuan ICU, Bila Ya</label>
													<select id="icu_acc" class="<?=($icu_acc!=$icu_acc_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($icu_acc == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(340) as $row){?>
														<option value="<?=$row->id?>" <?=($icu_acc == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($icu_acc == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-3">
													<label for="izin_operasi">Surat Izin Operasi</label>
													<select id="izin_operasi" class="<?=($izin_operasi!=$izin_operasi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($izin_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(341) as $row){?>
														<option value="<?=$row->id?>" <?=($izin_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($izin_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="izin_anestesi">Surat Izin Anestesi</label>
													<select id="izin_anestesi" class="<?=($izin_anestesi!=$izin_anestesi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($izin_anestesi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(342) as $row){?>
														<option value="<?=$row->id?>" <?=($izin_anestesi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($izin_anestesi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN PRA OPERASI (Disi Oleh Perawat Ruangan & Kamar Operasi)</h5>
												</div>
											</div>
											<div class="form-group">
												
												<div class="col-md-2 ">
													<label for="izin_anestesi">Tiba Dikamar Bedah</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_tiba_dikamar_beda!=$tgl_tiba_dikamar_beda_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_tiba_dikamar_beda" placeholder="HH/BB/TTTT" name="tgl_tiba_dikamar_beda" value="<?= $tgl_tiba_dikamar_beda ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="izin_anestesi">Waktu</label>
													<div class=" input-group">
														<input  type="text" class="time-datepicker form-control <?=($waktu_tiba_dikamar_beda!=$waktu_tiba_dikamar_beda_asal?'edited':'')?>" id="waktu_tiba_dikamar_beda" name="waktu_tiba_dikamar_beda" value="<?= $waktu_tiba_dikamar_beda ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_awal_opr">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_awal_opr"   name="tingkat_kesadaran_awal_opr" class="<?=($tingkat_kesadaran_awal_opr!=$tingkat_kesadaran_awal_opr_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_awal_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="gcs_e">GCS</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_e_opr!=$gcs_e_opr_asal?'edited':'')?>"  type="text" id="gcs_e_opr" name="gcs_e_opr" value="{gcs_e_opr}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_m_opr!=$gcs_m_opr_asal?'edited':'')?>"  type="text" id="gcs_m_opr" name="gcs_m_opr" value="{gcs_m_opr}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_v_opr!=$gcs_v_opr_asal?'edited':'')?>"  type="text" id="gcs_v_opr" name="gcs_v_opr" value="{gcs_v_opr}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1">Pupil</label>
													<div class="input-group">
														<input class="form-control  <?=($pupil_1_opr!=$pupil_1_opr_asal?'edited':'')?>"  type="text" id="pupil_1_opr" name="pupil_1_opr" value="{pupil_1_opr}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  <?=($pupil_2_opr!=$pupil_2_opr_asal?'edited':'')?>"  type="text" id="pupil_2_opr" name="pupil_2_opr" value="{pupil_2_opr}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label for="pernafasan_opr">Pernafasan</label>
													<select id="pernafasan_opr" class="<?=($pernafasan_opr!=$pernafasan_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pernafasan_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(327) as $row){?>
														<option value="<?=$row->id?>" <?=($pernafasan_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pernafasan_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="pernafasan_lain_opr">Keterangan Lain Pernafasan</label>
													<input id="pernafasan_lain_opr" class="form-control <?=($pernafasan_lain_opr!=$pernafasan_lain_opr_asal?'edited':'')?>" type="text"  value="{pernafasan_lain_opr}" name="pernafasan_lain_opr" placeholder="" >
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5" >TANDA VITAL</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv_opr">Tanda Vital Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control <?=($jam_tv_opr!=$jam_tv_opr_asal?'edited':'')?>" id="jam_tv_opr" name="jam_tv_opr" value="<?= ($jam_tv_opr?$jam_tv_opr:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran_opr">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_opr"   name="tingkat_kesadaran_opr" class="<?=($tingkat_kesadaran_opr!=$tingkat_kesadaran_opr_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input class="form-control decimal <?=($td_sistole_opr!=$td_sistole_opr_asal?'edited':'')?>"  type="text" id="td_sistole_opr"  value="{td_sistole_opr}" name="td_sistole_opr" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input class="form-control decimal <?=($td_diastole_opr!=$td_diastole_opr_asal?'edited':'')?>"  type="text" id="td_diastole_opr" value="{td_diastole_opr}"  name="td_diastole_opr" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input class="form-control decimal <?=($suhu_opr!=$suhu_opr_asal?'edited':'')?>"  type="text" id="suhu_opr" name="suhu_opr" value="{suhu_opr}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi_opr">Frekuensi Nadi</label>
													<div class="input-group">
														<input class="form-control decimal <?=($nadi_opr!=$nadi_opr_asal?'edited':'')?>"   type="text" id="nadi_opr" name="nadi_opr" value="{nadi_opr}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas_opr">Frekuensi Nafas</label>
													<div class="input-group">
														<input class="form-control decimal <?=($nafas_opr!=$nafas_opr_asal?'edited':'')?>"   type="text" id="nafas_opr" name="nafas_opr" value="{nafas_opr}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_opr">SpO2</label>
													<div class="input-group">
														<input class="form-control decimal <?=($spo2_opr!=$spo2_opr_asal?'edited':'')?>"   type="text" id="spo2_opr" name="spo2_opr" value="{spo2_opr}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Tinggi Badan</label>
													<div class="input-group">
														<input class="form-control decimal <?=($tinggi_badan_opr!=$tinggi_badan_opr_asal?'edited':'')?>"   type="text" id="tinggi_badan_opr"  value="{tinggi_badan_opr}" name="tinggi_badan_opr" placeholder="cm" required>
														<span class="input-group-addon">Cm</span>
														
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Berat Badan</label>
													<div class="input-group">
														<input class="form-control decimal <?=($berat_badan_opr!=$berat_badan_opr_asal?'edited':'')?>"  type="text" id="berat_badan_opr"  value="{berat_badan_opr}" name="berat_badan_opr" placeholder="Kg" required>
														<span class="input-group-addon">Kg</span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_ic">
															<thead>
																
																<tr>
																	<th width="5%">NO</th>
																	<th width="45%">LINE</th>
																	<th width="20%">LOKASI</th>
																	<th width="20%">KONDISI</th>
																	<th width="10%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top:-10px">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
															<thead>
																<tr>
																	<th width="5%" class="text-center">NO</th>
																	<th width="30%" class="text-center">JENIS PARAMETER</th>
																	<?
																	$q="SELECT H.jenis as id,M.ref as nama 
																	FROM `tranap_assesmen_askep_pengkajian` H 
																	INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='386'
																	WHERE H.assesmen_id='$assesmen_id'
																	GROUP BY H.jenis
																	ORDER BY H.jenis";
																	$list_header=$this->db->query($q)->result();
																	$jml_kolom=count($list_header);
																	$lebar_kolom=50 / $jml_kolom;
																	foreach($list_header as $r){
																	?>
																	<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
																	
																	<?}?>
																	<th width="15%" class="text-center">KETERANGAN</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<table width="100%">
														<tr>
															<td width="25" class="text-center"><strong>PERAWAT RUANGAN</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center"><strong>PERAWAT KAMAR BEDAH</strong></td>
														</tr>
														<tr>
															<td width="25" class="text-center"><strong>
																<?if ($perawat_ruangan_ttd){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_ruangan_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_ruangan_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT RUANGAN</button>
																<?}?>
															</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_ruangan_bedah_ttd){?>
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_ruangan_bedah_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan_bedah()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_ruangan_bedah_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_bedah()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT KAMAR BEDAH</button>
																<?}?>
															</td>
														</tr>
														<tr>
															<td width="25" class="text-center">
															<?if ($perawat_ruangan_id){?>
															<strong>( <?=get_nama_ppa($perawat_ruangan_id)?> )</strong> 
															<?}?>
															</td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_ruangan_bedah_id){?>
																<strong>( <?=get_nama_ppa($perawat_ruangan_bedah_id)?> )</strong> 
															<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>
											
											
										</div>
                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='2'?'in active':'')?>" id="simple-classic-progress-step2">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN INTRA OPERASI (Disi Oleh Perawat Kamar Bedah)</h5>
												</div>
											</div>
											<div class="form-group" style="margin-bottom=-10px">
												<div class="col-md-6 ">
													<label class="h5 " ><strong>DIAGNOSA KEPERAWATAN</strong></h5>
												</div>
												<div class="col-md-6 ">
													<label class="h5 " ><strong>RENCANA ASUHAN KEPERAWATAN</strong></h5>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-6 ">
													<?if($status_assemen !='2'){?>
													<div class="table-responsive">
														<table class="table" id="index_header">
															<thead>
																<tr>
																	<th width="55%">
																		Diagnosa
																	</th>
																	<th width="25%">
																		Priority
																	</th>
																	<th width="20%">
																		
																	</th>
																</tr>
																<tr>
																	<th width="55%">
																		<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
																		<select id="mdiagnosa_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																			<option value="" selected>Pilih Opsi</option>
																			<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="25%">
																		<select id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																			
																			<?for($i=1;$i<20;$i++){?>
																			<option value="<?=$i?>"><?=$i?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="20%">
																		<span class="input-group-btn">
																			<button class="btn btn-info" onclick="simpan_diagnosa_askep()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
																			<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
																		</span>
																	</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
													<?}?>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_diagnosa">
															<thead>
																<tr>
																	<th width="5%">No</th>
																	<th width="40%">Diagnosa Keperawatan</th>
																	<th width="10%">Priority</th>
																	<th width="25%">User</th>
																	<th width="20%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<div class="col-md-12 ">
														<label for="example-input-normal">Diagnosa</label>
														<select id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
															
														</select>
													</div>
													<div class="col-md-12 push-10-t">
														<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
															<thead></thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-4">
													<label for="catatan_diagnosa">Catatan</label>
													<input id="catatan_diagnosa" class="form-control <?=($catatan_diagnosa!=$catatan_diagnosa_asal?'edited':'')?>" type="text"  value="{catatan_diagnosa}" name="catatan_diagnosa" placeholder="" >
													
												</div>
												<div class="col-md-2">
													<label for="respon_mata">Respon Membuka Mata</label>
													<select id="respon_mata" class="<?=($respon_mata!=$respon_mata_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($respon_mata == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(344) as $row){?>
														<option value="<?=$row->id?>" <?=($respon_mata == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($respon_mata == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-3">
													<label for="respon_verbal">Respon Verbal</label>
													<select id="respon_verbal" class="<?=($respon_verbal!=$respon_verbal_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($respon_verbal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(345) as $row){?>
														<option value="<?=$row->id?>" <?=($respon_verbal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($respon_verbal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="respon_motorik">Respon Motorik</label>
													<select id="respon_motorik" class="<?=($respon_motorik!=$respon_motorik_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($respon_motorik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(346) as $row){?>
														<option value="<?=$row->id?>" <?=($respon_motorik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($respon_motorik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												
											</div>
											<div class="form-group">
												<div class="col-md-6 ">
													<label for="tabel_anestesi">Anestesi</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_anestesi">
															<thead>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<div class=" input-group">
																			<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_jam_anestesi!=$tgl_jam_anestesi_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_jam_anestesi" placeholder="HH/BB/TTTT" name="tgl_jam_anestesi" value="<?= $tgl_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			<input  type="text" class="time-datepicker form-control <?=($waktu_jam_anestesi!=$waktu_jam_anestesi_asal?'edited':'')?>" id="waktu_jam_anestesi" name="waktu_jam_anestesi" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	
																	
																	<th >
																		<button class="btn btn-info" onclick="simpan_anestesi()" id="btn_add_anestesi" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																<tr>
																	<th width="5%">NO</th>
																	<th width="45%">ANESTESI</th>
																	<th width="10%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<label for="tabel_anestesi">Pembedahan</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_pembedahan">
															<thead>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<div class=" input-group">
																			<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_jam_pembedahan!=$tgl_jam_pembedahan_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_jam_pembedahan" placeholder="HH/BB/TTTT" name="tgl_jam_pembedahan" value="<?= $tgl_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			<input  type="text" class="time-datepicker form-control <?=($waktu_jam_pembedahan!=$waktu_jam_pembedahan_asal?'edited':'')?>" id="waktu_jam_pembedahan" name="waktu_jam_pembedahan" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	
																	
																	<th >
																		<button class="btn btn-info" onclick="simpan_pembedahan()" id="btn_add_pembedahan" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																<tr>
																	<th width="5%">NO</th>
																	<th width="45%">PEMBEDAHAN</th>
																	<th width="10%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tipe_opr">Tipe Operasi</label>
													<select id="tipe_opr" class="<?=($tipe_opr!=$tipe_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($tipe_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(347) as $row){?>
														<option value="<?=$row->id?>" <?=($tipe_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tipe_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_bius">Jenis Pembiusan</label>
													<select id="jenis_bius" class="<?=($jenis_bius!=$jenis_bius_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_bius == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(348) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_bius == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bius == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="kesadaran_opr">Kesadaran</label>
													<select id="kesadaran_opr" class="<?=($kesadaran_opr!=$kesadaran_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kesadaran_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(349) as $row){?>
														<option value="<?=$row->id?>" <?=($kesadaran_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kesadaran_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="st_emosi_opr">Status Emosi</label>
													<select id="st_emosi_opr" class="<?=($st_emosi_opr!=$st_emosi_opr_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($st_emosi_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(350) as $row){?>
														<option value="<?=$row->id?>" <?=($st_emosi_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_emosi_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-2">
													<label for="canul">Posisi Canul Intra Vena</label>
													<select id="canul" class="<?=($canul!=$canul_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($canul == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(351) as $row){?>
														<option value="<?=$row->id?>" <?=($canul == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($canul == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_operasi">Jenis Operasi</label>
													<select id="jenis_operasi" class="<?=($jenis_operasi!=$jenis_operasi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(352) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="posisi_operasi">Posisi Operasi</label>
													<select id="posisi_operasi" class="<?=($posisi_operasi!=$posisi_operasi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_operasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(353) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_operasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_operasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<?
												$arr_diawasi=($diawasi?explode(',',$diawasi):array());
												$arr_dipasang_oleh=($dipasang_oleh?explode(',',$dipasang_oleh):array());
												$arr_dipasang_netral=($dipasang_netral?explode(',',$dipasang_netral):array());
												?>
												<div class="col-md-2">
													<label for="diawasi">Diawasi Oleh</label>
													<select id="diawasi" name="diawasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$arr_diawasi)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
														
													</select>
													
												</div>
												<div class="col-md-2">
													<label for="posisi_lengan">Posisi Lengan Tangan</label>
													<select id="posisi_lengan" class="<?=($posisi_lengan!=$posisi_lengan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_lengan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(354) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_lengan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_lengan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="kulit">Kulit Dibersihkan Menggunakan</label>
													<select id="kulit" class="<?=($kulit!=$kulit_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kulit == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(355) as $row){?>
														<option value="<?=$row->id?>" <?=($kulit == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kulit == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="urine">Urine Caterer</label>
													<select id="urine" class="<?=($urine!=$urine_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($urine == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(356) as $row){?>
														<option value="<?=$row->id?>" <?=($urine == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($urine == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-2">
													<label for="dipasang_oleh">Dipasang Oleh</label>
													<select id="dipasang_oleh" name="dipasang_oleh" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$arr_dipasang_oleh)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
														
													</select>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="disinfeksi">Disinfeksi Kulit</label>
													<select id="disinfeksi" class="<?=($disinfeksi!=$disinfeksi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($disinfeksi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(357) as $row){?>
														<option value="<?=$row->id?>" <?=($disinfeksi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($disinfeksi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-2">
													<label for="disinfeksi_lain">Disinfeksi Kulit Lainnya</label>
													<input id="disinfeksi_lain" class="form-control <?=($disinfeksi_lain!=$disinfeksi_lain_asal?'edited':'')?>" type="text"  value="{disinfeksi_lain}" name="disinfeksi_lain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="posisi_alat">Posisi Alat Bantu Yg Digunakan</label>
													<select id="posisi_alat" class="<?=($posisi_alat!=$posisi_alat_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_alat == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(358) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_alat == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_alat == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="posisi_alat_lain">Posisi Alat Bantu Lainnya</label>
													<input id="posisi_alat_lain" class="form-control <?=($posisi_alat_lain!=$posisi_alat_lain_asal?'edited':'')?>" type="text"  value="{posisi_alat_lain}" name="posisi_alat_lain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="diatermi">Pemakaian Diatermi</label>
													<select id="diatermi" class="<?=($diatermi!=$diatermi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($diatermi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(359) as $row){?>
														<option value="<?=$row->id?>" <?=($diatermi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($diatermi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="lokasi_netral">Lokasi Netral Elektrode</label>
													<select id="lokasi_netral" class="<?=($lokasi_netral!=$lokasi_netral_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($lokasi_netral == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(360) as $row){?>
														<option value="<?=$row->id?>" <?=($lokasi_netral == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($lokasi_netral == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="dipasang_netral">Dipasang Oleh</label>
													<select id="dipasang_netral" name="dipasang_netral" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=(in_array($r->id,$arr_dipasang_netral)?'selected':'')?>><?=$r->nama?></option>
														<?}?>
														
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pemeriksaan_kulit">Pemeriksaaan Kulit Sebelum Operasi</label>
													<select id="pemeriksaan_kulit" class="<?=($pemeriksaan_kulit!=$pemeriksaan_kulit_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemeriksaan_kulit == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(361) as $row){?>
														<option value="<?=$row->id?>" <?=($pemeriksaan_kulit == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemeriksaan_kulit == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pemeriksaan_kulit_setelah">Pemeriksaaan Kulit Setelah Operasi</label>
													<select id="pemeriksaan_kulit_setelah" class="<?=($pemeriksaan_kulit_setelah!=$pemeriksaan_kulit_setelah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemeriksaan_kulit_setelah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(362) as $row){?>
														<option value="<?=$row->id?>" <?=($pemeriksaan_kulit_setelah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemeriksaan_kulit_setelah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pemasangan_warm">Pemasangan Warm Blanket</label>
													<select id="pemasangan_warm" class="<?=($pemasangan_warm!=$pemasangan_warm_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemasangan_warm == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(363) as $row){?>
														<option value="<?=$row->id?>" <?=($pemasangan_warm == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemasangan_warm == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_warm">Jenis </label>
													<input id="jenis_warm" class="form-control <?=($jenis_warm!=$jenis_warm_asal?'edited':'')?>" type="text"  value="{jenis_warm}" name="jenis_warm" placeholder="" >
												</div>
												<?
													$jam_mulai=($jam_mulai?HumanTime($jam_mulai):HumanTime(date('H:i:s')));
													$jam_selesai=($jam_selesai?HumanTime($jam_selesai):HumanTime(date('H:i:s')));
												?>
												<div class="col-md-1">
													<label for="jam_mulai">Mulai </label>
														<input  type="text" class="time-datepicker form-control <?=($jam_mulai!=$jam_mulai_asal?'edited':'')?>" id="jam_mulai" name="jam_mulai" value="<?= $jam_mulai ?>" required>
												</div>
												<div class="col-md-1">
													<label for="jam_selesai">Selesai </label>
														<input  type="text" class="time-datepicker form-control <?=($jam_selesai!=$jam_selesai_asal?'edited':'')?>" id="jam_selesai" name="jam_selesai" value="<?= $jam_selesai ?>" required>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label class="css-input switch switch-sm switch-primary">
														<input type="checkbox" id="chck_st_tornique" <?=($st_tornique=='0'?'':'checked')?>><span></span> <strong>Pemakaian Tornique</strong>
													</label>
												</div>
												
											</div>
											<div class="form-group div_tornique" style="margin-top: -10px;">
												<div class="col-md-12">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_tornique">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="35%">LOKASI</th>
																	<th width="10%" class="text-center">JAM MULAI</th>
																	<th width="10%" class="text-center">JAM SELESAI</th>
																	<th width="30%" class="text-center">DIPASANG OLEH</th>
																	<th width="10%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<div class="input-group">
																			<select id="lokasi_tornique" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																				<option value="0" selected>-Tentukan Lokasi-</option>
																				<?foreach(list_variable_ref(364) as $row){?>
																				<option value="<?=$row->id?>"><?=$row->nama?></option>
																				<?}?>
																				
																			</select>
																			<span class="input-group-btn">
																				<button class="btn btn-primary" title="Tambah Lokasi" onclick="add_referensi('lokasi_tornique','364','Lokasi Tornique')" type="button"><i class="fa fa-plus"></i></button>
																			</span>
																		</div>
																		
																	</th>
																	<th>
																		<div class=" input-group">
																			<input  type="text" class="time-datepicker form-control" id="mulai_tornique" name="mulai_tornique" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	
																	
																	<th >
																		<div class=" input-group">
																			<input  type="text" class="time-datepicker form-control" id="selesai_tornique" name="selesai_tornique" value="<?= $waktu_tiba_dikamar_beda ?>" required>
																			<span class="input-group-addon"><i class="si si-clock"></i></span>
																		</div>
																	</th>
																	<th >
																		<select id="dipasang_tornique" name="dipasang_tornique" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
																			<option value="0" selected>- Pilih Petugas -</option>
																			<?foreach($list_ppa as $r){?>
																			<option value="<?=$r->id?>"><?=$r->nama?></option>
																			<?}?>
																			
																			
																		</select>
																	</th>
																	<th >
																		<button class="btn btn-info" onclick="simpan_tornique()" id="btn_add_tornique" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												
											</div>	
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="pemakaian_implan">Pemakaian Implant</label>
													<select id="pemakaian_implan" class="<?=($pemakaian_implan!=$pemakaian_implan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pemakaian_implan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(365) as $row){?>
														<option value="<?=$row->id?>" <?=($pemakaian_implan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemakaian_implan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_implan">Jenis  Imiplant</label>
													<input id="jenis_implan" class="form-control <?=($jenis_implan!=$jenis_implan_asal?'edited':'')?>" type="text"  value="{jenis_implan}" name="jenis_implan" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="lokasi_implan">Lokasi Implan</label>
													<input id="lokasi_implan" class="form-control <?=($lokasi_implan!=$lokasi_implan_asal?'edited':'')?>" type="text"  value="{lokasi_implan}" name="lokasi_implan" placeholder="" >
												</div>
												
												<div class="col-md-2">
													<label for="drain">Pemakaian Drain</label>
													<select id="drain" class="<?=($drain!=$drain_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($drain == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(366) as $row){?>
														<option value="<?=$row->id?>" <?=($drain == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($drain == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_drain">Jenis  Drain</label>
													<input id="jenis_drain" class="form-control <?=($jenis_drain!=$jenis_drain_asal?'edited':'')?>" type="text"  value="{jenis_drain}" name="jenis_drain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="lokasi_drain">Lokasi Drain</label>
													<input id="lokasi_drain" class="form-control <?=($lokasi_drain!=$lokasi_drain_asal?'edited':'')?>" type="text"  value="{lokasi_drain}" name="lokasi_drain" placeholder="" >
												</div>
											</div>						
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="irigasi_luka">Irigasi Luka</label>
													<select id="irigasi_luka" class="<?=($irigasi_luka!=$irigasi_luka_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($irigasi_luka == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(367) as $row){?>
														<option value="<?=$row->id?>" <?=($irigasi_luka == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($irigasi_luka == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="irigasi_luka_dengan">Irigasi Luka Dengan</label>
													<select id="irigasi_luka_dengan" class="<?=($irigasi_luka_dengan!=$irigasi_luka_dengan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($irigasi_luka_dengan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(368) as $row){?>
														<option value="<?=$row->id?>" <?=($irigasi_luka_dengan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($irigasi_luka_dengan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="irigasi_luka_lain">Irigasi Lainnya</label>
													<input id="irigasi_luka_lain" class="form-control <?=($irigasi_luka_lain!=$irigasi_luka_lain_asal?'edited':'')?>" type="text"  value="{irigasi_luka_lain}" name="irigasi_luka_lain" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="tampon">Tampon</label>
													<select id="tampon" class="<?=($tampon!=$tampon_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($tampon == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(369) as $row){?>
														<option value="<?=$row->id?>" <?=($tampon == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tampon == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<textarea id="riwayat_penyakit_nama" style="margin-top:10px" class="form-control <?=($riwayat_penyakit_nama!=$riwayat_penyakit_nama_asal?'edited':'')?>" name="riwayat_penyakit_nama" rows="3" style="width:100%"><?=$riwayat_penyakit_nama?></textarea>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="tabel_cairan">Pemakaian Cairan Inpus</label> 
													
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_cairan">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="20%" class="text-center">INPUT</th>
																	<th width="20%" class="text-center">JML (CC)</th>
																	<th width="20%" class="text-center">OUTPUT</th>
																	<th width="20%" class="text-center">JML (CC)</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<input  type="text" class="form-control" id="input_cairan" name="input_cairan" value="" required>
																	</th>
																	<th>
																		<div class=" input-group">
																			<input  type="text" class="form-control " onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="input_jml_cairan" name="input_jml_cairan" value="" required>
																			<span class="input-group-addon">CC</span>
																		</div>
																	</th>
																	<th>
																		<input  type="text" class="form-control" id="output_cairan" name="output_cairan" value="" required>
																	</th>
																	<th>
																		<div class=" input-group">
																			<input  type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="output_jml_cairan" name="output_jml_cairan" value="" required>
																			<span class="input-group-addon">CC</span>
																		</div>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_cairan()" id="btn_add_cairan" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
															<tfoot>
															<tr>
															  <th colspan="3">Keseimbangan Cairan dan Input dan Outpupt = Balance</th>
															  <th colspan="3"><input id="keseimbangan_cairan" class="form-control <?=($keseimbangan_cairan!=$keseimbangan_cairan_asal?'edited':'')?>" type="text"  value="{keseimbangan_cairan}" name="keseimbangan_cairan" placeholder="" ></th>
															</tr>
														  </tfoot>
														</table>
													</div>
												</div>
												<div class="col-md-6">
													<label for="tabel_spesimen">Spesimen</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_spesimen">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="40%" class="text-center">SPESIMEN</th>
																	<th width="40%" class="text-center">JENIS</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<select id="spesimen_id" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="0" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(370) as $row){?>
																			<option value="<?=$row->id?>" ><?=$row->nama?></option>
																			<?}?>
																			
																		</select>
																	</th>
																	
																	<th>
																		<input  type="text" class="form-control" id="spesimen_jenis" name="spesimen_jenis" value="" required>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_spesimen()" id="btn_add_spesimen" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="tabel_cairan">Perhitungan Kassa, Jarum, Bisturi DLL</label> 
													<div class="btn-group">
													<button class="btn btn-primary btn-xs" title="Tambah Referensi" onclick="add_referensi('','384','Perhitungan Kassa Dll')" type="button"><i class="fa fa-plus"></i> Tambah Referensi</button>
													<button class="btn btn-success btn-xs" title="Tambah Referensi" onclick="refresh_kassa()" type="button"><i class="fa fa-refresh"></i> Sync Referensi</button>
													</div>
													<br>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_kassa">
															<thead>
																<tr>
																	<th width="40%" class="text-center">PARAMETER</th>
																	<th width="15%" class="text-center">SEBELUM OPERASI</th>
																	<th width="15%" class="text-center">PENAMBAHAN SELAMA OPERASI</th>
																	<th width="15%" class="text-center">JUMLAH SETELAH OPERASI</th>
																	<th width="15%" class="text-center">JUMLAH YANG DIGUNAKAN</th>
																</tr>
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-12">
													<label for="tabel_cairan">Perhitungan Cairan Instrument</label> 
													<div class="btn-group">
													<button class="btn btn-primary btn-xs" title="Tambah Referensi" onclick="add_referensi('','385','Perhitungan Cairan Instrument')" type="button"><i class="fa fa-plus"></i> Tambah Referensi</button>
													<button class="btn btn-success btn-xs" title="Sync Referensi" onclick="refresh_instrumen()" type="button"><i class="fa fa-refresh"></i> Sync Referensi</button>
													</div>
													<br>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_instrumen">
															<thead>
																<tr>
																	<th width="40%" class="text-center">PARAMETER</th>
																	<th width="15%" class="text-center">JUMLAH PRE OPERASI</th>
																	<th width="15%" class="text-center">TAMBAHAN DURANTE OPERASI</th>
																	<th width="15%" class="text-center">JUMLAH POST OPERASI</th>
																	<th width="15%" class="text-center">SELISIH</th>
																</tr>
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
											</div>
                                        </div>
                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='3'?'in active':'')?>" id="simple-classic-progress-step3">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DURANTE OPERASI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-bottom=-10px">
												<div class="col-md-6 ">
													<label class="h5 " ><strong>DIAGNOSA KEPERAWATAN</strong></h5>
												</div>
												<div class="col-md-6 ">
													<label class="h5 " ><strong>RENCANA ASUHAN KEPERAWATAN</strong></h5>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 ">
													
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_diagnosa_durante">
															<thead>
																<tr>
																	<th width="5%">No</th>
																	<th width="40%">Diagnosa Keperawatan</th>
																	<th width="10%">Priority</th>
																	<th width="25%">User</th>
																	<th width="20%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<div class="col-md-12 ">
														<label for="diagnosa_durante_id_list">Diagnosa</label>
														<select id="diagnosa_durante_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
															
														</select>
													</div>
													<div class="col-md-12 push-10-t">
														<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan_durante">
															<thead></thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="catatan_all">Catatan</label>
													<textarea class="form-control <?=($catatan_all!=$catatan_all_asal?'edited':'')?>" name="catatan_all" rows="4" id="catatan_all"><?=$catatan_all?></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<table width="100%">
														<tr>
															<td width="33" class="text-center"><strong>PERAWAT ASSISTEN / INSTRUMEN</strong></td>
															<td width="33" class="text-center"><strong>PERAWAT SIRKULER</strong></td>
															<td width="33" class="text-center"><strong>PERAWAT ANESTESI</strong></td>
														</tr>
														<tr>
															<td width="33" class="text-center">
																<strong>
																<?if ($perawat_assisten_id){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_assisten_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_asisten()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_assisten_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_asisten()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT ASSISTEN / INSTRUMEN</button>
																<?}?>
																</strong>
															</td>
															<td width="33" class="text-center">
																<strong>
																<?if ($perawat_sirkuler_id){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_sirkuler_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_sirkuler()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_sirkuler_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_sirkuler()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT SIRKULER</button>
																<?}?>
																</strong>
															</td>
															
															<td width="33" class="text-center">
																<?if ($perawat_anestesi_id){?>
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_anestesi_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_anestesi()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_anestesi_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_anestesi()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT ANESTESI</button>
																<?}?>
															</td>
														</tr>
														<tr>
															<td width="33%" class="text-center">
															<?if ($perawat_assisten_id){?>
															<strong>( <?=get_nama_ppa($perawat_assisten_id)?> )</strong> 
															<?}?>
															</td>
															<td width="33%" class="text-center">
															<?if ($perawat_sirkuler_id){?>
															<strong>( <?=get_nama_ppa($perawat_sirkuler_id)?> )</strong> 
															<?}?>
															</td>
															<td width="33%" class="text-center">
																<?if ($perawat_anestesi_id){?>
																<strong>( <?=get_nama_ppa($perawat_anestesi_id)?> )</strong> 
															<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>
											
                                        </div>
                                        <!-- END Step 3 -->
										<!-- Step 4 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='4'?'in active':'')?>" id="simple-classic-progress-step4">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >DOKUMENTASI KEPERAWATAN PASKA OPERASI</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="ruang_pemulihan">Ruang Pemulihan</label>
													<select id="ruang_pemulihan" class="<?=($ruang_pemulihan!=$ruang_pemulihan_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($ruang_pemulihan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(369) as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_pemulihan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($ruang_pemulihan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="tgl_masuk_jam">Masuk Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_masuk_jam!=$tgl_masuk_jam_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_masuk_jam" placeholder="HH/BB/TTTT" name="tgl_masuk_jam" value="<?= $tgl_masuk_jam ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control <?=($waktu_masuk_jam!=$waktu_masuk_jam_asal?'edited':'')?>" id="waktu_masuk_jam" name="waktu_masuk_jam" value="<?= $waktu_masuk_jam ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="izin_anestesi">Keluar Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_keluar_jam!=$tgl_keluar_jam_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_keluar_jam" placeholder="HH/BB/TTTT" name="tgl_keluar_jam" value="<?= $tgl_keluar_jam ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control <?=($waktu_keluar_jam!=$waktu_keluar_jam_asal?'edited':'')?>" id="waktu_keluar_jam" name="waktu_keluar_jam" value="<?= $waktu_keluar_jam ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												
												<div class="col-md-2 ">
													<label for="kembali_ke">Kembali Ke</label>
													<select id="kembali_ke" name="kembali_ke" class="<?=($kembali_ke!=$kembali_ke_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
														<option value="0" <?=($kembali_ke=='' || $kembali_ke=='0'?'selected':'')?>>- Pilih Ruangan -</option>
														<?foreach(get_all('mruangan',array('status'=>1,'st_tampil'=>1,'idtipe'=>1)) as $r){?>
														<option value="<?=$r->id?>" <?=($kembali_ke==$r->id?'selected':'')?>><?=$r->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-2 ">
													<label for="ruangan_lainnya">Ruangan lainnya</label>
													<input id="ruangan_lainnya" class="form-control <?=($ruangan_lainnya!=$ruangan_lainnya_asal?'edited':'')?>" type="text"  value="{ruangan_lainnya}" name="ruangan_lainnya" placeholder="" >
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="keadaan_paska">Keadaan Umum Paska Operasi</label>
													<select id="keadaan_paska"   name="keadaan_paska" class="<?=($keadaan_paska!=$keadaan_paska_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($keadaan_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_paska">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_paska"   name="tingkat_kesadaran_paska" class="<?=($tingkat_kesadaran_paska!=$tingkat_kesadaran_paska_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="gcs_e_paska">GCS</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_e_paska!=$gcs_e_paska_asal?'edited':'')?>"  type="text" id="gcs_e_paska" name="gcs_e_paska" value="{gcs_e_paska}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m_paska">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_m_paska!=$gcs_m_paska_asal?'edited':'')?>"  type="text" id="gcs_m_paska" name="gcs_m_paska" value="{gcs_m_paska}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v_paska">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_v_paska!=$gcs_v_paska_asal?'edited':'')?>"  type="text" id="gcs_v_paska" name="gcs_v_paska" value="{gcs_v_paska}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1_paska">Pupil</label>
													<div class="input-group">
														<input class="form-control  <?=($pupil_1_paska!=$pupil_1_paska_asal?'edited':'')?>"  type="text" id="pupil_1_paska" name="pupil_1_paska" value="{pupil_1_paska}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  <?=($pupil_2_paska!=$pupil_2_paska_asal?'edited':'')?>"  type="text" id="pupil_2_paska" name="pupil_2_paska" value="{pupil_2_paska}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="kulit_datang">Keadaan Kulit Waktu Datang</label>
													<select id="kulit_datang" class="<?=($kulit_datang!=$kulit_datang_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kulit_datang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(372) as $row){?>
														<option value="<?=$row->id?>" <?=($kulit_datang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kulit_datang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="kulit_keluar">Keadaan Kulit Waktu Keluar</label>
													<select id="kulit_keluar" class="<?=($kulit_keluar!=$kulit_keluar_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($kulit_keluar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(373) as $row){?>
														<option value="<?=$row->id?>" <?=($kulit_keluar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kulit_keluar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="sirkulasi_paska">Sirkulasi Anggota Badan</label>
													<select id="sirkulasi_paska" class="<?=($sirkulasi_paska!=$sirkulasi_paska_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($sirkulasi_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(374) as $row){?>
														<option value="<?=$row->id?>" <?=($sirkulasi_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($sirkulasi_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="posisi_paska">Posisi Pasien</label>
													<select id="posisi_paska" class="<?=($posisi_paska!=$posisi_paska_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($posisi_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(375) as $row){?>
														<option value="<?=$row->id?>" <?=($posisi_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($posisi_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="pendarahan_paska">Pendarahan</label>
													<select id="pendarahan_paska" class="<?=($pendarahan_paska!=$pendarahan_paska_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pendarahan_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(376) as $row){?>
														<option value="<?=$row->id?>" <?=($pendarahan_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pendarahan_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="lokasi_pendarahan_paska">Lokasi Pendarahan</label>
													<input id="lokasi_pendarahan_paska" class="form-control <?=($lokasi_pendarahan_paska!=$lokasi_pendarahan_paska_asal?'edited':'')?>" type="text"  value="{lokasi_pendarahan_paska}" name="lokasi_pendarahan_paska" placeholder="" >
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="muntah_paska">Muntah</label>
													<select id="muntah_paska" class="<?=($muntah_paska!=$muntah_paska_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($muntah_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(377) as $row){?>
														<option value="<?=$row->id?>" <?=($muntah_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($muntah_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="mukosa_mulut">Mukosa Mulut</label>
													<select id="mukosa_mulut" class="<?=($mukosa_mulut!=$mukosa_mulut_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($mukosa_mulut == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(378) as $row){?>
														<option value="<?=$row->id?>" <?=($mukosa_mulut == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($mukosa_mulut == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jaringan_pa">Jaringan PA & Formulir</label>
													<select id="jaringan_pa" class="<?=($jaringan_pa!=$jaringan_pa_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jaringan_pa == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(379) as $row){?>
														<option value="<?=$row->id?>" <?=($jaringan_pa == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jaringan_pa == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jaringan_pa_dari">Jika YA PA Dikirim dari</label>
													<input id="jaringan_pa_dari" class="form-control <?=($jaringan_pa_dari!=$jaringan_pa_dari_asal?'edited':'')?>" type="text"  value="{jaringan_pa_dari}" name="jaringan_pa_dari" placeholder="" >
												</div>
												<div class="col-md-2">
													<label for="skrining_nyeri">Skrining Nyeri</label>
													<select id="skrining_nyeri" class="<?=($skrining_nyeri!=$skrining_nyeri_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($skrining_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(380) as $row){?>
														<option value="<?=$row->id?>" <?=($skrining_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($skrining_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="risko_jatuh_paska">Risiko Jatuh</label>
													<select id="risko_jatuh_paska" class="<?=($risko_jatuh_paska!=$risko_jatuh_paska_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($risko_jatuh_paska == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(381) as $row){?>
														<option value="<?=$row->id?>" <?=($risko_jatuh_paska == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($risko_jatuh_paska == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
											</div>

											<div class="form-group" >
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining_opr">
															<thead>
																<tr>
																	<th width="5%" class="text-center">NO</th>
																	<th width="30%" class="text-center">JENIS PARAMETER</th>
																	<?
																	$q="SELECT H.jenis as id,M.ref as nama 
																	FROM `tranap_assesmen_askep_pengkajian_opr` H 
																	INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='387'
																	WHERE H.assesmen_id='$assesmen_id'
																	GROUP BY H.jenis
																	ORDER BY H.jenis";
																	$list_header=$this->db->query($q)->result();
																	$jml_kolom=count($list_header);
																	$lebar_kolom=65 / $jml_kolom;
																	foreach($list_header as $r){
																	?>
																	<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
																	
																	<?}?>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-bottom=-10px">
												<div class="col-md-6 ">
													<label class="h5 " ><strong>DIAGNOSA KEPERAWATAN</strong></h5>
												</div>
												<div class="col-md-6 ">
													<label class="h5 " ><strong>RENCANA ASUHAN KEPERAWATAN</strong></h5>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 ">
													<?if($status_assemen !='2'){?>
													<div class="table-responsive">
														<table class="table" id="index_header_paska">
															<thead>
																<tr>
																	<th width="55%">
																		Diagnosa
																	</th>
																	<th width="25%">
																		Priority
																	</th>
																	<th width="20%">
																		
																	</th>
																</tr>
																<tr>
																	<th width="55%">
																		<input class="form-control " type="hidden" id="diagnosa_paska_id"  value="" placeholder="" >
																		<select id="mdiagnosa_paska_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
																			<option value="" selected>Pilih Opsi</option>
																			<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="25%">
																		<select id="prioritas_paska" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Prioritas" required>
																			
																			<?for($i=1;$i<20;$i++){?>
																			<option value="<?=$i?>"><?=$i?></option>
																			<?}?>
																		</select>
																	</th>
																	<th width="20%">
																		<span class="input-group-btn">
																			<button class="btn btn-info" onclick="simpan_diagnosa_paska_askep()" id="btn_add_diagnosa_paska" type="button"><i class="fa fa-plus"></i> Add</button>
																			<button class="btn btn-warning" onclick="clear_input_diagnosa_paska()" type="button"><i class="fa fa-refresh"></i></button>
																		</span>
																	</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
													<?}?>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_diagnosa_paska">
															<thead>
																<tr>
																	<th width="5%">No</th>
																	<th width="40%">Diagnosa Keperawatan</th>
																	<th width="10%">Priority</th>
																	<th width="25%">User</th>
																	<th width="20%">Action</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6 ">
													<div class="col-md-12 ">
														<label for="diagnosa_paska_id_list">Diagnosa</label>
														<select id="diagnosa_paska_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
															
														</select>
													</div>
													<div class="col-md-12 push-10-t">
														<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan_paska">
															<thead></thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
												</div>
											</div>
											
                                        </div>
                                        <!-- END Step 4 -->
										<!-- Step 4 -->
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='5'?'in active':'')?>" id="simple-classic-progress-step5">
                                            <div class="form-group">
												<div class="col-md-12 ">
													<label class="h5 text-primary" >SERAH TERIMA DENGAN RUANGAN</h5>
												</div>
											</div>
											<div class="form-group" >
												
												<div class="col-md-3 ">
													<label for="tgl_memanggil_perawat">Memanggil Perawat Ruangan Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_memanggil_perawat!=$tgl_memanggil_perawat_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_memanggil_perawat" placeholder="HH/BB/TTTT" name="tgl_memanggil_perawat" value="<?= $tgl_memanggil_perawat ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control <?=($waktu_memanggil_perawat!=$waktu_memanggil_perawat_asal?'edited':'')?>" id="waktu_memanggil_perawat" name="waktu_memanggil_perawat" value="<?= $waktu_memanggil_perawat ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="perawat">Nama Perawat Ruangan </label>
													<select id="perawat" name="perawat" class="<?=($perawat!=$perawat_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
														<option value="#" selected>- Pilih -</option>
														<?foreach($list_ppa as $r){?>
														<option value="<?=$r->id?>" <?=($perawat==$r->id?'selected':'')?>><?=$r->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="tgl_perawat_datang">Perawat Ruangan Datang Jam</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_perawat_datang!=$tgl_perawat_datang_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_perawat_datang" placeholder="HH/BB/TTTT" name="tgl_perawat_datang" value="<?= $tgl_perawat_datang ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control <?=($waktu_perawat_datang!=$waktu_perawat_datang_asal?'edited':'')?>" id="waktu_perawat_datang" name="waktu_perawat_datang" value="<?= $waktu_perawat_datang ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6">
													<label for="tabel_post">Dokumentasi Post Operasi</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_post">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="40%" class="text-center">NAMA DOKUMENTASI</th>
																	<th width="40%" class="text-center">KETERANGAN</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<select id="dokumen_id" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="0" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(382) as $row){?>
																			<option value="<?=$row->id?>" ><?=$row->nama?></option>
																			<?}?>
																			
																		</select>
																	</th>
																	
																	<th>
																		<input  type="text" class="form-control" id="keterangan_pos" name="keterangan_pos" value="" required>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_post()" id="btn_add_post" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
												<div class="col-md-6">
													<label for="tabel_barang">Barang / Dokumen Yang Dikembalikan</label>
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_barang">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="40%" class="text-center">NAMA </th>
																	<th width="40%" class="text-center">KETERANGAN</th>
																	<th width="15%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th colspan="2">
																		<select id="dokumen_barang_id" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="0" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(383) as $row){?>
																			<option value="<?=$row->id?>" ><?=$row->nama?></option>
																			<?}?>
																			
																		</select>
																	</th>
																	
																	<th>
																		<input  type="text" class="form-control" id="keterangan_barang" name="keterangan_barang" value="" required>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_barang()" id="btn_add_barang" type="button"><i class="fa fa-plus"></i> Add</button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" >
												
												<div class="col-md-3 ">
													<label for="tgl_pemberitahuan">Pemberitahuan Kepada Keluarga Pasien</label>
													<div class=" input-group date">
														<input tabindex="2" type="text" class="js-datepicker form-control <?=($tgl_pemberitahuan!=$tgl_pemberitahuan_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tgl_pemberitahuan" placeholder="HH/BB/TTTT" name="tgl_pemberitahuan" value="<?= $tgl_pemberitahuan ?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input  type="text" class="time-datepicker form-control <?=($waktu_pemberitahuan!=$waktu_pemberitahuan_asal?'edited':'')?>" id="waktu_pemberitahuan" name="waktu_pemberitahuan" value="<?= $waktu_pemberitahuan ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-2 ">
													<label for="keterangan_pemberitahuan">Keterangan</label>
													<input id="keterangan_pemberitahuan" class="form-control <?=($keterangan_pemberitahuan!=$keterangan_pemberitahuan_asal?'edited':'')?>" type="text"  value="{keterangan_pemberitahuan}" name="keterangan_pemberitahuan" placeholder="" >
												</div>
												<div class="col-md-2 ">
													<label for="keadaan_ruangan">Keadaan Umum</label>
													<select id="keadaan_ruangan"   name="keadaan_ruangan" class="<?=($keadaan_ruangan!=$keadaan_ruangan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($keadaan_ruangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												
												<div class="col-md-1">
													<label for="gcs_e_ruangan">GCS</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_e_ruangan!=$gcs_e_ruangan_asal?'edited':'')?>"  type="text" id="gcs_e_ruangan" name="gcs_e_ruangan" value="{gcs_e_ruangan}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_m_ruangan">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_m_ruangan!=$gcs_m_ruangan_asal?'edited':'')?>"  type="text" id="gcs_m_ruangan" name="gcs_m_ruangan" value="{gcs_m_ruangan}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="gcs_v_ruangan">&nbsp;</label>
													<div class="input-group">
														<input class="form-control  <?=($gcs_v_ruangan!=$gcs_v_ruangan_asal?'edited':'')?>"  type="text" id="gcs_v_ruangan" name="gcs_v_ruangan" value="{gcs_v_ruangan}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="pupil_1_ruangan">Pupil</label>
													<div class="input-group">
														<input class="form-control  <?=($pupil_1_ruangan!=$pupil_1_ruangan_asal?'edited':'')?>"  type="text" id="pupil_1_ruangan" name="pupil_1_ruangan" value="{pupil_1_ruangan}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
														<input class="form-control  <?=($pupil_2_ruangan!=$pupil_2_ruangan_asal?'edited':'')?>"  type="text" id="pupil_2_ruangan" name="pupil_2_ruangan" value="{pupil_2_ruangan}"  placeholder="" required>
														<span class="input-group-addon"><?=$satuan_pupil?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<label class="h5" >TANDA VITAL</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv_ruangan">Tanda Vital Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control <?=($jam_tv_ruangan!=$jam_tv_ruangan_asal?'edited':'')?>" id="jam_tv_ruangan" name="jam_tv_ruangan" value="<?= ($jam_tv_ruangan?$jam_tv_ruangan:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran_ruangan">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_ruangan"   name="tingkat_kesadaran_ruangan" class="<?=($tingkat_kesadaran_ruangan!=$tingkat_kesadaran_ruangan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(23) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_ruangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input class="form-control decimal <?=($td_sistole_ruangan!=$td_sistole_ruangan_asal?'edited':'')?>"  type="text" id="td_sistole_ruangan"  value="{td_sistole_ruangan}" name="td_sistole_ruangan" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input class="form-control decimal <?=($td_diastole_ruangan!=$td_diastole_ruangan_asal?'edited':'')?>"  type="text" id="td_diastole_ruangan" value="{td_diastole_ruangan}"  name="td_diastole_ruangan" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input class="form-control decimal <?=($suhu_ruangan!=$suhu_ruangan_asal?'edited':'')?>"  type="text" id="suhu_ruangan" name="suhu_ruangan" value="{suhu_ruangan}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi_ruangan">Frekuensi Nadi</label>
													<div class="input-group">
														<input class="form-control decimal <?=($nadi_ruangan!=$nadi_ruangan_asal?'edited':'')?>"   type="text" id="nadi_ruangan" name="nadi_ruangan" value="{nadi_ruangan}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas_ruangan">Frekuensi Nafas</label>
													<div class="input-group">
														<input class="form-control decimal <?=($nafas_ruangan!=$nafas_ruangan_asal?'edited':'')?>"   type="text" id="nafas_ruangan" name="nafas_ruangan" value="{nafas_ruangan}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_ruangan">SpO2</label>
													<div class="input-group">
														<input class="form-control decimal <?=($spo2_ruangan!=$spo2_ruangan_asal?'edited':'')?>"   type="text" id="spo2_ruangan" name="spo2_ruangan" value="{spo2_ruangan}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="catatan_ruangan">Catatan</label>
													<textarea class="form-control <?=($catatan_ruangan!=$catatan_ruangan_asal?'edited':'')?>" name="catatan_ruangan" rows="4" id="catatan_ruangan"><?=$catatan_ruangan?></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<table width="100%">
														<tr>
															<td width="25" class="text-center"><strong>PERAWAT RUANGAN</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center"><strong>PERAWAT RUANGAN PEMULIHAN</strong></td>
														</tr>
														<tr>
															<td width="25" class="text-center"><strong>
																<?if ($perawat_ruangan_5_ttd){?>
																	
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_ruangan_5_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan_5()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_ruangan_5_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_5()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT RUANGAN</button>
																<?}?>
															</strong></td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_pemullihan_5_ttd){?>
																	<div class="img-container fx-img-rotate-r">
																		<img class="img-responsive" src="<?=$perawat_pemullihan_5_ttd?>" alt="">
																		<?if ($status_assemen!='2'){?>
																		<div class="img-options">
																			<div class="img-options-content">
																				<div class="btn-group btn-group-sm">
																					<a class="btn btn-default" onclick="modal_ttd_ruangan_pemulihan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																					<a class="btn btn-default btn-danger" onclick="hapus_ttd('perawat_pemullihan_5_id')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																				</div>
																			</div>
																		</div>
																		<?}?>
																	</div>
																<?}else{?>
																	<br>
																	<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_pemulihan()"  type="button"><i class="fa fa-paint-brush"></i> PERAWAT RUANG PEMULIHAN</button>
																<?}?>
															</td>
														</tr>
														<tr>
															<td width="25" class="text-center">
															<?if ($perawat_ruangan_5_id){?>
															<strong>( <?=get_nama_ppa($perawat_ruangan_5_id)?> )</strong> 
															<?}?>
															</td>
															<td width="50" class="text-center"></td>
															<td width="25" class="text-center">
																<?if ($perawat_pemullihan_5_id){?>
																<strong>( <?=get_nama_ppa($perawat_pemullihan_5_id)?> )</strong> 
															<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>					
                                        </div>
                                        <!-- END Step 4 -->
									</div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-mini block-content-full border-t">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button class="wizard-prev btn btn-default" type="button"><i class="fa fa-arrow-left"></i> Kembali</button>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="wizard-next btn btn-default" type="button">Step Selanjutnya <i class="fa fa-arrow-right"></i></button>
												
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                           
						   </div>
                            <!-- END Simple Classic Progress Wizard -->
							</div>
						</div>
						<?}?>
					
						
					</div>
					
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
		
<script type="text/javascript" src="{js_path}pages/base_forms_wizard.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_skrining_askep();
		load_skrining_opr_askep();
		// load_hasil_assesmen_askep();
		load_ic_assesmen_askep();
		load_pengobatan_assesmen_askep();
		load_diagnosa_askep();
		load_diagnosa_durante_askep();
		load_diagnosa_paska_askep();
		load_anestesi_assesmen_askep();
		load_pembedahan_assesmen_askep();
		load_status_tornique();
		load_cairan_assesmen_askep();
		load_spesimen_assesmen_askep();
		load_kassa_assesmen_askep();
		load_instrumen_assesmen_askep();
		load_barang_assesmen_askep();
		load_post_assesmen_askep();
		load_awal_assesmen=false;
	}
	list_index_history_edit();
	// load_data_rencana_asuhan_askep(1);
});
function load_post_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_post_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_post tbody").empty();
				$("#tabel_post tbody").append(data.tabel);
			}
		});
}
function load_barang_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_barang_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_barang tbody").empty();
				$("#tabel_barang tbody").append(data.tabel);
			}
		});
}
function load_instrumen_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_instrumen_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_instrumen tbody").empty();
			$("#tabel_instrumen tbody").append(data.tabel);
			// console.LOG(data);
			$('.angka').number(true, 0);
			$("#cover-spin").hide();
		}
	});
}
function load_kassa_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_kassa_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_kassa tbody").empty();
			$("#tabel_kassa tbody").append(data.tabel);
			// console.LOG(data);
			$('.angka').number(true, 0);
			$("#cover-spin").hide();
		}
	});
}
function load_spesimen_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_spesimen_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_spesimen tbody").empty();
				$("#tabel_spesimen tbody").append(data.tabel);
			}
		});
}
function load_cairan_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_cairan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_cairan tbody").empty();
				$("#tabel_cairan tbody").append(data.tabel);
			}
		});
}
function load_status_tornique(){
	let st_tornique=$("#st_tornique").val();
	if (st_tornique=='1'){
		$(".div_tornique").show();
		load_tornique_assesmen_askep();
		
	}else{
		$(".div_tornique").hide();
		
	}
}
function load_pembedahan_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_pembedahan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_pembedahan tbody").empty();
				$("#tabel_pembedahan tbody").append(data.tabel);
			}
		});
}
function load_anestesi_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_anestesi_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_anestesi tbody").empty();
				$("#tabel_anestesi tbody").append(data.tabel);
			}
		});
}
function load_diagnosa_paska_askep(){
		
	let assesmen_id=$("#assesmen_id").val();
	$('#tabel_diagnosa_paska').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#tabel_diagnosa_paska').DataTable({
			autoWidth: false,
			serverSide: true,
			"searching": false,
			"processing": false,
			"bPaginate": false,
			"bFilter": false,
			"bInfo": false,
			"order": [],
			// "pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_paska_askep', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
	refresh_diagnosa_paska_askep();
}
function load_diagnosa_durante_askep(){
	
	let assesmen_id=$("#assesmen_id").val();
	$('#tabel_diagnosa_durante').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#tabel_diagnosa_durante').DataTable({
			autoWidth: false,
			serverSide: true,
			"searching": false,
			"processing": false,
			"bPaginate": false,
			"bFilter": false,
			"bInfo": false,
			"order": [],
			// "pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_durante_askep', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
	refresh_diagnosa_durante_askep();
}
function load_diagnosa_askep(){
		
	let assesmen_id=$("#assesmen_id").val();
	$('#tabel_diagnosa').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#tabel_diagnosa').DataTable({
			autoWidth: false,
			serverSide: true,
			"searching": false,
			"processing": false,
			"bPaginate": false,
			"bFilter": false,
			"bInfo": false,
			"order": [],
			// "pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/load_diagnosa_askep', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
	refresh_diagnosa_askep();
}
function load_pengobatan_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_pengobatan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_tindakan tbody").empty();
				$("#tabel_tindakan tbody").append(data.tabel);
			}
		});
}
function load_ic_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_ic_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_ic tbody").empty();
				$("#tabel_ic tbody").append(data.tabel);
			}
		});
}
function load_skrining_opr_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_opr_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_skrining_opr tbody").append(data.opsi);
			// console.LOG(data);
			$(".nilai_opr").select2();
			// get_skor_pengkajian_askep();
			$("#cover-spin").hide();
		}
	});
}
function load_skrining_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			// console.LOG(data);
			$(".nilai").select2();
			// get_skor_pengkajian_askep();
			$("#cover-spin").hide();
		}
	});
}
$(".wizard-next").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)+1;
	set_tab_wizard(tab_wizard);
});
$(".wizard-prev").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)-1;
	set_tab_wizard(tab_wizard);
});
function set_tab_wizard($id){
	var assesmen_id=$("#assesmen_id").val();
	$("#tab_wizard").val($id);
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/set_tab_wizard_askep/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			assesmen_id:assesmen_id,
			tab_wizard:$("#tab_wizard").val(),
	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Succes!', message : ' Wizard'});
		  
		}
	});
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_askep', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
		  $(".data_asuhan").removeAttr('disabled');
		 $(".data_asuhan_durante").removeAttr('disabled');
		 $(".data_asuhan_paska").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
		 $(".wizard-prev").removeAttr('disabled');
		 $(".wizard-next").removeAttr('disabled');
		 $("#diagnosa_durante_id_list").removeAttr('disabled');
		 $("#diagnosa_paska_id_list").removeAttr('disabled');
		 $("#diagnosa_id_list").removeAttr('disabled');
	}
}

function load_skrining_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_skrining_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				versi_edit:$("#versi_edit").val(),
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			// console.LOG(data);
			// $(".nilai").select2();
			// get_skor_pengkajian_askep();
			$("#cover-spin").hide();
		}
	});
}
function load_kontrol_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_kontrol_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:$("#assesmen_id").val(),
				versi_edit:$("#versi_edit").val(),
				
			},
		success: function(data) {
			$("#tabel_kontrol tbody").empty();
			$("#tabel_kontrol tbody").append(data.tabel);
		}
	});
}	
	
function load_tindakan_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	$.ajax({
			url: '{site_url}thistory_tindakan/load_tindakan_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					versi_edit:$("#versi_edit").val(),
					
				},
			success: function(data) {
				$("#tabel_tindakan tbody").empty();
				$("#tabel_tindakan tbody").append(data.tabel);
			}
		});
}
function load_hasil_assesmen_askep(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}thistory_tindakan/load_hasil_assesmen_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					versi_edit:$("#versi_edit").val(),
					
				},
			success: function(data) {
				$("#tabel_hasil tbody").empty();
				$("#tabel_hasil tbody").append(data.tabel);
			}
		});
}
function refresh_diagnosa_askep(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_askep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				load_data_rencana_asuhan_askep();
			}
		});
	}
function refresh_diagnosa_durante_askep(){
	let assesmen_id=$("#assesmen_id").val();
	
	$("#diagnosa_durante_id_list").empty();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_durante_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
			},
		success: function(data) {
			// alert(data);
			$("#diagnosa_durante_id_list").append(data);
			load_data_rencana_asuhan_durante_askep();
		}
	});
}
function refresh_diagnosa_paska_askep(){
	let assesmen_id=$("#assesmen_id").val();
	
	$("#diagnosa_paska_id_list").empty();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_diagnosa_paska_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				
			},
		success: function(data) {
			// alert(data);
			$("#diagnosa_paska_id_list").append(data);
			load_data_rencana_asuhan_paska_askep();
		}
	});
}
function load_data_rencana_asuhan_askep(diagnosa_id){
	if (diagnosa_id){
		
	}else{
		diagnosa_id=$("#diagnosa_id_list").val()
	}
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_data_rencana_asuhan_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				diagnosa_id:diagnosa_id,
				versi_edit:versi_edit,
				
			},
		success: function(data) {
			tabel=data;
			$("#tabel_rencana_asuhan tbody").empty().html(tabel);
			$("#cover-spin").hide();
			disabel_edit();
			// console.log(data)
			// $("#tabel_rencana_asuhan tbody").append(data);
		}
	});
}
$("#diagnosa_durante_id_list").on("change", function(){
	load_data_rencana_asuhan_durante_askep($(this).val());
});
function load_data_rencana_asuhan_durante_askep(diagnosa_durante_id){
	if (diagnosa_durante_id){
		
	}else{
		diagnosa_durante_id=$("#diagnosa_durante_id_list").val()
	}
	let assesmen_id=$("#assesmen_id").val();
	let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_data_rencana_asuhan_durante_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				diagnosa_id:diagnosa_durante_id,
				
			},
		success: function(data) {
			tabel=data;
			$("#tabel_rencana_asuhan_durante tbody").empty().html(tabel);
			$("#cover-spin").hide();
			disabel_edit();
			// console.log(data)
			// $("#tabel_rencana_asuhan_durante tbody").append(data);
		}
	});
}
$("#diagnosa_paska_id_list").on("change", function(){
	load_data_rencana_asuhan_paska_askep($(this).val());
});
function load_data_rencana_asuhan_paska_askep(diagnosa_paska_id){
	if (diagnosa_paska_id){
		
	}else{
		diagnosa_paska_id=$("#diagnosa_paska_id_list").val()
	}
	let assesmen_id=$("#assesmen_id").val();
	let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_data_rencana_asuhan_paska_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				diagnosa_id:diagnosa_paska_id,
				
			},
		success: function(data) {
			tabel=data;
			$("#tabel_rencana_asuhan_paska tbody").empty().html(tabel);
			$("#cover-spin").hide();
			disabel_edit();
			// console.log(data)
			// $("#tabel_rencana_asuhan_paska tbody").append(data);
		}
	});
}
function set_rencana_asuhan(id){
	 $("#diagnosa_id_list").val(id).trigger('change');
}
$("#diagnosa_id_list").on("change", function(){
	load_data_rencana_asuhan_askep($(this).val());
});
function load_tornique_assesmen_askep(){
let assesmen_id=$("#assesmen_id").val();
$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_tornique_assesmen_askep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:$("#assesmen_id").val(),
				
			},
		success: function(data) {
			$("#tabel_tornique tbody").empty();
			$("#tabel_tornique tbody").append(data.tabel);
		}
	});
}
</script>