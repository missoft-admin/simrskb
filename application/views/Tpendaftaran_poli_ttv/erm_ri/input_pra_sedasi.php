<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.peng_keterangan{
		background-color:#fdffe2;
	}
	.cls_tabel_kasssa{
		background-color:#fdffe2;
	}
	.cls_tabel_instrumen{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_2 canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_pra_sedasi'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_pra_sedasi' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						
						if ($tanggal_step_1){$tanggal_step_1=HumanDateShort($tanggal_step_1);}else{$tanggal_step_1=date('d-m-Y');}
						if ($tanggal_step_2){$tanggal_step_2=HumanDateShort($tanggal_step_2);}else{$tanggal_step_2=date('d-m-Y');}
						if ($tanggal_step_3){$tanggal_step_3=HumanDateShort($tanggal_step_3);}else{$tanggal_step_3=date('d-m-Y');}
						if ($tiba_pemulihan){$tiba_pemulihan=HumanDateLong($tiba_pemulihan);}else{$tiba_pemulihan=date('d-m-Y H:i:s');}
						if ($pindah_jam){$pindah_jam=HumanDateLong($pindah_jam);}else{$pindah_jam=date('d-m-Y H:i:s');}
						if ($keruang_ranap){$keruang_ranap=HumanDateLong($keruang_ranap);}else{$keruang_ranap=date('d-m-Y H:i:s');}
						if ($jam_tv_2){$jam_tv_2=HumanTime($jam_tv_2);}else{$jam_tv_2=date('H:i:s');}
						
						
					}else{
						$tanggaldaftar=date('d-m-Y');
						$waktudaftar=date('H:i:s');
						$tgl_tiba_dikamar_beda=date('d-m-Y');
						
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_pra_sedasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_pra_sedasi()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="idtipe" value="<?=$idtipe?>" >		
						<input type="hidden" id="tab_wizard" value="<?=$tab_wizard?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_pra_sedasi=='1'){?>
									<button class="btn btn-primary" id="btn_create_pra_sedasi" onclick="create_pra_sedasi()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_pra_sedasi()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_pra_sedasi()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										
										<?}?>
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_pra_sedasi" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_pra_sedasi()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id !=''){?>
						<div class="form-group">
							<div class="col-md-12">
								 <!-- Simple Classic Progress Wizard (.js-wizard-simple class is initialized in js/pages/base_forms_wizard.js) -->
                            <!-- For more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/ -->
                            <div class="js-wizard-simple block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="<?=($tab_wizard=='1'?'active':'')?>">
                                        <a href="#simple-classic-progress-step1" onclick="set_tab_wizard(1)" data-toggle="tab">PENGKAJIAN AWAL</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='2'?'active':'')?>">
                                        <a href="#simple-classic-progress-step2" onclick="set_tab_wizard(2)" data-toggle="tab">INTRA ANESTESI</a>
                                    </li>
                                    <li class="<?=($tab_wizard=='3'?'active':'')?>">
                                        <a href="#simple-classic-progress-step3" onclick="set_tab_wizard(3)" data-toggle="tab">PASCA ANESTESI</a>
                                    </li>
									
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="form-horizontal" action="#" method="post">
                                    <!-- Steps Progress -->
                                    <div class="block-content block-content-mini block-content-full border-b">
                                        <div class="wizard-progress progress progress-mini remove-margin-b">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
                                        </div>
                                    </div>
                                    <!-- END Steps Progress -->

                                    <!-- Steps Content -->
                                    <div class="block-content tab-content">
                                        <!-- Step 1 -->
										<div class="tab-pane fade fade-up   push-50  <?=($tab_wizard=='1'?'in active':'')?>" id="simple-classic-progress-step1">
											<?
												$list_ruang_operasi=get_all('mruangan',array('idtipe'=>'2','status'=>1));
												$arr_subject_penyakit=($subject_penyakit?explode(',',$subject_penyakit):array());
												$list_ppa_dokter=get_all('mppa',array('tipepegawai'=>'2','staktif'=>1));
											?>
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-center text-primary">PENGKAJIAN AWAL PRA ANESTESI & PRA SEDASI</h4>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tanggal_step_1">Tanggal</label>
													<div class="input-group date">
														<input id="tanggal_step_1" class="js-datepicker form-control auto_blur" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggal_step_1 ?>" >
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="ruang_opr">Ruang Operasi</label>
													<select id="ruang_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($ruang_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach($list_ruang_operasi as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
														<label for="jenis_bedah">Jenis Pembedahan</label>
														<select id="jenis_bedah" name="jenis_bedah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" <?=($jenis_bedah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
															<?foreach(list_variable_ref(389) as $row){?>
															<option value="<?=$row->id?>" <?=($jenis_bedah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
															<?}?>
															
														</select>
												</div>
												<div class="col-md-3">
													<label for="diagnosa">Diagnosa</label>
													<input id="diagnosa" class="form-control auto_blur " type="text"  value="{diagnosa}" name="diagnosa" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="jenis_opr">Jenis Operasi</label>
													<input id="jenis_opr" class="form-control auto_blur " type="text"  value="{jenis_opr}" name="jenis_opr" placeholder="" required>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">A. SUBJECTIF</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
														<select id="subject_penyakit" name="subject_penyakit" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
															<option value="" >Pilih Opsi</option>
															<?foreach(list_variable_ref(395) as $row){?>
															<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_subject_penyakit)?'selected':'')?>><?=$row->nama?></option>
															<?}?>
															
														</select>
												</div>
												<div class="col-md-12">
													<textarea id="subject_penyakit_nama" style="margin-top:10px" class="form-control auto_blur" name="subject_penyakit_nama" rows="2" style="width:100%"><?=$subject_penyakit_nama?></textarea>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label for="keterangan_step_1">Keterangan</label>
													<textarea id="keterangan_step_1" style="margin-top:10px" class="form-control auto_blur" name="keterangan_step_1" rows="1" style="width:100%"><?=$keterangan_step_1?></textarea>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">B. OBJEKTIF</h5>
												</div>
												<div class="col-md-12 ">
													<label for="#">Keadaan Umum</label>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-2">
													<label for="gcs_e">GCS</label>
													<div class="input-group">
														<input id="gcs_e"  class="form-control  auto_blur"  type="text" name="gcs_e" value="{gcs_e}"  placeholder="" required>
														<span class="input-group-addon">E</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="gcs_m">&nbsp;</label>
													<div class="input-group">
														<input id="gcs_m"  class="form-control  auto_blur"  type="text" name="gcs_m" value="{gcs_m}"  placeholder="" required>
														<span class="input-group-addon">M</span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="gcs_v">&nbsp;</label>
													<div class="input-group">
														<input id="gcs_v"  class="form-control  auto_blur"  type="text" name="gcs_v" value="{gcs_v}"  placeholder="" required>
														<span class="input-group-addon">V</span>
													</div>
												</div>
												<div class="col-md-3 ">
													<label for="example-input-normal">Tekanan Darah</label>
													<div class="input-group">
														<input id="td_sistole"   class="form-control decimal auto_blur"  type="text" value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input id="td_diastole"  class="form-control decimal auto_blur"  type="text" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="example-input-normal">Suhu Tubuh</label>
													<div class="input-group">
														<input id="suhu" class="form-control decimal auto_blur"  type="text"  name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi">Frekuensi Nadi</label>
													<div class="input-group">
														<input id="nadi" class="form-control decimal auto_blur"  type="text"  name="nadi" value="{nadi}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="respirasi">Respirasi</label>
													<div class="input-group">
														<input id="respirasi" class="form-control decimal auto_blur" type="text"  name="respirasi" value="{respirasi}"  placeholder="Respirasi" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2">SpO2</label>
													<div class="input-group">
														<input id="spo2" class="form-control decimal auto_blur" type="text"  name="spo2" value="{spo2}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="berat_badan">Berat Badan</label>
													<div class="input-group">
														<input id="berat_badan" class="form-control decimal auto_blur"  type="text"   value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
														<span class="input-group-addon">Kg</span>
													</div>
												</div>

											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label for="mallampati">Mallampati</label>
													<select id="mallampati" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($mallampati == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(396) as $row){?>
														<option value="<?=$row->id?>" <?=($mallampati == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($mallampati == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="gigi">Gigi</label>
													<select id="gigi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($gigi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(397) as $row){?>
														<option value="<?=$row->id?>" <?=($gigi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($gigi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-2">
													<label for="gol_darah">Golongan Darah</label>
													<select id="gol_darah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($gol_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(11) as $row){?>
														<option value="<?=$row->id?>" <?=($gol_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($gol_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-2">
													<label for="leher">Leher</label>
													<select id="leher" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($leher == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(398) as $row){?>
														<option value="<?=$row->id?>" <?=($leher == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($leher == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="keterangan_obj">Keterangan</label>
													<input id="keterangan_obj" class="form-control auto_blur" type="text"  value="{keterangan_obj}" name="keterangan_obj" placeholder="" >
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="kardio">Kardio Pulmonal</label>
													<textarea id="kardio" class="form-control auto_blur summer100" name="kardio" ><?=$kardio?></textarea>
												</div>
												<div class="col-md-6">
													<label for="masalah_lain">Masalah lain</label>
													<textarea id="masalah_lain" class="form-control auto_blur summer100" name="masalah_lain" ><?=$masalah_lain?></textarea>
													
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="lab">Laboratorium</label>
													<textarea id="lab" class="form-control auto_blur summer100" name="lab" ><?=$lab?></textarea>
												</div>
												<div class="col-md-6">
													<label for="ekg">EKG/Rontgen/CT-Scan</label>
													<textarea id="ekg" class="form-control auto_blur summer100" name="ekg" ><?=$ekg?></textarea>
												</div>
											</div>	
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6 ">
													<h5 class=" push-5 text-left text-primary">C. PENILAIAN</h5>
												</div>
												<div class="col-md-6 ">
													<h5 class=" push-5 text-left text-primary">D. PERENCANAAN ANESTESI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -15px;">
												<div class="col-md-6 ">
													<label for="status_fisik">Status Fisik</label>
													<select id="status_fisik" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($status_fisik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(399) as $row){?>
														<option value="<?=$row->id?>" <?=($status_fisik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($status_fisik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
												<div class="col-md-6 ">
													<label for="perencanaan_anestesi">Status Fisik</label>
													<select id="perencanaan_anestesi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($perencanaan_anestesi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(400) as $row){?>
														<option value="<?=$row->id?>" <?=($perencanaan_anestesi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($perencanaan_anestesi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
													</select>
												</div>
											</div>		
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">D. PREMEDIKASI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_premedikasi">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="35%" class="text-center">NAMA OBAT</th>
																	<th width="20%" class="text-center">DOSIS</th>
																	<th width="15%" class="text-center">JAM</th>
																	<th width="15%" class="text-center">HASIL</th>
																	<th width="10%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th>#</th>
																	<th>
																		<input  type="hidden" class="form-control" id="premedikasi_id" value="" required>
																		<input  type="text" class="form-control" id="obat_premedikasi" name="obat_premedikasi" value="" required>
																	</th>
																	<th>
																		<input  type="text" class="form-control " id="dosis_premedikasi" name="dosis_premedikasi" value="" required>
																	</th>
																	<th>
																		<div class="input-group date">
																			<input type="text" class="js-datetimepicker form-control date_time"  id="jam_premedikasi" placeholder="HH/BB/TTTT" value="<?=date('d-m-Y H:i:s')?>" required>
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																		</div>
																	</th>
																	<th>
																		<select id="hasil_premedikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="" selected>Pilih Opsi</option>
																			<?foreach(list_variable_ref(408) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama?></option>
																			<?}?>
																		</select>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info" onclick="simpan_premedikasi()" id="btn_add_premedikasi" type="button"><i class="fa fa-plus"></i> Add</button>
																		<button class="btn btn-warning" onclick="clear_premedikasi_pra_sedasi()" type="button"><i class="fa fa-refresh"></i></button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<?
													$jml_ttd=count($list_ttd_1);
													$jml_kolom=($jml_ttd>0?$jml_ttd+1:2);
													$lebar_kolom=100/$jml_kolom;
													$jml_kolom_dokter=$jml_kolom-1;
												?>
												<div class="col-md-12 ">
													<table width="100%" >
														
														<tr>
															<td class="text-center" colspan="<?=$jml_kolom-1?>">
																<strong>DOKTER ANESTESI</strong>
																<br>
																<button class="btn btn-sm btn-success" onclick="modal_faraf_add()"  type="button"><i class="fa fa-paint-brush"></i> TAMBAH TANDA TANGAN </button>
															</td>
															<td width="<?=$lebar_kolom?>%" class="text-center"><strong>PERAWAT / PENATA ANESTESI</strong></td>
														</tr>
														<tr>
															<?if($list_ttd_1){?>
																<?foreach($list_ttd_1 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<div class="img-container fx-img-rotate-r">
																			<img  class="" style="width:120px;height:120px; text-align: center;" src="<?=$r->ppa_dokter_ttd?>" alt="">
																			<?if ($status_assemen!='2'){?>
																			<div class="img-options">
																				<div class="img-options-content">
																					<div class="btn-group btn-group-sm">
																						<a class="btn btn-default" onclick="modal_faraf(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																						<a class="btn btn-default btn-danger" onclick="hapus_ttd(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																					</div>
																				</div>
																			</div>
																			<?}?>
																		</div>
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																<?if ($perawat_anes_id){?>
																<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($perawat_anes_id)?>" alt="" title="">
																<?}?>
															</td>
														</tr>
														<tr>
															<?if($list_ttd_1){?>
																<?foreach($list_ttd_1 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<strong>( <?=get_nama_ppa($r->ppa_dokter_id)?> )</strong>
																		<br>
																		(<?=HumanDateLong2($r->date_isi)?>)
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																
																<?if ($status_assemen!='2'){?>
																<select id="perawat_anes_id" name="perawat_anes_id" class="js-select2 form-control opsi_change_ttd" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="" <?=($perawat_anes_id==''?'selected':'')?>>- Pilih Perawat-</option>
																	<?foreach($list_ppa as $r){?>
																	<option value="<?=$r->id?>" <?=($perawat_anes_id==$r->id?'selected':'')?>><?=$r->nama?></option>
																	<?}?>
																	
																	
																</select>
																<?}else{?>
																	<?if ($perawat_anes_id){?>
																	<strong>( <?=get_nama_ppa($perawat_anes_id)?> )</strong> 
																	<?}?>
																
																<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>	
										</div>
                                        <div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='2'?'in active':'')?>" id="simple-classic-progress-step2">
											
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-center text-primary">LAPORAN INTRA ANESTESI</h4>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tanggal_step_2">Tanggal</label>
													<div class="input-group date">
														<input id="tanggal_step_2" class="js-datepicker form-control auto_blur" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggal_step_2 ?>" >
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="ruang_opr_2">Ruang Operasi</label>
													<select id="ruang_opr_2" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($ruang_opr_2 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach($list_ruang_operasi as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_opr_2 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="jenis_bedah_2">Jenis Pembedahan</label>
													<select id="jenis_bedah_2" name="jenis_bedah_2" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_bedah_2 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(389) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_bedah_2 == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah_2 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<?
													$arr_da=($da?explode(',',$da):array());
													$arr_penata_anestesi=($penata_anestesi?explode(',',$penata_anestesi):array());
												?>
												<div class="col-md-3">
													<label for="da">Dokter Anestesi</label>
													<select id="da" name="da" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<?foreach($list_ppa_dokter as $row){?>
														<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_da)?'selected':'')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="penata_anestesi">Penaata Perawat Anestesi</label>
													<select id="penata_anestesi" name="penata_anestesi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<?foreach($list_ppa as $row){?>
														<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_penata_anestesi)?'selected':'')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">F. PENILAIAN PRA INDUKSI</h5>
												</div>
											</div>
											<div class="form-group" >
												<div class="col-md-2 ">
													<label for="jam_tv_2">Jam</label>
													<div class="input-group">
														<input  type="text"  class="time-datepicker form-control auto_blur" id="jam_tv_2" name="jam_tv_2" value="<?= ($jam_tv_2?$jam_tv_2:HumanTime(date('H:i:s'))) ?>" required>
														<span class="input-group-addon"><i class="si si-clock"></i></span>
													</div>
												</div>
												<div class="col-md-4 ">
													<label for="tingkat_kesadaran_2">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_2" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_2 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-4 ">
													<label for="td_sistole_2">Tekanan Darah</label>
													<div class="input-group">
														<input id="td_sistole_2" class="form-control decimal auto_blur"  type="text"   value="{td_sistole_2}" name="td_sistole_2" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input id="td_diastole_2" class="form-control decimal auto_blur"  type="text"  value="{td_diastole_2}"  name="td_diastole_2" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="suhu_2">Suhu Tubuh</label>
													<div class="input-group">
														<input id="suhu_2" class="form-control decimal auto_blur"  type="text"  name="suhu_2" value="{suhu_2}"  placeholder="Suhu Tubuh" required>
														<span class="input-group-addon"><?=$satuan_suhu?></span>
													</div>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="nadi_2">Frekuensi Nadi</label>
													<div class="input-group">
														<input id="nadi_2" class="form-control decimal auto_blur"   type="text"  name="nadi_2" value="{nadi_2}" placeholder="Nadi" required>
														<span class="input-group-addon"><?=$satuan_nadi?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="nafas_2">Frekuensi Nafas</label>
													<div class="input-group">
														<input id="nafas_2" class="form-control decimal auto_blur"   type="text"  name="nafas_2" value="{nafas_2}"  placeholder="Frekuensi Nafas" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_2">SpO2</label>
													<div class="input-group">
														<input id="spo2_2" class="form-control decimal auto_blur"   type="text"  name="spo2_2" value="{spo2_2}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
												<div class="col-md-3">
													<label for="cath">I.V Cath No</label>
													<input id="cath" class="form-control auto_blur"   type="text"   value="{cath}" name="cath" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="keterangan_penilain_induksi">Keterangan </label>
													<input id="keterangan_penilain_induksi" class="form-control auto_blur"   type="text"   value="{keterangan_penilain_induksi}" name="keterangan_penilain_induksi" placeholder="" required>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">G. ANESTESI UMUM</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-2">
													<label for="induksi">Induksi</label>
													<select id="induksi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($induksi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(401) as $row){?>
														<option value="<?=$row->id?>" <?=($induksi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($induksi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="induksi_lain">Keterangan lainnya Induksi</label>
													<input id="induksi_lain" class="form-control auto_blur"   type="text"   value="{induksi_lain}" name="induksi_lain" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="ett">ETT/LMA No</label>
													<input id="ett" class="form-control auto_blur"   type="text"   value="{ett}" name="ett" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="teknik">Teknik</label>
													<input id="teknik" class="form-control auto_blur"   type="text"   value="{teknik}" name="teknik" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="teknik_ket">Keterangan Teknik</label>
													<input id="teknik_ket" class="form-control auto_blur"   type="text"   value="{teknik_ket}" name="teknik_ket" placeholder="" required>
												</div>
												
											</div>					
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-2">
													<label for="pengaturan_nafas">Pengaturan Nafas</label>
													<select id="pengaturan_nafas" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($pengaturan_nafas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(402) as $row){?>
														<option value="<?=$row->id?>" <?=($pengaturan_nafas == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengaturan_nafas == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="pengaturan_nafas_ket">Keterangan</label>
													<input id="pengaturan_nafas_ket" class="form-control auto_blur"   type="text"   value="{pengaturan_nafas_ket}" name="pengaturan_nafas_ket" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="ventilator">Ventilator Mode</label>
													<input id="ventilator" class="form-control auto_blur"   type="text"   value="{ventilator}" name="ventilator" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="ventilator_volume">Ventilator Tidal Vollume</label>
													<input id="ventilator_volume" class="form-control auto_blur"   type="text"   value="{ventilator_volume}" name="ventilator_volume" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="ventilator_rr">Ventilator RR</label>
													<input id="ventilator_rr" class="form-control auto_blur"   type="text"   value="{ventilator_rr}" name="ventilator_rr" placeholder="" required>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">H. MONITORING INTRA ANESTESI</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left">UPLOAD FILE</h5>
													<button class="btn btn-sm btn-warning" onclick="show_modal_upload(2)"  type="button"><i class="fa fa-upload"></i> Upload </button>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_upload_2">
															<thead>
																<tr>
																	<th width="50%" class="text-center">File</th>
																	<th width="15%" class="text-center">Size</th>
																	<th width="25%" class="text-center">User</th>
																	<th width="10%" class="text-center">X</th>
																	
																</tr>
															</thead>
															<tbody>
																
															</tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_medikasi">
															<thead>
																<tr>
																	<th width="5%">NO</th>
																	<th width="50%" class="text-center">NAMA OBAT</th>
																	<th width="25%" class="text-center">SATUAN</th>
																	<th width="20%" class="text-center">Action</th>
																</tr>
																<?if($status_assemen !='2'){?>
																<tr>
																	<th>#</th>
																	<th>
																		<input  type="hidden" class="form-control" id="medikasi_id" value="" required>
																		<input  type="text" class="form-control" id="obat_medikasi" name="obat_medikasi" value="" required>
																	</th>
																	
																	<th>
																		<select id="satuan_medikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																			<option value="" selected>Pilih Satuan</option>
																			<?foreach(list_variable_ref(403) as $row){?>
																			<option value="<?=$row->id?>"><?=$row->nama?></option>
																			<?}?>
																		</select>
																	</th>
																	
																	<th class="text-center">
																		<button class="btn btn-info btn-xs" onclick="simpan_medikasi()" id="btn_add_medikasi" type="button"><i class="fa fa-plus"></i> Add</button>
																		<button class="btn btn-warning btn-xs" onclick="clear_medikasi_pra_sedasi()" type="button"><i class="fa fa-refresh"></i></button>
																	</th>
																</tr>
																<?}?>
																
															</thead>
															<tbody></tbody>
															
														</table>
													</div>
												</div>
												<div class="col-md-3">
													<label for="masalah_durante">Masalah Durante Operasi / Anestesi</label>
													<textarea id="masalah_durante" class="form-control auto_blur summer100" name="masalah_durante" ><?=$masalah_durante?></textarea>
													
												</div>
												<div class="col-md-3">
													<label for="tindakan_durante">Tindakan</label>
													<textarea id="tindakan_durante" class="form-control auto_blur summer100" name="tindakan_durante" ><?=$tindakan_durante?></textarea>
													
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left text-primary">I. BLOK REGIONAL</h5>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-3">
													<label for="blok_teknik">Teknik</label>
													<select id="blok_teknik" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($blok_teknik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(404) as $row){?>
														<option value="<?=$row->id?>" <?=($blok_teknik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($blok_teknik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="blok_lokasi">Lokasi</label>
													<input id="blok_lokasi" class="form-control auto_blur"   type="text"   value="{blok_lokasi}" name="blok_lokasi" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="jenis_obat">Jenis Obat</label>
													<input id="jenis_obat" class="form-control auto_blur"   type="text"   value="{jenis_obat}" name="jenis_obat" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="dosis">Dosis</label>
													<input id="dosis" class="form-control auto_blur"   type="text"   value="{dosis}" name="dosis" placeholder="" required>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: 25px;">
												<?
													$jml_ttd=count($list_ttd_2);
													$jml_kolom=($jml_ttd>0?$jml_ttd+1:2);
													$lebar_kolom=100/$jml_kolom;
													$jml_kolom_dokter=$jml_kolom-1;
												?>
												<div class="col-md-12 ">
													<table width="100%" >
														
														<tr>
															<td class="text-center" colspan="<?=$jml_kolom-1?>">
																<strong>DOKTER ANESTESI</strong>
																<br>
																<button class="btn btn-sm btn-success" onclick="modal_faraf_add()"  type="button"><i class="fa fa-paint-brush"></i> TAMBAH TANDA TANGAN </button>
															</td>
															<td width="<?=$lebar_kolom?>%" class="text-center"><strong>PERAWAT / PENATA ANESTESI</strong></td>
														</tr>
														<tr>
															<?if($list_ttd_2){?>
																<?foreach($list_ttd_2 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<div class="img-container fx-img-rotate-r">
																			<img  class="" style="width:120px;height:120px; text-align: center;" src="<?=$r->ppa_dokter_ttd?>" alt="">
																			<?if ($status_assemen!='2'){?>
																			<div class="img-options">
																				<div class="img-options-content">
																					<div class="btn-group btn-group-sm">
																						<a class="btn btn-default" onclick="modal_faraf(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																						<a class="btn btn-default btn-danger" onclick="hapus_ttd(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																					</div>
																				</div>
																			</div>
																			<?}?>
																		</div>
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																<?if ($perawat_anes_id_2){?>
																<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($perawat_anes_id_2)?>" alt="" title="">
																<?}?>
															</td>
														</tr>
														<tr>
															<?if($list_ttd_2){?>
																<?foreach($list_ttd_2 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<strong>( <?=get_nama_ppa($r->ppa_dokter_id)?> )</strong>
																		<br>
																		(<?=HumanDateLong2($r->date_isi)?>)
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																
																<?if ($status_assemen!='2'){?>
																<select id="perawat_anes_id_2" name="perawat_anes_id_2" class="js-select2 form-control opsi_change_ttd" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="" <?=($perawat_anes_id_2==''?'selected':'')?>>- Pilih Perawat-</option>
																	<?foreach($list_ppa as $r){?>
																	<option value="<?=$r->id?>" <?=($perawat_anes_id_2==$r->id?'selected':'')?>><?=$r->nama?></option>
																	<?}?>
																	
																	
																</select>
																<?}else{?>
																	<?if ($perawat_anes_id_2){?>
																	<strong>( <?=get_nama_ppa($perawat_anes_id_2)?> )</strong> 
																	<?}?>
																
																<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>	
										</div>
										<div class="tab-pane fade fade-up  push-50 <?=($tab_wizard=='3'?'in active':'')?>" id="simple-classic-progress-step3">
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-center text-primary">LAPORAN PASCA ANESTESI</h4>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="tanggal_step_3">Tanggal</label>
													<div class="input-group date">
														<input id="tanggal_step_3" class="js-datepicker form-control auto_blur" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggal_step_3 ?>" >
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-1">
													<label for="ruang_opr_3">Ruang Operasi</label>
													<select id="ruang_opr_3" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($ruang_opr_3 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach($list_ruang_operasi as $row){?>
														<option value="<?=$row->id?>" <?=($ruang_opr_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-1">
													<label for="jenis_bedah_3">Jenis </label>
													<select id="jenis_bedah_3" name="jenis_bedah_3" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="" <?=($jenis_bedah_3 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(list_variable_ref(389) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_bedah_3 == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="tiba_pemulihan">Tiba Diruang Pemulihan</label>
													<div class="input-group date">
														<input type="text" class="js-datetimepicker form-control date_time"  id="tiba_pemulihan" placeholder="HH/BB/TTTT" value="<?=$tiba_pemulihan?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penyerah_id">Petugas Yang Menyerahkan</label>
														<div class="input-group">
														<select id="petugas_penyerah_id" name="petugas_penyerah_id" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penyerah_id==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penyerah_id==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penyerah_ttd?'default':'success')?>"  title="Tanda Tangan" onclick="modal_petugas('petugas_penyerah_id')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penerima_id">Petugas Yang Menerima</label>
														<div class="input-group">
														<select id="petugas_penerima_id" name="petugas_penerima_id" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penerima_id==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penerima_id==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penerima_ttd?'default':'success')?>"  title="Tanda Tangan" onclick="modal_petugas('petugas_penerima_id')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>	
																		
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label for="intruksi_pasca_anestesi">Intruksi Pasca Anestesi</label>
													<textarea id="intruksi_pasca_anestesi" style="margin-top:10px" class="form-control auto_blur" name="intruksi_pasca_anestesi" rows="2" style="width:100%"><?=$intruksi_pasca_anestesi?></textarea>
												</div>
											</div>
											<div class="form-group" >
												
												<div class="col-md-3 ">
													<label for="tingkat_kesadaran_3">Tingkat Kesadaran</label>
													<select id="tingkat_kesadaran_3" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(326) as $row){?>
														<option value="<?=$row->id?>"  <?=($tingkat_kesadaran_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3 ">
													<label for="td_sistole_3">Tekanan Darah</label>
													<div class="input-group">
														<input id="td_sistole_3" class="form-control decimal auto_blur"  type="text"   value="{td_sistole_3}" name="td_sistole_3" placeholder="Sistole" required>
														<span class="input-group-addon"><?=$satuan_td?> / </span>
														<input id="td_diastole_3" class="form-control decimal auto_blur"  type="text"  value="{td_diastole_3}"  name="td_diastole_3" placeholder="Diastole" required>
														<span class="input-group-addon"><?=$satuan_td?></span>
													</div>
												</div>
												<div class="col-md-2 ">
													<label for="respirasi_id_3">Nadi</label>
													<select id="respirasi_id_3" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(409) as $row){?>
														<option value="<?=$row->id?>"  <?=($respirasi_id_3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-2">
													<label for="respirasi_3">Respirasi</label>
													<div class="input-group">
														<input id="respirasi_3" class="form-control decimal auto_blur" type="text"  name="respirasi_3" value="{respirasi_3}"  placeholder="Respirasi" required>
														<span class="input-group-addon"><?=$satuan_nafas?></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="spo2_3">SpO2</label>
													<div class="input-group">
														<input id="spo2_3" class="form-control decimal auto_blur"   type="text"  name="spo2_3" value="{spo2_3}"  placeholder="Frekuensi spo2" required>
														<span class="input-group-addon"><?=$satuan_spo2?></span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<div class="col-md-12">
														<table class="block-table text-center ">
															<tbody>
																<tr>
																	<td class="border-r" style="width:70%">
																		<div>
																			<div class="input-group">
																				<input class="form-control" disabled type="text" readonly value="<?=$nama_kajian?>" id="nama_kajian" placeholder="nama_kajian" >
																				<span class="input-group-addon"><i class="fa fa-user"></i></span>
																			</div>
																		</div>
																		<div class="h5 font-w700 text-left text-primary push-5-t">Penilaian Rating Scale</div>
																	</td>
																	<td class="border-r"  style="width:30%">
																		<div class="h1 font-w700 text-primary" id="div_total_skor_nyeri"><?=$total_skor_nyeri?></div>
																		<div class="h5 text-uppercase push-5-t">Total Skor Skala Nyeri</div>
																	</td>
																</tr>
															</tbody>
														</table>
														
													</div>
													
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<div class="col-md-12">
														<div id="div_tabel_nrs" class="form-group">
															
														</div>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-6">
													<label for="komplikasi">Komplikasi / Penyakit Penyulit</label>
													<textarea id="komplikasi" class="form-control auto_blur summer100" name="komplikasi" ><?=$komplikasi?></textarea>
													
												</div>
												<div class="col-md-6">
													<label for="komplikasi_tindakan">Tindakan</label>
													<textarea id="komplikasi_tindakan" class="form-control auto_blur summer100" name="komplikasi_tindakan" ><?=$komplikasi_tindakan?></textarea>
													
												</div>
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<h5 class=" push-5 text-left">UPLOAD FILE</h5>
													<button class="btn btn-sm btn-warning" onclick="show_modal_upload(3)"  type="button"><i class="fa fa-upload"></i> Upload </button>
												</div>
												
											</div>
											<div class="form-group" style="margin-top: -5px;">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table table-bordered table-striped" id="tabel_upload_3">
															<thead>
																<tr>
																	<th width="50%" class="text-center">File</th>
																	<th width="15%" class="text-center">Size</th>
																	<th width="25%" class="text-center">User</th>
																	<th width="10%" class="text-center">X</th>
																	
																</tr>
															</thead>
															<tbody>
																
															</tbody>
															
														</table>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												
												<div class="col-md-12">
													<label for="metodi_penilain">Metode Penilaian</label>
													<select id="metodi_penilain" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" <?=($metodi_penilain == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<?foreach(get_all('mpasca_anestesi',array('staktif'=>1)) as $row){?>
														<option value="<?=$row->id?>" <?=($metodi_penilain == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
																
											</div>
											<div class="form-group" style="margin-top:-10px">
												<div class="col-md-12 ">
													<div class="table-responsive">
														<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
															<thead>
																<tr>
																	<th width="5%" class="text-center">NO</th>
																	<th width="15%" class="text-center">KRITERIA</th>
																	<?
																	$q="SELECT H.jenis as id,M.ref as nama 
																	FROM `tranap_pra_sedasi_skrining` H 
																	INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='407'
																	WHERE H.assesmen_id='$assesmen_id'
																	GROUP BY H.jenis
																	ORDER BY H.jenis";
																	$list_header=$this->db->query($q)->result();
																	$jml_kolom=count($list_header);
																	$lebar_kolom=80 / $jml_kolom;
																	foreach($list_header as $r){
																	?>
																	<th width="<?=$lebar_kolom?>%" class="text-center"><?=$r->nama?></th>
																	
																	<?}?>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
													<label class="h4 text-primary" id="label_footer"></label>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-12">
													<label for="keterangan_penilaian">Keterangan Lain-Lain</label>
													<textarea id="keterangan_penilaian" style="margin-top:10px" class="form-control auto_blur" name="keterangan_penilaian" rows="2" style="width:100%"><?=$keterangan_penilaian?></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<h4 class="font-w700 push-5 text-left">Pasca Anestesi Dirawat Inap</h4>
												</div>
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-2">
													<label for="lama_pasien">Lama Pasien Diruang Pemulihan</label>
													<input id="lama_pasien" class="form-control auto_blur"   type="text"   value="{lama_pasien}" name="lama_pasien" placeholder="" required>
												</div>
												<div class="col-md-2">
													<label for="pindah_jam">Pindah Dari Ruangan Pemulihan Jam</label>
													<div class="input-group date">
														<input type="text" class="js-datetimepicker form-control date_time"  id="pindah_jam" placeholder="HH/BB/TTTT" value="<?=$pindah_jam?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-2">
													<label for="keruang_ranap">Pindah Dari Ruangan Pemulihan Jam</label>
													<div class="input-group date">
														<input type="text" class="js-datetimepicker form-control date_time"  id="keruang_ranap" placeholder="HH/BB/TTTT" value="<?=$keruang_ranap?>" required>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penyerah_id_3">Petugas Yang Menyerahkan</label>
														<div class="input-group">
														<select id="petugas_penyerah_id_3" name="petugas_penyerah_id_3" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penyerah_id_3==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penyerah_id_3==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penyerah_ttd_3?'default':'success')?>"  title="Tanda Tangan" onclick="modal_petugas('petugas_penyerah_id_3')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>
												<div class="col-md-3">
														<label for="petugas_penerima_id_3">Petugas Yang Menerima</label>
														<div class="input-group">
														<select id="petugas_penerima_id_3" name="petugas_penerima_id_3" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
															<option value="" <?=($petugas_penerima_id_3==''?'selected':'')?>>- Pilih Perawat-</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>" <?=($petugas_penerima_id_3==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
														</select>
														<span class="input-group-btn">
															<button class="btn btn-<?=($petugas_penerima_ttd_3?'default':'success')?>" title="Tanda Tangan" onclick="modal_petugas('petugas_penerima_id_3')" type="button"><i class="fa fa-paint-brush"></i> </button>
														</span>
													</div>
												</div>	
																		
											</div>
											<?
												$arr_observasi_posisi=($observasi_posisi?explode(',',$observasi_posisi):array());
											?>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-6">
													<label for="observasi">Obeservasi Kesadaran, Tanda Vital (Tekanan Darah, Nadi, Pernapasan, Suhu) Setiap</label>
													<input id="observasi" class="form-control auto_blur"   type="text"   value="{observasi}" name="observasi" placeholder="" required>
												</div>
												<div class="col-md-3">
													<label for="observasi_posisi">Posisi</label>
													<select id="observasi_posisi" name="observasi_posisi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
														<option value="" >Pilih Opsi</option>
														<?foreach(list_variable_ref(406) as $row){?>
														<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_observasi_posisi)?'selected':'')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
												</div>
												<div class="col-md-3">
													<label for="puasa">Puasa Selama</label>
													<input id="puasa" class="form-control auto_blur"   type="text"   value="{puasa}" name="puasa" placeholder="" required>
												</div>
																
											</div>
											<div class="form-group" style="margin-top: -10px;">
												<div class="col-md-4">
													<label for="mual">Bila Timbul Mual / Muntah</label>
													<input id="mual" class="form-control auto_blur"   type="text"   value="{mual}" name="mual" placeholder="" required>
												</div>
												<div class="col-md-4">
													<label for="manajemen_nyeri">Manajemen Nyeri</label>
													<input id="manajemen_nyeri" class="form-control auto_blur"   type="text"   value="{manajemen_nyeri}" name="manajemen_nyeri" placeholder="" required>
												</div>
												<div class="col-md-4">
													<label for="penunjang">Pemeriksaan Penunjang & Peralatan Lainnya</label>
													<input id="penunjang" class="form-control auto_blur"   type="text"   value="{penunjang}" name="penunjang" placeholder="" required>
												</div>
																
											</div>
											<div class="form-group" style="margin-top: 25px;">
												<?
													$jml_ttd=count($list_ttd_3);
													$jml_kolom=($jml_ttd>0?$jml_ttd+1:2);
													$lebar_kolom=100/$jml_kolom;
													$jml_kolom_dokter=$jml_kolom-1;
												?>
												<div class="col-md-12 ">
													<table width="100%" >
														
														<tr>
															<td class="text-center" colspan="<?=$jml_kolom-1?>">
																<strong>DOKTER ANESTESI</strong>
																<br>
																<button class="btn btn-sm btn-success" onclick="modal_faraf_add()"  type="button"><i class="fa fa-paint-brush"></i> TAMBAH TANDA TANGAN </button>
															</td>
															<td width="<?=$lebar_kolom?>%" class="text-center"><strong>PERAWAT / PENATA ANESTESI</strong></td>
														</tr>
														<tr>
															<?if($list_ttd_3){?>
																<?foreach($list_ttd_3 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<div class="img-container fx-img-rotate-r">
																			<img  class="" style="width:120px;height:120px; text-align: center;" src="<?=$r->ppa_dokter_ttd?>" alt="">
																			<?if ($status_assemen!='2'){?>
																			<div class="img-options">
																				<div class="img-options-content">
																					<div class="btn-group btn-group-sm">
																						<a class="btn btn-default" onclick="modal_faraf(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																						<a class="btn btn-default btn-danger" onclick="hapus_ttd(<?=$r->id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																					</div>
																				</div>
																			</div>
																			<?}?>
																		</div>
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																<?if ($perawat_anes_id_2){?>
																<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($perawat_anes_id_2)?>" alt="" title="">
																<?}?>
															</td>
														</tr>
														<tr>
															<?if($list_ttd_3){?>
																<?foreach($list_ttd_3 as $r){?>
																	<td width="<?=$lebar_kolom?>%" class="text-center">
																		<strong>( <?=get_nama_ppa($r->ppa_dokter_id)?> )</strong>
																		<br>
																		(<?=HumanDateLong2($r->date_isi)?>)
																	</td>
																<?}?>
															<?}else{?>
																<td width="<?=$lebar_kolom?>%" class="text-center"><strong></strong></td>
															<?}?>
															
															<td width="<?=$lebar_kolom?>%" class="text-center">
																
																<?if ($status_assemen!='2'){?>
																<select id="perawat_anes_id_2" name="perawat_anes_id_2" class="js-select2 form-control opsi_change_ttd" style="width: 100%;" data-placeholder="Choose one..">
																	<option value="" <?=($perawat_anes_id_2==''?'selected':'')?>>- Pilih Perawat-</option>
																	<?foreach($list_ppa as $r){?>
																	<option value="<?=$r->id?>" <?=($perawat_anes_id_2==$r->id?'selected':'')?>><?=$r->nama?></option>
																	<?}?>
																	
																	
																</select>
																<?}else{?>
																	<?if ($perawat_anes_id_2){?>
																	<strong>( <?=get_nama_ppa($perawat_anes_id_2)?> )</strong> 
																	<?}?>
																
																<?}?>
															</td>
														</tr>
													</table>
												</div>
											</div>	
										</div>
									</div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-mini block-content-full border-t">
                                    <div class="block-content block-content-mini block-content-full border-t">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button class="wizard-prev btn btn-default" type="button"><i class="fa fa-arrow-left"></i> Kembali</button>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="wizard-next btn btn-default" type="button">Step Selanjutnya <i class="fa fa-arrow-right"></i></button>
												<?if ($status_assemen=='1'){?>
												
                                                <button class="wizard-finish btn btn-primary" onclick="close_assesmen()" type="button"><i class="fa fa-save"></i> SIMPAN</button>
												<?}else{?>
                                                <button class="wizard-finish btn btn-warning" onclick="simpan_template()" type="button"><i class="fa fa-save"></i> SIMPAN TEMPLATE</button>
												
												<?}?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                           
						   </div>
                            <!-- END Simple Classic Progress Wizard -->
							</div>
						</div>
						</div>
						<?}?>
					
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="pull-right">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_pra_sedasi=='1'){?>
										<button class="btn btn-primary" id="btn_create_pra_sedasi" onclick="create_pra_sedasi()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										
									</div>
									
								</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Pilih -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'st_tampil'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template_pra_sedasi()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_pra_sedasi()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_pra_sedasi()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?if ($assesmen_id!=''){?>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_2" ></div>
									</div>
								</div>
								<input type="hidden" id="variable_petugas" value="" >		
								<textarea id="signature64_2" name="signed_2" style="display: none"></textarea>
								<textarea id="petugas_penyerah_ttd" name="petugas_penyerah_ttd" style="display: none"><?=$petugas_penyerah_ttd?></textarea>
								<textarea id="petugas_penerima_ttd" name="petugas_penerima_ttd" style="display: none"><?=$petugas_penerima_ttd?></textarea>
								
								<textarea id="petugas_penyerah_ttd_3" name="petugas_penyerah_ttd_3" style="display: none"><?=$petugas_penyerah_ttd_3?></textarea>
								<textarea id="petugas_penerima_ttd_3" name="petugas_penerima_ttd_3" style="display: none"><?=$petugas_penerima_ttd_3?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="perawat_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Perugas" required>
										<option value="0">-Pilih -</option>
										<?foreach(get_all('mppa',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
									<label for="#">NAMA </label>
								</div>
							</div>
							
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_petugas()"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_paraf" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" >
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<input type="hidden" readonly id="ttd_id" name="ttd_id" value="">
								<input type="hidden" readonly id="nama_tabel" name="nama_tabel" value="">
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
						
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<label for="spo2_opr">NAMA</label>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<select id="petugas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" selected>- Pilih -</option>
											<?foreach($list_ppa_dokter as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
											<?}?>
											
											
										</select>
									</div>
								</div>
								
							</div>
						
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<label for="spo2_opr">TANGGAL</label>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<div class="input-group date">
											<input type="text" class="js-datetimepicker form-control date_time"  id="jam_ttd" placeholder="HH/BB/TTTT" value="<?=date('d-m-Y H:i:s')?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Clear</button>
							<button class="btn btn-sm btn-success" onclick="simpan_ttd_dokter()" type="button" id="btn_save_paraf"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?}?>
<div class="modal in" id="modal_upload" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">UPLOAD FILE </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<form class="dropzone" action="{base_url}Tpendaftaran_ranap_erm/upload_files_pra_sedasi" method="post" enctype="multipart/form-data">
									<input name="idtransaksi" id="idtransaksi" type="hidden" value="{assesmen_id}">
									<input name="idtransaksi_step" id="idtransaksi_step" type="hidden" value="">
								 </form>
							</div>
							
						</div>
					</div>
					
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
		
<script type="text/javascript" src="{js_path}pages/base_forms_wizard.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
var sig_2 = $('#sig_2').signature({syncField: '#signature64_2', syncFormat: 'PNG'});
let var_sig='';
var myDropzone;
function show_modal_upload(tab_wizard){
	$("#modal_upload").modal('show');
	$("#idtransaksi_step").val(tab_wizard);
}
function modal_petugas(variable_petugas){
	$("#variable_petugas").val(variable_petugas);
	$("#modal_ttd").modal('show');
	// petugas_id
	$('#perawat_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	let idpetugas=$("#"+variable_petugas).val();
	$("#perawat_id").val(idpetugas).trigger('change');
	
	if (variable_petugas=='petugas_penyerah_id'){
		var_sig=$("#petugas_penyerah_ttd").val();
	}
	if (variable_petugas=='petugas_penerima_id'){
		var_sig=$("#petugas_penerima_ttd").val();
	}
	if (variable_petugas=='petugas_penyerah_id_3'){
		var_sig=$("#petugas_penyerah_ttd_3").val();
	}
	if (variable_petugas=='petugas_penerima_id_3'){
		var_sig=$("#petugas_penerima_ttd_3").val();
	}
	if (var_sig){
		$("#signature64_2").val(var_sig);
		$('#sig_2').signature('enable').signature('draw', var_sig);
	}else{
		sig_2.signature('clear');
	}
	
}
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig_2.signature('clear');
	$("#signature64_2").val('');
});

$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function modal_faraf_add(){
	$("#ttd_id").val('');
	$("#modal_paraf").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_paraf')
	});
	
}
function simpan_ttd_dokter(){
	var ttd_id=$("#ttd_id").val();
	var signature64=$("#signature64").val();
	var tab_wizard=$("#tab_wizard").val();
	var petugas_id=$("#petugas_id").val();
	var jam_ttd=$("#jam_ttd").val();
	
	if (petugas_id==''){
		sweetAlert("Maaf...", "Pilih Dokter!", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_dokter_pra_sedasi/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			ttd_id:ttd_id,
			assesmen_id:$("#assesmen_id").val(),
			signature64:signature64,
			tab_wizard:tab_wizard,
			petugas_id:petugas_id,
			jam_ttd:jam_ttd,
	  },success: function(data) {
			$("#modal_paraf").modal('hide');
			location.reload();
			// $("#cover-spin").hide();
		}
	});
}
function simpan_ttd_petugas(){
	var ttd_id=$("#variable_petugas").val();
	var signature64=$("#signature64_2").val();
	var tab_wizard=$("#tab_wizard").val();
	var petugas_id=$("#perawat_id").val();
	
	if (petugas_id==''){
		sweetAlert("Maaf...", "Pilih Dokter!", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_petugas_sedasi/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			ttd_id:ttd_id,
			assesmen_id:$("#assesmen_id").val(),
			signature64:signature64,
			tab_wizard:tab_wizard,
			petugas_id:petugas_id,
	  },success: function(data) {
			$("#modal_paraf").modal('hide');
			location.reload();
			// $("#cover-spin").hide();
		}
	});
}
function hapus_ttd(ttd_id){
	let assesmen_id=$("#assesmen_id").val();
	let tab_wizard=$("#tab_wizard").val();
	$("#signature64").val('');
	let signature64=$("#signature64").val();
	
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/hapus_ttd_dokter_pra_sedasi/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			ttd_id:ttd_id,
			
	  },success: function(data) {
			$("#modal_paraf").modal('hide');
			location.reload();
			// $("#cover-spin").hide();
		}
	});
}
function modal_faraf($id){
	$("#ttd_id").val($id);
	$("#modal_paraf").modal('show');
	// petugas_id
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_paraf')
	});

	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_ttd_detail_pra_sedasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:$id,
				},
		success: function(data) {
			$("#petugas_id").val(data.ppa_dokter_id).trigger('change');
			$("#jam_ttd").val(data.date_isi);
			if (data.ppa_dokter_ttd){
				$("#signature64").val(data.ppa_dokter_ttd);
				$('#sig').signature('enable').signature('draw', data.ppa_dokter_ttd);
			}else{
				sig.signature('clear');
			}
			$("#cover-spin").hide();
		}
	});
}

$(".wizard-next").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)+1;
	set_tab_wizard(tab_wizard);
});
$(".wizard-prev").click(function(){
	let tab_wizard=$("#tab_wizard").val();
	tab_wizard=parseFloat(tab_wizard)-1;
	set_tab_wizard(tab_wizard);
});
function set_tab_wizard($id){
	var assesmen_id=$("#assesmen_id").val();
	$("#tab_wizard").val($id);
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/set_tab_wizard_pra_sedasi/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			assesmen_id:assesmen_id,
			tab_wizard:$("#tab_wizard").val(),
	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Succes!', message : ' Wizard'});
		  
		}
	});
}
$(document).ready(function() {
	$('.angka').number(true, 0);
	$(".btn_close_left").click(); 
	// add_referensi('lokasi_tornique','364','Lokasi Tornique');
	disabel_edit();
	$('.summer100').summernote({
	  height: 100,   //set editable area's height
	  dialogsInBody: true,
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	})	
	$(".date_time").datetimepicker({
		format: "DD-MM-YYYY HH:mm",
		// stepping: 30
	});

	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_premedikasi_pra_sedasi();
		load_medikasi_pra_sedasi();
		load_nrs();
		load_skrining_pra_sedasi();
		refresh_image();
		if ($("#status_assemen").val()!='2'){
			
			Dropzone.autoDiscover = false;
			myDropzone = new Dropzone(".dropzone", { 
			   autoProcessQueue: true,
			   maxFilesize: 30,
			});
			myDropzone.on("complete", function(file) {
			  refresh_image();
			  myDropzone.removeFile(file);
			});
		}
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian()
	}
	// modal_faraf_add();
	// $("#modal_edit").modal('show');
	// load_data_rencana_asuhan_pra_sedasi(1);
});
function refresh_image(){
	var id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_image_pra_sedasi/'+id+'/2',
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#tabel_upload_2 tbody').empty();
				$("#tabel_upload_2 tbody").append(data.detail);
				disabel_edit();
			}
			// console.log();
			
		}
	});
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_image_pra_sedasi/'+id+'/3',
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#tabel_upload_3 tbody').empty();
				$("#tabel_upload_3 tbody").append(data.detail);
				disabel_edit();
			}
			// console.log();
			
		}
	});
}
$(document).on("click",".hapus_file",function(){	
	var currentRow=$(this).closest("tr");          
	 var id=currentRow.find("td:eq(4)").text();
	 $.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/hapus_file_pra_sedasi',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
			currentRow.remove();
			// table.ajax.reload( null, false );				
		}
	});
});
function load_skrining_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	let metodi_penilain=$("#metodi_penilain").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_skrining_pra_sedasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				mrisiko_id:metodi_penilain,
				
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			$("#label_footer").html(data.isi_footer);
			// console.LOG(data);
			$(".nilai").select2();
			// get_skor_pengkajian_pra_sedasi();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai",function(){
	let assesmen_id=$("#assesmen_id").val();
	// let template_id=$("#template_id").val();
	var tr=$(this).closest('tr');
	
	let row_id=$(this).find("option:selected").attr('data-row_id');
	let skor=$(this).find("option:selected").attr('data-skor');
	let jenis=$(this).find("option:selected").attr('data-jenis');
	let nilai_nama=$(this).find("option:selected").attr('data-nama');
	var nilai_id=$(this).val();
	tr.find(".nilai_skor"+row_id).val(skor);
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/update_nilai_pra_sedasi', 
		  dataType: "json",
		  type: 'POST',
		  data: {
				row_id:row_id,
				nilai_id:nilai_id,
				assesmen_id:assesmen_id,
				nilai_nama:nilai_nama,
				skor:skor,
		  },success: function(data) {
			  // console.log(data);
			  udpate_footer(jenis);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
			}
		});
	
});
function udpate_footer(jenis){
	let assesmen_id=$("#assesmen_id").val();
	let mrisiko_id=$("#metodi_penilain").val();
	$.ajax({
	url: '{site_url}Tpendaftaran_ranap_erm/get_footer_skrining_pra_sedasi', 
	  dataType: "json",
	  type: 'POST',
	  data: {
			mrisiko_id:mrisiko_id,
			jenis:jenis,
			assesmen_id:assesmen_id,
	  },success: function(data) {
			// console.log(data);
			$("#footer_"+jenis).val(data.total_skor);
			$("#ket_skor_"+jenis).text(data.total_skor);
			$("#ket_"+jenis).html(data.ref_nilai);
	  }
	});
}
$(document).on("change","#metodi_penilain",function(){
	load_skrining_pra_sedasi();
	
});
function load_nrs(){
	assesmen_id=$("#assesmen_id").val();
	status_assemen=$("#status_assemen").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_nrs_fisio_pra_sedasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
			   },
		success: function(data) {
			$("#div_tabel_nrs").append(data);
		}
	});
}
function set_skala_nyeri($trx_id,$trx_nilai,$trx_tabel){
	let assesmen_id=$("#assesmen_id").val();
	let status_assemen=$("#status_assemen").val();
	if (status_assemen!='2'){
		
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/set_skala_nyeri_fisio', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:$("#assesmen_id").val(),
				nama_tabel:$trx_tabel,
				trx_id:$trx_id,
				trx_nilai:$trx_nilai,
			},
		success: function(data) {
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Assesmen.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				$("#total_skor_nyeri").val($trx_nilai);
				$("#div_total_skor_nyeri").text($trx_nilai);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				
			}
		}
	});
	}
}
function simpan_premedikasi(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let obat_premedikasi=$("#obat_premedikasi").val();
	let dosis_premedikasi=$("#dosis_premedikasi").val();
	let jam_premedikasi=$("#jam_premedikasi").val();
	let hasil_premedikasi=$("#hasil_premedikasi").val();
	let premedikasi_id=$("#premedikasi_id").val();
	
	
	let idpasien=$("#idpasien").val();
	if (obat_premedikasi==''){
		sweetAlert("Maaf...", "Isi Obat!", "error");
		return false;
	}
	if (dosis_premedikasi==''){
		sweetAlert("Maaf...", "Isi Dosis!", "error");
		return false;
	}
	if (jam_premedikasi==''){
		sweetAlert("Maaf...", "Isi Jam!", "error");
		return false;
	}
	if (hasil_premedikasi==''){
		sweetAlert("Maaf...", "Isi Hasil!", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_premedikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					premedikasi_id : premedikasi_id,
					obat : obat_premedikasi,
					dosis : dosis_premedikasi,
					jam : jam_premedikasi,
					hasil : hasil_premedikasi,
				},
			success: function(data) {
				// $("#kondisi").val('');
				$("#lokasi").val('0').trigger('change');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_premedikasi_pra_sedasi();
					clear_premedikasi_pra_sedasi();
					// $("#trx_").val('')
				}
			}
		});
	}
	
}
function clear_premedikasi_pra_sedasi(id){
	$("#premedikasi_id").val(id);
	$("#btn_add_premedikasi").html('<i class="fa fa-plus"></i> ADD');
	$("#premedikasi_id").val('');
	$("#obat_premedikasi").val('');
	$("#dosis_premedikasi").val('');
	$("#hasil_premedikasi").val('').trigger('change');
	
}
function edit_premedikasi_pra_sedasi(id){
	$("#premedikasi_id").val(id);
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_premedikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_premedikasi").html('<i class="fa fa-check"></i> UPDATE');
				$("#obat_premedikasi").val(data.obat);
				$("#dosis_premedikasi").val(data.dosis);
				$("#jam_premedikasi").val(data.jam);
				$("#hasil_premedikasi").val(data.hasil).trigger('change');
			}
		});
}
function load_premedikasi_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_premedikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_premedikasi tbody").empty();
				$("#tabel_premedikasi tbody").append(data.tabel);
			}
		});
}
function hapus_premedikasi_pra_sedasi(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Data?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_premedikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_premedikasi_pra_sedasi();		
			}
		});
	});			
}
function simpan_medikasi(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let obat_medikasi=$("#obat_medikasi").val();
	let dosis_medikasi=$("#dosis_medikasi").val();
	let jam_medikasi=$("#jam_medikasi").val();
	let satuan_medikasi=$("#satuan_medikasi").val();
	let medikasi_id=$("#medikasi_id").val();
	
	
	let idpasien=$("#idpasien").val();
	if (obat_medikasi==''){
		sweetAlert("Maaf...", "Isi Obat!", "error");
		return false;
	}
	if (dosis_medikasi==''){
		sweetAlert("Maaf...", "Isi Dosis!", "error");
		return false;
	}
	if (jam_medikasi==''){
		sweetAlert("Maaf...", "Isi Jam!", "error");
		return false;
	}
	if (satuan_medikasi==''){
		sweetAlert("Maaf...", "Isi Hasil!", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_medikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					medikasi_id : medikasi_id,
					obat : obat_medikasi,
					satuan : satuan_medikasi,
				},
			success: function(data) {
				// $("#kondisi").val('');
				$("#lokasi").val('0').trigger('change');
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Duplikat .",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Saved'});
					load_medikasi_pra_sedasi();
					clear_medikasi_pra_sedasi();
					// $("#trx_").val('')
				}
			}
		});
	}
	
}
function clear_medikasi_pra_sedasi(id){
	$("#medikasi_id").val(id);
	$("#btn_add_medikasi").html('<i class="fa fa-plus"></i> ADD');
	$("#medikasi_id").val('');
	$("#obat_medikasi").val('');
	$("#satuan_medikasi").val('').trigger('change');
	
}
function edit_medikasi_pra_sedasi(id){
	$("#medikasi_id").val(id);
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_medikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_medikasi").html('<i class="fa fa-check"></i> UPDATE');
				$("#obat_medikasi").val(data.obat);
				$("#satuan_medikasi").val(data.satuan).trigger('change');
			}
		});
}
function load_medikasi_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_medikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					
				},
			success: function(data) {
				$("#tabel_medikasi tbody").empty();
				$("#tabel_medikasi tbody").append(data.tabel);
			}
		});
}
function hapus_medikasi_pra_sedasi(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Data?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_medikasi_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deleted'});
				load_medikasi_pra_sedasi();		
			}
		});
	});			
}

$("#subject_penyakit").change(function(){
	generate_diagnosa_medis();
});

function generate_diagnosa_medis(){
	if ($("#status_assemen").val()!='2'){
		var select_button_text
			select_button_text = $('#subject_penyakit option:selected')
					.toArray().map(item => item.text).join();
		
		$("#subject_penyakit_nama").val(select_button_text);
		
	}
}


$(".opsi_change").change(function(){
	// console.log('OPSI CHANTE')
	// if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	// }
});
$(".opsi_change_ttd").change(function(){
	
	// if ($("#st_edited").val()=='0'){
		simpan_assesmen();
		// $("#cover-spin").show();
		// setTimeout(function() {
			// location.reload();
		  // //your code to be executed after 1 second
		// }, 500);

	// }
});

$(".auto_blur").blur(function(){
	console.log('BLUR')
	// if ($("#st_edited").val()=='0'){
		// $(this).removeClass('input_edited');
		simpan_assesmen();
	// }else{
		// if ($("#st_edited").val()=='1'){
			// if ($(this).val()!=before_edit){
				// console.log('Ada Peruabahan');
				// // $(this).attr('input_edited');
				 // $(this).addClass('input_edited');
			// }
		// }
	// }
	
});
$('.auto_blur').on('summernote.blur', function() {
	// console.log('BLUR')
	// if ($("#st_edited").val()=='0'){
		// $(this).removeClass('input_edited');
		simpan_assesmen();
	// }else{
		// if ($("#st_edited").val()=='1'){
			// if ($(this).val()!=before_edit){
				// console.log('Ada Peruabahan');
				// // $(this).attr('input_edited');
				 // $(this).addClass('input_edited');
			// }
		// }
	// }
});
function simpan_assesmen(){
// alert(load_awal_assesmen);
if (status_assemen !='2'){
	
	if (load_awal_assesmen==false){
	let assesmen_id=$("#assesmen_id").val();
	// alert($("#tanggal_keluar").val());return false;
	let tanggaldatang=$("#tanggaldatang").val();
	let waktudatang=$("#waktudatang").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	if (assesmen_id){
		console.log('SIMPAN');
		
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					// nama_template:nama_template,
					tanggaldatang:tanggaldatang,
					waktudatang:waktudatang,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					assesmen_id:$("#assesmen_id").val(),
					nama_template:nama_template,
					status_assemen:$("#status_assemen").val(),
					st_edited:$("#st_edited").val(),
					jml_edit:$("#jml_edit").val(),
					
					tanggal_step_1 : $("#tanggal_step_1").val(),
					ruang_opr : $("#ruang_opr").val(),
					jenis_bedah : $("#jenis_bedah").val(),
					diagnosa : $("#diagnosa").val(),
					jenis_opr : $("#jenis_opr").val(),
					subject_penyakit : $("#subject_penyakit").val(),
					subject_penyakit_nama : $("#subject_penyakit_nama").val(),
					keterangan_step_1 : $("#keterangan_step_1").val(),
					gcs_e : $("#gcs_e").val(),
					gcs_m : $("#gcs_m").val(),
					gcs_v : $("#gcs_v").val(),
					nadi : $("#nadi").val(),
					nafas : $("#nafas").val(),
					spo2 : $("#spo2").val(),
					td_sistole : $("#td_sistole").val(),
					td_diastole : $("#td_diastole").val(),
					suhu : $("#suhu").val(),
					tinggi_badan : $("#tinggi_badan").val(),
					berat_badan : $("#berat_badan").val(),
					mallampati : $("#mallampati").val(),
					gigi : $("#gigi").val(),
					gol_darah : $("#gol_darah").val(),
					leher : $("#leher").val(),
					keterangan_obj : $("#keterangan_obj").val(),
					kardio : $("#kardio").val(),
					masalah_lain : $("#masalah_lain").val(),
					lab : $("#lab").val(),
					ekg : $("#ekg").val(),
					status_fisik : $("#status_fisik").val(),
					perencanaan_anestesi : $("#perencanaan_anestesi").val(),
					perawat_anes_id : $("#perawat_anes_id").val(),
					perawat_anes_ttd : $("#perawat_anes_ttd").val(),
					tanggal_step_2 : $("#tanggal_step_2").val(),
					ruang_opr_2 : $("#ruang_opr_2").val(),
					jenis_bedah_2 : $("#jenis_bedah_2").val(),
					da : $("#da").val(),
					penata_anestesi : $("#penata_anestesi").val(),
					jam_tv_2 : $("#jam_tv_2").val(),
					tingkat_kesadaran_2 : $("#tingkat_kesadaran_2").val(),
					nadi_2 : $("#nadi_2").val(),
					nafas_2 : $("#nafas_2").val(),
					spo2_2 : $("#spo2_2").val(),
					td_sistole_2 : $("#td_sistole_2").val(),
					td_diastole_2 : $("#td_diastole_2").val(),
					suhu_2 : $("#suhu_2").val(),
					cath : $("#cath").val(),
					keterangan_penilain_induksi : $("#keterangan_penilain_induksi").val(),
					induksi : $("#induksi").val(),
					induksi_lain : $("#induksi_lain").val(),
					ett : $("#ett").val(),
					teknik : $("#teknik").val(),
					teknik_ket : $("#teknik_ket").val(),
					pengaturan_nafas : $("#pengaturan_nafas").val(),
					pengaturan_nafas_ket : $("#pengaturan_nafas_ket").val(),
					ventilator : $("#ventilator").val(),
					ventilator_volume : $("#ventilator_volume").val(),
					ventilator_rr : $("#ventilator_rr").val(),
					masalah_durante : $("#masalah_durante").val(),
					tindakan_durante : $("#tindakan_durante").val(),
					blok_teknik : $("#blok_teknik").val(),
					blok_lokasi : $("#blok_lokasi").val(),
					jenis_obat : $("#jenis_obat").val(),
					dosis : $("#dosis").val(),
					perawat_anes_id_2 : $("#perawat_anes_id_2").val(),
					perawat_anes_ttd_2 : $("#perawat_anes_ttd_2").val(),
					tanggal_step_3 : $("#tanggal_step_3").val(),
					ruang_opr_3 : $("#ruang_opr_3").val(),
					tiba_pemulihan : $("#tiba_pemulihan").val(),
					petugas_penyerah_id : $("#petugas_penyerah_id").val(),
					petugas_penyerah_ttd : $("#petugas_penyerah_ttd").val(),
					petugas_penerima_id : $("#petugas_penerima_id").val(),
					petugas_penerima_ttd : $("#petugas_penerima_ttd").val(),
					intruksi_pasca_anestesi : $("#intruksi_pasca_anestesi").val(),
					tingkat_kesadaran_3 : $("#tingkat_kesadaran_3").val(),
					td_sistole_3 : $("#td_sistole_3").val(),
					td_diastole_3 : $("#td_diastole_3").val(),
					spo2_3 : $("#spo2_3").val(),
					respirasi_id_3 : $("#respirasi_id_3").val(),
					respirasi_3 : $("#respirasi_3").val(),
					// total_skor_nyeri : $("#total_skor_nyeri").val(),
					nama_kajian : $("#nama_kajian").val(),
					singkatan_kajian : $("#singkatan_kajian").val(),
					mnyeri_id : $("#mnyeri_id").val(),
					komplikasi : $("#komplikasi").val(),
					komplikasi_tindakan : $("#komplikasi_tindakan").val(),
					metodi_penilain : $("#metodi_penilain").val(),
					lama_pasien : $("#lama_pasien").val(),
					pindah_jam : $("#pindah_jam").val(),
					keruang_ranap : $("#keruang_ranap").val(),
					petugas_penyerah_id_3 : $("#petugas_penyerah_id_3").val(),
					petugas_penyerah_ttd_3 : $("#petugas_penyerah_ttd_3").val(),
					petugas_penerima_id_3 : $("#petugas_penerima_id_3").val(),
					petugas_penerima_ttd_3 : $("#petugas_penerima_ttd_3").val(),
					intruksi_pasca_anestesi_3 : $("#intruksi_pasca_anestesi_3").val(),
					observasi : $("#observasi").val(),
					observasi_posisi : $("#observasi_posisi").val(),
					puasa : $("#puasa").val(),
					mual : $("#mual").val(),
					manajemen_nyeri : $("#manajemen_nyeri").val(),
					penunjang : $("#penunjang").val(),
					perawat_anes_id_3 : $("#perawat_anes_id_3").val(),
					perawat_anes_ttd_3 : $("#perawat_anes_ttd_3").val(),
					keterangan_penilaian : $("#keterangan_penilaian").val(),



				},
			success: function(data) {
				
						console.log(data);
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					if (data.status_assemen=='1'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}else{
						if (data.status_assemen=='3'){
							
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
						}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
							$("#cover-spin").show();
							location.reload();			
						}
						
					}
				}
			}
		});
		}
	 }
	}
}
function list_history_pengkajian(){
	let assesmen_id=$("#assesmen_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let mppa_id=$("#mppa_id").val();
	let iddokter=$("#iddokter").val();
	let idrawat_ranap=$("#idrawat_ranap").val();
	let notransaksi=$("#notransaksi").val();
	let tanggal_input_1=$("#tanggal_input_1").val();
	let tanggal_input_2=$("#tanggal_input_2").val();
	let idtipe=$("#idtipe").val();
	let idruang=$("#idruang").val();
	let idbed=$("#idbed").val();
	let idkelas=$("#idkelas").val();
	$('#index_history_kajian').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_history_kajian').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					{  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,5,6,7] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [1,2,3,5] },
					 { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_pra_sedasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						iddokter:iddokter,
						mppa_id:mppa_id,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						idrawat_ranap:idrawat_ranap,
						idtipe:idtipe,
						idruang:idruang,
						idbed:idbed,
						idkelas:idkelas,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
function create_pra_sedasi(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

$("#template_id").on("change", function(){
	let template_id=$(this).val();
	if (template_id!=template_id_last){
			swal({
			  title: 'Anda Yakain?',
			  text: "Mengganti Mode Penilaian!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Proses!'
			}).then(function(json_data) {
					$("#cover-spin").show();
					let assesmen_id=$("#assesmen_id").val();
					template_id_last=template_id;
					$.ajax({
						url: '{site_url}Tpendaftaran_ranap_erm/simpan_risiko_jatuh_template_pra_sedasi', 
						dataType: "JSON",
						method: "POST",
						data : {
								assesmen_id:assesmen_id,
								template_id:template_id,
								
							},
						success: function(data) {
							
									console.log(data);
							if (data==null){
								swal({
									title: "Gagal!",
									text: "Simpan TTV.",
									type: "error",
									timer: 1500,
									showConfirmButton: false
								});

							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Ganti Mode Penilaian '});
								location.reload();			
							}
						}
					});
			}, function(dismiss) {
			  if (dismiss === 'cancel' || dismiss === 'close') {
					$("#template_id").val(template_id_last).trigger('change.select2');
			  } 
			})
		}else{
			save_all_detail(kategori_id_last);
		}
		
	
	
});

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_durante_id_list").removeAttr('disabled');
		 $("#diagnosa_paska_id_list").removeAttr('disabled');
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $(".data_asuhan_durante").removeAttr('disabled');
		 $(".data_asuhan_paska").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
		 $(".wizard-prev").removeAttr('disabled');
		 $(".wizard-next").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_pra_sedasi();
}
function create_with_template_pra_sedasi(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_pra_sedasi(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_template_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_template_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$("#ruang_opr").change(function(){
	let ruang_opr=$(this).val();
	if ($("#ruang_opr_2").val()==''){
		$("#ruang_opr_2").val(ruang_opr).trigger('change.select2');
	}
	if ($("#ruang_opr_3").val()==''){
		$("#ruang_opr_3").val(ruang_opr).trigger('change.select2');
	}
});

$("#jenis_bedah").change(function(){
	let jenis_bedah=$(this).val();
	if ($("#jenis_bedah_2").val()==''){
		$("#jenis_bedah_2").val(jenis_bedah).trigger('change.select2');
	}
	if ($("#jenis_bedah_3").val()==''){
		$("#jenis_bedah_3").val(jenis_bedah).trigger('change.select2');
	}
});
$("#tanggal_step_1").change(function(){
	let tanggal_step_1=$(this).val();
	$("#tanggal_step_2").val(tanggal_step_1);
	$("#tanggal_step_3").val(tanggal_step_1);
});
$("#tingkat_kesadaran_2").change(function(){
	let tingkat_kesadaran_2=$(this).val();
	if ($("#tingkat_kesadaran_3").val()==''){
		$("#tingkat_kesadaran_3").val(tingkat_kesadaran_2).trigger('change.select2');
	}
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			// simpan_edukasi_pra_sedasi();
			// simpan_riwayat_penyakit_pra_sedasi();
			simpan_assesmen();
		});		
	
}
function close_template(){
if ($("#nama_template").val()==''){
	sweetAlert("Maaf...", "Tentukan Nama Template", "error");
	return false;
}

swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menyimpan Template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#status_assemen").val(4);
		nama_template=$("#nama_template").val();
		simpan_assesmen();
	});		

}



$(document).on('change','#template_assesmen_id',function(){
	if ($(this).val()!=''){
		$("#btn_create_with_template_pra_sedasi").removeAttr('disabled');
	}else{
		$("#btn_create_with_template_pra_sedasi").attr('disabled','disabled');
	}
});
$(document).on('change','#riwayat_penyakit_id',function(){
	var select_button_text
		select_button_text = $('#riwayat_penyakit_id option:selected')
				.toArray().map(item => item.text).join();
	$("#riwayat_penyakit_nama").val(select_button_text);
});
function list_index_template_pra_sedasi(){
	let assesmen_id=$("#assesmen_id").val();
	$('#index_template_assemen').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_template_assemen').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					{  className: "text-right", targets:[0] },
					 { "width": "5%", "targets": [0] },
					 { "width": "20%", "targets": [1] },
					 { "width": "15%", "targets": [3] },
					 { "width": "60%", "targets": [2] }
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/list_index_template_pra_sedasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}

function edit_template_assesmen(assesmen_id){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	// alert(pendaftaran_id_ranap);
	// return false;
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Edit Template?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/edit_template_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					
				},
			success: function(data) {
				
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
					$("#cover-spin").show();
					location.reload();	
				}
			}
		});
	});			
	
	
}

function edit_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_edit").modal('show');
	document.getElementById("modal_edit").style.zIndex = "1201";
	$("#alasan_id_edit").select2({
		dropdownParent: $("#modal_edit")
	  });
}
function hapus_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_hapus").modal('show');
	document.getElementById("modal_hapus").style.zIndex = "1201";
	$("#alasan_id").select2({
		dropdownParent: $("#modal_hapus")
	  });
	// document.getElementById("alasan_id").style.zIndex = "1202";
}
function save_edit_pra_sedasi(){
	let jml_edit=$("#jml_edit").val();
	let keterangan_edit=$("#keterangan_edit").val();
	let alasan_id=$("#alasan_id_edit").val();
	let assesmen_detail_id=$("#assesmen_detail_id").val();
	// alert(assesmen_detail_id);return false;
	$("#st_sedang_edit").val(1);
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_edit").modal('hide');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/save_edit_pra_sedasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				jml_edit:jml_edit,
				assesmen_id:assesmen_detail_id,
				alasan_id:alasan_id,
				keterangan_edit:keterangan_edit,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Gagal!",
					text: "Edit.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				$("#cover-spin").show();
				let pendaftaran_id=data.pendaftaran_id;
				$("#cover-spin").show();
				window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_ri/input_pra_sedasi'); ?>";
			}
		}
	});
}
function hapus_record_pra_sedasi(){
	let id=$("#assesmen_detail_id").val();
	let keterangan_hapus=$("#keterangan_hapus").val();
	let alasan_id=$("#alasan_id").val();
	
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_hapus").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Inputan Assesmen ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					keterangan_hapus:keterangan_hapus,
					alasan_id:alasan_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					list_history_pengkajian();
					$("#modal_hapus").modal('hide');
				}
			}
		});
	});

}
function hapus_template_assesmen(id){

	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Template Assesmen ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_pra_sedasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					keterangan_hapus:'HAPUS TEMPLATE',
					alasan_id:null,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				list_index_template_pra_sedasi();
			}
		});
	});

}
</script>