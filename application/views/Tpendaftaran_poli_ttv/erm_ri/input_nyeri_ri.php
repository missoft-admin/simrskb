<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_nyeri_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_nyeri_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						
						$tanggalpengkajian=HumanDateShort($tanggal_pengkajian);
						$waktupengkajian=HumanTime($tanggal_pengkajian);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_nyeri_ri=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_nyeri_ri=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="total_skor_nyeri" value="<?=$total_skor_nyeri?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_nyeri_ri=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_awal()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id?>/erm_rj/input_nyeri_ri" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Pengkajian</a>
										<?}?>
										<?if ($status_assemen=='2'){?>
										<button class="btn btn-default btn_back"  type="button" onclick="goBack()"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs">
							<li class="<?=($assesmen_id?'active':'')?>">
								<a href="#tab_11"><i class="si si-pencil"></i> PENILAIAN</a>
							</li>
							<li class="<?=($assesmen_id==''?'active':'')?>">
								<a href="#tab_22" onclick="list_history_pengkajian_bar()" ><i class="fa fa-history"></i> HISTORY PENILAIAN</a>
							</li>
							
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left <?=($assesmen_id?'active in':'')?>" id="tab_11">
								<?if ($assesmen_id!=''){?>
								<div class="form-group">
									<div class="col-md-12">
										<div class="col-md-6 ">
											<div class="col-md-4 ">
											<label for="example-input-normal">Tanggal Pengkajian</label>
												<div class="input-group date">
													<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggalpengkajian" placeholder="HH/BB/TTTT" name="tanggalpengkajian" value="<?= $tanggalpengkajian ?>" required>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<div class="col-md-3 ">
												<label for="example-input-normal">Waktu Pengkajian</label>
												<div class="input-group">
													<input tabindex="3" type="text"  class="time-datepicker form-control auto_blur" id="waktupengkajian" value="<?= $waktupengkajian ?>" required>
													<span class="input-group-addon"><i class="si si-clock"></i></span>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<div class="form-group" >
											<table class="block-table text-center ">
												<tbody>
													<tr>
														<td class="border-r" style="width:70%">
															<div>
																<div class="input-group">
																	<input class="form-control" disabled type="text" readonly value="<?=$nama_kajian?>" id="nama_kajian" placeholder="nama_kajian" >
																	<span class="input-group-addon"><i class="fa fa-user"></i></span>
																</div>
															</div>
															<div class="h5 font-w700 text-left text-primary push-5-t"><?=$isi_header?></div>
														</td>
														<td class="border-r"  style="width:30%">
															<div class="h1 font-w700 text-primary" id="div_total_skor_nyeri"><?=$total_skor_nyeri?></div>
															<div class="h5 text-uppercase push-5-t">Total Skor Skala Nyeri</div>
														</td>
													</tr>
												</tbody>
											</table>
											
										</div>
										
									</div>
								</div>
								<?if ($mnyeri_id=='1'){?>
								<div class="form-group">
									<div class="col-md-12">
										<div class="col-md-12">
											<div id="div_tabel_nrs" class="form-group">
												
											</div>
										</div>
									</div>
								</div>
								<?}else{?>
								<div class="form-group">
									<div class="col-md-12">
										<div class="col-md-12">
											<div class="form-group">
												<table class="table table-bordered" id="tabel_non_nrs">
													<thead>
														<tr>
															<th width="10%">No</th>
															<th width="40%">Pengkajian</th>
															<th width="30%">Jawaban</th>
															<th width="20%">Skor</th>
														</tr>
													</thead>
													<tbody>
														<!-- {{ @action }} -->
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								
								<?}?>
								
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="col-md-6 ">
											<label for="example-input-normal">Lokasi Nyeri</label>
											<select tabindex="8" id="lokasi_nyeri" name="lokasi_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($lokasi_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(44) as $row){?>
												<option value="<?=$row->id?>" <?=($lokasi_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($lokasi_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										<div class="col-md-6 ">
											<label for="example-input-normal">Penyebab Nyeri</label>
											<select tabindex="8" id="penyebab_nyeri" name="penyebab_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($penyebab_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(45) as $row){?>
												<option value="<?=$row->id?>" <?=($penyebab_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($penyebab_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										
									</div>
									<div class="col-md-6 ">
										<div class="col-md-6 ">
											<label for="example-input-normal">Durasi Nyeri</label>
											<select tabindex="8" id="durasi_nyeri" name="durasi_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($durasi_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(46) as $row){?>
												<option value="<?=$row->id?>" <?=($durasi_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($durasi_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										<div class="col-md-6 ">
											<label for="example-input-normal">Frekuensi Nyeri</label>
											<select tabindex="8" id="frek_nyeri" name="frek_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($frek_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(47) as $row){?>
												<option value="<?=$row->id?>" <?=($frek_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($frek_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="col-md-6 ">
											<label for="example-input-normal">Jenis Nyeri</label>
											<select tabindex="8" id="jenis_nyeri" name="jenis_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($jenis_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(48) as $row){?>
												<option value="<?=$row->id?>" <?=($jenis_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										<div class="col-md-6 ">
											<label for="example-input-normal">Skala Nyeri</label>
											<select tabindex="8" id="skala_nyeri" name="skala_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($skala_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(49) as $row){?>
												<option value="<?=$row->id?>" <?=($skala_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($skala_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										
									</div>
									<div class="col-md-6 ">
										<div class="col-md-6 ">
											<label for="example-input-normal">Karakteristik</label>
											<select tabindex="8" id="karakteristik_nyeri" name="karakteristik_nyeri" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($karakteristik_nyeri == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(50) as $row){?>
												<option value="<?=$row->id?>" <?=($karakteristik_nyeri == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($karakteristik_nyeri == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										<div class="col-md-6 ">
											<label for="example-input-normal">Nyeri Hilang Bila</label>
											<select tabindex="8" id="nyeri_hilang" name="nyeri_hilang" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($nyeri_hilang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(51) as $row){?>
												<option value="<?=$row->id?>" <?=($nyeri_hilang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($nyeri_hilang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12 ">
										<div class="col-md-12 ">
											<label class="text-primary" ><?=$isi_footer?></h5>
										</div>
										
									</div>
								</div>
								<?}?>
							</div>
							<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_22">
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<div id="div_tabel_bar"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
						
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<?foreach($list_poli as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_awal_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="jenis_ttd" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_ttd==''?'selected':'')?>>Silahkan Pilih</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Pasien Sendiri Sendiri</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?> >Keluarga Terdekat / Wali</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?> >Penanggung Jawab</option>
										
									</select>
									<label for="jenis_ttd">Yang Tandatangan</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							<div class="col-md-6 push-10">
								<div class="form-material">
									<select tabindex="30" id="ttd_hubungan" name="ttd_hubungan" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($ttd_hubungan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<label for="ttd_hubungan">Hubungan</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_default" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tentukan Pengkajian Nyeri <?=$mnyeri_id?></h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									
									<select tabindex="8" <?=($st_spesifik_nyeri_ri=='1' && $st_kunci_default_nyeri_ri=='1'?'disabled':'')?> id="mnyeri_id" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" <?=($mnyeri_id==0?'selected':'')?>>-Belum Dipilih-</option>
									<?foreach(get_all('mnyeri',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>" <?=($mnyeri_id==$row->id?'selected':'')?>><?=$row->nama.' ('.$row->singkatan.')'?></option>
									<?}?>
									</select>
									<label for="jenis_ttd">Pengkajian Nyeri</label>
								</div>
							</div>
							
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" onclick="create_assesmen()"><i class="fa fa-save"></i> Create</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_default_template" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tentukan Pengkajian Nyeri</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									
									<select tabindex="8" id="mnyeri_id_2" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" <?=($mnyeri_id==0?'selected':'')?>>-Belum Dipilih-</option>
									<?foreach(get_all('mnyeri',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>" <?=($mnyeri_id==$row->id?'selected':'')?>><?=$row->nama.' ('.$row->singkatan.')'?></option>
									<?}?>
									</select>
									<label for="jenis_ttd">Pengkajian Nyeri</label>
								</div>
							</div>
							
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" onclick="create_template()"><i class="fa fa-save"></i> Create</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function create_awal(){
	$("#modal_default").modal('show');
	$("#mnyeri_id").select2({
		dropdownParent: $("#modal_default")
	  });
}
function create_awal_template(){
	$("#modal_default_template").modal('show');
	$("#mnyeri_id_2").select2({
		dropdownParent: $("#modal_default_template")
	  });
}
$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		if ($("#mnyeri_id").val()=='1'){
			load_nrs();
			
		}else{
			load_non_nrs();
		}
		
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian_bar();
	}
	
});
function load_non_nrs(){
	assesmen_id=$("#assesmen_id").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap_erm/load_nilai_param_nyeri_ri', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#tabel_non_nrs tbody").empty();
			$("#tabel_non_nrs tbody").append(data.tabel);
			$(".nilai_nyeri_ri").select2();
			$("#total_skor_nyeri").val(data.total_skor);
			$("#div_total_skor_nyeri").text(data.total_skor);
		}
	});
}
$(document).on("change",".nilai_nyeri_ri",function(){
	assesmen_id=$("#assesmen_id").val();
	var tr=$(this).closest('tr');
	var parameter_id=tr.find(".parameter_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap_erm/update_nilai_nyeri_ri/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				parameter_id:parameter_id,nilai_id:nilai_id,assesmen_id:assesmen_id
		  },success: function(data) {
			  load_non_nrs();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
		  }
		});
	
});
function load_nrs(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap_erm/load_nrs', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#div_tabel_nrs").append(data);
			disabel_edit();
		}
	});
}
function set_skala_nyeri_ri($trx_id,$trx_nilai,$trx_tabel){
	
	let assesmen_id=$("#assesmen_id").val();
	let trx_id=$("#trx_id").val();
	// if (trx_id){
		// return false;
	// }else{
		
	$.ajax({
		url: '{site_url}tpendaftaran_ranap_erm/set_skala_nyeri_ri', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:$("#assesmen_id").val(),
				nama_tabel:$trx_tabel,
				trx_id:$trx_id,
				trx_nilai:$trx_nilai,
			},
		success: function(data) {
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Assesmen.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				$("#total_skor_nyeri").val($trx_nilai);
				$("#div_total_skor_nyeri").text($trx_nilai);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				
			}
		}
	});
	// }
}
function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd(){
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}

	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap_erm/simpan_ttd_asmed/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
	  load_ttd();
}
function load_ttd(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap_erm/load_ttd_asmed', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;

			$('#sig').signature('enable'). signature('draw', src);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#opsi_raadio").prop('disabled',true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen(){
	
	$("#modal_default").modal('hide');
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let mnyeri_id=$("#mnyeri_id").val();
	if (mnyeri_id=='0'){
		sweetAlert("Maaf...", "Tentukan Pengkajian Nyeri", "error");
		return false;

	}
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_nyeri_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
					mnyeri_id:mnyeri_id,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				$("#cover-spin").show();
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	// $("#template_assesmen_id").val(id).trigger('change');
	create_with_template(id);
}
function create_with_template(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_nyeri_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_nyeri_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(){
	$("#modal_default_template").modal('hide');
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let mnyeri_id=$("#mnyeri_id_2").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_template_nyeri_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					mnyeri_id:mnyeri_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_nyeri_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_template_nyeri_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}
	$("#modal_savae_template_assesmen").modal('hide');
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
function simpan_assesmen(){
	// alert($("#keluhan_utama").val());
	if (load_awal_assesmen==false){
		if (status_assemen !='2'){
		

		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tanggalpengkajian=$("#tanggalpengkajian").val();
		let waktupengkajian=$("#waktupengkajian").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		// if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/save_nyeri_ri', 
				dataType: "JSON",
				method: "POST",
				data : {
						tglpendaftaran:tglpendaftaran,
						tanggalpengkajian:tanggalpengkajian,
						waktupengkajian:waktupengkajian,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						lokasi_nyeri:$("#lokasi_nyeri").val(),
						penyebab_nyeri:$("#penyebab_nyeri").val(),
						durasi_nyeri:$("#durasi_nyeri").val(),
						frek_nyeri:$("#frek_nyeri").val(),
						jenis_nyeri:$("#jenis_nyeri").val(),
						skala_nyeri:$("#skala_nyeri").val(),
						karakteristik_nyeri:$("#karakteristik_nyeri").val(),
						nyeri_hilang:$("#nyeri_hilang").val(),
						total_skor_nyeri:$("#total_skor_nyeri").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
		}
	}
}
	
	
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_index_template_nyeri_ri', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/edit_template_nyeri_ri', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idpoli=$("#idpoli").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_history_pengkajian_nyeri_ri', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							idpoli:idpoli,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function list_history_pengkajian_bar(){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let assesmen_id=$("#assesmen_id").val();
		$("#cover-spin").show();
		clear_table();
		
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/generate_header_ri/',
			dataType: "json",
			type: "POST",
			data: {
				pendaftaran_id_ranap: pendaftaran_id_ranap
			},
			success: function(data) {
				var content='';
				content +='<tr>';
				content +='<th class="text-center"><strong>No</strong></th>';
				content +='<th class="text-center"><strong>Parameter</strong></th>';

				
				$('#index_history_kajian_bar thead').empty();
				
				var kolom=[];
				var kolom_libur=[];
				
				var kol=3;
				var i=0;
				$.each(data.list_tanggal, function (i,list_tanggal) {	
					
					content +='<th class="text-center "><strong>'+list_tanggal.tanggalpengkajian+'<br>'+list_tanggal.waktupengkajian+'</strong></th>';
					i=i+1;
				});
				content +='</tr>';
				$('#index_history_kajian_bar thead').append(content);
				$('#index_history_kajian_bar tbody').empty();
				$.ajax({
					url: '{site_url}tpendaftaran_ranap_erm/list_history_pengkajian_nyeri_bar/',
					dataType: "json",
					type: "POST",
					data: {
						pendaftaran_id_ranap: pendaftaran_id_ranap,
						assesmen_id: assesmen_id
					},
					success: function(data) {
						// alert(data.tabel);
						$('#index_history_kajian_bar tbody').append(data.tabel);
						$("#cover-spin").hide();
						$('#index_history_kajian_bar').DataTable().destroy();
						table = $('#index_history_kajian_bar').removeAttr('width').DataTable({
							"autoWidth": false,
							// "pageLength": 10,
							autoWidth: true,
							// scrollX: true,
							// scrollY: 450,
							"ordering": false,
							"paging": false,
							"bInfo" : false,
							scrollCollapse: true,
							"processing": true,
							// "serverSide": true,
							
							// fixedColumns:   {
								// left: 2,
								// right: 1
							// },
							
							
							// columnDefs: [				
								// { className: "text-center","targets": kolom},
								
							// ]
							
						});
						window.fc = new $.fn.dataTable.FixedColumns(table,
						  {
							leftColumns: 2,
							rightColumns: 1
						  });
					}
				});
				
				
				
			}
		});
		
	}
	function clear_table(){
		$tabel='<table class="table table-bordered table-striped" id="index_history_kajian_bar">';
		$tabel +='<thead></thead><tbody></tbody></table>';
		// alert($tabel);
		$("#div_tabel_bar").html($tabel);
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/save_edit_nyeri_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,	
				},
			success: function(data) {
				// console.log(data);
					// return false;
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					let pendaftaran_id_ranap=data.pendaftaran_id_ranap;
					
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id_ranap+"/erm_ri/input_nyeri_ri'); ?>";
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		$("#modal_hapus").modal('hide');
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_nyeri_ri', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_nyeri_ri', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	function goBack() {
	  window.history.back();
	}

</script>