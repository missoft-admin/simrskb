<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	fieldset {
	  background-color: #eeeeee;
	  
	}

	legend {
	  background-color: gray;
	  color: white;
	  padding: 5px 5px;
	}
	.error{
		color:red;
	}
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_lap_bedah' || $menu_kiri=='input_lap_bedah_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_lap_bedah' || $menu_kiri=='input_lap_bedah_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
					$waktudatang=date('H:i:s');
					$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					
					if ($st_input_assesmen=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_lap_bedah=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_lap_bedah()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-validation-bootstrap form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >	
						<input type="hidden" id="pendaftaran_id_ranap_ini" value="<?=($st_ranap=='1'?$pendaftaran_id_ranap:$pendaftaran_id)?>" >						
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_lap_bedah=='1'){?>
									<button class="btn btn-primary" id="btn_create_lap_bedah" onclick="create_lap_bedah()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_lap_bedah()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_lap_bedah()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_lap_bedah/<?=$assesmen_id?>" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
										<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>

									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_lap_bedah" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_lap_bedah()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<?
							if ($tanggal_opr){
								$tanggal_opr=HumanDateShort($tanggal_input);
							}else{
								$tanggal_opr=date('d-m-Y');
								
							}
							if ($mulai){$mulai=HumanTime($mulai);}else{$mulai=date('H:i:s');}
							if ($selesai){$selesai=HumanTime($selesai);}else{$selesai=date('H:i:s');}
							
							
							$list_ruang_operasi=get_all('mruangan',array('idtipe'=>'2','status'=>1));
							$list_ppa_dokter=get_all('mppa',array('tipepegawai'=>'2','staktif'=>1));
							$list_ppa_all=get_all('mppa',array('staktif'=>1));
							$list_kelompok_operasi=get_all('mkelompok_operasi',array('status'=>1));
						?>
							<div class="form-group" style="margin-top: -10px;">
								
								<div class="col-md-2">
										<label for="jenis_bedah">Jenis Pembedahan</label>
										<select id="jenis_bedah" name="jenis_bedah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($jenis_bedah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(389) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_bedah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bedah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
								</div>
								<div class="col-md-2">
									<label for="tanggal_opr">Tanggal Operasi</label>
									<div class="input-group date">
										<input id="tanggal_opr" class="js-datepicker form-control auto_blur" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggal_opr ?>" >
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
								<div class="col-md-2">
									<label for="mulai">Mulai</label>
									<div class="input-group">
										<input id="mulai" class="time-datepicker form-control auto_blur" type="text" name="mulai" value="<?= $mulai ?>" >
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-2">
									<label for="selesai">Selesai</label>
									<div class="input-group">
										<input id="selesai" class="time-datepicker form-control auto_blur" type="text" name="selesai" value="<?= $selesai ?>" >
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="ruang_opr">Ruang Operasi</label>
									<select id="ruang_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($ruang_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach($list_ruang_operasi as $row){?>
										<option value="<?=$row->id?>" <?=($ruang_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="dokter_bedah">Dokter Bedah</label>
									<select id="dokter_bedah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($dokter_bedah == '' ? 'selected' : '')?>>Pilih Opsi</option>
										<?foreach($list_ppa_dokter as $row){?>
										<option value="<?=$row->id?>" <?=($dokter_bedah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<? $arr_assisten_bedah=($assisten_bedah?explode(',',$assisten_bedah):array()); ?>
								<div class="col-md-6">
									<label for="assisten_bedah">Asisten Bedah</label>
									<select id="assisten_bedah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_all as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_assisten_bedah)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="assisten_bedah_luar">Assisten Bedah (Jika tidak ada dalam pilihan)</label>
									<input id="assisten_bedah_luar" class="form-control auto_blur " type="text"  value="{assisten_bedah_luar}" name="assisten_bedah_luar" placeholder="" >
								</div>
								<? $arr_perawat_instrumen=($perawat_instrumen?explode(',',$perawat_instrumen):array()); ?>
								<div class="col-md-6">
									<label for="perawat_instrumen">Perawat Instrumen</label>
									<select id="perawat_instrumen" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_all as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_perawat_instrumen)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<? $arr_da=($da?explode(',',$da):array()); ?>
								<div class="col-md-6">
									<label for="da">Dokter Anestesi</label>
									<select id="da" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_dokter as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_da)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<? $arr_perawat_anestesi=($perawat_anestesi?explode(',',$perawat_anestesi):array()); ?>
								<div class="col-md-6">
									<label for="perawat_anestesi">Perawat Anestesi</label>
									<select id="perawat_anestesi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_ppa_all as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id,$arr_perawat_anestesi)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="diagnosa_pra">Diagnosa Pra Bedah</label>
									<input id="diagnosa_pra" class="form-control auto_blur " type="text"  value="{diagnosa_pra}" name="diagnosa_pra" placeholder="" required>
								</div>
								<div class="col-md-6">
									<label for="diagnosa_pasca">Diagnosa Pasca Bedah</label>
									<input id="diagnosa_pasca" class="form-control auto_blur " type="text"  value="{diagnosa_pasca}" name="diagnosa_pasca" placeholder="" required>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="tindakan">Tindakan</label>
									<input id="tindakan" class="form-control auto_blur " type="text"  value="{tindakan}" name="tindakan" placeholder="" >
								</div>
								<div class="col-md-6">
									<label for="disinfektan">Disinfektan Kulit</label>
									<input id="disinfektan" class="form-control auto_blur " type="text"  value="{disinfektan}" name="disinfektan" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="indikasi">Indikasi Operasi</label>
									<input id="indikasi" class="form-control auto_blur " type="text"  value="{indikasi}" name="indikasi" placeholder="" >
								</div>
								<div class="col-md-6">
									<label for="posisi">Posisi</label>
									<input id="posisi" class="form-control auto_blur " type="text"  value="{posisi}" name="posisi" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-3">
									<label for="pemasangan_implan">Pemasangan Implant</label>
									<select id="pemasangan_implan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($pemasangan_implan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(390) as $row){?>
										<option value="<?=$row->id?>" <?=($pemasangan_implan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemasangan_implan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-3">
									<label for="jenis_implan">Jenis Implant</label>
									<input id="jenis_implan" class="form-control auto_blur " type="text"  value="{jenis_implan}" name="jenis_implan" placeholder="" >
								</div>
								<div class="col-md-3">
									<label for="dikirim">Dikirim Pathologi Anatomi (PA)</label>
									<select id="dikirim" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($dikirim == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(391) as $row){?>
										<option value="<?=$row->id?>" <?=($dikirim == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($dikirim == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-3">
									<label for="asal_jaringan">Asal Jaringan</label>
									<input id="asal_jaringan" class="form-control auto_blur " type="text"  value="{asal_jaringan}" name="asal_jaringan" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-2">
									<label for="transfusi_darah">Transfusi Darah</label>
									<select id="transfusi_darah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($transfusi_darah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(392) as $row){?>
										<option value="<?=$row->id?>" <?=($transfusi_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($transfusi_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-2">
									<label for="jumlah_darah">Jumlah Darah Masuk</label>
									<div class=" input-group">
										<input id="jumlah_darah" class="form-control auto_blur"  type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?=$jumlah_darah?>" required>
										<span class="input-group-addon">CC</span>
									</div>
								</div>
								<div class="col-md-2">
									<label for="pendarahan">Pendarahan</label>
									<div class=" input-group">
										<input id="pendarahan" class="form-control auto_blur"  type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?=$pendarahan?>" required>
										<span class="input-group-addon">CC</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="klasifikasi_opr">Klarifikasi Operasi</label>
									<select id="klasifikasi_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($klasifikasi_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(124) as $row){?>
										<option value="<?=$row->id?>" <?=($klasifikasi_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($klasifikasi_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="jenis_opr">Jenis Operasi</label>
									<select id="jenis_opr" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_opr == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(393) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-3">
									<label for="jenis_anestesi">Jenis Anestesi</label>
									<select id="jenis_anestesi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_anestesi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(394) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_anestesi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_anestesi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-9">
									<label for="obat_anestesi">Obat Anestesi</label>
									<input id="obat_anestesi" class="form-control auto_blur " type="text"  value="{obat_anestesi}" name="obat_anestesi" placeholder="" >
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="macam_sayatan">Macan sayatan (bila perlu dengan gambar)</label>
									<textarea id="macam_sayatan" class="form-control auto_blur" name="macam_sayatan" ><?=$macam_sayatan?></textarea>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									
									
									<label for="laporan_pemedahan">Laporan Pembedahan<br><i><?=$catatan_ina?>
									</i></label>
									<textarea id="laporan_pemedahan" class="form-control auto_blur" name="laporan_pemedahan" ><?=$laporan_pemedahan?></textarea>
									
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									
									<label for="komplikasi">Komplikasi</label>
									<textarea id="komplikasi" class="form-control auto_blur" name="komplikasi" ><?=$komplikasi?></textarea>
									
								</div>
								<div class="col-md-6">
									<label for="intruksi_post">Intruksi Post Operasi</label>
									<textarea id="intruksi_post" class="form-control auto_blur" name="intruksi_post" ><?=$intruksi_post?></textarea>
									
								</div>
								
							</div>
						<?}?>
					</div>
					
					<?php echo form_close() ?>
					<?if ($assesmen_id){?>
					<?if ($status_assemen!='2'){?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								
								<form class="dropzone" action="{base_url}Tpendaftaran_ranap_erm/upload_files_lap_bedah" method="post" enctype="multipart/form-data">
									<input name="idtransaksi" type="hidden" value="{assesmen_id}">
								  </form>
							</div>
						</div>
					</div>
					<br>
					<?}?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<label for="pupil_1_ruangan">Upload Label Implan</label>
								<div id="div_label_implan" class="row items-push js-gallery-advanced">
									
								</div>
							</div>
						</div>
					</div>
					<?}?>
					<?if ($assesmen_id){?>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<table style="width:100%">
									<tr>
										<td style="width:33%" class="text-bold text-center"><strong>LAPORAN DIBUAT OLEH</strong></td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center"><strong>DOKTER BEDAH</strong></td>
									</tr>
									<tr>
										<td style="width:33%" class="text-bold text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($created_ppa)?>" alt="" title="">
										</td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center">
											<?if ($ttd_dokter){?>
												<div class="img-container fx-img-rotate-r">
													<img class="img-responsive" src="<?=$ttd_dokter?>" alt="">
													<?if ($status_assemen!='2'){?>
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="modal_ttd_ruangan_pemulihan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd('dokter_bedah')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
													<?}?>
												</div>
											<?}else{?>
												<?if ($dokter_bedah){?>
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($dokter_bedah)?>" alt="" title="">
												<?}else{?>
													<?if ($status_assemen=='2'){?>
													<button class="btn btn-sm btn-danger btn-menunggu" type="button"><i class="si si-user"></i> BELUM DITENTUKAN</button>
													<?}?>
												<?}?>
											<?}?>
													
											
											</td>
									</tr>
									<tr>
										<td style="width:33%" class="text-bold text-center text-bold"><i class="text-bold"><?=get_nama_ppa($created_ppa)?></i></td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center text-bold">
											<i class="text-bold"><?=($dokter_bedah?get_nama_ppa($dokter_bedah):'')?></i>
											<?if ($ttd_dokter==''){?>
											<?if ($status_assemen!='2'){?>
											<br>
											<button class="btn btn-sm btn-success" onclick="modal_ttd_ruangan_pemulihan()"  type="button"><i class="fa fa-paint-brush"></i> TANDA TANGAN DOKTER</button>
											<?}?>
											<?}?>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					<?}?>
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-right push-20-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_lap_bedah=='1'){?>
										<button class="btn btn-primary" id="btn_create_lap_bedah" onclick="create_lap_bedah()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										<?if ($assesmen_id!=''){?>
											<?if ($status_assemen=='3'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_lap_bedah()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
											<?}?>
											<?if ($status_assemen=='1'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_lap_bedah()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
											<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
											<?}?>
											<?if ($assesmen_id!=''){?>
											<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_lap_bedah/<?=$assesmen_id?>" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
											<?}?>
											<?if ($status_assemen=='3'){?>
												<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
											<?}?>
											<?if ($status_assemen=='2'){?>
												<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
												<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_rj/input_lap_bedah" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
											<?}?>
										<?}?>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<?if ($st_ranap=='1'){?>
													<option value="<?=$pendaftaran_id_ranap?>" >Dirawat Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}else{?>
													<option value="<?=$pendaftaran_id?>" >Poliklinik Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>

												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template_lap_bedah()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<?if ($assesmen_id!=''){?>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"></textarea>
								<input type="hidden" id="jenis_ttd" value="" >		
								
								<textarea id="ttd_dokter" name="ttd_dokter" style="display: none"><?=$ttd_dokter?></textarea>
								
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="petugas_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Dokter" required>
										<option value="0">-Pilih Dokter-</option>
										<?foreach(get_all('mppa',array('staktif'=>1)) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
									<label for="petugas_id">NAMA DOKTER</label>
								</div>
							</div>
							
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_lap_bedah()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_lap_bedah()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_upload" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">UPLOAD FILE </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<form class="dropzone" action="{base_url}Tpendaftaran_ranap_erm/upload_files_2_lap_bedah" method="post" enctype="multipart/form-data">
									<input name="idtransaksi_2" id="idtransaksi_2" type="hidden" value="">
								 </form>
							</div>
							
						</div>
					</div>
					
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script type="text/javascript" src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
 <script src="{js_path}plugins/magnific-popup/magnific-popup.min.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var myDropzone;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});

$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function hapus_ttd(jenis_ttd){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64").val('');
	$("#jenis_ttd").val(jenis_ttd);
	var signature64=$("#signature64").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_lap_bedah/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				jenis_ttd:jenis_ttd,
				petugas_id:$("#dokter_bedah").val(),
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").show();
				location.reload();
			}
		});
}
function simpan_ttd(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var petugas_id=$("#petugas_id").val();
	if ($("#petugas_id").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Nama ", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_lap_bedah/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				jenis_ttd:jenis_ttd,
				petugas_id:petugas_id,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  location.reload();
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
$(document).ready(function() {
			// $("#form1").validate();
	
	disabel_edit();
	$('#macam_sayatan,#komplikasi,#intruksi_post').summernote({
	  height: 100,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	})	
	$('#laporan_pemedahan').summernote({
	  height: 400,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	})	

	$('.number').number(true, 0);
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		refresh_image_lap_bedah();
		load_awal_assesmen=false;
		if ($("#status_assemen").val()!='2'){
			
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image_lap_bedah();
		  myDropzone.removeFile(file);
		  
		});
		}
	}else{
		list_history_pengkajian()
	}
	
	// load_data_rencana_asuhan_lap_bedah(1);
});
function upload_file(assesmen_id){
	$("#modal_upload").modal('show');
	$("#idtransaksi_2").val(assesmen_id);
}
function modal_ttd_ruangan_pemulihan(){
	$("#jenis_ttd").val('dokter_bedah');
	$("#signature64").val($("#ttd_dokter").val());
	$("#modal_ttd").modal('show');
	$('#petugas_id').select2({
	   dropdownParent: $('#modal_ttd')
	});
	if ($("#signature64").val()!=''){
	$('#sig').signature('enable').signature('draw', $("#signature64").val());
	}else{
		sig.signature('clear');
	}
	$("#petugas_id").val($("#dokter_bedah").val()).trigger('change');
	

}
function refresh_image_lap_bedah(){
	var id=$("#assesmen_id").val();
	$('#div_label_implan').html('');
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/refresh_image_lap_bedah/'+id,
		dataType: "json",
		success: function(data) {
			// if (data.detail!=''){
				$("#div_label_implan").append(data.detail);
			// }
			// console.log();
			
		}
	});
}
function hapus_file($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus data?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/removeFile_lap_bedah',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					refresh_image_lap_bedah();
					// $("#cover-spin").hide();
					// filter_form();
				}
			});
		});
}
	$(".opsi_change").change(function(){
		console.log('OPSI CHANTE')
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});

	$(".auto_blur").blur(function(){
		console.log('BLUR')
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
		
	});
	$('.auto_blur').on('summernote.blur', function() {
		console.log('BLUR')
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
	});
	function simpan_assesmen(){
	// alert(load_awal_assesmen);
	if (status_assemen !='2'){
		
		if (load_awal_assesmen==false){
		let assesmen_id=$("#assesmen_id").val();
		// alert($("#tanggal_keluar").val());return false;
		let tanggaldatang=$("#tanggaldatang").val();
		let waktudatang=$("#waktudatang").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/save_lap_bedah', 
				dataType: "JSON",
				method: "POST",
				data : {
						// nama_template:nama_template,
						tanggaldatang:tanggaldatang,
						waktudatang:waktudatang,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						
						jenis_bedah : $("#jenis_bedah").val(),
						tanggal_opr : $("#tanggal_opr").val(),
						ruang_opr : $("#ruang_opr").val(),
						mulai : $("#mulai").val(),
						selesai : $("#selesai").val(),
						dokter_bedah : $("#dokter_bedah").val(),
						assisten_bedah : $("#assisten_bedah").val(),
						assisten_bedah_luar : $("#assisten_bedah_luar").val(),
						perawat_instrumen : $("#perawat_instrumen").val(),
						da : $("#da").val(),
						perawat_anestesi : $("#perawat_anestesi").val(),
						diagnosa_pra : $("#diagnosa_pra").val(),
						diagnosa_pasca : $("#diagnosa_pasca").val(),
						tindakan : $("#tindakan").val(),
						disinfektan : $("#disinfektan").val(),
						indikasi : $("#indikasi").val(),
						posisi : $("#posisi").val(),
						pemasangan_implan : $("#pemasangan_implan").val(),
						jenis_implan : $("#jenis_implan").val(),
						dikirim : $("#dikirim").val(),
						asal_jaringan : $("#asal_jaringan").val(),
						transfusi_darah : $("#transfusi_darah").val(),
						jumlah_darah : $("#jumlah_darah").val(),
						pendarahan : $("#pendarahan").val(),
						klasifikasi_opr : $("#klasifikasi_opr").val(),
						jenis_opr : $("#jenis_opr").val(),
						jenis_anestesi : $("#jenis_anestesi").val(),
						obat_anestesi : $("#obat_anestesi").val(),
						macam_sayatan : $("#macam_sayatan").val(),
						laporan_pemedahan : $("#laporan_pemedahan").val(),
						komplikasi : $("#komplikasi").val(),
						intruksi_post : $("#intruksi_post").val(),

					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
			}
		 }
		}
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		let st_ranap=$("#st_ranap").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_lap_bedah', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							st_ranap:st_ranap,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
function create_lap_bedah(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_lap_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_lap_bedah();
}
function create_with_template_lap_bedah(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_lap_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_lap_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_lap_bedah(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let st_ranap=$("#st_ranap").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_template_lap_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_lap_bedah(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_lap_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_lap_bedah(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_template_lap_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	// $("#form1").reportValidity();
	// if($("#form1").valid()){
		if ($('#jenis_bedah').val()=='0' || $('#jenis_bedah').val()=='') {sweetAlert("Maaf...", "Isi Jenis Pembedahan", "error");return false; return false; }
		if ($('#ruang_opr').val()=='0' || $('#ruang_opr').val()=='') {sweetAlert("Maaf...", "Lengkapi Ruang Operasi", "error");return false; return false; }
		if ($('#dokter_bedah').val()=='0' || $('#dokter_bedah').val()=='') {sweetAlert("Maaf...", "Lengkapi Dokter Bedah", "error");return false; return false; }
		if ($('#diagnosa_pra').val()=='0' || $('#diagnosa_pra').val()=='') {sweetAlert("Maaf...", "Lengkapi Diagnosa Pra Bedah", "error");return false; return false; }
		if ($('#diagnosa_pasca').val()=='0' || $('#diagnosa_pasca').val()=='') {sweetAlert("Maaf...", "Lengkapi Diagnosa Pasca Bedah", "error");return false; return false; }
		if ($('#tindakan').val()=='0' || $('#tindakan').val()=='') {sweetAlert("Maaf...", "Lengkapi Tindakan", "error");return false; return false; }
		if ($('#disinfektan').val()=='') {sweetAlert("Maaf...", "Lengkapi Disenfektan Kulit", "error");return false; return false; }
		if ($('#indikasi').val()=='') {sweetAlert("Maaf...", "Lengkapi Indikasi Operasi", "error");return false; return false; }
		if ($('#mulai').val()=='') {sweetAlert("Maaf...", "Lengkapi Jam Mulai", "error");return false; return false; }
		if ($('#selesai').val()=='') {sweetAlert("Maaf...", "Lengkapi Jam Selesai", "error");return false; return false; }
		if ($('#posisi').val()=='') {sweetAlert("Maaf...", "Lengkapi Posisi", "error");return false; return false; }
		if ($('#klasifikasi_opr').val()=='0' || $('#klasifikasi_opr').val()=='') {sweetAlert("Maaf...", "Lengkapi Klasifikasi Operasi", "error");return false; return false; }
		if ($('#laporan_pemedahan').summernote('isEmpty')) {sweetAlert("Maaf...", "Lengkapi Laporan", "error");return false; return false; }
		if ($('#komplikasi').summernote('isEmpty')) {sweetAlert("Maaf...", "Lengkai Komplikasi", "error");return false; return false; }
		if ($('#intruksi_post').summernote('isEmpty')) {sweetAlert("Maaf...", "Lengkai Intruksi Post Operasi", "error");return false; return false; }
	
	
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			// simpan_assesmen_ttv();
			// simpan_edukasi_lap_bedah();
			// simpan_riwayat_penyakit_lap_bedah();
			simpan_assesmen();
		});		
	// }
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}

	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template_lap_bedah").removeAttr('disabled');
		}else{
			$("#btn_create_with_template_lap_bedah").attr('disabled','disabled');
		}
	});
	function list_index_template_lap_bedah(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_index_template_lap_bedah', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id_ranap);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_template_lap_bedah', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_lap_bedah(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		// alert(assesmen_detail_id);return false;
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_edit_lap_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id=data.pendaftaran_id;
					let st_ranap=data.st_ranap;
					$("#cover-spin").show();
					if (st_ranap=='1'){
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_ri/input_lap_bedah'); ?>";
						
					}else{
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_rj/input_lap_bedah_rj'); ?>";
						
					}
				}
			}
		});
	}
	function hapus_record_lap_bedah(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_lap_bedah', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_lap_bedah', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template_lap_bedah();
				}
			});
		});

	}
	
</script>