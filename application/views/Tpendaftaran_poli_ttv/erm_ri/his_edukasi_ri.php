<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.edited_final{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	.edited2{
		color:red;
		font-weight: bold;
	}
	.edited_final {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	table.dataTable tbody td {
	  vertical-align: top;
	}
</style>
<style>
	
	#sig_ttd_petugas canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_sasaran canvas{
		width: 100% !important;
		height: auto;
	}
</style>
sini
<?if ($menu_kiri=='his_edukasi_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_edukasi_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_pernyataan;
					if ($assesmen_id){
						
						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
					}
					
					$disabel_input='';
					if ($st_input_edukasi=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ri">
	<?if ($st_lihat_edukasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="tipe_rj_ri" value="3" >		
						<input type="hidden" id="st_sedang_edit" value="" >		
						<div class="col-md-12">
							<?if($st_lihat_edukasi=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ri/input_edukasi_ri" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label class="text-primary">IDENTIFIKASI EDUKASI</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-3 col-xs-6">
										<div class="form-material input-group date">
											<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
											<label for="tanggaldaftar">Tanggal </label>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-3 col-xs-6">
										<div class="form-material input-group">
											<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
											<label for="waktupendaftaran">Waktu</label>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-12">
										<label for="example-input-normal">Bahasa </label> <?=($bahasa_array_str!=$bahasa_array_asal?text_danger('Changed'):'')?>
										<select tabindex="8" id="bahasa_array" name="bahasa_array[]" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
											<?foreach(list_variable_ref(81) as $row){?>
											<option value="<?=$row->id?>" <?=(in_array($row->id, $bahasa_array)?'selected':'')?> ><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="example-input-normal">Dibutuhkan Penerjemaah ?</label>
										<select tabindex="8" id="st_penerjemaah" name="st_penerjemaah" class="<?=($st_penerjemaah!=$st_penerjemaah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($st_penerjemaah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(38) as $row){?>
											<option value="<?=$row->id?>" <?=($st_penerjemaah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_penerjemaah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">Jika Ya, Sebutkan</label>
										<input class="form-control <?=($penerjemaah!=$penerjemaah_asal?'edited':'')?>" type="text" id="penerjemaah"  value="{penerjemaah}" placeholder="Kebutuhan Penterjemaah" >
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-6">
										<label for="example-input-normal">Pendidikan</label>
										<select tabindex="8" id="pendidikan" name="pendidikan" class="<?=($pendidikan!=$pendidikan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
											<?foreach(list_variable_ref(13) as $row){?>
											<option value="<?=$row->id?>" <?=($pendidikan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">Baca dan Tulis</label>
										<select tabindex="8" id="baca_tulis" name="baca_tulis" class="<?=($baca_tulis!=$baca_tulis_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($baca_tulis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(82) as $row){?>
											<option value="<?=$row->id?>" <?=($baca_tulis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($baca_tulis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-4 ">
										<label for="example-input-normal">Pilih Cara Edukasi</label>
										<select tabindex="8" id="cara_edukasi" name="cara_edukasi" class="<?=($cara_edukasi!=$cara_edukasi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($cara_edukasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(83) as $row){?>
											<option value="<?=$row->id?>" <?=($cara_edukasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($cara_edukasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-8 ">
										<label for="example-input-normal">Kesediaan Pasien / Keluarga untuk menerima informasi yang diberikan</label>
										<select tabindex="8" id="st_menerima_info" name="st_menerima_info" class="<?=($st_menerima_info!=$st_menerima_info_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($st_menerima_info == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(39) as $row){?>
											<option value="<?=$row->id?>" <?=($st_menerima_info == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_menerima_info == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-12">
										<label for="example-input-normal">Hambatan Edukasi Edukasi</label> <?=($hambatan_edukasi_array_str!=$hambatan_edukasi_array_asal?text_danger('Changed'):'')?>
										<select tabindex="8" id="hambatan_edukasi_array" name="hambatan_edukasi_array[]" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
											<?foreach(list_variable_ref(40) as $row){?>
											<option value="<?=$row->id?>" <?=(in_array($row->id, $hambatan_edukasi_array)?'selected':'')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12">
										<label for="example-input-normal">Identifikasi Kebutuhan Edukasi</label> <?=($medukasi_array_str!=$medukasi_array_asal?text_danger('Changed'):'')?>
										<select tabindex="8" id="medukasi_array" name="medukasi_array[]" class="js-select2  form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
											<?foreach(get_all('medukasi',array('staktif'=>1)) as $row){?>
											<option value="<?=$row->id?>" <?=(in_array($row->id, $medukasi_array)?'selected':'')?>><?=$row->judul?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
											<div class="table-responsive">
												<table class="table table-bordered" id="tabel_materi">
													<thead>
														<tr>
															<th width="15%" class="text-center">Tanggal Dan Jam</th>
															<th width="40%" class="text-center">
																Penjelasan Materi Edukasi
																
															</th>
															<th width="10%" class="text-center">Tanda Tangan<br>Petugas & Profesi</th>
															<th width="10%" class="text-center">Tanda Tangan<br>Sasaran Edukasi</th>
															<th width="15%" class="text-center">Evaluasi</th>
														</tr>
													
													</thead>
													<tbody></tbody>
												</table>
											</div>
									</div>
								</div>
							</div>
							
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();

		if (assesmen_id){
			load_index_materi();
			list_index_history_edit();
		}
	load_awal_assesmen=false;
});
function load_index_materi(){
	$("#cover-spin").show();
	var assesmen_id=$("#assesmen_id").val();
	var status_assemen=$("#status_assemen").val();
	var versi_edit=$("#versi_edit").val();
	
	// alert(id);
	$.ajax({
		url: '{site_url}thistory_tindakan/load_index_materi_edukasi/',
		dataType: "json",
		type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				versi_edit:versi_edit,
				
				
		  },
		success: function(data) {
			$("#tabel_materi tbody").empty();
			$("#tabel_materi tbody").append(data.tabel);
			$("#cover-spin").hide();
			disabel_edit();
			
		}
	});
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".edited").removeClass("edited").addClass("edited_final");
		 $(".his_filter").removeAttr('disabled');
	}
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	let tipe_rj_ri=$("#tipe_rj_ri").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_edukasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						tipe_rj_ri:tipe_rj_ri,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
</script>