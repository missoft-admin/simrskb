<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.swal2-container {
	  z-index:10000;
	}
	.cls_racikan_update {
		border: 0;
		outline: none;
		width:90%;
	}
	.cls_racikan_update:focus {

		border: 2px solid #000;
		background-color: #fdffe2;
		border: 1;
	}
	.no_transaksi {

		border: 2px solid #000;
		background-color: red;
		border: 2;
	}
	.input_review {
		border: 0;
		font-weight: bold;

		outline: none;
		width:90%;
	}
	.input_review:focus {
		font-weight: bold;

		border: 2px solid #000;
		background-color: #fdffe2;
		border: 1;
	}
	
	.dosis_nrd {
		border: 0;
		outline: none;
		width:100%;
	}
	.dosis_nrd:focus {

		border: 2px solid #000;
		background-color: #fdffe2;
		border: 1;
	}

	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
		#sig_ttd_penerima canvas{
			width: 100% !important;
			height: auto;
		}
</style>

<?if ($menu_kiri=='lihat_perubahan'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='lihat_perubahan' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_permintaan;
		if ($tanggal_penyerah){
			$tanggalpenyerahan=HumanDateShort($tanggal_penyerah);
			$waktupenyerahan=HumanTime($tanggal_penyerah);
		}else{
			$tanggalpenyerahan=date('d-m-Y');
			$waktupenyerahan=date('H:i:s');
		}
		if ($tanggal_telaah_resep){
			$tanggaltelaah_resep=HumanDateShort($tanggal_telaah_resep);
			$waktutelaah_resep=HumanTime($tanggal_telaah_resep);
		}else{
			$tanggaltelaah_resep=date('d-m-Y');
			$waktutelaah_resep=date('H:i:s');
		}
		if ($tanggal_proses_resep){
			$tanggalproses_resep=HumanDateShort($tanggal_proses_resep);
			$waktuproses_resep=HumanTime($tanggal_proses_resep);
		}else{
			$tanggalproses_resep=date('d-m-Y');
			$waktuproses_resep=date('H:i:s');
		}
		$tab=1;
		// if ($status_tindakan=='0'){
			// $tab='1';
		// }
		// if ($status_tindakan=='1'){
			// $tab='3';
		// }
		// if ($status_tindakan=='2'){
			// $tab='4';
		// }
		// if ($status_tindakan=='3'){
			// $tab='5';
		// }
		// if ($status_tindakan=='5'){
			// $tab='1';
		// }
		// $tab=3;
		if ($assesmen_id){
			$tanggalpermintaan=HumanDateShort($tanggal_permintaan);
			$waktupermintaan=HumanTime($tanggal_permintaan);
			
			
			$tanggaldaftar=HumanDateShort($tanggal_input);
			$waktudaftar=HumanTime($tanggal_input);
			
			
		}else{
			$waktupermintaan=date('H:i:s');
			$waktuinfo=date('H:i:s');
			$waktudaftar=date('H:i:s');
			$tanggaldaftar=date('d-m-Y');
			$tanggalpermintaan=date('d-m-Y');
			
			
		}
		
		$disabel_input='';
					
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_eresep=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($tab=='1'?'active':'')?>">
					<a href="#tab_1" onclick="set_tab(1)"> Telaah Resep</a>
				</li>
				
				<?php 
				if ($tab>1){
				if (UserAccesForm($user_acces_form,array('1842'))){ ?>	
				<?if($status_tindakan>0){?>
				<li class="<?=($tab=='2'?'active':'')?>" >
					<a href="#tab_1"  onclick="set_tab(2)"> Review Resep</a>
				</li>
				<?}?>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1838'))){ ?>	
				<?if($status_tindakan>0){?>
				<li class="<?=($tab=='3'?'active':'')?>" >
					<a href="#tab_2"  onclick="set_tab(3)"> Review Transaksi</a>
				</li>
				<?}?>
				<?}?>
				<?if($status_tindakan!=5){?>
				<?if($status_tindakan>1){?>
				<li class="<?=($tab=='4'?'active':'')?>">
					<a href="#tab_1" onclick="set_tab(4)"> Telaah Obat</a>
				</li>
				<?}?>
				<?if($status_tindakan>1){?>
				<li class="<?=($tab=='5'?'active':'')?>">
					<a href="#tab_1" onclick="set_tab(5)"> Penyerahan</a>
				</li>
				<?}?>
				<?}?>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="block">
				<div class="row">
					<input type="hidden" id="jml_iter" value="{jml_iter}"> 
					<input type="hidden" id="max_iter" value="{max_iter}"> 
					<input type="hidden" id="st_browse" value="0"> 
					<input type="hidden" id="idtipe_poli" value="{idtipe_poli}"> 
					<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}"> 
					<input type="hidden" id="idpoliklinik" value="{idpoliklinik}"> 
					<input type="hidden" id="idrekanan" value="{idrekanan}"> 
					<input type="hidden" id="iddokter" value="{iddokter}"> 
					<input type="hidden" id="assesmen_id" value="{assesmen_id}"> 
					<input type="hidden" readonly id="assesmen_detail_id" value="">
					<input type="hidden" readonly id="st_edited" value="{st_edited}">
					<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
					<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
					<input type="hidden" id="status_tindakan" value="<?=$status_tindakan?>" >		
					<input type="hidden" id="st_telaah_resep" value="<?=$st_telaah_resep?>" >		
					<input type="hidden" id="tab" value="<?=$tab?>" >		
					<input type="hidden" id="idpenjualan" value="<?=$idpenjualan?>" >		
					<input type="hidden" id="assesmen_id_resep" value="<?=$assesmen_id_resep?>" >		
					<input type="hidden" id="st_manual_resep" value="<?=$st_manual_resep?>" >		
					<input type="hidden" id="st_direct" value="<?=$st_direct?>" >		
					
					<div class="form-group">
						<div class="col-md-2 col-xs-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<div class="col-md-12">
									<div class="form-material">
											<input class="form-control" disabled type="text" readonly value="<?=$noresep?>" id="noresep" name="noresep" placeholder="No E Resep">
										<label >No. Resep</label>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="col-md-3 col-xs-12">
							<div class="col-md-7 col-xs-6">
								<div class="form-material input-group date">
									<input tabindex="2" type="text"  class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggaltelaah_resep" placeholder="HH/BB/TTTT" name="tanggaltelaah_resep" value="<?= $tanggaltelaah_resep ?>" required>
									<label for="tanggaltelaah_resep">Tanggal <i class="text-muted">Telaah</i></label>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="col-md-5 col-xs-6">
								<div class="form-material input-group">
									<input tabindex="3" type="text"   class="time-datepicker form-control " id="waktutelaah_resep" name="waktutelaah_resep" value="<?= $waktutelaah_resep ?>" required>
									<label for="waktupendaftaran">Waktu <i class="text-muted">Telaah</i></label>
									<span class="input-group-addon"><i class="si si-clock"></i></span>
								</div>
							</div>
							
						</div>
						<div class="col-md-2 col-xs-12">
							<div class="col-md-12 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
												<input class="form-control" disabled type="text" readonly value="<?=($nama_telaah?$nama_telaah:$login_nama_ppa)?>" id="nama_ppa_verifikasi" name="nama_ppa_verifikasi" placeholder="Nama PPA" required>
											<label >Petugas Verifikasi </label>
										</div>
									</div>
									
									
								</div>
							</div>
							
						</div>
						<?if ($status_tindakan>0){?>
						<div class="col-md-2 col-xs-12">
							<div class="col-md-12 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
												<input class="form-control no_transaksi" type="text" readonly value="<?=($nopenjualan?$nopenjualan:'BELUM ADA')?>" id="nopenjualan" name="nopenjualan" placeholder="NO NAME" required>
											<label >No. Penjualan </label>
										</div>
									</div>
									
									
								</div>
							</div>
							
						</div>
						<?}?>
						<?if ($st_direct=='1'){?>
							<?if ($status_tindakan=='0'){?>
								<div class="col-md-4 col-xs-12">
									<div class="col-md-12 col-xs-12">
										<div class="form-group" style="margin-bottom: 10px;">
											<div class="col-md-12">
												<div class="form-material">
													<button class="btn btn-primary btn-sm" id="btn_cari_erersep" onclick="cari_eresep()" type="button"><i class="fa fa-search"></i> Cari Data E-Resep</button>
													<button class="btn btn-danger  btn-sm" id="btn_upload" onclick="upload_manual()"  type="button"><i class="fa fa-upload"></i> Upload Manual Resep</button>
													<?if ($resep_image){?>
														<a class="btn btn-default  btn-sm" target="_blank" type="button" href="{base_url}assets/upload/resep/<?=$resep_image?>"><i class="fa fa-file-text-o text-primary"></i> Lihat Resep</a>
													<?}?>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							<?}?>
							<?if ($status_tindakan>0){?>
							<?if ($resep_image){?>
								<div class="col-md-1 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<a class="btn btn-default  btn-sm" target="_blank" type="button" href="{base_url}assets/upload/resep/<?=$resep_image?>"><i class="fa fa-file-text-o text-primary"></i> Lihat Resep</a>
										</div>
									</div>
								
								</div>
							<?}?>
							<?}?>
						
						<?}?>
					</div>
					<div class="col-md-12">
						<?if($st_lihat_eresep=='1'){?>
							<h4 class="font-w700 push-5 text-center text-primary">TELAAH {judul_ina}</h4>
							<h5 class="push-5 text-center">{judul_eng}</h5>
						<?}else{?>
							<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
						<?}?>
					</div>
					<div class="col-md-12">
						
					</div>
					
					
					<div class="form-group">
						<div class="col-md-6 ">
							<div class="col-md-4 ">
							<label for="example-input-normal">{waktu_ina}</label>
								<div class="input-group date">
									<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tanggal" data-date-format="dd/mm/yyyy" id="tanggalpermintaan" placeholder="HH/BB/TTTT" name="tanggalpermintaan" value="<?= $tanggalpermintaan ?>" required>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="col-md-3 ">
								<label for="example-input-normal"><i class="text-muted">{waktu_eng}</i></label>
								<div class="input-group">
									<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tanggal" id="waktupermintaan" value="<?= $waktupermintaan ?>" required>
									<span class="input-group-addon"><i class="si si-clock"></i></span>
								</div>
							</div>
							<div class="col-md-5 ">
								<label for="example-input-normal">{tujuan_ina} / <i class="text-muted">{tujuan_eng}</i></label>
								<select tabindex="4" id="tujuan_farmasi_id" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<?foreach($list_tujuan_eresep as $row){?>
								<option value="<?=$row->id?>" <?=($tujuan_farmasi_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama_tujuan?></option>
								<?}?>
								</select>
							</div>
						</div>
						<div class="col-md-6 ">
							<div class="col-md-12 ">
								<label for="iddokter_resep">{dokter_ina} / <i class="text-muted">{dokter_eng}</i></label>
								<select tabindex="5" id="iddokter_resep" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($iddokter_resep == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach($list_ppa as $row){?>
									<option value="<?=$row->id?>" <?=($iddokter_resep == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
								
							</div>
						</div>
						
					</div>
					<?if ($status_tindakan>0){?>
					<div class="form-group push-10-t">
						<div class="col-md-6 ">
							<div class="col-md-4 ">
							<label for="example-input-normal">Tanggal Proses</label>
								<div class="input-group date">
									<input tabindex="2" type="text" <?=($status_tindakan>1?'disabled':'')?> class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggalproses_resep" placeholder="HH/BB/TTTT" name="tanggalproses_resep" value="<?= $tanggalproses_resep ?>" required>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="col-md-3 ">
								<label for="example-input-normal">Waktu proses</label>
								<div class="input-group">
									<input tabindex="3" type="text"  <?=($status_tindakan>1?'disabled':'')?>  class="time-datepicker form-control " id="waktuproses_resep" name="waktuproses_resep" value="<?= $waktuproses_resep ?>" required>
									<span class="input-group-addon"><i class="si si-clock"></i></span>
								</div>
							</div>
							<div class="col-md-5 ">
								<label for="example-input-normal">User Proses</label>
								<input class="form-control" disabled type="text" readonly value="<?=($nama_proses?$nama_proses:$login_nama_ppa)?>" id="nama_ppa_verifikasi" name="nama_ppa_verifikasi" placeholder="Nama PPA" required>
							</div>
						</div>
						<div class="col-md-6 ">
						<?if ($status_tindakan>='2'){?>
							<div class="col-md-6 ">
								<label for="iddokter_resep">Petugas Ambil</label>
								<select tabindex="5" <?=($status_tindakan>=3?'disabled':'')?> id="user_ambil" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($user_ambil==''?'selected':'')?>>Pilih Opsi</option>
									<?foreach($list_ppa_all as $row){?>
									<option value="<?=$row->id?>" <?=($user_ambil==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
								
							</div>
							<div class="col-md-6 ">
								<label for="iddokter_resep">Petugas E-Ticket</label>
								<select tabindex="5" <?=($status_tindakan>=3?'disabled':'')?> id="user_etiket" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($user_ambil==''?'selected':'')?>>Pilih Opsi</option>
									<?foreach($list_ppa_all as $row){?>
									<option value="<?=$row->id?>" <?=($user_etiket==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
								
							</div>
						<?}?>
						</div>
						
					</div>
					<?}?>
					<div class="form-group pull-10-t">
						<div class="col-md-6 ">
							<div class="col-md-4 ">
							<label for="example-input-normal">{bb_ina} / <i class="text-muted">{bb_eng}</i></label>
								<div class="input-group date">
									<input tabindex="6" type="text" class="form-control auto_blur number" id="berat_badan" value="<?= $berat_badan ?>" required>
									<span class="input-group-addon">KG</span>
								</div>
							</div>
							<div class="col-md-4 ">
								<label for="example-input-normal">{tb_ina} / <i class="text-muted">{tb_eng}</i></label>
								<div class="input-group">
									<input tabindex="7" type="text" class="form-control auto_blur number" id="tinggi_badan" value="<?= $tinggi_badan ?>" required>
									<span class="input-group-addon">CM</span>
								</div>
							</div>
							<div class="col-md-4 ">
								<label for="example-input-normal">{luas_ina} / <i class="text-muted">{luas_eng}</i></label>
								<input tabindex="8" type="text" class="form-control auto_blur number" id="luas_permukaan" value="<?= $luas_permukaan ?>" required>
							</div>
						</div>
						<div class="col-md-6 ">
							<div class="col-md-12 ">
								<label for="diagnosa">{diagnosa_ina} / <i class="text-muted">{diagnosa_eng}</i></label>
								<input tabindex="9" type="text" class="form-control auto_blur" id="diagnosa" value="<?= $diagnosa ?>" required>
								
							</div>
						</div>
						
					</div>
					<div class="form-group">
						<div class="col-md-6 ">
							<div class="col-md-4 ">
								<label for="example-input-normal">{menyusui_ina} / <i class="text-muted">{menyusui_eng}</i></label>
								<select tabindex="10" id="menyusui" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($menyusui == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach(list_variable_ref(107) as $row){?>
									<option value="<?=$row->id?>" <?=($menyusui == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($menyusui == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
							</div>
							<div class="col-md-4 ">
								<label for="example-input-normal">{gangguan_ina} / <i class="text-muted">{gangguan_eng}</i></label>
								<select tabindex="11" id="gangguan_ginjal" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($gangguan_ginjal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach(list_variable_ref(101) as $row){?>
									<option value="<?=$row->id?>" <?=($gangguan_ginjal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($gangguan_ginjal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
							</div>
							<div class="col-md-4 ">
								<label for="example-input-normal">{puasa_ina} / <i class="text-muted">{puasa_eng}</i></label>
								<select tabindex="12" id="st_puasa" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($st_puasa == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach(list_variable_ref(102) as $row){?>
									<option value="<?=$row->id?>" <?=($st_puasa == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_puasa == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
							</div>
						</div>
						<div class="col-md-6 ">
							<div class="col-md-4 ">
								<label for="diagnosa">{alergi_ina} / <i class="text-muted">{alergi_eng}</i></label>
								<select tabindex="13" id="st_alergi" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($st_alergi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach(list_variable_ref(58) as $row){?>
									<option value="<?=$row->id?>" <?=($st_alergi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_alergi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
								
							</div>
							<div class="col-md-8 ">
								<label for="diagnosa">{alergi_detail_ina} / <i class="text-muted">{alergi_detail_eng}</i></label>
								<div class="input-group">
									<input tabindex="14" type="text" class="form-control auto_blur" id="alergi" value="<?= $alergi ?>" required>
									<span class="input-group-btn">
										<button class="btn btn-danger" type="button" onclick="lihat_his_alergi_pasien(<?=$idpasien?>)"><i class="fa fa-info"></i></button>
									</span>
								</div>
							</div>
						</div>
						
					</div>
					<div class="form-group">
						<div class="col-md-6 ">
							<div class="col-md-6 ">
								<label for="diagnosa">{prioritas_ina} / <i class="text-muted">{prioritas_eng}</i></label>
								<select tabindex="15" id="prioritas_cito" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($prioritas_cito == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach(list_variable_ref(103) as $row){?>
									<option value="<?=$row->id?>" <?=($prioritas_cito == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($prioritas_cito == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
							</div>
							<div class="col-md-6 ">
								<label for="example-input-normal">{pulang_ina} / <i class="text-muted">{pulang_eng}</i></label>
								<select tabindex="16" id="resep_pulang" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($resep_pulang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									<?foreach(list_variable_ref(104) as $row){?>
									<option value="<?=$row->id?>" <?=($resep_pulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($resep_pulang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
							</div>
							
						</div>
						<div class="col-md-6 ">
							
							<div class="col-md-12 ">
								<label for="diagnosa">{catatan_ina} / <i class="text-muted">{catatan_eng}</i></label>
									<input tabindex="17" type="text" class="form-control auto_blur " id="catatan_resep" value="<?= $catatan_resep ?>" required>
							</div>
						</div>
						
					</div>
				</div>
				</div>
				<hr>
				<div class="tab-pane fade fade-left <?=($tab=='1' || $tab=='2' || $tab=='4' || $tab=='5'?'active in':'')?>" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
					
					<div class="row" id="div_input_normal">
					
						<?if ($assesmen_id!=''){?>
							
							<?$tab_resep=1;?>
							<div class="form-group">
								<div class="col-md-3 ">
									<div class="block div_input_tab_1">
										<div class="form-group" style="margin-top:10px">
											<div class="col-md-12 ">
												<label class="h5 text-primary"><strong>TELAAH RESEP FARMASI (VERIFIKASI RESEP AWAL)</strong></label>
												<hr>
												<table class="table table-condensed table-bordered" id="tabel_telaah" style="background-color:#fff">
													<thead>
														<tr>
															<th class="text-center" style="width: 5%;">NO</th>
															<th class="text-center" style="width: 70%;">ASPEK TELAAH</th>
															<th class="text-center" style="width: 25%;">HASIL TELAAH</th>
															
														</tr>
													</thead>
													<tbody>
														<?
														$arr_nilai=list_variable_ref(109);
														foreach($list_telaah as $r){?>
															<tr>
															<?if ($r->lev=='0'){?>
																<td style="background-color:#8cd4df" colspan="3"><strong><?=$r->nama?></strong></td>
															<?}else{?>
																<td><?=$r->nourut_no?><input type="hidden" class="telaah_id" value="<?=$r->id?>"></td>
																<td><?=$r->nama?></td>
																<td>
																	<select tabindex="16" class="js-select2 form-control jawaban_resep " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																		<option value="" <?=($r->jawaban_resep == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
																		<?foreach($arr_nilai as $row){?>
																		<option value="<?=$row->id?>" <?=($r->jawaban_resep == $row->id? 'selected="selected"' : '')?>><?=$row->nama?></option>
																		<?}?>
																		
																	</select>
																</td>
															<?}?>
															</tr>
														<?}?>
													</tbody>
												</table>
												
											</div>
										</div>
										<?php if (UserAccesForm($user_acces_form,array('1837'))){ ?>	
										<div class="form-group" style="margin-top:5px">
											<div class="pull-right">
												<div class="col-md-12 ">
													<?if ($status_tindakan=='0'){?>
													<button class="btn btn-success" id="btn_simpan_telaah" onclick="simpan_telaah()" type="button"><i class="fa fa-save"></i> SIMPAN TELAAH</button>
													<?}?>
												</div>
											</div>
										</div>
										<?}?>
									</div>
									<div class="block div_input_tab_4">
										<div class="form-group" style="margin-top:10px">
											<div class="col-md-12 ">
												<label class="h5 text-primary"><strong>TELAAH OBAT FARMASI</strong></label>
												<hr>
												<table class="table table-condensed table-bordered" id="tabel_telaah_obat" style="background-color:#fff">
													<thead>
														<tr>
															<th class="text-center" style="width: 5%;">NO</th>
															<th class="text-center" style="width: 70%;">ASPEK TELAAH</th>
															<th class="text-center" style="width: 25%;">HASIL TELAAH</th>
															
														</tr>
													</thead>
													<tbody>
														<?
														$arr_nilai=list_variable_ref(109);
														foreach($list_telaah_obat as $r){?>
															<tr>
															<?if ($r->lev=='0'){?>
																<td style="background-color:#8cd4df" colspan="3"><strong><?=$r->nama?></strong></td>
															<?}else{?>
																<td><?=$r->nourut_no?><input type="hidden" class="telaah_id" value="<?=$r->id?>"></td>
																<td><?=$r->nama?></td>
																<td>
																	<select tabindex="16" class="js-select2 form-control jawaban_resep " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																		<option value="" <?=($r->jawaban_obat == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
																		<?foreach($arr_nilai as $row){?>
																		<option value="<?=$row->id?>" <?=($r->jawaban_obat == $row->id? 'selected="selected"' : '')?>><?=$row->nama?></option>
																		<?}?>
																		
																	</select>
																</td>
															<?}?>
															</tr>
														<?}?>
													</tbody>
												</table>
												
											</div>
										</div>
										<?php if (UserAccesForm($user_acces_form,array('1837'))){ ?>	
										<div class="form-group" style="margin-top:5px">
											<div class="pull-right">
												<div class="col-md-12 ">
													<?if ($status_tindakan=='0'){?>
													<button class="btn btn-success" id="btn_simpan_telaah" onclick="simpan_telaah()" type="button"><i class="fa fa-save"></i> SIMPAN TELAAH</button>
													<?}?>
												</div>
											</div>
										</div>
										<?}?>
									</div>
									
									<div class="block div_input_tab_3">
										<ul class="nav nav-tabs" data-toggle="tabs">
											<li id="div_1" class="<?=($tab_resep=='1'?'active':'')?>">
												<a href="#tab_resep_1"  onclick="set_form_tab(1)">Non Racikan</a>
											</li>
											<li  id="div_2" class="<?=($tab_resep=='2'?'active':'')?>">
												<a href="#tab_resep_2"   onclick="set_form_tab(2)">Racikan </a>
											</li>
											<?if ($st_direct=='1'){?>
											<li  id="div_3" class="<?=($tab_resep=='3'?'active':'')?>">
												<a href="#tab_resep_3" onclick="set_form_tab(3)">Paket</a>
											</li>
											<li  id="div_4" class="<?=($tab_resep=='4'?'active':'')?>">
												<a href="#tab_resep_4"  onclick="set_form_tab(4)">Template</a>
											</li>
											<?}?>
										</ul>
										<div class="block-content tab-content" style="background-color:#f5f5f5">
											<div class="tab-pane fade fade-left <?=($tab_resep=='1'?'active in':'')?>" id="tab_resep_1" style="min-height: 400px">
												<div class="form-group">
													<div class="col-md-12 ">
														<label for="idbarang_1">Nama Obat <span id="st_label_cover">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
														<div class="input-group">
															<select tabindex="18" id="idbarang_1" class="js-special-select2" style="width:100%"></select>
															<span class="input-group-btn">
																<button class="btn btn-info" type="button" onclick="show_modal_obat(1)"><i class="fa fa-search"></i></button>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="jumlah_1">Jumlah</label>
														<input tabindex="18" type="text" class="form-control  number auto_input" id="jumlah_1" value="" required>
														<input  type="hidden" readonly  id="idtipe_1" value="" required>
														<input  type="hidden" readonly id="st_cover_1" value="" required>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label class="text-primary">Aturan Pakai :</label>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="interval_1">Frekuensi / Interval</label>
														<select tabindex="19" id="interval_1" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="">Pilih Opsi</option>
															<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
															<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
															<?}?>
															
														</select>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="dosis_1">Dosis Satu kali minum</label>
														<input tabindex="20" type="text" class="form-control  number auto_input" id="dosis_1" value="" required>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="rute_1">Rute Pemberian</label>
														<select tabindex="21" id="rute_1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="">Pilih Opsi</option>
															<?foreach(list_variable_ref(74) as $row){?>
															<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
															<?}?>
															
														</select>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12">
															<label for="aturan_tambahan_1">Aturan tambahan</label>
															<input tabindex="22" type="text" class="form-control auto_input" id="aturan_tambahan_1" value="" required>
														</div>
													
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="iter_1">ITER</label>
														<?=(opsi_iter($max_iter,$jml_iter,'iter_1','opsi_change_edit'))?>
													</div>
												</div>
												<?php if (UserAccesForm($user_acces_form,array('1840'))){ ?>	
												<div class="form-group" style="margin-top:-10px">
													<div class="pull-right">
														<div class="col-md-12 ">
															<button class="btn btn-info" onclick="show_riwayat_obat(1)" type="button"><i class="fa fa-list"></i> Riwayat</button>
															<button class="btn btn-danger" onclick="clear_1()" type="button"><i class="fa fa-refresh"></i> Clear</button>
															<button class="btn btn-success" onclick="add_1()" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
														</div>
													</div>
												</div>
												<?}?>
											</div>
											<div class="tab-pane fade fade-left <?=($tab_resep=='2'?'active in':'')?>" id="tab_resep_2" style="min-height: 400px;">
											<!-- RACIKANFORM -->
												<div class="form-group" style="margin-top:-10px" hidden>
													<div class="col-md-12 ">
														<label for="racikan_id">ID RACIKAN</label>
														<input tabindex="2" type="text" readonly class="form-control auto_input" id="racikan_id" value="{racikan_id}" required>
														<input tabindex="2" type="text" readonly class="form-control auto_input" id="status_racikan" value="{status_racikan}" required>
													</div>
												</div>
												<div id="div_new_racikan">
													<div class="form-group">
														<div class="col-md-12 ">
															<button class="btn btn-primary btn-block btn-sm"  onclick="create_racikan()" type="button"><i class="si si-folder"></i> Buat Racikan Baru</button>
														</div>
													</div>
												</div>
												<div id="div_input_racikan">
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label for="nama_racik_2">Nama Racik</label>
															<input tabindex="2" type="text" class="form-control auto_input auto_blur_racikan" id="nama_racik_2" value="{nama_racikan}" required>
														</div>
														
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-4 ">
															<label for="jumlah_racik_2">Jumlah Racik</label>
															<input tabindex="2" type="text" class="form-control auto_input number auto_blur_racikan" id="jumlah_racik_2" value="{jumlah_racikan}" required>
														</div>
														<div class="col-md-8 ">
															<label for="diagnosa">Jenis Racik</label>
															<select tabindex="8" id="jenis_racik_2" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(list_variable_ref(105) as $row){?>
																<option value="<?=$row->id?>" <?=($jenis_racikan==$row->id?'selected':'')?> <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																<?}?>
																
															</select>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-6 ">
															<label for="interval_2">Frekuensi / Interval</label>
															<select tabindex="8" id="interval_2" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
																<option value="<?=$row->id?>" <?=($interval_racikan==$row->id?'selected':'')?> <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																<?}?>
																
															</select>
														</div>
														<div class="col-md-6 ">
															<label for="rute_2">Rute Pemberian</label>
															<select tabindex="8" id="rute_2" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(list_variable_ref(74) as $row){?>
																<option value="<?=$row->id?>"  <?=($rute_racikan==$row->id?'selected':'')?> <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																<?}?>
																
															</select>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-8 ">
															<label for="aturan_tambahan_2">Aturan tambahan</label>
															<input tabindex="2" type="text" class="form-control auto_input auto_blur_racikan" id="aturan_tambahan_2" value="{aturan_tambahan_racikan}" required>
														</div>
														<div class="col-md-4 ">
															<label for="iter_1">ITER</label>
															<?=(opsi_iter($max_iter,$jml_iter,'iter_2','opsi_iter'))?>
														</div>
													</div>
													<hr>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label for="idbarang_2">Nama Obat <span id="st_label_cover_2" style="display:none">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
															<div class="input-group">
																<select id="idbarang_2" class="js-special-select2" style="width:100%"></select>
																<span class="input-group-btn">
																	<button class="btn btn-info" type="button" onclick="show_modal_obat(2)"><i class="fa fa-search"></i></button>
																</span>
															</div>
															<input tabindex="2" type="hidden" readonly  id="idtipe_2" value="" required>
															<input tabindex="2" type="hidden" readonly id="st_cover_2" value="" required>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label class="text-primary">Quantity Dalam satu Racikan</label>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-3 ">
															<label for="jumlah_2">Jumlah</label>
															<input tabindex="2" type="text" class="form-control  number auto_input" id="jumlah_2" value="" required>
															
														</div>
														<div class="col-md-6 ">
															<label for="p1">P1 / P2</label>
															<div class="input-group">
																<input class="form-control number auto_input" type="text" id="p1" value="1" name="p1" placeholder="P1" required="">
																<span class="input-group-addon">/</span>
																<input class="form-control number auto_input" type="text" id="p2" value="1" name="p2" placeholder="P2" required="">
															</div>
														</div>
														<div class="col-md-3 ">
															<label for="dosis_2">Dosis</label>
															<input tabindex="2" type="text" class="form-control  number auto_input" id="dosis_2" value="" required>
															<input tabindex="2" type="hidden" class="form-control  number auto_input" id="dosis_master" value="" required>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<button class="btn btn-success btn-block btn-sm" title="Tambahkan" onclick="add_2()" type="button"><i class="fa fa-level-down"></i> ENTER</button>
														</div>
													</div>
													
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label class="h5 text-primary">DAFTAR OBAT</label>
															<table class="table table-condensed table-bordered" id="tabel_obat_tmp" style="background-color:#fff">
																<thead>
																	<tr>
																		<th class="text-center" style="width: 5%;">NO</th>
																		<th class="text-center" style="width: 30%;">NAMA</th>
																		<th class="text-center" style="width: 10%;">QTY</th>
																		<th class="text-center" style="width: 5%;">P1/P2</th>
																		<th class="text-center" style="width: 10%;">DOSIS</th>
																		<th class="text-center" style="width: 10%;">T-QTY</th>
																		<th class="text-center" style="width: 5%;">AKSI</th>
																	</tr>
																</thead>
																<tbody>
																	
																</tbody>
															</table>
															
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="pull-right">
															<div class="col-md-12 ">
																<button class="btn btn-info" id="btn_hapus_assesmen" onclick="show_riwayat_obat(1)" type="button"><i class="fa fa-list"></i> Riwayat</button>
																<button class="btn btn-danger" onclick="batalkan_racikan()" type="button"><i class="fa fa-trash"></i> Batalkan</button>
																<button class="btn btn-success"  onclick="simpan_racikan_final()" type="button"><i class="fa fa-save"></i> Tambahkan</button>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane fade fade-left <?=($tab_resep=='3'?'active in':'')?>" id="tab_resep_3" style="min-height: 400px;">
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="nama_paket">Nama Paket</label>
														<div class="input-group">
															<input tabindex="2" type="text" class="form-control  auto_input" id="nama_paket" value="" required>
															<span class="input-group-btn">
																<button class="btn btn-info new_paket" type="button" onclick="create_paket()"><i class="fa fa-plus"></i> Create Paket</button>
																<button class="btn btn-danger input_paket" type="button" onclick="batal_paket()"><i class="fa fa-trash"></i> Batalkan</button>
															</span>
														</div>
														<input tabindex="2" type="hidden" readonly class="form-control  auto_input" id="paket_id" value="" required>
														<input tabindex="2" type="hidden" readonly class="form-control  auto_input" id="status_paket" value="" required>
													</div>
												</div>
												<div class="new_paket">
													<table class="table table-condensed table-bordered" id="tabel_paket" style="background-color:#fff">
															<thead>
																<tr>
																	<th class="text-center" style="width: 3%;">NO</th>
																	<th class="text-center" style="width: 80%;">PAKET</th>
																	<th class="text-center" style="width: 15%;">AKSI</th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
												</div>
												<div class="input_paket">
														<div class="form-group">
															<div class="col-md-12 ">
																<label for="idbarang_3">Nama Obat <span id="st_label_cover_3">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
																<div class="input-group">
																	<select id="idbarang_3" class="js-special-select2" style="width:100%"></select>
																	<span class="input-group-btn">
																		<button class="btn btn-info" type="button" onclick="show_modal_obat(3)"><i class="fa fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="jumlah_3">Jumlah</label>
																<input tabindex="2" type="text" class="form-control  number auto_input" id="jumlah_3" value="" required>
																<input tabindex="2" type="hidden" readonly  id="idtipe_3" value="" required>
																<input tabindex="2" type="hidden" readonly id="st_cover_3" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label class="text-primary">Aturan Pakai :</label>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="interval_3">Frekuensi / Interval</label>
																<select tabindex="8" id="interval_3" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																	<option value="">Pilih Opsi</option>
																	<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
																	<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																	<?}?>
																	
																</select>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="dosis_3">Dosis Satu kali minum</label>
																<input tabindex="2" type="text" class="form-control  number auto_input" id="dosis_3" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="rute_3">Rute Pemberian</label>
																<select tabindex="8" id="rute_3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																	<option value="">Pilih Opsi</option>
																	<?foreach(list_variable_ref(74) as $row){?>
																	<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																	<?}?>
																	
																</select>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
																<div class="col-md-12">
																	<label for="aturan_tambahan_1">Aturan tambahan</label>
																	<input tabindex="2" type="text" class="form-control auto_input" id="aturan_tambahan_3" value="" required>
																</div>
															
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="iter_3">ITER</label>
																<?=(opsi_iter($max_iter,$jml_iter,'iter_3','opsi_change'))?>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="pull-right">
																<div class="col-md-12 ">
																	<button class="btn btn-danger" onclick="clear_3()" type="button"><i class="fa fa-refresh"></i> Clear</button>
																	<button class="btn btn-success" onclick="add_3()" type="button"><i class="fa fa-plus"></i> Tambahkan Ke Paket</button>
																</div>
															</div>
														</div>
													</div>
											</div>
											<div class="tab-pane fade fade-left <?=($tab_resep=='4'?'active in':'')?>" id="tab_resep_4" style="min-height: 400px;">
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="nama_template">Nama Template</label>
														<div class="input-group">
															<input tabindex="2" type="text" class="form-control  auto_input" id="nama_template" value="" required>
															<span class="input-group-btn">
																<button class="btn btn-info new_template" type="button" onclick="create_template()"><i class="fa fa-plus"></i> Create Template</button>
																<button class="btn btn-danger input_template" type="button" onclick="batal_template()"><i class="fa fa-trash"></i> Batalkan</button>
															</span>
														</div>
														<input tabindex="2" type="hidden" readonly class="form-control  auto_input" id="template_id" value="" required>
														<input tabindex="2" type="hidden" readonly class="form-control  auto_input" id="status_template" value="" required>
													</div>
												</div>
												<div class="new_template">
													<table class="table table-condensed table-bordered" id="tabel_template" style="background-color:#fff">
															<thead>
																<tr>
																	<th class="text-center" style="width: 3%;">NO</th>
																	<th class="text-center" style="width: 80%;">TEMPLATE</th>
																	<th class="text-center" style="width: 15%;">AKSI</th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
												</div>
												<div class="input_template">
														<div class="form-group">
															<div class="col-md-12 ">
																<label for="idbarang_4">Nama Obat <span id="st_label_cover_4">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
																<div class="input-group">
																	<select id="idbarang_4" class="js-special-select2" style="width:100%"></select>
																	<span class="input-group-btn">
																		<button class="btn btn-info" type="button" onclick="show_modal_obat(3)"><i class="fa fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="jumlah_4">Jumlah</label>
																<input tabindex="2" type="text" class="form-control  number auto_input" id="jumlah_4" value="" required>
																<input tabindex="2" type="hidden" readonly  id="idtipe_4" value="" required>
																<input tabindex="2" type="hidden" readonly id="st_cover_4" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label class="text-primary">Aturan Pakai :</label>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="interval_4">Frekuensi / Interval</label>
																<select tabindex="8" id="interval_4" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																	<option value="">Pilih Opsi</option>
																	<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
																	<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																	<?}?>
																	
																</select>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="dosis_4">Dosis Satu kali minum</label>
																<input tabindex="2" type="text" class="form-control  number auto_input" id="dosis_4" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="rute_4">Rute Pemberian</label>
																<select tabindex="8" id="rute_4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																	<option value="">Pilih Opsi</option>
																	<?foreach(list_variable_ref(74) as $row){?>
																	<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																	<?}?>
																	
																</select>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
																<div class="col-md-12">
																	<label for="aturan_tambahan_1">Aturan tambahan</label>
																	<input tabindex="2" type="text" class="form-control auto_input" id="aturan_tambahan_4" value="" required>
																</div>
															
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="iter_4">ITER</label>
																<?=(opsi_iter($max_iter,$jml_iter,'iter_4','opsi_change'))?>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="pull-right">
																<div class="col-md-12 ">
																	<button class="btn btn-danger" onclick="clear_4()" type="button"><i class="fa fa-refresh"></i> Clear</button>
																	<button class="btn btn-success" onclick="add_4()" type="button"><i class="fa fa-plus"></i> Tambahkan Ke Template</button>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>
									</div>
									
								</div>
								<div class="col-md-9">
									<div class="block">
										<ul class="nav nav-tabs" data-toggle="tabs">
											<li id="div_detail" class="active">
												<a href="#tab_detail" id="nama_tab_detail">ORDER DETAIL</a>
											</li>
											
										</ul>
										<div class="block-content tab-content" style="background-color:#f5f5f5">
											<div class="tab-pane fade fade-left active in" id="tab_detail" style="min-height: 400px">
												<div class="div_tab_4">
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-primary"><strong>OBAT TEMPLATE</strong></label>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div class="table-responsive">
																<table class="table table-bordered" id="tabel_obat_template" style="background-color:#fff">
																	<thead>
																		<tr>
																			<th class="text-center" style="width: 5%;">NO</th>
																			<th class="text-center" style="width: 30%;">NAMA</th>
																			<th class="text-center" style="width: 10%;">DOSIS</th>
																			<th class="text-center" style="width: 5%;">QTY</th>
																			<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
																			<th class="text-center" style="width: 10%;">RUTE PEMBERIAN</th>
																			<th class="text-center" style="width: 10%;">ATURAN TAMBAHAN</th>
																			<th class="text-center" style="width: 10%;">ITER</th>
																			<th class="text-center" style="width: 5%;">AKSI</th>
																		</tr>
																	</thead>
																	<tbody>
																		
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													
												</div>
												<div class="div_tab_3">
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-primary"><strong>OBAT PAKET</strong></label>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div class="table-responsive">
																<table class="table table-bordered" id="tabel_obat_paket" style="background-color:#fff">
																	<thead>
																		<tr>
																			<th class="text-center" style="width: 5%;">NO</th>
																			<th class="text-center" style="width: 30%;">NAMA</th>
																			<th class="text-center" style="width: 10%;">DOSIS</th>
																			<th class="text-center" style="width: 5%;">QTY</th>
																			<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
																			<th class="text-center" style="width: 10%;">RUTE PEMBERIAN</th>
																			<th class="text-center" style="width: 10%;">ATURAN TAMBAHAN</th>
																			<th class="text-center" style="width: 10%;">ITER</th>
																			<th class="text-center" style="width: 5%;">AKSI</th>
																		</tr>
																	</thead>
																	<tbody>
																		
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													
												</div>
												<div class="div_tab_1">
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-primary"><strong>RESEP NON RACIKAN</strong></label>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div class="table-responsive">
															<table class="table table-bordered" id="tabel_obat" style="background-color:#fff">
																<thead>
																	<tr>
																		<th class="text-center" style="width: 5%;">NO</th>
																		<th class="text-center" style="width: 30%;">NAMA</th>
																		<th class="text-center" style="width: 8%;">DOSIS</th>
																		<th class="text-center" style="width: 5%;">QTY</th>
																		<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
																		<th class="text-center" style="width: 8%;">RUTE PEMBERIAN</th>
																		<th class="text-center" style="width: 8%;">ATURAN TAMBAHAN</th>
																		<th class="text-center" style="width: 8%;">ITER</th>
																		<th class="text-center" style="width: 10%;">STATUS</th>
																		<th class="text-center" style="width: 10%;">AKSI</th>
																	</tr>
																</thead>
																<tbody>
																	
																</tbody>
															</table>
															
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-danger"><strong>RESEP RACIKAN</strong></label>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div id="div_tabel_racikan">
																
															</div>
														</div>
													</div>
													<?if ($status_tindakan>2){?>
													<?if ($status_tindakan!=5){?>
													<div class="form-group">
														<div class="col-md-12 text-center">
															<table width="100%">
																<tr>
																	<td class="texxt-center" style="width:33%"><strong>YANG MENYERAHKAN</strong></td>
																	<td class="texxt-center" style="width:33%"><strong></strong></td>
																	<td class="texxt-center" style="width:33%"><strong>YANG MENERIMA</strong></td>
																</tr>
																<tr>
																	<td class="texxt-center" style="width:33%"><img class="" style="width:100px;height:100px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd/<?=5?>" alt="" title=""></td>
																	<td class="texxt-center" style="width:33%"><strong></strong></td>
																	<td class="texxt-center" style="width:33%">
																		<?if ($penerima_ttd==''){?>
																			<?if ($status_tindakan!='5'){?>
																			<button type="button" class="btn btn-success btn-xs btn_ttd text-center" onclick="modal_ttd_penerima()"> <i class="fa fa-paint-brush"></i> Tanda Tangan</button>
																			<?}?>
																			<?}else{?>
																			<div class="img-container fx-img-rotate-r ">
																				<img class="img-responsive" src="{penerima_ttd}" alt="">
																				
																				<div class="img-options">
																					<div class="img-options-content">
																						<div class="btn-group btn-group-sm">
																							<button type="button" <?=($status_tindakan=='4'?'disabled':'')?> class="btn btn-default btn-xs" onclick="modal_ttd_penerima()" ><i class="fa fa-pencil"></i> </button>
																							<button type="button" <?=($status_tindakan=='4'?'disabled':'')?> class="btn btn-default btn-danger  btn-xs" onclick="hapus_ttd_penerima()" ><i class="fa fa-times"></i> </button>
																						</div>
																					</div>
																				</div>
																				
																			</div>
																			<?}?>
																	</td>
																</tr>
																<tr>
																	<td class="texxt-center" style="width:33%"><strong><?=$login_nama_ppa?></strong></td>
																	<td class="texxt-center" style="width:33%"><strong></strong></td>
																	<td class="texxt-center" style="width:33%"></td>
																</tr>
															</table>
														</div>
													</div>
													
													<?}?>
													<?}?>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								
							</div>
							
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_review_trx">
						<div class="row">
							<div class="progress progress-mini">
								<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
							</div>
							<div class="col-md-12">
							<h3 style="margin-bottom: 10px;">NON RACIKAN</h3>
							</div>
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered" id="index_review_trx">
										<thead>
											<tr>
												<th width="15%" class="text-center">Nama Obat</th>
												<th width="5%" class="text-center">Dosis</th>
												<th width="5%" class="text-center">Qty</th>
												<th width="8%" class="text-center">Frekuensi / Interval</th>
												<th width="8%" class="text-center">Rute Pemberian</th>
												<th width="5%" class="text-center">Aturan Tambahan</th> <!--55-->
												<th width="10%" class="text-center">Aturan Minum</th>
												<th width="5%" class="text-center">Qty Ambil</th>
												<th width="8%" class="text-center">Disc (%)</th>
												<th width="8%" class="text-center">Tuslah</th>
												<th width="10%" class="text-center">Expired</th>
												<th width="10%" class="text-center">Jumlah Harga</th>
												<th width="8%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="progress progress-mini">
								<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
							</div>
							<div class="col-md-12">
							<h3 style="margin-bottom: 10px;">RACIKAN</h3>
							</div>
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered" id="index_review_racikan_trx">
										<thead>
											<tr>
												<th width="20%" class="text-center">Nama Racikan</th>
												<th width="8%" class="text-center">Jenis Racikan</th>
												<th width="5%" class="text-center">Jumlah Racikan</th>
												<th width="8%" class="text-center">Frekuensi / Interval</th>
												<th width="8%" class="text-center">Rute Pemberian</th>
												<th width="8%" class="text-center">Aturan Tambahan</th> <!--55-->
												<th width="8%" class="text-center">Aturan Minum</th>
												<th width="5%" class="text-center">Qty Ambil</th>
												<th width="8%" class="text-center">Tuslah</th>
												<th width="10%" class="text-center">Expired</th>
												<th width="10%" class="text-center">Jumlah Harga</th>
												<th width="8%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">ORDER SAYA</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_his_order" placeholder="No Pendaftaran" name="notransaksi_his_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_his_order" name="idpoli_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_his_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_his_order" name="iddokter_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_history_order()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_history_order">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="12%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">Permintaan</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemberi Resep</th>
													<th width="9%">Diagnosa</th>
													<th width="10%">Tujuan Farmasi / Prioritas</th>
													<th width="10%">Status Pemeriksaan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>					
				</div>
				<div class="block">
					<div class="col-md-12 ">
						<div class="pull-right push-10-r">
							<?if($status_tindakan!='5'){?>
							<button class="btn btn-primary" id="btn_resep" onclick="cetak_resep()" type="button"><i class="si si-doc"></i> RESEP </button>
							<button class="btn btn-warning" id="btn_copy" onclick="copy_resep()" type="button"><i class="fa fa-copy"></i> COPY RESEP </button>
							<?}?>
							<?if($status_tindakan=='1'){?>
								<button class="btn btn-danger btn_ambil_resep" id="btn_tidak" onclick="set_tidak_diambil()" type="button"><i class="fa fa-times"></i> RESEP TIDAK DIAMBIL </button>
								<button class="btn btn-info btn_ambil_resep" id="btn_ambil" onclick="set_diambil()" type="button"><i class="si si-check"></i> RESEP DIAMBIL </button>
							<?}?>
							<?if($status_tindakan<'4'){?>
							<a href="{base_url}tfarmasi_tindakan" class="btn btn-success" id="btn_update" type="button"><i class="fa fa-save"></i> UPDATE & KELUAR </a>
							<?}?>
							<?if($status_tindakan=='5'){?>
								<button class="btn btn-danger" id="btn_batal_tidak_diambil" onclick="update_batal_tidak_diambil()" type="button"><i class="fa fa-check"></i> Kembalikan Status Review </button>
							<?}?>
							<?if($status_tindakan=='4'){?>
							<a href="{base_url}tfarmasi_tindakan" class="btn btn-default" id="btn_update" type="button"><i class="fa fa-mail-forward"></i> KELUAR </a>
							<?}?>
							<?if($status_tindakan=='5'){?>
							<a href="{base_url}tfarmasi_tindakan" class="btn btn-default" id="btn_update" type="button"><i class="fa fa-mail-forward"></i> KELUAR </a>
							<?}?>
							
							
							<?if($status_tindakan=='2' && $st_validasi=='0'){?>
							<button class="btn btn-danger" id="btn_batal_transaksi" onclick="batalkan_transaksi_resep()" type="button"><i class="fa fa-times"></i> BATALKAN TRANSAKSI OBAT </button>
							<?}?>
							<?if($status_tindakan=='2' && $st_validasi=='0'){?>
							<button class="btn btn-primary" id="btn_validasi" onclick="update_validasi()" type="button"><i class="fa fa-send"></i> VALIDASI </button>
							<?}?>
							<?if($status_tindakan==3 && $st_validasi=='1'){?>
							<button class="btn btn-danger" id="btn_serahkan" onclick="batalkan_validasi()" type="button"><i class="fa fa-times"></i> BATALKAN VALIDASI </button>
							<?}?>
							<?if($status_tindakan==3 && $st_validasi=='1'){?>
							<button class="btn btn-info" id="btn_serahkan" <?=($penerima_ttd==''?'disabled':'')?> onclick="modal_serahkan()" type="button"><i class="fa fa-handshake-o"></i> SERAHKAN </button>
							<?}?>
							
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal in" id="modal_ttd_penerima" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">TANDA TANGAN PENERIMA</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_penerima" ></div>
										<textarea id="signed_ttd_penerima" name="signed_ttd_penerima" style="display: none"><?=$penerima_ttd?></textarea>
									</div>
								</div>
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_penerima()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_penerima()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_serahkan" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENYERAHAN OBAT</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div class="col-md-4 ">
										<label for="example-input-normal">Tanggal Penyerahan</label>
											<div class="input-group date">
												<input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggalpenyerahan" placeholder="HH/BB/TTTT" name="tanggalpenyerahan" value="<?= $tanggalpenyerahan ?>" required>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										<div class="col-md-4 ">
											<label for="example-input-normal"><i class="text-muted">Waktu Penyerahan</i></label>
											<div class="input-group">
												<input tabindex="3" type="text" class="time-datepicker form-control" id="waktupenyerahan" value="<?= $waktupenyerahan ?>" required>
												<span class="input-group-addon"><i class="si si-clock"></i></span>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="update_serahkan()" ><i class="fa fa-handshake-o"></i> SERAHKAN</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_informasi_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Informasi</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom:10px" >
								<div class="col-md-6">
									<label for="idkategori">Nama</label>
									<input disabled type="text" class="form-control" id="info_nama" value="" required="" aria-required="true">
									
								</div>
								<div class="col-md-6">
									<label for="idkategori">Nama Generik</label>
									<select disabled tabindex="8" id="info_nama_generik"  name="nama_generik" class="js-select2 form-control js_92" style="width: 100%;">
									<option value="" >Pilih Opsi</option>	
										<?foreach(list_variable_ref(92) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
									<label for="idkategori">Dosis</label>
									<input disabled type="text" class="form-control number"   id="info_dosis" placeholder="Dosis" value="{dosis}" required="" aria-required="true">
								</div>
								<div class="col-md-3">
									<label for="idkategori">Merk</label>
										<select disabled tabindex="8" id="info_merk" class="js-select2 form-control js_93" style="width: 100%;">
											<option value="" >Pilih Opsi</option>
											<?foreach(list_variable_ref(93) as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>
								</div>
								<div class="col-md-2">
									<label for="idkategori">High Alert</label>
										<select disabled tabindex="8" id="info_high_alert" class="js-select2 form-control " style="width: 100%;">
											<option value="">Pilih Opsi</option>
											<option value="1" >YA</option>
											<option value="0" >TIDAK</option>
										</select>
										
								</div>
								<div class="col-md-4">
									<label for="idkategori">Nama Industri</label>
									<select disabled tabindex="8" id="info_nama_industri" class="js-select2 form-control js_97" style="width: 100%;">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(97) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-6">
									<label class="control-label" for="info_komposisi">Komposisi</label>
									<textarea disabled class="form-control" id="info_komposisi" placeholder="Komposisi"  required="" aria-required="true"></textarea>
								</div>
								<div class="col-md-6">
									<label class="control-label" for="alamat">Indikasi</label>
									<textarea disabled class="form-control" id="info_indikasi" placeholder="Indikasi" required="" aria-required="true"></textarea>
								</div>
								
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
									<label for="idkategori">Bentuk Sediaan</label>
									<select disabled tabindex="8"  id="info_bentuk_sediaan" class="js-select2 form-control js_94" style="width: 100%;">
										<option value="" >Pilih Opsi</option>
										<?foreach(list_variable_ref(94) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>
										<?}?>
									</select>
										
								</div>
								<div class="col-md-3" >
									<label for="idkategori">Jenis Penggunaan</label>
									<select disabled tabindex="8"  id="info_jenis_penggunaan" class="js-select2 form-control js_95" style="width: 100%;">
										<option value="" >TIDAK TERSEDIA</option>
										<?foreach(list_variable_ref(95) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>
										<?}?>
										</select>
										
									
								</div>
								<div class="col-md-3">
								
									<label for="idkategori">Golongan Obat</label>
									<select disabled tabindex="8" id="info_gol_obat"class="js-select2 form-control js_98" style="width: 100%;">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(98) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-3">
									<label for="idkategori">Satuan Besar</label>
									<input disabled type="text" class="form-control" id="info_satuan" value="" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
							</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_informasi_edit_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Informasi Perubahan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label for="idkategori" class="text-primary">STATUS PERUBAHAN</label>
									<input disabled type="text" class="form-control" id="info_status_perubahan" value="" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label class="text-primary">TERTULIS</label>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<table class="table table-bordered" id="tabel_awal">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width:50%">NAMA OBAT</th>
                                                <th class="text-center" style="width:10%">DOSIS</th>
                                                <th class="text-center" style="width:10%">JUMLAH</th>
                                                <th class="text-center" style="width:10%">FREKUENSI / INTERVAL</th>
                                                <th class="text-center" style="width:10%">RUTE PEMBERIAN</th>
                                                <th class="text-center" style="width:10%">ATURAN TAMBAHAN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label class="text-primary">MENJADI</label>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:10px" >
								<div class="col-md-12">
									<table class="table table-bordered" id="tabel_menjadi">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width:50%">NAMA OBAT</th>
                                                <th class="text-center" style="width:10%">DOSIS</th>
                                                <th class="text-center" style="width:10%">JUMLAH</th>
                                                <th class="text-center" style="width:10%">FREKUENSI / INTERVAL</th>
                                                <th class="text-center" style="width:10%">RUTE PEMBERIAN</th>
                                                <th class="text-center" style="width:10%">ATURAN TAMBAHAN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label for="idkategori" class="text-primary">ALASAN PERUBAHAN</label>
									<input disabled type="text" class="form-control" id="alasan_perubahan_info" value="" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:10px" >
								<div class="col-md-12">
									<table class="table" id="tabel_ttd">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="33%">PETUGAS FARMASI</th>
                                                <th class="text-center" style="33%"></th>
                                                <th class="text-center" style="34%">DISETUJUI DOKTER</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <tr>
                                                <td class="text-center" style="33%">&nbsp;</td>
                                                <td class="text-center" style="33%">&nbsp;</td>
                                                <td class="text-center" style="34%">&nbsp;</td>
                                            </tr>
											<tr>
                                                <td class="text-center" style="33%">NAMD PETUGAS</td>
                                                <td class="text-center" style="33%"></td>
                                                <td class="text-center" style="34%">NAMA DOKTER</td>
                                            </tr>
                                        </tbody>
                                    </table>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
							</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_informasi_edit_obat_racikan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Informasi Perubahan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label for="idkategori" class="text-primary">STATUS PERUBAHAN</label>
									<input disabled type="text" class="form-control" id="info_status_perubahan_racikan" value="" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label class="text-primary">TERTULIS</label>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<table class="table table-bordered" id="tabel_awal_racikan">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width:50%">NAMA OBAT</th>
                                                <th class="text-center" style="width:10%">DOSIS</th>
                                                <th class="text-center" style="width:10%">JUMLAH</th>
                                                <th class="text-center" style="width:10%">P1</th>
                                                <th class="text-center" style="width:10%">P2</th>
                                                <th class="text-center" style="width:10%">T-QTY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label class="text-primary">MENJADI</label>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:10px" >
								<div class="col-md-12">
									<table class="table table-bordered" id="tabel_menjadi_racikan">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width:50%">NAMA OBAT</th>
                                                <th class="text-center" style="width:10%">DOSIS</th>
                                                <th class="text-center" style="width:10%">JUMLAH</th>
                                                <th class="text-center" style="width:10%">P1</th>
                                                <th class="text-center" style="width:10%">P2</th>
                                                <th class="text-center" style="width:10%">T-QTY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px" >
								<div class="col-md-12">
									<label for="idkategori" class="text-primary">ALASAN PERUBAHAN</label>
									<input disabled type="text" class="form-control" id="alasan_perubahan_info_racikan" value="" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:10px" >
								<div class="col-md-12">
									<table class="table" id="tabel_ttd_racikan">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="33%">PETUGAS FARMASI</th>
                                                <th class="text-center" style="33%"></th>
                                                <th class="text-center" style="34%">DISETUJUI DOKTER</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <tr>
                                                <td class="text-center" style="33%">&nbsp;</td>
                                                <td class="text-center" style="33%">&nbsp;</td>
                                                <td class="text-center" style="34%">&nbsp;</td>
                                            </tr>
											<tr>
                                                <td class="text-center" style="33%">NAMD PETUGAS</td>
                                                <td class="text-center" style="33%"></td>
                                                <td class="text-center" style="34%">NAMA DOKTER</td>
                                            </tr>
                                        </tbody>
                                    </table>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
							</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idtipe_filter">Tipe Barang</label>
								<div class="col-md-8">
									<select id="idtipe_filter" class="form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(get_all('mdata_tipebarang') as $r){?>
										<option value="<?=$r->id?>" <?=(3==$r->id?'selected':'')?>><?=$r->nama_tipe?></option>
										<?}?>
									</select>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="merk_filter">Jenis Penggunaan</label>
								<div class="col-md-8">
									<select tabindex="3" id="jenis_penggunaan_filter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach(list_variable_ref(95) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="merk_filter">Merk</label>
								<div class="col-md-8">
									<select tabindex="3" id="merk_filter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach(list_variable_ref(93) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							
							
							
						</div>
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="bentuk_sediaan_filter">Merk</label>
								<div class="col-md-9">
									<select tabindex="3" id="bentuk_sediaan_filter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach(list_variable_ref(94) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-9">
									<select id="idkat_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-3 control-label" for="tipeid">Nama Barang</label>
								<div class="col-md-9">
									<input class="form-control" type="text" id="nama_barang_filter" name="nama_barang_filter" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang" onclick="loadBarang()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-bordered" id="filter_tabel_obat" style="background-color:#fff">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Merk</th>
								<th>Bentuk Sediaan</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>Harga</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_cari_resep" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">DATA E-RESEP</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="filter_tanggal_permintaan_1">Tanggal Permintaan</label>
								<div class="col-md-8">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="js-datepicker form-control " type="text" id="filter_tanggal_permintaan_1" data-date-format="dd-mm-yyyy"  value="" placeholder="from">
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="js-datepicker form-control " type="text" id="filter_tanggal_permintaan_2"  data-date-format="dd-mm-yyyy"  value="" placeholder="to">
								</div>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="filter_tanggal_pendaftaran_1">Tanggal Perndaftaran</label>
								<div class="col-md-8">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="js-datepicker form-control " type="text" id="filter_tanggal_pendaftaran_1" data-date-format="dd-mm-yyyy"  value="" placeholder="from">
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="js-datepicker form-control " type="text" id="filter_tanggal_pendaftaran_2"  data-date-format="dd-mm-yyyy"  value="" placeholder="to">
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="filter_nopendaftaran">No Registrasi</label>
								<div class="col-md-8">
									<input class="form-control" type="text" id="filter_nopendaftaran" value="">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="filter_tujuan_poli">Tujuan Poliklinik</label>
								<div class="col-md-9">
									<select tabindex="3" id="filter_tujuan_poli"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" selected>Opsi</option>
										<?foreach(get_all('mpoliklinik',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="filter_tujuan_dokter">Tujuan Dokter</label>
								<div class="col-md-9">
									<select tabindex="3" id="filter_tujuan_dokter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" selected>Opsi</option>
										<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-3 control-label" for="filter_tujuan_farmasi">Tujuan Farmasi</label>
								<div class="col-md-9">
									<select tabindex="3" id="filter_tujuan_farmasi"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" selected>Opsi</option>
										<?foreach(get_all('mtujuan_farmasi',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama_tujuan?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_resep" onclick="list_cari_rersep()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-bordered" id="index_cari_resep" style="background-color:#fff">
						<thead>
							<tr>
								<th>Action</th>
								<th>No Registrasi</th>
								<th>No Permintaan</th>
								<th>Tujuan</th>
								<th>Dokter Peminta</th>
								<th>Tujuan Farmasi</th>
								<th>Status Pemeriksaan</th>
								<th>Dibuat Oleh</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_upload_resep" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title"><i class="fa fa-file-image-o"></i> Upload Resep</h3>
				</div>
				<div class="block-content">
					<div class="col-md-12" style="margin-top: 10px;">
						
						<div class="block">
							<form action="{base_url}Tpoliklinik_resep/upload_resep" enctype="multipart/form-data" class="dropzone" id="image-upload">
								<input type="hidden" class="form-control"  id="assesmen_resep" name="assesmen_resep" value="{assesmen_id}" readonly>
								<div>
								  <h5>Upload Resep</h5>
								</div>
							</form>
						</div>
						
					</div> 
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="waktu_modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">WAKTU Minum Obat</h3>
				</div>
				<input type="hidden" value="" id="tipe_tabel">
				<div class="block-content">
					<table id="manage-tabel-Waktu" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 40%;">Waktu Minum</th>
								<th style="width: 30%;">Jam Minum</th>
								<th style="width: 30%;">Actions</th>
							</tr>
							<tr>
								<td>
									<select id="waktu_minum" class="form-control  input-sm" tabindex="3">
										<option value="" selected>-Pilih-</option>
										<option value="PAGI">PAGI</option>
										<option value="SIANG">SIANG</option>
										<option value="SORE">SORE</option>
										<option value="MALAM">MALAM</option>
									</select>
								</td>
								<td>
									<input type="text" name="jam_minum" id="jam_minum" value="00:00" class="form-control" placeholder="" value="" required="" aria-required="true">
								</td>

								<td class="text-center">
									<input type="hidden" id="id_detail_waktu">
									<button type="button" class="btn btn-sm  btn-primary btn_minum_obat" tabindex="20" id="addwaktu" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<button type='button' class='btn btn-sm btn-danger btn_minum_obat' tabindex="21" id="cancelwaktu" title="Hapus"><i class='glyphicon glyphicon-remove'></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary btn_minum_obat" disabled id="btn_ok_waktu" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in" id="modal_riwayat_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-bordered" id="tabel_riwayat_obat" style="background-color:#fff">
						<thead>
							<tr>
								<th class="text-center" style="width: 5%;">NO</th>
								<th class="text-center" style="width: 30%;">Waktu Permintaan</th>
								<th class="text-center" style="width: 30%;">Dokter</th>
								<th class="text-center" style="width: 30%;">Nama Obat</th>
								<th class="text-center" style="width: 10%;">Dosis</th>
								<th class="text-center" style="width: 5%;">Qty</th>
								<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
								<th class="text-center" style="width: 10%;">RUTE PEMBERIAN</th>
								<th class="text-center" style="width: 10%;">ATURAN TAMBAHAN</th>
								<th class="text-center" style="width: 5%;">AKSI</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_edit_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Edit</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12 ">
									<label for="idbarang_1_edit">Nama Obat <span id="st_label_cover_edit">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
									<div class="input-group">
										<select tabindex="18" id="idbarang_1_edit" class="js-special-select2" style="width:100%"></select>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" onclick="show_modal_obat(1)"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label for="jumlah_1_edit">Jumlah</label>
									<input tabindex="18" type="text" class="form-control  number auto_input" id="jumlah_1_edit" value="" required>
									<input  type="hidden" readonly  id="idtipe_1_edit" value="" >
									<input  type="hidden" readonly id="st_cover_1_edit" value="" >
									<input  type="hidden" readonly id="trx_id_obat" value="" >
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label class="text-primary">Aturan Pakai :</label>
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label for="interval_1_edit">Frekuensi / Interval</label>
									<select tabindex="19" id="interval_1_edit" class="form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label for="dosis_1_edit">Dosis Satu kali minum</label>
									<input tabindex="20" type="text" class="form-control  number auto_input" id="dosis_1_edit" value="" required>
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label for="rute_1_edit">Rute Pemberian</label>
									<select tabindex="21" id="rute_1_edit" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(74) as $row){?>
										<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
									<div class="col-md-12">
										<label for="aturan_tambahan_1_edit">Aturan tambahan</label>
										<input tabindex="22" type="text" class="form-control auto_input" id="aturan_tambahan_1_edit" value="" required>
									</div>
								
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label for="iter_1_edit">ITER</label>
									<?=(opsi_iter($max_iter,$jml_iter,'iter_1_edit','opsi_change_edit'))?>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-success" onclick="add_1_edit()" type="button"><i class="fa fa-save"></i> Simpan Perubahan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_edit_obat_racikan" role="dialog" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Edit</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label for="idbarang_edit_racikan">Nama Obat <span id="st_label_cover_edit_racikan" style="display:none">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
									<div class="input-group">
										<select id="idbarang_edit_racikan" class="js-special-select2" style="width:100%"></select>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" onclick="show_modal_obat(2)"><i class="fa fa-search"></i></button>
										</span>
									</div>
									<input tabindex="2" type="hidden" readonly  id="idtipe_edit_racikan" value="" required>
									<input tabindex="2" type="hidden" readonly id="trx_id_racikan_id" value="" required>
									<input tabindex="2" type="hidden" readonly id="st_cover_edit_racikan" value="" required>
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-12 ">
									<label class="text-primary">Quantity Dalam satu Racikan</label>
								</div>
							</div>
							<div class="form-group" style="margin-top:-10px">
								<div class="col-md-3 ">
									<label for="jumlah_edit_racikan">Jumlah</label>
									<input tabindex="2" type="text" class="form-control  number auto_input" id="jumlah_edit_racikan" value="" required>
									
								</div>
								<div class="col-md-6 ">
									<label for="p1">P1 / P2</label>
									<div class="input-group">
										<input class="form-control number auto_input" type="text" id="p1_edit_racikan" value="1" placeholder="P1" required="">
										<span class="input-group-addon">/</span>
										<input class="form-control number auto_input" type="text" id="p2_edit_racikan" value="1" placeholder="P2" required="">
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="dosis_edit_racikan">Dosis</label>
									<input tabindex="2" type="text" class="form-control  number auto_input" id="dosis_edit_racikan" value="" required>
									<input tabindex="2" type="text" class="form-control  number auto_input" id="dosis_master_edit_racikan" value="" required>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-success" onclick="add_edit_racikan()" type="button"><i class="fa fa-save"></i> Simpan Perubahan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_racikan_review" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Racikan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12 ">
									<div id="div_tabel_racikan_review">
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-danger" type="button" data-dismiss="modal">Tutup</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_tidak_diambil" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN RESEP TIDAK DIAMBIL </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control" type="text" value="" id="alasan_tidak_diambil" placeholder="Alasan" required>
									<label for="keterangan_edit">Alasan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_tidak_diambil()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var class_obat;
var tab=$("#tab").val();;
var sig_ttd_penerima = $('#sig_ttd_penerima').signature({syncField: '#signed_ttd_penerima', syncFormat: 'PNG'});
var myDropzone ;
var array_waktu_minum_obat;
function set_tab($tab){
	console.log($tab);
	tab=$tab;
	if (tab=='1'){
		disabel_edit();
		hide_all_tab();
		$(".div_input_tab_1").show();
		$("#tanggaltelaah_resep").removeAttr('disabled');
		$("#waktutelaah_resep").removeAttr('disabled');
		list_index_obat();
		// load_review_trx();
		// list_index_obat_review_trx();
		
	}
	// if (tab=='4'){
		// disabel_edit();
		// hide_all_tab();
		// $(".div_input_tab_4").show();
		
		// list_index_obat();
		
	// }
	// if (tab=='5'){
		// disabel_edit();
		// hide_all_tab();
		// $(".div_input_tab_1").show();
		// $(".div_input_tab_4").show();
		
		// list_index_obat();
		
	// }
	// if (tab=='2'){
		
		// hide_all_tab();
		// $(".div_input_tab_3").show();
		// list_index_obat();
	// }
	// if (tab=='3'){
		// hide_all_tab();
		// $(".div_input_tab_3").show();
		// load_review_trx();
		// $(".btn_ambil_resep").show();
	// }
	// if ($("#st_direct").val()=='1'){
		// if ($("#status_tindakan").val()=='0'){
			
			// Dropzone.autoDiscover = false;
			// myDropzone = new Dropzone(".dropzone", { 
			   // autoProcessQueue: true,
			   // maxFilesize: 30,
			// });
			// myDropzone.on("complete", function(file) {
			  // $("#cover-spin").show();
			  // location.reload();
			// });
		// }
		
		// $("#div_input_normal").hide();
		// $("#btn_resep").hide();
		// $("#btn_copy").hide();
		// if ($("#assesmen_id_resep").val()){
			// $("#div_input_normal").show();
			// $("#btn_resep").show();
			// $("#btn_copy").show();
			// console.log('Buka Normal');
		// }
		// if ($("#st_manual_resep").val()=='1'){
			// $("#div_input_normal").show();
			// console.log('Buka Normal2');
			// $("#btn_resep").show();
			// $("#btn_copy").show();
		// }
	// }
}
function upload_manual(){
	$("#modal_upload_resep").modal('show');
}
function hide_all_tab(){
	$("#tanggaltelaah_resep").prop('disabled',true);
	$("#waktutelaah_resep").prop('disabled',true);
	$(".div_input_tab_1").hide();
	$(".div_input_tab_3").hide();
	$(".div_input_tab_4").hide();
	$(".btn_ambil_resep").hide();
}
$(document).ready(function() {
	// $("#modal_informasi_edit_obat").modal("show");
	// alert(tab);
	set_tab(tab);
	$(".btn_close_left").click(); 
	clear_1();
	$(".div_tab_3").hide();
	$(".div_tab_4").hide();
	disabel_edit();
	set_input_racikan();
	$(".number").number(true,0,'.',',');
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		// list_index_obat();
		list_index_racikan_tampung();
		list_index_obat_racikan();
	}
	load_awal_assesmen=false;
	$('.js-special-select2').select2({
		minimumInputLength: 2,
		ajax: {
            url: '{site_url}Tpoliklinik_resep/get_obat/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
                tujuan_farmasi_id: $("#tujuan_farmasi_id").val(),
                idtipe_poli: $("#idtipe_poli").val(),
                idkelompokpasien: $("#idkelompokpasien").val(),
                idpoliklinik: $("#idpoliklinik").val(),
                idrekanan: $("#idrekanan").val(),
				
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: '['+item.kode+']'+' '+item.nama,
                            id: item.idbarang,
							idtipe : item.idtipe,
							satuan : item.nama_satuan,
							stok : item.stok,
							harga : (item.harga),
							bentuk_sediaan : item.bentuk_sediaan,
							cover : item.cover,
                        }
                    })
                };
            }
        },
		templateResult: formatOption,
		escapeMarkup: function (markup) {
			return markup;
		}
	});
	
});
function simpan_ttd_penerima(){
	let assesmen_id=$("#assesmen_id").val();
	let penerima_ttd=$("#signed_ttd_penerima").val();
	// ttd_view_petugas
	// $("#ttd_view_petugas").attr("src", ttd_petugas);
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpoliklinik_resep/simpan_ttd_penerima/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				penerima_ttd:penerima_ttd,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Tanda Tangan'});
			  // $("#cover-spin").hide();
				location.reload();
			}
		});
}
function clear_ttd_penerima(){
	sig_ttd_penerima.signature('clear');
	$("#signed_ttd_penerima").val('');
}
function hapus_ttd_penerima(){
	$("#signed_ttd_penerima").val('');
	simpan_ttd_penerima();
}
function modal_ttd_penerima(){
	$("#modal_ttd_penerima").modal('show');
}
function show_detail_racikan(id){
	$("#modal_racikan_review").modal('show');
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/show_detail_racikan/',
		dataType: "json",
		type: 'POST',
		data: {
			id:id,
			assesmen_id:assesmen_id,
			max_iter:max_iter,
			
		},
		success: function(data) {
			$("#div_tabel_racikan_review").html(data);
			$(".tabel_racikan_opsi").select2();
			$(".number").number(true,0,'.',',');
			$("#cover-spin").hide();
		}
	});

}
function set_input_racikan(){
	if ($("#racikan_id").val()==''){
		$("#div_new_racikan").show();
		$("#div_input_racikan").hide();
	}else{
		$("#div_new_racikan").hide();
		$("#div_input_racikan").show();
	}
}
function set_form_tab($form){
	$(".div_tab_4").hide();
	$(".div_tab_3").hide();
	$(".div_tab_1").hide();
	if ($form=='1'){
		$("#nama_tab_detail").html('ORDER DETAIL');
		set_cover();
		$(".div_tab_1").show();
	}else if ($form=='2'){
		$("#nama_tab_detail").html('PAKET DETAIL');
		set_cover_2();
		$(".div_tab_1").show();
	}else if ($form=='3'){
		$("#nama_tab_detail").html('PAKET DETAIL');
		get_info_paket();
		set_cover_3();
		$(".div_tab_3").show();
	}else if ($form=='4'){
		$("#nama_tab_detail").html('TEMPLATE DETAIL');
		get_info_template();
		set_cover_4();
		$(".div_tab_4").show();
	}
	
}

function gete_head_racikan(){
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_head_racikan/',
		dataType: "json",
		type: 'POST',
		data: {
			racikan_id:racikan_id,
			
		},
		success: function(data) {
			
		}
	});
}
$('#tabel_obat tbody').on('click', '.cls_racikan_update', function() {
	$(this).select();
	$(this).focus();
});
$('#index_review_trx tbody').on('click', '.input_review', function() {
	$(this).select();
	$(this).focus();
});
$('#index_review_racikan_trx tbody').on('click', '.input_review', function() {
	$(this).select();
	$(this).focus();
});


$('#tabel_obat tbody').on('blur', '.cls_racikan_update', function() {
	update_record_obat($(this));
	
});

$('#tabel_obat tbody').on('change', '.opsi_change_nr', function() {
	update_record_obat($(this));
	
});
$('#tabel_obat_paket tbody').on('click', '.cls_racikan_update', function() {
	$(this).select();
	$(this).focus();
});
$('#tabel_obat_paket tbody').on('blur', '.cls_racikan_update', function() {
	update_record_obat_paket($(this));
	
});
$('#tabel_obat_paket tbody').on('change', '.opsi_change_paket', function() {
	update_record_obat_paket($(this));
	
});
$('#tabel_telaah tbody').on('change', '.jawaban_resep', function() {
	let tabel=$(this);
	let id=tabel.closest('tr').find(".telaah_id").val();
	let jawaban_resep=tabel.closest('tr').find(".jawaban_resep").val();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_jawaban_resep', 
		dataType: "JSON",
		method: "POST",
		data : {
				id : id,
				jawaban_resep : jawaban_resep,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Telaah'});
				}
		}
	});
});
$('#tabel_telaah_obat tbody').on('change', '.jawaban_resep', function() {
	let tabel=$(this);
	let id=tabel.closest('tr').find(".telaah_id").val();
	let jawaban_resep=tabel.closest('tr').find(".jawaban_resep").val();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_jawaban_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				id : id,
				jawaban_obat : jawaban_resep,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Telaah'});
				}
		}
	});
});
function update_record_obat(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let dosis=tabel.closest('tr').find(".dosis_nr").val();
	let jumlah=tabel.closest('tr').find(".jumlah_nr").val();
	let aturan_tambahan=tabel.closest('tr').find(".aturan_tambahan_nr").val();
	let interval=tabel.closest('tr').find(".interval").val();
	let iter=tabel.closest('tr').find(".iter").val();
	let rute=tabel.closest('tr').find(".rute").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				dosis : dosis,
				jumlah : jumlah,
				aturan_tambahan : aturan_tambahan,
				interval : interval,
				iter : iter,
				rute : rute,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
	console.log(iter);
}
$('#index_review_trx tbody').on('blur', '.input_review', function() {
	
	hitung_review_obat($(this));
	update_record_review_obat($(this));
});
$('#index_review_racikan_trx tbody').on('blur', '.input_review', function() {
	
	hitung_review_obat_racikan($(this));
	update_record_review_obat_racikan($(this));
});
function hitung_review_obat(tabel){
	
	let kuantitas=tabel.closest('tr').find(".kuantitas").val();
	let jumlah=tabel.closest('tr').find(".jumlah").val();
	let harga_jual=tabel.closest('tr').find(".harga_jual").val();
	let diskon_rp=tabel.closest('tr').find(".diskon_rp").val();
	let tuslah=tabel.closest('tr').find(".tuslah").val();
	console.log('DISKON RP' + (diskon_rp) );
	console.log((parseFloat(harga_jual)*parseFloat(kuantitas)) +' ' +  (parseFloat(diskon_rp) * parseFloat(kuantitas)));
	let totalharga=(parseFloat(harga_jual)*parseFloat(kuantitas)) - (parseFloat(diskon_rp) * parseFloat(kuantitas)) + parseFloat(tuslah);
	tabel.closest('tr').find(".totalharga").val(totalharga);
	
	let gt=0;
	let total_sisa_obat=0;
	$('#index_review_trx tbody tr').each(function() {
		if ($(this).closest('tr').find(".totalharga").val()){
		gt=parseFloat(gt) + parseFloat($(this).closest('tr').find(".totalharga").val());
		total_sisa_obat=parseFloat(total_sisa_obat) + parseFloat($(this).closest('tr').find(".sisa_obat").val());
			console.log(parseFloat(tabel.closest('tr').find(".sisa_obat").val()));
		}
	});
	let totalharga_obat_racikan=parseFloat($("#totalharga_obat_racikan").val())
	$("#total_sisa_obat").val((total_sisa_obat));
	$("#totalharga_obat").val(formatNumber(gt));
	$("#totalharga_all").val(formatNumber(gt+totalharga_obat_racikan));
}
function hitung_review_obat_racikan(tabel){
	let sisa_obat=tabel.closest('tr').find(".sisa_obat").val();
	let kuantitas=tabel.closest('tr').find(".kuantitas").val();
	let jumlah=tabel.closest('tr').find(".jumlah").val();
	let harga_jual=tabel.closest('tr').find(".harga").val();
	let diskon_rp=0;
	let tuslah=tabel.closest('tr').find(".tuslah").val();
	console.log('DISKON RP' + (diskon_rp) );
	console.log((parseFloat(harga_jual)*parseFloat(kuantitas)) +' ' +  (parseFloat(diskon_rp) * parseFloat(kuantitas)));
	let totalharga=(parseFloat(harga_jual)*parseFloat(kuantitas)) - (parseFloat(diskon_rp) * parseFloat(kuantitas)) + parseFloat(tuslah);
	tabel.closest('tr').find(".totalharga").val(totalharga);
	
	let gt=0;
	let total_sisa_obat=0;
	$('#index_review_racikan_trx tbody tr').each(function() {
		if ($(this).closest('tr').find(".totalharga").val()){
		gt=parseFloat(gt) + parseFloat($(this).closest('tr').find(".totalharga").val());
		console.log(gt);
		total_sisa_obat=parseFloat(total_sisa_obat) + parseFloat($(this).closest('tr').find(".sisa_obat").val());	
		}
	});
	let totalharga_obat=parseFloat($("#totalharga_obat").val())
	$("#total_sisa_racikan").val((total_sisa_obat));
	$("#totalharga_obat_racikan").val(formatNumber(gt));
	$("#totalharga_all").val(formatNumber(gt+totalharga_obat));
	
}
$('#index_review_trx tbody').on('keyup', '.kuantitas', function() {
	let kuantitas=$(this).closest('tr').find(".kuantitas").val();
	let jumlah=$(this).closest('tr').find(".jumlah").val();
	let sisa_obat=0;;
	
	if (parseFloat(kuantitas)>parseFloat(jumlah)){
		$(this).val(jumlah);
	}
	if (parseFloat($(this).val())==parseFloat(jumlah)){
		$(this).closest('tr').find(".kuantitas").removeClass('text-red');
		// $(this).closest('tr').find(".kuantitas").addClass
	}else{
		$(this).closest('tr').find(".kuantitas").addClass('text-red');
	}
	sisa_obat=parseFloat(jumlah)-parseFloat($(this).closest('tr').find(".kuantitas").val());
	console.log($(this).closest('tr').find(".kuantitas").val());
	$(this).closest('tr').find(".sisa_obat").val(sisa_obat);
	hitung_review_obat($(this))
	
});
$('#index_review_trx tbody').on('change', '.opsi_review', function() {
	
	update_record_review_obat($(this));
	
});
$('#index_review_trx tbody').on('keyup', '.diskon', function() {
	let harga_jual=$(this).closest('tr').find(".harga_jual").val();
	let diskon=$(this).closest('tr').find(".diskon").val();
	if (parseFloat(diskon)>100){
		$(this).val(100);
		diskon=100;
	}
	let diskon_rp=roundNearest100_fix(parseFloat(harga_jual) * parseFloat(diskon)/100);
	
	$(this).closest('tr').find(".diskon_rp").val((diskon_rp));
	hitung_review_obat($(this))
	
});

$('#index_review_racikan_trx tbody').on('keyup', '.kuantitas', function() {
	let kuantitas=$(this).closest('tr').find(".kuantitas").val();
	let jumlah=$(this).closest('tr').find(".jumlah").val();
	
	if (parseFloat(kuantitas)>parseFloat(jumlah)){
		$(this).val(jumlah);
	}
	if (parseFloat($(this).val())==parseFloat(jumlah)){
		$(this).closest('tr').find(".kuantitas").removeClass('text-red');
		// $(this).closest('tr').find(".kuantitas").addClass
	}else{
		$(this).closest('tr').find(".kuantitas").addClass('text-red');
	}
	sisa_obat=parseFloat(jumlah)-parseFloat($(this).closest('tr').find(".kuantitas").val());
	$(this).closest('tr').find(".sisa_obat").val(sisa_obat);
	hitung_review_obat_racikan($(this))
	
});
$('#index_review_racikan_trx tbody').on('change', '.opsi_review', function() {
	
	update_record_review_obat_racikan($(this));
	
});

function update_record_review_obat(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let jenispenggunaan=tabel.closest('tr').find(".jenispenggunaan").val();
	let kuantitas=tabel.closest('tr').find(".kuantitas").val();
	let harga_dasar=tabel.closest('tr').find(".harga_dasar").val();
	let margin=tabel.closest('tr').find(".margin").val();
	let harga_jual=tabel.closest('tr').find(".harga_jual").val();
	let diskon=tabel.closest('tr').find(".diskon").val();
	let diskon_rp=tabel.closest('tr').find(".diskon_rp").val();
	let expire_date=tabel.closest('tr').find(".expire_date").val();
	let tuslah=tabel.closest('tr').find(".tuslah").val();
	let totalharga=tabel.closest('tr').find(".totalharga").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_review_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				jenispenggunaan : jenispenggunaan,
				kuantitas : kuantitas,
				harga_dasar : harga_dasar,
				margin : margin,
				harga_jual : harga_jual,
				diskon : diskon,
				diskon_rp : diskon_rp,
				expire_date : expire_date,
				tuslah : tuslah,
				totalharga : totalharga,
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
}
function update_record_review_obat_racikan(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let jenispenggunaan=tabel.closest('tr').find(".jenispenggunaan").val();
	let kuantitas=tabel.closest('tr').find(".kuantitas").val();
	let harga_jual=tabel.closest('tr').find(".harga").val();
	let expire_date=tabel.closest('tr').find(".expire_date").val();
	let tuslah=tabel.closest('tr').find(".tuslah").val();
	let totalharga=tabel.closest('tr').find(".totalharga").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_review_obat_racikan', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				jenispenggunaan : jenispenggunaan,
				kuantitas : kuantitas,
				harga_jual : harga_jual,
				expire_date : expire_date,
				tuslah : tuslah,
				totalharga : totalharga,
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
}
function simpan_telaah(){
	let assesmen_id=$("#assesmen_id").val();
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Simpan Telaah E-Resep ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/simpan_telaah', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id : assesmen_id,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Racikan E-Resep.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Save Telaah '});
							location.reload();
					}
				}
			});
		});

	
	
	console.log(iter);
}


function show_info_obat(idtipe,idbarang){
	$('.js-select2').select2({
	   dropdownParent: $('#modal_informasi_obat')
	});
	$("#cover-spin").show();
	$("#modal_informasi_obat").modal('show');
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/info_obat/',
		dataType: "json",
		type: 'POST',
		data: {
			idtipe:idtipe,
			idbarang:idbarang,
		},
		success: function(data) {
			$("#info_nama").val(data.nama);
			$("#info_nama_generik").val(data.nama_generik).trigger('change.select2');
			$("#info_dosis").val(data.dosis);
			$("#info_merk").val(data.merk).trigger('change.select2');
			$("#info_high_alert").val(data.high_alert).trigger('change.select2');
			$("#info_nama_industri").val(data.nama_industri).trigger('change.select2');
			$("#info_komposisi").val(data.komposisi);
			$("#info_indikasi").val(data.indikasi);
			$("#info_bentuk_sediaan").val(data.bentuk_sediaan).trigger('change.select2');
			$("#info_jenis_penggunaan").val(data.jenis_penggunaan).trigger('change.select2');
			$("#info_gol_obat").val(data.gol_obat).trigger('change.select2');
			$("#info_satuan").val(data.nama_satuan);
			$("#cover-spin").hide();
		}
	});
}
function lihat_perubahan_obat(id){
	
	$("#cover-spin").show();
	$("#modal_informasi_edit_obat").modal('show');
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/lihat_perubahan_obat/',
		dataType: "json",
		type: 'POST',
		data: {
			id:id,
		},
		success: function(data) {
			$("#tabel_awal tbody").html(data.tabel_1);
			$("#tabel_menjadi tbody").html(data.tabel_2);
			$("#tabel_ttd tbody").html(data.tabel_3);
			$("#alasan_perubahan_info").val(data.alasan_perubahan);
			$("#info_status_perubahan").val(data.label_perubahan);
			
			$("#cover-spin").hide();
		}
	});
}
function lihat_perubahan_obat_racikan(id){
	
	$("#cover-spin").show();
	$("#modal_informasi_edit_obat_racikan").modal('show');
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/lihat_perubahan_obat_racikan/',
		dataType: "json",
		type: 'POST',
		data: {
			id:id,
		},
		success: function(data) {
			$("#tabel_awal_racikan tbody").html(data.tabel_1);
			$("#tabel_menjadi_racikan tbody").html(data.tabel_2);
			$("#tabel_ttd_racikan tbody").html(data.tabel_3);
			$("#alasan_perubahan_info_racikan").val(data.alasan_perubahan);
			$("#info_status_perubahan_racikan").val(data.label_perubahan);
			
			$("#cover-spin").hide();
		}
	});
}
function show_riwayat_obat(jenis){
	// $('.js-select2').select2({
	   // dropdownParent: $('#modal_informasi_obat')
	// });\
	let iddokter='';
	if (jenis=='2'){
		iddokter=$("#iddokter_resep").val();
	}
	let idpasien=$("#idpasien").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
	$("#cover-spin").show();
	$("#modal_riwayat_obat").modal('show');
	$('#tabel_riwayat_obat').DataTable().destroy();	
	$('#tabel_riwayat_obat').DataTable({
		ordering: false,
		autoWidth: false,
		searching: true,
		serverSide: true,
		"processing": false,
		"order": [],
		// paging: false,
		// scrollCollapse: false,
		// scrollY: '300px',
		ajax: { 
			url: '{site_url}Tpoliklinik_resep/riwayat_obat', 
			type: "POST",
			dataType: 'json',
			data : {
					idpasien:idpasien,
					idkelompokpasien:idkelompokpasien,
					iddokter:iddokter,
				   }
		},
		columnDefs: [
			 {  className: "text-right", targets:[0] },
			 {  className: "text-center", targets:[0,1,2,4,5,6,7,8,9] },
			 { "width": "3%", "targets": [0] },
			 { "width": "5%", "targets": [4,5] },
			 { "width": "10%", "targets": [1,2,6,7,8,9] },
			 { "width": "25%", "targets": [3] },
			 // { "width": "25%", "targets": [1] }
		],
		
		"drawCallback": function( settings ) {
			 $("#cover-spin").hide();
			 $(".btn_terapkan").prop("disabled", true);
		 }  
	});
	
}
function show_modal_obat(versi){
	document.getElementById("modal_obat").style.zIndex = "1201";
	$('#merk_filter,#idtipe_filter,#idkat_filter,#jenis_penggunaan_filter,#bentuk_sediaan_filter').select2({
	   dropdownParent: $('#modal_obat')
	});
	$("#modal_obat").modal('show');
	document.getElementById("modal_obat").style.zIndex = "1500";
	if (versi=='1'){
		class_obat='selectObat';
	}else if(versi=='2'){
		class_obat='selectObatRacikan';
	
	}else if(versi=='3'){
		class_obat='selectObatPaket';
	}else if(versi=='4'){
		class_obat='selectObatTemplate';
	}
	
	$("#nama_barang_filter").focus();
	if ($("#nama_barang_filter").val()!=''){
		loadBarang();
	}
}
$(document).on("click", ".selectObat", function(event) {
	clear_1();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_1").val(idtipe);
	$("#st_cover_1").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_1").append(newOption).trigger('change');
	

});
$(document).on("click", ".selectObatPaket", function(event) {
	clear_3();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_3").val(idtipe);
	$("#st_cover_3").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_3").append(newOption).trigger('change');
});
$(document).on("click", ".selectObatTemplate", function(event) {
	clear_4();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_4").val(idtipe);
	$("#st_cover_4").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_4").append(newOption).trigger('change');
});
$(document).on("click", ".selectObatRacikan", function(event) {
	// clear_1();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	alert(idobat);
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_2").val(idtipe);
	$("#st_cover_2").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_2").append(newOption).trigger('change');
	

});
function loadBarang() {
	
	var idkelompokpasien = $("#idkelompokpasien").val();
	var idunit = $("#tujuan_farmasi_id").val();
	var idtipe = $("#idtipe_filter").val();
	var idkategori = $("#idkat_filter").val();
	var nama_barang = $("#nama_barang_filter").val();
	
	
	$('#filter_tabel_obat').DataTable().destroy();	
		$('#filter_tabel_obat').DataTable({
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/get_obat_filter', 
				type: "POST" ,
				dataType: 'json',
				data : {
						cls: class_obat,
						merk: $("#merk_filter").val(),
						jenis_penggunaan: $("#jenis_penggunaan_filter").val(),
						bentuk_sediaan: $("#bentuk_sediaan_filter").val(),
						idunit: idunit,
						idtipe: idtipe,
						idkategori: idkategori,
						nama_barang: nama_barang,
						idkelompokpasien: idkelompokpasien,
						idtipe_poli: $("#idtipe_poli").val(),
						idpoliklinik: $("#idpoliklinik").val(),
						idrekanan: $("#idrekanan").val(),
					   }
			},
			columnDefs: [{
					"width": "5%",
					"targets": [0],
					"orderable": true
				},
				{
					"width": "10%",
					"targets": [1, 3, 4,5,6,7],
					"orderable": true
				},
				
				{
					"width": "35%",
					"targets": [2],
					"orderable": true
				},
				{  className: "text-right", targets:[0,7] },
				{  className: "text-center", targets:[1,3,4,5,6] },
			],
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
}
$("#st_cover_1").change(function() {
	
});
function set_cover(){
	if ($("#st_cover_1").val()=='1'){
		$("#st_label_cover").show();
	}else{
		$("#st_label_cover").hide();
	}
}
function set_cover_2(){
	if ($("#st_cover_2").val()=='1'){
		$("#st_label_cover_2").show();
	}else{
		$("#st_label_cover_2").hide();
	}
}

function set_cover_3(){
	if ($("#st_cover_3").val()=='1'){
		$("#st_label_cover_3").show();
	}else{
		$("#st_label_cover_3").hide();
	}
}
function set_cover_edit(){
	if ($("#st_cover_1_edit").val()=='1'){
		$("#st_label_cover_edit").show();
	}else{
		$("#st_label_cover_edit").hide();
	}
}
function set_cover_edit_racikan(){
	if ($("#st_cover_edit_racikan").val()=='1'){
		$("#st_label_cover_edit_racikan").show();
	}else{
		$("#st_label_cover_edit_racikan").hide();
	}
}
function get_obat_detail_1_edit(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_1_edit").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_1_edit").val();
			st_cover=$("#st_cover_1_edit").val();
		}
	}
	$('#idtipe_1_edit').val(idtipe_barang);
	$('#st_cover_1_edit').val(st_cover);
	set_cover_edit();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		$("#modal_edit_obat").modal('hide');
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_1_edit").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_1_edit").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_1_edit").val(data.dosis);
				
				if (parseFloat($("#jumlah_1_edit").val())==0 || $("#jumlah_1_edit")=='1' ){
					$("#jumlah_1_edit").val('1');
				}
				
				$("#jumlah_1_edit").focus().select();
				
				
			}
		});
	} else {

	}
}
function get_obat_detail_edit_racikan(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_edit_racikan").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			// alert(idtipe_barang);
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_edit_racikan").val();
			st_cover=$("#st_cover_edit_racikan").val();
		}
	}
	$('#idtipe_edit_racikan').val(idtipe_barang);
	$('#st_cover_edit_racikant').val(st_cover);
	set_cover_edit_racikan();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		$("#modal_edit_obat_racikan").modal('hide');
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_edit_racikan").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_edit_racikan").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_edit_racikan").val(data.dosis);
				$("#dosis_master_edit_racikan").val(data.dosis);
				
				if (parseFloat($("#jumlah_edit_racikan").val())==0 || $("#jumlah_edit_racikan")=='1' ){
					$("#jumlah_edit_racikan").val('1');
				}
				
				$("#jumlah_edit_racikan").focus().select();
				
				
			}
		});
	} else {

	}
}
function get_obat_detail_1(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_1").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_1").val();
			st_cover=$("#st_cover_1").val();
		}
	}
	$('#idtipe_1').val(idtipe_barang);
	$('#st_cover_1').val(st_cover);
	set_cover();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		clear_1();
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_1").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_1").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_1").val(data.dosis);
				
				if (parseFloat($("#jumlah_1").val())==0 || $("#jumlah_1")=='1' ){
					$("#jumlah_1").val('1');
				}
				
				$("#jumlah_1").focus().select();
				
				
			}
		});
	} else {

	}
}
function get_obat_detail_3(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_3").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_3").val();
			st_cover=$("#st_cover_3").val();
		}
	}
	$('#idtipe_3').val(idtipe_barang);
	$('#st_cover_3').val(st_cover);
	set_cover();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		clear_3();
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_3").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_3").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_3").val(data.dosis);
				
				if (parseFloat($("#jumlah_3").val())==0 || $("#jumlah_3")=='' ){
					$("#jumlah_3").val('1');
				}
				
				$("#jumlah_3").focus().select();
				
				
			}
		});
	} else {

	}
}
function list_index_obat_racikan(){
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/list_index_obat_racikan_telaah_perubahan/',
		dataType: "json",
		type: 'POST',
		data: {
			assesmen_id:assesmen_id,
			max_iter:max_iter,
			
		},
		success: function(data) {
			$("#div_tabel_racikan").html(data);
			$(".tabel_racikan_opsi").select2();
			$(".number").number(true,0,'.',',');
			disabel_edit();
			$("#cover-spin").hide();
		}
	});

}
function get_obat_detail_2(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_2").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_2").val();
			st_cover=$("#st_cover_2").val();
		}
	}
	$('#idtipe_2').val(idtipe_barang);
	$('#st_cover_2').val(st_cover);
	set_cover_2();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		$("#idbarang_2").val(null).trigger('change.select2');
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_2").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_2").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_2").val(data.dosis);
				$("#dosis_master").val(data.dosis);
				
				if (parseFloat($("#jumlah_2").val())==0 || $("#jumlah_2").val()=='' ){
					$("#p1").val(1);
					$("#p2").val(1);
					$("#jumlah_2").val(1);
				}
				// alert($("#p1").val());
				$("#jumlah_2").focus().select();
				
			}
		});
	} else {

	}
}
$("#idbarang_1").change(function() {
	get_obat_detail_1();
});
$("#idbarang_1_edit").change(function() {
	get_obat_detail_1_edit();
});
$("#idbarang_edit_racikan").change(function() {
	get_obat_detail_edit_racikan();
});
$("#idbarang_2").change(function() {
	get_obat_detail_2();
});
$("#idbarang_3").change(function() {
	get_obat_detail_3();
});
$("#idbarang_4").change(function() {
	get_obat_detail_4();
});

function formatOption(option) {
	if (!option.id) {
		return option.text;
	}

	var info = $(option.element).data('info');
	// var bil='<p class="nice-copy"><span class="label label-danger"><i class="fa fa-times"></i> Tidak Bisa Diakses</span></p>';
                                        
	var $option = $(
		'<div class="select2-custom-container">' +
			'<div class="select2-custom-info">' +
				'<span><strong>' + option.text + '</strong></span>' + label_cover(option.cover) +
				'<span>Bentuk Sediaan : ' + option.bentuk_sediaan + '</span>' +
				'<span>Satuan : ' + option.satuan + '</span>' +
				'<span>Stok : ' + option.stok + '</span>' +
				'<span>Tipe : ' + option.idtipe + '</span>' +
				'<span>Harga : ' + roundNearest100(option.harga) + '</span>' +
				
			'</div>' +
		'</div>'
	);

	return $option;
}
function label_cover(cover){
	$bil='';
	if (cover=='1'){
		$bil='<p class="nice-copy"><span class="label label-warning"><i class="fa fa-exclamation-circle"></i> TIDAK DICOVER</span></p>';
	}
	if (cover=='2'){
		$bil='<p class="nice-copy"><span class="label label-danger"><i class="fa fa-times-circle"></i> TIDAK DAPAT DIPILIH</span></p>';
	}
	return $bil;
}
function roundNearest100_fix(num) {
   return (Math.ceil(num / 100) * 100);
}
function roundNearest100(num) {
   return formatNumber(Math.ceil(num / 100) * 100);
}
function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function create_assesmen(){
	
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe_poli=$("#idtipe_poli").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let iddokter=$("#iddokter").val();
	
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/create_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					idpasien:idpasien,
					iddokter:iddokter,
					idtipe_poli:idtipe_poli,
					idpoliklinik:idpoliklinik,
					berat_badan:<?=($header_tinggi_badan?$header_tinggi_badan:'0')?>,
					tinggi_badan:<?=($header_berat_badan?$header_berat_badan:'0')?>,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_racikan(){
	
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let jml_iter=$("#jml_iter").val();
	let st_manual_resep=$("#st_manual_resep").val();
	
	let template='Racikan Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/create_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
					iter:jml_iter,
					st_manual_resep:st_manual_resep,
				   },
			success: function(data) {
				$("#cover-spin").hide();
				if (data){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created Racikan'});
					$("#racikan_id").val(data.racikan_id);
					$("#status_racikan").val(data.status_racikan);
					$("#nama_racik_2").val(data.nama_racikan);
					$("#jumlah_racik_2").val(data.jumlah_racikan);
					$("#jenis_racik_2").val(data.jenis_racikan).trigger('change.select2');
					$("#interval_2").val(data.interval_racikan).trigger('change.select2');
					$("#rute_2").val(data.rute_racikan).trigger('change.select2');
					$("#aturan_tambahan_2").val(data.aturan_tambahan_racikan).trigger('change.select2');
					$("#iter_2").val(data.iter_racikan).trigger('change.select2');
					clear_2();
					set_input_racikan();
				}else{
					$.toaster({priority : 'error', title : 'Gagal!', message : ' Create Racikan'});
				}
				
			}
		});
	});

}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batal_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function setuju_obat(id,tipe){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Setuju Perubahan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/setuju_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					tipe:tipe,
				
				   },
			success: function(data) {
				if (tipe=='1'){
					list_index_obat();
				}
				if (tipe=='2'){
					list_index_obat_racikan();
				}
			}
		});
	});

}
function tolak_obat(id,tipe){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Menolak Perubahan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/tolak_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					tipe:tipe,
				
				   },
			success: function(data) {
				if (tipe=='1'){
					list_index_obat();
				}
				if (tipe=='2'){
					list_index_obat_racikan();
				}
			}
		});
	});

}
function hapus_assesmen(assesmen_id){
	// let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Notifikasi Pembatalan Order ?",
		text : "Apakah Anda Yakin akan Membatalkan permintaan ini ?. Jika dilakukan Pembatalan makan permintaan anda tidak akan terkirim kepada Farmasi Tujuan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_assesmen', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function disabel_edit(){
	if (status_assemen=='2'){
		
		
		 $(".auto_blur").prop("disabled", true);
		 $(".opsi_change").prop("disabled", true);
		 $(".auto_blur_tanggal").prop("disabled", true);
		$(".jawaban_resep").removeAttr('disabled');
		  $("#form_input :input").prop("disabled", true);
				 $(".opsi_review").prop("disabled", true);
				 $(".input_review").prop("disabled", true);
				 $(".btn_minum_obat").prop("disabled", true);
		 $(".btn_persetujuan").removeAttr('disabled');
		 $(".btn_lihat_perubahan").removeAttr('disabled');
	}
}
$(".auto_blur").blur(function(){
		simpan_assesmen();
	
	
});
$(".opsi_change").change(function(){
		simpan_assesmen();
	
	
});
function simpan_assesmen(){
	if ($("#status_tindakan").val() < 2){
		let assesmen_id=$("#assesmen_id").val();
		
		let tanggalpermintaan=$("#tanggalpermintaan").val();
		let waktupermintaan=$("#waktupermintaan").val();
		
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/save_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						tanggalpermintaan:tanggalpermintaan,
						waktupermintaan:waktupermintaan,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						tujuan_farmasi_id : $("#tujuan_farmasi_id").val(),
						iddokter_resep : $("#iddokter_resep").val(),
						berat_badan : $("#berat_badan").val(),
						tinggi_badan : $("#tinggi_badan").val(),
						luas_permukaan : $("#luas_permukaan").val(),
						diagnosa : $("#diagnosa").val(),
						menyusui : $("#menyusui").val(),
						gangguan_ginjal : $("#gangguan_ginjal").val(),
						st_puasa : $("#st_puasa").val(),
						st_alergi : $("#st_alergi").val(),
						alergi : $("#alergi").val(),
						prioritas_cito : $("#prioritas_cito").val(),
						resep_pulang : $("#resep_pulang").val(),
						catatan_resep : $("#catatan_resep").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan E-Resep.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save'});
						
					}
				}
			});
		}
	}
}
function simpan_racikan_final(){
	$("#cover-spin").show();
	$("#status_racikan").val(2);
	simpan_heaad_racikan();
}
function modal_serahkan(){
	$("#modal_serahkan").modal('show');
}
function simpan_heaad_racikan(){
	if (load_awal_assesmen==false){
		// if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		let st_manual_resep=$("#st_manual_resep").val();
		
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/simpan_heaad_racikan_telaah', 
				dataType: "JSON",
				method: "POST",
				data : {
						racikan_id : $("#racikan_id").val(),
						nama_racikan : $("#nama_racik_2").val(),
						jumlah_racikan : $("#jumlah_racik_2").val(),
						jenis_racikan : $("#jenis_racik_2").val(),
						interval_racikan : $("#interval_2").val(),
						rute_racikan : $("#rute_2").val(),
						aturan_tambahan_racikan : $("#aturan_tambahan_2").val(),
						iter_racikan : $("#iter_2").val(),
						status_racikan : $("#status_racikan").val(),
						st_manual_resep : $("#st_manual_resep").val(),

					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Racikan E-Resep.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_racikan=='1'){
							$('#tabel_obat_tmp').DataTable().ajax.reload( null, false );
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft Racikan'});
						}else{
								$("#cover-spin").show();
								location.reload();			
							// if (data.status_assemen=='2'){
								
							// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							// }else{
								// $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								// // alert('sini');
							// }
							
						}
					}
				}
			});
		}
		// }
	}
}
function clear_1(){
	$("#idbarang_1").val(null).trigger('change.select2');
	$("#idtipe_1").val('');
	$("#jumlah_1").val('');
	$("#dosis_1").val('');
	$("#aturan_tambahan_1").val('');
	$("#st_cover_1").val('');
	set_cover();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function clear_3(){
	$("#idbarang_3").val(null).trigger('change.select2');
	$("#idtipe_3").val('');
	$("#jumlah_3").val('');
	$("#dosis_3").val('');
	$("#aturan_tambahan_3").val('');
	$("#st_cover_3").val('');
	set_cover_3();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function list_index_obat(){
	// alert(tab);
	 
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$(".dt-filter").html('');
		$('#tabel_obat').DataTable().destroy();	
		$('#tabel_obat').DataTable({
			ordering: false,
			autoWidth: false,
			 "pageLength": 100,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: true,
			// scrollCollapse: false,
			// scrollY: '300px',
			"dom": '<"row"<"col-sm-6"l<"dt-filter pull-left push-5-l"><"clearfix">><"col-sm-6"f>>tip',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_obat_telaah_perubahan', 
				type: "POST",
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
						tab:tab,
					   }
			},
			columnDefs: [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 { "width": "3%", "targets": [0] },
				 { "width": "8%", "targets": [2,3,7,4,5] },
				 { "width": "10%", "targets": [6,8] },
				 { "width": "12%", "targets": [9] },
				 // { "width": "15%", "targets": [1] },
				 { "width": "22%", "targets": [1] }
			],
			initComplete: function(){
                $(".dt-filter").append('<button class="btn btn-info" id="btn_hapus_assesmen" title="Data Order Sebelumnya" onclick="show_riwayat_obat(2)" type="button"><i class="fa fa-list"></i> DATA ORDER SEBELUMNYA</button>');
                $(".dt-filter").append('<button onclick="list_index_obat()" type="button" title="Refresh"  class="btn btn-primary pull-5-l"><i class="fa fa-refresh"></i></button>');
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_nr").select2();
				 disabel_edit();
				 $(".number").number(true,0,'.',',');
				
			 }  
		});
}
function cari_eresep(){
	$("#modal_cari_resep").modal("show");
	$('#filter_tujuan_poli,#filter_tujuan_dokter,#filter_tujuan_farmasi').select2({
	   dropdownParent: $('#modal_cari_resep')
	});
	list_cari_rersep();
}
function list_cari_rersep(){
	// alert(tab);
	 
	let idpasien=$("#idpasien").val();
	let nopendaftaran=$("#filter_nopendaftaran").val();
	let tanggal_kirim_1=$("#filter_tanggal_permintaan_1").val();
	let tanggal_kirim_2=$("#filter_tanggal_permintaan_2").val();
	let tanggal_1=$("#filter_tanggal_pendaftaran_1").val();
	let tanggal_2=$("#filter_tanggal_pendaftaran_2").val();
	let idpoliklinik=$("#filter_tujuan_poli").val();
	let iddokter=$("#filter_tujuan_dokter").val();
	let tujuan_farmasi_id=$("#filter_tujuan_farmasi").val();
	
		$('#index_cari_resep').DataTable().destroy();	
		$('#index_cari_resep').DataTable({
			ordering: false,
			autoWidth: false,
			 "pageLength": 100,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: true,
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_cari_rersep', 
				type: "POST",
				dataType: 'json',
				data : {
						idpasien:idpasien,
						nopendaftaran:nopendaftaran,
						tanggal_kirim_1:tanggal_kirim_1,
						tanggal_kirim_2:tanggal_kirim_2,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						idpoliklinik:idpoliklinik,
						iddokter:iddokter,
						tujuan_farmasi_id:tujuan_farmasi_id,
					   }
			},
			columnDefs: [
				 {  className: "text-center", targets:[0,1,2,3,4,5,6,7] },
				 { "width": "5%", "targets": [0] },
				 { "width": "10%", "targets": [1,5,6] },
				 { "width": "15%", "targets": [2,3,4,7] },
				 // { "width": "25%", "targets": [1] }
			],
			
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 // $(".opsi_change_nr").select2();
				 // disabel_edit();
				 // $(".number").number(true,0,'.',',');
				
			 }  
		});
}
function list_index_obat_review_trx(){
	// alert(tab);
	 
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/list_index_obat_review_trx', 
			dataType: "json",
			type: 'POST',
			data: {
				assesmen_id:assesmen_id,
				max_iter:max_iter,
				idkelompokpasien:idkelompokpasien,
				tab:tab,
				
			},
			success: function(data) {
				$("#index_review_trx tbody").empty();
				$("#index_review_trx tbody").append(data.tabel);
				$(".opsi_review").select2();
				$(".number").number(true,0,'.',',');
				$(".expire_date").datepicker({
					format: "dd-mm-yyyy"
				});
				disabel_edit();
			}
		});
}
function list_index_obat_review_racikan_trx(){
	// alert(tab);
	 
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/list_index_obat_review_racikan_trx', 
			dataType: "json",
			type: 'POST',
			data: {
				assesmen_id:assesmen_id,
				max_iter:max_iter,
				idkelompokpasien:idkelompokpasien,
				tab:tab,
				
			},
			success: function(data) {
				$("#index_review_racikan_trx tbody").empty();
				$("#index_review_racikan_trx tbody").append(data.tabel);
				$(".opsi_review").select2();
				$(".number").number(true,0,'.',',');
				$(".expire_date").datepicker({
					format: "dd-mm-yyyy"
				});
				disabel_edit();
			}
		});
}
function load_review_trx(){
	list_index_obat_review_trx();
	list_index_obat_review_racikan_trx();
}
function list_index_racikan_tampung(){
	let racikan_id=$("#racikan_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_obat_tmp').DataTable().destroy();	
		$('#tabel_obat_tmp').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			searching: false, 
			paging: false, 
			info: false,
			"order": [],
			paging: true,
			// scrollCollapse: false,
			// scrollY: '300px',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_racikan_tampung', 
				type: "POST" ,
				dataType: 'json',
				data : {
						racikan_id:racikan_id,
						assesmen_id:assesmen_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
					   }
			},
			// columnDefs: [
				 // {  className: "text-right", targets:[0] },
				 // {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 // { "width": "5%", "targets": [0] },
				 // { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // // { "width": "15%", "targets": [1] },
				 // { "width": "25%", "targets": [1] }
			// ],
			
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_nr").select2();
				 disabel_edit();
				 $(".number").number(true,0,'.',',');
				
			 }  
		});
}
function terapkan_iter_all(){
	// alert($("#item_iter").val());return false;
	if ($("#item_iter").val()=="#"){
		swal({
			title: "Gagal",
			text: "Silahkan Isi ITER Dengan Benar",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});
		return false;
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerapakan ITER Ke Inputan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/terapkan_iter_all', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					iter:$("#item_iter").val(),
				   },
			success: function(data) {
				$("#cover-spin").hide();
				$('#tabel_obat').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Terapkan ITER'});
				
			}
		});
	});
	
}
function hapus_obat(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Obat E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat').DataTable().ajax.reload( null, false );
				clear_1();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}

function hapus_racikan_detail(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Obat Racikan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_racikan_detail', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat_tmp').DataTable().ajax.reload( null, false );
				// clear_2();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function hapus_racikan_detail_final(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Obat Racikan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_racikan_detail', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$("#cover-spin").hide();
				list_index_obat_racikan();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function batalkan_validasi(){
	let assesmen_id=$("#assesmen_id").val();
	let status_tindakan=$("#status_tindakan").val();
	let idpenjualan=$("#idpenjualan").val();
	batalkan_transaksi(assesmen_id,status_tindakan,idpenjualan);
}
function batalkan_transaksi_resep(){
	let assesmen_id=$("#assesmen_id").val();
	let status_tindakan=$("#status_tindakan").val();
	let idpenjualan=$("#idpenjualan").val();
	batalkan_transaksi(assesmen_id,status_tindakan,idpenjualan);
}
function batalkan_transaksi(assesmen_id,status_tindakan,idpenjualan){
	//2:Proses;3:validasi
	// alert(status_tindakan);
	let label_tindakan='';
	if (status_tindakan=='2'){
		label_tindakan=' Batalkan Transaksi ? Tindakan ini akan mengembalikan Stok Barang';
	}
	if (status_tindakan=='3'){
		label_tindakan=' Batalkan Validasi ? Tindakan ini hanya mengembalikan Status Tindakan';
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : label_tindakan,
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tfarmasi_tindakan/batalkan_transaksi',
			type: 'POST',
			dataType: "json",
			data: {
				status_tindakan: status_tindakan,
				assesmen_id: assesmen_id,
				idpenjualan: idpenjualan,
			
			},
			success: function(data) {
				location.reload();
			}
		});
	});

}
function batalkan_racikan(){
	let racikan_id=$("#racikan_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Membatalkan Racikan Ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batalkan_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					racikan_id:racikan_id,
				   },
			success: function(data) {
				$("#racikan_id").val('');
				set_input_racikan();
				$('#tabel_obat_tmp').DataTable().ajax.reload( null, false );
				// clear_2();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Racikan'});
			}
		});
	});

}
function ambil_resep(assesmen_id_resep,st_pemeriksaan){
	let label_tindakan='';
	if (st_pemeriksaan=='1'){//BELUM DI PROSES ALL
		label_tindakan=' Resep Yang Belum Diproses';
	}
	if (st_pemeriksaan=='2'){//AMBIL SISA
		label_tindakan=' Obat Sisanya';
	}
	$("#modal_cari_resep").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Mengambil "+label_tindakan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/ambil_resep', 
			dataType: "JSON",
			method: "POST",
			data : {
					idpasien:$("#idpasien").val(),
					pendaftaran_id:$("#pendaftaran_id").val(),
					assesmen_id:$("#assesmen_id").val(),
					assesmen_id_resep:assesmen_id_resep,
					st_pemeriksaan:st_pemeriksaan,
				   },
			success: function(data) {
				location.reload();
			}
		});
	});
}
function update_batal_tidak_diambil(){
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Review Obat Ulang ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/update_batal_tidak_diambil', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
				   },
			success: function(data) {
				location.reload();
			}
		});
	});

}
function hapus_racikan_head(id){
	let racikan_id=id;
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Racikan Ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batalkan_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					racikan_id:racikan_id,
				   },
			success: function(data) {
				$("#cover-spin").hide();
				$("#racikan_id").val('');
				set_input_racikan();
				list_index_obat_racikan();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Racikan'});
			}
		});
	});

}
function cek_duplicate_barang(){
	let status_find=false;
	$('#tabel_obat tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_1").val()==idtipe && $("#idbarang_1").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}

function cek_duplicate_racikan(){
	let status_find=false;
	$('#tabel_obat_tmp tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_2").val()==idtipe && $("#idbarang_2").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function add_1(){
	let cover=$("#st_cover_1").val();
	if (cek_duplicate_barang()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_non_racikan();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_1();
			  return false;
		  } 
		})

	}else{
		add_barang_non_racikan();
	}
	
}
function add_1_edit(){
	let cover=$("#st_cover_1_edit").val();
	// if (cek_duplicate_barang_edit()==true){
		// swal({
			// title: "Gagal",
			// text: "Barang Duplicate",
			// type: "error",
			// timer: 1500,
			// showConfirmButton: false
		// });
		// return false;
	// }
	$("#modal_edit_obat").modal('hide');
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			save_edit_non_racikan();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  // clear_1();
			  return false;
		  } 
		})

	}else{
		save_edit_non_racikan();
	}
	
}
function add_3(){
	let cover=$("#st_cover_3").val();
	if (cek_duplicate_paket()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_paket();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_3();
			  return false;
		  } 
		})

	}else{
		add_barang_paket();
	}
	
}
$("#dosis_2").keyup(function(){
	let dosis_master=$("#dosis_master").val();
	let dosis=$("#dosis_2").val();
	let dosis_koma=dosis/dosis_master;
	let p1=Math.ceil(dosis_koma);
	let p2=Math.ceil(p1/dosis_koma);
	$("#jumlah_2").val(p1);
	$("#p1").val(p1);
	$("#p2").val(p2);
});
$("#jumlah_2,#p1,#p2").keyup(function(){
	kalkulasi_dosis_raccikan();		
});
$("#jumlah_2").keyup(function(){
	$("#p1").val($(this).val())
	$("#p2").val(1)
	kalkulasi_dosis_raccikan();		
});

$("#p1,#p2").keyup(function(){
	let p=parseFloat($("#p1").val()) / parseFloat($("#p2").val());
	let jml=Math.ceil(p)
	$("#jumlah_2").val(jml)
	kalkulasi_dosis_raccikan();		
});

function kalkulasi_dosis_raccikan(){
	let jumlah=$("#jumlah_2").val();
	let p=parseFloat($("#p1").val()) / parseFloat($("#p2").val());
	let dosis_master=$("#dosis_master").val();
	let dosis=p * dosis_master;
	$("#dosis_2").val(dosis);
}


$("#dosis_edit_racikan").keyup(function(){
	let dosis_master=$("#dosis_master_edit_racikan").val();
	let dosis=$("#dosis_edit_racikan").val();
	let dosis_koma=dosis/dosis_master;
	let p1=Math.ceil(dosis_koma);
	let p2=Math.ceil(p1/dosis_koma);
	$("#jumlah_edit_racikan").val(p1);
	$("#p1_edit_racikan").val(p1);
	$("#p2_edit_racikan").val(p2);
});
$("#jumlah_edit_racikan,#p1_edit_racikan,#p2_edit_racikan").keyup(function(){
	kalkulasi_dosis_raccikan2();		
});
// $("#jumlah_2").keyup(function(){
	// $("#p1").val($(this).val())
	// $("#p2").val(1)
	// kalkulasi_dosis_raccikan2();		
// });

$("#p1_edit_racikan,#p2_edit_racikan").keyup(function(){
	let p=parseFloat($("#p1_edit_racikan").val()) / parseFloat($("#p2_edit_racikan").val());
	let jml=Math.ceil(p)
	$("#jumlah_edit_racikan").val(jml)
	kalkulasi_dosis_raccikan2();		
});

function kalkulasi_dosis_raccikan2(){
	let jumlah=$("#jumlah_edit_racikan").val();
	let p=parseFloat($("#p1_edit_racikan").val()) / parseFloat($("#p2_edit_racikan").val());
	let dosis_master=$("#dosis_master_edit_racikan").val();
	let dosis=p * dosis_master;
	$("#dosis_edit_racikan").val(dosis);
}


function add_barang_non_racikan(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let idbarang=$("#idbarang_1").val();
	let idtipe=$("#idtipe_1").val();
	let nama=$("#idbarang_1 option:selected").text();
	let jumlah=$("#jumlah_1").val();
	let interval=$("#interval_1").val();
	let rute=$("#rute_1").val();
	let aturan_tambahan=$("#aturan_tambahan_1").val();
	let iter=$("#iter_1").val();
	let dosis=$("#dosis_1").val();
	let cover=$("#st_cover_1").val();
	let st_manual_resep=$("#st_manual_resep").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_obat_edit', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id : assesmen_id,
				pendaftaran_id : pendaftaran_id,
				idpasien : idpasien,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				jumlah : jumlah,
				interval : interval,
				rute : rute,
				aturan_tambahan : aturan_tambahan,
				iter : iter,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				$('#tabel_obat').DataTable().ajax.reload( null, false );
				clear_1();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat'});
			}
		}
	});
}
function save_edit_non_racikan(){
	let id=$("#trx_id_obat").val();
	
	let assesmen_id=$("#assesmen_id").val();
	let idbarang=$("#idbarang_1_edit").val();
	let idtipe=$("#idtipe_1_edit").val();
	let nama=$("#idbarang_1_edit option:selected").text();
	let jumlah=$("#jumlah_1_edit").val();
	let interval=$("#interval_1_edit").val();
	let rute=$("#rute_1_edit").val();
	let aturan_tambahan=$("#aturan_tambahan_1_edit").val();
	let iter=$("#iter_1_edit").val();
	let dosis=$("#dosis_1_edit").val();
	let cover=$("#st_cover_1_edit").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#modal_edit_obat").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Melakukan Perubahan Resep, Proses Ini Memerlukan Persetujuan Dokter ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/save_edit_non_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					id : id,
					idbarang : idbarang,
					idtipe : idtipe,
					nama : nama,
					dosis : dosis,
					jumlah : jumlah,
					interval : interval,
					rute : rute,
					aturan_tambahan : aturan_tambahan,
					iter : iter,
					cover : cover,
					assesmen_id : assesmen_id,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan E-Resep.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
				}else{
					$('#tabel_obat').DataTable().ajax.reload( null, false );
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat'});
				}
			}
		});
	});


	
}

function add_2(){
	if ($("#nama_racik_2").val()==''){
		swal({title: "Gagal!",text: "Tentukan Nama Racikan",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if ($("#jumlah_racik_2").val()=='' || $("#jumlah_racik_2").val()=='0'){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if ($("#jenis_racik_2").val()=='' || $("#jenis_racik_2").val()==null){
		swal({title: "Gagal!",text: "Tentukan Jenis Racik",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	
	let cover=$("#st_cover_2").val();
	if (cek_duplicate_racikan()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_racikan();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_1();
			  return false;
		  } 
		})

	}else{
		add_barang_racikan();
	}
	
}
function add_barang_racikan(){
	let racikan_id=$("#racikan_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let idbarang=$("#idbarang_2").val();
	let idtipe=$("#idtipe_2").val();
	let nama=$("#idbarang_2 option:selected").text();
	let jumlah=$("#jumlah_2").val();
	let dosis_master=$("#dosis_master").val();
	let dosis=$("#dosis_2").val();
	let p1=$("#p1").val();
	let p2=$("#p2").val();
	let cover=$("#st_cover_2").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_obat_racikan', 
		dataType: "JSON",
		method: "POST",
		data : {
				racikan_id : racikan_id,
				assesmen_id : assesmen_id,
				pendaftaran_id : pendaftaran_id,
				idpasien : idpasien,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				dosis_master : dosis_master,
				jumlah : jumlah,
				p1 : p1,
				p2 : p2,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				list_index_racikan_tampung();
				clear_2();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat Racikan'});
			}
		}
	});
}

function clear_2(){
	$("#idbarang_2").val(null).trigger('change.select2');
	$("#idtipe_2").val('');
	$("#jumlah_2").val(1);
	$("#dosis_2").val('');
	$("#dosis_master").val('');
	$("#p1").val(1);
	$("#p2").val(1);
	$("#st_cover_2").val('');
	set_cover_2();
}


function copy_history_eresep(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/copy_history_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/create_template_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function show_edit_obat(id,tabel){
	
	$("#cover-spin").show();
	$("#modal_edit_obat").modal('show');
	document.getElementById("modal_edit_obat").style.zIndex = "1201";
	$('#interval_1_edit,#rute_1_edit,#iter_1_edit').select2({
	   dropdownParent: $('#modal_edit_obat')
	});
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/edit_obat', 
		dataType: "json",
		type: 'POST',
		data: {
			id:id,
		},
		success: function(data) {
			$("#trx_id_obat").val(data.id);
			$("#idtipe_1_edit").val(data.idtipe);
			$("#jumlah_1_edit").val(data.jumlah);
			$("#interval_1_edit").val(data.interval).trigger('change.select2');
			$("#dosis_1_edit").val(data.dosis);
			$("#rute_1_edit").val(data.rute).trigger('change.select2');
			$("#aturan_tambahan_1_edit").val(data.aturan_tambahan);
			$("#st_cover_1_edit").val(data.cover);
			$("#iter_1_edit").val(data.iter).trigger('change.select2');
			let nama = data.nama;
			let idobat = data.idbarang;
			let newOption = new Option(nama, idobat, true, true);
							
			$("#idbarang_1_edit").append(newOption).trigger('change.select2');
			
			set_cover_edit();
			$("#cover-spin").hide();
		}
	});
	$('#idbarang_1_edit').select2({
		minimumInputLength: 2,
		ajax: {
            url: '{site_url}Tpoliklinik_resep/get_obat/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
                tujuan_farmasi_id: $("#tujuan_farmasi_id").val(),
                idtipe_poli: $("#idtipe_poli").val(),
                idkelompokpasien: $("#idkelompokpasien").val(),
                idpoliklinik: $("#idpoliklinik").val(),
                idrekanan: $("#idrekanan").val(),
				
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: '['+item.kode+']'+' '+item.nama,
                            id: item.idbarang,
							idtipe : item.idtipe,
							satuan : item.nama_satuan,
							stok : item.stok,
							harga : (item.harga),
							bentuk_sediaan : item.bentuk_sediaan,
							cover : item.cover,
                        }
                    })
                };
            }
        },
		templateResult: formatOption,
		escapeMarkup: function (markup) {
			return markup;
		},
		dropdownParent: $('#modal_edit_obat')
	});
}
function show_edit_obat_racikan_detail(id,tabel){
	
	$("#cover-spin").show();
	$("#modal_edit_obat_racikan").modal('show');
	document.getElementById("modal_edit_obat_racikan").style.zIndex = "1201";
	// $('#interval_1_edit,#rute_1_edit,#iter_1_edit').select2({
	   // dropdownParent: $('#modal_edit_obat_racikan')
	// });
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/edit_obat_racikan', 
		dataType: "json",
		type: 'POST',
		data: {
			id:id,
		},
		success: function(data) {
			$("#trx_id_racikan_id").val(data.id);
			$("#idtipe_edit_racikan").val(data.idtipe);
			$("#jumlah_edit_racikan").val(data.jumlah);
			$("#p1_edit_racikan").val(data.p1);
			$("#p2_edit_racikan").val(data.p2);
			$("#dosis_edit_racikan").val(data.dosis);
			$("#dosis_master_edit_racikan").val(data.dosis_master);
			
			
			let nama = data.nama;
			let idobat = data.idbarang;
			let newOption = new Option(nama, idobat, true, true);
							
			$("#idbarang_edit_racikan").append(newOption).trigger('change.select2');
			
			set_cover_edit_racikan();
			$("#cover-spin").hide();
		}
	});
	
	$('#idbarang_edit_racikan').select2({
		minimumInputLength: 2,
		ajax: {
            url: '{site_url}Tpoliklinik_resep/get_obat/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
                tujuan_farmasi_id: $("#tujuan_farmasi_id").val(),
                idtipe_poli: $("#idtipe_poli").val(),
                idkelompokpasien: $("#idkelompokpasien").val(),
                idpoliklinik: $("#idpoliklinik").val(),
                idrekanan: $("#idrekanan").val(),
				
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: '['+item.kode+']'+' '+item.nama,
                            id: item.idbarang,
							idtipe : item.idtipe,
							satuan : item.nama_satuan,
							stok : item.stok,
							harga : (item.harga),
							bentuk_sediaan : item.bentuk_sediaan,
							cover : item.cover,
                        }
                    })
                };
            }
        },
		templateResult: formatOption,
		escapeMarkup: function (markup) {
			return markup;
		},
		dropdownParent: $('#modal_edit_obat_racikan')
	});
}
function add_edit_racikan(){
	let assesmen_id=$("#assesmen_id").val();
	let idtrx=$("#trx_id_racikan_id").val();
	let dosis_master=$("#dosis_master_edit_racikan").val();
	let dosis=$("#dosis_edit_racikan").val();
	let jumlah=$("#jumlah_edit_racikan").val();
	let p1=$("#p1_edit_racikan").val();
	let p2=$("#p2_edit_racikan").val();
	let idbarang=$("#idbarang_edit_racikan").val();
	let nama=$("#idbarang_edit_racikan option:selected").text();
	let idtipe=$("#idtipe_edit_racikan").val();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/simpan_detail_racikan_telaah', 
		dataType: "JSON",
		method: "POST",
		data : {
				id: idtrx,
				idtipe : idtipe,
				idbarang : idbarang,
				nama : nama,
				dosis : dosis,
				dosis_master : dosis_master,
				jumlah : jumlah,
				p1 : p1,
				p2 : p2,
				assesmen_id : assesmen_id,


			},
		success: function(data) {
			$("#modal_edit_obat_racikan").modal('hide');
			$("#idbarang_edit_racikan").val(null).trigger('change.select2');
			list_index_obat_racikan();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Updates'});
		}
	});
	
}
$("#hereby").change(function(){
	set_persetujuan();
});
function set_persetujuan(){
	
	let assesmen_id=$("#assesmen_id").val();
	let hereby=$("#hereby").val();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_persetujaun_eresep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				hereby:hereby,
			
			   },
		success: function(data) {
			$("#ket_permintaan_ina").html(data);
			// // $("#cover-spin").hide();
			
			// $.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
			// location.reload();
		}
	});
}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan E-Resept ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batal_template_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
 $(document).find('.js-summernote').on('summernote.blur', function() {
   if ($("#st_edited").val()=='0'){
	 
	   
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
  });
$(".auto_blur_racikan").blur(function(){
	// alert('sini');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_heaad_racikan();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});

$("#penerima_info").blur(function(){
	$("#penerima_info_paraf").val($("#penerima_info").val());
	$("#nama_kel_penerima_info").val($("#penerima_info").val());
	
});
$("#nama").blur(function(){
	$("#nama_pemberi_permintaan").html($("#nama").val());
	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	if ($("#waktu_req").val()=='1' && $("#tanggalpermintaan").val()==''){
		swal({title: "Field Harus Diisi",text: "Tanggal Pengajuan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		return false;
	}
	if ($("#tujuan_req").val()=='1' && ($("#tujuan_farmasi_id").val()=='' || $("#tujuan_farmasi_id").val()==null)){
		swal({title: "Field Harus Diisi",text: "Tujuan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#tujuan_farmasi_id").focus().select();
		return false;
	}
	if ($("#dokter_req").val()=='1' && ($("#iddokter_resep").val()=='' || $("#iddokter_resep").val()==null)){
		swal({title: "Field Harus Diisi",text: "Dokter Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#iddokter_resep").focus().select();
		return false;
	}
	if ($("#bb_req").val()=='1' && ($("#berat_badan").val()=='' || $("#berat_badan").val()==0)){
		swal({title: "Field Harus Diisi",text: "Berat Badan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#berat_badan").focus();
		return false;
	}
	if ($("#tb_req").val()=='1' && ($("#tinggi_badan").val()=='' || $("#tinggi_badan").val()==0)){
		swal({title: "Field Harus Diisi",text: "Tinggi Badan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#tinggi_badan").focus();
		return false;
	}
	if ($("#luas_req").val()=='1' && ($("#luas_permukaan").val()=='')){
		swal({title: "Field Harus Diisi",text: "Luas Permukaan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#luas_permukaan").focus();
		return false;
	}
	if ($("#diagnosa_req").val()=='1' && ($("#diagnosa").val()=='' || $("#diagnosa").val()==0)){
		swal({title: "Field Harus Diisi",text: "Diagnosa Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#diagnosa").focus();
		return false;
	}
	if ($("#menyusui_req").val()=='1' && ($("#menyusui").val()=='' || $("#menyusui").val()==null)){
		swal({title: "Field Harus Diisi",text: "Sedang menyusui Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#menyusui").focus();
		return false;
	}
	if ($("#gangguan_req").val()=='1' && ($("#gangguan_ginjal").val()=='' || $("#gangguan_ginjal").val()==null)){
		swal({title: "Field Harus Diisi",text: "Gangguan Ginjal Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#gangguan_ginjal").focus();
		return false;
	}
	if ($("#puasa_req").val()=='1' && ($("#st_puasa").val()=='' || $("#st_puasa").val()==null)){
		swal({title: "Field Harus Diisi",text: "Pasien Puasa Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#st_puasa").focus();
		return false;
	}
	if ($("#alergi_req").val()=='1' && ($("#st_alergi").val()=='' || $("#st_alergi").val()==null)){
		swal({title: "Field Harus Diisi",text: "Alergi Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#st_alergi").focus();
		return false;
	}
	if ($("#alergi_detail_req").val()=='1' && ($("#alergi").val()=='' || $("#alergi").val()==null)){
		swal({title: "Field Harus Diisi",text: "Alergi Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#alergi").focus();
		return false;
	}
	if ($("#prioritas_req").val()=='1' && ($("#prioritas_cito").val()=='' || $("#prioritas_cito").val()==null)){
		swal({title: "Field Harus Diisi",text: "Prioritas Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#prioritas_cito").focus();
		return false;
	}
	if ($("#pulang_req").val()=='1' && ($("#resep_pulang").val()=='' || $("#resep_pulang").val()==null)){
		swal({title: "Field Harus Diisi",text: "Resep PulangHarus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#resep_pulang").focus();
		return false;
	}
	if ($("#catatan_req").val()=='1' && ($("#catatan_resep").val()=='' || $("#catatan_resep").val()==null)){
		swal({title: "Field Harus Diisi",text: "Catata Resep Pulang PulangHarus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#catatan_resep").focus();
		return false;
	}
	// alert($("#form_input").valid());
	// if($("#form_input").valid()){
		// //loader
		let nama_tujuan=$("#tujuan_farmasi_id option:selected").text();
		swal({
			title: "Notifikasi",
			text : "Apakah Anda Yakin telah selesai membuat e-resep? Data permintaan Anda akan otomatis terkirim ke "+nama_tujuan+".",
			icon : "question",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			
			simpan_assesmen();
		});		
	
	
}
function set_diambil(){
	let total_sisa=parseFloat($("#total_sisa_obat").val()) + parseFloat($("#total_sisa_racikan").val())
	let label='';
	let st_ambil_sebagian=0;
	if (total_sisa>0){
		label=' Anda Yakin Obat diambil sebagian ?'
		st_ambil_sebagian=1;
	}else{
		label=' Anda Yakin Obat Diambil Semua ?'
		
	}
		swal({
			title: "Notifikasi",
			text : label,
			icon : "question",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/set_diambil', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id : $("#assesmen_id").val(),
						pendaftaran_id : $("#pendaftaran_id").val(),
						st_ambil_sebagian : st_ambil_sebagian,
						tanggalproses_resep : $("#tanggalproses_resep").val(),
						waktuproses_resep : $("#waktuproses_resep").val(),
						
					},
				success: function(data) {
					location.reload();
				}
			});
		});		
	
	
}
function update_validasi(){
	if ($("#user_etiket").val()==''){
		sweetAlert("Maaf...", "Isi Petugas E-Ticket!", "error");
		return false;
	}
	if ($("#user_ambil").val()==''){
		sweetAlert("Maaf...", "Isi Petugas Pengambil!", "error");
		return false;
	}
	swal({
		title: "Notifikasi",
		text : "Yakin Obat Sudah Sesuai dan Lanjut Validasi ?",
		icon : "question",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/update_validasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id : $("#assesmen_id").val(),
					user_etiket : $("#user_etiket").val(),
					user_ambil : $("#user_ambil").val(),
					
				},
			success: function(data) {
				location.reload();
			}
		});
	});		

	
}
function update_serahkan(){
	$("#modal_serahkan").modal("hide");
	swal({
		title: "Notifikasi",
		text : "Yakin Proses Penyerahan ?",
		icon : "question",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/update_serahkan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id : $("#assesmen_id").val(),
					tanggalpenyerahan : $("#tanggalpenyerahan").val(),
					waktupenyerahan : $("#waktupenyerahan").val(),
					assesmen_id_resep : $("#assesmen_id_resep").val(),
					st_direct : $("#st_direct").val(),
					
				},
			success: function(data) {
				location.reload();
			}
		});
	});		

	
}
function set_tidak_diambil(){
	$("#modal_tidak_diambil").modal('show');
}
function save_tidak_diambil(){
	$("#modal_tidak_diambil").modal('hide');
	let alasan_tidak_diambil=$("#alasan_tidak_diambil").val();
	if (alasan_tidak_diambil==''){
		swal({
			title: "Warning!",
			text: "Alasan Harus diisi.",
			type: "success",
			timer: 1500,
			showConfirmButton: false
		});
		$("#modal_tidak_diambil").modal('show');
		return false;

	}
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/set_tidak_diambil', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id : $("#assesmen_id").val(),
					alasan_tidak_diambil : alasan_tidak_diambil,
					tanggalproses_resep : $("#tanggalproses_resep").val(),
					waktuproses_resep : $("#waktuproses_resep").val(),
				},
			success: function(data) {
				location.reload();
			}
		});
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}
	$("#modal_savae_template_assesmen").modal('hide');
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}

	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_index_template_eresep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 // $("#index_template_assemen thead").remove();
					 disabel_edit();
				 }  
			});
		
	}
	
	function edit_assesmen(assesmen_id,pendaftaran_id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit E-Resep?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/edit_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan E-Resep.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						// window.location.href = "'{site_url}tpoliklinik_resep/tindakan/"+pendaftaran_id+"/erm_e_resep/lihat_perubahan'";
						window.location.href = "<?php echo site_url('tpoliklinik_resep/tindakan/"+pendaftaran_id+"/erm_e_resep/lihat_perubahan'); ?>";
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let profesi_id_filter=$("#profesi_id_filter").val();
		let st_verifikasi=$("#st_verifikasi").val();
		let st_owned=$("#st_owned").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						// {  className: "text-center", targets:[1,2,3,5,6,7] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "10%", "targets": [1,2,3,5] },
						 // { "width": "15%", "targets": [4] },
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_history_pengkajian_eresep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							st_owned:st_owned,
							st_verifikasi:st_verifikasi,
							pemberi_info:profesi_id_filter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 $("#index_history_kajian thead").remove();
				 }  
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		// $("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/save_edit_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
						location.reload();
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		$("#modal_hapus").modal('hide');
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan E-Resep ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/hapus_record_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template E-Resep ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/hapus_record_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	$(document).on("click",".edit",function(){
		// assesmen_id=$("#assesmen_id").val();
		let tr=$(this).closest('tr');
		tr.find(".tabel_racikan").removeAttr('disabled');
		tr.find(".tabel_racikan_opsi").removeAttr('disabled');
		tr.find(".edit").removeClass('edit').addClass('save').addClass('btn-primary').html('<i class="fa fa-save"></i>');
		tr.find(".hapus").removeClass('hapus').addClass('batal').addClass('btn-danger').html('<i class="fa fa-times"></i>');
		tr.find(".nama_racikan").focus();
		
	});
	$(document).on("click",".save",function(){
		let tr=$(this).closest('tr');
		let racikan_id=$(this).closest('tr').find(".racikan_id").val();
		let nama_racikan=$(this).closest('tr').find(".nama_racikan").val();
		let jumlah_racikan=$(this).closest('tr').find(".jumlah_racikan").val();
		let jenis_racikan=$(this).closest('tr').find(".jenis_racikan").val();
		let interval_racikan=$(this).closest('tr').find(".interval_racikan").val();
		let rute_racikan=$(this).closest('tr').find(".rute_racikan").val();
		let aturan_tambahan_racikan=$(this).closest('tr').find(".aturan_tambahan_racikan").val();
		let iter_racikan=$(this).closest('tr').find(".iter_racikan").val();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/simpan_heaad_racikan_telaah', 
			dataType: "JSON",
			method: "POST",
			data : {
					racikan_id :racikan_id ,
					nama_racikan : nama_racikan ,
					jumlah_racikan : jumlah_racikan ,
					jenis_racikan : jenis_racikan ,
					interval_racikan : interval_racikan ,
					rute_racikan : rute_racikan ,
					aturan_tambahan_racikan : aturan_tambahan_racikan ,
					iter_racikan : iter_racikan ,
					status_racikan : 2,

				},
			success: function(data) {
				
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Racikan E-Resep.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					list_index_obat_racikan();
				}
			}
		});
	});
	$(document).on("click",".batal",function(){
		list_index_obat_racikan();
	});
	$(document).on("click",".hapus",function(){
		let id=$(this).closest('tr').find(".racikan_id").val();
		hapus_racikan_head(id);
	});
	
	$(document).on("blur",".dosis_nrd",function(){
		assesmen_id=$("#assesmen_id").val();
	
		let tr=$(this).closest('tr');
		let idtrx=tr.find(".cls_idtrx").val();
		let jumlah_racikan=tr.find(".jumlah_racikan").val();
		let dosis=tr.find(".dosis").val();
		let jumlah=tr.find(".jumlah").val();
		let p1=tr.find(".p1").val();
		let p2=tr.find(".p2").val();
		let p=parseFloat(p1) / parseFloat(p2);
		let jml=Math.ceil(p);
		let total=jumlah_racikan * jumlah;
		console.log(jumlah_racikan);
		tr.find(".total").val(total);
		tr.find(".jumlah").val(jml);
		jumlah=jml;
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/simpan_detail_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					id: idtrx,
					jumlah_racikan : jumlah_racikan,
					dosis : dosis,
					jumlah : jumlah,
					p1 : p1,
					p2 : p2,


				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Updates'});
			}
		});
		
		
		
	});
	
	
	function validate_waktu() {
		if ($("#waktu_minum").val() == "") {
			swal({
				title: "Gagal!",
				text: "Lengkapi Waktu Minum.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			$("#waktu_minum").focus();
			return false;
		}
		if ($("#jam_minum").val() == "" || $("#jam_minum").val() == "00:00") {
			swal({
				title: "Gagal!",
				text: "Lengkapi Jam Minum.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});
			$("#waktu_minum").focus();
			return false;
		}

		return true;
	}

	
	function clear_detail_waktu() {
		$("#nomor_waktu").val('');
		$("#waktu_minum").val('');
		$("#jam_minum").val('00:00');

		var total_rows = 0;
		$('#manage-tabel-Waktu tbody tr').each(function() {
			total_rows += 1;
		});
		// alert(total_rows);
		if (total_rows > 0) {
			$("#btn_ok_waktu").attr('disabled', false);
		} else {
			$("#btn_ok_waktu").attr('disabled', true);
		}
		disabel_edit();
	}

	$("#addwaktu").click(function() {
		if (!validate_waktu()) return false;
		var duplicate = false;
		var content = "";
		var tabel='';
		var btn_hapus='<button class="btn btn-danger btn-xs hapuswaktu"><i class="fa fa-times"></i></button>';
		tabel +='<tr><td class="text-center">'+$("#waktu_minum").val()+'</td><td class="text-center">'+$("#jam_minum").val()+'</td><td class="text-center">'+btn_hapus+'</td></tr>';
		$("#manage-tabel-Waktu tbody").append(tabel);
		clear_detail_waktu();
	});
	$("#btn_ok_waktu").click(function() {
		let arr_waktu =[];
		let nourut=0;
		$('#manage-tabel-Waktu tbody tr').each(function() {
			let waktu=$(this).find('td:eq(0)').text();
			let jam=$(this).find('td:eq(1)').text();
			arr_waktu.push([nourut,waktu,jam]);
			// arr_waktu[nourut].push(jam)
			nourut ++;
        });
		let id=$("#id_detail_waktu").val();
		let tipe=$("#tipe_tabel").val();
		let link_url='';
		if (tipe=='1'){//Non Racikan
			 link_url='{site_url}Tpoliklinik_resep/update_minum_obat/';
		}else{
			 link_url='{site_url}Tpoliklinik_resep/update_minum_obat_racikan/';
		}
		$("#cover-spin").show();
		$.ajax({
			url: link_url,
			dataType: "json",
			type: 'POST',
			data: {
				id:id,
				arr_waktu:arr_waktu,
			},
			success: function(data) {
				$("#cover-spin").hide();
				if (tipe=='1'){
					list_index_obat_review_trx();
				}else{
					list_index_obat_review_racikan_trx();
					
				}
			}
		});

	});
	function tambah_waktu(id,tipe){
		$("#id_detail_waktu").val(id);
		$("#tipe_tabel").val(tipe);
		$("#waktu_modal").modal('show');
		clear_detail_waktu();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_info_minum_obat/',
			dataType: "json",
			type: 'POST',
			data: {
				id:id,
				tipe:tipe
			},
			success: function(data) {
				$("#cover-spin").hide();
				let myString=data.waktu_minum;
				$("#manage-tabel-Waktu tbody").empty();
				if (myString){
				
					var array_waktu_minum_obat = JSON.parse("[" + myString + "]");
					var btn_hapus='<button class="btn btn-danger btn-xs hapuswaktu btn_minum_obat"><i class="fa fa-times"></i></button>';
					var tabel='';
					for(let i = 0; i < array_waktu_minum_obat.length; i++){
						let innerArrayLength = array_waktu_minum_obat[i].length;
						for(let j = 0; j < innerArrayLength; j++) {
							tabel +='<tr><td class="text-center">'+array_waktu_minum_obat[i][j][1]+'</td><td class="text-center">'+array_waktu_minum_obat[i][j][2]+'</td><td class="text-center">'+btn_hapus+'</td></tr>';
							console.log(array_waktu_minum_obat[i][j][1]);
						}
					}
				}
				$("#manage-tabel-Waktu tbody").append(tabel);
				clear_detail_waktu();
			}
		});
		disabel_edit();
	};
	$(document).on("click", ".hapuswaktu", function() {
			
		$(this).closest('td').parent().remove();
			

	});
	//PAKET
function get_info_paket(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_info_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			
		},
		success: function(data) {
			$("#paket_id").val(data.paket_id);
			$("#nama_paket").val(data.nama_paket);
			$("#status_paket").val(data.status_paket);
			set_paket();
			$("#cover-spin").hide();
		}
	});
}
function set_paket(){
	if ($("#paket_id").val()==''){
		$(".input_paket").hide();
		$("#nama_paket").removeAttr('disabled');
		$(".new_paket").show();
		list_index_paket();
	}else{
		$("#nama_paket").attr('disabled',true);
		$(".input_paket").show();
		$(".new_paket").hide();
		list_index_obat_paket();
	}
}
function create_paket(){
	if ($("#nama_paket").val()==''){
		swal({
			title: "Informasi!",text: "Nama Paket Harus diisi",type: "error",timer: 1000,showConfirmButton: false
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/create_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			nama_paket : $("#nama_paket").val(),
		},
		success: function(data) {
			$("#paket_id").val(data.paket_id);
			$("#nama_paket").val(data.nama_paket);
			$("#status_paket").val(data.status_paket);
			set_paket();
			$("#cover-spin").hide();
		}
	});
}
function batal_paket(){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			paket_id : $("#paket_id").val(),
		},
		success: function(data) {
			$("#paket_id").val('');
			$(".dt-filter2").html('');
			$("#nama_paket").val('');
			$("#status_paket").val(0);
			set_paket();
			$("#cover-spin").hide();
		}
	});
}
function hapus_paket(paket_id){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			paket_id : paket_id,
		},
		success: function(data) {
			$("#status_paket").val('');
			$("#paket_id").val('');
			$(".dt-filter2").html('');
			$("#nama_paket").val('');
			$('#tabel_paket').DataTable().ajax.reload( null, false );
			$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
function update_record_obat_paket(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let dosis=tabel.closest('tr').find(".dosis_paket").val();
	let jumlah=tabel.closest('tr').find(".jumlah_paket").val();
	let aturan_tambahan=tabel.closest('tr').find(".aturan_tambahan_paket").val();
	let interval=tabel.closest('tr').find(".interval").val();
	let iter=tabel.closest('tr').find(".iter").val();
	let rute=tabel.closest('tr').find(".rute").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_obat_paket', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				dosis : dosis,
				jumlah : jumlah,
				aturan_tambahan : aturan_tambahan,
				interval : interval,
				iter : iter,
				rute : rute,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
	console.log(iter);
}
function list_data_paket_obat(id){
	$("#paket_id").val(id);
	$("#status_paket").val(2);
	list_index_obat_paket();
}
function list_index_obat_paket(){
	let status_paket=$("#status_paket").val();
	let paket_id=$("#paket_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_obat_paket').DataTable().destroy();	
		$('#tabel_obat_paket').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			"dom": '<"row"<"col-sm-6"l<"dt-filter2 pull-left push-5-l"><"clearfix">><"col-sm-6"f>>tip',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_obat_paket', 
				type: "POST",
				dataType: 'json',
				data : {
						paket_id:paket_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
					   }
			},
			columnDefs: [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 { "width": "5%", "targets": [0] },
				 { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // { "width": "15%", "targets": [1] },
				 { "width": "25%", "targets": [1] }
			],
			initComplete: function(){
				if (status_paket=='1'){
					$(".dt-filter2").append('<button onclick="close_paket()" type="button" class="btn btn-success pull-5-l"><i class="fa fa-save"></i> Simpan Paket</button>');
					$(".dt-filter2").append('<button onclick="batal_paket()" type="button" class="btn btn-danger pull-5-l"><i class="fa fa-trash"></i></button>');
				}
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_paket").select2();
				 $(".number").number(true,0,'.',',');
			 }  
		});
}
function list_index_paket(){
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_paket').DataTable().destroy();	
		$('#tabel_paket').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_paket', 
				type: "POST",
				dataType: 'json',
				data : {
					   }
			},
			columnDefs: [
				 // {  className: "text-right", targets:[0] },
				 // {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 // { "width": "5%", "targets": [0] },
				 // { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // // { "width": "15%", "targets": [1] },
				 // { "width": "25%", "targets": [1] }
			],
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
}
function hapus_obat_paket(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Paket Obat E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_obat_paket', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
				clear_3();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function cek_duplicate_paket(){
	let status_find=false;
	$('#tabel_obat_paket tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_3").val()==idtipe && $("#idbarang_3").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function add_barang_paket(){
	let paket_id=$("#paket_id").val();
	let idbarang=$("#idbarang_3").val();
	let idtipe=$("#idtipe_3").val();
	let nama=$("#idbarang_3 option:selected").text();
	let jumlah=$("#jumlah_3").val();
	let interval=$("#interval_3").val();
	let rute=$("#rute_3").val();
	let aturan_tambahan=$("#aturan_tambahan_3").val();
	let iter=$("#iter_3").val();
	let dosis=$("#dosis_3").val();
	let cover=$("#st_cover_3").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_barang_paket', 
		dataType: "JSON",
		method: "POST",
		data : {
				paket_id : paket_id,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				jumlah : jumlah,
				interval : interval,
				rute : rute,
				aturan_tambahan : aturan_tambahan,
				iter : iter,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
				clear_3();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat'});
			}
		}
	});
}
function gunakan_paket(paket_id){
	// let paket_id=paket_id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep Dari Paket ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/gunakan_paket', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					paket_id:paket_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Menggunakan Data Obat Paket'});
				location.reload();
			}
		});
	});

}
function terapkan_history_obat(id){
	// let paket_id=paket_id;
	$('#modal_riwayat_obat').modal('hide');
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Menggunakan Obat Dari History ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/terapkan_history_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					id:id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Proses.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				$('#modal_riwayat_obat').modal('show');
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Menggunakan Data History Resep'});
				list_index_obat();
			}
		});
	});

}
function close_paket(){
	
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Paket ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/close_paket', 
				dataType: "JSON",
				method: "POST",
				data : {
						paket_id : $("#paket_id").val(),
						
					},
				success: function(data) {
					$("#cover-spin").show();
					list_index_obat_paket();
					$("#paket_id").val('');
					$("#nama_paket").val('');
					set_paket();
				}
			});
		});		
	
}
function set_cover_4(){
	if ($("#st_cover_4").val()=='1'){
		$("#st_label_cover_4").show();
	}else{
		$("#st_label_cover_4").hide();
	}
}
function get_obat_detail_4(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_4").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_4").val();
			st_cover=$("#st_cover_4").val();
		}
	}
	$('#idtipe_4').val(idtipe_barang);
	$('#st_cover_4').val(st_cover);
	set_cover();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		clear_4();
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_4").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_4").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_4").val(data.dosis);
				
				if (parseFloat($("#jumlah_4").val())==0 || $("#jumlah_4")=='' ){
					$("#jumlah_4").val('1');
				}
				
				$("#jumlah_4").focus().select();
				
				
			}
		});
	} else {

	}
}
function clear_4(){
	$("#idbarang_4").val(null).trigger('change.select2');
	$("#idtipe_4").val('');
	$("#jumlah_4").val('');
	$("#dosis_4").val('');
	$("#aturan_tambahan_4").val('');
	$("#st_cover_4").val('');
	set_cover_4();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function add_4(){
	let cover=$("#st_cover_4").val();
	if (cek_duplicate_paket()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_paket();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_4();
			  return false;
		  } 
		})

	}else{
		add_barang_paket();
	}
	
}

	//TEMPLATE
function get_info_template(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_info_template/',
		dataType: "json",
		type: 'POST',
		data: {
			
		},
		success: function(data) {
			$("#template_id").val(data.template_id);
			$("#nama_template").val(data.nama_template);
			$("#status_template").val(data.status_template);
			set_template();
			$("#cover-spin").hide();
		}
	});
}
function set_template(){
	if ($("#template_id").val()==''){
		$(".input_template").hide();
		$("#nama_template").removeAttr('disabled');
		$(".new_template").show();
		list_index_template();
	}else{
		$("#nama_template").attr('disabled',true);
		$(".input_template").show();
		$(".new_template").hide();
		list_index_obat_template();
	}
}
function create_template(){
	if ($("#nama_template").val()==''){
		swal({
			title: "Informasi!",text: "Nama Template Harus diisi",type: "error",timer: 1000,showConfirmButton: false
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/create_template/',
		dataType: "json",
		type: 'POST',
		data: {
			nama_template : $("#nama_template").val(),
		},
		success: function(data) {
			$("#template_id").val(data.template_id);
			$("#nama_template").val(data.nama_template);
			$("#status_template").val(data.status_template);
			set_template();
			$("#cover-spin").hide();
		}
	});
}
function batal_template(){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_template/',
		dataType: "json",
		type: 'POST',
		data: {
			template_id : $("#template_id").val(),
		},
		success: function(data) {
			$("#template_id").val('');
			$(".dt-filter3").html('');
			$("#nama_template").val('');
			$("#status_template").val(0);
			set_template();
			$("#cover-spin").hide();
		}
	});
}
function hapus_template(template_id){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_template/',
		dataType: "json",
		type: 'POST',
		data: {
			template_id : template_id,
		},
		success: function(data) {
			$("#status_template").val('');
			$("#template_id").val('');
			$(".dt-filter3").html('');
			$("#nama_template").val('');
			$('#tabel_template').DataTable().ajax.reload( null, false );
			$('#tabel_obat_template').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
function update_record_obat_template(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let dosis=tabel.closest('tr').find(".dosis_template").val();
	let jumlah=tabel.closest('tr').find(".jumlah_template").val();
	let aturan_tambahan=tabel.closest('tr').find(".aturan_tambahan_template").val();
	let interval=tabel.closest('tr').find(".interval").val();
	let iter=tabel.closest('tr').find(".iter").val();
	let rute=tabel.closest('tr').find(".rute").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_obat_template', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				dosis : dosis,
				jumlah : jumlah,
				aturan_tambahan : aturan_tambahan,
				interval : interval,
				iter : iter,
				rute : rute,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
	console.log(iter);
}
function list_data_template_obat(id){
	$("#template_id").val(id);
	$("#status_template").val(2);
	list_index_obat_template();
}
function list_index_obat_template(){
	let status_template=$("#status_template").val();
	let template_id=$("#template_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_obat_template').DataTable().destroy();	
		$('#tabel_obat_template').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			"dom": '<"row"<"col-sm-6"l<"dt-filter3 pull-left push-5-l"><"clearfix">><"col-sm-6"f>>tip',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_obat_template', 
				type: "POST",
				dataType: 'json',
				data : {
						template_id:template_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
					   }
			},
			columnDefs: [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 { "width": "5%", "targets": [0] },
				 { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // { "width": "15%", "targets": [1] },
				 { "width": "25%", "targets": [1] }
			],
			initComplete: function(){
				if (status_template=='1'){
					$(".dt-filter3").append('<button onclick="close_template()" type="button" class="btn btn-success pull-5-l"><i class="fa fa-save"></i> Simpan Template</button>');
					$(".dt-filter3").append('<button onclick="batal_template()" type="button" class="btn btn-danger pull-5-l"><i class="fa fa-trash"></i></button>');
				}
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_template").select2();
				 $(".number").number(true,0,'.',',');
			 }  
		});
}
function list_index_template(){
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_template').DataTable().destroy();	
		$('#tabel_template').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_template', 
				type: "POST",
				dataType: 'json',
				data : {
					   }
			},
			columnDefs: [
				 // {  className: "text-right", targets:[0] },
				 // {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 // { "width": "5%", "targets": [0] },
				 // { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // // { "width": "15%", "targets": [1] },
				 // { "width": "25%", "targets": [1] }
			],
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
}
function hapus_obat_template(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Template Obat E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_obat_template', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat_template').DataTable().ajax.reload( null, false );
				clear_4();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function cek_duplicate_template(){
	let status_find=false;
	$('#tabel_obat_template tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_4").val()==idtipe && $("#idbarang_4").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function add_barang_template(){
	let template_id=$("#template_id").val();
	let idbarang=$("#idbarang_4").val();
	let idtipe=$("#idtipe_4").val();
	let nama=$("#idbarang_4 option:selected").text();
	let jumlah=$("#jumlah_4").val();
	let interval=$("#interval_4").val();
	let rute=$("#rute_4").val();
	let aturan_tambahan=$("#aturan_tambahan_4").val();
	let iter=$("#iter_4").val();
	let dosis=$("#dosis_4").val();
	let cover=$("#st_cover_4").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_barang_template', 
		dataType: "JSON",
		method: "POST",
		data : {
				template_id : template_id,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				jumlah : jumlah,
				interval : interval,
				rute : rute,
				aturan_tambahan : aturan_tambahan,
				iter : iter,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				$('#tabel_obat_template').DataTable().ajax.reload( null, false );
				clear_4();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat'});
			}
		}
	});
}
function gunakan_template(template_id){
	// let template_id=template_id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep Dari Template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/gunakan_template', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					template_id:template_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Menggunakan Data Obat Template'});
				location.reload();
			}
		});
	});

}
function close_template(){
	
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/close_template', 
				dataType: "JSON",
				method: "POST",
				data : {
						template_id : $("#template_id").val(),
						
					},
				success: function(data) {
					$("#cover-spin").show();
					list_index_obat_template();
					$("#template_id").val('');
					$("#nama_template").val('');
					set_template();
				}
			});
		});		
	
}
function set_cover_4(){
	if ($("#st_cover_4").val()=='1'){
		$("#st_label_cover_4").show();
	}else{
		$("#st_label_cover_4").hide();
	}
}
function clear_4(){
	$("#idbarang_4").val(null).trigger('change.select2');
	$("#idtipe_4").val('');
	$("#jumlah_4").val('');
	$("#dosis_4").val('');
	$("#aturan_tambahan_4").val('');
	$("#st_cover_4").val('');
	set_cover_4();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function add_4(){
	let cover=$("#st_cover_4").val();
	if (cek_duplicate_template()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_template();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_4();
			  return false;
		  } 
		})

	}else{
		add_barang_template();
	}
	
}
$('#tabel_obat_template tbody').on('click', '.cls_racikan_update', function() {
	$(this).select();
	$(this).focus();
});
$('#tabel_obat_template tbody').on('blur', '.cls_racikan_update', function() {
	update_record_obat_template($(this));
	
});
$('#tabel_obat_template tbody').on('change', '.opsi_change_paket', function() {
	update_record_obat_template($(this));
	
});
function list_my_order(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_my_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_my_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_resep/list_my_order', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_history_order(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_his_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_his_order").val();
	let iddokter=$("#iddokter_his_order").val();
	let idpoli=$("#idpoli_his_order").val();
	let notransaksi=$("#notransaksi_his_order").val();
	let tanggal_input_1=$("#tanggal_input_1_his_order").val();
	let tanggal_input_2=$("#tanggal_input_2_his_order").val();
	$('#index_history_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_history_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_resep/list_history_order', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}

</script>