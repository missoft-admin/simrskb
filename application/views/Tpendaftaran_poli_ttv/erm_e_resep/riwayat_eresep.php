<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.cls_racikan_update {
		border: 0;
		outline: none;
		width:50px;
	}
	.cls_racikan_update:focus {

		border: 2px solid #000;
		background-color: #fdffe2;
		border: 1;
	}
	
	.dosis_nrd {
		border: 0;
		outline: none;
		width:100%;
	}
	.dosis_nrd:focus {

		border: 2px solid #000;
		background-color: #fdffe2;
		border: 1;
	}

	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>
<?if ($menu_kiri=='riwayat_eresep'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='riwayat_eresep' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<div class="block animated fadeIn push-5-t" data-category="erm_e_resep">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1" onclick="set_tab(1)">Riwayat Resep</a>
				</li>
				<li class="">
					<a href="#tab_1"  onclick="set_tab(2)">Riwayat Resep Detail</a>
				</li>
				<li class="">
					<a href="#tab_1"  onclick="set_tab(3)">Riwayat Transaksi</a>
				</li>
				<li class="">
					<a href="#tab_1"  onclick="set_tab(4)">Riwayat Transaksi Detail</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary" id="judul"></h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_my_order" placeholder="No Pendaftaran" name="notransaksi_my_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_my_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_my_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_my_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_my_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_my_order" name="idpoli_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_my_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_my_order" name="iddokter_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="filter_all()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="div_tabel_1">
									<div class="table-responsive">
										<table class="table" id="index_tab_1">
											<thead>
												<tr>
													<th width="5%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">Permintaan</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemberi Resep</th>
													<th width="9%">Diagnosa</th>
													<th width="10%">Tujuan Farmasi / Prioritas</th>
													<th width="10%">Status Pemeriksaan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
										</div>
										</div>
										<div id="div_tabel_2">
										<div class="table-responsive">s
										<table class="table" id="index_tab_2">
											<thead>
												<tr>
													<th width="5%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">Permintaan</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Dokter Peminta</th>
													<th width="15%">Obat</th>
													<th width="5%">QTY</th>
													<th width="5%">Frekuensi/Interval</th>
													<th width="5%">Rute</th>
													<th width="5%">Aturan Tambahan</th>
													<th width="5%">Iter</th>
													<th width="5%">QTY Ambil</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
										</div>
										</div>
										<div id="div_tabel_3">
										<div class="table-responsive">
										<table class="table" id="index_tab_3">
											<thead>
												<tr>
													<th width="12%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">Permintaan</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemberi Resep</th>
													<th width="10%">Tujuan Farmasi / Prioritas</th>
													<th width="12%">User</th>
													<th width="10%">No Penjualan</th>
													<th width="10%">Nominal Resep</th>
													<th width="10%">Nominal Transaksi</th>
												   
												</tr>
											</thead>
											<tbody></tbody>
										</table>
										</div>
										</div>
										<div id="div_tabel_4">
										<div class="table-responsive">
										<table class="table" id="index_tab_4">
											<thead>
												<tr>
													<th width="12%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">Permintaan</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemberi Resep</th>
													<th width="15%">Nama Obat</th>
													<th width="5%">Dosis</th>
													<th width="5%">Jumlah</th>
													<th width="5%">Jumlah Ambil</th>
													<th width="10%">Nominal Resep</th>
													<th width="10%">Nominal Transaksi</th>
												   
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var tab;
$(document).ready(function() {
	// let idpasien=<?=$idpasien?>;
	// alert(idpasien)
	$(".btn_close_left").click(); 
	tab=<?=$tab?>;
	set_tab(tab);
	
});
function hide_all(){
	$("#div_tabel_1").hide();
	$("#div_tabel_2").hide();
	$("#div_tabel_3").hide();
	$("#div_tabel_4").hide();
}
function filter_all(){
	set_tab(tab);
}
function set_tab($tab){
	tab=$tab;
	hide_all();
	if ($tab=='1'){
		$("#div_tabel_1").show();
		$("#judul").text('RIWAYAT RESEP');
		// alert('sini');
		riwayat_1();
	}
	if ($tab=='2'){
		$("#div_tabel_2").show();
		$("#judul").text('RIWAYAT RESEP DETAIL');
		riwayat_2();
	}
	if ($tab=='3'){
		$("#div_tabel_3").show();
		$("#judul").text('RIWAYAT TRANSAKSI');
		riwayat_3();
	}
	if ($tab=='4'){
		$("#div_tabel_4").show();
		$("#judul").text('RIWAYAT TRANSAKSI DETAIL');
		riwayat_4();
	}
}
function riwayat_1(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_tab_1').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_tab_1').DataTable({
		autoWidth: false,
		searching: true,
		serverSide: true,
		"processing": false,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		columnDefs: [
				{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8] },
			],
		ajax: { 
			url: '{site_url}tpoliklinik_resep/riwayat_1', 
			type: "POST" ,
			dataType: 'json',
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					notransaksi:notransaksi,
					idpoli:idpoli,
					iddokter:iddokter,
					tgl_daftar_1:tgl_daftar_1,
					tgl_daftar_2:tgl_daftar_2,
					tanggal_input_1:tanggal_input_1,
					tanggal_input_2:tanggal_input_2,
					pendaftaran_id:pendaftaran_id,
					
				   }
		},
		"drawCallback": function( settings ) {
			 $("#cover-spin").hide();
		 }  
	});
}
function riwayat_2(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_tab_2').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_tab_2').DataTable({
		autoWidth: false,
		searching: true,
		serverSide: true,
		"processing": false,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		columnDefs: [
				{  className: "text-center", targets:[1,2,3,6,4,0,7,6,7,8,9,10,11] },
			],
		ajax: { 
			url: '{site_url}tpoliklinik_resep/riwayat_2', 
			type: "POST" ,
			dataType: 'json',
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					notransaksi:notransaksi,
					idpoli:idpoli,
					iddokter:iddokter,
					tgl_daftar_1:tgl_daftar_1,
					tgl_daftar_2:tgl_daftar_2,
					tanggal_input_1:tanggal_input_1,
					tanggal_input_2:tanggal_input_2,
					pendaftaran_id:pendaftaran_id,
					
				   }
		},
		"drawCallback": function( settings ) {
			 $("#cover-spin").hide();
		 }  
	});
}
function riwayat_3(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_tab_3').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_tab_3').DataTable({
		autoWidth: false,
		searching: true,
		serverSide: true,
		"processing": false,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		columnDefs: [
				{  className: "text-center", targets:[1,2,3,6,4,0,,5,6,7] },
				{  className: "text-right", targets:[9,8] },
			],
		ajax: { 
			url: '{site_url}tpoliklinik_resep/riwayat_3', 
			type: "POST" ,
			dataType: 'json',
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					notransaksi:notransaksi,
					idpoli:idpoli,
					iddokter:iddokter,
					tgl_daftar_1:tgl_daftar_1,
					tgl_daftar_2:tgl_daftar_2,
					tanggal_input_1:tanggal_input_1,
					tanggal_input_2:tanggal_input_2,
					pendaftaran_id:pendaftaran_id,
					
				   }
		},
		"drawCallback": function( settings ) {
			 $("#cover-spin").hide();
		 }  
	});
}
function riwayat_4(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_tab_4').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_tab_4').DataTable({
		autoWidth: false,
		searching: true,
		serverSide: true,
		"processing": false,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		columnDefs: [
				{  className: "text-center", targets:[1,2,3,6,4,0,6,7,8] },
				{  className: "text-right", targets:[9,10] },
			],
		ajax: { 
			url: '{site_url}tpoliklinik_resep/riwayat_4', 
			type: "POST" ,
			dataType: 'json',
			data : {
					assesmen_id:assesmen_id,
					idpasien:idpasien,
					notransaksi:notransaksi,
					idpoli:idpoli,
					iddokter:iddokter,
					tgl_daftar_1:tgl_daftar_1,
					tgl_daftar_2:tgl_daftar_2,
					tanggal_input_1:tanggal_input_1,
					tanggal_input_2:tanggal_input_2,
					pendaftaran_id:pendaftaran_id,
					
				   }
		},
		"drawCallback": function( settings ) {
			 $("#cover-spin").hide();
		 }  
	});
}
</script>