<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.cls_racikan_update {
		border: 0;
		outline: none;
		width:50px;
	}
	.cls_racikan_update:focus {

		border: 2px solid #000;
		background-color: #fdffe2;
		border: 1;
	}
	
	.dosis_nrd {
		border: 0;
		outline: none;
		width:100%;
	}
	.dosis_nrd:focus {

		border: 2px solid #000;
		background-color: #fdffe2;
		border: 1;
	}

	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>

<?if ($menu_kiri=='input_eresep' || $menu_kiri=='input_eresep_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_eresep' || $menu_kiri=='input_eresep_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_permintaan;
					if ($assesmen_id){
						$tanggalpermintaan=HumanDateShort($tanggal_permintaan);
						$waktupermintaan=HumanTime($tanggal_permintaan);
						
						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						
						$tanggalcreated=HumanDateShort($created_date);
						$waktucreated=HumanTime($created_date);
					}else{
						$waktupermintaan=date('H:i:s');
						$waktuinfo=date('H:i:s');
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggalpermintaan=date('d-m-Y');
						$tanggalcreated=date('d-m-Y');
						$waktucreated=date('H:i:s');
						
					}
					
					$disabel_input='';
					if ($st_input_eresep=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_eresep=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Order Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_my_order()" ><i class="fa fa-send"></i> Order Saya</a>
				</li>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_3" onclick="list_history_order()"><i class="fa fa-history"></i> Riwayat Order</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id?'active in':'')?>" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
					
					<div class="row">
						<input type="hidden" id="jml_iter" value="{jml_iter}"> 
						<input type="hidden" id="max_iter" value="{max_iter}"> 
						<input type="hidden" id="st_browse" value="0"> 
						<input type="hidden" id="idtipe_poli" value="{idtipe_poli}"> 
						<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}"> 
						<input type="hidden" id="idpoliklinik" value="{idpoliklinik}"> 
						<input type="hidden" id="idrekanan" value="{idrekanan}"> 
						<input type="hidden" id="iddokter" value="{iddokter}"> 
						<input type="hidden" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_sedang_edit" value="" >	
						<input type="hidden" id="waktu_req" value="{waktu_req}" >	
						<input type="hidden" id="tujuan_req" value="{tujuan_req}" >	
						<input type="hidden" id="dokter_req" value="{dokter_req}" >	
						<input type="hidden" id="bb_req" value="{bb_req}" >	
						<input type="hidden" id="tb_req" value="{tb_req}" >	
						<input type="hidden" id="luas_req" value="{luas_req}" >	
						<input type="hidden" id="diagnosa_req" value="{diagnosa_req}" >	
						<input type="hidden" id="menyusui_req" value="{menyusui_req}" >	
						<input type="hidden" id="gangguan_req" value="{gangguan_req}" >	
						<input type="hidden" id="puasa_req" value="{puasa_req}" >	
						<input type="hidden" id="alergi_req" value="{alergi_req}" >	
						<input type="hidden" id="alergi_detail_req" value="{alergi_detail_req}" >	
						<input type="hidden" id="prioritas_req" value="{prioritas_req}" >	
						<input type="hidden" id="pulang_req" value="{pulang_req}" >	
						<input type="hidden" id="catatan_req" value="{catatan_req}" >	
						<input type="hidden" id="st_ranap" value="<?=($asalrujukan=='3'?1:0)?>" >		
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input  type="text" disabled class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggalcreated" placeholder="HH/BB/TTTT" name="tanggalcreated" value="<?= $tanggalcreated ?>" required>
										<label for="tanggalcreated">Tanggal <i class="text-muted">Created Date</i></label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" disabled  class="time-datepicker form-control " id="waktucreated" name="waktucreated" value="<?= $waktucreated ?>" required>
										<label for="waktupendaftaran">Waktu <i class="text-muted">Time</i></label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 col-xs-12" hidden>
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-12">
							<?if($st_lihat_eresep=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_ina} <?=strtoupper(GetAsalRujukanKwitansi($asalrujukan))?></h4>
								<h5 class="push-5 text-center">{judul_eng}</h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					
					<div class="row">
					
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> E-Resep Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info E-Resep <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_eresep=='1'){?>
										<button class="btn btn-primary btn_validate_close" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
										
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-success" onclick="close_assesmen()" type="button"><i class="fa fa-send"></i> Simpan & Kirim</button>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpoliklinik_resep/tindakan/<?=$pendaftaran_id?>/erm_e_resep/input_eresep" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-4 ">
									<label for="example-input-normal">{waktu_ina}</label>
										<div class="input-group date">
											<input  type="text" class="js-datepicker form-control auto_blur_tanggal" data-date-format="dd/mm/yyyy" id="tanggalpermintaan" placeholder="HH/BB/TTTT" name="tanggalpermintaan" value="<?= $tanggalpermintaan ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-3 ">
										<label for="example-input-normal"><i class="text-muted">{waktu_eng}</i></label>
										<div class="input-group">
											<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tanggal" id="waktupermintaan" value="<?= $waktupermintaan ?>" required>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
									<div class="col-md-5 ">
										<label for="example-input-normal">{tujuan_ina} / <i class="text-muted">{tujuan_eng}</i></label>
										<select id="tujuan_farmasi_id" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<?foreach($list_tujuan_eresep as $row){?>
										<option value="<?=$row->id?>" <?=($tujuan_farmasi_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama_tujuan?></option>
										<?}?>
										</select>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="iddokter_resep">{dokter_ina} / <i class="text-muted">{dokter_eng}</i></label>
										<select  id="iddokter_resep" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($iddokter_resep == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach($list_ppa as $row){?>
											<option value="<?=$row->id?>" <?=($iddokter_resep == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										
									</div>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-3 ">
									<label for="example-input-normal">{bb_ina} / <i class="text-muted">{bb_eng}</i></label>
										<div class="input-group date">
											<input  type="text" class="form-control auto_blur number" id="berat_badan" value="<?= $berat_badan ?>" required>
											<span class="input-group-addon">KG</span>
										</div>
									</div>
									<div class="col-md-3 ">
										<label for="example-input-normal">{tb_ina} / <i class="text-muted">{tb_eng}</i></label>
										<div class="input-group">
											<input  type="text" class="form-control auto_blur number" id="tinggi_badan" value="<?= $tinggi_badan ?>" required>
											<span class="input-group-addon">CM</span>
										</div>
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">{luas_ina} / <i class="text-muted">{luas_eng}</i></label>
										<input  type="text" class="form-control auto_blur number" id="luas_permukaan" value="<?= $luas_permukaan ?>" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="diagnosa">{diagnosa_ina} / <i class="text-muted">{diagnosa_eng}</i></label>
										<input  type="text" class="form-control auto_blur" id="diagnosa" value="<?= $diagnosa ?>" required>
										
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-4 ">
										<label for="example-input-normal">{menyusui_ina} /<br> <i class="text-muted">{menyusui_eng}</i></label>
										<select  id="menyusui" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($menyusui == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(107) as $row){?>
											<option value="<?=$row->id?>" <?=($menyusui == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($menyusui == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-4 ">
										<label for="example-input-normal">{gangguan_ina} /<br> <i class="text-muted">{gangguan_eng}</i></label>
										<select  id="gangguan_ginjal" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($gangguan_ginjal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(101) as $row){?>
											<option value="<?=$row->id?>" <?=($gangguan_ginjal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($gangguan_ginjal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-4 ">
										<label for="example-input-normal">{puasa_ina} /<br> <i class="text-muted">{puasa_eng}</i></label>
										<select  id="st_puasa" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($st_puasa == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(102) as $row){?>
											<option value="<?=$row->id?>" <?=($st_puasa == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_puasa == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-4 ">
										<label for="diagnosa">{alergi_ina} /<br> <i class="text-muted">{alergi_eng}</i></label>
										<select  id="st_alergi" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($st_alergi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(58) as $row){?>
											<option value="<?=$row->id?>" <?=($st_alergi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_alergi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										
									</div>
									<div class="col-md-8 ">
										<label for="diagnosa">{alergi_detail_ina} /<br> <i class="text-muted">{alergi_detail_eng}</i></label>
										<div class="input-group">
											<input  type="text" class="form-control auto_blur" id="alergi" value="<?= $alergi ?>" required>
											<span class="input-group-btn">
												<button class="btn btn-danger" type="button" onclick="lihat_his_alergi_pasien(<?=$idpasien?>)"><i class="fa fa-info"></i></button>
											</span>
										</div>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="diagnosa">{prioritas_ina} / <i class="text-muted">{prioritas_eng}</i></label>
										<select  id="prioritas_cito" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($prioritas_cito == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(103) as $row){?>
											<option value="<?=$row->id?>" <?=($prioritas_cito == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($prioritas_cito == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">{pulang_ina} / <i class="text-muted">{pulang_eng}</i></label>
										<select  id="resep_pulang" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($resep_pulang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(104) as $row){?>
											<option value="<?=$row->id?>" <?=($resep_pulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($resep_pulang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
								<div class="col-md-6 ">
									
									<div class="col-md-12 ">
										<label for="diagnosa">{catatan_ina} / <i class="text-muted">{catatan_eng}</i></label>
											<input  type="text" class="form-control auto_blur " id="catatan_resep" value="<?= $catatan_resep ?>" required>
									</div>
								</div>
								
							</div>
							<?$tab_resep=1;?>
							<div class="form-group">
								<div class="col-md-4 ">
									<div class="block">
										<ul class="nav nav-tabs" data-toggle="tabs">
											<li id="div_1" class="<?=($tab_resep=='1'?'active':'')?>">
												<a href="#tab_resep_1"  onclick="set_form_tab(1)">Non Racikan</a>
											</li>
											<li  id="div_2" class="<?=($tab_resep=='2'?'active':'')?>">
												<a href="#tab_resep_2"   onclick="set_form_tab(2)">Racikan </a>
											</li>
											<li  id="div_3" class="<?=($tab_resep=='3'?'active':'')?>">
												<a href="#tab_resep_3" onclick="set_form_tab(3)">Paket</a>
											</li>
											<li  id="div_4" class="<?=($tab_resep=='4'?'active':'')?>">
												<a href="#tab_resep_4"  onclick="set_form_tab(4)">Template</a>
											</li>
											
										</ul>
										<div class="block-content tab-content" style="background-color:#f5f5f5">
											<div class="tab-pane fade fade-left <?=($tab_resep=='1'?'active in':'')?>" id="tab_resep_1" style="min-height: 400px">
												<div class="form-group">
													<div class="col-md-12 ">
														<label for="idbarang_1">Nama Obat <span id="st_label_cover">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
														<div class="input-group">
															<select  id="idbarang_1" class="js-special-select2" style="width:100%"></select>
															<span class="input-group-btn">
																<button class="btn btn-info" type="button" onclick="show_modal_obat(1)"><i class="fa fa-search"></i></button>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="jumlah_1">Jumlah</label>
														<input  type="text" class="form-control  number auto_input" id="jumlah_1" value="" required>
														<input  type="hidden" readonly  id="idtipe_1" value="" required>
														<input  type="hidden" readonly id="st_cover_1" value="" required>
														<input  type="hidden" readonly id="stok" value="">
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label class="text-primary">Aturan Pakai :</label>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="interval_3">Frekuensi / Interval</label>
														<div class="input-group">
															<select  id="interval_1" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
																<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																<?}?>
																
															</select>
															<span class="input-group-btn">
																<button class="btn btn-success" title="refresh" type="button" onclick="refresh_interval()"><i class="fa fa-refresh"></i></button>
																<a href="{base_url}minterval/create" target="_blank" title="Tambah Interval" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></a>
															</span>
														</div>
														
													</div>
												</div>
												
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="dosis_1">Dosis Satu kali minum</label>
														<input  type="text" class="form-control  number auto_input" id="dosis_1" value="" required>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="rute_1">Rute Pemberian</label>
														<select  id="rute_1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="">Pilih Opsi</option>
															<?foreach(list_variable_ref(74) as $row){?>
															<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
															<?}?>
															
														</select>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12">
															<label for="aturan_tambahan_1">Aturan tambahan</label>
															<input  type="text" class="form-control auto_input" id="aturan_tambahan_1" value="" required>
														</div>
													
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="iter_1">ITER</label>
														<?=(opsi_iter($max_iter,$jml_iter,'iter_1','opsi_change'))?>
													</div>
												</div>
												<div class="form-group" style="margin-top:-10px">
													<div class="pull-right">
														<div class="col-md-12 ">
															<button class="btn btn-info" onclick="show_riwayat_obat(1)" type="button"><i class="fa fa-list"></i> Riwayat</button>
															<button class="btn btn-danger" onclick="clear_1()" type="button"><i class="fa fa-refresh"></i> Clear</button>
															<button class="btn btn-success" onclick="add_1()" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane fade fade-left <?=($tab_resep=='2'?'active in':'')?>" id="tab_resep_2" style="min-height: 400px;">
											<!-- RACIKANFORM -->
												<div class="form-group" style="margin-top:-10px" hidden>
													<div class="col-md-12 ">
														<label for="racikan_id">ID RACIKAN</label>
														<input  type="text" readonly class="form-control auto_input" id="racikan_id" value="{racikan_id}" required>
														<input  type="text" readonly class="form-control auto_input" id="status_racikan" value="{status_racikan}" required>
													</div>
												</div>
												<div id="div_new_racikan">
													<div class="form-group">
														<div class="col-md-12 ">
															<button class="btn btn-primary btn-block btn-sm"  onclick="create_racikan()" type="button"><i class="si si-folder"></i> Buat Racikan Baru</button>
														</div>
													</div>
												</div>
												<div id="div_input_racikan">
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label for="nama_racik_2">Nama Racik</label>
															<input  type="text" class="form-control auto_input auto_blur_racikan" id="nama_racik_2" value="{nama_racikan}" required>
														</div>
														
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-4 ">
															<label for="jumlah_racik_2">Jumlah Racik</label>
															<input  type="text" class="form-control auto_input number auto_blur_racikan" id="jumlah_racik_2" value="{jumlah_racikan}" required>
														</div>
														<div class="col-md-8 ">
															<label for="diagnosa">Jenis Racik</label>
															<select  id="jenis_racik_2" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(list_variable_ref(105) as $row){?>
																<option value="<?=$row->id?>" <?=($jenis_racikan==$row->id?'selected':'')?> <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																<?}?>
																
															</select>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-6 ">
															<label for="interval_2">Frekuensi / Interval</label>
															<select  id="interval_2" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
																<option value="<?=$row->id?>" <?=($interval_racikan==$row->id?'selected':'')?> <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																<?}?>
																
															</select>
														</div>
														<div class="col-md-6 ">
															<label for="rute_2">Rute Pemberian</label>
															<select  id="rute_2" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(list_variable_ref(74) as $row){?>
																<option value="<?=$row->id?>"  <?=($rute_racikan==$row->id?'selected':'')?> <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																<?}?>
																
															</select>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-8 ">
															<label for="aturan_tambahan_2">Aturan tambahan</label>
															<input  type="text" class="form-control auto_input auto_blur_racikan" id="aturan_tambahan_2" value="{aturan_tambahan_racikan}" required>
														</div>
														<div class="col-md-4 ">
															<label for="iter_1">ITER</label>
															<?=(opsi_iter($max_iter,$jml_iter,'iter_2','opsi_iter'))?>
														</div>
													</div>
													<hr>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label for="idbarang_2">Nama Obat <span id="st_label_cover_2" style="display:none">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
															<div class="input-group">
																<select id="idbarang_2" class="js-special-select2" style="width:100%"></select>
																<span class="input-group-btn">
																	<button class="btn btn-info" type="button" onclick="show_modal_obat(2)"><i class="fa fa-search"></i></button>
																</span>
															</div>
															<input  type="hidden" readonly  id="idtipe_2" value="" required>
															<input  type="hidden" readonly id="st_cover_2" value="" required>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label class="text-primary">Dosis Sediaan Per Satuan Racikan</label>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-3 ">
															<label for="jumlah_2">Jumlah</label>
															<input  type="text" class="form-control  decimal-2 auto_input" id="jumlah_2" value="" required>
															
														</div>
														<div class="col-md-6 ">
															<label for="p1">P1 / P2</label>
															<div class="input-group">
																<input class="form-control decimal-2 auto_input" type="text" id="p1" value="1" name="p1" placeholder="P1" required="">
																<span class="input-group-addon">/</span>
																<input class="form-control decimal-2 auto_input" type="text" id="p2" value="1" name="p2" placeholder="P2" required="">
															</div>
														</div>
														<div class="col-md-3 ">
															<label for="dosis_2">Dosis</label>
															<input  type="text" class="form-control  number auto_input" id="dosis_2" value="" required>
															<input  type="hidden" class="form-control  number auto_input" id="dosis_master" value="" required>
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<button class="btn btn-success btn-block btn-sm" title="Tambahkan" onclick="add_2()" type="button"><i class="fa fa-level-down"></i> ENTER</button>
														</div>
													</div>
													
													<div class="form-group" style="margin-top:-10px">
														<div class="col-md-12 ">
															<label class="h5 text-primary">DAFTAR OBAT</label>
															<table class="table table-condensed table-bordered" id="tabel_obat_tmp" style="background-color:#fff">
																<thead>
																	<tr>
																		<th class="text-center" style="width: 5%;">NO</th>
																		<th class="text-center" style="width: 30%;">NAMA</th>
																		<th class="text-center" style="width: 10%;">QTY</th>
																		<th class="text-center" style="width: 5%;">P1/P2</th>
																		<th class="text-center" style="width: 10%;">DOSIS</th>
																		<th class="text-center" style="width: 10%;">T-QTY</th>
																		<th class="text-center" style="width: 5%;">AKSI</th>
																	</tr>
																</thead>
																<tbody>
																	
																</tbody>
															</table>
															
														</div>
													</div>
													<div class="form-group" style="margin-top:-10px">
														<div class="pull-right">
															<div class="col-md-12 ">
																<button class="btn btn-info" id="btn_hapus_assesmen" onclick="show_riwayat_obat(1)" type="button"><i class="fa fa-list"></i> Riwayat</button>
																<button class="btn btn-danger" onclick="batalkan_racikan()" type="button"><i class="fa fa-trash"></i> Batalkan</button>
																<button class="btn btn-success"  onclick="simpan_racikan_final()" type="button"><i class="fa fa-save"></i> Tambahkan</button>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane fade fade-left <?=($tab_resep=='3'?'active in':'')?>" id="tab_resep_3" style="min-height: 400px;">
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="nama_paket">Nama Paket</label>
														<div class="input-group">
															<input  type="text" class="form-control  auto_input" id="nama_paket" value="" required>
															<span class="input-group-btn">
																<button class="btn btn-info new_paket" type="button" onclick="create_paket()"><i class="fa fa-plus"></i> Create Paket</button>
																<button class="btn btn-danger input_paket" type="button" onclick="batal_paket()"><i class="fa fa-trash"></i> Batalkan</button>
															</span>
														</div>
														<input  type="hidden" readonly class="form-control  auto_input" id="paket_id" value="" required>
														<input  type="hidden" readonly class="form-control  auto_input" id="status_paket" value="" required>
													</div>
												</div>
												<div class="new_paket">
													<table class="table table-condensed table-bordered" id="tabel_paket" style="background-color:#fff">
															<thead>
																<tr>
																	<th class="text-center" style="width: 3%;">NO</th>
																	<th class="text-center" style="width: 80%;">PAKET</th>
																	<th class="text-center" style="width: 15%;">AKSI</th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
												</div>
												<div class="input_paket">
														<div class="form-group">
															<div class="col-md-12 ">
																<label for="idbarang_3">Nama Obat <span id="st_label_cover_3">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
																<div class="input-group">
																	<select id="idbarang_3" class="js-special-select2" style="width:100%"></select>
																	<span class="input-group-btn">
																		<button class="btn btn-info" type="button" onclick="show_modal_obat(3)"><i class="fa fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="jumlah_3">Jumlah</label>
																<input  type="text" class="form-control  number auto_input" id="jumlah_3" value="" required>
																<input  type="hidden" readonly  id="idtipe_3" value="" required>
																<input  type="hidden" readonly id="st_cover_3" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label class="text-primary">Aturan Pakai :</label>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="interval_3">Frekuensi / Interval</label>
																<div class="input-group">
																	<select  id="interval_3" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																		<option value="">Pilih Opsi</option>
																		<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
																		<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																		<?}?>
																		
																	</select>
																	<span class="input-group-btn">
																		<button class="btn btn-success" type="button" onclick="refresh_interval_paket()"><i class="fa fa-refresh"></i></button>
																		<a href="{base_url}minterval/create" target="_blank" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></a>
																	</span>
																</div>
																
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="dosis_3">Dosis Satu kali minum</label>
																<input  type="text" class="form-control  number auto_input" id="dosis_3" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="rute_3">Rute Pemberian</label>
																<select  id="rute_3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																	<option value="">Pilih Opsi</option>
																	<?foreach(list_variable_ref(74) as $row){?>
																	<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																	<?}?>
																	
																</select>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
																<div class="col-md-12">
																	<label for="aturan_tambahan_1">Aturan tambahan</label>
																	<input  type="text" class="form-control auto_input" id="aturan_tambahan_3" value="" required>
																</div>
															
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="iter_3">ITER</label>
																<?=(opsi_iter($max_iter,$jml_iter,'iter_3','opsi_change'))?>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="pull-right">
																<div class="col-md-12 ">
																	<button class="btn btn-danger" onclick="clear_3()" type="button"><i class="fa fa-refresh"></i> Clear</button>
																	<button class="btn btn-success" onclick="add_3()" type="button"><i class="fa fa-plus"></i> Tambahkan Ke Paket</button>
																</div>
															</div>
														</div>
													</div>
											</div>
											<div class="tab-pane fade fade-left <?=($tab_resep=='4'?'active in':'')?>" id="tab_resep_4" style="min-height: 400px;">
												<div class="form-group" style="margin-top:-10px">
													<div class="col-md-12 ">
														<label for="nama_template">Nama Template</label>
														<div class="input-group">
															<input  type="text" class="form-control  auto_input" id="nama_template" value="" required>
															<span class="input-group-btn">
																<button class="btn btn-info new_template" type="button" onclick="create_template()"><i class="fa fa-plus"></i> Create Template</button>
																<button class="btn btn-danger input_template" type="button" onclick="batal_template()"><i class="fa fa-trash"></i> Batalkan</button>
															</span>
														</div>
														<input  type="hidden" readonly class="form-control  auto_input" id="template_id" value="" required>
														<input  type="hidden" readonly class="form-control  auto_input" id="status_template" value="" required>
													</div>
												</div>
												<div class="new_template">
													<table class="table table-condensed table-bordered" id="tabel_template" style="background-color:#fff">
															<thead>
																<tr>
																	<th class="text-center" style="width: 3%;">NO</th>
																	<th class="text-center" style="width: 80%;">TEMPLATE</th>
																	<th class="text-center" style="width: 15%;">AKSI</th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
												</div>
												<div class="input_template">
														<div class="form-group">
															<div class="col-md-12 ">
																<label for="idbarang_4">Nama Obat <span id="st_label_cover_4">&nbsp;&nbsp;<?=(status_cover('1'))?></span></label> 
																<div class="input-group">
																	<select id="idbarang_4" class="js-special-select2" style="width:100%"></select>
																	<span class="input-group-btn">
																		<button class="btn btn-info" type="button" onclick="show_modal_obat(3)"><i class="fa fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="jumlah_4">Jumlah</label>
																<input  type="text" class="form-control  number auto_input" id="jumlah_4" value="" required>
																<input  type="hidden" readonly  id="idtipe_4" value="" required>
																<input  type="hidden" readonly id="st_cover_4" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label class="text-primary">Aturan Pakai :</label>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="interval_4">Frekuensi / Interval</label>
																<select  id="interval_4" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																	<option value="">Pilih Opsi</option>
																	<?foreach(get_all('minterval',array('status'=>1)) as $row){?>
																	<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																	<?}?>
																	
																</select>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="dosis_4">Dosis Satu kali minum</label>
																<input  type="text" class="form-control  number auto_input" id="dosis_4" value="" required>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="rute_4">Rute Pemberian</label>
																<select  id="rute_4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																	<option value="">Pilih Opsi</option>
																	<?foreach(list_variable_ref(74) as $row){?>
																	<option value="<?=$row->id?>" <?=($row->st_default=='1'?'selected':'')?>><?=$row->nama?></option>
																	<?}?>
																	
																</select>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
																<div class="col-md-12">
																	<label for="aturan_tambahan_1">Aturan tambahan</label>
																	<input  type="text" class="form-control auto_input" id="aturan_tambahan_4" value="" required>
																</div>
															
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																<label for="iter_4">ITER</label>
																<?=(opsi_iter($max_iter,$jml_iter,'iter_4','opsi_change'))?>
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="col-md-12 ">
																
															</div>
														</div>
														<div class="form-group" style="margin-top:-10px">
															<div class="pull-right">
																<div class="col-md-12 ">
																	<button class="btn btn-danger" onclick="clear_4()" type="button"><i class="fa fa-refresh"></i> Clear</button>
																	<button class="btn btn-success" onclick="add_4()" type="button"><i class="fa fa-plus"></i> Tambahkan Ke Template</button>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>
									</div>
									
								</div>
								<div class="col-md-8">
									<div class="block">
										<ul class="nav nav-tabs" data-toggle="tabs">
											<li id="div_detail" class="active">
												<a href="#tab_detail" id="nama_tab_detail">ORDER DETAIL</a>
											</li>
											
										</ul>
										<div class="block-content tab-content" style="background-color:#f5f5f5">
											<div class="tab-pane fade fade-left active in" id="tab_detail" style="min-height: 400px">
												<div class="div_tab_4">
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-primary"><strong>OBAT TEMPLATE</strong></label>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div class="table-responsive">
																<table class="table table-bordered" id="tabel_obat_template" style="background-color:#fff">
																	<thead>
																		<tr>
																			<th class="text-center" style="width: 5%;">NO</th>
																			<th class="text-center" style="width: 30%;">NAMA</th>
																			<th class="text-center" style="width: 10%;">DOSIS</th>
																			<th class="text-center" style="width: 5%;">QTY</th>
																			<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
																			<th class="text-center" style="width: 10%;">RUTE PEMBERIAN</th>
																			<th class="text-center" style="width: 10%;">ATURAN TAMBAHAN</th>
																			<th class="text-center" style="width: 10%;">ITER</th>
																			<th class="text-center" style="width: 5%;">AKSI</th>
																		</tr>
																	</thead>
																	<tbody>
																		
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													
												</div>
												<div class="div_tab_3">
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-primary"><strong>OBAT PAKET</strong></label>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div class="table-responsive">
																<table class="table table-bordered" id="tabel_obat_paket" style="background-color:#fff">
																	<thead>
																		<tr>
																			<th class="text-center" style="width: 5%;">NO</th>
																			<th class="text-center" style="width: 30%;">NAMA</th>
																			<th class="text-center" style="width: 10%;">DOSIS</th>
																			<th class="text-center" style="width: 5%;">QTY</th>
																			<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
																			<th class="text-center" style="width: 10%;">RUTE PEMBERIAN</th>
																			<th class="text-center" style="width: 10%;">ATURAN TAMBAHAN</th>
																			<th class="text-center" style="width: 10%;">ITER</th>
																			<th class="text-center" style="width: 5%;">AKSI</th>
																		</tr>
																	</thead>
																	<tbody>
																		
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													
												</div>
												<div class="div_tab_1">
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-primary"><strong>RESEP NON RACIKAN</strong></label>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div class="table-responsive">
															<table class="table table-bordered" id="tabel_obat" style="background-color:#fff">
																<thead>
																	<tr>
																		<th class="text-center" style="width: 5%;">NO</th>
																		<th class="text-center" style="width: 30%;">NAMA</th>
																		<th class="text-center" style="width: 10%;">DOSIS</th>
																		<th class="text-center" style="width: 5%;">QTY</th>
																		<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
																		<th class="text-center" style="width: 10%;">RUTE PEMBERIAN</th>
																		<th class="text-center" style="width: 10%;">ATURAN TAMBAHAN</th>
																		<th class="text-center" style="width: 10%;">ITER</th>
																		<th class="text-center" style="width: 5%;">AKSI</th>
																	</tr>
																</thead>
																<tbody>
																	
																</tbody>
															</table>
															
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<label class="h5 text-danger"><strong>RESEP RACIKAN</strong>&nbsp;&nbsp;&nbsp; </label><button class="btn btn-xs btn-primary" type="button" onclick="list_index_obat_racikan()"><i class="fa fa-refresh"></i> Refresh</button>
															
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12 ">
															<div id="div_tabel_racikan">
																
															</div>
														</div>
													</div>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								
							</div>
							
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">ORDER SAYA</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_my_order" placeholder="No Pendaftaran" name="notransaksi_my_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_my_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_my_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_my_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_my_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_my_order" name="idpoli_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_my_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_my_order" name="iddokter_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_my_order()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_my_order">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="12%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">Permintaan</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemberi Resep</th>
													<th width="9%">Diagnosa</th>
													<th width="10%">Tujuan Farmasi / Prioritas</th>
													<th width="10%">Status Pemeriksaan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_3">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row" >
							<div class="form-group">
								<div class="col-md-12">
									<div class="pull-right push-10-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_eresep=='1'){?>
											<button class="btn btn-primary btn-xs btn_validate_close" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
											
										<?}?>
										<?}?>
										<button class="btn btn-warning btn-xs" type="button" onclick="tampilkan_filter()" title="Tampilkan / Hide Filter"><i class="fa fa-eye"></i> </button>
									</div>
								</div>
							</div>
							<div id="div_filter_history" hidden>
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">ORDER SAYA</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_his_order" placeholder="No Pendaftaran" name="notransaksi_his_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_his_order" name="idpoli_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_his_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_his_order" name="iddokter_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_history_order()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_history_order">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="12%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">Permintaan</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemberi Resep</th>
													<th width="9%">Diagnosa</th>
													<th width="10%">Tujuan Farmasi / Prioritas</th>
													<th width="10%">Status Pemeriksaan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal fade in" id="modal_informasi_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Informasi</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom:10px" >
								<div class="col-md-6">
									<label for="idkategori">Nama</label>
									<input disabled type="text" class="form-control" id="info_nama" value="" required="" aria-required="true">
									
								</div>
								<div class="col-md-6">
									<label for="idkategori">Nama Generik</label>
									<select disabled  id="info_nama_generik"  name="nama_generik" class="js-select2 form-control js_92" style="width: 100%;">
									<option value="" >Pilih Opsi</option>	
										<?foreach(list_variable_ref(92) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
									<label for="idkategori">Dosis</label>
									<input disabled type="text" class="form-control number"   id="info_dosis" placeholder="Dosis" value="{dosis}" required="" aria-required="true">
								</div>
								<div class="col-md-3">
									<label for="idkategori">Merk</label>
										<select disabled  id="info_merk" class="js-select2 form-control js_93" style="width: 100%;">
											<option value="" >Pilih Opsi</option>
											<?foreach(list_variable_ref(93) as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>
								</div>
								<div class="col-md-2">
									<label for="idkategori">High Alert</label>
										<select disabled  id="info_high_alert" class="js-select2 form-control " style="width: 100%;">
											<option value="">Pilih Opsi</option>
											<option value="1" >YA</option>
											<option value="0" >TIDAK</option>
										</select>
										
								</div>
								<div class="col-md-4">
									<label for="idkategori">Nama Industri</label>
									<select disabled  id="info_nama_industri" class="js-select2 form-control js_97" style="width: 100%;">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(97) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-6">
									<label class="control-label" for="info_komposisi">Komposisi</label>
									<textarea disabled class="form-control" id="info_komposisi" placeholder="Komposisi"  required="" aria-required="true"></textarea>
								</div>
								<div class="col-md-6">
									<label class="control-label" for="alamat">Indikasi</label>
									<textarea disabled class="form-control" id="info_indikasi" placeholder="Indikasi" required="" aria-required="true"></textarea>
								</div>
								
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
									<label for="idkategori">Bentuk Sediaan</label>
									<select disabled   id="info_bentuk_sediaan" class="js-select2 form-control js_94" style="width: 100%;">
										<option value="" >Pilih Opsi</option>
										<?foreach(list_variable_ref(94) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>
										<?}?>
									</select>
										
								</div>
								<div class="col-md-3" >
									<label for="idkategori">Jenis Penggunaan</label>
									<select disabled   id="info_jenis_penggunaan" class="js-select2 form-control js_95" style="width: 100%;">
										<option value="" >TIDAK TERSEDIA</option>
										<?foreach(list_variable_ref(95) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>
										<?}?>
										</select>
										
									
								</div>
								<div class="col-md-3">
								
									<label for="idkategori">Golongan Obat</label>
									<select disabled  id="info_gol_obat"class="js-select2 form-control js_98" style="width: 100%;">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(98) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-3">
									<label for="idkategori">Satuan Besar</label>
									<input disabled type="text" class="form-control" id="info_satuan" value="" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
							</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idtipe_filter">Tipe Barang</label>
								<div class="col-md-8">
									<select id="idtipe_filter" class="form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(get_all('mdata_tipebarang') as $r){?>
										<option value="<?=$r->id?>" <?=(3==$r->id?'selected':'')?>><?=$r->nama_tipe?></option>
										<?}?>
									</select>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="merk_filter">Jenis Penggunaan</label>
								<div class="col-md-8">
									<select  id="jenis_penggunaan_filter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach(list_variable_ref(95) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="merk_filter">Merk</label>
								<div class="col-md-8">
									<select  id="merk_filter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach(list_variable_ref(93) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							
							
							
						</div>
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="bentuk_sediaan_filter">Merk</label>
								<div class="col-md-9">
									<select  id="bentuk_sediaan_filter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach(list_variable_ref(94) as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-9">
									<select id="idkat_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-3 control-label" for="tipeid">Nama Barang</label>
								<div class="col-md-9">
									<input class="form-control" type="text" id="nama_barang_filter" name="nama_barang_filter" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang" onclick="loadBarang()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-bordered" id="filter_tabel_obat" style="background-color:#fff">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Merk</th>
								<th>Bentuk Sediaan</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>Harga</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_riwayat_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-bordered" id="tabel_riwayat_obat" style="background-color:#fff">
						<thead>
							<tr>
								<th class="text-center" style="width: 5%;">NO</th>
								<th class="text-center" style="width: 30%;">Waktu Permintaan</th>
								<th class="text-center" style="width: 30%;">Dokter</th>
								<th class="text-center" style="width: 30%;">Nama Obat</th>
								<th class="text-center" style="width: 10%;">Dosis</th>
								<th class="text-center" style="width: 5%;">Qty</th>
								<th class="text-center" style="width: 10%;">FREKUENSI / INTERVAL</th>
								<th class="text-center" style="width: 10%;">RUTE PEMBERIAN</th>
								<th class="text-center" style="width: 10%;">ATURAN TAMBAHAN</th>
								<th class="text-center" style="width: 5%;">AKSI</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var class_obat;
var st_tampil=false;
var jml_row_racikan=0;
$(document).ready(function() {
	$(".btn_close_left").click(); 
	clear_1();
	$(".div_tab_3").hide();
	$(".div_tab_4").hide();
	disabel_edit();
	set_input_racikan();
	$(".number").number(true,0,'.',',');
	$(".decimal-2").number(true,2,'.',',');
	// $(".decimal-2").number(true,2);
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		list_index_obat();
		list_index_racikan_tampung();
		list_index_obat_racikan();
	}else{
		list_history_order();
		
	}
	load_awal_assesmen=false;
	$('.js-special-select2').select2({
		minimumInputLength: 2,
		ajax: {
            url: '{site_url}Tpoliklinik_resep/get_obat/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
                tujuan_farmasi_id: $("#tujuan_farmasi_id").val(),
                idtipe_poli: $("#idtipe_poli").val(),
                idkelompokpasien: $("#idkelompokpasien").val(),
                idpoliklinik: $("#idpoliklinik").val(),
                idrekanan: $("#idrekanan").val(),
				
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: '['+item.kode+']'+' '+item.nama,
                            id: item.idbarang,
							idtipe : item.idtipe,
							satuan : item.nama_satuan,
							stok : item.stok,
							harga : (item.harga),
							bentuk_sediaan : item.bentuk_sediaan,
							cover : item.cover,
                        }
                    })
                };
            }
        },
		templateResult: formatOption,
		escapeMarkup: function (markup) {
			return markup;
		}
	});
	
});
function refresh_interval(){
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/refresh_interval/',
		dataType: "json",
		success: function(data) {
			$('#interval_1').empty();
			$('#interval_1').append(data.opsi);
		}
	});

}
function refresh_interval_paket(){
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/refresh_interval/',
		dataType: "json",
		success: function(data) {
			$('#interval_3').empty();
			$('#interval_3').append(data.opsi);
		}
	});

}
function set_input_racikan(){
	if ($("#racikan_id").val()==''){
		$("#div_new_racikan").show();
		$("#div_input_racikan").hide();
	}else{
		$("#div_new_racikan").hide();
		$("#div_input_racikan").show();
	}
}
function set_form_tab($form){
	$(".div_tab_4").hide();
	$(".div_tab_3").hide();
	$(".div_tab_1").hide();
	if ($form=='1'){
		$("#nama_tab_detail").html('ORDER DETAIL');
		set_cover();
		$(".div_tab_1").show();
	}else if ($form=='2'){
		$("#nama_tab_detail").html('PAKET DETAIL');
		set_cover_2();
		$(".div_tab_1").show();
	}else if ($form=='3'){
		$("#nama_tab_detail").html('PAKET DETAIL');
		get_info_paket();
		set_cover_3();
		$(".div_tab_3").show();
	}else if ($form=='4'){
		$("#nama_tab_detail").html('TEMPLATE DETAIL');
		get_info_template();
		set_cover_4();
		$(".div_tab_4").show();
	}
	
}

function gete_head_racikan(){
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_head_racikan/',
		dataType: "json",
		type: 'POST',
		data: {
			racikan_id:racikan_id,
			
		},
		success: function(data) {
			
		}
	});
}
$('#tabel_obat tbody').on('click', '.cls_racikan_update', function() {
	$(this).select();
	$(this).focus();
});

$('#tabel_obat tbody').on('blur', '.cls_racikan_update', function() {
	update_record_obat($(this));
	
});

$('#tabel_obat tbody').on('change', '.opsi_change_nr', function() {
	update_record_obat($(this));
	
});
$('#tabel_obat_paket tbody').on('click', '.cls_racikan_update', function() {
	$(this).select();
	$(this).focus();
});
$('#tabel_obat_paket tbody').on('blur', '.cls_racikan_update', function() {
	update_record_obat_paket($(this));
	
});
$('#tabel_obat_paket tbody').on('change', '.opsi_change_paket', function() {
	update_record_obat_paket($(this));
	
});
function update_record_obat(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let dosis=tabel.closest('tr').find(".dosis_nr").val();
	let jumlah=tabel.closest('tr').find(".jumlah_nr").val();
	let aturan_tambahan=tabel.closest('tr').find(".aturan_tambahan_nr").val();
	let interval=tabel.closest('tr').find(".interval").val();
	let iter=tabel.closest('tr').find(".iter").val();
	let rute=tabel.closest('tr').find(".rute").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				dosis : dosis,
				jumlah : jumlah,
				aturan_tambahan : aturan_tambahan,
				interval : interval,
				iter : iter,
				rute : rute,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
	console.log(iter);
}

function tampilkan_filter(){
	if (st_tampil==true){
		$("#div_filter_history").hide();
		st_tampil=false;
	}else{
		$("#div_filter_history").show();
		st_tampil=true;	
	}
}
function show_info_obat(idtipe,idbarang){
	$('.js-select2').select2({
	   dropdownParent: $('#modal_informasi_obat')
	});
	$("#cover-spin").show();
	$("#modal_informasi_obat").modal('show');
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/info_obat/',
		dataType: "json",
		type: 'POST',
		data: {
			idtipe:idtipe,
			idbarang:idbarang,
		},
		success: function(data) {
			$("#info_nama").val(data.nama);
			$("#info_nama_generik").val(data.nama_generik).trigger('change.select2');
			$("#info_dosis").val(data.dosis);
			$("#info_merk").val(data.merk).trigger('change.select2');
			$("#info_high_alert").val(data.high_alert).trigger('change.select2');
			$("#info_nama_industri").val(data.nama_industri).trigger('change.select2');
			$("#info_komposisi").val(data.komposisi);
			$("#info_indikasi").val(data.indikasi);
			$("#info_bentuk_sediaan").val(data.bentuk_sediaan).trigger('change.select2');
			$("#info_jenis_penggunaan").val(data.jenis_penggunaan).trigger('change.select2');
			$("#info_gol_obat").val(data.gol_obat).trigger('change.select2');
			$("#info_satuan").val(data.nama_satuan);
			$("#cover-spin").hide();
		}
	});
}
function show_riwayat_obat(jenis){
	// $('.js-select2').select2({
	   // dropdownParent: $('#modal_informasi_obat')
	// });\
	let iddokter='';
	if (jenis=='2'){
		iddokter=$("#iddokter_resep").val();
	}
	let idpasien=$("#idpasien").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
	$("#cover-spin").show();
	$("#modal_riwayat_obat").modal('show');
	$('#tabel_riwayat_obat').DataTable().destroy();	
	$('#tabel_riwayat_obat').DataTable({
		ordering: false,
		autoWidth: false,
		searching: true,
		serverSide: true,
		"processing": false,
		"order": [],
		// paging: false,
		// scrollCollapse: false,
		// scrollY: '300px',
		ajax: { 
			url: '{site_url}Tpoliklinik_resep/riwayat_obat', 
			type: "POST",
			dataType: 'json',
			data : {
					idpasien:idpasien,
					idkelompokpasien:idkelompokpasien,
					iddokter:iddokter,
				   }
		},
		columnDefs: [
			 {  className: "text-right", targets:[0] },
			 {  className: "text-center", targets:[0,1,2,4,5,6,7,8,9] },
			 { "width": "3%", "targets": [0] },
			 { "width": "5%", "targets": [4,5] },
			 { "width": "10%", "targets": [1,2,6,7,8,9] },
			 { "width": "25%", "targets": [3] },
			 // { "width": "25%", "targets": [1] }
		],
		
		"drawCallback": function( settings ) {
			 $("#cover-spin").hide();
		 }  
	});
	
}
function show_modal_obat(versi){
	document.getElementById("modal_obat").style.zIndex = "1201";
	$('#merk_filter,#idtipe_filter,#idkat_filter,#jenis_penggunaan_filter,#bentuk_sediaan_filter').select2({
	   dropdownParent: $('#modal_obat')
	});
	$("#modal_obat").modal('show');
	if (versi=='1'){
		class_obat='selectObat';
	}else if(versi=='2'){
		class_obat='selectObatRacikan';
	
	}else if(versi=='3'){
		class_obat='selectObatPaket';
	}else if(versi=='4'){
		class_obat='selectObatTemplate';
	}
	
	$("#nama_barang_filter").focus();
	if ($("#nama_barang_filter").val()!=''){
		loadBarang();
	}
}
$(document).on("click", ".selectObat", function(event) {
	clear_1();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_1").val(idtipe);
	$("#st_cover_1").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_1").append(newOption).trigger('change');
	

});
$(document).on("click", ".selectObatPaket", function(event) {
	clear_3();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_3").val(idtipe);
	$("#st_cover_3").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_3").append(newOption).trigger('change');
});
$(document).on("click", ".selectObatTemplate", function(event) {
	clear_4();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_4").val(idtipe);
	$("#st_cover_4").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_4").append(newOption).trigger('change');
});
$(document).on("click", ".selectObatRacikan", function(event) {
	// clear_1();
	$("#st_browse").val("1");
	// alert($(this).data('idtipe'));
	let st_cover = ($(this).data('st_cover'));
	let idobat = ($(this).data('idobat'));
	// alert(idobat);
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_2").val(idtipe);
	$("#st_cover_2").val(st_cover);
	let nama = ($(this).data('nama'));
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_2").append(newOption).trigger('change');
	

});
function loadBarang() {
	
	var idkelompokpasien = $("#idkelompokpasien").val();
	var idunit = $("#tujuan_farmasi_id").val();
	var idtipe = $("#idtipe_filter").val();
	var idkategori = $("#idkat_filter").val();
	var nama_barang = $("#nama_barang_filter").val();
	
	
	$('#filter_tabel_obat').DataTable().destroy();	
		$('#filter_tabel_obat').DataTable({
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/get_obat_filter', 
				type: "POST" ,
				dataType: 'json',
				data : {
						cls: class_obat,
						merk: $("#merk_filter").val(),
						jenis_penggunaan: $("#jenis_penggunaan_filter").val(),
						bentuk_sediaan: $("#bentuk_sediaan_filter").val(),
						idunit: idunit,
						idtipe: idtipe,
						idkategori: idkategori,
						nama_barang: nama_barang,
						idkelompokpasien: idkelompokpasien,
						idtipe_poli: $("#idtipe_poli").val(),
						idpoliklinik: $("#idpoliklinik").val(),
						idrekanan: $("#idrekanan").val(),
					   }
			},
			columnDefs: [{
					"width": "5%",
					"targets": [0],
					"orderable": true
				},
				{
					"width": "10%",
					"targets": [1, 3, 4,5,6,7],
					"orderable": true
				},
				
				{
					"width": "35%",
					"targets": [2],
					"orderable": true
				},
				{  className: "text-right", targets:[0,7] },
				{  className: "text-center", targets:[1,3,4,5,6] },
			],
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
}
$("#st_cover_1").change(function() {
	
});
function set_cover(){
	if ($("#st_cover_1").val()=='1'){
		$("#st_label_cover").show();
	}else{
		$("#st_label_cover").hide();
	}
}
function set_cover_2(){
	if ($("#st_cover_2").val()=='1'){
		$("#st_label_cover_2").show();
	}else{
		$("#st_label_cover_2").hide();
	}
}

function set_cover_3(){
	if ($("#st_cover_3").val()=='1'){
		$("#st_label_cover_3").show();
	}else{
		$("#st_label_cover_3").hide();
	}
}

function get_obat_detail_1(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	let stok;
	data = $("#idbarang_1").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
			stok=data.stok;
		}else{
			idtipe_barang=$("#idtipe_1").val();
			st_cover=$("#st_cover_1").val();
			stok=$("#stok").val();
		}
	}
	$('#idtipe_1').val(idtipe_barang);
	$('#st_cover_1').val(st_cover);
	$('#stok').val(stok);
	set_cover();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		clear_1();
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_1").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_1").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_1").val(data.dosis);
					// alert($("#jumlah_1").val());
				
				// if (parseFloat($("#jumlah_1").val())==0 && $("#jumlah_1")=='1' && $("#jumlah_1")=='' ){
					$("#jumlah_1").val('1');
				// }
				
				$("#jumlah_1").focus().select();
				
				
			}
		});
	} else {

	}
}
function get_obat_detail_3(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_3").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_3").val();
			st_cover=$("#st_cover_3").val();
		}
	}
	$('#idtipe_3').val(idtipe_barang);
	$('#st_cover_3').val(st_cover);
	set_cover();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		clear_3();
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_3").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_3").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_3").val(data.dosis);
				
				if (parseFloat($("#jumlah_3").val())==0 || $("#jumlah_3")=='' ){
					$("#jumlah_3").val('1');
				}
				
				$("#jumlah_3").focus().select();
				
				
			}
		});
	} else {

	}
}
function list_index_obat_racikan(){
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/list_index_obat_racikan/',
		dataType: "json",
		type: 'POST',
		data: {
			assesmen_id:assesmen_id,
			max_iter:max_iter,
			
		},
		success: function(data) {
			if (data!=""){
			// console.log(data);
				jml_row_racikan=1;
			}else{
				
				jml_row_racikan=0;
			}
			$("#cover-spin").hide();
			$("#div_tabel_racikan").html(data);
			$(".tabel_racikan_opsi").select2();
			$(".number").number(true,0,'.',',');
			disabel_edit();
		}
	});

}
function get_obat_detail_2(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_2").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_2").val();
			st_cover=$("#st_cover_2").val();
		}
	}
	$('#idtipe_2').val(idtipe_barang);
	$('#st_cover_2').val(st_cover);
	set_cover_2();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		$("#idbarang_2").val(null).trigger('change.select2');
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_2").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_2").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_2").val(data.dosis);
				$("#dosis_master").val(data.dosis);
				
				if (parseFloat($("#jumlah_2").val())==0 || $("#jumlah_2").val()=='' ){
					$("#p1").val(1);
					$("#p2").val(1);
					$("#jumlah_2").val(1);
				}
				// alert($("#p1").val());
				$("#jumlah_2").focus().select();
				
			}
		});
	} else {

	}
}
$("#idbarang_1").change(function() {
	get_obat_detail_1();
});
$("#idbarang_2").change(function() {
	get_obat_detail_2();
});
$("#idbarang_3").change(function() {
	get_obat_detail_3();
});
$("#idbarang_4").change(function() {
	get_obat_detail_4();
});

function formatOption(option) {
	if (!option.id) {
		return option.text;
	}

	var info = $(option.element).data('info');
	// var bil='<p class="nice-copy"><span class="label label-danger"><i class="fa fa-times"></i> Tidak Bisa Diakses</span></p>';
                                        
	var $option = $(
		'<div class="select2-custom-container">' +
			'<div class="select2-custom-info">' +
				'<span><strong>' + option.text + '</strong></span>' + label_cover(option.cover) + ' ' +label_kosong(option.stok) +
				'<span>Bentuk Sediaan : ' + option.bentuk_sediaan + '</span>' +
				'<span>Satuan : ' + option.satuan + '</span>' +
				'<span>Stok : ' + (option.stok) + '</span>' +
				'<span>Harga : ' + roundNearest100(option.harga) + '</span>' +
				
			'</div>' +
		'</div>'
	);

	return $option;
}
function label_cover(cover){
	$bil='';
	if (cover=='1'){
		$bil='<p class="nice-copy"><span class="label label-warning"><i class="fa fa-exclamation-circle"></i> TIDAK DICOVER</span></p>';
	}
	if (cover=='2'){
		$bil='<p class="nice-copy"><span class="label label-danger"><i class="fa fa-times-circle"></i> TIDAK DAPAT DIPILIH</span></p>';
	}
	return $bil;
}
function label_kosong(stok){
	$bil='';
	if (stok<'1'){
		$bil='<p class="nice-copy"><span class="label label-danger"><i class="fa fa-exclamation-circle"></i> STOK OBAT KOSONG</span></p>';
	}
	
	return $bil;
}
function roundNearest100(num) {
   return formatNumber(Math.ceil(num / 100) * 100);
}
function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function create_assesmen(){
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe_poli=$("#idtipe_poli").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let asalrujukan=$("#asalrujukan").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let iddokter=$("#iddokter").val();
	let st_ranap=$("#st_ranap").val();
	
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/create_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					asalrujukan:asalrujukan,
					tglpendaftaran:tglpendaftaran,
					idpasien:idpasien,
					iddokter:iddokter,
					idtipe_poli:idtipe_poli,
					idpoliklinik:idpoliklinik,
					st_ranap:st_ranap,
					berat_badan:<?=($header_tinggi_badan?$header_tinggi_badan:'0')?>,
					tinggi_badan:<?=($header_berat_badan?$header_berat_badan:'0')?>,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_racikan(){
	
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let jml_iter=$("#jml_iter").val();
	
	let template='Racikan Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/create_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
					iter:jml_iter,
				   },
			success: function(data) {
				$("#cover-spin").hide();
				if (data){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created Racikan'});
					$("#racikan_id").val(data.racikan_id);
					$("#status_racikan").val(data.status_racikan);
					$("#nama_racik_2").val(data.nama_racikan);
					$("#jumlah_racik_2").val(data.jumlah_racikan);
					$("#jenis_racik_2").val(data.jenis_racikan).trigger('change.select2');
					$("#interval_2").val(data.interval_racikan).trigger('change.select2');
					$("#rute_2").val(data.rute_racikan).trigger('change.select2');
					$("#aturan_tambahan_2").val(data.aturan_tambahan_racikan).trigger('change.select2');
					$("#iter_2").val(data.iter_racikan).trigger('change.select2');
					clear_2();
					set_input_racikan();
				}else{
					$.toaster({priority : 'error', title : 'Gagal!', message : ' Create Racikan'});
				}
				
			}
		});
	});

}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batal_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function hapus_assesmen(assesmen_id){
	// let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Notifikasi Pembatalan Order ?",
		text : "Apakah Anda Yakin akan Membatalkan permintaan ini ?. Jika dilakukan Pembatalan makan permintaan anda tidak akan terkirim kepada Farmasi Tujuan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_assesmen', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form_input :input").prop("disabled", true);
		 // $(".btn_rencana").removeAttr('disabled');
		 // $(".btn_ttd").removeAttr('disabled');
	}
}
function simpan_assesmen(){
	if (load_awal_assesmen==false){
		if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		
		let tanggalpermintaan=$("#tanggalpermintaan").val();
		let waktupermintaan=$("#waktupermintaan").val();
		
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/save_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						tanggalpermintaan:tanggalpermintaan,
						waktupermintaan:waktupermintaan,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						tujuan_farmasi_id : $("#tujuan_farmasi_id").val(),
						iddokter_resep : $("#iddokter_resep").val(),
						berat_badan : $("#berat_badan").val(),
						tinggi_badan : $("#tinggi_badan").val(),
						luas_permukaan : $("#luas_permukaan").val(),
						diagnosa : $("#diagnosa").val(),
						menyusui : $("#menyusui").val(),
						gangguan_ginjal : $("#gangguan_ginjal").val(),
						st_puasa : $("#st_puasa").val(),
						st_alergi : $("#st_alergi").val(),
						alergi : $("#alergi").val(),
						prioritas_cito : $("#prioritas_cito").val(),
						resep_pulang : $("#resep_pulang").val(),
						catatan_resep : $("#catatan_resep").val(),

						
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan E-Resep.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
}
function simpan_racikan_final(){
	$("#cover-spin").show();
	$("#status_racikan").val(2);
	simpan_heaad_racikan();
}
function simpan_heaad_racikan(){
	if (load_awal_assesmen==false){
		if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/simpan_heaad_racikan', 
				dataType: "JSON",
				method: "POST",
				data : {
						racikan_id : $("#racikan_id").val(),
						nama_racikan : $("#nama_racik_2").val(),
						jumlah_racikan : $("#jumlah_racik_2").val(),
						jenis_racikan : $("#jenis_racik_2").val(),
						interval_racikan : $("#interval_2").val(),
						rute_racikan : $("#rute_2").val(),
						aturan_tambahan_racikan : $("#aturan_tambahan_2").val(),
						iter_racikan : $("#iter_2").val(),
						status_racikan : $("#status_racikan").val(),

					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Racikan E-Resep.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_racikan=='1'){
							$('#tabel_obat_tmp').DataTable().ajax.reload( null, false );
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft Racikan'});
						}else{
								$("#cover-spin").show();
								location.reload();			
							// if (data.status_assemen=='2'){
								
							// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							// }else{
								// $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								// // alert('sini');
							// }
							
						}
					}
				}
			});
		}
		}
	}
}
function clear_1(){
	$("#idbarang_1").val(null).trigger('change.select2');
	$("#idtipe_1").val('');
	$("#jumlah_1").val('');
	$("#dosis_1").val('');
	$("#aturan_tambahan_1").val('');
	$("#st_cover_1").val('');
	set_cover();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function clear_3(){
	$("#idbarang_3").val(null).trigger('change.select2');
	$("#idtipe_3").val('');
	$("#jumlah_3").val('');
	$("#dosis_3").val('');
	$("#aturan_tambahan_3").val('');
	$("#st_cover_3").val('');
	set_cover_3();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function list_index_obat(){
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_obat').DataTable().destroy();	
		$('#tabel_obat').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '300px',
			"dom": '<"row"<"col-sm-6"l<"dt-filter pull-left push-5-l"><"clearfix">><"col-sm-6"f>>tip',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_obat', 
				type: "POST",
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
						tujuan_id:$("#tujuan_farmasi_id").val(),
					   }
			},
			columnDefs: [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 { "width": "5%", "targets": [0] },
				 { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // { "width": "15%", "targets": [1] },
				 { "width": "25%", "targets": [1] }
			],
			initComplete: function(){
				let optCukai = '<select id="item_iter" class="form-control js-select2"><option value="#">- ITER -</option>';
                for (let i = 0; i < max_iter; i++) {
                optCukai += '<option value="'+i+'">'+i+'</option>';
				}
                
                
                $(".dt-filter").append(optCukai);
                $(".dt-filter").append('<button onclick="terapkan_iter_all()" title="Terapkan ITER" type="button" class="btn btn-success pull-5-l"><i class="si si-login"></i> Terapkan</button>');
                $(".dt-filter").append('<button class="btn btn-info" id="btn_hapus_assesmen" title="Data Order Sebelumnya" onclick="show_riwayat_obat(2)" type="button"><i class="fa fa-list"></i> DATA ORDER SEBELUMNYA</button>');
                $(".dt-filter").append('<button onclick="list_index_obat()" type="button" title="Refresh"  class="btn btn-primary pull-5-l"><i class="fa fa-refresh"></i></button>');
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_nr").select2();
				 disabel_edit();
				 $(".number").number(true,0,'.',',');
				
			 }  
		});
}

function list_index_racikan_tampung(){
	let racikan_id=$("#racikan_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_obat_tmp').DataTable().destroy();	
		$('#tabel_obat_tmp').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			searching: false, 
			paging: false, 
			info: false,
			"order": [],
			paging: true,
			// scrollCollapse: false,
			// scrollY: '300px',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_racikan_tampung', 
				type: "POST" ,
				dataType: 'json',
				data : {
						racikan_id:racikan_id,
						assesmen_id:assesmen_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
					   }
			},
			// columnDefs: [
				 // {  className: "text-right", targets:[0] },
				 // {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 // { "width": "5%", "targets": [0] },
				 // { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // // { "width": "15%", "targets": [1] },
				 // { "width": "25%", "targets": [1] }
			// ],
			
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_nr").select2();
				 disabel_edit();
				 $(".number").number(true,0,'.',',');
				
			 }  
		});
}
function terapkan_iter_all(){
	// alert($("#item_iter").val());return false;
	if ($("#item_iter").val()=="#"){
		swal({
			title: "Gagal",
			text: "Silahkan Isi ITER Dengan Benar",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});
		return false;
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerapakan ITER Ke Inputan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/terapkan_iter_all', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					iter:$("#item_iter").val(),
				   },
			success: function(data) {
				$("#cover-spin").hide();
				$('#tabel_obat').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Terapkan ITER'});
				
			}
		});
	});
	
}
function hapus_obat(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Obat E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat').DataTable().ajax.reload( null, false );
				clear_1();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}

function hapus_racikan_detail(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Obat Racikan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_racikan_detail', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat_tmp').DataTable().ajax.reload( null, false );
				// clear_2();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function hapus_racikan_detail_final(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Obat Racikan E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_racikan_detail', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$("#cover-spin").hide();
				list_index_obat_racikan();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function batalkan_racikan(){
	let racikan_id=$("#racikan_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Membatalkan Racikan Ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batalkan_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					racikan_id:racikan_id,
				   },
			success: function(data) {
				$("#racikan_id").val('');
				set_input_racikan();
				$('#tabel_obat_tmp').DataTable().ajax.reload( null, false );
				// clear_2();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Racikan'});
			}
		});
	});

}
function hapus_racikan_head(id){
	let racikan_id=id;
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Racikan Ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batalkan_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					racikan_id:racikan_id,
				   },
			success: function(data) {
				$("#cover-spin").hide();
				$("#racikan_id").val('');
				set_input_racikan();
				list_index_obat_racikan();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Racikan'});
			}
		});
	});

}
function cek_duplicate_barang(){
	let status_find=false;
	$('#tabel_obat tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_1").val()==idtipe && $("#idbarang_1").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}

function cek_duplicate_racikan(){
	let status_find=false;
	$('#tabel_obat_tmp tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_2").val()==idtipe && $("#idbarang_2").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function add_1(){
	let cover=$("#st_cover_1").val();
	let stok=$("#stok").val();
	if (cek_duplicate_barang()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (stok<1){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Stok Obat Ini Mungkin tidak tersedia!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			if (cover=='1'){//Tidak DIcoer
				swal({
				  title: 'Anda Yakin?',
				  text: "Obat Ini Tidak Dicover!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Lanjutkan',
				  cancelButtonText: "Batalkan",
				}).then(function(json_data) {
					add_barang_non_racikan();
				}, function(dismiss) {
				  if (dismiss === 'cancel' || dismiss === 'close') {
					  clear_1();
					  return false;
				  } 
				})

			}else{
				add_barang_non_racikan();
			}
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_1();
			  return false;
		  } 
		})

	}else{
		if (cover=='1'){//Tidak DIcoer
			swal({
			  title: 'Anda Yakin?',
			  text: "Obat Ini Tidak Dicover!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Lanjutkan',
			  cancelButtonText: "Batalkan",
			}).then(function(json_data) {
				add_barang_non_racikan();
			}, function(dismiss) {
			  if (dismiss === 'cancel' || dismiss === 'close') {
				  clear_1();
				  return false;
			  } 
			})

		}else{
			add_barang_non_racikan();
		}
	}
	
	
}
function add_3(){
	let cover=$("#st_cover_3").val();
	if (cek_duplicate_paket()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_paket();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_3();
			  return false;
		  } 
		})

	}else{
		add_barang_paket();
	}
	
}
$("#dosis_2").keyup(function(){
	let dosis_master=$("#dosis_master").val();
	let dosis=$("#dosis_2").val();
	let dosis_p1;
	let dosis_p2;
	let faktor_kali=1;
	if (dosis_master > dosis){
		dosis_p1=dosis/dosis;
		dosis_p2=dosis_master/dosis;
	}else{
		dosis_p1=dosis/dosis_master;
		dosis_p2=dosis_master/dosis_master;
	}
	if (dosis_p2 < 1){
		faktor_kali=1/dosis_p2;
	}
	
	console.log(dosis_p1);
	let p1=parseFloat(dosis_p1*faktor_kali);
	let p2=parseFloat(dosis_p2*faktor_kali);
	$("#jumlah_2").val(Math.ceil(p1));
	$("#p1").val(p1);
	$("#p2").val(p2);
});
// $("#jumlah_2,#p1,#p2").keyup(function(){
	// kalkulasi_dosis_raccikan();		
// });
$("#jumlah_2").keyup(function(){
	let p1=Math.ceil($(this).val())
	let p2=p1/parseFloat($(this).val())
	// console.log(p1 + ' '+);
	$("#p1").val(p1);
	$("#p2").val(p2);
	kalkulasi_dosis_raccikan();		
});

$("#p1,#p2").keyup(function(){
	let p=parseFloat($("#p1").val()) / parseFloat($("#p2").val());
	let jml=Math.ceil(p)
	$("#jumlah_2").val(jml)
	kalkulasi_dosis_raccikan();		
});

function kalkulasi_dosis_raccikan(){
	let jumlah=$("#jumlah_2").val();
	let p=parseFloat($("#p1").val()) / parseFloat($("#p2").val());
	let dosis_master=$("#dosis_master").val();
	let dosis=p * dosis_master;
	$("#dosis_2").val(dosis);
}
function add_barang_non_racikan(){
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let idbarang=$("#idbarang_1").val();
	let idtipe=$("#idtipe_1").val();
	let nama=$("#idbarang_1 option:selected").text();
	let jumlah=$("#jumlah_1").val();
	let interval=$("#interval_1").val();
	let rute=$("#rute_1").val();
	let aturan_tambahan=$("#aturan_tambahan_1").val();
	let iter=$("#iter_1").val();
	let dosis=$("#dosis_1").val();
	let cover=$("#st_cover_1").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id : assesmen_id,
				pendaftaran_id : pendaftaran_id,
				idpasien : idpasien,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				jumlah : jumlah,
				interval : interval,
				rute : rute,
				aturan_tambahan : aturan_tambahan,
				iter : iter,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				$('#tabel_obat').DataTable().ajax.reload( null, false );
				clear_1();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat'});
			}
		}
	});
}

function add_2(){
	if ($("#nama_racik_2").val()==''){
		swal({title: "Gagal!",text: "Tentukan Nama Racikan",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if ($("#jumlah_racik_2").val()=='' || $("#jumlah_racik_2").val()=='0'){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if ($("#jenis_racik_2").val()=='' || $("#jenis_racik_2").val()==null){
		swal({title: "Gagal!",text: "Tentukan Jenis Racik",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	
	let cover=$("#st_cover_2").val();
	if (cek_duplicate_racikan()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_racikan();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_1();
			  return false;
		  } 
		})

	}else{
		add_barang_racikan();
	}
	
}
function add_barang_racikan(){
	let racikan_id=$("#racikan_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let idbarang=$("#idbarang_2").val();
	let idtipe=$("#idtipe_2").val();
	let nama=$("#idbarang_2 option:selected").text();
	let jumlah=$("#jumlah_2").val();
	let dosis_master=$("#dosis_master").val();
	let dosis=$("#dosis_2").val();
	let p1=$("#p1").val();
	let p2=$("#p2").val();
	let cover=$("#st_cover_2").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_obat_racikan', 
		dataType: "JSON",
		method: "POST",
		data : {
				racikan_id : racikan_id,
				assesmen_id : assesmen_id,
				pendaftaran_id : pendaftaran_id,
				idpasien : idpasien,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				dosis_master : dosis_master,
				jumlah : jumlah,
				p1 : p1,
				p2 : p2,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				list_index_racikan_tampung();
				clear_2();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat Racikan'});
			}
		}
	});
}

function clear_2(){
	$("#idbarang_2").val(null).trigger('change.select2');
	$("#idtipe_2").val('');
	$("#jumlah_2").val(1);
	$("#dosis_2").val('');
	$("#dosis_master").val('');
	$("#p1").val(1);
	$("#p2").val(1);
	$("#st_cover_2").val('');
	set_cover_2();
}
function create_awal_template(){
	$("#modal_default_template").modal('show');
	$("#mnyeri_id_2").select2({
		dropdownParent: $("#modal_default_template")
	  });
}
function load_index_informasi(){
	$("#cover-spin").show();
	var assesmen_id=$("#assesmen_id").val();
	var status_assemen=$("#status_assemen").val();
	var ttd_dokter_pelaksana=$("#signature64_ttd_dokter").val();
	var ttd_penerima_info=$("#signature64_ttd_kel").val();
	// alert(id);
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/load_index_informasi_eresep/',
		dataType: "json",
		type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				ttd_dokter_pelaksana:ttd_dokter_pelaksana,
				ttd_penerima_info:ttd_penerima_info,
				
		  },
		success: function(data) {
			$("#tabel_informasi tbody").empty();
			$("#tabel_informasi tbody").append(data.tabel);
			$("#cover-spin").hide();
			disabel_edit();
			$('.auto_blur_tabel').summernote({
				height: 100,
			    codemirror: { // codemirror options
					theme: 'monokai'
				  },	
			  callbacks: {
				onBlur: function(contents, $editable) {
						assesmen_id=$("#assesmen_id").val();
						var tr=$(this).closest('tr');
						var informasi_id=tr.find(".informasi_id").val();
						var isi=$(this).val();
						$.ajax({
							url: '{site_url}Tpoliklinik_resep/update_isi_informasi_eresep/',
							dataType: "json",
							type: 'POST',
							data: {
							informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
							},success: function(data) {

							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
							}
						});
				}
			  }
			});
				
		}
	});
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}

function copy_history_eresep(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/copy_history_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/create_template_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
$(".opsi_change").change(function(){
	// console.log('OPSI CHANTE')
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});
$(".opsi_change_racikan").change(function(){
	// console.log('OPSI CHANTE')
	if ($("#st_edited").val()=='0'){
		simpan_heaad_racikan();
	}
});
$("#hereby").change(function(){
	set_persetujuan();
});
function set_persetujuan(){
	
	let assesmen_id=$("#assesmen_id").val();
	let hereby=$("#hereby").val();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_persetujaun_eresep', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				hereby:hereby,
			
			   },
		success: function(data) {
			$("#ket_permintaan_ina").html(data);
			// // $("#cover-spin").hide();
			
			// $.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
			// location.reload();
		}
	});
}
$(".auto_blur_tanggal").change(function(){
	console.log($("#waktupermintaan").val())
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan E-Resept ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/batal_template_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
 $(document).find('.js-summernote').on('summernote.blur', function() {
   if ($("#st_edited").val()=='0'){
	 
	   
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
  });
$(".auto_blur_racikan").blur(function(){
	// alert('sini');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_heaad_racikan();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
  

$(".auto_blur").blur(function(){
	// alert('sini');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
$("#penerima_info").blur(function(){
	$("#penerima_info_paraf").val($("#penerima_info").val());
	$("#nama_kel_penerima_info").val($("#penerima_info").val());
	
});
$("#nama").blur(function(){
	$("#nama_pemberi_permintaan").html($("#nama").val());
	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	if ($("#waktu_req").val()=='1' && $("#tanggalpermintaan").val()==''){
		swal({title: "Field Harus Diisi",text: "Tanggal Pengajuan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		return false;
	}
	if ($("#tujuan_req").val()=='1' && ($("#tujuan_farmasi_id").val()=='' || $("#tujuan_farmasi_id").val()==null)){
		swal({title: "Field Harus Diisi",text: "Tujuan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#tujuan_farmasi_id").focus().select();
		return false;
	}
	if ($("#dokter_req").val()=='1' && ($("#iddokter_resep").val()=='' || $("#iddokter_resep").val()==null)){
		swal({title: "Field Harus Diisi",text: "Dokter Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#iddokter_resep").focus().select();
		return false;
	}
	if ($("#bb_req").val()=='1' && ($("#berat_badan").val()=='' || $("#berat_badan").val()==0)){
		swal({title: "Field Harus Diisi",text: "Berat Badan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#berat_badan").focus();
		return false;
	}
	if ($("#tb_req").val()=='1' && ($("#tinggi_badan").val()=='' || $("#tinggi_badan").val()==0)){
		swal({title: "Field Harus Diisi",text: "Tinggi Badan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#tinggi_badan").focus();
		return false;
	}
	if ($("#luas_req").val()=='1' && ($("#luas_permukaan").val()=='')){
		swal({title: "Field Harus Diisi",text: "Luas Permukaan Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#luas_permukaan").focus();
		return false;
	}
	if ($("#diagnosa_req").val()=='1' && ($("#diagnosa").val()=='' || $("#diagnosa").val()==0)){
		swal({title: "Field Harus Diisi",text: "Diagnosa Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#diagnosa").focus();
		return false;
	}
	if ($("#menyusui_req").val()=='1' && ($("#menyusui").val()=='' || $("#menyusui").val()==null)){
		swal({title: "Field Harus Diisi",text: "Sedang menyusui Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#menyusui").focus();
		return false;
	}
	if ($("#gangguan_req").val()=='1' && ($("#gangguan_ginjal").val()=='' || $("#gangguan_ginjal").val()==null)){
		swal({title: "Field Harus Diisi",text: "Gangguan Ginjal Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#gangguan_ginjal").focus();
		return false;
	}
	if ($("#puasa_req").val()=='1' && ($("#st_puasa").val()=='' || $("#st_puasa").val()==null)){
		swal({title: "Field Harus Diisi",text: "Pasien Puasa Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#st_puasa").focus();
		return false;
	}
	if ($("#alergi_req").val()=='1' && ($("#st_alergi").val()=='' || $("#st_alergi").val()==null)){
		swal({title: "Field Harus Diisi",text: "Alergi Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#st_alergi").focus();
		return false;
	}
	if ($("#alergi_detail_req").val()=='1' && ($("#alergi").val()=='' || $("#alergi").val()==null)){
		swal({title: "Field Harus Diisi",text: "Alergi Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#alergi").focus();
		return false;
	}
	if ($("#prioritas_req").val()=='1' && ($("#prioritas_cito").val()=='' || $("#prioritas_cito").val()==null)){
		swal({title: "Field Harus Diisi",text: "Prioritas Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#prioritas_cito").focus();
		return false;
	}
	if ($("#pulang_req").val()=='1' && ($("#resep_pulang").val()=='' || $("#resep_pulang").val()==null)){
		swal({title: "Field Harus Diisi",text: "Resep Pulang Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#resep_pulang").focus();
		return false;
	}
	if ($("#catatan_req").val()=='1' && ($("#catatan_resep").val()=='' || $("#catatan_resep").val()==null)){
		swal({title: "Field Harus Diisi",text: "Catata Resep Pulang Pulang Harus diisi",type: "error",timer: 1500,showConfirmButton: false});
		$("#catatan_resep").focus();
		return false;
	}
	
	
	var table =$('#tabel_obat').DataTable()
	// var table = new DataTable('#tabel_obat');
	var jml_row_obat = table.rows().count();
	
	var jumlah_row_all=jml_row_obat + jml_row_racikan
	if (jumlah_row_all=='0'){
		swal({title: "Peringatan",text: "Tidak Ada Obat Yang Diinputkan",type: "error",timer: 1500,showConfirmButton: false});
		return false;
	}
	// alert(jumlah_row_all);return false;
	// alert($("#form_input").valid());
	// if($("#form_input").valid()){
		// //loader
		let nama_tujuan=$("#tujuan_farmasi_id option:selected").text();
		swal({
			title: "Notifikasi",
			text : "Apakah Anda Yakin telah selesai membuat e-resep? Data permintaan Anda akan otomatis terkirim ke "+nama_tujuan+".",
			icon : "question",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// return false;
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			simpan_assesmen();
		});		
	
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}
	$("#modal_savae_template_assesmen").modal('hide');
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}

	
	
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_index_template_eresep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 // $("#index_template_assemen thead").remove();
					 disabel_edit();
				 }  
			});
		
	}
	
	function edit_assesmen(assesmen_id,pendaftaran_id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit E-Resep?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/edit_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan E-Resep.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						// window.location.href = "'{site_url}tpoliklinik_resep/tindakan/"+pendaftaran_id+"/erm_e_resep/input_eresep'";
						window.location.href = "<?php echo site_url('tpoliklinik_resep/tindakan/"+pendaftaran_id+"/erm_e_resep/input_eresep'); ?>";
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let profesi_id_filter=$("#profesi_id_filter").val();
		let st_verifikasi=$("#st_verifikasi").val();
		let st_owned=$("#st_owned").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						// {  className: "text-center", targets:[1,2,3,5,6,7] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "10%", "targets": [1,2,3,5] },
						 // { "width": "15%", "targets": [4] },
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_history_pengkajian_eresep', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							st_owned:st_owned,
							st_verifikasi:st_verifikasi,
							pemberi_info:profesi_id_filter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 $("#index_history_kajian thead").remove();
				 }  
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		// $("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/save_edit_eresep', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
						location.reload();
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		$("#modal_hapus").modal('hide');
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan E-Resep ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/hapus_record_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template E-Resep ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/hapus_record_eresep', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	$(document).on("blur",".tabel_racikan",function(){
		assesmen_id=$("#assesmen_id").val();
		let tr=$(this).closest('tr');
		let nama_racikan=tr.find(".nama_racikan").val();
		let jumlah_racikan=tr.find(".jumlah_racikan").val();
		let jenis_racikan=tr.find(".jenis_racikan").val();
		let interval_racikan=tr.find(".interval_racikan").val();
		let rute_racikan=tr.find(".rute_racikan").val();
		let aturan_tambahan_racikan=tr.find(".aturan_tambahan_racikan").val();
		let iter_racikan=tr.find(".iter_racikan").val();
		let racikan_id=tr.find(".racikan_id").val();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/simpan_heaad_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					racikan_id: racikan_id,
					nama_racikan : nama_racikan,
					jumlah_racikan : jumlah_racikan,
					jenis_racikan : jenis_racikan,
					interval_racikan : interval_racikan,
					rute_racikan : rute_racikan,
					aturan_tambahan_racikan : aturan_tambahan_racikan,
					iter_racikan : iter_racikan,
					status_racikan : 2,


				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Updates'});
				list_index_obat_racikan();
			}
		});
	});
	// $(document).on("blur",".dosis_nrd",function(){
		// assesmen_id=$("#assesmen_id").val();
		// let tr=$(this).closest('tr');
		// let idtrx=tr.find(".cls_idtrx").val();
		// let jumlah_racikan=tr.find(".jumlah_racikan").val();
		// let dosis=tr.find(".dosis").val();
		// let jumlah=tr.find(".jumlah").val();
		// console.log(jumlah);
		// let p1=tr.find(".p1").val();
		// let p2=tr.find(".p2").val();
		// let p=parseFloat(p1) / parseFloat(p2);
		// let jml=Math.ceil(p);
		// let total=jumlah_racikan * jumlah;
		// console.log(jumlah_racikan);
		// tr.find(".total").val(total);
		// tr.find(".jumlah").val(jml);
		// jumlah=jml;
		// $.ajax({
			// url: '{site_url}Tpoliklinik_resep/simpan_detail_racikan', 
			// dataType: "JSON",
			// method: "POST",
			// data : {
					// id: idtrx,
					// jumlah_racikan : jumlah_racikan,
					// dosis : dosis,
					// jumlah : jumlah,
					// p1 : p1,
					// p2 : p2,
				// },
			// success: function(data) {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Updates'});
			// }
		// });
	// });
	$(document).on("blur",".dosis_nrd_jumlah",function(){
		let tr=$(this).closest('tr');
		let jumlah_racikan=tr.find(".jumlah_racikan").val();
		let dosis_master=tr.find(".dosis_master").val();
		let jumlah=tr.find(".jumlah").val();
		let p1=Math.ceil(jumlah);
		let p2=p1/jumlah;
		let p=parseFloat(p1) / parseFloat(p2);
		let total=jumlah_racikan * jumlah;
		let dosis=p * dosis_master;
		
		let tqty=Math.ceil(p*jumlah_racikan);
		let tqty_koma=(p*jumlah_racikan);
		tqty_koma=parseFloat(tqty_koma).toFixed(1);
		tr.find(".dosis").val(dosis);
		tr.find(".p1").val(p1);
		tr.find(".p2").val(p2);
		tr.find(".label_tqty").text(tqty_koma + " ("+tqty+")");
		simpan_detail_racikan(tr);
		
	});
	$(document).on("blur",".dosis_nrd_p1_p2",function(){
		let tr=$(this).closest('tr');
		let jumlah_racikan=tr.find(".jumlah_racikan").val();
		let dosis_master=tr.find(".dosis_master").val();
		let jumlah=tr.find(".jumlah").val();
		let p1=tr.find(".p1").val();
		let p2=tr.find(".p2").val();
		let p=parseFloat(p1) / parseFloat(p2);
		let jml=Math.ceil(p);
		let total=jumlah_racikan * jumlah;
		let dosis=p * dosis_master;
		let tqty=Math.ceil(p*jumlah_racikan);
		let tqty_koma=(p*jumlah_racikan);
		tqty_koma=parseFloat(tqty_koma).toFixed(1);
		tr.find(".dosis").val(dosis);
		tr.find(".jumlah").val(jml);
		tr.find(".label_tqty").text(tqty_koma + " ("+tqty+")");
		simpan_detail_racikan(tr);
		
	});
	$(document).on("blur",".dosis_nrd_dosis",function(){
		let tr=$(this).closest('tr');
		let jumlah_racikan=tr.find(".jumlah_racikan").val();
		let dosis_master=tr.find(".dosis_master").val();
		let dosis=tr.find(".dosis").val();
		let dosis_p1;
		let dosis_p2;
		let faktor_kali=1;
		if (dosis_master > dosis){
			dosis_p1=dosis/dosis;
			dosis_p2=dosis_master/dosis;
		}else{
			dosis_p1=dosis/dosis_master;
			dosis_p2=dosis_master/dosis_master;
		}
		if (dosis_p2 < 1){
			faktor_kali=1/dosis_p2;
		}
		
		let p1=parseFloat(dosis_p1*faktor_kali);
		let p2=parseFloat(dosis_p2*faktor_kali);
		tr.find(".jumlah").val(Math.ceil(p1));
		tr.find(".p1").val(p1);
		tr.find(".p2").val(p2);
		let p=parseFloat(p1) / parseFloat(p2);
		let tqty=Math.ceil(p*jumlah_racikan);
		let tqty_koma=(p*jumlah_racikan);
		tqty_koma=parseFloat(tqty_koma).toFixed(1);
		
		tr.find(".label_tqty").text(tqty_koma + " ("+tqty+")");
		simpan_detail_racikan(tr);
		
	});
	
	function simpan_detail_racikan(tr){
		assesmen_id=$("#assesmen_id").val();
		// let tr=$(this).closest('tr');
		let idtrx=tr.find(".cls_idtrx").val();
		let jumlah_racikan=tr.find(".jumlah_racikan").val();
		let dosis=tr.find(".dosis").val();
		let jumlah=tr.find(".jumlah").val();
		let p1=tr.find(".p1").val();
		let p2=tr.find(".p2").val();
		let p=parseFloat(p1) / parseFloat(p2);
		let jml=Math.ceil(p);
		let total=jumlah_racikan * jumlah;
		// console.log(jumlah_racikan);
		// tr.find(".total").val(total);
		// tr.find(".jumlah").val(jml);
		// jumlah=jml;
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/simpan_detail_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					id: idtrx,
					jumlah_racikan : jumlah_racikan,
					dosis : dosis,
					jumlah : jumlah,
					p1 : p1,
					p2 : p2,
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Updates'});
			}
		});
	}
	$(document).on("change",".tabel_racikan_opsi",function(){
		assesmen_id=$("#assesmen_id").val();
		let tr=$(this).closest('tr');
		let nama_racikan=tr.find(".nama_racikan").val();
		let jumlah_racikan=tr.find(".jumlah_racikan").val();
		let jenis_racikan=tr.find(".jenis_racikan").val();
		let interval_racikan=tr.find(".interval_racikan").val();
		let rute_racikan=tr.find(".rute_racikan").val();
		let aturan_tambahan_racikan=tr.find(".aturan_tambahan_racikan").val();
		let iter_racikan=tr.find(".iter_racikan").val();
		let racikan_id=tr.find(".racikan_id").val();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/simpan_heaad_racikan', 
			dataType: "JSON",
			method: "POST",
			data : {
					racikan_id: racikan_id,
					nama_racikan : nama_racikan,
					jumlah_racikan : jumlah_racikan,
					jenis_racikan : jenis_racikan,
					interval_racikan : interval_racikan,
					rute_racikan : rute_racikan,
					aturan_tambahan_racikan : aturan_tambahan_racikan,
					iter_racikan : iter_racikan,
					status_racikan : 2,


				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Updates'});
			}
		});
		
	});
	
	//PAKET
function get_info_paket(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_info_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			
		},
		success: function(data) {
			$("#paket_id").val(data.paket_id);
			$("#nama_paket").val(data.nama_paket);
			$("#status_paket").val(data.status_paket);
			set_paket();
			$("#cover-spin").hide();
		}
	});
}
function set_paket(){
	if ($("#paket_id").val()==''){
		$(".input_paket").hide();
		$("#nama_paket").removeAttr('disabled');
		$(".new_paket").show();
		list_index_paket();
	}else{
		$("#nama_paket").attr('disabled',true);
		$(".input_paket").show();
		$(".new_paket").hide();
		list_index_obat_paket();
	}
}
function create_paket(){
	if ($("#nama_paket").val()==''){
		swal({
			title: "Informasi!",text: "Nama Paket Harus diisi",type: "error",timer: 1000,showConfirmButton: false
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/create_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			nama_paket : $("#nama_paket").val(),
		},
		success: function(data) {
			$("#paket_id").val(data.paket_id);
			$("#nama_paket").val(data.nama_paket);
			$("#status_paket").val(data.status_paket);
			set_paket();
			$("#cover-spin").hide();
		}
	});
}
function batal_paket(){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			paket_id : $("#paket_id").val(),
		},
		success: function(data) {
			$("#paket_id").val('');
			$(".dt-filter2").html('');
			$("#nama_paket").val('');
			$("#status_paket").val(0);
			set_paket();
			$("#cover-spin").hide();
		}
	});
}
function hapus_paket(paket_id){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_paket/',
		dataType: "json",
		type: 'POST',
		data: {
			paket_id : paket_id,
		},
		success: function(data) {
			$("#status_paket").val('');
			$("#paket_id").val('');
			$(".dt-filter2").html('');
			$("#nama_paket").val('');
			$('#tabel_paket').DataTable().ajax.reload( null, false );
			$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
function update_record_obat_paket(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let dosis=tabel.closest('tr').find(".dosis_paket").val();
	let jumlah=tabel.closest('tr').find(".jumlah_paket").val();
	let aturan_tambahan=tabel.closest('tr').find(".aturan_tambahan_paket").val();
	let interval=tabel.closest('tr').find(".interval").val();
	let iter=tabel.closest('tr').find(".iter").val();
	let rute=tabel.closest('tr').find(".rute").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_obat_paket', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				dosis : dosis,
				jumlah : jumlah,
				aturan_tambahan : aturan_tambahan,
				interval : interval,
				iter : iter,
				rute : rute,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
	console.log(iter);
}
function list_data_paket_obat(id){
	$("#paket_id").val(id);
	$("#status_paket").val(2);
	list_index_obat_paket();
}
function list_index_obat_paket(){
	let status_paket=$("#status_paket").val();
	let paket_id=$("#paket_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_obat_paket').DataTable().destroy();	
		$('#tabel_obat_paket').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			"dom": '<"row"<"col-sm-6"l<"dt-filter2 pull-left push-5-l"><"clearfix">><"col-sm-6"f>>tip',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_obat_paket', 
				type: "POST",
				dataType: 'json',
				data : {
						paket_id:paket_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
					   }
			},
			columnDefs: [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 { "width": "5%", "targets": [0] },
				 { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // { "width": "15%", "targets": [1] },
				 { "width": "25%", "targets": [1] }
			],
			initComplete: function(){
				if (status_paket=='1'){
					$(".dt-filter2").append('<button onclick="close_paket()" type="button" class="btn btn-success pull-5-l"><i class="fa fa-save"></i> Simpan Paket</button>');
					$(".dt-filter2").append('<button onclick="batal_paket()" type="button" class="btn btn-danger pull-5-l"><i class="fa fa-trash"></i></button>');
				}
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_paket").select2();
				 $(".number").number(true,0,'.',',');
			 }  
		});
}
function list_index_paket(){
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_paket').DataTable().destroy();	
		$('#tabel_paket').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_paket', 
				type: "POST",
				dataType: 'json',
				data : {
					   }
			},
			columnDefs: [
				 // {  className: "text-right", targets:[0] },
				 // {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 // { "width": "5%", "targets": [0] },
				 // { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // // { "width": "15%", "targets": [1] },
				 // { "width": "25%", "targets": [1] }
			],
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
}
function hapus_obat_paket(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Paket Obat E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_obat_paket', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
				clear_3();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function cek_duplicate_paket(){
	let status_find=false;
	$('#tabel_obat_paket tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_3").val()==idtipe && $("#idbarang_3").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function add_barang_paket(){
	let paket_id=$("#paket_id").val();
	let idbarang=$("#idbarang_3").val();
	let idtipe=$("#idtipe_3").val();
	let nama=$("#idbarang_3 option:selected").text();
	let jumlah=$("#jumlah_3").val();
	let interval=$("#interval_3").val();
	let rute=$("#rute_3").val();
	let aturan_tambahan=$("#aturan_tambahan_3").val();
	let iter=$("#iter_3").val();
	let dosis=$("#dosis_3").val();
	let cover=$("#st_cover_3").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_barang_paket', 
		dataType: "JSON",
		method: "POST",
		data : {
				paket_id : paket_id,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				jumlah : jumlah,
				interval : interval,
				rute : rute,
				aturan_tambahan : aturan_tambahan,
				iter : iter,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				$('#tabel_obat_paket').DataTable().ajax.reload( null, false );
				clear_3();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat'});
			}
		}
	});
}
function gunakan_paket(paket_id){
	// let paket_id=paket_id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep Dari Paket ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/gunakan_paket', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					paket_id:paket_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Menggunakan Data Obat Paket'});
				location.reload();
			}
		});
	});

}
function terapkan_history_obat(id){
	// let paket_id=paket_id;
	$('#modal_riwayat_obat').modal('hide');
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Menggunakan Obat Dari History ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/terapkan_history_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					id:id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Proses.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				$('#modal_riwayat_obat').modal('show');
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Menggunakan Data History Resep'});
				list_index_obat();
			}
		});
	});

}
function close_paket(){
	
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Paket ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/close_paket', 
				dataType: "JSON",
				method: "POST",
				data : {
						paket_id : $("#paket_id").val(),
						
					},
				success: function(data) {
					$("#cover-spin").show();
					list_index_obat_paket();
					$("#paket_id").val('');
					$("#nama_paket").val('');
					set_paket();
				}
			});
		});		
	
}
function set_cover_4(){
	if ($("#st_cover_4").val()=='1'){
		$("#st_label_cover_4").show();
	}else{
		$("#st_label_cover_4").hide();
	}
}
function get_obat_detail_4(){
	let idtipe_barang;
	let nama_obat;
	let st_cover;
	data = $("#idbarang_4").select2('data')[0];
	console.log(data);
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
			st_cover=data.cover;
		}else{
			idtipe_barang=$("#idtipe_4").val();
			st_cover=$("#st_cover_4").val();
		}
	}
	$('#idtipe_4').val(idtipe_barang);
	$('#st_cover_4').val(st_cover);
	set_cover();
	if (st_cover=='2'){//Tidak Dipilih
		sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		clear_4();
		return false;
	}
	
	$("#st_browse").val("0");
	if ($("#idbarang_4").val() != '') {
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idtipe:idtipe_barang,
				idbarang:$("#idbarang_4").val(),
				tujuan_id:$("#tujuan_farmasi_id").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#dosis_4").val(data.dosis);
				
				if (parseFloat($("#jumlah_4").val())==0 || $("#jumlah_4")=='' ){
					$("#jumlah_4").val('1');
				}
				
				$("#jumlah_4").focus().select();
				
				
			}
		});
	} else {

	}
}
function clear_4(){
	$("#idbarang_4").val(null).trigger('change.select2');
	$("#idtipe_4").val('');
	$("#jumlah_4").val('');
	$("#dosis_4").val('');
	$("#aturan_tambahan_4").val('');
	$("#st_cover_4").val('');
	set_cover_4();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function add_4(){
	let cover=$("#st_cover_4").val();
	if (cek_duplicate_paket()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_paket();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_4();
			  return false;
		  } 
		})

	}else{
		add_barang_paket();
	}
	
}

	//TEMPLATE
function get_info_template(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/get_info_template/',
		dataType: "json",
		type: 'POST',
		data: {
			
		},
		success: function(data) {
			$("#template_id").val(data.template_id);
			$("#nama_template").val(data.nama_template);
			$("#status_template").val(data.status_template);
			set_template();
			$("#cover-spin").hide();
		}
	});
}
function set_template(){
	if ($("#template_id").val()==''){
		$(".input_template").hide();
		$("#nama_template").removeAttr('disabled');
		$(".new_template").show();
		list_index_template();
	}else{
		$("#nama_template").attr('disabled',true);
		$(".input_template").show();
		$(".new_template").hide();
		list_index_obat_template();
	}
}
function create_template(){
	if ($("#nama_template").val()==''){
		swal({
			title: "Informasi!",text: "Nama Template Harus diisi",type: "error",timer: 1000,showConfirmButton: false
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/create_template/',
		dataType: "json",
		type: 'POST',
		data: {
			nama_template : $("#nama_template").val(),
		},
		success: function(data) {
			$("#template_id").val(data.template_id);
			$("#nama_template").val(data.nama_template);
			$("#status_template").val(data.status_template);
			set_template();
			$("#cover-spin").hide();
		}
	});
}
function batal_template(){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_template/',
		dataType: "json",
		type: 'POST',
		data: {
			template_id : $("#template_id").val(),
		},
		success: function(data) {
			$("#template_id").val('');
			$(".dt-filter3").html('');
			$("#nama_template").val('');
			$("#status_template").val(0);
			set_template();
			$("#cover-spin").hide();
		}
	});
}
function hapus_template(template_id){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/batal_template/',
		dataType: "json",
		type: 'POST',
		data: {
			template_id : template_id,
		},
		success: function(data) {
			$("#status_template").val('');
			$("#template_id").val('');
			$(".dt-filter3").html('');
			$("#nama_template").val('');
			$('#tabel_template').DataTable().ajax.reload( null, false );
			$('#tabel_obat_template').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
function update_record_obat_template(tabel){
	let transaksi_id=tabel.closest('tr').find(".cls_idtrx").val();
	let dosis=tabel.closest('tr').find(".dosis_template").val();
	let jumlah=tabel.closest('tr').find(".jumlah_template").val();
	let aturan_tambahan=tabel.closest('tr').find(".aturan_tambahan_template").val();
	let interval=tabel.closest('tr').find(".interval").val();
	let iter=tabel.closest('tr').find(".iter").val();
	let rute=tabel.closest('tr').find(".rute").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/update_record_obat_template', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				dosis : dosis,
				jumlah : jumlah,
				aturan_tambahan : aturan_tambahan,
				interval : interval,
				iter : iter,
				rute : rute,
				
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
	console.log(iter);
}
function list_data_template_obat(id){
	$("#template_id").val(id);
	$("#status_template").val(2);
	list_index_obat_template();
}
function list_index_obat_template(){
	let status_template=$("#status_template").val();
	let template_id=$("#template_id").val();
	let max_iter=$("#max_iter").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_obat_template').DataTable().destroy();	
		$('#tabel_obat_template').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			"dom": '<"row"<"col-sm-6"l<"dt-filter3 pull-left push-5-l"><"clearfix">><"col-sm-6"f>>tip',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_obat_template', 
				type: "POST",
				dataType: 'json',
				data : {
						template_id:template_id,
						max_iter:max_iter,
						idkelompokpasien:idkelompokpasien,
					   }
			},
			columnDefs: [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 { "width": "5%", "targets": [0] },
				 { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // { "width": "15%", "targets": [1] },
				 { "width": "25%", "targets": [1] }
			],
			initComplete: function(){
				if (status_template=='1'){
					$(".dt-filter3").append('<button onclick="close_template()" type="button" class="btn btn-success pull-5-l"><i class="fa fa-save"></i> Simpan Template</button>');
					$(".dt-filter3").append('<button onclick="batal_template()" type="button" class="btn btn-danger pull-5-l"><i class="fa fa-trash"></i></button>');
				}
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change_template").select2();
				 $(".number").number(true,0,'.',',');
			 }  
		});
}
function list_index_template(){
	let idkelompokpasien=$("#idkelompokpasien").val();
		$('#tabel_template').DataTable().destroy();	
		$('#tabel_template').DataTable({
			ordering: false,
			autoWidth: false,
			// searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			paging: false,
			scrollCollapse: false,
			scrollY: '400px',
			ajax: { 
				url: '{site_url}Tpoliklinik_resep/list_index_template', 
				type: "POST",
				dataType: 'json',
				data : {
					   }
			},
			columnDefs: [
				 // {  className: "text-right", targets:[0] },
				 // {  className: "text-center", targets:[2,3,4,5,6,7,8] },
				 // { "width": "5%", "targets": [0] },
				 // { "width": "10%", "targets": [2,3,4,5,6,7,8] },
				 // // { "width": "15%", "targets": [1] },
				 // { "width": "25%", "targets": [1] }
			],
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
}
function hapus_obat_template(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Template Obat E-Resep ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/hapus_obat_template', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				$('#tabel_obat_template').DataTable().ajax.reload( null, false );
				clear_4();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Remove Obat'});
			}
		});
	});

}
function cek_duplicate_template(){
	let status_find=false;
	$('#tabel_obat_template tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		console.log(idtipe);
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe_4").val()==idtipe && $("#idbarang_4").val()==idbarang){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function add_barang_template(){
	let template_id=$("#template_id").val();
	let idbarang=$("#idbarang_4").val();
	let idtipe=$("#idtipe_4").val();
	let nama=$("#idbarang_4 option:selected").text();
	let jumlah=$("#jumlah_4").val();
	let interval=$("#interval_4").val();
	let rute=$("#rute_4").val();
	let aturan_tambahan=$("#aturan_tambahan_4").val();
	let iter=$("#iter_4").val();
	let dosis=$("#dosis_4").val();
	let cover=$("#st_cover_4").val();
	if (idbarang=='' || idbarang==null){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (idtipe==''){
		swal({title: "Gagal!",text: "Tentukan Obat",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	if (jumlah=='0' || jumlah==''){
		swal({title: "Gagal!",text: "Tentukan Jumlah",type: "error",timer: 1000,showConfirmButton: false});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_resep/add_barang_template', 
		dataType: "JSON",
		method: "POST",
		data : {
				template_id : template_id,
				idbarang : idbarang,
				idtipe : idtipe,
				nama : nama,
				dosis : dosis,
				jumlah : jumlah,
				interval : interval,
				rute : rute,
				aturan_tambahan : aturan_tambahan,
				iter : iter,
				cover : cover,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				$('#tabel_obat_template').DataTable().ajax.reload( null, false );
				clear_4();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Add Obat'});
			}
		}
	});
}
function gunakan_template(template_id){
	// let template_id=template_id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat E-Resep Dari Template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_resep/gunakan_template', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					template_id:template_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Menggunakan Data Obat Template'});
				location.reload();
			}
		});
	});

}
function close_template(){
	
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_resep/close_template', 
				dataType: "JSON",
				method: "POST",
				data : {
						template_id : $("#template_id").val(),
						
					},
				success: function(data) {
					$("#cover-spin").show();
					list_index_obat_template();
					$("#template_id").val('');
					$("#nama_template").val('');
					set_template();
				}
			});
		});		
	
}
function set_cover_4(){
	if ($("#st_cover_4").val()=='1'){
		$("#st_label_cover_4").show();
	}else{
		$("#st_label_cover_4").hide();
	}
}
// function get_obat_detail_4(){
	// let idtipe_barang;
	// let nama_obat;
	// let st_cover;
	// data = $("#idbarang_4").select2('data')[0];
	// console.log(data);
	// if (data){
		// if ($("#st_browse").val() == 0) {
			// idtipe_barang=data.idtipe;
			// st_cover=data.cover;
		// }else{
			// idtipe_barang=$("#idtipe_4").val();
			// st_cover=$("#st_cover_4").val();
		// }
	// }
	// $('#idtipe_4').val(idtipe_barang);
	// $('#st_cover_4').val(st_cover);
	// set_cover();
	// if (st_cover=='2'){//Tidak Dipilih
		// sweetAlert("Maaf...", "Data Obat Tidak Dapat Dipilih!", "error");
		// clear_4();
		// return false;
	// }
	
	// $("#st_browse").val("0");
	// if ($("#idbarang_4").val() != '') {
		// $.ajax({
			// url: '{site_url}Tpoliklinik_resep/get_obat_detail/',
			// dataType: "json",
			// type: 'POST',
			// data: {
				// idtipe:idtipe_barang,
				// idbarang:$("#idbarang_4").val(),
				// tujuan_id:$("#tujuan_farmasi_id").val(),
				
			// },
			// success: function(data) {
				// if (data == null) {
					// sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					// return false;
				// }
				// $("#dosis_4").val(data.dosis);
				
				// if (parseFloat($("#jumlah_4").val())==0 || $("#jumlah_4")=='' ){
					// $("#jumlah_4").val('1');
				// }
				
				// $("#jumlah_4").focus().select();
				
				
			// }
		// });
	// } else {

	// }
// }
function clear_4(){
	$("#idbarang_4").val(null).trigger('change.select2');
	$("#idtipe_4").val('');
	$("#jumlah_4").val('');
	$("#dosis_4").val('');
	$("#aturan_tambahan_4").val('');
	$("#st_cover_4").val('');
	set_cover_4();
	// $("#interval_1").val('').trigger('change.select2');
	// $("#rute_1").val('').trigger('change.select2');
}
function add_4(){
	let cover=$("#st_cover_4").val();
	if (cek_duplicate_template()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cover=='1'){//Tidak DIcoer
		swal({
		  title: 'Anda Yakin?',
		  text: "Obat Ini Tidak Dicover!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan',
		  cancelButtonText: "Batalkan",
		}).then(function(json_data) {
			add_barang_template();
		}, function(dismiss) {
		  if (dismiss === 'cancel' || dismiss === 'close') {
			  clear_4();
			  return false;
		  } 
		})

	}else{
		add_barang_template();
	}
	
}
$('#tabel_obat_template tbody').on('click', '.cls_racikan_update', function() {
	$(this).select();
	$(this).focus();
});
$('#tabel_obat_template tbody').on('blur', '.cls_racikan_update', function() {
	update_record_obat_template($(this));
	
});
$('#tabel_obat_template tbody').on('change', '.opsi_change_paket', function() {
	update_record_obat_template($(this));
	
});
function list_my_order(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_my_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_my_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_resep/list_my_order', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_history_order(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_his_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_his_order").val();
	let iddokter=$("#iddokter_his_order").val();
	let idpoli=$("#idpoli_his_order").val();
	let notransaksi=$("#notransaksi_his_order").val();
	let tanggal_input_1=$("#tanggal_input_1_his_order").val();
	let tanggal_input_2=$("#tanggal_input_2_his_order").val();
	$('#index_history_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_history_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_resep/list_history_order', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}

</script>