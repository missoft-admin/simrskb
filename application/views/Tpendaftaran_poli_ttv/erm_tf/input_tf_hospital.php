<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	.input-keterangan{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<style>
	
	#sig_ttd_penerima canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_tf_hospital' ||$menu_kiri=='input_tf_hospital_rj' || $menu_kiri=='terima_tf_hospital'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_tf_hospital'  ||$menu_kiri=='input_tf_hospital_rj'  || $menu_kiri=='terima_tf_hospital' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggalkontak=HumanDateShort($tanggal_kontak);
						$waktukontak=HumanTime($tanggal_kontak);
						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_tf_hospital=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_tf">
		<?if ($st_lihat_tf_hospital=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian  <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'Baru')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >	
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >								
						<div class="col-md-12">
							<?if($st_lihat_tf_hospital=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary"> {judul_header_ina}</h4>
									<h5 class="font-w700 push-5 text-center text-primary"><i> {judul_header_eng}</i></h5>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_tf_hospital=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-disk"></i> SIMPAN</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-save"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_tf/input_tf_hospital" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text"  class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_tf_hospital!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >TUJUAN TRANSFER / RUJUKAN</h5>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="tujuan_rs">Tujuan Rumah Sakit</label>
									<select id="tujuan_rs" name="tujuan_rs" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($tujuan_rs=='0'?'selected':'')?>>-Belum Ditentukan-</option>
										<?foreach($list_rs as $row){?>
											<option value="<?=$row->id?>" <?=($tujuan_rs==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tujuan_detail">Detail</label>
									<input id="tujuan_detail" class="auto_blur form-control"  type="text"  value="{tujuan_detail}"  placeholder="" required>
								</div>
								
								
								
							</div>
							<div class="col-md-6">
								<div class="col-md-5">
									<label for="staff_kontak">Staff Yang Melakukan Kontak </label>
									<select id="staff_kontak" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($staff_kontak==''?'selected':'')?>>-Silahkan Pilih Opsi-</option>
										<?foreach($list_ppa as $row){?>
											<option value="<?=$row->id?>" <?=($staff_kontak==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4">
									<label for="tanggalkontak">Tanggal Kontak</label>
									<div class="input-group date">
										<input id="tanggalkontak"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalkontak ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="waktukontak">&nbsp;</label>
									<div class="input-group">
										<input id="waktukontak" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktukontak ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
						</div>										
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="staff_terima">Nama Staff Yang Menerima Kontak</label>
									<input id="staff_terima" class="auto_blur form-control"  type="text"  value="{staff_terima}"  placeholder="" required>
								</div>
								<div class="col-md-6">
									<label for="notelepon_kontak">No Telepon</label>
									<input id="notelepon_kontak" class="auto_blur form-control"  type="text"  value="{notelepon_kontak}"  placeholder="" required>
								</div>
								
							</div>
							<?
								$tanggalberangkat=HumanDateShort($ambulance_berangkat);
								$waktuberangkat=HumanTime($ambulance_berangkat);
								$tanggaltiba=HumanDateShort($ambulance_tiba);
								$waktutiba=HumanTime($ambulance_tiba);
							?>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="tanggalberangkat">Ambulance Berangkat</label>
									<div class="input-group date">
										<input id="tanggalberangkat"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalberangkat ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="waktuberangkat" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktuberangkat ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="tanggaltiba">Ambulance tiba</label>
									<div class="input-group date">
										<input id="tanggaltiba"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggaltiba ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="waktutiba" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktutiba ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >ALASAN MERUJUK</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="alasan_merujuk">Alasan Merujuk</label>
									<select id="alasan_merujuk" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(292) as $row){?>
										<option value="<?=$row->id?>" <?=($alasan_merujuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="keterangan_merujuk">Keterangan</label>
									<input id="keterangan_merujuk" class="auto_blur form-control"  type="text"  value="{keterangan_merujuk}"  placeholder="" required>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="diagnosa_medis">Diagnosa Medis</label>
									<input id="diagnosa_medis" class="auto_blur form-control"  type="text"  value="{diagnosa_medis}"  placeholder="" required>
								</div>
								<div class="col-md-6">
									<label for="iddokter_merujuk">Dokter Yang Merujuk</label>
									<select id="iddokter_merujuk" name="iddokter_merujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_dokter as $row){?>
										<option value="<?=$row->id?>" <?=($iddokter_merujuk==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >CATATAN KLINIS</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4 ">
								<label class="col-xs-12">Alergi </label>
								<div class="col-md-12">
									<select id="riwayat_alergi" name="riwayat_alergi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_alergi=='0'?'selected':'')?>>Tidak Ada Alergi</option>
										<option value="1" <?=($riwayat_alergi=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_alergi=='2'?'selected':'')?>>Ada Alergi</option>
										
									</select>
								</div>
							</div>
							<div class="col-md-8 ">
								<label class="col-xs-12">Sebutkan Alergi </label>
								<div class="col-md-12">
									<textarea id="alergi_detail" class="form-control auto_blur" name="alergi_detail" rows="1" style="width:100%"><?=$alergi_detail?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="pengobatan">Pengobatan</label>
									<select id="pengobatan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($pengobatan=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($pengobatan=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($pengobatan=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_pengobatan push-10-t">
									<div class="form-group ">
										<div class="col-md-12 ">
											<div class="table-responsive">
												<table class="table table-condensed table-bordered" id="index_pengobatan">
													<thead>
														<tr>
															<th width="5%" class="text-center">No</th>
															<th width="80%" class="text-center">Pengobatan<input  type="hidden" id="pengobatan_detail" value="{pengobatan_detail}"></th>
															<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_pengobatan"></th>
														</tr>
														<tr>
															<th  class="text-center">#<input  type="hidden" id="no_pengobatan_var" value=""></th>
															<th  class="text-left">
																<input class="form-control text-left" type="text" id="nama_pengobatan_var" style="width: 100%;" placeholder="Pengobatan">
															</th>
															<th width="15%" class="text-center">
																<span class="input-group-btn">
																	<button class="btn btn-info btn-sm" onclick="simpan_pengobatan()" id="btn_add_pengobatan" type="button"><i class="fa fa-plus"></i></button>
																	<button class="btn btn-warning  btn-sm" onclick="clear_input_pengobatan()" type="button"><i class="fa fa-refresh"></i></button>
																</span>
															</th>
														</tr>
														
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
									
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="riwayat_penyakit">Riwayat Penyakit </label>
									<select id="riwayat_penyakit" name="riwayat_penyakit" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_penyakit=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($riwayat_penyakit=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_penyakit=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_riwayat push-10-t">
									<div class="form-group ">
										<div class="col-md-12 ">
											<div class="table-responsive">
												<table class="table table-condensed table-bordered"" id="index_riwayat_penyakit">
													<thead>
														<tr>
															<th width="5%" class="text-center">No</th>
															<th width="80%" class="text-center">Riwayat_penyakit<input  type="hidden" id="riwayat_penyakit_detail" value="{riwayat_penyakit_detail}"></th>
															<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_riwayat_penyakit"></th>
														</tr>
														<tr>
															<th  class="text-center">#<input  type="hidden" id="no_riwayat_penyakit_var" value=""></th>
															<th  class="text-left">
																<input class="form-control text-left" type="text" id="nama_riwayat_penyakit_var" style="width: 100%;" placeholder="Penyakit">
															</th>
															<th width="15%" class="text-center">
																<span class="input-group-btn">
																	<button class="btn btn-info btn-sm" onclick="simpan_riwayat_penyakit()" id="btn_add_riwayat_penyakit" type="button"><i class="fa fa-plus"></i></button>
																	<button class="btn btn-warning  btn-sm" onclick="clear_input_riwayat_penyakit()" type="button"><i class="fa fa-refresh"></i></button>
																</span>
															</th>
														</tr>
														
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-6">
									<label for="tindakan">Tindakan Yang Telah Dilakukan</label>
									<textarea id="tindakan" class="auto_blur form-control" name="tindakan" rows="3" style="width:100%"><?=$tindakan?></textarea>
								</div>
								<div class="col-md-6">
									<label for="intake_oral">Intake Oral Terakhir</label>
									<textarea id="intake_oral" class="auto_blur form-control" name="intake_oral" rows="3" style="width:100%"><?=$intake_oral?></textarea>
								</div>
							</div>
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >KONDISI PASIEN SAAT INI</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6">
								<div class="col-md-6 ">
									<label >GCS</label>
									<div class="input-group">
										<input id="gcs_eye" class="auto_blur form-control decimal "  type="text"   value="{gcs_eye}" placeholder="Eye" required>
										<span class="input-group-addon"><?=$satuan_gcs_eye?></span>
										<input id="gcs_voice" class="auto_blur form-control decimal "  type="text"  value="{gcs_voice}"  placeholder="Voice" required>
										<span class="input-group-addon"><?=$satuan_gcs_voice?></span>
										<input id="gcs_motion" class="auto_blur form-control decimal "  type="text"  value="{gcs_motion}"   placeholder="Motion" required>
										<span class="input-group-addon"><?=$satuan_gcs_motion?></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label >Pupil</label>
									<div class="input-group">
										<input id="pupil_1" class="auto_blur form-control decimal "  type="text"   value="{pupil_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_pupil?> / </span>
										<input id="pupil_2" class="auto_blur form-control decimal "  type="text"  value="{pupil_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_pupil?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-8">
									<label for="td_sistole">Tekanan darah</label>
									<div class="input-group">
										<input id="td_sistole" class="auto_blur form-control decimal "  type="text"   value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input id="td_diastole" class="auto_blur form-control decimal "  type="text"  value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="suhu">Suhu Tubuh</label>
									<div class="input-group">
										<input id="suhu" class="auto_blur form-control decimal "  type="text"  name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6 ">
								<div class="col-md-8">
									<label >Refleksi Cahaya</label>
									<div class="input-group">
										<input id="reflex_cahaya_1" class="auto_blur form-control decimal "  type="text"   value="{reflex_cahaya_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_cahaya?> / </span>
										<input id="reflex_cahaya_2" class="auto_blur form-control decimal "  type="text"  value="{reflex_cahaya_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_cahaya?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-4">
									<label for="nadi">Frekuensi Nadi</label>
									<div class="input-group">
										<input id="nadi" class="auto_blur form-control decimal "   type="text"  name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="nafas">Frekuensi Nafas</label>
									<div class="input-group">
										<input id="nafas" class="auto_blur form-control decimal "   type="text"  name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="spo2">SpO2</label>
									<div class="input-group">
										<input id="spo2" class="auto_blur form-control decimal "  type="text"  name="spo2" value="{spo2}"  placeholder="SpO2" required>
										<span class="input-group-addon"><?=$satuan_spo2?></span>
									</div>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="peralatan_medis">Peralatan Medis </label>
									<select id="peralatan_medis" name="peralatan_medis" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($peralatan_medis=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($peralatan_medis=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($peralatan_medis=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_peralatan push-10-t">
									
									<div class="table-responsive">
										<table class="table table-condensed table-bordered" id="index_peralatan_medis">
											<thead>
												<tr>
													<th width="5%" class="text-center">No<input  type="hidden" id="peralatan_medis_detail_id" value="{peralatan_medis_detail_id}"></th>
													<th width="80%" class="text-center">Peralatan Medis<input  type="hidden" id="peralatan_medis_detail" value="{peralatan_medis_detail}"></th>
													<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_peralatan_medis"></th>
												</tr>
												<tr>
													<th  class="text-center">#<input  type="hidden" id="no_peralatan_medis_var" value=""></th>
													<th  class="text-left">
														<select id="nama_peralatan_medis_var" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="">Pilih Opsi</option>
															<?foreach(list_variable_ref(293) as $row){?>
															<option value="<?=$row->id?>" ><?=$row->nama?></option>
															<?}?>
														</select>
														
													</th>
													<th width="15%" class="text-center">
														<span class="input-group-btn">
															<button class="btn btn-info btn-sm" onclick="simpan_peralatan_medis()" id="btn_add_peralatan_medis" type="button"><i class="fa fa-plus"></i></button>
															<button class="btn btn-warning  btn-sm" onclick="clear_input_peralatan_medis()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</th>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12">
									<label for="perawatan_lanjutan">Perawatan Pasien Lanjutan Yang Dibutuhkan</label>
									<textarea id="perawatan_lanjutan" class="auto_blur form-control" name="perawatan_lanjutan" rows="5" style="width:100%"><?=$perawatan_lanjutan?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							
							<?
								$tanggalserah_terima=HumanDateShort($waktu_serah_terima);
								$waktuserah_terima=HumanTime($waktu_serah_terima);
								
							?>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="kejadian_klinis">Kejadian Klinis Selama Dilakukan Transfer</label>
									<select id="kejadian_klinis" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(294) as $row){?>
										<option value="<?=$row->id?>" <?=($kejadian_klinis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tanggaltiba">Waktu Serah Terima Pasien</label>
									<div class="input-group date">
										<input id="tanggalserah_terima"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalserah_terima ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="waktuserah_terima" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktuserah_terima ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="staff_perujuk">Staff Yang Melakukan Rujukan </label>
									<select id="staff_perujuk" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($staff_perujuk==''?'selected':'')?>>-Pilih Staff-</option>
										<?foreach($list_ppa as $row){?>
											<option value="<?=$row->id?>" <?=($staff_perujuk==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="staff_penerima">Staff Yang Meneria Pasien</label>
									<input id="staff_penerima" class="auto_blur form-control"  type="text"  value="{staff_penerima}"  placeholder="" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							
							<div class="col-md-6">
								
							</div>
							<div class="col-md-6">
								<div class="col-md-12 text-center">
									<table style="width:100%">
										<tr>
											<td style="width:50%"></td>
											<td style="width:50%">
											<?if ($ttd_penerima==''){?>
												<button type="button" class="btn btn-success btn-xs btn_ttd text-center" onclick="modal_ttd_penerima()"> <i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}else{?>
												<div class="img-container fx-img-rotate-r ">
													<img class="" style="width:120px;height:120px; text-align: center;" src="{ttd_penerima}" alt="" title="">
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<button type="button" class="btn btn-default btn-xs" onclick="modal_ttd_penerima()" ><i class="fa fa-pencil"></i> </button>
																<button type="button" class="btn btn-default btn-danger  btn-xs" onclick="hapus_ttd_penerima()" ><i class="fa fa-times"></i> </button>
															</div>
														</div>
													</div>
													
												</div>
											<?}?>
											</td>
										</tr>
									</table>
								</div>
								
							</div>
						</div>		
					<?}?>
					</div>
					
						<!--BATS AKRHI -->
					
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<?if ($st_ranap=='1'){?>
													<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}else{?>
													<option value="<?=$pendaftaran_id?>" selected>Poliklinik Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<input type="hidden" id="pendaftaran_id_ranap_tmp" value="">
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_penerima" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="staff_penerima_var" placeholder="Nama" value="">
									<label for="staff_penerima_var">Nama Staff Yang Menerima Pasien</label>
								</div>
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_penerima" ></div>
										<textarea id="signed_ttd_penerima" name="signed_ttd_penerima" style="display: none"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<input type="hidden" readonly id="nama_field_ttd" value="">
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_penerima()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig_ttd_penerima = $('#sig_ttd_penerima').signature({syncField: '#signed_ttd_penerima', syncFormat: 'PNG'});

$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		set_riwayat_penyakit();
		generate_tabel();
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian();
	}
});
function hapus_ttd_penerima(){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/hapus_ttd_tf_hospital/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
$('#clear').click(function(e) {
	e.preventDefault();
	sig_ttd_penerima.signature('clear');
	$("#signed_ttd_penerima").val('');
});
function simpan_ttd_penerima(){
	let assesmen_id=$("#assesmen_id").val();
	let ttd_penerima=$("#signed_ttd_penerima").val();
	let staff_penerima=$("#staff_penerima_var").val();
	// ttd_view_petugas
	// $("#ttd_view_petugas").attr("src", ttd_petugas);
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}tpendaftaran_ranap_erm/simpan_ttd_penerima_tf_hospital/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			assesmen_id:assesmen_id,
			ttd_penerima:ttd_penerima,
			staff_penerima:staff_penerima,
	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Succes!', message : ' Tanda Tangan'});
		  // $("#cover-spin").hide();
			location.reload();
		}
	});
}
function modal_ttd_penerima(){
	$("#staff_penerima_var").val($("#staff_penerima").val());
	$("#modal_ttd_penerima").modal('show');
}
function generate_array_penyakit(){
	let nama_pengobatan_var='';
	
	let nama_riwayat_penyakit_var='';
	$('#index_pengobatan tbody tr').each(function() {
		if (nama_pengobatan_var!=''){nama_pengobatan_var +="#"+$(this).find('td:eq(1)').text()}else{nama_pengobatan_var +=$(this).find('td:eq(1)').text()};
		
	});
	$("#pengobatan_detail").val(nama_pengobatan_var);

	
	$('#index_riwayat_penyakit tbody tr').each(function() {
		if (nama_riwayat_penyakit_var!=''){nama_riwayat_penyakit_var +="#"+$(this).find('td:eq(1)').text()}else{nama_riwayat_penyakit_var +=$(this).find('td:eq(1)').text()};
		
	});
	$("#riwayat_penyakit_detail").val(nama_riwayat_penyakit_var);
	
	let nama_peralatan_medis_var='';
	let nama_peralatan_medis_var_id='';
	$('#index_peralatan_medis tbody tr').each(function() {
		if (nama_peralatan_medis_var!=''){nama_peralatan_medis_var +="#"+$(this).find('td:eq(1)').text()}else{nama_peralatan_medis_var +=$(this).find('td:eq(1)').text()};
		if (nama_peralatan_medis_var_id!=''){nama_peralatan_medis_var_id +="#"+$(this).find(".nama_peralatan_medis_var_id").val()}else{nama_peralatan_medis_var_id +=$(this).find(".nama_peralatan_medis_var_id").val()};
		
	});
	$("#peralatan_medis_detail").val(nama_peralatan_medis_var);
	$("#peralatan_medis_detail_id").val(nama_peralatan_medis_var_id);

	set_nomor();
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
}
function set_nomor(){
	let no=1;
	$('#index_pengobatan tbody tr').each(function() {
		$(this).find('td:eq(0)').text(no);
		no++;
	});
	no=1;
	$('#index_riwayat_penyakit tbody tr').each(function() {
		$(this).find('td:eq(0)').text(no);
		no++;
	});
	no=1;
	$('#index_peralatan_medis tbody tr').each(function() {
		$(this).find('td:eq(0)').text(no);
		no++;
	});

}
function generate_tabel(){
	
	let pengobatan_detail = $("#pengobatan_detail").val().split("#");
	let tabel='';
	if (pengobatan_detail){
	console.log(pengobatan_detail);
		
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_pengobatan"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_pengobatan"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	pengobatan_detail.forEach(myFunction)
	let no=1;
	function myFunction(item, index, arr) {
		if (item!==''){
		tabel +="<tr>";
		tabel +='<td class="text-center">'+(index+1)+'</td>';
		tabel +='<td class="text-left"><input type="hidden" class="nama_pengobatan_var" value="'+pengobatan_detail[index]+'">'+pengobatan_detail[index]+'</td>';
		tabel +='<td class="text-center">'+btn+'</td>';
		tabel +="</tr>";
		}
		
	}
	$('#index_pengobatan tbody').append(tabel);
	}
	clear_input_pengobatan();
	
	let riwayat_penyakit_detail = $("#riwayat_penyakit_detail").val().split("#");
	tabel='';
	if (riwayat_penyakit_detail){
	console.log(riwayat_penyakit_detail);
		
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_riwayat_penyakit"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_riwayat_penyakit"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	riwayat_penyakit_detail.forEach(myFunction)
	let no=1;
	function myFunction(item, index, arr) {
		if (item!==''){
		tabel +="<tr>";
		tabel +='<td class="text-center">'+(index+1)+'</td>';
		tabel +='<td class="text-left"><input type="hidden" class="nama_riwayat_penyakit_var" value="'+riwayat_penyakit_detail[index]+'">'+riwayat_penyakit_detail[index]+'</td>';
		tabel +='<td class="text-center">'+btn+'</td>';
		tabel +="</tr>";
		}
		
	}
	$('#index_riwayat_penyakit tbody').append(tabel);
	}
	clear_input_riwayat_penyakit();

	let peralatan_medis_detail = $("#peralatan_medis_detail").val().split("#");
	let peralatan_medis_detail_id = $("#peralatan_medis_detail_id").val().split("#");
	tabel='';
	if (peralatan_medis_detail){
	console.log(peralatan_medis_detail);
		
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_peralatan_medis"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_peralatan_medis"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	peralatan_medis_detail.forEach(myFunction)
	let no=1;
	function myFunction(item, index, arr) {
		if (item!==''){
		tabel +="<tr>";
		tabel +='<td class="text-center">'+(index+1)+'</td>';
		tabel +='<td class="text-left"><input type="hidden" class="nama_peralatan_medis_var_id" value="'+peralatan_medis_detail_id[index]+'"><input type="hidden" class="nama_peralatan_medis_var" value="'+peralatan_medis_detail[index]+'">'+peralatan_medis_detail[index]+'</td>';
		tabel +='<td class="text-center">'+btn+'</td>';
		tabel +="</tr>";
		}
		
	}
	$('#index_peralatan_medis tbody').append(tabel);
	}
	clear_input_peralatan_medis();
}
function simpan_pengobatan(){
	let tabel='';
	
	if ($("#nama_pengobatan_var").val()==""){
		sweetAlert("Maaf...", "Tentukan Pengobatan!", "error");
		return false;
	}
	let rowCount = $("#no_pengobatan_var").val();
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_pengobatan"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_pengobatan"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center">'+rowCount+'</td>';
	tabel +='<td class="text-left"><input type="hidden" class="nama_pengobatan_var" value="'+$("#nama_pengobatan_var").val()+'">'+$("#nama_pengobatan_var").val()+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	
	if ($("#rowindex_pengobatan").val() != '') {
		$('#index_pengobatan tbody tr:eq(' + $("#rowindex_pengobatan").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_pengobatan tbody').append(tabel);
	}
	// $("#index_pengobatan").append(tabel);
	clear_input_pengobatan();
	generate_array_penyakit();
}
$(document).on("click", ".btn_edit_pengobatan", function() {
	$("#rowindex_pengobatan").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_pengobatan").attr('disabled', true);
	$(".btn_hapus_pengobatan").attr('disabled', true);
	
	$("#no_pengobatan_var").val($(this).closest('tr').find("td:eq(0)").html());
	$("#nama_pengobatan_var").val($(this).closest('tr').find("td:eq(1) input").val());
	
	$("#nama_pengobatan_var").focus();
});
$(document).on("click", ".btn_hapus_pengobatan", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function clear_input_pengobatan(){
	
	let rowCount = ($('#index_pengobatan tr').length)-1;
	$("#nama_pengobatan_var").val("");
	$("#no_pengobatan_var").val(rowCount);
	
	$("#rowindex_pengobatan").val("");
	$(".btn_edit_pengobatan").attr('disabled', false);
	$(".btn_hapus_pengobatan").attr('disabled', false);
}
function simpan_riwayat_penyakit(){
	let tabel='';
	
	if ($("#nama_riwayat_penyakit_var").val()==""){
		sweetAlert("Maaf...", "Tentukan Riwayat Penyakit!", "error");
		return false;
	}
	let rowCount = $("#no_riwayat_penyakit_var").val();
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_riwayat_penyakit"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_riwayat_penyakit"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center">'+rowCount+'</td>';
	tabel +='<td class="text-left"><input type="hidden" class="nama_riwayat_penyakit_var" value="'+$("#nama_riwayat_penyakit_var").val()+'">'+$("#nama_riwayat_penyakit_var").val()+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	
	if ($("#rowindex_riwayat_penyakit").val() != '') {
		$('#index_riwayat_penyakit tbody tr:eq(' + $("#rowindex_riwayat_penyakit").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_riwayat_penyakit tbody').append(tabel);
	}
	// $("#index_riwayat_penyakit").append(tabel);
	clear_input_riwayat_penyakit();
	generate_array_penyakit();
}
$(document).on("click", ".btn_edit_riwayat_penyakit", function() {
	$("#rowindex_riwayat_penyakit").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_riwayat_penyakit").attr('disabled', true);
	$(".btn_hapus_riwayat_penyakit").attr('disabled', true);
	
	$("#no_riwayat_penyakit_var").val($(this).closest('tr').find("td:eq(0)").html());
	$("#nama_riwayat_penyakit_var").val($(this).closest('tr').find("td:eq(1) input").val());
	
	$("#nama_riwayat_penyakit_var").focus();
});
$(document).on("click", ".btn_hapus_riwayat_penyakit", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function clear_input_riwayat_penyakit(){
	
	let rowCount = ($('#index_riwayat_penyakit tr').length)-1;
	$("#nama_riwayat_penyakit_var").val("");
	$("#no_riwayat_penyakit_var").val(rowCount);
	
	$("#rowindex_riwayat_penyakit").val("");
	$(".btn_edit_riwayat_penyakit").attr('disabled', false);
	$(".btn_hapus_riwayat_penyakit").attr('disabled', false);
}
function simpan_peralatan_medis(){
	let tabel='';
	let nama_peralatan_medis_var=$("#nama_peralatan_medis_var option:selected").text();
	let nama_peralatan_medis_var_id=$("#nama_peralatan_medis_var").val();
	// alert(nama_peralatan_medis_var_id);
	if ($("#nama_peralatan_medis_var_id").val()==""){
		sweetAlert("Maaf...", "Tentukan Peralatan Medis!", "error");
		return false;
	}
	let rowCount = $("#no_peralatan_medis_var").val();
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_peralatan_medis"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_peralatan_medis"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center">'+rowCount+'</td>';
	tabel +='<td class="text-left"><input type="hidden" class="nama_peralatan_medis_var_id" value="'+nama_peralatan_medis_var_id+'"><input type="hidden" class="nama_peralatan_medis_var" value="'+nama_peralatan_medis_var+'">'+nama_peralatan_medis_var+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	console.log(tabel);
	if ($("#rowindex_peralatan_medis").val() != '') {
		$('#index_peralatan_medis tbody tr:eq(' + $("#rowindex_peralatan_medis").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_peralatan_medis tbody').append(tabel);
	}
	// $("#index_peralatan_medis").append(tabel);
	clear_input_peralatan_medis();
	generate_array_penyakit();
}
$("#index_peralatan_medis").on("click", ".btn_edit_peralatan_medis", function() {
	$("#rowindex_peralatan_medis").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_peralatan_medis").attr('disabled', true);
	$(".btn_hapus_peralatan_medis").attr('disabled', true);
	// alert($(this).closest('tr').find(".nama_peralatan_medis_var_id").val());
	$("#no_peralatan_medis_var").val($(this).closest('tr').find("td:eq(0)").html());
	$("#nama_peralatan_medis_var").val($(this).closest('tr').find(".nama_peralatan_medis_var_id").val()).trigger('change');
	
	$("#nama_peralatan_medis_var").focus();
});
$(document).on("click", ".btn_hapus_peralatan_medis", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function clear_input_peralatan_medis(){
	
	let rowCount = ($('#index_peralatan_medis tr').length)-1;
	$("#nama_peralatan_medis_var").val("").trigger('change');
	$("#no_peralatan_medis_var").val(rowCount);
	
	$("#rowindex_peralatan_medis").val("");
	$(".btn_edit_peralatan_medis").attr('disabled', false);
	$(".btn_hapus_peralatan_medis").attr('disabled', false);
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".btn-menunggu").removeAttr('disabled');
		 $(".btn_terima").removeAttr('disabled');
	}
}
function set_riwayat_penyakit(){
	let pengobatan=$("#pengobatan").val();
	if (pengobatan=='2'){
		$(".div_pengobatan").show();
	}else{
		$(".div_pengobatan").hide();
	}
	let riwayat_penyakit=$("#riwayat_penyakit").val();
	if (riwayat_penyakit=='2'){
		$(".div_riwayat").show();
	}else{
		$(".div_riwayat").hide();
	}
	let peralatan_medis=$("#peralatan_medis").val();
	if (peralatan_medis=='2'){
		$(".div_peralatan").show();
	}else{
		$(".div_peralatan").hide();
	}
	
}
$("#pengobatan,#riwayat_penyakit,#peralatan_medis").on("change", function(){
	set_riwayat_penyakit();
});
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
$("#riwayat_alergi").on("change", function(){
	
	set_riwayat_alergi();
});
function clear_input_alergi(){
	$("#alergi_id").val('');
	$("#btn_add_alergi").html('<i class="fa fa-plus"></i> Add');
	$("#input_jenis_alergi").val('');
	$("#input_detail_alergi").val('');
	$("#input_reaksi_alergi").val('');
}
function edit_alergi(id){
	$("#alergi_id").val(id);
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/edit_alergi_tf_hospital', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				
			},
		success: function(data) {
			$("#btn_add_alergi").html('<i class="fa fa-save"></i> Simpan');
			$("#input_jenis_alergi").val(data.input_jenis_alergi).trigger('change');
			$("#input_detail_alergi").val(data.input_detail_alergi);
			$("#input_reaksi_alergi").val(data.input_reaksi_alergi);
		}
	});
}
function hapus_alergi(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Data Alergi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/hapus_alergi_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
				load_alergi_tf_hospital();		
			}
		});
	});			
}
function set_riwayat_alergi(){
	let riwayat=$("#riwayat_alergi").val();
	if (riwayat=='2'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").show();
		$(".div_input_alergi").show();
		$("#li_alergi_1").addClass('active');
		$("#li_alergi_2").removeClass('active');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else if (riwayat=='1'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").hide();
		$(".div_input_alergi").hide();
		// // $('#alergi_2 > a').tab('show');
		// $('#alergi_1 > a').tab('hide');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else{
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
		$(".div_alergi_all").hide();
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	
		
	}
	
}
function create_assesmen(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_template_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_template_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
// $(".input-default").blur(function(){
	// simpan_default_warna();	
// });
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	if ($("#tujuan_rs").val()=='#'){
		sweetAlert("Maaf...", "Ruang / Kamar Asal Harus diisi", "error");
		return false;
	}
	if ($("#idunit_tujuan").val()=='#'){
		sweetAlert("Maaf...", "Ruang / Kamar Tujuan Harus diisi", "error");
		return false;
	}
	if ($("#idunit_tujuan").val()==$("#tujuan_rs").val()){
		sweetAlert("Maaf...", "Unit Tujuan & Unit Asal Tidak Boleh Sama", "error");
		return false;
	}
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
$(document).on('click','.pemeriksaan_diagnosis',function(){
	if (status_assemen!='2'){
		simpan_assesmen();
	}else{
		event.preventDefault();
	}
});
function simpan_alergi_tf_hospital(){// alert($("#keluhan_utama").val());
	let alergi_id=$("#alergi_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	let input_jenis_alergi=$("#input_jenis_alergi").val();
	let input_detail_alergi=$("#input_detail_alergi").val();
	let input_reaksi_alergi=$("#input_reaksi_alergi").val();
	if (input_detail_alergi==''){
		sweetAlert("Maaf...", "Isi Detail Alergi!", "error");
		return false;

	}
	if (input_reaksi_alergi==''){
		sweetAlert("Maaf...", "Isi Reaksi Alergi!", "error");
		return false;

	}
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/simpan_alergi_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					alergi_id:alergi_id,
					pendaftaran_id:pendaftaran_id,
					assesmen_id:assesmen_id,
					input_jenis_alergi:input_jenis_alergi,
					input_detail_alergi:input_detail_alergi,
					input_reaksi_alergi:input_reaksi_alergi,
					idpasien:idpasien,
				},
			success: function(data) {
				clear_input_alergi();
						$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Alergi.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					load_alergi_tf_hospital();
				}
			}
		});
	}
	
}
function terima(){// alert($("#keluhan_utama").val());
	let assesmen_id=$("#assesmen_id").val();
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Menerima Transfer ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
				if (assesmen_id){
					$.ajax({
						url: '{site_url}tpendaftaran_ranap_erm/terima_tf_hospital', 
						dataType: "JSON",
						method: "POST",
						data : {
								assesmen_id:assesmen_id,
							},
						success: function(data) {
							clear_input_alergi();
									$("#cover-spin").hide();
							if (data==null){
								swal({
									title: "Gagal!",
									text: "Simpan .",
									type: "error",
									timer: 1500,
									showConfirmButton: false
								});

							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Terima Transfer Berhasil'});
								location.reload();
							}
						}
					});
				}
				
		});		
	
}
function load_alergi_tf_hospital(){
	$('#alergi_2 > a').tab('hide');
	let assesmen_id=$("#assesmen_id").val();
	$('#index_alergi').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_alergi').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}tpendaftaran_poli_ttv/load_alergi_tf_hospital', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 disabel_edit();
			 }  
		});
	
}
function load_alergi_tf_hospital_his(){
	$('#alergi_1 > a').tab('hide');
	let assesmen_id=$("#assesmen_id").val();
	let idpasien=$("#idpasien").val();
	$('#index_alergi_his').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_alergi_his').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}tpendaftaran_poli_ttv/load_alergi_his', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
function simpan_assesmen(){
	// alert($("#keluhan_utama").val());
	// return false;
	if (load_awal_assesmen==false){
	if (status_assemen !='2'){
		var pemeriksaan_diagnosis = $("input[name='pemeriksaan_diagnosis[]']:checked").map(function () {
			return this.value;
		}).get();
			   // console.log(list)
		// return false;
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let tanggalkontak=$("#tanggalkontak").val();
		let waktukontak=$("#waktukontak").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		// if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/save_tf_hospital', 
				dataType: "JSON",
				method: "POST",
				data : {
						st_edited:st_edited,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						tujuan_rs : $("#tujuan_rs").val(),
						tujuan_detail : $("#tujuan_detail").val(),
						staff_kontak : $("#staff_kontak").val(),
						
						tanggalkontak : $("#tanggalkontak").val(),
						waktukontak : $("#waktukontak").val(),
						tanggalserah_terima : $("#tanggalserah_terima").val(),
						waktuserah_terima : $("#waktuserah_terima").val(),
						tanggalberangkat : $("#tanggalberangkat").val(),
						waktuberangkat : $("#waktuberangkat").val(),
						tanggaltiba : $("#tanggaltiba").val(),
						waktutiba : $("#waktutiba").val(),
						
						staff_terima : $("#staff_terima").val(),
						notelepon_kontak : $("#notelepon_kontak").val(),
						alasan_merujuk : $("#alasan_merujuk").val(),
						keterangan_merujuk : $("#keterangan_merujuk").val(),
						diagnosa_medis : $("#diagnosa_medis").val(),
						iddokter_merujuk : $("#iddokter_merujuk").val(),
						riwayat_alergi : $("#riwayat_alergi").val(),
						alergi_detail : $("#alergi_detail").val(),
						pengobatan : $("#pengobatan").val(),
						pengobatan_detail : $("#pengobatan_detail").val(),
						riwayat_penyakit : $("#riwayat_penyakit").val(),
						riwayat_penyakit_detail : $("#riwayat_penyakit_detail").val(),
						tindakan : $("#tindakan").val(),
						intake_oral : $("#intake_oral").val(),
						gcs_eye : $("#gcs_eye").val(),
						gcs_voice : $("#gcs_voice").val(),
						gcs_motion : $("#gcs_motion").val(),
						pupil_1 : $("#pupil_1").val(),
						pupil_2 : $("#pupil_2").val(),
						reflex_cahaya_1 : $("#reflex_cahaya_1").val(),
						reflex_cahaya_2 : $("#reflex_cahaya_2").val(),
						nadi : $("#nadi").val(),
						nafas : $("#nafas").val(),
						td_sistole : $("#td_sistole").val(),
						td_diastole : $("#td_diastole").val(),
						suhu : $("#suhu").val(),
						spo2 : $("#spo2").val(),
						peralatan_medis : $("#peralatan_medis").val(),
						peralatan_medis_detail : $("#peralatan_medis_detail").val(),
						peralatan_medis_detail_id : $("#peralatan_medis_detail_id").val(),
						perawatan_lanjutan : $("#perawatan_lanjutan").val(),
						kejadian_klinis : $("#kejadian_klinis").val(),
						staff_perujuk : $("#staff_perujuk").val(),
						staff_penerima : $("#staff_penerima").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_index_template_tf_hospital', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/edit_template_tf_hospital', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		let st_ranap=$("#st_ranap").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_history_pengkajian_tf_hospital', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							st_ranap:st_ranap,
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function edit_history_assesmen(id,pendaftaran_id_ranap){
		$("#pendaftaran_id_ranap_tmp").val(pendaftaran_id_ranap);
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let pendaftaran_id_ranap_tmp=$("#pendaftaran_id_ranap_tmp").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/save_edit_tf_hospital', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id_ranap=data.pendaftaran_id_ranap;
					let pendaftaran_id=data.pendaftaran_id;
					if (pendaftaran_id_ranap=='0'){
						window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_tf/input_tf_hospital_rj'); ?>";
					}else{
						window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id_ranap+"/erm_tf/input_tf_hospital'); ?>";
					}
						// window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id_ranap_tmp+"/erm_tf/input_tf_hospital'); ?>";
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_tf_hospital', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_tf_hospital', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	
</script>