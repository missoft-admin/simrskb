<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		
</style>

<?if ($menu_kiri=='input_pindah_bed'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_pindah_bed' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
		// echo $tanggal_pernyataan;
	if ($tanggal_input){
		$tgltransaksi=HumanDateShort($tanggal_input);
		$waktutransaksi=HumanTime($tanggal_input);
	}else{
		$tgltransaksi=date('d-m-Y');
		$waktutransaksi=date('H:i:s');
	}
	
	$disabel_input='';
	$disabel_cetak='disabled';
	
			$list_dokter=(get_all('mdokter',array('status'=>1)));		
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_tf">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-note"></i> Pencatatan Saat Ini</a>
				</li>
				<li class="">
					<a href="#tab_2" onclick="load_pindah_list_history()"><i class="fa fa-history"></i> Riwayat Pencatatan Pindah</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<input type="hidden" id="history_id" value="{history_id}" >		
					<div class="row">
						<div class="col-md-12">
								<h4 class="font-w700 push-5 text-center text-primary">TRANSAKSI PINDAH RUANGAN / BED</h4>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p>Info <a class="alert-link" href="javascript:void(0)"> Auto Save </a>!</p>
								</div>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									
								</div>
								
							</div>
						</div>
						<?php if (UserAccesForm($user_acces_form,array('2222'))){ ?>
						<div class="form-group">
							<div class="col-md-12 ">
								
								<div class="col-md-2 ">
									<label for="idruangan_asal">Ruangan Saat Ini</label>
									<select id="idruangan_asal" disabled class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($idruangan_asal == "0" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
										<option value="<?=$r->id?>" <?=($idruangan_asal == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-2 ">
									<label for="idkelas_asal">Kelas Saat Ini</label>
									<select id="idkelas_asal"  disabled class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="" <?=($idkelas_asal == "0" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>" <?=($idkelas_asal == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-2 ">
									<label for="idbed_asal">Bed Saat Ini</label>
									<select id="idbed_asal" disabled class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" <?=($idbed_asal == "0" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>" <?=($idbed_asal == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Mulai Dari </label>
									<div class="input-group date">
										<input type="text" class=" form-control " disabled id="tanggaldari_awal" placeholder="HH/BB/TTTT" value="<?= ($tanggaldari_asal!='0000-00-00'?HumanDateShort($tanggaldari_asal):'') ?>">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Sampai </label>
									<div class="input-group date">
										<input type="text" class=" form-control " disabled id="tanggalsampai_asal" placeholder="Tanggal Sampai" value="<?= ($tanggalsampai_asal!='0000-00-00'?HumanDateShort($tanggalsampai_asal):HumanDateShort(date('Y-m-d'))) ?>">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
							</div>
							
						</div>
						<?if ($statuscheckout=='0'){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="idtipe">Tanggal Pindah</label>
									<div class="input-group date">
										<input tabindex="1" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggal_pindah" placeholder="HH/BB/TTTT" value="<?= $tgltransaksi ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="idruangan">Tujuan Ruangan Baru</label>
									<select id="idruangan"  class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" selected>Pilih Ruangan</option>
										<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1,'st_tampil'=>1)) as $r){?>
										<option value="<?=$r->id?>" ><?=$r->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-2 ">
									<label for="idkelas">Tujuan Kelas Baru</label>
									<select id="idkelas"   class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="0" selected>Pilih Kelas</option>
										<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
									
								</div>
								<div class="col-md-2 ">
									<label for="idbed">Tujuan Bed Baru</label>
									<select id="idbed"  class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="0" selected>Pilih Bed</option>
									</select>
									
								</div>
								<div class="col-md-4 ">
									<label for="keterangan">Keterangan</label>
									<div class="input-group">
									<input id="keterangan"  class="form-control " type="text" value="">
									<span class="input-group-btn">
											
											<button onclick="add_pindah()" title="Simpan Pindah" class="btn btn-primary" type="button"><i class="fa fa-save"></i> Add</button>
											<button onclick="clear_pindah()" title="Clear Pindah" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</div>
							</div>
							
						</div>
						<?}?>
						<?}?>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-header-bg" id="index_pindah">
											<thead>
												<tr>
													<th width="5%" class="text-center">NO</th>
													<th width="15%" class="text-center">RUANGAN ASAL</th>
													<th width="8%" class="text-center">MULAI DARI ASAL</th>
													<th width="8%" class="text-center">SAMPAI ASAL</th>
													<th width="15%" class="text-center">RUANGAN TUJUAN</th>
													<th width="8%" class="text-center">MULAI DARI TUJUAN</th>
													<th width="8%" class="text-center">SAMPAI TUJUAN</th>
													<th width="10%" class="text-center">KETERANGAN</th>
													<th width="15%" class="text-center">USER</th>
												</tr>
											</thead>
											<tbody></tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>	
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">HISTORY PINDAH RUANGAN / BED</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Dirawat</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Kelular</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_keluar_1"  placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_keluar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="col-md-6">
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruangan_filter">Ruangan</label>
										<div class="col-md-8">
											<select id="idruangan_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach(get_all('mruangan',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="iddokter_filter">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="iddokter_filter">Kelompok Pasien</label>
										<div class="col-md-8">
											<select id="idkelompokpasien_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="load_pindah_list_history()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="js-table-sections table table-hover" id="index_history_pindah">
									<thead>
										<tr>
											<th width="3%">No</th>
											<th width="10%">Tipe</th>
											<th width="10%">No Pendaftaran</th>
											<th width="10%">Tanggal Dirawat</th>
											<th width="10%">Tanggal Keluar</th>
											<th width="20%">Ruang Perawatan</th>
											<th width="15%">Dokter</th>
											<th width="15%">Kelompok Pasien</th>
											<th width="10%">Action</th>
										   
										</tr>
										
									</thead>
									<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					
					<?php echo form_close() ?>
					
					
				</div>
			</div>
			
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
var tableRanap;
var detailRowsRanap = [];
$(document).ready(function() {

	clear_pindah();
	load_pindah_list();
});
$("#idkelas,#idruangan").change(function() {
	let idruangan = $("#idruangan").val();
	let idkelas = $("#idkelas").val();
	let id = $("#id").val();
	// alert(idruangan)
	getRanapBed(idruangan, idkelas, id);
});
function getRanapBed(idruangan, idkelas, id) {
	$.ajax({
		url: "{site_url}tpendaftaran_ranap/getBed/",
		method: "POST",
		dataType: "json",
		data : {
			idruangan:idruangan,
			idkelas:idkelas,
			id:id,
			
		},
		success: function(data) {
			$("#idbed").empty();
			$("#idbed").append("<option value='0'>Pilih Bed</option>");
			data.map((item) => {
				$("#idbed").append('<option value="'+item.id+'" '+(item.id==='0'?'disabled':'')+'> '+item.nama+'</option>');
			})
			$("#idbed").selectpicker('refresh');
		}
	});
	
}
function simpan_selesai(){
	if (($("#tanggal_selesai").val())==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Tanggal Selesai",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_selesai_pindah/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				history_id : $("#history_id").val(),
				tanggal_selesai : $("#tanggal_selesai").val(),
				
				
		  },success: function(data) {
				  $("#cover-spin").hide();
			  if (data==false){
				  swal({
						title: "Gagal",
						text: "Gagal Updaet",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
			  }else{
				  $("#modal_selesai").modal('hide');
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  clear_pindah();
				  load_pindah_list();
			  }
			}
		});
}
function simpan_selesai_utama(){
	if (($("#pindah_utama_baru").val())=='#'){
		swal({
			title: "Gagal",
			text: "Silahkan isi DPJP Baru",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#pindah_utama_baru").val() == $("#pindah_utama_asal").val())){
		swal({
			title: "Gagal",
			text: "Silahkan isi DPJP Berbeda",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#tanggal_selesai_utama").val())==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Tanggal Selesai",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#diagnosa_baru").val())==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi diagnosa",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_selesai_utama/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				history_id : $("#history_id").val(),
				tanggal_selesai : $("#tanggal_selesai_utama").val(),
				pindah_utama_asal : $("#pindah_utama_asal").val(),
				pindah_utama_baru : $("#pindah_utama_baru").val(),
				idrawatinap : $("#pendaftaran_id_ranap").val(),
				diagnosa : $("#diagnosa_baru").val(),
				keterangan : $("#keterangan_baru").val(),
		  },success: function(data) {
				  $("#cover-spin").hide();
			  if (data==false){
				  swal({
						title: "Gagal",
						text: "Gagal Updaet",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
			  }else{
				  $("#modal_selesai_utama").modal('hide');
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  clear_pindah();
				  load_pindah_list();
			  }
			}
		});
}
function clear_pindah(){
	$("#keterangan").val('');
	$("#idruangan").val('0').trigger('change');
	$("#idkelas").val('0').trigger('change');
	$("#idbed").val('0').trigger('change');
	
}

function edit_pindah(id,idtipe){
	clear_pindah();
	if (idtipe=='1'){
		$("#tipe_pindah").prop('disabled',true);
		$("#iddokter").prop('disabled',true);
	}else{
		$("#tipe_pindah").prop('disabled',true);
		$("#iddokter").prop('disabled',false);
	}
	$("#history_id").val(id);
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/get_edit_pindah/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			id : id,
			idtipe : idtipe,
			
	  },success: function(data) {
		  $("#cover-spin").hide();
			$("#history_id").val(data.id),
			$("#waktutransaksi").val(data.waktutransaksi),
			$("#tgltransaksi").val(data.tgltransaksi),
			$("#tipe_pindah").val(data.tipe_pindah).trigger('change.select2');
			$("#iddokter").val(data.iddokter).trigger('change.select2');
			$("#tanggaldari").val(data.tanggaldari);
			$("#tanggalsampai").val(data.tanggalsampai);
			$("#keterangan").val(data.keterangan);
			$("#diagnosa").val(data.diagnosa);
			$("#cover-spin").hide();
			$("#diagnosa").focus().select();
		}
	});
}
function add_pindah(){
	
	if (($("#idruangan").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Ruangan Baru",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#idkelas").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Kelas Baru",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#idbed").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Bed Baru",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#tanggal_pindah").val()==null || $("#tanggal_pindah").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan Tentukan Tanggal Pindah",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_pindah/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				idrawatinap : $("#pendaftaran_id_ranap").val(),
				history_id : $("#history_id").val(),
				tanggal_pindah : $("#tanggal_pindah").val(),
				idruangan : $("#idruangan").val(),
				idkelas : $("#idkelas").val(),
				idbed : $("#idbed").val(),
				keterangan : $("#keterangan").val(),
				
				
		  },success: function(data) {
			  if (data==false){
				  $("#cover-spin").hide();
				  swal({
						title: "Gagal",
						text: "Data Duplicate",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
			  }else{
				  $("#cover-spin").show();
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  location.reload();
			  }
			}
		});
}

function load_pindah_list(){
	$("#cover-spin").show();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		$('#index_pindah tbody').empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_pindah_list', 
			dataType: "json",
			type: "POST",
			data: {
				pendaftaran_id_ranap: pendaftaran_id_ranap,
			},
			success: function(data) {
				$('#st_dokter_utama').val(data.st_dokter_utama);
				$('#index_pindah tbody').append(data.tabel);
				$("#cover-spin").hide();
				
			}
		});
}

function load_pindah_list_history(){
	$('#index_history_pindah').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	tableRanap = $('#index_history_pindah').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					// {  className: "text-center", targets:[1,2,3,5,6,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/load_pindah_list_history', 
				type: "POST" ,
				dataType: 'json',
				data : {
						pendaftaran_id_ranap:$("#pendaftaran_id_ranap").val(),
						idpasien:$("#idpasien").val(),
						notransaksi:$("#notransaksi").val(),
						tgl_daftar_1:$("#tgl_daftar").val(),
						tgl_daftar_2:$("#tgl_daftar_2").val(),
						tanggal_keluar_1:$("#tanggal_keluar_1").val(),
						tanggal_keluar_2:$("#tanggal_keluar_2").val(),
						idruangan:$("#idruangan_filter").val(),
						iddokter:$("#iddokter_filter").val(),
						idkelompokpasien:$("#idkelompokpasien_filter").val(),
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
}

$('#index_history_pindah tbody').on('click', '.details-control', function() {
	
        var tr = $(this).closest('tr');
        var row = tableRanap.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRowsRanap);

        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();

            // Remove from the 'open' array
            detailRowsRanap.splice(idx, 1);
        } else {
            var idpendaftaran = tr.find("td:eq(0) span").data('idpendaftaran');
			$("#cover-spin").show();
            $.ajax({
                url: "{site_url}Tpendaftaran_ranap_erm/load_pindah_list_detail",
               type: "POST" ,
				dataType: 'json',
				data : {
						pendaftaran_id_ranap:idpendaftaran,
						
					   },
                success: function(data) {
					$("#cover-spin").hide();
                    tr.addClass('details');
                    row.child((data.tabel)).show();

                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRowsRanap.push(tr.attr('id'));
                    }
                }
            });

        }
    });

</script>