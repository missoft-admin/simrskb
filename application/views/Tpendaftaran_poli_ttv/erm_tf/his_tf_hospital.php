<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	}	
	
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.text-bold {
		font-weight: bold;		
	}
	 <?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<style>
	
	#sig_ttd_penerima canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='his_tf_hospital' || $menu_kiri=='his_tf_hospital_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_tf_hospital' || $menu_kiri=='his_tf_hospital_rj'  ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggalkontak=HumanDateShort($tanggal_kontak);
						$waktukontak=HumanTime($tanggal_kontak);

						$tanggalkontak_asal=HumanDateShort($tanggal_kontak_asal);
						$waktukontak_asal=HumanTime($tanggal_kontak_asal);

						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_tf_hospital=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_tf">
		<?if ($st_lihat_tf_hospital=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_tf_hospital=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary"> {judul_header_ina}</h4>
									<h5 class="font-w700 push-5 text-center text-primary"><i> {judul_header_eng}</i></h5>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_tf/input_tf_hospital" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text"  class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >TUJUAN TRANSFER / RUJUKAN</h5>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="tujuan_rs">Tujuan Rumah Sakit</label>
									<select id="tujuan_rs" name="tujuan_rs" class="<?=($tujuan_rs != $tujuan_rs_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($tujuan_rs=='0'?'selected':'')?>>-Belum Ditentukan-</option>
										<?foreach($list_rs as $row){?>
											<option value="<?=$row->id?>" <?=($tujuan_rs==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tujuan_detail">Detail</label>
									<input id="tujuan_detail" class="<?=($tujuan_detail!=$tujuan_detail_asal ?'edited':'')?> form-control"  type="text"  value="{tujuan_detail}"  placeholder="" required>
								</div>
								
								
								
							</div>
							<div class="col-md-6">
								<div class="col-md-5">
									<label for="staff_kontak">Staff Yang Melakukan Kontak </label>
									<select id="staff_kontak" class="<?=($staff_kontak != $staff_kontak_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($staff_kontak==''?'selected':'')?>>-Silahkan Pilih Opsi-</option>
										<?foreach($list_ppa as $row){?>
											<option value="<?=$row->id?>" <?=($staff_kontak==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4">
									<label for="tanggalkontak">Tanggal Kontak</label>
									<div class="input-group date">
										<input id="tanggalkontak"  class="<?=($tanggalkontak!=$tanggalkontak_asal ?'edited':'')?> js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalkontak ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="waktukontak">&nbsp;</label>
									<div class="input-group">
										<input id="waktukontak" class="<?=($waktukontak!=$waktukontak_asal ?'edited':'')?> time-datepicker form-control " type="text"   value="<?= $waktukontak ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
						</div>										
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="staff_terima">Nama Staff Yang Menerima Kontak</label>
									<input id="staff_terima" class="<?=($staff_terima!=$staff_terima_asal ?'edited':'')?> form-control"  type="text"  value="{staff_terima}"  placeholder="" required>
								</div>
								<div class="col-md-6">
									<label for="notelepon_kontak">No Telepon</label>
									<input id="notelepon_kontak" class="<?=($notelepon_kontak!=$notelepon_kontak_asal ?'edited':'')?> form-control"  type="text"  value="{notelepon_kontak}"  placeholder="" required>
								</div>
								
							</div>
							<?
								$tanggalberangkat=HumanDateShort($ambulance_berangkat);
								$waktuberangkat=HumanTime($ambulance_berangkat);

								$tanggalberangkat_asal=HumanDateShort($ambulance_berangkat_asal);
								$waktuberangkat_asal=HumanTime($ambulance_berangkat_asal);


								$tanggaltiba=HumanDateShort($ambulance_tiba);
								$waktutiba=HumanTime($ambulance_tiba);

								$tanggaltiba_asal=HumanDateShort($ambulance_tiba_asal);
								$waktutiba_asal=HumanTime($ambulance_tiba_asal);
							?>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="tanggalberangkat">Ambulance Berangkat</label>
									<div class="input-group date">
										<input id="tanggalberangkat"  class="<?=($tanggalberangkat!=$tanggalberangkat_asal ?'edited':'')?> js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalberangkat ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="waktuberangkat" class="<?=($waktuberangkat!=$waktuberangkat_asal ?'edited':'')?> time-datepicker form-control " type="text"   value="<?= $waktuberangkat ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="tanggaltiba">Ambulance tiba</label>
									<div class="input-group date">
										<input id="tanggaltiba"  class="<?=($tanggaltiba!=$tanggaltiba_asal ?'edited':'')?> js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggaltiba ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="waktutiba" class="<?=($waktutiba!=$waktutiba_asal ?'edited':'')?> time-datepicker form-control " type="text"   value="<?= $waktutiba ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >ALASAN MERUJUK</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="alasan_merujuk">Alasan Merujuk</label>
									<select id="alasan_merujuk" class="<?=($alasan_merujuk != $alasan_merujuk_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(292) as $row){?>
										<option value="<?=$row->id?>" <?=($alasan_merujuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="keterangan_merujuk">Keterangan</label>
									<input id="keterangan_merujuk" class="<?=($keterangan_merujuk!=$keterangan_merujuk_asal ?'edited':'')?> form-control"  type="text"  value="{keterangan_merujuk}"  placeholder="" required>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="diagnosa_medis">Diagnosa Medis</label>
									<input id="diagnosa_medis" class="<?=($diagnosa_medis!=$diagnosa_medis_asal ?'edited':'')?> form-control"  type="text"  value="{diagnosa_medis}"  placeholder="" required>
								</div>
								<div class="col-md-6">
									<label for="iddokter_merujuk">Dokter Yang Merujuk</label>
									<select id="iddokter_merujuk" class="<?=($iddokter_merujuk != $iddokter_merujuk_asal?'edited':'')?> form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_dokter as $row){?>
										<option value="<?=$row->id?>" <?=($iddokter_merujuk==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >CATATAN KLINIS</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4 ">
								<label class="col-xs-12">Alergi </label>
								<div class="col-md-12">
									<select id="riwayat_alergi" class="<?=($riwayat_alergi != $riwayat_alergi_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_alergi=='0'?'selected':'')?>>Tidak Ada Alergi</option>
										<option value="1" <?=($riwayat_alergi=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_alergi=='2'?'selected':'')?>>Ada Alergi</option>
										
									</select>
								</div>
							</div>
							<div class="col-md-8 ">
								<label class="col-xs-12">Sebutkan Alergi </label>
								<div class="col-md-12">
									<textarea id="alergi_detail" <?=($alergi_detail != $alergi_detail_asal ?'edited':'')?> class="form-control " name="alergi_detail" rows="1" style="width:100%"><?=$alergi_detail?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="pengobatan">Pengobatan</label>
									<select id="pengobatan" class="<?=($pengobatan != $pengobatan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($pengobatan=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($pengobatan=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($pengobatan=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_pengobatan push-10-t">
									<div class="form-group ">
										<div class="col-md-12 ">
											<div class="table-responsive">
												<table class="table table-condensed table-bordered" id="index_pengobatan">
													<thead>
														<tr>
															<th width="5%" class="text-center">No</th>
															<th width="80%" class="text-center">Pengobatan<input  type="hidden" id="pengobatan_detail" value="{pengobatan_detail}"></th>
															<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_pengobatan"></th>
														</tr>
														
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
									
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="riwayat_penyakit">Riwayat Penyakit </label>
									<select id="riwayat_penyakit" class="<?=($riwayat_penyakit != $riwayat_penyakit_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_penyakit=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($riwayat_penyakit=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_penyakit=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_riwayat push-10-t">
									<div class="form-group ">
										<div class="col-md-12 ">
											<div class="table-responsive">
												<table class="table table-condensed table-bordered"" id="index_riwayat_penyakit">
													<thead>
														<tr>
															<th width="5%" class="text-center">No</th>
															<th width="80%" class="text-center">Riwayat_penyakit<input  type="hidden" id="riwayat_penyakit_detail" value="{riwayat_penyakit_detail}"></th>
															<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_riwayat_penyakit"></th>
														</tr>
														
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<div class="col-md-6">
									<label for="tindakan">Tindakan Yang Telah Dilakukan</label>
									<textarea id="tindakan" class="<?=($tindakan!=$tindakan_asal ?'edited':'')?> form-control" name="tindakan" rows="3" style="width:100%"><?=$tindakan?></textarea>
								</div>
								<div class="col-md-6">
									<label for="intake_oral">Intake Oral Terakhir</label>
									<textarea id="intake_oral" class="<?=($intake_oral!=$intake_oral_asal ?'edited':'')?> form-control" name="intake_oral" rows="3" style="width:100%"><?=$intake_oral?></textarea>
								</div>
							</div>
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >KONDISI PASIEN SAAT INI</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6">
								<div class="col-md-6 ">
									<label >GCS</label>
									<div class="input-group">
										<input id="gcs_eye" class="<?=($gcs_eye!=$gcs_eye_asal ?'edited':'')?> form-control decimal "  type="text"   value="{gcs_eye}" placeholder="Eye" required>
										<span class="input-group-addon"><?=$satuan_gcs_eye?></span>
										<input id="gcs_voice" class="<?=($gcs_voice!=$gcs_voice_asal ?'edited':'')?> form-control decimal "  type="text"  value="{gcs_voice}"  placeholder="Voice" required>
										<span class="input-group-addon"><?=$satuan_gcs_voice?></span>
										<input id="gcs_motion" class="<?=($gcs_motion!=$gcs_motion_asal ?'edited':'')?> form-control decimal "  type="text"  value="{gcs_motion}"   placeholder="Motion" required>
										<span class="input-group-addon"><?=$satuan_gcs_motion?></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label >Pupil</label>
									<div class="input-group">
										<input id="pupil_1" class="<?=($pupil_1!=$pupil_1_asal ?'edited':'')?> form-control decimal "  type="text"   value="{pupil_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_pupil?> / </span>
										<input id="pupil_2" class="<?=($pupil_2!=$pupil_2_asal ?'edited':'')?> form-control decimal "  type="text"  value="{pupil_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_pupil?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-8">
									<label for="td_sistole">Tekanan darah</label>
									<div class="input-group">
										<input id="td_sistole" class="<?=($td_sistole!=$td_sistole_asal ?'edited':'')?> form-control decimal "  type="text"   value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input id="td_diastole" class="<?=($td_diastole!=$td_diastole_asal ?'edited':'')?> form-control decimal "  type="text"  value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="suhu">Suhu Tubuh</label>
									<div class="input-group">
										<input id="suhu" class="<?=($suhu!=$suhu_asal ?'edited':'')?> form-control decimal "  type="text"  name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6 ">
								<div class="col-md-8">
									<label >Refleksi Cahaya</label>
									<div class="input-group">
										<input id="reflex_cahaya_1" class="<?=($reflex_cahaya_1!=$reflex_cahaya_1_asal ?'edited':'')?> form-control decimal "  type="text"   value="{reflex_cahaya_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_cahaya?> / </span>
										<input id="reflex_cahaya_2" class="<?=($reflex_cahaya_2!=$reflex_cahaya_2_asal ?'edited':'')?> form-control decimal "  type="text"  value="{reflex_cahaya_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_cahaya?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-4">
									<label for="nadi">Frekuensi Nadi</label>
									<div class="input-group">
										<input id="nadi" class="<?=($nadi!=$nadi_asal ?'edited':'')?> form-control decimal "   type="text"  name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="nafas">Frekuensi Nafas</label>
									<div class="input-group">
										<input id="nafas" class="<?=($nafas!=$nafas_asal ?'edited':'')?> form-control decimal "   type="text"  name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-4">
									<label for="spo2">SpO2</label>
									<div class="input-group">
										<input id="spo2" class="<?=($spo2!=$spo2_asal ?'edited':'')?> form-control decimal "  type="text"  name="spo2" value="{spo2}"  placeholder="SpO2" required>
										<span class="input-group-addon"><?=$satuan_spo2?></span>
									</div>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="peralatan_medis">Peralatan Medis </label>
									<select id="peralatan_medis" class="<?=($peralatan_medis != $peralatan_medis_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($peralatan_medis=='0'?'selected':'')?>>Tidak Ada </option>
										<option value="1" <?=($peralatan_medis=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($peralatan_medis=='2'?'selected':'')?>>Ada </option>
										
									</select>
								</div>
								<div class="col-md-12 div_peralatan push-10-t">
									
									<div class="table-responsive">
										<table class="table table-condensed table-bordered" id="index_peralatan_medis">
											<thead>
												<tr>
													<th width="5%" class="text-center">No<input  type="hidden" id="peralatan_medis_detail_id" value="{peralatan_medis_detail_id}"></th>
													<th width="80%" class="text-center">Peralatan Medis<input  type="hidden" id="peralatan_medis_detail" value="{peralatan_medis_detail}"></th>
													<th width="15%" class="text-center">Aksi<input type="hidden" id="rowindex_peralatan_medis"></th>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12">
									<label for="perawatan_lanjutan">Perawatan Pasien Lanjutan Yang Dibutuhkan</label>
									<textarea id="perawatan_lanjutan" class="<?=($perawatan_lanjutan!=$perawatan_lanjutan_asal ?'edited':'')?> form-control" name="perawatan_lanjutan" rows="5" style="width:100%"><?=$perawatan_lanjutan?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							
							<?
								$tanggalserah_terima=HumanDateShort($waktu_serah_terima);
								$waktuserah_terima=HumanTime($waktu_serah_terima);

								$tanggalserah_terima_asal=HumanDateShort($waktu_serah_terima_asal);
								$waktuserah_terima_asal=HumanTime($waktu_serah_terima_asal);
								
							?>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="kejadian_klinis">Kejadian Klinis Selama Dilakukan Transfer</label>
									<select id="kejadian_klinis" class="<?=($kejadian_klinis != $kejadian_klinis_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(294) as $row){?>
										<option value="<?=$row->id?>" <?=($kejadian_klinis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tanggaltiba">Waktu Serah Terima Pasien</label>
									<div class="input-group date">
										<input id="tanggalserah_terima"  class="<?=($tanggalserah_terima!=$tanggalserah_terima_asal ?'edited':'')?> js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalserah_terima ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="waktuserah_terima" class="<?=($waktuserah_terima!=$waktuserah_terima_asal ?'edited':'')?> time-datepicker form-control " type="text"   value="<?= $waktuserah_terima ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="staff_perujuk">Staff Yang Melakukan Rujukan </label>
									<select id="staff_perujuk" class="<?=($staff_perujuk != $staff_perujuk_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($staff_perujuk==''?'selected':'')?>>-Pilih Staff-</option>
										<?foreach($list_ppa as $row){?>
											<option value="<?=$row->id?>" <?=($staff_perujuk==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="staff_penerima">Staff Yang Meneria Pasien</label>
									<input id="staff_penerima" class="<?=($staff_penerima!=$staff_penerima_asal ?'edited':'')?> form-control"  type="text"  value="{staff_penerima}"  placeholder="" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							
							<div class="col-md-6">
								
							</div>
							<div class="col-md-6">
								<div class="col-md-12 text-center">
									<table style="width:100%">
										<tr>
											<td style="width:50%"></td>
											<td style="width:50%">
											<?if ($ttd_penerima==''){?>
												<button type="button" class="btn btn-success btn-xs btn_ttd text-center" onclick="modal_ttd_penerima()"> <i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}else{?>
												<div class="img-container fx-img-rotate-r ">
													<img class="" style="width:120px;height:120px; text-align: center;" src="{ttd_penerima}" alt="" title="">
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<button type="button" class="btn btn-default btn-xs" onclick="modal_ttd_penerima()" ><i class="fa fa-pencil"></i> </button>
																<button type="button" class="btn btn-default btn-danger  btn-xs" onclick="hapus_ttd_penerima()" ><i class="fa fa-times"></i> </button>
															</div>
														</div>
													</div>
													
												</div>
											<?}?>
											</td>
										</tr>
									</table>
								</div>
								
							</div>
						</div>		
					<?}?>
					</div>
					
						<!--BATS AKRHI -->
					
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;

$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		set_riwayat_penyakit();
		generate_tabel();
		list_index_history_edit();
		load_awal_assesmen=false;
	}
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_tf_hospital', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function generate_array_penyakit(){
	let nama_pengobatan_var='';
	
	let nama_riwayat_penyakit_var='';
	$('#index_pengobatan tbody tr').each(function() {
		if (nama_pengobatan_var!=''){nama_pengobatan_var +="#"+$(this).find('td:eq(1)').text()}else{nama_pengobatan_var +=$(this).find('td:eq(1)').text()};
		
	});
	$("#pengobatan_detail").val(nama_pengobatan_var);

	
	$('#index_riwayat_penyakit tbody tr').each(function() {
		if (nama_riwayat_penyakit_var!=''){nama_riwayat_penyakit_var +="#"+$(this).find('td:eq(1)').text()}else{nama_riwayat_penyakit_var +=$(this).find('td:eq(1)').text()};
		
	});
	$("#riwayat_penyakit_detail").val(nama_riwayat_penyakit_var);
	
	let nama_peralatan_medis_var='';
	let nama_peralatan_medis_var_id='';
	$('#index_peralatan_medis tbody tr').each(function() {
		if (nama_peralatan_medis_var!=''){nama_peralatan_medis_var +="#"+$(this).find('td:eq(1)').text()}else{nama_peralatan_medis_var +=$(this).find('td:eq(1)').text()};
		if (nama_peralatan_medis_var_id!=''){nama_peralatan_medis_var_id +="#"+$(this).find(".nama_peralatan_medis_var_id").val()}else{nama_peralatan_medis_var_id +=$(this).find(".nama_peralatan_medis_var_id").val()};
		
	});
	$("#peralatan_medis_detail").val(nama_peralatan_medis_var);
	$("#peralatan_medis_detail_id").val(nama_peralatan_medis_var_id);

	set_nomor();
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
}
function set_nomor(){
	let no=1;
	$('#index_pengobatan tbody tr').each(function() {
		$(this).find('td:eq(0)').text(no);
		no++;
	});
	no=1;
	$('#index_riwayat_penyakit tbody tr').each(function() {
		$(this).find('td:eq(0)').text(no);
		no++;
	});
	no=1;
	$('#index_peralatan_medis tbody tr').each(function() {
		$(this).find('td:eq(0)').text(no);
		no++;
	});

}
function generate_tabel(){
	
	let pengobatan_detail = $("#pengobatan_detail").val().split("#");
	let tabel='';
	if (pengobatan_detail){
	console.log(pengobatan_detail);
		
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_pengobatan"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_pengobatan"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	pengobatan_detail.forEach(myFunction)
	let no=1;
	function myFunction(item, index, arr) {
		if (item!==''){
		tabel +="<tr>";
		tabel +='<td class="text-center">'+(index+1)+'</td>';
		tabel +='<td class="text-left"><input type="hidden" class="nama_pengobatan_var" value="'+pengobatan_detail[index]+'">'+pengobatan_detail[index]+'</td>';
		tabel +='<td class="text-center">'+btn+'</td>';
		tabel +="</tr>";
		}
		
	}
	$('#index_pengobatan tbody').append(tabel);
	}
	clear_input_pengobatan();
	
	let riwayat_penyakit_detail = $("#riwayat_penyakit_detail").val().split("#");
	tabel='';
	if (riwayat_penyakit_detail){
	console.log(riwayat_penyakit_detail);
		
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_riwayat_penyakit"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_riwayat_penyakit"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	riwayat_penyakit_detail.forEach(myFunction)
	let no=1;
	function myFunction(item, index, arr) {
		if (item!==''){
		tabel +="<tr>";
		tabel +='<td class="text-center">'+(index+1)+'</td>';
		tabel +='<td class="text-left"><input type="hidden" class="nama_riwayat_penyakit_var" value="'+riwayat_penyakit_detail[index]+'">'+riwayat_penyakit_detail[index]+'</td>';
		tabel +='<td class="text-center">'+btn+'</td>';
		tabel +="</tr>";
		}
		
	}
	$('#index_riwayat_penyakit tbody').append(tabel);
	}
	clear_input_riwayat_penyakit();

	let peralatan_medis_detail = $("#peralatan_medis_detail").val().split("#");
	let peralatan_medis_detail_id = $("#peralatan_medis_detail_id").val().split("#");
	tabel='';
	if (peralatan_medis_detail){
	console.log(peralatan_medis_detail);
		
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_peralatan_medis"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_peralatan_medis"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	peralatan_medis_detail.forEach(myFunction)
	let no=1;
	function myFunction(item, index, arr) {
		if (item!==''){
		tabel +="<tr>";
		tabel +='<td class="text-center">'+(index+1)+'</td>';
		tabel +='<td class="text-left"><input type="hidden" class="nama_peralatan_medis_var_id" value="'+peralatan_medis_detail_id[index]+'"><input type="hidden" class="nama_peralatan_medis_var" value="'+peralatan_medis_detail[index]+'">'+peralatan_medis_detail[index]+'</td>';
		tabel +='<td class="text-center">'+btn+'</td>';
		tabel +="</tr>";
		}
		
	}
	$('#index_peralatan_medis tbody').append(tabel);
	}
	clear_input_peralatan_medis();

	disabel_edit();
}
function simpan_pengobatan(){
	let tabel='';
	
	if ($("#nama_pengobatan_var").val()==""){
		sweetAlert("Maaf...", "Tentukan Pengobatan!", "error");
		return false;
	}
	let rowCount = $("#no_pengobatan_var").val();
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_pengobatan"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_pengobatan"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center">'+rowCount+'</td>';
	tabel +='<td class="text-left"><input type="hidden" class="nama_pengobatan_var" value="'+$("#nama_pengobatan_var").val()+'">'+$("#nama_pengobatan_var").val()+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	
	if ($("#rowindex_pengobatan").val() != '') {
		$('#index_pengobatan tbody tr:eq(' + $("#rowindex_pengobatan").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_pengobatan tbody').append(tabel);
	}
	// $("#index_pengobatan").append(tabel);
	clear_input_pengobatan();
	generate_array_penyakit();
}
$(document).on("click", ".btn_edit_pengobatan", function() {
	$("#rowindex_pengobatan").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_pengobatan").attr('disabled', true);
	$(".btn_hapus_pengobatan").attr('disabled', true);
	
	$("#no_pengobatan_var").val($(this).closest('tr').find("td:eq(0)").html());
	$("#nama_pengobatan_var").val($(this).closest('tr').find("td:eq(1) input").val());
	
	$("#nama_pengobatan_var").focus();
});
$(document).on("click", ".btn_hapus_pengobatan", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function clear_input_pengobatan(){
	
	let rowCount = ($('#index_pengobatan tr').length)-1;
	$("#nama_pengobatan_var").val("");
	$("#no_pengobatan_var").val(rowCount);
	
	$("#rowindex_pengobatan").val("");
	$(".btn_edit_pengobatan").attr('disabled', false);
	$(".btn_hapus_pengobatan").attr('disabled', false);
}
function simpan_riwayat_penyakit(){
	let tabel='';
	
	if ($("#nama_riwayat_penyakit_var").val()==""){
		sweetAlert("Maaf...", "Tentukan Riwayat Penyakit!", "error");
		return false;
	}
	let rowCount = $("#no_riwayat_penyakit_var").val();
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_riwayat_penyakit"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_riwayat_penyakit"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center">'+rowCount+'</td>';
	tabel +='<td class="text-left"><input type="hidden" class="nama_riwayat_penyakit_var" value="'+$("#nama_riwayat_penyakit_var").val()+'">'+$("#nama_riwayat_penyakit_var").val()+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	
	if ($("#rowindex_riwayat_penyakit").val() != '') {
		$('#index_riwayat_penyakit tbody tr:eq(' + $("#rowindex_riwayat_penyakit").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_riwayat_penyakit tbody').append(tabel);
	}
	// $("#index_riwayat_penyakit").append(tabel);
	clear_input_riwayat_penyakit();
	generate_array_penyakit();
}
$(document).on("click", ".btn_edit_riwayat_penyakit", function() {
	$("#rowindex_riwayat_penyakit").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_riwayat_penyakit").attr('disabled', true);
	$(".btn_hapus_riwayat_penyakit").attr('disabled', true);
	
	$("#no_riwayat_penyakit_var").val($(this).closest('tr').find("td:eq(0)").html());
	$("#nama_riwayat_penyakit_var").val($(this).closest('tr').find("td:eq(1) input").val());
	
	$("#nama_riwayat_penyakit_var").focus();
});
$(document).on("click", ".btn_hapus_riwayat_penyakit", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function clear_input_riwayat_penyakit(){
	
	let rowCount = ($('#index_riwayat_penyakit tr').length)-1;
	$("#nama_riwayat_penyakit_var").val("");
	$("#no_riwayat_penyakit_var").val(rowCount);
	
	$("#rowindex_riwayat_penyakit").val("");
	$(".btn_edit_riwayat_penyakit").attr('disabled', false);
	$(".btn_hapus_riwayat_penyakit").attr('disabled', false);
}
function simpan_peralatan_medis(){
	let tabel='';
	let nama_peralatan_medis_var=$("#nama_peralatan_medis_var option:selected").text();
	let nama_peralatan_medis_var_id=$("#nama_peralatan_medis_var").val();
	// alert(nama_peralatan_medis_var_id);
	if ($("#nama_peralatan_medis_var_id").val()==""){
		sweetAlert("Maaf...", "Tentukan Peralatan Medis!", "error");
		return false;
	}
	let rowCount = $("#no_peralatan_medis_var").val();
	let btn ='<span class="input-group-btn">';
		btn +='<button class="btn btn-info btn-xs btn_edit_peralatan_medis"   type="button"><i class="fa fa-pencil"></i></button>';
		btn +='<button class="btn btn-danger  btn-xs btn_hapus_peralatan_medis"  type="button"><i class="fa fa-times"></i></button>';
		btn +='</span>';
	
	tabel +='<td class="text-center">'+rowCount+'</td>';
	tabel +='<td class="text-left"><input type="hidden" class="nama_peralatan_medis_var_id" value="'+nama_peralatan_medis_var_id+'"><input type="hidden" class="nama_peralatan_medis_var" value="'+nama_peralatan_medis_var+'">'+nama_peralatan_medis_var+'</td>';
	tabel +='<td class="text-center">'+btn+'</td>';
	console.log(tabel);
	if ($("#rowindex_peralatan_medis").val() != '') {
		$('#index_peralatan_medis tbody tr:eq(' + $("#rowindex_peralatan_medis").val() + ')').html(tabel);
	} else {
		tabel ="<tr>"+tabel;
		tabel +="</tr>";
		$('#index_peralatan_medis tbody').append(tabel);
	}
	// $("#index_peralatan_medis").append(tabel);
	clear_input_peralatan_medis();
	generate_array_penyakit();
}
$("#index_peralatan_medis").on("click", ".btn_edit_peralatan_medis", function() {
	$("#rowindex_peralatan_medis").val($(this).closest('tr')[0].sectionRowIndex);			
	$(".btn_edit_peralatan_medis").attr('disabled', true);
	$(".btn_hapus_peralatan_medis").attr('disabled', true);
	// alert($(this).closest('tr').find(".nama_peralatan_medis_var_id").val());
	$("#no_peralatan_medis_var").val($(this).closest('tr').find("td:eq(0)").html());
	$("#nama_peralatan_medis_var").val($(this).closest('tr').find(".nama_peralatan_medis_var_id").val()).trigger('change');
	
	$("#nama_peralatan_medis_var").focus();
});
$(document).on("click", ".btn_hapus_peralatan_medis", function() {
	$(this).closest("tr").remove();
	generate_array_penyakit();
});
function clear_input_peralatan_medis(){
	
	let rowCount = ($('#index_peralatan_medis tr').length)-1;
	$("#nama_peralatan_medis_var").val("").trigger('change');
	$("#no_peralatan_medis_var").val(rowCount);
	
	$("#rowindex_peralatan_medis").val("");
	$(".btn_edit_peralatan_medis").attr('disabled', false);
	$(".btn_hapus_peralatan_medis").attr('disabled', false);
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
	}
}
function set_riwayat_penyakit(){
	let pengobatan=$("#pengobatan").val();
	if (pengobatan=='2'){
		$(".div_pengobatan").show();
	}else{
		$(".div_pengobatan").hide();
	}
	let riwayat_penyakit=$("#riwayat_penyakit").val();
	if (riwayat_penyakit=='2'){
		$(".div_riwayat").show();
	}else{
		$(".div_riwayat").hide();
	}
	let peralatan_medis=$("#peralatan_medis").val();
	if (peralatan_medis=='2'){
		$(".div_peralatan").show();
	}else{
		$(".div_peralatan").hide();
	}
	
}
$("#pengobatan,#riwayat_penyakit,#peralatan_medis").on("change", function(){
	set_riwayat_penyakit();
});
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
$("#riwayat_alergi").on("change", function(){
	
	set_riwayat_alergi();
});
function clear_input_alergi(){
	$("#alergi_id").val('');
	$("#btn_add_alergi").html('<i class="fa fa-plus"></i> Add');
	$("#input_jenis_alergi").val('');
	$("#input_detail_alergi").val('');
	$("#input_reaksi_alergi").val('');
}
function edit_alergi(id){
	$("#alergi_id").val(id);
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/edit_alergi_tf_hospital', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				
			},
		success: function(data) {
			$("#btn_add_alergi").html('<i class="fa fa-save"></i> Simpan');
			$("#input_jenis_alergi").val(data.input_jenis_alergi).trigger('change');
			$("#input_detail_alergi").val(data.input_detail_alergi);
			$("#input_reaksi_alergi").val(data.input_reaksi_alergi);
		}
	});
}

function set_riwayat_alergi(){
	let riwayat=$("#riwayat_alergi").val();
	if (riwayat=='2'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").show();
		$(".div_input_alergi").show();
		$("#li_alergi_1").addClass('active');
		$("#li_alergi_2").removeClass('active');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else if (riwayat=='1'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").hide();
		$(".div_input_alergi").hide();
		// // $('#alergi_2 > a').tab('show');
		// $('#alergi_1 > a').tab('hide');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else{
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
		$(".div_alergi_all").hide();
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	
		
	}
	
}

	
</script>