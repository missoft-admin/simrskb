<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	}	
	
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.text-bold {
		font-weight: bold;		
	}
	 <?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='his_rekon_obat'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_rekon_obat' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						// $tanggal_masuk_rs=HumanDateLong($tanggal_masuk_rs);
						$tanggalserahkan=HumanDateShort($tanggal_serahkan);
						$waktuserahkan=HumanTime($tanggal_serahkan);
						
						$tanggalserahkan_asal=HumanDateShort($tanggal_serahkan_asal);
						$waktuserahkan_asal=HumanTime($tanggal_serahkan_asal);

						$tanggalmasukrs=HumanDateShort($tanggal_masuk_rs);
						$waktumasukrs=HumanTime($tanggal_masuk_rs);

						
						$tanggalmasukrs_asal=HumanDateShort($tanggal_masuk_rs_asal);
						$waktumasukrs_asal=HumanTime($tanggal_masuk_rs_asal);

						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktumasukrs=date('H:i:s');
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggalmasukrs=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rekon">
	<?if ($st_lihat_rekon_obat=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_rekon_obat=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_rekon/input_rekon_obat" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >SAAT MASUK RUMAH SAKIT</h5>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-4">
								<div class="col-md-6">
									<label for="tanggalmasukrs">Waktu Masuk RS</label>
									<div class="input-group date">
										<input id="tanggalmasukrs"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalmasukrs ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="tanggalmasukrs">&nbsp;</label>
									<div class="input-group">
										<input id="waktumasukrs" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktumasukrs ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="col-md-7">
									<label for="obat_sebelum_masuk">Apakah Pasien Menggunakan Obat Sebelum Masuk Rawat Inap</label>
									<select id="obat_sebelum_masuk" name="obat_sebelum_masuk" class="<?=($obat_sebelum_masuk!=$obat_sebelum_masuk_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($obat_sebelum_masuk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="1" <?=($obat_sebelum_masuk == '1' ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($obat_sebelum_masuk == '0' ? 'selected="selected"' : '')?>>TIDAK</option>
									</select>
								</div>
								<div class="col-md-5">
									<label for="obat_alergi">Adakah Obat Yang Menyebabkan Alergi</label>
									<select id="obat_alergi" name="obat_alergi" class="<?=($obat_alergi!=$obat_alergi_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($obat_alergi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="1" <?=($obat_alergi == '1' ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($obat_alergi == '0' ? 'selected="selected"' : '')?>>TIDAK</option>
									</select>
								</div>
								
								
							</div>
						</div>
						<div class="form-group div_alergi" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >DAFTAR OBAT YANG MENYEBABKAN ALERGI</h5>
							</div>
						</div>
											
						<div class="form-group div_alergi" style="margin-top:-10px">
							<div class="col-md-12 ">
									<table style="width:100%" class="table table-bordered" id="tabel_alergi">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="30%">Nama Obat Yang Menyebabkan Alergi</th>
												<th width="30%">Keparahan Reaksi Alergi</th>
												<th width="25%">Bentuk Reaksi</th>
												<th width="10%" class="text-center">Action</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
							</div>
						</div>
						<div class="form-group div_konsumsi" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >DAFTAR OBAT YANG DIKONSUMSI DARI RUMAH</h5>
							</div>
						</div>
											
						<div class="form-group div_konsumsi" style="margin-top:-10px;">
							<div class="col-md-12 ">
								<table style="width:100%" class="table table-bordered" id="tabel_konsumsi">
									<thead>
										<tr>
											<th width="15%" class="text-center">Nama Dagang / <br>Generik / Fitofarmaka</th>
											<th width="10%" class="text-center">Dosis<br>(mg,ml,mcg,unit)</th>
											<th width="10%" class="text-center">Frekuensi</th>
											<th width="10%" class="text-center">Cara Pemberian</th>
											<th width="10%" class="text-center">Apakah Obat<br>Saat ini dikonsumsi</th>
											<th width="10%" class="text-center">Apakah Obat<br>dibawa dari rumah</th>
											<th width="10%" class="text-center">Obat Digunakan<br>Saat Dirawat</th>
											<th width="10%" class="text-center">Obat Diteruskan<br>Ketika Keluar RS</th>
											<th width="15%" class="text-center">Action</th>
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-7">
								<div class="col-md-6">
									<label for="fisik_serahkan">Fisik Obat Telah Diserahkan Ke Perawat</label>
									<select id="fisik_serahkan" name="fisik_serahkan" class="<?=($fisik_serahkan!=$fisik_serahkan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($fisik_serahkan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(290) as $row){?>
										<option value="<?=$row->id?>"  <?=($fisik_serahkan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($fisik_serahkan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="tanggalserahkan">Pada Tanggal</label>
									<div class="input-group date">
										<input id="tanggalserahkan"  class="<?=($tanggalserahkan!=$tanggalserahkan_asal?'edited':'')?> js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalserahkan ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="waktuserahkan">&nbsp;</label>
									<div class="input-group">
										<input id="waktuserahkan" class="<?=($waktuserahkan!=$waktuserahkan_asal?'edited':'')?> time-datepicker form-control " type="text"   value="<?= $waktuserahkan ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="col-md-12">
									<label for="ket_serahkan">Keterangan</label>
									<input id="ket_serahkan" class="<?=($ket_serahkan!=$ket_serahkan_asal?'edited':'')?> form-control " type="text"  value="{ket_serahkan}"  placeholder="Bentuk Reaksi" >
								</div>
								
							</div>
						</div>										
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >ASSESMEN FARMASI</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label for="hasil_rekon">Hasil Rekonsialisasi</label>
								<textarea id="hasil_rekon" class="form-control  <?=($hasil_rekon!=$hasil_rekon_asal?'edited':'')?>" name="story" rows="2" style="width:100%"><?=strip_tags($hasil_rekon)?></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-top:-15px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >INTRUKSI DOKTER</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label for="intruksi">Intruksi Dokter</label>
								<textarea id="intruksi" class="form-control  <?=($intruksi!=$intruksi_asal?'edited':'')?>" name="story" rows="2" style="width:100%"><?=strip_tags($intruksi)?></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-top:-15px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >DATA OBAT YANG DIBAWA PULANG PASIEN</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label for="obat_dibawa_pulang">Data Obat Yang Dibawa Pulang Pasien</label>
								<textarea id="obat_dibawa_pulang" class="form-control  <?=($obat_dibawa_pulang!=$obat_dibawa_pulang_asal?'edited':'')?>" name="story" rows="2" style="width:100%"><?=strip_tags($obat_dibawa_pulang)?></textarea>
							</div>
						</div>		
						<div class="form-group " style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >SERAH TERIMA PASIEN</h5>
							</div>
						</div>
						<div class="form-group " style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<table style="width:100%" class="table">
									<tr>
										<td colspan="2" class="text-center text-bold">TANDA TANGAN PADA SAAT PASIEN DIRAWAT
										</td>
										<td colspan="2" class="text-center text-bold">TANDA TANGAN PADA SAAT PASIEN PULANG</td>
									</tr>
									<tr>
										<td class="text-center text-bold" style="width:25%">PASIEN / KELUARGA <br><i class="text-muted">Patien / Family</i> </td>
										<td class="text-center text-bold" style="width:25%">TTD & NAMA PETUGAS <br><i class="text-muted">Signature Pharmacist</i> </td>
										<td class="text-center text-bold" style="width:25%">PASIEN / KELUARGA <br><i class="text-muted">Patien / Family</i> </td>
										<td class="text-center text-bold" style="width:25%">TTD & NAMA PETUGAS <br><i class="text-muted">Signature Pharmacist</i> </td>
										
									</tr>
									
									<tr>
										<td class="text-center text-bold" >
											<?if ($ttd_in_pasien){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_in_pasien?>" alt="" title="">
													
												</div>
											<?}else{?>
											<?}?>

										</td>
										<td class="text-center text-bold" >
											<?if ($ttd_petugas_in){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($ttd_petugas_in)?>" alt="" title="">
													
												</div>
											<?}else{?>
											<?}?>
										</td>
										<td class="text-center text-bold" >
											<?if ($ttd_out_pasien){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_out_pasien?>" alt="" title="">
													
												</div>
											<?}else{?>
											<?}?>
										</td>
										<td class="text-center text-bold" >
											<?if ($ttd_petugas_out){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($ttd_petugas_out)?>" alt="" title="">
													
												</div>
											<?}else{?>
											<?}?>
										</td>
										
									</tr>
									 <?
										// $ttd_petugas_in_nama='';
										// $ttd_petugas_in_nik='';
										
										// $ttd_petugas_out_nama='';
										// $ttd_petugas_out_nik='';
										// if ($ttd_petugas_in){
											// $data_petugas_in=get_nama_ppa_array($ttd_petugas_in);
											// $ttd_petugas_in_nama=$data_petugas_in['nama']
											// $ttd_petugas_in_nama=$data_petugas_in['nama']
										// }
										// if ($ttd_petugas_out){
											// $data_petugas_out=get_nama_ppa_array($ttd_petugas_out);
										// }
										
										
									 ?>
									<tr>
										<td class="text-center text-bold" ><?=($ttd_in_pasien?$ttd_in_pasien_nama:'')?><br><?=($ttd_in_pasien?HumandateLong($ttd_in_pasien_tanggal):'')?></td>
										<td class="text-center text-bold" ><?=get_nama_ppa($ttd_petugas_in)?><br><?=($ttd_petugas_in?HumandateLong($ttd_perugas_in_tanggal):'')?></td>
										<td class="text-center text-bold" ><?=($ttd_out_pasien?$ttd_out_pasien_nama:'')?><br><?=($ttd_out_pasien?HumandateLong($ttd_out_pasien_tanggal):'')?></td>
										<td class="text-center text-bold" ><?=get_nama_ppa($ttd_petugas_out)?><br><?=($ttd_petugas_out?HumandateLong($ttd_perugas_out_tanggal):'')?></td>
									</tr>
								</table>
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
									<h5 class="push-5 text-center text-primary">RIWAYAT {judul_header_eng}</h5>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
												<th width="10%">TTD Pasien<br>Masuk Rawat Inap</th>
												<th width="10%">TTD Pasien<br>Pulang</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<input type="hidden" id="pendaftaran_id_ranap_tmp" value="">
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Pasien </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama Pasien / Keluarga</label>
								</div>
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
						</div>
					</div>

				</div>
				<input type="hidden" readonly id="nama_field_ttd" value="">
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});

$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	
	$("#tanggal_masuk_rs").datetimepicker({
            format: "DD-MM-YYYY HH:mm",
            // stepping: 30
        });
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		list_index_history_edit();
		load_konsumsi_rekon_obat();
		load_alergi_rekon_obat();
		set_div();
		load_awal_assesmen=false;
	}
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	

});
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}
function load_konsumsi_rekon_obat(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_konsumsi tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_konsumsi_rekon_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				st_sedang_edit:$("#st_sedang_edit").val(),
				
				},
		success: function(data) {
			$("#tabel_konsumsi tbody").append(data.opsi);
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}

function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_rekon_obat', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}

$("#obat_sebelum_masuk,#obat_alergi").change(function(){
	set_div();
});
function load_alergi_rekon_obat(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_alergi tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_rekon_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				st_sedang_edit:$("#st_sedang_edit").val(),
				
				},
		success: function(data) {
			$("#tabel_alergi tbody").append(data.opsi);
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}
function set_div(){
	let obat_sebelum_masuk=$("#obat_sebelum_masuk").val();
	let obat_alergi=$("#obat_alergi").val();
	$(".div_alergi").hide();
	$(".div_konsumsi").hide();
	if (obat_alergi=='1'){
		$(".div_alergi").show();
	}
	if (obat_sebelum_masuk=='1'){
		$(".div_konsumsi").show();
	}
}

</script>