<style>
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	}	
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	.input-keterangan{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.text-bold {
		font-weight: bold;		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='setuju_rekon_obat'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='setuju_rekon_obat' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						// $tanggal_masuk_rs=HumanDateLong($tanggal_masuk_rs);
						$tanggalserahkan=HumanDateShort($tanggal_serahkan);
						$waktuserahkan=HumanTime($tanggal_serahkan);
						$tanggalmasukrs=HumanDateShort($tanggal_masuk_rs);
						$waktumasukrs=HumanTime($tanggal_masuk_rs);
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktumasukrs=date('H:i:s');
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggalmasukrs=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_rekon_obat=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rekon">
	<?if ($st_lihat_rekon_obat=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian  <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'Baru')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_rekon_obat=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_rekon_obat=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-disk"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-save"></i> Simpan Template</button>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >SAAT MASUK RUMAH SAKIT</h5>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-4">
								<div class="col-md-6">
									<label for="tanggalmasukrs">Waktu Masuk RS</label>
									<div class="input-group date">
										<input id="tanggalmasukrs"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalmasukrs ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="tanggalmasukrs">&nbsp;</label>
									<div class="input-group">
										<input id="waktumasukrs" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktumasukrs ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="col-md-7">
									<label for="obat_sebelum_masuk">Apakah Pasien Menggunakan Obat Sebelum Masuk Rawat Inap</label>
									<select id="obat_sebelum_masuk" name="obat_sebelum_masuk" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($obat_sebelum_masuk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="1" <?=($obat_sebelum_masuk == '1' ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($obat_sebelum_masuk == '0' ? 'selected="selected"' : '')?>>TIDAK</option>
									</select>
								</div>
								<div class="col-md-5">
									<label for="obat_alergi">Adakah Obat Yang Menyebabkan Alergi</label>
									<select id="obat_alergi" name="obat_alergi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($obat_alergi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="1" <?=($obat_alergi == '1' ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($obat_alergi == '0' ? 'selected="selected"' : '')?>>TIDAK</option>
									</select>
								</div>
								
								
							</div>
						</div>
						<div class="form-group div_alergi" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >DAFTAR OBAT YANG MENYEBABKAN ALERGI</h5>
							</div>
						</div>
											
						<div class="form-group div_alergi" style="margin-top:-10px">
							<div class="col-md-12 ">
									<table style="width:100%" class="table table-striped table-borderless table-header-bg" id="tabel_alergi">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="30%">Nama Obat Yang Menyebabkan Alergi</th>
												<th width="30%">Keparahan Reaksi Alergi</th>
												<th width="25%">Bentuk Reaksi</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
							</div>
						</div>
						<div class="form-group div_konsumsi" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >DAFTAR OBAT YANG DIKONSUMSI DARI RUMAH</h5>
							</div>
						</div>
											
						<div class="form-group div_konsumsi" style="margin-top:-10px;">
							<div class="col-md-12 ">
								<table style="width:100%" class="table table-striped table-borderless table-header-bg" id="tabel_konsumsi">
									<thead>
										<tr>
											<th width="15%" class="text-center">Nama Dagang / <br>Generik / Fitofarmaka</th>
											<th width="10%" class="text-center">Dosis<br>(mg,ml,mcg,unit)</th>
											<th width="10%" class="text-center">Frekuensi</th>
											<th width="10%" class="text-center">Cara Pemberian</th>
											<th width="10%" class="text-center">Apakah Obat<br>Saat ini dikonsumsi</th>
											<th width="10%" class="text-center">Apakah Obat<br>dibawa dari rumah</th>
											<th width="10%" class="text-center">Obat Digunakan<br>Saat Dirawat</th>
											<th width="10%" class="text-center">Obat Diteruskan<br>Ketika Keluar RS</th>
											<th width="15%" class="text-center">Action</th>
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div hidden>
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-7">
								<div class="col-md-6">
									<label for="fisik_serahkan">Fisik Obat Telah Diserahkan Ke Perawat</label>
									<select id="fisik_serahkan" name="fisik_serahkan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($fisik_serahkan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(290) as $row){?>
										<option value="<?=$row->id?>"  <?=($fisik_serahkan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($fisik_serahkan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="tanggalserahkan">Pada Tanggal</label>
									<div class="input-group date">
										<input id="tanggalserahkan"  class="auto_blur js-datepicker form-control " type="text"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?= $tanggalserahkan ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="waktuserahkan">&nbsp;</label>
									<div class="input-group">
										<input id="waktuserahkan" class="auto_blur time-datepicker form-control " type="text"   value="<?= $waktuserahkan ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="col-md-12">
									<label for="ket_serahkan">Keterangan</label>
									<input id="ket_serahkan" class="auto_blur form-control " type="text"  value="{ket_serahkan}"  placeholder="Bentuk Reaksi" >
								</div>
								
							</div>
						</div>										
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >ASSESMEN FARMASI</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label for="hasil_rekon">Hasil Rekonsialisasi</label>
								<textarea id="hasil_rekon" class="form-control auto_blur js-summernote" name="story" rows="2" style="width:100%"><?=$hasil_rekon?></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-top:-15px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >INTRUKSI DOKTER</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label for="intruksi">Intruksi Dokter</label>
								<textarea id="intruksi" class="form-control auto_blur js-summernote" name="story" rows="2" style="width:100%"><?=$intruksi?></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-top:-15px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >DATA OBAT YANG DIBAWA PULANG PASIEN</h5>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
								<label for="obat_dibawa_pulang">Data Obat Yang Dibawa Pulang Pasien</label>
								<textarea id="obat_dibawa_pulang" class="form-control auto_blur js-summernote" name="story" rows="2" style="width:100%"><?=$obat_dibawa_pulang?></textarea>
							</div>
						</div>		
						<div class="form-group " style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<label class="text-primary" >SERAH TERIMA PASIEN</h5>
							</div>
						</div>
						<div class="form-group " style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<table style="width:100%" class="table">
									<tr>
										<td colspan="2" class="text-center text-bold">TANDA TANGAN PADA SAAT PASIEN DIRAWAT
										</td>
										<td colspan="2" class="text-center text-bold">TANDA TANGAN PADA SAAT PASIEN PULANG</td>
									</tr>
									<tr>
										<td class="text-center text-bold" style="width:25%">PASIEN / KELUARGA <br><i class="text-muted">Patien / Family</i> </td>
										<td class="text-center text-bold" style="width:25%">TTD & NAMA PETUGAS <br><i class="text-muted">Signature Pharmacist</i> </td>
										<td class="text-center text-bold" style="width:25%">PASIEN / KELUARGA <br><i class="text-muted">Patien / Family</i> </td>
										<td class="text-center text-bold" style="width:25%">TTD & NAMA PETUGAS <br><i class="text-muted">Signature Pharmacist</i> </td>
										
									</tr>
									
									<tr>
										<td class="text-center text-bold" >
											<?if ($ttd_in_pasien){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_in_pasien?>" alt="" title="">
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="show_modal_ttd('ttd_in_pasien',<?=$assesmen_id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd_pasien('ttd_in_pasien',<?=$assesmen_id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success btn_ttd" onclick="show_modal_ttd('ttd_in_pasien',<?=$assesmen_id?>)" id="btn_ttd" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>

										</td>
										<td class="text-center text-bold" >
											<?if ($ttd_petugas_in){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($ttd_petugas_in)?>" alt="" title="">
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default btn-danger" onclick="hapus_ttd_petugas(1)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success btn_ttd" onclick="set_ttd_petugas(1)" id="btn_ttd" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
										</td>
										<td class="text-center text-bold" >
											<?if ($ttd_out_pasien){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_out_pasien?>" alt="" title="">
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="show_modal_ttd('ttd_in_pasien',<?=$assesmen_id?>)" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd_pasien('ttd_out_pasien',<?=$assesmen_id?>)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success btn_ttd" onclick="show_modal_ttd('ttd_in_pasien',<?=$assesmen_id?>)" id="btn_ttd" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
										</td>
										<td class="text-center text-bold" >
											<?if ($ttd_petugas_out){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($ttd_petugas_out)?>" alt="" title="">
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default btn-danger" onclick="hapus_ttd_petugas(2)" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success btn_ttd" onclick="set_ttd_petugas(2)" id="btn_ttd" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
										</td>
										
									</tr>
									 <?
										// $ttd_petugas_in_nama='';
										// $ttd_petugas_in_nik='';
										
										// $ttd_petugas_out_nama='';
										// $ttd_petugas_out_nik='';
										// if ($ttd_petugas_in){
											// $data_petugas_in=get_nama_ppa_array($ttd_petugas_in);
											// $ttd_petugas_in_nama=$data_petugas_in['nama']
											// $ttd_petugas_in_nama=$data_petugas_in['nama']
										// }
										// if ($ttd_petugas_out){
											// $data_petugas_out=get_nama_ppa_array($ttd_petugas_out);
										// }
										
										
									 ?>
									<tr>
										<td class="text-center text-bold" ><?=($ttd_in_pasien?$ttd_in_pasien_nama:'')?><br><?=($ttd_in_pasien?HumandateLong($ttd_in_pasien_tanggal):'')?></td>
										<td class="text-center text-bold" ><?=get_nama_ppa($ttd_petugas_in)?><br><?=($ttd_petugas_in?HumandateLong($ttd_perugas_in_tanggal):'')?></td>
										<td class="text-center text-bold" ><?=($ttd_out_pasien?$ttd_out_pasien_nama:'')?><br><?=($ttd_out_pasien?HumandateLong($ttd_out_pasien_tanggal):'')?></td>
										<td class="text-center text-bold" ><?=get_nama_ppa($ttd_petugas_out)?><br><?=($ttd_petugas_out?HumandateLong($ttd_perugas_out_tanggal):'')?></td>
									</tr>
								</table>
							</div>
						</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?><br><i><?=$judul_footer_eng?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
									<h5 class="push-5 text-center text-primary">RIWAYAT {judul_header_eng}</h5>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
												<th width="10%">TTD Pasien<br>Masuk Rawat Inap</th>
												<th width="10%">TTD Pasien<br>Pulang</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<input type="hidden" id="pendaftaran_id_ranap_tmp" value="">
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Pasien </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama Pasien / Keluarga</label>
								</div>
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<input type="hidden" readonly id="nama_field_ttd" value="">
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});

$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	
	$("#tanggal_masuk_rs").datetimepicker({
            format: "DD-MM-YYYY HH:mm",
            // stepping: 30
        });
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_konsumsi_rekon_obat();
		load_alergi_rekon_obat();
		set_div();
		load_awal_assesmen=false;
	}
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	

});
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function show_modal_ttd($nama_field_ttd,assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/get_tanda_tangan_rekon/', 
		dataType: "JSON",
		method: "POST",
		data : {
				nama_field_ttd: $nama_field_ttd,
				assesmen_id:assesmen_id,
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			// $("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
			$("#ttd_nama").val(data.ttd_nama);
			// $("#signature64").val(data.signature64);
			
		}
	});
	$("#nama_field_ttd").val($nama_field_ttd);
	$("#modal_ttd").modal('show');
}
function hapus_ttd_pasien(nama_field,ass_id){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/hapus_ttd_rekon/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:ass_id,
					nama_field:nama_field,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function hapus_ttd_pasien_index(nama_field,ass_id){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/hapus_ttd_rekon/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:ass_id,
					nama_field:nama_field,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					$('#index_history_kajian').DataTable().ajax.reload( null, false );
			  }
		});

	});
	
}
function hapus_ttd_petugas(nama_field){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/hapus_ttd_petugas_rekon/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					nama_field:nama_field,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function set_ttd_petugas(nama_field){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Tandatangani Petugas?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/set_ttd_petugas_rekon/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					nama_field:nama_field,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function simpan_ttd(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var nama_field=$("#nama_field_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var status_assemen=$("#status_assemen").val();
	if (ttd_nama==''){
		sweetAlert("Maaf...", "Isi Nama Tanda Tangan!", "error");
		return false;

	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/save_ttd_rekon/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				nama_field:nama_field,
				ttd_nama:ttd_nama,
		  },success: function(data) {
			  if (status_assemen=='0'){
				  $("#assesmen_id").val('');
				  $("#modal_ttd").modal('hide');
				  $('#index_history_kajian').DataTable().ajax.reload( null, false );
			  }else{
				location.reload();
				  
			  }
			}
		});
}

$("#obat_sebelum_masuk,#obat_alergi").change(function(){
	set_div();
});
function set_div(){
	let obat_sebelum_masuk=$("#obat_sebelum_masuk").val();
	let obat_alergi=$("#obat_alergi").val();
	$(".div_alergi").hide();
	$(".div_konsumsi").hide();
	if (obat_alergi=='1'){
		$(".div_alergi").show();
	}
	if (obat_sebelum_masuk=='1'){
		$(".div_konsumsi").show();
	}
}
function edit_alergi(id){
	$("#nama_obat_id").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/edit_alergi_rekon_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			// $("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
			$("#nama_obat").val(data.nama_obat);
			$("#bentuk_reaksi").val(data.bentuk_reaksi);
			$("#keparahan_alergi").val(data.keparahan_alergi).trigger('change');
			$("#keparahan_alergi").val(data.keparahan_alergi);
		}
	});
}
function hapus_alergi(id){
	// $("#nama_obat_id").val(id);
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Alergi?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_alergi_rekon_obat', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus Data.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
							load_alergi_rekon_obat();
					}
				}
			});
		});
		
	// $("#cover-spin").show();
	// $.ajax({
		// url: '{site_url}Tpendaftaran_ranap_erm/hapus_alergi_rekon_obat', 
		// dataType: "JSON",
		// method: "POST",
		// data : {
				// id:id,
				
			// },
		// success: function(data) {
			// $("#cover-spin").hide();
			// // $("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
			// $("#nama_obat").val(data.nama_obat);
			// $("#bentuk_reaksi").val(data.bentuk_reaksi);
			// $("#keparahan_alergi").val(data.keparahan_alergi);
		// }
	// });
}
function clear_alergi(){
	$("#keparahan_alergi").val('').trigger('change');
	$("#bentuk_reaksi").val('');
	$("#nama_obat").val('');
	$("#nama_obat_id").val('');
}
function simpan_alergi(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_obat_id=$("#nama_obat_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let nama_obat=$("#nama_obat").val();
	let keparahan_alergi=$("#keparahan_alergi").val();
	let bentuk_reaksi=$("#bentuk_reaksi").val();
	
	let idpasien=$("#idpasien").val();
	if (nama_obat==''){
		sweetAlert("Maaf...", "Isi nama_obat!", "error");
		return false;

	}
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_alergi_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_obat_id:nama_obat_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					nama_obat:nama_obat,
					keparahan_alergi:keparahan_alergi,
					bentuk_reaksi:bentuk_reaksi,
				},
			success: function(data) {
				clear_alergi();
				$("#cover-spin").hide();
				if (data==false){
				

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' nama_obat Berhasil Disimpan'});
					load_alergi_rekon_obat();
				}
			}
		});
	}
	
}
function setuju_satu($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan setuju  ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}trekon_obat/setuju_satu', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Proses.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					load_konsumsi_rekon_obat();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
function load_alergi_rekon_obat(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_alergi tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_alergi_rekon_obat_setuju', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				st_sedang_edit:$("#st_sedang_edit").val(),
				
				},
		success: function(data) {
			$("#tabel_alergi tbody").append(data.opsi);
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}
function edit_konsumsi(id){
	$("#konsumsi_id").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/edit_konsumsi_rekon_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			// $("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
			$("#nama_generik").val(data.nama_generik);
			$("#dosis").val(data.dosis);
			$("#cara").val(data.cara);
			$("#frekuensi").val(data.frekuensi);
			$("#obat_saat_ini").val(data.obat_saat_ini).trigger('change');
			$("#obat_dibawa").val(data.obat_dibawa).trigger('change');
			$("#obat_digunakan").val(data.obat_digunakan).trigger('change');
			$("#obat_diteruskan").val(data.obat_diteruskan).trigger('change');
		}
	});
}

function hapus_konsumsi(id){
	// $("#nama_obat_id").val(id);
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_konsumsi_rekon_obat', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus Data.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
							load_konsumsi_rekon_obat();
					}
				}
			});
		});
		

}
function clear_konsumsi(){
	$("#obat_diteruskan").val('').trigger('change');
	$("#obat_digunakan").val('').trigger('change');
	$("#obat_dibawa").val('').trigger('change');
	$("#obat_saat_ini").val('').trigger('change');
	$("#bentuk_reaksi").val('');
	$("#cara").val('');
	$("#frekuensi").val('');
	$("#dosis").val('');
	$("#nama_generik").val('');
	$("#konsumsi_id").val('');
}
function simpan_konsumsi(){
	let assesmen_id=$("#assesmen_id").val();
	let konsumsi_id=$("#konsumsi_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let nama_generik=$("#nama_generik").val();
	let dosis=$("#dosis").val();
	let frekuensi=$("#frekuensi").val();
	let cara=$("#cara").val();
	let obat_saat_ini=$("#obat_saat_ini").val();
	let obat_diteruskan=$("#obat_diteruskan").val();
	let obat_dibawa=$("#obat_dibawa").val();
	let obat_digunakan=$("#obat_digunakan").val();
	
	let idpasien=$("#idpasien").val();
	if (nama_generik==''){
		sweetAlert("Maaf...", "Isi nama_generik!", "error");
		return false;

	}
	
	$("#cover-spin").show();
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/simpan_konsumsi_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					konsumsi_id:konsumsi_id,
					assesmen_id:assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					nama_generik:nama_generik,
					dosis:dosis,
					frekuensi:frekuensi,
					cara:cara,
					obat_saat_ini:obat_saat_ini,
					obat_dibawa:obat_dibawa,
					obat_digunakan:obat_digunakan,
					obat_diteruskan:obat_diteruskan,
				},
			success: function(data) {
				clear_konsumsi();
				$("#cover-spin").hide();
				if (data==false){
				

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' nama_obat Berhasil Disimpan'});
					load_konsumsi_rekon_obat();
				}
			}
		});
	}
	
}
function load_konsumsi_rekon_obat(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_konsumsi tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_konsumsi_rekon_obat_setuju', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				st_sedang_edit:$("#st_sedang_edit").val(),
				
				},
		success: function(data) {
			$("#tabel_konsumsi tbody").append(data.opsi);
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".btn_setuju").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_with_template_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_alergi(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat nama_obat Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/copy_catata_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					assesmen_id:assesmen_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				load_alergi_rekon_obat();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/create_template_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/batal_template_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});
 $(document).find('.js-summernote').on('summernote.blur', function() {
   if ($("#st_edited").val()=='0'){
	 
	   
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
  });
$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
// $(".input-default").blur(function(){
	// simpan_default_warna();	
// });
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
function simpan_assesmen(){
	// alert($("#keluhan_utama").val());
	// return false;
	if (load_awal_assesmen==false){
	if (status_assemen !='2'){
		
	
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		// if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/save_rekon_obat', 
				dataType: "JSON",
				method: "POST",
				data : {
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						
						tanggalmasukrs : $("#tanggalmasukrs").val(),
						waktumasukrs : $("#waktumasukrs").val(),
						tanggalserahkan : $("#tanggalserahkan").val(),
						waktuserahkan : $("#waktuserahkan").val(),
						obat_sebelum_masuk : $("#obat_sebelum_masuk").val(),
						obat_alergi : $("#obat_alergi").val(),
						fisik_serahkan : $("#fisik_serahkan").val(),
						tanggal_serahkan : $("#tanggal_serahkan").val(),
						ket_serahkan : $("#ket_serahkan").val(),
						hasil_rekon : $("#hasil_rekon").val(),
						intruksi : $("#intruksi").val(),
						obat_dibawa_pulang : $("#obat_dibawa_pulang").val(),

					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_index_template_rekon_obat', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/edit_template_rekon_obat', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7,8,9] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_history_pengkajian_rekon_obat', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function edit_history_assesmen(id,pendaftaran_id_ranap){
		$("#pendaftaran_id_ranap_tmp").val(pendaftaran_id_ranap);
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let pendaftaran_id_ranap_tmp=$("#pendaftaran_id_ranap_tmp").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/save_edit_rekon_obat', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
						window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id_ranap_tmp+"/erm_rekon/input_rekon_obat'); ?>";
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_rekon_obat', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/hapus_record_rekon_obat', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	
</script>