<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		
</style>

<?if ($menu_kiri=='input_layanan_fisio'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_layanan_fisio' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
		// echo $tanggal_pernyataan;
	if ($norujukan){
		
		
		$tanggalheader=HumanDateShort($tanggal);
		$waktuheader=HumanTime($tanggal);
		
		$tgltransaksi=HumanDateShort($tanggal_transaksi);
		$waktutransaksi=HumanTime($tanggal_transaksi);
	}else{
		$tanggalheader=date('d-m-Y');
		$waktuheader=date('H:i:s');
		
		$tgltransaksi=date('d-m-Y');
		$waktutransaksi=date('H:i:s');
	}
	
	$disabel_input='';
	$disabel_cetak='disabled';
	
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-note"></i> Input Tindakan</a>
				</li>
				<li class="">
					<a href="#tab_2" onclick="list_history_layanan_fisio()"><i class="fa fa-history"></i> Riwayat Tindakan</a>
				</li>
				<li class="">
					<a href="#tab_3"><i class="fa fa-copy"></i> Paket</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<input type="hidden" id="idtipe_poli" value="{idtipe_poli}" >		
					<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}" >		
					<input type="hidden" id="idrekanan" value="{idrekanan}" >		
					<input type="hidden" id="idpoliklinik" value="{idpoliklinik}" >		
					<input type="hidden" id="statuspasienbaru" value="{statuspasienbaru}" >		
					<input type="hidden" id="pertemuan_id" value="{pertemuan_id}" >		
					<input type="hidden" id="transaksi_id" value="" >		
					<input type="hidden" id="st_browse" value="0" >		
					<input type="hidden" id="mppa_id" value="{mppa_id}" >		
					<input type="hidden" id="iddokter_ppa" value="{iddokter_ppa}" >		
					<input type="hidden" id="iddokter" value="{iddokter}" >		
					<input type="hidden" id="idtindakan" value="{idtindakan}" >		
					<input type="hidden" id="idrujukan" value="{idrujukan}" >		
					<input type="hidden" id="norujukan" value="{norujukan}" >		
					<div class="row">
						<div class="col-md-12">
								<h4 class="font-w700 push-5 text-center text-primary">Input Tindakan</h4>
								<h4 class="font-w700 push-5 text-center text-danger"><i>{norujukan}</i></h4>
						</div>
					</div>
					<?if($statuskasir_rajal!='2'){?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p>Info <a class="alert-link" href="javascript:void(0)"> Auto Save </a>!</p>
								</div>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<a href="{base_url}trujukan_fisioterapi/print_bukti_pemeriksaan/<?=$idrujukan?>" target="_blank" class="btn btn-info" hidden type="button"><i class="fa fa-print"></i> CETAK</a>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="idtipe">Tanggal Transaksi Header</label>
									<div class="input-group date">
										<input tabindex="1" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggalheader" placeholder="HH/BB/TTTT" name="tanggalheader" value="<?= $tanggalheader ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Waktu Tansaksi Tindakan</label>
									<div class="input-group date">
										<input tabindex="2" type="text"  class="time-datepicker form-control " id="waktuheader" name="waktuheader" value="<?= $waktuheader ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
									
								</div>
								
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="idtipe">Tanggal Tindakan</label>
									<div class="input-group date">
										<input tabindex="1" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tgltransaksi" placeholder="HH/BB/TTTT" name="tgltransaksi" value="<?= $tgltransaksi ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Waktu Tindakan</label>
									<div class="input-group date">
										<input tabindex="2" type="text"  class="time-datepicker form-control " id="waktutransaksi" name="waktutransaksi" value="<?= $waktutransaksi ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
									
								</div>
								<div class="col-md-3 ">
									<label for="profesi_id">Jenis Tenaga</label>
									<select tabindex="3" id="profesi_id"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach(list_variable_ref(21) as $r){?>
											<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
								<div class="col-md-5 ">
									<label for="totalkeseluruhan">Dokter / (PPA)</label>
										<div class="input-group">
											<select tabindex="3" id="mppa_id_pelaksana"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih PPA Pelaksana" required>
												<option value="#" selected>Pilih Opsi</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>" <?=($mppa_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
											<span class="input-group-addon"><i class="fa fa-user"></i></span>
										</div>
								</div>
							</div>
							
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								<div class="col-md-4 ">
									<label for="rowpath">Header</label>
										<select id="rowpath" style="width:100%"  tabindex="5" data-placeholder="Cari Layanan" class="form-control js-select2">
											<?foreach($list_header_path as $r){?>
											<option value="<?=$r->path?>" selected><?=$r->nama?></option>
											<?}?>
										</select>
								</div>
								<div class="col-md-8 ">
									<label for="idpelayanan">Nama Tindakan</label>
									<div class="input-group">
										<select id="idpelayanan" style="width:100%"  tabindex="5" data-placeholder="Cari Layanan" class="form-control js-select2"></select>
										<span class="input-group-btn">
											<button onclick="show_modal_layanan_fisio()" title="Cari Layanan" class="btn btn-primary" type="button" id="search_obat_1"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
								
							</div>
							
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								
								<div class="col-md-2 ">
									<label for="jasasarana">Jasa Sarana</label>
									<input class="form-control number" disabled type="text" id="jasasarana" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="jasapelayanan">Jasa Pelayanan</label>
									<input class="form-control number" disabled type="text" id="jasapelayanan" value="">
								</div>
								<div class="col-md-3 ">
									<label for="hargajual">Jasa Pelayanan Disc</label>
									<div class="input-group">
										<input class="form-control decimal diskon" <?=$disabel_input?> type="text" id="diskon"  value="0" placeholder="%">
										<span class="input-group-addon">% / Rp.</span>
										<input class="form-control number diskon" <?=$disabel_input?> type="text" id="diskonRp" value="0" placeholder="Rp" >
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="hargadasar">BHP</label>
									<input class="form-control number" disabled type="text" id="bhp" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="hargadasar">Biaya Perawatan</label>
									<input class="form-control number" disabled type="text" id="biayaperawatan" value="">
									
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-3 ">
									<label for="idtipe">Tarif</label>
									<input class="form-control number" disabled type="text" id="total" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Kuantitas</label>
									<input class="form-control number" type="text" id="kuantitas" value="">
									
								</div>
								
								<div class="col-md-3">
									<label for="totalkeseluruhan">Total</label>
									<div class="input-group">
										<input class="form-control number" type="text" disabled id="totalkeseluruhan" value="">
										<span class="input-group-btn">
											<button onclick="add_tindakan()" title="Add barang" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Add </button>
											<button onclick="clear_layanan_fisio()" title="Clear barang" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-header-bg" id="index_transaksi">
											<thead>
												<tr>
													<th width="15%" class="text-center">PELAKSANA TINDAKAN</th>
													<th width="20%" class="text-center">TINDAKAN</th>
													<th width="8%" class="text-center">JASA SARANA </th>
													<th width="8%" class="text-center">JASA PELAYANAN </th>
													<th width="8%" class="text-center">BHP</th>
													<th width="8%" class="text-center">BIAYA PERAWATAN</th>
													<th width="8%" class="text-center">TARIF</th>
													<th width="5%" class="text-center">QTY</th>
													<th width="10%" class="text-center">TOTAL</th>
													<th width="10%" class="text-center">ACTION</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot >
												<tr>
													<td colspan="7" class="text-right"><strong>TOTAL</strong></td>
													<td class="text-right"><label id="total_kuantitas"></label></td>
													<td class="text-right"><label id="total_penjualan"></label></td>
													<td></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>	
					<?}?>
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >Input Tindakan</h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
					<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">DAFTAR INPUT Tindakan</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<?foreach($list_poli as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_layanan_fisio()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_layanan_fisio">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">No Pendaftaran</th>
												<th width="10%">Tanggal Kunjungan</th>
												<th width="10%">Tujuan</th>
												<th width="10%">Poliklinik</th>
												<th width="15%">Dokter</th>
												<th width="15%">Total</th>
												<th width="10%">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						</div>
						<hr class="push-5-b">
						<div id="div_history">
						<div class="row">
							<div class="form-group">
								<div class="col-md-4 ">
									
								</div>
								<div class="col-md-8 ">
									<div class="pull-right push-10-r">
										<button class="btn btn-sm btn-default" onclick="back_history()" type="button"><i class="fa fa-reply"></i> Kembali </button>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">HISTORY INPUT TINDAKAN</h4>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-header-bg" id="index_transaksi_history">
												<thead>
												<tr>
													<th width="15%" class="text-center">PELAKSANA TINDAKAN</th>
													<th width="20%" class="text-center">TINDAKAN</th>
													<th width="8%" class="text-center">JASA SARANA </th>
													<th width="8%" class="text-center">JASA PELAYANAN </th>
													<th width="8%" class="text-center">BHP</th>
													<th width="8%" class="text-center">BIAYA PERAWATAN</th>
													<th width="8%" class="text-center">TARIF</th>
													<th width="5%" class="text-center">QTY</th>
													<th width="10%" class="text-center">TOTAL</th>
													<th width="10%" class="text-center">ACTION</th>
												</tr>
											</thead>
												<tbody></tbody>
											
												<tfoot >
													<tr>
														<td colspan="4" class="text-right"><strong>TOTAL</strong></td>
														<td class="text-right"><label id="total_kuantitas_history"></label></td>
														<td class="text-right"><label id="total_penjualan_history"></label></td>
														<td colspan="2"></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>	
					</div>	
					
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
					
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row div_filter_paket">
						<input type="hidden" readonly id="paket_id" value="{paket_id}"> 
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" readonly id="status_paket" value="<?=$status_paket?>" >		
						<input type="hidden" readonly id="paket_detail_id" value="" >		
						<div class="col-md-12">
							<h4 class="font-w700 push-5 text-center text-primary">DATA PAKET</h4>
						</div>
					</div>
					<div class="row div_filter_paket">
						<div class="form-group">
							
							<div class="col-md-8 ">
								<?if ($status_paket=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_paket=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Paket Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_paket=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_paket=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								
							</div>
							<div class="col-md-4 ">
								<div class="pull-right push-10-r">
									<?if ($paket_id==''){?>
									<button class="btn btn-primary" id="btn_create_paket" onclick="create_paket_layanan_fisio()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?if ($paket_id!=''){?>
										<?if ($status_paket=='1'){?>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_paket_layanan_fisio()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_paket_layanan_fisio()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<?if ($status_paket=='1'){?>
							<div class="form-group" >
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										<label for="nama_paket">Nama Paket </label>
										<input  id="nama_paket" value="{nama_paket}" class="form-control"  type="text">
										
									</div>
												
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered" id="index_paket">
												<thead>
													<tr>
														<th width="25%" class="text-center">Tindakan</th>
														<th width="10%" class="text-center">Jasa Sarana</th>
														<th width="10%" class="text-center">Jasa Pelayanan</th>
														<th width="10%" class="text-center">BHP</th>
														<th width="10%"  class="text-center">Biaya Perawatan</th>
														<th width="10%" class="text-center">Tarif</th>
														<th width="5%" class="text-center">Kuantitas</th>
														<th width="10%" class="text-center">Total</th>
														<th width="15%" class="text-center">ACTION</th>
													</tr>
													<tr>
														<td>
															<div class="input-group">
																<select id="idpelayanan_paket" style="width:100%" tabindex="5" data-placeholder="Cari Layanan" class="form-control js-select2"></select>
																<span class="input-group-btn">
																	<button onclick="show_modal_layanan_fisio_paket()" title="Cari Layanan" class="btn btn-primary" type="button" id="search_obat_paket"><i class="fa fa-search"></i></button>
																</span>
															</div>
														</td>
														<td>
															<input  id="jasasarana_paket" readonly value="" class="form-control"  type="text">
															<input  id="st_browse_paket" readonly value="0" class="form-control"  type="hidden">
														</td>
														<td>
															<input  id="jasapelayanan_paket" readonly value="" class="form-control number"  type="text">
														</td>
														<td>
															<input  id="bhp_paket" readonly value="" class="form-control number"  type="text">
														</td>
														<td>
															<input  id="biayaperawatan_paket" readonly value="" class="form-control number"  type="text">
														</td>
														<td>
															<input  id="total_paket" readonly value="" class="form-control number"  type="text">
														</td>
														<td>
															<input  id="kuantitas_paket" value="" class="form-control number"  type="text">
														</td>
														<td>
															<input  id="totalkeseluruhan_paket" readonly value="" class="form-control number"  type="text">
														</td>
														
														<td>
															<div class="btn-group">
																<button class="btn btn-sm btn-primary" onclick="add_tindakan_paket()"  title="Tambah" type="button"><i class="fa fa-plus"></i> Add</button>
																<button class="btn btn-sm btn-danger" onclick="clear_paket_layanan_fisio()" title="Clear" type="button"><i class="fa fa-refresh"></i></button>
															</div>
														</td>
													</tr>
												</thead>
												<tbody></tbody>
												<tfoot >
													<tr>
														<td colspan="6" class="text-right"><strong>TOTAL</strong></td>
														<td class="text-right"><label id="total_kuantitas_paket"></label></td>
														<td class="text-right"><label id="total_penjualan_paket"></label></td>
														<td></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						<?}?>
					</div>	
					<?if ($status_paket=='0'){?>
					<div class="div_filter_paket">
					<div class="row">
							
							<div class="form-group">
								<div class="col-md-6 ">
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Nama Paket</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="nama_paket_filter" placeholder="Nama Paket" value="">
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_layanan_fisio_paket()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_layanan_fisio_paket">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="30%">Nama Paket</th>
												<th width="15%">Total</th>
												<th width="20%">Uer</th>
												<th width="30%">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						</div>
						
						<div class="div_history_paket">
						<div class="row div_history_paket">
							<div class="form-group">
								<div class="col-md-4 ">
									
								</div>
								<div class="col-md-8 ">
									<div class="pull-right push-10-r">
										<button class="btn btn-sm btn-default" onclick="back_history_paket()" type="button"><i class="fa fa-reply"></i> Kembali </button>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">DETAIL PAKET</h4>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-header-bg" id="index_transaksi_history_paket">
												<thead>
													<tr>
														<th width="25%" class="text-center">Tindakan</th>
														<th width="10%" class="text-center">Jasa Sarana</th>
														<th width="10%" class="text-center">Jasa Pelayanan</th>
														<th width="10%" class="text-center">BHP</th>
														<th width="10%"  class="text-center">Biaya Perawatan</th>
														<th width="10%" class="text-center">Tarif</th>
														<th width="5%" class="text-center">Kuantitas</th>
														<th width="10%" class="text-center">Total</th>
														
													</tr>
												</thead>
												<tbody></tbody>
												<tfoot >
													<tr>
														<td colspan="6" class="text-right"><strong>TOTAL</strong></td>
														<td class="text-right"><label id="total_kuantitas_history_paket"></label></td>
														<td class="text-right"><label id="total_penjualan_history_paket"></label></td>
														
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>	
					</div>	
					<?}?>
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >Input Data Paket</h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal fade in" id="modal_layanan_fisio" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Layanan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="rowpath_filter">Unit Pelayanan</label>
								<div class="col-md-8">
									<select id="rowpath_filter" style="width:100%"  tabindex="5" data-placeholder="Cari Layanan" class="form-control">
										<?foreach($list_header_path as $r){?>
										<option value="<?=$r->path?>" selected><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Sub Parent</label>
								<div class="col-md-8">
									<select id="sub_parent" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
							</div>
							
							
						</div>
						<div class="col-md-6">
							
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-3 control-label" for="tipeid">Nama Tarif Pelayanan</label>
								<div class="col-md-9">
									<input class="form-control" type="text" id="nama_tarif_filter" value="">
									<input class="form-control" type="hidden" id="form_akses" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_tindakan" onclick="LoadLayanan()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="tabel_layanan_fisio_filter" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="width:5%">#</th>
								<th style="width:40%">Nama</th>
								<th style="width:10%">Jasa Sarana</th>
								<th style="width:10%">Jasa Pelayanan</th>
								<th style="width:10%">BHP</th>
								<th style="width:10%">Biaya Perawatan</th>
								<th style="width:15%">Total Tarif</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_layanan_fisio_paket" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Layanan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Unit Pelayanan</label>
								<div class="col-md-8">
									<select tabindex="3" id="profesi_id_paket_filter"  class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach($list_unitpelayanan as $r){?>
										<option value="<?=$r->id?>" <?=($profesi_id_default==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Tipe Barang</label>
								<div class="col-md-8">
									<select id="idtipe_paket_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach($list_tipe as $r){?>
										<option value="<?=$r->id?>" <?=($idtipe==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
							
						</div>
						<div class="col-md-6">
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-9">
									<select id="idkat_paket_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-3 control-label" for="tipeid">Nama Barang</label>
								<div class="col-md-9">
									<input class="form-control" type="text" id="nama_tindakan_paket_filter" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="LoadLayananPaket()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="tabel_obat_paket_filter" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Layanan</th>
								<th>Nama Layanan</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>Catatan</th>
								<th>Kartu Stok</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_layanan_fisio_copy" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Konfirmasi Penggunaan Paket Layanan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12 text-primary">
							<label class="h5">Periksa Kembali data paket yang akan diterapkan.</label>
						</div>
					</div>
					<div class="row push-10-t">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
								<div class="col-md-4 ">
									<select tabindex="3" id="mppa_id_pelaksana_copy"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih PPA Pelaksana" required>
										<option value="#" selected>Pilih Opsi</option>
										<?foreach($list_ppa_all as $r){?>
										<option value="<?=$r->id?>" <?=($mppa_id==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<input class="form-control" type="hidden" id="array_paket_id" value="">
						</div>
					</div>
					<div class="row push-10-t">
						<div class="col-md-12">
							<table width="100%" id="tabel_layanan_fisio_paket_copy" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:5%">#</th>
										<th style="width:20%">Dokter / MPPA</th>
										<th style="width:20%">Nama</th>
										<th style="width:8%">Jasa Sarana</th>
										<th style="width:8%">Jasa Pelayanan</th>
										<th style="width:8%">BHP</th>
										<th style="width:8%">Biaya Perawatan</th>
										<th style="width:8%">Tarif</th>
										<th style="width:5%">Kuantitas</th>
										<th style="width:10%">Total Tarif</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="8" class="text-right"><strong>TOTAL</strong></td>
										<td class="text-right"><label id="total_kuantitas_history_paket_modal"></label></td>
										<td class="text-right"><label id="total_penjualan_history_paket_modal"></label></td>
										
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="row push-10-b">
						<div class="col-md-12 text-danger">
							<label class="h5">
								Diskon Akan mengikuti setting yang diatur dalam pengaturan diskon &nbsp;&nbsp;&nbsp; 
								<br>
								<br>
							</label>
						</div>
					<br>
					</div>
					<div class="row push-10-b">
						
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
					<button class="btn btn-sm btn-success" type="button" onclick="terapkan_copy()"><i class="fa fa-check"></i> Terapkan</button>
				</div>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	// show_modal_layana	n();
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var targetTab = $(e.target).attr('href');
		localStorage.setItem('activeTab', targetTab);
	});

	// Baca nilai dari localStorage dan tetapkan tab yang sesuai sebagai tab aktif
	var activeTab = localStorage.getItem('activeTab');
	if (activeTab) {
		$('.nav-tabs a[href="' + activeTab + '"]').tab('show');
	}

	$('ul.nav-tabs').on('click', 'li', function () {
		var activeTab = $(this).find('a').attr('href');
		localStorage.setItem('activeTab', activeTab);
	});
	
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	select2_layanan_fisio();
	select2_subparent();
	// clear_layanan_fisio();
	load_transaksi_layanan_fisio_list();
	// list_history_layanan_fisio();
	load_tab3();
	// load_layanan_fisio_paket();
});
$(document).on("keyup", "#diskon", function() {
	var jasapelayanan = $("#jasapelayanan").val();
	if ($("#diskon").val() == '') {
		$("#diskon").val(0)
	}
	if (parseFloat($(this).val()) > 100) {
		$(this).val(100);
	}

	var discount_rupiah = parseFloat(jasapelayanan * parseFloat($(this).val()) / 100);
	$("#diskonRp").val(discount_rupiah);
	hitung_total();
});

$(document).on("keyup", "#diskonRp", function() {
	var jasapelayanan = $("#jasapelayanan").val();
	if ($("#diskonRp").val() == '') {
		$("#diskonRp").val(0)
	}
	if (parseFloat($(this).val()) > jasapelayanan) {
		$(this).val(jasapelayanan);
	}

	var discount_percent = parseFloat((parseFloat($(this).val() * 100)) / jasapelayanan);
	$("#diskon").val(discount_percent);
	hitung_total();
});
function load_tab3(){
	if ($("#paket_id").val()!=''){
		load_layanan_fisio_paket();
	}else{
		list_history_layanan_fisio_paket();
		
	}
	
}
function select2_layanan_fisio(){
	$.ajax({
		url: '{site_url}tpoliklinik_trx/select2_layanan_fisio/',
		dataType: "json",
		type: 'POST',
		data: {
			rowpath : $("#rowpath").val(),
		  },
		success: function(data) {
			// alert(data);
			$("#idpelayanan").empty();
			$("#idpelayanan").append(data);
			
			$("#idpelayanan_paket").empty();
			$("#idpelayanan_paket").append(data);
		}
	});
	
}
function select2_subparent(){
	$.ajax({
		url: '{site_url}tpoliklinik_trx/select2_subparent/',
		dataType: "json",
		type: 'POST',
		data: {
			rowpath : $("#rowpath").val(),
		  },
		success: function(data) {
			// alert(data);
			$("#sub_parent").empty();
			$("#sub_parent").append(data);
		}
	});
	
}
function get_layanan_fisio_detail(){
	// console.log();
	$("#st_browse").val("0");
	if ($("#idpelayanan").val() != '') {
		$.ajax({
			url: '{site_url}tpoliklinik_trx/get_layanan_fisio_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idpelayanan:$("#idpelayanan").val(),
				idkelompokpasien:$("#idkelompokpasien").val(),
				idrekanan:$("#idrekanan").val(),
				idtipe_poli:$("#idtipe_poli").val(),
				idpoliklinik:$("#idpoliklinik").val(),
				statuspasienbaru:$("#statuspasienbaru").val(),
				pertemuan_id:$("#pertemuan_id").val(),
				profesi_id:$("#profesi_id").val(),
				idkelas:'0',
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Layanan Tidak ditemukan!", "error");
					return false;
				}
				$("#jasasarana").val(data.jasasarana);
				$("#jasapelayanan").val(data.jasapelayanan);
				$("#bhp").val(data.bhp);
				$("#diskon").val(data.diskon_persen);
				
				if (data.st_diskon=='1'){
					$(".diskon").attr('disabled',true);
				}else{
					$(".diskon").removeAttr('disabled');
				}
				
				$("#diskonRp").val(data.diskon_rp);
				$("#diskon").val(data.diskon_persen);
				$("#biayaperawatan").val(data.biayaperawatan);
				$("#total").val(data.total);
				
				if (parseFloat($("#kuantitas").val())==0){
					$("#kuantitas").val('1');
					
				}
				
				hitung_total();
				$("#kuantitas").focus().select();
				
			}
		});
	} else {

	}
}
$("#idpelayanan").change(function() {
	get_layanan_fisio_detail();
});

$("#kuantitas").keyup(function() {
	hitung_total();
});

function hitung_total(){
	
	let jasasarana=parseFloat($("#jasasarana").val());
	let jasapelayanan=parseFloat($("#jasapelayanan").val());
	let bhp=parseFloat($("#bhp").val());
	let biayaperawatan=parseFloat($("#biayaperawatan").val());
	let diskon=parseFloat($("#diskonRp").val());
	let total=jasasarana+jasapelayanan+bhp+biayaperawatan-diskon;
	console.log(total);
	let kuantitas=parseFloat($("#kuantitas").val());
	let totalkeseluruhan=total*kuantitas;
	$("#totalkeseluruhan").val(totalkeseluruhan);
	$("#total").val(total);
}

function show_modal_layanan_fisio(){
	$("#form_akses").val('non_paket');
	document.getElementById("modal_layanan_fisio").style.zIndex = "1201";
	$('#rowpath_filter,#sub_parent').select2({
	   dropdownParent: $('#modal_layanan_fisio')
	});
	LoadLayanan();
	$("#modal_layanan_fisio").modal('show');
	$("#cover-spin").hide();
}
function show_modal_layanan_fisio_paket(){
	$("#form_akses").val('paket');
	document.getElementById("modal_layanan_fisio").style.zIndex = "1201";
	$('#rowpath_filter,#sub_parent').select2({
	   dropdownParent: $('#modal_layanan_fisio')
	});
	LoadLayanan();
	$("#modal_layanan_fisio").modal('show');
	$("#cover-spin").hide();
}
function clear_layanan_fisio(){
	$("#idpelayanan").val("").trigger('change.select2');
	$("#jasasarana").val(0);
	$("#jasapelayanan").val(0);
	$("#diskon").val(0);
	$("#diskonRp").val(0);
	$("#bhp").val(0);
	$("#kuantitas").val(0);
	$("#biayaperawatan").val(0);
	$("#total").val('');
	$("#idpelayanan").select().focus();
	$("#totalkeseluruhan").val('');
	$("#transaksi_id").val('');
	$("#div_info").hide();
	$(".diskon").removeAttr('disabled');
}

function edit_layanan_fisio(id){
	$("#transaksi_id").val(id);
	// get_edit_layanan_fisio
	$("cover-spin").show();
	$.ajax({
	  url: '{site_url}tpoliklinik_trx/get_edit_layanan_fisio/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			id : id,
			
	  },success: function(data) {
			$("#waktutransaksi").val(data.waktutransaksi),
			$("#tgltransaksi").val(data.tgltransaksi),
			
			// $("#idtipe").val(data.idtipe).trigger('change.select2');
			// var newOption = new Option(data.nama_tindakan, data.idobat, true, true);
					
			// $("#idpelayanan").append(newOption);
			// kt.val(null).trigger('change');

			$("#idpelayanan").val(data.idpelayanan).trigger('change.select2');
			$("#jasasarana").val(data.jasasarana);
			$("#jasapelayanan").val(data.jasapelayanan);
			$("#diskonRp").val(data.jasapelayanan_disc);
			$("#bhp").val(data.bhp);
			$("#biayaperawatan").val(data.biayaperawatan);
			$("#total").val(data.total);
			$("#kuantitas").val(data.kuantitas);
			$("#totalkeseluruhan").val(data.totalkeseluruhan);
			$("#profesi_id").val(data.profesi_id).trigger('change');
			
			$("cover-spin").hide();
			$("#kuantitas").focus().select();
			hitung_total();
			sleep(500).then(() => {
			$("#mppa_id_pelaksana").val(data.mppa_id).trigger('change');
				
			});
		}
	});
}
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
function add_tindakan(){
	if (cek_duplicate_tindakan()==true){
		swal({
			title: "Gagal",
			text: "Tindakan Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#mppa_id_pelaksana").val()==null || $("#mppa_id_pelaksana").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan Pilih Pelaksana",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#idpelayanan").val()==null || $("#idpelayanan").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan Tentukan Tindakan",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (parseFloat($("#kuantitas").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	if (parseFloat($("#totalkeseluruhan").val())=='0' && parseFloat($("#diskonRp").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Harga dan Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_layanan_fisio/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				transaksi_id : $("#transaksi_id").val(),
				idrujukan : $("#idrujukan").val(),
				tgltransaksi : $("#tgltransaksi").val(),
				waktutransaksi : $("#waktutransaksi").val(),
				pendaftaran_id : $("#pendaftaran_id").val(),
				idpasien : $("#idpasien").val(),
				iddokter : $("#iddokter_ppa").val(),
				idpelayanan : $("#idpelayanan").val(),
				jasasarana : $("#jasasarana").val(),
				jasasarana_disc : 0,
				jasapelayanan : $("#jasapelayanan").val(),
				jasapelayanan_disc : $("#diskonRp").val(),
				bhp : $("#bhp").val(),
				bhp_disc : 0,
				biayaperawatan : $("#biayaperawatan").val(),
				biayaperawatan_disc : 0,
				total : $("#total").val(),
				kuantitas : $("#kuantitas").val(),
				diskon : 0,
				totalkeseluruhan : $("#totalkeseluruhan").val(),
				pendaftaran_id : $("#pendaftaran_id").val(),
				idpasien : $("#idpasien").val(),
				
				profesi_id : $("#profesi_id").val(),
				mppa_id : $("#mppa_id_pelaksana").val(),

		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			  $("#cover-spin").hide();
			  clear_layanan_fisio();
			  load_transaksi_layanan_fisio_list();
			}
		});
}
function load_transaksi_layanan_fisio_list(){
	$("#cover-spin").show();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idrujukan=$("#idrujukan").val();
		$('#index_transaksi tbody').empty();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/load_transaksi_layanan_fisio_list', 
			dataType: "json",
			type: "POST",
			data: {
					pendaftaran_id: pendaftaran_id,
					idrujukan: idrujukan,
			},
			success: function(data) {
				$('#index_transaksi tbody').append(data.tabel);
				$("#total_penjualan").text(data.total_penjualan);
				$("#total_kuantitas").text(data.total_kuantitas);
			
				$("#cover-spin").hide();
				
			}
		});
}
function hapus_layanan_fisio(id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Data Tindakan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/hapus_layanan_fisio', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				load_transaksi_layanan_fisio_list();
			}
		});
	});

}

function LoadLayanan() {
	$("#cover-spin").hide();
	var idtipe = $("#idtipe_poli").val();
	var idkelompokpasien = $("#idkelompokpasien").val();
	var idrekanan = $("#idrekanan").val();
	// alert('ID TIPE :'+idtipe+' KEL '+idkelompokpasien+' REK '+idrekanan);
	// return false;
	$('#tabel_layanan_fisio_filter').DataTable().destroy();
	var table = $('#tabel_layanan_fisio_filter').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpoliklinik_trx/getTindakanRawatJalan_fisio/',
			type: "POST",
			dataType: 'json',
			data: {
				idtipe: idtipe,
				idkelompokpasien: idkelompokpasien,
				idrekanan: idrekanan,
				form_akses: $("#form_akses").val(),
				rowpath: $("#rowpath").val(),
				sub_parent: $("#sub_parent").val(),
				nama_tarif_filter: $("#nama_tarif_filter").val(),
				
			}
		},
		columnDefs: [{
				"width": "5%",
				"targets": [0],
				"orderable": true
			},
			{
				"width": "10%",
				"targets": [2,3,4,5],
				"orderable": true
			},
			{
				"width": "15%",
				"targets": [6],
				"orderable": true
			},
			{
				"width": "40%",
				"targets": [1],
				"orderable": true
			},
			{  className: "text-right", targets:[2,3,4,5,6,0] },
		]
	});
}


$("#idtipe_filter").change(function() {
	$("#idtipe").val($("#idtipe_filter").val()).trigger('change');
	load_kategori();
});
function load_kategori() {
	// var idtipe = $("#idtipe_filter").val();
	// $('#idkat_filter').find('option').remove();
	// $('#idkat_filter').append('<option value="#" selected>- Pilih Semua -</option>');
	// if (idtipe != '#') {

		// $.ajax({
			// url: '{site_url}tstockopname/list_kategori/' + idtipe,
			// dataType: "json",
			// success: function(data) {
				// $.each(data, function(i, row) {

					// $('#idkat_filter')
						// .append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
				// });
			// }
		// });

	// }
}
function TreeView($level, $name) {
	// console.log($name);
	$indent = '';
	for ($i = 0; $i < $level; $i++) {
		if ($i > 0) {
			$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		} else {
			$indent += '';
		}
	}

	if ($i > 0) {
		$indent += '└─';
	} else {
		$indent += '';
	}

	return $indent + ' ' + $name;
}
function TreeView_select2($level, $name) {
	// console.log($name);
	$indent = '';
	for ($i = 0; $i < $level; $i++) {
		if ($i > 0) {
			$indent += '  ';
		} else {
			$indent += '';
		}
	}

	if ($i > 0) {
		$indent += '└─';
	} else {
		$indent += '';
	}

	return $indent + ' ' + $name;
}
$(document).on("click", ".selectLayanan", function() {
	clear_layanan_fisio();
	$("#st_browse").val("1");
	// alert($(this).data('nama'));
	let idpelayanan = ($(this).data('idpelayanan'));
	let nama = ($(this).data('nama'));
	// let newOption = new Option(idobat, nama, true, true);
	
	let newOption = new Option(nama, idpelayanan, true, true);
					
	$("#idpelayanan").append(newOption).trigger('change');
	$("#modal_layanan_fisio").modal('hide');

});
$(document).on("click", ".selectLayananPaket", function() {
	clear_paket_layanan_fisio();
	$("#st_browse_paket").val("1");
	let idpelayanan = ($(this).data('idpelayanan'));
	let nama = ($(this).data('nama'));
	// let newOption = new Option(idobat, nama, true, true);
	
	let newOption = new Option(nama, idpelayanan, true, true);
					
	$("#idpelayanan_paket").append(newOption).trigger('change');
	$("#modal_layanan_fisio").modal('hide');
	
	// // alert($(this).data('nama'));
	// let idobat = ($(this).data('idobat'));
	// let idtipe = ($(this).data('idtipe'));
	// $("#idtipe_paket").val(idtipe).trigger('change');
	// let nama = ($(this).data('nama'));
	// // let newOption = new Option(idobat, nama, true, true);
	
	// let newOption = new Option(nama, idobat, true, true);
					
	// $("#idpelayanan_paket").append(newOption).trigger('change');
	

});
function list_history_layanan_fisio(){
	$("#div_history").hide();
	$("#div_filter").show();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let iddokter=$("#iddokter").val();
	let idpoli=$("#idpoli").val();
	let notransaksi=$("#notransaksi").val();
	let tanggal_input_1=$("#tanggal_input_1").val();
	let tanggal_input_2=$("#tanggal_input_2").val();
	$('#index_history_layanan_fisio').DataTable().destroy();	
	$("#cover-spin").show();
	table = $('#index_history_layanan_fisio').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_trx/list_history_layanan_fisio', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_history_layanan_fisio_paket(){
	$(".div_filter_paket").show();
	$(".div_history_paket").hide();
	let tgl_input_1=$("#tgl_input_1").val();
	let tgl_input_2=$("#tgl_input_2").val();
	let nama_paket=$("#nama_paket_filter").val();
	
	$('#index_history_layanan_fisio_paket').DataTable().destroy();	
	$("#cover-spin").show();
	table = $('#index_history_layanan_fisio_paket').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					// {  className: "text-center", targets:[1,2,3,6,4,0,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_trx/list_history_layanan_fisio_paket', 
				type: "POST" ,
				dataType: 'json',
				data : {
						tgl_input_1:tgl_input_1,
						tgl_input_2:tgl_input_2,
						nama_paket:nama_paket,
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function lihat_data_layanan_fisio(pendaftaran_id){
	$("#cover-spin").show();
	$("#div_filter").hide();
	$("#div_history").show();
	$('#index_transaksi_history tbody').empty();
	$.ajax({
			url: '{site_url}tpoliklinik_trx/lihat_data_layanan_fisio/',
			dataType: "json",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
				   },
			success: function(data) {
				$('#index_transaksi_history tbody').append(data.tabel);
				$("#total_penjualan_history").text(data.total_penjualan);
				$("#total_kuantitas_history").text(data.total_kuantitas);
				$("#cover-spin").hide();
			}
		});
	scrollToBottom();
	// window.scrollTo(0, document.body.scrollHeight);
}
function lihat_data_layanan_fisio_paket(paket_id){
	$("#cover-spin").show();
	$(".div_filter_paket").hide();
	$(".div_history_paket").show();
	$('#index_transaksi_history_paket tbody').empty();
	$.ajax({
			url: '{site_url}tpoliklinik_trx/lihat_data_layanan_fisio_paket/',
			dataType: "json",
			method: "POST",
			data : {
					paket_id:paket_id,
				   },
			success: function(data) {
				$('#index_transaksi_history_paket tbody').append(data.tabel);
				$("#total_penjualan_history_paket").text(data.total_penjualan);
				$("#total_kuantitas_history_paket").text(data.total_kuantitas);
				$("#cover-spin").hide();
			}
		});
	scrollToBottom();
	// window.scrollTo(0, document.body.scrollHeight);
}
function back_history(){
	$("#div_filter").show();
	$("#div_history").hide();
}
function back_history_paket(){
	$(".div_filter_paket").show();
	$(".div_history_paket").hide();
}
function scrollToBottom() {
  window.scrollTo(0, document.body.scrollHeight);
}
function copy_layanan_fisio(id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Menerapkan Data Tindakan ke Transaksi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/copy_layanan_fisio', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					idrujukan : $("#idrujukan").val(),
					pendaftaran_id : $("#pendaftaran_id").val(),
					idpasien : $("#idpasien").val(),
					// profesi_id : $("#mppa_id_pelaksana_copy").val(),
					iddokter : $("#iddokter_ppa").val(),
					mppa_id : $("#mppa_id_pelaksana").val(),
					profesi_id : $("#profesi_id").val(),
					// pertemuan_id : $("#pertemuan_id").val(),
					// statuspasienbaru : $("#statuspasienbaru").val(),
					// idtipe_poli : $("#idtipe_poli").val(),
					// idkelompokpasien : $("#idkelompokpasien").val(),
					// idrekanan : $("#idrekanan").val(),
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==true){
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				load_transaksi_layanan_fisio_list();
					  
				  }else{
					  swal({
						title: "Gagal",
						text: "Data Tidak ada yang berhasil di insert",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
				  }
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Duplicate Data'});
			}
		});
	});
	

}
function create_paket_layanan_fisio(){
		
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Paket "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/create_paket_layanan_fisio', 
			dataType: "JSON",
			method: "POST",
			data : {
					
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
$("#idpelayanan_paket").change(function() {
	get_layanan_fisio_detail_paket();
});

function get_layanan_fisio_detail_paket(){
	
	$("#st_browse_paket").val("0");
	if ($("#idpelayanan_paket").val() != '') {
		$.ajax({
			url: '{site_url}tpoliklinik_trx/get_layanan_fisio_detail_paket/',
			dataType: "json",
			type: 'POST',
			data: {
				idpelayanan:$("#idpelayanan_paket").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Layanan Tidak ditemukan!", "error");
					return false;
				}
				$("#jasasarana_paket").val(data.jasasarana);
				$("#jasapelayanan_paket").val(data.jasapelayanan);
				$("#bhp_paket").val(data.bhp);
				$("#biayaperawatan_paket").val(data.biayaperawatan);
				$("#total_paket").val(data.total);
				
				
				if (($("#kuantitas_paket").val())==0 || ($("#kuantitas_paket").val())==''){
					$("#kuantitas_paket").val('1');					
				}
				hitung_total_paket();
				$("#kuantitas_paket").focus().select();
				
			}
		});
	} else {

	}
}
function hitung_total_paket(){
	let jasasarana=parseFloat($("#jasasarana_paket").val());
	let jasapelayanan=parseFloat($("#jasapelayanan_paket").val());
	let bhp=parseFloat($("#bhp_paket").val());
	let biayaperawatan=parseFloat($("#biayaperawatan_paket").val());
	let diskon=0;
	let total=jasasarana+jasapelayanan+bhp+biayaperawatan-diskon;
	console.log(total);
	let kuantitas=parseFloat($("#kuantitas_paket").val());
	let totalkeseluruhan=total*kuantitas;
	$("#totalkeseluruhan_paket").val(totalkeseluruhan);
	$("#total_paket").val(total);
}
$("#kuantitas_paket").keyup(function() {
	hitung_total_paket();
});
function clear_paket_layanan_fisio(){
	$("#idpelayanan_paket").val(null).trigger('change.select2');
	$("#jasasarana_paket").val(0);
	$("#jasapelayanan_paket").val(0);
	// $("#diskon").val(0);
	// $("#diskonRp").val(0);
	$("#bhp_paket").val(0);
	$("#kuantitas_paket").val(0);
	$("#biayaperawatan_paket").val(0);
	$("#total_paket").val('');
	$("#idpelayanan").select().focus();
	$("#totalkeseluruhan_paket").val('');
	$("#paket_detail_id").val('');
}
function add_tindakan_paket(){
	if ($("#idpelayanan_paket").val()==null || $("#idpelayanan_paket").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Layanan / Alkes",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (parseFloat($("#kuantitas_paket").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	if (parseFloat($("#totalkeseluruhan_paket").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Harga dan Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cek_duplicate_tindakan_paket()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_paket_layanan_fisio/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_detail_id : $("#paket_detail_id").val(),
				paket_id : $("#paket_id").val(),
				idpelayanan : $("#idpelayanan_paket").val(),
				kuantitas : $("#kuantitas_paket").val(),
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			  $("#cover-spin").hide();
			  clear_paket_layanan_fisio();
			  load_layanan_fisio_paket();
			}
		});
}
$(document).on("blur","#nama_paket",function(){
	simpan_paket();
});
function simpan_paket(){
	
	$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_paket_head_fisio/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_id : $("#paket_id").val(),
				status_paket : $("#status_paket").val(),
				nama_paket : $("#nama_paket").val(),
				st_edited : $("#st_edited").val(),
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Paket'});
			  if (data.status_paket!='1'){
				  location.reload();		
			  }
			  
			}
		});
}
function close_paket_layanan_fisio(){
	if ($("#nama_paket").val()==null || $("#nama_paket").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Nama Paket",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menyimpan Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#status_paket").val(2);
		
		simpan_paket();
	});		
	
}
function load_layanan_fisio_paket(){
	$("#cover-spin").show();
	let paket_id=$("#paket_id").val();
		$('#index_paket tbody').empty();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/load_layanan_fisio_paket', 
			dataType: "json",
			type: "POST",
			data: {
					paket_id: paket_id,
			},
			success: function(data) {
				$('#index_paket tbody').append(data.tabel);
				$("#total_penjualan_paket").text(data.total_penjualan);
				$("#total_kuantitas_paket").text(data.total_kuantitas);
			
				$("#cover-spin").hide();
				
			}
		});
}
function edit_paket_detail_layanan_fisio(id){
	$("#paket_detail_id").val(id);
	// get_edit_layanan_fisio
	$("cover-spin").show();
	$.ajax({
	  url: '{site_url}tpoliklinik_trx/get_edit_paket_layanan_fisio/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			id : id,
			
	  },success: function(data) {
			$("#kuantitas").val(data.kuantitas);
			$("#idpelayanan_paket").val(data.idpelayanan).trigger('change');
			// hitung_total_paket();
			$("cover-spin").hide();
			$("#kuantitas_paket").focus().select();
			// hitung_total_paket();
		}
	});
}
function hapus_paket_detail_layanan_fisio(id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Detail Paket ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/hapus_paket_detail_layanan_fisio', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				load_layanan_fisio_paket();
			}
		});
	});

}
function LoadLayananPaket() {
	var profesi_id = $("#profesi_id_paket_filter").val();
	var idtipe = $("#idtipe_paket_filter").val();
	var idkategori = $("#idkat_paket_filter").val();
	var nama_tindakan = $("#nama_tindakan_paket_filter").val();
	$('#tabel_obat_paket_filter').DataTable().destroy();
	var table = $('#tabel_obat_paket_filter').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpoliklinik_trx/get_obat_paket_filter/',
			type: "POST",
			dataType: 'json',
			data: {
				profesi_id: profesi_id,
				idtipe: idtipe,
				idkategori: idkategori,
				nama_tindakan: nama_tindakan,
			}
		},
		columnDefs: [{
				"width": "5%",
				"targets": [0],
				"orderable": true
			},
			{
				"width": "10%",
				"targets": [1, 3, 4],
				"orderable": true
			},
			{
				"width": "15%",
				"targets": [5],
				"orderable": true
			},
			{
				"width": "40%",
				"targets": [2],
				"orderable": true
			},
		]
	});
}
function cek_duplicate_tindakan(){
	let status_find=false;
	$('#index_transaksi tbody tr').each(function() {
		let idpelayanan=$(this).find(".cls_idpelayanan").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idpelayanan").val()==idpelayanan && $("#transaksi_id").val()!=transaksi_id){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function cek_duplicate_tindakan_paket(){
	let status_find=false;
	$('#index_paket tbody tr').each(function() {
		let idpelayanan=$(this).find(".cls_idpelayanan").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
		if ($("#idpelayanan_paket").val()==idpelayanan && $("#paket_detail_id").val()!=transaksi_id){
			status_find=true;
		}
	});
	return status_find;
}
function edit_layanan_fisio_paket(paket_id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Edit Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_paket_edit_layanan_fisio/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_id : paket_id,
				status_paket : 1,
				st_edited : 1,
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Paket'});
			  if (data.status_paket=='1'){
				  location.reload();		
			  }
			  
			}
		});
	});		
	
}
function batal_paket_layanan_fisio(){
	let paket_id=$("#paket_id").val();
	let st_edited=$("#st_edited").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Membatalkan Simpan Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}tpoliklinik_trx/batal_paket_layanan_fisio', 
			dataType: "JSON",
			method: "POST",
			data : {
					paket_id:paket_id,
					st_edited:st_edited,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});		
	
}
function hapus_layanan_fisio_paket(paket_id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
		  url: '{site_url}tpoliklinik_trx/hapus_layanan_fisio_paket/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_id : paket_id,
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Paket'});
			  list_history_layanan_fisio_paket();
			  
			}
		});
	});		
}

function copy_layanan_fisio_paket_detail(detail_id){
	document.getElementById("modal_layanan_fisio_paket").style.zIndex = "1201";
	$('#mppa_id_pelaksana_copy').select2({
	   dropdownParent: $('#modal_layanan_fisio_copy')
	});
	$("#array_paket_id").val(detail_id);
	$("#modal_layanan_fisio_copy").modal('show');
	LoadLayananCopy();
}
function copy_layanan_fisio_paket(detail_id){
	document.getElementById("modal_layanan_fisio_paket").style.zIndex = "1201";
	$('#mppa_id_pelaksana_copy').select2({
	   dropdownParent: $('#modal_layanan_fisio_copy')
	});
	$("#array_paket_id").val(detail_id);
	$("#modal_layanan_fisio_copy").modal('show');
	LoadLayananCopy();
}
function LoadLayananCopy() {
	var rowpath = $("#rowpath").val();
	var mppa_id_pelaksana_copy = $("#mppa_id_pelaksana_copy").val();
	var array_paket_id = $("#array_paket_id").val();
	$('#tabel_layanan_fisio_paket_copy tbody').empty();
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}tpoliklinik_trx/LoadLayananCopy_fisio/',
			dataType: "json",
			method: "POST",
			data : {
					mppa_id_pelaksana_copy: mppa_id_pelaksana_copy,
					array_paket_id: array_paket_id,
					rowpath: rowpath,
				   },
			success: function(data) {
				$('#tabel_layanan_fisio_paket_copy tbody').append(data.tabel);
				$("#total_penjualan_history_paket_modal").text(data.total_penjualan);
				$("#total_kuantitas_history_paket_modal").text(data.total_kuantitas);
				$('.mppa_id').select2({
				   dropdownParent: $('#modal_layanan_fisio_copy')
				});
				$("#cover-spin").hide();
			}
		});
	// scrollToBottom();
	// window.scrollTo(0, document.body.scrollHeight);
}
// function LoadLayananCopy() {
	// var rowpath = $("#rowpath").val();
	// var mppa_id_pelaksana_copy = $("#mppa_id_pelaksana_copy").val();
	// var array_paket_id = $("#array_paket_id").val();
	// $('#tabel_layanan_fisio_paket_copy').DataTable().destroy();
	// var table = $('#tabel_layanan_fisio_paket_copy').DataTable({
		// "pageLength": 10,
		// "ordering": false,
		// "processing": true,
		// "serverSide": true,
		// "autoWidth": false,
		// "fixedHeader": true,
		// "searching": true,
		// "order": [],
		// "ajax": {
			// url: '{site_url}tpoliklinik_trx/LoadLayananCopy/',
			// type: "POST",
			// dataType: 'json',
			// data: {
				// mppa_id_pelaksana_copy: mppa_id_pelaksana_copy,
				// array_paket_id: array_paket_id,
				// rowpath: rowpath,
			// }
		// },
		// columnDefs: [{
				// "width": "5%",
				// "targets": [0],
				// "orderable": true
			// },
			// {
				// "width": "10%",
				// "targets": [1, 3, 4,5],
				// "orderable": true
			// },
			// {
				// "width": "12%",
				// "targets": [6,7],
				// "orderable": true
			// },
			// {
				// "width": "30%",
				// "targets": [2],
				// "orderable": true
			// },
			// {  className: "text-right", targets:[4] },
			// {  className: "text-center", targets:[0,1,3,5,6,7] },
		// ]
	// });
// }
$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}tpoliklinik_trx/find_mppa/'+$(this).val()+'/'+$("#mppa_id").val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#iddokter_ppa").val($("#iddokter").val());
				$("#mppa_id_pelaksana").empty();
				$("#mppa_id_pelaksana").append(data);
			}
		});
	}else{
		
		$("#mppa_id_pelaksana").empty();
	}

});
$("#mppa_id_pelaksana").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}tpoliklinik_trx/get_iddokter_ppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				if (data!=''){
				$("#iddokter_ppa").val(data);
					
				}else{
					
				$("#iddokter_ppa").val($("#iddokter").val());
				}
			}
		});
	}
});
$("#mppa_id_pelaksana_copy").change(function(){
	LoadLayananCopy();
})
function terapkan_copy(){
	let arr_idpelayanan=[];
	let arr_idtrx=[];
	let arr_kuantitas=[];
	let arr_mppa_id=[];
	$('#tabel_layanan_fisio_paket_copy tbody tr').each(function() {
		let mppa_id=$(this).find(".mppa_id").val();
		let idpelayanan=$(this).find(".cls_idpelayanan").val();
		let idtrx=$(this).find(".cls_idtrx").val();
		let kuantitas=$(this).find(".cls_kuantitas").val();
		arr_idpelayanan.push(idpelayanan);
		arr_idtrx.push(idtrx);
		arr_kuantitas.push(kuantitas);
		arr_mppa_id.push(mppa_id);
	});
	console.log(arr_mppa_id);
	console.log(arr_idpelayanan);
	$("#modal_layanan_fisio_copy").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerapkan Data Layanan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
		  url: '{site_url}tpoliklinik_trx/terapkan_copy_paket_fisio/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				idrujukan : $("#idrujukan").val(),
				iddokter : $("#iddokter").val(),
				pendaftaran_id : $("#pendaftaran_id").val(),
				idpasien : $("#idpasien").val(),
				// idpoliklinik : $("#idpoliklinik").val(),
				// pertemuan_id : $("#pertemuan_id").val(),
				statuspasienbaru : $("#statuspasienbaru").val(),
				idtipe_poli : $("#idtipe_poli").val(),
				idkelompokpasien : $("#idkelompokpasien").val(),
				idrekanan : $("#idrekanan").val(),
				arr_idpelayanan : arr_idpelayanan,
				arr_idtrx : arr_idtrx,
				arr_kuantitas : arr_kuantitas,
				arr_mppa_id : arr_mppa_id,
		  },success: function(data) {
			  $("#cover-spin").hide();
			  if (data==true){
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  
			  }else{
				  swal({
					title: "Gagal",
					text: "Barang Tidak ada yang berhasil di insert",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			  }
			 $("#modal_layanan_fisio_copy").modal('hide');
			 load_transaksi_layanan_fisio_list();
			}
		});
	});
	
	
}
</script>