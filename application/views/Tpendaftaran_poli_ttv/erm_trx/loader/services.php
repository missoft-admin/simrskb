<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	function updateProsesKwitansiDetail(idrawatinap,idmetode,tipekontraktor,idkontraktor){
		$("#cover-spin").show();
		 $.ajax({
            url: '{site_url}Tbilling_ranap/updateProsesKwitansiDetail',
            method: 'POST',
            data: {idrawatinap:idrawatinap,idmetode:idmetode,tipekontraktor:tipekontraktor,idkontraktor:idkontraktor},
            success: function (data) {
				$("#cover-spin").hide();
                swal({
					title: "Berhasil!",
					text: message,
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
            }
        });
	}
    function updateTanggalPembayaranTransaksi(payload) {
        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateTanggalPembayaran',
            method: 'POST',
            data: payload,
            success: function (data) {
                $("#message_pembayaran").html('<div class="alert alert-success" style="padding: 8px 10px 5px 15px;"><p><b>Berhasil!</b> Tanggal Pembayaran Telah di Ubah</p></div>');

                setTimeout(function () {
                    $("#message_pembayaran").html('<button class="btn btn-success" id="btnUpdateTanggalPembayaran" style="font-size:13px;"><i class="fa fa-filter"></i> Simpan</button>');
                }, 3000);
            }
        });
    }
    
	function updateStatusDeleteRuanganRanap(payload) {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}trawatinap_tindakan/updateStatusDeleteRuanganRanap',
			method: 'POST',
			data: payload,
			success: function (data) {
				var message = (payload.statusverifikasi == '1' ? 'Ruangan Rawat Inap Telah Diverifikasi' : 'Verifikasi Ruangan Rawat Inap Telah Dibatalkan');
				swal({
					title: "Berhasil!",
					text: message,
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				$("#cover-spin").show();
				location.reload();
			}
		});
	}
    function updateCatatanTransaksi(payload) {
		console.log(payload);
        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveCatatanNew',
            method: 'POST',
            data: payload,
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Catatan telah tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
            }
        });
    }
    
    function prosesValidasi(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}Tbilling_ranap/prosesValidasi',
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di lakukan proses validasi.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
	function prosesBatalValidasi(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}Tbilling_ranap/prosesBatalValidasi',
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: " Proses validasi telah Dibatalkan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function lockTransaksiKasir(idpendaftaran) {
		$("#cover-spin").show();
        $.ajax({
            url: '{site_url}trawatinap_tindakan/lockKasir/' + idpendaftaran,
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi Telah Di-Lock.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }

    function unlockTransaksiKasir(idpendaftaran) {
		$("#cover-spin").show();
        $.ajax({
            url: '{site_url}trawatinap_tindakan/unlockKasir/' + idpendaftaran,
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi Telah Di-Lock.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function prosesTransaksiKasir(idpendaftaran) {
		$("#cover-spin").show();
        $.ajax({
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
            },
            url: '{site_url}Tbilling_ranap/prosesTransaksi',
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di proses.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function prosesBatalTransaksi(payload) {
		$("#cover-spin").show();
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}Tbilling_ranap/prosesBatalTransaksi',
            success: function (data) {
				$("#cover-spin").hide();
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di batalkan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
				$("#cover-spin").show();
                location.reload();
            }
        });
    }
	function prosesKembaliValidasi(payload) {
		$("#cover-spin").show();
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}Tbilling_ranap/prosesKembaliValidasi',
            success: function (data) {
				$("#cover-spin").hide();
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di batalkan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
				$("#cover-spin").show();
                location.reload();
            }
        });
    }
	function prosesBatalTransaksiKasir(idpendaftaran) {
		$("#cover-spin").show();
        $.ajax({
            method: 'POST',
            data: {idpendaftaran:idpendaftaran},
            url: '{site_url}trawatinap_tindakan/prosesBatalTransaksiPembayaran',
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di batalkan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    // function prosesBatalTransaksiKasir(idpendaftaran) {
		// $("#cover-spin").show();
        // $.ajax({
            // method: 'POST',
            // data: payload,
            // url: '{site_url}trawatinap_tindakan/prosesBatalTransaksi',
            // success: function (data) {
                // swal({
                    // title: "Berhasil!",
                    // text: "Transaksi telah di batalkan.",
                    // type: "success",
                    // timer: 1500,
                    // showConfirmButton: false
                // });
                // // location.reload();
            // }
        // });
    // }
    
    function prosesCetakKwitansi(payload, tipeKontraktor, idkontraktor) {
		
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesCetakKwitansi',
            success: function (idkwitansi) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Cetak Kwitansi Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function () {
					$("#cover-spin").show();
                    var win = window.open('{site_url}trawatinap_tindakan/print_kwitansi/' + idkwitansi + '/' + tipeKontraktor + '/' + idkontraktor, '_blank');
                    win.focus();

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 2000);
            }
        });
    }

    function prosesCetakKwitansiImplant(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesCetakKwitansiImplant',
            success: function (idkwitansi) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Cetak Kwitansi Implant Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function () {
					$("#cover-spin").show();
                    var win = window.open('{site_url}trawatinap_tindakan/print_kwitansi_implant/' + idkwitansi, '_blank');
                    win.focus();

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 2000);
            }
        });
    }

    function prosesCetakFakturImplant(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesCetakFakturImplant',
            success: function (idfaktur) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Cetak Faktur Implant Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function () {
					$("#cover-spin").show();
                    var win = window.open('{site_url}trawatinap_tindakan/print_faktur_implant/' + idfaktur, '_blank');
                    win.focus();

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 2000);
            }
        });
    }
    
    function updateStatusVerifikasiTindakan(payload) {
		$("#cover-spin").show();
        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateStatusTindakanDetail',
            method: 'POST',
            data: payload,
            success: function (data) {
                var message = (payload.statusverifikasi == '1' ? 'Tindakan Telah Diverifikasi' : 'Verifikasi Tindakan Telah Dibatalkan');
                swal({
                    title: "Berhasil!",
                    text: message,
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
				$("#cover-spin").show();
                location.reload();
            }
        });
    }
    
    function updateStatusVerifikasiRuanganRanap(payload) {
		$("#cover-spin").show();
        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateStatusRuanganRanap',
            method: 'POST',
            data: payload,
            success: function (data) {
                var message = (payload.statusverifikasi == '1' ? 'Ruangan Rawat Inap Telah Diverifikasi' : 'Verifikasi Ruangan Rawat Inap Telah Dibatalkan');
                swal({
                    title: "Berhasil!",
                    text: message,
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
	function updateProsesKwitansi(id) {
		$("#cover-spin").show();
        $.ajax({
            url: '{site_url}Tbilling_ranap/updateProsesKwitansi',
            method: 'POST',
            data: {id:id},
            success: function (data) {
				$("#cover-spin").hide();
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di batalkan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
				$("#cover-spin").show();
                location.reload();
            }
        });
    }
	function updateStatusDeleteTindakan(payload) {
		$("#cover-spin").show();
        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateStatusDeleteTindakan',
            method: 'POST',
            data: payload,
            success: function (data) {
                var message ='Tindakan Telah Dihapus' ;
                swal({
                    title: "Berhasil!",
                    text: message,
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }


	function getStatusCheckout(idpendaftaran) {
		$("#status-checkout").html('<i class="fa fa-spinner fa-spin"></i>');

		$.ajax({
			dataType: 'json',
			url: '{site_url}trawatinap_tindakan/statusCheckout/' + idpendaftaran,
			success: function (data) {
				if (data.status == 1) {
					$("#status-checkout").removeClass('badge-danger');
					$("#status-checkout").addClass('badge-success');
					$("#status-checkout").html('Telah Diaktifkan');
				} else {
					$("#status-checkout").removeClass('badge-success');
					$("#status-checkout").addClass('badge-danger');
					$("#status-checkout").html('Belum Diaktifkan');
				}
			}
		});
	}

	function getDokter() {
		$.ajax({
			url: "{site_url}trawatinap_tindakan/getDokterPJ",
			dataType: "json",
			success: function (data) {
				$("#tTindakanIdDokter").empty();
				$("#tTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#tTindakanIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
				}

				$("#tTindakanGlobalIdDokter").empty();
				$("#tTindakanGlobalIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#tTindakanGlobalIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
				}

				$("#tVisiteIdDokter").empty();
				$("#tVisiteIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#tVisiteIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
				}

				$("#fVisiteIdDokter").empty();
				$("#fVisiteIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#fVisiteIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
				}

				$("#fTindakanIdDokter").empty();
				$("#fTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#fTindakanIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
				}
			}
		});
	}

    
</script>