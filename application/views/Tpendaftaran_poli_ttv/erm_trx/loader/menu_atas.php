<div class="row">
	<div class="col-md-12">
		<div class="form-group" style ="margin-bottom:15px">
			<div class="col-md-12">
					<h4 class="font-w700 push-5 text-center text-primary">TRANSAKSI PASIEN RAWAT INAP</h4>
					<h5 class="font-w400 text-center text-bold">In Patient Transcantions</h5>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-4 ">
				<?if ($statustransaksi=='1'){?>
					<?if ($status_selesai_verifikasi!='0'){?>
						<div class="alert alert-success alert-dismissable" id="peringatan_assesmen" style="display:block">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p><i class="fa <?=($statuskasir=='0'?'fa-unlock':'fa-lock')?> text-danger"> </i> <?=($statuskasir=='0'?'UnLocked':'Locked')?>  |  <span class="lbl_info_detail"> Selesai</p>
						</div>
					<?}else{?>
					<div class="alert alert-success alert-dismissable" id="peringatan_assesmen" style="display:block">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<p><i class="fa <?=($statuskasir=='0'?'fa-unlock':'fa-lock')?> text-danger"> </i> <?=($statuskasir=='0'?'UnLocked':'Locked')?>  |  <span class="lbl_info_detail">  <?=($status_proses_kwitansi=='1'?' Proses Kwitansi':'Proses Transaksi Pembayaran')?></p>
						</div>
					<?}?>
				<?}else{?>
					<?if ($statusvalidasi=='1'){?>
						<div class="alert alert-info alert-dismissable" id="peringatan_assesmen" style="display:block">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p><i class="fa <?=($statuskasir=='0'?'fa-unlock':'fa-lock')?> text-danger"> </i> <?=($statuskasir=='0'?'UnLocked':'Locked')?>  |  <span class="lbl_info_detail"> Proses Validasi </p>
						</div>
					<?}else{?>
						<div class="alert alert-info alert-dismissable" id="peringatan_assesmen" style="display:block">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p><i class="fa <?=($statuskasir=='0'?'fa-unlock':'fa-lock')?> text-danger"> </i> <?=($statuskasir=='0'?'UnLocked':'Locked')?>  |  <span class="lbl_info_detail"> Proses Pemeriksaan </p>
						</div>
					<?}?>
				<?}?>
				
			</div>
			<div class="col-md-8 ">
				<div class="pull-right push-10-r">
					<?if ($statustransaksi=='0'){?>
						<?if ($statuskasir=='0'){?>
							<button type="button"  class="btn btn-sm btn-danger btn_lockKasir" ><i class="fa fa-lock"></i> Lock</button>
						<?}else{?>
							<button type="button"  class="btn btn-sm btn-danger btn_unlockKasir" ><i class="fa fa-unlock"></i> UnLock</button>
						<?}?>
						<?if ($statusvalidasi=='0'){?>
							<button type="button"  class="btn btn-sm btn-warning btn_proses_validasi" ><i class="fa fa-check-square-o"></i> Proses Validasi</button>
						<?}?>
						<?if ($statusvalidasi=='1'){?>
							<button type="button"  class="btn btn-sm btn-warning btn_proses_unvalidasi" ><i class="fa fa-refresh"></i> Batal Validasi</button>
							<button type="button"  class="btn btn-sm btn-success btn_proses_pembayaran" ><i class="fa fa-credit-card"></i> Proses Pembayaran</button>
						<?}?>
					<?}else{?>
						<? if ($statuspembayaran=='0'){?>
							<button type="button"  class="btn btn-sm btn-warning btn_kembali_validasi"><i class="si si-action-undo"></i> Kembalikan Ke Validasi</button>
						<?}else{?>
							<?if ($status_proses_kwitansi=='0'){?>
								<?if ($st_batal=='1'){?>
							
								<button type="button" id="batalTransaksi" data-toggle="modal" data-target="#ActionBatalTransaksiModal" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Batalkan Transaksi</button>
								
								<?}?>
							<?}?>
							
							<?if ($status_proses_kwitansi=='0'){?>
								<button type="button" id="btn_proses_kwitansi" onclick="start_proses_kwitansi(<?=$pendaftaran_id_ranap?>)" class="btn btn-sm btn-success"><i class="si si-arrow-right"></i> Proses Kwitansi</button>
							<?}?>
							<?if ($status_proses_kwitansi=='1'){?>
								<?if ($status_selesai=='0'){?>
								<?if ($st_selesai=='1'){?>
								<button type="button" onclick="update_selesai(1)" class="btn btn-sm btn-success"><i class="fa fa-save"></i> SELESAI</button>
								<?}?>
								<?}?>
								<?if ($status_selesai_verifikasi=='0'){?>
								<?if ($st_selesai_verifikasi=='1'){?>
								<button type="button" onclick="update_selesai(2)"  class="btn btn-sm btn-primary"><i class="fa fa-save"></i> SELESAI VERIFIKASI</button>
								<?}?>
								<?}?>
							<?}?>
						<?}?>
						
					<?}?>
					<?if ($status_proses_card_pass=='0'){?>
					<?if ($tab>=2){?>
						<button type="button"  class="btn btn-sm btn-primary btn_card_passs"><i class="si si-envelope-letter"></i> CARD PASS</button>
					<?}?>
					<?}else{?>
						<a href="{base_url}assets/upload/kwitansi/CP_<?=$nopendaftaran?>.pdf" title="Cetak PDF" target="_blank" class="btn btn-danger" ><i class="fa fa-file-pdf-o"></i> CARD PASS</a>
					<?}?>
				</div>
				
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group push-10-t" >
		<div class="col-md-12 ">
			<div class="col-md-3 ">
				<label for="header_total_keseluruhan">TOTAL KESELURUHAN</label>
				<input class="form-control number header_total_keseluruhan" disabled type="text" value="">
				
			</div>
			<div class="col-md-3 ">
				<label for="header_total_verifikasi">TOTAL TELAH DIVERIFIKASI</label>
				<input class="form-control number header_total_verifikasi" disabled type="text"  value="">
				
			</div>
			<div class="col-md-3 ">
				<label for="">TOTAL DEPOSIT</label>
				<input class="form-control number header_total_deposit" disabled type="text" value="<?=$totaldeposit?>">
				
			</div>
			<div class="col-md-3 ">
				<label for="header_total_harus_dibayar">TOTAL HARUS DIBAYAR</label>
				<input class="form-control number header_total_harus_dibayar" disabled type="text" value="">
			</div>
			
		</div>
		
		
	</div>
	<div class="form-group push-10-t" >
		<div class="col-md-12 ">
			<div class="col-md-6 ">
				<label for="">CATATAN BILLING</label>
				<textarea class="form-control js-summernote catatan_billing" rows="4"><?=$catatan?></textarea>
				
			</div>
			<div class="col-md-6 ">
				<label for="">CATATAN INTERNAL</label>
				<textarea class="form-control js-summernote catatan_internal" rows="4"><?=$catatan_internal?></textarea>
				
			</div>
		</div>
		<?if ($status_selesai_verifikasi=='0'){?>
		<div class="col-md-12 ">
			<div class="col-md-6 ">
				<button type="button" class="btn btn-primary" id="saveNote">Simpan Catatan</button>
			</div>
			
		</div>
		<?}?>
	</div>
</div>