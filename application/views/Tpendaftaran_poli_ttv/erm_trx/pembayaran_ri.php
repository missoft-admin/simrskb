<style>
.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
	color: #fff;
	background-color: #57c1d1;
	border-color: transparent;
}
.text-bold {
	font-weight: bold;
}

</style>

<?if ($menu_kiri=='pembayaran_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='pembayaran_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
	$disabled_trx_validasi='0';
	if ($statustransaksi=='1'){
		if ($st_verifikasi_disabled=='1'){
			$disabled_trx_validasi='1';
		}
	
	}
					
						
?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($tab=='1'?'active':'')?>">
					<a href="#tab_1"><i class="si si-note"></i> Pemeriksaaan</a>
				</li>
				<li class="<?=($tab=='2'?'active':'')?>">
					<a href="#tab_1" ><i class="fa fa-history"></i> Validasi</a>
				</li>
				<?if ($statustransaksi=='1'){?>
				<li class="<?=($tab=='3'?'active':'')?>">
					<a href="#tab_3" ><i class="fa fa-history"></i> Pembayaran</a>
				</li>
				<?}?>
				<?if ($statuspembayaran=='1'){?>
					<?if ($status_proses_kwitansi!='0'){?>
					<li class="<?=($tab=='4'?'active':'')?>">
						<a href="#tab_3" ><i class="fa fa-history"></i> Proses Kwitansi</a>
					</li>
					<li class="<?=($tab=='5'?'active':'')?>">
						<a href="#tab_5" ><i class="fa fa-history"></i> Ringkasan</a>
					</li>
					<?}?>
				<?}?>
			</ul>
			<input type="hidden" id="nopendaftaran" value="<?=$nopendaftaran?>">
			<input type="hidden" id="tempIdRawatInap" value="<?=$pendaftaran_id_ranap?>">
			<input type="hidden" id="Tmpstatus_proses_kwitansi" value="<?=$status_proses_kwitansi?>">
			<input type="hidden" id="status_card" value="<?=$status_card?>">
			<input type="hidden" id="nominal_sudah_dibayar" value="<?=$nominal_sudah_dibayar?>">
			<input type="hidden" id="idtipe" value="<?=$idtipe?>">
			<input type="hidden" id="idPembayaran" value="<?=$idPembayaran?>">
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($tab=='1' || $tab=='2'?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<?php require __DIR__ . '/loader/menu_atas.php'; ?>
					
					<div class="row">
						<div class="form-group push-10-t" >
						<div class="col-md-12 ">
							<div class="col-md-12 ">
							
							
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_ruangan.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_ranap.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_igd.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_laboratorium.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_radiologi.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_fisioterapi.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_operasi.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/tindakan_administrasi.php'; ?>
							<hr>
							<?php require __DIR__ . '/section_pembayaran/section_rekapitulasi.php'; ?>
							
								
							</div>
						</div>
						</div>
					</div>	
					
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($tab=='3' || $tab=='4'?'active in':'')?>" id="tab_3">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
						<?php require __DIR__ . '/loader/menu_atas.php'; ?>
						<hr class="push-5-b">
						<?php require __DIR__ . '/section_pembayaran/section_pembayaran.php'; ?>
						<!--BATS AKRHI -->
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?>" id="tab_5">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group" style ="margin-bottom:15px">
									<div class="col-md-12">
											<h4 class="font-w700 push-5 text-center text-primary">TRANSAKSI PASIEN RAWAT INAP</h4>
											<h5 class="font-w400 text-center text-bold">In Patient Transcantions</h5>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4 ">
										
									</div>
									<div class="col-md-8 ">
										<div class="pull-right push-10-r">
											<?if ($status_proses_card_pass=='0'){?>
											<?if ($tab>=2){?>
												<button type="button"  class="btn btn-sm btn-primary btn_card_passs"><i class="si si-envelope-letter"></i> CARD PASS</button>
											<?}?>
											<?}else{?>
												<a href="{base_url}assets/upload/kwitansi/CP_<?=$nopendaftaran?>.pdf" title="Cetak PDF" target="_blank" class="btn btn-danger" ><i class="fa fa-file-pdf-o"></i> CARD PASS</a>
											<?}?>
										</div>
										
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group" style ="margin-bottom:15px">
									<div class="col-md-3">
										<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
											<div class="block-content block-content-full bg-success">
												<div class="h4 font-w700 text-white">VALIDASI</div>
												<div class="h5 text-white-op text-uppercase push-5-t"><span class="fa fa-calendar"></span> <?=($tanggal_statusvalidasi?HumanDateLong($tanggal_statusvalidasi):'')?></div>
											</div>
											<div class="block-content block-content-full block-content-mini bg-gray-lighter">
												<i class="fa fa-user-md text-success"></i> <?=($user_validasi?$user_validasi:'-')?>
											</div>
										</a>
									</div>
									<div class="col-md-3">
										<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
											<div class="block-content block-content-full bg-success">
												<div class="h4 font-w700 text-white">PEMBAYARAN</div>
												<div class="h5 text-white-op text-uppercase push-5-t"><span class="fa fa-calendar"></span> <?=($tanggal_statusbayar?HumanDateLong($tanggal_statusbayar):'')?></div>
											</div>
											<div class="block-content block-content-full block-content-mini bg-gray-lighter">
												<i class="fa fa-user-md text-success"></i> <?=($user_id_statusbayar?$user_bayar:'-')?>
											</div>
										</a>
									</div>
									<div class="col-md-3">
										<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
											<div class="block-content block-content-full bg-success">
												<div class="h4 font-w700 text-white">PROSES KWITANSI</div>
												<div class="h5 text-white-op text-uppercase push-5-t"><span class="fa fa-calendar"></span> <?=($status_proses_kwitansi?HumanDateLong($tanggal_proses_kwitansi):'')?></div>
											</div>
											<div class="block-content block-content-full block-content-mini bg-gray-lighter">
												<i class="fa fa-user-md text-success"></i> <?=($user_id_proses_kwitansi?$user_kwitansi:'-')?>
											</div>
										</a>
									</div>
									<div class="col-md-3">
										<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
											<div class="block-content block-content-full bg-success">
												<div class="h4 font-w700 text-white">CARD PASS</div>
												<div class="h5 text-white-op text-uppercase push-5-t"><span class="fa fa-calendar"></span> <?=($status_proses_card_pass?HumanDateLong($tanggal_proses_card_pass):'')?></div>
											</div>
											<div class="block-content block-content-full block-content-mini bg-gray-lighter">
												<i class="fa fa-user-md text-success"></i> <?=($user_id_proses_card_pass?$user_car_pass:'-')?>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>


<?php require __DIR__ . '/loader/services.php'; ?>

<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
  $("#cover-spin").show();
var idpendaftaran = $("#tempIdRawatInap").val();
var status_proses_kwitansi = $("#Tmpstatus_proses_kwitansi").val();
var status_card = $("#status_card").val();
$(document).ready(function() {
		$("#tanggal_proses_card_pass").datetimepicker({
            format: "DD-MM-YYYY HH:mm:ss",
            // stepping: 30
        });
		$('.number').number(true, 0, '.', ',');
        $('.discount').number(true, 2, '.', ',');
        $(".time-datepicker").datetimepicker({
            format: "HH:mm:ss"
        });
		$('.js-summernote').summernote({
		  height: 100,   //set editable area's height
			dialogsInBody: true,
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
		if (status_proses_kwitansi!='0'){
		list_kwitansi();
			
		}
		Dropzone.autoDiscover = false;
		myDropzone_p = new Dropzone("#image-upload-pembayaran", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone_p.on("complete", function(file) {
		  
		  refresh_image_pembayaran();
		  myDropzone_p.removeFile(file);
		  
		});
		
		myDropzone_card = new Dropzone("#image-upload-card", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone_card.on("complete", function(file) {
		  
		  refresh_image_card_pass();
		  myDropzone_card.removeFile(file);
		  
		});
	
		$(".header_total_keseluruhan").val($("#tmp_totalKeseluruhan").val());
		$(".header_total_verifikasi").val($("#tmp_totalVerifikasi").val());
		$(".header_total_harus_dibayar").val($("#tmp_totalHarusDibayar").val());
		$(".header_total_deposit").val($("#tmp_totaldeposit").val());
		let sisa=parseFloat($("#tmp_totalHarusDibayar").val()) - parseFloat($("#nominal_sudah_dibayar").val());
				// alert(sisa);
		if (status_card=='1'){
			if (sisa > 0){
				$(".btn_card_passs").attr('disabled', 'disabled');
			}
		}
		
		$("#cover-spin").hide();
		// $("#gt_final").val($("#tmp_totalHarusDibayar").val());
		// alert($("#gt_final").val());
		// initFormInput();
		// getActionButton();
		getDokter();
	$('#idalasan_batal').select2({
		ajax: {
			url: '{site_url}trawatinap_tindakan/getAlasanBatal',
			dataType: 'json',
			processResults: function (data) {
				var res = data.results.map(function (item) {
					return {
						id: item.id,
						text: item.keterangan
					}
				})
				return {
					results: res
				}
			},
		}
	});
	 $(document).find('.catatan_billing').on('summernote.blur', function() {
		$(".catatan_billing").summernote('code',$(this).val());
		console.log($(this).val());
	});
	 $(document).find('.catatan_internal').on('summernote.blur', function() {
		$(".catatan_internal").summernote('code',$(this).val());
		console.log($(this).val());
	});
	$(document).on("change","#kwitansi_idmetode",function(){
		 let tipekontraktor=$(this).find(":selected").data("tipekontraktor");
		 let idkontraktor=$(this).find(":selected").data("idkontraktor");
		 let st_insert=$(this).find(":selected").data("st_insert");
		 $("#kwitansi_tipe_kontraktor").val(tipekontraktor);
		 $("#kwitansi_idkontraktor").val(idkontraktor);
		 let idrawatinap=$("#tempIdRawatInap").val();
		 let idmetode=$("#kwitansi_idmetode").val();
		 let kwitansi_idedit=$("#kwitansi_idedit").val();
		 let gt_rp=$("#gt_rp").val();
		 $("#cover-spin").show();
		 if (st_insert=='1'){
			 if (kwitansi_idedit==''){
				$(".btn_proses_kwitansi").attr('disabled', 'disabled');
				// alert(kwitansi_idedit);
			 }else{
			 $(".btn_proses_kwitansi").removeAttr("disabled");
				 
			 }
		 }else{
			 $(".btn_proses_kwitansi").removeAttr("disabled");
			 // $(".btn_proses_kwitansi").attr('disabled', 'disabled');
		 }
		 if (kwitansi_idedit==''){
			 
		 $.ajax({
			url: '{site_url}Tbilling_ranap/find_nominal_kwitansi/',
				method: 'POST',
				dataType: "json",
				data: {
					idrawatinap:idrawatinap,
					tipekontraktor:tipekontraktor,
					idkontraktor:idkontraktor,
					idmetode:idmetode,
					gt_rp:gt_rp,
				},
				success: function(data) {
					$("#cover-spin").hide();
					$('#kwitansi_nominal').val(data.nominal);
					
				}
		});
		 }

		 // alert(tipekontraktor);
	});
});
function edit_kwitansi(id){
	let idrawatinap=$("#tempIdRawatInap").val();
	$("#kwitansi_idedit").val(id);
	$("#ProsesKwitansiModal").modal('show');
	$('#kwitansi_idmetode').empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tbilling_ranap/find_jenis_kwitansi/',
			method: 'POST',
			dataType: "json",
            data: {idrawatinap:idrawatinap},
			success: function(data) {
				// $("#cover-spin").hide();
				$('#kwitansi_idmetode').append(data.hasil);
				$.ajax({
					url: '{site_url}Tbilling_ranap/find_edit_kwitansi/',
						method: 'POST',
						dataType: "json",
						data: {id:id},
						success: function(data) {
							$("#cover-spin").hide();
							// alert(data.tipe_proses);
							$('#kwitansi_nominal').val(data.nominal);
							$('#kwitansi_tanggal').val(data.tanggal);
							$('#kwitansi_terima_dari').val(data.sudahterimadari);
							$('#kwitansi_idmetode').val(data.tipe_proses).trigger('change.select2');
							$('#kwitansi_deskripsi').val(data.deskripsi);
						}
				});
			}
	});
	
}
function proses_kwitansi(id){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Generate Kwitansi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tbilling_ranap/proses_kwitansi', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					   },
				success: function(data) {
					$("#cover-spin").hide();
					list_kwitansi();
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

				}
			});
		});
}
function list_kwitansi(){
	let id=$("#tempIdRawatInap").val();
	
	$("#cover-spin").show();
	$('#tabel_list_kwitansi').DataTable().destroy();	
	// alert(ruangan_id);
	table = $('#tabel_list_kwitansi').DataTable({
		autoWidth: false,
		searching: true,
		serverSide: true,
		
		"processing": false,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		"columnDefs": [
				{ "targets": [5],  className: "text-right" },
				{ "targets": [0,1,2,6],  className: "text-center" },
				
			],
		ajax: { 
			url: '{site_url}tbilling_ranap/list_kwitansi', 
			type: "POST" ,
			dataType: 'json',
			data : {
					id:id,
					
				   }
		},
		"drawCallback": function( settings ) {
			
			 $("#cover-spin").hide();
		 }  
	});
}
function updateproseskwitansi(st_proses){
	let nominal=$("#kwitansi_nominal").val();
	let tanggal=$("#kwitansi_tanggal").val();
	let tipe_proses=$("#kwitansi_idmetode").val();
	let sudahterimadari=$("#kwitansi_terima_dari").val();
	let deskripsi=$("#kwitansi_deskripsi").val();
	let idrawatinap=$("#tempIdRawatInap").val();
	let noregister=$("#nopendaftaran").val();
	let tipekontraktor=$("#kwitansi_tipe_kontraktor").val();
	let idkontraktor=$("#kwitansi_idkontraktor").val();
	let idedit=$("#kwitansi_idedit").val();
	if (kwitansi_idmetode=='#'){
		sweetAlert("Maaf...", "Jenis Kwitansi Belum dipilih!", "error");
		return false;
	}
	if (nominal=='0'){
		sweetAlert("Maaf...", "Nominal harus diisi !", "error");
		return false;
	}
	if (sudahterimadari==''){
		sweetAlert("Maaf...", "Sudah diterima harus diisi !", "error");
		return false;
	}
	if (deskripsi==''){
		sweetAlert("Maaf...", "Deskripsi harus diisi !", "error");
		return false;
	}

	var payload= {
		idrawatinap : idrawatinap,
		tipe_proses : tipe_proses,
		tanggal : tanggal,
		sudahterimadari : sudahterimadari,
		deskripsi : deskripsi,
		nominal : nominal,
		noregister : noregister,
		tipekontraktor : tipekontraktor,
		idkontraktor : idkontraktor,
		st_proses : st_proses,
		idedit : idedit,

	}

	swal({
		title: "Apakah anda yakin Proses Kwitansi?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		$("#cover-spin").show();
		 $.ajax({
            url: '{site_url}Tbilling_ranap/updateProsesKwitansiDetail',
            method: 'POST',
            data: payload,
            success: function (data) {
				$("#cover-spin").hide();
                swal({
					title: "Berhasil!",
					text: "Behasil diproses",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				$("#ProsesKwitansiModal").modal('hide');
				list_kwitansi();
            }
        });
	});
	
	// alert('sini');
}
function show_hapus_biasa(id){
	$("#alasanhapus_id").val(id);
	$("#AlasanHapusKwitansiModal").modal({backdrop: 'static', keyboard: false}).show();
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}Tbilling_ranap/get_alasan', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				   },
			success: function(data) {
				$('#alasanhapus').val(data.alasanhapus);
				$("#cover-spin").hide();
			}
		});
	$('#alasanhapus').focus();
}
function validate_approval(){
	var table = $('#tabel_user').dataTable();
	 //Get the total rows
	 if (table.fnGetData().length > 0){
		 $("#btn_simpan_approval").attr('disabled',false);
	 }
}
function load_user_approval(id){
	// modal_approval
	$("#idtrx_approval").val(id);
	$("#btn_simpan_approval").attr('disabled',true);
	$("#modal_approval").modal({backdrop: 'static', keyboard: false}).show();
	$('#tabel_user').DataTable().destroy();
	table=$('#tabel_user').DataTable({
		"autoWidth": false,
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"order": [],
		"ajax": {
			url: '{site_url}Tbilling_ranap/load_user_approval',
			type: "POST",
			dataType: 'json',
			data : {
				id:id,

			   }
		},
		"columnDefs": [
			 // {  className: "text-right", targets:[0] },
			 {  className: "text-center", targets:[1,2,3,4] },
			 // { "width": "10%", "targets": [0] },
			 // { "width": "40%", "targets": [1] },
			 // { "width": "25%", "targets": [2,3] },

		]
	});
	setTimeout(function() {
	  validate_approval();
	}, 1500);

}
$(document).on("click","#btn_simpan_approval",function(){
	let text = $("#alasanhapus_2").val();
	let length = text.length;
	if (length < 10) {
		sweetAlert("Maaf...", "Tentukan Alasan Minimal 10 Karakter!", "error");
		$("#modal_approval").modal({backdrop: 'static', keyboard: false}).show();
		$('#alasanhapus').focus();
		return false;
	}
		
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Melanjutkan Step Persetujuan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#d26a5c",
		cancelButtonText: "Tidak Jadi",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tbilling_ranap/simpan_proses_peretujuan/',
			type: "POST",
			dataType: 'json',
			data : {
				id:$("#idtrx_approval").val(),
				alasanhapus:$("#alasanhapus_2").val(),

			   },
			complete: function() {
				$("#cover-spin").hide();
				swal({
					title: "Berhasil!",
					text: "Proses Approval Berhasil.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				list_kwitansi();
			}
		});
	});

	return false;
});
function update_selesai(status_selesai_verifikasi){
	let idpendaftaran = $("#tempIdRawatInap").val();
	let idPembayaran = $("#idPembayaran").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Selesai Transaksi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#d26a5c",
		cancelButtonText: "Tidak Jadi",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tbilling_ranap/update_selesai/',
			type: "POST",
			dataType: 'json',
			data : {
				status_selesai_verifikasi:status_selesai_verifikasi,
				idpendaftaran:idpendaftaran,
				idPembayaran:idPembayaran,
			   },
			complete: function() {
				$("#cover-spin").hide();
				swal({
					title: "Berhasil!",
					text: "Proses Approval Berhasil.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				window.location.href = "<?php echo site_url('tbilling_ranap'); ?>";
			}
		});
	});

}
function list_user(idtrx){
	$("#modal_user").modal('show');

	// alert(idrka);
	$.ajax({
		url: '{site_url}Tbilling_ranap/list_user/'+idtrx,
		dataType: "json",
		success: function(data) {
			$("#tabel_user_proses tbody").empty();
			$("#tabel_user_proses tbody").append(data.detail);
		}
	});
}
function hapus_kwitansi(){
	let id=$("#alasanhapus_id").val();
	let text = $("#alasanhapus").val();
	let length = text.length;
	if (length < 10) {
		sweetAlert("Maaf...", "Tentukan Alasan Minimal 10 Karakter!", "error");
		$("#AlasanHapusKwitansiModal").modal({backdrop: 'static', keyboard: false}).show();
		$('#alasanhapus').focus();
		return false;
	}
		
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Kwitansi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tbilling_ranap/hapus_kwitansi', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						alasanhapus:$("#alasanhapus").val(),
						
					   },
				success: function(data) {
					list_kwitansi();
					$("#AlasanHapusKwitansiModal").modal('hide');
					$("#cover-spin").hide();
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

				}
			});
		});

}
// Tipe Pasien
$(document).on('change', '#idtipepasien', function() {
	var idtipepasien = $(this).val();

	$.ajax({
		url: '{site_url}tverifikasi_transaksi/updateTipePasien',
		method: 'POST',
		dataType: 'json',
		data: {
			idpendaftaran: idpendaftaran,
			idtipepasien: idtipepasien,
			table: 'trawatinap_pendaftaran'
		},
		success: function(data) {
			if (data.status) {
				swal({
					title: 'Berhasil',
					text: 'Tipe Pasien berhasil diubah.',
					type: 'success',
					timer: 1500
				});
			}
		}
	});
});
$(document).on('change', '#tanggal_pembayaran', function() {
	
// find_tanggal();
	
});
function find_tanggal(){
	var tanggal_pembayaran = $("#tanggal_pembayaran").val();
	$.ajax({
		url: '{site_url}tbilling_ranap/find_tanggal_setoran',
		method: 'POST',
		dataType: 'json',
		data: {
			tanggal: tanggal_pembayaran,
		},
		success: function(data) {
			if (data==true) {
				swal({
					title: 'Tanggal Setoran',
					text: 'Tanggal Tersebut sudah ada dalam data setoran harian.',
					type: 'success',
					timer: 1500
				});
				$("#tanggal_pembayaran").val('');
			}
		}
	});
}
	
// Tanggal & Waktu Pembayaran
$(document).on('click', '#btnUpdateTanggalPembayaran', function () {
	var tanggalPembayaran = $('#tanggal_pembayaran').val();
	var waktuPembayaran = $('#waktu_pembayaran').val();
	if (tanggalPembayaran==''){
		sweetAlert("Maaf...", "Tanggal Pembayaran ", "error");
		$('#tanggal_pembayaran').focus();
		return false;
	}
	if (waktuPembayaran==''){
		sweetAlert("Maaf...", "Tanggal Waktu ", "error");
		$('#waktuPembayaran').focus();
		return false;
	}
	var tanggal_pembayaran = $("#tanggal_pembayaran").val();
	$.ajax({
		url: '{site_url}tbilling_ranap/find_tanggal_setoran',
		method: 'POST',
		dataType: 'json',
		data: {
			tanggal: tanggal_pembayaran,
		},
		success: function(data) {
			if (data==true) {
				sweetAlert("Maaf...", "Tanggal Sudah Ada Transaksi Setoran Tunai ", "error");
				$("#tanggal_pembayaran").val('');
			}else{
				var payload = {
					idpendaftaran: idpendaftaran,
					tanggal_pembayaran: tanggalPembayaran,
					waktu_pembayaran: waktuPembayaran
				}

				updateTanggalPembayaranTransaksi(payload);
			}
		}
	});
	
});

// Catatan Transaksi
$(document).on('click', '#saveNote', function () {
	var idpendaftaran = $('#tempIdRawatInap').val();
	var catatan = $('.catatan_billing').val();
	var catatan_internal = $('.catatan_internal').val();
	// alert(catatan_internal);
	var payload = {
		idpendaftaran: idpendaftaran,
		catatan: catatan,
		catatan_internal: catatan_internal,
	}

	updateCatatanTransaksi(payload);
});

// Lock Kasir
$(document).on('click', '.btn_lockKasir', function () {
	lockTransaksiKasir(idpendaftaran);
});

// UnLock Kasir
$(document).on('click', '.btn_unlockKasir', function () {
	unlockTransaksiKasir(idpendaftaran);
});

// Proses Validasi
$(document).on('click', '.btn_proses_validasi', function () {
	var tempRuanganRanap = $('#tempRuanganRanap tr').get().map(function (row) {
		return $(row).find('td').get().map(function (cell) {
			return $(cell).html();
		});
	});

	var tempAdministrasiRanap = $('#tempAdministrasiRanap tr').get().map(function (row) {
		return $(row).find('td').get().map(function (cell) {
			return $(cell).html();
		});
	});

	var payload= {
		idpendaftaran: idpendaftaran,
		ruangranap: JSON.stringify(tempRuanganRanap),
		admranap: JSON.stringify(tempAdministrasiRanap)
	}

	swal({
		title: "Apakah anda yakin ingin melakukan proses validasi?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		prosesValidasi(payload);
	});
});
$(document).on('click', '.btn_proses_unvalidasi', function () {
	
	var payload= {
		idpendaftaran: idpendaftaran,
	}

	swal({
		title: "Apakah anda yakin ingin melakukan membatalkan proses validasi?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		prosesBatalValidasi(payload);
	});
});
$(document).on('click', '.btn_kembali_validasi', function () {
	
	var payload= {
		idpendaftaran: idpendaftaran,
	}

	swal({
		title: "Apakah anda yakin ingin melakukan Kembali ke proses validasi?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		prosesKembaliValidasi(payload);
	});
});
// Proses Transaksi Kasir
$(document).on('click', '.btn_proses_pembayaran', function () {
	swal({
		title: "Apakah anda yakin ingin melakukan proses transaksi?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		prosesTransaksiKasir(idpendaftaran);
	});
});
// $(document).on('click', '.btn_kembali_validasi', function () {
	// swal({
		// title: "Apakah anda yakin ingin kembali melakukan proses validasi?",
		// type: "warning",
		// showCancelButton: true,
		// confirmButtonColor: "#DD6B55",
		// confirmButtonText: "Ya",
		// cancelButtonText: "Tidak",
		// closeOnConfirm: false,
		// closeOnCancel: false
	// }).then(function () {
		// prosesBatalTransaksiKasir(idpendaftaran);
	// });
// });

// Batal Transaksi
// $('#idalasan_batal').select2({
	// ajax: {
		// url: '{site_url}trawatinap_tindakan/getAlasanBatal',
		// dataType: 'json',
		// processResults: function (data) {
			// var res = data.results.map(function (item) {
				// return {
					// id: item.id,
					// text: item.keterangan
				// }
			// })
			// return {
				// results: res
			// }
		// },
	// }
// });

// Proses Batal Transaksi
$(document).on('click', '#saveBatalTransaksi', function () {
	var alasanbatal = $('#idalasan_batal').val();
	var alasan_batal_transaksi = $('#alasan_batal_transaksi').val();
	let length = alasan_batal_transaksi.length;
	if (alasanbatal=='#') {
		sweetAlert("Maaf...", "Tentukan Alasan ", "error");
		$('#idalasan_batal').focus();
		return false;
	}
	if (length < 10) {
		sweetAlert("Maaf...", "Tentukan Alasan Minimal 10 Karakter!", "error");
		$('#alasan_batal_transaksi').focus();
		return false;
	}
	var payload = {
		idpendaftaran: idpendaftaran,
		alasanbatal: alasanbatal,
		alasan_batal_transaksi: alasan_batal_transaksi,
	}

	prosesBatalTransaksi(payload);
});

// Proses Cetak Kwitansi
$(document).on('click', '#cetakProsesKwitansi', function () {
	var idmetode = $('#kwitansi_idmetode').val();
	var sudahterimadari = $('#kwitansi_terima_dari').val();
	var tipeKontraktor = $('#kwitansi_tipe_kontraktor').val();
	var idkontraktor = $('#kwitansi_idkontraktor').val();
	var deskripsi = $('#kwitansi_deskripsi').val();

	var payload = {
		idrawatinap: idpendaftaran,
		tipe_proses: idmetode,
		sudahterimadari: (sudahterimadari != "" ? sudahterimadari : "<?=$namapasien?>"),
		deskripsi: (deskripsi != "" ? deskripsi : "Tindakan dan Rawat Inap Pasien a/n <?=$namapasien?>"),
	}

	prosesCetakKwitansi(payload, tipeKontraktor, idkontraktor);
});

// Proses Cetak Kwitansi Implant
$(document).on('click', '#cetakKwitansiImplant', function () {
	var sudahterimadari = $('#kimplant_terima_dari').val();
	var deskripsi = $('#kimplant_deskripsi').val();

	var payload = {
		idrawatinap: idpendaftaran,
		sudahterimadari: (sudahterimadari != "" ? sudahterimadari : "<?=$namapasien?>"),
		deskripsi: (deskripsi != "" ? deskripsi : "Implant Pasien a/n <?=$namapasien?>"),
	}

	prosesCetakKwitansiImplant(payload);
});

// Proses Cetak Faktur Implant
$(document).on('click', '#cetakFakturImplant', function () {
	var nomedrec = $('#fimplant_nomedrec').val();
	var namapasien = $('#fimplant_namapasien').val();
	var namadokter = $('#fimplant_dokter').val();
	var diagnosa = $('#fimplant_diagnosa').val();
	var tanggaloperasi = $('#fimplant_tanggaloperasi').val();
	var operasi = $('#fimplant_operasi').val();
	var iddistributor = $('#fimplant_distributor').val();

	var payload = {
		idrawatinap: idpendaftaran,
		nomedrec: nomedrec,
		namapasien: namapasien,
		namadokter: namadokter,
		diagnosa: diagnosa,
		tanggaloperasi: tanggaloperasi,
		operasi: operasi,
		iddistributor: iddistributor,
	}

	prosesCetakFakturImplant(payload);
});

// Verifikasi Tindakan
$(document).on('click', '.verifTindakan', function () {
	var idrow = $(this).data('idrow');
	var table = $(this).data('table');
	var statusracikan = $(this).data('statusracikan');
	var statusverifikasi = '1';

	var payload = {
		idrow: idrow,
		table: table,
		statusracikan: statusracikan,
		statusverifikasi: statusverifikasi
	}

	swal({
		title: "Apakah anda yakin ingin memverifikasi data ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateStatusVerifikasiTindakan(payload);
	});
});
// Hapus Tindakan
$(document).on('click', '.deleteTindakan', function () {
	var idrow = $(this).data('idrow');
	var table = $(this).data('table');
	// var statusracikan = $(this).data('statusracikan');
	var status = '0';

	var payload = {
		idrow: idrow,
		table: table,
		// statusracikan: statusracikan,
		status: status
	}

	swal({
		title: "Apakah anda yakin ingin Menghapus data ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateStatusDeleteTindakan(payload);
	});
});

// Unverfikasi Tindakan
$(document).on('click', '.unverifTindakan', function () {
	var idrow = $(this).data('idrow');
	var table = $(this).data('table');
	var statusracikan = $(this).data('statusracikan');
	var statusverifikasi = '0';

	var payload = {
		idrow: idrow,
		table: table,
		statusracikan: statusracikan,
		statusverifikasi: statusverifikasi
	}

	swal({
		title: "Apakah anda yakin ingin membatalkan verifikasi data ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateStatusVerifikasiTindakan(payload);
	});
});

// Verifikasi Ruangan Ranap
$(document).on('click', '.verifRuanganRanap', function () {
	var idrow = $(this).data('idrow');
	var table = $(this).data('table');
	var statusverifikasi = '1';

	var payload = {
		idrow: idrow,
		table: table,
		statusverifikasi: statusverifikasi
	}

	swal({
		title: "Apakah anda yakin ingin memverifikasi data ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateStatusVerifikasiRuanganRanap(payload);
	});
});

// Unverfikasi Ruangan Ranap
$(document).on('click', '.unverifRuanganRanap', function () {
	var idrow = $(this).data('idrow');
	var table = $(this).data('table');
	var statusverifikasi = '0';

	var payload = {
		idrow: idrow,
		table: table,
		statusverifikasi: statusverifikasi
	}

	swal({
		title: "Apakah anda yakin ingin membatalkan verifikasi data ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateStatusVerifikasiRuanganRanap(payload);
	});
});

// Proses Kwitansi
$(document).on('click', '.prosesKwitansi', function () {
	$('#kwitansi_idmetode').val($(this).data('idmetode'));
	$('#kwitansi_tipe_kontraktor').val($(this).data('tipekontraktor'));
	$('#kwitansi_idkontraktor').val($(this).data('idkontraktor'));
});
function show_modal_batal(){
	$("#HistoryBatalTransaksiModal").modal('hide');
	$("#HistoryBatalModal").modal('show');
}
function list_pemabayaran(id){
	$("#HistoryBatalTransaksiModal").modal('show');
	$("#HistoryBatalModal").modal('hide');
	$('#tabel_list_trx').DataTable().destroy();	
	$("#cover-spin").show();
	
	// alert(ruangan_id);
	table = $('#tabel_list_trx').DataTable({
		autoWidth: false,
		searching: true,
		serverSide: true,
		
		"processing": false,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		"columnDefs": [
				// { "width": "10%", "targets": 0},
				// { "width": "40%", "targets": 1},
				// { "width": "20%", "targets": 2},
				// { "width": "20%", "targets": 3},
				// { "width": "10%", "targets": 4},
				
			],
		ajax: { 
			url: '{site_url}tbilling_ranap/list_pemabayaran', 
			type: "POST" ,
			dataType: 'json',
			data : {
					id:id,
					
				   }
		},
		"drawCallback": function( settings ) {
			
			 $("#cover-spin").hide();
		 }  
	});
}
$(document).on('click', '.deleteRuanganRanap', function () {
	var idrow = $(this).data('idrow');
	var table = $(this).data('table');
	var statusverifikasi = '0';

	var payload = {
		idrow: idrow,
		table: table,
		statusverifikasi: statusverifikasi
	}

	swal({
		title: "Apakah anda yakin ingin membatalkan Transaksi data ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateStatusDeleteRuanganRanap(payload);
	});
});

function start_proses_kwitansi(id){
	swal({
		title: "Apakah anda yakin ingin Melanjutkan Proses Kwitansi?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateProsesKwitansi(id);
	});
}



function tutup_modal_kwitansi(){
	$("#ProsesKwitansiModal").modal('hide');
}
function show_modal_kwitansi(){
	let idrawatinap=$("#tempIdRawatInap").val();
	$("#kwitansi_idedit").val('');
	$("#ProsesKwitansiModal").modal('show');
	$('#kwitansi_idmetode').empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tbilling_ranap/find_jenis_kwitansi/',
			method: 'POST',
			dataType: "json",
            data: {idrawatinap:idrawatinap},
			success: function(data) {
				$("#cover-spin").hide();
				$('#kwitansi_idmetode').append(data.hasil);
				$('#kwitansi_deskripsi').val(data.deskripsi_kwitansi_label);
				$('#kwitansi_nominal').val(0);
			}
	});

}
function genererate_kwitansi(idmetode,tipekontraktor,idkontraktor){
	let idrawatinap=$("#tempIdRawatInap").val();
	swal({
		title: "Apakah anda yakin Generate Kwitansi?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya",
		cancelButtonText: "Tidak",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then(function () {
		updateProsesKwitansiDetail(idrawatinap,idmetode,tipekontraktor,idkontraktor);
	});
	// alert(idmetode+' - '+tipekontraktor+' - '+idkontraktor);
}
function show_upload_bukti(id){
	$("#modal_bukti_pembayaran").modal('show');
	$("#idupload_pembayaran2").val(id);
	$("#idupload_pembayaran").val(id);
	refresh_image_pembayaran();
}
function refresh_image_pembayaran(){
	var id=$("#idupload_pembayaran2").val();
	$('#list_file_pembayaran tbody').empty();
	$.ajax({
		url: '{site_url}tbilling_ranap/refresh_image_pembayaran/'+id,
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#list_file_pembayaran tbody').empty();
				$("#box_file2").attr("hidden",false);
				$("#list_file_pembayaran tbody").append(data.detail);
			}
			// console.log();
			
		}
	});
}
$(document).on("click",".hapus_file_pembayaran",function(){	
	var currentRow=$(this).closest("tr");          
	 var id=currentRow.find("td:eq(4)").text();
	 $.ajax({
		url: '{site_url}tbilling_ranap/hapus_file_pembayaran',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
			currentRow.remove();
			// refresh_image_pembayaran();
			// table.ajax.reload( null, false );				
		}
	});
});
$(document).on('click', '.btn_card_passs', function () {
	$("#modal_card_pass").modal('show');
	$("#idupload_card").val(idpendaftaran);
	$("#idupload_card2").val(idpendaftaran);
	refresh_image_card_pass();
});
function refresh_image_card_pass(){
	var id=$("#idupload_card2").val();
	$('#list_file_card_pass tbody').empty();
	$.ajax({
		url: '{site_url}tbilling_ranap/refresh_image_card_pass/'+id,
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#list_file_card_pass tbody').empty();
				$("#box_file2").attr("hidden",false);
				$("#list_file_card_pass tbody").append(data.detail);
			}
			// console.log();
			
		}
	});
}
$(document).on("click",".hapus_file_card_pass",function(){	
	var currentRow=$(this).closest("tr");          
	 var id=currentRow.find("td:eq(4)").text();
	 $.ajax({
		url: '{site_url}tbilling_ranap/hapus_file_card_pass',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
			currentRow.remove();
			// refresh_image_pembayaran();
			// table.ajax.reload( null, false );				
		}
	});
});
$(document).on("click",".btn_proses_card_passs",function(){	
	let info_proses_card_pass=$("#info_proses_card_pass").val();
	let tanggal_proses_card_pass=$("#tanggal_proses_card_pass").val();
	if (info_proses_card_pass==''){
		sweetAlert("Maaf...", "Informasi Card Pass harus diisi!", "error");
		return false;
	}
	if (tanggal_proses_card_pass==''){
		sweetAlert("Maaf...", "Tanggal Card Pass harus diisi!", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tbilling_ranap/updateCardPass',
		method: 'POST',
		data: {
			idpendaftaran:idpendaftaran,
			info_proses_card_pass:info_proses_card_pass,
			tanggal_proses_card_pass:tanggal_proses_card_pass,
		},
		success: function (data) {
			$("#cover-spin").hide();
			swal({
				title: "Berhasil!",
				text: "Card Pass Created.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
			$("#cover-spin").show();
			location.reload();
		}
	});
});
</script>
<!-- Modal Included -->
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_action_tindakan') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_action_visite') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_administrasi') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_administrasi_ranap') ?>

<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_kamar_ok') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_operator_ok') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_asisten_ok') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_ruangan_ranap') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_tindakan_farmasi') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_tindakan_global') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_tindakan_ranap') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_edit_visite_ranap') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_pembayaran') ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_history_pembatalan'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_konfirmasi_pembatalan'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_proses_kwitansi'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_cetak_faktur_implant'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_kwitansi_implant'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_bukti_pembayaran'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_trx/modal_pembayaran/modal_card_pass'); ?>
