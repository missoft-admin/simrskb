<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_fc_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_fc_detail?></span></b>
<table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapFullCare = 0;?>
        <?php $totalRanapFullCareVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idpelayanan?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
					<?php if (UserAccesForm($user_acces_form,array('2640'))){?>
                    <button type="button" class="btn btn-sm btn-primary editDataTindakan" data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal" data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>><i class="fa fa-pencil"></i></button>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form,array('2641'))){?>
                    <button type="button" class="btn btn-sm btn-danger removeDataTindakan" data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>><i class="fa fa-trash"></i></button>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form,array('2642'))){?>
                    <button type="button" class="btn btn-sm btn-success verifTindakan" data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan" <?=($statuspembayaran == 1 ? 'disabled' : '')?>><i class="fa fa-check"></i> Verifikasi</button>
					<?php } ?>
                <?php } else { ?>
					<?php if (UserAccesForm($user_acces_form,array('2643'))){?>
                    <button type="button" class="btn btn-sm btn-danger unverifTindakan" data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan" <?=($statuspembayaran == 1 ? 'disabled' : '')?>><i class="fa fa-times"></i> Verifikasi</button>
					<?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
        </tr>
        <?php
            $totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapFullCareVerif = $totalRanapFullCareVerif + $row->totalkeseluruhan;
            }
        ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRanapFullCare)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_ecg_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_ecg_detail?></span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapECG = 0;?>
        <?php $totalRanapECGVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idpelayanan?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2640'))){?>
                <button type="button" data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal"
                    data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakan"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2641'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger removeDataTindakan"><i class="fa fa-trash"></i></button>
                <?php } ?>

                <!-- Action Verifikasi -->
				<?php if (UserAccesForm($user_acces_form,array('2642'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2643'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
        </tr>
        <?php
            $totalRanapECG = $totalRanapECG + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapECGVerif = $totalRanapECGVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRanapECG)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_visite_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_visite_detail?></span></b>
<table id="historyVisiteDokter" class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:35%">Dokter</th>
            <th style="width:10%">Ruangan</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapVisite = 0;?>
        <?php $totalRanapVisiteVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idpelayanan?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=$row->namadokter?></td>
            <td hidden><?=$row->idruangan?></td>
            <td><?=$row->namaruangan?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td hidden><?=number_format($row->subtotal)?></td>
            <td hidden><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2650'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiVisiteModal" data-idrow="<?=$row->iddetail?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataVisiteDokter"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2651'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger removeDataVisiteDokter"><i class="fa fa-trash"></i></button>
                <?php } ?>

                <!-- Action Verifikasi -->
				<?php if (UserAccesForm($user_acces_form,array('2652'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_visite"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2653'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_visite"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
        </tr>
        <?php
            $totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapVisiteVerif = $totalRanapVisiteVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="6"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRanapVisite)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<hr>

<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_alat_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_alat_detail?></span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapSewaAlat = 0;?>
        <?php $totalRanapSewaAlatVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idpelayanan?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2640'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal"
                    data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakan"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2641'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger removeDataTindakan"><i class="fa fa-trash"></i></button>
                <?php } ?>

                <!-- Action Verifikasi -->
				<?php if (UserAccesForm($user_acces_form,array('2642'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2643'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
        </tr>
        <?php
            $totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapSewaAlatVerif = $totalRanapSewaAlatVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRanapSewaAlat)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_ambulance_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_ambulance_detail?></span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapAmbulance = 0;?>
        <?php $totalRanapAmbulanceVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idpelayanan?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2640'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal"
                    data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakan"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2641'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger removeDataTindakan"><i class="fa fa-trash"></i></button>
                <?php } ?>

                <!-- Action Verifikasi -->
				<?php if (UserAccesForm($user_acces_form,array('2642'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2643'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
        </tr>
        <?php
            $totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapAmbulanceVerif = $totalRanapAmbulanceVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRanapAmbulance)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_obat_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_obat_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Obat</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapObat = 0;?>
        <?php $totalRanapObatVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->namaunit?></td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-success"><?=$row->nopendaftaran ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunit?></td>
            <td><?=$row->namaunit?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trawatinap_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalRanapObat = $totalRanapObat + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapObatVerif = $totalRanapObatVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>

        <?php $totalFarmasiObat = 0;?>
        <?php $totalFarmasiObatVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->unitpelayanan?></td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-primary"><?=$row->nopenjualan ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
                <?php if ($row->statusracikan == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiObatVerif = $totalFarmasiObatVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>

        <?php $totalFarmasiReturObat = 0;?>
        <?php $totalFarmasiReturObatVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->unitpelayanan?></td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-danger"><?=$row->nopengembalian ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-success verifTindakan"><i class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger unverifTindakan"><i class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalFarmasiReturObat = $totalFarmasiReturObat + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiReturObatVerif = $totalFarmasiReturObatVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3">
                <?=number_format($totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_alkes_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_alkes_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Alat Kesehatan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapAlkes = 0;?>
        <?php $totalRanapAlkesVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->namaunit?></td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-success"><?=$row->nopendaftaran ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunit?></td>
            <td><?=$row->namaunit?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapAlkesVerif = $totalRanapAlkesVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>

        <?php $totalFarmasiAlkes = 0;?>
        <?php $totalFarmasiAlkesVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran) as $row) { ?>
        <tr>
            <td>Farmasi</td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-primary"><?=$row->nopenjualan ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none">1</td>
            <td>Farmasi</td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiAlkesVerif = $totalFarmasiAlkesVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>

        <?php $totalFarmasiReturAlkes = 0;?>
        <?php $totalFarmasiReturAlkesVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->unitpelayanan?></td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-danger"><?=$row->nopengembalian ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-success verifTindakan"><i class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger unverifTindakan"><i class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalFarmasiReturAlkes = $totalFarmasiReturAlkes + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiReturAlkesVerif = $totalFarmasiReturAlkesVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3">
                <?=number_format($totalRanapAlkes + $totalFarmasiAlkes + $totalFarmasiReturAlkes)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_alkes_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_alkes_detail?></span></b>

<b><span class="label label-default" style="font-size:12px">ALAT BANTU</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Alat Kesehatan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapAlkesBantu = 0;?>
        <?php $totalRanapAlkesBantuVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->namaunit?></td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-success"><?=$row->nopenjualan ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunit?></td>
            <td><?=$row->namaunit?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalRanapAlkesBantu = $totalRanapAlkesBantu + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapAlkesBantuVerif = $totalRanapAlkesBantuVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>

        <?php $totalFarmasiAlkesBantu = 0;?>
        <?php $totalFarmasiAlkesBantuVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkesBantu($idpendaftaran) as $row) { ?>
        <tr>
            <td>Farmasi</td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-primary"><?=$row->nopenjualan ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none">1</td>
            <td>Farmasi</td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalFarmasiAlkesBantu = $totalFarmasiAlkesBantu + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiAlkesBantuVerif = $totalFarmasiAlkesBantuVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>

        <?php $totalFarmasiReturAlkesBantu = 0;?>
        <?php $totalFarmasiReturAlkesBantuVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->unitpelayanan?></td>
            <td><?=DMYFormat($row->tanggal)?><br><span class="text-danger"><?=$row->nopengembalian ?></span></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2660'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form,array('2662'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-success verifTindakan"><i class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2663'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger unverifTindakan"><i class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
			<td style="display: none"><?=DMYFormat($row->tanggal)?></td>
        </tr>
        <?php
            $totalFarmasiReturAlkesBantu = $totalFarmasiReturAlkesBantu + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiReturAlkesBantuVerif = $totalFarmasiReturAlkesBantuVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3">
                <?=number_format($totalRanapAlkesBantu + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu)?>
            </td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_lain_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_lain_detail?></span></b>

<table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRanapLainLain = 0;?>
        <?php $totalRanapLainLainVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, '6') as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idpelayanan?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2640'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal"
                    data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakan"><i class="fa fa-pencil"></i></button>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form,array('2641'))){?>
				<button type="button"  data-idrow="<?=$row->iddetail?>" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger removeDataTindakan"><i class="fa fa-trash"></i></button>
                <?php } ?>

                <!-- Action Verifikasi -->
                <?php if (UserAccesForm($user_acces_form,array('2642'))){?>
				<button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2643'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trawatinap_tindakan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </td>
        </tr>
        <?php
            $totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRanapLainLainVerif = $totalRanapLainLainVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRanapLainLain)?></td>
        </tr>
    </tfoot>
</table>