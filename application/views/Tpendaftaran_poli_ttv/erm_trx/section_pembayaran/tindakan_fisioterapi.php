<b><span class="label label-success" style="font-size:12px"><?=$label_fis_section?></span></b> 
<b><span class="label label-default" style="font-size:12px"><?=$label_fis_detail?></span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalFisio = 0;?>
        <?php $totalFisioVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idfisioterapi?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2714'))){?>
                <button type="button" data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_fisioterapi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <?php }?>
				 <?php if (UserAccesForm($user_acces_form,array('2715'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="trujukan_fisioterapi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger deleteTindakan"><i
                        class="fa fa-trash"></i>
                    Hapus</button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2716'))){?>
                <button type="button" data-idrow="<?=$row->iddetail?>" data-table="trujukan_fisioterapi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2717'))){?>
                <button type="button" data-idrow="<?=$row->iddetail?>" data-table="trujukan_fisioterapi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalFisio = $totalFisio + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFisioVerif = $totalFisioVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalFisio)?></td>
        </tr>
    </tfoot>
</table>