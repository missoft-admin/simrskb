<b><span class="label label-success" style="font-size:12px"><?=$label_ko_sewa_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_sewa_detail?></span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKSewaAlat = 0;?>
        <?php $totalOKSewaAlatVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td hidden><?=$row->idtarif?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden>0</td>
            <td>-</td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2718'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_sewaalat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2720'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_sewaalat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2721'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_sewaalat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalOKSewaAlatVerif = $totalOKSewaAlatVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKSewaAlat)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_alkes_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_alkes_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Transaksi</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Alat Kesehatan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKAlkes = 0;?>
        <?php $totalOKAlkesVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2722'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2724'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2725'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalOKAlkesVerif = $totalOKAlkesVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKAlkes)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_obat_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_obat_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Transaksi</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Obat</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKObat = 0;?>
        <?php $totalOKObatVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2726'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2728'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2729'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKObat = $totalOKObat + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalOKObatVerif = $totalOKObatVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKObat)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_narcose_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_narcose_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Transaksi</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Obat</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKObatNarcose = 0;?>
        <?php $totalOKObatNarcoseVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2730'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_narcose"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2732'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_narcose"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2733'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_narcose"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalOKObatNarcoseVerif = $totalOKObatNarcoseVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKObatNarcose)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_implan_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_implan_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Transaksi</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Implan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKImplan = 0;?>
        <?php $totalOKImplanVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2734'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_implan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2736'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_implan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2737'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_implan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKImplan = $totalOKImplan + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalOKImplanVerif = $totalOKImplanVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKImplan)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_ruangan_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_ruangan_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:20%">Jumlah Setelah Diskon</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKSewaKamar = 0;?>
        <?php $totalOKSewaKamarVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td hidden><?=$row->idtarif?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2738'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiKamarOperasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_ruangan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataSewaKamarOperasi"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2739'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_ruangan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger deleteTindakan"><i
                        class="fa fa-trash"></i>
                    Hapus</button>
                <?php }?>
                <!-- Action Verifikasi -->
				<?php if (UserAccesForm($user_acces_form,array('2740'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_ruangan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifSewaKamarOK"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2741'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_ruangan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifSewaKamarOK"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalOKSewaKamarVerif = $totalOKSewaKamarVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKSewaKamar)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_do_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_do_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:10%">Tindakan</th>
            <th style="width:15%">Nama Dokter</th>
            <th style="width:10%">Tarif</th>
            <th style="width:10%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKJasaDokterOperator = 0;?>
        <?php $totalOKJasaDokterOperatorVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterOpertor($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td hidden><?=$row->idtarif?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=$row->namadokter?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2745'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiDokterOperatorModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasado"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataDokterOperator"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2747'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasado"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2748'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasado"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKJasaDokterOperator = $totalOKJasaDokterOperator + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalOKJasaDokterOperatorVerif = $totalOKJasaDokterOperatorVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKJasaDokterOperator)?></td>
        </tr>
    </tfoot>
</table>
<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_da_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_da_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:10%">Tindakan</th>
            <th style="width:15%">Nama Dokter</th>
            <th style="width:10%">Nominal Acuan</th>
            <th style="width:7%">Persentase (%)</th>
            <th style="width:10%">Tarif</th>
            <th style="width:8%">Diskon (%)</th>
            <th style="width:10%">Total Tarif</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKJasaDokterAnesthesi = 0;?>
        <?php $totalOKJasaDokterAnesthesiVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td hidden><?=$row->idtarif?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=$row->namadokter?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->persen)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->grandtotal)?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2753'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiAsistenOperasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasada"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataAsistenOperasi"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2755'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasada"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2756'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasada"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKJasaDokterAnesthesi = $totalOKJasaDokterAnesthesi + $row->grandtotal;
            if ($row->statusverifikasi == 1) {
                $totalOKJasaDokterAnesthesiVerif = $totalOKJasaDokterAnesthesiVerif + $row->grandtotal;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="8"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKJasaDokterAnesthesi)?></td>
        </tr>
    </tfoot>
</table>

<hr>
<b><span class="label label-success" style="font-size:12px"><?=$label_ko_dao_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ko_dao_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:10%">Tindakan</th>
            <th style="width:15%">Nama Dokter</th>
            <th style="width:10%">Nominal Acuan</th>
            <th style="width:7%">Persentase (%)</th>
            <th style="width:10%">Tarif</th>
            <th style="width:8%">Diskon (%)</th>
            <th style="width:10%">Total Tarif</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalOKJasaAsisten = 0;?>
        <?php $totalOKJasaAsistenVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaloperasi)?></td>
            <td hidden><?=$row->idtarif?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->idpegawai?></td>
            <td><?=$row->namapegawai?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->persen)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->grandtotal)?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
				<?php if (UserAccesForm($user_acces_form,array('2760'))){?>
                <button type="button"  data-toggle="modal" data-target="#EditTransaksiAsistenOperasiModal"
                    data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->namatarif == 'Jasa Asisten Anasthesi' ? 'tkamaroperasi_jasadaa' : 'tkamaroperasi_jasaao')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataAsistenOperasi"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2761'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->namatarif == 'Jasa Asisten Anasthesi' ? 'tkamaroperasi_jasadaa' : 'tkamaroperasi_jasaao')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
				<?php if (UserAccesForm($user_acces_form,array('2762'))){?>
                <button type="button"  data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->namatarif == 'Jasa Asisten Anasthesi' ? 'tkamaroperasi_jasadaa' : 'tkamaroperasi_jasaao')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalOKJasaAsisten = $totalOKJasaAsisten + $row->grandtotal;
            if ($row->statusverifikasi == 1) {
                $totalOKJasaAsistenVerif = $totalOKJasaAsistenVerif + $row->grandtotal;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalOKJasaAsisten)?></td>
        </tr>
    </tfoot>
</table>