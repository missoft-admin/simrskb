<?php $totalRanapRuangan = 0; ?>
<b><span class="label label-success" style="font-size:12px"><?=$label_ranap_ruangan_section?></span></b>
<b><span class="label label-default" style="font-size:12px"><?=$label_ranap_ruangan_detail?></span></b>

<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Ruangan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Hari</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:20%">Jumlah Setelah Diskon</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody id="tempRuanganRanap">
        <?php if ($statusvalidasi == 1) { ?>
        <!-- Tarif Ruangan Rawat Inap Setelah Proses Validasi -->
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapRuanganValidasi($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaldari)?> - <?=DMYFormat($row->tanggalsampai)?></td>
            <td hidden><?=$row->idtarif?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->idruangan?></td>
            <td hidden><?=$row->idkelas?></td>
            <td hidden><?=$row->tanggaldari?></td>
            <td hidden><?=$row->tanggalsampai?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->total)?></td>
            <td><?=number_format($row->jumlahhari)?></td>
            <td><?=number_format($row->total * $row->jumlahhari)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, ($row->total * $row->jumlahhari)); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td>
				<?php if ($disabled_trx_validasi == 0) { ?>
                <?php if ($row->statusverifikasi == 0) { ?>
                <?php if (UserAccesForm($user_acces_form,array('2631'))){?>
				<button data-toggle="modal" type="button" data-idrow="<?=$row->id?>" data-table="trawatinap_ruangan" <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-primary editDataRuangRanap"><i class="fa fa-pencil"></i></button>
                <?php }?>
				<?php if (UserAccesForm($user_acces_form,array('2639'))){?>
				 <button  type="button" data-idrow="<?=$row->id?>" data-table="trawatinap_ruangan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger deleteRuanganRanap"><i
                        class="fa fa-trash"></i>
                    Delete</button>
                <?php }?>
                
                <?php if (UserAccesForm($user_acces_form,array('2632'))){?>
				<button type="button" data-idrow="<?=$row->id?>" data-table="trawatinap_ruangan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifRuanganRanap"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php }?>
                <?php } else { ?>
                <?php if (UserAccesForm($user_acces_form,array('2633'))){?>
                <button  type="button" data-idrow="<?=$row->id?>" data-table="trawatinap_ruangan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifRuanganRanap"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
                <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php $totalRanapRuangan = $totalRanapRuangan + $row->totalkeseluruhan; ?>
        <?php }?>
        <!-- EOF Tarif Ruangan Rawat Inap Setelah Proses Validasi -->
        <?php } else { ?>
        <!-- Tarif Ruangan Rawat Inap Sebelum Validasi -->
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapRuanganGroup($idpendaftaran) as $row) { ?>
        <?php $tarifRanapRuangan = $this->Trawatinap_tindakan_model->getTarifRuangan($row->idkelompokpasien, $row->idruangan, $row->idkelas); ?>
        <?php $totalKeseluruhan = $tarifRanapRuangan['total'] * $row->jumlahhari; ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggaldari)?> - <?=DMYFormat($row->tanggalsampai)?></td>
            <td>Ruang <?=$row->namaruangan?>, Kelas <?=$row->namakelas?></td>
            <td><?=number_format($tarifRanapRuangan['total'])?></td>
            <td><?=number_format($row->jumlahhari)?></td>
            <td><?=number_format($totalKeseluruhan)?></td>
            <td>0</td>
            <td><?=number_format($totalKeseluruhan)?></td>
            <td>
                <button disabled class="btn btn-sm btn-danger"><i class="fa fa-refresh"></i> Menunggu Proses
                    Validasi</button>
            </td>
            <td hidden><?=$idpendaftaran?></td> <!-- td:no::9 -->
            <td hidden><?=$tarifRanapRuangan['idtarif']?></td> <!-- td:no::10 -->
            <td hidden>Ruang <?=$row->namaruangan?>, Kelas <?=$row->namakelas?></td> <!-- td:no::11 -->
            <td hidden><?=$row->idruangan?></td> <!-- td:no::12 -->
            <td hidden><?=$row->idkelas?></td> <!-- td:no::13 -->
            <td hidden><?=YMDFormat($row->tanggaldari)?></td> <!-- td:no::14 -->
            <td hidden><?=YMDFormat($row->tanggalsampai)?></td> <!-- td:no::15 -->
            <td hidden><?=$row->jumlahhari?></td> <!-- td:no::16 -->
            <td hidden><?=$tarifRanapRuangan['jasasarana']?></td> <!-- td:no::17 -->
            <td hidden><?=$tarifRanapRuangan['jasapelayanan']?></td> <!-- td:no::18 -->
            <td hidden><?=$tarifRanapRuangan['bhp']?></td> <!-- td:no::19 -->
            <td hidden><?=$tarifRanapRuangan['biayaperawatan']?></td> <!-- td:no::20 -->
            <td hidden><?=$tarifRanapRuangan['total']?></td> <!-- td:no::21 -->
        </tr>
        <?php $totalRanapRuangan = $totalRanapRuangan + $totalKeseluruhan; ?>
        <?php }?>
        <!-- EOF Tarif Ruangan Rawat Inap Sebelum Validasi -->
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="2"><?=number_format($totalRanapRuangan)?></td>
        </tr>
    </tfoot>
</table>
