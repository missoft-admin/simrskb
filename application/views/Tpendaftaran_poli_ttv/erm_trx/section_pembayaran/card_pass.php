<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=$judul_card_ina?></title>
    <style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
            margin-top: 1,5em;
            margin-left: 2,3em;
            margin-right: 2em;
            margin-bottom: 1.5em;
        }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 {
        font-size: 16px !important;
      }
	  .content-2 td {
        margin: 0px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
	  .content-2 {
        font-size: 14px !important;
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-normal{
		font-size: 16px !important;
      }
	  .text-white{
		color: #fff;
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
        font-weight: bold;
      }
	  .text-judul-rs{
        font-size: 24px  !important;
        font-weight: bold;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
		}
		legend {
			background:#fff;
		}
		.center {
		  display: block;
		  margin-left: auto;
		  margin-right: auto;
		  width: 50%;
		}
    }


  </style>
    <script type="text/javascript">
    	// try {
    		// this.print();
    	// }
    	// catch(e) {
    		// window.onload = window.print;
    	// }
    </script>
  </head>
  <body>
    
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().'assets/upload/logo_setting/'.$logo_card?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_card?><br><?=$telepone_card?><br> <?=$email_card?></td>
		</tr>
		
		
	</table>
	<table class="content">
		
		<tr>
			<td width="100%" class="text-center text-header"><strong><?=$judul_card_ina?></strong><br><i><?=$judul_card_eng?></i></td>
		</tr>
		
		
	</table>
    <br>
	<table class="content-2">
      <tr>
        <td class="" style="width:150px"><strong><?=strip_tags($paragraf1_card_ina)?></strong><br><i><?=strip_tags($paragraf1_card_eng)?></i></td>
      </tr>
	 
    </table>
    <table class="content-2">
      <tr>
        <td class="" style="width:150px"><strong><?=strip_tags($no_register_card_ina)?></strong><br><i><?=strip_tags($no_register_card_eng)?></i></td>
        <td class="text-center" style="width:20px">:</td>
        <td class="text-italic"><?=$nopendaftaran;?></td>
      </tr>
	 
      <tr>
        <td class=""><strong><?=strip_tags($nama_card_ina)?></strong><br><i><?=strip_tags($nama_card_eng)?></i></td>
        <td class="text-center" style="width:20px">:</td>
        <td class="text-italic"><?=strtoupper($namapasien);?></td>
      </tr>
      <tr>
        <td class=""><strong><?=strip_tags($deskripsi_card_ina)?></strong><br><i><?=strip_tags($deskripsi_card_eng)?></i></td>
        <td class="text-center" style="width:20px">:</td>
        <td class="text-italic">Diizinkan Untuk Pulang</td>
      </tr>
      
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
    </table>
	<table class="content-2">
      <tr>
        <td class="" style="width:150px"><strong><?=strip_tags($paragraf2_card_ina)?></strong><br><i><?=strip_tags($paragraf2_card_eng)?></i></td>
      </tr>
	 
    </table>
  
    <table class="content-2">
      <tr>
        <td style="width:70%">&nbsp;</td>
        <td style="width:30%" class="text-center"><strong>Bandung, <?=tanggal_indo_DMY($tanggal_proses_card_pass)?></strong></td>
      </tr>
	  <tr>
        <td style="width:70%">&nbsp;</td>
        <td style="width:30%" class="text-center"><strong><?=strip_tags($yg_menerima_card_ina)?></strong><br><i><?=strip_tags($yg_menerima_card_eng)?></i></td>
      </tr>
	  <tr>
        <td style="width:70%">&nbsp;</td>
        <td style="width:30%" class="text-center">
		
		<img class="" style="width:100px;height:90px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd_user_name/<?=($user_id_proses_card_pass?$user_id_proses_card_pass:$user_id_proses_card_pass)?>" alt="" title="">
		
		</td>
      </tr>
	  <tr>
	  <?
	  $iduser_inputx=($user_id_proses_card_pass?$user_id_proses_card_pass:$user_id_proses_card_pass);
	  $nama_user=get_nama_user_nama($iduser_inputx);
	  $nama_user_generate=get_nama_user_nama($user_id_proses_card_pass);
	  ?>
        <td style="width:70%"><strong><?=strip_tags($footer_card_ina)?></strong> <i><?=strip_tags($footer_card_eng)?></i>
		<br><i><?=strip_tags($generated_card_ina).' : '.$nama_user_generate.' | '.HumanDateLong2($tanggal_proses_card_pass)?> </i> | <strong> ORIGINAL</strong>
		</td>
        <td style="width:30%" class="text-center"> (
		<?=$nama_user_generate?> )
		</td>
      </tr>
     
    </table>
    <br>
    <div class="border-dotted"></div>
    <br>
    
  </body>
</html>
