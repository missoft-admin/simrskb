
            <h5 style="margin-bottom: 10px;">Pembayaran</h5>
			<hr>
            <table id="cara_pembayaran" class="table table-bordered table-striped" style="margin-top: 20px;">
                <tbody>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm " readonly type="text" id="gt_all" name="gt_all"
                                    value="<?=number_format($totalKeseluruhan)?>">
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>DISKON</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input class="form-control text_diskon" <?=(UserAccesForm($user_acces_form,array('2783'))?'':'disabled')?>  <?=($status_proses_kwitansi!='0'?'disabled':'')?> type="text" id="diskon_rp" name="diskon_rp"
                                        placeholder="Discount Rp" value="{diskon_rp}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"> %. </span>
                                    <input class="form-control text_diskon" <?=(UserAccesForm($user_acces_form,array('2784'))?'':'disabled')?>  <?=($status_proses_kwitansi!='0'?'disabled':'')?> type="text" id="diskon_persen" name="diskon_persen"
                                        placeholder="Discount %" value="{diskon_persen}">
                                </div>
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm number" readonly type="text" id="gt_rp" name="gt_rp"
                                    value="<?=($totalKeseluruhan)?>">
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL DEPOSIT </b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm number" readonly type="text" id="gt_deposit"
                                    name="gt_deposit" value="<?=($totaldeposit)?>">
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL HARUS DIBAYAR </b></label>
                        </th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm number" readonly type="text" id="gt_final" name="gt_final"
                                    value="<?=($totalHarusDibayar)?>">
                        </th>
                    </tr>
                    <tr class="bg-light dker">
                        <th colspan="5"></th>
                        <th colspan="2">
							
							<?if ($statuspembayaran=='0'){?>
                            <?php if ((UserAccesForm($user_acces_form,array('2776')))) { ?>
                            <button class="btn btn-success  btn_pembayaran" id="btn_pembayaran" data-toggle="modal"
                                data-target="#PembayaranModal" type="button">Pilih Cara Pembayaran</button>
                            <?php } ?>
                            <?php } ?>
                        </th>
                    </tr>
                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-12">
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <div class="control-group">
                        
                        <div class="row" style="margin-top: 10px; margin-bottom: 25px;">
                            <div class="form-group">
                                <label class="col-md-2 control-label" style="margin-top: 10px;">Tanggal Pembayaran 
                                    :</label>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input <?=($st_edit_tgl_bayar=='0'?'disabled':'')?> type="text" class="js-datepicker form-control"
                                                id="tanggal_pembayaran" data-date-format="dd/mm/yyyy"
                                                placeholder="HH/BB/TTTT" value="<?=HumanDateShort($tanggal_pembayaran)?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input <?=($st_edit_tgl_bayar=='0'?'disabled':'')?> type="text" class="time-datepicker form-control"
                                                id="waktu_pembayaran" value="{waktu_pembayaran}">
                                        </div>
										<?if ($status_proses_kwitansi=='0'){?>
                                        <div class="col-md-8" id="message_pembayaran">
                                            <button type="button" <?=($st_edit_tgl_bayar=='0'?'disabled':'')?> class="btn btn-success" id="btnUpdateTanggalPembayaran"
                                                style="font-size:13px;"><i class="fa fa-filter"></i> Simpan</button>
                                        </div>
										<?}?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12"
                            style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
                            <table id="manage_tabel_pembayaran" class="table table-striped table-bordered"
                                style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">#</th>
                                        <th style="width: 25%;">Jenis Pembayaran</th>
                                        <th style="width: 35%;">Keterangan</th>
                                        <th style="width: 10%;">Nominal</th>
                                        <th style="width: 10%;">Action</th>
                                    </tr>
                                </thead>
                                <input type="hidden" id="rowindex">
                                <input type="hidden" id="nomor">
                                <tbody>
                                    <?php $no = 1;?>
                                    <?php foreach ($detail_pembayaran as $row) { ?>
                                    <tr>
                                        <td hidden><?=$row->idmetode?></td>
                                        <td><?=$no++?></td>
                                        <td><?=metodePembayaran($row->idmetode)?></td>
                                        <td><?=$row->keterangan?></td>
                                        <td hidden><?=$row->nominal?></td>
                                        <td><?=number_format($row->nominal)?></td>
                                        <td hidden>idkasir</td>
                                        <td hidden><?=$row->idmetode?></td>
                                        <td hidden><?=$row->idpegawai?></td>
                                        <td hidden><?=$row->idbank?></td>
                                        <td hidden><?=$row->ket_cc?></td>
                                        <td hidden><?=$row->idkontraktor?></td>
                                        <td hidden><?=$row->keterangan?></td>
                                        <td>
											<?if ($statuspembayaran=='0'){?>
                                           <?php if ((UserAccesForm($user_acces_form,array('2777')))) { ?>
                                            <button type='button' class='btn btn-sm btn-info edit'><i
                                                    class='glyphicon glyphicon-pencil'></i></button>
                                            <?php } ?>
                                            &nbsp;&nbsp;
                                            <?php if ((UserAccesForm($user_acces_form,array('2778')))) { ?>
                                            <button type='button' class='btn btn-sm btn-danger hapus'><i
                                                    class='glyphicon glyphicon-remove'></i></button>
                                            <?php } ?>
                                            <?php }else{ ?>
												<?if ($row->jml_file > 0){?>
												<button type='button' title="Upload File" onclick="show_upload_bukti(<?=$row->id?>)" class='btn btn-sm btn-default'><i class='fa fa-upload'></i> <?=$row->jml_file?> Files</button>
												<?php }else{ ?>
												<button type='button' title="Upload File" onclick="show_upload_bukti(<?=$row->id?>)" class='btn btn-sm btn-warning'><i class='fa fa-upload'></i></button>
												<?php } ?>
                                            <?php } ?>
                                        </td>
                                        <td hidden><?=$row->jaminan?></td>
                                        <td hidden><?=$row->tracenumber?></td>
                                        <?php if ($row->idmetode =='8') { ?>
                                        <td hidden><?=$row->tipekontraktor?></td>
                                        <?php }else{ ?>
                                        <td hidden>0</td>
                                        <?php } ?>
                                    </tr>
                                    <?php }?>
                                </tbody>
                                <tfoot id="foot_pembayaran">
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Diskon Rp.</b></label></th>
                                        <th colspan="2">
                                            <input type="text" class="form-control number input-sm"
                                                id="diskon_pembayaran" readonly value="{diskon_rp}" />
                                        </th>
                                    </tr>
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
                                        <th colspan="2">
                                            <?php if ($total_pembayaran): ?>
                                            <input type="text" class="form-control input-sm " readonly id="bayar_rp"
                                                name="bayar_rp" value="<?=$total_pembayaran?>" />
                                            <?php else: ?>
                                            <input type="text" class="form-control input-sm " readonly id="bayar_rp"
                                                name="bayar_rp" value="{bayar_rp}" />
                                            <?php endif?>
                                        </th>
                                    </tr>
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Sisa Rp.</b></label></th>
                                        <th colspan="2">
                                            <input type="text" class="form-control input-sm " readonly id="sisa_rp"
                                                name="sisa_rp" value="{sisa_rp}" />
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
       
  
        
        <input type="hidden" id="pos_tabel_pembayaran" name="pos_tabel_pembayaran">
        <input type="hidden" id="idpendaftaran" value="{idpendaftaran}">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<div class="pull-right push-10-r">
						<?php if ($bataltransaksi > 0) { ?>
							<button class="btn btn-danger" type="button" data-toggle="modal" data-target="#HistoryBatalModal"><i class="fa fa-history"></i> History Batal</button>&nbsp;
						<?php } ?>
						<?if ($st_simpan=='1'){?>
							<?if ($statuspembayaran=='0'){?>
								<?php if (UserAccesForm($user_acces_form,array('2779'))){ ?>
									<button type="submit" class="btn btn-primary btn_simpan_pembayaran" id="btn_save"><i class="fa fa-save"></i> Simpan Pembayaran</button> &nbsp;
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<?if ($st_simpan_proses=='1'){?>
							<?if ($statuspembayaran=='0'){?>
								<?php if (UserAccesForm($user_acces_form,array('2779'))){ ?>
									<button type="submit" class="btn btn-success btn_simpan_pembayaran" id="btn_save_proses_kwitansi"><i class="si si-check"></i> Simpan & Proses Kwitansi</button> &nbsp;
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
        </div>
		<hr>
		<?if ($status_proses_kwitansi!='0'){?>
		<div class="row">
			<div class="col-sm-12">
				 <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                 </div>
             </div>
			
			<div class="col-sm-12">
				<div class="form-group">
					<div class="col-sm-12">
						<button class="btn btn-success"  title="Proses Kwitansi" onclick="show_modal_kwitansi()" type="button"><span class="fa fa-list"></span> PROSES KWITANSI</button>
						<button class="btn btn-warning" onclick="list_kwitansi()" title="Refresh List Kwitansi" type="button"><span class="si si-reload"></span></button>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<table class="table table-bordered table-striped"  id="tabel_list_kwitansi"width="100%">
							<thead>
								<tr>
									<th class="text-center" style="width: 2%;">#</th>
									<th class="text-center" style="width: 10%;">Tanggal</th>
									<th class="text-center" style="width: 10%;">Jenis Kwitansi</th>
									<th class="text-center" style="width: 15%;">Sudah Diterima dari</th>
									<th class="text-center" style="width: 20%;">Deskripsi</th>
									<th class="text-center" style="width: 10%;">Nominal</th>
									<th class="text-center" style="width: 10%;">Status</th>
									<th class="text-center" style="width: 15%;">Action</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
		<hr>
		<?}?>
<!-- <script type="text/javascript">
    $("#btn_save").click((function() {
        $("#btn_save").attr('disabled', 'disabled');
    }));
</script> -->
