<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		
</style>

<?if ($menu_kiri=='input_bmhp_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_bmhp_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
		// echo $tanggal_pernyataan;
	if ($tanggal_transaksi){
		
		
		$tgltransaksi=HumanDateShort($tanggal_transaksi);
		$waktutransaksi=HumanTime($tanggal_transaksi);
	}else{
		$tgltransaksi=date('d-m-Y');
		$waktutransaksi=date('H:i:s');
	}
	
	$disabel_input='';
	$disabel_cetak='disabled';
	
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1" onclick="load_bmhp_list()"><i class="si si-note"></i> Input BMHP</a>
				</li>
				<li class="">
					<a href="#tab_2" onclick="list_history_bmhp()"><i class="fa fa-history"></i> Riwayat BMHP</a>
				</li>
				<li class="">
					<a href="#tab_3"  onclick="load_bmhp_paket()"><i class="fa fa-copy"></i> Paket</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					<?if($statuskasir!='2'){?>
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<input type="hidden" id="idtipe_poli" value="{idtipe_poli}" >		
					<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}" >		
					<input type="hidden" id="idrekanan" value="{idrekanan}" >		
					<input type="hidden" id="idpoliklinik" value="{idpoliklinik}" >		
					<input type="hidden" id="statuspasienbaru" value="{statuspasienbaru}" >		
					<input type="hidden" id="pertemuan_id" value="{pertemuan_id}" >		
					<input type="hidden" id="transaksi_id" value="" >		
					<input type="hidden" id="st_browse" value="0" >		
					<input type="hidden" id="asal_rujukan" value="{asal_rujukan}" >		
					<input type="hidden" id="idruangan" value="{idruangan}" >		
					<input type="hidden" id="idkelas" value="{idkelas}" >		
					<div class="row">
						<div class="col-md-12">
								<h4 class="font-w700 push-5 text-center text-primary">INPUT BMHP RAWAT INAP</h4>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p>Info <a class="alert-link" href="javascript:void(0)"> Auto Save </a>!</p>
								</div>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
									
									
								</div>
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="idtipe">Tanggal</label>
									<div class="input-group date">
										<input tabindex="1" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tgltransaksi" placeholder="HH/BB/TTTT" name="tgltransaksi" value="<?= $tgltransaksi ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Waktu</label>
									<div class="input-group date">
										<input tabindex="2" type="text"  class="time-datepicker form-control " id="waktutransaksi" name="waktutransaksi" value="<?= $waktutransaksi ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
									
								</div>
								<div class="col-md-3 ">
									<label for="totalkeseluruhan">Unit Pelayanan</label>
									<select tabindex="3" id="idunit"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach($list_unitpelayanan as $r){?>
										<option value="<?=$r->id?>" <?=($idunit_default==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-5 ">
									<label for="totalkeseluruhan">Nama Profesional Pemberi Asuhan (PPA)</label>
										<div class="input-group">
											<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
											<span class="input-group-addon"><i class="fa fa-user"></i></span>
										</div>
								</div>
							</div>
							
						</div>
						<div class="form-group" >
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="idtipe">Tipe Barang</label>
									<select tabindex="4" id="idtipe"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Tipe Barang</option>
										<?foreach($list_tipe as $r){?>
										<option value="<?=$r->id?>" <?=($idtipe==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
									
								</div>
															
								<div class="col-md-8 ">
									<label for="idbarang">Barang</label>
									<div class="input-group">
										<select id="idbarang" style="width:100%"  tabindex="5" data-placeholder="Cari Obat" class="form-control"></select>
										<span class="input-group-btn">
											<button onclick="show_modal_obat()" title="Cari Obat" class="btn btn-primary" type="button" id="search_obat_1"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="satuan">Satuan</label>
									<input class="form-control" disabled type="text" id="satuan" value="">
								</div>
								<div class="col-md-4 ">
									<label for="hargadasar">Harga</label>
									<input class="form-control number" disabled type="text" id="harga_before_diskon" value="">
									<input class="form-control number" disabled type="hidden" id="hargadasar" value="">
									<input class="form-control number" disabled type="hidden" id="margin" value="">
									<input class="form-control number" disabled type="hidden" id="diskon" value="">
									<input class="form-control number" disabled type="hidden" id="diskon_persen" value="">
									<input class="form-control decimal" disabled type="hidden" id="pembulatan" value="">
									<input class="form-control number" disabled type="hidden" id="stok" value="">
								</div>
								<div class="col-md-4 ">
									<label for="hargajual">Harga - (Diskon)</label>
									
									<input class="form-control number" disabled type="text" id="hargajual" value="">
										
									
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label for="idtipe">Kuantitas</label>
									<input class="form-control number" type="text" id="kuantitas" value="">
									
								</div>
								<div class="col-md-8 ">
									<label for="totalkeseluruhan">Total</label>
									<div class="input-group">
										<input class="form-control number" type="text" disabled id="totalkeseluruhan" value="">
										<span class="input-group-btn">
											<?if($statuskasir=='0'){?>
											<button onclick="add_barang()" title="Add barang" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Add </button>
											<button onclick="clear_bmhp()" title="Clear barang" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
											<?}?>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 " id="div_info">
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p id="lbl_info">Info :<strong> aa! </strong></p>
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-header-bg" id="index_obat">
											<thead>
												<tr>
													<th width="10%" class="text-center">UNIT PELAYANAN</th>
													<th width="10%" class="text-center">TANGGAL</th>
													<th width="25%" class="text-center">BARANG</th>
													<th width="12%" class="text-center">HARGA</th>
													<th width="8%" class="text-center">QTY </th>
													<th width="12%" class="text-center">TOTAL</th>
													<th width="13%" class="text-center">USER</th>
													<th width="10%" class="text-center">ACTION</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot >
												<tr>
													<td colspan="4" class="text-right"><strong>TOTAL</strong></td>
													<td class="text-right"><label id="total_kuantitas"></label></td>
													<td class="text-right"><label id="total_penjualan"></label></td>
													<td colspan="2"></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >Input BMHP</h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					<?}?>
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
					<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">DAFTAR INPUT BMHP</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Input</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Kelas Perawatan</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_bmhp()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_bmhp">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">No Pendaftaran</th>
												<th width="10%">Tanggal Kunjungan</th>
												<th width="10%">Tujuan</th>
												<th width="10%">Poliklinik</th>
												<th width="15%">Dokter</th>
												<th width="15%">Total</th>
												<th width="10%">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						</div>
						<hr class="push-5-b">
						<div id="div_history">
						<div class="row">
							<div class="form-group">
								<div class="col-md-4 ">
									
								</div>
								<div class="col-md-8 ">
									<div class="pull-right push-10-r">
										<button class="btn btn-sm btn-default" onclick="back_history()" type="button"><i class="fa fa-reply"></i> Kembali </button>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">HISTORY INPUT BMHP</h4>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-header-bg" id="index_obat_history">
												<thead>
													<tr>
														<th width="10%" class="text-center">UNIT PELAYANAN</th>
														<th width="10%" class="text-center">TANGGAL</th>
														<th width="25%" class="text-center">BARANG</th>
														<th width="12%" class="text-center">HARGA</th>
														<th width="8%" class="text-center">QTY </th>
														<th width="12%" class="text-center">TOTAL</th>
														<th width="13%" class="text-center">USER</th>
														<th width="10%" class="text-center">ACTION</th>
													</tr>
												</thead>
												<tbody></tbody>
												<tfoot >
													<tr>
														<td colspan="4" class="text-right"><strong>TOTAL</strong></td>
														<td class="text-right"><label id="total_kuantitas_history"></label></td>
														<td class="text-right"><label id="total_penjualan_history"></label></td>
														<td colspan="2"></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>	
					</div>	
					
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
					
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row div_filter_paket">
						<input type="hidden" readonly id="paket_id" value="{paket_id}"> 
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" readonly id="status_paket" value="<?=$status_paket?>" >		
						<input type="hidden" readonly id="paket_detail_id" value="" >		
						<div class="col-md-12">
							<h4 class="font-w700 push-5 text-center text-primary">DATA PAKET</h4>
						</div>
					</div>
					<div class="row div_filter_paket">
						<div class="form-group">
							
							<div class="col-md-8 ">
								<?if ($status_paket=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_paket=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Paket Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_paket=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_paket=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								
							</div>
							<div class="col-md-4 ">
								<div class="pull-right push-10-r">
									<?if ($paket_id==''){?>
									<button class="btn btn-primary" id="btn_create_paket" onclick="create_paket_bmhp()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?if ($paket_id!=''){?>
										<?if ($status_paket=='1'){?>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_paket_bmhp()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_paket_bmhp()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<?if ($status_paket=='1'){?>
							<div class="form-group" >
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										<label for="nama_paket">Nama Paket </label>
										<input  id="nama_paket" value="{nama_paket}" class="form-control"  type="text">
										
									</div>
												
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered" id="index_paket">
												<thead>
													<tr>
														<th width="15%" class="text-center">Tipe</th>
														<th width="30%" class="text-center">Barang</th>
														<th width="10%" class="text-center">Satuan</th>
														<th width="10%" class="text-center">Harga</th>
														<th width="8%"  class="text-center">Kuantitas</th>
														<th width="12%" class="text-center">Total</th>
														<th width="10%" class="text-center">ACTION</th>
													</tr>
													<tr>
														<td>
															<select tabindex="15" id="idtipe_paket"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="" selected>Tipe Barang</option>
																<?foreach($list_tipe as $r){?>
																<option value="<?=$r->id?>" <?=($idtipe==$r->id?'selected':'')?>><?=$r->nama?></option>
																<?}?>
																
															</select>
														</td>
														<td>
															<div class="input-group">
																<select id="idbarang_paket" style="width:100%" tabindex="5" data-placeholder="Cari Obat" class="form-control"></select>
																<span class="input-group-btn">
																	<button onclick="show_modal_obat_paket()" title="Cari Obat" class="btn btn-primary" type="button" id="search_obat_paket"><i class="fa fa-search"></i></button>
																</span>
															</div>
														</td>
														<td>
															<input  id="satuan_paket" readonly value="" class="form-control"  type="text">
															<input  id="idsatuan_paket" readonly value="" class="form-control"  type="hidden">
															<input  id="st_browse_paket" readonly value="0" class="form-control"  type="hidden">
														</td>
														<td>
															<input  id="harga_paket" readonly value="" class="form-control number"  type="text">
														</td>
														<td>
															<input  id="kuantitas_paket" value="" class="form-control number"  type="text">
														</td>
														<td>
															<input  id="total_harga_paket" readonly value="" class="form-control number"  type="text">
														</td>
														
														<td>
															<div class="btn-group">
																<button class="btn btn-primary" onclick="add_barang_paket()"  title="Tambah" type="button"><i class="fa fa-plus"></i> Add</button>
																<button class="btn btn-danger" onclick="clear_paket_bmhp()" title="Clear" type="button"><i class="fa fa-refresh"></i></button>
															</div>
														</td>
													</tr>
												</thead>
												<tbody></tbody>
												<tfoot >
													<tr>
														<td colspan="4" class="text-right"><strong>TOTAL</strong></td>
														<td class="text-right"><label id="total_kuantitas_paket"></label></td>
														<td class="text-right"><label id="total_penjualan_paket"></label></td>
														<td colspan="2"></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						<?}?>
					</div>	
					<?if ($status_paket=='0'){?>
					<div class="div_filter_paket">
					<div class="row">
							
							<div class="form-group">
								<div class="col-md-6 ">
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Nama Paket</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="nama_paket_filter" placeholder="Nama Paket" value="">
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_bmhp_paket()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_bmhp_paket">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="30%">Nama Paket</th>
												<th width="15%">Total</th>
												<th width="20%">Uer</th>
												<th width="30%">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						</div>
						
						<div class="div_history_paket">
						<div class="row div_history_paket">
							<div class="form-group">
								<div class="col-md-4 ">
									
								</div>
								<div class="col-md-8 ">
									<div class="pull-right push-10-r">
										<button class="btn btn-sm btn-default" onclick="back_history_paket()" type="button"><i class="fa fa-reply"></i> Kembali </button>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">DETAIL PAKET</h4>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-header-bg" id="index_obat_history_paket">
												<thead>
													<tr>
														<th width="15%" class="text-center">Tipe</th>
														<th width="30%" class="text-center">Barang</th>
														<th width="10%" class="text-center">Satuan</th>
														<th width="10%" class="text-center">Harga</th>
														<th width="8%"  class="text-center">Kuantitas</th>
														<th width="12%" class="text-center">Total</th>
														<th width="10%" class="text-center">ACTION</th>
													</tr>
												</thead>
												<tbody></tbody>
												<tfoot >
													<tr>
														<td colspan="4" class="text-right"><strong>TOTAL</strong></td>
														<td class="text-right"><label id="total_kuantitas_history_paket"></label></td>
														<td class="text-right"><label id="total_penjualan_history_paket"></label></td>
														<td colspan="2"></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>	
					</div>	
					<?}?>
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >Input Data Paket</h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal fade in" id="modal_obat" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Unit Pelayanan</label>
								<div class="col-md-8">
									<select tabindex="3" id="idunit_filter"  class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach($list_unitpelayanan as $r){?>
										<option value="<?=$r->id?>" <?=($idunit_default==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Tipe Barang</label>
								<div class="col-md-8">
									<select id="idtipe_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach($list_tipe as $r){?>
										<option value="<?=$r->id?>" <?=($idtipe==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
							
						</div>
						<div class="col-md-6">
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-9">
									<select id="idkat_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-3 control-label" for="tipeid">Nama Barang</label>
								<div class="col-md-9">
									<input class="form-control" type="text" id="nama_barang_filter" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang" onclick="loadBarang()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="tabel_obat_filter" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>Catatan</th>
								<th>Kartu Stok</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_obat_paket" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Unit Pelayanan</label>
								<div class="col-md-8">
									<select tabindex="3" id="idunit_paket_filter"  class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach($list_unitpelayanan as $r){?>
										<option value="<?=$r->id?>" <?=($idunit_default==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Tipe Barang</label>
								<div class="col-md-8">
									<select id="idtipe_paket_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach($list_tipe as $r){?>
										<option value="<?=$r->id?>" <?=($idtipe==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
							
						</div>
						<div class="col-md-6">
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-9">
									<select id="idkat_paket_filter" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-3 control-label" for="tipeid">Nama Barang</label>
								<div class="col-md-9">
									<input class="form-control" type="text" id="nama_barang_paket_filter" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="idkat"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="loadBarangPaket()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="tabel_obat_paket_filter" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>Catatan</th>
								<th>Kartu Stok</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_obat_copy" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Konfirmasi Penggunaan Paket</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12 text-primary">
							<label class="h5">Periksa Kembali data paket yang akan diterapkan. Barang mungkin tidak tersedia pada unit yang telah dipilih.</label>
						</div>
					</div>
					<div class="row push-10-t">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
								<div class="col-md-4 ">
									<label for="idunit_copy">Unit Pelayanan</label>
									<select tabindex="3" id="idunit_copy"  class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach($list_unitpelayanan as $r){?>
										<option value="<?=$r->id?>" <?=($idunit_default==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<input class="form-control" type="hidden" id="array_paket_id" value="">
						</div>
					</div>
					<div class="row push-10-t">
						<div class="col-md-12">
							<table width="100%" id="tabel_obat_paket_copy" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>#</th>
										<th>Kode Obat</th>
										<th>Nama Obat</th>
										<th>Satuan</th>
										<th>Harga</th>
										<th>Stok</th>
										<th>Catatan</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row push-10-b">
						<div class="col-md-12 text-danger">
							<label class="h5">
								Barang paket yang akan dimasukan kedalam transaksi hanya barang yang berstatus &nbsp;&nbsp;&nbsp; <?=text_primary('TERSEDIA')?>
								<br>
								<br>
							</label>
						</div>
					<br>
					</div>
					<div class="row push-10-b">
						
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
					<button class="btn btn-sm btn-success" type="button" onclick="terapkan_copy()"><i class="fa fa-check"></i> Terapkan</button>
				</div>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	// show_modal_obat();
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var targetTab = $(e.target).attr('href');
		localStorage.setItem('activeTab', targetTab);
	});

	// Baca nilai dari localStorage dan tetapkan tab yang sesuai sebagai tab aktif
	var activeTab = localStorage.getItem('activeTab');
	// alert(activeTab);
	if (activeTab) {
		$('.nav-tabs a[href="' + activeTab + '"]').tab('show');
	}

	$('ul.nav-tabs').on('click', 'li', function () {
		var activeTab = $(this).find('a').attr('href');
		localStorage.setItem('activeTab', activeTab);
	});
	
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	select2_barang();
	clear_bmhp();
	if (activeTab=='#tab_1'){
		load_bmhp_list();
	}
	if (activeTab=='#tab_2'){
		list_history_bmhp();
	}
	if (activeTab=='#tab_3'){
		load_tab3();
	}
	
	
});
function load_tab3(){
	if ($("#paket_id").val()!=''){
		load_bmhp_paket();
	}else{
		list_history_bmhp_paket();
		
	}
	
}
function select2_barang(){
	$("#idbarang").select2({
		minimumInputLength: 2,
		noResults: 'Obat Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		ajax: {
			url: '{site_url}tpoliklinik_trx/get_obat/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,

			data: function(params) {
				var query = {
					search: params.term,
					idtipe: $("#idtipe").val(),
					idunit: $("#idunit").val(),
				}
				return query;
			},
			processResults: function(data) {
				return {
					results: $.map(data, function(item) {
						return {
						text: item.kode+' - '+item.nama ,
							id: item.idbarang,
							idtipe: item.idtipe
						}
					})
				};
			}
		}
	});
	$("#idbarang_paket").select2({
		minimumInputLength: 2,
		noResults: 'Obat Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		ajax: {
			url: '{site_url}tpoliklinik_trx/get_obat/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,

			data: function(params) {
				var query = {
					search: params.term,
					idtipe: $("#idtipe_paket").val(),
					idunit: $("#idunit").val(),
				}
				return query;
			},
			processResults: function(data) {
				return {
					results: $.map(data, function(item) {
						return {
						text: item.kode+' - '+item.nama ,
							id: item.idbarang,
							idtipe: item.idtipe
						}
					})
				};
			}
		}
	});
}
function get_obat_detail(){
	let idtipe_barang=$("#idtipe").val();
	console.log(idtipe_barang);
	data = $("#idbarang").select2('data')[0];
	if (data){
		if ($("#st_browse").val() == 0) {
			idtipe_barang=data.idtipe;
		}
	}
	$("#st_browse").val("0");
	if ($("#idbarang").val() != '') {
		$.ajax({
			url: '{site_url}tpoliklinik_trx/get_obat_detail_ri/',
			dataType: "json",
			type: 'POST',
			data: {
				idbarang:$("#idbarang").val(),
				idtipe:idtipe_barang,
				idkelompokpasien:$("#idkelompokpasien").val(),
				idrekanan:$("#idrekanan").val(),
				asal_rujukan:$("#asal_rujukan").val(),
				idruangan:$("#idruangan").val(),
				idkelas:$("#idkelas").val(),
				idunit:$("#idunit").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#hargadasar").val(data.hargadasar);
				$("#satuan").val(data.singkatan);
				$("#margin").val(data.margin);
				$("#hargajual").val(data.hargajual);
				$("#harga_before_diskon").val(data.harga_before_diskon);
				$("#diskon_persen").val(data.diskon_persen);
				$("#diskon").val(data.diskon_rp);
				$("#pembulatan").val(data.pembulatan);
				$("#stok").val(data.stok);
				$("#lbl_info").text('Stok : '+data.stok);
				$("#div_info").show();
				if (parseFloat($("#kuantitas").val())==0){
					$("#kuantitas").val('1');
					
				}
				
				hitung_total();
				$("#kuantitas").focus().select();
				
			}
		});
	} else {

	}
}
$("#idbarang").change(function() {
	get_obat_detail();
});

$("#kuantitas").keyup(function() {
	hitung_total();
});

function hitung_total(){
	let hargajual=parseFloat($("#hargajual").val());
	let kuantitas=parseFloat($("#kuantitas").val());
	let totalkeseluruhan=hargajual*kuantitas;
	$("#totalkeseluruhan").val(totalkeseluruhan);
}

function show_modal_obat(){
	document.getElementById("modal_obat").style.zIndex = "1201";
	$('#idunit_filter,#idtipe_filter,#idkat_filter').select2({
	   dropdownParent: $('#modal_obat')
	});
	$("#idtipe_filter").val($("#idtipe").val()).trigger('change');
	$("#idunit_filter").val($("#idunit").val()).trigger('change');
	loadBarang();
	$("#modal_obat").modal('show');
}
function show_modal_obat_paket(){
	document.getElementById("modal_obat_paket").style.zIndex = "1201";
	$('#idunit_paket_filter,#idtipe_paket_filter,#idkat_paket_filter').select2({
	   dropdownParent: $('#modal_obat_paket')
	});
	$("#idtipe_paket_filter").val($("#idtipe_paket").val()).trigger('change');
	$("#idunit_paket_filter").val($("#idunit").val()).trigger('change');
	loadBarangPaket();
	$("#modal_obat_paket").modal('show');
}
function clear_bmhp(){
	$("#idbarang").val(null).trigger('change.select2');
	$("#margin").val(0);
	$("#hargajual").val(0);
	$("#diskon").val(0);
	$("#totalkeseluruhan").val(0);
	$("#harga_before_diskon").val(0);
	$("#pembulatan").val(0);
	$("#satuan").val('');
	$("#idbarang").focus();
	$("#kuantitas").val('');
	$("#transaksi_id").val('');
	$("#div_info").hide();
}

function edit_bmhp(id,idtipe){
	$("#transaksi_id").val(id);
	// get_edit_bmhp
	$("cover-spin").show();
	$.ajax({
	  url: '{site_url}tpoliklinik_trx/get_edit_bmhp_ri/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			id : id,
			idtipe : idtipe,
			
	  },success: function(data) {
			$("#waktutransaksi").val(data.waktutransaksi),
			$("#tgltransaksi").val(data.tgltransaksi),
			$("#idtipe").val(idtipe).trigger('change.select2');
			var newOption = new Option(data.nama_barang, data.idobat, true, true);
					
			$("#idbarang").append(newOption);
			// kt.val(null).trigger('change');

			$("#idunit").val(data.idunit).trigger('change.select2');
			$("#idbarang").val(data.idobat).trigger('change.select2');
			$("#hargadasar").val(data.hargadasar);
			$("#margin").val(data.margin);
			$("#hargajual").val(data.hargajual);
			$("#kuantitas").val(data.kuantitas);
			$("#diskon").val(data.diskon);
			$("#totalkeseluruhan").val();
			$("#stok").val(data.stok);
			$("#satuan").val(data.singkatan);
			// $("#satuan").val(data.singkatan);
			$("#harga_before_diskon").val(data.harga_before_diskon);
			$("#diskon_persen").val(data.diskon_persen);
			$("#pembulatan").val(data.pembulatan);
			$("cover-spin").hide();
			hitung_total();
			$("#kuantitas").focus().select();
		}
	});
}
function add_barang(){
	// if (cek_duplicate_barang()==true){
		// swal({
			// title: "Gagal",
			// text: "Barang Duplicate",
			// type: "error",
			// timer: 1500,
			// showConfirmButton: false
		// });
		// return false;
	// }
	if ($("#idbarang").val()==null || $("#idbarang").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Obat / Alkes",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (parseFloat($("#kuantitas").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	let stok=$("#stok").val();
	let stok_baru=parseFloat(stok) - parseFloat($("#kuantitas").val());
	// console.log(stok_baru);
	// if (parseFloat(stok_baru)<0){
		// swal({
			// title: "Gagal",
			// text: "Stok Tidak Mencukupi",
			// type: "error",
			// timer: 1500,
			// showConfirmButton: false
		// });
		// return false;
	// }
	if (parseFloat($("#totalkeseluruhan").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Harga dan Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	// alert($("#idbarang").val());
	// return false;
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_bmhp_ri/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				transaksi_id : $("#transaksi_id").val(),
				tgltransaksi : $("#tgltransaksi").val(),
				waktutransaksi : $("#waktutransaksi").val(),
				pendaftaran_id_ranap : $("#pendaftaran_id_ranap").val(),
				idpasien : $("#idpasien").val(),
				idunit : $("#idunit").val(),
				idtipe : $("#idtipe").val(),
				idobat : $("#idbarang").val(),
				hargadasar : $("#hargadasar").val(),
				margin : $("#margin").val(),
				hargajual : $("#hargajual").val(),
				kuantitas : $("#kuantitas").val(),
				diskon : $("#diskon").val(),
				totalkeseluruhan : $("#totalkeseluruhan").val(),
				statusverifikasi : $("#statusverifikasi").val(),
				// status : $("#status").val(),
				stok : $("#stok").val(),
				idpoliklinik : $("#idpoliklinik").val(),
				harga_before_diskon : $("#harga_before_diskon").val(),
				diskon_persen : $("#diskon_persen").val(),
				pembulatan : $("#pembulatan").val(),
		  },success: function(data) {
				  $("#cover-spin").hide();
			  if (data==true){
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  clear_bmhp();
				  load_bmhp_list();
			  }else{
				  swal({
					title: "Gagal",
					text: "Penyimpanan Gagal",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			  }
			  
			}
		});
}
function load_bmhp_list(){
	$("#cover-spin").show();
	let pendaftaran_id=$("#pendaftaran_id_ranap").val();
		$('#index_obat tbody').empty();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/load_bmhp_list_ri', 
			dataType: "json",
			type: "POST",
			data: {
					idrawatinap: pendaftaran_id,
			},
			success: function(data) {
				$('#index_obat tbody').append(data.tabel);
				$("#total_penjualan").text(data.total_penjualan);
				$("#total_kuantitas").text(data.total_kuantitas);
			
				$("#cover-spin").hide();
				
			}
		});
}
function hapus_bmhp(id,idtipe){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Data Obat ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/hapus_bmhp_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					idtipe:idtipe,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				load_bmhp_list();
			}
		});
	});

}

function loadBarang() {
	var idunit = $("#idunit_filter").val();
	var idtipe = $("#idtipe_filter").val();
	var idkategori = $("#idkat_filter").val();
	var nama_barang = $("#nama_barang_filter").val();
	$('#tabel_obat_filter').DataTable().destroy();
	var table = $('#tabel_obat_filter').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpoliklinik_trx/get_obat_filter/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				idtipe: idtipe,
				idkategori: idkategori,
				nama_barang: nama_barang,
			}
		},
		columnDefs: [{
				"width": "5%",
				"targets": [0],
				"orderable": true
			},
			{
				"width": "10%",
				"targets": [1, 3, 4],
				"orderable": true
			},
			{
				"width": "15%",
				"targets": [5],
				"orderable": true
			},
			{
				"width": "40%",
				"targets": [2],
				"orderable": true
			},
		]
	});
}

$("#idtipe").change(function() {
	$("#idbarang").val(null).trigger('change.select2');
	// clear_bmhp();
});
$("#idtipe_filter").change(function() {
	$("#idtipe").val($("#idtipe_filter").val()).trigger('change');
	load_kategori();
});
function load_kategori() {
	var idtipe = $("#idtipe_filter").val();
	$('#idkat_filter').find('option').remove();
	$('#idkat_filter').append('<option value="#" selected>- Pilih Semua -</option>');
	if (idtipe != '#') {

		$.ajax({
			url: '{site_url}tstockopname/list_kategori/' + idtipe,
			dataType: "json",
			success: function(data) {
				$.each(data, function(i, row) {

					$('#idkat_filter')
						.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
				});
			}
		});

	}
}
function TreeView($level, $name) {
	// console.log($name);
	$indent = '';
	for ($i = 0; $i < $level; $i++) {
		if ($i > 0) {
			$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		} else {
			$indent += '';
		}
	}

	if ($i > 0) {
		$indent += '└─';
	} else {
		$indent += '';
	}

	return $indent + ' ' + $name;
}
$(document).on("click", ".selectObat", function() {
	clear_bmhp();
	$("#st_browse").val("1");
	// alert($(this).data('nama'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe").val(idtipe).trigger('change');
	let nama = ($(this).data('nama'));
	// let newOption = new Option(idobat, nama, true, true);
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang").append(newOption).trigger('change');
	

});
$(document).on("click", ".selectObatPaket", function() {
	clear_bmhp();
	$("#st_browse_paket").val("1");
	// alert($(this).data('nama'));
	let idobat = ($(this).data('idobat'));
	let idtipe = ($(this).data('idtipe'));
	$("#idtipe_paket").val(idtipe).trigger('change');
	let nama = ($(this).data('nama'));
	// let newOption = new Option(idobat, nama, true, true);
	
	let newOption = new Option(nama, idobat, true, true);
					
	$("#idbarang_paket").append(newOption).trigger('change');
	

});
function list_history_bmhp(){
	$("#div_history").hide();
	$("#div_filter").show();
	let pendaftaran_id=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let iddokter=$("#iddokter").val();
	let idpoli=$("#idpoli").val();
	let notransaksi=$("#notransaksi").val();
	let tanggal_input_1=$("#tanggal_input_1").val();
	let tanggal_input_2=$("#tanggal_input_2").val();
	$('#index_history_bmhp').DataTable().destroy();	
	$("#cover-spin").show();
	table = $('#index_history_bmhp').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_trx/list_history_bmhp_ri', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_history_bmhp_paket(){
	$(".div_filter_paket").show();
	$(".div_history_paket").hide();
	let tgl_input_1=$("#tgl_input_1").val();
	let tgl_input_2=$("#tgl_input_2").val();
	let nama_paket=$("#nama_paket_filter").val();
	
	$('#index_history_bmhp_paket').DataTable().destroy();	
	$("#cover-spin").show();
	table = $('#index_history_bmhp_paket').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					// {  className: "text-center", targets:[1,2,3,6,4,0,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_trx/list_history_bmhp_paket', 
				type: "POST" ,
				dataType: 'json',
				data : {
						tgl_input_1:tgl_input_1,
						tgl_input_2:tgl_input_2,
						nama_paket:nama_paket,
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function lihat_data_bmhp(pendaftaran_id){
	$("#cover-spin").show();
	$("#div_filter").hide();
	$("#div_history").show();
	$('#index_obat_history tbody').empty();
	$.ajax({
			url: '{site_url}tpoliklinik_trx/lihat_data_bmhp_ri/',
			dataType: "json",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
				   },
			success: function(data) {
				$('#index_obat_history tbody').append(data.tabel);
				$("#total_penjualan_history").text(data.total_penjualan);
				$("#total_kuantitas_history").text(data.total_kuantitas);
				$("#cover-spin").hide();
			}
		});
	scrollToBottom();
	// window.scrollTo(0, document.body.scrollHeight);
}
function lihat_data_bmhp_paket(paket_id){
	$("#cover-spin").show();
	$(".div_filter_paket").hide();
	$(".div_history_paket").show();
	$('#index_obat_history_paket tbody').empty();
	$.ajax({
			url: '{site_url}tpoliklinik_trx/lihat_data_bmhp_paket/',
			dataType: "json",
			method: "POST",
			data : {
					paket_id:paket_id,
				   },
			success: function(data) {
				$('#index_obat_history_paket tbody').append(data.tabel);
				$("#total_penjualan_history_paket").text(data.total_penjualan);
				$("#total_kuantitas_history_paket").text(data.total_kuantitas);
				$("#cover-spin").hide();
			}
		});
	scrollToBottom();
	// window.scrollTo(0, document.body.scrollHeight);
}
function back_history(){
	$("#div_filter").show();
	$("#div_history").hide();
}
function back_history_paket(){
	$(".div_filter_paket").show();
	$(".div_history_paket").hide();
}
function scrollToBottom() {
  window.scrollTo(0, document.body.scrollHeight);
}
function copy_bmhp(id,idtipe){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Menerapkan Data Obat ke Transaksi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/copy_bmhp_ri', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					pendaftaran_id : $("#pendaftaran_id_ranap").val(),
					idpasien : $("#idpasien").val(),
					idunit : $("#idunit_copy").val(),
					asal_rujukan : $("#asal_rujukan").val(),
					idruangan : $("#idruangan").val(),
					idkelas : $("#idkelas").val(),
					idkelompokpasien : $("#idkelompokpasien").val(),
					idrekanan : $("#idrekanan").val(),
					idtipe : idtipe,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data>0){
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				load_bmhp_list();
					  
				  }else{
					  swal({
						title: "Gagal",
						text: "Barang Tidak ada yang berhasil di insert",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
				  }
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Duplicate Data'});
			}
		});
	});
	

}
function create_paket_bmhp(){
		
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Paket "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/create_paket_bmhp', 
			dataType: "JSON",
			method: "POST",
			data : {
					
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
$("#idbarang_paket").change(function() {
	get_obat_detail_paket();
});
$("#idtipe_paket").change(function() {
	$("#idbarang_paket").val(null).trigger('change.select2');
	// clear_paket_bmhp();
});
function get_obat_detail_paket(){
	let idtipe_barang=$("#idtipe_paket").val();
	console.log(idtipe_barang);
	let data = $("#idbarang_paket").select2('data')[0];
	if (data){
		if ($("#st_browse_paket").val() == 0) {
			idtipe_barang=data.idtipe;
		}
	}
	// alert($("#idunit").val());
	$("#st_browse_paket").val("0");
	if ($("#idbarang_paket").val() != '') {
		$.ajax({
			url: '{site_url}tpoliklinik_trx/get_obat_detail_paket/',
			dataType: "json",
			type: 'POST',
			data: {
				idbarang:$("#idbarang_paket").val(),
				idtipe:idtipe_barang,
				idunit:$("#idunit").val(),
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				}
				$("#idsatuan_paket").val(data.idsatuankecil);
				$("#satuan_paket").val(data.singkatan);
				$("#harga_paket").val(data.hargajual);
				
				// if (parseFloat($("#kuantitas_paket").val())==0){
					$("#kuantitas_paket").val('1');					
				// }
				hitung_total_paket();
				$("#kuantitas_paket").focus().select();
				
			}
		});
	} else {

	}
}
function hitung_total_paket(){
	let hargajual=parseFloat($("#harga_paket").val());
	let kuantitas=parseFloat($("#kuantitas_paket").val());
	let totalkeseluruhan=hargajual*kuantitas;
	$("#total_harga_paket").val(totalkeseluruhan);
}
$("#kuantitas_paket").keyup(function() {
	hitung_total_paket();
});
function clear_paket_bmhp(){
	$("#idbarang_paket").val(null).trigger('change.select2');
	$("#margin").val(0);
	$("#harga_paket").val(0);
	$("#kuantitas_paket").val(0);
	$("#total_harga_paket").val(0);
	$("#satuan_paket").val('');
	$("#idbarang_paket").focus();
	$("#kuantitas_paket").val('');
	$("#paket_detail_id").val('');
}
function add_barang_paket(){
	if ($("#idbarang_paket").val()==null || $("#idbarang_paket").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Obat / Alkes",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (parseFloat($("#kuantitas_paket").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	if (parseFloat($("#total_harga_paket").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Harga dan Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (cek_duplicate_barang_paket()==true){
		swal({
			title: "Gagal",
			text: "Barang Duplicate",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_paket_bmhp/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_detail_id : $("#paket_detail_id").val(),
				paket_id : $("#paket_id").val(),
				idtipe : $("#idtipe_paket").val(),
				idobat : $("#idbarang_paket").val(),
				nama_barang : $("#idbarang_paket option:selected").text(),
				nama_satuan : $("#satuan_paket").val(),
				idsatuan : $("#idsatuan_paket").val(),
				harga : $("#harga_paket").val(),
				kuantitas : $("#kuantitas_paket").val(),
				diskon : $("#diskon").val(),
				total_harga : $("#total_harga_paket").val(),
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			  $("#cover-spin").hide();
			  clear_paket_bmhp();
			  load_bmhp_paket();
			}
		});
}
$(document).on("blur","#nama_paket",function(){
	simpan_paket();
});
function simpan_paket(){
	
	$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_paket/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_id : $("#paket_id").val(),
				status_paket : $("#status_paket").val(),
				nama_paket : $("#nama_paket").val(),
				st_edited : $("#st_edited").val(),
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Paket'});
			  if (data.status_paket!='1'){
				  location.reload();		
			  }
			  
			}
		});
}
function close_paket_bmhp(){
	if ($("#nama_paket").val()==null || $("#nama_paket").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Nama Paket",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menyimpan Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#status_paket").val(2);
		
		simpan_paket();
	});		
	
}
function load_bmhp_paket(){
	$("#cover-spin").show();
	let paket_id=$("#paket_id").val();
		$('#index_paket tbody').empty();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/load_bmhp_paket', 
			dataType: "json",
			type: "POST",
			data: {
					paket_id: paket_id,
			},
			success: function(data) {
				$('#index_paket tbody').append(data.tabel);
				$("#total_penjualan_paket").text(data.total_penjualan);
				$("#total_kuantitas_paket").text(data.total_kuantitas);
			
				$("#cover-spin").hide();
				
			}
		});
}
function edit_paket_detail_bmhp (id){
	$("#paket_detail_id").val(id);
	// get_edit_bmhp
	$("cover-spin").show();
	$.ajax({
	  url: '{site_url}tpoliklinik_trx/get_edit_paket_bmhp/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			id : id,
			
	  },success: function(data) {
			$("#idtipe_paket").val(data.idtipe).trigger('change.select2');
			var newOption = new Option(data.nama_barang, data.idobat, true, true);
					
			$("#idbarang_paket").append(newOption);
			// kt.val(null).trigger('change');

			// $("#idbarang_paket").val(data.idobat);
			$("#harga_paket").val(data.harga);
			$("#kuantitas_paket").val(data.kuantitas);
			$("#idsatuan_paket").val(data.idsatuan);
			$("#satuan_paket").val(data.nama_satuan);
			$("#total_harga_paket").val(data.total_harga);
			$("cover-spin").hide();
			$("#kuantitas_paket").focus().select();
			// hitung_total_paket();
		}
	});
}
function hapus_paket_detail_bmhp(id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Detail Paket ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/hapus_paket_detail_bmhp', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				load_bmhp_paket();
			}
		});
	});

}
function loadBarangPaket() {
	var idunit = $("#idunit_paket_filter").val();
	var idtipe = $("#idtipe_paket_filter").val();
	var idkategori = $("#idkat_paket_filter").val();
	var nama_barang = $("#nama_barang_paket_filter").val();
	$('#tabel_obat_paket_filter').DataTable().destroy();
	var table = $('#tabel_obat_paket_filter').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpoliklinik_trx/get_obat_paket_filter/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				idtipe: idtipe,
				idkategori: idkategori,
				nama_barang: nama_barang,
			}
		},
		columnDefs: [{
				"width": "5%",
				"targets": [0],
				"orderable": true
			},
			{
				"width": "10%",
				"targets": [1, 3, 4],
				"orderable": true
			},
			{
				"width": "15%",
				"targets": [5],
				"orderable": true
			},
			{
				"width": "40%",
				"targets": [2],
				"orderable": true
			},
		]
	});
}
function cek_duplicate_barang(){
	let status_find=false;
	$('#index_obat tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
			if ($("#idtipe").val()==idtipe && $("#idbarang").val()==idbarang && $("#transaksi_id").val()!=transaksi_id){
				status_find=true;
			}
		// 
	});
	return status_find;
}
function cek_duplicate_barang_paket(){
	let status_find=false;
	$('#index_paket tbody tr').each(function() {
		let idtipe=$(this).find(".cls_idtipe").val();
		let idbarang=$(this).find(".cls_idobat").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
		if ($("#idtipe_paket").val()==idtipe && $("#idbarang_paket").val()==idbarang && $("#paket_detail_id").val()!=transaksi_id){
			status_find=true;
		}
	});
	return status_find;
}
function edit_bmhp_paket(paket_id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Edit Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
		  url: '{site_url}tpoliklinik_trx/simpan_paket_edit/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_id : paket_id,
				status_paket : 1,
				st_edited : 1,
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Paket'});
			  if (data.status_paket=='1'){
				  location.reload();		
			  }
			  
			}
		});
	});		
	
}
function batal_paket_bmhp(){
	let paket_id=$("#paket_id").val();
	let st_edited=$("#st_edited").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Membatalkan Simpan Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}tpoliklinik_trx/batal_paket_bmhp', 
			dataType: "JSON",
			method: "POST",
			data : {
					paket_id:paket_id,
					st_edited:st_edited,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});		
	
}
function hapus_bmhp_paket(paket_id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Paket?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
		  url: '{site_url}tpoliklinik_trx/hapus_bmhp_paket/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				paket_id : paket_id,
				
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Paket'});
			  list_history_bmhp_paket();
			  
			}
		});
	});		
}

function copy_bmhp_paket_detail(detail_id){
	document.getElementById("modal_obat_paket").style.zIndex = "1201";
	$('#idunit_copy').select2({
	   dropdownParent: $('#modal_obat_copy')
	});
	$("#array_paket_id").val(detail_id);
	$("#modal_obat_copy").modal('show');
	loadBarangCopy();
}
function copy_bmhp_paket(detail_id){
	document.getElementById("modal_obat_paket").style.zIndex = "1201";
	$('#idunit_copy').select2({
	   dropdownParent: $('#modal_obat_copy')
	});
	$("#array_paket_id").val(detail_id);
	$("#modal_obat_copy").modal('show');
	loadBarangCopy();
}
function loadBarangCopy() {
	var idkelompokpasien = $("#idkelompokpasien").val();
	var idunit = $("#idunit_copy").val();
	var array_paket_id = $("#array_paket_id").val();
	$('#tabel_obat_paket_copy').DataTable().destroy();
	var table = $('#tabel_obat_paket_copy').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpoliklinik_trx/loadBarangCopy/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				array_paket_id: array_paket_id,
				idkelompokpasien: idkelompokpasien,
			}
		},
		columnDefs: [{
				"width": "5%",
				"targets": [0],
				"orderable": true
			},
			{
				"width": "10%",
				"targets": [1, 3, 4,5],
				"orderable": true
			},
			{
				"width": "12%",
				"targets": [6,7],
				"orderable": true
			},
			{
				"width": "30%",
				"targets": [2],
				"orderable": true
			},
			{  className: "text-right", targets:[4] },
			{  className: "text-center", targets:[0,1,3,5,6,7] },
		]
	});
}
$("#idunit_copy").change(function(){
	loadBarangCopy();
})
function terapkan_copy(){
	$("#modal_obat_copy").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerapkan Data Obat ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
		  url: '{site_url}tpoliklinik_trx/terapkan_copy_ri/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id : $("#pendaftaran_id_ranap").val(),
				idpasien : $("#idpasien").val(),
				idunit : $("#idunit_copy").val(),
				idbarang : $("#array_paket_id").val(),
				asal_rujukan : $("#asal_rujukan").val(),
				idruangan : $("#idruangan").val(),
				idkelas : $("#idkelas").val(),
				idkelompokpasien : $("#idkelompokpasien").val(),
				idrekanan : $("#idrekanan").val(),
		  },success: function(data) {
			  $("#cover-spin").hide();
			  if (data>0){
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  
			  }else{
				  swal({
					title: "Gagal",
					text: "Barang Tidak ada yang berhasil di insert",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			  }
			 $("#modal_obat_copy").modal('hide');
			 load_bmhp_list();
			}
		});
	});
	
	
}
</script>