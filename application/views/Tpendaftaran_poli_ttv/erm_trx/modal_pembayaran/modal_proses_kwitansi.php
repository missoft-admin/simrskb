<!-- Modal Proses Kwitasi -->
<div class="modal in" id="ProsesKwitansiModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout" style="width:50%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Proses Kwitansi</h3>
                </div>
				
                <div class="block-content">
                    <div class="form-horizontal">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 control-label" for="" style="margin-top: 5px;">Jenis Kwitansi</label>
                            <div class="col-md-9">
                                <select id="kwitansi_idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Jenis Kwitansi">
									
								</select>
                            </div>
                        </div>
						<div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 control-label" for="" style="margin-top: 5px;">Nominal Kwitansi</label>
                            <div class="col-md-9">
                                <input type="text" id="kwitansi_nominal" readonly class="form-control number" placeholder="Nominal" value="">
                            </div>
                        </div>
						<div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 control-label" for="" style="margin-top: 5px;">Tanggal</label>
                            <div class="col-md-9">
								<div class="input-group date">
									<input class="js-datepicker form-control" data-date-format="dd/mm/yyyy" type="text" value="<?=(date('d-m-Y'))?>" id="kwitansi_tanggal" <?=($st_edit_tgl_kwitansi=='0'?'disabled':'')?> placeholder="Choose a date..">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
                            </div>
                        </div>
						<div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 control-label" for=""  style="margin-top: 5px;">Sudah Terima Dari</label>
                            <div class="col-md-9">
                                <input type="text" id="kwitansi_terima_dari" class="form-control" placeholder="Sudah Terima Dari" value="<?=($st_default_nama=='1'?$namapasien:'')?>">
                            </div>
                        </div>
						
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 control-label" for="" style="margin-top: 5px;">Deskripsi</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="kwitansi_deskripsi" placeholder="Deskripsi" rows="2"></textarea>
                            </div>
                        </div>
                    </div>

                    <br><br>

                    
                </div>
            </div>
            <div class="modal-footer">
                
                <input type="hidden" id="kwitansi_tipe_kontraktor" value="">
                <input type="hidden" id="kwitansi_idkontraktor" value="">
                <input type="hidden" id="kwitansi_idedit" value="">
				<?if ($st_simpan_kwitansi){?>
                <button onclick="updateproseskwitansi(0)" class="btn btn-sm btn-success btn_proses_kwitansi" type="button"><i class="fa fa-save"></i> Proses </button>
				<?}?>
				<?if ($st_simpan_generate_kwitansi){?>
                <button onclick="updateproseskwitansi(1)" class="btn btn-sm btn-primary btn_proses_kwitansi" type="button"><i class="si si-check"></i> Proses Selesai</button>
				<?}?>
                <button class="btn btn-sm btn-default" onclick="tutup_modal_kwitansi()" type="button" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in black-overlay" id="AlasanHapusKwitansiModal" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Hapus Kwitansi</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
				<input type="hidden" class="form-control" id="alasanhapus_id" value="">
				<input type="text" class="form-control" id="alasanhapus" rows="4" value="">
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" onclick="hapus_kwitansi()" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
						<input type="hidden" class="form-control" id="idtrx_approval" placeholder="" name="idtrx_approval" value="">
						<div class="form-group" style="margin-bottom: 18px;">
						<div class="col-md-12">
							<label class="control-label" for="">Alasan Hapus</label>
							<input type="text" class="form-control" id="alasanhapus_2" value="">
						</div>
						</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group" style="margin-bottom: 8px;">
						<div class="col-md-12">
						<div class="col-md-12">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:20%">Step</th>
										<th style="width:30%">Deskripsi</th>
										<th style="width:20%">User</th>
										<th style="width:15%">Jika Setuju</th>
										<th style="width:15%">Jika Menolak</th>
									</tr>
									
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:25%">Step</th>
										<th style="width:25%">Deskripsi</th>
										<th style="width:25%">User</th>
										<th style="width:25%">Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">

</script>