<div class="modal fade in black-overlay" id="modal_bukti_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">UPLOAD BUKTI PEMBAYARAN</h3>
				</div>
				<div class="block-content">
									
					<input type="hidden" class="form-control" name="idupload_pembayaran2" id="idupload_pembayaran2" value="" readonly>
					
					<form action="{base_url}Tbilling_ranap/upload_pembayaran" enctype="multipart/form-data"  class="dropzone" id="image-upload-pembayaran">
						<div class="row" disabled>
							<div class="form-group" style="margin-bottom: 8px;">
								<div class="col-md-12">
									<input type="hidden" class="form-control" name="idupload_pembayaran" id="idupload_pembayaran" value="" readonly>
									<div>
									  <h5>Bukti Upload</h5>
									</div>
								</div>

							</div>
						</div>
					</form>
					
					<br>
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="form-group" style="margin-bottom: 8px;">
							<div class="col-md-12">
							<div class="col-md-12">
								<table class="table table-bordered" id="list_file_pembayaran">
									<thead>
										<tr>
											<th width="50%" class="text-center">File</th>
											<th width="15%" class="text-center">Size</th>
											<th width="35%" class="text-center">User</th>
											<th width="35%" class="text-center">X</th>
											
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							</div>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>