<!-- Modal History Pembatalan -->
<div class="modal in" id="HistoryBatalModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">History Pembatalan</h3>
                </div>
                <div class="block-content">
                    <table class="table table-bordered table-striped" width="100%">
						<thead>
							<tr>
								<th class="text-center" style="width: 5%;">Action</th>
								<th class="text-center" style="width: 10%;">User</th>
								<th class="text-center" style="width: 15%;">Alasan</th>
								<th class="text-center" style="width: 8%;">Sub Total</th>
								<th class="text-center" style="width: 10%;">Diskon</th>
								<th class="text-center" style="width: 8%;">Total</th>
								<th class="text-center" style="width: 8%;">Total Deposit</th>
								<th class="text-center" style="width: 8%;">Total Dibayar</th>
								<th class="text-center" style="width: 8%;">Sisa</th>
								<th class="text-center" style="width: 10%;">Waktu Transaksi</th>
							</tr>
						</thead>
                        <tbody>
                            <?php foreach ($historyBatal as $row) { ?>
                            <tr>
								<td class="text-center"><button class="btn btn-warning btn-xs" type="button" onclick="list_pemabayaran(<?=$row->id?>)"><i class="fa fa-align-justify"></i></button></td>
								<td class="text-center"><?=$row->namauser.'<br>'.HumanDateLong($row->tanggalbatal) ?> </td>
								<td class="text-center text-primary"><?=$row->alasanbatal_label.'<br>'.$row->alasan_batal_transaksi?></td>
								<td class="text-right"><?=number_format($row->subtotal,0)?></td>
								<td class="text-right"><?=number_format($row->diskonrp,0)?> (<?=number_format($row->diskonpersen,2)?> %)</td>
								<td class="text-right"><?=number_format($row->total,0)?></td>
								<td class="text-right"><?=number_format($row->deposit,0)?></td>
								<td class="text-right"><?=number_format($row->pembayaran,0)?></td>
								<td class="text-right"><?=number_format($row->sisa,0)?></td>
								<td class="text-center"><?=$row->namauserinput.'<br>'.HumanDateLong($row->tanggal) ?> </td>
                                
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal History Pembatalan -->
<div class="modal in" id="HistoryBatalTransaksiModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">History Pembatalan Detail</h3>
                </div>
                <div class="block-content">
                    <table class="table table-bordered table-striped"  id="tabel_list_trx"width="100%">
						<thead>
							<tr>
								<th class="text-center" style="width: 10%;">#</th>
								<th class="text-center" style="width: 20%;">Jenis Pembayaran</th>
								<th class="text-center" style="width: 40%;">Keterangan</th>
								<th class="text-center" style="width: 20%;">Nominal</th>
								<th class="text-center" style="width: 20%;">File</th>
							</tr>
						</thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" onclick="show_modal_batal()" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>