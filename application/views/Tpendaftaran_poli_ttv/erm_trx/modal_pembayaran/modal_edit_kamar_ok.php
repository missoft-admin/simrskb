<div class="modal fade in black-overlay" id="EditTransaksiKamarOperasiModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI KO</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tKamarOperasiRowId" readonly value="">
							<input type="hidden" id="tKamarOperasiTable" readonly value="">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tKamarOperasiNoRegistrasi">No. Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tKamarOperasiNoRegistrasi" placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tKamarOperasiNamaTarif">Nama Tarif</label>
									<div class="col-md-9">
										<input type="hidden" id="tKamarOperasiIdTarif">
										<input type="text" class="form-control input-sm" id="tKamarOperasiNamaTarif" placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tKamarOperasiTanggal">Tanggal Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tKamarOperasiTanggal" placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tKamarOperasiItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Item Biaya</th>
								<th style="width: 25%;">Harga</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jasa Sarana</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2742'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiHarga" id="tKamarOperasiJasaSarana" placeholder="Jasa Sarana"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm discount tdKamarOperasiDiskonPersen" id="tKamarOperasiJasaSaranaDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiDiskonRupiah" id="tKamarOperasiJasaSaranaDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tKamarOperasiJasaSaranaTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Jasa Pelayanan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2742'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiHarga" id="tKamarOperasiJasaPelayanan" placeholder="Jasa Pelayanan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm discount tdKamarOperasiDiskonPersen" id="tKamarOperasiJasaPelayananDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiDiskonRupiah" id="tKamarOperasiJasaPelayananDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tKamarOperasiJasaPelayananTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>BHP</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2742'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiHarga" id="tKamarOperasiBHP" placeholder="BHP"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm discount tdKamarOperasiDiskonPersen" id="tKamarOperasiBHPDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiDiskonRupiah" id="tKamarOperasiBHPDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tKamarOperasiBHPTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Biaya Perawatan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2742'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiHarga" id="tKamarOperasiBiayaPerawatan" placeholder="Biaya Perawatan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm discount tdKamarOperasiDiskonPersen" id="tKamarOperasiBiayaPerawatanDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2743'))?'':'disabled')?> type="text" class="form-control input-sm number tdKamarOperasiDiskonRupiah" id="tKamarOperasiBiayaPerawatanDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tKamarOperasiBiayaPerawatanTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Sub Total (Rp)</b></label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tKamarOperasiSubTotal" placeholder="Sub Total (Rp)" readonly value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Kuantitas</b></label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tKamarOperasiKuantitas" readonly placeholder="Kuantitas" value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4"><label class="pull-right"><b>Total (Rp)</b></label></th>
								<th>
									<input type="text" class="form-control input-sm number" id="tKamarOperasiTotal" placeholder="Total (Rp)" readonly  value="0">
								</th>
							</tr>
							<tr>
								<th style="width: 85%;" colspan="4">
									<label class="pull-right">
										<b>Discount Semua</b>
									</label>
								</th>
								<th style="width: 15%;">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input <?=(UserAccesForm($user_acces_form,array('2744'))?'':'disabled')?> class="form-control number input-sm" type="text" id="tKamarOperasiDiskonRupiah" placeholder="Discount (Rp)" value="0">
										</div>
										<div class="input-group">
											<span class="input-group-addon"> %. </span>
											<input <?=(UserAccesForm($user_acces_form,array('2744'))?'':'disabled')?> class="form-control discount input-sm" type="text" id="tKamarOperasiDiskonPersen" placeholder="Discount (%)" value="0">
										</div>
								</th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right">
										<b>Grand Total (Rp)</b>
									</label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tKamarOperasiGrandTotal" placeholder="Grand Total (Rp)" readonly value="0">
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataSewaKamarOperasi">
				<button class="btn btn-sm btn-primary" id="saveDataSewaKamarOperasi" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataSewaKamarOperasi" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('keyup', '#tKamarOperasiKuantitas', function() {
			var hargajual = $('#tKamarOperasiSubTotal').val().replace(/,/g, '');
			var kuantitas = $(this).val().replace(/,/g, '');
			$('#tKamarOperasiTotal').val(parseInt(hargajual) * parseInt(kuantitas));
		});

		$(document).on('click', '#saveDataSewaKamarOperasi', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var idrow = $('#tKamarOperasiRowId').val();
			var table = $('#tKamarOperasiTable').val();
			var tanggal = $('#tKamarOperasiTanggal').val();
			var idpelayanan = $('#tKamarOperasiIdTarif').val();
			var jasasarana = $('#tKamarOperasiJasaSarana').val();
			var jasasarana_disc = $('#tKamarOperasiJasaSaranaDiskonRupiah').val();
			var jasapelayanan = $('#tKamarOperasiJasaPelayanan').val();
			var jasapelayanan_disc = $('#tKamarOperasiJasaPelayananDiskonRupiah').val();
			var bhp = $('#tKamarOperasiBHP').val();
			var bhp_disc = $('#tKamarOperasiBHPDiskonRupiah').val();
			var biayaperawatan = $('#tKamarOperasiBiayaPerawatan').val();
			var biayaperawatan_disc = $('#tKamarOperasiBiayaPerawatanDiskonRupiah').val();
			var subtotal = $('#tKamarOperasiSubTotal').val();
			var kuantitas = $('#tKamarOperasiKuantitas').val();
			var totalkeseluruhan = $('#tKamarOperasiTotal').val();
			var diskon = $('#tKamarOperasiDiskonRupiah').val();

			if(tanggal == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#tKamarOperasiTanggal').focus();
				});
				return false;
			}

			if(idpelayanan == ''){
				sweetAlert("Maaf...", "Tindakan belum dipilih !", "error").then((value) => {
					$("#tKamarOperasiIdTarif").focus();
				});
				return false;
			}

			if(subtotal == '' || subtotal <= 0){
				sweetAlert("Maaf...", "Sub Total tidak boleh kosong !", "error").then((value) => {
					$('#tKamarOperasiTarif').focus();
				});
				return false;
			}

			if(kuantitas == '' || kuantitas <= 0){
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#tKamarOperasiKuantitas').focus();
				});
				return false;
			}

			if(totalkeseluruhan == '' || totalkeseluruhan <= 0){
				sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
					$('#tKamarOperasiKuantitas').focus();
				});
				return false;
			}
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}trawatinap_tindakan/updateSewaKamarOK',
				method: 'POST',
				data: {
					idpendaftaran: idpendaftaran,
					idrow: idrow,
					table: table,
					jasasarana: jasasarana,
					jasasarana_disc: jasasarana_disc,
					jasapelayanan: jasapelayanan,
					jasapelayanan_disc: jasapelayanan_disc,
					bhp: bhp,
					bhp_disc: bhp_disc,
					biayaperawatan: biayaperawatan,
					biayaperawatan_disc: biayaperawatan_disc,
					subtotal: subtotal,
					diskon: diskon,
					totalkeseluruhan: (totalkeseluruhan - diskon)
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Sewa Kamar Operasi Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataSewaKamarOperasi', function() {
		  var idrow = $(this).data('idrow');
		  var table = $(this).data('table');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();
			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();
			var jasasarana = $(this).closest('tr').find("td:eq(4)").html();
			var jasasarana_disc = $(this).closest('tr').find("td:eq(5)").html();
			var jasapelayanan = $(this).closest('tr').find("td:eq(6)").html();
			var jasapelayanan_disc = $(this).closest('tr').find("td:eq(7)").html();
			var bhp = $(this).closest('tr').find("td:eq(8)").html();
			var bhp_disc = $(this).closest('tr').find("td:eq(9)").html();
			var biayaperawatan = $(this).closest('tr').find("td:eq(10)").html();
			var biayaperawatan_disc = $(this).closest('tr').find("td:eq(11)").html();
			var subtotal = $(this).closest('tr').find("td:eq(12)").html();
			var kuantitas = $(this).closest('tr').find("td:eq(13)").html();
			var total = $(this).closest('tr').find("td:eq(14)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(15)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(16)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(17)").html();

			$("#tKamarOperasiRowId").val(idrow);
			$("#tKamarOperasiTable").val(table);
			$("#tKamarOperasiNoRegistrasi").val(nopendaftaran);
			$("#tKamarOperasiTanggal").val(tanggal);

			$('#tKamarOperasiIdTarif').val(idtarif);
			$('#tKamarOperasiNamaTarif').val(namatarif);

			$("#tKamarOperasiJasaSarana").val(jasasarana);
			$("#tKamarOperasiJasaSaranaDiskonRupiah").val(jasasarana_disc);

			$("#tKamarOperasiJasaPelayanan").val(jasapelayanan);
			$("#tKamarOperasiJasaPelayananDiskonRupiah").val(jasapelayanan_disc);

			$("#tKamarOperasiBHP").val(bhp);
			$("#tKamarOperasiBHPDiskonRupiah").val(bhp_disc);

			$("#tKamarOperasiBiayaPerawatan").val(biayaperawatan);
			$("#tKamarOperasiBiayaPerawatanDiskonRupiah").val(biayaperawatan_disc);

			$("#tKamarOperasiSubTotal").val(subtotal);
			$("#tKamarOperasiKuantitas").val(kuantitas);
			$("#tKamarOperasiTotal").val(total);

			$("#tKamarOperasiDiskonRupiah").val(total_disc_rp);
			$("#tKamarOperasiDiskonPersen").val(total_disc_percent);
			$("#tKamarOperasiGrandTotal").val(grandtotal);

			calculateDiscountKamarOperasi();
		});

		$(document).on('click', '#cancelDataSewaKamarOperasi', function() {
			$("#tKamarOperasiRowId").val('');
			$("#tKamarOperasiTable").val('');

			$("#tKamarOperasiNoRegistrasi").val('');
			$("#tKamarOperasiTanggal").val('');

			$('#tKamarOperasiIdTarif').val('');
			$('#tKamarOperasiNamaTarif').val('');

			$("#tKamarOperasiJasaSarana").val('');
			$("#tKamarOperasiJasaSaranaDiskonPersen").val('');

			$("#tKamarOperasiJasaPelayanan").val('');
			$("#tKamarOperasiJasaPelayananDiskonPersen").val('');

			$("#tKamarOperasiBHP").val('');
			$("#tKamarOperasiBHPDiskonPersen").val('');

			$("#tKamarOperasiBiayaPerawatan").val('');
			$("#tKamarOperasiBiayaPerawatanDiskonPersen").val('');

			$("#tKamarOperasiSubTotal").val('');
			$("#tKamarOperasiKuantitas").val('');
			$("#tKamarOperasiTotal").val('');

			$("#tKamarOperasiDiskonRupiah").val('');
			$("#tKamarOperasiDiskonPersen").val('');
			$("#tKamarOperasiGrandTotal").val('');
		});

		$(document).on('click', '.verifSewaKamarOK', function() {
		  var idrow = $(this).data('idrow');
		  var table = $(this).data('table');

		  event.preventDefault();
		  swal({
	      title: "Apakah anda yakin ingin memverifikasi data ini?",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText: "Ya",
	      cancelButtonText: "Tidak",
	      closeOnConfirm: false,
	      closeOnCancel: false
	    }).then(function() {
	      $.ajax({
	        url: '{site_url}trawatinap_tindakan/updateStatusSewaKamarOK',
	        method: 'POST',
	        data: {
	          idrow: idrow,
	          table: table,
	          statusverifikasi: '1'
	        },
	        success: function(data) {
	          swal({
	            title: "Berhasil!",
	            text: "Sewa Kamar Operasi Telah Diverifikasi.",
	            type: "success",
	            timer: 1500,
	            showConfirmButton: false
	          });
						location.reload();
	        }
	      });
	    });
    });

    $(document).on('click', '.unverifSewaKamarOK', function() {
		  var idrow = $(this).data('idrow');
		  var table = $(this).data('table');

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin membatalkan verifikasi data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      $.ajax({
		        url: '{site_url}trawatinap_tindakan/updateStatusSewaKamarOK',
		        method: 'POST',
		        data: {
		          idrow: idrow,
		          table: table,
		          statusverifikasi: '0'
		        },
		        success: function(data) {
    					swal({
    						title: "Berhasil!",
    						text: "Sewa Kamar Operasi Telah Dibatalkan.",
    						type: "success",
    						timer: 1500,
    						showConfirmButton: false
    					});
    					location.reload();
		        }
		      });
		    });
		});

		$("#tKamarOperasiDiskonPersen").keyup(function(){
			if ($("#tKamarOperasiDiskonPersen").val() == ''){
				$("#tKamarOperasiDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100){
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tKamarOperasiTotal").val() * parseFloat($(this).val())/100);
			$("#tKamarOperasiDiskonRupiah").val(discount_rupiah);

			calculateTotalSewaKamarOperasi();
		});

		$("#tKamarOperasiDiskonRupiah").keyup(function(){
			if ($("#tKamarOperasiDiskonRupiah").val()==''){
				$("#tKamarOperasiDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tKamarOperasiTotal").val()){
				$(this).val($("#tKamarOperasiTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val()*100))/$("#tKamarOperasiTotal").val());
			$("#tKamarOperasiDiskonPersen").val(discount_percent);

			calculateTotalSewaKamarOperasi();
		});

		$("#tKamarOperasiKuantitas").keyup(function(){
			calculateTotalSewaKamarOperasi();
		});

		$(document).on("keyup", ".tdKamarOperasiHarga", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	var beforePrice = $(this).val();
    	var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(beforePrice);
    	$(this).closest('tr').find("td:eq(2) input").val(discount);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));

    	calculateTotalSewaKamarOperasi();
    	return false;
    });

    $(document).on("keyup", ".tdKamarOperasiDiskonPersen", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var afterPrice = 0;
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val()/100)));

    	$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

    	calculateTotalSewaKamarOperasi();
    	return false;
    });

    $(document).on("keyup", ".tdKamarOperasiDiskonRupiah", function () {//Diskon RP Keyup
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > beforePrice){
    		$(this).val(beforePrice);
    	}

    	var afterPrice = 0;
    	afterPrice = parseFloat($(this).val()*100) / parseFloat(beforePrice);

    	$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

    	calculateTotalSewaKamarOperasi();
    	return false;
    });
});

function calculateTotalSewaKamarOperasi() {
	var subTotal, totalTransaction, totalDiscountRupiah;

	subTotal = (parseFloat($("#tKamarOperasiJasaSaranaTotal").val()) + parseFloat($("#tKamarOperasiJasaPelayananTotal").val()) + parseFloat($("#tKamarOperasiBHPTotal").val()) + parseFloat($("#tKamarOperasiBiayaPerawatanTotal").val()));
	$("#tKamarOperasiSubTotal").val(subTotal);

	total = parseFloat(subTotal) * parseFloat($("#tKamarOperasiKuantitas").val());
	$("#tKamarOperasiTotal").val(total);

	discountRupiah = parseFloat($("#tKamarOperasiDiskonRupiah").val());

	grandTotal = total - discountRupiah;
	$("#tKamarOperasiGrandTotal").val(parseFloat(grandTotal));
}

function calculateDiscountKamarOperasi(){
	var discount = 0;
	var beforePrice = 0;
	var discountPrice = 0;

	$('#tKamarOperasiItem tbody tr').each(function() {
		beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
		discountRupiah = $(this).closest('tr').find("td:eq(3) input").val();
		discountPercent = (discountRupiah / beforePrice) * 100;

		$(this).closest('tr').find("td:eq(2) input").val(discountPercent);
		$(this).closest('tr').find("td:eq(4) input").val(beforePrice - discountRupiah);
	});

	calculateTotalSewaKamarOperasi();
}
</script>
