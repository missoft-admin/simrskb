<!-- Modal Kwitasi Implant -->
<div class="modal in" id="CetakKwitansiImplantModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout" style="width: 80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Cetak Kwitansi Implant</h3>
                </div>
                <div class="block-content">
                    <div class="form-horizontal">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Sudah Terima Dari</label>
                            <div class="col-md-10">
                                <input type="text" id="kimplant_terima_dari" class="form-control" placeholder="Sudah Terima Dari" value="{implant_last_sudah_terima_dari}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Deskripsi</label>
                            <div class="col-md-10">
                                <textarea class="form-control" id="kimplant_deskripsi" placeholder="Deskripsi" rows="10">{implant_last_deskripsi}</textarea>
                            </div>
                        </div>
                    </div>

                    <br><br>

                    <h5><i class="fa fa-history"></i> History</h5>
                    <hr style="border-top-width: 2px;margin-top: 8px;">
                    <table class="table table-bordered table-striped" width="100%">
                        <tbody>
                            <?php foreach ($historyProsesKwitansiImplant as $row) { ?>
                            <tr>
                                <td>
                                    <?=$row->tanggal?> - User : <?=$row->namauser?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cetakKwitansiImplant" class="btn btn-sm btn-success" type="button" style="width: 20%;"><i class="fa fa-check"></i> Cetak</button>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>