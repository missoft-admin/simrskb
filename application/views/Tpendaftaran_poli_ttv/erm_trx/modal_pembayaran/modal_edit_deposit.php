<!-- Modal Rincian Deposit -->
<div class="modal in" id="RincianDepositModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title titleRincian">RINCIAN DEPOSIT</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
				  <div class="form-group" style="margin-bottom: 8px;">
				    <label class="col-md-4 control-label">Tanggal</label>
				    <div class="col-md-8" style="margin-bottom: 5px;">
				      <input type="text" id="depositTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
				    </div>
				  </div>
				  <div class="form-group" style="margin-bottom: 8px;">
				    <label class="col-md-4 control-label">Metode Pembayaran</label>
				    <div class="col-md-8" style="margin-bottom: 5px;">
				      <select class="js-select2 form-control" id="depositIdMetodePembayaran" style="width: 100%;" data-placeholder="Pilih Opsi">
				        <option value=""></option>
				        <option value="1">Tunai</option>
				        <option value="2">Debit</option>
				        <option value="3">Kredit</option>
				        <option value="4">Transfer</option>
				      </select>
				    </div>
				  </div>
				  <div class="form-group" style="display: none;margin-bottom:8px;" id="depositMetodeBank">
				    <label class="col-md-4 control-label">Pembayaran</label>
				    <div class="col-md-8" style="margin-bottom: 5px;">
				      <select class="js-select2 form-control" id="depositIdBank" style="width: 100%;" data-placeholder="Pilih Opsi">
				        <option value="">Pilih Opsi</option>
				      </select>
				    </div>
				  </div>
				  <div class="form-group" style="margin-bottom:8px">
				    <label class="col-md-4 control-label" for="">Nominal</label>
				    <div class="col-md-8" style="margin-bottom: 5px;">
			        <input type="text" class="form-control number" id="depositNominal" placeholder="Nominal" value="">
				    </div>
				  </div>
				  <div class="form-group" style="margin-bottom:8px">
				    <label class="col-md-4 control-label" for="">Terima Dari</label>
				    <div class="col-md-8" style="margin-bottom: 5px;">
			        <input type="text" class="form-control" id="depositTerimaDari" placeholder="Terima Dari" value="">
				    </div>
				  </div>
				  <div class="form-group" style="margin-bottom:8px">
				    <label class="col-md-4 control-label" for="">&nbsp;</label>
				    <div class="col-md-8" style="margin-bottom: 5px;">
			        <button id="saveDataDeposit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus-circle"></i> Tambah</button>
				    </div>
				  </div>
			    <div class="col-md-12">
		        <hr style="margin-top:10px; margin-bottom:5px;">
			    </div>

					<a href="" target="_blank" id="historyPrintAll" class="btn btn-sm btn-success" style="width: 20%;margin-top: 10px;margin-bottom: 10px;float:right"><i class="fa fa-print"></i> Print All</a>
					<button class="btn btn-sm btn-success" type="button" style="width: 20%;margin-top: 10px;margin-bottom: 10px;margin-right: 5px;float:right" disabled><i class="fa fa-money"></i> Refund</button>
					<table id="historyDeposit" class="table table-bordered table-striped" style="margin-top: 10px;">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Metode</th>
								<th>Nominal</th>
								<th>Terima Dari</th>
								<th>User Input</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr style="background-color:white;">
								<td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
								<td id="depositTotal" style="font-weight:bold;"></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Konfirmasi Hapus Deposit -->
<div class="modal fade in black-overlay" id="AlasanHapusDepositModal" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed remove-margin-b">
				<div class="block-header bg-danger">
					<h3 class="block-title">Alasan Hapus</h3>
				</div>
			</div>
			<div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
				<textarea class="form-control" id="alasanhapus" placeholder="Alasan Hapus" rows="10" required></textarea>
			</div>
			<input type="hidden" id="tempIdRow">
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" id="removeDeposit" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('click', '#openModalRincian', function() {
			var idpendaftaran = '{idpendaftaran}';
			$("#historyPrintAll").attr('href', '{site_url}trawatinap_tindakan/print_rincian_deposit/' + idpendaftaran);

			getHistoryDeposit(idpendaftaran);
		});

		$(document).on('change', '#depositIdMetodePembayaran', function() {
			var idmetodepembayaran = $(this).val();

			if (idmetodepembayaran == 1) {
				$('#depositMetodeBank').hide();
			} else {

				// Get Bank
				$.ajax({
					url: '{site_url}trawatinap_tindakan/getBank',
					dataType: 'json',
					success: function(data) {
						$("#depositIdBank").empty();
						$("#depositIdBank").append("<option value=''>Pilih Opsi</option>");
						for(var i = 0;i < data.length;i++){
							$("#depositIdBank").append("<option value='"+data[i].id+"'>"+data[i].nama+"</option>");
						}
					}
				});

				$('#depositMetodeBank').show();
			}
		});

		$(document).on('click', '#saveDataDeposit', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var tanggal = $('#depositTanggal').val();
			var idmetodepembayaran = $('#depositIdMetodePembayaran option:selected').val();
			var idbank = $('#depositIdBank option:selected').val();
			var nominal = $('#depositNominal').val();
			var terimadari = $('#depositTerimaDari').val();
			var idtipe = 1;

			if(tanggal == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#depositTanggal').focus();
				});
				return false;
			}

			if(idmetodepembayaran == ''){
				sweetAlert("Maaf...", "Metode Pembayaran belum dipilih !", "error").then((value) => {
					$('#depositIdMetodePembayaran').select2('open');
				});
				return false;
			}

			if(idmetodepembayaran != 1){
				if(idbank == ''){
					sweetAlert("Maaf...", "Bank belum dipilih !", "error").then((value) => {
						$('#depositIdBank').select2('open');
					});
					return false;
				}
			}

			if(nominal == '' || nominal <= 0){
				sweetAlert("Maaf...", "Nominal tidak boleh kosong !", "error").then((value) => {
					$('#depositNominal').focus();
				});
				return false;
			}

			$.ajax({
				url: '{site_url}trawatinap_tindakan/saveDataDeposit',
				method: 'POST',
				data: {
					idrawatinap: idpendaftaran,
					tanggal: tanggal,
					idmetodepembayaran: idmetodepembayaran,
					idbank: idbank,
					nominal: nominal,
					idtipe: idtipe,
					terimadari: terimadari
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Deposit Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					getHistoryDeposit(idpendaftaran);

					$('#depositIdMetodePembayaran').select2("trigger", "select", {data : {id: '',text: ''}});
					$('#depositMetodeBank').hide();
					$('#depositIdBank').select2("trigger", "select", {data : {id: '',text: ''}});
					$('#depositNominal').val('');
					$('#depositTerimaDari').val('');
				}
			});
		});

		$(document).on('click', '.openModalAlasanHapusDeposit', function() {
			$('#tempIdRow').val($(this).data('idrow'));
		});

		$(document).on('click', '#removeDeposit', function() {
			var idrow = $('#tempIdRow').val();
			var idrawatinap = $('#tempIdRawatInap').val();
			var alasanhapus = $('#alasanhapus').val();

			event.preventDefault();

			// Update Status
			$.ajax({
				url: '{site_url}trawatinap_tindakan/removeDataDeposit',
				method: 'POST',
				data: {
					idrow: idrow,
					alasanhapus: alasanhapus,
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Deposit Telah Dihapus.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					$("#alasanhapus").val('');
					getHistoryDeposit(idrawatinap);
				}
			});
		});
	});

	function getHistoryDeposit(idpendaftaran) {
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getHistoryDeposit/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				$('#historyDeposit tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					var metode = '';
					if(data[i].idmetodepembayaran == 1){
						metode = data[i].metodepembayaran;
					}else{
						metode = data[i].metodepembayaran + ' ( ' + data[i].namabank+ ' )';
					}
					var action = '';
					action = '<div class="btn-group">';
					action += '<a href="{site_url}trawatinap_tindakan/print_kwitansi_deposit/' + data[i].id + '" target="_blank" class="btn btn-sm btn-success">Print <i class="fa fa-print"></i></a>';
					action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger openModalAlasanHapusDeposit" data-toggle="modal" data-target="#AlasanHapusDepositModal">Hapus <i class="fa fa-close"></i></button>';
					action += '</div>';

					$('#historyDeposit tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + metode + '</td><td>' + $.number(data[i].nominal) + '</td><td>' + (data[i].terimadari ? data[i].terimadari : "-")  + '</td><td>' + data[i].namapetugas + '</td><td>' + action + '</td></tr>');
				}

				getTotalHistoryDeposit();
			}
		});
	}

	function getTotalHistoryDeposit()
	{
		var totalDeposit = 0;
		$('#historyDeposit tbody tr').each(function() {
			totalDeposit +=parseFloat($(this).find('td:eq(2)').text().replace(/,/g, ''));
		});

		$("#depositTotal").html($.number(totalDeposit));

		// Update Rincian Verifikasi
		$("#totalDeposit").html($.number(totalDeposit));

		var totalkeseluruhan = $("#grandTotal").html().replace(/,/g, '');
		$("#grandTotalPembayaran").html($.number(parseFloat(totalkeseluruhan)-parseFloat(totalDeposit)));
	}
</script>
