<div class="modal in" id="ActionVisiteModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md" style="width:80%">
		<div class="modal-content">
			<div class="block-content">
				<table class="table table-bordered table-striped" style="margin-top: 10px;">
					<thead>
						<tr>
							<th style="display:none">Detail ID</th>
							<th>Tanggal</th>
							<th>Dokter</th>
							<th>Ruangan</th>
							<th>Tarif</th>
							<th>Aksi</th>
						</tr>
						<tr>
							<th style="display:none">
								<input type="text" id="fVisiteRowId" readonly value="">
								<input type="text" id="fVisiteIdTarif" readonly value="">
							</th>
							<th style="width:10%">
								<input type="text" id="fVisiteTanggal" class="js-datepicker form-control"
									data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?= date("d/m/Y")?>">
							</th>
							<th style="width:10%">
								<select id="fVisiteIdDokter" class="js-select2 form-control" style="width: 100%;"
									data-placeholder="Pilih Dokter">
									<option value="">Pilih Dokter</option>
								</select>
							</th>
							<th style="width:10%">
								<input type="hidden" class="form-control" id="fVisiteIdRuangan" placeholder="ID Ruangan"
									value="{idruangan}" readonly="true">
								<input type="hidden" class="form-control" id="fVisiteIdKelas" placeholder="ID Kelas"
									value="{idkelas}" readonly="true">
								<input type="text" class="form-control" id="fVisiteRuangan" placeholder="Ruangan"
									value="{namaruangan}" readonly="true">
							</th>
							<th style="width:10%">
								<input type="hidden" class="form-control" id="fVisiteJasaSarana" placeholder="ID Ruangan" value="" readonly="true">
								<input type="hidden" class="form-control" id="fVisiteJasaPelayanan" placeholder="ID Ruangan" value="" readonly="true">
								<input type="hidden" class="form-control" id="fVisiteBHP" placeholder="ID Ruangan" value="" readonly="true">
								<input type="hidden" class="form-control" id="fVisiteJasaBiayaPerawatan" placeholder="ID Ruangan" value="" readonly="true">
								<input type="text" class="form-control number" id="fVisiteTarif" placeholder="Tarif"
									value="" readonly="true">
							</th>
							<th id="actionVisiteDokter" style="width:10%">
								<button id="saveActionVisiteDokter" class="btn btn-primary"><i
										class="fa fa-plus-circle"></i> Tambah</button>
							</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(document).on('change', '#fVisiteIdDokter', function () {
			var idkategori = $(this).find(':selected').data('idkategori');
			var idkelompokpasien = '{idkelompokpasien}';
			var idruangan = $("#fVisiteIdRuangan").val();
			var idkelas = $("#fVisiteIdKelas").val();
			getTarifVisiteDokter(idkategori, idkelompokpasien, idruangan, idkelas);
		});

		$(document).on('click', '#saveActionVisiteDokter', function () {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var idRow = $('#fVisiteRowId').val();
			var tanggalvisite = $('#fVisiteTanggal').val();
			var iddokter = $('#fVisiteIdDokter option:selected').val();
			var idruangan = $('#fVisiteIdRuangan').val();
			var namaruangan = $('#fVisiteRuangan').val();
			var idpelayanan = $('#fVisiteIdTarif').val();
			var jasasarana = $('#fVisiteJasaSarana').val();
			var jasapelayanan = $('#fVisiteJasaPelayanan').val();
			var bhp = $('#fVisiteBHP').val();
			var biayaperawatan = $('#fVisiteJasaBiayaPerawatan').val();
			var tarif = $('#fVisiteTarif').val();

			if (!isDuplicateVisiteDokter() && validationSubmitVisiteDokter()) {
				submitVisiteDokter({
					idpendaftaran: idpendaftaran,
					idrow: idRow,
					tanggalvisite: tanggalvisite,
					idruangan: idruangan,
					iddokter: iddokter,
					idpelayanan: idpelayanan,
					jasasarana: jasasarana,
					jasapelayanan: jasapelayanan,
					bhp: bhp,
					biayaperawatan: biayaperawatan,
					tarif: tarif,
				});
			}
		});
	});

	function getTarifVisiteDokter(idkategori, idkelompokpasien, idruangan, idkelas) {
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getTarifVisiteDokter/' + idkategori + '/' + idkelompokpasien + '/' + idruangan + '/' + idkelas,
			method: 'GET',
			success: function (data) {
				$("#fVisiteIdTarif").val(data.idtarif);
				$("#fVisiteJasaSarana").val(data.jasasarana);
				$("#fVisiteJasaPelayanan").val(data.jasapelayanan);
				$("#fVisiteBHP").val(data.bhp);
				$("#fVisiteBiayaPerawatan").val(data.biayaperawatan);
				$("#fVisiteTarif").val(data.total);
			}
		});
	}

	function isDuplicateVisiteDokter() {
		var tanggal = $('#fVisiteTanggal').val();
		var iddokter = $('#fVisiteIdDokter option:selected').val();
		var idruangan = $('#fVisiteIdRuangan').val();
		var namaruangan = $('#fVisiteRuangan').val();
		
		var status = false;
		$('#historyVisiteDokter tbody tr').filter(function () {
			var $cells = $(this).children('td');
			if ($cells.eq(1).text() === tanggal && $cells.eq(3).text() === iddokter && $cells.eq(5).text() === idruangan) {
				sweetAlert("Maaf...", $("#fVisiteIdDokter option:selected").text() + " sudah melakukan visite pada tanggal " + tanggal + " pada ruangan " + namaruangan + " !", "error");
				status = true;
			}
		});

		return status;
	}

	function validationSubmitVisiteDokter() {
		var tanggal = $('#fVisiteTanggal').val();
		var iddokter = $('#fVisiteIdDokter option:selected').val();
		var idruangan = $('#fVisiteIdRuangan').val();
		var tarif = $('#fVisiteTarif').val();
		
		if (tanggal == '') {
			sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
				$('#fAlkesTanggal').focus();
			});
			return false;
		}

		if (iddokter == '') {
			sweetAlert("Maaf...", "Dokter belum dipilih !", "error").then((value) => {
				$('#fVisiteIdDokter').select2('open');
			});
			return false;
		}

		if (idruangan == '') {
			sweetAlert("Maaf...", "Ruangan tidak boleh kosong !", "error").then((value) => {
				$('#fVisiteRuangan').focus();
			});
			return false;
		}

		if (tarif == '' || tarif <= 0) {
			sweetAlert("Maaf...", "Tarif tidak boleh kosong !", "error").then((value) => {
				$('#fVisiteTarif').focus();
			});
			return false;
		}

		return true;
	}

	function submitVisiteDokter(payload) {
		$.ajax({
			url: '{site_url}trawatinap_tindakan/saveDataVisiteDokter',
			method: 'POST',
			data: payload,
			success: function (data) {
				swal({
					title: "Berhasil!",
					text: "Data Visite Dokter Telah Tersimpan.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				location.reload();
			}
		});
	}
</script>