<!-- Modal Konfirmasi Pembatalan Transaksi -->
<div class="modal in" id="ActionBatalTransaksiModal" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-xs modal-dialog-popout">
		<div class="modal-content">

			<input type="hidden" name="nomedrecdet" id="nomedrecdet" value="">
			<div class="block block-themed remove-margin-b">
				<div class="block-header bg-danger">
					<h3 class="block-title">Konfirmasi Pembatalan Transaksi</h3>
				</div>
			</div>
			<div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
				<label for="material-text">Anda yakin untuk membatalkan transaksi? Jika anda sudah yakin dengan pembatalan silahkan isi
					alasannya.</label>
			</div>
			<div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
				<label for="material-text">Plilih Alasan</label>
				<select id="idalasan_batal" class="form-control" style="width: 100%;" data-placeholder="Choose one.."
					required>
					<option value="#" selected>- Alasan -</option>
				</select>
			</div>
			<div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
				<label for="material-text">Alasan Detail</label>
				<input class="form-control" id="alasan_batal_transaksi" type="text" value="">
			</div>
<br>
<br>
<br>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-success" id="saveBatalTransaksi" type="submit" data-dismiss="modal"><i
						class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>