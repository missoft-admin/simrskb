<div class="modal fade in black-overlay" id="ActionEditAdministrasiRanapModal" role="dialog" aria-hidden="true"
	style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i
									class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI RAWAT INAP</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">

							<input type="hidden" id="tAdministrasiRanapRowId" readonly value="">
							<input type="hidden" id="tAdministrasiRanapTable" readonly value="">

							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAdministrasiRanapNoRegistrasi">No.
										Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm"
											id="tAdministrasiRanapNoRegistrasi" placeholder="No. Registrasi" readonly
											value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAdministrasiRanapNamaTarif">Nama
										Tarif</label>
									<div class="col-md-9">
										<input type="hidden" id="tAdministrasiRanapIdTarif">
										<input type="text" class="form-control input-sm"
											id="tAdministrasiRanapNamaTarif" placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAdministrasiRanapTanggal">Tanggal
										Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tAdministrasiRanapTanggal"
											placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tAdministrasiRanapItem" class="table table-striped table-bordered"
						style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Tarif</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input <?=(UserAccesForm($user_acces_form,array('2773'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiRanapHarga"
										id="tAdministrasiRanapTarif" placeholder="Tarif"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2774'))?'':'disabled')?> type="text"
										class="form-control input-sm discount tdAdministrasiRanapDiskonPersen"
										id="tAdministrasiRanapDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2774'))?'':'disabled')?> type="text"
										class="form-control input-sm number tdAdministrasiRanapDiskonRupiah"
										id="tAdministrasiRanapDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tAdministrasiRanapTotal"
										placeholder="Total (Rp)" readonly value="0"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataAdministrasiRanap">
				<button class="btn btn-sm btn-primary" id="saveDataAdministrasiRanap" type="button"
					data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataAdministrasiRanap" type="button"
					data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$("#tAdministrasiRanapDiskonPersen").keyup(function () {
			if ($("#tAdministrasiRanapDiskonPersen").val() == '') {
				$("#tAdministrasiRanapDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100) {
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tAdministrasiRanapTotal").val() * parseFloat($(this).val()) / 100);
			$("#tAdministrasiRanapDiskonRupiah").val(discount_rupiah);
		});

		$("#tAdministrasiRanapDiskonRupiah").keyup(function () {
			if ($("#tAdministrasiRanapDiskonRupiah").val() == '') {
				$("#tAdministrasiRanapDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tAdministrasiRanapTotal").val()) {
				$(this).val($("#tAdministrasiRanapTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val() * 100)) / $("#tAdministrasiRanapTotal").val());
			$("#tAdministrasiRanapDiskonPersen").val(discount_percent);
		});

		$(document).on('click', '#saveDataAdministrasiRanap', function () {
			var idrow = $('#tAdministrasiRanapRowId').val();
			var table = $('#tAdministrasiRanapTable').val();
			var tanggal = $('#tAdministrasiRanapTanggal').val();
			var idtarif = $('#tAdministrasiRanapIdTarif').val();
			var tarif = $('#tAdministrasiRanapTarif').val();
			var diskon = $('#tAdministrasiRanapDiskonRupiah').val();
			var totalkeseluruhan = $('#tAdministrasiRanapTotal').val();

			if (tanggal == '') {
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#tAdministrasiRanapTanggal').focus();
				});
				return false;
			}

			if (tarif == '' || tarif <= 0) {
				sweetAlert("Maaf...", "Tarif tidak boleh kosong !", "error").then((value) => {
					$('#tAdministrasiRanapTarif').focus();
				});
				return false;
			}

			if (totalkeseluruhan == '' || totalkeseluruhan <= 0) {
				sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
					$('#tAdministrasiRanapKuantitas').focus();
				});
				return false;
			}

			$.ajax({
				url: '{site_url}trawatinap_tindakan/updateAdministrasi',
				method: 'POST',
				data: {
					table: table,
					idrow: idrow,
					tanggal: tanggal,
					idadministrasi: idtarif,
					tarif: tarif,
					diskon: diskon,
					totalkeseluruhan: totalkeseluruhan
				},
				success: function (data) {
					swal({
						title: "Berhasil!",
						text: "Data Tindakan Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataAdministrasiRanap', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();
			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();
			var total = $(this).closest('tr').find("td:eq(4)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(5)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(6)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(7)").html();

			$("#tAdministrasiRanapRowId").val(idrow);
			$("#tAdministrasiRanapTable").val(table);

			$("#tAdministrasiRanapNoRegistrasi").val(nopendaftaran);
			$("#tAdministrasiRanapTanggal").val(tanggal);

			$('#tAdministrasiRanapIdTarif').val(idtarif);
			$('#tAdministrasiRanapNamaTarif').val(namatarif);

			$("#tAdministrasiRanapTarif").val(total);

			$("#tAdministrasiRanapDiskonRupiah").val(total_disc_rp);
			$("#tAdministrasiRanapDiskonPersen").val(total_disc_percent);
			$("#tAdministrasiRanapTotal").val(grandtotal);

			calculateDiscountAdministrasiRanap();
		});

		$(document).on('click', '#cancelDataAdministrasiRanap', function () {
			("#tAdministrasiRanapRowId").val('');
			$("#tAdministrasiRanapTable").val('');

			$("#tAdministrasiRanapNoRegistrasi").val('');
			$("#tAdministrasiRanapTanggal").val('');

			$('#tAdministrasiRanapIdTarif').val('');
			$('#tAdministrasiRanapNamaTarif').val('');

			$("#tAdministrasiRanapTarif").val('');
			$("#tAdministrasiRanapDiskonRupiah").val('');
			$("#tAdministrasiRanapDiskonPersen").val('');
			$("#tAdministrasiRanapTotal").val('');
		});

		$(document).on("keyup", ".tdAdministrasiRanapHarga", function () {
			if ($(this).val() == '') {
				$(this).val(0);
			}

			var beforePrice = $(this).val();
			var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(2) input").val()) *
				100) / parseFloat(beforePrice);
			$(this).closest('tr').find("td:eq(1) input").val(discount);
			$(this).closest('tr').find("td:eq(3) input").val(beforePrice - parseFloat($(this).closest('tr')
				.find("td:eq(2) input").val()));

			calculateTotalAdministrasiRanap();
			return false;
		});

		$(document).on("keyup", ".tdAdministrasiRanapDiskonPersen", function () {
			if ($(this).val() == '') {
				$(this).val(0);
			}
			if (parseFloat($(this).val()) > 100) {
				$(this).val(100);
			}

			var afterPrice = 0;
			var beforePrice = $(this).closest('tr').find("td:eq(0) input").val();
			afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val() / 100)));

			$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
			$(this).closest('tr').find("td:eq(3) input").val(beforePrice - afterPrice);

			calculateTotalAdministrasiRanap();
			return false;
		});

		$(document).on("keyup", ".tdAdministrasiRanapDiskonRupiah", function () { //Diskon RP Keyup
			var beforePrice = $(this).closest('tr').find("td:eq(0) input").val();
			if ($(this).val() == '') {
				$(this).val(0);
			}
			if (parseFloat($(this).val()) > beforePrice) {
				$(this).val(beforePrice);
			}

			var afterPrice = 0;
			afterPrice = parseFloat($(this).val() * 100) / parseFloat(beforePrice);

			$(this).closest('tr').find("td:eq(1) input").val(afterPrice);
			$(this).closest('tr').find("td:eq(3) input").val(beforePrice - $(this).val());

			calculateTotalAdministrasiRanap();
			return false;
		});
	});

	function calculateTotalAdministrasiRanap() {
		var subTotal, totalTransaction, totalDiscountRupiah;

		tarif = parseFloat($("#tAdministrasiRanapTarif").val());
		$("#tAdministrasiRanapTarif").val(tarif);

		discountRupiah = parseFloat($("#tAdministrasiRanapDiskonRupiah").val());

		grandTotal = tarif - discountRupiah;
		$("#tAdministrasiRanapTotal").val(parseFloat(grandTotal));
	}

	function calculateDiscountAdministrasiRanap() {
		var discount = 0;
		var beforePrice = 0;
		var discountPrice = 0;

		$('#tAdministrasiRanapItem tbody tr').each(function () {
			beforePrice = $(this).closest('tr').find("td:eq(0) input").val();
			discountRupiah = $(this).closest('tr').find("td:eq(2) input").val();
			discountPercent = (discountRupiah / beforePrice) * 100;

			$(this).closest('tr').find("td:eq(1) input").val(discountPercent);
			$(this).closest('tr').find("td:eq(3) input").val(beforePrice - discountRupiah);
		});

		calculateTotalAdministrasiRanap();
	}
</script>