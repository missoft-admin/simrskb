<div class="modal fade in black-overlay" id="ActionEditAdministrasiModal" role="dialog" aria-hidden="true"
	style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i
									class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI RAWAT JALAN</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">

							<input type="hidden" id="tAdministrasiRowId" readonly value="">
							<input type="hidden" id="tAdministrasiTable" readonly value="">

							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAdministrasiNoRegistrasi">No.
										Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tAdministrasiNoRegistrasi"
											placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAdministrasiNamaTarif">Nama
										Tarif</label>
									<div class="col-md-9">
										<input type="hidden" id="tAdministrasiIdTarif">
										<input type="text" class="form-control input-sm" id="tAdministrasiNamaTarif"
											placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAdministrasiTanggal">Tanggal
										Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tAdministrasiTanggal"
											placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tAdministrasiItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Item Biaya</th>
								<th style="width: 25%;">Harga</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jasa Sarana</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2766'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiHarga"
										id="tAdministrasiJasaSarana" placeholder="Jasa Sarana"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm discount tdAdministrasiDiskonPersen"
										id="tAdministrasiJasaSaranaDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiDiskonRupiah"
										id="tAdministrasiJasaSaranaDiskonRupiah" placeholder="Diskon (Rp)" value="0">
								</td>
								<td><input type="text" class="form-control input-sm number"
										id="tAdministrasiJasaSaranaTotal" placeholder="Total Rp" readonly value="0">
								</td>
							</tr>
							<tr>
								<td>Jasa Pelayanan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2766'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiHarga"
										id="tAdministrasiJasaPelayanan" placeholder="Jasa Pelayanan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm discount tdAdministrasiDiskonPersen"
										id="tAdministrasiJasaPelayananDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiDiskonRupiah"
										id="tAdministrasiJasaPelayananDiskonRupiah" placeholder="Diskon (Rp)" value="0">
								</td>
								<td><input type="text" class="form-control input-sm number"
										id="tAdministrasiJasaPelayananTotal" placeholder="Total Rp" readonly value="0">
								</td>
							</tr>
							<tr>
								<td>BHP</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2766'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiHarga"
										id="tAdministrasiBHP" placeholder="BHP"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm discount tdAdministrasiDiskonPersen"
										id="tAdministrasiBHPDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiDiskonRupiah"
										id="tAdministrasiBHPDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tAdministrasiBHPTotal"
										placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Biaya Perawatan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2766'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiHarga"
										id="tAdministrasiBiayaPerawatan" placeholder="Biaya Perawatan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm discount tdAdministrasiDiskonPersen"
										id="tAdministrasiBiayaPerawatanDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2767'))?'':'disabled')?> type="text" class="form-control input-sm number tdAdministrasiDiskonRupiah"
										id="tAdministrasiBiayaPerawatanDiskonRupiah" placeholder="Diskon (Rp)"
										value="0"></td>
								<td><input type="text" class="form-control input-sm number"
										id="tAdministrasiBiayaPerawatanTotal" placeholder="Total Rp" readonly value="0">
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right"><b>Sub Total (Rp)</b></label>
								</th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tAdministrasiSubTotal"
										placeholder="Sub Total (Rp)" readonly value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right"><b>Kuantitas</b></label>
								</th>
								<th colspan="2">
									<input <?=(UserAccesForm($user_acces_form,array('2768'))?'':'disabled')?> type="text" class="form-control input-sm number" id="tAdministrasiKuantitas"
										placeholder="Kuantitas" value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3"><label class="pull-right"><b>Total (Rp)</b></label></th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tAdministrasiTotal"
										placeholder="Total (Rp)" readonly value="0">
								</th>
							</tr>
							<tr>
								<th style="width: 85%;" colspan="3">
									<label class="pull-right">
										<b>Discount Semua</b>
									</label>
								</th>
								<th style="width: 15%;" colspan="2">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input <?=(UserAccesForm($user_acces_form,array('2769'))?'':'disabled')?> class="form-control number input-sm" type="text"
											id="tAdministrasiDiskonRupiah" placeholder="Discount (Rp)" value="0">
									</div>
									<div class="input-group">
										<span class="input-group-addon"> %. </span>
										<input <?=(UserAccesForm($user_acces_form,array('2769'))?'':'disabled')?> class="form-control discount input-sm" type="text"
											id="tAdministrasiDiskonPersen" placeholder="Discount (%)" value="0">
									</div>
								</th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right">
										<b>Grand Total (Rp)</b>
									</label>
								</th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tAdministrasiGrandTotal"
										placeholder="Grand Total (Rp)" readonly value="0">
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataAdministrasi">
				<button class="btn btn-sm btn-primary" id="saveDataAdministrasi" type="button" data-dismiss="modal"><i
						class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataAdministrasi" type="button"
					data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$("#tAdministrasiKuantitas").keyup(function () {
			var hargajual = $('#tAdministrasiSubTotal').val().replace(/,/g, '');
			var kuantitas = $(this).val().replace(/,/g, '');
			$('#tAdministrasiTotal').val(parseInt(hargajual) * parseInt(kuantitas));

			calculateDiscountAdministrasi();
		});

		$("#tAdministrasiDiskonPersen").keyup(function () {
			if ($("#tAdministrasiDiskonPersen").val() == '') {
				$("#tAdministrasiDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100) {
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tAdministrasiTotal").val() * parseFloat($(this).val()) / 100);
			$("#tAdministrasiDiskonRupiah").val(discount_rupiah);

			calculateDiscountAdministrasi();
		});

		$("#tAdministrasiDiskonRupiah").keyup(function () {
			if ($("#tAdministrasiDiskonRupiah").val() == '') {
				$("#tAdministrasiDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tAdministrasiTotal").val()) {
				$(this).val($("#tAdministrasiTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val() * 100)) / $("#tAdministrasiTotal").val());
			$("#tAdministrasiDiskonPersen").val(discount_percent);

			calculateDiscountAdministrasi();
		});

		$(document).on('click', '#saveDataAdministrasi', function () {
			var idrow = $('#tAdministrasiRowId').val();
			var table = $('#tAdministrasiTable').val();
			var tanggal = $('#tAdministrasiTanggal').val();
			var idtarif = $('#tAdministrasiIdTarif').val();
			var jasasarana = $('#tAdministrasiJasaSarana').val();
			var jasasarana_disc = $('#tAdministrasiJasaSaranaDiskonRupiah').val();
			var jasapelayanan = $('#tAdministrasiJasaPelayanan').val();
			var jasapelayanan_disc = $('#tAdministrasiJasaPelayananDiskonRupiah').val();
			var bhp = $('#tAdministrasiBHP').val();
			var bhp_disc = $('#tAdministrasiBHPDiskonRupiah').val();
			var biayaperawatan = $('#tAdministrasiBiayaPerawatan').val();
			var biayaperawatan_disc = $('#tAdministrasiBiayaPerawatanDiskonRupiah').val();
			var subtotal = $('#tAdministrasiSubTotal').val();
			var kuantitas = $('#tAdministrasiKuantitas').val();
			var totalkeseluruhan = $('#tAdministrasiTotal').val();
			var diskon = $('#tAdministrasiDiskonRupiah').val();

			if (tanggal == '') {
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#tAdministrasiTanggal').focus();
				});
				return false;
			}

			if (subtotal == '' || subtotal <= 0) {
				sweetAlert("Maaf...", "Sub Total tidak boleh kosong !", "error").then((value) => {
					$('#tAdministrasiTarif').focus();
				});
				return false;
			}

			if (kuantitas == '' || kuantitas <= 0) {
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#tAdministrasiKuantitas').focus();
				});
				return false;
			}

			if (totalkeseluruhan == '' || totalkeseluruhan <= 0) {
				sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
					$('#tAdministrasiTotal').focus();
				});
				return false;
			}

			$.ajax({
				url: '{site_url}trawatinap_tindakan/updateAdministrasi',
				method: 'POST',
				data: {
					table: table,
					idrow: idrow,
					tanggal: tanggal,
					idadministrasi: idtarif,
					jasasarana: jasasarana,
					jasasarana_disc: jasasarana_disc,
					jasapelayanan: jasapelayanan,
					jasapelayanan_disc: jasapelayanan_disc,
					bhp: bhp,
					bhp_disc: bhp_disc,
					biayaperawatan: biayaperawatan,
					biayaperawatan_disc: biayaperawatan_disc,
					subtotal: subtotal,
					kuantitas: kuantitas,
					diskon: diskon,
					totalkeseluruhan: (totalkeseluruhan - diskon)
				},
				success: function (data) {
					swal({
						title: "Berhasil!",
						text: "Data Tindakan Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataAdministrasi', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();
			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();
			var jasasarana = $(this).closest('tr').find("td:eq(4)").html();
			var jasasarana_disc = $(this).closest('tr').find("td:eq(5)").html();
			var jasapelayanan = $(this).closest('tr').find("td:eq(6)").html();
			var jasapelayanan_disc = $(this).closest('tr').find("td:eq(7)").html();
			var bhp = $(this).closest('tr').find("td:eq(8)").html();
			var bhp_disc = $(this).closest('tr').find("td:eq(9)").html();
			var biayaperawatan = $(this).closest('tr').find("td:eq(10)").html();
			var biayaperawatan_disc = $(this).closest('tr').find("td:eq(11)").html();
			var subtotal = $(this).closest('tr').find("td:eq(12)").html();
			var kuantitas = $(this).closest('tr').find("td:eq(13)").html();
			var total = $(this).closest('tr').find("td:eq(14)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(15)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(16)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(17)").html();

			$("#tAdministrasiRowId").val(idrow);
			$("#tAdministrasiTable").val(table);

			$("#tAdministrasiNoRegistrasi").val(nopendaftaran);
			$("#tAdministrasiTanggal").val(tanggal);

			$('#tAdministrasiIdTarif').val(idtarif);
			$('#tAdministrasiNamaTarif').val(namatarif);

			$("#tAdministrasiJasaSarana").val(jasasarana);
			$("#tAdministrasiJasaSaranaDiskonRupiah").val(jasasarana_disc);

			$("#tAdministrasiJasaPelayanan").val(jasapelayanan);
			$("#tAdministrasiJasaPelayananDiskonRupiah").val(jasapelayanan_disc);

			$("#tAdministrasiBHP").val(bhp);
			$("#tAdministrasiBHPDiskonRupiah").val(bhp_disc);

			$("#tAdministrasiBiayaPerawatan").val(biayaperawatan);
			$("#tAdministrasiBiayaPerawatanDiskonRupiah").val(biayaperawatan_disc);

			$("#tAdministrasiSubTotal").val(subtotal);
			$("#tAdministrasiKuantitas").val(kuantitas);
			$("#tAdministrasiTotal").val(total);

			$("#tAdministrasiDiskonRupiah").val(total_disc_rp);
			$("#tAdministrasiDiskonPersen").val(total_disc_percent);
			$("#tAdministrasiGrandTotal").val(grandtotal);

			calculateDiscountAdministrasi();
		});

		$(document).on('click', '#cancelDataAdministrasi', function () {
			$("#tAdministrasiRowId").val('');
			$("#tAdministrasiNoRegistrasi").val('');
			$("#tAdministrasiTanggal").val('');

			$('#tAdministrasiIdTarif').val('');
			$('#tAdministrasiNamaTarif').val('');

			$("#tAdministrasiJasaSarana").val('');
			$("#tAdministrasiJasaSaranaDiskonPersen").val('');

			$("#tAdministrasiJasaPelayanan").val('');
			$("#tAdministrasiJasaPelayananDiskonPersen").val('');

			$("#tAdministrasiBHP").val('');
			$("#tAdministrasiBHPDiskonPersen").val('');

			$("#tAdministrasiBiayaPerawatan").val('');
			$("#tAdministrasiBiayaPerawatanDiskonPersen").val('');

			$("#tAdministrasiSubTotal").val('');
			$("#tAdministrasiKuantitas").val('');
			$("#tAdministrasiTotal").val('');

			$("#tAdministrasiDiskonRupiah").val('');
			$("#tAdministrasiDiskonPersen").val('');
			$("#tAdministrasiGrandTotal").val('');
		});

		$(document).on('click', '.verifAdministrasi', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');

			event.preventDefault();
			swal({
				title: "Apakah anda yakin ingin memverifikasi data ini?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				$.ajax({
					url: '{site_url}trawatinap_tindakan/updateStatusTindakanDetail',
					method: 'POST',
					data: {
						idrow: idrow,
						table: table,
						statusverifikasi: '1'
					},
					success: function (data) {
						swal({
							title: "Berhasil!",
							text: "Administrasi Telah Diverifikasi.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
						location.reload();
					}
				});
			});
		});

		$(document).on('click', '.unverifAdministrasi', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');

			event.preventDefault();
			swal({
				title: "Apakah anda yakin ingin membatalkan verifikasi data ini?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				$.ajax({
					url: '{site_url}trawatinap_tindakan/updateStatusTindakanDetail',
					method: 'POST',
					data: {
						idrow: idrow,
						table: table,
						statusverifikasi: '0'
					},
					success: function (data) {
						swal({
							title: "Berhasil!",
							text: "Administrasi Telah Dibatalkan.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
						location.reload();
					}
				});
			});
		});

		$(document).on("keyup", ".tdAdministrasiHarga", function () {
			if ($(this).val() == '') {
				$(this).val(0);
			}

			var beforePrice = $(this).val();
			var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) *
				100) / parseFloat(beforePrice);
			$(this).closest('tr').find("td:eq(2) input").val(discount);
			$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr')
				.find("td:eq(3) input").val()));

			calculateTotalAdministrasi();
			return false;
		});

		$(document).on("keyup", ".tdAdministrasiDiskonPersen", function () {
			if ($(this).val() == '') {
				$(this).val(0);
			}
			if (parseFloat($(this).val()) > 100) {
				$(this).val(100);
			}

			var afterPrice = 0;
			var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
			afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val() / 100)));

			$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
			$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

			calculateTotalAdministrasi();
			return false;
		});

		$(document).on("keyup", ".tdAdministrasiDiskonRupiah", function () { //Diskon RP Keyup
			var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
			if ($(this).val() == '') {
				$(this).val(0);
			}
			if (parseFloat($(this).val()) > beforePrice) {
				$(this).val(beforePrice);
			}

			var afterPrice = 0;
			afterPrice = parseFloat($(this).val() * 100) / parseFloat(beforePrice);

			$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
			$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

			calculateTotalAdministrasi();
			return false;
		});
	});

	function calculateTotalAdministrasi() {
		var subTotal, totalTransaction, totalDiscountRupiah;

		subTotal = (parseFloat($("#tAdministrasiJasaSaranaTotal").val()) + parseFloat($("#tAdministrasiJasaPelayananTotal")
			.val()) + parseFloat($("#tAdministrasiBHPTotal").val()) + parseFloat($(
			"#tAdministrasiBiayaPerawatanTotal").val()));
		$("#tAdministrasiSubTotal").val(subTotal);

		total = parseFloat(subTotal) * parseFloat($("#tAdministrasiKuantitas").val());
		$("#tAdministrasiTotal").val(total);

		discountRupiah = parseFloat($("#tAdministrasiDiskonRupiah").val());

		grandTotal = total - discountRupiah;
		$("#tAdministrasiGrandTotal").val(parseFloat(grandTotal));
	}

	function calculateDiscountAdministrasi() {
		var discount = 0;
		var beforePrice = 0;
		var discountPrice = 0;

		$('#tAdministrasiItem tbody tr').each(function () {
			beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
			discountRupiah = $(this).closest('tr').find("td:eq(3) input").val();
			discountPercent = (discountRupiah / beforePrice) * 100;

			$(this).closest('tr').find("td:eq(2) input").val(discountPercent);
			$(this).closest('tr').find("td:eq(4) input").val(beforePrice - discountRupiah);
		});

		calculateTotalAdministrasi();
	}
</script>