<div class="modal fade in black-overlay" id="EditTransaksiDokterOperatorModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI DOKTER</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tDokterOperatorRowId" readonly value="">
							<input type="hidden" id="tDokterOperatorTable" readonly value="">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tDokterOperatorNoRegistrasi">No. Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tDokterOperatorNoRegistrasi" placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tDokterOperatorNamaTarif">Nama Tarif</label>
									<div class="col-md-9">
										<input type="hidden" id="tDokterOperatorIdTarif">
										<input type="text" class="form-control input-sm" id="tDokterOperatorNamaTarif" placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tDokterOperatorTanggal">Tanggal Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tDokterOperatorTanggal" placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tDokterOperatorTanggal">Nama Dokter</label>
									<div class="col-md-9">
										<input type="hidden" id="tDokterOperatorIdDokter">
										<input type="text" class="form-control input-sm" id="tDokterOperatorNamaDokter" placeholder="Nama Dokter" readonly value="">
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tDokterOperatorItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Item Biaya</th>
								<th style="width: 25%;">Harga</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jasa Sarana</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2749'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorHarga" id="tDokterOperatorJasaSarana" placeholder="Jasa Sarana"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm discount tdDokterOperatorDiskonPersen" id="tDokterOperatorJasaSaranaDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorDiskonRupiah" id="tDokterOperatorJasaSaranaDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tDokterOperatorJasaSaranaTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Jasa Pelayanan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2749'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorHarga" id="tDokterOperatorJasaPelayanan" placeholder="Jasa Pelayanan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm discount tdDokterOperatorDiskonPersen" id="tDokterOperatorJasaPelayananDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorDiskonRupiah" id="tDokterOperatorJasaPelayananDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tDokterOperatorJasaPelayananTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>BHP</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2749'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorHarga" id="tDokterOperatorBHP" placeholder="BHP"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm discount tdDokterOperatorDiskonPersen" id="tDokterOperatorBHPDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorDiskonRupiah" id="tDokterOperatorBHPDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tDokterOperatorBHPTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Biaya Perawatan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2749'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorHarga" id="tDokterOperatorBiayaPerawatan" placeholder="Biaya Perawatan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm discount tdDokterOperatorDiskonPersen" id="tDokterOperatorBiayaPerawatanDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2750'))?'':'disabled')?> type="text" class="form-control input-sm number tdDokterOperatorDiskonRupiah" id="tDokterOperatorBiayaPerawatanDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tDokterOperatorBiayaPerawatanTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Sub Total (Rp)</b></label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tDokterOperatorSubTotal" placeholder="Sub Total (Rp)" readonly value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Kuantitas</b></label>
								</th>
								<th>
									<input <?=(UserAccesForm($user_acces_form,array('2751'))?'':'disabled')?> type="text" class="form-control input-sm number" id="tDokterOperatorKuantitas" placeholder="Kuantitas" value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4"><label class="pull-right"><b>Total (Rp)</b></label></th>
								<th>
									<input type="text" class="form-control input-sm number" id="tDokterOperatorTotal" placeholder="Total (Rp)" readonly  value="0">
								</th>
							</tr>
							<tr>
								<th style="width: 85%;" colspan="4">
									<label class="pull-right">
										<b>Discount Semua</b>
									</label>
								</th>
								<th style="width: 15%;">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input <?=(UserAccesForm($user_acces_form,array('2752'))?'':'disabled')?> class="form-control number input-sm" type="text" id="tDokterOperatorDiskonRupiah" placeholder="Discount (Rp)" value="0">
										</div>
										<div class="input-group">
											<span class="input-group-addon"> %. </span>
											<input <?=(UserAccesForm($user_acces_form,array('2752'))?'':'disabled')?> class="form-control discount input-sm" type="text" id="tDokterOperatorDiskonPersen" placeholder="Discount (%)" value="0">
										</div>
								</th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right">
										<b>Grand Total (Rp)</b>
									</label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tDokterOperatorGrandTotal" placeholder="Grand Total (Rp)" readonly value="0">
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataDokterOperator">
				<button class="btn btn-sm btn-primary" id="saveDataDokterOperator" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataDokterOperator" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('keyup', '#tDokterOperatorKuantitas', function() {
			var hargajual = $('#tDokterOperatorSubTotal').val().replace(/,/g, '');
			var kuantitas = $(this).val().replace(/,/g, '');
			$('#tDokterOperatorTotal').val(parseInt(hargajual) * parseInt(kuantitas));
		});

		$(document).on('click', '#saveDataDokterOperator', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var idrow = $('#tDokterOperatorRowId').val();
			var table = $('#tDokterOperatorTable').val();
			var tanggal = $('#tDokterOperatorTanggal').val();
			var idpelayanan = $('#tDokterOperatorIdTarif').val();
			var jasasarana = $('#tDokterOperatorJasaSarana').val();
			var jasasarana_disc = $('#tDokterOperatorJasaSaranaDiskonRupiah').val();
			var jasapelayanan = $('#tDokterOperatorJasaPelayanan').val();
			var jasapelayanan_disc = $('#tDokterOperatorJasaPelayananDiskonRupiah').val();
			var bhp = $('#tDokterOperatorBHP').val();
			var bhp_disc = $('#tDokterOperatorBHPDiskonRupiah').val();
			var biayaperawatan = $('#tDokterOperatorBiayaPerawatan').val();
			var biayaperawatan_disc = $('#tDokterOperatorBiayaPerawatanDiskonRupiah').val();
			var subtotal = $('#tDokterOperatorSubTotal').val();
			var kuantitas = $('#tDokterOperatorKuantitas').val();
			var totalkeseluruhan = $('#tDokterOperatorTotal').val();
			var diskon = $('#tDokterOperatorDiskonRupiah').val();

			if(tanggal == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#tDokterOperatorTanggal').focus();
				});
				return false;
			}

			if(idpelayanan == ''){
				sweetAlert("Maaf...", "Tindakan belum dipilih !", "error").then((value) => {
					$("#tDokterOperatorIdTarif").focus();
				});
				return false;
			}

			if(subtotal == '' || subtotal <= 0){
				sweetAlert("Maaf...", "Sub Total tidak boleh kosong !", "error").then((value) => {
					$('#tDokterOperatorTarif').focus();
				});
				return false;
			}

			if(kuantitas == '' || kuantitas <= 0){
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#tDokterOperatorKuantitas').focus();
				});
				return false;
			}

			if(totalkeseluruhan == '' || totalkeseluruhan <= 0){
				sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
					$('#tDokterOperatorKuantitas').focus();
				});
				return false;
			}
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}trawatinap_tindakan/updateSewaKamarOK',
				method: 'POST',
				data: {
					idpendaftaran: idpendaftaran,
					idrow: idrow,
					table: table,
					jasasarana: jasasarana,
					jasasarana_disc: jasasarana_disc,
					jasapelayanan: jasapelayanan,
					jasapelayanan_disc: jasapelayanan_disc,
					bhp: bhp,
					bhp_disc: bhp_disc,
					biayaperawatan: biayaperawatan,
					biayaperawatan_disc: biayaperawatan_disc,
					subtotal: subtotal,
					diskon: diskon,
					totalkeseluruhan: (totalkeseluruhan - diskon)
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data  Kamar Operasi Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataDokterOperator', function() {
		  var idrow = $(this).data('idrow');
		  var table = $(this).data('table');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();

			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();

			var iddokter = $(this).closest('tr').find("td:eq(4)").html();
			var namadokter = $(this).closest('tr').find("td:eq(5)").html();

			var jasasarana = $(this).closest('tr').find("td:eq(6)").html();
			var jasasarana_disc = $(this).closest('tr').find("td:eq(7)").html();

			var jasapelayanan = $(this).closest('tr').find("td:eq(8)").html();
			var jasapelayanan_disc = $(this).closest('tr').find("td:eq(9)").html();

			var bhp = $(this).closest('tr').find("td:eq(10)").html();
			var bhp_disc = $(this).closest('tr').find("td:eq(11)").html();

			var biayaperawatan = $(this).closest('tr').find("td:eq(12)").html();
			var biayaperawatan_disc = $(this).closest('tr').find("td:eq(13)").html();

			var subtotal = $(this).closest('tr').find("td:eq(14)").html();
			var kuantitas = $(this).closest('tr').find("td:eq(15)").html();
			var total = $(this).closest('tr').find("td:eq(16)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(17)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(18)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(19)").html();

			$("#tDokterOperatorRowId").val(idrow);
			$("#tDokterOperatorTable").val(table);
			$("#tDokterOperatorNoRegistrasi").val(nopendaftaran);
			$("#tDokterOperatorTanggal").val(tanggal);

			$('#tDokterOperatorIdTarif').val(idtarif);
			$('#tDokterOperatorNamaTarif').val(namatarif);

			$('#tDokterOperatorIdDokter').val(iddokter);
			$('#tDokterOperatorNamaDokter').val(namadokter);

			$("#tDokterOperatorJasaSarana").val(jasasarana);
			$("#tDokterOperatorJasaSaranaDiskonRupiah").val(jasasarana_disc);

			$("#tDokterOperatorJasaPelayanan").val(jasapelayanan);
			$("#tDokterOperatorJasaPelayananDiskonRupiah").val(jasapelayanan_disc);

			$("#tDokterOperatorBHP").val(bhp);
			$("#tDokterOperatorBHPDiskonRupiah").val(bhp_disc);

			$("#tDokterOperatorBiayaPerawatan").val(biayaperawatan);
			$("#tDokterOperatorBiayaPerawatanDiskonRupiah").val(biayaperawatan_disc);

			$("#tDokterOperatorSubTotal").val(subtotal);
			$("#tDokterOperatorKuantitas").val(kuantitas);
			$("#tDokterOperatorTotal").val(total);

			$("#tDokterOperatorDiskonRupiah").val(total_disc_rp);
			$("#tDokterOperatorDiskonPersen").val(total_disc_percent);
			$("#tDokterOperatorGrandTotal").val(grandtotal);

			calculateDiscountDokterOperator();
		});

		$(document).on('click', '#cancelDataDokterOperator', function() {
			$("#tDokterOperatorRowId").val('');
			$("#tDokterOperatorTable").val('');

			$("#tDokterOperatorNoRegistrasi").val('');
			$("#tDokterOperatorTanggal").val('');

			$('#tDokterOperatorIdTarif').val('');
			$('#tDokterOperatorNamaTarif').val('');

			$("#tDokterOperatorJasaSarana").val('');
			$("#tDokterOperatorJasaSaranaDiskonPersen").val('');

			$("#tDokterOperatorJasaPelayanan").val('');
			$("#tDokterOperatorJasaPelayananDiskonPersen").val('');

			$("#tDokterOperatorBHP").val('');
			$("#tDokterOperatorBHPDiskonPersen").val('');

			$("#tDokterOperatorBiayaPerawatan").val('');
			$("#tDokterOperatorBiayaPerawatanDiskonPersen").val('');

			$("#tDokterOperatorSubTotal").val('');
			$("#tDokterOperatorKuantitas").val('');
			$("#tDokterOperatorTotal").val('');

			$("#tDokterOperatorDiskonRupiah").val('');
			$("#tDokterOperatorDiskonPersen").val('');
			$("#tDokterOperatorGrandTotal").val('');
		});

		$("#tDokterOperatorDiskonPersen").keyup(function(){
			if ($("#tDokterOperatorDiskonPersen").val() == ''){
				$("#tDokterOperatorDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100){
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tDokterOperatorTotal").val() * parseFloat($(this).val())/100);
			$("#tDokterOperatorDiskonRupiah").val(discount_rupiah);

			calculateTotalDokterOperator();
		});

		$("#tDokterOperatorDiskonRupiah").keyup(function(){
			if ($("#tDokterOperatorDiskonRupiah").val()==''){
				$("#tDokterOperatorDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tDokterOperatorTotal").val()){
				$(this).val($("#tDokterOperatorTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val()*100))/$("#tDokterOperatorTotal").val());
			$("#tDokterOperatorDiskonPersen").val(discount_percent);

			calculateTotalDokterOperator();
		});

		$("#tDokterOperatorKuantitas").keyup(function(){
			calculateTotalDokterOperator();
		});

		$(document).on("keyup", ".tdDokterOperatorHarga", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	var beforePrice = $(this).val();
    	var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(beforePrice);
    	$(this).closest('tr').find("td:eq(2) input").val(discount);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));

    	calculateTotalDokterOperator();
    	return false;
    });

    $(document).on("keyup", ".tdDokterOperatorDiskonPersen", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var afterPrice = 0;
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val()/100)));

    	$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

    	calculateTotalDokterOperator();
    	return false;
    });

    $(document).on("keyup", ".tdDokterOperatorDiskonRupiah", function () {//Diskon RP Keyup
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > beforePrice){
    		$(this).val(beforePrice);
    	}

    	var afterPrice = 0;
    	afterPrice = parseFloat($(this).val()*100) / parseFloat(beforePrice);

    	$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

    	calculateTotalDokterOperator();
    	return false;
    });
});

function calculateTotalDokterOperator() {
	var subTotal, totalTransaction, totalDiscountRupiah;

	subTotal = (parseFloat($("#tDokterOperatorJasaSaranaTotal").val()) + parseFloat($("#tDokterOperatorJasaPelayananTotal").val()) + parseFloat($("#tDokterOperatorBHPTotal").val()) + parseFloat($("#tDokterOperatorBiayaPerawatanTotal").val()));
	$("#tDokterOperatorSubTotal").val(subTotal);

	total = parseFloat(subTotal) * parseFloat($("#tDokterOperatorKuantitas").val());
	$("#tDokterOperatorTotal").val(total);

	discountRupiah = parseFloat($("#tDokterOperatorDiskonRupiah").val());

	grandTotal = total - discountRupiah;
	$("#tDokterOperatorGrandTotal").val(parseFloat(grandTotal));
}

function calculateDiscountDokterOperator(){
	var discount = 0;
	var beforePrice = 0;
	var discountPrice = 0;

	$('#tDokterOperatorItem tbody tr').each(function() {
		beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
		discountRupiah = $(this).closest('tr').find("td:eq(3) input").val();
		discountPercent = (discountRupiah / beforePrice) * 100;

		$(this).closest('tr').find("td:eq(2) input").val(discountPercent);
		$(this).closest('tr').find("td:eq(4) input").val(beforePrice - discountRupiah);
	});

	calculateTotalDokterOperator();
}
</script>
