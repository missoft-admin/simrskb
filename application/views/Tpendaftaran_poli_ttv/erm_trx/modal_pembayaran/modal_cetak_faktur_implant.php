<!-- Modal Cetak Faktur Implant -->
<div class="modal in" id="CetakFakturImplantModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout" style="width: 80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Cetak Faktur Implant</h3>
                </div>
                <div class="block-content">
                    <div class="form-horizontal">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">No. Medrec</label>
                            <div class="col-md-10">
                                <input type="text" id="fimplant_nomedrec" class="form-control" disabled value="{nomedrec}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Nama Pasien</label>
                            <div class="col-md-10">
                                <input type="text" id="fimplant_namapasien" class="form-control" disabled value="{namapasien}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Diagnosa</label>
                            <div class="col-md-10">
                                <input type="text" id="fimplant_diagnosa" class="form-control" disabled value="{diagnosa}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Tanggal Operasi</label>
                            <div class="col-md-10">
                                <input type="text" id="fimplant_tanggaloperasi" class="form-control" disabled value="{tanggaloperasi}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Operasi</label>
                            <div class="col-md-10">
                                <input type="text" id="fimplant_operasi" class="form-control" disabled value="{operasi}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Dokter</label>
                            <div class="col-md-10">
                                <input type="text" id="fimplant_dokter" class="form-control" disabled value="{namadokterpenanggungjawab}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Distributor</label>
                            <div class="col-md-10">
                                <select id="fimplant_distributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Distributor" multiple>
									<option value="">Pilih Distributor</option>
                                    <?php foreach (get_all('mdistributor', ['status' => '1']) as $row) { ?>
                                        <option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php } ?>
								</select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cetakFakturImplant" class="btn btn-sm btn-success" type="button" style="width: 20%;"><i class="fa fa-check"></i> Cetak</button>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>