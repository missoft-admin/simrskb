<div class="modal fade in black-overlay" id="EditTransaksiTindakanFarmasiModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
        <div class="modal-content">
            <div class="block block-themed">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">EDIT TRANSAKSI OBAT, ALKES RAWAT INAP</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <form class="form-horizontal">
                            <input type="hidden" id="tFarmasiRowId" readonly value="">
                            <input type="hidden" id="tFarmasiTable" readonly value="">
                            <input type="hidden" id="tFarmasiStatusRacikan" readonly value="">

                            <input type="hidden" id="tFarmasiIdObat" readonly value="">
                            <input type="hidden" id="tFarmasiIdUnitPelayanan" readonly value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="tFarmasiNoRegistrasi">No. Registrasi</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tFarmasiNoRegistrasi" placeholder="No. Registrasi" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="tFarmasiNamaTarif">Nama Obat</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tFarmasiNamaTarif" placeholder="Nama Obat" readonly value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="tFarmasiTanggal">Tanggal Transaksi</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tFarmasiTanggal" placeholder="Tanggal Transaksi" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="tFarmasiIdDokter">Unit Pelayanan</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tFarmasiUnitPelayanan" placeholder="Unit Pelayanan" readonly value="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <table id="tFarmasiItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th style="width: 20%;">Harga Dasar</th>
                                <th style="width: 10%;">Margin (%)</th>
                                <th style="width: 20%;">Harga Jual</th>
                                <th style="width: 10%;">Diskon (%)</th>
                                <th style="width: 10%;">Diskon (Rp)</th>
                                <th style="width: 20%;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" <?=(UserAccesForm($user_acces_form,array('2664'))?'':'disabled')?> class="form-control input-sm number tdFarmasiHargaDasar" id="tFarmasiHargaDasar" placeholder="Harga Dasar"></td>
                                <td><input type="text" <?=(UserAccesForm($user_acces_form,array('2665'))?'':'disabled')?> class="form-control input-sm discount tdFarmasiMarginPercent" id="tFarmasiMargin" placeholder="Margin (%)"></td>
                                <td><input type="text" class="form-control input-sm number" readonly id="tFarmasiHargaJual" placeholder="Harga Jual"></td>
                                <td><input type="text" <?=(UserAccesForm($user_acces_form,array('2666'))?'':'disabled')?> class="form-control input-sm discount tdFarmasiDiscountPercent" id="tFarmasiDiskonPersen" placeholder="Diskon (%)"></td>
                                <td><input type="text" <?=(UserAccesForm($user_acces_form,array('2666'))?'':'disabled')?> class="form-control input-sm number tdFarmasiDiscountRupiah" id="tFarmasiDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
                                <td><input type="text" class="form-control input-sm number" id="tFarmasiSubTotal" placeholder="Total Rp" readonly value="0"></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="bg-light dker">
                                <th colspan="5">
                                    <label class="pull-right"><b>Kuantitas</b></label>
                                </th>
                                <th>
                                    <input type="text" class="form-control input-sm number" readonly id="tFarmasiKuantitas" placeholder="Kuantitas"></th>
                            </tr>
                            <tr class="bg-light dker">
                                <th colspan="5">
                                    <label class="pull-right">
                                        <b>Grand Total (Rp)</b>
                                    </label>
                                </th>
                                <th>
                                    <input type="text" class="form-control input-sm number" id="tFarmasiGrandTotal" placeholder="Grand Total (Rp)" readonly value="0">
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer" id="actionDataObat">
                <button class="btn btn-sm btn-primary" id="saveDataTindakanFarmasi" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
                <button class="btn btn-sm btn-default" id="cancelDataObat" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '.editDataTindakanFarmasi', function() {
        var idrow = $(this).data('idrow');
        var table = $(this).data('table');
        var statusracikan = $(this).data('statusracikan');

        var notransaksi = $(this).closest('tr').find("td:eq(0)").html();
        var tanggal = $(this).closest('tr').find("td:eq(14)").html();

        var namatarif = $(this).closest('tr').find("td:eq(2)").html();

        var hargadasar = $(this).closest('tr').find("td:eq(3)").html();
        var margin = $(this).closest('tr').find("td:eq(4)").html();
        var hargajual = $(this).closest('tr').find("td:eq(5)").html();

        var kuantitas = $(this).closest('tr').find("td:eq(6)").html();
        var subtotal = $(this).closest('tr').find("td:eq(7)").html();

        var diskon_rupiah = $(this).closest('tr').find("td:eq(8)").html();
        var diskon_persen = $(this).closest('tr').find("td:eq(9)").html();

        var grandtotal = $(this).closest('tr').find("td:eq(10)").html();

        var idunitpelayanan = $(this).closest('tr').find("td:eq(11)").html();
        var unitpelayanan = $(this).closest('tr').find("td:eq(12)").html();

        $("#tFarmasiRowId").val(idrow);
        $("#tFarmasiTable").val(table);
        $("#tFarmasiStatusRacikan").val(statusracikan);

        $("#tFarmasiNoRegistrasi").val(notransaksi);
        $("#tFarmasiTanggal").val(tanggal);

        $("#tFarmasiNamaTarif").val(namatarif);

        $("#tFarmasiHargaDasar").val(hargadasar);
        $("#tFarmasiMargin").val(margin);
        $("#tFarmasiHargaJual").val(hargajual);

        $("#tFarmasiKuantitas").val(kuantitas);
        $("#tFarmasiSubTotal").val(parseFloat(grandtotal.replace(/,/g, '')) / parseFloat(kuantitas.replace(/,/g, '')));

        $("#tFarmasiDiskonRupiah").val(diskon_rupiah);
        $("#tFarmasiDiskonPersen").val(diskon_persen);

        $("#tFarmasiGrandTotal").val(grandtotal);

        $("#tFarmasiIdUnitPelayanan").val(idunitpelayanan);
        $("#tFarmasiUnitPelayanan").val(unitpelayanan);
    });

    $(document).on("keyup", ".tdFarmasiHargaDasar, .tdFarmasiMarginPercent", function() {
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var hargaDasar = parseFloat($("#tFarmasiHargaDasar").val());
        var margin = parseFloat($("#tFarmasiMargin").val());
        var hargaJual = hargaDasar + (hargaDasar * margin / 100);
        var diskonPersen = parseFloat($("#tFarmasiDiskonPersen").val());
        var diskonRupiah = (hargaJual * diskonPersen / 100);
        var hargaSetelahDiskon = hargaJual - diskonRupiah;
        var kuantitas = parseFloat($("#tFarmasiKuantitas").val());
        var totalHarga = hargaSetelahDiskon * kuantitas;

        $("#tFarmasiHargaJual").val(hargaJual);
        $("#tFarmasiSubTotal").val(hargaSetelahDiskon);
        $("#tFarmasiGrandTotal").val(totalHarga);

        return false;
    });

    $(document).on("keyup", ".tdFarmasiDiscountPercent", function() {
        if ($(this).val() == '') {
            $(this).val(0);
        }

        if (parseFloat($(this).val()) > 100) {
            $(this).val(100);
        }

        var diskonRupiah = 0;
        var hargaJual = parseFloat($("#tFarmasiHargaJual").val());
        var kuantitas = parseFloat($("#tFarmasiKuantitas").val());

        diskonRupiah = hargaJual * (parseFloat($(this).val()) / 100);

        var hargaSetelahDiskon = hargaJual - diskonRupiah;
        var totalHarga = (hargaJual - diskonRupiah) * kuantitas;

        $("#tFarmasiDiskonRupiah").val(diskonRupiah);
        $("#tFarmasiSubTotal").val(hargaSetelahDiskon);
        $("#tFarmasiGrandTotal").val(totalHarga);

        return false;
    });

    $(document).on("keyup", ".tdFarmasiDiscountRupiah", function() {
        var hargaJual = $("#tFarmasiHargaJual").val();
        var kuantitas = parseFloat($("#tFarmasiKuantitas").val());

        if ($(this).val() == '') {
            $(this).val(0);
        }

        if (parseFloat($(this).val()) > hargaJual) {
            $(this).val(hargaJual);
        }

        var diskonPersen = 0;
        diskonPersen = parseFloat($(this).val() * 100) / parseFloat(hargaJual);

        var hargaSetelahDiskon = hargaJual - $(this).val();
        var totalHarga = (hargaJual - $(this).val()) * kuantitas;

        $("#tFarmasiDiskonPersen").val(diskonPersen);
        $("#tFarmasiSubTotal").val(hargaSetelahDiskon);
        $("#tFarmasiGrandTotal").val(totalHarga);

        return false;
    });

    $(document).on('click', '#saveDataTindakanFarmasi', function() {
        var idrow = $("#tFarmasiRowId").val();
        var table = $("#tFarmasiTable").val();
        var statusRacikan = $("#tFarmasiStatusRacikan").val();
        var hargaDasar = $('#tFarmasiHargaDasar').val();
        var marginPersen = $('#tFarmasiMargin').val();
        var hargaJual = $('#tFarmasiHargaJual').val();
        var kuantitas = $('#tFarmasiKuantitas').val();
        var diskon = $('#tFarmasiDiskonPersen').val();
        var diskonRp = $('#tFarmasiDiskonRupiah').val();
        var total = $('#tFarmasiGrandTotal').val();

        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateDataTindakanFarmasi',
            method: 'POST',
            data: {
                idrow: idrow,
                table: table,
                statusracikan: statusRacikan,
                hargadasar: hargaDasar,
                margin: marginPersen,
                hargajual: hargaJual,
                kuantitas: kuantitas,
                diskon: diskon,
                diskon_rp: diskonRp,
                total: total
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Telah Diubah.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                location.reload();
            }
        });
    });
});

</script>
