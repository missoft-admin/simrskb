<div class="modal in" id="ActionTindakanModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" style="width:80%">
        <div class="modal-content">
            <div class="block-content">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="display:none">Detail ID</th>
                            <th>Tanggal</th>
                            <th>Tindakan</th>
                            <th>Tarif</th>
                            <th>Kuantitas</th>
                            <th>Jumlah</th>
                            <th>Dokter</th>
                            <th>Aksi</th>
                        </tr>
                        <tr>
                            <th style="display:none;">
                                <input type="text" id="fTindakanRowId" readonly value="">
                            </th>
                            <th style="width:10%">
                                <input type="text" id="fTindakanTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?= date("d/m/Y")?>">
                            </th>
                            <th style="width:10%">
                                <div class="input-group">
                                    <select id="fTindakanId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                    </select>
                                    <span id="fTindakanSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubTindakanModal"><i class="fa fa-search"></i></span>
                                </div>
                            </th>
                            <th style="width:10%">
                                <input type="hidden" id="fTindakanJasaSarana" value="" readonly="true">
                                <input type="hidden" id="fTindakanJasaPelayanan" value="" readonly="true">
                                <input type="hidden" id="fTindakanBHP" value="" readonly="true">
                                <input type="hidden" id="fTindakanBiayaPerawatan" value="" readonly="true">
                                <input type="text" class="form-control number" id="fTindakanTarif" placeholder="Tarif" value="" readonly="true">
                            </th>
                            <th style="width:10%">
                                <input type="text" class="form-control number" id="fTindakanKuantitas" placeholder="Kuantitas" value="">
                            </th>
                            <th style="width:10%">
                                <input type="text" class="form-control number" id="fTindakanJumlah" placeholder="Jumlah" value="" readonly="true">
                            </th>
                            <th style="width:10%">
                                <select id="fTindakanIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Dokter">
                                    <option value="">Pilih Dokter</option>
                                </select>
                            </th>
                            <th id="actionTindakanRanap" style="width:10%">
                                <button id="saveActionTindakanRanap" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in black-overlay" id="SubTindakanModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Tindakan</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-val">
                                <label class="col-sm-4" for="idtipe">Tipe</label>
                                <div class="col-sm-8">
                                    <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="1">Full Care</option>
                                        <option value="2">ECG</option>
                                        <option value="4">Sewa Alat</option>
                                        <option value="5">Ambulance</option>
                                        <option value="6">Lain-lain</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table width="100%" id="listTindakanTable" class="table table-striped table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Jasa Sarana</th>
                                <th>Jasa Pelayanan</th>
                                <th>BHP</th>
                                <th>Biaya Perawatan</th>
                                <th>Total Tarif</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#fTindakanId").select2({
        minimumInputLength: 2,
        ajax: {
            url: '{site_url}trawatinap_tindakan/getTindakanRawatInap',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function(params) {
                var query = {
                    search: params.term,
                }
                return query;
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $("#idtipe").change(function() {
        var idpendaftaran = $('#tempIdRawatInap').val();
        var idtipe = $(this).val();
        filterTarifTindakanRanap(idpendaftaran, idtipe);
    });

    $(document).on('click', '#fTindakanSearch', function() {
        var idpendaftaran = $('#tempIdRawatInap').val();
        var idtipe = '1';
        filterTarifTindakanRanap(idpendaftaran, idtipe);
    });

    $(document).on('click', '.selectTindakanTable', function() {
        var idpendaftaran = $('#tempIdRawatInap').val();
        var idtindakan = $(this).closest('tr').find("td:eq(0) a").data('idtindakan');
        var referenceAction = 'modal';
        getDetailTindakanRanap(idpendaftaran, idtindakan, referenceAction);
    });

    $(document).on('change', '#fTindakanId', function() {
        var idpendaftaran = $('#tempIdRawatInap').val();
        var idtindakan = $(this).val();
        var referenceAction = 'select';
        getDetailTindakanRanap(idpendaftaran, idtindakan, referenceAction);
    });

    $(document).on('keyup', '#fTindakanKuantitas', function() {
        var hargajual = $('#fTindakanTarif').val().replace(/,/g, '');
        var kuantitas = $(this).val().replace(/,/g, '');
        var jumlah = parseFloat(hargajual) * parseFloat(kuantitas);
        $('#fTindakanJumlah').val(jumlah);
    });

    $(document).on('click', '#saveActionTindakanRanap', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idRow = $('#fTindakanRowId').val();
        var tanggal = $('#fTindakanTanggal').val();
        var iddokter = $('#fTindakanIdDokter option:selected').val();
        var idpelayanan = $('#fTindakanId option:selected').val();
        var jasasarana = $('#fTindakanJasaSarana').val();
        var jasapelayanan = $('#fTindakanJasaPelayanan').val();
        var bhp = $('#fTindakanBHP').val();
        var biayaperawatan = $('#fTindakanBiayaPerawatan').val();
        var total = $('#fTindakanTarif').val();
        var kuantitas = $('#fTindakanKuantitas').val();
        var totalkeseluruhan = $('#fTindakanJumlah').val();

        if (tanggal == '') {
            sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanTanggal').focus();
            });
            return false;
        }

        if (idpelayanan == '') {
            sweetAlert("Maaf...", "Tindakan belum dipilih !", "error").then((value) => {
                $("#fTindakanId").select2('open');
            });
            return false;
        }

        if (total == '' || total <= 0) {
            sweetAlert("Maaf...", "Tarif tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanTarif').focus();
            });
            return false;
        }

        if (kuantitas == '' || kuantitas <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanKuantitas').focus();
            });
            return false;
        }

        if (totalkeseluruhan == '' || totalkeseluruhan <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanJumlah').focus();
            });
            return false;
        }

        var payload = {
            idpendaftaran: idpendaftaran,
            idrow: idRow,
            tanggal: tanggal,
            iddokter: iddokter,
            idpelayanan: idpelayanan,
            jasasarana: jasasarana,
            jasasarana_disc: 0,
            jasapelayanan: jasapelayanan,
            jasapelayanan_disc: 0,
            bhp: bhp,
            bhp_disc: 0,
            biayaperawatan: biayaperawatan,
            biayaperawatan_disc: 0,
            subtotal: total,
            kuantitas: kuantitas,
            diskon: 0,
            totalkeseluruhan: totalkeseluruhan,
        }

        submitTindakanRanap(payload);
    });
});

function filterTarifTindakanRanap(idpendaftaran, idtipe) {
    $('#listTindakanTable').DataTable().destroy();
    $('#listTindakanTable').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trawatinap_tindakan/getListTindakanRawatInap/' + idpendaftaran + '/' + idtipe,
            type: "POST",
            dataType: 'json'
        }
    });
}

function getDetailTindakanRanap(idpendaftaran, idtindakan, referenceAction) {
    $.ajax({
        url: "{site_url}trawatinap_tindakan/getDetailTindakanRawatInap/" + idpendaftaran + "/" + idtindakan,
        dataType: "json",
        success: function(data) {
            $("#fTindakanJasaSarana").val(data.jasasarana);
            $("#fTindakanJasaPelayanan").val(data.jasapelayanan);
            $("#fTindakanBHP").val(data.bhp);
            $("#fTindakanBiayaPerawatan").val(data.biayaperawatan);
            $("#fTindakanTarif").val(data.total);
            $("#fTindakanKuantitas").val(1);
            $("#fTindakanJumlah").val(data.total);

            if (referenceAction == 'modal') {
                $('#fTindakanId').select2("trigger", "select", {
                    data: {
                        id: idtindakan,
                        text: data.nama
                    }
                });
            }

            $("#fTindakanKuantitas").focus();
        }
    });
}

function submitTindakanRanap(payload) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/saveDataTindakan',
        method: 'POST',
        data: payload,
        success: function(data) {
            swal({
                title: "Berhasil!",
                text: "Data Tindakan Telah Tersimpan.",
                type: "success",
                timer: 1500,
                showConfirmButton: false
            });

            location.reload();
        }
    });
}

</script>
