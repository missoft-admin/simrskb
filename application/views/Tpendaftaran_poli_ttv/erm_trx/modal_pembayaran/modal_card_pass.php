<div class="modal fade in black-overlay" id="modal_card_pass" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">PROSES CARD PASS</h3>
				</div>
				<?
					$tgl=date('d-m-Y H:i:s');
				?>
				<div class="block-content">
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-sm-3">
									<label for="material-text">Tanggal</label>
									<div class="input-group">
										<input class="js-datetimepicker form-control input-sm" type="text" id="tanggal_proses_card_pass" value="<?=$tgl?>" />
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									
								</div>
								<div class="col-sm-9">
									<label for="material-text">Informasi Card Pass</label>
									<div class="input-group">
										<input class="form-control" id="info_proses_card_pass" type="text" value="">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn_proses_card_passs"><i class="fa fa-level-down"></i> PROSES CARD PASS</button>
										</span>
									</div>
								</div>
							</div>
						</div>

					</div>
					<?php echo form_close() ?>
								
					<input type="hidden" class="form-control" name="idupload_card2" id="idupload_card2" value="" readonly>
					<form action="{base_url}Tbilling_ranap/upload_card_pass" enctype="multipart/form-data"  class="dropzone" id="image-upload-card">
						<div class="row" disabled>
							<div class="form-group" style="margin-bottom: 8px;">
								<div class="col-md-12">
									<input type="hidden" class="form-control" name="idupload_card" id="idupload_card" value="" readonly>
									<div>
									  <h5>Upload</h5>
									</div>
								</div>

							</div>
						</div>
					</form>
					
					<br>
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="form-group" style="margin-bottom: 8px;">
							<div class="col-md-12">
							<div class="col-md-12">
								<table class="table table-bordered" id="list_file_card_pass">
									<thead>
										<tr>
											<th width="50%" class="text-center">File</th>
											<th width="15%" class="text-center">Size</th>
											<th width="35%" class="text-center">User</th>
											<th width="35%" class="text-center">X</th>
											
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							</div>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>