<div class="modal fade in black-overlay" id="EditTransaksiTindakanRanapModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI TINDAKAN RAWAT INAP</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tTindakanRowId" readonly value="">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanNoRegistrasi">No. Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tTindakanNoRegistrasi" placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanNamaTarif">Nama Tarif</label>
									<div class="col-md-9">
										<div class="input-group">
											<select id="tTindakanIdTarif" <?=(UserAccesForm($user_acces_form,array('2648'))?'':'disabled')?> class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
												<option value="">Pilih Opsi</option>
											</select>
											<span id="tTindakanSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubTindakanRanapModal"><i class="fa fa-search"></i></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanTanggal">Tanggal Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tTindakanTanggal" placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanIdDokter">Dokter</label>
									<div class="col-md-9">
										<select id="tTindakanIdDokter" <?=(UserAccesForm($user_acces_form,array('2649'))?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Dokter">
											<option value="">Pilih Dokter</option>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tTindakanItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Item Biaya</th>
								<th style="width: 25%;">Harga</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jasa Sarana</td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2644'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapHarga" id="tTindakanJasaSarana" placeholder="Jasa Sarana"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm discount tdTindakanRanapDiskonPersen" id="tTindakanJasaSaranaDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapDiskonRupiah" id="tTindakanJasaSaranaDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text"  class="form-control input-sm number" id="tTindakanJasaSaranaTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Jasa Pelayanan</td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2644'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapHarga" id="tTindakanJasaPelayanan" placeholder="Jasa Pelayanan"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm discount tdTindakanRanapDiskonPersen" id="tTindakanJasaPelayananDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapDiskonRupiah" id="tTindakanJasaPelayananDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tTindakanJasaPelayananTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>BHP</td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2644'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapHarga" id="tTindakanBHP" placeholder="BHP"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm discount tdTindakanRanapDiskonPersen" id="tTindakanBHPDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapDiskonRupiah" id="tTindakanBHPDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tTindakanBHPTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Biaya Perawatan</td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2644'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapHarga" id="tTindakanBiayaPerawatan" placeholder="Biaya Perawatan"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm discount tdTindakanRanapDiskonPersen" id="tTindakanBiayaPerawatanDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" <?=(UserAccesForm($user_acces_form,array('2645'))?'':'disabled')?> class="form-control input-sm number tdTindakanRanapDiskonRupiah" id="tTindakanBiayaPerawatanDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tTindakanBiayaPerawatanTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Sub Total (Rp)</b></label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tTindakanSubTotal" placeholder="Sub Total (Rp)" readonly value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Kuantitas</b></label>
								</th>
								<th>
									<input type="text" <?=(UserAccesForm($user_acces_form,array('2646'))?'':'disabled')?> class="form-control input-sm number" id="tTindakanKuantitas" placeholder="Kuantitas" value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4"><label class="pull-right"><b>Total (Rp)</b></label></th>
								<th>
									<input type="text" class="form-control input-sm number" id="tTindakanTotal" placeholder="Total (Rp)" readonly  value="0">
								</th>
							</tr>
							<tr>
								<th style="width: 85%;" colspan="4">
									<label class="pull-right">
										<b>Discount Semua</b>
									</label>
								</th>
								<th style="width: 15%;">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input <?=(UserAccesForm($user_acces_form,array('2647'))?'':'disabled')?> class="form-control number input-sm" type="text" id="tTindakanDiskonRupiah" placeholder="Discount (Rp)" value="0">
										</div>
										<div class="input-group">
											<span class="input-group-addon"> %. </span>
											<input <?=(UserAccesForm($user_acces_form,array('2647'))?'':'disabled')?> class="form-control discount input-sm" type="text" id="tTindakanDiskonPersen" placeholder="Discount (%)" value="0">
										</div>
								</th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right">
										<b>Grand Total (Rp)</b>
									</label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tTindakanGrandTotal" placeholder="Grand Total (Rp)" readonly value="0">
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataTindakan">
				<button class="btn btn-sm btn-primary" id="saveDataTindakan" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataTindakan" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in black-overlay" id="SubTindakanRanapModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Tindakan</h3>
				</div>
				<div class="block-content">
					<table width="100%" id="listTindakanRanapTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Jasa Sarana</th>
								<th>Jasa Pelayanan</th>
								<th>BHP</th>
								<th>Biaya Perawatan</th>
								<th>Total Tarif</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#tTindakanIdTarif").select2({
			minimumInputLength: 2,
			ajax: {
				url: '{site_url}trawatinap_tindakan/getTindakanRawatInap',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
				data: function(params) {
				var query = {
					search: params.term,
				}
				return query;
				},
				processResults: function(data) {
				return {
					results: $.map(data, function(item) {
					return {
						text: item.nama,
						id: item.id
					}
					})
				};
				}
			}
		}).on('select2:selecting', function (e) {
			var idpendaftaran = $('#tempIdRawatInap').val();
			var idtindakan = $(this).val();

			// getDetailTindakanRawatInap(idpendaftaran, idtindakan);
		});

		$(document).on('click', '#tTindakanSearch', function() {
			var idpendaftaran = $('#tempIdRawatInap').val();
			var idtipe = '1';

			$('#listTindakanRanapTable').DataTable().destroy();
			$('#listTindakanRanapTable').DataTable({
					"autoWidth": false,
					"pageLength": 10,
					"ordering": true,
					"processing": true,
					"serverSide": true,
					"order": [],
					"ajax": {
						url: '{site_url}trawatinap_tindakan/getListTindakanRawatInap/' + idpendaftaran + '/' + idtipe,
						type: "POST",
						dataType: 'json'
					}
				});
		});

		$(document).on('click', '.selectTindakanTable', function() {
			var idpendaftaran = $('#tempIdRawatInap').val();
			var idtindakan = $(this).closest('tr').find("td:eq(0) a").data('idtindakan');

			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDetailTindakanRawatInap/" + idpendaftaran + "/" + idtindakan,
				dataType:"json",
				success:function(data){
					$("#tTindakanJasaSarana").val(data.jasasarana);
					$("#tTindakanJasaSaranaDiskonPersen").val(0);
					$("#tTindakanJasaSaranaDiskonRupiah").val(0);
					$("#tTindakanJasaPelayanan").val(data.jasapelayanan);
					$("#tTindakanJasaPelayananDiskonPersen").val(0);
					$("#tTindakanJasaPelayananDiskonRupiah").val(0);
					$("#tTindakanBHP").val(data.bhp);
					$("#tTindakanBHPDiskonPersen").val(0);
					$("#tTindakanBHPDiskonRupiah").val(0);
					$("#tTindakanBiayaPerawatan").val(data.biayaperawatan);
					$("#tTindakanBiayaPerawatanDiskonPersen").val(0);
					$("#tTindakanBiayaPerawatanDiskonRupiah").val(0);
					$("#tTindakanSubTotal").val(data.total);
					$("#tTindakanKuantitas").val(1);
					$("#tTindakanTotal").val(data.total);
					$("#tTindakanDiskonPersen").val(0);
					$("#tTindakanDiskonRupiah").val(0);
					$("#tTindakanGrandTotal").val(data.total);

					$('#tTindakanIdTarif').select2("trigger", "select", {
						data : {
							id: idtindakan,
							text: data.nama
						}
					});

					$("#tTindakanKuantitas").focus();
				}
			});
		});

		$(document).on('keyup', '#tTindakanKuantitas', function() {
			var hargajual = $('#tTindakanSubTotal').val().replace(/,/g, '');
			var kuantitas = $(this).val().replace(/,/g, '');
			$('#tTindakanTotal').val(parseInt(hargajual) * parseInt(kuantitas));
		});

		$(document).on('click', '#saveDataTindakan', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var idrow = $('#tTindakanRowId').val();
			var tanggal = $('#tTindakanTanggal').val();
			var iddokter = $('#tTindakanIdDokter option:selected').val();
			var idpelayanan = $('#tTindakanIdTarif option:selected').val();
			var jasasarana = $('#tTindakanJasaSarana').val();
			var jasasarana_disc = $('#tTindakanJasaSaranaDiskonRupiah').val();
			var jasapelayanan = $('#tTindakanJasaPelayanan').val();
			var jasapelayanan_disc = $('#tTindakanJasaPelayananDiskonRupiah').val();
			var bhp = $('#tTindakanBHP').val();
			var bhp_disc = $('#tTindakanBHPDiskonRupiah').val();
			var biayaperawatan = $('#tTindakanBiayaPerawatan').val();
			var biayaperawatan_disc = $('#tTindakanBiayaPerawatanDiskonRupiah').val();
			var subtotal = $('#tTindakanSubTotal').val();
			var kuantitas = $('#tTindakanKuantitas').val();
			var totalkeseluruhan = $('#tTindakanTotal').val();
			var diskon = $('#tTindakanDiskonRupiah').val();

			if(tanggal == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanTanggal').focus();
				});
				return false;
			}

			if(idpelayanan == ''){
				sweetAlert("Maaf...", "Tindakan belum dipilih !", "error").then((value) => {
					$("#tTindakanIdTarif").select2('open');
				});
				return false;
			}

			if(subtotal == '' || subtotal <= 0){
				sweetAlert("Maaf...", "Sub Total tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanTarif').focus();
				});
				return false;
			}

			if(kuantitas == '' || kuantitas <= 0){
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanKuantitas').focus();
				});
				return false;
			}

			if(totalkeseluruhan == '' || totalkeseluruhan <= 0){
				sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanKuantitas').focus();
				});
				return false;
			}

			$.ajax({
				url: '{site_url}trawatinap_tindakan/saveDataTindakan',
				method: 'POST',
				data: {
					idpendaftaran: idpendaftaran,
					idrow: idrow,
					tanggal: tanggal,
					iddokter: iddokter,
					idpelayanan: idpelayanan,
					jasasarana: jasasarana,
					jasasarana_disc: jasasarana_disc,
					jasapelayanan: jasapelayanan,
					jasapelayanan_disc: jasapelayanan_disc,
					bhp: bhp,
					bhp_disc: bhp_disc,
					biayaperawatan: biayaperawatan,
					biayaperawatan_disc: biayaperawatan_disc,
					subtotal: subtotal,
					kuantitas: kuantitas,
					diskon: diskon,
					totalkeseluruhan: (totalkeseluruhan - diskon)
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Tindakan Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					$("#cover-spin").show();
					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataTindakan', function() {
		  var idrow = $(this).data('idrow');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();
			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();
			var jasasarana = $(this).closest('tr').find("td:eq(4)").html();
			var jasasarana_disc = $(this).closest('tr').find("td:eq(5)").html();
			var jasapelayanan = $(this).closest('tr').find("td:eq(6)").html();
			var jasapelayanan_disc = $(this).closest('tr').find("td:eq(7)").html();
			var bhp = $(this).closest('tr').find("td:eq(8)").html();
			var bhp_disc = $(this).closest('tr').find("td:eq(9)").html();
			var biayaperawatan = $(this).closest('tr').find("td:eq(10)").html();
			var biayaperawatan_disc = $(this).closest('tr').find("td:eq(11)").html();
			var subtotal = $(this).closest('tr').find("td:eq(12)").html();
			var kuantitas = $(this).closest('tr').find("td:eq(13)").html();
			var total = $(this).closest('tr').find("td:eq(14)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(15)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(16)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(17)").html();
			var iddokter = $(this).closest('tr').find("td:eq(18)").html();
			var namadokter = $(this).closest('tr').find("td:eq(19)").html();

			$("#tTindakanRowId").val(idrow);
			$("#tTindakanNoRegistrasi").val(nopendaftaran);
			$("#tTindakanTanggal").val(tanggal);

			$('#tTindakanIdTarif').select2("trigger", "select", {data : {id: idtarif,text: namatarif}});
			$('#tTindakanIdDokter').select2("trigger", "select", {data : {id: iddokter,text: namadokter}});

			$("#tTindakanJasaSarana").val(jasasarana);
			$("#tTindakanJasaSaranaDiskonRupiah").val(jasasarana_disc);

			$("#tTindakanJasaPelayanan").val(jasapelayanan);
			$("#tTindakanJasaPelayananDiskonRupiah").val(jasapelayanan_disc);

			$("#tTindakanBHP").val(bhp);
			$("#tTindakanBHPDiskonRupiah").val(bhp_disc);

			$("#tTindakanBiayaPerawatan").val(biayaperawatan);
			$("#tTindakanBiayaPerawatanDiskonRupiah").val(biayaperawatan_disc);

			$("#tTindakanSubTotal").val(subtotal);
			$("#tTindakanKuantitas").val(kuantitas);
			$("#tTindakanTotal").val(total);

			$("#tTindakanDiskonRupiah").val(total_disc_rp);
			$("#tTindakanDiskonPersen").val(total_disc_percent);
			$("#tTindakanGrandTotal").val(grandtotal);

			calculateDiscountTindakan();
		});

		$(document).on('click', '#cancelDataTindakan', function() {
			$("#tTindakanRowId").val('');
			$("#tTindakanNoRegistrasi").val('');
			$("#tTindakanTanggal").val('');

			$('#tTindakanIdTarif').select2("trigger", "select", {data : {id: '',text: ''}});
			$('#tTindakanIdDokter').select2("trigger", "select", {data : {id: '',text: ''}});

			$("#tTindakanJasaSarana").val('');
			$("#tTindakanJasaSaranaDiskonPersen").val('');

			$("#tTindakanJasaPelayanan").val('');
			$("#tTindakanJasaPelayananDiskonPersen").val('');

			$("#tTindakanBHP").val('');
			$("#tTindakanBHPDiskonPersen").val('');

			$("#tTindakanBiayaPerawatan").val('');
			$("#tTindakanBiayaPerawatanDiskonPersen").val('');

			$("#tTindakanSubTotal").val('');
			$("#tTindakanKuantitas").val('');
			$("#tTindakanTotal").val('');

			$("#tTindakanDiskonRupiah").val('');
			$("#tTindakanDiskonPersen").val('');
			$("#tTindakanGrandTotal").val('');
		});

		$(document).on('click', '.removeDataTindakan', function() {
		  var idrow = $(this).data('idrow');
		  var idrawatinap = $('#tempIdRawatInap').val();

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin menghapus data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      $.ajax({
		        url: '{site_url}trawatinap_tindakan/removeDataTindakan',
		        method: 'POST',
		        data: {
		          idrow: idrow
		        },
		        success: function(data) {
		          swal({
		            title: "Berhasil!",
		            text: "Data Deposit Telah Dihapus.",
		            type: "success",
		            timer: 1500,
		            showConfirmButton: false
		          });
					$("#cover-spin").show();
		          location.reload();
		        }
		      });
		    });
		});

		$("#tTindakanDiskonPersen").keyup(function(){
			if ($("#tTindakanDiskonPersen").val() == ''){
				$("#tTindakanDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100){
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tTindakanTotal").val() * parseFloat($(this).val())/100);
			$("#tTindakanDiskonRupiah").val(discount_rupiah);

			calculateTotalTindakan();
		});

		$("#tTindakanDiskonRupiah").keyup(function(){
			if ($("#tTindakanDiskonRupiah").val()==''){
				$("#tTindakanDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tTindakanTotal").val()){
				$(this).val($("#tTindakanTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val()*100))/$("#tTindakanTotal").val());
			$("#tTindakanDiskonPersen").val(discount_percent);

			calculateTotalTindakan();
		});

		$("#tTindakanKuantitas").keyup(function(){
			calculateTotalTindakan();
		});

		$(document).on("keyup", ".tdTindakanRanapHarga", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	var beforePrice = $(this).val();
    	var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(beforePrice);
    	$(this).closest('tr').find("td:eq(2) input").val(discount);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));

    	calculateTotalTindakan();
    	return false;
    });

    $(document).on("keyup", ".tdTindakanRanapDiskonPersen", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var afterPrice = 0;
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val()/100)));

    	$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

    	calculateTotalTindakan();
    	return false;
    });

    $(document).on("keyup", ".tdTindakanRanapDiskonRupiah", function () {//Diskon RP Keyup
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > beforePrice){
    		$(this).val(beforePrice);
    	}

    	var afterPrice = 0;
    	afterPrice = parseFloat($(this).val()*100) / parseFloat(beforePrice);

    	$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

    	calculateTotalTindakan();
    	return false;
    });
});

function calculateTotalTindakan() {
	var subTotal, totalTransaction, totalDiscountRupiah;

	subTotal = (parseFloat($("#tTindakanJasaSaranaTotal").val()) + parseFloat($("#tTindakanJasaPelayananTotal").val()) + parseFloat($("#tTindakanBHPTotal").val()) + parseFloat($("#tTindakanBiayaPerawatanTotal").val()));
	$("#tTindakanSubTotal").val(subTotal);

	total = parseFloat(subTotal) * parseFloat($("#tTindakanKuantitas").val());
	$("#tTindakanTotal").val(total);

	discountRupiah = parseFloat($("#tTindakanDiskonRupiah").val());

	grandTotal = total - discountRupiah;
	$("#tTindakanGrandTotal").val(parseFloat(grandTotal));
}

function calculateDiscountTindakan(){
	var discount = 0;
	var beforePrice = 0;
	var discountPrice = 0;

	$('#tTindakanItem tbody tr').each(function() {
		beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
		discountRupiah = $(this).closest('tr').find("td:eq(3) input").val();
		if (beforePrice>0){
		discountPercent = (discountRupiah / beforePrice) * 100;
			
		}else{
			discountPercent=0;
		}

		$(this).closest('tr').find("td:eq(2) input").val(discountPercent);
		$(this).closest('tr').find("td:eq(4) input").val(beforePrice - discountRupiah);
	});

	calculateTotalTindakan();
}

function getDetailTindakanRawatInap(idpendaftaran, idtindakan) {
	$.ajax({
		url:"{site_url}trawatinap_tindakan/getDetailTindakanRawatInap/" + idpendaftaran + "/" + idtindakan,
		dataType:"json",
		success:function(data){
			$("#tTindakanJasaSarana").val(data.jasasarana);
			$("#tTindakanJasaSaranaDiskonPersen").val(0);
			$("#tTindakanJasaSaranaDiskonRupiah").val(0);
			$("#tTindakanJasaPelayanan").val(data.jasapelayanan);
			$("#tTindakanJasaPelayananDiskonPersen").val(0);
			$("#tTindakanJasaPelayananDiskonRupiah").val(0);
			$("#tTindakanBHP").val(data.bhp);
			$("#tTindakanBHPDiskonPersen").val(0);
			$("#tTindakanBHPDiskonRupiah").val(0);
			$("#tTindakanBiayaPerawatan").val(data.biayaperawatan);
			$("#tTindakanBiayaPerawatanDiskonPersen").val(0);
			$("#tTindakanBiayaPerawatanDiskonRupiah").val(0);
			$("#tTindakanSubTotal").val(data.total);
			$("#tTindakanKuantitas").val(1);
			$("#tTindakanTotal").val(data.total);
			$("#tTindakanDiskonPersen").val(0);
			$("#tTindakanDiskonRupiah").val(0);
			$("#tTindakanGrandTotal").val(data.total);

			$("#tTindakanKuantitas").focus();
		}
	});
}
</script>
