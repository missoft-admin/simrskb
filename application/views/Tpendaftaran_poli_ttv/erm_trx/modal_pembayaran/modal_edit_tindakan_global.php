<div class="modal fade in black-overlay" id="EditTransaksiTindakanGlobalModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI GLOBAL</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">

							<input type="hidden" id="tTindakanGlobalRowId" readonly value="">
							<input type="hidden" id="tTindakanGlobalTable" readonly value="">

							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanGlobalNoRegistrasi">No. Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tTindakanGlobalNoRegistrasi" placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanGlobalNamaTarif">Nama Tarif</label>
									<div class="col-md-9">
										<input type="hidden" id="tTindakanGlobalIdTarif">
										<input type="text" class="form-control input-sm" id="tTindakanGlobalNamaTarif" placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanGlobalTanggal">Tanggal Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tTindakanGlobalTanggal" placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tTindakanGlobalIdDokter">Dokter</label>
									<div class="col-md-9">
										<select <?=(UserAccesForm($user_acces_form,array('2681'))?'':'disabled')?> id="tTindakanGlobalIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Dokter">
											<option value="">Pilih Dokter</option>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tTindakanGlobalItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Item Biaya</th>
								<th style="width: 25%;">Harga</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jasa Sarana</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2676'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalHarga" id="tTindakanGlobalJasaSarana" placeholder="Jasa Sarana"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm discount tdTindakanGlobalDiskonPersen" id="tTindakanGlobalJasaSaranaDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalDiskonRupiah" id="tTindakanGlobalJasaSaranaDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tTindakanGlobalJasaSaranaTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Jasa Pelayanan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2676'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalHarga" id="tTindakanGlobalJasaPelayanan" placeholder="Jasa Pelayanan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm discount tdTindakanGlobalDiskonPersen" id="tTindakanGlobalJasaPelayananDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalDiskonRupiah" id="tTindakanGlobalJasaPelayananDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tTindakanGlobalJasaPelayananTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>BHP</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2676'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalHarga" id="tTindakanGlobalBHP" placeholder="BHP"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm discount tdTindakanGlobalDiskonPersen" id="tTindakanGlobalBHPDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalDiskonRupiah" id="tTindakanGlobalBHPDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tTindakanGlobalBHPTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Biaya Perawatan</td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2676'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalHarga" id="tTindakanGlobalBiayaPerawatan" placeholder="Biaya Perawatan"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm discount tdTindakanGlobalDiskonPersen" id="tTindakanGlobalBiayaPerawatanDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input <?=(UserAccesForm($user_acces_form,array('2677'))?'':'disabled')?> type="text" class="form-control input-sm number tdTindakanGlobalDiskonRupiah" id="tTindakanGlobalBiayaPerawatanDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tTindakanGlobalBiayaPerawatanTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right"><b>Sub Total (Rp)</b></label>
								</th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tTindakanGlobalSubTotal" placeholder="Sub Total (Rp)" readonly value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right"><b>Kuantitas</b></label>
								</th>
								<th colspan="2">
									<input type="text" <?=(UserAccesForm($user_acces_form,array('2678'))?'':'disabled')?> class="form-control input-sm number" id="tTindakanGlobalKuantitas" placeholder="Kuantitas" value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3"><label class="pull-right"><b>Total (Rp)</b></label></th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tTindakanGlobalTotal" placeholder="Total (Rp)" readonly  value="0">
								</th>
							</tr>
							<tr>
								<th style="width: 85%;" colspan="3">
									<label class="pull-right">
										<b>Discount Semua</b>
									</label>
								</th>
								<th style="width: 15%;" colspan="2">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input <?=(UserAccesForm($user_acces_form,array('2679'))?'':'disabled')?> class="form-control number input-sm" type="text" id="tTindakanGlobalDiskonRupiah" placeholder="Discount (Rp)" value="0">
										</div>
										<div class="input-group">
											<span class="input-group-addon"> %. </span>
											<input  <?=(UserAccesForm($user_acces_form,array('2679'))?'':'disabled')?> class="form-control discount input-sm" type="text" id="tTindakanGlobalDiskonPersen" placeholder="Discount (%)" value="0">
										</div>
								</th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right">
										<b>Grand Total (Rp)</b>
									</label>
								</th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tTindakanGlobalGrandTotal" placeholder="Grand Total (Rp)" readonly value="0">
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataTindakanGlobal">
				<button class="btn btn-sm btn-primary" id="saveDataTindakanGlobal" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataTindakanGlobal" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#tTindakanGlobalKuantitas").keyup(function(){
			var hargajual = $('#tTindakanGlobalSubTotal').val().replace(/,/g, '');
			var kuantitas = $(this).val().replace(/,/g, '');
			$('#tTindakanGlobalTotal').val(parseInt(hargajual) * parseInt(kuantitas));

			calculateDiscountTindakanGlobal();
		});

		$("#tTindakanGlobalDiskonPersen").keyup(function(){
			if ($("#tTindakanGlobalDiskonPersen").val() == ''){
				$("#tTindakanGlobalDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100){
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tTindakanGlobalTotal").val() * parseFloat($(this).val())/100);
			$("#tTindakanGlobalDiskonRupiah").val(discount_rupiah);

			calculateDiscountTindakanGlobal();
		});

		$("#tTindakanGlobalDiskonRupiah").keyup(function(){
			if ($("#tTindakanGlobalDiskonRupiah").val()==''){
				$("#tTindakanGlobalDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tTindakanGlobalTotal").val()){
				$(this).val($("#tTindakanGlobalTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val()*100))/$("#tTindakanGlobalTotal").val());
			$("#tTindakanGlobalDiskonPersen").val(discount_percent);

			calculateDiscountTindakanGlobal();
		});

		$(document).on('click', '#saveDataTindakanGlobal', function() {
			var idrow = $('#tTindakanGlobalRowId').val();
			var table = $('#tTindakanGlobalTable').val();
			var tanggal = $('#tTindakanGlobalTanggal').val();
			var iddokter = $('#tTindakanGlobalIdDokter option:selected').val();
			var idpelayanan = $('#tTindakanGlobalIdTarif').val();
			var jasasarana = $('#tTindakanGlobalJasaSarana').val();
			var jasasarana_disc = $('#tTindakanGlobalJasaSaranaDiskonRupiah').val();
			var jasapelayanan = $('#tTindakanGlobalJasaPelayanan').val();
			var jasapelayanan_disc = $('#tTindakanGlobalJasaPelayananDiskonRupiah').val();
			var bhp = $('#tTindakanGlobalBHP').val();
			var bhp_disc = $('#tTindakanGlobalBHPDiskonRupiah').val();
			var biayaperawatan = $('#tTindakanGlobalBiayaPerawatan').val();
			var biayaperawatan_disc = $('#tTindakanGlobalBiayaPerawatanDiskonRupiah').val();
			var subtotal = $('#tTindakanGlobalSubTotal').val();
			var kuantitas = $('#tTindakanGlobalKuantitas').val();
			var totalkeseluruhan = $('#tTindakanGlobalTotal').val();
			var diskon = $('#tTindakanGlobalDiskonRupiah').val();

			if(tanggal == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanGlobalTanggal').focus();
				});
				return false;
			}

			if(subtotal == '' || subtotal <= 0){
				sweetAlert("Maaf...", "Sub Total tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanGlobalTarif').focus();
				});
				return false;
			}

			if(kuantitas == '' || kuantitas <= 0){
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanGlobalKuantitas').focus();
				});
				return false;
			}

			if(totalkeseluruhan == '' || totalkeseluruhan <= 0){
				sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
					$('#tTindakanGlobalKuantitas').focus();
				});
				return false;
			}

			$.ajax({
				url: '{site_url}trawatinap_tindakan/updateDataTindakanGlobal',
				method: 'POST',
				data: {
					table: table,
					idrow: idrow,
					tanggal: tanggal,
					iddokter: iddokter,
					idpelayanan: idpelayanan,
					jasasarana: jasasarana,
					jasasarana_disc: jasasarana_disc,
					jasapelayanan: jasapelayanan,
					jasapelayanan_disc: jasapelayanan_disc,
					bhp: bhp,
					bhp_disc: bhp_disc,
					biayaperawatan: biayaperawatan,
					biayaperawatan_disc: biayaperawatan_disc,
					subtotal: subtotal,
					kuantitas: kuantitas,
					diskon: diskon,
					totalkeseluruhan: (totalkeseluruhan - diskon)
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Tindakan Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					$("#cover-spin").show();
					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataTindakanGlobal', function() {
		  var idrow = $(this).data('idrow');
		  var table = $(this).data('table');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();
			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();
			var jasasarana = $(this).closest('tr').find("td:eq(4)").html();
			var jasasarana_disc = $(this).closest('tr').find("td:eq(5)").html();
			var jasapelayanan = $(this).closest('tr').find("td:eq(6)").html();
			var jasapelayanan_disc = $(this).closest('tr').find("td:eq(7)").html();
			var bhp = $(this).closest('tr').find("td:eq(8)").html();
			var bhp_disc = $(this).closest('tr').find("td:eq(9)").html();
			var biayaperawatan = $(this).closest('tr').find("td:eq(10)").html();
			var biayaperawatan_disc = $(this).closest('tr').find("td:eq(11)").html();
			var subtotal = $(this).closest('tr').find("td:eq(12)").html();
			var kuantitas = $(this).closest('tr').find("td:eq(13)").html();
			var total = $(this).closest('tr').find("td:eq(14)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(15)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(16)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(17)").html();
			var iddokter = $(this).closest('tr').find("td:eq(18)").html();
			// alert(iddokter);
			var namadokter = $(this).closest('tr').find("td:eq(19)").html();
			$('#tTindakanGlobalIdDokter').select2({
			   dropdownParent: $('#EditTransaksiTindakanGlobalModal')
			});
			$("#tTindakanGlobalRowId").val(idrow);
			$("#tTindakanGlobalTable").val(table);

			$("#tTindakanGlobalNoRegistrasi").val(nopendaftaran);
			$("#tTindakanGlobalTanggal").val(tanggal);

			$('#tTindakanGlobalIdTarif').val(idtarif);
			$('#tTindakanGlobalNamaTarif').val(namatarif);

			$('#tTindakanGlobalIdDokter').select2("trigger", "select", {data : {id: iddokter,text: namadokter}});

			$("#tTindakanGlobalJasaSarana").val(jasasarana);
			$("#tTindakanGlobalJasaSaranaDiskonRupiah").val(jasasarana_disc);

			$("#tTindakanGlobalJasaPelayanan").val(jasapelayanan);
			$("#tTindakanGlobalJasaPelayananDiskonRupiah").val(jasapelayanan_disc);

			$("#tTindakanGlobalBHP").val(bhp);
			$("#tTindakanGlobalBHPDiskonRupiah").val(bhp_disc);

			$("#tTindakanGlobalBiayaPerawatan").val(biayaperawatan);
			$("#tTindakanGlobalBiayaPerawatanDiskonRupiah").val(biayaperawatan_disc);

			$("#tTindakanGlobalSubTotal").val(subtotal);
			$("#tTindakanGlobalKuantitas").val(kuantitas);
			$("#tTindakanGlobalTotal").val(total);

			$("#tTindakanGlobalDiskonRupiah").val(total_disc_rp);
			$("#tTindakanGlobalDiskonPersen").val(total_disc_percent);
			$("#tTindakanGlobalGrandTotal").val(grandtotal);

			calculateDiscountTindakanGlobal();
		});

		$(document).on('click', '#cancelDataTindakanGlobal', function() {
			$("#tTindakanGlobalRowId").val('');
			$("#tTindakanGlobalNoRegistrasi").val('');
			$("#tTindakanGlobalTanggal").val('');

			$('#tTindakanGlobalIdTarif').val('');
			$('#tTindakanGlobalNamaTarif').val('');

			$('#tTindakanGlobalIdDokter').select2("trigger", "select", {data : {id: '',text: ''}});

			$("#tTindakanGlobalJasaSarana").val('');
			$("#tTindakanGlobalJasaSaranaDiskonPersen").val('');

			$("#tTindakanGlobalJasaPelayanan").val('');
			$("#tTindakanGlobalJasaPelayananDiskonPersen").val('');

			$("#tTindakanGlobalBHP").val('');
			$("#tTindakanGlobalBHPDiskonPersen").val('');

			$("#tTindakanGlobalBiayaPerawatan").val('');
			$("#tTindakanGlobalBiayaPerawatanDiskonPersen").val('');

			$("#tTindakanGlobalSubTotal").val('');
			$("#tTindakanGlobalKuantitas").val('');
			$("#tTindakanGlobalTotal").val('');

			$("#tTindakanGlobalDiskonRupiah").val('');
			$("#tTindakanGlobalDiskonPersen").val('');
			$("#tTindakanGlobalGrandTotal").val('');
		});

		$(document).on("keyup", ".tdTindakanGlobalHarga", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	var beforePrice = $(this).val();
    	var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(beforePrice);
    	$(this).closest('tr').find("td:eq(2) input").val(discount);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));

    	calculateTotalTindakanGlobal();
    	return false;
    });

    $(document).on("keyup", ".tdTindakanGlobalDiskonPersen", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var afterPrice = 0;
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val()/100)));

    	$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

    	calculateTotalTindakanGlobal();
    	return false;
    });

    $(document).on("keyup", ".tdTindakanGlobalDiskonRupiah", function () {//Diskon RP Keyup
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > beforePrice){
    		$(this).val(beforePrice);
    	}

    	var afterPrice = 0;
    	afterPrice = parseFloat($(this).val()*100) / parseFloat(beforePrice);

    	$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

    	calculateTotalTindakanGlobal();
    	return false;
    });
});

function calculateTotalTindakanGlobal() {
	var subTotal, totalTransaction, totalDiscountRupiah;

	subTotal = (parseFloat($("#tTindakanGlobalJasaSaranaTotal").val()) + parseFloat($("#tTindakanGlobalJasaPelayananTotal").val()) + parseFloat($("#tTindakanGlobalBHPTotal").val()) + parseFloat($("#tTindakanGlobalBiayaPerawatanTotal").val()));
	$("#tTindakanGlobalSubTotal").val(subTotal);

	total = parseFloat(subTotal) * parseFloat($("#tTindakanGlobalKuantitas").val());
	$("#tTindakanGlobalTotal").val(total);

	discountRupiah = parseFloat($("#tTindakanGlobalDiskonRupiah").val());

	grandTotal = total - discountRupiah;
	$("#tTindakanGlobalGrandTotal").val(parseFloat(grandTotal));
}

function calculateDiscountTindakanGlobal(){
	var discount = 0;
	var beforePrice = 0;
	var discountPrice = 0;

	$('#tTindakanGlobalItem tbody tr').each(function() {
		beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
		discountRupiah = $(this).closest('tr').find("td:eq(3) input").val();
		discountPercent = (discountRupiah / beforePrice) * 100;

		$(this).closest('tr').find("td:eq(2) input").val(discountPercent);
		$(this).closest('tr').find("td:eq(4) input").val(beforePrice - discountRupiah);
	});

	calculateTotalTindakanGlobal();
}
</script>
