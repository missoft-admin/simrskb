<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		
</style>
<style>
	#sig_2 canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_visit'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_visit' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
		// echo $tanggal_pernyataan;
	if ($tanggal_transaksi){
		
		
		$tgltransaksi=HumanDateShort($tanggal_transaksi);
		$waktutransaksi=HumanTime($tanggal_transaksi);
	}else{
		$tgltransaksi=date('d-m-Y');
		$waktutransaksi=date('H:i:s');
	}
	
	$disabel_input='';
	$disabel_cetak='disabled';
	
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-note"></i> Input Tindakan</a>
				</li>
				<li class="">
					<a href="#tab_2" onclick="list_history_layanan()"><i class="fa fa-history"></i> Riwayat Tindakan</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<input type="hidden" id="idtipe_poli" value="{idtipe_poli}" >		
					<input type="hidden" id="idpoliklinik" value="{idpoliklinik}" >		
					<input type="hidden" id="statuspasienbaru" value="{statuspasienbaru}" >		
					<input type="hidden" id="pertemuan_id" value="{pertemuan_id}" >		
					<input type="hidden" id="transaksi_id" value="" >		
					<input type="hidden" id="st_browse" value="0" >		
					<input type="hidden" id="mppa_id" value="{mppa_id}" >		
					<input type="hidden" id="iddokter_asal" value="{iddokter}" >		
					<input type="hidden" id="idkategori_dokter" value="{idkategori_dokter}" >		
					<input type="hidden" id="st_duplikate_visit" value="{st_duplikate_visit}" >		
					<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}" >		
					<input type="hidden" id="idrekanan" value="{idrekanan}" >		
					<input type="hidden" id="idtipe" value="{idtipe}" >		
					<input type="hidden" id="idtipepasien" value="{idtipepasien}" >		
					<div class="row">
						<div class="col-md-12">
								<h4 class="font-w700 push-5 text-center text-primary">VISIT DOKTER RAWAT INAP</h4>
						</div>
					</div>
					<?if($statuskasir_rajal!='2'){?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p>Info <a class="alert-link" href="javascript:void(0)"> Auto Save </a>!</p>
								</div>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
									
									
								</div>
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="tgltransaksi">Tanggal Visite</label>
									<div class="input-group date">
										<input tabindex="1" type="text" class="js-datepicker form-control " data-date-format="dd-mm-yyyy" id="tgltransaksi" placeholder="HH/BB/TTTT" name="tgltransaksi" value="<?= $tgltransaksi ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								
								
								<div class="col-md-5 ">
									<label for="totalkeseluruhan">Dokter </label>
										<div class="input-group">
											<select tabindex="3" id="iddokter"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih PPA Pelaksana" required>
												<option value="#" selected>Pilih Opsi</option>
												<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>" <?=($iddokter==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
											<span class="input-group-addon"><i class="fa fa-user"></i></span>
										</div>
								</div>
								<div class="col-md-3 ">
									<label for="idruangan">Pilih Ruangan</label>
									<select tabindex="3" id="idruangan" <?=($st_ruangan_visit=='0'?'disabled':'')?> class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
											<option value="<?=$r->id?>" <?=($idruangan==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
								<div class="col-md-2 ">
									<label for="idkelas">Pilih Kelas</label>
									<select tabindex="3" id="idkelas" <?=($st_kelas_visit=='0'?'disabled':'')?> class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" selected>Pilih Opsi</option>
										<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
											<option value="<?=$r->id?>" <?=($idkelas==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
										
									</select>
									
								</div>
							</div>
							
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								
								<div class="col-md-5 ">
									<label for="idpelayanan">Nama Tarif</label>
									<select tabindex="3" id="idpelayanan" disabled class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" selected>Tidak Dipililh</option>
										<?foreach(get_all('mtarif_visitedokter',array('status'=>1)) as $r){?>
											<option value="<?=$r->id?>" <?=($idpelayanan==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-2 ">
									<label for="jasasarana">Jasa Sarana</label>
									<input class="form-control number" disabled type="text" id="jasasarana" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="jasapelayanan">Jasa Pelayanan</label>
									<input class="form-control number" disabled type="text" id="jasapelayanan" value="">
								</div>
								<div class="col-md-3 ">
									<label for="hargajual">Jasa Pelayanan Disc</label>
									<div class="input-group">
										<input class="form-control decimal diskon" <?=($st_diskon_visit=='0'?'disabled':'')?> type="text" id="diskon"  value="0" placeholder="%">
										<span class="input-group-addon">% / Rp.</span>
										<input class="form-control number diskon" <?=($st_diskon_visit=='0'?'disabled':'')?>  type="text" id="diskonRp" value="0" placeholder="Rp" >
									</div>
									
								</div>
							</div>
							
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								
								
								<div class="col-md-2 ">
									<label for="hargadasar">BHP</label>
									<input class="form-control number" disabled type="text" id="bhp" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="hargadasar">Biaya Perawatan</label>
									<input class="form-control number" disabled type="text" id="biayaperawatan" value="">
									
								</div>
								<div class="col-md-3 ">
									<label for="idtipe">Tarif</label>
									<input class="form-control number" disabled type="text" id="total" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Kuantitas</label>
									<input class="form-control number" <?=($st_kuantitas_visit=='0'?'disabled':'')?>  type="text" id="kuantitas" value="1">
									
								</div>
								
								<div class="col-md-3">
									<label for="totalkeseluruhan">Total</label>
									<div class="input-group">
										<input class="form-control number" type="text" disabled id="totalkeseluruhan" value="">
										<span class="input-group-btn">
											<button onclick="add_tindakan()" title="Add Pelayanan" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Add </button>
											<button onclick="clear_layanan()" title="Clear Pelayanan" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-header-bg" id="index_transaksi">
											<thead>
												<tr>
													<th width="3%" class="text-center">No</th>
													<th width="15%" class="text-center">VISITE</th>
													<th width="9%" class="text-center">RUANGAN - KELAS</th>
													<th width="10%" class="text-center">NAMA TARIF</th>
													<th width="8%" class="text-center">JASA SARANA</th>
													<th width="8%" class="text-center">JASA PELAYANAN</th>
													<th width="8%" class="text-center">BHP</th>
													<th width="8%" class="text-center">BIAYA PERAWATAN</th>
													<th width="8%" class="text-center">TARIF</th>
													<th width="5%" class="text-center">QTY</th>
													<th width="8%" class="text-center">TOTAL</th>
													<th width="12%" class="text-center">ACTION</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot >
												<tr>
													<td colspan="10" class="text-right"><strong>TOTAL</strong></td>
													<td class="text-right"><label id="total_penjualan"></label></td>
													<td></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>	
					<?}?>
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" >Input Tindakan</h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
					<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">DAFTAR INPUT VISITE</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Kelas Perawatan</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_layanan()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_layanan">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">No Pendaftaran</th>
												<th width="10%">Tanggal Kunjungan</th>
												<th width="10%">Tujuan</th>
												<th width="10%">Ruang Perawatan</th>
												<th width="15%">Dokter</th>
												<th width="15%">Total</th>
												<th width="10%">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						</div>
						<hr class="push-5-b">
						<div id="div_history">
						<div class="row">
							<div class="form-group">
								<div class="col-md-4 ">
									
								</div>
								<div class="col-md-8 ">
									<div class="pull-right push-10-r">
										<button class="btn btn-sm btn-default" onclick="back_history()" type="button"><i class="fa fa-reply"></i> Kembali </button>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">HISTORY INPUT TINDAKAN</h4>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-header-bg" id="index_transaksi_history">
												<thead>
												<tr>
													<th width="3%" class="text-center">No</th>
													<th width="15%" class="text-center">VISITE</th>
													<th width="9%" class="text-center">RUANGAN - KELAS</th>
													<th width="10%" class="text-center">NAMA TARIF</th>
													<th width="8%" class="text-center">JASA SARANA</th>
													<th width="8%" class="text-center">JASA PELAYANAN</th>
													<th width="8%" class="text-center">BHP</th>
													<th width="8%" class="text-center">BIAYA PERAWATAN</th>
													<th width="8%" class="text-center">TARIF</th>
													<th width="5%" class="text-center">QTY</th>
													<th width="8%" class="text-center">TOTAL</th>
												</tr>
											</thead>
											<tbody></tbody>
											
												<tfoot >
													<tr>
														<td colspan="10" class="text-right"><strong>TOTAL</strong></td>
														<td class="text-right"><label id="total_penjualan_history"></label></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>	
					</div>	
					
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
					
					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal fade in black-overlay" id="modal_upload" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">BUKTI VISITE </h3>
				</div>
				<div class="block-content">
				<?php echo form_open('#','class="form-horizontal push-10-t" ') ?>
				</form>
				<div class="col-md-12" style="margin-top: 10px;">
					<div class="block" id="box_file2">					
						 <table class="table table-bordered" id="list_file_bukti">
							<thead>
								<tr>
									<th width="50%" class="text-center">File</th>
									<th width="15%" class="text-center">Size</th>
									<th width="35%" class="text-center">User</th>
									<th width="35%" class="text-center">X</th>
									
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
					<div class="block">
						<form action="{base_url}tpoliklinik_trx/upload_bukti" enctype="multipart/form-data" class="dropzone" id="image_bukti">
							<input type="hidden" class="form-control" name="iddetail_visite" id="iddetail_visite" value="" readonly>
							<div>
							  <h5>Upload Bukti Visite</h5>
							</div>
						</form>
					</div>
					
				</div> 
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" id="btn_selesai" type="button"><i class="fa fa-check"></i> Selesai</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
				</div>
				
			</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Dokter </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_2" ></div>
									</div>
								</div>
								<textarea id="signature64_2" name="signed_2" style="display: none"></textarea>
							</div>
							<input type="hidden" readonly id="ttd_id" name="ttd_id" value="">
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
var myDropzone3 ;
var sig_2 = $('#sig_2').signature({syncField: '#signature64_2', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig_2.signature('clear');
	$("#signature64_2").val('');
});
$(document).ready(function() {
	
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	select2_layanan();
	// select2_subparent();
	// clear_layanan();
	
	load_transaksi_layanan_list();
	// list_history_layanan();
	// load_tab3();
	Dropzone.autoDiscover = false;
	myDropzone3 = new Dropzone("#image_bukti", { 
	   autoProcessQueue: true,
	   maxFilesize: 30,
	});
	myDropzone3.on("complete", function(file) {		  
	  refresh_image_bukti();
	  myDropzone3.removeFile(file);
	  
	});
});
function ttd_dokter(id){
	$("#ttd_id").val(id);
	$("#modal_ttd").modal('show');
	$.ajax({
		url: '{site_url}tpoliklinik_trx/get_ttd_visit/'+id,
		dataType: "json",
		success: function(data) {
			$("#signature64_2").val(data.ttd_dokter);
			sig_2.signature('enable').signature('draw', data.ttd_dokter);
		}
	});
}
$(document).on("click",".hapus_file_bukti",function(){	
	var currentRow=$(this).closest("tr");          
	 var id=currentRow.find("td:eq(4)").text();
	 $.ajax({
		url: '{site_url}tpoliklinik_trx/hapus_file_bukti',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
			currentRow.remove();
			// table.ajax.reload( null, false );				
		}
	});
});
$(document).on("click","#btn_save_ttd",function(){	
	var ttd_id=$("#ttd_id").val();          
	var ttd_dokter=$("#signature64_2").val();          
	 $.ajax({
		url: '{site_url}tpoliklinik_trx/simpan_ttd_visite',
		type: 'POST',
		data: {ttd_id: ttd_id,ttd_dokter:ttd_dokter},
		success : function(data) {				
			load_transaksi_layanan_list();
			$("#modal_ttd").modal('hide');
		}
	});
});
function refresh_image_bukti(){
	var id=$("#iddetail_visite").val();
	$('#list_file_bukti tbody').empty();
	$.ajax({
		url: '{site_url}tpoliklinik_trx/refresh_image_bukti/'+id,
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#list_file_bukti tbody').empty();
				// $("#box_file2").attr("hidden",false);
				$("#list_file_bukti tbody").append(data.detail);
			}
			load_transaksi_layanan_list();
			
		}
	});
}
function upload_bukti(id){
	$("#iddetail_visite").val(id);
	$("#modal_upload").modal('show');
	refresh_image_bukti();
}
$(document).on("keyup", "#diskon", function() {
	var jasapelayanan = $("#jasapelayanan").val();
	if ($("#diskon").val() == '') {
		$("#diskon").val(0)
	}
	if (parseFloat($(this).val()) > 100) {
		$(this).val(100);
	}

	var discount_rupiah = parseFloat(jasapelayanan * parseFloat($(this).val()) / 100);
	$("#diskonRp").val(discount_rupiah);
	hitung_total();
});

$(document).on("keyup", "#diskonRp", function() {
	var jasapelayanan = $("#jasapelayanan").val();
	if ($("#diskonRp").val() == '') {
		$("#diskonRp").val(0)
	}
	if (parseFloat($(this).val()) > jasapelayanan) {
		$(this).val(jasapelayanan);
	}

	var discount_percent = parseFloat((parseFloat($(this).val() * 100)) / jasapelayanan);
	$("#diskon").val(discount_percent);
	hitung_total();
});
function load_tab3(){
	// if ($("#paket_id").val()!=''){
		// load_layanan_paket();
	// }else{
		// list_history_layanan_paket();
		
	// }
	
}
function select2_layanan(){
	let idkelompokpasien=$("#idkelompokpasien").val();
	let idrekanan=$("#idrekanan").val();
	let idruangan=$("#idruangan").val();
	let idkategori_dokter=$("#idkategori_dokter").val();
	let iddokter=$("#iddokter").val();
	let idkelas=$("#idkelas").val();
	let idtipe=$("#idtipe").val();
	$.ajax({
		url: '{site_url}tpoliklinik_trx/get_tarif_visite/',
		dataType: "json",
		type: 'POST',
		data: {
			idkelompokpasien : idkelompokpasien,
			idrekanan : idrekanan,
			idruangan : idruangan,
			idkategori_dokter : idkategori_dokter,
			iddokter : iddokter,
			idkelas : idkelas,
			idtipe : idtipe,
		  },
		success: function(data) {
			// alert(data);
			if (data!=null){
				$("#idpelayanan").val(data.idtarif).trigger('change.select2');
				$("#jasasarana").val(data.jasasarana);
				$("#jasapelayanan").val(data.jasapelayanan);
				$("#bhp").val(data.bhp);
				$("#biayaperawatan").val(data.biayaperawatan);
				$("#total").val(data.total);
				$("#diskon").val(data.diskon_persen);
				$("#diskonRp").val(data.diskon_rp);
				hitung_total();
				
			}else{
				$("#idpelayanan").val('0').trigger('change.select2');
				$("#jasasarana").val(0);
				$("#jasapelayanan").val(0);
				$("#bhp").val(0);
				$("#biayaperawatan").val(0);
				$("#total").val(0);
				$("#diskon").val(0);
				$("#diskonRp").val(0);
				hitung_total();
				
			}
			// $("#idpelayanan").append(data);
			
			// $("#idpelayanan_paket").empty();
			// $("#idpelayanan_paket").append(data);
		}
	});
	
}

function get_layanan_detail(){
		let idkelompokpasien=$("#idkelompokpasien").val();
		let idrekanan=$("#idrekanan").val();
		let idruangan=$("#idruangan").val();
		let idkategori_dokter=$("#idkategori_dokter").val();
		let iddokter=$("#iddokter").val();
		let idkelas=$("#idkelas").val();
	$("#st_browse").val("0");
	if ($("#idpelayanan").val() != '') {
		$.ajax({
			url: '{site_url}tpoliklinik_trx/get_layanan_visit_detail/',
			dataType: "json",
			type: 'POST',
			data: {
				idkelompokpasien : idkelompokpasien,
				idrekanan : idrekanan,
				idruangan : idruangan,
				idkategori_dokter : idkategori_dokter,
				iddokter : iddokter,
				idkelas : idkelas,
				
			},
			success: function(data) {
				if (data == null) {
					sweetAlert("Maaf...", "Data Layanan Tidak ditemukan!", "error");
					return false;
				}
				$("#jasasarana").val(data.jasasarana);
				$("#jasapelayanan").val(data.jasapelayanan);
				$("#bhp").val(data.bhp);
				$("#diskon").val(data.diskon_persen);
				
				if (data.st_diskon=='1'){
					$(".diskon").attr('disabled',true);
				}else{
					$(".diskon").removeAttr('disabled');
				}
				
				$("#diskonRp").val(data.diskon_rp);
				$("#diskon").val(data.diskon_persen);
				$("#biayaperawatan").val(data.biayaperawatan);
				$("#total").val(data.total);
				
				if (parseFloat($("#kuantitas").val())==0){
					$("#kuantitas").val('1');
					
				}
				
				hitung_total();
				$("#kuantitas").focus().select();
				
			}
		});
	} else {

	}
}
$("#idpelayanan").change(function() {
	// get_layanan_detail();
});
$("#idruangan,#idkelas,#iddokter").change(function() {
	select2_layanan();
});

$("#kuantitas").keyup(function() {
	hitung_total();
});

function hitung_total(){
	
	let jasasarana=parseFloat($("#jasasarana").val());
	let jasapelayanan=parseFloat($("#jasapelayanan").val());
	let bhp=parseFloat($("#bhp").val());
	let biayaperawatan=parseFloat($("#biayaperawatan").val());
	let diskon=parseFloat($("#diskonRp").val());
	let total=jasasarana+jasapelayanan+bhp+biayaperawatan-diskon;
	console.log(total);
	let kuantitas=parseFloat($("#kuantitas").val());
	let totalkeseluruhan=total*kuantitas;
	$("#totalkeseluruhan").val(totalkeseluruhan);
	$("#total").val(total);
}

function show_modal_layanan(){
	$("#form_akses").val('non_paket');
	document.getElementById("modal_layanan").style.zIndex = "1201";
	$('#rowpath_filter,#sub_parent').select2({
	   dropdownParent: $('#modal_layanan')
	});
	LoadLayanan();
	$("#modal_layanan").modal('show');
	$("#cover-spin").hide();
}
function show_modal_layanan_paket(){
	$("#form_akses").val('paket');
	document.getElementById("modal_layanan").style.zIndex = "1201";
	$('#rowpath_filter,#sub_parent').select2({
	   dropdownParent: $('#modal_layanan')
	});
	LoadLayanan();
	$("#modal_layanan").modal('show');
	$("#cover-spin").hide();
}
function clear_layanan(){
	$("#idpelayanan").val("").trigger('change.select2');
	$("#iddokter").val($("#iddokter_asal").val()).trigger('change.select2');
	$("#jasasarana").val(0);
	$("#jasapelayanan").val(0);
	$("#diskon").val(0);
	$("#diskonRp").val(0);
	$("#bhp").val(0);
	$("#kuantitas").val(1);
	$("#biayaperawatan").val(0);
	$("#total").val('');
	$("#idpelayanan").select().focus();
	$("#totalkeseluruhan").val('');
	$("#transaksi_id").val('');
	
	$("#div_info").hide();
	$(".diskon").removeAttr('disabled');
	select2_layanan();
}

function edit_layanan(id){
	$("#transaksi_id").val(id);
	// get_edit_layanan
	$("cover-spin").show();
	$.ajax({
	  url: '{site_url}tpoliklinik_trx/get_edit_layanan_visite/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			id : id,
			
	  },success: function(data) {
			$("#tgltransaksi").val(data.tgltransaksi),
			
			// $("#idtipe").val(data.idtipe).trigger('change.select2');
			// var newOption = new Option(data.nama_tindakan, data.idobat, true, true);
					
			// $("#idpelayanan").append(newOption);
			// kt.val(null).trigger('change');

			$("#iddokter").val(data.iddokter).trigger('change.select2');
			$("#idruangan").val(data.idruangan).trigger('change.select2');
			$("#idkelas").val(data.idkelas).trigger('change.select2');
			$("#idpelayanan").val(data.idpelayanan).trigger('change.select2');
			$("#jasasarana").val(data.jasasarana);
			$("#jasapelayanan").val(data.jasapelayanan);
			$("#diskonRp").val(data.jasapelayanan_disc);
			$("#bhp").val(data.bhp);
			$("#biayaperawatan").val(data.biayaperawatan);
			$("#total").val(data.total);
			$("#kuantitas").val(data.kuantitas);
			$("#totalkeseluruhan").val(data.totalkeseluruhan);
			
			$("cover-spin").hide();
			$("#kuantitas").focus().select();
			hitung_total();
			sleep(500).then(() => {
			$("#mppa_id_pelaksana").val(data.mppa_id).trigger('change');
				
			});
		}
	});
}
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
function cek_duplicate_tindakan(){
	let status_find=false;
	$('#index_transaksi tbody tr').each(function() {
		let idpelayanan=$(this).find(".cls_idpelayanan").val();
		let iddokter=$(this).find(".cls_iddokter").val();
		let tanggal=$(this).find(".cls_tanggal").val();
		let idruangan=$(this).find(".cls_idruangan").val();
		let transaksi_id=$(this).find(".cls_idtrx").val();
		console.log(tanggal+ ' - '+$("#tgltransaksi").val());
		console.log(idruangan+ ' - '+$("#idruangan").val());
		console.log(iddokter+ ' - '+$("#iddokter").val());
			if ($("#iddokter").val()==iddokter && $("#tgltransaksi").val()==tanggal && $("#idruangan").val()==idruangan){
				status_find=true;
			}
		// 
	});
	console.log(status_find);
	return status_find;
}
function add_tindakan(){
	
	
	if ($("#idpelayanan").val()==null || $("#idpelayanan").val()=='' || $("#idpelayanan").val()=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan Tentukan Nama Tarif",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (parseFloat($("#kuantitas").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#iddokter").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Dokter",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	if (parseFloat($("#totalkeseluruhan").val())=='0' && parseFloat($("#diskonRp").val())=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Harga dan Kuantitas",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#st_duplikate_visit").val()=='0'){
		let nama_dokter=$("#iddokter option:selected").text();
		if (cek_duplicate_tindakan()==true){
			swal({
				title: "Gagal",
				text: "Tanggal Tersebut telah diinputkan",
				type: "error",
				timer: 1500,
				showConfirmButton: false
			});
			return false;
		}else{
			simpan_tindakan();
		}
	}else{
		let nama_dokter=$("#iddokter option:selected").text();
		let hasil_duplicaate=cek_duplicate_tindakan();
		if (hasil_duplicaate==true){
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Menambahakan Data Duplicate "+ nama_dokter +" ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				simpan_tindakan();
			});

		}else{
			simpan_tindakan();
		}
	}
	
	// alert(iddokter);
	
}
function simpan_tindakan(){
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}tpoliklinik_trx/simpan_layanan_visite/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			transaksi_id : $("#transaksi_id").val(),
			tgltransaksi : $("#tgltransaksi").val(),
			pendaftaran_id : $("#pendaftaran_id_ranap").val(),
			idruangan : $("#idruangan").val(),
			idpasien : $("#idpasien").val(),
			idkelas : $("#idkelas").val(),
			iddokter : $("#iddokter").val(),
			idpelayanan : $("#idpelayanan").val(),
			jasasarana : $("#jasasarana").val(),
			jasasarana_disc : 0,
			jasapelayanan : $("#jasapelayanan").val(),
			jasapelayanan_disc : $("#diskonRp").val(),
			bhp : $("#bhp").val(),
			bhp_disc : 0,
			biayaperawatan : $("#biayaperawatan").val(),
			biayaperawatan_disc : 0,
			total : $("#total").val(),
			kuantitas : $("#kuantitas").val(),
			diskon : 0,
			totalkeseluruhan : $("#totalkeseluruhan").val(),
			idtipepasien : $("#idtipepasien").val(),
			idtipe : $("#idtipe").val(),
	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		  $("#cover-spin").hide();
		  // clear_layanan();
		  load_transaksi_layanan_list();
		}
	});
}
function load_transaksi_layanan_list(){
	$("#cover-spin").show();
	let pendaftaran_id=$("#pendaftaran_id_ranap").val();
		$('#index_transaksi tbody').empty();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/load_transaksi_layanan_visite_list', 
			dataType: "json",
			type: "POST",
			data: {
					pendaftaran_id: pendaftaran_id,
			},
			success: function(data) {
				$('#index_transaksi tbody').append(data.tabel);
				$("#total_penjualan").text(data.total_penjualan);
				$("#total_kuantitas").text(data.total_kuantitas);
			
				$("#cover-spin").hide();
				
			}
		});
}
function hapus_layanan(id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Data Tindakan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/hapus_layanan_visite', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				load_transaksi_layanan_list();
			}
		});
	});

}

function LoadLayanan() {
	$("#cover-spin").hide();
	var idtipe = $("#idtipe_poli").val();
	var idkelompokpasien = $("#idkelompokpasien").val();
	var idrekanan = $("#idrekanan").val();
	// alert('ID TIPE :'+idtipe+' KEL '+idkelompokpasien+' REK '+idrekanan);
	// return false;
	$('#tabel_layanan_filter').DataTable().destroy();
	var table = $('#tabel_layanan_filter').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpoliklinik_trx/getTindakanRawatJalan/',
			type: "POST",
			dataType: 'json',
			data: {
				idtipe: idtipe,
				idkelompokpasien: idkelompokpasien,
				idrekanan: idrekanan,
				form_akses: $("#form_akses").val(),
				rowpath: $("#rowpath").val(),
				sub_parent: $("#sub_parent").val(),
				nama_tarif_filter: $("#nama_tarif_filter").val(),
				
			}
		},
		columnDefs: [{
				"width": "5%",
				"targets": [0],
				"orderable": true
			},
			{
				"width": "10%",
				"targets": [2,3,4,5],
				"orderable": true
			},
			{
				"width": "15%",
				"targets": [6],
				"orderable": true
			},
			{
				"width": "40%",
				"targets": [1],
				"orderable": true
			},
			{  className: "text-right", targets:[2,3,4,5,6,0] },
		]
	});
}


$("#idtipe_filter").change(function() {
	$("#idtipe").val($("#idtipe_filter").val()).trigger('change');
	load_kategori();
});
function load_kategori() {
	// var idtipe = $("#idtipe_filter").val();
	// $('#idkat_filter').find('option').remove();
	// $('#idkat_filter').append('<option value="#" selected>- Pilih Semua -</option>');
	// if (idtipe != '#') {

		// $.ajax({
			// url: '{site_url}tstockopname/list_kategori/' + idtipe,
			// dataType: "json",
			// success: function(data) {
				// $.each(data, function(i, row) {

					// $('#idkat_filter')
						// .append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
				// });
			// }
		// });

	// }
}
function TreeView($level, $name) {
	// console.log($name);
	$indent = '';
	for ($i = 0; $i < $level; $i++) {
		if ($i > 0) {
			$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		} else {
			$indent += '';
		}
	}

	if ($i > 0) {
		$indent += '└─';
	} else {
		$indent += '';
	}

	return $indent + ' ' + $name;
}
function TreeView_select2($level, $name) {
	// console.log($name);
	$indent = '';
	for ($i = 0; $i < $level; $i++) {
		if ($i > 0) {
			$indent += '  ';
		} else {
			$indent += '';
		}
	}

	if ($i > 0) {
		$indent += '└─';
	} else {
		$indent += '';
	}

	return $indent + ' ' + $name;
}
$(document).on("click", ".selectLayanan", function() {
	clear_layanan();
	$("#st_browse").val("1");
	// alert($(this).data('nama'));
	let idpelayanan = ($(this).data('idpelayanan'));
	let nama = ($(this).data('nama'));
	// let newOption = new Option(idobat, nama, true, true);
	
	let newOption = new Option(nama, idpelayanan, true, true);
					
	$("#idpelayanan").append(newOption).trigger('change');
	$("#modal_layanan").modal('hide');

});
function list_history_layanan(){
	$("#div_history").hide();
	$("#div_filter").show();
	let pendaftaran_id=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let iddokter=$("#iddokter").val();
	let idpoli=$("#idpoli").val();
	let notransaksi=$("#notransaksi").val();
	// let tanggal_input_1=$("#tanggal_input_1").val();
	// let tanggal_input_2=$("#tanggal_input_2").val();
	$('#index_history_layanan').DataTable().destroy();	
	$("#cover-spin").show();
	table = $('#index_history_layanan').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_trx/list_history_layanan_visite', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						// tanggal_input_1:tanggal_input_1,
						// tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function lihat_data_layanan(pendaftaran_id){
	$("#cover-spin").show();
	$("#div_filter").hide();
	$("#div_history").show();
	$('#index_transaksi_history tbody').empty();
	$.ajax({
			url: '{site_url}tpoliklinik_trx/lihat_data_layanan_visite/',
			dataType: "json",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
				   },
			success: function(data) {
				$('#index_transaksi_history tbody').append(data.tabel);
				$("#total_penjualan_history").text(data.total_penjualan);
				$("#total_kuantitas_history").text(data.total_kuantitas);
				$("#cover-spin").hide();
			}
		});
	scrollToBottom();
	// window.scrollTo(0, document.body.scrollHeight);
}
function back_history(){
	$("#div_filter").show();
	$("#div_history").hide();
}
function back_history_paket(){
	$(".div_filter_paket").show();
	$(".div_history_paket").hide();
}
function scrollToBottom() {
  window.scrollTo(0, document.body.scrollHeight);
}
function copy_layanan(id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Menerapkan Data Tindakan ke Transaksi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_trx/copy_layanan', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					pendaftaran_id : $("#pendaftaran_id").val(),
					idpasien : $("#idpasien").val(),
					// profesi_id : $("#mppa_id_pelaksana_copy").val(),
					iddokter : $("#iddokter_ppa").val(),
					mppa_id : $("#mppa_id_pelaksana").val(),
					profesi_id : $("#profesi_id").val(),
					// pertemuan_id : $("#pertemuan_id").val(),
					// statuspasienbaru : $("#statuspasienbaru").val(),
					// idtipe_poli : $("#idtipe_poli").val(),
					// idkelompokpasien : $("#idkelompokpasien").val(),
					// idrekanan : $("#idrekanan").val(),
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==true){
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				load_transaksi_layanan_list();
					  
				  }else{
					  swal({
						title: "Gagal",
						text: "Data Tidak ada yang berhasil di insert",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
				  }
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Duplicate Data'});
			}
		});
	});
	

}


function LoadLayananCopy() {
	var rowpath = $("#rowpath").val();
	var mppa_id_pelaksana_copy = $("#mppa_id_pelaksana_copy").val();
	var array_paket_id = $("#array_paket_id").val();
	$('#tabel_layanan_paket_copy tbody').empty();
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}tpoliklinik_trx/LoadLayananCopy/',
			dataType: "json",
			method: "POST",
			data : {
					mppa_id_pelaksana_copy: mppa_id_pelaksana_copy,
					array_paket_id: array_paket_id,
					rowpath: rowpath,
				   },
			success: function(data) {
				$('#tabel_layanan_paket_copy tbody').append(data.tabel);
				$("#total_penjualan_history").text(data.total_penjualan);
				$("#total_kuantitas_history").text(data.total_kuantitas);
				$('.mppa_id').select2({
				   dropdownParent: $('#modal_layanan_copy')
				});
				$("#cover-spin").hide();
			}
		});
	// scrollToBottom();
	// window.scrollTo(0, document.body.scrollHeight);
}
function terapkan_copy(){
	let arr_idpelayanan=[];
	let arr_idtrx=[];
	let arr_kuantitas=[];
	let arr_mppa_id=[];
	$('#tabel_layanan_paket_copy tbody tr').each(function() {
		let mppa_id=$(this).find(".mppa_id").val();
		let idpelayanan=$(this).find(".cls_idpelayanan").val();
		let idtrx=$(this).find(".cls_idtrx").val();
		let kuantitas=$(this).find(".cls_kuantitas").val();
		arr_idpelayanan.push(idpelayanan);
		arr_idtrx.push(idtrx);
		arr_kuantitas.push(kuantitas);
		arr_mppa_id.push(mppa_id);
	});
	console.log(arr_mppa_id);
	console.log(arr_idpelayanan);
	$("#modal_layanan_copy").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerapkan Data Layanan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
		  url: '{site_url}tpoliklinik_trx/terapkan_copy_paket/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				iddokter : $("#iddokter").val(),
				pendaftaran_id : $("#pendaftaran_id").val(),
				idpasien : $("#idpasien").val(),
				// idpoliklinik : $("#idpoliklinik").val(),
				// pertemuan_id : $("#pertemuan_id").val(),
				statuspasienbaru : $("#statuspasienbaru").val(),
				idtipe_poli : $("#idtipe_poli").val(),
				idkelompokpasien : $("#idkelompokpasien").val(),
				idrekanan : $("#idrekanan").val(),
				arr_idpelayanan : arr_idpelayanan,
				arr_idtrx : arr_idtrx,
				arr_kuantitas : arr_kuantitas,
				arr_mppa_id : arr_mppa_id,
		  },success: function(data) {
			  $("#cover-spin").hide();
			  if (data==true){
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  
			  }else{
				  swal({
					title: "Gagal",
					text: "Barang Tidak ada yang berhasil di insert",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			  }
			 $("#modal_layanan_copy").modal('hide');
			 load_transaksi_layanan_list();
			}
		});
	});
	
	
}
</script>