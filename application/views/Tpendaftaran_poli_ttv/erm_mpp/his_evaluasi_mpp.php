<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffa8a8;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.border-bottom{
        border-bottom:1px solid #e9e9e9 !important;
		
      }
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	 <?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='his_evaluasi_mpp'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_evaluasi_mpp' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						// $tanggal_masuk_rs=HumanDateLong($tanggal_masuk_rs);
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$tanggaldaftar_asal=HumanDateShort($tanggal_input_asal);
						$waktudaftar=HumanTime($tanggal_input);
						$waktudaftar_asal=HumanTime($tanggal_input_asal);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_evaluasi_mpp=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_mpp">
	<?if ($st_lihat_evaluasi_mpp=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
						<div class="col-md-12">
							<?if($st_lihat_evaluasi_mpp=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="pull-right push-10-r">
								<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
							</div>
							
						</div>
					</div>
					<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control <?=($waktudaftar!=$waktudaftar_asal?'edited':'')?>" id="waktudaftar" name="waktudaftar" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group" style="margin-top:10px;!important">
							
							<div class="col-md-12">
								<div class="col-md-12">
									<label for=""><?=$catatan_ina?></label>
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >IDENTIFIKASI SKRINING</h5>
							</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-20px;!important">
							
							<div class="col-md-12">
								<div class="col-md-12">
									<label for="indentifikasi_skrining">Identifikasi / Skrining pasien terdapat Jawaban "<strong>YA</strong>"</label>
									<textarea class="form-control <?=($indentifikasi_skrining!=$indentifikasi_skrining_asal?'edited':'')?>" id="indentifikasi_skrining"  rows="3" placeholder="Identifikasi / Skrining pasien"><?=$indentifikasi_skrining?></textarea>
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >ASSESMEN MELIPUTI</h5>
							</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-20px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="fisik">Fisik, Fungsional, Kekuatan / Kemampuan / Kemandirian</label>
									<select id="fisik" name="fisik" class="<?=($fisik!=$fisik_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($fisik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(302) as $row){?>
										<option value="<?=$row->id?>"  <?=($fisik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($fisik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="riwayat_kesehatan">Riwayat Kesehatan</label>
									<input id="riwayat_kesehatan" class="<?=($riwayat_kesehatan!=$riwayat_kesehatan_asal?'edited':'')?> form-control "  type="text"  value="{riwayat_kesehatan}"  placeholder="Riwayat Kesehatan" required>
								</div>
								<div class="col-md-3">
									<label for="perilaku">Prilaku Psiko-sosio-Kultural</label>
									<input id="perilaku" class="<?=($perilaku!=$perilaku_asal?'edited':'')?> form-control "  type="text"  value="{perilaku}"  placeholder="Prilaku Psiko-sosio-Kultural" required>
								</div>
								<div class="col-md-3">
									<label for="kesehatan_mental">Kesehatan Mental Kognitif</label>
									<input id="kesehatan_mental" class="<?=($kesehatan_mental!=$kesehatan_mental_asal?'edited':'')?> form-control "  type="text"  value="{kesehatan_mental}"  placeholder="Kesehatan Mental Kognitif" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="lingkungan">Lingkungan Tempat Tinggal</label>
									<input id="lingkungan" class="<?=($lingkungan!=$lingkungan_asal?'edited':'')?> form-control "  type="text"  value="{lingkungan}"  placeholder="Lingkungan Tempat Tinggal" required>
								</div>
								<div class="col-md-3">
									<label for="dukungan_kel">Dukungan Keluarga, Kemampuan merawat dari pemberi asuhan</label>
									<select id="dukungan_kel" name="dukungan_kel" class="<?=($dukungan_kel!=$dukungan_kel_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($dukungan_kel == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(303) as $row){?>
										<option value="<?=$row->id?>"  <?=($dukungan_kel == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($dukungan_kel == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
								<div class="col-md-3">
									<label for="finansial">Finansial</label>
									<select id="finansial" name="finansial" class="<?=($finansial!=$finansial_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($finansial == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(304) as $row){?>
										<option value="<?=$row->id?>"  <?=($finansial == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($finansial == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="finansial_ket">Keterangan Finansial</label>
									<input id="finansial_ket" class="<?=($finansial_ket!=$finansial_ket_asal?'edited':'')?> form-control "  type="text"  value="{finansial_ket}"  placeholder="Keterangan Finansial" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="obat_alternative">Riwayat Penggunaan Obat Alternative</label>
									<select id="obat_alternative" name="obat_alternative" class="<?=($obat_alternative!=$obat_alternative_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($obat_alternative == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(305) as $row){?>
										<option value="<?=$row->id?>"  <?=($obat_alternative == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($obat_alternative == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="obat_alternative_lain">Riwayat Lainnya Penggunaan Obat Alternative</label>
									<input id="obat_alternative_lain" class="<?=($obat_alternative_lain!=$obat_alternative_lain_asal?'edited':'')?> form-control "  type="text"  value="{obat_alternative_lain}"  placeholder="Riwayat Lainnya Penggunaan Obat Alternative" required>
								</div>
								<div class="col-md-3">
									<label for="trauma">Riwayat Trauma / Kekerasan</label>
									<select id="trauma" name="trauma" class="<?=($trauma!=$trauma_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($trauma == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(306) as $row){?>
										<option value="<?=$row->id?>"  <?=($trauma == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($trauma == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="pemahanan">Pemahaman Tentang Kesehatan</label>
									<select id="pemahanan" name="pemahanan" class="<?=($pemahanan!=$pemahanan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pemahanan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(307) as $row){?>
										<option value="<?=$row->id?>"  <?=($pemahanan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemahanan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="harapan">Harapan Terhadap hasil asuhan, Kemampuan menerima perubahan</label>
									<select id="harapan" name="harapan" class="<?=($harapan!=$harapan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($harapan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(308) as $row){?>
										<option value="<?=$row->id?>"  <?=($harapan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($harapan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="perkiraan_rawat"><br>Perkiraan Hari Rawat Inap</label>
									<div class="input-group">
										<input id="perkiraan_rawat" class="<?=($perkiraan_rawat!=$perkiraan_rawat_asal?'edited':'')?> form-control " onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text"  value="{perkiraan_rawat}"  placeholder="Perkiraan Hari Rawat Inap" required>
										<span class="input-group-addon">Hari</span>
								</div>
								</div>
								<div class="col-md-3">
									<label for="discharge_planing"><br>Discharge Planning</label>
									<select id="discharge_planing" name="discharge_planing" class="<?=($discharge_planing!=$discharge_planing_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($discharge_planing == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(309) as $row){?>
										<option value="<?=$row->id?>"  <?=($discharge_planing == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($discharge_planing == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="discharge_planing_lain"><br>Keterangan lainnya Discharge Planning </label>
									<input id="discharge_planing_lain" class="<?=($discharge_planing_lain!=$discharge_planing_lain_asal?'edited':'')?> form-control "  type="text"  value="{discharge_planing_lain}"  placeholder="Keterangan lainnya Discharge Planning " required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="perencanaan_lanjutan">Perencanaan Lanjutan</label>
									<select id="perencanaan_lanjutan" name="perencanaan_lanjutan" class="<?=($perencanaan_lanjutan!=$perencanaan_lanjutan_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($perencanaan_lanjutan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(310) as $row){?>
										<option value="<?=$row->id?>"  <?=($perencanaan_lanjutan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($perencanaan_lanjutan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
								<div class="col-md-3">
									<label for="aspek_legal">Aspek Legal</label>
									<select id="aspek_legal" name="aspek_legal" class="<?=($aspek_legal!=$aspek_legal_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($aspek_legal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(311) as $row){?>
										<option value="<?=$row->id?>"  <?=($aspek_legal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($aspek_legal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="discharge_planing_lain">Sebutkan Aspek Legal Lainnya </label>
									<input id="discharge_planing_lain" class="<?=($discharge_planing_lain!=$discharge_planing_lain_asal?'edited':'')?> form-control "  type="text"  value="{discharge_planing_lain}"  placeholder="Sebutkan Aspek Legal Lainnya " required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >IDENTIFIKASI MASALAH DAN KESEMPATAN</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$kesempatan);
						?>
						<div class="form-group" style="margin-top:-20px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-12">
									<label for="kesempatan" class="<?=($kesempatan!=$kesempatan_asal?'edited2':'')?>">Identifikasi Masalah dan Kesempatan</label>
									<select id="kesempatan" name="kesempatan" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(312) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >PERENCANAAN MPP</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$perencanaan_mpp);
						?>
						<div class="form-group" style="margin-top:-20px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-12">
									<label for="perencanaan_mpp" class="<?=($perencanaan_mpp!=$perencanaan_mpp_asal?'edited2':'')?>">Identifikasi Masalah dan perencanaan_mpp</label>
									<select id="perencanaan_mpp" name="perencanaan_mpp" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(313) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="jangka_pendek">Jangka Pendek </label>
									<input id="jangka_pendek" class="<?=($jangka_pendek!=$jangka_pendek_asal?'edited':'')?> form-control "  type="text"  value="{jangka_pendek}"  placeholder="Jangka Pendek " required>
								</div>
								<div class="col-md-3">
									<label for="jangka_panjang">Jangka Panjang </label>
									<input id="jangka_panjang" class="<?=($jangka_panjang!=$jangka_panjang_asal?'edited':'')?> form-control "  type="text"  value="{jangka_panjang}"  placeholder="Jangka Panjang " required>
								</div>
								<div class="col-md-3">
									<label for="kebutuhan_perjalanan">Kebutuhan Perjalanan </label>
									<input id="kebutuhan_perjalanan" class="<?=($kebutuhan_perjalanan!=$kebutuhan_perjalanan_asal?'edited':'')?> form-control "  type="text"  value="{kebutuhan_perjalanan}"  placeholder="Kebutuhan Perjalanan " required>
								</div>
								<div class="col-md-3">
									<label for="lain_lain">Lain-Lain</label>
									<input id="lain_lain" class="<?=($lain_lain!=$lain_lain_asal?'edited':'')?> form-control "  type="text"  value="{lain_lain}"  placeholder="Lain-Lain" required>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:5px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >RENCANA PEMERIKSAAN PENUNJANG</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$rencana_penunjang);
						?>
						<div class="form-group" style="margin-top:-25px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="rencana_penunjang" class="<?=($rencana_penunjang!=$rencana_penunjang_asal?'edited2':'')?>">Rencana Pemeriksaan Penunjang</label>
									<select id="rencana_penunjang" name="rencana_penunjang" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(314) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="rencana_penunjang_lain">Rencana Pemeriksaan Penunjang Lainnya</label>
									<input id="rencana_penunjang_lain" class="<?=($rencana_penunjang_lain!=$rencana_penunjang_lain_asal?'edited':'')?> form-control "  type="text"  value="{rencana_penunjang_lain}"  placeholder="Rencana Pemeriksaan Penunjang Lainnya" required>
								</div>
							</div>
							
						</div>	

						<div class="form-group" style="margin-top:5px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >TEMPAT PERAWATAN</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$tempat_perawatan);
						?>
						<div class="form-group" style="margin-top:-25px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="tempat_perawatan" class="<?=($tempat_perawatan!=$tempat_perawatan_asal?'edited2':'')?>">Tempat Perawatan</label>
									<select id="tempat_perawatan" name="tempat_perawatan" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(315) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tempat_perawatan_lain">Tempat Perawatan Lainnya</label>
									<input id="tempat_perawatan_lain" class="<?=($tempat_perawatan_lain!=$tempat_perawatan_lain_asal?'edited':'')?> form-control "  type="text"  value="{tempat_perawatan_lain}"  placeholder="Tempat Perawatan Lainnya" required>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:5px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >REVISI SASARAN</h5>
							</div>
							</div>
						</div>			
						<div class="form-group" style="margin-top:-25px;!important">			
							<div class="col-md-12">
								<div class="col-md-12">
									<label for="revisi">Revisi Sasaran Jangka Panjang / Jangka Pendek </label>
									<textarea class="form-control <?=($revisi!=$revisi_asal?'edited':'')?>" id="revisi"  rows="3" placeholder="Revisi Sasaran Jangka Panjang / Jangka Pendek"><?=$revisi?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-6 ">
									<table width="100%">
										<tr>
											<td width="33%" class="text-center text-bold"><strong>YANG MENGKAJI</strong></td>
											<td width="33%"></td>
											<td width="33%"></td>
										</tr>
										<tr>
											<td width="33%" class="text-center"><img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$created_ppa?>" alt="" title=""></td>
											<td width="33%"></td>
											<td width="33%"></td>
										</tr>
										<tr>
											<td width="33%" class="text-center"><strong><?=get_nama_ppa($created_ppa)?></strong></td>
											<td width="33%"></td>
											<td width="33%"></td>
										</tr>
									</table>
								
								</div>
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	
	$("#tanggal_masuk_rs").datetimepicker({
            format: "DD-MM-YYYY HH:mm",
            // stepping: 30
        });
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		list_index_history_edit();
		load_awal_assesmen=false;
	}
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	let st_ranap=$("#st_ranap").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_evaluasi_mpp', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						st_ranap:st_ranap,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}


function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#gambar_id").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

</script>