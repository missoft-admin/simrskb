<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	.input-keterangan{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.border-bottom{
        border-bottom:1px solid #e9e9e9 !important;
		
      }
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_evaluasi_mpp'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_evaluasi_mpp' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						// $tanggal_masuk_rs=HumanDateLong($tanggal_masuk_rs);
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_evaluasi_mpp=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_mpp">
	<?if ($st_lihat_evaluasi_mpp=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian  <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'Baru')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
						<input type="hidden" id="idtipe" value="<?=$idtipe?>" >		
						<div class="col-md-12">
							<?if($st_lihat_evaluasi_mpp=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
						
					</div>
					<div class="row">
						
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_evaluasi_mpp=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-disk"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-save"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpoliklinik_ranap/tindakan/<?=$pendaftaran_id_ranap?>/erm_mpp/input_evaluasi_mpp" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
									<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_evaluasi_mpp!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group" style="margin-top:10px;!important">
							
							<div class="col-md-12">
								<div class="col-md-12">
									<label for=""><?=$catatan_ina?></label>
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >IDENTIFIKASI SKRINING</h5>
							</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-20px;!important">
							
							<div class="col-md-12">
								<div class="col-md-12">
									<label for="indentifikasi_skrining">Identifikasi / Skrining pasien terdapat Jawaban "<strong>YA</strong>"</label>
									<textarea class="form-control auto_blur" id="indentifikasi_skrining"  rows="3" placeholder="Identifikasi / Skrining pasien"><?=$indentifikasi_skrining?></textarea>
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >ASSESMEN MELIPUTI</h5>
							</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-top:-20px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="fisik">Fisik, Fungsional, Kekuatan / Kemampuan / Kemandirian</label>
									<select id="fisik" name="fisik" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($fisik == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(302) as $row){?>
										<option value="<?=$row->id?>"  <?=($fisik == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($fisik == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="riwayat_kesehatan"><br>Riwayat Kesehatan</label>
									<input id="riwayat_kesehatan" class="auto_blur form-control "  type="text"  value="{riwayat_kesehatan}"  placeholder="Riwayat Kesehatan" required>
								</div>
								<div class="col-md-3">
									<label for="perilaku"><br>Prilaku Psiko-sosio-Kultural</label>
									<input id="perilaku" class="auto_blur form-control "  type="text"  value="{perilaku}"  placeholder="Prilaku Psiko-sosio-Kultural" required>
								</div>
								<div class="col-md-3">
									<label for="kesehatan_mental"><br>Kesehatan Mental Kognitif</label>
									<input id="kesehatan_mental" class="auto_blur form-control "  type="text"  value="{kesehatan_mental}"  placeholder="Kesehatan Mental Kognitif" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="lingkungan"><br>Lingkungan Tempat Tinggal</label>
									<input id="lingkungan" class="auto_blur form-control "  type="text"  value="{lingkungan}"  placeholder="Lingkungan Tempat Tinggal" required>
								</div>
								<div class="col-md-3">
									<label for="dukungan_kel">Dukungan Keluarga, Kemampuan merawat dari pemberi asuhan</label>
									<select id="dukungan_kel" name="dukungan_kel" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($dukungan_kel == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(303) as $row){?>
										<option value="<?=$row->id?>"  <?=($dukungan_kel == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($dukungan_kel == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
								<div class="col-md-3">
									<label for="finansial"><br>Finansial</label>
									<select id="finansial" name="finansial" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($finansial == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(304) as $row){?>
										<option value="<?=$row->id?>"  <?=($finansial == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($finansial == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="finansial_ket"><br>Keterangan Finansial</label>
									<input id="finansial_ket" class="auto_blur form-control "  type="text"  value="{finansial_ket}"  placeholder="Keterangan Finansial" required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="obat_alternative">Riwayat Penggunaan Obat Alternative</label>
									<select id="obat_alternative" name="obat_alternative" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($obat_alternative == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(305) as $row){?>
										<option value="<?=$row->id?>"  <?=($obat_alternative == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($obat_alternative == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="obat_alternative_lain">Riwayat Lainnya Penggunaan Obat Alternative</label>
									<input id="obat_alternative_lain" class="auto_blur form-control "  type="text"  value="{obat_alternative_lain}"  placeholder="Riwayat Lainnya Penggunaan Obat Alternative" required>
								</div>
								<div class="col-md-3">
									<label for="trauma">Riwayat Trauma / Kekerasan</label>
									<select id="trauma" name="trauma" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($trauma == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(306) as $row){?>
										<option value="<?=$row->id?>"  <?=($trauma == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($trauma == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="pemahanan">Pemahaman Tentang Kesehatan</label>
									<select id="pemahanan" name="pemahanan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($pemahanan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(307) as $row){?>
										<option value="<?=$row->id?>"  <?=($pemahanan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pemahanan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="harapan">Harapan Terhadap hasil asuhan, Kemampuan menerima perubahan</label>
									<select id="harapan" name="harapan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($harapan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(308) as $row){?>
										<option value="<?=$row->id?>"  <?=($harapan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($harapan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="perkiraan_rawat"><br>Perkiraan Hari Rawat Inap</label>
									<div class="input-group">
										<input id="perkiraan_rawat" class="auto_blur form-control " onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text"  value="{perkiraan_rawat}"  placeholder="Perkiraan Hari Rawat Inap" required>
										<span class="input-group-addon">Hari</span>
								</div>
								</div>
								<div class="col-md-3">
									<label for="discharge_planing"><br>Discharge Planning</label>
									<select id="discharge_planing" name="discharge_planing" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($discharge_planing == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(309) as $row){?>
										<option value="<?=$row->id?>"  <?=($discharge_planing == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($discharge_planing == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="discharge_planing_lain"><br>Keterangan lainnya Discharge Planning </label>
									<input id="discharge_planing_lain" class="auto_blur form-control "  type="text"  value="{discharge_planing_lain}"  placeholder="Keterangan lainnya Discharge Planning " required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="perencanaan_lanjutan">Perencanaan Lanjutan</label>
									<select id="perencanaan_lanjutan" name="perencanaan_lanjutan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($perencanaan_lanjutan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(310) as $row){?>
										<option value="<?=$row->id?>"  <?=($perencanaan_lanjutan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($perencanaan_lanjutan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
								<div class="col-md-3">
									<label for="aspek_legal">Aspek Legal</label>
									<select id="aspek_legal" name="aspek_legal" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($aspek_legal == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(311) as $row){?>
										<option value="<?=$row->id?>"  <?=($aspek_legal == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($aspek_legal == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="discharge_planing_lain">Sebutkan Aspek Legal Lainnya </label>
									<input id="discharge_planing_lain" class="auto_blur form-control "  type="text"  value="{discharge_planing_lain}"  placeholder="Sebutkan Aspek Legal Lainnya " required>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >IDENTIFIKASI MASALAH DAN KESEMPATAN</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$kesempatan);
						?>
						<div class="form-group" style="margin-top:-20px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-12">
									<label for="kesempatan">Identifikasi Masalah dan Kesempatan</label>
									<select id="kesempatan" name="kesempatan" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(312) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >PERENCANAAN MPP</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$perencanaan_mpp);
						?>
						<div class="form-group" style="margin-top:-20px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-12">
									<label for="perencanaan_mpp">Identifikasi Masalah dan perencanaan_mpp</label>
									<select id="perencanaan_mpp" name="perencanaan_mpp" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(313) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:-10px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-3">
									<label for="jangka_pendek">Jangka Pendek </label>
									<input id="jangka_pendek" class="auto_blur form-control "  type="text"  value="{jangka_pendek}"  placeholder="Jangka Pendek " required>
								</div>
								<div class="col-md-3">
									<label for="jangka_panjang">Jangka Panjang </label>
									<input id="jangka_panjang" class="auto_blur form-control "  type="text"  value="{jangka_panjang}"  placeholder="Jangka Panjang " required>
								</div>
								<div class="col-md-3">
									<label for="kebutuhan_perjalanan">Kebutuhan Perjalanan </label>
									<input id="kebutuhan_perjalanan" class="auto_blur form-control "  type="text"  value="{kebutuhan_perjalanan}"  placeholder="Kebutuhan Perjalanan " required>
								</div>
								<div class="col-md-3">
									<label for="lain_lain">Lain-Lain</label>
									<input id="lain_lain" class="auto_blur form-control "  type="text"  value="{lain_lain}"  placeholder="Lain-Lain" required>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:5px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >RENCANA PEMERIKSAAN PENUNJANG</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$rencana_penunjang);
						?>
						<div class="form-group" style="margin-top:-25px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="rencana_penunjang">Rencana Pemeriksaan Penunjang</label>
									<select id="rencana_penunjang" name="rencana_penunjang" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(314) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="rencana_penunjang_lain">Rencana Pemeriksaan Penunjang Lainnya</label>
									<input id="rencana_penunjang_lain" class="auto_blur form-control "  type="text"  value="{rencana_penunjang_lain}"  placeholder="Rencana Pemeriksaan Penunjang Lainnya" required>
								</div>
							</div>
							
						</div>	

						<div class="form-group" style="margin-top:5px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >TEMPAT PERAWATAN</h5>
							</div>
							</div>
						</div>
						<?
							$arr=explode(',',$tempat_perawatan);
						?>
						<div class="form-group" style="margin-top:-25px;!important">
							<div class="col-md-12 " style="margin-top:10px;">
								<div class="col-md-6">
									<label for="tempat_perawatan">Tempat Perawatan</label>
									<select id="tempat_perawatan" name="tempat_perawatan" class="js-select2 form-control opsi_change" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(list_variable_ref(315) as $row){?>
										<option value="<?=$row->id?>"  <?=(in_array($row->id, $arr)?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="tempat_perawatan_lain">Tempat Perawatan Lainnya</label>
									<input id="tempat_perawatan_lain" class="auto_blur form-control "  type="text"  value="{tempat_perawatan_lain}"  placeholder="Tempat Perawatan Lainnya" required>
								</div>
							</div>
							
						</div>	
						<div class="form-group" style="margin-top:5px;!important">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" >REVISI SASARAN</h5>
							</div>
							</div>
						</div>			
						<div class="form-group" style="margin-top:-25px;!important">			
							<div class="col-md-12">
								<div class="col-md-12">
									<label for="revisi">Revisi Sasaran Jangka Panjang / Jangka Pendek </label>
									<textarea class="form-control auto_blur" id="revisi"  rows="3" placeholder="Revisi Sasaran Jangka Panjang / Jangka Pendek"><?=$revisi?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-6 ">
									<table width="100%">
										<tr>
											<td width="33%" class="text-center text-bold"><strong>YANG MENGKAJI</strong></td>
											<td width="33%"></td>
											<td width="33%"></td>
										</tr>
										<tr>
											<td width="33%" class="text-center"><img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$created_ppa?>" alt="" title=""></td>
											<td width="33%"></td>
											<td width="33%"></td>
										</tr>
										<tr>
											<td width="33%" class="text-center"><strong><?=get_nama_ppa($created_ppa)?></strong></td>
											<td width="33%"></td>
											<td width="33%"></td>
										</tr>
									</table>
								
								</div>
							</div>
						</div>


						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></i></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
							
								<div class="col-md-12 ">
									<div class="pull-right push-10-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_mpp=='1'){?>
										<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										
											<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<?if ($st_ranap=='1'){?>
													<option value="<?=$pendaftaran_id_ranap?>" selected>Dirawat Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}else{?>
													<option value="<?=$pendaftaran_id?>" selected>Poliklinik Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<input type="hidden" id="pendaftaran_id_ranap_tmp" value="">
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="jenis_ttd" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_ttd==''?'selected':'')?>>Silahkan Pilih</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Pasien Sendiri Sendiri</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?> >Keluarga Terdekat / Wali</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?> >Penanggung Jawab</option>
										
									</select>
									<label for="jenis_ttd">Yang Tandatangan</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							<div class="col-md-6 push-10">
								<div class="form-material">
									<select tabindex="30" id="ttd_hubungan" name="ttd_hubungan" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($ttd_hubungan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<label for="ttd_hubungan">Hubungan</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_ttd()" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	
	$("#tanggal_masuk_rs").datetimepicker({
            format: "DD-MM-YYYY HH:mm",
            // stepping: 30
        });
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		// load_catatan_evaluasi_mpp();
		load_awal_assesmen=false;
	}else{
		list_history_pengkajian();
	}
});

function load_catatan_evaluasi_mpp(){
	let assesmen_id=$("#assesmen_id").val();
	$("#tabel_catatan tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tpoliklinik_ranap/load_catatan_evaluasi_mpp', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				st_sedang_edit:$("#st_sedang_edit").val(),
				
				},
		success: function(data) {
			$("#tabel_catatan tbody").append(data.opsi);
			disabel_edit();
			$("#cover-spin").hide();
		}
	});
}

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#gambar_id").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	let idtipe=$("#idtipe").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/create_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
					st_ranap:st_ranap,
					idtipe:idtipe,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();

	let idtipe=$("#idtipe").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/create_with_template_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
					idtipe:idtipe,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	let idtipe=$("#idtipe").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/create_with_template_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
					idtipe:idtipe,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_catatan(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Catatan Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/copy_catata_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					assesmen_id:assesmen_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				load_catatan_evaluasi_mpp();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let st_ranap=$("#st_ranap").val();
	let idtipe=$("#idtipe").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/create_template_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
					st_ranap:st_ranap,
					idtipe:idtipe,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/batal_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/batal_template_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
// $(".input-default").blur(function(){
	// simpan_default_warna();	
// });
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
function simpan_assesmen(){
	// alert($("#keluhan_utama").val());
	// return false;
	if (load_awal_assesmen==false){
	if (status_assemen !='2'){
		
	
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		// if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tpoliklinik_ranap/save_evaluasi_mpp', 
				dataType: "JSON",
				method: "POST",
				data : {
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						
						indentifikasi_skrining : $("#indentifikasi_skrining").val(),
						fisik : $("#fisik").val(),
						riwayat_kesehatan : $("#riwayat_kesehatan").val(),
						perilaku : $("#perilaku").val(),
						kesehatan_mental : $("#kesehatan_mental").val(),
						lingkungan : $("#lingkungan").val(),
						dukungan_kel : $("#dukungan_kel").val(),
						finansial : $("#finansial").val(),
						finansial_ket : $("#finansial_ket").val(),
						obat_alternative : $("#obat_alternative").val(),
						obat_alternative_lain : $("#obat_alternative_lain").val(),
						trauma : $("#trauma").val(),
						pemahanan : $("#pemahanan").val(),
						harapan : $("#harapan").val(),
						perkiraan_rawat : $("#perkiraan_rawat").val(),
						discharge_planing : $("#discharge_planing").val(),
						discharge_planing_lain : $("#discharge_planing_lain").val(),
						perencanaan_lanjutan : $("#perencanaan_lanjutan").val(),
						aspek_legal : $("#aspek_legal").val(),
						aspek_legal_lain : $("#aspek_legal_lain").val(),
						kesempatan : $("#kesempatan").val(),
						perencanaan_mpp : $("#perencanaan_mpp").val(),
						jangka_pendek : $("#jangka_pendek").val(),
						jangka_panjang : $("#jangka_panjang").val(),
						kebutuhan_perjalanan : $("#kebutuhan_perjalanan").val(),
						lain_lain : $("#lain_lain").val(),
						rencana_penunjang : $("#rencana_penunjang").val(),
						rencana_penunjang_lain : $("#rencana_penunjang_lain").val(),
						tempat_perawatan : $("#tempat_perawatan").val(),
						tempat_perawatan_lain : $("#tempat_perawatan_lain").val(),
						revisi : $("#revisi").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpoliklinik_ranap/list_index_template_evaluasi_mpp', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpoliklinik_ranap/edit_template_evaluasi_mpp', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		let st_ranap=$("#st_ranap").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpoliklinik_ranap/list_history_pengkajian_evaluasi_mpp', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							st_ranap:st_ranap,
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function edit_history_assesmen(id,pendaftaran_id_ranap){
		$("#pendaftaran_id_ranap_tmp").val(pendaftaran_id_ranap);
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let pendaftaran_id_ranap_tmp=$("#pendaftaran_id_ranap_tmp").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpoliklinik_ranap/save_edit_evaluasi_mpp', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let st_ranap=data.st_ranap;
					if (st_ranap=='1'){
						let pendaftaranid=data.pendaftaran_id_ranap;
						window.location.href = "<?php echo site_url('tpoliklinik_ranap/tindakan/"+pendaftaranid+"/1/erm_mpp/input_evaluasi_mpp'); ?>";
						
					}else{
						let pendaftaranid=data.pendaftaran_id;
						window.location.href = "<?php echo site_url('tpoliklinik_ranap/tindakan/"+pendaftaranid+"/0/erm_mpp/input_evaluasi_mpp'); ?>";
					}
						// window.location.href = "<?php echo site_url('tpoliklinik_ranap/tindakan/"+pendaftaran_id_ranap_tmp+"/erm_mpp/input_evaluasi_mpp'); ?>";
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpoliklinik_ranap/hapus_record_evaluasi_mpp', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpoliklinik_ranap/hapus_record_evaluasi_mpp', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	
</script>