<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_perencanaan' ? 'block' : 'none') ?>">
	<div class="block animated fadeIn bg-gray" data-category="erm_perencanaan">
		<div class="push-5-t bg-gray-light">
			<?php 
				if ($asalrujukan=='3'){
					$url_utama_konsul=$base_url.'tpoliklinik_ranap/tindakan/'.$pendaftaran_id_ranap.'/1/';
					$st_ranap='1';
				}else{
					$st_ranap='0';
					$url_utama_konsul=$base_url.'tpoliklinik_ranap/tindakan/'.$pendaftaran_id.'/0/';
				}
				// $url_utama_konsul=$base_url.'tpoliklinik_ranap/tindakan/'.$pendaftaran_id.'/';
				$login_tipepegawai=$this->session->userdata('login_tipepegawai');
				$login_pegawai_id=$this->session->userdata('login_pegawai_id');
				
			?>
			<ul class="nav nav-pills nav-stacked push">
				<?if ($st_lihat_rencana_ranap=='1'){?>
				<li class="<?=($menu_kiri=='input_perencanaan_ranap' || $menu_kiri=='his_perencanaan_ranap' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_perencanaan/input_perencanaan_ranap"><i class="fa fa-caret-right push-5-r"></i> Rawat Inap / ODS</a></li>
				<?}?>
				<?if ($st_lihat_rencana_bedah=='1'){?>
				<li class="<?=($menu_kiri=='input_perencanaan_bedah' || $menu_kiri=='his_perencanaan_bedah' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_perencanaan/input_perencanaan_bedah"><i class="fa fa-caret-right push-5-r"></i> Penjadwalan Operasi</a></li>
				<?}?>
				<?if ($st_lihat_estimasi_biaya=='1'){?>
				<li class="<?=($menu_kiri=='input_estimasi_biaya' || $menu_kiri=='his_estimasi_biaya' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_perencanaan/input_estimasi_biaya"><i class="fa fa-caret-right push-5-r"></i> Estimasi Biaya</a></li>
				<?}?>
			</ul>
			
		</div>


	</div>
</div>
<?if ($st_lihat_rencana_ranap=='1'){?>
	<?if ($menu_kiri=='input_perencanaan_ranap'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/input_perencanaan_ranap');
	}?>
	<?if ($menu_kiri=='his_perencanaan_ranap'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/his_perencanaan_ranap');
}}?>

<?if ($st_lihat_rencana_bedah=='1'){?>
	<?if ($menu_kiri=='input_perencanaan_bedah'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/input_perencanaan_bedah');
	}?>
	<?if ($menu_kiri=='his_perencanaan_bedah'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/his_perencanaan_bedah');
	}?>	
<?}?>
<?if ($st_lihat_estimasi_biaya=='1'){?>
	<?if ($menu_kiri=='input_estimasi_biaya'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/input_estimasi_biaya');
	}?>
	<?if ($menu_kiri=='his_estimasi_biaya'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/his_estimasi_biaya');
	}?>
<?}?>	



