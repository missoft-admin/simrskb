<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_ri' ? 'block' : 'none') ?>">
	<!-- Music -->
	<?if ($pendaftaran_id_ranap){?>
	<div class="block animated fadeIn bg-gray" data-category="erm_ri">
		
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
				<?php 
					$url_utama_ri=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id_ranap.'/';
					$url_utama_rj=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id_ranap.'/';
				?>
				<?if ($st_lihat_ttv_ri=='1'){?>
				<li class="<?=($menu_kiri=='input_ttv_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_ttv_ri"><i class="fa fa-caret-right push-5-r"></i>Pemeriksaan TTV RI</a></li>
				<?}?>
				<?if ($st_lihat_asmed_ri=='1'){?>
				<li class="<?=($menu_kiri=='input_asmed_ri' || $menu_kiri=='his_asmed_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_asmed_ri"><i class="fa fa-caret-right push-5-r"></i>Pengkajian Medis RI</a></li>
				<?}?>
				<?if ($st_lihat_assesmen_ri=='1'){?>
				<li class="<?=($menu_kiri=='input_assesmen_ri' || $menu_kiri=='his_assesmen_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_assesmen_ri"><i class="fa fa-caret-right push-5-r"></i>Pengkajian Keperawatan RI</a></li>
				<?}?>
				<?if ($st_lihat_risiko_jatuh=='1'){?>
				<li class="<?=($menu_kiri=='input_risiko_jatuh' || $menu_kiri=='his_risiko_jatuh' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_risiko_jatuh"><i class="fa fa-caret-right push-5-r"></i>Pengkajian Risiko Jatuh</a></li>
				<?}?>
				<?if ($st_lihat_sga=='1'){?>
				<li class="<?=($menu_kiri=='input_sga' || $menu_kiri=='his_sga' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_sga"><i class="fa fa-caret-right push-5-r"></i>Subjective Global Assesment</a></li>
				<?}?>
				<?if ($st_lihat_gizi_anak=='1'){?>
				<li class="<?=($menu_kiri=='input_gizi_anak' || $menu_kiri=='his_gizi_anak' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_gizi_anak"><i class="fa fa-caret-right push-5-r"></i>Yorkhill Malnutrition Score</a></li>
				<?}?>
				<?if ($st_lihat_gizi_lanjutan=='1'){?>
				<li class="<?=($menu_kiri=='input_gizi_lanjutan' || $menu_kiri=='his_gizi_lanjutan' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_gizi_lanjutan"><i class="fa fa-caret-right push-5-r"></i>Assesmen Gizi Lanjutan</a></li>
				<?}?>
				<?if ($st_lihat_discarge=='1'){?>
				<li class="<?=($menu_kiri=='input_discarge' || $menu_kiri=='his_discarge' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_discarge"><i class="fa fa-caret-right push-5-r"></i>Discarge Planing</a></li>
				<?}?>
				<?if ($st_lihat_implementasi_kep=='1'){?>
				<li class="<?=($menu_kiri=='input_implementasi_kep' || $menu_kiri=='his_implementasi_kep' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_implementasi_kep"><i class="fa fa-caret-right push-5-r"></i>Implementasi Keperawatan</a></li>
				<?}?>
				<?if ($st_lihat_timbang=='1'){?>
				<li class="<?=($menu_kiri=='input_timbang' || $menu_kiri=='his_timbang' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_timbang"><i class="fa fa-caret-right push-5-r"></i>Timbang Terima</a></li>
				<?}?>
				<?if ($st_lihat_edukasi=='1'){?>
				<li class="<?=($menu_kiri=='input_edukasi_ri' || $menu_kiri=='his_edukasi_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_edukasi_ri"><i class="fa fa-caret-right push-5-r"></i>Cat Edukasi Terintegrasi</a></li>
				<?}?>
				<?if ($st_lihat_nyeri_ri=='1'){?>
				<li class="<?=($menu_kiri=='input_nyeri_ri' || $menu_kiri=='his_nyeri_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_nyeri_ri"><i class="fa fa-caret-right push-5-r"></i>Pengkajian Nyeri RI</a></li>
				<?}?>
				<?if ($st_lihat_fisio_ri=='1'){?>
				<li class="<?=($menu_kiri=='input_fisio_ri' || $menu_kiri=='his_fisio_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_fisio_ri"><i class="fa fa-caret-right push-5-r"></i>Assesmen Fisioterapi RI</a></li>
				<?}?>
				<?if ($st_lihat_cppt=='1'){?>
				<li class="<?=($menu_kiri=='input_cppt_ri' || $menu_kiri=='his_cppt_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_ri/input_cppt_ri"><i class="fa fa-caret-right push-5-r"></i>CPPT Rawat Inap </a></li>
				<?}?>
				<?if ($st_lihat_assesmen_pulang=='1'){?>
				<li class="<?=($menu_kiri=='input_assesmen_pulang' || $menu_kiri=='his_assesmen_pulang' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_assesmen_pulang"><i class="fa fa-caret-right push-5-r"></i>Ringkasan Pulang Keperawatan </a></li>
				<?}?>
				<?if ($st_lihat_assesmen_askep=='1'){?>
				<li class="<?=($menu_kiri=='input_assesmen_askep' || $menu_kiri=='his_assesmen_askep' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_assesmen_askep"><i class="fa fa-caret-right push-5-r"></i>Asuhan Keperawatan Perioperatif </a></li>
				<?}?>
				<?if ($st_lihat_keselamatan=='1'){?>
				<li class="<?=($menu_kiri=='input_assesmen_keselamatan' || $menu_kiri=='his_assesmen_keselamatan' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_assesmen_keselamatan"><i class="fa fa-caret-right push-5-r"></i>Cheklist Keselamatan Pasien </a></li>
				<?}?>
				<?if ($st_lihat_lap_bedah=='1'){?>
				<li class="<?=($menu_kiri=='input_lap_bedah' || $menu_kiri=='his_lap_bedah' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_lap_bedah"><i class="fa fa-caret-right push-5-r"></i>Laporan Pembedahan </a></li>
				<?}?>
				<?if ($st_lihat_pra_sedasi=='1'){?>
				<li class="<?=($menu_kiri=='input_pra_sedasi' || $menu_kiri=='his_pra_sedasi' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_pra_sedasi"><i class="fa fa-caret-right push-5-r"></i>Pra Anestesi & Pra Sedasi</a></li>
				<?}?>
				<?if ($st_lihat_ringkasan_pulang=='1'){?>
				<li class="<?=($menu_kiri=='input_ringkasan_pulang' || $menu_kiri=='his_ringkasan_pulang' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_ringkasan_pulang"><i class="fa fa-caret-right push-5-r"></i>Ringkasan Keluar Medis</a></li>
				<?}?>
				<?if ($st_lihat_pra_bedah=='1'){?>
				<li class="<?=($menu_kiri=='input_pra_bedah' || $menu_kiri=='his_pra_bedah' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ri/input_pra_bedah"><i class="fa fa-caret-right push-5-r"></i>Pra Bedah</a></li>
				<?}?>
				
			</ul>
			
		</div>
	</div>
	<?}?>
	<!-- END Music -->
</div>
<?

if ($menu_kiri=='input_pra_bedah'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_pra_bedah');
}
if ($menu_kiri=='his_pra_bedah'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_pra_bedah');
}
if ($menu_kiri=='his_ringkasan_pulang'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_ringkasan_pulang');
}
if ($menu_kiri=='input_ringkasan_pulang'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_ringkasan_pulang');
}
if ($menu_kiri=='his_pra_sedasi'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_pra_sedasi');
}
if ($menu_kiri=='input_pra_sedasi'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_pra_sedasi');
}
if ($menu_kiri=='his_lap_bedah'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_lap_bedah');
}
if ($menu_kiri=='input_lap_bedah'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_lap_bedah');
}
if ($menu_kiri=='his_assesmen_keselamatan'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_assesmen_keselamatan');
}
if ($menu_kiri=='input_assesmen_keselamatan'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_assesmen_keselamatan');
}
if ($menu_kiri=='input_assesmen_askep'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_assesmen_askep');
}
if ($menu_kiri=='his_assesmen_askep'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_assesmen_askep');
}
if ($menu_kiri=='input_assesmen_pulang'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_assesmen_pulang');
}

if ($menu_kiri=='his_assesmen_pulang'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_assesmen_pulang');
}
if ($menu_kiri=='input_cppt_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_cppt');
}
if ($menu_kiri=='his_cppt_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_cppt');
}
if ($menu_kiri=='input_fisio_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_fisio_ri');
}
if ($menu_kiri=='his_fisio_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_fisio_ri');
}
if ($menu_kiri=='input_nyeri_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_nyeri_ri');
}
if ($menu_kiri=='his_nyeri_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_nyeri_ri');
}
if ($menu_kiri=='input_ttv_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_ttv_ri');
}
if ($menu_kiri=='input_asmed_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_asmed_ri');
}
if ($menu_kiri=='his_asmed_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_asmed_ri');
}
if ($menu_kiri=='input_assesmen_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_assesmen_ri');
}
if ($menu_kiri=='his_assesmen_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_assesmen_ri');
}
if ($menu_kiri=='input_risiko_jatuh'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_risiko_jatuh');
}
if ($menu_kiri=='his_risiko_jatuh'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_risiko_jatuh');
}
if ($menu_kiri=='input_sga'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_sga');
}
if ($menu_kiri=='his_sga'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_sga');
}
if ($menu_kiri=='input_gizi_anak'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_gizi_anak');
}
if ($menu_kiri=='his_gizi_anak'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_gizi_anak');
}
if ($menu_kiri=='input_gizi_lanjutan'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_gizi_lanjutan');
}
if ($menu_kiri=='his_gizi_lanjutan'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_gizi_lanjutan');
}
if ($menu_kiri=='input_discarge'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_discarge');
}
if ($menu_kiri=='his_discarge'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_discarge');
}
if ($st_lihat_implementasi_kep=='1'){
if ($menu_kiri=='input_implementasi_kep'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_implementasi_kep');
}
if ($menu_kiri=='his_implementasi_kep'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_implementasi_kep');
}
}
if ($st_lihat_timbang=='1'){
if ($menu_kiri=='input_timbang'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_timbang');
}
if ($menu_kiri=='his_timbang'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_timbang');
}
}
if ($st_lihat_edukasi=='1'){
	if ($menu_kiri=='input_edukasi_ri' && $menu_atas=='erm_ri'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_edukasi_ri');
	}
	if ($menu_kiri=='his_edukasi_ri' && $menu_atas=='erm_ri'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_edukasi_ri');
	}
}
?>