<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_konsul' ? 'block' : 'none') ?>">
	<div class="block animated fadeIn bg-gray" data-category="erm_konsul">
		<div class="push-5-t bg-gray-light">
			<?php 
				if ($asalrujukan=='3'){
					$url_utama_konsul=$base_url.'tkonsul/tindakan/'.$pendaftaran_id_ranap.'/1/';
					$st_ranap='1';
				}else{
					$st_ranap='0';
					$url_utama_konsul=$base_url.'tkonsul/tindakan/'.$pendaftaran_id.'/0/';
				}
				$login_tipepegawai=$this->session->userdata('login_tipepegawai');
				$login_pegawai_id=$this->session->userdata('login_pegawai_id');
				$notif_jawab='';
				if ($menu_atas=='erm_konsul'){
				if ($login_tipepegawai=='2'){//DOKTER
					$q="SELECT COUNT(H.konsul_id) as jml FROM tpoliklinik_konsul H
						WHERE (H.iddokter='$login_pegawai_id' OR (H.iddokter='0' AND H.idpoliklinik IN (SELECT idpoliklinik FROM mpoliklinik_konsul_dokter WHERE iddokter='$login_pegawai_id'))) 
						AND H.idpasien='$idpasien' AND H.st_jawab='0' AND H.status_konsul='2'  AND H.iddokter_dari !='$login_pegawai_id'";
						// echo ($q);
					$jml=$this->db->query($q)->row('jml');
					if ($jml >0 ){
						$notif_jawab='<span class="badge badge-danger pull-right">'.$jml.'</span>';
					}
				}
				}
			?>
			<ul class="nav nav-pills nav-stacked push">
				<?php if (UserAccesForm($user_acces_form,array('1797'))){ ?>
				<li class="<?=($menu_kiri=='input_konsul' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_konsul/input_konsul"><i class="fa fa-caret-right push-5-r"></i> Konsul Baru <?=($asalrujukan=='3'?'Rawat Inap':'Rawat Jalan')?></a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1798'))){ ?>
				<li class="<?=($menu_kiri=='his_konsul' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_konsul/his_konsul"><i class="fa fa-caret-right push-5-r"></i> Daftar Konsul</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1799'))){ ?>
				<li class="<?=($menu_kiri=='jawab_konsul' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_konsul/jawab_konsul"><i class="fa fa-caret-right push-5-r"></i> Jawab Konsul <?=$notif_jawab?> </a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1800'))){ ?>
				<li class="<?=($menu_kiri=='all_konsul' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_konsul/all_konsul"><i class="fa fa-caret-right push-5-r"></i> History Konsul</a></li>
				<?}?>
				
			</ul>
			
		</div>


	</div>
</div>
<?
if ($menu_kiri=='input_konsul'){
$this->load->view('Tpendaftaran_poli_ttv/erm_konsul/input_konsul');
}
if ($menu_kiri=='his_konsul'){
$this->load->view('Tpendaftaran_poli_ttv/erm_konsul/his_konsul');
}
if ($menu_kiri=='jawab_konsul'){
$this->load->view('Tpendaftaran_poli_ttv/erm_konsul/jawab_konsul');
}
if ($menu_kiri=='all_konsul'){
$this->load->view('Tpendaftaran_poli_ttv/erm_konsul/all_konsul');
}
?>

