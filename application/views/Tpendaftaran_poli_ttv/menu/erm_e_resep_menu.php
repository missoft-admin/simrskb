<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_e_resep' ? 'block' : 'none') ?>">
	<div class="block animated fadeIn bg-gray" data-category="erm_e_resep">
		<div class="push-5-t bg-gray-light">
			<?php 
				$url_utama_form=$base_url.'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/';
				$url_utama_form_ri=$base_url.'tpoliklinik_resep/tindakan/'.$pendaftaran_id_ranap.'/';
				
			?>
			<ul class="nav nav-pills nav-stacked push">
				<?php if ($st_lihat_eresep){ ?>
					<?if ($pendaftaran_id_ranap){?>
					<li class="<?=($menu_kiri=='input_eresep_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_form_ri?>erm_e_resep/input_eresep_ri"><i class="fa fa-caret-right push-5-r"></i> E-Resep Rawat Inap </a></li>
					<?}else{?>
					<li class="<?=($menu_kiri=='input_eresep' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_form?>erm_e_resep/input_eresep"><i class="fa fa-caret-right push-5-r"></i> E-Resep Rawat Jalan</a></li>
					<?}?>
				<?}?>
				<li class="<?=($menu_kiri=='riwayat_eresep' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_form?>erm_e_resep/riwayat_eresep"><i class="fa fa-caret-right push-5-r"></i> Riwayat </a></li>
				
			</ul>
			
		</div>


	</div>
</div>
<?
if ($st_lihat_eresep){
	
if ($menu_kiri=='input_eresep' || $menu_kiri=='input_eresep_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_e_resep/input_eresep');
}

}
if ($menu_kiri=='riwayat_eresep'){
$this->load->view('Tpendaftaran_poli_ttv/erm_e_resep/riwayat_eresep');
}
if ($menu_kiri=='input_telaah' || $menu_kiri=='input_telaah_ri'){
$this->load->view('Tpendaftaran_poli_ttv/erm_e_resep/input_telaah');
}
if ($menu_kiri=='lihat_perubahan'){
$this->load->view('Tpendaftaran_poli_ttv/erm_e_resep/lihat_perubahan');
}
?>

