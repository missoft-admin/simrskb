<div class="col-sm-2 menu_kiri" style="display: none;">
	<!-- Music -->
	<div class="block animated fadeIn bg-gray" data-category="erm_penunjang">
		
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
				<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM PENUNJANG Menu 1</a></li>
				<li class="active"><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM PENUNJANG Menu 2</a></li>
				<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM PENUNJANG Menu 3</a></li>
				<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM PENUNJANG Menu 4</a></li>
				<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM PENUNJANG Menu 5</a></li>
				
			</ul>
			
		</div>
	</div>
	<!-- END Music -->
</div>
<div class="col-sm-10 menu_kanan" style="display: none;">
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<div class="block bg-white push-5-t" data-category="erm_penunjang">
			
			<div class="block-content">
				<ul class="nav nav-pills nav-stacked push">
					<li class="active">
						<a href="base_pages_inbox.html">
							<span class="badge pull-right">3</span><i class="fa fa-fw fa-inbox push-5-r"></i> Inbox
						</a>
					</li>
					<li>
						<a href="javascript:void(0)">
							<span class="badge pull-right">48</span><i class="fa fa-fw fa-star push-5-r"></i> Starred
						</a>
					</li>
					<li>
						<a href="javascript:void(0)">
							<span class="badge pull-right">1480</span><i class="fa fa-fw fa-send push-5-r"></i> Sent
						</a>
					</li>
					<li>
						<a href="javascript:void(0)">
							<span class="badge pull-right">2</span><i class="fa fa-fw fa-pencil push-5-r"></i> Draft
						</a>
					</li>
					<li>
						<a href="javascript:void(0)">
							<span class="badge pull-right">1987</span><i class="fa fa-fw fa-folder push-5-r"></i> Archive
						</a>
					</li>
					<li>
						<a href="javascript:void(0)">
							<span class="badge pull-right">10</span><i class="fa fa-fw fa-trash push-5-r"></i> Trash
						</a>
					</li>
				</ul>
			</div>
		<!-- END Layout API -->
	</div>
	<!-- END Music -->
</div>
