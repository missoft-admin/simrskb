<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_mpp' ? 'block' : 'none') ?>">
	<div class="block animated fadeIn bg-gray" data-category="erm_mpp">
		<div class="push-5-t bg-gray-light">
			<?php 
				if ($asalrujukan=='3'){
					$url_utama_mpp=$base_url.'tpoliklinik_ranap/tindakan/'.$pendaftaran_id_ranap.'/1/';
					$st_ranap='1';
				}else{
					$st_ranap='0';
					$url_utama_mpp=$base_url.'tpoliklinik_ranap/tindakan/'.$pendaftaran_id.'/0/';
				}
				// $url_utama_mpp=$base_url.'tpoliklinik_ranap/tindakan/'.$pendaftaran_id.'/';
				$login_tipepegawai=$this->session->userdata('login_tipepegawai');
				$login_pegawai_id=$this->session->userdata('login_pegawai_id');
				
			?>
			<ul class="nav nav-pills nav-stacked push">
				<?if ($st_lihat_mpp=='1'){?>
				<li class="<?=($menu_kiri=='input_mpp' || $menu_kiri=='his_mpp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_mpp?>erm_mpp/input_mpp"><i class="fa fa-caret-right push-5-r"></i> Skrining MPP</a></li>
				<?}?>
				<?if ($st_lihat_evaluasi_mpp=='1'){?>
				<li class="<?=($menu_kiri=='input_evaluasi_mpp' || $menu_kiri=='his_evaluasi_mpp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_mpp?>erm_mpp/input_evaluasi_mpp"><i class="fa fa-caret-right push-5-r"></i> Evaluasi Awal MPP</a></li>
				<?}?>
				<?if ($st_lihat_imp_mpp=='1'){?>
				<li class="<?=($menu_kiri=='input_imp_mpp' || $menu_kiri=='his_imp_mpp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_mpp?>erm_mpp/input_imp_mpp"><i class="fa fa-caret-right push-5-r"></i> Catatan Evaluasi MPP</a></li>
				<?}?>
				
			</ul>
			
		</div>


	</div>
</div>
<?if ($st_lihat_mpp=='1'){?>
	<?if ($menu_kiri=='input_mpp'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_mpp/input_mpp');
	}?>
	<?if ($menu_kiri=='his_mpp'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_mpp/his_mpp');
}}?>
<?if ($st_lihat_evaluasi_mpp=='1'){?>
	<?if ($menu_kiri=='input_evaluasi_mpp'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_mpp/input_evaluasi_mpp');
	}?>
	<?if ($menu_kiri=='his_evaluasi_mpp'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_mpp/his_evaluasi_mpp');
}}?>
<?if ($st_lihat_imp_mpp=='1'){?>
	<?if ($menu_kiri=='input_imp_mpp'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_mpp/input_imp_mpp');
	}?>
	<?if ($menu_kiri=='his_imp_mpp'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_mpp/his_imp_mpp');
}}?>




