<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<style>
	.fixed-header {
		position: fixed;
		top: 60px;
		z-index: 1040;
		width: calc(100% - 70px);
	}
	// .modal {
		
		// z-index: 1201;
	// }
	// .js-datepicker {
		
		// z-index: 1202;
	// }

	.fixed-header-full {
		position: fixed;
		top: 60px;
		/* left: 26rem; */
		z-index: 1040;
		width: calc(100% - 270px);
	}
	
	.separator-header {
		margin-top: 125px;
	}
	.kbw-signature canvas{
		width: 120% !important;
		
		
	}
</style>
<?
$path_tindakan=$this->session->userdata('path_tindakan');
?>
<div class="row " >
	<div class="header-freeze">
		<div class="col-sm-12 bg-primary ">
			<div class="block block-themed">
				<div class="block-header bg-primary" style="padding-bottom:0;padding-top:10px;padding-right:0px">
					<div class="block-options-simple">
						<button class="btn btn-warning btn-xs menu_besar" type="button" onclick="kecilkan()" title="Kecilkan Header"><i class="fa fa-eye-slash"></i> Hidden</button> 
						<button class="btn btn-warning btn-xs menu_kecil" type="button" onclick="besarkan()" title="Munculkan Header"><i class="fa fa-expand"></i> Tampilkan</button> 
						<button class="btn btn-danger btn-xs" type="button" title="Final Transaksi"><i class="fa fa-save push-5-r"></i> Final Transaksi </button>
						<a href="{base_url}<?=$path_tindakan?>" class="btn btn-default btn-xs menu_click" type="button" title="Keluar"><i class="fa fa-mail-forward"></i> Keluar / Kembali</a>
						<input type="hidden" readonly class="form-control input-sm" id="pendaftaran_id" value="{pendaftaran_id}"> 
						<input type="hidden" readonly class="form-control input-sm" id="pendaftaran_id_ranap" value="{pendaftaran_id_ranap}"> 
						<input type="hidden" readonly class="form-control input-sm" id="statuskasir_rajal" value="{statuskasir_rajal}"> 
						<input type="hidden" readonly class="form-control input-sm" id="statuskasir" value="{statuskasir}"> 
						<input type="hidden" readonly class="form-control input-sm" id="asalrujukan" value="{asalrujukan}"> 
						<input type="hidden" readonly class="form-control input-sm" id="login_ppa_id" value="{login_ppa_id}"> 
						<input type="hidden" readonly class="form-control input-sm" id="trx_id_label" value="{trx_id}"> 
					</div>
				</div>
				<div class="block-content bg-primary" style="padding-top:0">
					<div class="menu_besar animated fadeIn pull-r-l">
						<div class="col-md-3 bg-primary">
							<table class="text-left">
								<tbody>
									<tr>
										<td style="width: 15%;">
											<div class="">
												<img class="img-avatar" src="<?=site_url()?>assets/upload/avatars/<?=$def_image?>" alt="">
											</div>
										</td>
										<input type="hidden" readonly id="idpasien" value="{idpasien}"> 
										<td class="bg-primary text-white" style="width: 85%;">
											<div class="h4 font-w700 text-white"> <?=$title_nama.'. '.$namapasien?></div>
											<div class="h5 text-white  push-5-t text-white">{no_medrec} <?=($statuskasir=='2'?text_danger('CLOSED'):text_info('OPENED'))?></div>
											<div class="h5 text-white  push-5-t text-white">{jk}, <?=HumanDateShort($tanggal_lahir)?> | <?=$umurtahun.' Tahun '.$umurbulan.' Bulan '.$umurhari.' Hari'?></div>
											<div class="h5 text-white text-uppercase push-5-t"> 
											<div class="btn-group" role="group">
												<button class="btn btn-default btn-sm text-danger" type="button"><?=text_alergi($riwayat_alergi_header)?></button>
												<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien(<?=$idpasien?>)"><i class="fa fa-info"></i></button>
											</div>
											<?if ($nama_tindakan_setuju){?>
											<button class="btn btn-success  btn-xs" type="button" onclick="show_list_tindakan(<?=$pendaftaran_id_ranap?>)"><?=$nama_tindakan_setuju?></button>
											<?}?>
											<?if ($status_proses_card_pass){?>
											<a href="<?=base_url()?>assets/upload/kwitansi/CP_<?=$nopendaftaran?>.pdf" target="_blank" class="btn btn-sm btn-warning">PERSETUJUAN PULANG</a>
											<?}?>
											</div>
										</td>
									</tr>
								</tbody>
							</table>			
						</div>
						
						<div class="col-md-6 bg-primary">
							<table class="block-table text-left">
								<tbody>
									<tr>
										<td class="bg-primary" style="width: 20%;">
											<div class="text-white text-uppercase  text-center"> Tekanan Darah </div>
											<div class="push-5-t"> 
												<div class="input-group">
													<input class="form-control  input-sm text-center" disabled style="color:<?=$warna_sistole?>;background-color:white" type="text" readonly value="{header_td_sistole} <?=$satuan_td?> / " placeholder="Sistole">
													
													<input class="form-control  input-sm text-center" disabled style="color:<?=$warna_diastole?>;background-color:white" type="text" readonly value="{header_td_diastole} <?=$satuan_td?>"  placeholder="Diastole">
												</div>
											</div>
											<div class="text-white text-uppercase  text-center push-5-t">Risiko Jatuh</div>
											<div class="text-white"><input type="text" disabled style="background-color:white" class="form-control input-sm text-center" id="head_resiko_jatuh" value="{var_header_risiko_jatuh}"> </div>
										</td>
										<td class="bg-primary" style="width: 20%;">
											<div class="text-white text-uppercase  text-center">Suhu Tubuh</div>
											<div class="push-5-t"> <input type="text" readonly style="color:<?=$warna_suhu?>;background-color:white"  class="form-control input-sm text-center" id="head_suhu" value="<?=($header_suhu!=''?$header_suhu.' '.$satuan_suhu:'')?>">  </div>
											<div class="text-white  text-center text-uppercase push-5-t">Risiko Nyeri</div>
											<div class="text-white"><input type="text" readonly style="background-color:white" class="form-control input-sm text-center" id="head_risiko_nyeri" value="<?=$risiko_nyeri_nilai.' '.$risiko_nyeri_singkatan?>"> </div>
										</td>
										<td class="bg-primary" style="width: 20%;">
											<div class="text-white text-uppercase text-center">Frek Nadi</div>
											<div class="push-5-t"> <input type="text" readonly style="color:<?=$warna_nadi?>;background-color:white"  class="form-control input-sm text-center" id="head_nadi" value="<?=($header_nadi!=''?$header_nadi.' '.$satuan_nadi:'')?>">  </div>
											<div class="text-white text-uppercase  text-center push-5-t">Berat Badan</div>
											<div class="text-white"><input type="text" readonly style="color:<?=$warna_berat?>;background-color:white" class="form-control input-sm text-center" id="head_berat_badan" value="<?=($header_berat_badan!=''?$header_berat_badan.' Kg':'')?>"> </div>
										</td>
										<td class="bg-primary" style="width: 20%;">
											<div class="text-white text-uppercase  text-center">Frek Nafas</div>
											<div class="push-5-t"> <input type="text" readonly style="color:<?=$warna_nafas?>;background-color:white"  class="form-control input-sm text-center" id="head_nafas" value="<?=($header_nafas!=''?$header_nafas.' '.$satuan_nafas:'')?>">  </div>
											<div class="text-white text-uppercase  text-center push-5-t">Tinggi Badan</div>
											<div class="text-white"><input type="text" readonly style="color:<?=$warna_berat?>;background-color:white"  class="form-control input-sm text-center" id="head_tinggi_badan" value="<?=($header_tinggi_badan!=''?$header_tinggi_badan.' Cm':'')?>"> </div>
										</td>
										<td class="bg-primary" style="width: 20%;">
											<div class="text-white text-uppercase  text-center">SpO2</div>
											<div class="push-5-t"> <input type="text" readonly style="color:<?=$warna_spo2?>;background-color:white"  class="form-control input-sm text-center" id="head_nafas" value="<?=($header_spo2!=''?$header_spo2.' '.$satuan_spo2:'')?>">  </div>
											<div class="text-white text-uppercase  text-center push-5-t">EWS SKOR</div>
											<div class="text-white"> <input type="text" readonly style="color:<?=$warna_ews_head?>;background-color:white"  class="form-control input-sm text-center" id="head_ews" value="<?=($total_skor_ews_head!=''?$total_skor_ews_head:'')?> ">  </div>
										</td>
									</tr>
									
								</tbody>
							</table>			
						</div>
						<div class="col-md-3 bg-primary">
							<table class="block-table text-left">
								<tbody>
									<tr>
										<td class="bg-primary" style="width: 100%;">
											<div class="h4 text-white push-5-t"> REGISTRATION INFO 	</div>
											<div class="h5 push-5-t"> <?=$nopendaftaran.', '.HumanDateLong($tanggaldaftar)?> </div>
											<?if ($st_poli_st_ranap=='poli'){?>
											<div class="h5 text-white text-uppercase"><?=GetAsalRujukanR($idtipe_poli).' - '.$nama_poli?></div>
											<?}else{?>
											<div class="h5 text-white text-uppercase"><?=($nama_ruangan).($nama_kelas?' - '.$nama_kelas:'').($nama_bed?' - '.$nama_bed:'')?></div>
											
											<?}?>
											<div class="h5 text-white"><i class="fa fa-user-md"></i> <?=$nama_dokter?></div>
											<div class="h5 text-uppercase"><i class="fa fa-street-view"></i> <?=($nama_rekanan?$nama_kelompok.' - '.$nama_rekanan:$nama_kelompok)?></div>
										</td>
									</tr>
								</tbody>
							</table>			
						</div>
					</div>
					<div class="menu_kecil animated fadeIn pull-r-l">
						<div class="col-md-12 bg-primary push-20">
							<table class="text-left">
								<tbody>
									<tr>
										<td class="bg-primary text-white" style="width: 100%;">
                                            <div style="width:100%">&nbsp;</div>
											<div class="btn-group" role="group">
												<button class="btn btn-default btn-sm text-danger" type="button"> <?=text_alergi($riwayat_alergi_header)?></button>
												<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien(<?=$idpasien?>)"><i class="fa fa-info"></i></button>
												&nbsp;&nbsp;&nbsp;<span class="h5 text-left text-uppercase">
												<?=$no_medrec?> | <?=$title_nama.'. '.$namapasien?> | <?=$jk?>,  <?=HumanDateShort($tanggal_lahir)?> |  <?=$umurtahun.' Tahun '.$umurbulan.' Bulan '.$umurhari.' Hari'?> | <?=($nama_rekanan?$nama_kelompok.' - '.$nama_rekanan:$nama_kelompok)?></span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>			
						</div>
					</div>
				</div>
			</div>
		</div>
		<?if ($trx_id=='#'){
			$url_utama_form_e_resep=$base_url.'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/erm_e_resep/input_eresep';
			
			?>
		<div class="col-sm-12 bg-warning-light" id="div_menu_atas">
			<!-- Custom files functionality is initialized in js/pages/base_pages_files.js -->
			<!-- Add the category value you want each link in .js-media-filter to filter out in its data-category attribute. Add the value 'all' to show all items -->
			<ul class="js-media-filter nav nav-pills text-uppercase" style="margin-left:-15px">
				<?php if (UserAccesForm($user_acces_form,array('1625'))){ ?>
						<li class="<?=(in_array($menu_atas, array('erm_rj')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_rj"><i class="si si-folder-alt push-5-r"></i> ERM RJ</a></li>
				<?}?>
				<?if ($pendaftaran_id_ranap){?>
				<?php if (UserAccesForm($user_acces_form,array('1626'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_ri')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_ri"><i class="si si-folder-alt push-5-r"></i> ERM RI</a></li>
				<?}?>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1627'))){ ?>
				<li class=""><a href="javascript:void(0)" data-category="erm_penunjang"><i class="si si-docs push-5-r"></i> PENUNJANG</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1628'))){ ?>
					<?if ($pendaftaran_id_ranap==''){?>
						<li class="<?=(in_array($menu_atas, array('erm_e_resep')) ? 'active' : '') ?>"><a href="<?=$url_utama_form_e_resep?>" data-category="erm_e_resep"><i class="si si-layers push-5-r"></i> E-RESEP</a></li>
					<?}else{?>
						<li class="<?=(in_array($menu_atas, array('erm_e_resep')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_e_resep"><i class="si si-layers push-5-r"></i> E-RESEP</a></li>
					<?}?>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1629'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_lab')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_laboratorium"><i class="si si-picture push-5-r text-uppercase"></i> LABORATORIUM</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1630'))){ ?>
					<li class="<?=(in_array($menu_atas, array('erm_rad')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_radiologi"><i class="si si-picture push-5-r text-uppercase"></i> RADIOLOGI</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1631'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_trx')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_trx"><i class="si si-note push-5-r"></i> TRANSAKSI</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1632'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_perencanaan')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_perencanaan"><i class="si si-call-in push-5-r"></i> PERENCANAAN</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1633'))){ ?>
				<li class=""><a href="javascript:void(0)" data-category="erm_surat"><i class="si si-envelope push-5-r"></i> PENERBITAN SURAT</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1634'))){ ?>
				<li class=""><a href="javascript:void(0)" data-category="rujukan"><i class="si si-envelope-letter push-5-r"></i> RUJUKAN</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1635'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_konsul')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_konsul"><i class="si si-call-out push-5-r"></i> KONSULTASI</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1846'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_rm')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_rm"><i class="si si-call-in push-5-r"></i> REHABILITAS MEDIS</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('1986'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_dpjp')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_dpjp"><i class="fa fa-user-md push-5-r"></i> DPJP</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2000'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_site')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_site"><i class="si si-cursor push-5-r"></i> SITE MARKING</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2037'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_rekon')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_rekon"><i class="fa fa-map-signs"></i> REKONSILIASI</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2045'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_tf')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_tf"><i class="si si-loop"></i> TRANSFER</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2077'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_per')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_per"><i class="fa fa-check"></i> PERSETUJUAN / PERNYATAAN</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2097'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_doc')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_doc"><i class="si si-doc"></i> DOCUMENT</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2182'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_hasil_penunjang')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_hasil_penunjang"><i class="si si-docs"></i> HASIL PEMERIKSAAN PENUNJANG</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2187'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_komunikasi')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_komunikasi"><i class="si si-call-in push-5-r"></i> KOMUNIKASI EFEKTIF</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2209'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_mpp')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_mpp"><i class="si si-user push-5-r"></i> MPP</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2375'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_survey')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_survey"><i class="si si-bar-chart"></i> SURVEY</a></li>
				<?}?>
				<?if ($list_trx_kamar_bedah){?>
				<?php if (UserAccesForm($user_acces_form,array('2289'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_bedah')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_bedah"><i class="fa fa-behance-square push-5-r"></i> KAMAR BEDAH</a></li>
				<?}?>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2383'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_ews')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_ews"><i class="fa fa-warning"></i> EWS</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2395'))){ ?>
				<li class="<?=(in_array($menu_atas, array('erm_observasi')) ? 'active' : '') ?>"><a href="javascript:void(0)" data-category="erm_observasi"><i class="fa fa-tv"></i> OBSERVASI</a></li>
				<?}?>
				
				
			</ul>
		</div>
		<?}?>
	</div>
	<div class="content-verification">
	<?$this->load->view('Tpendaftaran_poli_ttv/menu/_content')?>
	</div>
</div>
<div class="modal in" id="modal_header_his_alergi_pasien" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">HISTORY ALERGI</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="table-responsive">
									<table class="table" id="index_header_alergi">
										<thead>
											<tr>
												<th width="10%">No</th>
												<th width="15%">Jenis Alergi</th>
												<th width="20%">Detail Alergi</th>
												<th width="30%">Reaksi</th>
												<th width="15%">User Input</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}pages/base_pages_files.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<? $this->load->view('Ttindakan_ranap_deposit/modal_tindakan_opr')?>
<script type="text/javascript">
$(document).ready(function(){
	
let trx_id_label=$("#trx_id_label").val();
if (trx_id_label!=='#'){
	$(".btn_close_left").click(); 
	$(".menu_kiri").hide();
	$(".btn_kembali_ass").hide();
	
}else{
	// $(".menu_kiri").show();
}
besarkan();
$(".decimal").number(true,1,'.',',');	
	var pinStatus = true;
	$("#pinheader").click(function () {
		pinStatus = !pinStatus;
		if (pinStatus) {
			$("#pinheader").html('<i class="fa fa-expand"></i> Close');
		} else {
			$("#pinheader").html('<i class="fa fa-eye-slash"></i> Freeze');

			$('.header-freeze').removeClass('fixed-header');
			$('.header-freeze').removeClass('fixed-header-full');
			$('.content-verification').removeClass('separator-header');
		}
	});

	$(window).scroll(function () {
		if (pinStatus) {
			$("#pinheader").html('<i class="fa fa-close"></i> Close');
			if ($(window).scrollTop() >= 500) {
				if (localStorage.getItem("sidebar") == 'full') {
					$('.header-freeze').addClass('fixed-header-full');
				} else {
					$('.header-freeze').addClass('fixed-header');
				}

				$('.content-verification').addClass('separator-header');
			} else {
				if (localStorage.getItem("sidebar") == 'full') {
					$('.header-freeze').removeClass('fixed-header-full');
				} else {
					$('.header-freeze').removeClass('fixed-header');
				}

				$('.content-verification').removeClass('separator-header');
			}
		} else {
			$("#pinheader").html('<i class="fa fa-thumb-tack"></i> Freeze');
		}
	});
});
function kecilkan(){
	$(".menu_kecil").show();
	$(".menu_besar").hide();
}
function besarkan(){
	$(".menu_kecil").hide();
	$(".menu_besar").show();
}
function kecilkan_menu(){
	$("#erm_rj_menu").hide();
}
function lihat_his_alergi_pasien(idpasien){
	$("#modal_header_his_alergi_pasien").modal('show');
	
	$('#index_header_alergi').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_header_alergi').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
		
			ajax: { 
				url: '{site_url}tpendaftaran_poli_ttv/load_alergi_his_header', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idpasien:idpasien,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
function goBack() {
  window.history.back();
}
</script>