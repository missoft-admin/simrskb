<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_rj' ? 'block' : 'none') ?>">
	<div class="block animated fadeIn bg-gray" data-category="erm_rj">
		<div class="push-5-t bg-gray-light">
			
			<ul class="nav nav-pills nav-stacked push">
			<?php 
				if ($st_ranap=='1'){
					$pendaftaran_id_poli=$idpoliklinik;
				}else{
					$pendaftaran_id_poli=$pendaftaran_id;
				}
				$url_utama_rj=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id_poli.'/';
				$url_utama_kes=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id_poli.'/';
			?>
				<?php if (UserAccesForm($user_acces_form, ['2179'])) { ?>
				<li class="<?=($menu_kiri=='lihat_gc' || $menu_kiri=='his_gc' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/lihat_gc"><i class="fa fa-caret-right push-5-r"></i>General Consent</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form, ['2180'])) { ?>
				<li class="<?=($menu_kiri=='lihat_sp' || $menu_kiri=='his_sp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/lihat_sp"><i class="fa fa-caret-right push-5-r"></i>Skrining Pasien</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form, ['2181'])) { ?>
				<li class="<?=($menu_kiri=='lihat_sc' || $menu_kiri=='his_sc' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/lihat_sc"><i class="fa fa-caret-right push-5-r"></i>Skrining Covid</a></li>
				<?}?>
				<?if ($st_lihat_ttv=='1'){?>
				<li class="<?=($menu_kiri=='input_ttv' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_ttv"><i class="fa fa-caret-right push-5-r"></i>Pemeriksaan TTV</a></li>
				<?}?>
				<?if ($st_lihat_assesmen=='1'){?>
				<li class="<?=($menu_kiri=='input_assesmen_rj' || $menu_kiri=='his_assesmen_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_assesmen_rj"><i class="fa fa-caret-right push-5-r"></i>Assemen Keperawatan RJ</a></li>
				<?}?>
				<?if ($st_lihat_asmed=='1'){?>
				<li class="<?=($menu_kiri=='input_asmed_rj' || $menu_kiri=='his_asmed_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_asmed_rj"><i class="fa fa-caret-right push-5-r"></i>Assesmen Medis RJ</a></li>
				<?}?>
				<?if ($st_lihat_nyeri=='1'){?>
				<li class="<?=($menu_kiri=='input_nyeri_rj' || $menu_kiri=='his_nyeri_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_nyeri_rj"><i class="fa fa-caret-right push-5-r"></i>Pengkajian Nyeri RJ</a></li>
				<?}?>
				<?if ($st_lihat_triage=='1'){?>
				<li class="<?=($menu_kiri=='input_triage' || $menu_kiri=='his_triage' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_triage"><i class="fa fa-caret-right push-5-r"></i>Triage</a></li>
				<?}?>
				<?if ($st_lihat_asmed_igd=='1'){?>
				<li class="<?=($menu_kiri=='input_asmed_igd' || $menu_kiri=='his_asmed_igd' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_asmed_igd"><i class="fa fa-caret-right push-5-r"></i>Assesmen Medis IGD</a></li>
				<?}?>
				<?if ($st_lihat_assesmen_igd=='1'){?>
				<li class="<?=($menu_kiri=='input_assesmen_igd' || $menu_kiri=='his_assesmen_igd' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_assesmen_igd"><i class="fa fa-caret-right push-5-r"></i>Assesmen Keperawatan IGD</a></li>
				<?}?>
				<?if ($st_lihat_cppt=='1'){?>
				<li class="<?=($menu_kiri=='input_cppt' || $menu_kiri=='his_cppt' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_cppt"><i class="fa fa-caret-right push-5-r"></i>CPPT</a></li>
				<?}?>
				
				<?if ($st_lihat_edukasi=='1'){?>
				<li class="<?=($menu_kiri=='input_edukasi' || $menu_kiri=='his_edukasi' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_edukasi"><i class="fa fa-caret-right push-5-r"></i>Cat Edukasi Terintegrasi</a></li>
				<?}?>
				<?if ($st_lihat_asmed_fisio=='1'){?>
				<li class="<?=($menu_kiri=='input_asmed_fisio' || $menu_kiri=='his_asmed_fisio' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_rj/input_asmed_fisio"><i class="fa fa-caret-right push-5-r"></i>Assesmen Fisioterapi</a></li>
				<?}?>
				<?if ($st_lihat_keselamatan=='1'){?>
				<li class="<?=($menu_kiri=='input_assesmen_keselamatan_rj' || $menu_kiri=='his_assesmen_keselamatan_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_kes?>erm_rj/input_assesmen_keselamatan_rj"><i class="fa fa-caret-right push-5-r"></i>Cheklist Keselamatan Pasien</a></li>
				<?}?>
				<?if ($st_lihat_lap_bedah=='1'){?>
				<li class="<?=($menu_kiri=='input_lap_bedah_rj' || $menu_kiri=='his_lap_bedah_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_kes?>erm_rj/input_lap_bedah_rj"><i class="fa fa-caret-right push-5-r"></i>Laporan Pembedahan </a></li>
				<?}?>
				<?if ($st_lihat_implementasi_kep=='1'){?>
				<li class="<?=($menu_kiri=='input_implementasi_kep_rj' || $menu_kiri=='his_implementasi_kep_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_kes?>erm_rj/input_implementasi_kep_rj"><i class="fa fa-caret-right push-5-r"></i>Implementasi Keperawatan</a></li>
				<?}?>
				<?if ($st_lihat_pra_bedah=='1'){?>
				<li class="<?=($menu_kiri=='input_pra_bedah_rj' || $menu_kiri=='input_pra_bedah_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_kes?>erm_rj/input_pra_bedah_rj"><i class="fa fa-caret-right push-5-r"></i>Pra Bedah</a></li>
				<?}?>
			</ul>
			
		</div>


	</div>
</div>

<?
if ($st_lihat_pra_bedah=='1'){
if ($menu_kiri=='input_pra_bedah_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_pra_bedah');
}
if ($menu_kiri=='his_pra_bedah_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_pra_bedah');
}
}

if ($st_lihat_implementasi_kep=='1'){
if ($menu_kiri=='input_implementasi_kep_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_implementasi_kep');
}
if ($menu_kiri=='his_implementasi_kep_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_implementasi_kep');
}
}

if ($st_lihat_keselamatan=='1'){
if ($menu_kiri=='input_assesmen_keselamatan_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_assesmen_keselamatan');
}
}
if ($menu_kiri=='his_assesmen_keselamatan_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_assesmen_keselamatan');
}
if ($st_lihat_lap_bedah=='1'){
if ($menu_kiri=='input_lap_bedah_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/input_lap_bedah');
}
}
if ($menu_kiri=='his_lap_bedah_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_ri/his_lap_bedah');
}
?>
<?
if ($menu_kiri=='input_ttv'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_ttv');
}
?>
<? 
if ($menu_kiri=='input_assesmen_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_assesmen_rj');
}
?>

<? 
if ($menu_kiri=='his_assesmen_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_assesmen_rj');
}
?>
<? 
if ($menu_kiri=='input_asmed_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_asmed_rj');
}
if ($menu_kiri=='input_asmed_fisio'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_asmed_fisio');
}
if ($menu_kiri=='his_asmed_fisio'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_asmed_fisio');
}
?>
<? 
if ($menu_kiri=='his_asmed_rj'){
	// print_r('sini');exit;
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_asmed_rj');
}
?>
<? 
if ($menu_kiri=='input_nyeri_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_nyeri_rj');
}
?>
<? 
if ($menu_kiri=='his_nyeri_rj'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_nyeri_rj');
}
?>

<? 
if ($menu_kiri=='input_triage'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_triage');
}
?>
<? 
if ($menu_kiri=='his_triage'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_triage');
}
?>
<? 
if ($menu_kiri=='input_asmed_igd'){
	// echo 'sini';
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_asmed_igd');
}
?>
<? 
if ($menu_kiri=='his_asmed_igd'){
	// print_r('sini');exit;
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_asmed_igd');
}
?>
<? 
if ($menu_kiri=='input_assesmen_igd'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_assesmen_igd');
}
?>
<? 
if ($menu_kiri=='his_assesmen_igd'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_assesmen_igd');
}
?>
<? 
if ($menu_kiri=='input_cppt'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_cppt');
}
?>
<? 
if ($menu_kiri=='his_cppt'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_cppt');
}
?>

<? 
if ($menu_kiri=='input_edukasi'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_edukasi');
}
?>
<? 
if ($menu_kiri=='his_edukasi'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_edukasi');
}
if ($menu_kiri=='lihat_gc' || $menu_kiri=='his_gc'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/lihat_gc');
}
if ($menu_kiri=='lihat_sp' || $menu_kiri=='his_sp'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/lihat_sp');
}
if ($menu_kiri=='lihat_sc' || $menu_kiri=='his_sc'){
$this->load->view('Tpendaftaran_poli_ttv/erm_rj/lihat_sc');
}
?>
