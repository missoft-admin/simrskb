<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_trx' ? 'block' : 'none') ?>;">
	<div class="block animated fadeIn bg-gray" data-category="erm_trx">
		<div class="push-10-t bg-gray-light">
			<?
				$url_utama_trx_rj='';
				$url_utama_trx_ri='';
				if ($asalrujukan=='3'){
					$url_utama_trx_ri=base_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id_ranap.'/1/';
					$url_pembayaran=base_url().'tbilling_ranap/tindakan/'.$pendaftaran_id_ranap.'/1/';
					$st_ranap='1';
				}else{
					$st_ranap='0';
					// $url_utama_trx_rj=base_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/0/';
					$url_utama_trx_rj=$base_url.'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/0/';
				}
			?>
			<ul class="nav nav-pills nav-stacked push">
				<!-- <php if ($st_lihat_input_bmhp == '1') { ?> -->
					<li class="<?= ($menu_kiri == 'input_bmhp' ? 'active' : ''); ?> menu_click"><a href="<?=$url_utama_trx_rj?>erm_trx/input_bmhp"><i class="fa fa-caret-right push-5-r"></i>Penggunaan BMHP</a></li>
				<!-- <php } ?> -->
				<!-- <php if ($st_lihat_input_layanan == '1') { ?> -->
					<li class="<?= ($menu_kiri == 'input_layanan' ? 'active' : ''); ?> menu_click"><a href="<?=$url_utama_trx_rj?>erm_trx/input_layanan"><i class="fa fa-caret-right push-5-r"></i>Tindakan Pelayanan</a></li>
					<li class="<?= ($menu_kiri == 'input_layanan_fisio' ? 'active' : ''); ?> menu_click"><a href="<?=$url_utama_trx_rj?>erm_trx/input_layanan_fisio"><i class="fa fa-caret-right push-5-r"></i>Tindakan Fisioterapi RJ</a></li>
					<?if ($pendaftaran_id_ranap){?>
					<li class="<?= ($menu_kiri == 'input_layanan_fisio_ri' ? 'active' : ''); ?> menu_click"><a href="<?=$url_utama_trx_ri?>erm_trx/input_layanan_fisio_ri"><i class="fa fa-caret-right push-5-r"></i>Tindakan Fisioterapi RI</a></li>
					<?}?>
					<?if ($pendaftaran_id_ranap){?>
					<li class="<?= ($menu_kiri == 'input_visit' ? 'active' : ''); ?> menu_click"><a href="<?=$url_utama_trx_ri?>erm_trx/input_visit"><i class="fa fa-caret-right push-5-r"></i>Visite Dokter RI</a></li>
					<?}?>
					<?if ($pendaftaran_id_ranap){?>
					<li class="<?= ($menu_kiri == 'input_bmhp_ri' ? 'active' : ''); ?> menu_click"><a href="<?=$url_utama_trx_ri?>erm_trx/input_bmhp_ri"><i class="fa fa-caret-right push-5-r"></i>Penggunaan BMHP RI</a></li>
					<?}?>
					<?if ($pendaftaran_id_ranap){?>
					<li class="<?= ($menu_kiri == 'input_layanan_ri' ? 'active' : ''); ?> menu_click"><a href="<?=$url_utama_trx_ri?>erm_trx/input_layanan_ri"><i class="fa fa-caret-right push-5-r"></i>Tindakan Pelayanan RI</a></li>
					<?}?>
					<?if ($pendaftaran_id_ranap){?>
					<li class="<?= ($menu_kiri == 'pembayaran_ri' ? 'active' : ''); ?> menu_click"><a href="<?=$url_pembayaran?>erm_trx/pembayaran_ri"><i class="fa fa-caret-right push-5-r"></i>Pembayaran Rawat Inap</a></li>
					<?}?>
			</ul>
		</div>
	</div>
</div>

<?php
	if ($menu_kiri == 'pembayaran_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/pembayaran_ri');
	}
?>
<?php
	if ($menu_kiri == 'input_bmhp'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/input_bmhp');
	}
?>

<?php 
	if ($menu_kiri == 'input_layanan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/input_layanan');
	}
?>
<?php 
	if ($menu_kiri == 'input_layanan_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/input_layanan_ri');
	}
?>
<?php 
	if ($menu_kiri == 'input_layanan_fisio'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/input_layanan_fisio');
	}
?>
<?php 
	if ($menu_kiri == 'input_layanan_fisio_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/input_layanan_fisio_ri');
	}
?>
<?php 
	if ($menu_kiri == 'input_visit'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/input_visit');
	}
?>
<?php
	if ($menu_kiri == 'input_bmhp_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_trx/input_bmhp_ri');
	}
?>
