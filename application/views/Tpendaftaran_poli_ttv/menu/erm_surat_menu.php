<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_surat' ? 'block' : 'none') ?>">
	<div class="block animated fadeIn bg-gray" data-category="erm_surat">
		<div class="push-5-t bg-gray-light">
			<?php 
				if ($asalrujukan=='3'){
					$url_utama_konsul=$base_url.'tsurat/tindakan/'.$pendaftaran_id_ranap.'/1/';
					$st_ranap='1';
				}else{
					$st_ranap='0';
					$url_utama_konsul=$base_url.'tsurat/tindakan/'.$pendaftaran_id.'/0/';
				}
				// $url_utama_konsul=$base_url.'tsurat/tindakan/'.$pendaftaran_id.'/';
				$login_tipepegawai=$this->session->userdata('login_tipepegawai');
				$login_pegawai_id=$this->session->userdata('login_pegawai_id');
				
			?>
			<ul class="nav nav-pills nav-stacked push">
				<?php if (UserAccesForm($user_acces_form,array('2251'))){?>
				<li class="<?=($menu_kiri=='input_surat_sakit' || $menu_kiri=='his_surat_sakit' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_sakit"><i class="fa fa-caret-right push-5-r"></i> Surat Keterangan Sakit</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2258'))){?>
				<li class="<?=($menu_kiri=='input_surat_sehat' || $menu_kiri=='his_surat_sehat' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_sehat"><i class="fa fa-caret-right push-5-r"></i> Surat Keterangan Sehat</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2267'))){?>
				<li class="<?=($menu_kiri=='input_surat_kontrol' || $menu_kiri=='his_surat_kontrol' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_kontrol"><i class="fa fa-caret-right push-5-r"></i> Surat Kontrol Ulang</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2273'))){?>
				<li class="<?=($menu_kiri=='input_surat_lanjut' || $menu_kiri=='his_surat_lanjut' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_lanjut"><i class="fa fa-caret-right push-5-r"></i> Surat Konsul Lanjut</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2279'))){?>
				<li class="<?=($menu_kiri=='input_surat_rawat' || $menu_kiri=='his_surat_rawat' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_rawat"><i class="fa fa-caret-right push-5-r"></i> Surat Keterangan Rawat</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2344'))){?>
				<li class="<?=($menu_kiri=='input_surat_external' || $menu_kiri=='his_surat_external' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_external"><i class="fa fa-caret-right push-5-r"></i> Surat Pemeriksaan Eksternal</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2399'))){?>
				<li class="<?=($menu_kiri=='input_surat_amputasi' || $menu_kiri=='his_surat_amputasi' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_amputasi"><i class="fa fa-caret-right push-5-r"></i> Surat Jalan Amputasi</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2554'))){?>
				<li class="<?=($menu_kiri=='input_surat_diagnosa' || $menu_kiri=='his_surat_diagnosa' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_diagnosa"><i class="fa fa-caret-right push-5-r"></i> Surat Diagnosa</a></li>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2560'))){?>
				<li class="<?=($menu_kiri=='input_surat_skdp' || $menu_kiri=='his_surat_skdp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_konsul?>erm_surat/input_surat_skdp"><i class="fa fa-caret-right push-5-r"></i> Surat Ket. Dalam Perawatan</a></li>
				<?}?>
			</ul>
			
		</div>


	</div>
</div>
	<?php if (UserAccesForm($user_acces_form,array('2560'))){?>
	<?if ($menu_kiri=='input_surat_skdp'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_skdp');
	}?>
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('2554'))){?>
	<?if ($menu_kiri=='input_surat_diagnosa'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_diagnosa');
	}?>
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('2399'))){?>
	<?if ($menu_kiri=='input_surat_amputasi'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_amputasi');
	}?>
	<?}?>
	
	<?php if (UserAccesForm($user_acces_form,array('2344'))){?>
	<?if ($menu_kiri=='input_surat_external'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_external');
	}?>
	<?}?>
	
	
	<?php if (UserAccesForm($user_acces_form,array('2279'))){?>
	<?if ($menu_kiri=='input_surat_rawat'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_rawat');
	}?>
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('2273'))){?>
	<?if ($menu_kiri=='input_surat_lanjut'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_lanjut');
	}?>
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('2267'))){?>
	<?if ($menu_kiri=='input_surat_kontrol'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_kontrol');
	}?>
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('2258'))){?>
	<?if ($menu_kiri=='input_surat_sehat'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_sehat');
	}?>
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('2251'))){?>
	<?if ($menu_kiri=='input_surat_sakit'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_surat/input_surat_sakit');
	}?>
	<?}?>
	


