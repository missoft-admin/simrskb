<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_dpjp' ? 'block' : 'none') ?>">
	<!-- Music -->
	
	<div class="block animated fadeIn bg-gray" data-category="erm_dpjp">
		
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
				<?php 
					$url_utama_ri=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id_ranap.'/';
					$url_utama_rj=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/';
				?>
				<?if ($pendaftaran_id_ranap){?>
				<?if ($st_lihat_dpjp=='1'){?>
				<li class="<?=($menu_kiri=='input_dpjp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_dpjp/input_dpjp"><i class="fa fa-caret-right push-5-r"></i>Pencatatan DPJP</a></li>
				<?}?>
				<?}?>
				<?if ($st_lihat_pindah_dpjp=='1'){?>
				<?if ($st_ranap=='1'){?>
				<li class="<?=($menu_kiri=='input_pindah_dpjp' || $menu_kiri=='his_pindah_dpjp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_dpjp/input_pindah_dpjp"><i class="fa fa-caret-right push-5-r"></i>Pindah DPJP</a></li>
				<?}?>
				<?if ($st_ranap=='0'){?>
				<li class="<?=($menu_kiri=='input_pindah_dpjp_rj' || $menu_kiri=='his_pindah_dpjp_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_dpjp/input_pindah_dpjp_rj"><i class="fa fa-caret-right push-5-r"></i>Pindah DPJP</a></li>
				<?}?>
				<?}?>
				<?if ($st_lihat_permintaan_dpjp=='1'){?>
				<?if ($st_ranap=='1'){?>
				<li class="<?=($menu_kiri=='input_permintaan_dpjp' || $menu_kiri=='his_permintaan_dpjp' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_dpjp/input_permintaan_dpjp"><i class="fa fa-caret-right push-5-r"></i>Permintaan DPJP</a></li>
				<?}?>
				<?if ($st_ranap=='0'){?>
				<li class="<?=($menu_kiri=='input_permintaan_dpjp_rj' || $menu_kiri=='his_permintaan_dpjp_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_dpjp/input_permintaan_dpjp_rj"><i class="fa fa-caret-right push-5-r"></i>Permintaan DPJP</a></li>
				<?}?>
				<?}?>
				
			</ul>
			
		</div>
	</div>
	
	<!-- END Music -->
</div>
<?
if ($menu_kiri=='input_dpjp'){
	if ($st_lihat_dpjp=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_dpjp/input_dpjp');
	}
}
if ($menu_kiri=='input_pindah_dpjp' || $menu_kiri=='input_pindah_dpjp_rj'){
	if ($st_lihat_pindah_dpjp=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_dpjp/input_pindah_dpjp');
	}
}
if ($menu_kiri=='his_pindah_dpjp' || $menu_kiri=='his_pindah_dpjp_rj'){
	if ($st_lihat_pindah_dpjp=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_dpjp/his_pindah_dpjp');
	}
}
if ($menu_kiri=='input_permintaan_dpjp' || $menu_kiri=='input_permintaan_dpjp_rj'){
	if ($st_lihat_pindah_dpjp=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_dpjp/input_permintaan_dpjp');
	}
}
if ($menu_kiri=='his_permintaan_dpjp' || $menu_kiri=='his_permintaan_dpjp_rj'){
	if ($st_lihat_pindah_dpjp=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_dpjp/his_permintaan_dpjp');
	}
}


?>