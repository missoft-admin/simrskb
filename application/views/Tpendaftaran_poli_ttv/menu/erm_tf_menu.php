<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_tf' ? 'block' : 'none') ?>">
	<!-- Music -->
	
	<div class="block animated fadeIn bg-gray" data-category="erm_tf">
		
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
				<?
					$url_utama_trx_rj='';
					$url_utama_trx_ri='';
					if ($asalrujukan=='3'){
						$url_utama_trx_ri=base_url().'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id_ranap.'/';
						$st_ranap='1';
					}else{
						$st_ranap='0';
						// $url_utama_trx_rj=base_url().'tpoliklinik_trx/tindakan/'.$pendaftaran_id.'/0/';
						$url_utama_trx_rj=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/';
					}
				?>
				<?php 
					$url_utama_ri=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id_ranap.'/';
				?>
				<?if ($st_lihat_intra_hospital=='1'){?>
					<?if ($st_ranap=='1'){?>
						<li class="<?=($menu_kiri=='input_intra_hospital' || $menu_kiri=='his_intra_hospital' || $menu_kiri=='terima_intra_hospital' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_trx_ri?>erm_tf/input_intra_hospital"><i class="fa fa-caret-right push-5-r"></i> Transfer Intra Hospital </a></li>
					<?}else{?>
						<li class="<?=($menu_kiri=='input_intra_hospital_rj' || $menu_kiri=='his_intra_hospital_rj' || $menu_kiri=='terima_intra_hospital' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_trx_rj?>erm_tf/input_intra_hospital_rj"><i class="fa fa-caret-right push-5-r"></i> Transfer Intra Hospital </a></li>
					<?}?>
				<?}?>
				
				
				<?if ($st_lihat_tf_hospital=='1'){?>
					<?if ($st_ranap=='1'){?>
						<li class="<?=($menu_kiri=='input_tf_hospital' || $menu_kiri=='his_tf_hospital' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_trx_ri?>erm_tf/input_tf_hospital"><i class="fa fa-caret-right push-5-r"></i> Transfer Keluar </a></li>
					<?}else{?>
						<li class="<?=($menu_kiri=='input_tf_hospital_rj' || $menu_kiri=='his_tf_hospital_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_trx_rj?>erm_tf/input_tf_hospital_rj"><i class="fa fa-caret-right push-5-r"></i> Transfer Keluar </a></li>
					<?}?>
				<?}?>
				<?php if (UserAccesForm($user_acces_form,array('2222'))){ ?>
				<li class="<?=($menu_kiri=='input_pindah_bed' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_tf/input_pindah_bed"><i class="fa fa-caret-right push-5-r"></i> Transaksi Pindah Ruangan / Bed</a></li>
				<?}?>
				
			</ul>
			
		</div>
	</div>
	
	<!-- END Music -->
</div>
<?
if (UserAccesForm($user_acces_form,array('2222'))){ 
if ($menu_kiri=='input_pindah_bed'){
	$this->load->view('Tpendaftaran_poli_ttv/erm_tf/input_pindah_bed');
}
}
if ($menu_kiri=='input_intra_hospital' || $menu_kiri=='input_intra_hospital_rj' || $menu_kiri=='terima_intra_hospital'){
	if ($st_lihat_intra_hospital=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_tf/input_intra_hospital');
	}
}
if ($menu_kiri=='his_intra_hospital' || $menu_kiri=='his_intra_hospital_rj'){
	if ($st_lihat_intra_hospital=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_tf/his_intra_hospital');
	}
}
if ($menu_kiri=='input_tf_hospital' || $menu_kiri=='terima_tf_hospital' || $menu_kiri=='input_tf_hospital_rj' || $menu_kiri=='terima_tf_hospital_rj'){
	if ($st_lihat_tf_hospital=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_tf/input_tf_hospital');
	}
}
if ($menu_kiri=='his_tf_hospital'){
	if ($st_lihat_tf_hospital=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_tf/his_tf_hospital');
	}
}

?>