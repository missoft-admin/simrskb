<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_ews' ? 'block' : 'none') ?>">
	<!-- Music -->
	<div class="block animated fadeIn bg-gray" data-category="erm_ews">
		
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
				<?php 
					if ($asalrujukan=='3'){
						$st_ranap='1';
						$url_utama_ri=$base_url.'tews/tindakan/'.$pendaftaran_id_ranap.'/'.$st_ranap.'/';
					}else{
						$st_ranap='0';
						$url_utama_ri=$base_url.'tews/tindakan/'.$pendaftaran_id.'/'.$st_ranap.'/';
					}
				?>
				<?if ($st_lihat_ews=='1'){?>
				<li class="<?=($menu_kiri=='input_ews' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ews/input_ews"><i class="fa fa-caret-right push-5-r"></i>EWS Dewasa</a></li>
				<?}?>
				<?if ($st_lihat_pews=='1'){?>
				<li class="<?=($menu_kiri=='input_pews' || $menu_kiri=='his_pews' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_ews/input_pews"><i class="fa fa-caret-right push-5-r"></i>PEWS</a></li>
				<?}?>
				
			</ul>
			
		</div>
	</div>
	<!-- END Music -->
</div>
<?
if ($menu_kiri=='input_ews'){
	if ($st_lihat_ews=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_ews/input_ews');
	}
}
if ($menu_kiri=='input_pews'){
	if ($st_lihat_pews=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_ews/input_pews');
	}
}
if ($menu_kiri=='his_pews'){
	if ($st_lihat_pews=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_ews/his_pews');
	}
}
?>