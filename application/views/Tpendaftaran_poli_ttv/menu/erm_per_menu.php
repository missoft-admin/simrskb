<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_per' ? 'block' : 'none') ?>">
	<!-- Music -->
	
	
	<div class="block animated fadeIn bg-gray" data-category="erm_per">
		
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
				<?php 
					if ($pendaftaran_id_ranap){
						$url_utama_ri=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id_ranap.'/';
						$url_utama_rj=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/';
					}else{
						$url_utama_rj=$base_url.'tpendaftaran_ranap_erm/tindakan/'.$pendaftaran_id.'/';
					}
					
					if ($st_ranap=='1'){
						$url_ic=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id_ranap.'/erm_per/input_ic_ri';
					}else{
						$url_ic=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_per/input_ic';
						
					}
					if ($st_ranap=='1'){
						$url_icu=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id_ranap.'/erm_per/input_icu_ri';
					}else{
						$url_icu=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_per/input_icu';
						
					}
					if ($st_ranap=='1'){
						$url_isolasi=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id_ranap.'/erm_per/input_isolasi_ri';
					}else{
						$url_isolasi=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_per/input_isolasi';
						
					}
					if ($st_ranap=='1'){
						$url_dnr=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id_ranap.'/erm_per/input_dnr_ri';
					}else{
						$url_dnr=$base_url.'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/erm_per/input_dnr';
						
					}
				?>
				<?if ($st_lihat_ic=='1'){?>
				<li class="<?=($menu_kiri=='input_ic' || $menu_kiri=='his_ic' || $menu_kiri=='input_ic_ri' || $menu_kiri=='his_ic_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_ic?>"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Tin. Kedokteran</a></li>
				<?}?>
				
				
				<?if ($st_lihat_tindakan_anestesi=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_tindakan_anestesi_rj' || $menu_kiri=='input_tindakan_anestesi' || $menu_kiri=='his_tindakan_anestesi_rj' || $menu_kiri=='his_tindakan_anestesi'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_tindakan_anestesi"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Tin. Anestesi</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_tindakan_anestesi_rj' || $menu_kiri=='input_tindakan_anestesi' || $menu_kiri=='his_tindakan_anestesi_rj' || $menu_kiri=='his_tindakan_anestesi' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_tindakan_anestesi_rj"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Tin. Anestesi</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_risiko_tinggi=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_risiko_tinggi_rj' || $menu_kiri=='input_risiko_tinggi' || $menu_kiri=='his_risiko_tinggi_rj' || $menu_kiri=='his_risiko_tinggi'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_risiko_tinggi"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Risiko Tinggi</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_risiko_tinggi_rj' || $menu_kiri=='input_risiko_tinggi' || $menu_kiri=='his_risiko_tinggi_rj' || $menu_kiri=='his_risiko_tinggi' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_tindakan_anestesi_rj"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Risiko Tinggi</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_aps=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_aps'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_aps"><i class="fa fa-caret-right push-5-r"></i> Pulang Paksa (APS)</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_aps_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_aps_rj"><i class="fa fa-caret-right push-5-r"></i>  Pulang Paksa (APS)</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_info_ods=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_info_ods'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_info_ods"><i class="fa fa-caret-right push-5-r"></i> Informasi ODS</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_info_ods_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_info_ods_rj"><i class="fa fa-caret-right push-5-r"></i>  Informasi ODS</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_tf_darah=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_tf_darah_rj' || $menu_kiri=='input_tf_darah' || $menu_kiri=='his_tf_darah_rj' || $menu_kiri=='his_tf_darah'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_tf_darah"><i class="fa fa-caret-right push-5-r"></i> Pemberian Produk Darah</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_tf_darah_rj' || $menu_kiri=='input_tf_darah' || $menu_kiri=='his_tf_darah_rj' || $menu_kiri=='his_tf_darah' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_tindakan_anestesi_rj"><i class="fa fa-caret-right push-5-r"></i> Pemberian Produk Darah</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_invasif=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_invasif_rj' || $menu_kiri=='input_invasif' || $menu_kiri=='his_invasif_rj' || $menu_kiri=='his_invasif'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_invasif"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Invasif</a></li>

					<?}else{?>
						<li class="<?=($menu_kiri=='input_invasif_rj' || $menu_kiri=='input_invasif' || $menu_kiri=='his_invasif_rj' || $menu_kiri=='his_invasif' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_tindakan_anestesi_rj"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Invasif</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_icu=='1'){?>
				<li class="<?=($menu_kiri=='input_icu' || $menu_kiri=='his_icu' || $menu_kiri=='input_icu_ri' || $menu_kiri=='his_icu_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_icu?>"><i class="fa fa-caret-right push-5-r"></i> Informed Consent ICU / HCU</a></li>
				<?}?>
				<?if ($st_lihat_isolasi=='1'){?>
				<li class="<?=($menu_kiri=='input_isolasi' || $menu_kiri=='his_isolasi' || $menu_kiri=='input_isolasi_ri' || $menu_kiri=='his_isolasi_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_isolasi?>"><i class="fa fa-caret-right push-5-r"></i> Informed Consent Isolasi</a></li>
				<?}?>
				<?if ($st_lihat_dnr=='1'){?>
				<li class="<?=($menu_kiri=='input_dnr' || $menu_kiri=='his_dnr' || $menu_kiri=='input_dnr_ri' || $menu_kiri=='his_dnr_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_dnr?>"><i class="fa fa-caret-right push-5-r"></i> Penolakan Resusitasi (DNR)</a></li>
				<?}?>
				<?if ($st_lihat_rohani=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_rohani'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_rohani"><i class="fa fa-caret-right push-5-r"></i> Pelayanan Kerohanian</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_rohani_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_rohani_rj"><i class="fa fa-caret-right push-5-r"></i>  Pelayanan Kerohanian</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_privasi=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_privasi'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_privasi"><i class="fa fa-caret-right push-5-r"></i> Permintaan Privasi</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_privasi_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_privasi_rj"><i class="fa fa-caret-right push-5-r"></i>  Permintaan Privasi</a></li>
					
					<?}?>
				<?}?>
				<?if ($st_lihat_opinion=='1'){?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_opinion'  ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_per/input_opinion"><i class="fa fa-caret-right push-5-r"></i> Second Opinion</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_opinion_rj' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_per/input_opinion_rj"><i class="fa fa-caret-right push-5-r"></i>  Second Opinion</a></li>
					
					<?}?>
				<?}?>
			</ul>
			
		</div>
	</div>
	<!-- END Music -->
</div>

<? 
if ($st_lihat_opinion=='1'){
	if ($menu_kiri=='input_opinion' || $menu_kiri=='input_opinion_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_opinion');
	}
	if ($menu_kiri=='his_opinion' || $menu_kiri=='his_opinion_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_opinion');
	}
}
if ($st_lihat_privasi=='1'){
	if ($menu_kiri=='input_privasi' || $menu_kiri=='input_privasi_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_privasi');
	}
	if ($menu_kiri=='his_privasi' || $menu_kiri=='his_privasi_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_privasi');
	}
}

if ($st_lihat_ic=='1'){
	if ($menu_kiri=='input_ic' || $menu_kiri=='input_ic_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_ic');
	}
	if ($menu_kiri=='his_ic' || $menu_kiri=='his_ic_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_ic');
	}
}
?>
<? 
if ($st_lihat_icu=='1'){
	if ($menu_kiri=='input_icu' || $menu_kiri=='input_icu_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_icu');
	}
	if ($menu_kiri=='his_icu' || $menu_kiri=='his_icu_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_icu');
	}
}
?>
<? 
if ($st_lihat_isolasi=='1'){
	if ($menu_kiri=='input_isolasi' || $menu_kiri=='input_isolasi_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_isolasi');
	}
	if ($menu_kiri=='his_isolasi' || $menu_kiri=='his_isolasi_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_isolasi');
	}
}
?>
<? 
if ($st_lihat_dnr=='1'){
	if ($menu_kiri=='input_dnr' || $menu_kiri=='input_dnr_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/input_dnr');
	}
	if ($menu_kiri=='his_dnr' || $menu_kiri=='his_dnr_ri'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rj/his_dnr');
	}
}
?>

<?
if ($st_lihat_info_ods=='1'){
	if ($menu_kiri=='input_info_ods' || $menu_kiri=='input_info_ods_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_info_ods');
	}
	if ($menu_kiri=='his_info_ods' || $menu_kiri=='his_info_ods_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_info_ods');
	}
}
if ($st_lihat_invasif=='1'){
	if ($menu_kiri=='input_invasif' || $menu_kiri=='input_invasif_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_invasif');
	}
	if ($menu_kiri=='his_invasif' || $menu_kiri=='his_invasif_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_invasif');
	}
}
if ($st_lihat_rohani=='1'){
	if ($menu_kiri=='input_rohani' || $menu_kiri=='input_rohani_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_rohani');
	}
	if ($menu_kiri=='his_rohani' || $menu_kiri=='his_rohani_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_rohani');
	}
}
if ($st_lihat_aps=='1'){
	if ($menu_kiri=='input_aps' || $menu_kiri=='input_aps_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_aps');
	}
	if ($menu_kiri=='his_aps' || $menu_kiri=='his_aps_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_aps');
	}
}
if ($menu_kiri=='input_tindakan_anestesi' || $menu_kiri=='input_tindakan_anestesi_rj'){
	if ($st_lihat_tindakan_anestesi=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_tindakan_anestesi');
	}
}
if ($menu_kiri=='his_tindakan_anestesi' || $menu_kiri=='his_tindakan_anestesi_rj'){
	if ($st_lihat_tindakan_anestesi=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_tindakan_anestesi');
	}
}
if ($menu_kiri=='input_risiko_tinggi' || $menu_kiri=='input_risiko_tinggi_rj'){
	if ($st_lihat_risiko_tinggi=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_risiko_tinggi');
	}
}
if ($menu_kiri=='his_risiko_tinggi' || $menu_kiri=='his_risiko_tinggi_rj'){
	if ($st_lihat_risiko_tinggi=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_risiko_tinggi');
	}
}
if ($menu_kiri=='input_tf_darah' || $menu_kiri=='input_tf_darah_rj'){
	if ($st_lihat_tf_darah=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/input_tf_darah');
	}
}
if ($menu_kiri=='his_tf_darah' || $menu_kiri=='his_tf_darah_rj'){
	if ($st_lihat_tf_darah=='1'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_per/his_tf_darah');
	}
}

?>