<?php $asal_rujukan = (in_array($asal_rujukan_poli_ranap, [1, 2]) ? 'rawat_jalan' : 'rawat_inap'); ?>
<?php
if ('rawat_inap' === $asal_rujukan) {
    $idpendaftaran = $pendaftaran_id_ranap;
	} else {
		$idpendaftaran = $pendaftaran_id;
	}
?>

<?php $url_laboratorium_umum = site_url().'term_laboratorium_umum/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>
<?php $url_laboratorium_patologi_anatomi = site_url().'term_laboratorium_patologi_anatomi/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>
<?php $url_laboratorium_bank_darah = site_url().'term_laboratorium_bank_darah/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>

<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_lab' ? 'block' : 'none') ?>;">
	<div class="block animated fadeIn bg-gray" data-category="erm_laboratorium">
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
					<li class="<?= ($menu_kiri == 'laboratorium_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_laboratorium_umum; ?>erm_lab/laboratorium_permintaan"><i class="fa fa-caret-right push-5-r"></i>Laboratorium</a></li>
					<li class="<?= ($menu_kiri == 'patologi_anatomi_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_laboratorium_patologi_anatomi; ?>erm_lab/patologi_anatomi_permintaan"><i class="fa fa-caret-right push-5-r"></i>Patologi Anatomi</a></li>
					<li class="<?= ($menu_kiri == 'bank_darah_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_laboratorium_bank_darah; ?>erm_lab/bank_darah_permintaan"><i class="fa fa-caret-right push-5-r"></i>Bank Darah</a></li>
			</ul>
		</div>
	</div>
</div>

<?php
	if ($menu_kiri == 'laboratorium_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/laboratorium/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'laboratorium_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/laboratorium/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'laboratorium_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/laboratorium/split');
	}
?>

<?php 
	if ($menu_kiri == 'patologi_anatomi_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/patologi_anatomi/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'patologi_anatomi_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/patologi_anatomi/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'patologi_anatomi_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/patologi_anatomi/split');
	}
?>

<?php 
	if ($menu_kiri == 'bank_darah_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'bank_darah_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'bank_darah_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/split');
	}
?>