<!-- Add the category value for each item in its data-category attribute (for the filter functionality to work) -->
<div class="js-media-filter-items row bg-white">
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_rj_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_ri_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_lab_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_rad_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_trx_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/penjang_form')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_konsul_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_e_resep_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_rm_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_perencanaan_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_dpjp_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_site_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_rekon_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_tf_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_per_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_doc_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_hasil_penunjang_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_komunikasi')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_mpp_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_surat_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_bedah_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_survey_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_ews_menu')?>
    <?$this->load->view('Tpendaftaran_poli_ttv/menu/erm_observasi_menu')?>
</div>

<script type="text/javascript">
// $(document).on("click",".btn_close_left",function(){
// alert('Kiri');
// $(this).removeClass('btn_close_left').addClass('btn_close_right');
// $('.menu_kanan').removeClass('col-sm-10').addClass('col-sm-12');
// $(this).parent().prev(".menu_kiri").hide();
// $(this).parent().removeClass('col-sm-10').addClass('col-sm-12');
// console.log("kiri")
// });

$(document).on("click", ".btn_close_left", function() {
    // alert('Kanan');
    const aktif = $(".js-media-filter > li.active > a").data('category')
    $(this).removeClass('btn_close_left').addClass('btn_close_right');
    // $('.menu_kanan').removeClass('col-sm-12').addClass('col-sm-10');
    $(this).parent().removeClass('col-sm-10').addClass('col-sm-12');

    $('.js-media-filter-items').find(`div[data-category="${aktif}"]`).parent('.menu_kiri').hide()
    console.log("aktif", aktif)
    console.log("kiri")
});

$(document).on("click", ".btn_close_right", function() {
    // alert('Kanan');
    const aktif = $(".js-media-filter > li.active > a").data('category')
    $(this).removeClass('btn_close_right').addClass('btn_close_left');
    // $('.menu_kanan').removeClass('col-sm-12').addClass('col-sm-10');
    $(this).parent().removeClass('col-sm-12').addClass('col-sm-10');

    $('.js-media-filter-items').find(`div[data-category="${aktif}"]`).parent('.menu_kiri').show()
    console.log("aktif", aktif)
    console.log("kanan")
});
</script>
