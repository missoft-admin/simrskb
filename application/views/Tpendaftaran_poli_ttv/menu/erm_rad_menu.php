<?php $asal_rujukan = (in_array($asal_rujukan_poli_ranap, [1, 2]) ? 'rawat_jalan' : 'rawat_inap'); ?>
<?php
if ('rawat_inap' === $asal_rujukan) {
    $idpendaftaran = $pendaftaran_id_ranap;
	} else {
		$idpendaftaran = $pendaftaran_id;
	}
?>

<?php $url_radiologi_xray = site_url().'term_radiologi_xray/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>
<?php $url_radiologi_usg = site_url().'term_radiologi_usg/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>
<?php $url_radiologi_ctscan = site_url().'term_radiologi_ctscan/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>
<?php $url_radiologi_mri = site_url().'term_radiologi_mri/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>
<?php $url_radiologi_lainnya = site_url().'term_radiologi_lainnya/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>

<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_rad' ? 'block' : 'none') ?>;">
	<div class="block animated fadeIn bg-gray" data-category="erm_radiologi">
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
					<li class="<?= ($menu_kiri == 'xray_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_radiologi_xray; ?>erm_rad/xray_permintaan"><i class="fa fa-caret-right push-5-r"></i>X-Ray</a></li>
					<li class="<?= ($menu_kiri == 'usg_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_radiologi_usg; ?>erm_rad/usg_permintaan"><i class="fa fa-caret-right push-5-r"></i>USG</a></li>
					<li class="<?= ($menu_kiri == 'ctscan_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_radiologi_ctscan; ?>erm_rad/ctscan_permintaan"><i class="fa fa-caret-right push-5-r"></i>CT-Scan</a></li>
					<li class="<?= ($menu_kiri == 'mri_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_radiologi_mri; ?>erm_rad/mri_permintaan"><i class="fa fa-caret-right push-5-r"></i>MRI</a></li>
					<li class="<?= ($menu_kiri == 'lainnya_permintaan' ? 'active' : ''); ?> menu_click"><a href="<?= $url_radiologi_lainnya; ?>erm_rad/lainnya_permintaan"><i class="fa fa-caret-right push-5-r"></i>Lain-lain</a></li>
			</ul>
		</div>
	</div>
</div>

<?php
	if ($menu_kiri == 'xray_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/xray/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'usg_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'ctscan_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/ctscan/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'mri_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/mri/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'lainnya_permintaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/lainnya/permintaan');
	}
?>

<?php
	if ($menu_kiri == 'xray_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/xray/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'usg_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'ctscan_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/ctscan/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'mri_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/mri/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'lainnya_pemeriksaan'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/lainnya/pemeriksaan');
	}
?>

<?php
	if ($menu_kiri == 'xray_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/xray/split');
	}
?>

<?php
	if ($menu_kiri == 'usg_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/split');
	}
?>

<?php
	if ($menu_kiri == 'ctscan_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/ctscan/split');
	}
?>

<?php
	if ($menu_kiri == 'mri_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/mri/split');
	}
?>

<?php
	if ($menu_kiri == 'lainnya_split'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_rad/lainnya/split');
	}
?>