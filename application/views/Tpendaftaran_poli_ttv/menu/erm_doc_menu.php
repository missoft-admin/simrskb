<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_doc' ? 'block' : 'none') ?>">
	<!-- Music -->
	
	
	<div class="block animated fadeIn bg-gray" data-category="erm_doc">
		
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
				<?php 
					if ($pendaftaran_id_ranap){
						$url_utama_ri=$base_url.'tdokumen/upload/'.$pendaftaran_id_ranap.'/';
						$url_utama_rj=$base_url.'tdokumen/upload/'.$pendaftaran_id.'/';
					}else{
						$url_utama_rj=$base_url.'tdokumen/upload/'.$pendaftaran_id.'/';
					}
				?>
					<?if ($pendaftaran_id_ranap){?>
						<li class="<?=($menu_kiri=='input_upload_rj' || $menu_kiri=='input_upload_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_ri?>erm_doc/input_upload_ri"><i class="fa fa-caret-right push-5-r"></i> Upload Document</a></li>
			
					<?}else{?>
						<li class="<?=($menu_kiri=='input_upload_rj' || $menu_kiri=='input_upload_ri' ? 'active' : '') ?> menu_click"><a href="<?=$url_utama_rj?>erm_doc/input_upload_rj"><i class="fa fa-caret-right push-5-r"></i> Upload Document</a></li>
					
					<?}?>
				
			</ul>
			
		</div>
	</div>
	<!-- END Music -->
</div>
<?
if ($menu_kiri=='input_upload_ri' || $menu_kiri=='input_upload_rj'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_doc/input_upload');
}

?>