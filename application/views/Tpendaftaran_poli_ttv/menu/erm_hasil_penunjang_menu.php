<?php $asal_rujukan = (in_array($asal_rujukan_poli_ranap, [1, 2]) ? 'rawat_jalan' : 'rawat_inap'); ?>
<?php
if ('rawat_inap' === $asal_rujukan) {
    $idpendaftaran = $pendaftaran_id_ranap;
	} else {
		$idpendaftaran = $pendaftaran_id;
	}
?>

<?php $url_hasil_penunjang = site_url().'term_hasil_penunjang/tindakan/'.$asal_rujukan.'/'.$idpendaftaran.'/'; ?>

<div class="col-sm-2 menu_kiri" style="display: <?=($menu_atas=='erm_hasil_penunjang' ? 'block' : 'none') ?>;">
	<div class="block animated fadeIn bg-gray" data-category="erm_hasil_penunjang">
		<div class="push-10-t bg-gray-light">
			<ul class="nav nav-pills nav-stacked push">
					<li class="<?= ($menu_kiri == 'internal' ? 'active' : ''); ?> menu_click"><a href="<?= $url_hasil_penunjang; ?>erm_hasil_penunjang/internal"><i class="fa fa-caret-right push-5-r"></i>Hasil Penunjang Internal</a></li>
					<li class="<?= ($menu_kiri == 'eksternal' ? 'active' : ''); ?> menu_click"><a href="<?= $url_hasil_penunjang; ?>erm_hasil_penunjang/eksternal"><i class="fa fa-caret-right push-5-r"></i>Hasil Penunjang Eksternal</a></li>
			</ul>
		</div>
	</div>
</div>

<?php
	if ($menu_kiri == 'internal'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_hasil_penunjang/internal');
	}
?>

<?php 
	if ($menu_kiri == 'eksternal'){
		$this->load->view('Tpendaftaran_poli_ttv/erm_hasil_penunjang/eksternal');
	}
?>