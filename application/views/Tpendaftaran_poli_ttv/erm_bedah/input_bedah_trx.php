<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		
</style>

<?if ($menu_kiri=='input_bedah_trx'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_bedah_trx' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
	
		$waktumulaioperasi=HumanTime($waktumulaioperasi);
		$waktuselesaioperasi=HumanTime($waktuselesaioperasi);
		$tanggaloperasi=HumanDateShort($tanggaloperasi);
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_bedah">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1" onclick="load_transaksi_layanan_list()"><i class="si si-note"></i> TRANSAKSI  </a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					
					
					<?$this->load->view('Tpendaftaran_poli_ttv/erm_bedah/bedah_header'); ?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label class="control-label" for="tdpjp">Dokter Bedah</label>
									<select name="tdpjp" readonly id="tdpjp" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Semua Dokter</option>
										<?foreach ($dokterumum as $row){?>
											<option value="<?=$row->id?>" <?=($dpjp_id==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label class="control-label" for="tt">Tanggal Operasi </label>
									<div class="input-group date">
										<input type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggaloperasi" placeholder="HH/BB/TTTT" name="tanggaloperasi" value="<?= $tanggaloperasi ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label class="control-label" for="tt">Waktu Operasi </label>
									<div class=" input-group date">
										<input type="text" class="time-datepicker form-control form-control"id="waktumulaioperasi" value="<?=($waktumulaioperasi)?>">
										<span class="input-group-addon">
											<span class="fa fa-angle-right"></span>
										</span>
										<input type="text" class="time-datepicker form-control form-control" id="waktuselesaioperasi" value="<?=HumanTimeShort($waktuselesaioperasi)?>">
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label class="control-label" for="tdpjp">Diagnosa</label>
									<input type="text" class="form-control" id="diagnosa" value="{diagnosa}">
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12">
									<label class="control-label" for="tdpjp">Nama Tindakan Pembedahan</label>
									<input type="text" class="form-control" id="operasi" value="{operasi}">
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-6 ">
								<div class="col-md-5 ">
									<label class="control-label" for="kelompok_operasi_id">Kelompok Operasi</label>
									<select name="kelompok_operasi_id" id="kelompok_operasi_id" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelompok Operasi</option>
											<?foreach ($mkelompok_operasi as $row){?>
												<option value="<?=$row->id?>" <?=($kelompok_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
									</select>
								</div>
								<div class="col-md-3 ">
									<label class="control-label" for="jenis_operasi_id">Jenis Operasi</label>
									<select name="jenis_operasi_id" id="jenis_operasi_id" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Operasi</option>

										<?foreach(list_variable_ref(124) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="jenis_bedah">Jenis Pembedahan</label>
									<select name="jenis_bedah" id="jenis_bedah" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Pembedahan</option>
										<?foreach(list_variable_ref(389) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_bedah==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-4 ">
									<label class="control-label" for="jenis_anestesi_id">Jenis Anestesi</label>
									<select name="jenis_anestesi_id" id="jenis_anestesi_id" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Anesthesi</option>

										<?foreach(list_variable_ref(394) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_anestesi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="kelas_tarif_id">Kelas Tarif</label>
									<select name="kelas_tarif_id" id="kelas_tarif_id" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelas Tarif</option>
										<?foreach ($mkelas as $row){?>
											<option value="<?=$row->id?>" <?=($kelas_tarif_id==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="ruang_id">Ruang</label>
									<select name="ruang_id" id="ruang_id" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Ruang Operasi</option>
										<?foreach ($mruangan as $row){?>
											<option value="<?=$row->id?>" <?=($ruang_id==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-4 ">
									<label class="control-label" for="tarif_jenis_opr">Tarif Jenis Operasi</label>
									<select name="tarif_jenis_opr" id="tarif_jenis_opr" class="js-select2 form-control" style="width: 100%;">
											<option value="0">Pilih Tarif Jenis Operasi</option>
											<?foreach ($mjenis_operasi as $row){?>
												<option value="<?=$row->id?>" <?=($tarif_jenis_opr==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="jenis_operasi_id">Kelompok Diagnosa</label>
									<select id="kelompok_tindakan" class="js-select2 form-control" style="width: 100%;" multiple>
										<?foreach ($kelompok_tindakan_list as $row){?>
											<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="jenis_operasi_id">Kelompok Diagnosa</label>
									<select id="kelompok_diagnosa" class="js-select2 form-control" style="width: 100%;" multiple>
										<?foreach ($kelompok_diagnosa_list as $row){?>
											<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-4 ">
									<label class="control-label" for="konsulen_id">Dokter Konsulen</label>
									<select class="js-select2 form-control disabel" id="konsulen_id" name="konsulen_id[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
										<?foreach($dokterumum as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id, $tind_konsulen)?'selected':'')?>><?=$row->nama?></option>

										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="instrumen_id">Petugas Instrumen</label>
									<select class="js-select2 form-control disabel" id="instrumen_id" name="instrumen_id[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
										<?foreach($list_instrumen as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id, $tind_instrumen)?'selected':'')?>><?=$row->nama?></option>

										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="sirkuler_id">Perawat Sirkuler</label>
									<select class="js-select2 form-control disabel"  id="sirkuler_id" name="sirkuler_id[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
										<?foreach($list_instrumen as $row){?>
										<option value="<?=$row->id?>" <?=(in_array($row->id, $tind_sirkuler)?'selected':'')?>><?=$row->nama?></option>

										<?}?>
									</select>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label class="control-label" for="catatan">Catatan</label>
									<textarea class="form-control js-summernote-custom-height disabel" id="catatan" placeholder="Catatan" name="catatan" rows="5"><?=$catatan?></textarea>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<table style="width:100%">
									<tr>
										<td style="width:33%" class="text-center">YANG MENGAJUKAN JADWAL</td>
										<td style="width:33%" class="text-center">YANG MENYETUJUI JADWAL</td>
										<td style="width:33%" class="text-center">DOKTER PERUJUK</td>
									</tr>
									<tr>
										<td style="width:33%" class="text-center"><?=($user_pengaju?get_nama_user($user_pengaju):'')?></td>
										<td style="width:33%" class="text-center"><?=($user_menyetujui?get_nama_user($user_menyetujui):'')?></td>
										<td style="width:33%" class="text-center"><?=($nama_pengaju?$nama_pengaju:'')?></td>
									</tr>
									<tr>
										<td style="width:33%" class="text-center"><?=($tanggal_diajukan?HumanDateLong($tanggal_diajukan):'')?></td>
										<td style="width:33%" class="text-center"><?=($tanggal_disetujui?HumanDateLong($tanggal_disetujui):'')?></td>
										<td style="width:33%" class="text-center"><?=($nama_pengaju?HumanDateLong($tanggal_pengajuan):'')?></td>
									</tr>
									
								</table>
							</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-3">
									<label class="control-label" for="total_do">Total Dokter Operator</label>
									<input type="text" class="form-control" disabled id="total_do" value="<?=number_format($total_do)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_sewa_ruangan">Total Sewa Ruangan</label>
									<input type="text" class="form-control" disabled id="total_sewa_ruangan" value="<?=number_format($total_sewa_ruangan)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_fullcare">Total Full Care</label>
									<input type="text" class="form-control" disabled id="total_fullcare" value="<?=number_format($total_fullcare)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_da">Total Dokter Anestesi</label>
									<input type="text" class="form-control" disabled id="total_da" value="<?=number_format($total_da)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_daa">Total Assisten Anestesi</label>
									<input type="text" class="form-control" disabled id="total_daa" value="<?=number_format($total_daa)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_dao">Total Assisten Operator</label>
									<input type="text" class="form-control" disabled id="total_dao" value="<?=number_format($total_dao)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_narcose">Total Narcose</label>
									<input type="text" class="form-control" disabled id="total_narcose" value="<?=number_format($total_narcose)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_obat">Total Obat</label>
									<input type="text" class="form-control" disabled id="total_obat" value="<?=number_format($total_obat)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_alkes">Total Alkes</label>
									<input type="text" class="form-control" disabled id="total_alkes" value="<?=number_format($total_alkes)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_implan">Total Implant</label>
									<input type="text" class="form-control" disabled id="total_implan" value="<?=number_format($total_implan)?>">
								</div>
								<div class="col-md-3">
									<label class="control-label" for="total_sewa">Total Sewa Alat</label>
									<input type="text" class="form-control" disabled id="total_sewa" value="<?=number_format($total_sewa)?>">
								</div>
								
							</div>
							
						</div>
					</div>	
					
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>

<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	$("#waktumulaioperasi,#waktuselesaioperasi").datetimepicker({
		format: "HH:mm",
		stepping: 30
	});
	disable_edit();
});
$("#kelompok_operasi_id,#jenis_operasi_id,#jenis_bedah,#jenis_anestesi_id").change(function(){
	get_tarif();
});
function simpan_assesmen(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tkamar_bedah/simpan_tranaksi_bedah', 
		dataType: "JSON",
		method: "POST",
		data : {
				pendaftaran_bedah_id:$("#pendaftaran_bedah_id").val(),
				tdpjp : $("#tdpjp").val(),
				tanggaloperasi : $("#tanggaloperasi").val(),
				waktumulaioperasi : $("#waktumulaioperasi").val(),
				waktuselesaioperasi : $("#waktuselesaioperasi").val(),
				diagnosa : $("#diagnosa").val(),
				operasi : $("#operasi").val(),
				catatan : $("#catatan").val(),
				jenis_operasi_id : $("#jenis_operasi_id").val(),
				jenis_anestesi_id : $("#jenis_anestesi_id").val(),
				kelompok_operasi_id : $("#kelompok_operasi_id").val(),
				kelompok_tindakan_id : $("#kelompok_tindakan_id").val(),
				kelas_tarif_id : $("#kelas_tarif_id").val(),
				jenis_bedah : $("#jenis_bedah").val(),
				tarif_jenis_opr : $("#tarif_jenis_opr").val(),
				kelompok_tindakan : $("#kelompok_tindakan").val(),
				kelompok_diagnosa : $("#kelompok_diagnosa").val(),
				konsulen_id : $("#konsulen_id").val(),
				instrumen_id : $("#instrumen_id").val(),
				sirkuler_id : $("#sirkuler_id").val(),
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Gagal!",
					text: "Simpan Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
			}
		}
	});
}
function get_tarif(){
	let idkelompokpasien=$("#idkelompokpasien").val();
	let idrekanan=$("#idrekanan").val();
	let kelompok_operasi=$("#kelompok_operasi_id").val();
	let jenis_operasi=$("#jenis_operasi_id").val();
	let jenis_bedah=$("#jenis_bedah").val();
	let jenis_anestesi=$("#jenis_anestesi_id").val();
	
	$.ajax({
		url: '{site_url}tkamar_bedah/get_tarif/',
		dataType: "json",
		method: "POST",
		data : {
				idkelompokpasien:idkelompokpasien,
				idrekanan:idrekanan,
				kelompok_operasi:kelompok_operasi,
				jenis_operasi:jenis_operasi,
				jenis_bedah:jenis_bedah,
				jenis_anestesi:jenis_anestesi,
			},

		success: function(data) {
			$("#cover-spin").hide();
			if (data){
			$("#tarif_jenis_opr").val(data.tarif_jenis_opr).trigger('change');
				
			}else{
				$("#tarif_jenis_opr").val(0).trigger('change');
				
			}
		}
	});
}
</script>