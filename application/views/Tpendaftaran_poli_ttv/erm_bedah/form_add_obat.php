<div class="form-group" >
	<div class="col-md-6 ">
		<div class="col-md-4 ">
			<label for="idtipe">Tipe Barang</label>
			<select tabindex="4" id="idtipe"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
				<option value="" selected>Tipe Barang</option>
				<?foreach($list_tipe as $r){?>
				<option value="<?=$r->id?>" <?=($def_idtipe==$r->id?'selected':'')?>><?=$r->nama?></option>
				<?}?>
				
			</select>
			
		</div>
									
		<div class="col-md-8 ">
			<label for="idbarang">Barang</label>
			<div class="input-group">
				<select id="idbarang" style="width:100%"  tabindex="5" data-placeholder="Cari Obat" class="form-control"></select>
				<span class="input-group-btn">
					<button onclick="show_modal_obat()" title="Cari Obat" class="btn btn-primary" type="button" id="search_obat_1"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
	</div>
	<div class="col-md-6 ">
		<div class="col-md-4 ">
			<label for="satuan">Satuan</label>
			<input class="form-control" disabled type="text" id="satuan" value="">
		</div>
		<div class="col-md-4 ">
			<label for="hargadasar">Harga</label>
			<input class="form-control number" disabled type="text" id="harga_before_diskon" value="">
			<input class="form-control number" disabled type="hidden" id="hargadasar" value="">
			<input class="form-control number" disabled type="hidden" id="margin" value="">
			<input class="form-control number" disabled type="hidden" id="diskon" value="">
			<input class="form-control number" disabled type="hidden" id="diskon_persen" value="">
			<input class="form-control decimal" disabled type="hidden" id="pembulatan" value="">
			<input class="form-control number" disabled type="hidden" id="stok" value="">
		</div>
		<div class="col-md-4 ">
			<label for="hargajual">Harga - (Diskon)</label>
			
			<input class="form-control number" disabled type="text" id="hargajual" value="">
				
			
		</div>
	</div>
	
</div>
<div class="form-group">
	<div class="col-md-6 ">
		<div class="col-md-4 ">
			<label for="idtipe">Kuantitas</label>
			<input class="form-control number" type="text" id="kuantitas" value="">
			
		</div>
		<div class="col-md-8 ">
			<label for="totalkeseluruhan">Total</label>
			<div class="input-group">
				<input class="form-control number" type="text" disabled id="totalkeseluruhan" value="">
				<span class="input-group-btn">
					<button onclick="add_barang()" <?=($statuskasir_rajal!='0'?'disabled':'')?> title="Add barang" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Add </button>
					<button onclick="clear_narcose()" <?=($statuskasir_rajal!='0'?'disabled':'')?> title="Clear barang" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
				</span>
			</div>
		</div>
	</div>
	<div class="col-md-6 ">
		<div class="col-md-12 " id="div_info">
			<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<p id="lbl_info">Info :<strong> aa! </strong></p>
			</div>
		</div>
	</div>
</div>