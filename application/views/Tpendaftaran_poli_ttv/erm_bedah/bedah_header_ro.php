
<?
	
		$waktumulaioperasi=HumanTime($waktumulaioperasi);
		$waktuselesaioperasi=HumanTime($waktuselesaioperasi);
		$tanggaloperasi=HumanDateShort($tanggaloperasi);
?>

<div class="row">
	<div class="form-group">
		<div class="col-md-6 ">
			<div class="col-md-12 ">
				<label class="control-label" for="tdpjp">Dokter Bedah</label>
				<select name="tdpjp" disabled id="tdpjp" class="js-select2 form-control" style="width: 100%;">
					<option value="0">Semua Dokter</option>
					<?foreach ($dokterumum as $row){?>
						<option value="<?=$row->id?>" <?=($dpjp_id==$row->id?'selected':'')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="col-md-6 ">
			<div class="col-md-6 ">
				<label class="control-label" for="tanggaloperasi">Tanggal Operasi </label>
				<div class="input-group date">
					<input type="text" disabled class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggaloperasi" placeholder="HH/BB/TTTT" name="tanggaloperasi" value="<?= $tanggaloperasi ?>" required>
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>
			<div class="col-md-6 ">
				<label class="control-label" for="tt">Waktu Operasi </label>
				<div class=" input-group date">
					<input type="text" disabled class="time-datepicker form-control form-control"id="waktumulaioperasi" value="<?=($waktumulaioperasi)?>">
					<span class="input-group-addon">
						<span class="fa fa-angle-right"></span>
					</span>
					<input type="text" disabled class="time-datepicker form-control form-control" id="waktuselesaioperasi" value="<?=HumanTimeShort($waktuselesaioperasi)?>">
					<span class="input-group-addon"><i class="si si-clock"></i></span>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group" style="margin-top:-10px">
		<div class="col-md-6 ">
			<div class="col-md-12">
				<label class="control-label" for="tdpjp">Diagnosa</label>
				<input type="text" disabled class="form-control" id="diagnosa" value="{diagnosa}">
			</div>
		</div>
		<div class="col-md-6">
			<div class="col-md-12">
				<label class="control-label" for="tdpjp">Nama Tindakan Pembedahan</label>
				<input type="text" disabled class="form-control" id="operasi" value="{operasi}">
			</div>
		</div>
	</div>
	<div class="form-group" style="margin-top:-10px">
		<div class="col-md-6 ">
			<div class="col-md-5 ">
				<label class="control-label" for="kelompok_operasi_id">Kelompok Operasi</label>
				<select name="kelompok_operasi_id" disabled id="kelompok_operasi_id" class="js-select2 form-control" style="width: 100%;">
					<option value="0">Pilih Kelompok Operasi</option>
						<?foreach ($mkelompok_operasi as $row){?>
							<option value="<?=$row->id?>" <?=($kelompok_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
				</select>
			</div>
			<div class="col-md-3 ">
				<label class="control-label" for="jenis_operasi_id">Jenis Operasi</label>
				<select name="jenis_operasi_id" disabled id="jenis_operasi_id" class="js-select2 form-control" style="width: 100%;">
					<option value="0">Pilih Jenis Operasi</option>

					<?foreach(list_variable_ref(124) as $row){?>
						<option value="<?=$row->id?>" <?=($jenis_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
			<div class="col-md-4 ">
				<label class="control-label" for="jenis_bedah">Jenis Pembedahan</label>
				<select name="jenis_bedah" disabled id="jenis_bedah" class="js-select2 form-control" style="width: 100%;">
					<option value="0">Pilih Jenis Pembedahan</option>
					<?foreach(list_variable_ref(389) as $row){?>
						<option value="<?=$row->id?>" <?=($jenis_bedah==$row->id?'selected':'')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="col-md-6 ">
			<div class="col-md-4 ">
				<label class="control-label" for="jenis_anestesi_id">Jenis Anestesi</label>
				<select name="jenis_anestesi_id" disabled id="jenis_anestesi_id" class="js-select2 form-control" style="width: 100%;">
					<option value="0">Pilih Jenis Anesthesi</option>

					<?foreach(list_variable_ref(394) as $row){?>
						<option value="<?=$row->id?>" <?=($jenis_anestesi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
			<div class="col-md-4 ">
				<label class="control-label" for="kelas_tarif_id">Kelas Tarif</label>
				<select name="kelas_tarif_id" disabled id="kelas_tarif_id" class="js-select2 form-control" style="width: 100%;">
					<option value="0">Pilih Kelas Tarif</option>
					<?foreach ($mkelas as $row){?>
						<option value="<?=$row->id?>" <?=($kelas_tarif_id==$row->id?'selected':'')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
			<div class="col-md-4 ">
				<label class="control-label" for="ruang_id">Ruang</label>
				<select name="ruang_id" disabled id="ruang_id" class="js-select2 form-control" style="width: 100%;">
					<option value="0">Pilih Ruang Operasi</option>
					<?foreach ($mruangan as $row){?>
						<option value="<?=$row->id?>" <?=($ruang_id==$row->id?'selected':'')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		
	</div>
	<div class="form-group" style="margin-top:-10px">
		<div class="col-md-6 ">
			<div class="col-md-12 ">
				<label class="control-label" for="tarif_jenis_opr">Tarif Jenis Operasi</label>
				<select name="tarif_jenis_opr" disabled id="tarif_jenis_opr" class="js-select2 form-control" style="width: 100%;">
						<option value="0">Pilih Tarif Jenis Operasi</option>
						<?foreach ($mjenis_operasi as $row){?>
							<option value="<?=$row->id?>" <?=($tarif_jenis_opr==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
				</select>
			</div>
			
		</div>
		
	</div>
</div>