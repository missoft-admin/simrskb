<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		.total{
			font-size: 18px;
			position: relative;
		}
		.total_red{
			color:#d64d4d;
			font-size: 18px;
			position: relative;
		}
</style>

<?if ($menu_kiri=='input_bedah_da'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_bedah_da' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

	<div class="block animated fadeIn push-5-t" data-category="erm_bedah">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1" onclick="load_transaksi_layanan_list()"><i class="si si-note"></i> TRANSAKSI </a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					
					
					<?$this->load->view('Tpendaftaran_poli_ttv/erm_bedah/bedah_header'); ?>
					<?$this->load->view('Tpendaftaran_poli_ttv/erm_bedah/bedah_header_ro'); ?>
					
						
					<div class="row">
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12">
								<div class="col-md-12">
									<table id="tabel_ref_da" class="table table-striped table-bordered" style="margin-bottom: 0;">
										<thead>
											<tr>
												<th style="width: 60%;">DOKTER OPERATOR</th>
												<th style="width: 15%;">TOTAL OPERATOR</th>
												<th style="width: 10%;">PESENTASE %</th>
												<th style="width: 15%;">TOTAL ANESTHESI</th>

											</tr>
											<tr>
												<td>
													<?
														$q="
															SELECT H.namadokteroperator,J.ref as jenis,H.nama_tarif,H.opr_ke
															FROM tkamaroperasi_jasado H
															LEFT JOIN merm_referensi J ON J.nilai=H.jenis_operasi_id AND J.ref_head_id='124'

															WHERE H.idpendaftaranoperasi='$pendaftaran_bedah_id' AND H.`status`='1'
															ORDER BY H.opr_ke ASC
														";
														$hasil=$this->db->query($q)->result();
														$lbl_dokter_operator='';
														$persen_da=($persen_da?$persen_da:$da_persen);
														$total_o=0;
														foreach ($hasil as $row){
															$lbl_dokter_operator .='<li><strong> ('.$row->opr_ke.') '.$row->namadokteroperator.'</strong> <label class="text-primary">( '.$row->jenis.' )</label> - '.$row->nama_tarif.'</li>';
															
														}
														$lbl_dokter_operator='
															<ul>
																'.$lbl_dokter_operator.'
															</ul>
														';
														$total_da=$total_do * ($persen_da/100);
													?>
													<span ><?=$lbl_dokter_operator?></span>
												</td>
												<td><input type="text" name="total_o" id="total_o" class="form-control total_o number" value="<?=$total_do?>" readonly></td>
												<td><input type="text" name="persen_da" id="persen_da" <?=($da_edit_persen=='0'?'disabled':'')?> class="form-control persen disabel"  value="<?=($persen_da)?>" ></td>
												<td><input type="text" name="total_da" id="total_da" class="form-control number"  value="<?=$total_da?>" readonly></td>
											</tr>

										</thead>
									</table>
								</div>
							</div>
						</div>
						<? if (UserAccesForm($user_acces_form,array('2320'))){ ?>
						<?if ($statuskasir_rajal=='0'){?>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12">
								<div class="col-md-3 ">
									<label class="control-label" for="trx_iddokter">Dokter Anestesi</label>
									<select name="trx_iddokter"  id="trx_iddokter" class="js-select2 form-control" style="width: 100%;">
										<option value="0" selected>Pilih Dokter</option>
										<?foreach($list_dokter_anestesi as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-1 ">
									<label class="control-label" for="persen_dokter_da">(%)</label>
									<input type="text" name="persen_dokter_da" id="persen_dokter_da" class="form-control persen disabel"  value="0" >
								</div>
								<div class="col-md-2 ">
									<label class="control-label" for="sub_dokter_da">Jasa Medis Anestesi</label>
									<input type="text" name="sub_dokter_da" readonly id="sub_dokter_da" class="form-control number disabel"  value="0" >
								</div>
								<div class="col-md-3 ">
									<label for="hargajual">Discount</label>
									<div class="input-group">
										<input class="form-control decimal diskon" <?=($da_pilih_diskon=='0'?'disabled':'')?> type="text" id="diskon"  value="0" placeholder="%">
										<span class="input-group-addon">% / Rp.</span>
										<input class="form-control number diskon" <?=($da_pilih_diskon=='0'?'disabled':'')?> type="text" id="diskonRp" value="0" placeholder="Rp" >
									</div>
									
								</div>
								<div class="col-md-3">
									<label for="totalkeseluruhan">Total</label>
									<div class="input-group">
										<input class="form-control number" type="text" disabled id="totalkeseluruhan" value="">
										<span class="input-group-btn">
											<button onclick="add_tindakan()" title="Add barang" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Add </button>
											<button onclick="clear_layanan()" title="Clear barang" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</div>
								
							</div>
						</div>
						<?}?>
						<?}?>
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-12">
							<div class="col-md-12">
								<table id="tabel_da" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<input type="hidden" id="trx_id_tabel" value="" >
											<th style="width: 30%;" class="text-left">DOKTER ANESTHESI</th>
											<th style="width: 10%;" class="text-center">PERSENTASE</th>
											<th style="width: 15%;" class="text-right">JASA MEDIS ANESTHESI</th>
											<th style="width: 15%;" class="text-right">DISCOUNT</th>
											<th style="width: 15%;" class="text-right">TOTAL TARIF</th>
											<th style="width: 15%;" class="text-center">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									<tfoot id="foot-total-nonracikan">
										<tr>

											<th class="hidden-phone">
												<span class="pull-right total"><b>TOTAL JASA DOKTER ANESTHESI</b></span></th>
											<th class="text-center"><b><span id="lbl_total_persen_da" class="total_red">0</span></b></th>
											<th colspan="2">
													<input type="hidden" id="gt_da" name="gt_da" value="0">
													<input type="hidden" id="gt_persen_da" name="gt_persen_da" value="0">
											</th>
											<th class="text-right"><b><span id="lbl_total_da" class="total_red">0</span></b></th>
											<th class="text-right"></th>
											
										</tr>
									</tfoot>
							</table>
								
							</div>
							</div>
						</div>
					</div>	
					
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>

<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
	var gt_persen_da=$("#gt_persen_da").val();
	let sisa_persen=100-parseFloat(gt_persen_da);
	var persen_dokter_da=sisa_persen;
	var before_edit=$("#persen_da").val();
$(document).ready(function() {
	$("#waktumulaioperasi,#waktuselesaioperasi").datetimepicker({
		format: "HH:mm",
		stepping: 30
	});
	$('.number').number(true, 0, '.', ',');
	$('.persen').number(true, 2, '.', ',');
	$("#persen_dokter_da").val(persen_dokter_da);
	disable_edit();
	tarif_anesthesi();
	list_index_layanan();
	// total_tarif_anesthesi();
	// load_dokter_anestesi();
});
$(document).on("keyup", "#diskon", function() {
	var jasapelayanan = $("#sub_dokter_da").val();
	if ($("#diskon").val() == '') {
		$("#diskon").val(0)
	}
	if (parseFloat($(this).val()) > 100) {
		$(this).val(100);
	}

	var discount_rupiah = parseFloat(jasapelayanan * parseFloat($(this).val()) / 100);
	$("#diskonRp").val(discount_rupiah);
	tarif_anesthesi();
});
$(document).on("keyup", "#persen_da", function() {
	if (parseFloat($("#persen_da").val())>100){
		$("#persen_da").val(100);
	}
	let total_da=parseFloat($("#total_o").val()) * $("#persen_da").val()/100;
	$("#total_da").val(total_da);
	tarif_anesthesi();
});

$(document).on("keyup", "#diskonRp", function() {
	var jasapelayanan = $("#sub_dokter_da").val();
	if ($("#diskonRp").val() == '') {
		$("#diskonRp").val(0)
	}
	if (parseFloat($(this).val()) > jasapelayanan) {
		$(this).val(jasapelayanan);
	}

	var discount_percent = parseFloat((parseFloat($(this).val() * 100)) / jasapelayanan);
	$("#diskon").val(discount_percent);
	tarif_anesthesi();
});
$("#persen_dokter_da").keyup(function(){
	// if ($("#row_da").val() !=''){//EDIT
		// var batas=100-parseFloat($("#gt_persen_da").val())+parseFloat($("#persen_da_before").val());
	// }else{
		// var batas=100-parseFloat($("#gt_persen_da").val());
	// }
	// console.log(batas);
	if (parseFloat($("#persen_dokter_da").val())>sisa_persen){
		$("#persen_dokter_da").val(sisa_persen);
		$.toaster({priority : 'danger', title : 'Warning!', message : 'Melebihi 100%'});
	}

	tarif_anesthesi();

});
function tarif_anesthesi(){
	var total_o = parseFloat($("#total_da").val());
	var persen_da = parseFloat($("#persen_dokter_da").val());
	var total=total_o*persen_da/100;
	$("#sub_dokter_da").val(total);
	let totalkeseluruhan=parseFloat(total) - parseFloat($("#diskonRp").val());
	$("#totalkeseluruhan").val(totalkeseluruhan);
}
function edit(id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tkamar_bedah/get_edit_da/'+id,
		dataType: "json",
		success: function(data) {
			
			$("#trx_id_tabel").val(data.id);
			$("#trx_iddokter").val(data.iddokteranastesi).trigger('change.select2');
			$("#persen_dokter_da").val(data.persen);
			$("#sub_dokter_da").val(data.total_before_diskon);
			$("#diskonRp").val(data.diskon);
			if (data.diskon>0){
				let diskon=parseFloat(data.diskon/data.total_before_diskon)*100;
				$("#diskon").val(diskon);
			}else{
				$("#diskon").val(0);
				
			}
			$("#cover-spin").hide();
			$("#totalkeseluruhan").val(data.totalkeseluruhan);
			sisa_persen=100-parseFloat($("#gt_persen_da").val())+parseFloat(data.persen);
			console.log(sisa_persen);
		}
	});
}
function hapus(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkamar_bedah/hapus_da/'+id,
			dataType: "json",
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				clear_layanan();
			}
		});
	});


}
function hitung_total(){
	let total=parseFloat($("#jasasarana").val()) + parseFloat($("#jasapelayanan").val()) + parseFloat($("#bhp").val())+ parseFloat($("#biayaperawatan").val()) - parseFloat($("#diskonRp").val())
	$("#total").val(total);
	$("#totalkeseluruhan").val(total);
}

function add_tindakan(){
	
	if ($("#trx_iddokter").val()=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan Pilih Dokter",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#sub_dokter_da").val()=='0'){
		swal({
			title: "Gagal",
			text: "Tidak ada tarif yang tersedia",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tkamar_bedah/simpan_da/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				trx_id_tabel : $("#trx_id_tabel").val(),
				idpendaftaranoperasi : $("#pendaftaran_bedah_id").val(),
				iddokteroperator : $("#trx_iddokter").val(),
				namadokteroperator : $("#trx_iddokter option:selected").text(),
				total_acuan : $("#total_da").val(),
				persen : $("#persen_dokter_da").val(),
				diskon : $("#diskonRp").val(),
				totalkeseluruhan : $("#totalkeseluruhan").val(),
				jenis_operasi_id : $("#jenis_operasi_id").val(),

		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			  $("#cover-spin").hide();
			  // list_index_layanan();
			  clear_layanan();
			}
		});
}
function clear_layanan(){
	$("#trx_iddokter").val('0').trigger('change');
	
	list_index_layanan();
}
function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
function list_index_layanan(){
	var pendaftaran_bedah_id = $("#pendaftaran_bedah_id").val();
	var total_da = $("#total_da").val();
	var persen_da = $("#persen_da").val();
	$("#trx_id_tabel").val('');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tkamar_bedah/list_index_da/',
		type: "POST",
		dataType: 'json',
		data: {
			pendaftaran_bedah_id: pendaftaran_bedah_id,
			persen_da: persen_da,
			total_da: total_da,
		},
		success: function(data) {
			$('#tabel_da tbody').empty();
			$('#tabel_da tbody').append(data.tabel);
			$("#gt_persen_da").val((data.lbl_total_persen_da));
			$('#lbl_total_persen_da').text(formatNumber(data.lbl_total_persen_da)+' %');
			$('#lbl_total_da').text(formatNumber(data.totalkeseluruhan));
			$("#gt_da").val((data.totalkeseluruhan));
			sisa_persen=100-parseFloat(data.lbl_total_persen_da);
			console.log(sisa_persen);
			$("#persen_dokter_da").val(sisa_persen);
			tarif_anesthesi();
			$("#cover-spin").hide();
			disable_edit();
		}
	});
}
$("#persen_da").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$("#persen_da").blur(function(){
	if (before_edit != $(this).val()){
		$.ajax({
		  url: '{site_url}tkamar_bedah/update_persen_da/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				idpendaftaranoperasi : $("#pendaftaran_bedah_id").val(),
				persen_da : $("#persen_da").val(),
				total_o : $("#total_o").val(),

		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			  $("#cover-spin").hide();
			  // list_index_layanan();
			  clear_layanan();
			}
		});
	}
	 
});

</script>