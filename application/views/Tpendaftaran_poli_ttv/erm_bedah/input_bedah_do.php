<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		
</style>

<?if ($menu_kiri=='input_bedah_do'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_bedah_do' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

	<div class="block animated fadeIn push-5-t" data-category="erm_bedah">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1" onclick="load_transaksi_layanan_list()"><i class="si si-note"></i> TRANSAKSI </a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					
					
					<?$this->load->view('Tpendaftaran_poli_ttv/erm_bedah/bedah_header'); ?>
					<?$this->load->view('Tpendaftaran_poli_ttv/erm_bedah/bedah_header_ro'); ?>
					<? if (UserAccesForm($user_acces_form,array('2317'))){ ?>
					<?if ($statuskasir_rajal=='0'){?>	
					<div class="row">
						<div class="form-group" style="margin-top:-10px">
							<div class="col-md-6">
								<div class="col-md-6 ">
									<label class="control-label" for="trx_iddokter">Dokter</label>
									<select name="trx_iddokter"  id="trx_iddokter" class="js-select2 form-control" style="width: 100%;">
										<option value="0" selected>Pilih Dokter</option>

										<?foreach($dokterumum as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-2 ">
									<label class="control-label" for="trx_opr_ke">Operator Ke</label>
									<select name="trx_opr_ke" <?=($do_pilih_operator=='0'?'disabled':'')?> id="trx_opr_ke" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih</option>

										<?for($i=1;$i<10;$i++){?>
											<option value="<?=$i?>"><?=$i?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label class="control-label" for="trx_jenis_operasi_id">Jenis Operasi</label>
									<select name="trx_jenis_operasi_id" <?=($do_jenis_opr=='0'?'disabled':'')?> id="trx_jenis_operasi_id" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Operasi</option>

										<?foreach(list_variable_ref(124) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
								
							</div>
							<div class="col-md-6">
								<div class="col-md-3 ">
									<label class="control-label" for="trx_kelas_tarif_id">Kelas Tarif</label>
									<select name="trx_kelas_tarif_id" <?=($do_kelas_tarif=='0'?'disabled':'')?>  id="trx_kelas_tarif_id" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelas Tarif</option>
										<?foreach ($mkelas as $row){?>
											<option value="<?=$row->id?>" <?=($kelas_tarif_id==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
								<div class="col-md-9 ">
									<label class="control-label" for="id_tarif">Nama Tarif Jenis Operasi </label>
									<select name="id_tarif" <?=($do_nama_tarif=='0'?'disabled':'')?> id="id_tarif" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Tarif</option>
										<?foreach(get_all('mtarif_operasi',array('idtipe'=>3,'status'=>1)) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="jasasarana">Jasa Sarana</label>
									<input class="form-control number" disabled type="text" id="jasasarana" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="jasapelayanan">Jasa Pelayanan</label>
									<input class="form-control number" disabled type="text" id="jasapelayanan" value="">
								</div>
								<div class="col-md-4 ">
									<label for="hargajual">Jasa Pelayanan Disc</label>
									<div class="input-group">
										<input class="form-control decimal diskon" <?=($do_pilih_disc_operator=='0'?'disabled':'')?> type="text" id="diskon"  value="0" placeholder="%">
										<span class="input-group-addon">% / Rp.</span>
										<input class="form-control number diskon" <?=($do_pilih_disc_operator=='0'?'disabled':'')?> type="text" id="diskonRp" value="0" placeholder="Rp" >
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="hargadasar">BHP</label>
									<input class="form-control number" disabled type="text" id="bhp" value="">
									
								</div>
								<div class="col-md-2 ">
									<label for="hargadasar">Biaya Perawatan</label>
									<input class="form-control number" disabled type="text" id="biayaperawatan" value="">
									
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-3 ">
									<label for="idtipe">Tarif</label>
									<input class="form-control number" disabled type="text" id="total" value="">
									
								</div>
								
								<div class="col-md-3">
									<label for="totalkeseluruhan">Total</label>
									<div class="input-group">
										<input class="form-control number" type="text" disabled id="totalkeseluruhan" value="">
										<span class="input-group-btn">
											<button onclick="add_tindakan()" title="Add barang" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Add </button>
											<button onclick="clear_layanan()" title="Clear barang" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
						<?}?>
						<?}?>
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<table id="tabel_sr" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<input type="hidden" id="trx_id_tabel" value="" >
										<input type="hidden" id="ttarif_jenis_opr" value="" >
										<input type="hidden" id="total_do" value="<?=$total_do?>" >
										<input type="hidden" id="persen_da" value="<?=$persen_da?>" >
										<tr>
											<th style="width: 2%;">NO</th>
											<th style="width: 15%;" class="text-center">DOKTER</th>
											<th style="width: 5%;" class="text-center">KE-</th>
											<th style="width: 8%;" class="text-center">JENIS OPERASI</th>
											<th style="width: 5%;" class="text-center">KELAS TARIF</th>
											<th style="width: 10%;" class="text-center">NAMA TARIF</th>
											<th style="width: 8%;" class="text-right">Jasa Sarana</th>
											<th style="width: 8%;" class="text-right">Jasa Pelayanan</th>
											<th style="width: 5%;" class="text-right">BHP</th>
											<th style="width: 8%;" class="text-right">Biaya Perawatan</th>
											<th style="width: 8%;" class="text-right">Tarif</th>
											<th style="width: 8%;" class="text-right">Total</th>
											<th style="width: 8%;" class="text-center">Action</th>
										</tr>
										
									</thead>
									<tbody></tbody>
									<tfoot id="foot-total-nonracikan">
										<tr>

											<th colspan="9">
												<span class="pull-right total h5"><b>TOTAL </b></span></th>
											<th class="text-right">
												<b><span id="lbl_total_ruangan" class="h5 total">0</span></b>
												<input type="hidden" id="total_ruangan" name="total_ruangan" class="tarif_ruangan" value="0"> 
											</th>
											<?if($auto_input_sr=='0'){?>
											<th></th>
											<?}?>
										</tr>
									</tfoot>
							</table>
							</div>
							</div>
						</div>
					</div>	
					
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>

<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
let total_do=$("#total_do");
$(document).ready(function() {
	$("#waktumulaioperasi,#waktuselesaioperasi").datetimepicker({
		format: "HH:mm",
		stepping: 30
	});
	if ($("#statuskasir_rajal").val()=='0'){
		get_tarif();
	}
		disable_edit();
	list_index_do();
});
function edit(id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tkamar_bedah/get_edit_do/'+id,
		dataType: "json",
		success: function(data) {
			
			$("#trx_id_tabel").val(data.id);
			$("#id_tarif").val(data.id_tarif).trigger('change.select2');
			$("#trx_kelas_tarif_id").val(data.kelas_tarif).trigger('change.select2');
			$("#trx_jenis_operasi_id").val(data.jenis_operasi_id).trigger('change.select2');
			$("#trx_opr_ke").val(data.opr_ke).trigger('change.select2');
			$("#trx_iddokter").val(data.iddokteroperator).trigger('change.select2');
			$("#jasasarana").val(data.jasasarana);
			$("#jasapelayanan").val(data.jasapelayanan);
			$("#diskonRp").val(data.jasapelayanan_disc);
			if (data.jasapelayanan_disc>0){
				let diskon=parseFloat(data.jasapelayanan_disc/data.jasapelayanan)*100;
				$("#diskon").val(diskon);
			}else{
				$("#diskon").val(0);
				
			}
			$("#bhp").val(data.bhp);
			$("#biayaperawatan").val(data.biayaperawatan);
			$("#total").val(data.total);
			$("#totalkeseluruhan").val(data.totalkeseluruhan);
			
			$("#cover-spin").hide();
			console.log(data);
		}
	});
}
function hapus(id){
	let pendaftaran_bedah_id=$("#pendaftaran_bedah_id").val()
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkamar_bedah/hapus_do/'+id+'/'+pendaftaran_bedah_id,
			dataType: "json",
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				clear_layanan();
			}
		});
	});


}
function hitung_total(){
	let total=parseFloat($("#jasasarana").val()) + parseFloat($("#jasapelayanan").val()) + parseFloat($("#bhp").val())+ parseFloat($("#biayaperawatan").val()) - parseFloat($("#diskonRp").val())
	$("#total").val(total);
	$("#totalkeseluruhan").val(total);
}
$(document).on("keyup", "#diskon", function() {
	var jasapelayanan = $("#jasapelayanan").val();
	if ($("#diskon").val() == '') {
		$("#diskon").val(0)
	}
	if (parseFloat($(this).val()) > 100) {
		$(this).val(100);
	}

	var discount_rupiah = parseFloat(jasapelayanan * parseFloat($(this).val()) / 100);
	$("#diskonRp").val(discount_rupiah);
	hitung_total();
});

$(document).on("keyup", "#diskonRp", function() {
	var jasapelayanan = $("#jasapelayanan").val();
	if ($("#diskonRp").val() == '') {
		$("#diskonRp").val(0)
	}
	if (parseFloat($(this).val()) > jasapelayanan) {
		$(this).val(jasapelayanan);
	}

	var discount_percent = parseFloat((parseFloat($(this).val() * 100)) / jasapelayanan);
	$("#diskon").val(discount_percent);
	hitung_total();
});
function add_tindakan(){
	
	if ($("#trx_iddokter").val()=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan Pilih Dokter",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#trx_opr_ke").val()=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan Pilih Operator Ke-",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#trx_jenis_operasi_id").val()=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan Pilih Jenis Operasi",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#id_tarif").val()=='0'){
		swal({
			title: "Gagal",
			text: "Silahkan Pilih Tarif",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tkamar_bedah/simpan_do/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				trx_id_tabel : $("#trx_id_tabel").val(),
				idpendaftaranoperasi : $("#pendaftaran_bedah_id").val(),
				id_tarif : $("#id_tarif").val(),
				kelas_tarif : $("#trx_kelas_tarif_id").val(),
				nama_tarif : $("#id_tarif option:selected").text(),
				jasasarana : $("#jasasarana").val(),
				jasasarana_disc : 0,
				jasapelayanan : $("#jasapelayanan").val(),
				jasapelayanan_disc : $("#diskonRp").val(),
				bhp : $("#bhp").val(),
				bhp_disc : 0,
				biayaperawatan : $("#biayaperawatan").val(),
				biayaperawatan_disc : 0,
				total : $("#total").val(),
				diskon : 0,
				totalkeseluruhan : $("#totalkeseluruhan").val(),
				jenis_operasi_id : $("#trx_jenis_operasi_id").val(),
				opr_ke : $("#trx_opr_ke").val(),
				iddokteroperator : $("#trx_iddokter").val(),
				namadokteroperator : $("#trx_iddokter option:selected").text(),

		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			  $("#cover-spin").hide();
			  list_index_do();
			}
		});
}
function clear_layanan(){
	list_index_do();
}
function load_tarif_ruangan(){
	var jenis_operasi_id = $("#ttarif_jenis_opr").val();
	var kelas_tarif_id = $("#trx_kelas_tarif_id").val();
	
	$.ajax({
		url: '{site_url}tkamar_bedah/load_tarif_do/',
		type: "POST",
		dataType: 'json',
		data: {
			jenis_operasi_id: jenis_operasi_id,
			kelas_tarif_id: kelas_tarif_id,

		},
		success: function(data) {
			if (data=='error') {
				$("#id_tarif").val(0).trigger('change.select2');;
				$("#jasasarana").val(0);
				$("#jasapelayanan").val(0);
				$("#bhp").val(0);
				$("#biayaperawatan").val(0);
				$("#total").val(0);
				$("#totalkeseluruhan").val(0);
			}else{
				$("#id_tarif").val(data.id).trigger('change.select2');;
				$("#jasasarana").val(data.jasasarana);
				$("#jasapelayanan").val(data.jasapelayanan);
				$("#bhp").val(data.bhp);
				$("#biayaperawatan").val(data.biayaperawatan);
				$("#total").val(data.total);
				$("#totalkeseluruhan").val(data.total);
			}
			$('.number').number(true, 0, '.', ',');
			
			// $("#lbl_total_ruangan").text(formatNumber($(".tarif_ruangan").val()));
			// $("#lbl_total_da").text(formatNumber(total_grand));
			// clear_do();

			// console.log(data);
		}
	});
}
function load_tarif_single(){
	var id_tarif = $("#id_tarif").val();
	var kelas_tarif_id = $("#trx_kelas_tarif_id").val();
	
	$.ajax({
		url: '{site_url}tkamar_bedah/load_tarif_do_single/',
		type: "POST",
		dataType: 'json',
		data: {
			id_tarif: id_tarif,
			kelas_tarif_id: kelas_tarif_id,

		},
		success: function(data) {
			if (data=='error') {
				// $("#id_tarif").val(0).trigger('change.select2');;
				$("#jasasarana").val(0);
				$("#jasapelayanan").val(0);
				$("#bhp").val(0);
				$("#biayaperawatan").val(0);
				$("#total").val(0);
				$("#totalkeseluruhan").val(0);
			}else{
				// $("#id_tarif").val(data.id).trigger('change.select2');;
				$("#jasasarana").val(data.jasasarana);
				$("#jasapelayanan").val(data.jasapelayanan);
				$("#bhp").val(data.bhp);
				$("#biayaperawatan").val(data.biayaperawatan);
				$("#total").val(data.total);
				$("#totalkeseluruhan").val(data.total);
				hitung_total();
			}
			$('.number').number(true, 0, '.', ',');
			// $("#lbl_total_ruangan").text(formatNumber($(".tarif_ruangan").val()));
			// $("#lbl_total_da").text(formatNumber(total_grand));
			// clear_do();

			// console.log(data);
		}
	});
}
function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
function list_index_do(){
	var pendaftaran_bedah_id = $("#pendaftaran_bedah_id").val();
	var auto_input_sr = $("#auto_input_sr").val();
	$("#trx_id_tabel").val('');
	$.ajax({
		url: '{site_url}tkamar_bedah/list_index_do/',
		type: "POST",
		dataType: 'json',
		data: {
			pendaftaran_bedah_id: pendaftaran_bedah_id,
			auto_input_sr: auto_input_sr,
		},
		success: function(data) {
			$('#tabel_sr tbody').empty();
			$('#tabel_sr tbody').append(data.tabel);
			$('#total_ruangan').text(data.totalkeseluruhan);
			if (total_do != data.totalkeseluruhan){
				$('#total_do').val(data.totalkeseluruhan);
				total_do=data.totalkeseluruhan;
				update_total_do();
			}
			$("#lbl_total_ruangan").text(formatNumber(data.totalkeseluruhan));
			disable_edit();
		}
	});
}
function update_total_do(){
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}tkamar_bedah/update_persen_da/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			idpendaftaranoperasi : $("#pendaftaran_bedah_id").val(),
			persen_da : $("#persen_da").val(),
			total_o : $("#total_do").val(),

	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		  $("#cover-spin").hide();
		  // list_index_layanan();
		  clear_layanan();
		}
	});
}
// $("#trx_kelas_tarif_id,#trx_jenis_operasi_id").change(function(){
	// load_tarif_ruangan();
// });
$("#id_tarif").change(function(){
	load_tarif_single();
});
$("#trx_jenis_operasi_id,#trx_kelas_tarif_id").change(function(){
	get_tarif();
});
function get_tarif(){
	let idkelompokpasien=$("#idkelompokpasien").val();
	let idrekanan=$("#idrekanan").val();
	let kelompok_operasi=$("#kelompok_operasi_id").val();
	let jenis_operasi=$("#trx_jenis_operasi_id").val();
	let jenis_bedah=$("#jenis_bedah").val();
	let jenis_anestesi=$("#jenis_anestesi_id").val();
	
	$.ajax({
		url: '{site_url}tkamar_bedah/get_tarif/',
		dataType: "json",
		method: "POST",
		data : {
				idkelompokpasien:idkelompokpasien,
				idrekanan:idrekanan,
				kelompok_operasi:kelompok_operasi,
				jenis_operasi:jenis_operasi,
				jenis_bedah:jenis_bedah,
				jenis_anestesi:jenis_anestesi,
			},

		success: function(data) {
			$("#cover-spin").hide();
			if (data){
				$("#ttarif_jenis_opr").val(data.tarif_jenis_opr);
				load_tarif_ruangan();
			}else{
				$("#id_tarif").val(0).trigger('change');;
				$("#ttarif_jenis_opr").val(0);
			}
		}
	});
}
</script>