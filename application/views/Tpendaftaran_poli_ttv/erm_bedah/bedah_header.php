<div class="row">
	<input type="hidden" id="pendaftaran_bedah_id" value="{pendaftaran_bedah_id}" >		
	<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}" >		
	<input type="hidden" id="auto_input_sr" value="{auto_input_sr}" >		
	<input type="hidden" id="auto_input_fc" value="{auto_input_fc}" >	
	<input type="hidden" id="statuskasir_rajal" value="{statuskasir_rajal}" >		
	<input type="hidden" id="idrekanan" value="<?=($idkelompokpasien!='1'?0:$idrekanan)?>" >		
	
	<div class="row">
		<div class="col-md-12">
			<h4 class="font-w700 push-5 text-center text-primary">{judul_form}</h4>
			<h5 class="font-w700 push-5 text-center text-danger"><i>{notransaksi}</i></h5>
		</div>
	</div>
	<?if ($statuskasir_rajal=='0'){?>
	<div class="form-group">
		<div class="col-md-4 ">
		</div>
		<div class="col-md-8 ">
			<div class="pull-right push-10-r">
				<? if (UserAccesForm($user_acces_form,array('2309'))){ ?>
				<button class="btn btn-primary btn-sm" onclick="simpan_assesmen()" type="button"><i class="fa fa-save"></i> SIMPAN</button>
				<?}?>
				<? if (UserAccesForm($user_acces_form,array('2310'))){ ?>
				<button class="btn btn-warning btn-sm" onclick="pilih_ko(<?=$pendaftaran_id_ranap?>,<?=$pendaftaran_bedah_id?>)" type="button"><i class="fa fa-folder-open-o"></i> BROWSE DATA</button>
				<?}?>
				
				
			</div>
			
		</div>
	</div>
	<?}?>
</div>

<script type="text/javascript">

function disable_edit(){
	if ($("#statuskasir_rajal").val()=='2'){
		
		$("#form1 :input").prop("disabled", true);
	}
}
</script>