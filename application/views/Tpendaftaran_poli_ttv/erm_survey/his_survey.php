<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_survey'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_survey' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						$tglsurvey=HumanDateShort($tanggal_survey);
						$waktusurvey=HumanTime($tanggal_survey);

						$tanggaldaftar_asal=HumanDateShort($tanggal_input_asal);
						$waktudaftar_asal=HumanTime($tanggal_input_asal);
						$tglsurvey_asal=HumanDateShort($tanggal_survey_asal);
						$waktusurvey_asal=HumanTime($tanggal_survey_asal);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tglsurvey=date('d-m-Y');
						$waktusurvey=date('H:i:s');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_sga=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_survey">
	<?if ($st_lihat_sga=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_sga=='1'){?>
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
									<h5 class="font-w700 push-5 text-center text-primary"><i>RIWAYAT {judul_header_eng}</i></h5>
								</div>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									<button class="btn btn-default btn_ttd menu_click" type="button" onclick="goBack()"><i class="fa fa-reply"></i> Kembali</button>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_survey/input_sga" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="tglsurvey">Waktu Survery</label>
									<div class="input-group date">
										<input type="text" class="js-datepicker form-control <?=($tglsurvey!=$tglsurvey_asal?'edited':'')?>" id="tglsurvey"  data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tglsurvey" value="<?= $tglsurvey ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-2 ">
									<label for="waktusurvey">&nbsp;</label>
									<div class="input-group">
										<input tabindex="3" type="text" class="time-datepicker form-control <?=($waktusurvey!=$waktusurvey_asal?'edited':'')?>" id="waktusurvey" name="waktusurvey" value="<?= $waktusurvey ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-8 ">
									<label for="waktusurvey">Profile</label>
									<div class="input-group">
									<select id="jenis_ttd" class="<?=($jenis_ttd!=$jenis_ttd_asal?'edited':'')?> form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($jenis_ttd==''?'selected':'0')?>>- Pilih yang Bertandatangan -</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Penanggung Jawab</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?>>Pengantar</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?>>Pasien Sendiri</option>
										<option value="4" <?=($jenis_ttd=='4'?'selected':'')?>>Lainnya</option>
										
									</select>
									<span class="input-group-btn">
										<button class="btn btn-default  btn_load" onclick="load_profile()" type="button"><i class="fa fa-refresh"></i> load data</button>
									</span>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group" style="margin-top:10px!important">
							<div class="col-md-12 ">
								<div class="col-md-4 ">
									<label for="nama_profile"><?=$profile_nama_ina?> / <i><?=$profile_nama_eng?></i></label>
									<input class="form-control <?=($nama_profile!=$nama_profile_asal?'edited':'')?>" id="nama_profile" type="text"  value="<?=$nama_profile?>"  placeholder="{profile_nama_ina}">
								</div>
								<div class="col-md-4 ">
									<label for="ttl_profile"><?=$ttl_profile_ina?> / <i><?=$ttl_profile_eng?></i></label>
									<div class="input-group date">
										<input type="text" class="js-datepicker form-control <?=($ttl_profile!=$ttl_profile_asal?'edited':'')?>" id="ttl_profile" data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" value="<?= ($ttl_profile?HumanDateShort($ttl_profile):'') ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-4 ">
									<label for="nama_profile"><?=$umur_profile_ina?> / <i><?=$umur_profile_ina?></i></label>
									<input class="form-control <?=($umur_profile!=$umur_profile_asal?'edited':'')?>" id="umur_profile" type="text"  value="<?=$umur_profile?>"  placeholder="{umur_profile_ina}">
								</div>
							</div>
						</div>
						<div class="form-group" style="margin-top:10px!important">
							<div class="col-md-12 ">
								<div class="col-md-4 ">
									<label for="nama_profile"><?=$jk_profile_ina?> / <i><?=$jk_profile_eng?></i></label>
									<select class="<?=($jk_profile != $jk_profile_asal?'edited':'')?> form-control opsi_change" id="jk_profile" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="" <?=($jk_profile == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(1) as $row){?>
										<option value="<?=$row->id?>" <?=($jk_profile == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="nama_profile"><?=$pendidikan_profile_ina?> / <i><?=$pendidikan_profile_eng?></i></label>
									<select class="<?=($pendidikan_profile != $pendidikan_profile_asal?'edited':'')?> form-control opsi_change" id="pendidikan_profile" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="" <?=($pendidikan_profile == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(13) as $row){?>
										<option value="<?=$row->id?>" <?=($pendidikan_profile == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="nama_profile"><?=$pekerjaan_profile_ina?> / <i><?=$pekerjaan_profile_eng?></i></label>
									<select class="<?=($pekerjaan_profile != $pekerjaan_profile_asal?'edited':'')?> form-control opsi_change" id="pekerjaan_profile" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="" <?=($pekerjaan_profile == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(6) as $row){?>
										<option value="<?=$row->id?>" <?=($pekerjaan_profile == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group" style="margin-top:10px!important">
							<div class="col-md-12 ">
								<div class="col-md-4 ">
									<label for="nama_profile"><?=$mengetahui_rs_ina?> / <i><?=$mengetahui_rs_eng?></i></label>
									<select class="<?=($mengetahui_rs != $mengetahui_rs_asal?'edited':'')?> form-control opsi_change" id="mengetahui_rs" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="" <?=($mengetahui_rs == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(420) as $row){?>
										<option value="<?=$row->id?>" <?=($mengetahui_rs == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="lainnya_profile"><?=$mengetahui_lain_ina?> / <i><?=$mengetahui_lain_eng?></i></label>
									<input class="form-control <?=($lainnya_profile!=$lainnya_profile_asal?'edited':'')?>" id="lainnya_profile" type="text"  value="<?=$lainnya_profile?>"  placeholder="{mengetahui_lain_ina}">
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group" >
									<table class="block-table text-center ">
										<tbody>
											<tr>
												<td class="border-r" style="width:70%">
													<div>
														<div class="input-group">
															<input class="form-control" disabled type="text" readonly value="<?=$nama_kajian?>" id="nama_kajian" placeholder="nama_kajian" >
															<span class="input-group-addon"><i class="fa fa-user"></i></span>
														</div>
													</div>
													<div class="h5 font-w700 text-left text-primary push-5-t"><?=$isi_header?></div>
												</td>
												<td class="border-r"  style="width:30%">
													<div class="h1 font-w700 <?=($total_skor_survey_kepuasan != $total_skor_survey_kepuasan_asal?'text-danger':'text-primary')?>" id="div_total_skor_survey_kepuasan"><?=$total_skor_survey_kepuasan?></div>
													<div class="h5 text-uppercase push-5-t">Total Penilaian</div>
												</td>
											</tr>
										</tbody>
									</table>
									
								</div>
								
							</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<table class="table table-bordered" id="tabel_non_nrs">
											<thead>
												<tr>
													<th width="10%">No</th>
													<th width="40%">Pertanyaan</th>
													<th width="30%">Jawaban</th>
													<th width="20%" hidden>Skor</th>
												</tr>
											</thead>
											<tbody>
												<!-- {{ @action }} -->
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label class="text-primary" ><?=$isi_footer?></h5>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								
								<label for="lainnya_profile"><?=$kritik_ina?> / <i><?=$kritik_eng?></i> <?=($kritik != $kritik_asal?text_danger('BERUBAH'):'')?></label>
								<textarea class="form-control js-summernote auto_blur" id="kritik"><?=$kritik?></textarea>
							</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-8 ">
									
								</div>
								<div class="col-md-4 ">
									<?if ($ttd){?>
										<div class="img-container fx-img-rotate-r text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd?>" alt="" title="">
											<div class="img-options">
												<div class="img-options-content">
													<div class="btn-group btn-group-sm">
														<a class="btn btn-default" onclick="modal_ttd()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
														<a class="btn btn-default btn-danger" onclick="hapus_ttd('ttd_pernyataan')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
													</div>
												</div>
											</div>
										</div>
										<p class="text-center nama_ttd" style="text-align: center;"><?=$nama_profile?></p>
									<?}else{?>
										<button class="btn btn-sm btn-success" onclick="modal_ttd()" id="btn_ttd_lm" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
									<?}?>
								</div>
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;


$(document).ready(function() {
	disabel_edit();
	$(".decimal").number(true,2,'.',',');
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	$('.js-summernote').summernote({
		  height: 100,   //set editable area's height
			dialogsInBody: true,
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
		
	if (assesmen_id){
		list_index_history_edit();
		load_nilai_param();
		load_awal_assesmen=false;
	}
});
function load_nilai_param(){
	assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_nilai_param_survey_kepuasan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				versi_edit:versi_edit,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#tabel_non_nrs tbody").empty();
			$("#tabel_non_nrs tbody").append(data.tabel);
			// $(".nilai_survey_kepuasan").select2();
			$("#total_skor_survey_kepuasan").val(data.total_skor);
			$("#div_total_skor_survey_kepuasan").text(data.total_skor);
			disabel_edit();
		}
	});
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_survey', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}

</script>