<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	
	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
	.center {
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  width: 50%;
	}
</style>

<?if ($menu_kiri=='input_surat_amputasi'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_surat_amputasi' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_permintaan;
	if ($assesmen_id){
		$tglpendaftaran=HumanDateShort($tanggal_input);
		$waktupendaftaran=HumanTime($tanggal_input);
		
	}else{
		$tglpendaftaran=date('d-m-Y');
		$waktupendaftaran=date('H:i:s');
		
	}
	
	?>
	<div class="block animated fadeIn push-5-t" data-category="erm_surat">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Surat Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_surat_amputasi()" ><i class="fa fa-send"></i> Daftar Surat </a>
				</li>
				<?php if (UserAccesForm($user_acces_form,array('2402'))){?>
				<li class="">
					<a href="#tab_3" onclick="list_surat_amputasi_history()"><i class="fa fa-history"></i> Riwayat Surat</a>
				</li>
				<?}?>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
					
					<div class="row">
						<input type="hidden" id="idtipe_poli" value="{idtipe_poli}"> 
						<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}"> 
						<input type="hidden" id="idpoliklinik" value="{idpoliklinik}"> 
						<input type="hidden" id="idrekanan" value="{idrekanan}"> 
						<input type="hidden" id="iddokter" value="{iddokter}"> 
						<input type="hidden" id="assesmen_id" value="{assesmen_id}"> 
					

						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="assesmen_detail_id" value="" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_edited" value="<?=$st_edited?>" >		
						<input type="hidden" id="st_ranap" value="<?=($st_ranap)?>"> 
						
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-4 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text"  class="js-datepicker form-control " data-date-format="dd-mm-yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tglpendaftaran ?>" required>
										<label for="tglpendaftaran">Tanggal <i class="text-muted">Created Date</i></label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-4 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text"   class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktupendaftaran ?>" required>
										<label for="waktupendaftaran">Waktu <i class="text-muted">Time</i></label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-12">
								<h4 class="font-w700 push-5 text-center text-primary">{judul_ina}</h4>
								<?if($judul_eng){?>
									<h5 class="push-5 text-center">{judul_eng}</h5>
								<?}?>
							
						</div>
					</div>
					
					<div class="row">
					
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' ){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Surat Belum Disimpan</a>!</p>
									</div>
								<?}?>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?php if (UserAccesForm($user_acces_form,array('2400'))){?>
									<?if ($assesmen_id==''){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-success" onclick="close_assesmen()" type="button"><i class="fa fa-send"></i> Simpan Surat</button>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										<?php if (UserAccesForm($user_acces_form,array('2403'))){?>
										<?if ($assesmen_id!=''){?>
										<a href="<?=site_url()?>tsurat/show_cetak/<?=$assesmen_id?>/7/0" target="_blank" class="btn btn-info btn_cetak" type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										<?}?>
										
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$paragraf_1_ina?><br><i class="text-muted"><?=$paragraf_1_eng?></i></label>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$nama_pasien_ina?><br><i class="text-muted"><?=$nama_pasien_eng?></i></label>
										<input disabled type="text" class="form-control  " value="<?= $no_medrec.' - '.$namapasien.' - '.HumanDateShort($tanggal_lahir).' - '.$umurtahun ?> Tahun" >
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$alamat_pasien_ina?><br><i class="text-muted"><?=$alamat_pasien_eng?></i></label>
										<input disabled type="text" class="form-control  " value="<?=($alamatpasien)?>" >
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$paragraf_2_ina?><br><i class="text-muted"><?=$paragraf_2_eng?></i></label>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$dari_ina?><br><i class="text-muted"><?=$dari_eng?></i></label>
										<input disabled type="text" class="form-control  " value="<?=HumanDateShort($tanggal_daftar)?> <?=($tanggal_checkout?' s/d '.HumanDateShort($tanggal_checkout):'')?>" >
									</div>
									
								</div>
							</div>
							<?if ($list_pra_bedah){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="btn-group">
											<button class="btn btn-xs btn-success" type="button">Generate Laporan Operasi</button>
											<div class="btn-group">
												<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
													<?foreach($list_pra_bedah as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="load_lap_bedah(<?=$r->assesmen_id?>)"><?=HumanDateShort($r->tanggal_opr).' - '.$r->tindakan?></a>
													</li>
													<?}?>
													
												</ul>
											</div>
										</div>
									</div>
									
								</div>
							</div>
							<?}?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$diagnosa_ina?><br><i class="text-muted"><?=$diagnosa_eng?></i></label> &nbsp;&nbsp;&nbsp;
										<input type="text" class="form-control  auto_blur" id="diagnosa" value="<?=($diagnosa)?>" >
									</div>
									
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="tindakan"><?=$tindakan_ina?><br><i class="text-muted"><?=$tindakan_eng?></i></label>
										<input type="text" class="form-control  auto_blur" id="tindakan" value="<?=($tindakan)?>" >
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-2 ">
										<label for="tindakan"><?=$tanggal_tindakan_ina?><br><i class="text-muted"><?=$tanggal_tindakan_eng?></i></label>
										<div class="input-group date">
											<input tabindex="2" type="text"  class="js-datepicker form-control " data-date-format="dd-mm-yyyy" id="tanggal_tindakan" placeholder="HH/BB/TTTT"  value="<?= ($tanggal_tindakan?HumanDateShort($tanggal_tindakan):'') ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$paragraf_3_ina?><br><i class="text-muted"><?=$paragraf_3_eng?></i></label>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="nama_tubuh"><?=$bagian_ina?><br><i class="text-muted"><?=$bagian_eng?></i></label>
										<input type="text" class="form-control  auto_blur" id="nama_tubuh" value="<?=($nama_tubuh)?>" >
									</div>
									
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="alamat_pulang"><?=$serta_ina?>&nbsp;&nbsp;&nbsp;
										<button class="btn btn-xs btn-success" type="button" onclick="load_alamat_amputasi()"><i class="si si-paper-clip"></i> Generate Data Pasien</button>
										<br><i class="text-muted"><?=$serta_eng?></i></label> 
										
										<input type="text" class="form-control  auto_blur" id="alamat_pulang" value="<?=($alamat_pulang)?>" >
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="pembawa_pulang"><?=$dibawa_ina?><br><i class="text-muted"><?=$dibawa_eng?></i></label>
										<select id="pembawa_pulang" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="<?=$serta_ina?>">
											<option value="#" <?=($pembawa_pulang=='#'?'selected':'')?>>- Pilih yang Bertandatangan -</option>
											<option value="1" <?=($pembawa_pulang=='1'?'selected':'')?>>Penanggung Jawab</option>
											<option value="2" <?=($pembawa_pulang=='2'?'selected':'')?>>Pengantar</option>
											<option value="3" <?=($pembawa_pulang=='3'?'selected':'')?>>Pasien Sendiri</option>
											<option value="4" <?=($pembawa_pulang=='4'?'selected':'')?>>Lainnya</option>
											
										</select>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										<label for="nama"><?=$nama_ina?><br><i class="text-muted"><?=$nama_eng?></i></label>
										<input type="text" class="form-control  auto_blur" id="nama" value="<?=($nama)?>" >
									</div>
									<div class="col-md-6 ">
										<label for="nik"><?=$nik_ina?><br><i class="text-muted"><?=$nik_eng?></i></label>
										<input type="text" class="form-control  auto_blur" id="nik" value="<?=($nik)?>" >
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										<label for="alamat"><?=$alamat_ina?><br><i class="text-muted"><?=$alamat_eng?></i></label>
										<input type="text" class="form-control  auto_blur" id="alamat" value="<?=($alamat)?>" >
									</div>
									<div class="col-md-6 ">
										<label for="hubungan"><?=$hubungan_ina?><br><i class="text-muted"><?=$hubungan_eng?></i></label>
										<select id="hubungan"  class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="0" <?=($hubungan == '0' ? 'selected="selected"' : '')?>>Diri Sendiri</option>
											<?foreach(list_variable_ref(9) as $row){?>
											<option value="<?=$row->id?>" <?=($hubungan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$paragraf_4_ina?><br><i class="text-muted"><?=$paragraf_4_eng?></i></label>
										
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal"><?=$note_ina?><br><i class="text-muted"><?=$note_eng?></i></label>
										
									</div>
									
								</div>
							</div>
						<?}?>
					</div>	
					<br>
					<br>
					<?if ($assesmen_id!=''){?>
					<?
						$arr_mengetahui=get_nama_ppa_array($mengetahui);
						$arr_saksi=get_nama_ppa_array($saksi_rs);
					?>
					<br>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="col-md-12 ">
								<table class="table table-bordered">
									<tr>
										<td style="width:30%" class="text-center"><strong>{pemohon_ina} <br> <i class="text-muted">{pemohon_eng}</strong></td>
										<td style="width:30%" class="text-center"></td>
										<td style="width:30%" class="text-center"><strong>{mengetahui_ina} <br> <i class="text-muted">{mengetahui_eng}</strong></td>
									</tr>
									<tr>
										<td style="width:30%" class="text-center">
											<?if ($pemohon_ttd){?>
												<div class="img-container fx-img-rotate-r">
													<img class="center" src="<?=$pemohon_ttd?>" alt="">
													<?if ($status_assemen!='2'){?>
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="modal_ttd('pemohon')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd('pemohon')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
													<?}?>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success" onclick="modal_ttd('pemohon')"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
										</td>
										<td style="width:30%" class="text-center">
											
										</td>
										<td style="width:30%" class="text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($mengetahui?$mengetahui:$login_ppa_id)?>" alt="" title="">
										</td>
									</tr>
									
									<tr>
										<td style="width:30%" class="text-center"><label id="nama_pemberi_pernyataan"><?=$pemohon?></label></td>
										<td style="width:30%" class="text-center"></label></td>
										<td style="width:30%" class="text-center">
											<?if ($status_assemen=='1'){?>
											<select id="mengetahui" name="mengetahui" class="js-select2 form-control opsi_saksi_rs" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Pilih Mengetahui -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>" <?=($mengetahui==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
											</select>
											<?}else{?>
												<strong><?=$arr_mengetahui['nama']?></strong><br><i>(<?=$arr_mengetahui['nik']?>)</i>
											<?}?>
										</td>
									</tr>
									<tr>
										<td style="width:30%" class="text-center"><strong>{saksi_kel_ina} <br> <i class="text-muted">{saksi_kel_eng}</strong></td>
										<td style="width:30%" class="text-center"></td>
										<td style="width:30%" class="text-center"><strong>{saksi_rs_ina} <br> <i class="text-muted">{saksi_rs_eng}</strong></td>
									</tr>
									<tr>
										<td style="width:30%" class="text-center">
											<?if ($saksi_keluarga_ttd){?>
												<div class="img-container fx-img-rotate-r">
													<img class="center" src="<?=$saksi_keluarga_ttd?>" alt="">
													<?if ($status_assemen!='2'){?>
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="modal_ttd('saksi_keluarga')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd('saksi_keluarga')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
													<?}?>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success" onclick="modal_ttd('saksi_keluarga')"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
										</td>
										<td style="width:30%" class="text-center">
											
										</td>
										<td style="width:30%" class="text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($saksi_rs?$saksi_rs:$login_ppa_id)?>" alt="" title="">
										</td>
									</tr>
									
									<tr>
										<td style="width:30%" class="text-center"><label id="nama_pemberi_pernyataan"><?=$saksi_keluarga?></label></td>
										<td style="width:30%" class="text-center"></label></td>
										<td style="width:30%" class="text-center">
											<?if ($status_assemen=='1'){?>
											<select id="saksi_rs" name="saksi_rs" class="js-select2 form-control opsi_saksi_rs" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Pilih Saksi RS -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>" <?=($saksi_rs==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
											</select>
											<?}else{?>
												<strong><?=$arr_saksi['nama']?></strong><br><i>(<?=$arr_saksi['nik']?>)</i>
											<?}?>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<br>
					<div class="row">
					<div class="col-md-12">
						<i class="font-w700 push-5 text-left text-primary">{footer_ina}</i>
						<?if($footer_eng){?>
							<br>
							<i class="font-w700 push-5 text-left">{footer_eng}</i>
						<?}?>
							
					</div>
					</div
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?}?>
					
					<br>
					<br>
					<br>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">DAFTAR {judul_ina}</h4>
										<?if($judul_eng){?>
											<h5 class="push-5 text-center">{judul_eng}</h5>
										<?}?>
									</div>
									<div class="col-md-12 ">
										<div class="pull-right push-10-r">
											<?php if (UserAccesForm($user_acces_form,array('2280'))){?>
											<?if ($assesmen_id==''){?>
											<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
											<?}?>
											<?}?>
											
										</div>
										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_my_order" placeholder="No Pendaftaran / No Pengajuan" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tgl_daftar_my_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_my_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pembuatan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_my_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_my_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Polikinik</label>
											<div class="col-md-8">
												
												<select tabindex="13" id="poli_my_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													<option value="4">ODS</option>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="dpjp_filter">Dokter</label>
											<div class="col-md-8">
												<select tabindex="12" id="dpjp_my_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All </option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_surat_amputasi()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_my_order">
											<thead>
												<tr>
													<th width="8%">No Registrasi</th>
													<th width="10%">Pasien</th>
													<th width="9%">No Surat</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemohon</th>
													<th width="5%">Jml Cetak</th>
													<th width="10%">Pembuat</th>
													<th width="12%">Action</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_ina}</h4>
										<?if($judul_eng){?>
											<h5 class="push-5 text-center">{judul_eng}</h5>
										<?}?>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_his_order" placeholder="No Pendaftaran / No Pengajuan" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tgl_daftar_his_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_his_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_his_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Polikinik</label>
											<div class="col-md-8">
												
												<select tabindex="13" id="poli_his_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													<option value="4">ODS</option>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="dpjp_filter">Dokter</label>
											<div class="col-md-8">
												<select tabindex="12" id="dpjp_his_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All </option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_surat_amputasi_history()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_history_surat">
											<thead>
												<tr>
													<th width="8%">No Registrasi</th>
													<th width="10%">Pasien</th>
													<th width="9%">No Surat</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Pemohon</th>
													<th width="5%">Jml Cetak</th>
													<th width="10%">Pembuat</th>
													<th width="12%">Action</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>					
				</div>
				
			</div>
		</div>
	</div>
</div>
<?}?>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?if ($assesmen_id){?>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan  </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
							<input type="hidden" readonly id="nama_field_ttd" value="">
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="col-md-12">
								<label for="example-input-normal">NAMA</label>
								<input tabindex="32" type="text" class="form-control "  id="nama_ttd" value="">
							</div>
						</div>
					</div>

				</div>
				<br>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<textarea id="pemohon_ttd" name="signed" style="display: none"><?=$pemohon_ttd?></textarea>
<textarea id="saksi_keluarga_ttd" name="signed" style="display: none"><?=$saksi_keluarga_ttd?></textarea>
<input type="hidden" id="saksi_keluarga" value="<?=($saksi_keluarga)?>"> 
<input type="hidden" id="pemohon" value="<?=($pemohon)?>"> 
<?}?>
<? $this->load->view('Tsurat/modal_user_his_cetak')?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var class_obat;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$(document).ready(function() {
	// $(".btn_close_left").click(); 
	$(".number").number(true,0,'.',',');
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	disable_edit();
	if ($("#assesmen_id").val()){
		$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	}else{
		list_surat_amputasi();
	}
	
});
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
$(document).on("click","#btn_save_ttd",function(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var nama_field=$("#nama_field_ttd").val();
	var nama_ttd=$("#nama_ttd").val();
	if (nama_ttd==''){
		sweetAlert("Maaf...", "Isi Nama Tanda Tangan", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tsurat/save_ttd_amputasi/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				nama_field:nama_field,
				nama_ttd:nama_ttd,
		  },success: function(data) {
				location.reload();
			}
		});
	
});
function load_lap_bedah(id){
	// alert(id);
	$("#cover-spin").show();
		$.ajax({
		url: '{site_url}tsurat/load_lap_bedah_surat_amputasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:id,
				
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#tindakan").val(data.tindakan);
			$("#tanggal_tindakan").val(data.tanggal_tindakan);
			$("#diagnosa").val(data.diagnosa);
		}
	});
	
}
function load_alamat_amputasi(){
	let idpasien=$("#idpasien").val();
	$("#cover-spin").show();
		$.ajax({
		url: '{site_url}tsurat/load_alamat_amputasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:idpasien,
				
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#alamat_pulang").val(data.alamat);
		}
	});
	
}
function modal_ttd(nama_field){
	$("#nama_field_ttd").val(nama_field);
	$("#nama_ttd").val($("#"+nama_field).val());
	
	var signature64=$("#"+nama_field+'_ttd').val();
	if (signature64){
	sig.signature('enable').signature('draw', signature64);
	$("#signature64").val(signature64)
		
	}
	$("#modal_ttd").modal('show');
}
function disable_edit(){
	if (status_assemen=='2'){
		$("#form_input :input").prop("disabled", true);
		$(".btn_cetak").removeAttr('disabled');
		$(".btn_back").removeAttr('disabled');
	}
}

function close_assesmen(){
	if (parseFloat($("#lama").val())<1){
		sweetAlert("Maaf...", "Tentukan Tanggal Dengan benar", "error");
		return false;
	}

	swal({
		title: "Notifikasi",
		text : "Apakah Anda Yakin telah selesai membuat Surat?",
		icon : "question",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#status_assemen").val(2);
		
		simpan_assesmen();
	});		

	
}
function create_assesmen(){
	
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe=$("#idtipe").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let st_ranap=$("#st_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let iddokter=$("#iddokter").val();
	
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Surat "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tsurat/create_assesmen_surat_amputasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					st_ranap:st_ranap,
					tglpendaftaran:tglpendaftaran,
					idpasien:idpasien,
					iddokter:iddokter,
					idtipe:idtipe,
					idpoliklinik:idpoliklinik,
					
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Pengajuan Surat ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tsurat/batal_assesmen_surat_amputasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$(".opsi_change").change(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".opsi_saksi_rs").change(function(){
		if ($("#st_edited").val()=='0'){
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsurat/simpan_saksi_amputasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						mengetahui : $("#mengetahui").val(),
						saksi_rs : $("#saksi_rs").val(),
						
					},
				success: function(data) {
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Petugas'});
						location.reload();	
					}
				}
			});
		}
});
$(".auto_blur_tgl").change(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur_tgl").blur(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur").blur(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
 $(document).find('.js-summernote').on('summernote.blur', function() {
	if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
  });
function simpan_assesmen(){
		if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		// alert(konsul_id);return false;
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tsurat/simpan_assesmen_surat_amputasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						status_assemen:$("#status_assemen").val(),
						st_edited:$("#st_edited").val(),
						assesmen_id:$("#assesmen_id").val(),
						
						tglpendaftaran : $("#tglpendaftaran").val(),
						waktupendaftaran : $("#waktupendaftaran").val(),
						diagnosa : $("#diagnosa").val(),
						tindakan : $("#tindakan").val(),
						tanggal_tindakan : $("#tanggal_tindakan").val(),
						nama_tubuh : $("#nama_tubuh").val(),
						alamat_pulang : $("#alamat_pulang").val(),
						pembawa_pulang : $("#pembawa_pulang").val(),
						nama : $("#nama").val(),
						alamat : $("#alamat").val(),
						nik : $("#nik").val(),
						hubungan : $("#hubungan").val(),
					},
				success: function(data) {
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='2'){
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
	}
}

function list_surat_amputasi(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let dpjp=$("#dpjp_my_order").val();
	let poli=$("#poli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_my_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_my_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					// {  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tsurat/list_surat_amputasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						dpjp:dpjp,
						poli:poli,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_surat_amputasi_history(){
	// $("#div_history").hide();
	// $("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_his_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_his_order").val();
	let dpjp=$("#dpjp_his_order").val();
	let poli=$("#poli_his_order").val();
	let notransaksi=$("#notransaksi_his_order").val();
	let tanggal_input_1=$("#tanggal_input_1_his_order").val();
	let tanggal_input_2=$("#tanggal_input_2_his_order").val();
	$('#index_history_surat').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_history_surat').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					// {  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tsurat/list_surat_amputasi_history', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						dpjp:dpjp,
						poli:poli,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
$("#pembawa_pulang").change(function(){
	if ($("#assesmen_id").val()!=''){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		
		let idpasien=$("#idpasien").val();
		let pembawa_pulang=$("#pembawa_pulang").val();
		let assesmen_id=$("#assesmen_id").val();
		let st_ranap=$("#st_ranap").val();
		
		let template=$("#pembawa_pulang option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mengganti Permintaan Menjadi "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tsurat/edit_pilih_surat_amputasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						pilih_ttd_id:pembawa_pulang,
						assesmen_id:assesmen_id,
						st_ranap:st_ranap,
					   },
				success: function(data) {
					$("#cover-spin").hide();
					$("#nama").val(data.nama_ttd);
					$("#alamat").val(data.alamat_ttd);
					$("#hubungan").val(data.hubungan_ttd).trigger('change');
					console.log(data);
				}
			});
		});
	}
});
</script>