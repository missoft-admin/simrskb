<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	
	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>

<?if ($menu_kiri=='input_surat_diagnosa'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_surat_diagnosa' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_permintaan;
	if ($assesmen_id){
		$mulai=HumanDateShort($tanggal_pelayanan_awal);
		$selesai=HumanDateShort($tanggal_pelayanan_akhir);
		$tglpendaftaran=HumanDateShort($tanggal_input);
		$waktupendaftaran=HumanTime($tanggal_input);
		
	}else{
		$tglpendaftaran=date('d-m-Y');
		$waktupendaftaran=date('H:i:s');
		
	}
	$q="SELECT H.id,H.tanggal,MD.nama as nama_dokter 
		FROM `tpoliklinik_pendaftaran` H 
		LEFT JOIN mdokter MD ON MD.id=H.iddokter
		WHERE H.idpasien='$idpasien' AND H.`status` !='0' ORDER BY H.id DESC LIMIT 15";
		$list_trx_poli=$this->db->query($q)->result();
	$q="SELECT H.id,H.tanggaldaftar as tanggal,MD.nama as nama_dokter 
		FROM `trawatinap_pendaftaran` H 
		LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
		WHERE H.idpasien='$idpasien' AND H.`status` !='0' ORDER BY H.id DESC LIMIT 15";
		$list_trx_ranap=$this->db->query($q)->result();
	
	?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rm">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Surat Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_surat_diagnosa()" ><i class="fa fa-send"></i> Daftar Surat </a>
				</li>
				<?php if (UserAccesForm($user_acces_form,array('2557'))){?>
				<li class="">
					<a href="#tab_3" onclick="list_surat_diagnosa_history()"><i class="fa fa-history"></i> Riwayat Surat</a>
				</li>
				<?}?>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
					
					<div class="row">
						<input type="hidden" id="idtipe_poli" value="{idtipe_poli}"> 
						<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}"> 
						<input type="hidden" id="idpoliklinik" value="{idpoliklinik}"> 
						<input type="hidden" id="idrekanan" value="{idrekanan}"> 
						<input type="hidden" id="iddokter" value="{iddokter}"> 
						<input type="hidden" id="assesmen_id" value="{assesmen_id}"> 
					

						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="iddokter" value="<?=$iddokter?>" >		
						<input type="hidden" id="idtipe" value="<?=$idtipe?>" >		
						<input type="hidden" id="assesmen_detail_id" value="" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_edited" value="<?=$st_edited?>" >		
						<input type="hidden" id="st_ranap" value="<?=($st_ranap)?>"> 
						<input type="hidden" id="pendaftaran_id" value="<?=($pendaftaran_id)?>"> 
						<input type="hidden" id="pendaftaran_id_ranap" value="<?=($pendaftaran_id_ranap)?>"> 
						<input type="hidden" id="idpasien" value="<?=($idpasien)?>"> 
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-4 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text"  class="js-datepicker form-control " data-date-format="dd-mm-yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tglpendaftaran ?>" required>
										<label for="tglpendaftaran">Tanggal <i class="text-muted">Created Date</i></label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-4 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text"   class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktupendaftaran ?>" required>
										<label for="waktupendaftaran">Waktu <i class="text-muted">Time</i></label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-12">
								<h4 class="font-w700 push-5 text-center text-primary">{judul_ina}</h4>
								<?if($judul_eng){?>
									<h5 class="push-5 text-center">{judul_eng}</h5>
								<?}?>
							
						</div>
					</div>
					
					<div class="row">
					
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' ){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Surat Belum Disimpan</a>!</p>
									</div>
								<?}?>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?php if (UserAccesForm($user_acces_form,array('2555'))){?>
									<?if ($assesmen_id==''){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-warning" id="btn_ganti_assesmen" onclick="ganti_assesmen()"  type="button"><i class="fa fa-folder-open"></i> Ganti Kunjungan</button>
										<button class="btn btn-success" onclick="close_assesmen()" type="button"><i class="fa fa-save"></i> Simpan Surat</button>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<a href="<?=site_url()?>tsurat/show_cetak/<?=$assesmen_id?>/8/0" target="_blank" class="btn btn-info btn_cetak" type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$paragraf_1_ina?> <br><i class="text-muted"><?=$paragraf_1_eng?></i></label>
										<select id="dpjp" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($dpjp == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
											<option value="<?=$row->id?>" <?=($dpjp==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
							
							</div>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$paragraf_2_ina?> <br><i class="text-muted"><?=$paragraf_2_eng?></i></label>
										
									</div>
									
								</div>
							
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa">Nama Pasien / <i class="text-muted">Name Patien</i></label>
										<input disabled type="text" class="form-control  " value="<?= $nomedrec_pasien.' - '.$nama_pasien ?>" >
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa">Alamat / <i class="text-muted">Address</i></label>
										<input disabled type="text" class="form-control  " value="<?=($alamat_pasien)?>" >
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										<label for="diagnosa"><?=$pelayanan_ina?> <br><i class="text-muted"><?=$pelayanan_eng?></i></label>
										<input disabled type="text" class="form-control  " value="<?=($st_ranap=='1'?'RAWAT INAP':'RAWAT JALAN')?>" >
									</div>
									<?
										$tanggal_rawat='';
										if ($st_ranap=='1'){//RANAP
											$tanggal_rawat=tanggal_indo_DMY($tanggal_pelayanan_awal).($tanggal_pelayanan_akhir?' s.d '.tanggal_indo_DMY($tanggal_pelayanan_akhir):'');
										}else{
											$tanggal_rawat=tanggal_indo_DMY($tanggal_pelayanan_awal);
										}
									?>
									<div class="col-md-6 ">
										<label for="diagnosa"><?=$tanggal_tindakan_ina?> <br><i class="text-muted"><?=$tanggal_tindakan_eng?></i></label>
										<input disabled type="text" class="form-control  " value="<?=($tanggal_rawat)?>" >
									</div>
									
								</div>
							</div>
							<?
								$q="SELECT H.assesmen_id,TP.tanggal,TP.iddokter,MD.nama,TP.id,TP.nopendaftaran FROM `tpoliklinik_asmed` H
								LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.pendaftaran_id
								LEFT JOIN mdokter MD ON MD.id=TP.iddokter
								WHERE H.idpasien='$idpasien' AND H.status_assemen='2'
								ORDER BY H.tanggal_input DESC,TP.tanggal DESC";
								$list_trx_ass_rj=$this->db->query($q)->result();
								
								$q="SELECT H.assesmen_id,TP.tanggal,TP.iddokter,MD.nama,TP.id,TP.nopendaftaran 
								FROM `tpoliklinik_asmed_igd` H
								LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.pendaftaran_id
								LEFT JOIN mdokter MD ON MD.id=TP.iddokter
								WHERE H.idpasien='$idpasien' AND H.status_assemen='2'
								ORDER BY H.tanggal_input DESC,TP.tanggal DESC";
								$list_trx_ass_igd=$this->db->query($q)->result();
								
								$q="SELECT H.assesmen_id,TP.tanggaldaftar as tanggal,TP.iddokterpenanggungjawab iddokter,MD.nama,TP.id,TP.nopendaftaran 
								FROM `tranap_asmed_ri` H
								LEFT JOIN trawatinap_pendaftaran TP ON TP.id=H.pendaftaran_id_ranap
								LEFT JOIN mdokter MD ON MD.id=TP.iddokterpenanggungjawab
								WHERE H.idpasien='$idpasien' AND H.status_assemen='2'
								ORDER BY H.tanggal_input DESC,TP.tanggaldaftar DESC";
								$list_trx_ass_ranap=$this->db->query($q)->result();
								if ($st_ranap=='0'){
									$q="SELECT H.assesmen_id,TP.tanggal,TP.iddokter,MD.nama,TP.id,TP.nopendaftaran 
										FROM `tpoliklinik_cppt` H
										LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.pendaftaran_id
										LEFT JOIN mdokter MD ON MD.id=TP.iddokter
										WHERE H.idpasien='$idpasien' AND H.st_ranap='0' AND H.status_assemen='2'
										ORDER BY H.tanggal_input DESC,TP.tanggal DESC";
								}else{
									$q="
										SELECT H.assesmen_id,TP.tanggaldaftar as tanggal,TP.iddokterpenanggungjawab iddokter,MD.nama,TP.id,TP.nopendaftaran 
										FROM `tpoliklinik_cppt` H
										LEFT JOIN trawatinap_pendaftaran TP ON TP.id=H.pendaftaran_id_ranap
										LEFT JOIN mdokter MD ON MD.id=TP.iddokterpenanggungjawab
										WHERE H.idpasien='$idpasien' AND H.st_ranap='1' AND H.status_assemen='2'
										ORDER BY H.tanggal_input DESC,TP.tanggaldaftar DESC
									";
								}
								$list_trx_cppt=$this->db->query($q)->result();
								
									$q="SELECT H.assesmen_id,TP.tanggaldaftar as tanggal,TP.iddokterpenanggungjawab iddokter,MD.nama,TP.id,TP.nopendaftaran 
										FROM `tranap_ringkasan_pulang` H
										LEFT JOIN trawatinap_pendaftaran TP ON TP.id=H.pendaftaran_id_ranap
										LEFT JOIN mdokter MD ON MD.id=TP.iddokterpenanggungjawab
										WHERE H.idpasien='146' AND H.status_assemen='2'
										ORDER BY H.tanggal_input DESC,TP.tanggaldaftar DESC";
								$list_trx_ringkasan=$this->db->query($q)->result();
								
							?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$paragraf_3_ina?> <br><i class="text-muted"><?=$paragraf_3_eng?></i></label>
										<div>
											<?if ($list_trx_ass_rj){?>
											<div class="btn-group">
                                                <button class="btn btn-primary btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Assesmen Medis Rawat Jalan</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_trx_ass_rj as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_rj_all(<?=$r->assesmen_id?>,<?=$r->id?>)"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
											<?}?>
											<?if ($list_trx_ass_igd){?>
											<div class="btn-group">
                                                <button class="btn btn-danger btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Assesmen Medis IGD</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_trx_ass_igd as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_igd_all(<?=$r->assesmen_id?>,<?=$r->id?>)"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
											<?}?>
											<?if ($list_trx_ass_ranap){?>
											<div class="btn-group">
                                                <button class="btn btn-success btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Assesmen Medis Rawat Inap</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_trx_ass_ranap as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_ranap_all(<?=$r->assesmen_id?>,<?=$r->id?>)"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
											<?}?>
											<?if ($list_trx_cppt){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate CPPT Medis</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_trx_cppt as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_cppt_all(<?=$r->assesmen_id?>,<?=$r->id?>)"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
											<?}?>
											<?if ($list_trx_ringkasan){?>
											<div class="btn-group">
                                                <button class="btn btn-info btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Ringkasan Medis</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_trx_ringkasan as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_ringkasan_all(<?=$r->assesmen_id?>,<?=$r->id?>)"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
											<?}?>
											
										</div>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$riwayat_ina?> &nbsp;&nbsp;&nbsp;
										<?if ($list_trx_ass_rj){?>
											
											<div class="btn-group">
												<button class="btn btn-primary btn-sm dropdown-toggle" title="Generate Assesmen Medis Rawat Jalan" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_rj as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_rj(<?=$r->assesmen_id?>,'riwayat_penyakit')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_ass_igd){?>
											<div class="btn-group">
												<button class="btn btn-danger btn-sm dropdown-toggle" title="Generate Assesmen Medis IGD" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_igd as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_igd(<?=$r->assesmen_id?>,'riwayat_penyakit')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											
											<?if ($list_trx_ass_ranap){?>
											<div class="btn-group">
												<button class="btn btn-success btn-sm dropdown-toggle" title="Generate Assesmen Medis Rawat Inap" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_ranap as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_ranap(<?=$r->assesmen_id?>,'riwayat_penyakit')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_cppt){?>
											<div class="btn-group">
												<button class="btn btn-warning btn-sm dropdown-toggle" title="Generate CPPT Medis" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_cppt as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_cppt(<?=$r->assesmen_id?>,'riwayat_penyakit')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_ringkasan){?>
											<div class="btn-group">
												<button class="btn btn-info btn-sm dropdown-toggle" title="Generate Assesmen Ringkasan Medis" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ringkasan as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_ringkasan(<?=$r->assesmen_id?>,'riwayat_penyakit')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											
											
										<br><i class="text-muted"><?=$riwayat_eng?></i></label>
										
										
										<textarea class="form-control js-summernote auto_blur" id="riwayat_penyakit"  rows="2"> <?=$riwayat_penyakit?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="penunjang"><?=$penunjang_ina?> 
										&nbsp;&nbsp;&nbsp;
										<?if ($st_ranap=='0'){?>
										<?if ($list_trx_poli){?>
											<div class="btn-group">
                                                <button class="btn btn-info btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Data Penunjang</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_trx_poli as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_all(<?=$r->id?>,0)"><?=HumanDateShort($r->tanggal).' ('.$r->nama_dokter.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
											<?}?>
										 <?}?>
										 <?if ($st_ranap=='1'){?>
										<?if ($list_trx_ranap){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Data Penunjang</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_trx_ranap as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_all(<?=$r->id?>,1)"><?=HumanDateShort($r->tanggal).' ('.$r->nama_dokter.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
											<?}?>
										 <?}?>
										 <?
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_laboratorium_umum` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_laboratorium_umum` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab=$this->db->query($q)->result();
										 
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_laboratorium_bank_darah` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_laboratorium_bank_darah` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab_darah=$this->db->query($q)->result();
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_laboratorium_patologi_anatomi` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_laboratorium_patologi_anatomi` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab_patologi=$this->db->query($q)->result();
										 
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_xray` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_xray` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab_xray=$this->db->query($q)->result();
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_usg` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_usg` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab_usg=$this->db->query($q)->result();
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_mri` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_mri` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab_mri=$this->db->query($q)->result();
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_ctscan` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_ctscan` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab_ct=$this->db->query($q)->result();
										 if ($st_ranap=='1'){
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_lainnya` H
												WHERE H.asal_rujukan='3' AND H.pendaftaran_id='$pendaftaran_id_ranap'";
										 }else{
											 $q="SELECT H.id,H.nomor_permintaan,DATE(H.waktu_permintaan) as tanggal 
											FROM `term_radiologi_lainnya` H
												WHERE H.asal_rujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
										 }
										 $list_lab_lain=$this->db->query($q)->result();
										 
										 ?>
										 <?if ($list_lab){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Laboratorium</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'Laboratorium')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										 <?if ($list_lab_darah){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Bank Darah</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab_darah as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'Bank Darah')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										 <?if ($list_lab_patologi){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Patologi Anatomi</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab_patologi as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'Patologi Anatomi')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										 <?if ($list_lab_xray){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> X-Ray</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab_xray as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'X-Ray')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										 <?if ($list_lab_usg){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> USG</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab_usg as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'USG')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										 <?if ($list_lab_mri){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> MRI</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab_mri as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'MRI')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										 <?if ($list_lab_ct){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> CT-CSAN</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab_ct as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'CT-CSAN')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										 <?if ($list_lab_lain){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Lainnya</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_lab_lain as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_penunjang_satuan_diagnosa(<?=$r->id?>,'LAINNYA')"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										<br><i class="text-muted"><?=$penunjang_eng?></i></label>
										<textarea class="form-control js-summernote auto_blur" id="penunjang"  rows="2"> <?=$penunjang?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$diagnosa_akhir_ina?> 
										&nbsp;&nbsp;&nbsp;
										<?if ($list_trx_ass_rj){?>
											
											<div class="btn-group">
												<button class="btn btn-primary btn-sm dropdown-toggle" title="Generate Assesmen Medis Rawat Jalan" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_rj as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_rj(<?=$r->assesmen_id?>,'diagnosa_akhir')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_ass_igd){?>
											<div class="btn-group">
												<button class="btn btn-danger btn-sm dropdown-toggle" title="Generate Assesmen Medis IGD" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_igd as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_igd(<?=$r->assesmen_id?>,'diagnosa_akhir')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											
											<?if ($list_trx_ass_ranap){?>
											<div class="btn-group">
												<button class="btn btn-success btn-sm dropdown-toggle" title="Generate Assesmen Medis Rawat Inap" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_ranap as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_ranap(<?=$r->assesmen_id?>,'diagnosa_akhir')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_cppt){?>
											<div class="btn-group">
												<button class="btn btn-warning btn-sm dropdown-toggle" title="Generate CPPT Medis" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_cppt as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_cppt(<?=$r->assesmen_id?>,'diagnosa_akhir')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_ringkasan){?>
											<div class="btn-group">
												<button class="btn btn-info btn-sm dropdown-toggle" title="Generate Assesmen Ringkasan Medis" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ringkasan as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_ringkasan(<?=$r->assesmen_id?>,'diagnosa_akhir')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
										<br><i class="text-muted"><?=$diagnosa_akhir_eng?></i></label>
										<textarea class="form-control js-summernote auto_blur" id="diagnosa_akhir"  rows="2"> <?=$diagnosa_akhir?></textarea>
									</div>
									
								</div>
							</div>
							<?
								 if ($st_ranap=='1'){
									 $q="SELECT H.assesmen_id as id,H.noresep as nomor_permintaan,DATE(H.tanggal_permintaan) as tanggal 
									 FROM `tpoliklinik_e_resep` H
									WHERE H.asalrujukan='3' AND H.pendaftaran_id_ranap='$pendaftaran_id_ranap'";
								 }else{
									$q="SELECT H.assesmen_id as id,H.noresep as nomor_permintaan,DATE(H.tanggal_permintaan) as tanggal 
									 FROM `tpoliklinik_e_resep` H
									WHERE H.asalrujukan !='3' AND H.pendaftaran_id='$pendaftaran_id'";
								 }
								 $list_obat=$this->db->query($q)->result();
							?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$terapi_ina?> &nbsp;&nbsp;&nbsp;
										<?if ($list_obat){?>
											<div class="btn-group">
                                                <button class="btn btn-warning btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Data Terapi</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_obat as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_terapi_diagnosa(<?=$r->id?>)"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										<br><i class="text-muted"><?=$terapi_eng?></i></label>
										<textarea class="form-control js-summernote auto_blur" id="terapi"  rows="2"> <?=$terapi?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$tindakan_ina?> 
										&nbsp;&nbsp;&nbsp;
										<?
											if ($st_ranap=='1'){
												 $q="SELECT H.assesmen_id as id,H.tindakan as nomor_permintaan,DATE(H.tanggal_input) as tanggal 
												FROM `tranap_lap_bedah` H
												WHERE H.st_ranap='1' AND H.pendaftaran_id='$pendaftaran_id_ranap' AND H.status_assemen='2'";
											 }else{
												 $q="SELECT H.assesmen_id as id,H.tindakan as nomor_permintaan,DATE(H.tanggal_input) as tanggal 
												FROM `tranap_lap_bedah` H
												WHERE H.st_ranap='0' AND H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='2'";
											 }
											 $list_tindakan=$this->db->query($q)->result();
												
										?>
										<?if ($list_tindakan){?>
											<div class="btn-group">
                                                <button class="btn btn-default btn-sm" type="button"><i class="glyphicon glyphicon-download-alt"></i> Generate Laporan Operasi</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                       <?foreach($list_tindakan as $r){?>
                                                        <li>
                                                            <a tabindex="-1" href="javascript:void(0)" onclick="generate_tindakan_diagnosa(<?=$r->id?>)"><?=HumanDateShort($r->tanggal).' ('.$r->nomor_permintaan.')' ?></a>
                                                        </li>
													   <?}?>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
										 <?}?>
										<?if ($list_trx_ass_rj){?>
											
											<div class="btn-group">
												<button class="btn btn-primary btn-sm dropdown-toggle" title="Generate Assesmen Medis Rawat Jalan" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_rj as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_rj(<?=$r->assesmen_id?>,'tindakan')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_ass_igd){?>
											<div class="btn-group">
												<button class="btn btn-danger btn-sm dropdown-toggle" title="Generate Assesmen Medis IGD" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_igd as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_igd(<?=$r->assesmen_id?>,'tindakan')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											
											<?if ($list_trx_ass_ranap){?>
											<div class="btn-group">
												<button class="btn btn-success btn-sm dropdown-toggle" title="Generate Assesmen Medis Rawat Inap" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ass_ranap as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_ranap(<?=$r->assesmen_id?>,'tindakan')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_cppt){?>
											<div class="btn-group">
												<button class="btn btn-warning btn-sm dropdown-toggle" title="Generate CPPT Medis" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_cppt as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_cppt(<?=$r->assesmen_id?>,'tindakan')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
											<?if ($list_trx_ringkasan){?>
											<div class="btn-group">
												<button class="btn btn-info btn-sm dropdown-toggle" title="Generate Assesmen Ringkasan Medis" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ringkasan as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_ringkasan(<?=$r->assesmen_id?>,'tindakan')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
										<br><i class="text-muted"><?=$tindakan_eng?></i></label>
										<textarea class="form-control js-summernote auto_blur" id="tindakan"  rows="2"> <?=$tindakan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="diagnosa"><?=$keadaan_ina?> 
											<?if ($list_trx_ringkasan){?>
											<div class="btn-group">
												<button class="btn btn-info btn-sm dropdown-toggle" title="Generate Assesmen Ringkasan Medis" data-toggle="dropdown" aria-expanded="false">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
												   <?foreach($list_trx_ringkasan as $r){?>
													<li>
														<a tabindex="-1" href="javascript:void(0)" onclick="generate_ringkasan(<?=$r->assesmen_id?>,'prognis')"><?=HumanDateShort($r->tanggal).' ('.$r->nama.')' ?></a>
													</li>
												   <?}?>
												   
												</ul>
                                            </div>
											<?}?>
										<br><i class="text-muted"><?=$keadaan_eng?></i></label>
										<textarea class="form-control js-summernote auto_blur" id="keadaan"  rows="2"> <?=$keadaan?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<table width="100%">
										<tr>
											<td width="75%" class="text-left"><strong><?=$paragraf_4_ina?></strong><br><i class="text-muted"><?=$paragraf_4_eng?></i></td>
											<td width="25%" class="text-center"><strong><?=$dokter_ina?></strong><br><i class="text-muted"><?=$dokter_eng?></i></td>
										</tr>
										<tr>
											<td width="75%" class="text-center">&nbsp;</td>
											<td width="25%" class="text-center"><strong>
												<img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dpjp; ?>" width="100px">
											</strong></td>
											
										</tr>
										<tr>
											<td width="75%" class="text-center">&nbsp;</td>
											<td width="25%" class="text-center">
												<select id="dpjp_ttd" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="" <?=($dpjp == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>" <?=($dpjp==$row->id?'selected':'')?>><?=$row->nama?></option>
													<?}?>
													
												</select>
											</td>
											
										</tr>
										
									</table>
									
								</div>
							</div>
						<?}?>
					</div>	
					<?if ($assesmen_id!=''){?>
					<div class="row">
					<div class="col-md-12">
						<i class="font-w700 push-5 text-left text-primary">{footer_ina}</i>
						<?if($footer_eng){?>
						<br>
							<label class="class="text-muted">{footer_eng}</label>
						<?}?>
							
					</div>
					</div>
					<?}?>
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">DAFTAR {judul_ina}</h4>
										<?if($judul_eng){?>
											<h5 class="push-5 text-center">{judul_eng}</h5>
										<?}?>
									</div>
									<div class="col-md-12 ">
										<div class="pull-right push-10-r">
											<?php if (UserAccesForm($user_acces_form,array('2555'))){?>
											<?if ($assesmen_id==''){?>
											<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
											<?}?>
											<?}?>
											
										</div>
										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_my_order" placeholder="No Pendaftaran / No Pengajuan" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tgl_daftar_my_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_my_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pembuatan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_my_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_my_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Polikinik</label>
											<div class="col-md-8">
												
												<select tabindex="13" id="poli_my_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													<option value="4">ODS</option>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="dpjp_filter">Dokter</label>
											<div class="col-md-8">
												<select tabindex="12" id="dpjp_my_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All </option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_surat_diagnosa()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_my_order">
											<thead>
												<tr>
													<th width="8%">No Registrasi</th>
													<th width="10%">Pasien</th>
													<th width="9%">No Surat</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Dokter</th>
													<th width="10%">Tanggal</th>
													<th width="5%">Jml Cetak</th>
													<th width="10%">Pembuat</th>
													<th width="12%">Action</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_ina}</h4>
										<?if($judul_eng){?>
											<h5 class="push-5 text-center">{judul_eng}</h5>
										<?}?>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_his_order" placeholder="No Pendaftaran / No Pengajuan" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tgl_daftar_his_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_his_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_his_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Polikinik</label>
											<div class="col-md-8">
												
												<select tabindex="13" id="poli_his_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													<option value="4">ODS</option>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="dpjp_filter">Dokter</label>
											<div class="col-md-8">
												<select tabindex="12" id="dpjp_his_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All </option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_surat_diagnosa_history()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_history_surat">
											<thead>
												<tr>
													<th width="8%">No Registrasi</th>
													<th width="10%">Pasien</th>
													<th width="9%">No Surat</th>
													<th width="10%">Tujuan</th>
													<th width="10%">Dokter</th>
													<th width="10%">Tanggal</th>
													<th width="5%">Jml Cetak</th>
													<th width="10%">Pembuat</th>
													<th width="12%">Action</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>					
				</div>
				
			</div>
		</div>
	</div>
</div>
<?}?>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tsurat/modal_user_his_cetak')?>

<div class="modal" id="modal_pendaftaran" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Kunjungan Pasien</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="notransaksi_2" placeholder="No Pendaftaran" name="notransaksi" value="">
								</div>
							</div>
						</div>
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_21" name="tanggal_1" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndexPoli()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_poli">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Action</th>
											<th width="10%">Pendaftaran</th>
											<th width="10%">Tanggal</th>
											<th width="8%">No Medrec</th>
											<th width="20%">Nama Pasien</th>
											<th width="15%">Dokter</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var class_obat;
$(document).ready(function() {
	// $(".btn_close_left").click(); 
	$(".number").number(true,0,'.',',');
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	disable_edit();
	if ($("#assesmen_id").val()){
		$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	}else{
		list_surat_diagnosa();
	}
	
});
function ganti_assesmen(){
	$("#modal_pendaftaran").modal('show');
	getIndexPoli();
}
function getIndexPoli() {
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_poli').DataTable().destroy();
    table = $('#index_poli').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6] },
						 { "width": "5%", "targets": [0]},
						 { "width": "10%", "targets": [1,2,4]},
						 { "width": "20%", "targets": [3,5,6]},
						
					],
            ajax: { 
                url: '{site_url}tsurat/getIndexPoliCari',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_21").val(),
						tanggal_2:$("#tanggal_22").val(),
						notransaksi:$("#notransaksi_2").val(),
						idpasien:$("#idpasien").val(),
						st_ranap:$("#st_ranap").val(),
						pendaftaran_id:$("#pendaftaran_id").val(),
						pendaftaran_id_ranap:$("#pendaftaran_id_ranap").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function generate_rj_all(assesmen_id,pendaftaran_id){
	generate_rj(assesmen_id,'riwayat_penyakit');
	generate_rj(assesmen_id,'diagnosa_akhir');
	generate_rj(assesmen_id,'tindakan');
}
function generate_rj(assesmen_id,nama_var){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_rj_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			if (nama_var=='riwayat_penyakit'){
			$("#riwayat_penyakit").summernote('code',data.riwayat_penyakit);
			}
			if (nama_var=='diagnosa_akhir'){
			$("#diagnosa_akhir").summernote('code',data.diagnosa_akhir);
			}
			if (nama_var=='tindakan'){
			$("#tindakan").summernote('code',data.tindakan);
			}
			
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
			simpan_assesmen();
		}
	});
}
function generate_igd_all(assesmen_id,pendaftaran_id){
	generate_igd(assesmen_id,'riwayat_penyakit');
	generate_igd(assesmen_id,'diagnosa_akhir');
	generate_igd(assesmen_id,'tindakan');
}
function generate_igd(assesmen_id,nama_var){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_igd_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			if (nama_var=='riwayat_penyakit'){
			$("#riwayat_penyakit").summernote('code',data.riwayat_penyakit);
			}
			if (nama_var=='diagnosa_akhir'){
			$("#diagnosa_akhir").summernote('code',data.diagnosa_akhir);
			}
			if (nama_var=='tindakan'){
			$("#tindakan").summernote('code',data.tindakan);
			}
			
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
			simpan_assesmen();
		}
	});
}
function generate_ranap_all(assesmen_id,pendaftaran_id){
	generate_ranap(assesmen_id,'riwayat_penyakit');
	generate_ranap(assesmen_id,'diagnosa_akhir');
	generate_ranap(assesmen_id,'tindakan');
}
function generate_ranap(assesmen_id,nama_var){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_ranap_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			if (nama_var=='riwayat_penyakit'){
			$("#riwayat_penyakit").summernote('code',data.riwayat_penyakit);
			}
			if (nama_var=='diagnosa_akhir'){
			$("#diagnosa_akhir").summernote('code',data.diagnosa_akhir);
			}
			if (nama_var=='tindakan'){
			$("#tindakan").summernote('code',data.tindakan);
			}
			
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
			simpan_assesmen();
		}
	});
}
function generate_cppt_all(assesmen_id,pendaftaran_id){
	generate_cppt(assesmen_id,'riwayat_penyakit');
	generate_cppt(assesmen_id,'diagnosa_akhir');
	generate_cppt(assesmen_id,'tindakan');
}
function generate_cppt(assesmen_id,nama_var){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_cppt_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			if (nama_var=='riwayat_penyakit'){
			$("#riwayat_penyakit").summernote('code',data.riwayat_penyakit);
			}
			if (nama_var=='diagnosa_akhir'){
			$("#diagnosa_akhir").summernote('code',data.diagnosa_akhir);
			}
			if (nama_var=='tindakan'){
			$("#tindakan").summernote('code',data.tindakan);
			}
			
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
			simpan_assesmen();
		}
	});
}
function generate_ringkasan_all(assesmen_id,pendaftaran_id){
	generate_ringkasan(assesmen_id,'riwayat_penyakit');
	generate_ringkasan(assesmen_id,'diagnosa_akhir');
	generate_ringkasan(assesmen_id,'tindakan');
	generate_ringkasan(assesmen_id,'prognis');
}
function generate_ringkasan(assesmen_id,nama_var){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_ringkasan_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			if (nama_var=='riwayat_penyakit'){
			$("#riwayat_penyakit").summernote('code',data.riwayat_penyakit);
			}
			if (nama_var=='diagnosa_akhir'){
			$("#diagnosa_akhir").summernote('code',data.diagnosa_akhir);
			}
			if (nama_var=='tindakan'){
			$("#tindakan").summernote('code',data.tindakan);
			}
			if (nama_var=='prognis'){
			$("#keadaan").summernote('code',data.prognis);
			}
			
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
			simpan_assesmen();
		}
	});
}
function generate_penunjang_all(pendaftaran_id,st_ranap){
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_penunjang_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				pendaftaran_id_ranap:pendaftaran_id,
				st_ranap:st_ranap,
				var_get:'all',
				
			   },
		success: function(data) {
			$("#penunjang").summernote('code',data.hasil);
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
		}
	});
}
function generate_penunjang_satuan_diagnosa(trx_id,var_get){
	let penunjang_asal=$("#penunjang").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_penunjang_satuan_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				trx_id: trx_id,
				var_get: var_get,
				
			   },
		success: function(data) {
			penunjang_asal=penunjang_asal+data.hasil;
			$("#penunjang").summernote('code',penunjang_asal);
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
		}
	});
}
function generate_terapi_diagnosa(trx_id){
	let penunjang_asal=$("#terapi").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_terapi_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				trx_id: trx_id,
				
			   },
		success: function(data) {
			penunjang_asal=penunjang_asal+data.hasil;
			$("#terapi").summernote('code',penunjang_asal);
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
		}
	});
}
function generate_tindakan_diagnosa(trx_id){
	let penunjang_asal=$("#tindakan").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tsurat/generate_tindakan_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				trx_id: trx_id,
				
			   },
		success: function(data) {
			penunjang_asal=penunjang_asal+data.hasil;
			$("#tindakan").summernote('code',penunjang_asal);
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Generate'});
		}
	});
}
$("#dpjp").change(function(){
	$("#dpjp_ttd").val($("#dpjp").val()).trigger('change.select2');
});
$("#dpjp_ttd").change(function(){
	$("#dpjp").val($("#dpjp_ttd").val()).trigger('change.select2');
});

function get_selisih(){
	let tgl_1=$("#mulai").val();
	let tgl_2=$("#selesai").val();
	$.ajax({
		url: '{site_url}tsurat/selisih_tanggal_surat_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				tgl_1:tgl_1,
				tgl_2:tgl_2,
				
			},
		success: function(data) {
			$("#lama").val(data)
		}
	});
}
$("#lama").keyup(function(){
	if ($(this).val()=='' || $(this).val()=='0'){
		$(this).val(1);
	}
	get_selisih_add();
});
$("#lama").focus(function(){
	$("#lama").select();
});
function get_selisih_add(){
	let tgl_1=$("#mulai").val();
	let lama=$("#lama").val();
	$.ajax({
		url: '{site_url}tsurat/get_selisih_add_surat_diagnosa', 
		dataType: "JSON",
		method: "POST",
		data : {
				tgl_1:tgl_1,
				lama:lama,
				
			},
		success: function(data) {
			$("#selesai").val(data)
		}
	});
}
function disable_edit(){
	if (status_assemen=='2'){
		$("#form_input :input").prop("disabled", true);
		$(".btn_cetak").removeAttr('disabled');
		$(".btn_back").removeAttr('disabled');
	}
}

function close_assesmen(){
	if (parseFloat($("#lama").val())<1){
		sweetAlert("Maaf...", "Tentukan Tanggal Dengan benar", "error");
		return false;
	}

	swal({
		title: "Notifikasi",
		text : "Apakah Anda Yakin telah selesai membuat Surat?",
		icon : "question",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#status_assemen").val(2);
		
		simpan_assesmen();
	});		

	
}
function pilih_data_pendaftaran(tanggal_1,tanggal_2){
	// alert(tanggal_1);
	
	swal({
		title: "Notifikasi",
		text : "Apakah Anda Yakin Akan mengganti kunjungan?",
		icon : "question",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tsurat/ganti_tanggal_diagnosa', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id: $("#assesmen_id").val(),
					tanggal_1:tanggal_1,
					tanggal_2:tanggal_2,
				   },
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Changed'});
				location.reload();
			}
		});
	});		

	
}
function create_assesmen(){
	
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe=$("#idtipe").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let st_ranap=$("#st_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let iddokter=$("#iddokter").val();
	
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Surat "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tsurat/create_assesmen_surat_diagnosa', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					st_ranap:st_ranap,
					tglpendaftaran:tglpendaftaran,
					idpasien:idpasien,
					iddokter:iddokter,
					idtipe:idtipe,
					idpoliklinik:idpoliklinik,
					
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Pengajuan Surat ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tsurat/batal_assesmen_surat_diagnosa', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$(".opsi_change ").change(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur_tgl").change(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur_tgl").blur(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur").blur(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
 $(document).find('.js-summernote').on('summernote.blur', function() {
	if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
  });
function simpan_assesmen(){
		if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		// alert(konsul_id);return false;
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tsurat/simpan_assesmen_surat_diagnosa', 
				dataType: "JSON",
				method: "POST",
				data : {
						status_assemen:$("#status_assemen").val(),
						st_edited:$("#st_edited").val(),
						assesmen_id:$("#assesmen_id").val(),
						
						tglpendaftaran : $("#tglpendaftaran").val(),
						waktupendaftaran : $("#waktupendaftaran").val(),
						dpjp : $("#dpjp").val(),
						riwayat_penyakit : $("#riwayat_penyakit").val(),
						penunjang : $("#penunjang").val(),
						diagnosa_akhir : $("#diagnosa_akhir").val(),
						terapi : $("#terapi").val(),
						tindakan : $("#tindakan").val(),
						keadaan : $("#keadaan").val(),
					},
				success: function(data) {
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='2'){
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
	}
}

function list_surat_diagnosa(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let dpjp=$("#dpjp_my_order").val();
	let poli=$("#poli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_my_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_my_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tsurat/list_surat_diagnosa', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						dpjp:dpjp,
						poli:poli,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_surat_diagnosa_history(){
	// $("#div_history").hide();
	// $("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_his_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_his_order").val();
	let dpjp=$("#dpjp_his_order").val();
	let poli=$("#poli_his_order").val();
	let notransaksi=$("#notransaksi_his_order").val();
	let tanggal_input_1=$("#tanggal_input_1_his_order").val();
	let tanggal_input_2=$("#tanggal_input_2_his_order").val();
	$('#index_history_surat').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_history_surat').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tsurat/list_surat_diagnosa_history', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						dpjp:dpjp,
						poli:poli,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}

</script>