<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	
	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>

<?if ($menu_kiri=='input_perencanaan'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_perencanaan' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_permintaan;
					if ($assesmen_id){
						$tanggalcreated=HumanDateShort($created_date);
						$waktucreated=HumanTime($created_date);
						$tanggalpermintaan=HumanDateShort($tanggal_permintaan);
						$waktupermintaan=HumanTime($tanggal_permintaan);
						
					}else{
						$tanggalpermintaan=date('d-m-Y');
						$waktupermintaan=date('H:i:s');
						$tanggalcreated=date('d-m-Y');
						$waktucreated=date('H:i:s');
						
					}
					
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rm">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Perencanaan Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_my_order()" ><i class="fa fa-send"></i> Perencanaan Saya</a>
				</li>
				<?php if (UserAccesForm($user_acces_form,array('1855'))){?>
				<li class="">
					<a href="#tab_3" onclick="list_history_order()"><i class="fa fa-history"></i> Riwayat Perencanaan</a>
				</li>
				<?}?>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
					
					<div class="row">
						<input type="hidden" id="idtipe_poli" value="{idtipe_poli}"> 
						<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}"> 
						<input type="hidden" id="idpoliklinik" value="{idpoliklinik}"> 
						<input type="hidden" id="idrekanan" value="{idrekanan}"> 
						<input type="hidden" id="iddokter" value="{iddokter}"> 
						<input type="hidden" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" id="st_ranap" value="{st_ranap}"> 
					

						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="iddokter" value="<?=$iddokter?>" >		
						<input type="hidden" id="idtipe" value="<?=$idtipe?>" >		
						
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" disabled class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggalcreated" placeholder="HH/BB/TTTT" name="tanggalcreated" value="<?= $tanggalcreated ?>" required>
										<label for="tanggalcreated">Tanggal <i class="text-muted">Created Date</i></label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" disabled  class="time-datepicker form-control " id="waktucreated" name="waktucreated" value="<?= $waktucreated ?>" required>
										<label for="waktupendaftaran">Waktu <i class="text-muted">Time</i></label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-12">
							<?if($st_lihat_eresep=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_per_ina}</h4>
								<h5 class="push-5 text-center">{judul_per_eng}</h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					
					<div class="row">
					
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' ){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Perencanaan Belum Disimpan</a>!</p>
									</div>
								<?}?>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?php if (UserAccesForm($user_acces_form,array('1851'))){ ?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-success" onclick="close_assesmen()" type="button"><i class="fa fa-send"></i> Simpan & Kirim Perencanaan</button>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<a href="{site_url}tpoliklinik_rm_order/cetak_rencana_rehab/<?=$assesmen_id?>" target="_blank" class="btn btn-info" type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										
										
									<?}?>
									<?if ($status_assemen=='2'){?>
										<button class="btn btn-default btn_back" type="button" onclick="goBack()"><i class="fa fa-reply"></i> Kembali</button>
									<?}?>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<p><strong>{paragraph_1_ina} </strong></p>
										<p class="text-muted">{paragraph_1_eng}</p>
									</div>
									<div class="col-md-12 " >
										<p><strong>{paragraph_2_ina} </strong><i>{paragraph_2_eng}</i></p>
									</div>
								</div>
							
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="diagnosa">{nama_pasien_ina} / <i class="text-muted">{nama_pasien_eng}</i></label>
										<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?= $nomedrec_pasien.' - '.$nama_pasien ?>" required>
									</div>
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="diagnosa">{ttl_ina} / <i class="text-muted">{ttl_eng}</i></label>
										<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?=HumanDateShort($ttl_pasien).' - '.$umur_pasien ?> Tahun" required>
									</div>
									
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-6 ">
										<div class="col-md-12 ">
											<label for="dengan_diagnosa">{diagnosa_per_ina} / <i class="text-muted">{diagnosa_per_eng}</i></label>
											<textarea class="form-control js-summernote auto_blur" id="dengan_diagnosa" width="100%"  rows="2" placeholder="Dengan Diagnosa"> <?=$dengan_diagnosa?></textarea>
										</div>
									</div>
									<div class="col-md-6 ">
										<div class="col-md-12 ">
											<label for="fisio">{fisio_ina} / <i class="text-muted">{fisio_eng}</i></label>
											<textarea class="form-control js-summernote auto_blur" id="fisio"  width="100%"  rows="2" placeholder="Fisio"> <?=$fisio?></textarea>
										</div>
									</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="diagnosa">{sebanyak_per_ina} / <i class="text-muted">{sebanyak_per_eng}</i></label>
										<select tabindex="13" id="sebanyak" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($st_alergi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(112) as $row){?>
											<option value="<?=$row->id?>" <?=($sebanyak == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($sebanyak == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">{selama_per_ina} / <i class="text-muted">{selama_per_eng}</i></label>
										<select tabindex="12" id="selama" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($selama == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(111) as $row){?>
											<option value="<?=$row->id?>" <?=($selama == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($selama == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="example-input-normal">{kontrol_per_ina} / <i class="text-muted">{kontrol_per_eng}</i></label>
										<input tabindex="14" type="text" class="form-control auto_blur" id="kontrol_kembali" value="<?= $kontrol_kembali ?>" required>
									</div>
									
								</div>
								
							</div>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<p><strong>{paragraph_3_ina} </strong></p>
										<p class="text-muted">{paragraph_3_eng}</p>
									</div>
									
								</div>
							
							</div>
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">{judul_per_ina} SAYA</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_my_order" placeholder="No Pendaftaran" name="notransaksi_my_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_my_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_my_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_my_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_my_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_my_order" name="idpoli_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_my_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_my_order" name="iddokter_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter_all as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_my_order()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_my_order">
											<thead>
												<tr>
													<th width="10%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">No.Perencanaan</th>
													<th width="9%">Tanggal Pembuatan</th>
													<th width="10%">Dokter</th>
													<th width="12%">Diagnosa</th>
													<th width="9%">Fisioterapi</th>
													<th width="10%">Sebanyak</th>
													<th width="10%">Selama</th>
													<th width="9%">Kunjungan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_per_ina}</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_his_order" placeholder="No Pendaftaran" name="notransaksi_his_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_his_order" name="idpoli_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_his_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_his_order" name="iddokter_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_history_order()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_history_order">
											<thead>
												<tr>
													<th width="10%">Action</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">No.Perencanaan</th>
													<th width="9%">Tanggal Pembuatan</th>
													<th width="10%">Dokter</th>
													<th width="12%">Diagnosa</th>
													<th width="9%">Fisioterapi</th>
													<th width="10%">Sebanyak</th>
													<th width="10%">Selama</th>
													<th width="9%">Kunjungan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<? $this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/modal_email')?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var class_obat;
$(document).ready(function() {
	// $(".btn_close_left").click(); 
	$(".number").number(true,0,'.',',');
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	disable_edit();
	if ($("#assesmen_id").val()){
		// load_master();
		// load_tarif();
		$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  dialogsInBody: true,
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	}
	list_my_order();
});
function disable_edit(){
	if (status_assemen=='2'){
		$("#form_input :input").prop("disabled", true);
		$(".btn_back").removeAttr("disabled");
	}
}
// $("#idpoli_my_order").change(function(){
		// $.ajax({
			// url: '{site_url}tkonsul/find_dokter/',
			// dataType: "json",
			// method: "POST",
			// data : {
					// idpoliklinik:$(this).val(),
				   // },
			// success: function(data) {
				// // alert(data);
				// $("#iddokter_my_order").empty();
				// $("#iddokter_my_order").append(data);
			// }
		// });

// });
function close_assesmen(){
	
		let nama_tujuan='Rehabilitas Medis';
		swal({
			title: "Notifikasi",
			text : "Apakah Anda Yakin telah selesai membuat Perencanaan? Data permintaan Anda akan otomatis terkirim ke "+nama_tujuan+".",
			icon : "question",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			
			simpan_assesmen();
		});		
	
	
}
function load_master(){
	let idtarif_header=$("#idtarif_header").val();
	let assesmen_id=$("#assesmen_id").val();
	$('#tabel_master').DataTable().destroy();	
	table = $('#tabel_master').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            paging: false,
			scrollCollapse: true,
			scrollY: '400px',
			"columnDefs": [
					{ "width": "5%", "targets": [0,1],  className: "text-center" },
					{ "width": "60%", "targets": [2],  className: "text-left" },
					{ "width": "30%", "targets": 3,  className: "text-right" },
				],
            ajax: { 
                url: '{site_url}Tpoliklinik_rm_order/load_master', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idtarif_header:idtarif_header,
						assesmen_id:assesmen_id,
					   }
            },
			"drawCallback": function( settings ) {
				disable_edit();
				
			 }  
        });
}
function load_tarif(){
	let assesmen_id=$("#assesmen_id").val();
	$('#tabel_tarif').DataTable().destroy();	
	table = $('#tabel_tarif').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            paging: false,
			scrollCollapse: true,
			scrollY: '400px',
			"columnDefs": [
					{ "width": "5%", "targets": [0],  className: "text-center" },
					{ "width": "60%", "targets": [1],  className: "text-left" },
					{ "width": "25%", "targets": 2,  className: "text-right" },
					{ "width": "10%", "targets": 3,  className: "text-center" },
				],
            ajax: { 
                url: '{site_url}Tpoliklinik_rm_order/load_tarif', 
                type: "POST" ,
                dataType: 'json',
				data : {
						assesmen_id:assesmen_id
					   }
            },
			
			"drawCallback": function( settings ) {
				disable_edit();
				
			 }, 
			"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api(), data;
				console.log(data);
				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
	 
				// computing column Total of the complete result 
				var monTotal = api
					.column( 2 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );
					
				// Update footer by showing the total with the reference of the column index 
			$( api.column( 1 ).footer() ).html('Total Tarif');
				$( api.column(2).footer() ).html($.fn.dataTable.render.number(',', '.', 0, '').display(monTotal));
			},
    });
}

function create_assesmen(){
	
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe=$("#idtipe").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let iddokter=$("#iddokter").val();
	let idtarif_header=$("#idtarif_header").val();
	let st_ranap=$("#st_ranap").val();
	
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Perencanaan "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_rm_order/create_assesmen_perencanaan', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					st_ranap:st_ranap,
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					idpasien:idpasien,
					iddokter:iddokter,
					idtipe:idtipe,
					idpoliklinik:idpoliklinik,
					idtarif_header:idtarif_header,
					
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Pengajuan Perencanaan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_rm_order/batal_assesmen_per', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$(".opsi_change ").change(function(){
		simpan_assesmen();
});
$(".auto_blur_tgl").change(function(){
		simpan_assesmen();
});
$(".auto_blur_tgl").blur(function(){
		simpan_assesmen();
});
$(".auto_blur").blur(function(){
		simpan_assesmen();
});
 $(document).find('.js-summernote').on('summernote.blur', function() {
	simpan_assesmen();
  });
function simpan_assesmen(){
		if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		// alert(konsul_id);return false;
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpoliklinik_rm_order/simpan_assesmen_per', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						dengan_diagnosa : $("#dengan_diagnosa").val(),
						fisio : $("#fisio").val(),
						sebanyak : $("#sebanyak").val(),
						selama : $("#selama").val(),
						kontrol_kembali : $("#kontrol_kembali").val(),
						status_assemen : $("#status_assemen").val(),

					},
				success: function(data) {
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='2'){
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
	}
}

function list_my_order(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_my_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_my_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9,10] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpoliklinik_rm_order/list_my_order_per', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_history_order(){
	// $("#div_history").hide();
	// $("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_his_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_his_order").val();
	let iddokter=$("#iddokter_his_order").val();
	let idpoli=$("#idpoli_his_order").val();
	let notransaksi=$("#notransaksi_his_order").val();
	let tanggal_input_1=$("#tanggal_input_1_his_order").val();
	let tanggal_input_2=$("#tanggal_input_2_his_order").val();
	$('#index_history_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_history_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9,10] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpoliklinik_rm_order/list_history_order_per', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
$(document).on("click",".chck_tarif",function(){	
	let idtarif = $(this).data("idtarif");let kelas = $(this).data("kelas");let path = $(this).data("path");let id = $(this).data("id");
	let level = $(this).data("level");let check = $(this).is(":checked");
	if (level==0){
		$("#cover-spin").show();
	}
	 if (check) {
		$.ajax({
			url: '{site_url}Tpoliklinik_rm_order/simpan_tarif', 
			dataType: "JSON",method: "POST",
			data : {
					idtarif:idtarif,path:path,kelas:kelas,level:level,assesmen_id:$("#assesmen_id").val(),idpasien:$("#idpasien").val(),
				},
			success: function(data) {
				if (data==null){
					
				}else{
					if (level>0){
						load_tarif();
						
					}else{
						$("#cover-spin").hide();
						load_tarif();
						load_master();
						
					}
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Tarif'});
					
				}
			}
		});
	} else {
		hapus_tarif(id,level,path);
		
	}

});
function hapus_tarif(id,level,path){
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}Tpoliklinik_rm_order/hapus_tarif',
			type: 'POST',
			data: {id: id,level: level,path:path,assesmen_id:$("#assesmen_id").val()},
			complete: function() {
				$("#cover-spin").hide();
				load_tarif();
				load_master();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}

function copy_order(assesmen_id){
	let template_assesmen_id=assesmen_id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe=$("#idtipe_poli").val();
	let idtarif_header=$("#idtarif_header").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Perencanaan Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_rm_order/copy_order', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
					idpoliklinik:idpoliklinik,
					idtipe:idtipe,
					idtarif_header:idtarif_header,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});
}
function goBack() {
  window.history.back();
}

</script>