<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	
	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>
<?if ($menu_kiri=='pilih_program'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='pilih_program' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		$tab='1';
	?>
		
	<div class="block animated fadeIn push-5-t" data-category="erm_rm">
		<input type="hidden" id="program_rm_id" value="{program_rm_id}">
		<input type="hidden" id="tipe_rm" value="{tipe_rm}">
		<input type="hidden" id="idpasien" value="{idpasien}">
		<input type="hidden" id="pendaftaran_id" value="{pendaftaran_id}">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1" onclick="list_order_rm()"> Order</a>
				</li>
				<li class="">
					<a href="#tab_2" onclick="list_perencanaan()"> Perencanaan</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">DAFTAR PROGRAM REHABILITAS</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_my_order" placeholder="No Pendaftaran" name="notransaksi_my_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_my_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_my_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_my_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_my_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_my_order" name="idpoli_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_my_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_my_order" name="iddokter_my_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter_all as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_order_rm()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_my_order">
											<thead>
												<tr>
													<th width="10%">Action</th>
													<th width="9%">Status Program</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">No.Perencanaan</th>
													<th width="9%">Tanggal Pembuatan</th>
													<th width="10%">Dokter</th>
													<th width="12%">Diagnosa</th>
													<th width="9%">Fisioterapi</th>
													<th width="10%">Sebanyak</th>
													<th width="10%">Selama</th>
													<th width="9%">Kunjungan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">DAFTAR PROGRAM REHABILITAS MEDIS</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_his_order" placeholder="No Pendaftaran" name="notransaksi_his_order" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_his_order" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
											<div class="col-md-8">
												<select id="idpoli_his_order" name="idpoli_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Poliklinik -</option>
													<?foreach($list_poli as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="iddokter_his_order">Tujuan Dokter</label>
											<div class="col-md-8">
												<select id="iddokter_his_order" name="iddokter_his_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>- Semua Dokter -</option>
													<?foreach($list_dokter as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_perencanaan()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_perencanaan">
											<thead>
												<tr>
													<th width="10%">Action</th>
													<th width="9%">Status Program</th>
													<th width="9%">No Pendaftaran</th>
													<th width="9%">No.Perencanaan</th>
													<th width="9%">Tanggal Pembuatan</th>
													<th width="10%">Dokter</th>
													<th width="12%">Diagnosa</th>
													<th width="9%">Fisioterapi</th>
													<th width="10%">Sebanyak</th>
													<th width="10%">Selama</th>
													<th width="9%">Kunjungan</th>
													<th width="12%">User</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
$(document).ready(function() {
	// $(".btn_close_left").click(); 
	$(".number").number(true,0,'.',',');
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	list_order_rm();
	
});
function apply_order($assesmen_id,$tipe_rm,$pendaftaran_id,$st_ranap){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menggunakan Program ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpoliklinik_rm_order/apply_order', 
				dataType: "JSON",
				method: "POST",
				data : {
						program_rm_id:$assesmen_id,
						tipe_rm:$tipe_rm,
						pendaftaran_id:$pendaftaran_id,
						st_ranap:$st_ranap,
						
					},
				success: function(data) {
					$("#program_rm_id").val($assesmen_id);
					$("#tipe_rm").val($tipe_rm);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Program Berhasil diterapkan'});
					if ($tipe_rm=='1'){
						list_order_rm();
					}else{
						list_perencanaan();
						
					}
					
					
					$("#cover-spin").hide();						
				}
			});
		});		
}
function disable_edit(){
	if (status_assemen=='2'){
		$("#form_input :input").prop("disabled", true);
	}
}
function list_order_rm(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let iddokter=$("#iddokter_my_order").val();
	let idpoli=$("#idpoli_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	let program_rm_id=$("#program_rm_id").val();
	let tipe_rm=$("#tipe_rm").val();
	$('#index_my_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_my_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9,10] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpoliklinik_rm_order/list_order_rm', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						program_rm_id:program_rm_id,
						tipe_rm:tipe_rm,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_perencanaan(){
	// $("#div_history").hide();
	// $("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_his_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_his_order").val();
	let iddokter=$("#iddokter_his_order").val();
	let idpoli=$("#idpoli_his_order").val();
	let notransaksi=$("#notransaksi_his_order").val();
	let tanggal_input_1=$("#tanggal_input_1_his_order").val();
	let tanggal_input_2=$("#tanggal_input_2_his_order").val();
	let program_rm_id=$("#program_rm_id").val();
	let tipe_rm=$("#tipe_rm").val();
	$('#index_perencanaan').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_perencanaan').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9,10] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpoliklinik_rm_order/list_perencanaan', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						program_rm_id:program_rm_id,
						tipe_rm:tipe_rm,
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
</script>