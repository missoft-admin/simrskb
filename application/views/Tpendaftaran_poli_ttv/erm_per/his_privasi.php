<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	}	
	.text-bold{
        font-weight: bold;
      }
	  table {
		font-family: Verdana, sans-serif;
        font-size: 12px !important;
        border-collprivasie: collprivasie !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 13px;
		 
      }
      .content td {
        padding: 10px;
		border: 0px solid #6033FF;
      }
	  .has-error2 {
		border-color: #d26a5c;
	}
	  .select2-selection {
		  border-color: green; /* example */
		}
      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 14px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
		border-bottom:1px solid #000 !important;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='his_privasi' || $menu_kiri=='his_privasi_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_privasi' || $menu_kiri=='his_privasi_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
	
	$disabel_input='';
	$disabel_cetak='disabled';
	$list_dokter=(get_all('mdokter',array('status'=>1)));		
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_per">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-note"></i> Pernyataan History</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
					<input type="hidden" readonly id="st_edited" value="{st_edited}">
					<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
					<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
					<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
					<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
					<input type="hidden" id="assesmen_detail_id" value="" >			
					<div class="row">
						<div class="col-md-12">
							<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<h4 class="font-w400 push-5 text-center"><i>{judul_header_eng}</i></h4>
						</div>
					</div>
					<div class="row">
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-4 ">
								<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p>Info <a class="alert-link" href="javascript:void(0)"> Perubahan Ke - <?=$jml_edit?> </a>!</p>
								</div>
								
							</div>
							
						</div>
						<?}?>
						<div class="form-group" >
							<div class="col-md-12 ">
								<table class="myFormat">
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{paragraf_1_ina}<br><i class="text-muted">{paragraf_1_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{pilih_ttd_ina}<br><i class="text-muted">{pilih_ttd_eng}</i></td>
									</tr>
									<tr>
										<td style="width:50%%" colspan="3">
											<select id="pilih_ttd_id" class="<?=($pilih_ttd_id!=$pilih_ttd_id_asal?'edited':'')?> form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($pilih_ttd_id=='#'?'selected':'')?>>- Pilih yang Bertandatangan -</option>
												<option value="1" <?=($pilih_ttd_id=='1'?'selected':'')?>>Penanggung Jawab</option>
												<option value="2" <?=($pilih_ttd_id=='2'?'selected':'')?>>Pengantar</option>
												<option value="3" <?=($pilih_ttd_id=='3'?'selected':'')?>>Pasien Sendiri</option>
												<option value="4" <?=($pilih_ttd_id=='4'?'selected':'')?>>Lainnya</option>
												
											</select>
										</td>
										<td style="width:50%" colspan="3" class="text-bold">
											<?if ($st_input_privasi=='1'){?>
											<?if ($assesmen_id==''){?>
											<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_awal()" type="button"><i class="si si-doc"></i> New </button>
											<?}?>
											<?}?>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group" style="margin-top:-10px!important">
							<div class="col-md-12 ">
								<table>
									<tr>
										<td style="width:13%" class="text-bold">{nama_ttd_ina} : <br><i class="text-muted">{nama_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<input id="nama_ttd"  class="form-control pull-15-r <?=($nama_ttd!=$nama_ttd_asal?'edited':'')?>"  value="{nama_ttd}"  type="text">
										</td>
										<td style="width:13%" class="text-bold push-15-l">{ttl_ttd_ina} : <br><i class="text-muted">{ttl_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<div class="input-group date">
												<input id="ttl_ttd" class="js-time-modal js-datepicker form-control <?=($ttl_ttd!=$ttl_ttd_asal?'edited':'')?>"  type="text" data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" value="<?=($ttl_ttd?HumanDateShort($ttl_ttd):'')?>" required>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</td>
										<td style="width:13%" class="text-bold push-15-l">{umur_ttd_ina} : <br><i class="text-muted">{umur_ttd_eng}</i></td>
										<td style="width:21%" class="">
											<input id="umur_ttd" class="form-control <?=($umur_ttd!=$umur_ttd_asal?'edited':'')?>"  value="{umur_ttd}"  type="text">
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td style="width:13%" class="text-bold">{alamat_ttd_ina} : <br><i class="text-muted">{alamat_ttd_eng}</i></td>
										<td style="width:53%" class="" colspan="3">
											<input id="alamat_ttd" class="form-control pull-15-r <?=($alamat_ttd!=$alamat_ttd_asal?'edited':'')?>"   value="{alamat_ttd}"  type="text">
										</td>
										<td style="width:13%" class="text-bold push-15-l">{hubungan_ina} : <br><i class="text-muted">{hubungan_eng}</i></td>
										<td style="width:21%" class="">
											<select id="hubungan_ttd"  class="<?=($hubungan_ttd!=$hubungan_ttd_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="0" <?=($hubungan_ttd == '0' ? 'selected="selected"' : '')?>>Diri Sendiri</option>
												<?foreach(list_variable_ref(9) as $row){?>
												<option value="<?=$row->id?>" <?=($hubungan_ttd == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
											</select>
										</td>
									</tr>
								</table>
								</table>
								<table class="myFormat">
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{menyatakan_ina}<br><i class="text-muted">{menyatakan_eng}</i></td>
									</tr>
									
									<tr>
										<td style="width:100%%" colspan="3">
											<select id="hereby"  class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($hereby == '' ? 'selected="selected"' : '')?>>Pilih</option>
												<?foreach(list_variable_ref(434) as $row){?>
												<option value="<?=$row->id?>" <?=($hereby == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
											</select>
										</td>
										
									</tr>
									
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{paragraf_2_ina}<br><i class="text-muted">{paragraf_2_eng}</i></td>
									</tr>
									
								</table>
								<table class="myFormat">
								<tr>
									<td>
									<div class="table-responsive">
										<table class="table table-bordered" id="tabel_nama" >
											<thead>
												<tr>
													<th width="4%" class="text-center">No</th>
													<th width="30%" class="text-center">{nama_tabel_ina} / <i class="text-muted">{nama_tabel_eng}</i></th>
													<th width="50%" class="text-center">{ket_tabel_ina} / <i class="text-muted">{ket_tabel_eng}</i></th>
													<th width="16%" class="text-center">Action</th>
												</tr>
												<tr>
													<td class="text-center" style="vertical-align: top;">
														<input class="form-control" id="nama_detail_id" type="hidden">
													</td>
													<td class="text-center" style="vertical-align: top;">
														<input class="form-control" id="nama" type="text">
													</td>
													<td class="text-center" style="vertical-align: top;">
														<input class="form-control" id="keterangan" type="text">
													</td>
													
													<td width="10%" class="text-center" style="vertical-align: top;">
														<span class="input-group-btn">
															<button class="btn btn-success" title="Simpan" onclick="simpan_nama()" type="button"><i class="fa fa-save"></i></button>
															<button class="btn btn-warning"  title="Clear" onclick="clear_nama()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</td>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
									</td>
								</tr>
								</table>
								<table class="myFormat">
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{saya_juga_ina}<br><i class="text-muted">{saya_juga_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%%" colspan="3">
											<select id="izin"  class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($izin == '' ? 'selected="selected"' : '')?>>Pilih</option>
												<?foreach(list_variable_ref(433) as $row){?>
												<option value="<?=$row->id?>" <?=($izin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
											</select>
										</td>
										
									</tr>
									
									
								</table>
								<table class="myFormat">
								<tr>
									<td>
									<div class="table-responsive">
										<table class="table table-bordered" id="tabel_berupa" >
											<thead>
												<tr>
													<th width="4%" class="text-center">No</th>
													<th width="30%" class="text-center">{berupa_ina} / <i class="text-muted">{berupa_eng}</i></th>
													<th width="50%" class="text-center">{ket_tabel_2_ina} / <i class="text-muted">{ket_tabel_2_eng}</i></th>
													<th width="16%" class="text-center">Action</th>
												</tr>
												<tr>
													<td class="text-center" style="vertical-align: top;">
														<input class="form-control" id="berupa_detail_id" type="hidden">
													</td>
													<td class="text-center" style="vertical-align: top;">
														<select id="berupa"  class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" selected>Pilih</option>
															<?foreach(list_variable_ref(435) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
														</select>
													</td>
													<td class="text-center" style="vertical-align: top;">
														<input class="form-control" id="keterangan_berupa" type="text">
													</td>
													
													<td width="10%" class="text-center" style="vertical-align: top;">
														<span class="input-group-btn">
															<button class="btn btn-success" title="Simpan" onclick="simpan_berupa()" type="button"><i class="fa fa-save"></i></button>
															<button class="btn btn-warning"  title="Clear" onclick="clear_berupa()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</td>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
									</td>
								</tr>
								</table>
								<table>
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{terhadap_ina}<br><i class="text-muted">{terhadap_eng}</i></td>
									</tr>
									<tr>
										<td style="width:13%" class="text-bold">{noreg_ina}<br><i class="text-muted">{noreg_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {nopendaftaran}</td>
										<td style="width:13%" class="text-bold">{nomedrec_ina}<br><i class="text-muted">{nomedrec_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {no_medrec}</td>
										<td style="width:13%" class="text-bold">{nama_pasien_ina}<br><i class="text-muted">{nama_pasien_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {namapasien}</td>
									</tr>
									<tr>
										<td style="width:13%" class="text-bold">{ttl_pasien_ina}<br><i class="text-muted">{ttl_pasien_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: <?=($tanggal_lahir?HumanDateShort($tanggal_lahir):'')?></td>
										<td style="width:13%" class="text-bold">{umur_pasien_ina}<br><i class="text-muted">{umur_pasien_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {umurtahun} Tahun {umurbulan} Bulan {umurhari} Hari</td>
										<td style="width:13%" class="text-bold">{jk_ina}<br><i class="text-muted">{jk_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {jk}</td>
									</tr>
								</table>
								<table>
									<tr>
										<td style="width:100%" class="text-bold">{paragraf_3_ina}<br><i class="text-muted">{paragraf_3_eng}</i></td>
									</tr>
								</table>
									
								<table>
									<tr>
										<td style="width:100%" class="text-bold"><?=strip_tags($paragraf_4_ina)?><br><i class="text-muted"><?=strip_tags($paragraf_4_eng)?></i></td>
									</tr>
								</table>
									
								<table>
									<tr>
										<td style="width:25%" class="text-center text-bold">{yg_pernyataan_ina}<br><i class="text-muted">{yg_pernyataan_eng}</i></td>
										<td style="width:25%" class="text-center text-bold">{yg_menerima_ina}<br><i class="text-muted">{yg_menerima_eng}</i></td>
										<td style="width:25%" class="text-center text-bold">{saksi_kel_ina}<br><i class="text-muted">{saksi_kel_eng}</i></td>
										<td style="width:25%" class="text-center text-bold">{saksi_rs_ina}<br><i class="text-muted">{saksi_rs_eng}</i></td>
									</tr>
									<tr>
										<td style="width:25%" class="text-center">
											<?if ($ttd_pernyataan){?>
											<div class="img-container fx-img-rotate-r text-center">
												<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_pernyataan?>" alt="" title="">
												<?if ($status_assemen=='1'){?>
												<div class="img-options">
													<div class="img-options-content">
														<div class="btn-group btn-group-sm">
															<a class="btn btn-default" onclick="modal_ttd('ttd_pernyataan')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
															<a class="btn btn-default btn-danger" onclick="hapus_ttd('ttd_pernyataan')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
														</div>
													</div>
												</div>
											</div>
											<?}?>
										<?}else{?>
											<button class="btn btn-sm btn-success" onclick="modal_ttd('ttd_pernyataan')" id="btn_ttd_lm" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
										<?}?>
										</td>
										<td style="width:25%" class="text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($yg_menerima?$yg_menerima:$login_ppa_id)?>" alt="" title="">
										</td>
										<td style="width:25%" class="text-center">
											<?if ($saksi_kel_ttd){?>
											<div class="img-container fx-img-rotate-r text-center">
												<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$saksi_kel_ttd?>" alt="" title="">
												<?if ($status_assemen=='1'){?>
												<div class="img-options">
													<div class="img-options-content">
														<div class="btn-group btn-group-sm">
															<a class="btn btn-default" onclick="modal_ttd('saksi_kel_ttd')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
															<a class="btn btn-default btn-danger" onclick="hapus_ttd('saksi_kel_ttd')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
														</div>
													</div>
												</div>
												<?}?>
											</div>
										<?}else{?>
											<button class="btn btn-sm btn-success" onclick="modal_ttd('saksi_kel_ttd')" id="btn_ttd_lm" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
										<?}?>
										</td>
										<td style="width:25%" class="text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id?$petugas_id:$login_ppa_id)?>" alt="" title="">
										</td>
									</tr>
									<tr>
										<td style="width:25%" class="text-center text-bold">{nama_ttd}</td>
										<td style="width:25%" class="text-center text-bold"><?=get_nama_ppa($yg_menerima)?>
										<?if ($status_assemen=='1'){?>
										<br>
										<button class="btn btn-xs btn-success" onclick="show_modal_ttd_petugas('yg_menerima')"type="button"><i class="fa fa-pencil"></i> </button>
										<?}?>
										</td>
										<td style="width:25%" class="text-center text-bold">{saksi_kel}</td>
										<td style="width:25%" class="text-center text-bold"><?=get_nama_ppa($petugas_id)?>
										<?if ($status_assemen=='1'){?>
										<br>
										<button class="btn btn-xs btn-success" onclick="show_modal_ttd_petugas('petugas_id')"type="button"><i class="fa fa-pencil"></i> </button>
										<?}?>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<?}?>
					</div>	
					<?php echo form_close() ?>
					
				</div>
			</div>
			
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
var status_assemen=$("#status_assemen").val();
$(document).ready(function() {
	$(".btn_close_left").click();
	validate_simpan();
	disabel_edit();
	load_nama();
	load_berupa();
});
function save_edit_assesmen(){
	let jml_edit=$("#jml_edit").val();
	let keterangan_edit=$("#keterangan_edit").val();
	let alasan_id=$("#alasan_id_edit").val();
	let assesmen_detail_id=$("#assesmen_detail_id").val();
	$("#st_sedang_edit").val(1);
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_edit").modal('hide');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/save_edit_assesmen_privasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				jml_edit:jml_edit,
				assesmen_id:assesmen_detail_id,
				alasan_id:alasan_id,
				keterangan_edit:keterangan_edit,
				idpasien:$("#idpasien").val(),
				pendaftaran_id:$("#pendaftaran_id").val(),
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Gagal!",
					text: "Edit.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				let pendaftaran_id_ranap=data.pendaftaran_id_ranap;
				$("#cover-spin").show();
				window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id_ranap+"/erm_dpjp/his_privasi'); ?>";
				
				
			}
		}
	});
}
function hapus_record_assesmen(){
	let id=$("#assesmen_detail_id").val();
	let keterangan_hapus=$("#keterangan_hapus").val();
	let alasan_id=$("#alasan_id").val();
	
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_hapus").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Pernyataan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_privasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					keterangan_hapus:keterangan_hapus,
					alasan_id:alasan_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					list_history_pengkajian();
					$("#modal_hapus").modal('hide');
				}
			}
		});
	});

}
function list_history_pengkajian(){
	let assesmen_id=$("#assesmen_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let mppa_id=$("#mppa_id").val();
	let iddokter=$("#iddokter").val();
	let idrawat_ranap=$("#idrawat_ranap").val();
	let notransaksi=$("#notransaksi").val();
	let tanggal_input_1=$("#tanggal_input_1").val();
	let tanggal_input_2=$("#tanggal_input_2").val();
	let idtipe=$("#idtipe").val();
	let idruang=$("#idruang").val();
	let idbed=$("#idbed").val();
	let idkelas=$("#idkelas").val();
	$('#index_history_kajian').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_history_kajian').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					{  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,5,6,7] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [1,2,3,5] },
					 { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_privasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						iddokter:iddokter,
						mppa_id:mppa_id,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						idrawat_ranap:idrawat_ranap,
						idtipe:idtipe,
						idruang:idruang,
						idbed:idbed,
						idkelas:idkelas,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Pernyataan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			
			simpan_assesmen();
		});		
	
}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Pernyataan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_assesmen_privasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function hapus_ttd(nama_field){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/hapus_ttd/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					nama_field:nama_field,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function modal_ttd(){
	$("#nama_field_ttd").val('ttd_pernyataan');
	$("#modal_ttd").modal('show');
}
function show_modal_ttd_petugas(){
	$("#modal_ttd_petugas").modal('show');
}
$(document).on("click","#btn_save_ttd",function(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var nama_field=$("#nama_field_ttd").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/save_ttd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				nama_field:nama_field,
		  },success: function(data) {
				location.reload();
			}
		});
	
});
function create_awal(){
	if ($("#pilih_ttd_id").val()=='#'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Yang Bertandatangan",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	
	let idpasien=$("#idpasien").val();
	let pilih_ttd_id=$("#pilih_ttd_id").val();
	
	let template=$("#pilih_ttd_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Pernyataan "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_privasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					pilih_ttd_id:pilih_ttd_id,
				   },
			success: function(data) {
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});
}


$("#ttl_ttd").blur(function(){
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/getUmur', 
		dataType: "JSON",
		method: "POST",
		data : {
				ttl_ttd:$("#ttl_ttd").val(),
				
			   },
		success: function(data) {
			$("#umur_ttd").val(data);
		}
	});
	
});
$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
function set_petugas(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/getNik', 
		dataType: "JSON",
		method: "POST",
		data : {
				nip:$("#nip").val(),
				assesmen_id:$("#assesmen_id").val(),
			   },
		success: function(data) {
			if (data){
				$.toaster({priority : 'success', title : 'Succes!', message : ' Mengganti Pernyataan'});
				location.reload();
			}else{
				$("#cover-spin").hide();
				swal({
					title: "Gagal",
					text: "Petugas Tidak ditemukan",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				return false;
			}
			
		}
	});
}
function edit_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_edit").modal('show');
	document.getElementById("modal_edit").style.zIndex = "1201";
	$("#alasan_id_edit").select2({
		dropdownParent: $("#modal_edit")
	  });
}
function validate_simpan(){
	let st_simpan=true;
	if ($("#nama_ttd").val()==''){
		st_simpan=false;
	}
	if ($("#iddokter_baru").val()=='#'){
		st_simpan=false;
	}
	if ($("#ttd_pernyataan").val()==''){
		// st_simpan=false;
	}
	if ($("#iddokter_awal").val()==$("#iddokter_baru").val()){
		st_simpan=false;
	}
	
	if (st_simpan==true){
		$(".btn_simpan").prop('disabled',false);
	}else{
		$(".btn_simpan").prop('disabled',true);
		
	}
}
function hapus_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_hapus").modal('show');
	document.getElementById("modal_hapus").style.zIndex = "1201";
	$("#alasan_id").select2({
		dropdownParent: $("#modal_hapus")
	  });
	// document.getElementById("alasan_id").style.zIndex = "1202";
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		
	}
}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Pernyataan Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_privasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function load_berupa(){
	$("#cover-spin").show();
	var assesmen_id=$("#assesmen_id").val();
	var status_assemen=$("#status_assemen").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_berupa_privasi/',
		dataType: "json",
		type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				
				
		  },
		success: function(data) {
			$("#tabel_berupa tbody").empty();
			$("#tabel_berupa tbody").append(data.tabel);
			$("#cover-spin").hide();
			disabel_edit();
				
		}
	});
}
function load_nama(){
	$("#cover-spin").show();
	var assesmen_id=$("#assesmen_id").val();
	var status_assemen=$("#status_assemen").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/load_nama_privasi/',
		dataType: "json",
		type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				
				
		  },
		success: function(data) {
			$("#tabel_nama tbody").empty();
			$("#tabel_nama tbody").append(data.tabel);
			$("#cover-spin").hide();
			disabel_edit();
				
		}
	});
}
// pilih_ttd_id
$("#pilih_ttd_id").change(function(){
	if ($("#assesmen_id").val()!=''){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		
		let idpasien=$("#idpasien").val();
		let pilih_ttd_id=$("#pilih_ttd_id").val();
		let assesmen_id=$("#assesmen_id").val();
		
		let template=$("#pilih_ttd_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mengganti Pernyataan Menjadi "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_pilih_privasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						pilih_ttd_id:pilih_ttd_id,
						assesmen_id:assesmen_id,
					   },
				success: function(data) {
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Mengganti Pernyataan'});
					location.reload();
				}
			});
		});
	}
});
$("#provinsi_id_ttd").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kabupaten_id_ttd").empty();
			$("#kecamatan_id_ttd").empty();
			$("#kelurahan_id_ttd").empty();
			$("#kodepos_ttd").val('');
			$("#kabupaten_id_ttd").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kabupaten_id_ttd").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			// alert(kode);
			$("#kecamatan_id_ttd").empty();
			$("#kelurahan_id_ttd").empty();
			$("#kodepos_ttd").val('');
			$("#kecamatan_id_ttd").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kecamatan_id_ttd").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kelurahan_id_ttd").empty();
			$("#kodepos_ttd").val('');
			$("#kelurahan_id_ttd").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kelurahan_id_ttd").change(function() {
	var kode = $(this).val();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kodepos_ttd").val(data.kodepos);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
</script>