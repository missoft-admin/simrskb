<style>
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	}	
	.auto_blur{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	fieldset {
	  background-color: #eeeeee;
	  
	}

	legend {
	  background-color: gray;
	  color: white;
	  padding: 5px 5px;
	}
	.error{
		color:red;
	}
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_info_ods' || $menu_kiri=='input_info_ods_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_info_ods' || $menu_kiri=='input_info_ods_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
					$waktudatang=date('H:i:s');
					$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					
					if ($st_input_assesmen=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_per">
	<?if ($st_lihat_info_ods=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_info_ods()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-validation-bootstrap form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >	
						
						<input type="hidden" id="pendaftaran_id_ranap_ini" value="<?=($st_ranap=='1'?$pendaftaran_id_ranap:$pendaftaran_id)?>" >						
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_info_ods=='1'){?>
									<button class="btn btn-primary" id="btn_create_info_ods" onclick="create_info_ods()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_info_ods()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_info_ods()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<a href="{base_url}tpendaftaran_ranap_erm/cetak_info_ods/<?=$assesmen_id?>" class="btn btn-info" target="_blank" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
										<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
										<?}?>

									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input  type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_info_ods" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_info_ods()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<?
							if ($tanggal_opr){
								$tanggal_opr=HumanDateShort($tanggal_opr);
							}else{
								$tanggal_opr=date('d-m-Y');
								
							}
							if ($mulai){$mulai=HumanTime($mulai);}else{$mulai=date('H:i:s');}
							
							
							$list_ppa_dokter=get_all('mppa',array('tipepegawai'=>'2','staktif'=>1));
							$list_ppa_all=get_all('mppa',array('staktif'=>1));
						?>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_1_ina?></strong><br><i class="text-muted"><?=$paragraf_1_eng?></i></label>
									
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="dpjp_ppa"><strong><?=$dpjp_ina?></strong><br><i class="text-muted"><?=$dpjp_eng?></i></label>
									<select id="dpjp_ppa" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($dpjp_ppa == '' ? 'selected' : '')?>>Pilih Opsi</option>
										<?foreach($list_ppa_dokter as $row){?>
										<option value="<?=$row->id?>" <?=($dpjp_ppa == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-2">
									<label for="tanggal_opr"><strong><?=$tanggal_tindakan_ina?></strong><br><i class="text-muted"><?=$tanggal_tindakan_eng?></i></label>
									<div class="input-group date">
										<input id="tanggal_opr" class="js-datepicker form-control auto_blur" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggal_opr ?>" >
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
								<div class="col-md-2">
									<label for="mulai"><strong><?=$waktu_tindakan_ina?></strong><br><i class="text-muted"><?=$waktu_tindakan_eng?></i></label>
									<div class="input-group">
										<input id="mulai" class="time-datepicker form-control auto_blur" type="text" name="mulai" value="<?= $mulai ?>" >
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="rencana_bedah"><strong><?=$rencana_ina?></strong><br><i class="text-muted"><?=$rencana_eng?></i></label>
									<input id="rencana_bedah" class="form-control auto_blur " type="text"  value="{rencana_bedah}" name="rencana_bedah" placeholder="" >
								</div>
								
							</div>
							
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-4">
									<label for="jenis_bius"><strong><?=$jenis_bius_ina?></strong><br><i class="text-muted"><?=$jenis_bius_eng?></i></label>
									<select id="jenis_bius" name="jenis_bius" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_bius == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(348) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_bius == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bius == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-2">
									<label for="pengencer_darah"><strong><?=$minum_ina?></strong><br><i class="text-muted"><?=$minum_eng?></i></label>
									<select id="pengencer_darah" name="pengencer_darah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_bius == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(431) as $row){?>
										<option value="<?=$row->id?>" <?=($pengencer_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengencer_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="detail_pengencer"><strong><?=$jika_ya_ina?></strong><br><i class="text-muted"><?=$jika_ya_eng?></i></label>
									<input id="detail_pengencer" class="form-control auto_blur " type="text"  value="{detail_pengencer}" name="detail_pengencer" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$info_ina?></strong><br><i class="text-muted"><?=$info_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_2_ina?></strong><br><i class="text-muted"><?=$paragraf_2_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_3_ina?></strong><br><i class="text-muted"><?=$paragraf_3_eng?></i></label>
								</div>
								
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_4_ina?></strong><br><i class="text-muted"><?=$paragraf_4_eng?></i></label>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-3">
									<label for="jenis_pasien"><strong><?=$label_pasien_ina?></strong><br><i class="text-muted"><?=$label_pasien_eng?></i></label>
									<select id="jenis_pasien" name="jenis_pasien" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_pasien == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(428) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_pasien == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_pasien == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-5">
									<label for="informasi"><strong><?=$label_info_ina?></strong><br><i class="text-muted"><?=$label_info_eng?></i></label>
									<input id="informasi" readonly class="form-control auto_blur " type="text"  value="{informasi}" name="informasi" placeholder="" >
								</div>
								<div class="col-md-4">
									<label for="puasa_mulai"><strong><?=$label_puasa_ina?></strong><br><i class="text-muted"><?=$label_puasa_eng?></i></label>
									<input id="puasa_mulai" class="form-control auto_blur " type="text"  value="{puasa_mulai}" name="puasa_mulai" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_5_ina?></strong><br><i class="text-muted"><?=$paragraf_5_eng?></i></label>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: 10px;">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="index_dokumen">
											<thead>
												<tr>
													<th width="5%" class="text-center">No</th>
													<th width="30%" class="text-center"><strong><?=$paragraf_dok_ina?></strong><br><i class="text-muted"><?=$paragraf_dok_eng?></i></th>
													<th width="15%" class="text-center"><strong><?=$label_status_ina?></strong><br><i class="text-muted"><?=$label_status_eng?></i></th>
													<th width="35%" class="text-center"><strong><?=$label_ket_ina?></strong><br><i class="text-muted"><?=$label_ket_eng?></i></th>
													<?if ($status_assemen=='1' || $status_assemen=='3'){?>
													<th width="15%" class="text-center"><strong>ACTION</strong><br><i class="text-muted">Aksi</i></th>
													<?}?>
												</tr>
												<?if ($status_assemen=='1' || $status_assemen=='3'){?>
												<tr>
													<th width="5%" class="text-center">#</th>
													<th width="30%" class="text-center">
														<select id="nama_dok_id" name="nama_dok_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(list_variable_ref(430) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th width="15%" class="text-center">
														<select id="status_dok" name="status_dok" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(list_variable_ref(429) as $row){?>
															<option value="<?=$row->id?>" ><?=$row->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th width="35%" class="text-center">
														<input id="ket_dok" class="form-control" type="text" style="width: 100%;"  value="" placeholder="" >
													</th>
													<th width="15%" class="text-center">
														<button class="btn btn-primary btn-sm" type="button" onclick="add_dok()" id="btn_tambah_dok" ><i class="fa fa-save"></i> Tambah</button>
														<button class="btn btn-warning btn-sm" type="button" onclick="clear_dok()" id="btn_clear_info" name="btn_tambah_info"><i class="fa fa-refresh"></i> Clear</button>
													</th>
												</tr>
												<?}?>
												<input type="hidden" id="dok_id" value="" >	
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_6_ina?></strong><br><i class="text-muted"><?=$paragraf_6_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_7_ina?></strong><br><i class="text-muted"><?=$paragraf_7_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$label_catatan_ina?></strong><br><i class="text-muted"><?=$label_catatan_eng?></i></label>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								
								<div class="col-md-12">
									<label class="push-20-l"><?=$content_catatan_ina?></label>
								</div>
								
							</div>
						<?}?>
					</div>
					
					<?php echo form_close() ?>
					
					<?if ($assesmen_id){?>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<table style="width:100%">
									<tr>
										<td style="width:33%" class="text-bold text-center"><strong><?=$label_keluarga_ina?></strong><br><i class="text-muted"><?=$label_keluarga_eng?></i></td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center"><strong><?=$label_petugas_ina?></strong><br><i class="text-muted"><?=$label_petugas_eng?></i></td>
									</tr>
									<tr>
										
										<td style="width:33%" class="text-bold text-center">
											<?if ($ttd_pasien){?>
												<div class="img-container fx-img-rotate-r">
													<img style="width:120px;height:120px; text-align: center;" src="<?=$ttd_pasien?>" alt="">
													<?if ($status_assemen!='2'){?>
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="modal_ttd('ttd_pasien')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd('ttd_pasien')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
													<?}?>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success" onclick="modal_ttd('ttd_pasien')" id="btn_ttd_lm" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
													
											
										</td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id)?>" alt="" title="">
										</td>
									</tr>
									<?
										$arr_saksi=get_nama_ppa_array($petugas_id);
									?>
									<tr>
										<td style="width:33%" class="text-bold text-center text-bold">
											<strong><?=($nama_pasien)?></strong>
											
										</td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center text-bold"><i class="text-bold">
											<?if ($status_assemen=='1'){?>
											<select id="petugas_id" name="petugas_id" class="js-select2 form-control opsi_petugas_id" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Pilih Saksi RS -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>" <?=($petugas_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
											</select>
											<?}else{?>
												<strong><?=$arr_saksi['nama']?></strong><br><i>(<?=$arr_saksi['nik']?>)</i>
											<?}?>
												
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					<?}?>
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header_ina}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-right push-20-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_info_ods=='1'){?>
										<button class="btn btn-primary" id="btn_create_info_ods" onclick="create_info_ods()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										<?if ($assesmen_id!=''){?>
											<?if ($status_assemen=='3'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_info_ods()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
											<?}?>
											<?if ($status_assemen=='1'){?>
											<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_info_ods()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
											<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
											<?}?>
											<?if ($assesmen_id!=''){?>
											<a href="{base_url}tpendaftaran_ranap_erm/cetak_ringkasan_info_ods/<?=$assesmen_id?>" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
											<?}?>
											<?if ($status_assemen=='3'){?>
												<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
											<?}?>
											<?if ($status_assemen=='2'){?>
												<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
												<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_per/input_info_ods" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
											<?}?>
										<?}?>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<?if ($st_ranap=='1'){?>
													<option value="<?=$pendaftaran_id_ranap?>" >Dirawat Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}else{?>
													<option value="<?=$pendaftaran_id?>" >Poliklinik Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>

												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template_info_ods()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<?if ($assesmen_id){?>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan  </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
							<input type="hidden" readonly id="nama_field_ttd" value="">
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="col-md-12">
								<label for="example-input-normal">NAMA</label>
								<input tabindex="32" type="text" class="form-control "  id="nama_field" value="">
							</div>
						</div>
					</div>

				</div><br>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="nama_pasien" value="<?=$nama_pasien?>" >	
<input type="hidden" id="ttd_pasien" value="<?=$ttd_pasien?>" >	
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_info_ods()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_info_ods()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tpendaftaran_poli_ttv/erm_perencanaan/modal_email')?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script type="text/javascript" src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
 <script src="{js_path}plugins/magnific-popup/magnific-popup.min.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var myDropzone;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
function modal_ttd(nama_field_ttd){
	$("#nama_field_ttd").val(nama_field_ttd);
	if (nama_field_ttd=='ttd_pasien'){
			$("#nama_field").val($("#nama_pasien").val());
	}
	var signature64=$("#"+nama_field_ttd).val();
	if (signature64){
		sig.signature('enable').signature('draw', signature64);
		$("#signature64").val(signature64)
	}
	$("#modal_ttd").modal('show');
}
$(document).on("click","#btn_save_ttd",function(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var nama_field_ttd=$("#nama_field_ttd").val();
	var nama_field=$("#nama_field").val();
	if (nama_field==''){
		sweetAlert("Maaf...", "Nama Tandatangan Harus diisi!", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/save_ttd_info_ods/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				nama_field:nama_field,
				nama_field_ttd:nama_field_ttd,
		  },success: function(data) {
				location.reload();
			}
		});
	
});
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
$(".opsi_petugas_id").change(function(){
		if ($("#st_edited").val()=='0'){
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/simpan_petugas_info_ods', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						petugas_id : $("#petugas_id").val(),
						
					},
				success: function(data) {
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Petugas'});
						location.reload();	
					}
				}
			});
		}
});
function hapus_ttd(nama_field_ttd){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64").val('');
	var signature64=$("#signature64").val();
	var nama_field_ttd=nama_field_ttd;
	var nama_field=$("#nama_field").val();
	
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/save_ttd_info_ods/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			assesmen_id:assesmen_id,
			signature64:signature64,
			nama_field:nama_field,
			nama_field_ttd:nama_field_ttd,
	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
		  $("#cover-spin").show();
			location.reload();
		}
	});
}
$(document).ready(function() {
			// $("#form1").validate();
	
	disabel_edit();

	$('.number').number(true, 0);
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_awal_assesmen=false;
		load_dok_info_ods();
	}else{
		list_history_pengkajian()
	}
	
	// load_data_rencana_asuhan_info_ods(1);
});

	$(".opsi_change").change(function(){
		console.log('OPSI CHANTE')
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});
	

	$(".auto_blur").blur(function(){
		console.log('BLUR')
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
		
	});
	$('.auto_blur').on('summernote.blur', function() {
		console.log('BLUR')
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
	});
	function simpan_assesmen(){
	// alert(load_awal_assesmen);
	if (status_assemen !='2'){
		
		if (load_awal_assesmen==false){
		let assesmen_id=$("#assesmen_id").val();
		// alert($("#tanggal_keluar").val());return false;
		let tanggaldatang=$("#tanggaldatang").val();
		let waktudatang=$("#waktudatang").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/save_info_ods', 
				dataType: "JSON",
				method: "POST",
				data : {
						// nama_template:nama_template,
						tanggaldatang:tanggaldatang,
						waktudatang:waktudatang,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						
						dpjp_ppa : $("#dpjp_ppa").val(),
						rencana_bedah : $("#rencana_bedah").val(),
						tanggal_opr : $("#tanggal_opr").val(),
						mulai : $("#mulai").val(),
						jenis_bius : $("#jenis_bius").val(),
						pengencer_darah : $("#pengencer_darah").val(),
						detail_pengencer : $("#detail_pengencer").val(),
						jenis_pasien : $("#jenis_pasien").val(),
						informasi : $("#informasi").val(),
						puasa_mulai : $("#puasa_mulai").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
			}
		 }
		}
	}
	function add_dok(){
		let assesmen_id=$("#assesmen_id").val();
		// alert($("#tanggal_keluar").val());return false;
		if ($("#nama_dok_id").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Dokumen!", "error");
			return false;
		}
		if ($("#status_dok").val()==''){
			sweetAlert("Maaf...", "Tentukan Status Dokumen!", "error");
			return false;
		}
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/save_dok_info_ods', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id : $("#assesmen_id").val(),
						id : $("#dok_id").val(),
						nama_dok_id : $("#nama_dok_id").val(),
						status_dok : $("#status_dok").val(),
						ket_dok : $("#ket_dok").val(),
					},
				success: function(data) {
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Document.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						clear_dok();
						load_dok_info_ods();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Save Document'});
					}
				}
			});
			}
	}
	function edit_dok(id){
		$("#dok_id").val(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/get_dok_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id
				},
			success: function(data) {
				$("#nama_dok_id").val(data.nama_dok_id).trigger('change');
				$("#status_dok").val(data.status_dok).trigger('change');
				$("#ket_dok").val(data.ket_dok);
			}
		});
	}
	function clear_dok(){
		$("#dok_id").val('');
		$("#nama_dok_id").val('').trigger('change');
		$("#status_dok").val('').trigger('change');
		$("#ket_dok").val('');
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		let st_ranap=$("#st_ranap").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_info_ods', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							st_ranap:st_ranap,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
function create_info_ods(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}

function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_info_ods();
}
function create_with_template_info_ods(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_info_ods(){
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let st_ranap=$("#st_ranap").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_template_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					idpasien:idpasien,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_info_ods(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_info_ods(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_template_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").show();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	// $("#form1").reportValidity();
	// if($("#form1").valid()){
		
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			// simpan_assesmen_ttv();
			// simpan_edukasi_info_ods();
			// simpan_riwayat_penyakit_info_ods();
			simpan_assesmen();
		});		
	// }
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}

	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template_info_ods").removeAttr('disabled');
		}else{
			$("#btn_create_with_template_info_ods").attr('disabled','disabled');
		}
	});
	function list_index_template_info_ods(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_index_template_info_ods', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_dok_info_ods(){
		let assesmen_id=$("#assesmen_id").val();
		let status_assemen=$("#status_assemen").val();
		$('#index_dokumen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_dokumen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/load_dok_info_ods', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							status_assemen:status_assemen,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap_ini").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id_ranap);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_template_info_ods', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_info_ods(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		// alert(assesmen_detail_id);return false;
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_edit_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id=data.pendaftaran_id;
					let st_ranap=data.st_ranap;
					$("#cover-spin").show();
					if (st_ranap=='1'){
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_per/input_info_ods'); ?>";
						
					}else{
					window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_per/input_info_ods_rj'); ?>";
						
					}
				}
			}
		});
	}
	$("#jenis_pasien").change(function(){
		let jenis_pasien=$("#jenis_pasien").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/get_informasi_info_ods', 
			dataType: "JSON",
			method: "POST",
			data : {
					jenis_pasien:jenis_pasien,
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#informasi").val(data.informasi);
			}
		});
	});
	
	function hapus_record_info_ods(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_info_ods', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_info_ods', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template_info_ods();
				}
			});
		});

	}
	function  hapus_dok(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Dokument ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_dok_info_ods', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					load_dok_info_ods();
				}
			});
		});

	}
	
</script>