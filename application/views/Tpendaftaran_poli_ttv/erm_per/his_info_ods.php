<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
</style>
<style>
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<?if ($menu_kiri=='his_info_ods'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_info_ods' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_per">
	<?if ($st_lihat_info_ods=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row" style="margin-bottom:20px">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >							
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
								<h5 class="font-w400 push-5 text-center"><strong><i>{judul_header_eng}</i></strong></h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 ">
							<div class="pull-right push-10-r">
								<button class="btn btn-default menu_click btn_back" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
							</div>
							
						</div>
					</div>
			
						
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="row">
						<?if ($assesmen_id!=''){?>
						<?
							if ($tanggal_opr){
								$tanggal_opr=HumanDateShort($tanggal_opr);
								$tanggal_opr_asal=HumanDateShort($tanggal_opr_asal);
							}else{
								$tanggal_opr=date('d-m-Y');
								
							}
							if ($mulai){$mulai=HumanTime($mulai);}else{$mulai=date('H:i:s');}
							
							
							$list_ppa_dokter=get_all('mppa',array('tipepegawai'=>'2','staktif'=>1));
							$list_ppa_all=get_all('mppa',array('staktif'=>1));
						?>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_1_ina?></strong><br><i class="text-muted"><?=$paragraf_1_eng?></i></label>
									
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-6">
									<label for="dpjp_ppa"><strong><?=$dpjp_ina?></strong><br><i class="text-muted"><?=$dpjp_eng?></i></label>
									<select id="dpjp_ppa" class="<?=($dpjp_ppa!=$dpjp_ppa_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($dpjp_ppa == '' ? 'selected' : '')?>>Pilih Opsi</option>
										<?foreach($list_ppa_dokter as $row){?>
										<option value="<?=$row->id?>" <?=($dpjp_ppa == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-2">
									<label for="tanggal_opr"><strong><?=$tanggal_tindakan_ina?></strong><br><i class="text-muted"><?=$tanggal_tindakan_eng?></i></label>
									<div class="input-group date">
										<input id="tanggal_opr" class="js-datepicker form-control <?=($tanggal_opr!=$tanggal_opr_asal?'edited':'')?>" type="text" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggal_opr ?>" >
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
								<div class="col-md-2">
									<label for="mulai"><strong><?=$waktu_tindakan_ina?></strong><br><i class="text-muted"><?=$waktu_tindakan_eng?></i></label>
									<div class="input-group">
										<input id="mulai" class="time-datepicker form-control <?=($mulai!=$mulai_asal?'edited':'')?>" type="text" name="mulai" value="<?= $mulai ?>" >
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="rencana_bedah"><strong><?=$rencana_ina?></strong><br><i class="text-muted"><?=$rencana_eng?></i></label>
									<input id="rencana_bedah" class="form-control <?=($rencana_bedah!=$rencana_bedah_asal?'edited':'')?> " type="text"  value="{rencana_bedah}" name="rencana_bedah" placeholder="" >
								</div>
								
							</div>
							
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-4">
									<label for="jenis_bius"><strong><?=$jenis_bius_ina?></strong><br><i class="text-muted"><?=$jenis_bius_eng?></i></label>
									<select id="jenis_bius" name="jenis_bius" class="<?=($jenis_bius!=$jenis_bius_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_bius == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(348) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_bius == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_bius == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-2">
									<label for="pengencer_darah"><strong><?=$minum_ina?></strong><br><i class="text-muted"><?=$minum_eng?></i></label>
									<select id="pengencer_darah" name="pengencer_darah" class="<?=($pengencer_darah!=$pengencer_darah_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_bius == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(431) as $row){?>
										<option value="<?=$row->id?>" <?=($pengencer_darah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pengencer_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="detail_pengencer"><strong><?=$jika_ya_ina?></strong><br><i class="text-muted"><?=$jika_ya_eng?></i></label>
									<input id="detail_pengencer" class="form-control <?=($detail_pengencer!=$detail_pengencer_asal?'edited':'')?> " type="text"  value="{detail_pengencer}" name="detail_pengencer" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$info_ina?></strong><br><i class="text-muted"><?=$info_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_2_ina?></strong><br><i class="text-muted"><?=$paragraf_2_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_3_ina?></strong><br><i class="text-muted"><?=$paragraf_3_eng?></i></label>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_4_ina?></strong><br><i class="text-muted"><?=$paragraf_4_eng?></i></label>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-3">
									<label for="jenis_pasien"><strong><?=$label_pasien_ina?></strong><br><i class="text-muted"><?=$label_pasien_eng?></i></label>
									<select id="jenis_pasien" name="jenis_pasien" class="<?=($jenis_pasien!=$jenis_pasien_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_pasien == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(428) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_pasien == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_pasien == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-5">
									<label for="informasi"><strong><?=$label_info_ina?></strong><br><i class="text-muted"><?=$label_info_eng?></i></label>
									<input id="informasi" readonly class="form-control <?=($informasi!=$informasi_asal?'edited':'')?> " type="text"  value="{informasi}" name="informasi" placeholder="" >
								</div>
								<div class="col-md-4">
									<label for="puasa_mulai"><strong><?=$label_puasa_ina?></strong><br><i class="text-muted"><?=$label_puasa_eng?></i></label>
									<input id="puasa_mulai" class="form-control <?=($puasa_mulai!=$puasa_mulai_asal?'edited':'')?> " type="text"  value="{puasa_mulai}" name="puasa_mulai" placeholder="" >
								</div>
							</div>
							<div class="form-group" style="margin-top: -10px;">
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_5_ina?></strong><br><i class="text-muted"><?=$paragraf_5_eng?></i></label>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: 10px;">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table table-bordered table-striped" id="index_dokumen">
											<thead>
												<tr>
													<th width="5%" class="text-center">No</th>
													<th width="30%" class="text-center"><strong><?=$paragraf_dok_ina?></strong><br><i class="text-muted"><?=$paragraf_dok_eng?></i></th>
													<th width="15%" class="text-center"><strong><?=$label_status_ina?></strong><br><i class="text-muted"><?=$label_status_eng?></i></th>
													<th width="35%" class="text-center"><strong><?=$label_ket_ina?></strong><br><i class="text-muted"><?=$label_ket_eng?></i></th>
													<?if ($status_assemen=='1' || $status_assemen=='3'){?>
													<th width="15%" class="text-center"><strong>ACTION</strong><br><i class="text-muted">Aksi</i></th>
													<?}?>
												</tr>
												
												<input type="hidden" id="dok_id" value="" >	
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_6_ina?></strong><br><i class="text-muted"><?=$paragraf_6_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$paragraf_7_ina?></strong><br><i class="text-muted"><?=$paragraf_7_eng?></i></label>
								</div>
								<div class="col-md-12">
									<label for="dpjp_ppa"><strong><?=$label_catatan_ina?></strong><br><i class="text-muted"><?=$label_catatan_eng?></i></label>
								</div>
								
							</div>
							<div class="form-group" style="margin-top: -10px;">
								
								<div class="col-md-12">
									<label class="push-20-l"><?=$content_catatan_ina?></label>
								</div>
								
							</div>
						<?}?>
						</div>
					</div>
					
					
						
					<?if ($assesmen_id){?>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group" style="margin-top:-5px;!important">
							<div class="col-md-12 ">
								<table style="width:100%">
									<tr>
										<td style="width:33%" class="text-bold text-center"><strong><?=$label_keluarga_ina?></strong><br><i class="text-muted"><?=$label_keluarga_eng?></i></td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center"><strong><?=$label_petugas_ina?></strong><br><i class="text-muted"><?=$label_petugas_eng?></i></td>
									</tr>
									<tr>
										
										<td style="width:33%" class="text-bold text-center">
											<?if ($ttd_pasien){?>
												<div class="img-container fx-img-rotate-r">
													<img style="width:120px;height:120px; text-align: center;" src="<?=$ttd_pasien?>" alt="">
													<?if ($status_assemen!='2'){?>
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="modal_ttd('ttd_pasien')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd('ttd_pasien')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
													<?}?>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success" onclick="modal_ttd('ttd_pasien')" id="btn_ttd_lm" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
													
											
										</td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id)?>" alt="" title="">
										</td>
									</tr>
									<?
										$arr_saksi=get_nama_ppa_array($petugas_id);
									?>
									<tr>
										<td style="width:33%" class="text-bold text-center text-bold">
											<strong><?=($nama_pasien)?></strong>
											
										</td>
										<td style="width:34%">&nbsp;</td>
										<td style="width:33%" class="text-bold text-center text-bold"><i class="text-bold">
											<?if ($status_assemen=='1'){?>
											<select id="petugas_id" name="petugas_id" class="js-select2 form-control opsi_petugas_id" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Pilih Saksi RS -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>" <?=($petugas_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
											</select>
											<?}else{?>
												<strong><?=$arr_saksi['nama']?></strong><br><i>(<?=$arr_saksi['nik']?>)</i>
											<?}?>
												
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_dok_info_ods();
		
		load_awal_assesmen=false;
	}
	list_index_history_edit();
	// load_data_rencana_asuhan_pulang(1);
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_info_ods', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}

function load_dok_info_ods(){
	let assesmen_id=$("#assesmen_id").val();
	$("#index_dokumen tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_dok_info_ods', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				versi_edit:$("#versi_edit").val(),
				},
		success: function(data) {
			$("#index_dokumen tbody").append(data.tabel);
			// console.LOG(data);
			// $(".nilai").select2();
			// get_skor_pengkajian_pulang();
			$("#cover-spin").hide();
		}
	});
}

</script>