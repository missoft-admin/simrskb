<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	table.dataTable tbody td {
	  vertical-align: top;
	}
</style>
<style>
	
	#sig_ttd_petugas canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_sasaran canvas{
		width: 100% !important;	
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_invasif' || $menu_kiri=='input_invasif_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_invasif' || $menu_kiri=='input_invasif_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
	
		// echo $tanggal_pernyataan;
					if ($assesmen_id){
						
						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
					}
					
					$disabel_input='';
					if ($st_input_invasif=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_per">
	<?if ($st_lihat_invasif=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id?'active':'')?>">
					<a href="#tab_1" onclick="set_tab(1)"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<input class="form-control input-sm"  type="hidden" readonly id="invasif_detail_id" value="" />
			<textarea id="signature64_ttd_sasaran" name="signed_ttd_sasaran" style="display: none"></textarea>
			<textarea id="signature64_ttd_sasaran_2" name="signature64_ttd_sasaran_2" style="display: none"></textarea>
			<textarea id="signature64_ttd_petugas" name="signed_ttd_petugas" style="display: none"></textarea>
			<textarea id="signature64_ttd_petugas_2" style="display: none"></textarea>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="tipe_rj_ri" value="<?=($asal_rujukan_poli_ranap<3?$asal_rujukan_poli_ranap:'3')?>" >		
						<input type="hidden" id="st_sedang_edit" value="" >		
						<div class="col-md-12">
							<?if($st_lihat_invasif=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_invasif=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<a href="{base_url}tpendaftaran_ranap_erm/cetak_invasif/<?=$assesmen_id?>" target="_blank" class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_per/input_invasif" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Pengkajian</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-12 ">
								<?if($status_assemen!='2'){?>
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered" id="index_header" >
												<thead>
													<tr>
														<th width="15%" class="text-center"><strong><?=$tanggal_jam_ina?></strong><br><i><?=$tanggal_jam_eng?></i></th>
														<th width="30%" class="text-center">
															<strong><?=$nama_tindakan_ina?></strong><br><i><?=$nama_tindakan_eng?></i>
															<div class="input-group">
																<select tabindex="8" id="minvasif_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Tindakan">
																	<option value="" selected>Terapkan Dari Master</option>
																	<?foreach(get_all('minvasif',array('staktif'=>1)) as $row){?>
																		<option value="<?=$row->id?>" ><?=$row->nama?></option>
																	<?}?>
																</select>
																<span class="input-group-btn">
																	<button class="btn btn-default" onclick="terapkan_invasif()" type="button">Terapkan</button>
																</span>
															</div>
															
														</th>
														<th width="30%" class="text-center"><strong><?=$ket_ina?></strong><br><i><?=$ket_eng?></i></th>
														<th width="15%" class="text-center"><strong><?=$ttd_petugas_ina?></strong><br><i><?=$ttd_petugas_eng?></i></th>
														<th width="10%" class="text-center">Action</th>
													</tr>
													<tr>
														<td class="text-center" style="vertical-align: top;">
																
															<div class="input-group date">
																<input class="js-datetimepicker form-control input-sm" type="text" id="input_tanggal" name="input_tanggal" value="<?=date('d-m-Y H:i:s')?>" />
																<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
															</div>
														</td>
														<td class="text-left" style="vertical-align: top;">
															<textarea class="js-summernote form-control" id="materi_invasif"  rows="3" placeholder="Isi keterangan"></textarea>
														</td>
														<td class="text-left" style="vertical-align: top;">
															<textarea class="js-summernote form-control" id="ket_invasif"  rows="3" placeholder="Detail"></textarea>
														</td>
														<td class="text-center" style="vertical-align: top;">
															
															<select id="petugas_id" name="petugas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
																<option value="" <?=($st_setting_dokter=='0'?'selected':'')?>>Pilih</option>
																<?foreach($list_dokter_pelaksana as $r){?>
																<option value="<?=$r->id?>" <?=($st_setting_dokter=='1'?'selected':'')?> <?=($login_ppa_id==$r->id?'selected':'')?>><?=$r->nama?></option>
																<?}?>
																
																
															</select>
														</td>
														
														<td class="text-center" style="vertical-align: top;">
															<span class="input-group-btn">
																<button class="btn btn-info btn-sm" onclick="simpan_materi_invasif()" type="button"><i class="fa fa-save"></i></button>
																<button class="btn btn-warning  btn-sm" onclick="clear_invasif()" type="button"><i class="fa fa-refresh"></i></button>
															</span>
														</td>
													</tr>
													
												</thead>
												<tbody></tbody>
											</table>
										</div>
								</div>
								<?}?>
								<div class="col-md-12 ">
										<h4 class="text-center text-primary"><?=$daftar_persetujuan_ina?><br><i><?=$daftar_persetujuan_eng?></i></h4>
								</div>
								<div class="col-md-12 ">
									<div class="col-md-4">
										<label class="control-label" for="tanggal_input_1">Tanggal</label>
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control his_filter" type="text" id="tanggal_filter_1" name="tanggal_filter_1" placeholder="From" value=""/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control his_filter" type="text" id="tanggal_filter_2" placeholder="To" value=""/>
										</div>
									</div>
									<div class="col-md-3">
										<label class="control-label" for="tanggal_input_1">Tindakan</label>
										<select id="filter_tindakan" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
											<?foreach(get_all('minvasif',array('staktif'=>1)) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									<div class="col-md-2">
										<label class="control-label" for="tanggal_input_1">Pesetujuan</label>
										<select id="filter_persetujuan"  class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#" selected>ALL</option>
											<?foreach(get_all('mjenis_persetujuan_invasif',array('staktif'=>1)) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									<div class="col-md-2">
										<label class="control-label" for="tanggal_input_1">Tandatangan</label>
										<select id="filter_ttd " name="st_persetujuan" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#" selected>ALL</option>
											<option value="1">SUDAH</option>
											<option value="0">BELUM</option>
											
										</select>
									</div>
									<div class="col-md-1">
										<label class="control-label" style="font-color#fff">Filter</label>
										<button class="btn btn-sm btn-success his_filter" onclick="load_index_materi()" type="button"><i class="fa fa-search"></i> Search</button>
									</div>
									
								</div>
								<div class="col-md-12 " style="margin-top:15px">
									<div class="table-responsive">
										<table class="table table-bordered" id="tabel_materi">
											<thead>
												<tr>
													<th width="15%" class="text-center"><strong><?=$tanggal_jam_ina?></strong><br><i><?=$tanggal_jam_eng?></i></th>
													<th width="25%" class="text-center">
														<strong><?=$nama_tindakan_ina?></strong><br><i><?=$nama_tindakan_eng?></i>
													</th>
													<th width="15%" class="text-center"><strong><?=$ket_ina?></strong><br><i><?=$ket_eng?></i></th>
													<th width="12%" class="text-center"><strong><?=$ttd_petugas_ina?></strong><br><i><?=$ttd_petugas_eng?></i></th>
													<th width="10%" class="text-center"><strong><?=$persetujuan_ina?></strong><br><i><?=$persetujuan_eng?></i></th>
													<th width="12%" class="text-center"><strong><?=$ttd_pasien_ina?></strong><br><i><?=$ttd_pasien_eng?></i></th>
													<th width="10%" class="text-center">Action</th>
												</tr>
											
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
							
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-right push-20-r">
										<?if ($assesmen_id==''){?>
										<?if ($st_input_invasif=='1'){?>
										<button class="btn btn-primary" id="btn_create_info_ods" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
										<?}?>
										<
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<?if ($st_ranap=='1'){?>
													<option value="<?=$pendaftaran_id_ranap?>" >Dirawat Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>
												<?}else{?>
													<option value="<?=$pendaftaran_id?>" >Poliklinik Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>

												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table table-bordered table-striped" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_invasif!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="20%">Action</th>
												<th width="70%">Template</th>
												<th width="10%">Jumlah</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_ttd_petugas" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title"><?=$ttd_petugas_ina.' / <i class="text-muted">'.$ttd_petugas_eng.'</i>'?></h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_petugas" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{ttd_petugas_ina} / <i class="text-muted">{ttd_petugas_eng}</i></label>
										<input tabindex="32" type="text" class="form-control " id="nama_petugas" value="">
									</div>
								</div>
								
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_petugas()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_petugas()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_sasaran" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PERNYATAAN</h3>
				</div>
				<div class="block-content">
					<div class="row">
					
						<div class="form-horizontal">
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {paragraf_1_ina} / <i class="text-muted">{paragraf_1_eng}</i> </label>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-3 ">
										<label for="hubungan_id">{yang_ttd_ina} / <i class="text-muted">{yang_ttd_eng}</i></label>
											<select id="jenis_ttd" class="js-select2 form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="#" <?=($jenis_ttd=='#'?'selected':'')?>>- Pilih yang Bertandatangan -</option>
												<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Penanggung Jawab</option>
												<option value="2" <?=($jenis_ttd=='2'?'selected':'')?>>Pengantar</option>
												<option value="3" <?=($jenis_ttd=='3'?'selected':'')?>>Pasien Sendiri</option>
												<option value="4" <?=($jenis_ttd=='4'?'selected':'')?>>Lainnya</option>
											
										</select>
									</div>
									<div class="col-md-3 ">
										<label for="nama_pernyataan">{nama_ina} / <i class="text-muted">{nama_eng}</i></label>
										<input  type="text" class="form-control "   id="nama_pernyataan" placeholder="{nama_pernyataan}" value="{nama_pernyataan}" required>
									</div>
									<div class="col-md-3 ">
										<label for="nama_pernyataan">{memberikan_ina} / <i class="text-muted">{memberikan_eng}</i></label>
										<select  id="hereby" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($hereby == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(get_all('mjenis_persetujuan_invasif',array('staktif'=>'1')) as $row){?>
											<option value="<?=$row->id?>" <?=($hereby == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-3 ">
										<label for="untuk_tindakan">{untuk_ina} / <i class="text-muted">{untuk_eng}</i></label>
										<input  type="text" class="form-control "   id="untuk_tindakan" placeholder="{untuk_tindakan}" value="{untuk_tindakan}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-3 ">
										<label for="hubungan_id">{terhadap_ina} / <i class="text-muted">{terhadap_eng}</i></label>
											<select  id="terhadap" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" <?=($terhadap == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(79) as $row){?>
												<option value="<?=$row->id?>" <?=($terhadap == $row->id ? 'selected="selected"' : '')?> <?=($terhadap == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
												<?}?>
											</select>
									</div>
									<div class="col-md-3 ">
										<label for="st_setting_pemberi_info"> {nama_pasien_ina} / <i class="text-muted">{nama_pasien_eng}</i> </label>
										<input  type="text" disabled class="form-control "   id="nama_pasien" placeholder="{nama_pasien}" name="nama_pasien" value="{nama_pasien}" required>
									</div>
									<div class="col-md-3 ">
										<label for="st_setting_pemberi_info"> {tanggal_lahir_ina} / <i class="text-muted">{tanggal_lahir_eng}</i> </label>
										<input  type="text" disabled class="form-control "   id="tanggal_lahir_pasien" placeholder="{tanggal_lahir_pasien}" name="tanggal_lahir_pasien" value="<?=HumanDateShort($tanggal_lahir_pasien)?>" required>
									</div>
									<div class="col-md-3 ">
										<label for="st_setting_pemberi_info"> {umur_ina} / <i class="text-muted">{umur_eng}</i> </label>
										<input  type="text" disabled class="form-control "   id="umur_pasien" placeholder="{umur_pasien}" name="umur_pasien" value="{umur_pasien}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-3 ">
										<label for="st_setting_pemberi_info"> {jenis_kelamin_ina} / <i class="text-muted">{jenis_kelamin_eng}</i> </label>
										<input  type="text" disabled class="form-control "   id="jenis_kelamin_pasien" placeholder="{jenis_kelamin_pasien}" name="jenis_kelamin_pasien" value="<?=($jenis_kelamin_pasien)?>" required>
									</div>
									<div class="col-md-3 ">
										<label for="st_setting_pemberi_info"> {norek_ina} / <i class="text-muted">{norek_eng}</i> </label>
										<input  type="text" disabled class="form-control "   id="no_medrec" placeholder="{no_medrec}" name="no_medrec" value="{no_medrec}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label id="ket_pernyataan_ina"> </label>
									</div>
									
								</div>
								
							</div>
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{sign_ina} / <i class="text-muted">{sign_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_sasaran" ></div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_sasaran()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_sasaran()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var tab_active=1;
var sig_ttd_petugas = $('#sig_ttd_petugas').signature({syncField: '#signature64_ttd_petugas', syncFormat: 'PNG'});
var sig_ttd_sasaran = $('#sig_ttd_sasaran').signature({syncField: '#signature64_ttd_sasaran', syncFormat: 'PNG'});
	function terapkan_invasif(){
		let minvasif_id=$("#minvasif_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/terapkan_invasif', 
			dataType: "JSON",
			method: "POST",
			data : {
					minvasif_id:minvasif_id,
				
				   },
			success: function(data) {
				$("#materi_invasif").summernote("code", data);
				$("#cover-spin").hide();
				
			}
		});
	}
	function set_tab(tab){
		tab_active=tab;
	}
	function create_awal_template(){
		$("#modal_default_template").modal('show');
		$("#mnyeri_id_2").select2({
			dropdownParent: $("#modal_default_template")
		  });
	}
	
	$(document).ready(function() {
		$("#input_tanggal").datetimepicker({
			format: "DD-MM-YYYY HH:mm",
			// stepping: 30
		});
		$('.js-summernote').summernote({
			  height: 100,   //set editable area's height
			  
			  codemirror: { // codemirror options
				theme: 'monokai'
			  }	
			})
		
		disabel_edit();
		// set_ttd_assesmen();
		$(".time-datepicker").datetimepicker({
			format: "HH:mm:ss"
		});
		// $("#tabel_rencana_asuhan tbody").empty();
		let assesmen_id=$("#assesmen_id").val();
		
			if (assesmen_id){
				load_index_materi();
			}else{
				list_history_pengkajian();
			}
		load_awal_assesmen=false;
		

	});
	$("#hereby").change(function(){
		set_persetujuan();
	});
	function set_persetujuan(){
		
		let assesmen_id=$("#assesmen_id").val();
		let hereby=$("#hereby").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/get_persetujaun_invasif', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					hereby:hereby,
				
				   },
			success: function(data) {
				$("#ket_pernyataan_ina").html(data);
				// // $("#cover-spin").hide();
				
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				// location.reload();
			}
		});
	}
	
	function disabel_edit(){
		if (status_assemen=='2'){
			 $("#form1 :input").prop("disabled", true);
			 $(".his_filter").removeAttr('disabled');
			 // $(".btn_ttd").removeAttr('disabled');
		}
	}
	function simpan_template(){
		$("#modal_savae_template_assesmen").modal('show');
		
	}
	function create_assesmen(){
		
		$("#modal_default").modal('hide');
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let tipe_rj_ri=$("#tipe_rj_ri").val();
		// let template_id=$("#template_id").val();
		let idpasien=$("#idpasien").val();
		
		let template='Baru';
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/create_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						idpasien:idpasien,
						tipe_rj_ri:tipe_rj_ri,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	function gunakan_template_assesmen(id){
		$("#template_assesmen_id").val(id).trigger('change');
		create_with_template();
	}
	function create_with_template(){
		let template_assesmen_id=$("#template_assesmen_id").val();
		// let template_assesmen_id=id;
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		let tipe_rj_ri=$("#tipe_rj_ri").val();
		// alert(pendaftaran_id_ranap);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen Dari template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						template_assesmen_id:template_assesmen_id,
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						tipe_rj_ri:tipe_rj_ri,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	function copy_history_assesmen(id){
		let template_assesmen_id=id;
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tipe_rj_ri=$("#tipe_rj_ri").val();
		let idpasien=$("#idpasien").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen Dari Duplikasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						template_assesmen_id:template_assesmen_id,
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						tipe_rj_ri:tipe_rj_ri,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	// function create_template(){
		// let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		// let pendaftaran_id=$("#pendaftaran_id").val();
		// let tglpendaftaran=$("#tglpendaftaran").val();
		// let waktupendaftaran=$("#waktupendaftaran").val();
		// let idpasien=$("#idpasien").val();
		// // alert(idpasien);
		// // return false;
		// let template='Baru';
		// swal({
			// title: "Apakah Anda Yakin ?",
			// text : "Membuat Template "+template+" ?",
			// type : "success",
			// showCancelButton: true,
			// confirmButtonText: "Ya",
			// confirmButtonColor: "#34a263",
			// cancelButtonText: "Batalkan",
		// }).then(function() {
			// $("#cover-spin").show();
			// $.ajax({
				// url: '{site_url}Tpendaftaran_ranap_erm/create_template_invasif', 
				// dataType: "JSON",
				// method: "POST",
				// data : {
						// pendaftaran_id:pendaftaran_id,
						// pendaftaran_id_ranap:pendaftaran_id_ranap,
						// tglpendaftaran:tglpendaftaran,
						// waktupendaftaran:waktupendaftaran,
						// idpasien:idpasien,
					   // },
				// success: function(data) {
					
					// $("#cover-spin").hide();
					
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					// location.reload();
				// }
			// });
		// });

	// }
	function create_template(){
		
		$("#modal_default").modal('hide');
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let tipe_rj_ri=$("#tipe_rj_ri").val();
		// let template_id=$("#template_id").val();
		let idpasien=$("#idpasien").val();
		
		let template='Template Baru';
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat  "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/create_template_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						idpasien:idpasien,
						tipe_rj_ri:tipe_rj_ri,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	$(".opsi_change").change(function(){
		// console.log('OPSI CHANTE')
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});

	

	$(".auto_blur_tanggal").change(function(){
		console.log($("#waktupernyataan").val())
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	});
	function batal_assesmen(){
		let assesmen_id=$("#assesmen_id").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		
		let template=$("#template_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Assesment ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/batal_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						st_edited:st_edited,
						jml_edit:jml_edit,
					
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
					location.reload();
				}
			});
		});

	}
	function batal_template(){
		let assesmen_id=$("#assesmen_id").val();
		let nama_template=$("#nama_template").val();
		
		let template=$("#template_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Assesment ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/batal_template_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						nama_template:nama_template,
					
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
					location.reload();
				}
			});
		});

	}
	

	$(".auto_blur").blur(function(){
		// alert('sini');
		if ($("#st_edited").val()=='0'){
			$(this).removeClass('input_edited');
			simpan_assesmen();
		}else{
			if ($("#st_edited").val()=='1'){
				if ($(this).val()!=before_edit){
					console.log('Ada Peruabahan');
					// $(this).attr('input_edited');
					 $(this).addClass('input_edited');
				}
			}
		}
		
	});
	$("#penerima_info").blur(function(){
		$("#penerima_info_paraf").val($("#penerima_info").val());
		$("#nama_kel_penerima_info").val($("#penerima_info").val());
		
	});
	$("#nama").blur(function(){
		$("#nama_pemberi_pernyataan").html($("#nama").val());
		
	});

	$(".auto_blur").focus(function(){
		before_edit=$(this).val();
		console.log(before_edit);
	});
	function close_assesmen(){
		swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menyimpan Inputan Assesmen?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$("#cover-spin").show();
				$("#status_assemen").val(2);
				
				simpan_assesmen();
			});		
		
	}
	function close_template(){
		if ($("#nama_template").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Template", "error");
			return false;
		}
		$("#modal_savae_template_assesmen").modal('hide');
		swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menyimpan Template ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$("#status_assemen").val(4);
				nama_template=$("#nama_template").val();
				simpan_assesmen();
			});		
		
	}
	function simpan_assesmen(){
		if (load_awal_assesmen==false){
			if (status_assemen !='2'){
			let assesmen_id=$("#assesmen_id").val();
			// alert(assesmen_id);return false;
			let tglpendaftaran=$("#tglpendaftaran").val();
			let waktupendaftaran=$("#waktupendaftaran").val();
			let tanggalinformasi=$("#tanggalinfo").val();
			let waktuinformasi=$("#waktuinfo").val();
			let tanggalpernyataan=$("#tanggalpernyataan").val();
			let waktupernyataan=$("#waktupernyataan").val();
			
			let st_edited=$("#st_edited").val();
			let jml_edit=$("#jml_edit").val();
			if (assesmen_id){
				// console.log('SIMPAN');
				
				$.ajax({
					url: '{site_url}Tpendaftaran_ranap_erm/save_invasif', 
					dataType: "JSON",
					method: "POST",
					data : {
							tglpendaftaran:tglpendaftaran,
							waktupendaftaran:waktupendaftaran,
							assesmen_id:$("#assesmen_id").val(),
							nama_template:nama_template,
							status_assemen:$("#status_assemen").val(),
							
							st_edited:$("#st_edited").val(),
							jml_edit:$("#jml_edit").val(),
						},
					success: function(data) {
						
								console.log(data);
						if (data==null){
							swal({
								title: "Gagal!",
								text: "Simpan Assesmen.",
								type: "error",
								timer: 1500,
								showConfirmButton: false
							});

						}else{
							if (data.status_assemen=='1'){
								$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
							}else{
								if (data.status_assemen=='3'){
									
								$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
								}else{
									$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
									$("#cover-spin").show();
									// alert('sini');
									location.reload();			
								}
								
							}
						}
					}
				});
			}
			}
		}
	}
	
	
	
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		tab_active=3;
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_ranap_erm/list_index_template_invasif', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 // $("#index_template_assemen thead").remove();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_template_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		let st_ranap=$("#st_ranap").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_invasif', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							st_ranap:st_ranap,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let materi_invasif=$("#materi_invasif").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		// $("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_edit_invasif', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					materi_invasif:materi_invasif,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
						location.reload();
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		$("#modal_hapus").modal('hide');
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	function hapus_materi_invasif(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Materi Edukasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/hapus_materi_invasif', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					load_index_materi();
				}
			});
		});

	}
	function clear_invasif(){
		$("#invasif_detail_id").val('');
		$("#materi_invasif").summernote("code", '');
		$("#ket_invasif").summernote("code", '');
		$("#signature64_ttd_sasaran").val('')
		$("#signature64_ttd_sasaran_2").val('')
		$("#signature64_ttd_petugas").val('')
		$("#signature64_ttd_petugas_2").val('')
	}
	function simpan_materi_invasif(){
		if (load_awal_assesmen==false){
			if (status_assemen !='2'){
			let assesmen_id=$("#assesmen_id").val();
			// alert(assesmen_id);return false;
			let input_tanggal=$("#input_tanggal").val();
			let materi_invasif=$("#materi_invasif").val();
			let ket_invasif=$("#ket_invasif").val();
			let invasif_detail_id=$("#invasif_detail_id").val();
			let petugas_id=$("#petugas_id").val();
			let minvasif_id=$("#minvasif_id").val();
			if (materi_invasif==''){
				sweetAlert("Maaf...", "Isi Tindakan", "error");
				return false;
			}
			if (ket_invasif==''){
				sweetAlert("Maaf...", "Isi Keterangan", "error");
				return false;
			}
			if (petugas_id==''){
				sweetAlert("Maaf...", "Tentukan Petugas", "error");
				return false;
			}
			

			if (assesmen_id){
				// console.log('SIMPAN');
				
				$.ajax({
					url: '{site_url}Tpendaftaran_ranap_erm/simpan_materi_invasif', 
					dataType: "JSON",
					method: "POST",
					data : {
							assesmen_id:$("#assesmen_id").val(),
							input_tanggal:input_tanggal,
							materi_invasif:materi_invasif,
							ket_invasif:ket_invasif,
							invasif_detail_id:invasif_detail_id,
							petugas_id:petugas_id,
							minvasif_id:minvasif_id,
							
						},
					success: function(data) {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Save Edukasi'});
						load_index_materi();
						clear_invasif();
					}
				});
			}
			}
		}
	}
	
	function load_index_materi(){
		$("#cover-spin").show();
		var assesmen_id=$("#assesmen_id").val();
		var status_assemen=$("#status_assemen").val();
		var tipe_rj_ri=$("#tipe_rj_ri").val();
		var tanggal_filter_1=$("#tanggal_filter_1").val();
		var tanggal_filter_2=$("#tanggal_filter_2").val();
		var filter_tindakan=$("#filter_tindakan").val();
		var filter_persetujuan=$("#filter_persetujuan").val();
		var filter_ttd=$("#filter_ttd").val();
		
		// alert(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_index_materi_invasif/',
			dataType: "json",
			type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					status_assemen:status_assemen,
					tipe_rj_ri:tipe_rj_ri,
					tanggal_filter_1:tanggal_filter_1,
					tanggal_filter_2:tanggal_filter_2,
					filter_tindakan:filter_tindakan,
					filter_persetujuan:filter_persetujuan,
					filter_ttd:filter_ttd,
					
					
			  },
			success: function(data) {
				$("#tabel_materi tbody").empty();
				$("#tabel_materi tbody").append(data.tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// $('.auto_blur_tabel').summernote({
					// height: 100,
					// codemirror: { // codemirror options
						// theme: 'monokai'
					  // },	
				  // callbacks: {
					// onBlur: function(contents, $editable) {
							// assesmen_id=$("#assesmen_id").val();
							// var tr=$(this).closest('tr');
							// var informasi_id=tr.find(".informasi_id").val();
							// var isi=$(this).val();
							// $.ajax({
								// url: '{site_url}Tpendaftaran_ranap_erm/update_isi_informasi_invasif/',
								// dataType: "json",
								// type: 'POST',
								// data: {
								// informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
								// },success: function(data) {

								// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
								// }
							// });
					// }
				  // }
				// });
					
			}
		});
	}
	function edit_materi_invasif(id){
		$("#cover-spin").show();
		$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/edit_materi_invasif/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				id:id,
		  },success: function(data) {
			  $("#cover-spin").hide();
			  // simpan_materi_invasif()
				// alert(data.input_tanggal);
				$("#input_tanggal").val(data.input_tanggal);
				$("#petugas_id").val(data.petugas_id).trigger('change');
				$("#minvasif_id").val(data.minvasif_id).trigger('change');
				$("#invasif_detail_id").val(data.id);
				$("#nama_petugas").val(data.ttd_petugas_nama);
				$("#materi_invasif").summernote("code", data.materi_invasif);
				$("#ket_invasif").summernote("code", data.ket_invasif);
				$("#signature64_ttd_petugas").val(data.ttd_petugas);
				// $("#modal_ttd_petugas").modal('show');
				$('#sig_ttd_petugas').signature('enable').signature('draw', data.ttd_petugas);
		  }
		});
	}
	function modal_ttd_petugas(id){
		$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/get_ttd_petugas_invasif/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				id:id,
		  },success: function(data) {
				$("#invasif_detail_id").val(data.id);
				$("#nama_petugas").val(data.ttd_petugas_nama);
				$("#signature64_ttd_petugas").val(data.ttd_petugas);
				$("#signature64_ttd_petugas_2").val(data.ttd_petugas);
				$("#modal_ttd_petugas").modal('show');
				if (data.ttd_petugas){
					$('#sig_ttd_petugas').signature('enable').signature('draw', data.ttd_petugas);
					$("#signature64_ttd_petugas").val($("#signature64_ttd_petugas_2").val());
				}else{
					clear_ttd_petugas();
				}
		  }
		});
		
	}
	function clear_ttd_petugas(){
		sig_ttd_petugas.signature('clear');
		$("#signature64_ttd_petugas").val('');
	}
	function simpan_ttd_petugas(){
		let id=$("#invasif_detail_id").val();
		let ttd_petugas=$("#signature64_ttd_petugas").val();
		let ttd_petugas_nama=$("#nama_petugas").val();
				$("#modal_ttd_petugas").modal('hide');
		if (ttd_petugas==''){
				swal({
					title: "Gagal!",
					text: "Tentukan Tanda Tangan Petugas.",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				$("#modal_ttd_petugas").modal('show');
				return false;
		}
		
		if (ttd_petugas_nama==''){
				swal({
					title: "Gagal!",
					text: "Tentukan Nama Tanda Tangan Petugas.",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				return false;
		}
		// ttd_view_petugas
		$("#ttd_view_petugas").attr("src", ttd_petugas);
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_petugas_invasif/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_petugas:ttd_petugas,
					ttd_petugas_nama:ttd_petugas_nama,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Tanda Tangan'});
				  $("#cover-spin").hide();
					if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					clear_invasif();
					$("#modal_ttd_petugas").modal('hide');
				}
			});
	}
	function hapus_ttd_petugas(id){
		var ttd_petugas='';
		let ttd_petugas_nama=$("#nama_petugas").val();
		
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_petugas_invasif/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_petugas:ttd_petugas,
					ttd_petugas_nama:ttd_petugas_nama,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
				  $("#cover-spin").hide();
					if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					$("#modal_ttd_petugas").modal('hide');
				}
			});
	}
	
	function modal_ttd_sasaran(id){
		$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/get_ttd_sasaran_invasif/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				id:id,
		  },success: function(data) {
				$("#invasif_detail_id").val(data.id);
				$("#hereby").val(data.hereby).trigger('change');
				$("#jenis_ttd").val(data.jenis_ttd).trigger('change.select2');
				$("#terhadap").val(data.terhadap).trigger('change');
				$("#nama_pernyataan").val(data.nama_pernyataan);
				$("#untuk_tindakan").val(data.untuk_tindakan);
				$("#signature64_ttd_sasaran_2").val(data.ttd_sasaran);
				$("#signature64_ttd_sasaran").val(data.ttd_sasaran);
				console.log(data.ttd_sasaran);
				$("#modal_ttd_sasaran").modal('show');
				if (data.ttd_sasaran){
				$('#sig_ttd_sasaran').signature('enable').signature('draw', $("#signature64_ttd_sasaran_2").val());
					$("#signature64_ttd_sasaran").val($("#signature64_ttd_sasaran_2").val());
				}else{
					clear_ttd_sasaran();
				}
		  }
		});
		
	}
	function clear_ttd_sasaran(){
		sig_ttd_sasaran.signature('clear');
		$("#signature64_ttd_sasaran").val('');
	}
	function simpan_ttd_sasaran(){
		let id=$("#invasif_detail_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let jenis_ttd=$("#jenis_ttd").val();
		let ttd_sasaran=$("#signature64_ttd_sasaran").val();
		let nama_pernyataan=$("#nama_pernyataan").val();
		let hereby=$("#hereby").val();
		let untuk_tindakan=$("#untuk_tindakan").val();
		let terhadap=$("#terhadap").val();
				$("#modal_ttd_sasaran").modal('hide');
		if (jenis_ttd==''){
				swal({
					title: "Gagal!",
					text: "Isi Yang Bertandatangan.",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				$("#modal_ttd_sasaran").modal('show');
				return false;
		}
		
		if (nama_pernyataan==''){
				swal({
					title: "Gagal!",
					text: "Tentukan Nama Tanda Tangan .",
					type: "danger",
					timer: 1000,
					showConfirmButton: false
				});
				return false;
		}
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_sasaran_invasif/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_sasaran:ttd_sasaran,
					ttd_sasaran_nama:nama_pernyataan,
					hereby:hereby,
					assesmen_id:assesmen_id,
					untuk_tindakan:untuk_tindakan,
					terhadap:terhadap,
					jenis_ttd:jenis_ttd,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Tanda Tangan'});
				  $("#cover-spin").hide();
				    if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					$("#signature64_ttd_sasaran").val('')
					$("#signature64_ttd_sasaran_2").val('')
					$("#modal_ttd_sasaran").modal('hide');
					clear_invasif();
				}
			});
	}
	function hapus_ttd_sasaran(id){
		var ttd_sasaran='';
		let ttd_sasaran_nama=$("#nama_sasaran").val();
		
		$("#cover-spin").show();
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/simpan_ttd_sasaran_invasif/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					id:id,
					ttd_sasaran:ttd_sasaran,
					ttd_sasaran_nama:ttd_sasaran_nama,
			  },success: function(data) {
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
				  $("#cover-spin").hide();
					if (tab_active==1){
						load_index_materi();
						
					}else{
						
						list_history_pengkajian();
					}
					$("#modal_ttd_sasaran").modal('hide');
				}
			});
	}
	$("#jenis_ttd").change(function(){
		get_jenis_ttd_invasif();
	});
	function get_jenis_ttd_invasif(){
		
		let assesmen_id=$("#assesmen_id").val();
		let jenis_ttd=$("#jenis_ttd").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tipe_rj_ri=$("#tipe_rj_ri").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/get_jenis_ttd_invasif', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					jenis_ttd:jenis_ttd,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tipe_rj_ri:tipe_rj_ri,
				
				   },
			success: function(data) {
				$("#nama_pernyataan").val(data.nama_ttd);
				
			}
		});
	}
</script>