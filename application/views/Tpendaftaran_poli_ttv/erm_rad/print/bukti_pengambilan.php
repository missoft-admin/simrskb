<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>BUKTI PENGAMBILAN RADIOLOGI</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 14px !important;
      }
      table {
        font-size: 14px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 9px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
      try {
        this.print();
      }
      catch(e) {
        window.onload = window.print;
      }
    </script>
  </head>
  <body>
    <table class="content-2">
      <tr>
        <td rowspan="2" style="width:10%"><img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"></td>
      </tr>
      <tr>
        <td style="text-align:left" colspan="2">
          &nbsp;&nbsp;INSTALASI RADIOLOGI<br>
          <b>BUKTI PENGAMBILAN HASIL<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RADIOLOGI</b>
        </td>
      </tr>
      <tr>
        <td style="width:100px">NO REGISTER</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nomor_permintaan; ?></td>
      </tr>
      <tr>
        <td style="width:100px">NO REKAM MEDIS</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nomor_medrec; ?></td>
      </tr>
      <tr>
        <td style="width:100px">NAMA PASIEN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nama_pasien; ?></td>
      </tr>
      <tr>
        <td style="width:100px">TANGGAL LAHIR / UMUR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date("Y-m-d", strtotime($tanggal_lahir));?> / <?= (($umur_tahun)? $umur_tahun.' th ':'').(($umur_bulan)? $umur_bulan.' bln ':'').(($umur_hari)? $umur_hari.' hr ':'')?></td>
      </tr>
      <tr>
        <td style="width:100px">TANGGAL PEMERIKSAAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $waktu_pemeriksaan; ?></td>
      </tr>
      <tr>
        <td style="width:100px">NOMOR FOTO</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nomor_foto; ?></td>
      </tr>
      <tr>
        <td style="width:100px">TANGGAL PENGAMBILAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $waktu_pengambilan; ?></td>
      </tr>
    </table>
    <br>
    <table>
      <tr>
        <td style="width:10%" class="text-left"><b><?= strip_tags($pengaturan_printout['label_footer']); ?></b></td>
        <td colspan="2">Foto yang sudah dilihat dokter Orthopaedi,<br>diambil kembali setelah 3 hari kedepan<br>untuk di Expertise dokter Ahli Radiologi<br><b>(Harap Dibawa Saat Pengambilan)</b></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="width:10%" class="text-left"><img src="<?php echo base_url(); ?>qrcode/qr_code_ttd_dokter/<?php echo $dokter_peminta_id; ?>" width="100px"></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="width:10%" class="text-left">( <?= $dokter_peminta; ?> )</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
