<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>BUKTI PEMERIKSAAN RADIOLOGI</title>
	<style>
		@font-face {
			font-family: Merchant Copy Regular;
			src: url('<?= base_url() ?>assets/fonts/merchant-copy.regular.ttf');
		}

		@font-face {
			font-family: Fake Receipt;
			src: url('<?= base_url() ?>assets/fonts/fakereceipt.ttf');
		}

		@page {
			margin: 15px;
		}

		body {
			-webkit-print-color-adjust: exact;
			font-family: Merchant Copy Regular;
			font-size: 16px;
		}

		table {
			font-size: 16px !important;
			border-collapse: collapse !important;
			width: 95% !important;
		}

		td {
			padding: 2px;
		}

		.header {
			font-family: Fake Receipt;
		}

		.content td {
			margin: 3px;
			border: 0px solid #6033FF;
		}

		/* Border */
		.border-full {
			border: 0px solid #000 !important;
		}

		/* Text Position */
		.text-center {
			text-align: center !important;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		/* Text Style */
		.text-italic {
			font-style: italic;
		}

		.text-bold {
			font-weight: bold;
		}
	</style>
</head>

<body>
	<div style="width:210">
		<!-- Header -->
		<table style="width:100%;">
			<tr>
				<td class="text-center" rowspan="5" style="padding: 0px; border: 0px solid #6033FF;">
					RSKB HALMAHERA SIAGA<br>
					JL. LLRE. Martadinata No. 28<br>
					Telp. +6222-4206061<br>
					Bandung, 40115
				</td>
			</tr>
		</table>

		<!-- Body -->
		<table class="content" style="width:100%">
			<tr>
				<td colspan="2" class="header text-center">BUKTI PEMERIKSAAN RADIOLOGI</td>
			</tr>
			<tr>
				<td style="width:30%">NO REGISTER</td>
				<td style="width:70%">: <?= $nomor_permintaan; ?></td>
			</tr>
			<tr>
				<td>TANGGAL & JAM</td>
				<td>: <?= $waktu_permintaan; ?></td>
			</tr>
			<tr>
				<td>NO REKAM MEDIS</td>
				<td>: <?= $nomor_medrec; ?></td>
			</tr>
			<tr>
				<td>NAMA PASIEN</td>
				<td>: <?= $nama_pasien; ?></td>
			</tr>
			<tr>
				<td>TANGGAL LAHIR</td>
				<td>: <?= $tanggal_lahir; ?></td>
			</tr>
			<tr>
				<td>UMUR</td>
				<td>: <?= (($umur_tahun)? $umur_tahun.' th ':'').(($umur_bulan)? $umur_bulan.' bln ':'').(($umur_hari)? $umur_hari.' hr ':'')?></td>
			</tr>
			<tr>
				<td>ASAL RUJUKAN</td>
				<td>: <?= $asal_rujukan; ?></td>
			</tr>
			<tr>
				<td>DOKTER PERUJUK</td>
				<td>: <?= $dokter_perujuk; ?></td>
			</tr>
			<tr>
				<td>KELOMPOK PASIEN</td>
				<td>: <?= $kelompok_pasien; ?></td>
			</tr>
		</table>

		<!-- Detail -->
		<table class="content" style="width:100%">
			<tr>
				<td colspan="5" class="header text-center">RINCIAN PEMERIKSAAN</td>
			</tr>
			<tr>
				<td class="border-full text-center" style="width:5%">NO</td>
				<td class="border-full text-center" style="width:20%">PEMERIKSAAN</td>
				<td class="border-full text-center" style="width:25%">HARGA</td>
				<td class="border-full text-center" style="width:25%">QTY</td>
				<td class="border-full text-center" style="width:25%">TOTAL</td>
			</tr>
			<?php $number = 0; ?>
			<?php $totalharga = 0; ?>
			<?php foreach($list_pemeriksaan as $row){ ?>
				<?php $number = $number + 1; ?>
				<?php $totalharga = $totalharga + $row->totalkeseluruhan; ?>
				<tr>
					<td class="border-full text-center"> <?=strtoupper(($number))?></td>
					<td class="border-full text-left"> <?=strtoupper(($row->namapemeriksaan_detail))?></td>
					<td class="border-full text-right"><?= number_format($row->total, 0)?></td>
					<td class="border-full text-center"><?= number_format($row->kuantitas, 0)?></td>
					<td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0)?></td>
				</tr>
			<?php } ?>
			<tr>
				<td colspan="3"></td>
				<td class="text-right">TOTAL</td>
				<td class="text-right"><?=number_format($totalharga,0)?></td>
			</tr>
		</table>

		<!-- Footer -->
		<table style="width:100%">
			<tr>
				<td><?= strip_tags($pengaturan_printout['label_footer']); ?></td>
			</tr>
			<?php if ($pengaturan_printout['tampilkan_tanda_tangan'] == 1) { ?>
      <tr>
        <td>
          <img src="<?php echo base_url(); ?>qrcode/qr_code_ttd_dokter/<?php echo $dokter_peminta_id; ?>" width="100px">
        </td>
      </tr>
      <?php } ?>
			<tr>
				<td>(<?= $dokter_peminta; ?>)</td>
			</tr>
      <?php if ($pengaturan_printout['tampilkan_tanggal_jam_cetak'] == 1) { ?>
			<tr>
				<td>Tanggal & Jam Cetak : <?=date('Y-m-d h:m:s')?></td>
			</tr>
      <?php } ?>
		</table>
	</div>
</body>

</html>
