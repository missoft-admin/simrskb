<script type="text/javascript">
function buatDraftPermintaan() {
    let pendaftaran_id = '{pendaftaran_id}';
    let pasien_id = '{idpasien}';
    let asal_rujukan = '{asal_rujukan_poli_ranap}';
    let jenis_pemeriksaan = $("#jenis_pemeriksaan option:selected").val();
    let rencana_pemeriksaan = $("#rencana_pemeriksaan").val();

    swal({
        title: "Apakah Anda Yakin?",
        text: "Membuat Draft Permintaan?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#34a263",
        cancelButtonText: "Batalkan",
    }).then(function() {
        $("#cover-spin").show();
        $.ajax({
            url: '{site_url}term_radiologi_ctscan/buat_draft_permintaan',
            dataType: "JSON",
            method: "POST",
            data: {
                pendaftaran_id: pendaftaran_id,
                pasien_id: pasien_id,
                asal_rujukan: asal_rujukan,
                jenis_pemeriksaan: jenis_pemeriksaan,
                rencana_pemeriksaan: rencana_pemeriksaan,
            },
            success: function(data) {
                $.toaster({ priority: 'success', title: 'Success!', message: 'Draft Permintaan berhasil dibuat.' });
                location.reload();
            }
        });
    });
}

function batalDraftPermintaan() {
    let id = '{transaksi_id}';

    swal({
        title: "Apakah Anda Yakin?",
        text: "Membatalkan Draft Permintaan?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#34a263",
        cancelButtonText: "Batalkan",
    }).then(function() {
        $("#cover-spin").show();
        $.ajax({
            url: '{site_url}term_radiologi_ctscan/batal_draft_permintaan/' + id,
            dataType: "JSON",
            success: function(data) {
                $.toaster({ priority: 'success', title: 'Success!', message: 'Draft Permintaan berhasil dibatalkan.' });
                location.reload();
            }
        });
    });
}

function updateDraftPermintaan() {
    let payload = {
        transaksi_id: $("#transaksi_id").val(),
        pendaftaran_id: $("#pendaftaran_id").val(),
        pasien_id: $("#pasien_id").val(),
        dokter_perujuk_id: $("#dokter_perujuk_id").val(),
        ppa_id: $("#ppa_id").val(),
        jenis_pemeriksaan: $("#jenis_pemeriksaan option:selected").val(),
        rencana_pemeriksaan: $("#rencana_pemeriksaan").val(),
        tujuan_radiologi: $("#tujuan_radiologi option:selected").val(),
        dokter_peminta_id: $("#dokter_peminta_id option:selected").val(),
        diagnosa: $("#diagnosa").val(),
        catatan_permintaan: $("#catatan_permintaan").val(),
        tanggal_permintaan: $("#tanggal_permintaan").val(),
        waktu_permintaan: $("#waktu_permintaan").val(),
        prioritas: $("#prioritas option:selected").val(),
        pasien_puasa: $("#pasien_puasa option:selected").val(),
        pengiriman_hasil: $("#pengiriman_hasil option:selected").val(),
        alergi_bahan_kontras: $("#alergi_bahan_kontras option:selected").val(),
        pasien_hamil: $("#pasien_hamil option:selected").val(),
        data_pemeriksaan: $("#data_pemeriksaan").val()
    };

    $.ajax({
        url: '{site_url}term_radiologi_ctscan/update_draft_permintaan', 
        type: 'POST',
        data: payload,
        success: function (result) {
            $.toaster({ priority: 'success', title: 'Success!', message: 'Draft Permintaan berhasil disimpan.' });
        },
        error: function (error) {
            console.error('Error cloning order:', error);
            alert('Error cloning order. Please try again.');
        }
    });
}

function resetFormPermintaan() {
    selectedPemeriksaan = [];
    
    $('#table-pemeriksaan input[type="checkbox"]').prop('checked', false);
    $('#table-order tbody').empty();
    $('#table-order tfoot td:eq(1)').text('0');

    // Reset other form elements if needed
    $('#jenis_pemeriksaan, #tujuan_radiologi, #dokter_peminta_id, #prioritas, #pasien_puasa, #pengiriman_hasil, #alergi_bahan_kontras, #pasien_hamil').val('').trigger('change');
    $('#rencana_pemeriksaan, #diagnosa, #catatan_permintaan').val('');
    $('#rencana_pemeriksaan, #tanggal_permintaan').val('<?= date('d/m/Y') ?>');
    $('#waktu_permintaan').val('<?= date('H:i:s') ?>');

    $('#data_pemeriksaan').val('[]');
}

function updateSelectedPemeriksaan(pemeriksaanId, isChecked) {
    if (isChecked) {
        addSelectedPemeriksaan(pemeriksaanId);
    } else {
        removeSelectedPemeriksaan(pemeriksaanId);
    }
}

function addSelectedPemeriksaan(pemeriksaanId) {
    $('#table-pemeriksaan .child-checkbox').each(function() {
        const itemPemeriksaanId = String($(this).val());

        if (itemPemeriksaanId == pemeriksaanId) {
            let childPemeriksaan = {
                pemeriksaan_id: $(this).val(),
                radiologi_id: $(this).data('radiologi-id'),
                tipe_id: $(this).data('tipe-id'),
                kelas: $(this).data('kelas'),
                nama_tarif: $(this).data('nama-tarif'),
                nama_pemeriksaan: $(this).data('nama-pemeriksaan'),
                nama_pemeriksaan_detail: $(this).data('nama-pemeriksaan-detail'),
                path: $(this).data('path'),
                jasa_sarana: $(this).data('jasa-sarana'),
                jasa_pelayanan: $(this).data('jasa-pelayanan'),
                bhp: $(this).data('bhp'),
                biaya_perawatan: $(this).data('biaya-perawatan'),
                total: $(this).data('total'),
                kuantitas: $(this).data('kuantitas'),
                diskon: $(this).data('diskon'),
                total_keseluruhan: $(this).data('total-keseluruhan'),
                status_verifikasi: 0,
            };

            if (!isDuplicatePemeriksaan(childPemeriksaan)) {
                selectedPemeriksaan.push(childPemeriksaan);
                saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);
            }
        }
    });
}

function removeSelectedPemeriksaan(pemeriksaanId) {
    selectedPemeriksaan = selectedPemeriksaan.filter(item => item.pemeriksaan_id != pemeriksaanId);
    saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);
}

function isDuplicatePemeriksaan(pemeriksaan) {
    return selectedPemeriksaan.some(item =>
        item.pemeriksaan_id == pemeriksaan.pemeriksaan_id
    );
}

function updateDaftarPemeriksaan(statusForm = '') {
    let totalHarga = 0;
    $('#table-order tbody').empty();

    $.each(selectedPemeriksaan, function(index, pemeriksaan) {
        let row = `<tr>
            <td class="text-center">${index + 1}</td>
            <td>${pemeriksaan.nama_pemeriksaan}</td>
            <td class="text-right">${$.number(pemeriksaan.total)}</td>
            ${statusForm != 'lihat_permintaan' && `<td class="text-center">
                <button class="btn btn-sm btn-danger btn-remove" data-index="${index}">
                    <i class="fa fa-trash"></i> Hapus
                </button>
            </td>`}
        </tr>`;

        totalHarga += parseInt(pemeriksaan.total);
        $('#table-order tbody').append(row);
    });

    $('#table-order tfoot td:eq(1)').text(totalHarga.toLocaleString());

    $('.btn-remove').on('click', function() {
        let indexToRemove = $(this).data('index');
        let pemeriksaanToRemove = selectedPemeriksaan[indexToRemove];

        selectedPemeriksaan.splice(indexToRemove, 1);
        updateDaftarPemeriksaan();
        $('#table-pemeriksaan input[value="' + pemeriksaanToRemove.pemeriksaan_id + '"]').prop('checked', false);
    });
}

function loadSelectedPemeriksaan(transaksiId) {
    const savedData = localStorage.getItem(`selectedPemeriksaanRadiologiPermintaan_${transaksiId}`);
    return savedData ? JSON.parse(savedData) : [];
}

function saveSelectedPemeriksaan(transaksiId, data) {
    localStorage.setItem(`selectedPemeriksaanRadiologiPermintaan_${transaksiId}`, JSON.stringify(data));
}

function lihatPermintaan(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_rad/ctscan_permintaan/' + transaksiId + '/lihat_permintaan', '_blank');
}

function clonePermintaan(asalRujukan, transaksiId) {
    swal({
        title: "Konfirmasi Duplikasi Permintaan",
        text: "Apakah Anda yakin ingin membuat transaksi permintaan dari duplikasi?",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#34a263",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $("#cover-spin").show();
            $.ajax({
            url: '{site_url}term_radiologi_ctscan/clone_permintaan/' + transaksiId, 
            dataType: "JSON",
            success: function (result) {
                $.toaster({priority: 'success', title: 'Berhasil!', message: 'Data Permintaan berhasil diduplikasi.'});
                window.location = '{base_url}term_radiologi_ctscan/tindakan/' + asalRujukan + '/' + '{pendaftaran_id}' + '/erm_rad/ctscan_permintaan';
            },
            error: function (error) {
                console.error('Error cloning order:', error);
                alert('Error cloning order. Please try again.');
            }
            });
        }
    });
}

function cetakPermintaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_pemeriksaan/' + transaksiId, '_blank');
}

function editPermintaan(asalRujukan, pendaftaranId, transaksiId, version) {
    window.open('{base_url}term_radiologi_ctscan/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_rad/ctscan_permintaan/' + transaksiId + '/edit_permintaan', '_blank');
}

function batalPermintaan(transaksiId) {
    swal({
        title: 'Konfirmasi Batal Permintaan',
        text: 'Apakah anda yakin akan membatalkan permintaan ini? Jika dilakukan pembatalan maka permintaan anda tidak akan terkirim kepada radiologi tujuan, tekan tombol "Ya" untuk membatalkan.',
        type : "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.toaster({priority: 'success', title: 'Berhasil!', message: 'Data Permintaan berhasil dibatalkan.'});
            $.ajax({
                url: '{site_url}term_radiologi_ctscan/batal_draft_permintaan/' + transaksiId,
                success: function (result) {
                    $('#table-order-saya').DataTable().ajax.reload();
                },
                error: function (error) {
                    console.error('Error deleting order:', error);
                    alert('Error deleting order. Please try again.');
                }
            });
        }
    });
}

function lihatHasilPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan_hasil/proses/' + transaksiId + '/lihat_hasil_pemeriksaan', '_blank');
}

function cetakPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_pemeriksaan/' + transaksiId, '_blank');
}

function cetakBuktiPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pemeriksaan/' + transaksiId, '_blank');
}

function cetakBuktiPemeriksaanSmall(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pemeriksaan_small/' + transaksiId, '_blank');
}

function cetakBuktiPengambilanPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pengambilan_pemeriksaan/' + transaksiId, '_blank');
}

function cetakBuktiPengambilanPemeriksaanSmall(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pengambilan_pemeriksaan_small/' + transaksiId, '_blank');
}

function cetakHasilPemeriksaan(hasilPemeriksaanId, typeFormat) {
    window.open('{base_url}term_radiologi_ctscan_expertise/cetak_hasil_pemeriksaan/' + hasilPemeriksaanId + '/' + typeFormat, '_blank');
}
</script>
