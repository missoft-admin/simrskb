<style>
.auto_blur {
    background-color: #d0f3df;
}

.tab-content {
    background-color: #f9f9f9;
}

.gray-background {
  background-color: #f2f2f2 !important;
}

.white-background {
  background-color: #ffffff !important;
}

/* Increase z-index for modal and select dropdowns */
.modal {
    z-index: 1201; /* Adjust the value if needed */
}

.select2-container {
    z-index: 9999 !important;
}
</style>

<?php if ($menu_kiri == 'usg_pemeriksaan') { ?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri == 'usg_pemeriksaan' ? 'block' : 'none') ?>;">
    <button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
    <div class="block animated fadeIn push-10-t" data-category="erm_radiologi">
      <div class="block">
          <ul class="nav nav-tabs" data-toggle="tabs">
              <li class="active">
                  <a href="#tab_order_baru"><i class="fa fa-list"></i> Order &nbsp; <span class="badge badge-success pull-right">Proses Pemeriksaan</span></a>
              </li>
          </ul>
          <div class="block-content tab-content">
              <div class="tab-pane fade fade-left active in" id="tab_order_baru">
                <?= form_open('term_radiologi_usg/simpan_pemeriksaan','class="form-horizontal push-10-t" id="form-work"') ?>
                <div class="row">
                  <div class="col-md-12 text-right">
                    <?php if ($statuskasir != 2) { ?>
                      <?php if ($status_form == 'edit_pemeriksaan') { ?>
                        <button type="submit" class="btn btn-warning" name="btn_submit" data-type="form-submit-update"><i class="fa fa-save"></i> Simpan Perubahan</button>
                      <?php } ?>
                      <button type="submit" class="btn btn-success" name="btn_submit" data-type="form-submit-process"><i class="fa fa-check-circle"></i> Proses Transaksi</button>
                      <a href="#" class="btn btn-danger" onclick="batalPermintaan('{transaksi_id}')"><i class="fa fa-trash"></i> Batalkan Permintaan</a>
                    <?php } ?>
                    <a href="#" class="btn btn-primary" onclick="cetakPermintaan('{transaksi_id}')"><i class="fa fa-print"></i> Cetak Permintaan</a>
                    <a href="{site_url}term_radiologi_usg/tindakan/{asal_rujukan_status}/{pendaftaran_id}/erm_rad/usg_permintaan" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <h4 class="font-w700 push-5 text-center text-primary"><?= strip_tags($pengaturan_form['label_judul']); ?></h4>
                  <h4 class="push-5 text-center"><i><?= strip_tags($pengaturan_form['label_judul_eng']); ?></i></h4>
                </div>

                <br><br>

                <div class="row push-10-t">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Jenis Pemeriksaan</label>
                          <div class="col-xs-12">
                            <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                              <?php foreach(list_variable_ref(86) as $row){ ?>
                                <option value="<?= $row->id; ?>" <?= ($jenis_pemeriksaan == '' && $row->st_default == '1' ? 'selected' : ''); ?> <?= ($row->id == $jenis_pemeriksaan ? 'selected' : ''); ?>><?= $row->nama; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Rencana Pemeriksaan</label>
                          <div class="col-xs-12">
                            <input type="text" name="rencana_pemeriksaan" id="rencana_pemeriksaan" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" value="{rencana_pemeriksaan}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Tanggal & Waktu Pembuatan</label>
                          <div class="col-xs-12">
                            <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_waktu_pembuatan}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Nama Profesional Pemberi Asuhan</label>
                          <div class="col-xs-12">
                            <input type="text" class="form-control" disabled value="{login_nip_ppa} - {login_nama_ppa}">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <?php if ($status_form != '') { ?>
                <div class="form-input-pemeriksaan">
                  <hr>

                  <div class="row push-10-t">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_tujuan_radiologi']); ?></label>
                            <div class="col-xs-12">
                              <select name="tujuan_radiologi" id="tujuan_radiologi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach ($list_tujuan_radiologi as $r) { ?>
                                  <option value="<?= $r->id; ?>" <?= $row->id == $tujuan_radiologi ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_dokter_peminta_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <select name="dokter_peminta_id" id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                                  <option value="<?= $r->id; ?>" <?= $r->id == $login_pegawai_id || $r->id == $dpjp ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_diagnosa']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="diagnosa" id="diagnosa" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_diagnosa']); ?>" value="{diagnosa}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="catatan_permintaan" id="catatan_permintaan" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?>" value="{catatan_permintaan}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_waktu_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_permintaan" id="tanggal_permintaan" placeholder="HH/BB/TTTT" value="{tanggal_permintaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_permintaan" id="waktu_permintaan" value="{waktu_permintaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_prioritas']); ?></label>
                            <div class="col-xs-12">
                              <select name="prioritas" id="prioritas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(249) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $prioritas && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $prioritas ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pasien_puasa']); ?></label>
                            <div class="col-xs-12">
                              <select name="pasien_puasa" id="pasien_puasa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(87) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $pasien_puasa && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $pasien_puasa ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pengiriman_hasil']); ?></label>
                            <div class="col-xs-12">
                              <select name="pengiriman_hasil" id="pengiriman_hasil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(88) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $pengiriman_hasil && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_alergi_bahan_kontras']); ?></label>
                            <div class="col-xs-12">
                              <select name="alergi_bahan_kontras" id="alergi_bahan_kontras" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(252) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $alergi_bahan_kontras && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $alergi_bahan_kontras ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pasien_hamil']); ?></label>
                            <div class="col-xs-12">
                              <select name="pasien_hamil" id="pasien_hamil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(253) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $pasien_hamil && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $pasien_hamil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <hr>

                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Pemeriksaan</label>
                            <div class="col-xs-12">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pemeriksaan" id="tanggal_pemeriksaan" placeholder="HH/BB/TTTT" value="{tanggal_pemeriksaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_pemeriksaan" id="waktu_pemeriksaan" value="{waktu_pemeriksaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Petugas Pemeriksa</label>
                            <div class="col-xs-12">
                              <select name="petugas_pemeriksaan" class="js-select2 form-control" id="petugas_pemeriksaan" style="width: 100%;" data-placeholder="Pilih Opsi">
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Nomor Foto</label>
                            <div class="col-xs-12">
                              <input type="text" name="nomor_foto" id="nomor_foto" class="form-control" placeholder="Nomor Foto" value="{nomor_foto}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Dokter Radiologi</label>
                            <div class="col-xs-12">
                              <select name="dokter_radiologi" class="js-select2 form-control" id="dokter_radiologi" style="width: 100%;" data-placeholder="Pilih Opsi" <?= ($status_pemeriksaan >= 6 ? 'disabled' : ''); ?>>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Jumlah Expose</label>
                            <div class="col-xs-12">
                              <input type="text" name="jumlah_expose" id="jumlah_expose" class="form-control" placeholder="Jumlah Expose" value="{jumlah_expose}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Jumlah Film</label>
                            <div class="col-xs-12">
                              <input type="text" name="jumlah_film" id="jumlah_film" class="form-control" placeholder="Jumlah Film" value="{jumlah_film}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">QP</label>
                            <div class="col-xs-12">
                              <input type="text" name="qp" id="qp" class="form-control" placeholder="QP" value="{qp}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">MAS</label>
                            <div class="col-xs-12">
                              <input type="text" name="mas" id="mas" class="form-control" placeholder="MAS" value="{mas}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Posisi</label>
                            <div class="col-xs-12">
                              <input type="text" name="posisi" id="posisi" class="form-control" placeholder="Posisi" value="{posisi}">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <hr>

                  <div class="row">
                    <div class="col-md-6">
                      <h6 class="font-w700 push-5 text-primary">DAFTAR PEMERIKSAAN</h6>
                      <div class="table-header text-right" style="margin-bottom: 25px;">
                          <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-filter">
                              <i class="fa fa-filter"></i> Filter
                          </button>
                      </div>
                      <table class="table table-striped table-striped table-hover table-bordered" id="table-pemeriksaan">
                        <thead>
                          <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Pemeriksaan</th>
                            <th class="text-center">Harga</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                    </div>
                    <div class="col-md-6">
                      <h6 class="font-w700 push-5 text-primary">DAFTAR ORDER</h6>
                      <table class="table table-striped table-striped table-hover table-bordered" id="table-order">
                        <thead>
                          <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Pemeriksaan</th>
                            <th class="text-center">Harga</th>
                            <?php if ('lihat_pemeriksaan' != $status_form) { ?>
                            <th class="text-center">Aksi</th>
                            <?php } ?>
                          </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                          <tr>
                            <td class="text-right" colspan="2">Total</td>
                            <td class="text-right"></td>
                            <?php if ('lihat_pemeriksaan' != $status_form) { ?>
                            <td></td>
                            <?php } ?>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
                <?php } ?>

                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="{pendaftaran_id}">
                <input type="hidden" id="transaksi_id" name="transaksi_id" value="{transaksi_id}">
                <input type="hidden" id="pasien_id" name="pasien_id" value="{idpasien}">
                <input type="hidden" id="dokter_perujuk_id" name="dokter_perujuk_id" value="{dpjp}">
                <input type="hidden" id="data_pemeriksaan" name="data_pemeriksaan" value="">
                <input type="hidden" id="status_form" name="status_form" value="{status_form}">
                <input type="hidden" id="ppa_id" name="ppa_id" value="{login_ppa_id}">
                <input type="hidden" id="form_submit" name="form_submit" value="">
                <?= form_close() ?>
              </div>
          </div>
      </div>
    </div>
</div>
<? } ?>

<?php $this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/modal/modal_filter'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/modal/modal_edit_pemeriksaan'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/modal/modal_ubah_kelas_tarif_satuan'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/modal/modal_ubah_tarif'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_rad/usg/services_pemeriksaan'); ?>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
      $("#cover-spin").hide();

      $(".time-datepicker").datetimepicker({
          format: "HH:mm:ss"
      });

      $("#tujuan_radiologi").on("change", function() {
          let value = $(this).val();
          loadDataDokterRadiologi(value);
      });

      $('#table-pemeriksaan').on('click', '.child-checkbox', function() {
          let isChecked = $(this).prop('checked');
          let pemeriksaanId = $(this).val();

          updateSelectedPemeriksaan(pemeriksaanId, isChecked);
          updateDaftarPemeriksaan();
      });

      $("#form-work input").on('blur', function() {
          updateDraftPermintaan();
      });

      $("#form-work select").on('change', function() {
          updateDraftPermintaan();
      });

      $('#form-reset').on('click', function () {
          resetFormPermintaan();
      });

      $('button[name="btn_submit"]').on('click', function (event) {
          event.preventDefault();

          $('#form_submit').val($(this).data('type'));

          if ($('#diagnosa').val().trim() == '') {
              swal({
                  title: "Peringatan",
                  text: "Diagnosa tidak boleh kosong!",
                  type: "warning",
                  confirmButtonColor: "#34a263",
                  confirmButtonText: "OK",
              });
              return;
          }
          
          swal({
            title: "Konfirmasi Order Pemeriksaan",
            text: "Apakah Anda yakin akan mengirim permintaan ini? Permintaan Anda akan dikirim kepada radiologi tujuan. Tekan Ya untuk melanjutkan.",
            type : "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
          }).then((willSubmit) => {
              if (willSubmit) {
                $("#cover-spin").show();
                $('#data_pemeriksaan').val(JSON.stringify(selectedPemeriksaan));
                $('#form-work').submit();
                
                localStorage.removeItem(`selectedPemeriksaanRadiologi_${transaksiId}`);
              }
          });
      });
  });

  function getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeRad, jenisPemeriksaan, anatomiTubuh, posisiPemeriksaan, namaPemeriksaan) {
      $('#table-pemeriksaan').DataTable().destroy();
      $('#table-pemeriksaan').DataTable({
          "bSort": false,
          "autoWidth": false,
          "pageLength": 10,
          "ordering": true,
          "processing": true,
          "serverSide": true,
          "order": [],
          "ajax": {
              url: '{site_url}term_radiologi_usg/get_daftar_pemeriksaan_radiologi',
              type: "POST",
              dataType: 'json',
              data: {
                  idkelas: kelasId,
                  idkelompokpasien: kelompokPasienId,
                  idrekanan: rekananId,
                  idtipe: tipeRad,
                  jenis_pemeriksaan: jenisPemeriksaan,
                  anatomi_tubuh: anatomiTubuh,
                  posisi_pemeriksaan: posisiPemeriksaan,
                  nama_pemeriksaan: namaPemeriksaan,
              }
          },
          "columnDefs": [
              {
                  "width": "10%",
                  "targets": [0, 1, 3],
                  "className": "text-center"
              },
              {
                  "width": "10%",
                  "targets": [2],
              },
          ],
          "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
          "drawCallback": function(settings) {
            let statusKasir = '{statuskasir}';
            $('#table-pemeriksaan input[type="checkbox"]').each(function() {
                let checkboxValue = $(this).val();
                let isChecked = selectedPemeriksaan.some(function(item) {
                    return item.pemeriksaan_id == checkboxValue && item.status_delete == 0;
                });

                $(this).prop('checked', isChecked);

                if (statusKasir == 2) {
                  $(this).prop('disabled', true);
                }
            });
          },
      });
  }

  var transaksiId = '{transaksi_id}';
  var storedPemeriksaan = loadSelectedPemeriksaan(transaksiId);
  var selectedPemeriksaan = storedPemeriksaan.length > 0 ? storedPemeriksaan : <?= json_encode($selectedPemeriksaan); ?>;

  updateDaftarPemeriksaan('{status_form}');
  loadDataDokterRadiologi('{tujuan_radiologi}', '{dokter_radiologi_id}');
  loadDataPetugasProsesPemeriksaan('{tujuan_radiologi}', '{petugas_pemeriksaan}');

  jQuery(function() {
      BaseTableDatatables.init();
      let kelasId = '{idkelas}';
      let kelompokPasienId = '{idkelompokpasien}';
      let rekananId = '{idrekanan}';
      let tipeRad = '2';
      let jenisPemeriksaan = [];
      let anatomiTubuh = [];
      let posisiPemeriksaan = [];
      let namaPemeriksaan = '';

      getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeRad, jenisPemeriksaan, anatomiTubuh, posisiPemeriksaan, namaPemeriksaan);
  });
</script>