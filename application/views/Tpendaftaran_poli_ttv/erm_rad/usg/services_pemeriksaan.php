<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
function cetakPermintaan(transaksiId) {
    window.open('{base_url}term_radiologi_usg/cetak_pemeriksaan/' + transaksiId, '_blank');
}

function batalPermintaan(transaksiId) {
    swal({
        title: 'Konfirmasi Batal Permintaan',
        text: 'Apakah anda yakin akan membatalkan permintaan ini? Jika dilakukan pembatalan maka permintaan anda tidak akan terkirim kepada radiologi tujuan, tekan tombol "Ya" untuk membatalkan.',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.toaster({
                priority: 'success',
                title: 'Berhasil!',
                message: 'Data Permintaan berhasil dibatalkan.'
            });
            $.ajax({
                url: '{site_url}term_radiologi_usg/batal_draft_permintaan/' + transaksiId,
                success: function(result) {
                    $('#table-order-saya').DataTable().ajax.reload();
                },
                error: function(error) {
                    console.error('Error deleting order:', error);
                    alert('Error deleting order. Please try again.');
                }
            });
        }
    });
}

function updateDraftPermintaan() {
    let payload = {
        transaksi_id: $("#transaksi_id").val(),
        pendaftaran_id: $("#pendaftaran_id").val(),
        pasien_id: $("#pasien_id").val(),
        dokter_perujuk_id: $("#dokter_perujuk_id").val(),
        ppa_id: $("#ppa_id").val(),
        jenis_pemeriksaan: $("#jenis_pemeriksaan option:selected").val(),
        rencana_pemeriksaan: $("#rencana_pemeriksaan").val(),
        tujuan_radiologi: $("#tujuan_radiologi option:selected").val(),
        dokter_peminta_id: $("#dokter_peminta_id option:selected").val(),
        diagnosa: $("#diagnosa").val(),
        catatan_permintaan: $("#catatan_permintaan").val(),
        tanggal_permintaan: $("#tanggal_permintaan").val(),
        waktu_permintaan: $("#waktu_permintaan").val(),
        prioritas: $("#prioritas option:selected").val(),
        pasien_puasa: $("#pasien_puasa option:selected").val(),
        pengiriman_hasil: $("#pengiriman_hasil option:selected").val(),
        alergi_bahan_kontras: $("#alergi_bahan_kontras option:selected").val(),
        pasien_hamil: $("#pasien_hamil option:selected").val(),
        tanggal_pemeriksaan: $("#tanggal_pemeriksaan").val(),
        waktu_pemeriksaan: $("#waktu_pemeriksaan").val(),
        petugas_pemeriksaan: $("#petugas_pemeriksaan").val(),
        nomor_foto: $("#nomor_foto").val(),
        dokter_radiologi: $("#dokter_radiologi option:selected").val(),
        jumlah_expose: $("#jumlah_expose").val(),
        jumlah_film: $("#jumlah_film").val(),
        qp: $("#qp").val(),
        mas: $("#mas").val(),
        posisi: $("#posisi").val(),
        data_pemeriksaan: $("#data_pemeriksaan").val()
    };

    $.ajax({
        url: '{site_url}term_radiologi_usg/update_draft_permintaan',
        type: 'POST',
        data: payload,
        success: function(result) {
            $.toaster({
                priority: 'success',
                title: 'Success!',
                message: 'Draft Permintaan berhasil disimpan.'
            });
        },
        error: function(error) {
            console.error('Error cloning order:', error);
            alert('Error cloning order. Please try again.');
        }
    });
}

function updateSelectedPemeriksaan(pemeriksaanId, isChecked) {
    if (isChecked) {
        addSelectedPemeriksaan(pemeriksaanId);
    } else {
        removeSelectedPemeriksaan(pemeriksaanId);
    }
}

function addSelectedPemeriksaan(pemeriksaanId) {
    $('#table-pemeriksaan .child-checkbox').each(function() {
        const itemPemeriksaanId = String($(this).val());

        if ((itemPemeriksaanId == pemeriksaanId)) {
            var existingPemeriksaan = selectedPemeriksaan.find(item => item.pemeriksaan_id == itemPemeriksaanId);

            if (existingPemeriksaan) {
                if (existingPemeriksaan.status_delete == 1) {
                    existingPemeriksaan.status_delete = 0;
                }
            } else {
                var childPemeriksaan = {
                    pemeriksaan_id: $(this).val(),
                    radiologi_id: $(this).data('radiologi-id'),
                    tipe_id: $(this).data('tipe-id'),
                    kelas: $(this).data('kelas'),
                    nama_tarif: $(this).data('nama-tarif'),
                    nama_pemeriksaan: $(this).data('nama-pemeriksaan'),
                    nama_pemeriksaan_detail: $(this).data('nama-pemeriksaan-detail'),
                    path: $(this).data('path'),
                    jasa_sarana: $(this).data('jasa-sarana'),
                    jasa_pelayanan: $(this).data('jasa-pelayanan'),
                    bhp: $(this).data('bhp'),
                    biaya_perawatan: $(this).data('biaya-perawatan'),
                    total: $(this).data('total'),
                    kuantitas: $(this).data('kuantitas'),
                    diskon: $(this).data('diskon'),
                    total_keseluruhan: $(this).data('total-keseluruhan'),
                    status_delete: 0,
                };

                if (!isDuplicatePemeriksaan(childPemeriksaan)) {
                    selectedPemeriksaan.push(childPemeriksaan);
                }
            }

            saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);
        }
    });
}

function removeSelectedPemeriksaan(pemeriksaanId) {
    var pemeriksaanToRemoveList = selectedPemeriksaan.filter(item => item.pemeriksaan_id == pemeriksaanId);

    pemeriksaanToRemoveList.forEach(pemeriksaanToRemove => {
        if (pemeriksaanToRemove.status_database) {
            pemeriksaanToRemove.status_delete = 1;
            saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);
        } else {
            selectedPemeriksaan = selectedPemeriksaan.filter(item => item != pemeriksaanToRemove);
        }
    });

    saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);
}

function resetFormPermintaan() {
    selectedPemeriksaan = [];

    $('#table-pemeriksaan input[type="checkbox"]').prop('checked', false);
    $('#table-order tbody').empty();
    $('#table-order tfoot td:eq(1)').text('0');

    // Reset other form elements if needed
    $('#jenis_pemeriksaan, #tujuan_radiologi, #dokter_peminta_id, #prioritas, #pasien_puasa, #pengiriman_hasil').val('').trigger('change');
    $('#rencana_pemeriksaan, #diagnosa, #catatan_permintaan').val('');
    $('#rencana_pemeriksaan, #tanggal_permintaan').val('<?= date('d/m/Y') ?>');
    $('#waktu_permintaan').val('<?= date('H:i:s') ?>');
    $('#data_pemeriksaan').val('[]');
}

function updateDaftarPemeriksaan(statusForm = '') {
    let statusKasir = '{statuskasir}';
    var totalHarga = 0;

    $('#table-order tbody').empty();

    $.each(selectedPemeriksaan, function(index, pemeriksaan) {
        var buttonIcon = (pemeriksaan.status_delete == 1) ? '<i class="fa fa-refresh"></i>' : '<i class="fa fa-trash"></i>';
        var buttonText = (pemeriksaan.status_delete == 1) ? 'Reactive' : 'Hapus';
        var buttonClass = (pemeriksaan.status_delete == 1) ? 'btn-warning' : 'btn-danger';

        var row = `<tr>
            <td class="text-center">${index + 1}</td>
            <td>${pemeriksaan.nama_pemeriksaan}</td>
            <td class="text-right">${$.number(pemeriksaan.total)}</td>`;

        if (statusKasir != 2) {
            row += `<td class="text-center">
                ${pemeriksaan.status_delete == 0 ? `<button type="button" class="btn btn-sm btn-info btn-edit" data-index="${index}" data-toggle="modal" data-target="#modal-edit-pemeriksaan">
                    <i class="fa fa-pencil"></i> Edit
                </button>` : ''}
                <button type="button" class="btn btn-sm ${buttonClass} btn-remove" data-index="${index}" data-status="${pemeriksaan.status_delete}">
                    ${buttonIcon} ${buttonText}
                </button>
                ${pemeriksaan.status_delete == 0 ? `<button type="button" class="btn btn-sm btn-success btn-change" data-index="${index}" data-toggle="modal" data-target="#modal-ubah-kelas-tarif">
                    <i class="fa fa-level-up"></i>
                </button>` : ''}
            </td>`;
        }

        row += `</tr>`;
        totalHarga += parseInt(pemeriksaan.total);
        $('#table-order tbody').append(row);
    });

    $('#table-order tfoot td:eq(1)').text(totalHarga.toLocaleString());

    $('.btn-remove').on('click', function() {
        var indexToEdit = $(this).data('index');
        var pemeriksaanToRemove = selectedPemeriksaan[indexToEdit];

        if (pemeriksaanToRemove.status_database) {
            pemeriksaanToRemove.status_delete = pemeriksaanToRemove.status_delete == 1 ? 0 : 1;
            $('#table-pemeriksaan input[value="' + pemeriksaanToRemove.pemeriksaan_id + '"]').prop('checked', !pemeriksaanToRemove.status_delete);
        } else {
            $('#table-pemeriksaan input[value="' + pemeriksaanToRemove.pemeriksaan_id + '"]').prop('checked', 0);
            selectedPemeriksaan = selectedPemeriksaan.filter(item => item != pemeriksaanToRemove);
        }

        saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);
        updateDaftarPemeriksaan(statusKasir);
    });
}

function isDuplicatePemeriksaan(pemeriksaan) {
    return selectedPemeriksaan.some(item =>
        item.pemeriksaan_id == pemeriksaan.pemeriksaan_id
    );
}

function loadSelectedPemeriksaan(transaksiId) {
    const savedData = localStorage.getItem(`selectedPemeriksaanRadiologi_${transaksiId}`);
    return savedData ? JSON.parse(savedData) : [];
}

function saveSelectedPemeriksaan(transaksiId, data) {
    localStorage.setItem(`selectedPemeriksaanRadiologi_${transaksiId}`, JSON.stringify(data));
}

function loadDataPetugasProsesPemeriksaan(tujuanRadiologi, petugasId = '') {
    $.ajax({
        url: '{site_url}term_radiologi_usg/petugas_proses_pemeriksaan/' + tujuanRadiologi,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if ($('#petugas_pemeriksaan').data('select2')) {
                $('#petugas_pemeriksaan').select2('destroy');
            }
            $("#petugas_pemeriksaan").empty();

            response.petugas.map(function(petugas) {
                var selected = (petugasId != '' && petugas.iduser == petugasId) ? 'selected' : '';
                $("#petugas_pemeriksaan").append('<option value="' + petugas.iduser + '"' + selected + '>' + petugas.nama_petugas + '</option>');
            });

            $("#petugas_pemeriksaan").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataDokterRadiologi(tujuanRadiologi, dokterId = '') {
    $.ajax({
        url: '{site_url}term_radiologi_usg/dokter_radiologi/' + tujuanRadiologi,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if ($('#dokter_radiologi').data('select2')) {
                $('#dokter_radiologi').select2('destroy');
            }
            $("#dokter_radiologi").empty();

            response.dokter.map(function(dokter) {
                var selected = (dokterId != '' && dokter.iddokter == dokterId) || (dokter.iddokter == '' && dokter.status_default == '1') ? 'selected' : '';
                $("#dokter_radiologi").append('<option value="' + dokter.iddokter + '" ' + selected + '>' + dokter.nama_dokter + '</option>');
            });

            $("#dokter_radiologi").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

</script>
