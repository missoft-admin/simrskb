<style>
.gray-background {
    background-color: #f2f2f2 !important;
}

.white-background {
    background-color: #ffffff !important;
}
</style>

<!-- Filter Modal -->
<div class="modal fade in" id="modal-ubah-tarif" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h5 class="block-title">Ubah Tarif Radiologi</h5>
                </div>
            </div>
            <div class="block-content">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="edit-tarif-tipe-radiologi" class="col-xs-12">Tipe</label>
                                <div class="col-xs-12">
                                    <select id="edit-tarif-tipe-radiologi" class="js-select2 form-control" style="width: 100%;" disabled>
                                        <?php foreach (list_variable_ref(245) as $row) { ?>
                                            <option value="<?php echo $row->id; ?>" <?php echo $row->id == $tipe_layanan ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="edit-tarif-head-parent" class="col-xs-12">Head Parent</label>
                                <div class="col-xs-12">
                                    <select id="edit-tarif-head-parent" class="js-select2 form-control" style="width: 100%;">
                                        <option value="0">Semua</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="edit-tarif-sub-parent" class="col-xs-12">Sub Parent</label>
                                <div class="col-xs-12">
                                    <select class="js-select2 form-control" id="edit-tarif-sub-parent" multiple style="width: 100%;"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-xs-12">&nbsp;</label>
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-success" id="btn-filter-tarif" style="width: 100%;">
                                        Filter <i class="fa fa-filter"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <table class="table table-bordered table-striped" id="tarif-radiologi">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Tarif</th>
                        <th>Harga</th>
                        <th>Aksi</th>
                    </tr>
                </thead></table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
let selected = { id: '' }

$(document).ready(function() {
    let kelasId = '{idkelas}';
    let kelompokPasienId = '{idkelompokpasien}';
    let rekananId = '{idrekanan}';
    let tipeRad = '{tipe_layanan}';
    
    getHeadParent(kelasId, kelompokPasienId, rekananId, tipeRad);
    
    $(document).on('click', '.btn-switch', function() {
        var indexToEdit = $(this).data('index');
        var pemeriksaanToEdit = selectedPemeriksaan[indexToEdit];
        fillEditTarifModal(pemeriksaanToEdit);
    });

    $(document).on('click', '#btn-filter-tarif', function() {
        let tipeRadiologi = $("#edit-tarif-tipe-radiologi option:selected").val();
        let headParent = $("#edit-tarif-head-parent option:selected").val();
        let subParent = $("#edit-tarif-sub-parent").val();

        getDaftarTarifRadiologi(kelasId, kelompokPasienId, rekananId, tipeRadiologi, headParent, subParent);
    });

    $(document).on('change', '#edit-tarif-head-parent', function() {
        const headParent = $(this).val();

        if ($(this).val() != '') {
            getSubParent(kelasId, kelompokPasienId, rekananId, tipeRad, headParent);
        }
    });

    $(document).on('click', '.select-tarif', function() {
        selected = { id: $(this).data('radiologi-id') }

        var indexToEdit = $('#modal-ubah-tarif').data('index');
        var pemeriksaanToEdit = selectedPemeriksaan[indexToEdit];

        // Update data pemeriksaan berdasarkan input modal
        pemeriksaanToEdit.radiologi_id = $(this).data('radiologi-id');
        pemeriksaanToEdit.nama_tarif = $(this).data('nama-tarif');
        pemeriksaanToEdit.path = $(this).data('path');
        pemeriksaanToEdit.jasa_sarana = $(this).data('jasa-sarana');
        pemeriksaanToEdit.jasa_pelayanan = $(this).data('jasa-pelayanan');
        pemeriksaanToEdit.bhp = $(this).data('bhp');
        pemeriksaanToEdit.biaya_perawatan = $(this).data('biaya-perawatan');
        pemeriksaanToEdit.total = $(this).data('total');
        pemeriksaanToEdit.total_keseluruhan = (pemeriksaanToEdit.total * pemeriksaanToEdit.kuantitas) - pemeriksaanToEdit.diskon;

        // Simpan kembali data terupdate
        saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);
        updateSelectedButton(selected.id);
        updateDaftarPemeriksaan();
        
        // Tutup modal
        $('#modal-ubah-tarif').modal('hide');
    });
});

function getDaftarTarifRadiologi(kelas, kelompokPasienId, rekananId, tipeRadiologi, headParent, subParent) {
    $('#tarif-radiologi').DataTable().destroy();
    $('#tarif-radiologi').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}term_radiologi_usg/get_tarif_radiologi',
            type: "POST",
            dataType: 'json',
            data: {
                idkelas: kelas,
                idkelompokpasien: kelompokPasienId,
                idrekanan: rekananId,
                idtipe: tipeRadiologi,
                head_parent: headParent,
                sub_parent: JSON.stringify(subParent),
            }
        },
        "columnDefs": [
            {
                "width": "10%",
                "targets": [0],
                "className": "text-center"
            },
            {
                "width": "50%",
                "targets": [1],
                "className": "text-left"
            },
            {
                "width": "10%",
                "targets": [2],
                "className": "text-left"
            },
            {
                "width": "10%",
                "targets": [3],
                "className": "text-center"
            },
        ],
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "rowCallback": function(row, data) {
            let isTarifKelompok = data[4];

            if (isTarifKelompok == 1) {
                $(row).addClass('gray-background');
            } else {
            $(row).addClass('white-background');
            }
        },
        "drawCallback": function(settings) {
            updateSelectedButton(selected.id);
        },
    });
}

function updateSelectedButton(selectedId) {
    $('#tarif-radiologi .select-tarif').each(function() {
        let id = $(this).data('radiologi-id');
        if (selectedId == id) {
            $(this).prop('disabled', true);
            $(this).html('<i class="fa fa-check"></i> TERPILIH');
            $(this).removeClass('btn-primary').addClass('btn-success');
        } else {
            $(this).prop('disabled', false);
            $(this).html('<i class="fa fa-level-down"></i> UBAH TARIF');
            $(this).removeClass('btn-success').addClass('btn-primary');
        }
    });
}

function getHeadParent(kelasId, kelompokPasienId, rekananId, tipeRad) {
    $('#edit-tarif-head-parent').html('');

    $.ajax({
        url: '{site_url}term_radiologi_usg/get_tarif_head_parent_radiologi',
        method: 'POST',
        data: {
            idkelas: kelasId,
            idkelompokpasien: kelompokPasienId,
            idrekanan: rekananId,
            idtipe: tipeRad,
            head_parent: '',
        },
        success: function(data) {
            $('#edit-tarif-head-parent').append('<option value="">Semua</option>');
            $('#edit-tarif-head-parent').append(data);
        }
    });
}

function getSubParent(kelasId, kelompokPasienId, rekananId, tipeRad, headParent) {
    $('#edit-tarif-sub-parent').html('');

    $.ajax({
        url: '{site_url}term_radiologi_usg/get_tarif_head_parent_radiologi',
        method: 'POST',
        data: {
            idkelas: kelasId,
            idkelompokpasien: kelompokPasienId,
            idrekanan: rekananId,
            idtipe: tipeRad,
            head_parent: headParent,
        },
        success: function(data) {
            $('#edit-tarif-sub-parent').append(data);
        }
    });
}

function fillEditTarifModal(pemeriksaan) {
    console.log(pemeriksaan);
    $('#edit-tarif-tipe-radiologi').select2('destroy');
    $('#edit-tarif-tipe-radiologi').val(pemeriksaan.tipe_id);
    $('#edit-tarif-tipe-radiologi').select2();

    // Simpan index pemeriksaan yang akan diupdate
    $('#modal-ubah-tarif').data('index', selectedPemeriksaan.indexOf(pemeriksaan));

    selected.id = pemeriksaan.radiologi_id;
}
</script>
