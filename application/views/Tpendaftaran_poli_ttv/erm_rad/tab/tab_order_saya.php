<h6 class="font-w700 push-5 text-primary">ORDER SAYA</h6> 
<div class="row push-10-t">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group row">
          <label class="col-xs-12" for="">Tanggal Permintaan</label>
          <div class="col-xs-12">
            <div class="input-group date">
              <input class="form-control js-datepicker" id="os-tanggal-permintaan-dari" type="text" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="Tanggal Dari" value="<?= date("01/m/Y"); ?>">
              <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
              <input class="form-control js-datepicker" id="os-tanggal-permintaan-sampai" type="text" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="Tanggal Sampai" value="<?= date("d/m/Y"); ?>">
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group row">
          <label class="col-xs-12" for="">Tanggal Pendaftaran</label>
          <div class="col-xs-12">
            <div class="input-group date">
              <input class="form-control js-datepicker" id="os-tanggal-pendaftaran-dari" type="text" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="Tanggal Dari" value="">
              <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
              <input class="form-control js-datepicker" id="os-tanggal-pendaftaran-sampai" type="text" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="Tanggal Sampai" value="">
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group row">
          <label class="col-xs-12" for="">No. Registrasi</label>
          <div class="col-xs-12">
            <input type="text" class="form-control" id="os-noregistrasi" placeholder="No. Registrasi" value="">
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group row">
          <label class="col-xs-12" for="">Tujuan Klinik</label>
          <div class="col-xs-12">
            <select class="js-select2 form-control" id="os-tujuan-klinik" style="width: 100%;" data-placeholder="Pilih Opsi">
              <option value="0" selected>Semua Tujuan Klinik</option>
                <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                    <option value="<?= $row->id?>"><?= $row->nama?></option>
                <?php }?>
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group row">
          <label class="col-xs-12" for="">Tujuan Dokter</label>
          <div class="col-xs-12">
            <select class="js-select2 form-control" id="os-tujuan-dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
              <option value="0" selected>Semua Tujuan Dokter</option>
              <?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
                <option value="<?=$row->id?>"><?=$row->nama?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group row">
          <label class="col-xs-12" for="">Tujuan Radiologi</label>
          <div class="col-xs-12">
            <select class="js-select2 form-control" id="os-tujuan-radiologi" style="width: 100%;" data-placeholder="Pilih Opsi">
              <option value="0" selected>Semua Tujuan Radiologi</option>
              <?php foreach (get_all('merm_pengaturan_tujuan_radiologi', array('status' => 1)) as $row) { ?>
                <option value="<?=$row->id?>"><?=$row->nama?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group row">
          <label class="col-xs-12" for="">&nbsp;</label>
          <div class="col-xs-12">
            <button type="button" class="btn btn-primary" id="btn-filter-order-saya"><i class="fa fa-filter"></i> Filter</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<table class="table table-striped table-striped table-hover table-bordered" id="table-order-saya" style="width: 100%;">
  <thead>
    <tr>
      <th class="text-center" width="15%">Aksi</th>
      <th class="text-center" width="5%">No. Registrasi</th>
      <th class="text-center" width="5%">Waktu Permintaan</th>
      <th class="text-center" width="5%">Nomor Permintaan</th>
      <th class="text-center" width="5%">Tujuan Dokter & Klinik</th>
      <th class="text-center" width="5%">Dokter Peminta</th>
      <th class="text-center" width="5%">Diagnosa</th>
      <th class="text-center" width="5%">Tujuan Radiologi</th>
      <th class="text-center" width="5%">Prioritas</th>
      <th class="text-center" width="5%">Status Pemeriksaan</th>
      <th class="text-center" width="5%">Dibuat Oleh</th>
    </tr>
  </thead>
  <tbody></tbody>
</table>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '#btn-filter-order-saya', function() {
        let tanggalPermintaanDari = $('#os-tanggal-permintaan-dari').val();
        let tanggalPermintaanSampai = $('#os-tanggal-permintaan-sampai').val();
        let tanggalPendaftaranDari = $('#os-tanggal-pendaftaran-dari').val();
        let tanggalPendaftaranSampai = $('#os-tanggal-pendaftaran-sampai').val();
        let nomorRegistrasi = $('#os-noregistrasi').val();
        let tujuanKlinik = $('#os-tujuan-klinik').val();
        let tujuanDokter = $('#os-tujuan-dokter').val();
        let tujuanRadiologi = $('#os-tujuan-radiologi').val();

        getDaftarOrderSaya(tanggalPermintaanDari, tanggalPermintaanSampai, tanggalPendaftaranDari, tanggalPendaftaranSampai, nomorRegistrasi, tujuanKlinik, tujuanDokter, tujuanRadiologi);
    });
});
</script>
