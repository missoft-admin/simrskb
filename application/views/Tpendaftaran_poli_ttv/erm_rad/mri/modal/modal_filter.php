<!-- Filter Modal -->
<div class="modal fade in" id="modal-filter" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h5 class="block-title">Filter Options</h5>
                </div>
            </div>
            <div class="block-content">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jenis_pemeriksaan" class="col-xs-12">Jenis Pemeriksaan</label>
                                <div class="col-xs-12">
                                    <select id="filter_jenis_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                        <?php foreach (list_variable_ref(246) as $row) { ?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="anatomi_tubuh" class="col-xs-12">Anatomi Tubuh</label>
                                <div class="col-xs-12">
                                    <select id="filter_anatomi_tubuh" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                        <?php foreach (list_variable_ref(248) as $row) { ?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="posisi_pemeriksaan" class="col-xs-12">Posisi Pemeriksaan</label>
                                <div class="col-xs-12">
                                    <select id="filter_posisi_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                        <?php foreach (list_variable_ref(247) as $row) { ?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama_pemeriksaan" class="col-xs-12">Nama Pemeriksaan</label>
                                <div class="col-xs-12">
                                    <input type="text" id="filter_nama_pemeriksaan" class="form-control" style="width: 100%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-filter">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '#btn-filter', function() {
        let kelasId = '{idkelas}';
        let kelompokPasienId = '{idkelompokpasien}';
        let rekananId = '{idrekanan}';
        let tipeRad = '4';
        let jenisPemeriksaan = $("#filter_jenis_pemeriksaan").val();
        let anatomiTubuh = $("#filter_anatomi_tubuh").val();
        let posisiPemeriksaan = $("#filter_posisi_pemeriksaan").val();
        let namaPemeriksaan = $("#filter_nama_pemeriksaan").val();

        getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeRad, jenisPemeriksaan, anatomiTubuh, posisiPemeriksaan, namaPemeriksaan);
    });
});
</script>
