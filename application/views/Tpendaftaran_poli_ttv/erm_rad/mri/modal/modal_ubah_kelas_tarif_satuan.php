<!-- Modal Ubah Kelas Tarif -->
<div class="modal fade" id="modal-ubah-kelas-tarif" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Ubah Kelas Tarif Satuan</h3>
				</div>
                <div class="block-content">
                    <div class="form-group">
                        <label for="edit-kelas">Kelas Tarif</label>
                        <select id="edit-kelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <?php foreach (get_all('mkelas', ['status' => 1]) as $r) { ?>
                            <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="edit-kelas-nama-pemeriksaan">Tarif Pelayanan</label>
                        <input type="text" class="form-control" id="edit-kelas-nama-pemeriksaan" disabled>
                    </div>
                    <div class="form-group">
                        <label for="edit-kelas-total-keseluruhan">Total Harga</label>
                        <input type="text" class="form-control format-number" id="edit-kelas-total-keseluruhan" disabled>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btn-ubah-kelas-tarif" class="btn btn-success" data-dismiss="modal">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                    
                    <input type="hidden" id="edit-kelas-id-tarif" value="">
                    <input type="hidden" id="edit-kelas-jasa-sarana" value="">
                    <input type="hidden" id="edit-kelas-jasa-pelayanan" value="">
                    <input type="hidden" id="edit-kelas-bhp" value="">
                    <input type="hidden" id="edit-kelas-biaya-perawatan" value="">
                    <input type="hidden" id="edit-kelas-total" value="">
                    <input type="hidden" id="edit-kelas-kuantitas" value="">
                    <input type="hidden" id="edit-kelas-diskon" value="">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".format-number").number(true, 0, '.', ',');

    $(document).on("click", ".btn-change", function() {
        var indexToEdit = $(this).data('index');
        var pemeriksaanToEdit = selectedPemeriksaan[indexToEdit];
        fillEditKelasTarifModal(pemeriksaanToEdit);
    });

    $(document).on("change", "#edit-kelas", function() {
        let tarifId = $("#edit-kelas-id-tarif").val();
        let kelasTarif = $("#edit-kelas option:selected").val();
        
        $("#cover-spin").show();
        
        $.ajax({
        url: '{site_url}term_radiologi_mri/get_tarif_pemeriksaan_by_kelas/' + tarifId + '/' + kelasTarif,
            success: function (result) {
                $('#edit-kelas-jasa-sarana').val(result.jasa_sarana);
                $('#edit-kelas-jasa-pelayanan').val(result.jasa_pelayanan);
                $('#edit-kelas-bhp').val(result.bhp);
                $('#edit-kelas-biaya-perawatan').val(result.biaya_perawatan);
                $('#edit-kelas-total').val(result.total);

                let kuantitas = $('#edit-kelas-kuantitas').val();
                let diskon = $('#edit-kelas-diskon').val();
                $('#edit-kelas-total-keseluruhan').val(formatCurrency((result.total * kuantitas) - diskon));

                $("#cover-spin").hide();
            },
            error: function (error) {
                // Handle error if deletion fails
                console.error('Error update:', error);
                alert('Error update. Please try again.');
            }
        });
    });

    $(document).on("click", "#btn-ubah-kelas-tarif", function() {
        var indexToEdit = $('#modal-ubah-kelas-tarif').data('index');
        var pemeriksaanToEdit = selectedPemeriksaan[indexToEdit];

        // Update data pemeriksaan berdasarkan input modal
        pemeriksaanToEdit.kelas = $('#edit-kelas option:selected').val();
        pemeriksaanToEdit.jasa_sarana = $('#edit-kelas-jasa-sarana').val();
        pemeriksaanToEdit.jasa_pelayanan = $('#edit-kelas-jasa-pelayanan').val();
        pemeriksaanToEdit.bhp = $('#edit-kelas-bhp').val();
        pemeriksaanToEdit.biaya_perawatan = $('#edit-kelas-biaya-perawatan').val();
        pemeriksaanToEdit.total = $('#edit-kelas-total').val();
        pemeriksaanToEdit.kuantitas = $('#edit-kelas-kuantitas').val();
        pemeriksaanToEdit.diskon = $('#edit-kelas-diskon').val();
        pemeriksaanToEdit.total_keseluruhan = $('#edit-kelas-total-keseluruhan').val();

        // Simpan kembali data terupdate
        saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);

        // Tutup modal
        $('#modal-ubah-kelas-tarif').modal('hide');

        // Perbarui tabel order
        updateDaftarPemeriksaan();
    });
});

function fillEditKelasTarifModal(pemeriksaan) {
    $('#edit-kelas').select2('destroy');
    $('#edit-kelas').val(pemeriksaan.kelas);
    $('#edit-kelas').select2();

    $('#edit-kelas-id-tarif').val(pemeriksaan.radiologi_id);
    $('#edit-kelas-nama-pemeriksaan').val(pemeriksaan.nama_pemeriksaan);
    $('#edit-kelas-jasa-sarana').val(pemeriksaan.jasa_sarana);
    $('#edit-kelas-jasa-pelayanan').val(pemeriksaan.jasa_pelayanan);
    $('#edit-kelas-bhp').val(pemeriksaan.bhp);
    $('#edit-kelas-biaya-perawatan').val(pemeriksaan.biaya_perawatan);
    $('#edit-kelas-total').val(pemeriksaan.total);
    $('#edit-kelas-kuantitas').val(pemeriksaan.kuantitas);
    $('#edit-kelas-diskon').val(pemeriksaan.diskon);
    $('#edit-kelas-total-keseluruhan').val(formatCurrency(pemeriksaan.total_keseluruhan));

    // Simpan index pemeriksaan yang akan diupdate
    $('#modal-ubah-kelas-tarif').data('index', selectedPemeriksaan.indexOf(pemeriksaan));
}
</script>
