<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	div.dataTables_wrapper {
        margin: 0 auto;
    }
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
   
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='his_pews'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_pews' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_ews">
	<?if ($st_lihat_pews=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="total_skor_pews" value="<?=$total_skor_pews?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
						
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<a href="{site_url}tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_ews/input_pews" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-2 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?
							$list_mresiko=get_all('mpews',array('staktif'=>1));
							$q="SELECT *FROM mpews WHERE id='$mpews_id'";
							$data_master=$this->db->query($q)->row();
						?>
						<div class="block">
							<ul class="nav nav-tabs" data-toggle="tabs">
								<li class="<?=($tab_utama=='1'?'active':'')?>">
									<a href="#tab_utama_1" ><i class="si si-check"></i> Penilaian</a>
								</li>
								
							</ul>
							<input type="hidden" id="tab_utama" name="tab_utama" value="{tab_utama}">
							<div class="block-content tab-content">
								<div class="tab-pane fade fade-left <?=($tab_utama=='1'?'active in':'')?>" id="tab_utama_1">
									<div class="row">
										<?if ($assesmen_id!=''){?>
											<div class="form-group">
												<div class="col-md-6 ">
													<div class="col-md-6 ">
														<label for="mpews_id_text">Metode Penilain</label>
														<select  id="mpews_id_text" disabled class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" <?=($mpews_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
															<?foreach($list_mresiko as $row){?>
															<option value="<?=$row->id?>" <?=($mpews_id == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
															<?}?>
														</select>
													</div>
													<div class="col-md-6 ">
														<label for="example-input-normal">Skor Pengkajian</label>
														<input class="form-control <?=($total_skor_pews!=$total_skor_pews_asal?'edited':'')?> " readonly type="text" id="total_skor_pews"  value="{total_skor_pews}" placeholder="Skor Pengkajian" >
													</div>
												</div>
												<div class="col-md-6 ">
													<div class="col-md-4 ">
														<label for="example-input-normal">Tanggal Pengkajian</label>
														<div class="input-group date">
															<input type="text" class="<?=($tanggal_pengkajian!=$tanggal_pengkajian_asal?'edited':'')?> form-control " data-date-format="dd/mm/yyyy" id="tanggal_pengkajian" placeholder="HH/BB/TTTT"  value="<?= HumanDateShort($tanggal_pengkajian) ?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
													</div>
													<div class="col-md-8 ">
														<label for="example-input-normal">Hasil Pengkajian</label>
														<input class="form-control <?=($nama_hasil_pengkajian!=$nama_hasil_pengkajian_asal?'edited':'')?>" readonly type="text" id="nama_hasil_pengkajian"  value="{nama_hasil_pengkajian}" placeholder="Hasil Pengkajian" >
													</div>
													<div class="col-md-6 " hidden>
														<label for="example-input-normal">Tindakan</label>
														<input class="form-control <?=($nama_tindakan!=$nama_tindakan_asal?'edited':'')?>" readonly type="text" id="nama_tindakan"  value="{nama_tindakan}" placeholder="Tindakan" >
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<div class="col-md-12 ">
														<div class="table-responsive">
															<table class="table table-bordered table-condensed table-header-bg" id="tabel_skrining">
																<thead>
																	<tr>
																		<th width="30%">Skrining</th>
																		<th width="60%">Jawaban</th>
																		<th width="10%" class="text-center">Skor</th>
																	</tr>
																</thead>
																<tbody></tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 ">
													<div class="col-md-12 ">
														<label class="" ><?=$data_master->isi_footer?></h5>
													</div>
												</div>
											</div>
											<?}else{?>
												<div class="form-group">
													<div class="col-md-12 ">
														<div class="col-md-12 ">
															<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
																<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
																<p>Info <a class="alert-link" href="javascript:void(0)"> Klik New Untuk Memulai Penilaian</a>!</p>
															</div>
														</div>
														
													</div>
												</div>
											<?}?>
									</div>
								</div>
							</div>
						</div>
						
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;

$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_pews();
		list_index_history_edit();
	}
	
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
						// { "width": "5%", "targets": [0] },
						// { "width": "20%", "targets": [1] },
						// { "width": "15%", "targets": [3] },
						// { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_pews', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
function load_pews(){
	let mpews_id=$("#mpews_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let versi_edit=$("#versi_edit").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_pews', 
		dataType: "JSON",
		method: "POST",
		data : {
				versi_edit:versi_edit,
				assesmen_id:assesmen_id,
				mpews_id:mpews_id,
				
				},
		success: function(data) {
			$("#tabel_skrining tbody").append(data.opsi);
			$(".nilai").select2();
			// get_skor_pengkajian();
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
}
function disabel_edit(){
	 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".his_filter").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $('.edited').css('background', '#fff2f1');
}
</script>