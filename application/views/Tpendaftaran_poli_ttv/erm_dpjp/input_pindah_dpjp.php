<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	}	
	.text-bold{
        font-weight: bold;
      }
	  table {
		font-family: Verdana, sans-serif;
        font-size: 12px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 13px;
		 
      }
      .content td {
        padding: 10px;
		border: 0px solid #6033FF;
      }
	  .has-error2 {
		border-color: #d26a5c;
	}
	  .select2-selection {
		  border-color: green; /* example */
		}
      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 14px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
		border-bottom:1px solid #000 !important;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_pindah_dpjp' || $menu_kiri=='input_pindah_dpjp_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_pindah_dpjp' || $menu_kiri=='input_pindah_dpjp_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
	
	$disabel_input='';
	$disabel_cetak='disabled';
	$list_dokter=(get_all('mdokter',array('status'=>1)));		
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_pindah_dpjp">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-note"></i> Pernyataan Saat ini</a>
				</li>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()"><i class="fa fa-history"></i> Riwayat Pernyataan</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
					<input type="hidden" readonly id="st_edited" value="{st_edited}">
					<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
					<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
					<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
					<input type="hidden" id="ttd_pernyataan" value="<?=$ttd_pernyataan?>" >		
					<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
					<input type="hidden" id="assesmen_detail_id" value="" >		
					<div class="row">
						<div class="col-md-12">
							<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<h4 class="font-w400 push-5 text-center"><i>{judul_header_eng}</i></h4>
						</div>
					</div>
					<div class="row">
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1'){?>
								<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p>Info <a class="alert-link" href="javascript:void(0)"> Auto Save </a>!</p>
								</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($status_assemen=='1'){?>
										<button class="btn btn-success btn_simpan" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN & KIRIM</button>
										<button class="btn btn-danger"  onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
									<?}?>
									<?if ($status_assemen=='2'){?>
											<a href="{site_url}Tpendaftaran_ranap_erm/tindakan/<?=$pendaftaran_id_ranap?>/erm_dpjp/input_pindah_dpjp" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
									<?}?>
									<?if ($status_assemen=='2'){?>
										<?if ($iddokter_login==$iddokter_awal && $st_jawab=='0'){?>
											  <?php if (UserAccesForm($user_acces_form,array('1991'))){ ?>
											  <button class="btn btn-sm btn-success btn_setuju" onclick="setuju_pindah(<?=$assesmen_id?>)" type="button"><i class="fa fa-check"></i> SETUJU PINDAH</button>
											  <?}?>
											  <?php if (UserAccesForm($user_acces_form,array('1992'))){ ?>
											  <button class="btn btn-sm btn-danger btn_setuju" onclick="tolak_pindah(<?=$assesmen_id?>)" type="button"><i class="fa fa-times"></i> TOLAK PINDAH</button>
											  <?}?>
										<?}?>
										<?if ($iddokter_login==$iddokter_baru && $st_jawab_baru=='0'){?>
											  <?php if (UserAccesForm($user_acces_form,array('1991'))){ ?>
											  <button class="btn btn-sm btn-success btn_setuju" onclick="setuju_terima(<?=$assesmen_id?>)" type="button"><i class="fa fa-check"></i> SETUJU TERIMA</button>
											  <?}?>
											  <?php if (UserAccesForm($user_acces_form,array('1992'))){ ?>
											  <button class="btn btn-sm btn-danger btn_setuju" onclick="tolak_terima(<?=$assesmen_id?>)" type="button"><i class="fa fa-times"></i> TOLAK TERIMA</button>
											  <?}?>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<?}?>
						<div class="form-group" >
							<div class="col-md-12 ">
								<table class="myFormat">
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{paragraf_1_ina}<br><i class="text-muted">{paragraf_1_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{pilih_ttd_ina}<br><i class="text-muted">{pilih_ttd_eng}</i></td>
									</tr>
									<tr>
										<td style="width:50%%" colspan="3">
											<select id="pilih_ttd_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($pilih_ttd_id=='#'?'selected':'')?>>- Pilih yang Bertandatangan -</option>
												<option value="1" <?=($pilih_ttd_id=='1'?'selected':'')?>>Penanggung Jawab</option>
												<option value="2" <?=($pilih_ttd_id=='2'?'selected':'')?>>Pengantar</option>
												<option value="3" <?=($pilih_ttd_id=='3'?'selected':'')?>>Pasien Sendiri</option>
												<option value="4" <?=($pilih_ttd_id=='4'?'selected':'')?>>Lainnya</option>
												
											</select>
										</td>
										<td style="width:50%" colspan="3" class="text-bold">
											<?if ($st_input_pindah_dpjp=='1'){?>
											<?if ($assesmen_id==''){?>
											<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_awal()" type="button"><i class="si si-doc"></i> New </button>
											<?}?>
											<?}?>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group" style="margin-top:-10px!important">
							<div class="col-md-12 ">
								<table>
									<tr>
										<td style="width:13%" class="text-bold">{nama_ttd_ina} : <br><i class="text-muted">{nama_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<input id="nama_ttd"  class="form-control pull-15-r auto_blur"  value="{nama_ttd}"  type="text">
										</td>
										<td style="width:13%" class="text-bold push-15-l">{ttl_ttd_ina} : <br><i class="text-muted">{ttl_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<div class="input-group date">
												<input id="ttl_ttd" class="js-time-modal js-datepicker form-control auto_blur"  type="text" data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" value="<?=($ttl_ttd?HumanDateShort($ttl_ttd):'')?>" required>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</td>
										<td style="width:13%" class="text-bold push-15-l">{umur_ttd_ina} : <br><i class="text-muted">{umur_ttd_eng}</i></td>
										<td style="width:21%" class="">
											<input id="umur_ttd" class="form-control auto_blur"  value="{umur_ttd}"  type="text">
										</td>
									</tr>
									<tr>
										<td style="width:13%" class="text-bold">{alamat_ttd_ina} : <br><i class="text-muted">{alamat_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<input id="alamat_ttd" class="form-control pull-15-r auto_blur"   value="{alamat_ttd}"  type="text">
										</td>
										<td style="width:13%" class="text-bold push-15-l">{provinsi_ttd_ina} : <br><i class="text-muted">{provinsi_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<select id="provinsi_id_ttd" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="">Pilih Opsi</option>
												<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
												<option value="<?= $a->id; ?>" <?=($provinsi_id_ttd == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
											<?php endforeach; ?>
											</select>
										</td>
										<td style="width:13%" class="text-bold push-15-l">{kab_ttd_ina} : <br><i class="text-muted">{kab_ttd_eng}</i></td>
										<td style="width:21%" class="">
											<select id="kabupaten_id_ttd" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
												<option value="">Pilih Opsi</<option>
												<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id_ttd]) as $r):?>
												<option value="<?= $r->id; ?>" <?=($kabupaten_id_ttd == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
												<?php endforeach; ?>
											</select>
										</td>
									</tr>
									<tr>
										<td style="width:13%" class="text-bold">{kec_ttd_ina} : <br><i class="text-muted">{kec_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<select id="kecamatan_id_ttd" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="">Pilih Opsi</<option>
												<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id_ttd]) as $r):?>
												<option value="<?= $r->id; ?>"<?=($kecamatan_id_ttd == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
												<?php endforeach; ?>
											</select>
										</td>
										<td style="width:13%" class="text-bold push-15-l">{desa_ttd_ina} : <br><i class="text-muted">{desa_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<select id="kelurahan_id_ttd" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="">Pilih Opsi</<option>
												<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id_ttd]) as $r):?>
												<option value="<?= $r->id; ?>"<?=($kelurahan_id_ttd == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
												<?php endforeach; ?>
											</select>
										</td>
										<td style="width:13%" class="text-bold push-15-l ">{kodepos_ttd_ina} : <br><i class="text-muted">{kodepos_ttd_eng}</i></td>
										<td style="width:21%" class="">
											<input id="kodepos_ttd"  class="form-control pull-15-r auto_blur" value="{kodepos_ttd}"  type="text">
											
										</td>
									</tr>
									<tr>
										<td style="width:13%" class="text-bold">{rw_ttd_ina} : <br><i class="text-muted">{rw_ttd_eng}</i></td>
										<td style="width:20%" class="">
											<input id="rw_ttd" class="form-control pull-15-r auto_blur"  value="{rw_ttd}"  type="text">
										</td>
										<td style="width:13%" class="text-bold push-15-l  id="kodepos_ttd" ">{rt_ttd_ina} : <br><i class="text-muted">{rt_ttd_ina}</i></td>
										<td style="width:20%" class="">
											<input id="rt_ttd" class="form-control pull-15-r auto_blur" value="{rt_ttd}"  type="text">
										</td>
										<td style="width:13%" class="text-bold push-15-l">{hubungan_ina} : <br><i class="text-muted">{hubungan_eng}</i></td>
										<td style="width:21%" class="">
											<select id="hubungan_ttd"  class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="0" <?=($hubungan_ttd == '0' ? 'selected="selected"' : '')?>>Diri Sendiri</option>
												<?foreach(list_variable_ref(9) as $row){?>
												<option value="<?=$row->id?>" <?=($hubungan_ttd == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
											</select>
										</td>
									</tr>
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{paragraf_2_ina}<br><i class="text-muted">{paragraf_2_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%" colspan="3" class="text-bold">{dari_dokter_ina}<br><i class="text-muted">{dari_dokter_eng}</i></td>
										<td style="width:100%" colspan="3" class="text-bold">{jadi_dokter_ina}<br><i class="text-muted">{jadi_dokter_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%" colspan="3" class="">
											<select id="iddokter_awal" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>" <?=($r->id==$iddokter_awal?'selected':'')?>><?=$r->nama?></option>
												<?}?>
											</select>
										</td>
										<td style="width:100%" colspan="3" class="">
											<select id="iddokter_baru" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>" <?=($r->id==$iddokter_baru?'selected':'')?>><?=$r->nama?></option>
												<?}?>
											</select>
										</td>
									</tr>
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{paragraf_3_ina}<br><i class="text-muted">{paragraf_3_eng}</i></td>
									</tr>
									<tr>
										<td style="width:13%" class="text-bold">{noreg_ina}<br><i class="text-muted">{noreg_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {nopendaftaran}</td>
										<td style="width:13%" class="text-bold">{nomedrec_ina}<br><i class="text-muted">{nomedrec_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {no_medrec}</td>
										<td style="width:13%" class="text-bold">{nama_pasien_ina}<br><i class="text-muted">{nama_pasien_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {namapasien}</td>
									</tr>
									<tr>
										<td style="width:13%" class="text-bold">{ttl_pasien_ina}<br><i class="text-muted">{ttl_pasien_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: <?=($tanggal_lahir?HumanDateShort($tanggal_lahir):'')?></td>
										<td style="width:13%" class="text-bold">{umur_pasien_ina}<br><i class="text-muted">{umur_pasien_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {umurtahun} Tahun {umurbulan} Bulan {umurhari} Hari</td>
										<td style="width:13%" class="text-bold">{jk_ina}<br><i class="text-muted">{jk_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: {jk}</td>
									</tr>
									<?if ($st_ranap=='1'){?>
									<tr>
										<td style="width:13%" class="text-bold">{ruangan_ina}<br><i class="text-muted">{ruangan_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: <?=($nama_ruangan)?></td>
										<td style="width:13%" class="text-bold">{kelas_ina}<br><i class="text-muted">{kelas_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: <?=($nama_kelas?$nama_kelas:'')?></td>
										<td style="width:13%" class="text-bold">{bed_ina}<br><i class="text-muted">{bed_eng}</i></td>
										<td style="width:20%" class="font-s12 ">: <?=($nama_bed?$nama_bed:'')?></td>
									</tr>
									<?}?>
									<tr>
										<td style="width:100%" colspan="6" class="text-bold">{paragraf_4_ina}<br><i class="text-muted">{paragraf_4_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%" colspan="2" class="text-center text-bold">{yg_pernyataan_ina}<br><i class="text-muted">{yg_pernyataan_eng}</i></td>
										<td style="width:100%" colspan="2" class="text-center"></td>
										<td style="width:100%" colspan="2" class="text-center text-bold">{saksi_rs_ina}<br><i class="text-muted">{saksi_rs_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%" colspan="2" class="text-center">
											<?if ($ttd_pernyataan){?>
											<div class="img-container fx-img-rotate-r text-center">
												<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_pernyataan?>" alt="" title="">
												<div class="img-options">
													<div class="img-options-content">
														<div class="btn-group btn-group-sm">
															<a class="btn btn-default" onclick="modal_ttd()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
															<a class="btn btn-default btn-danger" onclick="hapus_ttd('ttd_pernyataan')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
														</div>
													</div>
												</div>
											</div>
										<?}else{?>
											<button class="btn btn-sm btn-success" onclick="modal_ttd()" id="btn_ttd_lm" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
										<?}?>
										</td>
										<td style="width:100%" colspan="2" class="text-center"></td>
										<td style="width:100%" colspan="2" class="text-center">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id?$petugas_id:$login_ppa_id)?>" alt="" title="">
										</td>
									</tr>
									<tr>
										<td style="width:100%" colspan="2" class="text-center text-bold">( {nama_ttd} )</td>
										<td style="width:100%" colspan="2" class="text-center"></td>
										<td style="width:100%" colspan="2" class="text-center text-bold">
										( {nama_petugas} ) <br>
										<button class="btn btn-xs btn-success" onclick="show_modal_ttd_petugas()"type="button"><i class="fa fa-pencil"></i> Tanda Tangan</button>
										</td>
									</tr>
									<tr>
										<td style="width:100%" colspan="2" class="text-center text-bold">{ttd_dpjp_lama_ina}<br><i class="text-muted">{ttd_dpjp_lama_eng}</i></td>
										<td style="width:100%" colspan="2" class="text-center"></td>
										<td style="width:100%" colspan="2" class="text-center text-bold">{ttd_dpjp_baru_ina}<br><i class="text-muted">{ttd_dpjp_baru_eng}</i></td>
									</tr>
									<tr>
										<td style="width:100%" colspan="2" class="text-center text-bold">
											<?if ($st_jawab=='1'){?>
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd_dokter/<?=($iddokter_awal?$iddokter_awal:$login_ppa_id)?>" alt="" title="">
											<?}else{?>
												<button class="btn btn-sm btn-danger"  id="btn_ttd_lm" type="button">BELUM MENJAWAB</button>
											<?}?>
										</td>
										<td style="width:100%" colspan="2" class="text-center"></td>
										<td style="width:100%" colspan="2" class="text-center text-bold">
											<?if ($st_jawab_baru=='1'){?>
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd_dokter/<?=($iddokter_baru?$iddokter_baru:$login_ppa_id)?>" alt="" title="">
											<?}else{?>
												<button class="btn btn-sm btn-danger"  id="btn_ttd_lm" type="button">BELUM MENJAWAB</button>
											<?}?>
										</td>
									</tr>
									<tr>
										<td style="width:100%" colspan="2" class="text-center text-bold">({nama_dokter_awal})</td>
										<td style="width:100%" colspan="2" class="text-center"></td>
										<td style="width:100%" colspan="2" class="text-center text-bold">({nama_dokter_baru})</td>
									</tr>
								</table>
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									<?if ($status_assemen=='1'){?>
										<button class="btn btn-success btn_simpan" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i>  SIMPAN & KIRIM</button>
										<button class="btn btn-danger"  onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
									<?}?>
									<?if ($status_assemen=='2'){?>
										<?if ($iddokter_login==$iddokter_awal && $st_jawab=='0'){?>
											  <?php if (UserAccesForm($user_acces_form,array('1991'))){ ?>
											  <button class="btn btn-sm btn-success btn_setuju" onclick="setuju_pindah(<?=$assesmen_id?>)" type="button"><i class="fa fa-check"></i> SETUJU PINDAH</button>
											  <?}?>
											  <?php if (UserAccesForm($user_acces_form,array('1992'))){ ?>
											  <button class="btn btn-sm btn-danger btn_setuju" onclick="tolak_pindah(<?=$assesmen_id?>)" type="button"><i class="fa fa-times"></i> TOLAK PINDAH</button>
											  <?}?>
										<?}?>
										<?if ($iddokter_login==$iddokter_baru && $st_jawab_baru=='0'){?>
											  <?php if (UserAccesForm($user_acces_form,array('1991'))){ ?>
											  <button class="btn btn-sm btn-success btn_setuju" onclick="setuju_terima(<?=$assesmen_id?>)" type="button"><i class="fa fa-check"></i> SETUJU TERIMA</button>
											  <?}?>
											  <?php if (UserAccesForm($user_acces_form,array('1992'))){ ?>
											  <button class="btn btn-sm btn-danger btn_setuju" onclick="tolak_terima(<?=$assesmen_id?>)" type="button"><i class="fa fa-times"></i> TOLAK TERIMA</button>
											  <?}?>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<?}?>
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?if ($st_ranap=='1'){?>
													<option value="<?=$pendaftaran_id_ranap?>" >Dirawat Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
												<?}else{?>
													<option value="<?=$pendaftaran_id?>" >Poliklinik Saat Ini <?=$nopendaftaran?></option>
													<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id ,'idpasien'=>$idpasien)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
													<?}?>

												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
					
				</div>
			</div>
			
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select  id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select  id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan  </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
							<input type="hidden" readonly id="nama_field_ttd" value="">
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_petugas" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Masukan NIP</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<label for="example-input-normal">Masukan NIP</label>
										<input class="form-control " type="text" id="nip"  value="" placeholder="Nip Petugas" >
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" onclick="set_petugas()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
var status_assemen=$("#status_assemen").val();
$(document).ready(function() {
	$(".btn_close_left").click();
	validate_simpan();
	disabel_edit();
});
function save_edit_assesmen(){
	let jml_edit=$("#jml_edit").val();
	let keterangan_edit=$("#keterangan_edit").val();
	let alasan_id=$("#alasan_id_edit").val();
	let assesmen_detail_id=$("#assesmen_detail_id").val();
	$("#st_sedang_edit").val(1);
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_edit").modal('hide');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/save_edit_assesmen_pindah_dpjp', 
		dataType: "JSON",
		method: "POST",
		data : {
				jml_edit:jml_edit,
				assesmen_id:assesmen_detail_id,
				alasan_id:alasan_id,
				keterangan_edit:keterangan_edit,
				idpasien:$("#idpasien").val(),
				pendaftaran_id:$("#pendaftaran_id").val(),
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Gagal!",
					text: "Edit.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				let pendaftaran_id_ranap=data.pendaftaran_id_ranap;
				let pendaftaran_id=data.pendaftaran_id;
				let st_ranap=data.st_ranap;
				$("#cover-spin").show();
				if (st_ranap=='1'){
				window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id_ranap+"/erm_dpjp/input_pindah_dpjp'); ?>";
					
				}else{
					
				window.location.href = "<?php echo site_url('tpendaftaran_ranap_erm/tindakan/"+pendaftaran_id+"/erm_dpjp/input_pindah_dpjp_rj'); ?>";
				}
				
				
			}
		}
	});
}
function hapus_record_assesmen(){
	let id=$("#assesmen_detail_id").val();
	let keterangan_hapus=$("#keterangan_hapus").val();
	let alasan_id=$("#alasan_id").val();
	
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_hapus").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Pernyataan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_record_assesmen_pindah_dpjp', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					keterangan_hapus:keterangan_hapus,
					alasan_id:alasan_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					list_history_pengkajian();
					$("#modal_hapus").modal('hide');
				}
			}
		});
	});

}
function list_history_pengkajian(){
	let assesmen_id=$("#assesmen_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let mppa_id=$("#mppa_id").val();
	let iddokter=$("#iddokter").val();
	let idrawat_ranap=$("#idrawat_ranap").val();
	let notransaksi=$("#notransaksi").val();
	let tanggal_input_1=$("#tanggal_input_1").val();
	let tanggal_input_2=$("#tanggal_input_2").val();
	let idtipe=$("#idtipe").val();
	let idruang=$("#idruang").val();
	let idbed=$("#idbed").val();
	let idkelas=$("#idkelas").val();
	let st_ranap=$("#st_ranap").val();
	$('#index_history_kajian').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_history_kajian').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					{  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,5,6,7] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [1,2,3,5] },
					 { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/list_history_pengkajian_pindah_dpjp', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						iddokter:iddokter,
						mppa_id:mppa_id,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						idrawat_ranap:idrawat_ranap,
						idtipe:idtipe,
						idruang:idruang,
						idbed:idbed,
						idkelas:idkelas,
						st_ranap:st_ranap,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Pernyataan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			
			simpan_assesmen();
		});		
	
}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Pernyataan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/batal_assesmen_pindah_dpjp', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function hapus_ttd(nama_field){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpendaftaran_ranap_erm/hapus_ttd/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					nama_field:nama_field,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function modal_ttd(){
	$("#nama_field_ttd").val('ttd_pernyataan');
	$("#modal_ttd").modal('show');
}
function show_modal_ttd_petugas(){
	$("#modal_ttd_petugas").modal('show');
}
$(document).on("click","#btn_save_ttd",function(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var nama_field=$("#nama_field_ttd").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/save_ttd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				nama_field:nama_field,
		  },success: function(data) {
				location.reload();
			}
		});
	
});
function create_awal(){
	if ($("#pilih_ttd_id").val()=='#'){
		swal({
			title: "Gagal",
			text: "Silahkan isi Yang Bertandatangan",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	let pilih_ttd_id=$("#pilih_ttd_id").val();
	
	let template=$("#pilih_ttd_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Pernyataan "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_pindah_dpjp', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
					pilih_ttd_id:pilih_ttd_id,
				   },
			success: function(data) {
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});
}

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$("#ttl_ttd").blur(function(){
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/getUmur', 
		dataType: "JSON",
		method: "POST",
		data : {
				ttl_ttd:$("#ttl_ttd").val(),
				
			   },
		success: function(data) {
			$("#umur_ttd").val(data);
		}
	});
	
});
$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
function set_petugas(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_ranap_erm/getNik', 
		dataType: "JSON",
		method: "POST",
		data : {
				nip:$("#nip").val(),
				assesmen_id:$("#assesmen_id").val(),
			   },
		success: function(data) {
			if (data){
				$.toaster({priority : 'success', title : 'Succes!', message : ' Mengganti Pernyataan'});
				location.reload();
			}else{
				$("#cover-spin").hide();
				swal({
					title: "Gagal",
					text: "Petugas Tidak ditemukan",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				return false;
			}
			
		}
	});
}
function edit_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_edit").modal('show');
	document.getElementById("modal_edit").style.zIndex = "1201";
	$("#alasan_id_edit").select2({
		dropdownParent: $("#modal_edit")
	  });
}
function validate_simpan(){
	let st_simpan=true;
	if ($("#nama_ttd").val()==''){
		st_simpan=false;
	}
	if ($("#iddokter_baru").val()=='#'){
		st_simpan=false;
	}
	if ($("#ttd_pernyataan").val()==''){
		// st_simpan=false;
	}
	if ($("#iddokter_awal").val()==$("#iddokter_baru").val()){
		st_simpan=false;
	}
	
	if (st_simpan==true){
		$(".btn_simpan").prop('disabled',false);
	}else{
		$(".btn_simpan").prop('disabled',true);
		
	}
}
function hapus_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_hapus").modal('show');
	document.getElementById("modal_hapus").style.zIndex = "1201";
	$("#alasan_id").select2({
		dropdownParent: $("#modal_hapus")
	  });
	// document.getElementById("alasan_id").style.zIndex = "1202";
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".btn_setuju").prop("disabled", false);
		
	}
}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Pernyataan Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/create_with_template_pindah_dpjp', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function simpan_assesmen(){
	
if (status_assemen !='2'){
	

	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		console.log('SIMPAN');
		
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/save_pindah_dpjp', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					status_assemen:$("#status_assemen").val(),
					st_edited:$("#st_edited").val(),
					jml_edit:$("#jml_edit").val(),
					nama_ttd : $("#nama_ttd").val(),
					ttl_ttd : $("#ttl_ttd").val(),
					umur_ttd : $("#umur_ttd").val(),
					alamat_ttd : $("#alamat_ttd").val(),
					provinsi_id_ttd : $("#provinsi_id_ttd").val(),
					kabupaten_id_ttd : $("#kabupaten_id_ttd").val(),
					kecamatan_id_ttd : $("#kecamatan_id_ttd").val(),
					kelurahan_id_ttd : $("#kelurahan_id_ttd").val(),
					rw_ttd : $("#rw_ttd").val(),
					rt_ttd : $("#rt_ttd").val(),
					kodepos_ttd : $("#kodepos_ttd").val(),
					hubungan_ttd : $("#hubungan_ttd").val(),
					iddokter_awal : $("#iddokter_awal").val(),
					iddokter_baru : $("#iddokter_baru").val(),

				},
			success: function(data) {
					validate_simpan();
				
						console.log(data);
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
				}else{
					if (data.status_assemen=='1'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}else{
						location.reload();		
					}
				}
			}
		});
		}
	}
}

// pilih_ttd_id
$("#pilih_ttd_id").change(function(){
	if ($("#assesmen_id").val()!=''){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		
		let idpasien=$("#idpasien").val();
		let pilih_ttd_id=$("#pilih_ttd_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let st_ranap=$("#st_ranap").val();
		
		let template=$("#pilih_ttd_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mengganti Pernyataan Menjadi "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_ranap_erm/edit_pilih_pindah_dpjp', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						pilih_ttd_id:pilih_ttd_id,
						assesmen_id:assesmen_id,
						st_ranap:st_ranap,
					   },
				success: function(data) {
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Mengganti Pernyataan'});
					location.reload();
				}
			});
		});
	}
});
$("#provinsi_id_ttd").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kabupaten_id_ttd").empty();
			$("#kecamatan_id_ttd").empty();
			$("#kelurahan_id_ttd").empty();
			$("#kodepos_ttd").val('');
			$("#kabupaten_id_ttd").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kabupaten_id_ttd").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			// alert(kode);
			$("#kecamatan_id_ttd").empty();
			$("#kelurahan_id_ttd").empty();
			$("#kodepos_ttd").val('');
			$("#kecamatan_id_ttd").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kecamatan_id_ttd").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kelurahan_id_ttd").empty();
			$("#kodepos_ttd").val('');
			$("#kelurahan_id_ttd").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kelurahan_id_ttd").change(function() {
	var kode = $(this).val();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kodepos_ttd").val(data.kodepos);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
function setuju_pindah($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan setuju Pindah DPJP ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpindah_dpjp/setuju_pindah', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					  window.history.back();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
function tolak_pindah($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menolak Pindah DPJP ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpindah_dpjp/tolak_pindah', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					  window.history.back();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
function setuju_terima($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerima DPJP Baru?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpindah_dpjp/setuju_terima', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					  window.history.back();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
function tolak_terima($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerima DPJP Baru?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpindah_dpjp/tolak_terima', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					  window.history.back();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
</script>