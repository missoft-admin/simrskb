<style>
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		
</style>

<?if ($menu_kiri=='input_dpjp'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_dpjp' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>

<?
		// echo $tanggal_pernyataan;
	if ($tanggal_input){
		$tgltransaksi=HumanDateShort($tanggal_input);
		$waktutransaksi=HumanTime($tanggal_input);
	}else{
		$tgltransaksi=date('d-m-Y');
		$waktutransaksi=date('H:i:s');
	}
	
	$disabel_input='';
	$disabel_cetak='disabled';
	
			$list_dokter=(get_all('mdokter',array('status'=>1)));		
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_dpjp">
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-note"></i> Pencatatan Saat Ini</a>
				</li>
				<li class="">
					<a href="#tab_2" onclick="load_dpjp_list_history()"><i class="fa fa-history"></i> Riwayat Pencatatan DPJP</a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<input type="hidden" id="history_id" value="" >		
					<input type="hidden" id="iddokter_asal" value="" >		
					<input type="hidden" id="tipe_dpjp_asal" value="" >		
					<input type="hidden" id="st_replace_utama" value="0" >		
					<input type="hidden" id="st_dokter_utama" value="" >		
					<div class="row">
						<div class="col-md-12">
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
								<h4 class="font-w400 push-5 text-center"><i>{judul_header_eng}</i></h4>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p>Info <a class="alert-link" href="javascript:void(0)"> Auto Save </a>!</p>
								</div>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									
								</div>
								
							</div>
						</div>
						<?if ($st_input_dpjp=='1'){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="idtipe">Tanggal</label>
									<div class="input-group date">
										<input tabindex="1" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tgltransaksi" placeholder="HH/BB/TTTT" name="tgltransaksi" value="<?= $tgltransaksi ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								<div class="col-md-2 ">
									<label for="idtipe">Waktu</label>
									<div class="input-group date">
										<input tabindex="2" type="text"  class="time-datepicker form-control " id="waktutransaksi" name="waktutransaksi" value="<?= $waktutransaksi ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
									
								</div>
								
								<div class="col-md-5 ">
									<label for="totalkeseluruhan">Nama Profesional Pemberi Asuhan (PPA)</label>
										<div class="input-group">
											<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
											<span class="input-group-addon"><i class="fa fa-user"></i></span>
										</div>
								</div>
							</div>
							
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								<div class="col-md-4 ">
									<label for="diagnosa">Diagnosa</label>
									<input id="diagnosa"  class="form-control " type="text" value="">
								</div>
								<div class="col-md-4 ">
									<label for="iddokter">DPJP</label>
									<select id="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Dokter -</option>
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="tipe_dpjp">Tipe DPJP</label>
									<select id="tipe_dpjp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Tipe -</option>
										<?foreach(list_variable_ref(254) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group" >
							<div class="col-md-12 ">
								<div class="col-md-2 ">
									<label for="tanggaldari">Tanggal Awal</label>
									<div class="input-group date">
										<input id="tanggaldari" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" value="" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
								<div class="col-md-2 ">
									<label for="tanggalsampai">Tanggal Akhir</label>
									<div class="input-group date">
										<input id="tanggalsampai" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" value="" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="keterangan">Keterangan</label>
									<input id="keterangan"  class="form-control " type="text" value="">
								</div>
								<div class="col-md-2 ">
									<label for="keterangan">&nbsp;</label>
									<div class="input-group">
										<span class="input-group-btn">
											<button onclick="add_dpjp()" title="Simpan DPJP" class="btn btn-primary" type="button"><i class="fa fa-save"></i></button>
											<button onclick="clear_dpjp()" title="Clear DPJP" class="btn  btn-danger" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
						<?}?>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered table-header-bg" id="index_dpjp">
											<thead>
												<tr>
													<th width="5%" class="text-center">NO</th>
													<th width="8%" class="text-center">TANGGAL INPUT</th>
													<th width="17%" class="text-center">DIAGNOSA</th>
													<th width="15%" class="text-center">DPJP</th>
													<th width="8%" class="text-center">TIPE</th>
													<th width="10%" class="text-center">TANGGAL AWAL</th>
													<th width="10%" class="text-center">TANGGAL AKHIR</th>
													<th width="20%" class="text-center">KETERANGAN</th>
													<th width="12%" class="text-center">ACTION</th>
												</tr>
											</thead>
											<tbody></tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>	
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">HISTORY {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Dirawat</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Kelular</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_keluar_1"  placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_keluar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="col-md-6">
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruangan_filter">Ruangan</label>
										<div class="col-md-8">
											<select id="idruangan_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach(get_all('mruangan',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="iddokter_filter">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="iddokter_filter">Kelompok Pasien</label>
										<div class="col-md-8">
											<select id="idkelompokpasien_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua -</option>
												<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="load_dpjp_list_history()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="js-table-sections table table-hover" id="index_history_dpjp">
									<thead>
										<tr>
											<th width="10%">Tipe Pelayanan</th>
											<th width="10%">No Registrasi</th>
											<th width="10%">Tanggal Dirawat</th>
											<th width="10%">Tanggal Keluar</th>
											<th width="10%">Ruang Perawatan</th>
											<th width="10%">DPJP Awal</th>
											<th width="10%">Kelompok Pasien</th>
											<th width="10%">Action</th>
										</tr>
										
									</thead>
									<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					
					<?php echo form_close() ?>
					
					
				</div>
			</div>
			
		</div>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal" id="modal_selesai" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Tanggal Selesai</h3>
				</div>
				
				<div class="block-content">
					<div class="row push-10" >
						
						<div class="form-group" >
							<label class="col-md-3 control-label" for="tanggal">Tanggal Selesai</label>
							<div class="col-md-6 ">
								<div class="input-group date">
									<input id="tanggal_selesai" type="text" class="js-time-modal js-datepicker form-control " data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" value="{tanggal_selesai}" required>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-danger" type="button" onclick="simpan_selesai()" id="btn_selesai"><i class="fa fa-save"></i> Proses</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_selesai_utama" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pengganti DPJP Utama</h3>
				</div>
				
				<div class="block-content">
					<div class="row push-10" >						
						<div class="form-group" >
							<label class="col-md-3 control-label" for="tanggal">DPJP Utama Asal</label>
							<div class="col-md-9 ">
								<select id="dpjp_utama_asal" disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Dokter -</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
					</div>
					<div class="row push-10" >						
						<div class="form-group" >
							<label class="col-md-3 control-label" for="tanggal">Tanggal Selesai</label>
							<div class="col-md-6 ">
								<div class="input-group date">
									<input id="tanggal_selesai_utama" type="text" class="js-time-modal js-datepicker form-control " data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" value="{tanggal_selesai}" required>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row push-10" >						
						<div class="form-group" >
							<label class="col-md-3 control-label" for="tanggal">DPJP Utama Pengganti</label>
							<div class="col-md-9 ">
								<select id="dpjp_utama_baru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Dokter -</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
					</div>
					<div class="row push-10" >						
						<div class="form-group" >
							<label class="col-md-3 control-label" for="tanggal">Diagnosa</label>
							<div class="col-md-9 ">
								<input id="diagnosa_baru" class="form-control " type="text" value="">
							</div>
						</div>
					</div>
					<div class="row push-10" >						
						<div class="form-group" >
							<label class="col-md-3 control-label" for="tanggal">Keterangan</label>
							<div class="col-md-9 ">
								<input id="keterangan_baru" class="form-control " type="text" value="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-danger" type="button" onclick="simpan_selesai_utama()" id="btn_selesai"><i class="fa fa-save"></i> Proses</button>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
var tableRanap;
var detailRowsRanap = [];
$(document).ready(function() {

	clear_dpjp();
	load_dpjp_list();
});
function selesai_dpjp($history_id,$tipe_dpjp){
	$("#history_id").val($history_id);
	$("#tipe_dpjp_asal").val($tipe_dpjp);
	$("#modal_selesai").modal('show');
	
}
function selesai_dpjp_utama($history_id,$iddokter){
	$("#history_id").val($history_id);
	$("#iddokter_asal").val($iddokter);
	$("#dpjp_utama_asal").val($iddokter).trigger('change');
	
	$("#modal_selesai_utama").modal('show');
	
}
function simpan_selesai(){
	if (($("#tanggal_selesai").val())==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Tanggal Selesai",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_selesai_dpjp/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				history_id : $("#history_id").val(),
				tanggal_selesai : $("#tanggal_selesai").val(),
				
				
		  },success: function(data) {
				  $("#cover-spin").hide();
			  if (data==false){
				  swal({
						title: "Gagal",
						text: "Gagal Updaet",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
			  }else{
				  $("#modal_selesai").modal('hide');
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  clear_dpjp();
				  load_dpjp_list();
			  }
			}
		});
}
function simpan_selesai_utama(){
	if (($("#dpjp_utama_baru").val())=='#'){
		swal({
			title: "Gagal",
			text: "Silahkan isi DPJP Baru",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#dpjp_utama_baru").val() == $("#dpjp_utama_asal").val())){
		swal({
			title: "Gagal",
			text: "Silahkan isi DPJP Berbeda",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#tanggal_selesai_utama").val())==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Tanggal Selesai",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (($("#diagnosa_baru").val())==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi diagnosa",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_selesai_utama/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				history_id : $("#history_id").val(),
				tanggal_selesai : $("#tanggal_selesai_utama").val(),
				dpjp_utama_asal : $("#dpjp_utama_asal").val(),
				dpjp_utama_baru : $("#dpjp_utama_baru").val(),
				idrawatinap : $("#pendaftaran_id_ranap").val(),
				diagnosa : $("#diagnosa_baru").val(),
				keterangan : $("#keterangan_baru").val(),
		  },success: function(data) {
				  $("#cover-spin").hide();
			  if (data==false){
				  swal({
						title: "Gagal",
						text: "Gagal Updaet",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
			  }else{
				  $("#modal_selesai_utama").modal('hide');
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  clear_dpjp();
				  location.reload();
			  }
			}
		});
}
function clear_dpjp(){
	$("#tipe_dpjp").val(null).trigger('change.select2');
	$("#iddokter").val(null).trigger('change.select2');
	$("#keterangan").val('');
	$("#diagnosa").val('');
	$("#tanggaldari").val('');
	$("#tanggalsampai").val('');
	$("#tanggalsampai").prop('disabled',true);
	$("#st_replace_utama").val('0');
	$("#iddokter_asal").val('');
	$("#tipe_dpjp_asal").val('');
}

function edit_dpjp(id,idtipe){
	clear_dpjp();
	if (idtipe=='1'){
		$("#tipe_dpjp").prop('disabled',true);
		$("#iddokter").prop('disabled',true);
	}else{
		$("#tipe_dpjp").prop('disabled',true);
		$("#iddokter").prop('disabled',false);
	}
	$("#history_id").val(id);
	$("#cover-spin").show();
	$.ajax({
	  url: '{site_url}Tpendaftaran_ranap_erm/get_edit_dpjp/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			id : id,
			idtipe : idtipe,
			
	  },success: function(data) {
		  $("#cover-spin").hide();
			$("#history_id").val(data.id),
			$("#waktutransaksi").val(data.waktutransaksi),
			$("#tgltransaksi").val(data.tgltransaksi),
			$("#tipe_dpjp").val(data.tipe_dpjp).trigger('change.select2');
			$("#iddokter").val(data.iddokter).trigger('change.select2');
			$("#tanggaldari").val(data.tanggaldari);
			$("#tanggalsampai").val(data.tanggalsampai);
			$("#keterangan").val(data.keterangan);
			$("#diagnosa").val(data.diagnosa);
			$("#cover-spin").hide();
			$("#diagnosa").focus().select();
		}
	});
}
function add_dpjp(){
	if ($("#history_id").val()==''){
		if ($("#st_dokter_utama").val()=='1' && $("#tipe_dpjp").val()=='1'){
			swal({
				title: "Gagal",
				text: "DPJP Utama Sudah Ada",
				type: "error",
				timer: 1500,
				showConfirmButton: false
			});
			return false;
		}
	}
	if (($("#diagnosa").val())==''){
		swal({
			title: "Gagal",
			text: "Silahkan isi Diagnosa",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if ($("#iddokter").val()==null || $("#iddokter").val()==''){
		swal({
			title: "Gagal",
			text: "Silahkan Tentukan DPJP",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	
	
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_ranap_erm/simpan_dpjp/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				history_id : $("#history_id").val(),
				tgltransaksi : $("#tgltransaksi").val(),
				waktutransaksi : $("#waktutransaksi").val(),
				idrawatinap : $("#pendaftaran_id_ranap").val(),
				diagnosa : $("#diagnosa").val(),
				iddokter : $("#iddokter").val(),
				tipe_dpjp : $("#tipe_dpjp").val(),
				tanggaldari : $("#tanggaldari").val(),
				tanggalsampai : $("#tanggalsampai").val(),
				keterangan : $("#keterangan").val(),
				
		  },success: function(data) {
				  $("#cover-spin").hide();
			  if (data==false){
				  swal({
						title: "Gagal",
						text: "Data Duplicate",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
			  }else{
				  $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
				  clear_dpjp();
				  load_dpjp_list();
			  }
			}
		});
}

function load_dpjp_list(){
	$("#cover-spin").show();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		$('#index_dpjp tbody').empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/load_dpjp_list', 
			dataType: "json",
			type: "POST",
			data: {
				pendaftaran_id_ranap: pendaftaran_id_ranap,
			},
			success: function(data) {
				$('#st_dokter_utama').val(data.st_dokter_utama);
				$('#index_dpjp tbody').append(data.tabel);
				$("#cover-spin").hide();
				
			}
		});
}

function load_dpjp_list_history(){
	$('#index_history_dpjp').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	tableRanap = $('#index_history_dpjp').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					// {  className: "text-center", targets:[1,2,3,5,6,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpendaftaran_ranap_erm/load_dpjp_list_history', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idpasien:$("#idpasien").val(),
						notransaksi:$("#notransaksi").val(),
						tgl_daftar_1:$("#tgl_daftar").val(),
						tgl_daftar_2:$("#tgl_daftar_2").val(),
						tanggal_keluar_1:$("#tanggal_keluar_1").val(),
						tanggal_keluar_2:$("#tanggal_keluar_2").val(),
						idruangan:$("#idruangan_filter").val(),
						iddokter:$("#iddokter_filter").val(),
						idkelompokpasien:$("#idkelompokpasien_filter").val(),
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
}

$('#index_history_dpjp tbody').on('click', '.details-control', function() {
	
        var tr = $(this).closest('tr');
        var row = tableRanap.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRowsRanap);

        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();

            // Remove from the 'open' array
            detailRowsRanap.splice(idx, 1);
        } else {
            var idpendaftaran = tr.find("td:eq(0) span").data('idpendaftaran');
			$("#cover-spin").show();
            $.ajax({
                url: "{site_url}Tpendaftaran_ranap_erm/load_dpjp_list_detail",
               type: "POST" ,
				dataType: 'json',
				data : {
						pendaftaran_id_ranap:idpendaftaran,
						
					   },
                success: function(data) {
					$("#cover-spin").hide();
                    tr.addClass('details');
                    row.child((data.tabel)).show();

                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRowsRanap.push(tr.attr('id'));
                    }
                }
            });

        }
    });
function hapus_dpjp(id,idtipe){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Data DPJP ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_ranap_erm/hapus_dpjp', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					idtipe:idtipe,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				load_dpjp_list();
			}
		});
	});

}

</script>