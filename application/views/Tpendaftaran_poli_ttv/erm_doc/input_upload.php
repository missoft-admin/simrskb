<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
</style>
<style>
.dropzone {
height: 150px;
min-height: 0px !important;
}   
</style>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_upload_rj' || $menu_kiri=='input_upload_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
	
		$waktudaftar=date('H:i:s');
		$tanggaldaftar=date('d-m-Y');
		
	
	?>
		<div class="block ">
		<div class="block-content ">
			<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
			<div class="row push-10-t">
				<div class="col-md-12 push-10-t">
					<div class="form-group">
						<div class="col-md-6 col-xs-12">
							<div class="col-md-3 col-xs-6">
								<div class="form-material input-group date">
									<input  type="text" class="js-datepicker form-control " disabled data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
									<label for="tanggaldaftar">Tanggal </label>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="col-md-3 col-xs-6">
								<div class="form-material input-group">
									<input  type="text"  class="time-datepicker form-control " disabled id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
									<label for="waktupendaftaran">Waktu</label>
									<span class="input-group-addon"><i class="si si-clock"></i></span>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="col-md-12 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
				</div>
						
			</div>	
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12">
							<h4 class="font-w700 text-center text-primary">DOCUMENT</h4>
						</div>
					</div>
				
					<div class="form-group">
						<div class="col-sm-12">
						<div class="col-sm-12">
							<table class="table table-bordered" id="index_doc">
								<thead>
									<tr>
										<th class="text-center" style="width:5%">No</th>
										<th class="text-center" style="width:50%">KETERANGAN</th>
										<th class="text-center" style="width:15%">FILE</th>
										<th class="text-center" style="width:20%">CREATED BY</th>
										<th class="text-center" style="width:10%">Action</th>
									</tr>
									<tr>
										<th class="text-center">#<input type="hidden" class="form-control" id="dokument_id" width="100%" value=""></th>
										<th class="text-center"><input type="text" style="width: 100%"  class="form-control" id="keterangan" placeholder="KETERANGAN" width="100%" value=""></th>
										
										<th class="text-center"><span class="text-danger">File setelah simpan</span></th>
										<th class="text-center"></th>
										<th class="text-center">
											<button type="button" class="btn btn-sm btn-primary" onclick="simpan_doc()" title="Masukan Item"><i class="fa fa-save"></i></button>
											<button type="button" class="btn btn-sm btn-warning" onclick="clear_doc()"title="Batalkan"><i class="fa fa-refresh"></i></button>
										</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
					
		</div>
		</div>
	<!-- END Music -->
</div>

<div class="modal in" id="modal_file" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Document File's </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12">
								<form action="{base_url}Tdokumen/upload_file" enctype="multipart/form-data" class="dropzone" id="image-upload">
									<input type="hidden" value="" id="dok_id" name="dokument_id" readonly>
									<input type="hidden" value="" id="pasien_id" name="idpasien" value="<?=$idpasien?>" readonly>
									<div>
									  <h5>Upload Document</h5>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php echo form_open_multipart('mfasilitas_bed/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								 <table class="table table-bordered" id="list_file">
									<thead>
										<tr>
											<th width="40%" class="text-center">File</th>
											<th width="25%" class="text-center">Size</th>
											<th width="25%" class="text-center">User</th>
											<th width="10%" class="text-center">X</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var idpasien=$("#idpasien").val();

	$(document).ready(function() {
		load_index();
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.csv,.tsv,.ppt,.pptx,.pages,.odt,.rtf",
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  load_index();
		  myDropzone.removeFile(file);
		  
		});
	   Dropzone.options.dropzone = {
			acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.csv,.tsv,.ppt,.pptx,.pages,.odt,.rtf",     
		};
	});
	function file_upload(dokument_id){
		$("#dok_id").val(dokument_id);
		$("#pasien_id").val($("#idpasien").val());
		refresh_image();
		$("#modal_file").modal('show');
	}
	function hapus_file(dokument_id){
		$.ajax({
			url: '{site_url}Tdokumen/hapus_file', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:dokument_id,
				},
			success: function(data) {
				refresh_image();
				load_index();
			}
		});
	}
	function clear_doc(){
		$("#keterangan").val('');
		$("#dokument_id").val('');
		$(".btn_edit").attr('disabled',false);
	}
	function edit_dokumen(id){
		$("#dokument_id").val(id);
		$.ajax({
			url: '{site_url}Tdokumen/edit_dokumen', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				},
			success: function(data) {
				$(".btn_edit").attr('disabled',true);
				$("#keterangan").val(data.keterangan);
			}
		});
		
	}
	function refresh_image(){
		// alert($("#mfasilitas_id").val());
		var id=$("#dok_id").val();
		$('#list_file tbody').empty();
		// alert(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tdokumen/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				$("#cover-spin").hide();
				if (data.detail!=''){
					$('#list_file tbody').empty();
					$("#list_file tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	function simpan_doc(){
		if ($("#keterangan").val()==''){
			$("#cover-spin").hide();
			swal({
				title: "Gagal",
				text: "Silahkan Isi Keterangan",
				type: "error",
				timer: 1500,
				showConfirmButton: false
			});
			return false;
		}
		$.ajax({
			url: '{site_url}Tdokumen/simpan_doc', 
			dataType: "JSON",
			method: "POST",
			data : {
					dokument_id:$("#dokument_id").val(),
					keterangan:$("#keterangan").val(),
					idpasien:$("#idpasien").val(),
					pendaftaran_id:$("#pendaftaran_id").val(),
					pendaftaran_id_ranap:$("#pendaftaran_id_ranap").val(),
					
				},
			success: function(data) {
			
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
				}else{
					if (data){
						load_index();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						clear_doc();
					}else{
						location.reload();		
						clear_doc();
					}
				}
			}
		});
	}
	function hapus_dokumen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Dokumen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tdokumen/hapus_dokumen', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					load_index();
				}
			});
		});

	}
	function load_index(){
		// alert('sini');
		$('#index_doc').DataTable().destroy();	
		let idpasien=$("#idpasien").val();
		
		table = $('#index_doc').DataTable({
				autoWidth: false,
				searching: false,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				"columnDefs": [
						// { "width": "3%", "targets": 0,  className: "text-right" },
						{ "targets": [0,2,3,4],  className: "text-center" },
						// { "width": "12%", "targets": [8,7,11,10],  className: "text-center" },
						// { "width": "15%", "targets": 12,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}Tdokumen/load_index', 
					type: "POST" ,
					dataType: 'json',
					data : {
							idpasien:idpasien,
							
							
						   }
				}
			});
		$("#cover-spin").hide();
	}
</script>