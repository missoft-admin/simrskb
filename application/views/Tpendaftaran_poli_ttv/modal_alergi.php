
<div class="modal in" id="modal_header_his_alergi_pasien" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">HISTORY ALERGI</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="form-horizontal" id="filter-datatable">
                            <div class="col-md-12 push-10">
                                <div class="table-responsive">
                                    <table class="table" id="index_header_alergi">
                                        <thead>
                                            <tr>
                                                <th width="10%">No</th>
                                                <th width="15%">Jenis Alergi</th>
                                                <th width="20%">Detail Alergi</th>
                                                <th width="30%">Reaksi</th>
                                                <th width="15%">User Input</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-mini block-content-full border-t">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

function lihat_his_alergi_pasien(idpasien) {
    $("#modal_header_his_alergi_pasien").modal('show');

    $('#index_header_alergi').DataTable().destroy();
    $("#cover-spin").show();
    // alert(ruangan_id);
    table = $('#index_header_alergi').DataTable({
        autoWidth: false,
        searching: true,
        serverSide: true,
        "processing": false,
        "order": [],
        "pageLength": 10,
        "ordering": false,

        ajax: {
            url: '{site_url}tpendaftaran_poli_ttv/load_alergi_his_header',
            type: "POST",
            dataType: 'json',
            data: {
                idpasien: idpasien,

            }
        },
        "drawCallback": function(settings) {
            $("#cover-spin").hide();
        }
    });

}

</script>
