<style type="text/css">
.tab-content {
    background-color: #f9f9f9;
}
</style>

<?php if ($menu_kiri == 'laboratorium_split') { ?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri == 'laboratorium_split' ? 'block' : 'none') ?>;">
    <button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
    <div class="block animated fadeIn push-10-t" data-category="erm_laboratorium">
      <div class="block">
          <ul class="nav nav-tabs" data-toggle="tabs">
              <li class="active">
                  <a href="#"><i class="fa fa-list"></i> Order &nbsp; <span class="badge badge-success pull-right">Split</span></a>
              </li>
          </ul>
          <div class="block-content tab-content">
              <div class="tab-pane fade fade-left active in" id="">
                <?= form_open('term_laboratorium_umum/simpan_split_pemeriksaan','class="form-horizontal push-10-t" id="form-work"') ?>
                
                <div class="row">
                  <div class="col-md-12 text-right">
                      <button type="submit" name="btn_submit" data-type="form-submit" class="btn btn-success"><i class="fa fa-save"></i> Split Order</button>
                      <a href="{site_url}term_laboratorium_umum_permintaan" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <h4 class="font-w700 push-5 text-center text-primary"><?= strip_tags($pengaturan_form['label_judul']); ?></h4>
                  <h4 class="push-5 text-center"><i><?= strip_tags($pengaturan_form['label_judul_eng']); ?></i></h4>
                </div>

                <br><br>

                <div class="row push-10-t">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Jenis Pemeriksaan</label>
                          <div class="col-xs-12">
                            <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= ($status_form == '' ? 'disabled' : ''); ?>>
                              <?php foreach(list_variable_ref(86) as $row){ ?>
                                <option value="<?= $row->id; ?>" <?= ($jenis_pemeriksaan == '' && $row->st_default == '1' ? 'selected' : ''); ?> <?= ($row->id == $jenis_pemeriksaan ? 'selected' : ''); ?>><?= $row->nama; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Rencana Pemeriksaan</label>
                          <div class="col-xs-12">
                            <input type="text" name="rencana_pemeriksaan" id="rencana_pemeriksaan" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" value="{rencana_pemeriksaan}" <?= ($status_form == '' ? 'disabled' : ''); ?>>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Tanggal & Waktu Pembuatan</label>
                          <div class="col-xs-12">
                            <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_waktu_pembuatan}" <?= ($status_form == '' ? 'disabled' : ''); ?>>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Nama Profesional Pemberi Asuhan</label>
                          <div class="col-xs-12">
                            <input type="text" class="form-control" disabled value="{login_nip_ppa} - {login_nama_ppa}">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-input-pemeriksaan">
                  <hr>

                  <div class="row push-10-t">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_tujuan_laboratorium']); ?></label>
                            <div class="col-xs-12">
                              <select name="tujuan_laboratorium" id="tujuan_laboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= '' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach ($list_tujuan_laboratorium as $r) { ?>
                                  <option value="<?= $r->id; ?>" <?= $row->id == $tujuan_laboratorium ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_dokter_peminta_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <select name="dokter_peminta_id" id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= '' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                                  <option value="<?= $r->id; ?>" <?= $r->id == $login_pegawai_id || $r->id == $dpjp ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_diagnosa']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="diagnosa" id="diagnosa" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_diagnosa']); ?>" value="{diagnosa}" <?= '' == $status_form ? 'disabled' : ''; ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="catatan_permintaan" id="catatan_permintaan" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?>" value="{catatan_permintaan}" <?= '' == $status_form ? 'disabled' : ''; ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_waktu_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_permintaan" id="tanggal_permintaan" placeholder="HH/BB/TTTT" value="{tanggal_permintaan}" <?= '' == $status_form ? 'disabled' : ''; ?>>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_permintaan" id="waktu_permintaan" value="{waktu_permintaan}" <?= '' == $status_form ? 'disabled' : ''; ?>>
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_prioritas']); ?></label>
                            <div class="col-xs-12">
                              <select name="prioritas" id="prioritas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= '' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(85) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $prioritas && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $prioritas ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pasien_puasa']); ?></label>
                            <div class="col-xs-12">
                              <select name="pasien_puasa" id="pasien_puasa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= '' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(87) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $pasien_puasa && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $pasien_puasa ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pengiriman_hasil']); ?></label>
                            <div class="col-xs-12">
                              <select name="pengiriman_hasil" id="pengiriman_hasil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= '' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(88) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $pengiriman_hasil && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <hr>

                  <div class="row">
                    <div class="col-md-12">
                      <h6 class="font-w700 push-5 text-primary">DAFTAR ORDER</h6>
                      <table class="table table-striped table-striped table-hover table-bordered" id="table-order">
                        <thead>
                          <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Pemeriksaan</th>
                            <th class="text-center">Harga</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                          <tr>
                            <td class="text-right" colspan="3">Total</td>
                            <td class="text-right"></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>

                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="{pendaftaran_id}">
                <input type="hidden" id="transaksi_id" name="transaksi_id" value="{transaksi_id}">
                <input type="hidden" id="pasien_id" name="pasien_id" value="{idpasien}">
                <input type="hidden" id="dokter_perujuk_id" name="dokter_perujuk_id" value="{dpjp}">
                <input type="hidden" id="data_pemeriksaan" name="data_pemeriksaan" value="">
                <input type="hidden" id="selectedOrderIds" name="selected_order_ids" value="">
                <input type="hidden" id="status_form" name="status_form" value="{status_form}">
                <input type="hidden" id="ppa_id" name="ppa_id" value="{login_ppa_id}">
                <input type="hidden" id="form_submit" name="form_submit" value="">
                <?= form_close() ?>
              </div>
          </div>
      </div>
    </div>
</div>
<? } ?>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
  var selectedPemeriksaan = <?= json_encode($selectedPemeriksaan); ?>;

  jQuery(function() {
    BaseTableDatatables.init();
    getDaftarOrder();
  });

  $(document).ready(function() {
      $("#cover-spin").hide();

      $('button[name="btn_submit"]').on('click', function (event) {
          event.preventDefault();

          $('#form_submit').val($(this).data('type'));
          
          swal({
            title: "Konfirmasi Split Order Pemeriksaan",
            text: "Apakah Anda yakin akan melakukan split permintaan ini? Permintaan Anda akan dikirim kepada laboratorium tujuan. Tekan Ya untuk melanjutkan.",
            type : "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
          }).then((willSubmit) => {
              if (willSubmit) {
                $("#cover-spin").show();
                submitForm();
              }
          });
      });
  });

  function submitForm() {
      let selectedOrderIds = $('.order-checkbox:checked').map(function() {
          return $(this).val();
      }).get();

      $('#selectedOrderIds').val(JSON.stringify(selectedOrderIds));
      $('#form-work').submit();
  }

  function getDaftarOrder() {
      let totalHarga = 0;
      $('#table-order tbody').empty();

      $.each(selectedPemeriksaan, function(index, pemeriksaan) {
          let row = `<tr>
              <td class="text-center">
                <div class="checkbox">
                  <label>
                      <input type="checkbox" class="order-checkbox" value="${pemeriksaan.id}">
                  </label>
                </div>
              </td>
              <td class="text-center">${index + 1}</td>
              <td>${pemeriksaan.nama_pemeriksaan}</td>
              <td class="text-right">${$.number(pemeriksaan.total)}</td>
          </tr>`;

          totalHarga += parseInt(pemeriksaan.total);
          $('#table-order tbody').append(row);
      });

      $('#table-order tfoot td:eq(1)').text(totalHarga.toLocaleString());
  }
</script>