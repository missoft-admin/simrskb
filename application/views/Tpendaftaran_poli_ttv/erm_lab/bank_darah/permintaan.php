<style type="text/css">
.auto_blur {
    background-color: #d0f3df;
}

.tab-content {
    background-color: #f9f9f9;
}

.gray-background {
  background-color: #f2f2f2 !important;
}

.white-background {
  background-color: #ffffff !important;
}

.modal {
    z-index: 1201;
}

.select2-container {
    z-index: 9999 !important;
}
</style>

<?php if ($menu_kiri == 'bank_darah_permintaan') { ?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri == 'bank_darah_permintaan' ? 'block' : 'none') ?>;">
    <button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
    <div class="block animated fadeIn push-10-t" data-category="erm_laboratorium">
      <div class="block">
          <ul class="nav nav-tabs" data-toggle="tabs">
              <li class="active">
                  <a href="#tab_order_baru"><i class="fa fa-list"></i> Order &nbsp; <span class="badge badge-success pull-right"><?= $label_form; ?></span></a>
              </li>
              <li class="">
                  <a href="#tab_order_saya" ><i class="fa fa-list"></i> Order Saya</a>
              </li>
              <li class="">
                  <a href="#tab_riwayat_order" ><i class="fa fa-list"></i> Riwayat Order</a>
              </li>
          </ul>
          <div class="block-content tab-content">
              <div class="tab-pane fade fade-left active in" id="tab_order_baru">
                <?= form_open('term_laboratorium_bank_darah/simpan_permintaan','class="form-horizontal push-10-t" id="form-work"') ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <?php if ($status_form == '') { ?>
                        <button class="btn btn-sm btn-primary" type="button" onclick="buatDraftPermintaan()"><i class="si si-doc"></i> New</button>
                      <?php } else if ($status_form == 'draft') { ?>
                        <button type="submit" class="btn btn-sm btn-success" name="btn_submit" data-type="form-submit"><i class="fa fa-send"></i> Kirim Order</button>
                        <button class="btn btn-sm btn-primary" id="form-reset" type="reset"><i class="fa fa-refresh"></i> Reset</button>
                        <button class="btn btn-sm btn-danger" type="button" onclick="batalDraftPermintaan()"><i class="fa fa-times"></i> Batal</button>
                      <?php } else if ($status_form == 'input_permintaan') { ?>
                        <button type="submit" class="btn btn-warning" name="btn_submit" data-type="form-submit-only"><i class="fa fa-save"></i> Simpan Permintaan</button>
                        <button type="submit" class="btn btn-success" name="btn_submit" data-type="form-submit-and-process"><i class="fa fa-check-circle"></i> Simpan & Proses Transaksi</button>
                        <a href="{site_url}term_laboratorium_bank_darah/print_bukti_permintaan_laboratorium/<?php $transaksi_id; ?>" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Dokumen</a>
                      <?php } else if ($status_form == 'edit_permintaan') { ?>
                        <button type="submit" class="btn btn-sm btn-success" name="btn_submit" data-type="form-submit"><i class="fa fa-send"></i> Update Order</button>
                      <?php } ?>
                      
                      <?php if ($status_form != '') { ?>
                        <a href="{site_url}term_laboratorium_bank_darah_permintaan" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                      <?php } ?>
                    </div>
                  </div>

                  <hr>
                  
                  <div class="row">
                    <h4 class="font-w700 push-5 text-center text-primary"><?= strip_tags($pengaturan_form['label_judul']); ?></h4>
                    <h4 class="push-5 text-center"><i><?= strip_tags($pengaturan_form['label_judul_eng']); ?></i></h4>
                  </div>

                  <br><br>

                  <div class="row push-10-t">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Jenis Pemeriksaan</label>
                            <div class="col-xs-12">
                              <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= ($status_form == 'lihat_permintaan' ? 'disabled' : ''); ?>>
                                <?php foreach(list_variable_ref(86) as $row){ ?>
                                  <option value="<?= $row->id; ?>" <?= ($jenis_pemeriksaan == '' && $row->st_default == '1' ? 'selected' : ''); ?> <?= ($row->id == $jenis_pemeriksaan ? 'selected' : ''); ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Rencana Pemeriksaan</label>
                            <div class="col-xs-12">
                              <input type="text" name="rencana_pemeriksaan" id="rencana_pemeriksaan" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" value="{rencana_pemeriksaan}" <?= ($status_form == 'lihat_permintaan' ? 'disabled' : ''); ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Tanggal & Waktu Pembuatan</label>
                            <div class="col-xs-12">
                              <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_waktu_pembuatan}" <?= ($status_form == 'lihat_permintaan' ? 'disabled' : ''); ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Nama Profesional Pemberi Asuhan</label>
                            <div class="col-xs-12">
                              <input type="text" class="form-control" disabled value="{login_nip_ppa} - {login_nama_ppa}">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <?php if ($status_form != '') { ?>
                  <div class="form-input-pemeriksaan">
                    <hr>

                    <div class="row push-10-t">
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_tujuan_laboratorium']); ?></label>
                              <div class="col-xs-12">
                                <select name="tujuan_laboratorium" id="tujuan_laboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                  <?php foreach ($list_tujuan_laboratorium as $r) { ?>
                                    <option value="<?= $r->id; ?>" <?= $r->id == $tujuan_laboratorium ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_dokter_peminta_pemeriksaan']); ?></label>
                              <div class="col-xs-12">
                                <select name="dokter_peminta_id" id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                  <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                                    <option value="<?= $r->id; ?>" <?= $r->id == $login_pegawai_id || $r->id == $dpjp ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_diagnosa']); ?></label>
                              <div class="col-xs-12">
                                <input type="text" name="diagnosa" id="diagnosa" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_diagnosa']); ?>" value="{diagnosa}" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?></label>
                              <div class="col-xs-12">
                                <input type="text" name="catatan_permintaan" id="catatan_permintaan" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?>" value="{catatan_permintaan}" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_waktu_pemeriksaan']); ?></label>
                              <div class="col-xs-12">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="input-group date">
                                      <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_permintaan" id="tanggal_permintaan" placeholder="HH/BB/TTTT" value="{tanggal_permintaan}" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="input-group">
                                      <input type="text" class="time-datepicker form-control" name="waktu_permintaan" id="waktu_permintaan" value="{waktu_permintaan}" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                      <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_prioritas']); ?></label>
                              <div class="col-xs-12">
                                <select name="prioritas" id="prioritas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                  <?php foreach (list_variable_ref(85) as $row) { ?>
                                    <option value="<?= $row->id; ?>" <?= '' == $prioritas && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $prioritas ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_indikasi_transfusi']); ?></label>
                              <div class="col-xs-12">
                                <input type="text" name="indikasi_transfusi" id="indikasi_transfusi" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_indikasi_transfusi']); ?>" value="{indikasi_transfusi}" <?php echo 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_hb']); ?></label>
                              <div class="col-xs-12">
                                <input type="text" name="hb" id="hb" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_hb']); ?>" value="{hb}" <?php echo 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_riwayat_transfusi_sebelumnya']); ?></label>
                              <div class="col-xs-12">
                                <input type="text" name="riwayat_transfusi_sebelumnya" id="riwayat_transfusi_sebelumnya" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_riwayat_transfusi_sebelumnya']); ?>" value="{riwayat_transfusi_sebelumnya}" <?php echo 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_riwayat_kehamilan_sebelumnya']); ?></label>
                              <div class="col-xs-12">
                                <input type="text" name="riwayat_kehamilan_sebelumnya" id="riwayat_kehamilan_sebelumnya" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_riwayat_kehamilan_sebelumnya']); ?>" value="{riwayat_kehamilan_sebelumnya}" <?php echo 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group row">
                              <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_keterangan_lainnya']); ?></label>
                              <div class="col-xs-12">
                                <input type="text" name="keterangan_lainnya" id="keterangan_lainnya" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_keterangan_lainnya']); ?>" value="{keterangan_lainnya}" <?php echo 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <hr>

                    <div class="row">
                      <?php if ('lihat_permintaan' != $status_form) { ?>
                      <div class="col-md-6">
                        <h6 class="font-w700 push-5 text-primary">DAFTAR PEMERIKSAAN</h6>
                        <div class="table-header text-right" style="margin-bottom: 25px;">
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-filter">
                                <i class="fa fa-filter"></i> Filter
                            </button>
                        </div>
                        <table class="table table-striped table-striped table-hover table-bordered" id="table-pemeriksaan">
                          <thead>
                            <tr>
                              <th class="text-center">#</th>
                              <th class="text-center">No</th>
                              <th class="text-center">Nama Pemeriksaan</th>
                              <th class="text-center">Harga</th>
                            </tr>
                          </thead>
                          <tbody></tbody>
                        </table>
                        <hr>
                        <p>Keterangan : <?= strip_tags($pengaturan_form['label_catatan']); ?></p>
                      </div>
                      <?php } ?>
                      <div class="<?= 'lihat_permintaan' == $status_form ? 'col-md-12' : 'col-md-6'; ?>">
                        <h6 class="font-w700 push-5 text-primary">DAFTAR ORDER</h6>
                        <table class="table table-striped table-striped table-hover table-bordered" id="table-order">
                          <thead>
                            <tr>
                              <th class="text-center">No</th>
                              <th class="text-center">Nama Pemeriksaan</th>
                              <th class="text-center">Harga</th>
                              <?php if ('lihat_permintaan' != $status_form) { ?>
                              <th class="text-center">Aksi</th>
                              <?php } ?>
                            </tr>
                          </thead>
                          <tbody></tbody>
                          <tfoot>
                            <tr>
                              <td class="text-right" colspan="2">Total</td>
                              <td class="text-right"></td>
                              <?php if ('lihat_permintaan' != $status_form) { ?>
                              <td></td>
                              <?php } ?>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                  
                  <input type="hidden" id="ref_asal_rujukan" name="ref_asal_rujukan" value="{ref_asal_rujukan}">
                  <input type="hidden" id="asal_rujukan_status" name="asal_rujukan_status" value="{asal_rujukan_status}">
                  <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="{pendaftaran_id}">
                  <input type="hidden" id="transaksi_id" name="transaksi_id" value="{transaksi_id}">
                  <input type="hidden" id="pasien_id" name="pasien_id" value="{idpasien}">
                  <input type="hidden" id="dokter_perujuk_id" name="dokter_perujuk_id" value="{dpjp}">
                  <input type="hidden" id="data_pemeriksaan" name="data_pemeriksaan" value="">
                  <input type="hidden" id="status_form" name="status_form" value="{status_form}">
                  <input type="hidden" id="ppa_id" name="ppa_id" value="{login_ppa_id}">
                  <input type="hidden" id="form_submit" name="form_submit" value="">
                <?= form_close() ?>
              </div>

              <div class="tab-pane fade fade-left " id="tab_order_saya">
                <?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/tab/tab_order_saya'); ?>
              </div>
              
              <div class="tab-pane fade fade-left " id="tab_riwayat_order">
                <?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/tab/tab_riwayat_order'); ?>
              </div>
          </div>
      </div>
    </div>
</div>
<? } ?>

<?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/modal/modal_filter'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/services_permintaan'); ?>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
      $("#cover-spin").hide();

      $(".time-datepicker").datetimepicker({
          format: "HH:mm:ss"
      });

      $('#table-pemeriksaan').on('click', '.parent-checkbox', function() {
          let isChecked = $(this).prop('checked');
          let isParent = $(this).hasClass('parent-checkbox');
          let path = $(this).data('path');

          updateChildCheckboxes(path, isChecked);
          updateSelectedPemeriksaan(isParent, isChecked, path);
          updateDaftarPemeriksaan();
      });

      $('#table-pemeriksaan').on('click', '.child-checkbox', function() {
          let isChecked = $(this).prop('checked');
          let isParent = $(this).hasClass('parent-checkbox');
          let path = $(this).data('path');

          updateParentCheckbox(path);
          updateSelectedPemeriksaan(isParent, isChecked, path);
          updateDaftarPemeriksaan();
      });

      $("#form-work input").on('blur', function() {
          updateDraftPermintaan();
      });

      $("#form-work select").on('change', function() {
          updateDraftPermintaan();
      });

      $('#form-reset').on('click', function () {
          resetFormPermintaan();
      });

      $('button[name="btn_submit"]').on('click', function (event) {
          event.preventDefault();

          $('#form_submit').val($(this).data('type'));

          if ($('#diagnosa').val().trim() == '') {
              swal({
                  title: "Peringatan",
                  text: "Diagnosa tidak boleh kosong!",
                  type: "warning",
                  confirmButtonColor: "#34a263",
                  confirmButtonText: "OK",
              });
              return;
          }
          
          swal({
            title: "Konfirmasi Order Pemeriksaan",
            text: "Apakah Anda yakin akan mengirim permintaan ini? Permintaan Anda akan dikirim kepada laboratorium tujuan. Tekan Ya untuk melanjutkan.",
            type : "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
          }).then((willSubmit) => {
              if (willSubmit) {
                $("#cover-spin").show();
                $('#data_pemeriksaan').val(JSON.stringify(selectedPemeriksaan));
                $('#form-work').submit();
      
                localStorage.removeItem(`selectedPemeriksaanLaboratoriumPermintaan_${transaksiId}`);
              }
          });
      });
  });

  function getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeLab, statusPaket, headParent, subParent) {
      $('#table-pemeriksaan').DataTable().destroy();
      $('#table-pemeriksaan').DataTable({
          "bSort": false,
          "autoWidth": false,
          "pageLength": 10,
          "ordering": true,
          "processing": true,
          "serverSide": true,
          "order": [],
          "ajax": {
              url: '{site_url}term_laboratorium_bank_darah/get_daftar_pemeriksaan_laboratorium',
              type: "POST",
              dataType: 'json',
              data: {
                  idkelas: kelasId,
                  idkelompokpasien: kelompokPasienId,
                  idrekanan: rekananId,
                  idtipe: tipeLab,
                  idpaket: statusPaket,
                  idtindakan: headParent,
                  idsubheader: JSON.stringify(subParent),
              }
          },
          "columnDefs": [
              {
                  "width": "10%",
                  "targets": [0, 1, 2, 3],
                  "className": "text-center"
              },
          ],
          "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
          "rowCallback": function(row, data) {
              let isTarifKelompok = data[4];

              if (isTarifKelompok == 1) {
                  $(row).addClass('gray-background');
              } else {
                $(row).addClass('white-background');
              }
          },
          "drawCallback": function(settings) {
            $('#table-pemeriksaan input[type="checkbox"]').each(function() {
                let checkboxValue = $(this).val();
                let isChecked = selectedPemeriksaan.some(function(item) {
                    return item.pemeriksaan_id == checkboxValue;
                });

                $(this).prop('checked', isChecked);
            });
        },
      });
  }

  function getDaftarOrderSaya(tanggalPermintaanDari, tanggalPermintaanSampai, tanggalPendaftaranDari, tanggalPendaftaranSampai, nomorRegistrasi, tujuanKlinik, tujuanDokter, tujuanLaboratorium) {
      $('#table-order-saya').DataTable().destroy();
      $('#table-order-saya').DataTable({
          "bSort": false,
          "autoWidth": true,
          "pageLength": 10,
          "ordering": true,
          "processing": true,
          "serverSide": true,
          "order": [],
          "ajax": {
              url: '{site_url}term_laboratorium_bank_darah/get_daftar_order_laboratorium/' + '{pasien_id}' + '/' + '{login_ppa_id}',
              type: "POST",
              dataType: 'json',
              data: {
                  tanggal_permintaan_dari: tanggalPermintaanDari,
                  tanggal_permintaan_sampai: tanggalPermintaanSampai,
                  tanggal_pendaftaran_dari: tanggalPendaftaranDari,
                  tanggal_pendaftaran_sampai: tanggalPendaftaranSampai,
                  nomor_registrasi: nomorRegistrasi,
                  tujuan_klinik: tujuanKlinik,
                  tujuan_dokter: tujuanDokter,
                  tujuan_laboratorium: tujuanLaboratorium,
              }
          },
          "columnDefs": [
              {
                  "width": "10%",
                  "targets": [0, 1, 2, 3, 4, 5, 6, 7, 9, 10],
                  "className": "text-center"
              },
          ],
          "drawCallback": function(settings) {
            let statusForm = '{status_form}';

            $('#table-order-saya .view-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-order-saya .clone-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-order-saya .print-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-order-saya .edit-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-order-saya .delete-order').toggleClass('disabled', statusForm == 'draft');
        }
      });
  }

  function getDaftarRiwayatOrder(tanggalPermintaanDari, tanggalPermintaanSampai, tanggalPendaftaranDari, tanggalPendaftaranSampai, nomorRegistrasi, tujuanKlinik, tujuanDokter, tujuanLaboratorium) {
      $('#table-riwayat-order').DataTable().destroy();
      $('#table-riwayat-order').DataTable({
          "bSort": false,
          "autoWidth": true,
          "pageLength": 10,
          "ordering": true,
          "processing": true,
          "serverSide": true,
          "order": [],
          "ajax": {
              url: '{site_url}term_laboratorium_bank_darah/get_daftar_order_laboratorium/' + '{pasien_id}',
              type: "POST",
              dataType: 'json',
              data: {
                  tanggal_permintaan_dari: tanggalPermintaanDari,
                  tanggal_permintaan_sampai: tanggalPermintaanSampai,
                  tanggal_pendaftaran_dari: tanggalPendaftaranDari,
                  tanggal_pendaftaran_sampai: tanggalPendaftaranSampai,
                  nomor_registrasi: nomorRegistrasi,
                  tujuan_klinik: tujuanKlinik,
                  tujuan_dokter: tujuanDokter,
                  tujuan_laboratorium: tujuanLaboratorium,
              }
          },
          "columnDefs": [
              {
                  "width": "10%",
                  "targets": [0, 1, 2, 3, 4, 5, 6, 7, 9, 10],
                  "className": "text-center"
              },
          ],
          "drawCallback": function(settings) {
            let statusForm = '{status_form}';

            $('#table-riwayat-order .view-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-riwayat-order .clone-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-riwayat-order .print-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-riwayat-order .edit-order').toggleClass('disabled', statusForm == 'draft');
            $('#table-riwayat-order .delete-order').toggleClass('disabled', statusForm == 'draft');
        }
      });
  }

  <?php if ('' != $status_form) { ?>
    var transaksiId = '{transaksi_id}';
    var storedPemeriksaan = loadSelectedPemeriksaan(transaksiId);
    var selectedPemeriksaan = storedPemeriksaan.length > 0 ? storedPemeriksaan : <?= json_encode($selectedPemeriksaan); ?>;
    updateDaftarPemeriksaan('{status_form}');
  <?php } else { ?>
    var selectedPemeriksaan = [];
  <?php } ?>

  jQuery(function() {
      BaseTableDatatables.init();
      let kelasId = '{idkelas}';
      let kelompokPasienId = '{idkelompokpasien}';
      let rekananId = '{idrekanan}';
      let tipeLab = '3';
      let statusPaket = '2';
      let headParent = '0';
      let subParent = [];

      getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeLab, statusPaket, headParent, subParent);
      
      let tanggalPermintaanDari = '';
      let tanggalPermintaanSampai = '';
      let tanggalPendaftaranDari = '';
      let tanggalPendaftaranSampai = '';
      let nomorRegistrasi = '';
      let tujuanKlinik = '0';
      let tujuanDokter = '0';
      let tujuanLaboratorium = '0';

      getDaftarOrderSaya(tanggalPermintaanDari, tanggalPermintaanSampai, tanggalPendaftaranDari, tanggalPendaftaranSampai, nomorRegistrasi, tujuanKlinik, tujuanDokter, tujuanLaboratorium);
      getDaftarRiwayatOrder(tanggalPermintaanDari, tanggalPermintaanSampai, tanggalPendaftaranDari, tanggalPendaftaranSampai, nomorRegistrasi, tujuanKlinik, tujuanDokter, tujuanLaboratorium);
  });
</script>