<!-- Modal Edit Pemeriksaan -->
<div class="modal fade" id="modal-edit-pemeriksaan" data-index="" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h5 class="block-title">Edit Pemeriksaan</h5>
                </div>
            </div>
            <div class="modal-body">
                <form id="editForm">
                    <div class="form-group">
                        <label for="edit-nama-pemeriksaan">Nama Tarif</label>
                        <input type="text" class="form-control" id="edit-nama-pemeriksaan" disabled>
                    </div>
                    <div class="form-group">
                        <label for="edit-harga-tarif">Harga Tarif</label>
                        <input type="text" class="form-control format-number" id="edit-harga-tarif" disabled>
                    </div>
                    <div class="form-group">
                        <label for="edit-kuantitas">Kuantitas</label>
                        <input type="text" class="form-control format-number" id="edit-kuantitas">
                    </div>
                    <div class="form-group">
                        <label for="edit-discount-rp">Discount Rp</label>
                        <input type="text" class="form-control format-number" id="edit-discount-rp">
                    </div>
                    <div class="form-group">
                        <label for="edit-discount-persen">Discount Persen</label>
                        <input type="text" class="form-control format-discount" id="edit-discount-persen">
                    </div>
                    <div class="form-group">
                        <label for="edit-total-akhir">Jumlah / Total Akhir</label>
                        <input type="text" class="form-control format-number" id="edit-total-akhir" disabled>
                    </div>
                    <button type="button" class="btn btn-primary" id="btn-update-pemeriksaan" data-dismiss="modal">Simpan Perubahan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(".format-number").number(true, 0, '.', ',');
    $(".format-discount").number(true, 2, '.', ',');
    
    $(document).on('click', '.btn-edit', function() {
        var indexToEdit = $(this).data('index');
        var pemeriksaanToEdit = selectedPemeriksaan[indexToEdit];
        fillEditModal(pemeriksaanToEdit);
    });
    
    $('#btn-update-pemeriksaan').on('click', function() {
        var indexToEdit = $('#modal-edit-pemeriksaan').data('index');
        var pemeriksaanToEdit = selectedPemeriksaan[indexToEdit];

        // Update data pemeriksaan berdasarkan input modal
        pemeriksaanToEdit.kuantitas = $('#edit-kuantitas').val();
        pemeriksaanToEdit.diskon = $('#edit-discount-rp').val();
        pemeriksaanToEdit.total_keseluruhan = $('#edit-total-akhir').val();

        // Simpan kembali data terupdate
        saveSelectedPemeriksaan(transaksiId, selectedPemeriksaan);

        // Tutup modal
        $('#modal-edit-pemeriksaan').modal('hide');

        // Perbarui tabel order
        updateDaftarPemeriksaan();
    });

    $('#edit-kuantitas').on('input', function () {
        calculatePemeriksaanTotal();
    });

    $('#edit-discount-rp').on('input', function () {
        calculateDiscountPersen();
    });

    $('#edit-discount-persen').on('input', function () {
        calculateDiscountRp();
    });
});

function fillEditModal(pemeriksaan) {
    $('#edit-nama-pemeriksaan').val(pemeriksaan.nama_pemeriksaan);
    $('#edit-harga-tarif').val(formatCurrency(pemeriksaan.total));
    $('#edit-kuantitas').val(pemeriksaan.kuantitas);
    $('#edit-discount-rp').val(pemeriksaan.diskon);
    $('#edit-discount-persen').val((pemeriksaan.diskon / pemeriksaan.total) * 100);
    $('#edit-total-akhir').val(pemeriksaan.total_keseluruhan);

    // Simpan index pemeriksaan yang akan diupdate
    $('#modal-edit-pemeriksaan').data('index', selectedPemeriksaan.indexOf(pemeriksaan));
}

function calculatePemeriksaanTotal() {
    var hargaTarif = parseFloat($('#edit-harga-tarif').val().replace(/[^0-9.-]+/g, ''));
    var kuantitas = parseInt($('#edit-kuantitas').val()) || 0;
    var discountRp = parseFloat($('#edit-discount-rp').val().replace(/[^0-9.-]+/g, '')) || 0;

    var totalAkhir = hargaTarif * kuantitas - discountRp;
    $('#edit-total-akhir').val(totalAkhir.toFixed(2));
}

function calculateDiscountPersen() {
    var hargaTarif = parseFloat($('#edit-harga-tarif').val().replace(/[^0-9.-]+/g, ''));
    var kuantitas = parseInt($('#edit-kuantitas').val()) || 0;
    var discountRp = parseFloat($('#edit-discount-rp').val().replace(/[^0-9.-]+/g, '')) || 0;
    var discountPersen = parseFloat($('#edit-discount-persen').val()) || 0;

    // Validasi Max
    var totalSebelum = hargaTarif * kuantitas;
    if (discountRp > totalSebelum) {
        $('#edit-discount-rp').val(totalSebelum);
    }

    discountPersen = Math.min(discountPersen, 100);
    
    // Jika diskon dalam rupiah diubah, hitung diskon dalam persen
    if (hargaTarif * kuantitas > 0) {
        discountPersen = (discountRp / (hargaTarif * kuantitas)) * 100;
        discountPersen = Math.min(discountPersen, 100);
        $('#edit-discount-persen').val(discountPersen.toFixed(2));
    }

    var totalAkhir = hargaTarif * kuantitas - discountRp;
    $('#edit-total-akhir').val(totalAkhir.toFixed(2));
}

function calculateDiscountRp() {
    var hargaTarif = parseFloat($('#edit-harga-tarif').val().replace(/[^0-9.-]+/g, ''));
    var kuantitas = parseInt($('#edit-kuantitas').val()) || 0;
    var discountRp = parseFloat($('#edit-discount-rp').val().replace(/[^0-9.-]+/g, '')) || 0;
    var discountPersen = parseFloat($('#edit-discount-persen').val()) || 0;

    // Validasi Max
    if (discountPersen > 100) {
        $('#edit-discount-persen').val(100);
    }

    discountPersen = Math.min(discountPersen, 100);

    // Jika diskon dalam persen diubah, hitung diskon dalam rupiah
    discountRp = (hargaTarif * kuantitas * (discountPersen / 100));
    $('#edit-discount-rp').val(discountRp.toFixed(2));

    var totalAkhir = hargaTarif * kuantitas - discountRp;
    $('#edit-total-akhir').val(totalAkhir.toFixed(2));
}

function formatCurrency(value) {
    return $.number(value, 0, '.', ',');
}
</script>