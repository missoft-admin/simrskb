<style>
.auto_blur {
    background-color: #d0f3df;
}

.tab-content {
    background-color: #f9f9f9;
}

.gray-background {
  background-color: #f2f2f2 !important;
}

.white-background {
  background-color: #ffffff !important;
}

/* Increase z-index for modal and select dropdowns */
.modal {
    z-index: 1201; /* Adjust the value if needed */
}

.select2-container {
    z-index: 9999 !important;
}
</style>

<?php if ($menu_kiri == 'bank_darah_pemeriksaan') { ?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri == 'bank_darah_pemeriksaan' ? 'block' : 'none') ?>;">
    <button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
    <div class="block animated fadeIn push-10-t" data-category="erm_laboratorium">
      <div class="block">
          <ul class="nav nav-tabs" data-toggle="tabs">
              <li class="active">
                  <a href="#tab_order_baru"><i class="fa fa-list"></i> Order &nbsp; <span class="badge badge-success pull-right">Proses Pemeriksaan</span></a>
              </li>
          </ul>
          <div class="block-content tab-content">
              <div class="tab-pane fade fade-left active in" id="tab_order_baru">
                <?= form_open('term_laboratorium_bank_darah/simpan_pemeriksaan','class="form-horizontal push-10-t" id="form-work"') ?>
                <div class="row">
                  <div class="col-md-12 text-right">
                    <?php if ($statuskasir != 2) { ?>
                      <?php if ($status_form == 'edit_pemeriksaan') { ?>
                        <button type="submit" class="btn btn-warning" name="btn_submit" data-type="form-submit-update"><i class="fa fa-save"></i> Simpan Perubahan</button>
                      <?php } ?>
                      <button type="submit" class="btn btn-success" name="btn_submit" data-type="form-submit-process"><i class="fa fa-check-circle"></i> Proses Transaksi</button>
                      <a href="#" class="btn btn-danger" onclick="batalPermintaan('{transaksi_id}')"><i class="fa fa-trash"></i> Batalkan Permintaan</a>
                    <?php } ?>
                    <a href="#" class="btn btn-primary" onclick="cetakPermintaan('{transaksi_id}')"><i class="fa fa-print"></i> Cetak Permintaan</a>
                    <a href="{site_url}term_laboratorium_bank_darah/tindakan/{asal_rujukan_status}/{pendaftaran_id}/erm_lab/bank_darah_permintaan" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <h4 class="font-w700 push-5 text-center text-primary"><?= strip_tags($pengaturan_form['label_judul']); ?></h4>
                  <h4 class="push-5 text-center"><i><?= strip_tags($pengaturan_form['label_judul_eng']); ?></i></h4>
                </div>

                <br><br>

                <div class="row push-10-t">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Jenis Pemeriksaan</label>
                          <div class="col-xs-12">
                            <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                              <?php foreach(list_variable_ref(86) as $row){ ?>
                                <option value="<?= $row->id; ?>" <?= ($jenis_pemeriksaan == '' && $row->st_default == '1' ? 'selected' : ''); ?> <?= ($row->id == $jenis_pemeriksaan ? 'selected' : ''); ?>><?= $row->nama; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Rencana Pemeriksaan</label>
                          <div class="col-xs-12">
                            <input type="text" name="rencana_pemeriksaan" id="rencana_pemeriksaan" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" value="{rencana_pemeriksaan}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Tanggal & Waktu Pembuatan</label>
                          <div class="col-xs-12">
                            <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_waktu_pembuatan}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-xs-12" for="">Nama Profesional Pemberi Asuhan</label>
                          <div class="col-xs-12">
                            <input type="text" class="form-control" disabled value="{login_nip_ppa} - {login_nama_ppa}">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <?php if ($status_form != '') { ?>
                <div class="form-input-pemeriksaan">
                  <hr>

                  <div class="row push-10-t">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_tujuan_laboratorium']); ?></label>
                            <div class="col-xs-12">
                              <select name="tujuan_laboratorium" id="tujuan_laboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach ($list_tujuan_laboratorium as $r) { ?>
                                  <option value="<?= $r->id; ?>" <?= $r->id == $tujuan_laboratorium ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_dokter_peminta_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <select name="dokter_peminta_id" id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                                  <option value="<?= $r->id; ?>" <?= $r->id == $login_pegawai_id || $r->id == $dpjp ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_diagnosa']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="diagnosa" id="diagnosa" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_diagnosa']); ?>" value="{diagnosa}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="catatan_permintaan" id="catatan_permintaan" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?>" value="{catatan_permintaan}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_waktu_pemeriksaan']); ?></label>
                            <div class="col-xs-12">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_permintaan" id="tanggal_permintaan" placeholder="HH/BB/TTTT" value="{tanggal_permintaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_permintaan" id="waktu_permintaan" value="{waktu_permintaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_prioritas']); ?></label>
                            <div class="col-xs-12">
                              <select name="prioritas" id="prioritas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(85) as $row) { ?>
                                  <option value="<?= $row->id; ?>" <?= '' == $prioritas && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $prioritas ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_indikasi_transfusi']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="indikasi_transfusi" id="indikasi_transfusi" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_indikasi_transfusi']); ?>" value="{indikasi_transfusi}" <?php echo 'view' == $status_form ? 'disabled' : ''; ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_hb']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="hb" id="hb" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_hb']); ?>" value="{hb}" <?php echo 'view' == $status_form ? 'disabled' : ''; ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_riwayat_transfusi_sebelumnya']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="riwayat_transfusi_sebelumnya" id="riwayat_transfusi_sebelumnya" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_riwayat_transfusi_sebelumnya']); ?>" value="{riwayat_transfusi_sebelumnya}" <?php echo 'view' == $status_form ? 'disabled' : ''; ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_riwayat_kehamilan_sebelumnya']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="riwayat_kehamilan_sebelumnya" id="riwayat_kehamilan_sebelumnya" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_riwayat_kehamilan_sebelumnya']); ?>" value="{riwayat_kehamilan_sebelumnya}" <?php echo 'view' == $status_form ? 'disabled' : ''; ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_keterangan_lainnya']); ?></label>
                            <div class="col-xs-12">
                              <input type="text" name="keterangan_lainnya" id="keterangan_lainnya" class="form-control" placeholder="<?= strip_tags($pengaturan_form['label_keterangan_lainnya']); ?>" value="{keterangan_lainnya}" <?php echo 'view' == $status_form ? 'disabled' : ''; ?>>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <hr>

                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Pengambilan Sample</label>
                            <div class="col-xs-12">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pengambilan_sample" id="tanggal_pengambilan_sample" placeholder="HH/BB/TTTT" value="{tanggal_pengambilan_sample}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_pengambilan_sample" id="waktu_pengambilan_sample" value="{waktu_pengambilan_sample}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Petugas Pengambil Sample</label>
                            <div class="col-xs-12">
                              <select name="petugas_pengambilan_sample" class="js-select2 form-control" id="petugas-pengambil-sample" style="width: 100%;" data-placeholder="Pilih Opsi">
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Nomor Laboratorium</label>
                            <div class="col-xs-12">
                              <input type="text" name="nomor_laboratorium" id="nomor_laboratorium" class="form-control" placeholder="Nomor Laboratorium" value="{nomor_laboratorium}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Dokter Laboratorium</label>
                            <div class="col-xs-12">
                              <select name="dokter_laboratorium" class="js-select2 form-control" id="dokter-laboratorium" style="width: 100%;" data-placeholder="Pilih Opsi">
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-xs-12" for="">Dokter Penganggung Jawab</label>
                            <div class="col-xs-12">
                              <select class="js-select2 form-control" id="dokter-penanggung-jawab" disabled style="width: 100%;">
                                <?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
                                  <option value="<?=$row->id?>" <?= $row->id == $dokter_penanggung_jawab ? 'selected' : ''; ?>><?=$row->nama?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <hr>

                  <div class="row">
                    <div class="col-md-6">
                      <h6 class="font-w700 push-5 text-primary">DAFTAR PEMERIKSAAN</h6>
                      <div class="table-header text-right" style="margin-bottom: 25px;">
                          <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-filter">
                              <i class="fa fa-filter"></i> Filter
                          </button>
                      </div>
                      <table class="table table-striped table-striped table-hover table-bordered" id="table-pemeriksaan">
                        <thead>
                          <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Pemeriksaan</th>
                            <th class="text-center">Harga</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                    </div>
                    <div class="col-md-6">
                      <h6 class="font-w700 push-5 text-primary">DAFTAR ORDER</h6>
                      <table class="table table-striped table-striped table-hover table-bordered" id="table-order">
                        <thead>
                          <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Pemeriksaan</th>
                            <th class="text-center">Harga</th>
                            <?php if ('lihat_pemeriksaan' != $status_form) { ?>
                            <th class="text-center">Aksi</th>
                            <?php } ?>
                          </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                          <tr>
                            <td class="text-right" colspan="2">Total</td>
                            <td class="text-right"></td>
                            <?php if ('lihat_pemeriksaan' != $status_form) { ?>
                            <td></td>
                            <?php } ?>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
                <?php } ?>

                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="{pendaftaran_id}">
                <input type="hidden" id="transaksi_id" name="transaksi_id" value="{transaksi_id}">
                <input type="hidden" id="pasien_id" name="pasien_id" value="{idpasien}">
                <input type="hidden" id="dokter_perujuk_id" name="dokter_perujuk_id" value="{dpjp}">
                <input type="hidden" id="dokter_penanggung_jawab" name="dokter_penanggung_jawab" value="{dokter_penanggung_jawab}">
                <input type="hidden" id="data_pemeriksaan" name="data_pemeriksaan" value="">
                <input type="hidden" id="status_form" name="status_form" value="{status_form}">
                <input type="hidden" id="ppa_id" name="ppa_id" value="{login_ppa_id}">
                <input type="hidden" id="form_submit" name="form_submit" value="">
                <?= form_close() ?>
              </div>
          </div>
      </div>
    </div>
</div>
<? } ?>

<?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/modal/modal_filter'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/modal/modal_edit_pemeriksaan'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/modal/modal_ubah_kelas_tarif_satuan'); ?>
<?php $this->load->view('Tpendaftaran_poli_ttv/erm_lab/bank_darah/services_pemeriksaan'); ?>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
      $("#cover-spin").hide();

      $(".time-datepicker").datetimepicker({
          format: "HH:mm:ss"
      });

      $("#tujuan_laboratorium").on("change", function() {
          let value = $(this).val();
          loadDataDokterLaboratorium(value);
          loadDataDokterPenanggungJawab(value);
      });

      $('#table-pemeriksaan').on('click', '.parent-checkbox', function() {
          var isChecked = $(this).prop('checked');
          var isParent = $(this).hasClass('parent-checkbox');
          var path = $(this).data('path');

          updateChildCheckboxes(path, isChecked);
          updateSelectedPemeriksaan(isParent, isChecked, path);
          updateDaftarPemeriksaan();
      });

      $('#table-pemeriksaan').on('click', '.child-checkbox', function() {
          var isChecked = $(this).prop('checked');
          var isParent = $(this).hasClass('parent-checkbox');
          var path = $(this).data('path');

          updateParentCheckbox(path);
          updateSelectedPemeriksaan(isParent, isChecked, path);
          updateDaftarPemeriksaan();
      });

      $("#form-work input").on('blur', function() {
          updateDraftPermintaan();
      });

      $("#form-work select").on('change', function() {
          updateDraftPermintaan();
      });

      $('#form-reset').on('click', function () {
          resetFormPermintaan();
      });

      $('button[name="btn_submit"]').on('click', function (event) {
          event.preventDefault();

          $('#form_submit').val($(this).data('type'));

          if ($('#diagnosa').val().trim() == '') {
              swal({
                  title: "Peringatan",
                  text: "Diagnosa tidak boleh kosong!",
                  type: "warning",
                  confirmButtonColor: "#34a263",
                  confirmButtonText: "OK",
              });
              return;
          }
          
          swal({
            title: "Konfirmasi Order Pemeriksaan",
            text: "Apakah Anda yakin akan mengirim permintaan ini? Permintaan Anda akan dikirim kepada laboratorium tujuan. Tekan Ya untuk melanjutkan.",
            type : "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
          }).then((willSubmit) => {
              if (willSubmit) {
                $("#cover-spin").show();
                $('#data_pemeriksaan').val(JSON.stringify(selectedPemeriksaan));
                $('#form-work').submit();
                
                localStorage.removeItem(`selectedPemeriksaanLaboratorium_${transaksiId}`);
              }
          });
      });
  });

  function getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeLab, statusPaket, headParent, subParent) {
      $('#table-pemeriksaan').DataTable().destroy();
      $('#table-pemeriksaan').DataTable({
          "bSort": false,
          "autoWidth": false,
          "pageLength": 10,
          "ordering": true,
          "processing": true,
          "serverSide": true,
          "order": [],
          "ajax": {
              url: '{site_url}term_laboratorium_bank_darah/get_daftar_pemeriksaan_laboratorium',
              type: "POST",
              dataType: 'json',
              data: {
                  idkelas: kelasId,
                  idkelompokpasien: kelompokPasienId,
                  idrekanan: rekananId,
                  idtipe: tipeLab,
                  idpaket: statusPaket,
                  idtindakan: headParent,
                  idsubheader: JSON.stringify(subParent),
              }
          },
          "columnDefs": [{
                  "width": "5%",
                  "targets": 0,
                  "orderable": true,
                  "class": "text-center"
              },
              {
                  "width": "5%",
                  "targets": 1,
                  "orderable": true,
                  "class": "text-center"
              },
              {
                  "width": "15%",
                  "targets": 2,
                  "orderable": true
              },
              {
                  "width": "5%",
                  "targets": 3,
                  "orderable": true,
                  "class": "text-right"
              }
          ],
          "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
          "rowCallback": function(row, data) {
              let isTarifKelompok = data[4];

              if (isTarifKelompok == 1) {
                  $(row).addClass('gray-background');
              } else {
                $(row).addClass('white-background');
              }
          },
          "drawCallback": function(settings) {
            let statusKasir = '{statuskasir}';
            $('#table-pemeriksaan input[type="checkbox"]').each(function() {
                let checkboxValue = $(this).val();
                let isChecked = selectedPemeriksaan.some(function(item) {
                    return item.pemeriksaan_id == checkboxValue && item.status_delete == 0;
                });

                $(this).prop('checked', isChecked);

                if (statusKasir == 2) {
                  $(this).prop('disabled', true);
                }
            });
          },
      });
  }

  var transaksiId = '{transaksi_id}';
  var storedPemeriksaan = loadSelectedPemeriksaan(transaksiId);
  var selectedPemeriksaan = storedPemeriksaan.length > 0 ? storedPemeriksaan : <?= json_encode($selectedPemeriksaan); ?>;

  updateDaftarPemeriksaan('{status_form}');
  loadDataDokterLaboratorium('{tujuan_laboratorium}')
  loadDataPetugasPengambilSample('{tujuan_laboratorium}')
  loadDataDokterPenanggungJawab('{tujuan_laboratorium}')

  jQuery(function() {
      BaseTableDatatables.init();
      let kelasId = '{idkelas}';
      var kelompokPasienId = '{idkelompokpasien}';
      var rekananId = '{idrekanan}';
      var tipeLab = '3';
      var statusPaket = '2';
      var headParent = '0';
      var subParent = [];

      getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeLab, statusPaket, headParent, subParent);      
  });
</script>