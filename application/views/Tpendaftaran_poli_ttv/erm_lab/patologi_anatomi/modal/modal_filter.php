<!-- Filter Modal -->
<div class="modal fade in" id="modal-filter" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h5 class="block-title">Filter Options</h5>
                </div>
            </div>
            <div class="block-content">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tipe-lab" class="col-xs-12">Tipe</label>
                                <div class="col-xs-12">
                                    <select id="tipe-lab" disabled class="js-select2 form-control" style="width: 100%;">
                                        <option value="1">Umum</option>
                                        <option value="2" selected>Patologi Anatomi</option>
                                        <option value="3">Bank Darah</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="head-parent" class="col-xs-12">Head Parent</label>
                                <div class="col-xs-12">
                                    <select id="head-parent" class="js-select2 form-control" style="width: 100%;">
                                        <option value="0">Semua</option>
                                        <?php foreach ($head_parent as $row) { ?>
                                        <option value="<?= $row->path; ?>"><?= TreeView($row->level, $row->nama); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sub-parent" class="col-xs-12">Sub Parent</label>
                                <div class="col-xs-12">
                                    <select class="js-select2 form-control" id="sub-parent" multiple style="width: 100%;"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status-paket" class="col-xs-12">Status Paket</label>
                                <div class="col-xs-12">
                                    <select class="js-select2 form-control" id="status-paket" style="width: 100%;">
                                        <option value="0">Tidak</option>
                                        <option value="1">Ya</option>
                                        <option value="2" selected>Semua</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-filter">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '#btn-filter', function() {
        let kelasId = '{idkelas}';
        let kelompokPasienId = '{idkelompokpasien}';
        let rekananId = '{idrekanan}';
        let tipeLab = $("#tipe-lab option:selected").val();
        let statusPaket = $("#status-paket option:selected").val();
        let headParent = $("#head-parent option:selected").val();
        let subParent = $("#sub-parent").val();

        getDaftarPemeriksaan(kelasId, kelompokPasienId, rekananId, tipeLab, statusPaket, headParent, subParent);
    });

    $(document).on('change', '#head-parent', function() {
        var tipeLab = $("#tipe-lab option:selected").val();
        var headParent = $(this).val();

        if ($(this).val() != '') {
            $('#sub-parent').html('');
            $.ajax({
                url: '{site_url}trujukan_laboratorium/find_subparent/' + tipeLab + '/' + headParent,
                dataType: "json",
                success: function(data) {
                    $('#sub-parent').append(data);
                }
            });
        }
    });
});
</script>
