<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bukti Permintaan Laboratorium</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 14px !important;
      }
      table {
        font-size: 14px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 9px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td>
          <img src="<?= base_url() ?>assets/upload/pengaturan_printout_laboratorium/<?= strip_tags($pengaturan_printout['logo']); ?>" width="100px">
        </td>
        <td>
          &nbsp;<b><?= strip_tags($pengaturan_printout['label_header']); ?></b>
        </td>
      </tr>
    </table>
    <br>
    <table class="content-2">
      <tr>
        <td style="width:200px">NO REGISTER</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nomor_registrasi; ?></td>

        <td style="width:200px">NO PERMINTAAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nomor_permintaan; ?></td>
      </tr>
      <tr>
        <td style="width:200px">WAKTU PERMINTAAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $waktu_permintaan; ?></td>

        <td style="width:200px">RENCANA PEMERIKSAAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $rencana_pemeriksaan;?></td>
      </tr>
      <tr>
        <td style="width:200px">NO REKAM MEDIS</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nomedrec; ?></td>

        <td style="width:200px">NAMA PASIEN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nama_pasien; ?></td>
      </tr>
      <tr>
        <td style="width:200px">TANGGAL LAHIR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= date("Y-m-d", strtotime($tanggal_lahir)); ?></td>

        <td style="width:200px">DOKTER PEMINTA</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $dokter_peminta; ?></td>
      </tr>
      <tr>
        <td style="width:200px">DIAGNOSA</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $diagnosa; ?></td>

        <td style="width:200px">CATATAN PERMINTAAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $catatan_permintaan; ?></td>
      </tr>
      <tr>
        <td style="width:200px">PRIORITAS</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $prioritas; ?></td>

        <td style="width:200px"></td>
        <td class="text-center" style="width:20px"></td>
        <td></td>
      </tr>
    </table>
    <p class="text-center "><b><?= strip_tags($pengaturan_printout['label_subheader']); ?></b></p>
    <table>
      <tr>
        <td class="text-center border-full">NO</td>
        <td class="text-center border-full">PEMERIKSAAN</td>
        <td class="text-center border-full">KUANTITAS</td>
      </tr>
      <?php $number = 1; ?>
      <?php foreach  ($list_pemeriksaan as $row){ ?>
        <tr>
          <td class="text-center border-full"><?=$number++?></td>
          <td class="border-full"><?=strtoupper($row->nama)?></td>
          <td class="text-right border-full"><?=number_format($row->kuantitas)?></td>
        </tr>
      <?php } ?>
    </table>
    <br>
    <table>
      <tr>
        <td style="width:20%" class="text-center"><?= strip_tags($pengaturan_printout['label_footer']); ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <?php if ($pengaturan_printout['tampilkan_tanda_tangan'] == 1) { ?>
      <tr>
        <td class="text-center">
          <img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dokter_peminta_id; ?>" width="100px">
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <? } ?>
      <tr>
        <td style="width:20%" class="text-center"><?= $dokter_peminta; ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    
    <?php if ($pengaturan_printout['tampilkan_tanggal_jam_cetak'] == 1) { ?>
    <p style="margin-top: 50px;"><i>Tanggal & Jam Cetak <?= date("d-m-Y h:i:s") ?> | User Created : <?= $this->session->userdata('user_name'); ?> <?= date("d-m-Y h:i:s") ?></i></p>
    <?php } ?>
  </body>
</html>
