<style>
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited_final {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='his_cppt'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_cppt' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		if ($assesmen_id){
			$tanggaldaftar=HumanDateShort($tanggal_input);
			$waktudaftar=HumanTime($tanggal_input);
		}else{
			$waktudaftar=date('H:i:s');
			$tanggaldaftar=date('d-m-Y');
			
		}
		
		$disabel_input='';
		$disabel_input_ttv='';
		// if ($status_ttv!='1'){
			// $disabel_input_ttv='disabled';
		// }
		if ($st_input_cppt=='0'){
			$disabel_input='disabled';
		}
		$disabel_cetak='';
		if ($st_cetak_assesmen=='0'){
			$disabel_cetak='disabled';
		}
		
			
		?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_cppt=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="dokter_pjb" value="<?=$dokter_pjb?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<input type="hidden" id="st_sedang_edit" value="" >		
						<div class="col-md-12">
							<?if($st_lihat_cppt=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_cppt" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>ALL</option>
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%"></th>
												
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<?if ($field_subjectif=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="subjectif" class="<?=(trim($subjectif)!=trim($subjectif_asal)?'text-danger':'')?>">Subjectif (S)</label> <?=(trim($subjectif)!=trim($subjectif_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data <?=(trim($subjectif)!=trim($subjectif_asal)?'edited':'')?>" id="subjectif"  rows="3" placeholder="Subjectif"> <?=$subjectif?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
							<?if ($field_objectif=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="objectif" class="<?=(trim($objectif)!=trim($objectif_asal)?'text-danger':'')?>">Objectif (O)</label> <?=(trim($objectif)!=trim($objectif_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data <?=(trim($objectif)!=trim($objectif_asal)?'edited':'')?>" id="objectif"  rows="3" placeholder="Bbjectif"> <?=$objectif?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
							<?if ($field_assesmen=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="assemen" class="<?=(trim($assemen)!=trim($assemen_asal)?'text-danger':'')?>">Assesmen (A)</label> <?=(trim($assemen)!=trim($assemen_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data <?=(trim($assemen)!=trim($assemen_asal)?'edited':'')?>" id="assemen"  rows="3" placeholder="Bbjectif"> <?=$assemen?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
							<?if ($field_planing=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="planing" class="<?=(trim($planing)!=trim($planing_asal)?'text-danger':'')?>">Planing (P)</label> <?=(trim($planing)!=trim($planing_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data <?=(trim($planing)!=trim($planing_asal)?'edited':'')?>" id="planing"  rows="3" placeholder="Planing"> <?=$planing?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
							<?if ($field_intruction=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="intruksi" class="<?=(trim($intruksi)!=trim($intruksi_asal)?'text-danger':'')?>">Intruksi (I)</label> <?=(trim($intruksi)!=trim($intruksi_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data  <?=(trim($intruksi)!=trim($intruksi_asal)?'edited':'')?>" id="intruksi"  rows="3" placeholder="intruksi"> <?=$intruksi?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
							<?if ($field_intervensi=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="intervensi" class="<?=(trim($intervensi)!=trim($intervensi_asal)?'text-danger':'')?>">Intervensi (I)</label> <?=(trim($intervensi)!=trim($intervensi_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data <?=(trim($intervensi)!=trim($intervensi_asal)?'edited':'')?>" id="intervensi"  rows="3" placeholder="intervensi"> <?=$intervensi?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
							<?if ($field_evaluasi=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="evaluasi" class="<?=(trim($evaluasi)!=trim($evaluasi_asal)?'text-danger':'')?>">Evaluasi (E)</label> <?=(trim($evaluasi)!=trim($evaluasi_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data <?=(trim($evaluasi)!=trim($evaluasi_asal)?'edited':'')?>" id="evaluasi"  rows="3" placeholder="evaluasi"> <?=$evaluasi?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
							<?if ($field_reasessmen=='1'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="reassesmen" class="<?=(trim($reassesmen)!=trim($reassesmen_asal)?'text-danger':'')?>">Reassesmen (R)</label> <?=(trim($reassesmen)!=trim($reassesmen_asal)?text_danger('Changed'):'')?>
										<textarea class="form-control isi_data  <?=(trim($reassesmen)!=trim($reassesmen_asal)?'edited':'')?>" id="reassesmen"  rows="3" placeholder="reassesmen"> <?=$reassesmen?></textarea>
									</div>
									
								</div>
							</div>
							<?}?>
						
						
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;

$(document).ready(function() {
	list_index_history_edit();
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	// $("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

	load_awal_assesmen=false;
});
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".isi_data").removeAttr('disabled');
		
		 $(".edited").removeClass("edited").addClass("edited_final");
		 $(".isi_data").removeClass("isi_data").addClass("js-summernote");
	}
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "20%", "targets": [1] },
					 // { "width": "15%", "targets": [3] },
					 // { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_cppt', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $("#index_pencarian_history thead").remove();
			 }  
		});
	
}
</script>