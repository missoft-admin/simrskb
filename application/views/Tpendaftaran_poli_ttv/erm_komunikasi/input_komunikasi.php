<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

  
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_komunikasi'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_komunikasi' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						$tanggallaporan=HumanDateShort($tanggal_laporan);
						$waktulaporan=HumanTime($tanggal_laporan);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggallaporan=date('H:i:s');
						$waktulaporan=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_komunikasi=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_komunikasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':' Baru')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id?'active in':'')?>" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
						<input type="hidden" id="tipe_input" value="<?=$tipe_input?>" >		
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="tipe_input_nama" value="<?=$tipe_input_nama?>" >		
						<input type="hidden" id="st_sedang_edit" value="" >		
						<div class="col-md-12">
							<?if($st_lihat_komunikasi=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header} {tipe_input_nama}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru <?=$tipe_input_nama?> Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing <?=$tipe_input_nama?> Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template <?=$tipe_input_nama?> Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_komunikasi=='1'){?>
										<?if ($st_sbar=='1'){?>
										<button class="btn btn-warning btn_create_assesmen" onclick="create_assesmen(1)" title="Add SBAR" type="button"><i class="fa fa-plus"></i> S B A R</button>
										<?}?>
										<?if ($st_kritis=='1'){?>
										<button class="btn btn-danger btn_create_assesmen" onclick="create_assesmen(2)" title="Add Hasil Kritis" type="button"><i class="fa fa-plus"></i> HASIL KRITIS</button>
										<?}?>
										<?if ($st_notes=='1'){?>
										<button class="btn btn-primary btn_create_assesmen" onclick="create_assesmen(3)" title="Add Notes" type="button"><i class="fa fa-plus"></i> NOTES</button>
										<?}?>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tkomunikasi/tindakan/<?=$pendaftaran_id?>/erm_rj/input_komunikasi" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Pengkajian</a>
										<?}?>
									<?}?>
										<button class="btn btn-default btn_back"  type="button" onclick="goBack()"><i class="fa fa-reply"></i> Kembali</button>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal Input</label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu Input</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-2 ">
										<label for="ppa_pemberi">Tanggal</label>
										<div class="input-group date">
											<input type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggallaporan" placeholder="HH/BB/TTTT" name="tanggallaporan" value="<?= $tanggallaporan ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-2 ">
										<label for="ppa_pemberi">Waktu</label>
										<div class="input-group date">
											<input type="text" class="time-datepicker form-control auto_blur" id="waktulaporan" name="waktulaporan" value="<?= $waktulaporan ?>" required>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
									<?if ($tipe_input=='1'){?>
									<div class="col-md-8 ">
										<label for="ppa_pemberi">Pemberi Perintah</label>
										<select id="ppa_pemberi" <?=($status_assemen=='2'?'disabled':'')?> name="ppa_pemberi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($ppa_pemberi==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_ppa as $r){?>
											<option value="<?=$r->id?>" <?=($ppa_pemberi==$r->id?'selected':'')?>><?=$r->nama?></option>
											<?}?>
											
										</select>
									</div>
									<?}?>
									<?if ($tipe_input=='2'){?>
									<div class="col-md-4 ">
										<label for="dilaporkan_ppa">Dilaporkan Kepada</label>
										<select id="dilaporkan_ppa" <?=($status_assemen=='2'?'disabled':'')?> name="dilaporkan_ppa" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($dilaporkan_ppa==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_ppa as $r){?>
											<option value="<?=$r->id?>" <?=($dilaporkan_ppa==$r->id?'selected':'')?>><?=$r->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-4 ">
										<label for="jenis_laporan">Jenis Laporan Nilai Kritis</label>
										<select id="jenis_laporan" <?=($status_assemen=='2'?'disabled':'')?> name="jenis_laporan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($jenis_laporan==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach(list_variable_ref(298) as $r){?>
											<option value="<?=$r->id?>" <?=($jenis_laporan=='' && $r->st_default=='1'?'selected':'')?> <?=($jenis_laporan==$r->id?'selected':'')?>><?=$r->nama?></option>
											<?}?>
											
										</select>
									</div>
									<?}?>
								</div>
							</div>
							<?if ($tipe_input=='1'){?>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="situation">SITUATION</label>
										<textarea class="form-control js-summernote auto_blur" id="situation"  rows="2" placeholder="situation"> <?=$situation?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="background">BACKGROUND</label>
										<textarea class="form-control js-summernote auto_blur" id="background"  rows="2" placeholder="background"> <?=$background?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="assemen">ASSESMEN</label>
										<textarea class="form-control js-summernote auto_blur" id="assemen"  rows="2" placeholder="assemen"> <?=$assemen?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="recomendation">RECOMENDATION</label>
										<textarea class="form-control js-summernote auto_blur" id="recomendation"  rows="2" placeholder="recomendation"> <?=$recomendation?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<?
										$arr_tulbakon=explode(',',$tulbakon);
										
										foreach(list_variable_ref(299) as $row){?>
										<label class="checkbox-inline" for="tulbakon_<?=$row->id?>">
											<input type="checkbox" class="tulbakon" id="tulbakon_<?=$row->id?>" <?=(in_array($row->id, $arr_tulbakon)?'checked':'')?> name="tulbakon" value="<?=$row->id?>"> <?=$row->nama?>
										</label>
										<?}?>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
										<table width="100%">
											<tr>
												<td class="text-center" width="20%"><strong>YANG MENERIMA</strong></td>
												<td class="text-center" width="20%"><strong><?=($st_verifikasi?'PEMBERI PERINTAH':'')?></strong></td>
												<td class="text-center" width="20%"><strong><?=($st_pelaksanaan?'PELAKSANA':'')?></strong></td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
											</tr>
											<tr>
												<td class="text-center" width="20%">
													<img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $created_ppa; ?>" width="100px">
												</td>
												<td class="text-center" width="20%">
													<?if ($st_verifikasi){?>
													
													<img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $ppa_pemberi; ?>" width="100px">
													<?}?>
												</td>
												<td class="text-center" width="20%">
													<?if ($st_pelaksanaan){?>
													<img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $ppa_pelaksana; ?>" width="100px">
													<?}?>
												</td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
											</tr>
											<tr>
												<td class="text-center" width="20%"><strong>( <?=get_nama_ppa($created_ppa)?></strong> )</td>
												<td class="text-center" width="20%"><strong><?=($st_verifikasi?'( '.get_nama_ppa($ppa_pemberi).' )':'')?></strong></td>
												<td class="text-center" width="20%"><strong><?=($st_pelaksanaan?'( '.get_nama_ppa($ppa_pelaksana).' )':'')?></strong></td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
											</tr>
										</table>
									
								</div>
							</div>
							<?}?>
							<?if ($tipe_input=='2'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="nilai_kritis">NILAI KRITIS YANG DILAPORKAN</label>
										<textarea class="form-control js-summernote auto_blur" id="nilai_kritis"  rows="2" placeholder="nilai_kritis"> <?=$nilai_kritis?></textarea>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
										<table width="100%">
											<tr>
												<td class="text-center" width="20%"><strong>YANG MELAPORKAN</strong></td>
												<td class="text-center" width="20%"><strong><?=($st_verifikasi?'DPJP':'')?></strong></td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
											</tr>
											<tr>
												<td class="text-center" width="20%">
													<img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $created_ppa; ?>" width="100px">
												</td>
												<td class="text-center" width="20%">
													<?if ($st_verifikasi){?>
													
													<img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $dilaporkan_ppa; ?>" width="100px">
													<?}?>
												</td>
												<td class="text-center" width="20%">
													
												</td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
											</tr>
											<tr>
												<td class="text-center" width="20%"><strong>( <?=get_nama_ppa($created_ppa)?></strong> )</td>
												<td class="text-center" width="20%"><strong><?=($st_verifikasi?'( '.get_nama_ppa($dilaporkan_ppa).' )':'')?></strong></td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
												<td class="text-center" width="20%"></td>
											</tr>
										</table>
									
								</div>
							</div>
							
							<?}?>
							<?if ($tipe_input=='3'){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="notes">NOTES</label>
										<textarea class="form-control js-summernote auto_blur" id="notes"  rows="2" placeholder="notes"> <?=$notes?></textarea>
									</div>
									
								</div>
							</div>
							
							
							<?}?>
						<?}?>
						
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left  <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_komunikasi=='1'){?>
										<?if ($st_sbar=='1'){?>
										<button class="btn btn-warning btn_create_assesmen" onclick="create_assesmen(1)" title="Add SBAR" type="button"><i class="fa fa-plus"></i> S B A R</button>
										<?}?>
										<?if ($st_kritis=='1'){?>
										<button class="btn btn-danger btn_create_assesmen" onclick="create_assesmen(2)" title="Add Hasil Kritis" type="button"><i class="fa fa-plus"></i> HASIL KRITIS</button>
										<?}?>
										<?if ($st_notes=='1'){?>
										<button class="btn btn-primary btn_create_assesmen" onclick="create_assesmen(3)" title="Add Notes" type="button"><i class="fa fa-plus"></i> NOTES</button>
										<?}?>
									<?}?>
									<?}?>
									
								</div>
								
							</div>
						</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="notransaksi">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tgl_daftar">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="mppa_id">Add By Person</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="mppa_id">Jenis Inputan</label>
										<div class="col-md-8">
											<select id="jenis_inputan" name="jenis_inputan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<option value="1" selected>SBAR</option>
												<option value="2" selected>HASIL KRITIS</option>
												<option value="3" selected>NOTES</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal_input_1">Periode Penginputan</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idpoli">Add By Profesi</label>
										<div class="col-md-8">
											<select id="profesi_id_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="iddokter">Owned By Me</label>
										<div class="col-md-8">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>ALL</option>
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="mppa_id">Status Verifikasi</label>
										<div class="col-md-8">
											<select id="st_verifikasi" name="st_verifikasi" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>ALL</option>
												<option value="1">SUDAH DIVERIFIKASI</option>
												<option value="0">MENUNGGU DIVERIFIKASI</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="mppa_id">Status Dikerjakan</label>
										<div class="col-md-8">
											<select id="st_pelaksanaan" name="st_pelaksanaan" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>ALL</option>
												<option value="1">TELAH DILAKSANAKAN</option>
												<option value="0">BELUM DILAKSANAKAN</option>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-top: 5px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
											
									</div>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												
												<th width="100%">Action</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-warning btn-xs" type="button" <?=($st_input_komunikasi!='1'?'disabled':'')?> onclick="create_template(1)"><i class="fa fa-plus"></i> Template S B A R</button>
										<button class="btn btn-danger btn-xs " type="button" <?=($st_input_komunikasi!='1'?'disabled':'')?> onclick="create_template(2)"><i class="fa fa-plus"></i> Template HASIL KRITIS</button>
										<button class="btn btn-primary btn-xs " type="button" <?=($st_input_komunikasi!='1'?'disabled':'')?> onclick="create_template(3)"><i class="fa fa-plus"></i> Template NOTES</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="20%">Jenis</th>
												<th width="40%">Nama</th>
												<th width="20%">Jumlah Penggunaan</th>
												<th width="20%">Action</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_pelaksanaan" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PELAKSANAAN S B A R</h3>
				</div>
				<div class="block-content">
					<input type="hidden" id="assesmen_id_pelaksanaan" value="" >		
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-8 ">
									<label for="recomendation">Waktu Pelaksanaan</label>
									<div class="input-group date">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="waktu_pelaksanaan" placeholder="HH/BB/TTTT" value="" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
								</div>
								<div class="col-md-4 ">
									<label for="recomendation">&nbsp;&nbsp;</label>
									
									<div class="input-group date">
										<input type="text" class="time-datepicker form-control" id="time_pelaksanaan" value="" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group push-10-t">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="ket_pelaksanaan">Deskripsi</label>
									<textarea class="form-control" id="ket_pelaksanaan"  rows="4" placeholder="Deskripsi"></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group push-10-t">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="ket_pelaksanaan"></label>
								</div>
								
							</div>
						</div>
						
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="simpan_pelaksanaan()" id="btn_hapus"><i class="fa fa-save"></i> Simpan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="jenis_ttd" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_ttd==''?'selected':'')?>>Silahkan Pilih</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Pasien Sendiri Sendiri</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?> >Keluarga Terdekat / Wali</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?> >Penanggung Jawab</option>
										
									</select>
									<label for="jenis_ttd">Yang Tandatangan</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							<div class="col-md-6 push-10">
								<div class="form-material">
									<select tabindex="30" id="ttd_hubungan" name="ttd_hubungan" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($ttd_hubungan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<label for="ttd_hubungan">Hubungan</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_pelaksanaan_lihat" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PELAKSANAAN S B A R</h3>
				</div>
				<div class="block-content">
					<input type="hidden" id="assesmen_id_pelaksanaan" value="" >		
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="recomendation">User Pelaksanaan</label>
									<div class="input-group date">
										<input type="text" class="form-control" disabled id="user_pelaksanaan_lihat" value="">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="recomendation">Waktu Pelaksanaan</label>
									<div class="input-group date">
										<input type="text" class="form-control" disabled data-date-format="dd/mm/yyyy" id="waktu_pelaksanaan_lihat" placeholder="HH/BB/TTTT" value="">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group push-10-t">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="ket_pelaksanaan">Deskripsi</label>
									<textarea class="form-control" disabled id="ket_pelaksanaan_lihat"  rows="4" placeholder="Deskripsi"></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group push-10-t">
							<div class="col-md-12 ">
									<label for="ket_pelaksanaan"></label>
								</div>
						</div>
						
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}plugins/summernote/summernote.min.js"></script>
<script src="{js_path}plugins/ckeditor/ckeditor.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});


$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	// $("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	$('.js-summernote').summernote({
		  height: 100,   //set editable area's height
		  dialogsInBody: true,
		  codemirror: { // codemirror options
			theme: 'monokai'
		  },
		  onImageUpload: function(files, editor, welEditable) {
		  sendFile(files[0], editor, welEditable);
		}
	})

	load_awal_assesmen=false;
	list_history_pengkajian();
	
});

function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd(){
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}

	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tkomunikasi/simpan_ttd_asmed/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
	  load_ttd();
}
function load_ttd(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}tkomunikasi/load_ttd_asmed', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;

			$('#sig').signature('enable'). signature('draw', src);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
	}
}
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen(tipe_input){
	
	$("#modal_default").modal('hide');
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let st_ranap=$("#st_ranap").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let tipe_nama='';
	if (tipe_input=='1'){
		tipe_nama=' S B A R';
	}
	if (tipe_input=='2'){
		tipe_nama=' HASIL KRITIS';
	}
	if (tipe_input=='3'){
		tipe_nama=' NOTES';
	}
	let template='Baru' + tipe_nama;
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/create_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
					tipe_input:tipe_input,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	// let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/create_with_template_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					st_ranap:st_ranap,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function lihat_pelaksanaan(id){
	$("#modal_pelaksanaan_lihat").modal('show');
	$.ajax({
		url: '{site_url}tkomunikasi/lihat_pelaksanaan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:id,
			   },
		success: function(data) {
			$("#waktu_pelaksanaan_lihat").val(data.waktu_pelaksanaan);
			$("#user_pelaksanaan_lihat").val(data.nama);
			$("#ket_pelaksanaan_lihat").val(data.ket_pelaksanaan);
			$("#cover-spin").hide();
			// $('#index_history_kajian').DataTable().ajax.reload( null, false );
		}
	});
}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/create_with_template_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(tipe_input){
	
	$("#modal_default").modal('hide');
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let st_ranap=$("#st_ranap").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let tipe_nama='';
	if (tipe_input=='1'){
		tipe_nama=' S B A R';
	}
	if (tipe_input=='2'){
		tipe_nama=' HASIL KRITIS';
	}
	if (tipe_input=='3'){
		tipe_nama=' NOTES';
	}
	let template='Baru' + tipe_nama;
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/create_template_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
					tipe_input:tipe_input,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});
	// let pendaftaran_id=$("#pendaftaran_id").val();
	// let tglpendaftaran=$("#tglpendaftaran").val();
	// let waktupendaftaran=$("#waktupendaftaran").val();
	// let idpasien=$("#idpasien").val();
	// // alert(idpasien);
	// // return false;
	// let template='Baru';
	// swal({
		// title: "Apakah Anda Yakin ?",
		// text : "Membuat Template "+template+" ?",
		// type : "success",
		// showCancelButton: true,
		// confirmButtonText: "Ya",
		// confirmButtonColor: "#34a263",
		// cancelButtonText: "Batalkan",
	// }).then(function() {
		// $("#cover-spin").show();
		// $.ajax({
			// url: '{site_url}tkomunikasi/create_template_komunikasi', 
			// dataType: "JSON",
			// method: "POST",
			// data : {
					// pendaftaran_id:pendaftaran_id,
					// pendaftaran_id_ranap:pendaftaran_id_ranap,
					// tglpendaftaran:tglpendaftaran,
					// waktupendaftaran:waktupendaftaran,
					// idpasien:idpasien,
				   // },
			// success: function(data) {
				
				// $("#cover-spin").hide();
				
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				// location.reload();
			// }
		// });
	// });

}

function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	let tipe_input_nama=$("#tipe_input_nama").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment "+tipe_input_nama+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/batal_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/batal_template_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
 $(document).find('.js-summernote').on('summernote.blur', function() {
   if ($("#st_edited").val()=='0'){
	 
	   
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
  });
  

$(".auto_blur").blur(function(){
	// alert('sini');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	var tulbakon= $('input[name="tulbakon"]:checked').map(function() {
	   return this.value;
	}).get();
	// alert(tulbakon);
	
	
	if ($("#tipe_input").val()=='1'){
		if ($("#ppa_pemberi").val()=='' || $("#ppa_pemberi").val()==null){
				sweetAlert("Maaf...", "Tentukan Pemberi Perintah", "error");
				return false;

		}
		if (tulbakon==''){
			sweetAlert("Maaf...", "Tentukan Tulbakon", "error");
			return false;

		}
	}
	if ($("#tipe_input").val()=='2'){
		if ($("#dilaporkan_ppa").val()==''){
			sweetAlert("Maaf...", "Tentukan Dilaporkan Kepada", "error");
			return false;

		}
		if ($("#jenis_laporan").val()==''){
			sweetAlert("Maaf...", "Tentukan Jenis Laporan", "error");
			return false;

		}
	}
	let tipe_input_nama=$("#tipe_input_nama").val();
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen "+tipe_input_nama+"?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}
	$("#modal_savae_template_assesmen").modal('hide');
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
$(".opsi_change").change(function(){
	// console.log('OPSI CHANTE')
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});
$('.tulbakon').on('click', function (e) {
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
})
function simpan_assesmen(){
	if (load_awal_assesmen==false){
		if (status_assemen !='2'){
		
// alert('sini');
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tanggallaporan=$("#tanggallaporan").val();
		let waktulaporan=$("#waktulaporan").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		let ppa_pemberi=$("#ppa_pemberi").val();
		
		var tulbakon= $('input[name="tulbakon"]:checked').map(function() {
		   return this.value;
		}).get();
		
		console.log(tulbakon);
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tkomunikasi/save_komunikasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						tanggallaporan:tanggallaporan,
						waktulaporan:waktulaporan,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						situation : $("#situation").val(),
						background : $("#background").val(),
						assemen : $("#assemen").val(),
						recomendation : $("#recomendation").val(),
						dilaporkan_ppa : $("#dilaporkan_ppa").val(),
						jenis_laporan : $("#jenis_laporan").val(),
						nilai_kritis : $("#nilai_kritis").val(),
						notes : $("#notes").val(),
						tulbakon : tulbakon,
						ppa_pemberi : ppa_pemberi,
						// intervensi : $("#intervensi").val(),
						// evaluasi : $("#evaluasi").val(),
						// reassesmen : $("#reassesmen").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
}
	
	
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					// ],
				ajax: { 
					url: '{site_url}tkomunikasi/list_index_template_komunikasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 // $("#index_template_assemen thead").remove();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tkomunikasi/edit_template_komunikasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let profesi_id_filter=$("#profesi_id_filter").val();
		let st_verifikasi=$("#st_verifikasi").val();
		let st_pelaksanaan=$("#st_pelaksanaan").val();
		let st_owned=$("#st_owned").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let jenis_inputan=$("#jenis_inputan").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				"paging": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						// {  className: "text-center", targets:[1,2,3,5,6,7] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "10%", "targets": [1,2,3,5] },
						 // { "width": "15%", "targets": [4] },
					// ],
					"dom": '<"row"<"col-sm-6"l<"dt-filter pull-left push-5-l"><"clearfix">><"col-sm-6"f>>tip',
				ajax: { 
					url: '{site_url}tkomunikasi/list_history_pengkajian_komunikasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							st_owned:st_owned,
							st_verifikasi:st_verifikasi,
							st_pelaksanaan:st_pelaksanaan,
							// profesi_id:profesi_id_filter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							jenis_inputan:jenis_inputan,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 $("#index_history_kajian thead").remove();
				 },
				initComplete: function(settings, json){
					// console.log(settings);
					console.log(json.data_jml_verif_sbar);
					console.log(json.data_jml_verif_kritis);
					if (json.data_jml_verif_sbar>0){
					$(".dt-filter").append('<button onclick="verif_all()" title="Verifikasi SBAR" type="button" class="btn btn-primary pull-5-l"><i class="si si-check"></i> VERIFIKASI ALL SBAR <span class="label label-danger">'+json.data_jml_verif_sbar+'</span></button>');
					}
					if (json.data_jml_verif_kritis>0){
					$(".dt-filter").append(' <button onclick="verif_all_kritis()" title="Verifikasi HASIL KRITIS" type="button" class="btn btn-info pull-5-l"><i class="si si-check"></i> VERIFIKASI ALL HASIL KRITIS <span class="label label-danger">'+json.data_jml_verif_kritis+'</span></button>');
						
					}
				},				 
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		// $("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/save_edit_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id_ranap=data.pendaftaran_id_ranap;
					let pendaftaran_id=data.pendaftaran_id;
					let st_ranap=data.st_ranap;
					$("#cover-spin").show();
					if (st_ranap=='1'){
					window.location.href = "<?php echo site_url('tkomunikasi/tindakan/"+pendaftaran_id_ranap+"/1/erm_komunikasi/input_komunikasi'); ?>";
						
					}else{
						
					window.location.href = "<?php echo site_url('tkomunikasi/tindakan/"+pendaftaran_id+"/0/erm_komunikasi/input_komunikasi'); ?>";
					}
				}
			}
		});
	}
	function hapus_record_assesmen(){
		$("#modal_hapus").modal('hide');
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tkomunikasi/hapus_record_komunikasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tkomunikasi/hapus_record_komunikasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	function verifikasi_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Verifikasi Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tkomunikasi/verifikasi_komunikasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					// $('#index_history_kajian').DataTable().ajax.reload( null, false );
					list_history_pengkajian();
				}
			});
		});

	}
	function laksanakan_assesmen(id){
		
		$("#assesmen_id_pelaksanaan").val(id);
		$("#modal_pelaksanaan").modal('show');

	}
	function simpan_pelaksanaan(){
		if ($("#waktu_pelaksanaan").val()==''){
			sweetAlert("Maaf...", "Tentukan Waktu Pelaksanaan", "error");
			return false;
		}
		if ($("#time_pelaksanaan").val()==''){
			sweetAlert("Maaf...", "Tentukan Waktu Pelaksanaan", "error");
			return false;
		}
		if ($("#ket_pelaksanaan").val()==''){
			sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
			return false;
		}

		$("#cover-spin").show();
		$("#modal_pelaksanaan").modal('hide');
		$.ajax({
			url: '{site_url}tkomunikasi/simpan_pelaksanaan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id_pelaksanaan").val(),
					ket_pelaksanaan:$("#ket_pelaksanaan").val(),
					waktu_pelaksanaan:$("#waktu_pelaksanaan").val(),
					time_pelaksanaan:$("#time_pelaksanaan").val(),
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				$('#index_history_kajian').DataTable().ajax.reload( null, false );
				// location.reload();
			}
		});
		
	}
	function verif_all(){
		let idpasien=$("#idpasien").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Verifikasi SBAR Semua?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tkomunikasi/verifikasi_komunikasi_all', 
				dataType: "JSON",
				method: "POST",
				data : {
						idpasien:idpasien,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_history_pengkajian();
				}
			});
		});

	}
	function verif_all_kritis(){
		let idpasien=$("#idpasien").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Verifikasi Hasil Kritis Semua?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tkomunikasi/verifikasi_komunikasi_all_kritis', 
				dataType: "JSON",
				method: "POST",
				data : {
						idpasien:idpasien,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_history_pengkajian();
				}
			});
		});

	}
	function goBack() {
	  window.history.back();
	}

</script>