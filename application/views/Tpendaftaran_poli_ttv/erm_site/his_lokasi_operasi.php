<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.input-keterangan{
		background-color:#d0f3df;
	}
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	}	
	.text-bold{
        font-weight: bold;
      }
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
	
</style>

<?if ($menu_kiri=='his_lokasi_operasi' || $menu_kiri=='his_lokasi_operasi_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_lokasi_operasi' || $menu_kiri=='his_lokasi_operasi_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_lokasi_operasi=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_site">
	<?if ($st_lihat_lokasi_operasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
						<div class="col-md-12">
							<?if($st_lihat_lokasi_operasi=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header_ina}</h4>
								<h5 class="font-w700 push-5 text-center text-primary">{judul_header_eng}</h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<button class="btn btn-default btn_back menu_click" onclick="goBack()" type="button"><i class="fa fa-reply"></i> Kembali</button>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) <?=$st_input_assesmen?></label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<label for="diagnosa">Diagnosa<?=$diagnosa_asal?></label>
								<input id="diagnosa" class="<?=($diagnosa!=$diagnosa_asal?'edited':'')?> form-control"  type="text"   value="{diagnosa}" placeholder="" >
							</div>
							<div class="col-md-4 ">
								<label for="rencana_tindakan">Rencana Tindakan</label>
								<input id="rencana_tindakan" class="<?=($rencana_tindakan!=$rencana_tindakan_asal?'edited':'')?> form-control"  type="text"   value="{rencana_tindakan}" placeholder="" >
							</div>
							<div class="col-md-2 ">
								<label for="rencana_tindakan">Tanggal Operasi</label>
								<div class="input-group date">
									<input id="tanggal_operasi"  class="<?=($tanggal_operasi!=$tanggal_operasi_asal?'edited':'')?> js-datepicker form-control " type="text"data-date-format="dd/mm/yyyy"  placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= ($tanggal_operasi?HumanDateShort($tanggal_operasi):'') ?>" required>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
								
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class=" text-primary" >STATUS LOKALIS</label>
							</div>
							
						</div>
						<input class="form-control " readonly type="hidden" id="src_gambar"  value="{upload_path}anatomi/{gambar_tubuh}" placeholder="" >
						<input class="form-control " readonly type="hidden" id="base_url"  value="{upload_path}anatomi/" placeholder="" >
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-sm-4">
									<label>Template</label>
									<select id="gambar_id"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(get_all('tranap_lokasi_operasi_lokalis_gambar',array('assesmen_id'=>$assesmen_id))as $r){?>
										<option value="<?=$r->id?>" <?=($r->st_default=='1'?'selected':'')?>><?=$r->nama_template_lokasi?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4">
									<label>Jenis Drawing</label>
									<select id="jenis_drawing"  class="js-select2 form-control input-default" style="width: 100%;" data-placeholder="Choose one..">
										<option value="free_draw" <?=($jenis_drawing=='free_draw'?'selected':'')?>>Free Draw</option>
										<option value="segitiga" <?=($jenis_drawing=='segitiga'?'selected':'')?>>Triangle</option>
										<option value="kotak" <?=($jenis_drawing=='kotak'?'selected':'')?>>Box</option>
										<option value="bulat" <?=($jenis_drawing=='bulat'?'selected':'')?>>Round</option>
										<option value="silang" <?=($jenis_drawing=='silang'?'selected':'')?>>Cross</option>
										<option value="diamond" <?=($jenis_drawing=='diamond'?'selected':'')?>>Diamond</option>
										<option value="bintang" <?=($jenis_drawing=='bintang'?'selected':'')?>>Star</option>
										<option value="target" <?=($jenis_drawing=='target'?'selected':'')?>>Crosshairs</option>
										<option value="hati" <?=($jenis_drawing=='hati'?'selected':'')?>>Heart</option>
										<option value="bendera" <?=($jenis_drawing=='bendera'?'selected':'')?>>Flag</option>
										<option value="tempe" <?=($jenis_drawing=='tempe'?'selected':'')?>>Bookmark</option>
									</select>
								</div>
								<div class="col-md-4">
									<label>Warna</label>
									<div class="js-colorpicker input-group colorpicker-element">
										<input class="form-control input-default" readonly type="text" id="warna" name="warna" value="<?=$warna?>">
										<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<canvas id="canvas" width="1300" height="768" ></canvas>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered" id="simbol-list">
											<thead>
												<tr>
													<th style="width:5%" class="text-center">No.</th>
													<th style="width:10%" class="text-center">Simbol</th>
													<th style="width:75%">Keterangan</th>
													<th style="width:10%" class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<div class="col-md-12">
									<label>Keterangan</label>
									<input class="form-control <?=($txt_keterangan!=$txt_keterangan_asal?'edited':'')?>" type="text" id="txt_keterangan"  value="{txt_keterangan}" placeholder="Keterangan" >
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label><?=$paragraf_1_ina?></label><br><label class="text-muted"><i><?=$paragraf_1_eng?></label></i>
							</div>
								
						</div>
						<div class="form-group">
							<div class="col-md-12 text-center">
								<table width="100%">
									<tr>
										<td class="center" style="width:33%">
											<label><?=$pasien_ina?></label><br><label class="text-muted"><i><?=$pasien_eng?></label></i>
										</td>
										<td class="center" style="width:33%">
										
										</td>
										<td class="center" style="width:33%">
											<label><?=$pasien_ina?></label><br><label class="text-muted"><i><?=$pasien_eng?></label></i>
										</td>
									</tr>
									<tr>
										<td class="center" style="width:33%">
											<?if ($ttd_pasien){?>
												<div class="img-container fx-img-rotate-r text-center">
													<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_pasien?>" alt="" title="">
													<div class="img-options">
														<div class="img-options-content">
															<div class="btn-group btn-group-sm">
																<a class="btn btn-default" onclick="modal_ttd()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																<a class="btn btn-default btn-danger" onclick="hapus_ttd('ttd_pasien')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
															</div>
														</div>
													</div>
												</div>
											<?}else{?>
												<button class="btn btn-sm btn-success" onclick="modal_ttd()" id="btn_ttd_lm" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<?}?>
										</td>
										<td class="center" style="width:33%">
										
										</td>
										<td class="center" style="width:33%">
											<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id?$petugas_id:$login_ppa_id)?>" alt="" title="">
										</td>
									</tr>
									<tr>
										<td class="center" style="width:33%">
											<label>(<?=($ttd_nama?$ttd_nama:'BELUM TANDA TANGAN')?>)</label>
										</td>
										<td class="center" style="width:33%">
										
										</td>
										<td class="center" style="width:33%">
											<label>(<?=get_nama_ppa($petugas_id)?>)</label>
										</td>
									</tr>
								</table>
							</div>
						</div>
						
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<div class="form-group">
							<div class="col-md-4 text-center">
								
							</div>
							<div class="col-md-4 ">
							</div>
							<div class="col-md-4 text-center">
								
							</div>
								
						</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer_ina?></h5>
								<label class="text-primary" ><?=$judul_footer_eng?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
const font      = '400 30px "Font Awesome 6 Free"';
const font_2    = '900 40px "Font Awesome 6 Free"';
const font_3    = '900 30px "Font Awesome 6 Free"';
const marker_type = [{key:'bulat',content:'\uf10c',class_name:'fa-circle',font:font},
                     {key:'segitiga',content:'\uf0d8',class_name:'fa-caret-up',font:font_2},
                     {key:'kotak',content:'\uf096',class_name:'fa-square',font:font},
                     {key:'silang',content:'\uf00d',class_name:'fa-times',font:font_3},
                     {key:'diamond',content:'\uf219',class_name:'fa-diamond',font:font_3},
                     {key:'tempe',content:'\uf097',class_name:'fa-bookmark',font:font},
                     {key:'bintang',content:'\uf006',class_name:'fa-star',font:font},
                     {key:'target',content:'\uf05b',class_name:'fa-crosshairs',font:font_3},
                     {key:'hati',content:'\uf08a',class_name:'fa-heart',font:font},
                     {key:'bendera',content:'\uf11d',class_name:'fa-flag',font:font},
                     {key:'free_draw',content:'',class_name:'fa-pencil',font:font_3},
                    ];

var selected_marker = null;
<?if ($assesmen_id){?>
	

var canvas      = document.getElementById('canvas');
var context     = canvas.getContext("2d");

context.lineWidth=2;
var st_drawing  = false;
var path     = [];
// const change_marker = document.querySelectorAll('.change-marker');
// change_marker.forEach(el => el.addEventListener('click', event => {
    // selected_marker = marker_type.find(o => o.key === el.id);
    
// }));
$("#jenis_drawing").on("change", function(){
	set_marker();
}); 


function set_marker(){
	 // $("#jenis_drawing").val();
	 selected_marker = marker_type.find(({ key }) => key === $("#jenis_drawing").val());
	 // console.log(Markers);
	 // selected_marker = marker_type.find(o => o.key === el.id);
}

document.fonts.load(font).then((_) => {
    
    // Map sprite
    var mapSprite   = new Image();
    // mapSprite.src   = "human_body.jpeg";
    mapSprite.src   = $("#src_gambar").val();
    
    // selected_marker=$("#jenis_drawing").val();
	set_marker();
    // console.log(selected_marker);
    
    var Marker = function () {
        this.Width = 20;
        this.Height = 20;
        this.XPos = 0;
        this.YPos = 0;
    }

    var Markers = new Array();
	var str_key='';
	var str_path='';
	var selected_marker2=null;
	<? foreach($list_lokalis as $r){?>
	
	str_path='<?=$r->path?>';
	str_key='<?=$r->key?>';
	selected_marker2 = marker_type.find(({ key }) => key === str_key);
		 var marker      = new Marker();
		marker.XPos     = '<?=$r->XPos?>';
		marker.YPos     = '<?=$r->YPos?>';
		marker.key      = '<?=$r->key?>';
		marker.content  = selected_marker2.content;
		marker.warna    = '<?=$r->warna?>';
		if (str_path){
			marker.path  = JSON.parse("[" + str_path + "]");
		}
		// marker.path    = <?=($r->path)?>;
			 
		marker.keterangan    = '<?=$r->keterangan?>';
		marker.class_name    = '<?=$r->class_name?>';
		marker.font    = selected_marker2.font;
		marker.path    = <?= ($r->path)? "JSON.parse('".$r->path."')" : $r->path?>;

		Markers.push(marker);
		load_data();
		console.log(Markers);
	<?}?>
    var mouseClicked = function (mouse) {
		if (status_assemen!='2'){
			if(selected_marker != null){
				var rect        = canvas.getBoundingClientRect();
				if(selected_marker.key == 'free_draw'){
					st_drawing = true;
					context.beginPath();
					context.moveTo(mouse.x - rect.left, mouse.y - rect.top);
					
				}else{
					// Get corrent mouse coords
					
					var mouseXPos = (mouse.x - rect.left)+10;
					var mouseYPos = (mouse.y - rect.top) +30;

					
					// Move the marker when placed to a better location
					const warna      = document.getElementById('warna');
					var marker      = new Marker();
					marker.XPos     = mouseXPos - (marker.Width / 2);
					marker.YPos     = mouseYPos - marker.Height;
					marker.key      = selected_marker.key;
					marker.content  = selected_marker.content;
					marker.warna    = warna.value;
					marker.keterangan    = '';
					marker.class_name    = selected_marker.class_name;
					marker.font    = selected_marker.font;
					marker.path    = '';
					

					Markers.push(marker);
				}
			}
        }
    }

    // Add mouse click event listener to canvas
    canvas.addEventListener("mousedown", mouseClicked, false);
    
    canvas.onmousemove = draw_line;
    canvas.onmouseup = stopDrawing;

    var firstLoad = function () {
        context.font = "15px Georgia";
        context.textAlign = "center";
    }

    firstLoad();

    var main = function () {
        draw();
    };
    
    //\uf10c bulat
    //\uf0d8 segitiga
    //\uf096 kotak
    //\uf00d silang
    //\uf219 diamond

    var draw = function () {
        if(st_drawing == false){
			
            // Clear Canvas
            context.fillStyle = "#000";
            context.fillRect(0, 0, canvas.width, canvas.height);

            // Draw map
            // Sprite, X location, Y location, Image width, Image height
            // You can leave the image height and width off, if you do it will draw the image at default size
          
				// console.log(Markers.length);
            context.drawImage(mapSprite, 0, 0, 1300, 768);
            
            // Draw markers
            for (var i = 0; i < Markers.length; i++) {
              // note that I wasn't able to find \uF006 defined in the provided CSS
              // falling back to fa-bug for demo
                
                var tempMarker = Markers[i];
                // console.log(tempMarker);
                context.font = tempMarker.font;
                context.fillStyle = tempMarker.warna;
                // Draw marker
                //context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);
                let markerText = i + 1;
                let textMeasurements = context.measureText(markerText);
                
                
                if(tempMarker.key != 'free_draw'){
                    context.fillText(tempMarker.content, tempMarker.XPos, tempMarker.YPos);
                    // Calculate postion text
                    
                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 50, textMeasurements.width, 20);
                    
                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, tempMarker.XPos, tempMarker.YPos - 35);
                }else{
                    context.beginPath();
                    const begin_path =  tempMarker.path[0];
                    context.moveTo(begin_path.x, begin_path.y);
                    context.strokeStyle  = tempMarker.warna;
                    tempMarker.path.map( (p)=> {
                         context.lineTo(p.x, p.y);
                         context.stroke();
                    })
                    
                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(begin_path.x - (textMeasurements.width / 2), begin_path.y - 25, textMeasurements.width, 20);
                    
                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, begin_path.x, begin_path.y - 10);
                    
                }
            } 
        }
       
    };
    
    function draw_line(e){
        if (st_drawing == true && selected_marker.key == 'free_draw'){
             const rect        = canvas.getBoundingClientRect();
             const x = e.x - rect.left;
             const y = e.y - rect.top;
             path.push({x:x,y:y});
             const warna      = document.getElementById('warna');
             context.strokeStyle  = warna.value;
             console.log(context);
             console.log(x+' : '+y);
             context.lineTo(x, y);
             context.stroke();
        }
    }
    
    function stopDrawing() {
        if(selected_marker != null && selected_marker.key == 'free_draw'){
            if(path.length > 0){
                const warna      = document.getElementById('warna');
                const marker        = new Marker();
                marker.key          = selected_marker.key;
                marker.content      = '';
                marker.path         = path;
                marker.warna        = warna.value;
                marker.keterangan   = '';
                marker.class_name    = selected_marker.class_name;
                Markers.push(marker);
            }
            st_drawing = false;
            path = [];
        }
		// console.log('STIP');
        load_data();
   }
   $(document).on("blur",".input-keterangan",function(){	
		let index=$(this).attr("data-id");
		Markers[index].keterangan=$(this).val();
		simpan_status_lokalis();
		// $(this).closest('tr').find(".isiField").
	   
   });
   // $(".input-keterangan").blur(function(){
	// });

   function load_data(){
    const table = document.getElementById('simbol-list');
    let table_content = '';
    table.querySelector('tbody').innerHTML = '';
    for (var i = 0; i < Markers.length; i++) {
        const row = Markers[i];
        table_content += '<tr>';
        table_content +='<td>'+(i+1)+'</td>';
        table_content +='<td class="text-center"><i class="fa '+row.class_name+'"></i></td>';
        table_content +='<td><input data-id="'+ i +'" class="input-keterangan form-control input-sm"  type="text" value="'+row.keterangan+'"/></td>';
        table_content +='<td><button class="hapus-data btn btn-sm btn-danger" data-id="'+ i +'"><i class="fa fa-trash"></i></button></td>';
        table_content +='</tr>';
    }
    table.querySelector('tbody').innerHTML = table_content;
    // console.log(Markers);\
	disabel_edit();
    simpan_status_lokalis();
    const hapus_data = document.querySelectorAll('.hapus-data');
    hapus_data.forEach(el => el.addEventListener('click', event => {
        Markers.splice(el.dataset.id, 1);
        load_data();
    }));
   }



function convertCanvasToImage(aaa) {

    something = aaa[0].toDataURL("image/png");

}

function simpan_status_lokalis(){// alert($("#keluhan_utama").val());
	   
	let canvas = $("#canvas")[0];
	let gambar_final = canvas.toDataURL("image/png");

	// $("#elememt-to-write-to").html('<img src="'+gambar_final+'"/>');
	// console.log(gambar_final);
	let assesmen_id=$("#assesmen_id").val();
	let gambar_id=$("#gambar_id").val();
	const jsonString = JSON.stringify(Object.assign({}, Markers));
	const json_obj = JSON.parse(jsonString);
	console.log(json_obj);
	// $("#cover-spin").show();
	// if (Markers.length>0){
		if (assesmen_id){
			$.ajax({
				url: '{site_url}tpendaftaran_ranap_erm/simpan_status_lokalis_lokasi_operasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						gambar_id:gambar_id,
						gambar_final:gambar_final,
						jsonString:json_obj,
						
					},
				success: function(data) {
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Default Warna'});
				}
			});
		}
	// }
	
}
    setInterval(main, (1000 / 60)); // Refresh 60 times a second
    $("#gambar_id").on("change", function(){
		load_gambar();
	}); 
    function load_gambar(){
		let assesmen_id=$("#assesmen_id").val();
		let gambar_id=$("#gambar_id").val();
		let status_assemen=$("#status_assemen").val();
		let base_url=$("#base_url").val();
		
		var canvas      = document.getElementById('canvas');
		var context     = canvas.getContext("2d");
			$("#cover-spin").show();	
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_erm/load_gambar_lokasi_operasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					gambar_id:gambar_id,
					status_assemen:status_assemen,
					
				},
			success: function(data) {
				$("#src_gambar").val(base_url+data.gambar_tubuh);
				// alert('sini');
				st_drawing  = false;
				// Map sprite
				mapSprite   = new Image();
				// mapSprite.src   = "human_body.jpeg";
				// context.canvas.width = 50; 
				// context.canvas.height = 50; 
				// mapSprite.onload = function() { 
					// context.drawImage(mapSprite, 0, 0,50,50); 
				// }; 

				mapSprite.src   = $("#src_gambar").val();
				// context.mapSprite(mapSprite, 0, 0,1300 ,900);
				 Markers = new Array();
				var str_key='';
				var selected_marker2=null;
				
				$.each(data.list_lokalis, function (i,r) {
					str_key=r.key;
					selected_marker2 = marker_type.find(({ key }) => key === str_key);
					 var marker      = new Marker();
					marker.XPos     = r.XPos;
					marker.YPos     = r.YPos;
					marker.key      = r.key;
					marker.content  = selected_marker2.content;
					marker.warna    = r.warna;
					marker.keterangan    = r.keterangan;
					marker.class_name    = r.class_name;
					marker.font     = selected_marker.font;
					marker.path     = JSON.parse(r.path);

					Markers.push(marker);
					// console.log(Markers);
					// $('#nojabatan')
					// .append('<option value="' + pegawai_item.nojabatan + '">' + pegawai_item.namajabatan + '</option>');
				});
				 const table = document.getElementById('simbol-list');
					let table_content = '';
					table.querySelector('tbody').innerHTML = '';
					table.querySelector('tbody').innerHTML = table_content;
					load_data();
					$("#cover-spin").hide();
				draw();
				// alert('sini');
			}
		});
	}
})
<?}?>
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	let st_ranap=$("#st_ranap").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
						// { "width": "5%", "targets": [0] },
						// { "width": "20%", "targets": [1] },
						// { "width": "15%", "targets": [3] },
						// { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_lokasi_operasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
						}
			},
			"drawCallback": function( settings ) {
					$("#cover-spin").hide();
				}  
		});
	
}
$(document).ready(function() {
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		list_index_history_edit();
		load_awal_assesmen=false;
	}
});

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $(".btn_back").removeAttr('disabled');
		 $('.edited').css('background', '#fff2f1');
	}
}

</script>