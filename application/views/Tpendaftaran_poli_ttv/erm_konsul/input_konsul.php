<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   
</style>

<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_konsul' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($konsul_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_cetak='';
					$disabel_input='';
					$disabel_input_ttv='';
					
					$login_pegawai_id=$this->session->userdata('login_pegawai_id');
					$login_nama_ppa=$this->session->userdata('login_nama_ppa');	
					$login_tipepegawai=$this->session->userdata('login_tipepegawai');	
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Konsultasi <?=($status_konsul=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="konsul_id" value="{konsul_id}"> 
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_konsul" value="<?=$status_konsul?>" >	
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >								
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<div class="col-md-12">
							<h4 class="font-w700 push-5 text-center text-primary">KONSULTASI</h4>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_konsul=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_konsul" style="display:<?=($status_konsul=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_konsul=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_konsul" style="display:<?=($status_konsul=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_konsul=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_konsul" style="display:<?=($status_konsul=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($konsul_id==''){?>
									<?php if (UserAccesForm($user_acces_form,array('1797'))){ ?>
									<button class="btn btn-primary" id="btn_create_konsul" onclick="create_konsul()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($konsul_id){?>
										<?if ($status_konsul=='1'){?>
										<button class="btn btn-success" id="btn_simpan_konsul" onclick="close_konsul()" type="button"><i class="fa fa-send-o"></i> Kirim Permintaan</button>
										<button class="btn btn-danger" id="btn_hapus_konsul" onclick="batal_konsul()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										<?if ($konsul_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
						<?
							$q="SELECT M.id,M.nama FROM mpoliklinik_konsul H INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik";
							$list_poli=$this->db->query($q)->result();
						?>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
								<?if ($konsul_id==''){?>
									<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
									<select tabindex="8" id="idpoli" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" <?=($idpoli == '0' ? 'selected="selected"' : '')?>>Pilih Poliklinik</option>
										<?foreach($list_poli as $row){?>
										<option value="<?=$row->id?>" <?=($idpoli == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
									<label >Poliklinik PPA</label>
								</div>
								</div>
								</div>
								<?}?>
								</div>
							</div>
							<?
								
								$where='';
								if ($login_tipepegawai=='2'){
									$where=" AND id='$login_pegawai_id'";
									if ($konsul_id==''){
										$iddokter_dari=$login_pegawai_id;
									}
								}else{
									if ($konsul_id==''){
										$iddokter_dari='';
									}
								}
								
								$q="select *FROM mdokter where status='1'".$where;
								$list_dokter_dari=$this->db->query($q)->result();
							?>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<select id="iddokter_dari" name="iddokter_dari" <?=($konsul_id?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="" selected>Pilih Opsi</option>
													<?foreach($list_dokter_dari as $row){?>
													<option value="<?=$row->id?>" <?=($iddokter_dari==$row->id?'selected':'')?>><?=$row->nama?></option>
													<?}?>
												</select>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama (PPA) Pembuat</label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($konsul_id!=''){?>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										
										<label for="konsul_alasan">Tujuan Konsultasi </label>
										<select tabindex="8" id="idpoliklinik" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="0" <?=($idpoliklinik == '0' ? 'selected="selected"' : '')?>>Pilih Tujuan Konsultasi</option>
											<?foreach($list_poli as $row){?>
											<option value="<?=$row->id?>" <?=($idpoliklinik == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="konsul_diminta">Spesifik Dokter</label>
										<select tabindex="8" id="iddokter" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Dokter" required>
											<option value="0" <?=($iddokter == '0' ? 'selected="selected"' : '')?>>Dokter Belum dipilih</option>
											<?foreach($list_dokter_all as $row){?>
											<option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										<label for="konsul_alasan">Alasan Konsultasi</label>
										<textarea class="form-control js-summernote auto_blur" id="konsul_alasan"  rows="2" placeholder="Alasan"> <?=$konsul_alasan?></textarea>
									</div>
									<div class="col-md-6 ">
										<label for="konsul_diminta">Konsul Yang Diminta</label>
										<textarea class="form-control js-summernote auto_blur" id="konsul_diminta"  rows="2" placeholder="Alasan"> <?=$konsul_diminta?></textarea>
									</div>
									
								</div>
							</div>
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
	</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_konsul=$("#status_konsul").val();
var load_awal_konsul=true;
var nama_template;
var idpasien=$("#idpasien").val();
var pendaftaran_id=$("#pendaftaran_id").val();
var before_edit;
$("#idpoliklinik").change(function(){
		$.ajax({
			url: '{site_url}tkonsul/find_dokter/',
			dataType: "json",
			method: "POST",
			data : {
					idpoliklinik:$(this).val(),
				   },
			success: function(data) {
				// alert(data);
				$("#iddokter").empty();
				$("#iddokter").append(data);
			}
		});

});
$("#idpoli").change(function(){
		$.ajax({
			url: '{site_url}tkonsul/find_dokter/',
			dataType: "json",
			method: "POST",
			data : {
					idpoliklinik:$(this).val(),
				   },
			success: function(data) {
				// alert(data);
				$("#iddokter_dari").empty();
				$("#iddokter_dari").append(data);
			}
		});

});
function create_awal_template(){
	$("#modal_default_template").modal('show');
	$("#mnyeri_id_2").select2({
		dropdownParent: $("#modal_default_template")
	  });
}
$(document).ready(function() {
	// alert(status_konsul);
	// document.getElementById("nadi").style.color = "#eb0404";
	
	disabel_edit();
	// set_ttd_konsul();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	// $("#tabel_rencana_asuhan tbody").empty();
	let konsul_id=$("#konsul_id").val();
	$('.js-summernote').summernote({
		  height: 120,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

	load_awal_konsul=false;
});

function disabel_edit(){
	if (status_konsul=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}

function create_konsul(){
	
	$("#modal_default").modal('hide');
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	let iddokter_dari=$("#iddokter_dari").val();
	if (iddokter_dari==''){
		sweetAlert("Maaf...", "Tentukan Dokter Pembuat Konsul", "error");
		return false;

	}
	if (iddokter_dari=='0'){
		sweetAlert("Maaf...", "Tentukan Dokter Pembuat Konsul", "error");
		return false;

	}
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Konsultasi "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkonsul/create_konsul', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					st_ranap:st_ranap,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
					iddokter_dari:iddokter_dari,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_konsul(){
	let konsul_id=$("#konsul_id").val();
	let st_edited=$("#st_edited").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Pengajuan Konsul ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkonsul/batal_konsul', 
			dataType: "JSON",
			method: "POST",
			data : {
					konsul_id:konsul_id,
					st_edited:st_edited,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

 $(document).find('.js-summernote').on('summernote.blur', function() {
   if ($("#st_edited").val()=='0'){
	 
	   
		$(this).removeClass('input_edited');
		simpan_konsul();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
  });
  

$(".auto_blur").blur(function(){
	// alert('sini');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_konsul();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_konsul(){
	let ket='';
	// alert($("#iddokter").val());
	if ($("#iddokter").val()=='0'){
		ket=' Tanpa Spesifik Dokter';
	}else{
		ket='Ke ' + $("#iddokter option:selected").text();
	}
	if ($("#idpoliklinik").val()=='0' && $("#iddokter").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Tujuan Poliklinik Konsul", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Mengirim Dokument "+ket+"?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_konsul").val(2);
			
			simpan_konsul();
		});		
	
}

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_konsul();
	}
});
$(".auto_blur_tgl").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_konsul();
	}
});
$(".auto_blur_tgl").blur(function(){
	if ($("#st_edited").val()=='0'){
		simpan_konsul();
	}
});
function simpan_konsul(){
	if (load_awal_konsul==false){
		if (status_konsul !='2'){
		
// alert('sini');
		let konsul_id=$("#konsul_id").val();
		// alert(konsul_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		if (konsul_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}tkonsul/save_konsul', 
				dataType: "JSON",
				method: "POST",
				data : {
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						konsul_id:$("#konsul_id").val(),
						nama_template:nama_template,
						status_konsul:$("#status_konsul").val(),
						subjectif : $("#subjectif").val(),
						idpoliklinik : $("#idpoliklinik").val(),
						iddokter : $("#iddokter").val(),
						konsul_alasan : $("#konsul_alasan").val(),
						konsul_diminta : $("#konsul_diminta").val(),
						konsul_jawaban : $("#konsul_jawaban").val(),
						konsul_anjuran : $("#konsul_anjuran").val(),
						status_konsul : $("#status_konsul").val(),
						st_edited:$("#st_edited").val(),
						
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_konsul=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_konsul=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
}

	
</script>