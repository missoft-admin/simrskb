<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   
</style>

<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='jawab_konsul' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($konsul_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					
					$disabel_cetak='';
					$disabel_input='';
					$disabel_input_ttv='';
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Jawab Konsultasi <?=($status_konsul=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="konsul_id" value="{trx_id}"> 
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_konsul" value="<?=$status_konsul?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="login_pegawai_id" value="<?=$login_pegawai_id?>" >		
						<input type="hidden" id="iddokter_dari_edit" value="" >		
						<input type="hidden" id="iddokter_jawab_edit" value="" >
						<div class="col-md-4 ">
						</div>
						
					</div>
					<div id="div_daftar">
					<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">DAFTAR KONSULTASI</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;" hidden>
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Input</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
										<div class="col-md-8">
										<?
											$login_pegawai_id=$this->session->userdata('login_pegawai_id');
											$login_nama_ppa=$this->session->userdata('login_nama_ppa');
											$q="SELECT M.id,M.nama 
											FROM mpoliklinik_konsul_dokter H
											INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik
											WHERE H.iddokter='$login_pegawai_id'";
											$list_poli=$this->db->query($q)->result();
										?>
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<?foreach($list_poli as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<?
										$login_pegawai_id=$this->session->userdata('login_pegawai_id');
										$login_nama_ppa=$this->session->userdata('login_nama_ppa');
									?>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="<?=$login_pegawai_id?>" selected><?=$login_nama_ppa?></option>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Status</label>
										<div class="col-md-8">
											<select id="st_jawab" name="st_jawab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- SEMUA PENGAJUAN -</option>
												<option value="1" >TELAH ADA JAWABAN</option>
												<option value="0" >MENUNGGU JAWABAN</option>
											
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_konsul()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">No Pendaftaran</th>
												<th width="10%">No Registrasi Konsul</th>
												<th width="10%">Dari Dokter</th>
												<th width="10%">Tujuan Poliklinik</th>
												<th width="15%">Tujuan Dokter</th>
												<th width="15%">Alasan Konsul</th>
												<th width="15%">Status Konsul</th>
												<th width="10%">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						</div>
						<hr class="push-5-b">
						<div class="row" id="div_history">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">DATA KONSULTASI</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 col-xs-12">
									<div class="col-md-3 col-xs-6">
										<div class="form-material input-group date">
											<input tabindex="2" type="text" disabled class="form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
											<label for="tanggaldaftar">Tanggal </label>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-3 col-xs-6">
										<div class="form-material input-group">
											<input tabindex="3" type="text" disabled <?=$disabel_input?> class="form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
											<label for="waktupendaftaran">Waktu</label>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12">
										
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="col-md-12">
												<div class="form-material">
													<div class="input-group">
														<input class="form-control" disabled type="text" readonly value="" id="nama_ppa" placeholder="Nama PPA" required>
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
													</div>
													<label >Nama Profesional Pemberi Asuhan (PPA) </label>
												</div>
											</div>
										
										</div>
									</div>
									
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-6 ">
										
										<label for="konsul_alasan">Tujuan Konsultasi </label>
										<input class="form-control" disabled type="text" readonly value="" id="tujuan_poli" >
									</div>
									<div class="col-md-6 ">
										<label for="konsul_diminta">Spesifik Dokter</label>
										<input class="form-control" disabled type="text" readonly value="" id="tujuan_dokter" >
									</div>
									
								</div>
							</div>
							<div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
										<?if (UserAccesForm($user_acces_form,array('2355'))){?>
										<li class="btn_edit_konsul">
                                            <a type="button" class="btn btn-warning btn-sm" onclick="edit_konsul()"><i class="fa fa-pencil"></i> EDIT</a>
                                        </li>
										<?}?>
                                        <li>
                                            <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">FORMULIR PERMINTAAN KONSUL</h3>
                                </div>
                                <div class="block-content">
                                   <div class="form-group">
											<div class="col-md-6 ">
												<label for="konsul_alasan">Alasan Konsultasi</label>
												<textarea class="form-control js-summernote auto_blur" disabled id="konsul_alasan"  rows="2" placeholder="Alasan"> </textarea>
											</div>
											<div class="col-md-6 ">
												<label for="konsul_diminta">Konsul Yang Diminta</label>
												<textarea class="form-control js-summernote auto_blur" disabled id="konsul_diminta"  rows="2" placeholder="Alasan"></textarea>
											</div>
											
									</div>
									<div class="form-group btn_simpan_edit_konsul" hidden>
										<div class="col-md-6 ">
											<button class="btn btn-primary " onclick="simpan_konsul()" type="button"><i class="fa fa-save"></i> Simpan Edit </button>
										</div>
									</div>
                                </div>
                            </div>
							<div class="block block-themed">
                                <div class="block-header bg-success">
                                    <ul class="block-options">
										<?if (UserAccesForm($user_acces_form,array('2355'))){?>
											<li class="btn_edit_jawab">
												<a type="button" class="btn btn-warning btn-sm"  onclick="edit_jawab()"><i class="fa fa-pencil"></i> EDIT</a>
											</li>
										<?}?>
                                        <li>
                                            <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">JAWABAN PERMINTAAN KONSUL</h3>
                                </div>
                                <div class="block-content">
                                   <div class="form-group">
											<div class="col-md-6 ">
												<label for="konsul_alasan">Jawaban Konsultasi</label>
												<textarea class="form-control js-summernote auto_blur" disabled id="konsul_jawaban"  rows="2" placeholder="Alasan"> </textarea>
											</div>
											<div class="col-md-6 ">
												<label for="konsul_diminta">Anjuran</label>
												<textarea class="form-control js-summernote auto_blur" disabled id="konsul_anjuran"  rows="2" placeholder="Alasan"></textarea>
											</div>
											
									</div>
									<div class="form-group btn_simpan_edit_jawab" hidden>
										<div class="col-md-6 ">
											<button class="btn btn-primary " onclick="simpan_jawab()" type="button"><i class="fa fa-save"></i> Simpan Edit </button>
										</div>
									</div>
                                </div>
                            </div>
							<div class="row">
								<div class="col-md-4 ">
								</div>
								<div class="col-md-8 ">
									<div class="pull-right push-10-r">
										<?php if (UserAccesForm($user_acces_form,array('1799'))){ ?>
										<button class="btn btn-primary" id="btn_jawaban" onclick="kirim_jawaban()" type="button"><i class="fa fa-save"></i> Kirim Jawaban </button>
										<button class="btn btn-default"  onclick="kembali_daftar()" type="button"><i class="fa fa-reply"></i> Kembali </button>
										<?}?>
										
									</div>
									
								</div>
							</div>
					</div>	
					
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
	</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_konsul=$("#status_konsul").val();
var load_awal_konsul=true;
var nama_template;
var idpasien=$("#idpasien").val();
var pendaftaran_id=$("#pendaftaran_id").val();
var before_edit;
function list_history_konsul(){
	let assesmen_id=$("#assesmen_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let mppa_id=$("#mppa_id").val();
	let iddokter=$("#iddokter").val();
	let idpoli=$("#idpoli").val();
	let notransaksi=$("#notransaksi").val();
	let tanggal_input_1=$("#tanggal_input_1").val();
	let tanggal_input_2=$("#tanggal_input_2").val();
	let st_jawab=$("#st_jawab").val();
	$('#index_history_kajian').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_history_kajian').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tkonsul/list_history_konsul_dokter', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						idpoli:idpoli,
						iddokter:iddokter,
						mppa_id:mppa_id,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						st_jawab:st_jawab,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
// $("#idpoli").change(function(){
		// $.ajax({
			// url: '{site_url}tkonsul/find_dokter/',
			// dataType: "json",
			// method: "POST",
			// data : {
					// idpoliklinik:$(this).val(),
				   // },
			// success: function(data) {
				// // alert(data);
				// $("#iddokter").empty();
				// $("#iddokter").append(data);
			// }
		// });

// });

$(document).ready(function() {
	// alert(status_konsul);
	// document.getElementById("nadi").style.color = "#eb0404";
	
	disabel_edit();
	// set_ttd_konsul();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	// $("#tabel_rencana_asuhan tbody").empty();
	let konsul_id=$("#konsul_id").val();
	if ($("#trx_id").val()!='#'){
		lihat_data($("#trx_id").val());
		$("#div_daftar").hide();
		$("#div_history").show();
		
	}else{
		$("#div_daftar").show();
		$("#div_history").hide();
	}
	$('.js-summernote').summernote({
		  height: 120,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	list_history_konsul();
	load_awal_konsul=false;
});
function lihat_data(konsul_id){
	$("#cover-spin").show();
	$("#div_history").show();
	$("#konsul_id").val(konsul_id);
	$.ajax({
			url: '{site_url}tkonsul/find_konsul/',
			dataType: "json",
			method: "POST",
			data : {
					konsul_id:konsul_id,
				   },
			success: function(data) {
				$("#tglpendaftaran").val(data.tglpendaftaran);
				$("#waktupendaftaran").val(data.waktupendaftaran);
				$("#tujuan_poli").val(data.nama_poli);
				$("#nama_ppa").val(data.nama_dokter_dari);
				$("#tujuan_dokter").val(data.nama_dokter);
				$('#konsul_alasan').summernote('code',data.konsul_alasan);
				$('#konsul_diminta').summernote('code',data.konsul_diminta);
				$('#konsul_jawaban').summernote('code',data.konsul_jawaban);
				$('#konsul_anjuran').summernote('code',data.konsul_anjuran);
				// $('#konsul_alasan').summernote('disable');
				// $('#konsul_diminta').summernote('disable');
				// $('#konsul_jawaban').summernote('disable');
				// $('#konsul_anjuran').summernote('disable');
				$("#div_daftar").hide();
				$("#div_history").show();
				$("#cover-spin").hide();
				if (data.st_jawab=='1'){
					$("#btn_jawaban").hide();
				}else{
					$("#btn_jawaban").show();
				}
				$("#iddokter_dari_edit").val(data.iddokter_dari);
				$("#iddokter_jawab_edit").val(data.iddokter);
				if ($("#iddokter_dari_edit").val()==$("#login_pegawai_id").val()){
					$(".btn_edit_konsul").show();
				}else{
					$(".btn_edit_konsul").hide();
				}
				if ($("#iddokter_jawab_edit").val()==$("#login_pegawai_id").val()){
					$(".btn_edit_jawab").show();
				}else{
					$(".btn_edit_jawab").hide();
				}
				// $('html, body').animate({ scrollTop: $("#div_history").offset().top}, 500);
					window.scrollTo(0, document.body.scrollHeight);
			}
		});
	// $('html, body').animate({ scrollTop: $("#div_history").offset().top-400 }, 500);
}
function edit_konsul(){
	$('#konsul_alasan').summernote('enable');
	$('#konsul_diminta').summernote('enable');
	$(".btn_simpan_edit_konsul").show();
}
function simpan_konsul(){
	let konsul_id=$('#konsul_id').val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tkonsul/simpan_edit_konsul/',
		dataType: "json",
		method: "POST",
		data : {
				konsul_id:konsul_id,
				konsul_diminta:$("#konsul_diminta").val(),
				konsul_alasan:$("#konsul_alasan").val(),
			   },
		success: function(data) {
			$('#konsul_alasan').summernote('disable');
			$('#konsul_diminta').summernote('disable');
			$(".btn_simpan_edit_konsul").hide();
			$("#cover-spin").hide();
			swal({
				title: "Berhasil!",
				text: "Proses.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});

		}
	});
}
function edit_jawab(){
	$('#konsul_jawaban').summernote('enable');
	$('#konsul_anjuran').summernote('enable');
	$(".btn_simpan_edit_jawab").show();
}
function simpan_jawab(){
	let konsul_id=$('#konsul_id').val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tkonsul/simpan_edit_jawab/',
		dataType: "json",
		method: "POST",
		data : {
				konsul_id:konsul_id,
				konsul_jawaban:$("#konsul_jawaban").val(),
				konsul_anjuran:$("#konsul_anjuran").val(),
			   },
		success: function(data) {
			$('#konsul_jawaban').summernote('disable');
			$('#konsul_anjuran').summernote('disable');
			$(".btn_simpan_edit_jawab").hide();
			$("#cover-spin").hide();
			swal({
				title: "Berhasil!",
				text: "Proses.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});

		}
	});
}
function hapus_data(konsul_id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Hapus Data Konsultasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
	
	$.ajax({
		url: '{site_url}tkonsul/hapus_data/',
		dataType: "json",
		method: "POST",
		data : {
				konsul_id:konsul_id,
			   },
		success: function(data) {
			
			$("#cover-spin").hide();
			swal({
				title: "Berhasil!",
				text: "Proses.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
			list_history_konsul();
		}
	});
	
	});

	
}
function disabel_edit(){
	if (status_konsul=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".auto_blur").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}
function kirim_jawaban(){
	if (load_awal_konsul==false){
		
		let konsul_id=$("#konsul_id").val();
		
		if (konsul_id){
			// console.log('SIMPAN');
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tkonsul/kirim_jawaban', 
				dataType: "JSON",
				method: "POST",
				data : {
			
						konsul_id : konsul_id,
						konsul_alasan : $("#konsul_alasan").val(),
						konsul_diminta : $("#konsul_diminta").val(),
						konsul_jawaban : $("#konsul_jawaban").val(),
						konsul_anjuran : $("#konsul_anjuran").val(),
						
					},
				success: function(data) {
					// location.reload();	
					set_daftar();
					list_history_konsul();
					$("#cover-spin").show();
				}
			});
		}
		
	}
}
function set_jawaban(){
	$("#div_daftar").hide();
	$("#div_history").show();
	 $('html, body').animate({ scrollTop: $("#div_history").offset().top}, 500);
}
function set_daftar(){
	$("#div_daftar").show();
	$("#div_history").hide();
	$('html, body').animate({ scrollTop: $("#div_daftar").offset().top}, 500);
}
function kembali_daftar(){
	set_daftar();
	list_history_konsul();
}
</script>