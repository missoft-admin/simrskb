<style>
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1;
	}
	.edited2{
		color:#d26a5c;
		font-weight: bold;
	}
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   
</style>
<?if ($menu_kiri=='his_triage'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_triage' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						$tanggaldatang=HumanDateShort($tanggal_datang);
						$waktudatang=HumanTime($tanggal_datang);
						
						$tanggaldatang_asal=HumanDateShort($tanggal_datang_asal);
						$waktudatang_asal=HumanTime($tanggal_datang_asal);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
						
					}
						$disabel_input='';
						$disabel_input_ttv='';
						// if ($status_ttv!='1'){
							// $disabel_input_ttv='disabled';
						// }
						if ($st_input_triage=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_triage=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_triage=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_triage/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >	
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >								
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_triage" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tgl" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tgl" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >KEDATANGAN </h5>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Tanggal Masuk</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="<?=($tanggaldatang!=$tanggaldatang_asal?'edited':'')?> form-control auto_blur_tgl" data-date-format="dd/mm/yyyy" id="tanggaldatang" placeholder="HH/BB/TTTT" name="tanggaldatang" value="<?= $tanggaldatang ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Jam Masuk</label>
									<div class="input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="<?=($waktudatang!=$waktudatang_asal?'edited':'')?> time-datepicker form-control auto_blur_tgl" id="waktudatang" value="<?= $waktudatang ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Surat pengantar / Rujukan</label>
									<select tabindex="8" id="surat_pengantar" class="<?=($surat_pengantar!=$surat_pengantar_asal?'edited':'')?>  form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($surat_pengantar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(55) as $row){?>
										<option value="<?=$row->id?>" <?=($surat_pengantar == $row->id ? 'selected="selected"' : '')?> <?=($surat_pengantar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Sarana Transportasi</label>
									<select tabindex="8" id="sarana_transport" class="<?=($sarana_transport!=$sarana_transport_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sarana_transport == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(52) as $row){?>
										<option value="<?=$row->id?>" <?=($sarana_transport == $row->id ? 'selected="selected"' : '')?> <?=($sarana_transport == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
									
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Asal Rujukan</label>
									<select tabindex="32" name="idasalpasien" id="idasalpasien" class="<?=($idasalpasien!=$idasalpasien_asal?'edited':'')?> form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?php foreach (get_all('mpasien_asal') as $r):?>
											<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<?php if ($pendaftaran_id != '' && ($idasalpasien == '2' || $idasalpasien == '3')) {?>
								<?php
									if ($idasalpasien == 2) {
										$idtipeRS = 1;
									} elseif ($idasalpasien == 3) {
										$idtipeRS = 2;
									}
									
								}else{
									$idtipeRS='';
								}
								?>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Nama Fasilitas Kesehatan</label>
										<select tabindex="33" name="idrujukan" id="idrujukan" class="<?=($idrujukan!=$idrujukan_asal?'edited':'')?> form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach (get_all('mrumahsakit', ['idtipe' => $idtipeRS]) as $r) {?>
												<option value="<?= $r->id?>" <?=($idrujukan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
											<?php }?>
										</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Cara Masuk</label>
									<select tabindex="8" id="cara_masuk" name="cara_masuk" class="<?=($cara_masuk!=$cara_masuk_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($cara_masuk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(53) as $row){?>
										<option value="<?=$row->id?>" <?=($cara_masuk == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($cara_masuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Macam Kasus</label>
									<select tabindex="8" id="macam_kasus" name="macam_kasus" class="<?=($macam_kasus!=$macam_kasus_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($macam_kasus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(54) as $row){?>
										<option value="<?=$row->id?>" <?=($macam_kasus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($macam_kasus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Nama pengantar</label>
									<input tabindex="29" type="text" class="form-control <?=($namapengantar!=$namapengantar_asal?'edited':'')?>"   id="namapengantar" placeholder="Nama Pengantar" name="namapengantar" value="{namapengantar}" required>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">No Tlp Penanggung Jawab</label>
									<input tabindex="32" type="text" class="form-control <?=($teleponpengantar!=$teleponpengantar_asal?'edited':'')?>"   id="teleponpengantar" placeholder="Telepon" name="teleponpengantar" value="{teleponpengantar}" required>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Tanda Tanda Vital </h5>
								<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">GCS</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($gcs_eye!=$gcs_eye_asal?'edited':'')?>"  type="text" id="gcs_eye"  value="{gcs_eye}" placeholder="Eye" required>
										<span class="input-group-addon"><?=$satuan_gcs_eye?></span>
										<input class="form-control decimal auto_blur_ttv <?=($gcs_voice!=$gcs_voice_asal?'edited':'')?>"  type="text" id="gcs_voice" value="{gcs_voice}"  placeholder="Voice" required>
										<span class="input-group-addon"><?=$satuan_gcs_voice?></span>
										<input class="form-control decimal auto_blur_ttv <?=($gcs_motion!=$gcs_motion_asal?'edited':'')?>"  type="text" id="gcs_motion" value="{gcs_motion}"   placeholder="Motion" required>
										<span class="input-group-addon"><?=$satuan_gcs_motion?></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Pupil</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($pupil_1!=$pupil_1_asal?'edited':'')?>"  type="text" id="pupil_1"  value="{pupil_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_pupil?> / </span>
										<input class="form-control decimal auto_blur_ttv <?=($pupil_2!=$pupil_2_asal?'edited':'')?>"  type="text" id="pupil_2" value="{pupil_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_pupil?></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Refleksi Cahaya</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($reflex_cahaya_1!=$reflex_cahaya_1_asal?'edited':'')?>"  type="text" id="reflex_cahaya_1"  value="{reflex_cahaya_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_cahaya?> / </span>
										<input class="form-control decimal auto_blur_ttv <?=($reflex_cahaya_2!=$reflex_cahaya_2_asal?'edited':'')?>"  type="text" id="reflex_cahaya_2" value="{reflex_cahaya_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_cahaya?></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($td_sistole!=$td_sistole_asal?'edited':'')?>"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal auto_blur_ttv <?=($td_diastole!=$td_diastole_asal?'edited':'')?>"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="example-input-normal">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv blur_nadi <?=($nadi!=$nadi_asal?'edited':'')?>"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Nadi</label>
									<input class="form-control auto_blur_ttv <?=($ket_nadi!=$ket_nadi_asal?'edited':'')?>" readonly  type="text" id="ket_nadi" value="{ket_nadi}"  placeholder="Keterangan Nadi">
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($nafas!=$nafas_asal?'edited':'')?>"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($suhu!=$suhu_asal?'edited':'')?>"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">SPO2</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($spo2!=$spo2_asal?'edited':'')?>"   type="text" id="spo2"  value="{spo2}"  placeholder="{satuan_spo2}" required>
										<span class="input-group-addon"><?=$satuan_spo2?></span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Akral</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($akral!=$akral_asal?'edited':'')?>"  type="text" id="akral"  value="{akral}" placeholder="{satuan_akral}" required>
										<span class="input-group-addon"><?=$satuan_akral?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Tinggi Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($tinggi_badan!=$tinggi_badan_asal?'edited':'')?>"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
										<span class="input-group-addon">Cm</span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Berat Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv <?=($berat_badan!=$berat_badan_asal?'edited':'')?>"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Lain-Lain</h5>
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Kasus Alergi</label>
									<select tabindex="8" id="kasus_alergi" name="kasus_alergi" class="<?=($kasus_alergi!=$kasus_alergi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kasus_alergi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(58) as $row){?>
										<option value="<?=$row->id?>" <?=($kasus_alergi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kasus_alergi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Jika Ada Sebutkan</label>
									<input class="form-control auto_blur <?=($alergi!=$alergi_asal?'edited':'')?>" type="text" id="alergi"  value="{alergi}" placeholder="Detail Alergi" >
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Status Psikologi </label>
									<select tabindex="8" id="st_psikologi" name="st_psikologi" class="<?=($st_psikologi!=$st_psikologi_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_psikologi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(57) as $row){?>
										<option value="<?=$row->id?>" <?=($st_psikologi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_psikologi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Risiko Jatuh  Morse Skor</label>
									<select tabindex="8" id="risiko_jatuh_morse" name="risiko_jatuh_morse" class="<?=($risiko_jatuh_morse!=$risiko_jatuh_morse_asal?'edited':'')?>  form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($risiko_jatuh_morse == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(59) as $row){?>
										<option value="<?=$row->id?>" <?=($risiko_jatuh_morse == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($risiko_jatuh_morse == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Risiko Jatuh  Morse Skor</label>
									<select tabindex="8" id="risiko_pasien_anak" name="risiko_pasien_anak" class="<?=($risiko_pasien_anak!=$risiko_pasien_anak_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($risiko_pasien_anak == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(56) as $row){?>
										<option value="<?=$row->id?>" <?=($risiko_pasien_anak == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($risiko_pasien_anak == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >TRIAGE</h5>
								</div>
						</div>
						<?
							$q="SELECT *FROM tpoliklinik_triage_kat H WHERE H.assesmen_id='$assesmen_id' ";
							$header=$this->db->query($q)->result();
						?>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-6 ">
								<label for="example-input-normal">Triage</label>
									<select id="kategori_id" name="kategori_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($kategori_id==''?'selected':'')?>>- Belum dipilih -</option>
										<?foreach($header as $r){?>
										<option value="<?=$r->mtriage_id?>" <?=($r->mtriage_id==$kategori_id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered" id="tabel_pemeriksaan">
											
										</table>
									</div>
								</div>
							</div>
							
						</div>
						
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_triage=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var kategori_id_tmp=$("#kategori_id").val();
$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	// set_ttd_triage();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_pemeriksaan();
		list_index_history_edit();
	}
	
	// load_data_rencana_asuhan(1);
});
function load_pemeriksaan(){
	let kategori=$("#kategori_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let status_assemen=$("#status_assemen").val();
	let versi_edit=$("#versi_edit").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}thistory_tindakan/load_pemeriksaan_triage', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				versi_edit:versi_edit,
				
				},
		success: function(data) {
			$("#tabel_pemeriksaan").append(data.tabel);
			disabel_edit();
			$(".nilai").select2();
			// set_kategori();
			$(".item_all").prop("disabled", true);
			if (kategori!='#'){
				$(".chk_kategori_"+kategori).removeAttr('disabled');
				$(".input_kategori_"+kategori).removeAttr('disabled');
			}
			$("#cover-spin").hide();
			load_awal_triage=false;
		}
	});
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "20%", "targets": [1] },
					 // { "width": "15%", "targets": [3] },
					 // { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_triage', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".his_filter").removeAttr('disabled');
		 $('.edited').css('background', '#fff2f1');
	}
}

</script>