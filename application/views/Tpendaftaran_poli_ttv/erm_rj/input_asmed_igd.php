<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	.input-keterangan{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_asmed_igd'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_asmed_igd' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					if ($tanggal_kontrol){
						
						$tanggal_kontrol=HumanDateShort($tanggal_kontrol);
					}
					$disabel_input='';
					$disabel_input_ttv='';
					// if ($status_ttv!='1'){
						// $disabel_input_ttv='disabled';
					// }
					if ($st_input_asmed_igd=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_asmed_igd=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="tanggal" value="<?=$tanggal?>" >		
						
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_asmed_igd=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-disk"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_asmed_igd" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_asmed_igd!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) <?=$st_input_assesmen?></label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >ANAMNESIS </h5>
							</div>
						</div>
						<input class="form-control"  type="hidden" id="st_anamnesa" name="st_anamnesa" value="<?=$st_anamnesa?>" >				
								
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Keluhan Utama </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" value="1" id="auto_anamnesa" name="anamnesa" onclick="set_auto()" <?=($st_anamnesa=='1'?'checked':'')?>><span></span> Auto Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  value="1" id="allo_anamnesa" name="anamnesa" onclick="set_allo()" <?=($st_anamnesa=='2'?'checked':'')?>><span></span> Allo Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control auto_blur push-5-r"  type="text"  id="nama_anamnesa" name="nama_anamnesa" value="{nama_anamnesa}" placeholder="Tuliskan Nama" >
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control auto_blur"  type="text" id="hubungan_anamnesa" name="hubungan_anamnesa" value="{hubungan_anamnesa}" placeholder="Hubungan" >
								</div>
								<div class="col-md-12 push-10-t">
									<textarea id="keluhan_utama" class="form-control auto_blur" name="story" rows="2" style="width:100%"><?=$keluhan_utama?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit" name="st_riwayat_penyakit" value="<?=$st_riwayat_penyakit?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(0)" <?=($st_riwayat_penyakit=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(1)" <?=($st_riwayat_penyakit=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									<select id="riwayat_penyakit" name="riwayat_penyakit" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach($list_riwayat_penyakit as $r){?>
										<option value="<?=$r->id?>" <?=$r->pilih?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_lainnya" class="form-control auto_blur" name="riwayat_penyakit_lainnya" rows="2" style="width:100%"><?=$riwayat_penyakit_lainnya?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit_dahulu" name="st_riwayat_penyakit_dahulu" value="<?=$st_riwayat_penyakit_dahulu?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit Dahulu</label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  name="opsi_rp_dahulu" onclick="set_riwayat_1(0)" <?=($st_riwayat_penyakit_dahulu=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_rp_dahulu" onclick="set_riwayat_1(1)" <?=($st_riwayat_penyakit_dahulu=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									<select id="riwayat_penyakit_dahulu_list" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach($list_riwayat_penyakit_dahulu as $r){?>
										<option value="<?=$r->id?>" <?=$r->pilih?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_dahulu" class="form-control auto_blur" name="riwayat_penyakit_dahulu" rows="2" style="width:100%"><?=$riwayat_penyakit_dahulu?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit_keluarga" value="<?=$st_riwayat_penyakit_keluarga?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit Keluarga</label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_rp_keluarga" onclick="set_riwayat_2(0)" <?=($st_riwayat_penyakit_keluarga=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_rp_keluarga" onclick="set_riwayat_2(1)" <?=($st_riwayat_penyakit_keluarga=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									<select id="riwayat_penyakit_keluarga_list" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach($list_riwayat_penyakit_keluarga as $r){?>
										<option value="<?=$r->id?>" <?=$r->pilih?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_keluarga" class="form-control auto_blur" rows="2" style="width:100%"><?=$riwayat_penyakit_keluarga?></textarea>
								</div>
								
							</div>
						</div>
						

						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary">PEMERIKSAAN FISIK</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-striped" id="index_header">
											<thead>
												<tr>
													<th width="30%">Anatomi Tubuh</th>
													<th width="50%">Jika Tidak Normal Jelaskan</th>
													<th width="20%"></th>
												</tr>
												<tr>
													<td width="30%">
														<input class="form-control " type="hidden" id="anatomi_id"  value="" placeholder="" >
														<select tabindex="8" id="manatomi_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Anatomi Tubuh" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(get_all('manatomi',array('staktif'=>1)) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->nama_english.')'?></option>
															<?}?>
														</select>
														<div class="help-block text-danger"><span class="text-danger">Inputkan hasil pemeriksaan pasien yang ditemukan kelainan</span></div>
													</td>
													<td width="55%">
														<input class="form-control " type="text" id="ket_anatomi"  value="" placeholder="" >
													</td>
													<td width="15%">
														<span class="input-group-btn">
															<button class="btn btn-info" onclick="simpan_anatomi()" id="btn_add_anatomi" type="button"><i class="fa fa-plus"></i> Add</button>
															<button class="btn btn-warning" onclick="clear_input_anatomi()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</td>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-striped" id="tabel_anatomi">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="25%">Anatomi Tubuh</th>
													<th width="40%">Deskripsi Tidak Normal</th>
													<th width="15%">User</th>
													<th width="15%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class=" text-primary" >STATUS LOKALIS</label>
							</div>
							<div class="col-md-6 ">
								<label class="text-primary" >KETERANGAN</label>
							</div>
						</div>
						<input class="form-control " readonly type="hidden" id="src_gambar"  value="{upload_path}anatomi/{gambar_tubuh}" placeholder="" >
						<input class="form-control " readonly type="hidden" id="base_url"  value="{upload_path}anatomi/" placeholder="" >
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<canvas id="canvas" width="680" height="500" ></canvas>
								</div>
								<div class="col-sm-4">
									<label>Template</label>
									<select id="gambar_id"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(get_all('tpoliklinik_asmed_igd_lokalis_gambar',array('assesmen_id'=>$assesmen_id))as $r){?>
										<option value="<?=$r->id?>" <?=($r->st_default=='1'?'selected':'')?>><?=$r->nama_template_lokasi?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-4">
									<label>Jenis Drawing</label>
									<select id="jenis_drawing"  class="js-select2 form-control input-default" style="width: 100%;" data-placeholder="Choose one..">
										<option value="free_draw" <?=($jenis_drawing=='free_draw'?'selected':'')?>>Free Draw</option>
										<option value="segitiga" <?=($jenis_drawing=='segitiga'?'selected':'')?>>Triangle</option>
										<option value="kotak" <?=($jenis_drawing=='kotak'?'selected':'')?>>Box</option>
										<option value="bulat" <?=($jenis_drawing=='bulat'?'selected':'')?>>Round</option>
										<option value="silang" <?=($jenis_drawing=='silang'?'selected':'')?>>Cross</option>
										<option value="diamond" <?=($jenis_drawing=='diamond'?'selected':'')?>>Diamond</option>
										<option value="bintang" <?=($jenis_drawing=='bintang'?'selected':'')?>>Star</option>
										<option value="target" <?=($jenis_drawing=='target'?'selected':'')?>>Crosshairs</option>
										<option value="hati" <?=($jenis_drawing=='hati'?'selected':'')?>>Heart</option>
										<option value="bendera" <?=($jenis_drawing=='bendera'?'selected':'')?>>Flag</option>
										<option value="tempe" <?=($jenis_drawing=='tempe'?'selected':'')?>>Bookmark</option>
									</select>
								</div>
								<div class="col-md-4">
									<label>Warna</label>
									<div class="js-colorpicker input-group colorpicker-element">
										<input class="form-control input-default" readonly type="text" id="warna" name="warna" value="<?=$warna?>">
										<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="simbol-list">
											<thead>
												<tr>
													<th style="width:5%">No.</th>
													<th style="width:10%">Simbol</th>
													<th style="width:75%">Keterangan</th>
													<th style="width:10%">Aksi</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<div class="col-md-12">
									<label>Keterangan</label>
									<input class="form-control auto_blur" type="text" id="txt_keterangan"  value="{txt_keterangan}" placeholder="Keterangan" >
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="h5 text-primary" ></h5>
								
							</div>
							<div class="col-md-6 ">
								<label class="h5 text-primary" ></h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Pemeriksaan Penunjang</label>
									<textarea id="pemeriksaan_penunjang" class="form-control auto_blur" name="pemeriksaan_penunjang" rows="2" style="width:100%"><?=$pemeriksaan_penunjang?></textarea>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Intruksi</label>
									<textarea id="intruksi" class="form-control auto_blur" name="intruksi" rows="2" style="width:100%"><?=$intruksi?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Rencana Asuhan</label>
									<textarea id="rencana_asuhan" class="form-control auto_blur" rows="2" style="width:100%"><?=$rencana_asuhan?></textarea>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa Utama</label>
									<input class="form-control auto_blur" type="text" id="diagnosa_utama"  value="{diagnosa_utama}" placeholder="Diagnosa Utama" >
									
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
								
									<div class="table-responsive">
										<table class="table" id="index_header">
											<thead>
												<tr>
													<th width="55%">
														Diagnosa tambahan
													</th>
													<th width="30%">
														Priority
													</th>
													<th width="15%">
														
													</th>
												</tr>
												<tr>
													<td width="55%">
														<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
														<select tabindex="8" id="mdiagnosa_id" class="form-control " style="width: 100%;" data-placeholder="Cari Diagnosa" required>
															
														</select>
													</td>
													<td width="30%">
														<select tabindex="8" id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															
															<?for($i=1;$i<20;$i++){?>
															<option value="<?=$i?>"><?=$i?></option>
															<?}?>
														</select>
													</td>
													<td width="15%">
														<span class="input-group-btn">
															<button class="btn btn-info" onclick="simpan_diagnosa()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
															<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</td>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="45%">Diagnosa Tambahan</th>
													<th width="15%">Priority</th>
													<th width="15%">User</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label>Keterangan</label>
									<input class="form-control auto_blur" type="text" id="txt_keterangan_diagnosa"  value="{txt_keterangan_diagnosa}" placeholder="Keterangan" >
								</div>
							</div>
							
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Edukasi</h5>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Kebutuhan Edukasi</label>
									<select tabindex="8" id="medukasi_id" name="medukasi_id[]" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_edukasi as $row){?>
										<option value="<?=$row->id?>"  <?=$row->pilih?>><?=$row->judul?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Catatan lain Edukasi</label>
									<input class="form-control auto_blur" type="text" id="catatan_edukasi"  value="{catatan_edukasi}" name="catatan_edukasi" placeholder="Catatan" >
									
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Ringkasan Kondisi Keluar</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Keadaan Umum</label>
									<select tabindex="8" id="keadaan_umum" name="keadaan_umum" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(61) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_umum == $row->id ? 'selected="selected"' : '')?> <?=($keadaan_umum == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Tingkat Kesadaran</label>
									<select tabindex="8" id="tingkat_kesadaran" <?=$disabel_input_ttv?>  style="background-color:#d0f3df;" name="tingkat_kesadaran" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur" type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal auto_blur" type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="example-input-normal">Pernafasan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="example-input-normal">GDS</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="gds" name="gds" value="{gds}"  placeholder="GDS" required>
										<span class="input-group-addon"><?=$satuan_gds?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Status Pulang</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Status Pulang</label>
									<select tabindex="8" id="st_pulang" name="st_pulang" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(62) as $row){?>
										<option value="<?=$row->id?>" <?=($st_pulang == $row->id ? 'selected="selected"' : '')?> <?=($st_pulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Pasien Keluar IGD (Tanggal)</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="<?= $tanggal_keluar ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Rencana Kontrol</label>
									<select tabindex="8" id="rencana_kontrol" name="rencana_kontrol" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($rencana_kontrol == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(43) as $row){?>
										<option value="<?=$row->id?>" <?=($rencana_kontrol == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($rencana_kontrol == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Tanggal Kontrol</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggal_kontrol" placeholder="dd-mm-yyyy" value="<?= $tanggal_kontrol ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							
							<?
								if ($idpoli_kontrol==''){
									$idpoli_kontrol=$idpoliklinik;
								}
								if ($iddokter_kontrol=='0' || $iddokter_kontrol==''){
									$iddokter_kontrol=$iddokter;
								}
							?>
							<div class="col-md-6">
								<div class="col-md-12">
									<label for="example-input-normal">Poliklinik </label>
									<select id="idpoli_kontrol" name="idpoli_kontrol" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0"  <?=($idpoli_kontrol==''?'selected':'')?>>- Tidak Ada -</option>
										<?foreach($list_poli as $r){?>
										<option value="<?=$r->id?>" <?=($idpoli_kontrol==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
									
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12 ">
									<label for="example-input-normal">Dokter</label>
									<select id="iddokter_kontrol" name="iddokter_kontrol" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0"  <?=($iddokter_kontrol==''?'selected':'')?>>- Tidak Ada -</option>
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"  <?=($iddokter_kontrol==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
										
									</select>
									
								</div>
								
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<?foreach($list_poli as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="jenis_ttd" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_ttd==''?'selected':'')?>>Silahkan Pilih</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Pasien Sendiri Sendiri</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?> >Keluarga Terdekat / Wali</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?> >Penanggung Jawab</option>
										
									</select>
									<label for="jenis_ttd">Yang Tandatangan</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							<div class="col-md-6 push-10">
								<div class="form-material">
									<select tabindex="30" id="ttd_hubungan" name="ttd_hubungan" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($ttd_hubungan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<label for="ttd_hubungan">Hubungan</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_ttd()" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
const font      = '400 30px "Font Awesome 6 Free"';
const font_2    = '900 40px "Font Awesome 6 Free"';
const font_3    = '900 30px "Font Awesome 6 Free"';
const marker_type = [{key:'bulat',content:'\uf10c',class_name:'fa-circle',font:font},
                     {key:'segitiga',content:'\uf0d8',class_name:'fa-caret-up',font:font_2},
                     {key:'kotak',content:'\uf096',class_name:'fa-square',font:font},
                     {key:'silang',content:'\uf00d',class_name:'fa-times',font:font_3},
                     {key:'diamond',content:'\uf219',class_name:'fa-diamond',font:font_3},
                     {key:'tempe',content:'\uf097',class_name:'fa-bookmark',font:font},
                     {key:'bintang',content:'\uf006',class_name:'fa-star',font:font},
                     {key:'target',content:'\uf05b',class_name:'fa-crosshairs',font:font_3},
                     {key:'hati',content:'\uf08a',class_name:'fa-heart',font:font},
                     {key:'bendera',content:'\uf11d',class_name:'fa-flag',font:font},
                     {key:'free_draw',content:'',class_name:'fa-pencil',font:font_3},
                    ];

var selected_marker = null;
<?if ($assesmen_id){?>
	

var canvas      = document.getElementById('canvas');
var context     = canvas.getContext("2d");

context.lineWidth=2;
var st_drawing  = false;
var path     = [];
// const change_marker = document.querySelectorAll('.change-marker');
// change_marker.forEach(el => el.addEventListener('click', event => {
    // selected_marker = marker_type.find(o => o.key === el.id);
    
// }));
$("#jenis_drawing").on("change", function(){
	set_marker();
}); 


function set_marker(){
	 // $("#jenis_drawing").val();
	 selected_marker = marker_type.find(({ key }) => key === $("#jenis_drawing").val());
	 // console.log(Markers);
	 // selected_marker = marker_type.find(o => o.key === el.id);
}

document.fonts.load(font).then((_) => {
    
    // Map sprite
    var mapSprite   = new Image();
    // mapSprite.src   = "human_body.jpeg";
    mapSprite.src   = $("#src_gambar").val();
    
    // selected_marker=$("#jenis_drawing").val();
	set_marker();
    // console.log(selected_marker);
    
    var Marker = function () {
        this.Width = 20;
        this.Height = 20;
        this.XPos = 0;
        this.YPos = 0;
    }

    var Markers = new Array();
	var str_key='';
	var str_path='';
	var selected_marker2=null;
	<? foreach($list_lokalis as $r){?>
	
	str_path='<?=$r->path?>';
	str_key='<?=$r->key?>';
	selected_marker2 = marker_type.find(({ key }) => key === str_key);
		 var marker      = new Marker();
		marker.XPos     = '<?=$r->XPos?>';
		marker.YPos     = '<?=$r->YPos?>';
		marker.key      = '<?=$r->key?>';
		marker.content  = selected_marker2.content;
		marker.warna    = '<?=$r->warna?>';
		if (str_path){
			marker.path  = JSON.parse("[" + str_path + "]");
		}
		// marker.path    = <?=($r->path)?>;
			 
		marker.keterangan    = '<?=$r->keterangan?>';
		marker.class_name    = '<?=$r->class_name?>';
		marker.font    = selected_marker2.font;
		marker.path    = <?= ($r->path)? "JSON.parse('".$r->path."')" : $r->path?>;

		Markers.push(marker);
		load_data();
		console.log(Markers);
	<?}?>
    var mouseClicked = function (mouse) {
		if (status_assemen!='2'){
			if(selected_marker != null){
				var rect        = canvas.getBoundingClientRect();
				if(selected_marker.key == 'free_draw'){
					st_drawing = true;
					context.beginPath();
					context.moveTo(mouse.x - rect.left, mouse.y - rect.top);
					
				}else{
					// Get corrent mouse coords
					
					var mouseXPos = (mouse.x - rect.left)+10;
					var mouseYPos = (mouse.y - rect.top) +30;

					
					// Move the marker when placed to a better location
					const warna      = document.getElementById('warna');
					var marker      = new Marker();
					marker.XPos     = mouseXPos - (marker.Width / 2);
					marker.YPos     = mouseYPos - marker.Height;
					marker.key      = selected_marker.key;
					marker.content  = selected_marker.content;
					marker.warna    = warna.value;
					marker.keterangan    = '';
					marker.class_name    = selected_marker.class_name;
					marker.font    = selected_marker.font;
					marker.path    = '';
					

					Markers.push(marker);
				}
			}
        }
    }

    // Add mouse click event listener to canvas
    canvas.addEventListener("mousedown", mouseClicked, false);
    
    canvas.onmousemove = draw_line;
    canvas.onmouseup = stopDrawing;

    var firstLoad = function () {
        context.font = "15px Georgia";
        context.textAlign = "center";
    }

    firstLoad();

    var main = function () {
        draw();
    };
    
    //\uf10c bulat
    //\uf0d8 segitiga
    //\uf096 kotak
    //\uf00d silang
    //\uf219 diamond

    var draw = function () {
        if(st_drawing == false){
			
            // Clear Canvas
            context.fillStyle = "#000";
            context.fillRect(0, 0, canvas.width, canvas.height);

            // Draw map
            // Sprite, X location, Y location, Image width, Image height
            // You can leave the image height and width off, if you do it will draw the image at default size
          
				// console.log(Markers.length);
            context.drawImage(mapSprite, 0, 0, 680, 500);
            
            // Draw markers
            for (var i = 0; i < Markers.length; i++) {
              // note that I wasn't able to find \uF006 defined in the provided CSS
              // falling back to fa-bug for demo
                
                var tempMarker = Markers[i];
                // console.log(tempMarker);
                context.font = tempMarker.font;
                context.fillStyle = tempMarker.warna;
                // Draw marker
                //context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);
                let markerText = i + 1;
                let textMeasurements = context.measureText(markerText);
                
                
                if(tempMarker.key != 'free_draw'){
                    context.fillText(tempMarker.content, tempMarker.XPos, tempMarker.YPos);
                    // Calculate postion text
                    
                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 50, textMeasurements.width, 20);
                    
                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, tempMarker.XPos, tempMarker.YPos - 35);
                }else{
                    context.beginPath();
                    const begin_path =  tempMarker.path[0];
                    context.moveTo(begin_path.x, begin_path.y);
                    context.strokeStyle  = tempMarker.warna;
                    tempMarker.path.map( (p)=> {
                         context.lineTo(p.x, p.y);
                         context.stroke();
                    })
                    
                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(begin_path.x - (textMeasurements.width / 2), begin_path.y - 25, textMeasurements.width, 20);
                    
                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, begin_path.x, begin_path.y - 10);
                    
                }
            } 
        }
       
    };
    
    function draw_line(e){
        if (st_drawing == true && selected_marker.key == 'free_draw'){
             const rect        = canvas.getBoundingClientRect();
             const x = e.x - rect.left;
             const y = e.y - rect.top;
             path.push({x:x,y:y});
             const warna      = document.getElementById('warna');
             context.strokeStyle  = warna.value;
             console.log(context);
             console.log(x+' : '+y);
             context.lineTo(x, y);
             context.stroke();
        }
    }
    
    function stopDrawing() {
        if(selected_marker != null && selected_marker.key == 'free_draw'){
            if(path.length > 0){
                const warna      = document.getElementById('warna');
                const marker        = new Marker();
                marker.key          = selected_marker.key;
                marker.content      = '';
                marker.path         = path;
                marker.warna        = warna.value;
                marker.keterangan   = '';
                marker.class_name    = selected_marker.class_name;
                Markers.push(marker);
            }
            st_drawing = false;
            path = [];
        }
		// console.log('STIP');
        load_data();
   }
   $(document).on("blur",".input-keterangan",function(){	
		let index=$(this).attr("data-id");
		Markers[index].keterangan=$(this).val();
		simpan_status_lokalis();
		// $(this).closest('tr').find(".isiField").
	   
   });
   // $(".input-keterangan").blur(function(){
	// });

   function load_data(){
    const table = document.getElementById('simbol-list');
    let table_content = '';
    table.querySelector('tbody').innerHTML = '';
    for (var i = 0; i < Markers.length; i++) {
        const row = Markers[i];
        table_content += '<tr>';
        table_content +='<td>'+(i+1)+'</td>';
        table_content +='<td class="text-center"><i class="fa '+row.class_name+'"></i></td>';
        table_content +='<td><input data-id="'+ i +'" class="input-keterangan form-control input-sm"  type="text" value="'+row.keterangan+'"/></td>';
        table_content +='<td><button class="hapus-data btn btn-sm btn-danger" data-id="'+ i +'"><i class="fa fa-trash"></i></button></td>';
        table_content +='</tr>';
    }
    table.querySelector('tbody').innerHTML = table_content;
    // console.log(Markers);\
	disabel_edit();
    simpan_status_lokalis();
    const hapus_data = document.querySelectorAll('.hapus-data');
    hapus_data.forEach(el => el.addEventListener('click', event => {
        Markers.splice(el.dataset.id, 1);
        load_data();
    }));
   }



function convertCanvasToImage(aaa) {

    something = aaa[0].toDataURL("image/png");

}
   function simpan_status_lokalis(){// alert($("#keluhan_utama").val());
		   // var ctx  = document.createElement("canvas");

// var ctx1 = ctx.getContext("2d");    

// var asd  = convertCanvasToImage(ctx1)
		let canvas = $("#canvas")[0];
		let gambar_final = canvas.toDataURL("image/png");

		// $("#elememt-to-write-to").html('<img src="'+gambar_final+'"/>');
		// console.log(gambar_final);
		let assesmen_id=$("#assesmen_id").val();
		let gambar_id=$("#gambar_id").val();
		const jsonString = JSON.stringify(Object.assign({}, Markers));
		const json_obj = JSON.parse(jsonString);
		console.log(json_obj);
		// $("#cover-spin").show();
		// if (Markers.length>0){
			if (assesmen_id){
				$.ajax({
					url: '{site_url}Tpendaftaran_poli_ttv/simpan_status_lokalis_igd', 
					dataType: "JSON",
					method: "POST",
					data : {
							assesmen_id:assesmen_id,
							gambar_id:gambar_id,
							gambar_final:gambar_final,
							jsonString:json_obj,
							
						},
					success: function(data) {
						// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Default Warna'});
					}
				});
			}
		// }
		
	}
    setInterval(main, (1000 / 60)); // Refresh 60 times a second
    $("#gambar_id").on("change", function(){
		load_gambar();
	}); 
    function load_gambar(){
		let assesmen_id=$("#assesmen_id").val();
		let gambar_id=$("#gambar_id").val();
		let status_assemen=$("#status_assemen").val();
		let base_url=$("#base_url").val();
		
		var canvas      = document.getElementById('canvas');
		var context     = canvas.getContext("2d");
			$("#cover-spin").show();	
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/load_gambar_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					gambar_id:gambar_id,
					status_assemen:status_assemen,
					
				},
			success: function(data) {
				$("#src_gambar").val(base_url+data.gambar_tubuh);
				
				st_drawing  = false;
				// Map sprite
				mapSprite   = new Image();
				// mapSprite.src   = "human_body.jpeg";
				mapSprite.src   = $("#src_gambar").val();
				 Markers = new Array();
				var str_key='';
				var selected_marker2=null;
				
				$.each(data.list_lokalis, function (i,r) {
					str_key=r.key;
					selected_marker2 = marker_type.find(({ key }) => key === str_key);
					 var marker      = new Marker();
					marker.XPos     = r.XPos;
					marker.YPos     = r.YPos;
					marker.key      = r.key;
					marker.content  = selected_marker2.content;
					marker.warna    = r.warna;
					marker.keterangan    = r.keterangan;
					marker.class_name    = r.class_name;
					marker.font     = selected_marker.font;
					marker.path     = JSON.parse(r.path);

					Markers.push(marker);
					// console.log(Markers);
					// $('#nojabatan')
					// .append('<option value="' + pegawai_item.nojabatan + '">' + pegawai_item.namajabatan + '</option>');
				});
				 const table = document.getElementById('simbol-list');
					let table_content = '';
					table.querySelector('tbody').innerHTML = '';
					table.querySelector('tbody').innerHTML = table_content;
					load_data();
					$("#cover-spin").hide();
				draw();
				// alert('sini');
			}
		});
	}
})
<?}?>
// $(".input-keterangan").keyup(function(){
	// console.log($(this).val()));		
// });




// $('#clear_2').click(function(e) {
	// alert('sini');
	// // e.preventDefault();
	
// });
function hapus_ttd(){
	sig.signature('clear');
	$("#signature64").val('');
}
$(document).ready(function() {
	// $str='[{"x":"515.9375","y":"378.5520820617676"},{"x":"517.9375","y":"376.5520820617676"},{"x":"520.9375","y":"373.5520820617676"},{"x":"527.9375","y":"370.5520820617676"},{"x":"533.9375","y":"368.5520820617676"},{"x":"540.9375","y":"367.5520820617676"},{"x":"546.9375","y":"365.5520820617676"},{"x":"551.9375","y":"364.5520820617676"},{"x":"552.9375","y":"363.5520820617676"},{"x":"553.9375","y":"363.5520820617676"},{"x":"556.9375","y":"363.5520820617676"},{"x":"557.9375","y":"363.5520820617676"},{"x":"558.9375","y":"365.5520820617676"},{"x":"560.9375","y":"366.5520820617676"},{"x":"561.9375","y":"368.5520820617676"},{"x":"563.9375","y":"369.5520820617676"},{"x":"565.9375","y":"372.5520820617676"},{"x":"566.9375","y":"375.5520820617676"},{"x":"567.9375","y":"378.5520820617676"},{"x":"567.9375","y":"380.5520820617676"},{"x":"568.9375","y":"383.5520820617676"},{"x":"568.9375","y":"385.5520820617676"},{"x":"569.9375","y":"388.5520820617676"},{"x":"569.9375","y":"390.5520820617676"},{"x":"569.9375","y":"394.5520820617676"},{"x":"569.9375","y":"397.5520820617676"},{"x":"568.9375","y":"400.5520820617676"},{"x":"560.9375","y":"408.5520820617676"},{"x":"559.9375","y":"409.5520820617676"},{"x":"557.9375","y":"410.5520820617676"},{"x":"556.9375","y":"412.5520820617676"},{"x":"555.9375","y":"412.5520820617676"},{"x":"552.9375","y":"412.5520820617676"},{"x":"550.9375","y":"409.5520820617676"},{"x":"545.9375","y":"406.5520820617676"},{"x":"541.9375","y":"403.5520820617676"},{"x":"539.9375","y":"402.5520820617676"},{"x":"538.9375","y":"400.5520820617676"},{"x":"537.9375","y":"398.5520820617676"},{"x":"536.9375","y":"397.5520820617676"},{"x":"535.9375","y":"396.5520820617676"},{"x":"532.9375","y":"395.5520820617676"},{"x":"532.9375","y":"394.5520820617676"},{"x":"531.9375","y":"394.5520820617676"},{"x":"530.9375","y":"393.5520820617676"},{"x":"530.9375","y":"392.5520820617676"},{"x":"530.9375","y":"390.5520820617676"},{"x":"529.9375","y":"390.5520820617676"},{"x":"529.9375","y":"388.5520820617676"},{"x":"529.9375","y":"387.5520820617676"},{"x":"529.9375","y":"386.5520820617676"},{"x":"530.9375","y":"385.5520820617676"},{"x":"531.9375","y":"384.5520820617676"},{"x":"535.9375","y":"382.5520820617676"},{"x":"537.9375","y":"379.5520820617676"},{"x":"538.9375","y":"378.5520820617676"},{"x":"542.9375","y":"376.5520820617676"},{"x":"545.9375","y":"375.5520820617676"},{"x":"548.9375","y":"373.5520820617676"},{"x":"550.9375","y":"372.5520820617676"},{"x":"553.9375","y":"372.5520820617676"},{"x":"556.9375","y":"372.5520820617676"},{"x":"558.9375","y":"372.5520820617676"},{"x":"560.9375","y":"372.5520820617676"},{"x":"561.9375","y":"372.5520820617676"},{"x":"565.9375","y":"373.5520820617676"},{"x":"566.9375","y":"373.5520820617676"},{"x":"566.9375","y":"374.5520820617676"},{"x":"567.9375","y":"375.5520820617676"},{"x":"568.9375","y":"376.5520820617676"},{"x":"570.9375","y":"377.5520820617676"},{"x":"571.9375","y":"379.5520820617676"},{"x":"573.9375","y":"380.5520820617676"},{"x":"573.9375","y":"382.5520820617676"},{"x":"575.9375","y":"384.5520820617676"},{"x":"576.9375","y":"385.5520820617676"},{"x":"576.9375","y":"387.5520820617676"},{"x":"577.9375","y":"387.5520820617676"}]	';
	// var array = JSON.parse("[" + $str + "]");
	// console.log(array);
	// alert($str);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		set_anamnesa();
		set_riwayat($("#st_riwayat_penyakit").val())
		set_riwayat_1($("#st_riwayat_penyakit_dahulu").val())
		set_riwayat_2($("#st_riwayat_penyakit_keluarga").val())
		load_anatomi();
		load_diagnosa();
		
		
		load_awal_assesmen=false;
	}
	$("#mdiagnosa_id").select2({
        minimumInputLength: 2,
		tags: true,
		noResults: 'Diagnosa Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}Tpendaftaran_poli_ttv/get_diagnosa_medis/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });
	// load_data_rencana_asuhan(1);
});
function set_img_tubuh(){
	
	var src =$("#src_gambar").val();

	$('#sig_tubuh').signature('enable'). signature('draw', src);
}
function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}

function simpan_ttd(){
	$("#modal_ttd").modal('hide');
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_asmed_igd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
	  load_ttd();
}
function load_ttd(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/load_ttd_asmed_igd', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;

			$('#sig').signature('enable'). signature('draw', src);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#gambar_id").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}
function set_riwayat(st_riwayat_penyakit){
	$("#st_riwayat_penyakit").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit").removeAttr('disabled');
		$("#riwayat_penyakit_lainnya").removeAttr('disabled');
		$("#riwayat_penyakit").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit").val('').trigger('change');
		$("#riwayat_penyakit").removeAttr('required');
		$("#riwayat_penyakit").attr('disabled','disabled');
		$("#riwayat_penyakit_lainnya").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
function set_riwayat_1(st_riwayat_penyakit){
	$("#st_riwayat_penyakit_dahulu").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit_dahulu").removeAttr('disabled');
		$("#riwayat_penyakit_dahulu_list").removeAttr('disabled');
		
		$("#riwayat_penyakit_dahulu").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit_dahulu_list").val('').trigger('change');
		$("#riwayat_penyakit_dahulu").removeAttr('required');
		$("#riwayat_penyakit_dahulu_list").attr('disabled','disabled');
		$("#riwayat_penyakit_dahulu").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
function set_riwayat_2(st_riwayat_penyakit){
	$("#st_riwayat_penyakit_keluarga").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit_keluarga").removeAttr('disabled');
		$("#riwayat_penyakit_keluarga_list").removeAttr('disabled');
		
		$("#riwayat_penyakit_dahulu").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit_keluarga_list").val('').trigger('change');
		$("#riwayat_penyakit_keluarga").removeAttr('required');
		$("#riwayat_penyakit_keluarga").attr('disabled','disabled');
		$("#riwayat_penyakit_keluarga_list").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
// $("#riwayat_penyakit").change(function(){
	// generate_riwayat_penyakit();
// });

//riwayat_penyakit
function generate_riwayat_penyakit(){
	if ($("#status_assemen").val()!='2'){
	var select_button_text
	if ($("#st_riwayat_penyakit").val()=='1'){
		select_button_text = $('#riwayat_penyakit option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
	}
	
	$("#riwayat_penyakit_lainnya").val(select_button_text);
	
	var select_button_text
	if ($("#st_riwayat_penyakit_dahulu").val()=='1'){
		select_button_text = $('#riwayat_penyakit_dahulu_list option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
	}
	
	$("#riwayat_penyakit_dahulu").val(select_button_text);
	var select_button_text
	if ($("#st_riwayat_penyakit_keluarga").val()=='1'){
		select_button_text = $('#riwayat_penyakit_keluarga_list option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
	}
	
	$("#riwayat_penyakit_keluarga").val(select_button_text);
	}
}
function simpan_riwayat_penyakit(jenis){// alert($("#keluhan_utama").val());
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let riwayat_penyakit=$("#riwayat_penyakit").val();
	let riwayat_penyakit_dahulu=$("#riwayat_penyakit_dahulu_list").val();
	let riwayat_penyakit_keluarga=$("#riwayat_penyakit_keluarga_list").val();
	console.log('SIMPAN RIWYAT');
	if (load_awal_assesmen==false){
	if (assesmen_id){
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/simpan_riwayat_penyakit_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					riwayat_penyakit:riwayat_penyakit,
					riwayat_penyakit_dahulu:riwayat_penyakit_dahulu,
					riwayat_penyakit_keluarga:riwayat_penyakit_keluarga,
					assesmen_id:$("#assesmen_id").val(),
					pendaftaran_id:$("#pendaftaran_id").val(),
					idpasien:idpasien,
					jenis:jenis,
					
				},
			success: function(data) {
				
						console.log(data);
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
				}
			}
		});
	}
	}
	
}
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_template_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_template_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

function set_allo(){
	$("#st_anamnesa").val('2');
	set_anamnesa();
}
function set_auto(){
	$("#st_anamnesa").val('1');
	set_anamnesa();
}

function set_anamnesa(){
	if ($("#st_anamnesa").val()=='1'){
		$(".allo_anamnesa").attr('disabled','disabled');
		$(".allo_anamnesa").removeAttr('required');
		$("#nama_anamnesa").val('');
		$("#hubungan_anamnesa").val('');
	}else{
		$(".allo_anamnesa").removeAttr('disabled');
		$(".allo_anamnesa").attr('required');
		
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
$("#medukasi_id").change(function(){
	if ($("#st_edited").val()=='0'){
	simpan_edukasi();
	}
});
$("#riwayat_penyakit").change(function(){
	generate_riwayat_penyakit();
	if ($("#st_edited").val()=='0'){
		simpan_riwayat_penyakit(1);
	}
});
$("#riwayat_penyakit_dahulu_list").change(function(){
	generate_riwayat_penyakit();
	if ($("#st_edited").val()=='0'){
		simpan_riwayat_penyakit(2);
	}
});
$("#riwayat_penyakit_keluarga_list").change(function(){
	generate_riwayat_penyakit();
	if ($("#st_edited").val()=='0'){
		simpan_riwayat_penyakit(3);
	}
});
$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
// $(".input-default").blur(function(){
	// simpan_default_warna();	
// });
$(".input-default").change(function(){
	simpan_default_warna();	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			simpan_edukasi();
			simpan_riwayat_penyakit(1);
			simpan_riwayat_penyakit(2);
			simpan_riwayat_penyakit(3);
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
function simpan_assesmen(){
	// alert($("#keluhan_utama").val());
	if (load_awal_assesmen==false){
	if (status_assemen !='2'){
		
	
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		// if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/save_asmed_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						keluhan_utama:$("#keluhan_utama").val(),
						st_anamnesa:$("#st_anamnesa").val(),
						nama_anamnesa:$("#nama_anamnesa").val(),
						hubungan_anamnesa:$("#hubungan_anamnesa").val(),
						st_riwayat_penyakit:$("#st_riwayat_penyakit").val(),
						riwayat_penyakit_lainnya:$("#riwayat_penyakit_lainnya").val(),
						st_riwayat_penyakit_dahulu:$("#st_riwayat_penyakit_dahulu").val(),
						riwayat_penyakit_dahulu:$("#riwayat_penyakit_dahulu").val(),
						st_riwayat_penyakit_keluarga:$("#st_riwayat_penyakit_keluarga").val(),
						riwayat_penyakit_keluarga:$("#riwayat_penyakit_keluarga").val(),
						pemeriksaan_penunjang:$("#pemeriksaan_penunjang").val(),
						rencana_asuhan:$("#rencana_asuhan").val(),
						intruksi:$("#intruksi").val(),
						catatan_edukasi:$("#catatan_edukasi").val(),
						rencana_kontrol:$("#rencana_kontrol").val(),
						tanggal_kontrol:$("#tanggal_kontrol").val(),
						idpoli_kontrol:$("#idpoli_kontrol").val(),
						iddokter_kontrol:$("#iddokter_kontrol").val(),
						diagnosa_utama:$("#diagnosa_utama").val(),
						txt_keterangan:$("#txt_keterangan").val(),
						txt_keterangan_diagnosa:$("#txt_keterangan_diagnosa").val(),
						keadaan_umum:$("#keadaan_umum").val(),
						tingkat_kesadaran:$("#tingkat_kesadaran").val(),
						td_sistole:$("#td_sistole").val(),
						td_diastole:$("#td_diastole").val(),
						nafas:$("#nafas").val(),
						gds:$("#gds").val(),
						st_pulang:$("#st_pulang").val(),
						tanggal_keluar:$("#tanggal_keluar").val(),
						
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
	
	
	
	function simpan_alergi(){// alert($("#keluhan_utama").val());
		let alergi_id=$("#alergi_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		let input_jenis_alergi=$("#input_jenis_alergi").val();
		let input_detail_alergi=$("#input_detail_alergi").val();
		let input_reaksi_alergi=$("#input_reaksi_alergi").val();
		if (input_detail_alergi==''){
			sweetAlert("Maaf...", "Isi Detail Alergi!", "error");
			return false;

		}
		if (input_reaksi_alergi==''){
			sweetAlert("Maaf...", "Isi Reaksi Alergi!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_alergi', 
				dataType: "JSON",
				method: "POST",
				data : {
						alergi_id:alergi_id,
						pendaftaran_id:pendaftaran_id,
						assesmen_id:assesmen_id,
						input_jenis_alergi:input_jenis_alergi,
						input_detail_alergi:input_detail_alergi,
						input_reaksi_alergi:input_reaksi_alergi,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_alergi();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_alergi();
					}
				}
			});
		}
		
	}
	
	function simpan_default_warna(){// alert($("#keluhan_utama").val());
		let warna=$("#warna").val();
		let jenis_drawing=$("#jenis_drawing").val();
	
		// $("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_default_warna', 
				dataType: "JSON",
				method: "POST",
				data : {
						warna:warna,
						jenis_drawing:jenis_drawing,
						
					},
				success: function(data) {
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Default Warna'});
				}
			});
		}
		
	}
	
	
	function simpan_edukasi(){// alert($("#keluhan_utama").val());
		let assesmen_id=$("#assesmen_id").val();
		let medukasi_id=$("#medukasi_id").val();
		console.log('SIMPAN EDUKASI');
		if (load_awal_assesmen==false){
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_edukasi_asmed_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						medukasi_id:medukasi_id,
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}
				}
			});
		}
		}
		
	}
	
	function load_skrining_risiko_jatuh(){
		let assesmen_id=$("#assesmen_id").val();
		$("#tabel_skrining tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/load_skrining_risiko_jatuh', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
					},
			success: function(data) {
				$("#tabel_skrining tbody").append(data.opsi);
				$(".nilai").select2();
				get_skor_pengkajian();
				$("#cover-spin").hide();
			}
		});
	}
	
	$(document).on("change",".nilai",function(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		var risiko_nilai_id=tr.find(".risiko_nilai_id").val();
		var nilai_id=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/update_nilai_risiko_jatuh', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					risiko_nilai_id:risiko_nilai_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					$("#st_tindakan").val(data.st_tindakan);
					 $("#nama_tindakan").val(data.nama_tindakan);
					 $("#skor_pengkajian").val(data.skor);
					 $("#risiko_jatuh").val(data.ref_nilai);
				}
			});
		
	});
	function get_skor_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#template_id").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/update_nilai_risiko_jatuh', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },
			  success: function(data) {
				 $("#st_tindakan").val(data.st_tindakan);
				 $("#nama_tindakan").val(data.nama_tindakan);
				 $("#skor_pengkajian").val(data.skor);
				 $("#risiko_jatuh").val(data.ref_nilai);
			  }
		});
		
	}
	
	function simpan_anatomi(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let manatomi_id=$("#manatomi_id").val();
		let anatomi_id=$("#anatomi_id").val();
		let ket_anatomi=$("#ket_anatomi").val();
		
		let idpasien=$("#idpasien").val();
		if (manatomi_id==''){
			sweetAlert("Maaf...", "Isi Anatomi Tubuh!", "error");
			return false;

		}
		if (manatomi_id==null){
			sweetAlert("Maaf...", "Isi Anatomi Tubuh!", "error");
			return false;

		}
		if (ket_anatomi==''){
			sweetAlert("Maaf...", "Isi Deskripsi Tidak Normal!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_anatomi_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						anatomi_id:anatomi_id,
						pendaftaran_id:pendaftaran_id,
						manatomi_id:manatomi_id,
						ket_anatomi:ket_anatomi,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_anatomi();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Diagnosa.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_anatomi();
					}
				}
			});
		}
		
	}
	function clear_input_anatomi(){
		$("#anatomi_id").val('');
		$("#manatomi_id").val('').trigger('change');
		$("#ket_anatomi").val('');
		$("#btn_add_anatomi").html('<i class="fa fa-plus"></i> Add');
	}
	function load_anatomi(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_anatomi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_anatomi').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_anatomi_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
	}
	function edit_anatomi(id){
		$("#anatomi_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_anatomi_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_anatomi").html('<i class="fa fa-save"></i> Simpan');
				$("#manatomi_id").val(data.manatomi_id).trigger('change');
				$("#ket_anatomi").val(data.ket_anatomi);
			}
		});
	}
	function hapus_anatomi(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Anatomi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_anatomi_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_anatomi();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	
	
	function simpan_diagnosa(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let mdiagnosa_id=$("#mdiagnosa_id").val();
		let diagnosa_id=$("#diagnosa_id").val();
		let prioritas=$("#prioritas").val();
		let mdiagnosa_nama=$("#mdiagnosa_id option:selected").text();	
		// alert($("#mdiagnosa_id").val());
		let idpasien=$("#idpasien").val();
		if (mdiagnosa_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mdiagnosa_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (load_awal_assesmen==false){
			
	
			if (assesmen_id){
				$.ajax({
					url: '{site_url}Tpendaftaran_poli_ttv/simpan_diagnosa_asmed_igd', 
					dataType: "JSON",
					method: "POST",
					data : {
							diagnosa_id:diagnosa_id,
							pendaftaran_id:pendaftaran_id,
							mdiagnosa_id:mdiagnosa_id,
							mdiagnosa_nama:mdiagnosa_nama,
							prioritas:prioritas,
							assesmen_id:assesmen_id,
							idpasien:idpasien,
						},
					success: function(data) {
						
						$("#cover-spin").hide();
						if (data==false){
							swal({
								title: "Gagal!",
								text: "Duplikat Diagnosa / Prioritas.",
								type: "error",
								timer: 1500,
								showConfirmButton: false
							});

						}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
							load_diagnosa();
							clear_input_diagnosa();
						}
					}
				});
			}
		}
		
	}
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val('').trigger('change');
		
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function load_diagnosa(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_diagnosa_asmed_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa();
	}
	function refresh_diagnosa(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/get_last_prioritas_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	// function load_prioritas(){
		// let assesmen_id=$("#assesmen_id").val();
	   // $.ajax({
			// url: '{site_url}Tpendaftaran_poli_ttv/get_last_prioritas_asmed_igd', 
			// dataType: "JSON",
			// method: "POST",
			// data : {
					// assesmen_id:assesmen_id,
					
				// },
			// success: function(data) {
				// $("#cover-spin").hide();


				// $("#prioritas").val(data.prioritas).trigger('change');
			// }
		// });
    // }
	function edit_diagnosa_igd(id){
		$("#diagnosa_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_diagnosa_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
				// $("#mdiagnosa_id").val(data.mdiagnosa_id).trigger('change');
				let newOption = new Option(data.nama, data.mdiagnosa_id, true, true);
					
					$("#mdiagnosa_id").append(newOption);

				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	
	function hapus_diagnosa_igd(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_diagnosa_asmed_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_diagnosa();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	
	
	// $(".data_asuhan").on("click", function(){
		// let id_det;
		 // check = $(this).is(":checked");
		// alert(check);
			// // if(check) {
				// // $(this).closest('tr').find(".isiField").removeAttr("disabled");
			// // } else {
				// // $(this).closest('tr').find(".isiField").attr('disabled', 'disabled');
			// // }
	// }); 
	$(document).on('click','.data_asuhan',function(){
		var isChecked = $(this).is(':checked');
		var data_id=$(this).closest('td').find(".data_id").val();
		var pilih;
		if (isChecked){
		  pilih=1;
		}else{
		  pilih=0;
		}
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/update_data', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:data_id,
					pilih:pilih,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
				// load_diagnosa();		
			}
		});
	  
	});
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_index_template_asmed_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/edit_template_asmed_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idpoli=$("#idpoli").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_history_pengkajian_asmed_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							idpoli:idpoli,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_edit_asmed_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					let pendaftaran_id_ranap=data.pendaftaran_id;
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tpendaftaran_poli_ttv/tindakan/"+pendaftaran_id_ranap+"/erm_rj/input_asmed_igd'); ?>";
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_asmed_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_asmed_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	$(document).on('change','#rencana_kontrol',function(){
		set_rencana_kontrol();
	});
	function set_rencana_kontrol(){
		let rencana_kontrol=$("#rencana_kontrol").val();
		let tanggal=$("#tanggal").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/set_rencana_kontrol', 
			dataType: "JSON",
			method: "POST",
			data : {
					tanggal:tanggal,
					rencana_kontrol:rencana_kontrol,
				   },
			success: function(data) {
				// alert(data);
				$("#tanggal_kontrol").val(data);
				simpan_assesmen();
			}
		});
	}
</script>