<style>
	.edited_final{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	.edited2{
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.edited_final {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='his_assesmen_igd'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_assesmen_igd' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					$waktutindakan=date('H:i:s');
					$waktuinfus=date('H:i:s');
					$tanggalinfus=date('d-m-Y');
					$tanggaltindakan=date('d-m-Y');
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_assesmen_igd=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_assesmen_igd" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_igd" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_igd()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Tanggal Masuk</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="<?=($tanggal_datang!=$tanggal_datang_asal?'edited':'')?> js-datepicker form-control auto_blur_tgl" data-date-format="dd/mm/yyyy" id="tanggaldatang" placeholder="HH/BB/TTTT" name="tanggaldatang" value="<?= $tanggaldatang ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Jam Masuk</label>
									<div class="input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="<?=($tanggal_datang!=$tanggal_datang_asal?'edited':'')?> time-datepicker form-control auto_blur_tgl" id="waktudatang" value="<?= $waktudatang ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Surat pengantar / Rujukan</label>
									<select tabindex="8" id="surat_pengantar" class="<?=($surat_pengantar!=$surat_pengantar_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($surat_pengantar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(55) as $row){?>
										<option value="<?=$row->id?>" <?=($surat_pengantar == $row->id ? 'selected="selected"' : '')?> <?=(($surat_pengantar == '0'||$surat_pengantar == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Sarana Transportasi</label>
									<select tabindex="8" id="sarana_transport" class="<?=($sarana_transport!=$sarana_transport_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sarana_transport == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(52) as $row){?>
										<option value="<?=$row->id?>" <?=($sarana_transport == $row->id ? 'selected="selected"' : '')?> <?=(($sarana_transport == '0' || $sarana_transport == '')  && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
									
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Asal Rujukan</label>
									<select tabindex="32" name="idasalpasien" id="idasalpasien" class="<?=($idasalpasien!=$idasalpasien_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?php foreach (get_all('mpasien_asal') as $r):?>
											<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<?php if ($pendaftaran_id != '' && ($idasalpasien == '2' || $idasalpasien == '3')) {?>
								<?php
									if ($idasalpasien == 2) {
										$idtipeRS = 1;
									} elseif ($idasalpasien == 3) {
										$idtipeRS = 2;
									}
									
								}else{
									$idtipeRS='';
								}
								?>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Nama Fasilitas Kesehatan</label>
										<select tabindex="33" name="idrujukan" id="idrujukan" class="<?=($idrujukan!=$idrujukan_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach (get_all('mrumahsakit', ['idtipe' => $idtipeRS]) as $r) {?>
												<option value="<?= $r->id?>" <?=($idrujukan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
											<?php }?>
										</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Cara Masuk</label>
									<select tabindex="8" id="cara_masuk" name="cara_masuk" class="<?=($cara_masuk!=$cara_masuk_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($cara_masuk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(53) as $row){?>
										<option value="<?=$row->id?>" <?=($cara_masuk == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($cara_masuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Macam Kasus</label>
									<select tabindex="8" id="macam_kasus" name="macam_kasus" class="<?=($macam_kasus!=$macam_kasus_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($macam_kasus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(54) as $row){?>
										<option value="<?=$row->id?>" <?=($macam_kasus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($macam_kasus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Nama pengantar</label>
									<input tabindex="29" type="text" class="form-control <?=($namapengantar!=$namapengantar_asal?'edited':'')?>"   id="namapengantar" placeholder="Nama Pengantar" name="namapengantar" value="{namapengantar}" required>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">No Tlp Penanggung Jawab</label>
									<input tabindex="32" type="text" class="form-control <?=($teleponpengantar!=$teleponpengantar_asal?'edited':'')?>"   id="teleponpengantar" placeholder="Telepon" name="teleponpengantar" value="{teleponpengantar}" required>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >KASUS KECELAKAAN LALULINTAS </h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Kasus Kecelakaan ?</label>
									<select tabindex="8" id="st_kecelakaan" name="st_kecelakaan" class="<?=($st_kecelakaan!=$st_kecelakaan_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="1" <?=($st_kecelakaan == '1' ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($st_kecelakaan == '0' ? 'selected="selected"' : '')?>>TIDAK</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Jenis Kecelakaan</label>
									<select tabindex="8" id="jenis_kecelakaan" name="jenis_kecelakaan" class="<?=($jenis_kecelakaan!=$jenis_kecelakaan_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_kecelakaan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(19) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kecelakaan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_kecelakaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="jenis_kendaraan_pasien">Jenis Kendaraan Pasien</label>
									<select tabindex="8" id="jenis_kendaraan_pasien" class="<?=($jenis_kendaraan_pasien!=$jenis_kendaraan_pasien_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_kendaraan_pasien == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(63) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kendaraan_pasien == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_kendaraan_pasien == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="jenis_kendaraan_lawan">Jenis Kendaraan Lawan</label>
									<select tabindex="8" id="jenis_kendaraan_lawan" class="<?=($jenis_kendaraan_lawan!=$jenis_kendaraan_lawan_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_kendaraan_lawan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(64) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kendaraan_lawan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_kendaraan_lawan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="lokasi_kecelakaan">Lokasi Kecelakaan</label>
									<select tabindex="8" id="lokasi_kecelakaan" class="<?=($lokasi_kecelakaan!=$lokasi_kecelakaan_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($lokasi_kecelakaan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(65) as $row){?>
										<option value="<?=$row->id?>" <?=($lokasi_kecelakaan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($lokasi_kecelakaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="lokasi_lainnya">Jika Tidak ada dalam pilihan, sebutkan</label>
									<textarea id="lokasi_lainnya" class="<?=($lokasi_lainnya!=$lokasi_lainnya_asal?'edited':'')?> form-control auto_blur" name="story" rows="1" style="width:100%"><?=$lokasi_lainnya?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >ANAMNESIS </h5>
							</div>
						</div>
						<input class="form-control"  type="hidden" id="st_anamnesa" name="st_anamnesa" value="<?=$st_anamnesa?>" >				
								
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Keluhan Utama </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" value="1" id="auto_anamnesa" name="anamnesa" onclick="set_auto()" <?=($st_anamnesa=='1'?'checked':'')?>><span></span> Auto Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  value="1" id="allo_anamnesa" name="anamnesa" onclick="set_allo()" <?=($st_anamnesa=='2'?'checked':'')?>><span></span> Allo Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control auto_blur push-5-r <?=($nama_anamnesa!=$nama_anamnesa_asal?'edited':'')?>"  type="text"  id="nama_anamnesa" name="nama_anamnesa" value="{nama_anamnesa}" placeholder="Tuliskan Nama" >
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control <?=($hubungan_anamnesa!=$hubungan_anamnesa_asal?'edited':'')?> "  type="text" id="hubungan_anamnesa" name="hubungan_anamnesa" value="{hubungan_anamnesa}" placeholder="Hubungan" >
								</div>
								<div class="col-md-12 push-10-t">
									<textarea id="keluhan_utama" class="form-control <?=($keluhan_utama!=$keluhan_utama_asal?'edited':'')?>" name="story" rows="2" style="width:100%"><?=$keluhan_utama?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit" name="st_riwayat_penyakit" value="<?=$st_riwayat_penyakit?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(0)" <?=($st_riwayat_penyakit=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(1)" <?=($st_riwayat_penyakit=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									<select id="riwayat_penyakit" name="riwayat_penyakit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach($list_riwayat_penyakit as $r){?>
										<option value="<?=$r->id?>" <?=$r->pilih?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_lainnya" class="form-control <?=($riwayat_penyakit_lainnya!=$riwayat_penyakit_lainnya_asal?'edited':'')?>" name="riwayat_penyakit_lainnya" rows="2" style="width:100%"><?=$riwayat_penyakit_lainnya?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Alergi </label>
								<div class="col-md-12">
									<select id="riwayat_alergi" name="riwayat_alergi" class="<?=($riwayat_alergi!=$riwayat_alergi_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_alergi=='0'?'selected':'')?>>Tidak Ada Alergi</option>
										<option value="1" <?=($riwayat_alergi=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_alergi=='2'?'selected':'')?>>Ada Alergi</option>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Pengobatan </label>
								<div class="col-md-12">
									<textarea id="riwayat_pengobatan" class="form-control <?=($riwayat_pengobatan!=$riwayat_pengobatan_asal?'edited':'')?> " name="riwayat_pengobatan" rows="1" style="width:100%"><?=$riwayat_pengobatan?></textarea>
								</div>
							</div>
						</div>
						
						<div class="form-group div_alergi_all">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="block">
										<ul class="nav nav-tabs nav-tabs-alt nav-justified" data-toggle="tabs">
											<li class="" id="li_alergi_1">
												<a href="#alergi_1"  onclick="load_alergi_igd()"><i class="fa fa-pencil "></i> Data Alergi</a>
											</li>
											<li class="" id="li_alergi_2">
												<a href=" #alergi_2" onclick="load_alergi_igd_his()"><i class="fa fa-history"></i> History Alergi</a>
											</li>
											
										</ul>
										<div class="block-content tab-content ">
											<div class="tab-pane " id="alergi_1">
												<div class="table-responsive div_data_alergi">
													<table class="table" id="index_alergi">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="tab-pane " id="alergi_2">
												<div class="table-responsive">
													<table class="table" id="index_alergi_his">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							
							</div>
							<div class="col-md-6 ">
									
							</div>
							
						</div>
						<div class="form-group ">
							
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="st_hamil">Status Kehamilan</label>
									<select id="st_hamil" class="<?=($st_hamil!=$st_hamil_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($st_hamil=='0'?'selected':'')?>>TIDAK</option>
										<option value="1" <?=($st_hamil=='1'?'selected':'')?>>YA</option>
										
									</select>
								</div>
								<div class="col-md-3 ">
									<label for="hamil_g">G</label>
									<div class="input-group">
										<input class="form-control <?=($hamil_g!=$hamil_g_asal?'edited':'')?> "  type="text" id="hamil_g" value="{hamil_g}"  placeholder="G" required>
										<span class="input-group-addon">G</span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="hamil_g">P</label>
									<div class="input-group">
										<input class="form-control <?=($hamil_p!=$hamil_p_asal?'edited':'')?>"  type="text" id="hamil_p" value="{hamil_p}"  placeholder="P" required>
										<span class="input-group-addon">P</span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="hamil_g">A</label>
									<div class="input-group">
										<input class="form-control <?=($hamil_a!=$hamil_a_asal?'edited':'')?>"  type="text" id="hamil_a" value="{hamil_a}"  placeholder="A" required>
										<span class="input-group-addon">A</span>
									</div>
								</div>
								
							</div>
							
						</div>
						<div class="form-group ">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="index_obat">
											<thead>
												<tr>
													<th width="30%">Obat</th>
													<th width="30%">Dosis</th>
													<th width="30%">Waktu Penggunaan</th>
													<th width="10%">Action</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="st_menyusui">Status Menyusui</label>
									<select id="st_menyusui" class="<?=($st_menyusui!=$st_menyusui_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($st_menyusui=='0'?'selected':'')?>>TIDAK</option>
										<option value="1" <?=($st_menyusui=='1'?'selected':'')?>>YA</option>
										
									</select>
								</div>
								<div class="col-md-9 ">
									<label for="info_menyusui">Informasi Lainnya</label>
									<input type="text" class="form-control input-sm <?=($info_menyusui!=$info_menyusui_asal?'edited':'')?> " id="info_menyusui" value="{info_menyusui}"> 
								</div>
								
								
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Fisik </h5>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Keadaan Umum</label>
									<select tabindex="8" id="keadaan_umum"   name="keadaan_umum" class="<?=($keadaan_umum!=$keadaan_umum_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(61) as $row){?>
										<option value="<?=$row->id?>"  <?=($keadaan_umum == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Tingkat Kesadaran</label>
									<select tabindex="8" id="tingkat_kesadaran"   name="tingkat_kesadaran" class="<?=($tingkat_kesadaran!=$tingkat_kesadaran_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nadi!=$nadi_asal?'edited':'')?>"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nafas!=$nafas_asal?'edited':'')?>"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal <?=($td_sistole!=$td_sistole_asal?'edited':'')?>"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal <?=($td_diastole!=$td_diastole_asal?'edited':'')?>"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal <?=($suhu!=$suhu_asal?'edited':'')?>"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Tinggi Badan</label>
									<div class="input-group">
										<input class="form-control decimal <?=($tinggi_badan!=$tinggi_badan_asal?'edited':'')?>"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
										<span class="input-group-addon">Cm</span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Berat Badan</label>
									<div class="input-group">
										<input class="form-control decimal <?=($berat_badan!=$berat_badan_asal?'edited':'')?>"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Psikologis, Sosial Ekonomi, Spiritual </h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Hubungan Pasien Dengan Anggota Keluarga</label>
									<select tabindex="8" id="hubungan_anggota_keluarga"  class="<?=($hubungan_anggota_keluarga!=$hubungan_anggota_keluarga_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($hubungan_anggota_keluarga == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(41) as $row){?>
										<option value="<?=$row->id?>" <?=($hubungan_anggota_keluarga == $row->id ? 'selected="selected"' : '')?> <?=($hubungan_anggota_keluarga == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Status Psikologis</label>
									<select tabindex="8" id="st_psikologis" name="st_psikologis" class="<?=($st_psikologis!=$st_psikologis_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_psikologis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(26) as $row){?>
										<option value="<?=$row->id?>" <?=($st_psikologis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_psikologis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Sosial Ekonomi</label>
									<select tabindex="8" id="st_sosial_ekonomi" name="st_sosial_ekonomi" class="<?=($st_sosial_ekonomi!=$st_sosial_ekonomi_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_sosial_ekonomi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(27) as $row){?>
										<option value="<?=$row->id?>" <?=($st_sosial_ekonomi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_sosial_ekonomi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Spiritual</label>
									<select tabindex="8" id="st_spiritual" name="st_spiritual" class="<?=($st_spiritual!=$st_spiritual_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_spiritual == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(28) as $row){?>
										<option value="<?=$row->id?>" <?=($st_spiritual == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_spiritual == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Pendidikan</label>
									<select tabindex="8" id="pendidikan" name="pendidikan" class=" form-control  select2_edit" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pendidikan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(13) as $row){?>
										<option value="<?=$row->id?>" <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Pekerjaan</label>
									<select tabindex="8" id="pekerjaan" name="pekerjaan" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pekerjaan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(6) as $row){?>
										<option value="<?=$row->id?>" <?=($pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Agama</label>
									<select tabindex="8" id="agama" name="agama" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($agama == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(3) as $row){?>
										<option value="<?=$row->id?>" <?=($agama == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Status Nutrisi</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Keluhan, Perubahan Nafsu Makan</label>
									<select tabindex="8" id="st_nafsu_makan" name="st_nafsu_makan" class="<?=($st_nafsu_makan!=$st_nafsu_makan_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_nafsu_makan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(29) as $row){?>
										<option value="<?=$row->id?>"  <?=($st_nafsu_makan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_nafsu_makan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penurunan Berat Badan, 3 Bln terakhir</label>
									<select tabindex="8" id="st_turun_bb" name="st_turun_bb" class="<?=($st_turun_bb!=$st_turun_bb_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_turun_bb == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(30) as $row){?>
										<option value="<?=$row->id?>" <?=($st_turun_bb == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_turun_bb == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Mual</label>
									<select tabindex="8" id="st_mual" name="st_mual" class="<?=($st_mual!=$st_mual_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_mual == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(31) as $row){?>
										<option value="<?=$row->id?>" <?=($st_mual == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_mual == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Muntah</label>
									<select tabindex="8" id="st_muntah" name="st_muntah" class="<?=($st_muntah!=$st_muntah_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_muntah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(32) as $row){?>
										<option value="<?=$row->id?>" <?=($st_muntah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_muntah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Faktor Pemberat (Komorbid)</label>
									<select tabindex="8" id="faktor_komorbid" name="faktor_komorbid" class="<?=($faktor_komorbid!=$faktor_komorbid_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($faktor_komorbid == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(66) as $row){?>
										<option value="<?=$row->id?>"  <?=($faktor_komorbid == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($faktor_komorbid == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penurunan Kapasitas Fungsi</label>
									<select tabindex="8" id="penurunan_fungsi" name="penurunan_fungsi" class="<?=($penurunan_fungsi!=$penurunan_fungsi_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($penurunan_fungsi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(67) as $row){?>
										<option value="<?=$row->id?>"  <?=($penurunan_fungsi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($penurunan_fungsi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >SKRINING RISIKO CEDERA</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<?
									$q="SELECT *FROM mrisiko_jatuh WHERE idtipe='2' AND staktif='1'";
									$list_template_risiko=$this->db->query($q)->result();
									?>
									<label for="template_id">Mode Penilaian Risiko Jatuh </label>
									<select id="template_id"  name="template_id" class="<?=($template_id!=$template_id_asal?'edited':'')?> js-select2 form-control"  data-placeholder="Choose one..">
										<option value="" <?=($template_id=='0'?'selected':'')?>>-Belum dipilih-</option>
										<?foreach($list_template_risiko as $r){?>
										<option value="<?=$r->id?>" <?=($template_id==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Skor Pengkajian</label>
									<input class="form-control <?=($skor_pengkajian!=$skor_pengkajian_asal?'edited':'')?>" readonly type="text" id="skor_pengkajian"  value="{skor_pengkajian}" placeholder="Skor Pengkajian" >
									<input class="form-control " type="hidden" id="st_tindakan"  value="" placeholder="" >
								</div>
								<div class="col-md-9 ">
									<label for="example-input-normal">Hasil Pengkajian</label>
									<input class="form-control <?=($risiko_jatuh!=$risiko_jatuh_asal?'edited':'')?>" readonly type="text" id="risiko_jatuh"  value="{risiko_jatuh}" name="risiko_jatuh" placeholder="Hasil Pengkajian" >
								</div>
							</div>
						</div>
						<?if ($risiko_jatuh_header){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" ><?=$risiko_jatuh_header?></h5>
							</div>
						</div>
						
						<div class="form-group push-5-t">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-condensed" id="tabel_skrining">
											<thead>
												<tr>
													<th width="80%">Skrining</th>
													<th width="20%">Jawaban</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tindakan</label>
									<input class="form-control <?=($nama_tindakan!=$nama_tindakan_asal?'edited':'')?>" readonly type="text" id="nama_tindakan"  value="{nama_tindakan}" placeholder="Tindakan" >
								</div>
								
								<div class="col-md-6 ">
									<label for="example-input-normal">Apakah Tindakan Dilakukan</label>
									<select tabindex="8" id="st_tindakan_action" name="st_tindakan_action" class="<?=($st_tindakan_action!=$st_tindakan_action_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_tindakan_action == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="1" <?=($st_tindakan_action == 1 ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($st_tindakan_action == 0 ? 'selected="selected"' : '')?>>TIDAK</option>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary"><?=$risiko_jatuh_footer?></label>
							</div>
						</div>
						<?}?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >STATUS FUNGSIONAL</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Status Fungsional</label>
									<select tabindex="8" id="st_fungsional" name="st_fungsional" class="<?=($st_fungsional!=$st_fungsional_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_fungsional == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(33) as $row){?>
										<option value="<?=$row->id?>" <?=($st_fungsional == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_fungsional == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penggunaan Alat Bantu</label>
									<select tabindex="8" id="st_alat_bantu" name="st_alat_bantu" class="<?=($st_alat_bantu!=$st_alat_bantu_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_alat_bantu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(34) as $row){?>
										<option value="<?=$row->id?>" <?=($st_alat_bantu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_alat_bantu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Cacat Tubuh</label>
									<select tabindex="8" id="st_cacat" name="st_cacat" class="<?=($st_cacat!=$st_cacat_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_cacat == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(35) as $row){?>
										<option value="<?=$row->id?>" <?=($st_cacat == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_cacat == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Lainnya Sebutkan</label>
									<input class="form-control <?=($cacat_lain!=$cacat_lain_asal?'edited':'')?> " type="text" id="cacat_lain"  value="{cacat_lain}" name="cacat_lain" placeholder="Lainnya" >
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >SKRINING DECUBITUS, P3 DAN BATUK</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Terdapat Risiko Luka Decibitus</label>
									<select tabindex="8" id="st_decubitus" name="st_decubitus" class="<?=($st_decubitus!=$st_decubitus_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_decubitus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(68) as $row){?>
										<option value="<?=$row->id?>" <?=($st_decubitus == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_decubitus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Perencanaan Pemulangan Pasien (P3)</label>
									<select tabindex="8" id="st_alat_bantu" name="st_p3" class="<?=($st_alat_bantu!=$st_alat_bantu_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_p3 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(69) as $row){?>
										<option value="<?=$row->id?>" <?=($st_p3 == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_p3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4">
									<label for="st_riwayat_demam">Riwayat Demam</label>
									<select tabindex="8" id="st_riwayat_demam" name="st_riwayat_demam" class="<?=($st_riwayat_demam!=$st_riwayat_demam_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_riwayat_demam == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(70) as $row){?>
										<option value="<?=$row->id?>" <?=($st_riwayat_demam == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_riwayat_demam == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4">
									<label for="st_riwayat_demam">Berkeringat Pada malam hari tanpa aktivitas</label>
									<select tabindex="8" id="st_berkeringat_malam" class="<?=($st_berkeringat_malam!=$st_berkeringat_malam_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_berkeringat_malam == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(70) as $row){?>
										<option value="<?=$row->id?>" <?=($st_berkeringat_malam == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_berkeringat_malam == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4">
									<label for="st_riwayat_demam">Riwayat Bepergian dari daerah wabah</label>
									<select tabindex="8" id="st_pergi_area_wabah" class="<?=($st_pergi_area_wabah!=$st_pergi_area_wabah_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_pergi_area_wabah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(73) as $row){?>
						<option value="<?=$row->id?>" <?=(($st_pergi_area_wabah == '0' || $st_pergi_area_wabah == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_pergi_area_wabah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="st_riwayat_demam">Riwayat Pemakaian obat jangka panjang</label>
									<select tabindex="8" id="st_pemakaian_obat_panjang" class="<?=($st_pemakaian_obat_panjang!=$st_pemakaian_obat_panjang_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_pemakaian_obat_panjang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(71) as $row){?>
										<option value="<?=$row->id?>" <?=(($st_pemakaian_obat_panjang == '0' || $st_pemakaian_obat_panjang == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_pemakaian_obat_panjang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="st_riwayat_demam">Riwayat Berat badan turun tanpa sebab yang diketahui</label>
									<select tabindex="8" id="st_turun_bb_tanpa_sebab" name="st_turun_bb_tanpa_sebab" class="<?=($st_turun_bb_tanpa_sebab!=$st_turun_bb_tanpa_sebab_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=(($st_turun_bb_tanpa_sebab == ''  || $st_turun_bb_tanpa_sebab == '0')  ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(72) as $row){?>
										<option value="<?=$row->id?>" <?=(($st_turun_bb_tanpa_sebab == '0' || $st_turun_bb_tanpa_sebab=='') && $row->st_default=='1'? 'selected="selected"' : '')?> <?=($st_turun_bb_tanpa_sebab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="h5 text-primary" >DIAGNOSA KEPERAWATAN</h5>
							</div>
							<div class="col-md-6 ">
								<label class="h5 text-primary" >RENCANA ASUHAN KEPERAWATAN</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Diagnosa Keperawatan</th>
													<th width="10%">Priority</th>
													<th width="25%">User</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa</label>
									<select tabindex="8" id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
								<div class="col-md-12 push-10-t">
									<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
										<thead></thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="intruksi_keperawatan" class="text-primary">Intruksi Keperawatan</label>
									<textarea class="form-control auto_blur" id="intruksi_keperawatan"  rows="2" placeholder="Intruksi Keperawatan"> <?=$intruksi_keperawatan?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >PEMBERIAN OBAT / INFUS</h5>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered" id="index_obat_infus">
											<thead>
												<tr>
													<th width="9%" class="text-center">Tanggal</th>
													<th width="7%" class="text-center">Jam</th>
													<th width="17%" class="text-center">Nama Obat / Infus</th>
													<th width="5%" class="text-center">Kuantitas</th>
													<th width="8%" class="text-center">Dosis</th>
													<th width="10%" class="text-center">Rute Pemberian</th>
													<th width="12%" class="text-center">Diperiksa Oleh</th>
													<th width="12%" class="text-center">Diberikan Oleh</th>
													<th width="8%" class="text-center">Versi Edited</th>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >TINDAKAN</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered" id="index_tindakan">
											<thead>
												<tr>
													<th width="12%">Tanggal</th>
													<th width="10%">Jam</th>
													<th width="25%">Nama Tindakan</th>
													<th width="15%">Petugas Yang Melaksanakan</th>
													<th width="10%">Waktu Mulai</th>
													<th width="10%">Waktu Selesai</th>
													<th width="20%">Action</th>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >EDUKASI</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Kesediaan Pasien / Keluarga untuk menerima informasi yang diberikan</label>
									<select tabindex="8" id="st_menerima_info" name="st_menerima_info" class="<?=($st_menerima_info!=$st_menerima_info_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_menerima_info == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(39) as $row){?>
										<option value="<?=$row->id?>" <?=($st_menerima_info == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_menerima_info == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat hambatan dalam edukasi</label>
									<select tabindex="8" id="st_hambatan_edukasi" name="st_hambatan_edukasi" class="<?=($st_hambatan_edukasi!=$st_hambatan_edukasi_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_hambatan_edukasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(40) as $row){?>
										<option value="<?=$row->id?>" <?=($st_hambatan_edukasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_hambatan_edukasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Dibutuhkan Penerjemaah ? (Jika Ya Sebutkan)</label>
									<select tabindex="8" id="st_penerjemaah" name="st_penerjemaah" class="<?=($st_penerjemaah!=$st_penerjemaah_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_penerjemaah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(38) as $row){?>
										<option value="<?=$row->id?>" <?=($st_penerjemaah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_penerjemaah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penerjemaah Yang Dibutuhkan</label>
									<input class="form-control <?=($penerjemaah!=$penerjemaah_asal?'edited':'')?>" type="text" id="penerjemaah"  value="{penerjemaah}" name="penerjemaah" placeholder="Penerjemaah Yang Dibutuhkan" >
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Kebutuhan Edukasi</label>
									<input class="form-control <?=($edukasi_current!=$edukasi_last?'edited':'')?>" type="text"  value="{edukasi_current}"placeholder="Edukasi" >
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Catatan lain Edukasi</label>
									<input class="form-control <?=($catatan_edukasi!=$catatan_edukasi_asal?'edited':'')?>" type="text" id="catatan_edukasi"  value="{catatan_edukasi}" name="catatan_edukasi" placeholder="Catatan" >
									
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >RINGKASAN KONDISI KELUAR</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Keadaan Umum</label>
									<select tabindex="8" id="keadaan_umum_keluar" name="keadaan_umum_keluar" class="<?=($keadaan_umum_keluar!=$keadaan_umum_keluar_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($keadaan_umum_keluar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(61) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_umum_keluar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($keadaan_umum_keluar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="tingkat_kesadaran_keluar">Tingkat Kesadaran</label>
									<select tabindex="8" id="tingkat_kesadaran_keluar" class="<?=($tingkat_kesadaran_keluar!=$tingkat_kesadaran_keluar_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($tingkat_kesadaran_keluar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(40) as $row){?>
										<option value="<?=$row->id?>" <?=($tingkat_kesadaran_keluar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tingkat_kesadaran_keluar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal <?=($td_sistole_keluar!=$td_sistole_keluar_asal?'edited':'')?>"  type="text" id="td_sistole_keluar"  value="{td_sistole_keluar}" name="td_sistole_keluar" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal <?=($td_diastole_keluar!=$td_diastole_keluar_asal?'edited':'')?>"  type="text" id="td_diastole_keluar" value="{td_diastole_keluar}"  name="td_diastole_keluar" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Pernafasan</label>
									<div class="input-group">
										<input class="form-control decimal <?=($nafas_keluar!=$nafas_keluar_asal?'edited':'')?>"   type="text" id="nafas_keluar" value="{nafas_keluar}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">GDS</label>
									<div class="input-group">
										<input class="form-control decimal <?=($gds_keluar!=$gds_keluar_asal?'edited':'')?>"   type="text" id="gds_keluar" value="{gds_keluar}"  placeholder="GDS" required>
										<span class="input-group-addon"><?=$satuan_gds?></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Status Pulang</label>
									<select tabindex="8" id="st_pulang" name="st_pulang" class="<?=($st_pulang!=$st_pulang_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(62) as $row){?>
										<option value="<?=$row->id?>" <?=($st_pulang == $row->id ? 'selected="selected"' : '')?> <?=($st_pulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Pasien Keluar IGD (Tanggal)</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control <?=($tanggal_keluar!=$tanggal_keluar_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="<?= $tanggal_keluar ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Rencana Kontrol</label>
									<select tabindex="8" id="rencana_kontrol" name="rencana_kontrol" class="<?=($rencana_kontrol!=$rencana_kontrol_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($rencana_kontrol == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(43) as $row){?>
										<option value="<?=$row->id?>" <?=(($rencana_kontrol == '0' || $rencana_kontrol == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($rencana_kontrol == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Tanggal Kontrol</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control <?=($tanggal_kontrol!=$tanggal_kontrol_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tanggal_kontrol" placeholder="dd-mm-yyyy" value="<?= $tanggal_kontrol ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							
							<?
								if ($idpoli_kontrol==''){
									$idpoli_kontrol=$idpoliklinik;
								}
								if ($iddokter_kontrol=='0' || $iddokter_kontrol==''){
									$iddokter_kontrol=$iddokter;
								}
							?>
							<div class="col-md-6">
								<div class="col-md-12">
									<label for="example-input-normal">Poliklinik </label>
									<select id="idpoli_kontrol" name="idpoli_kontrol" class="<?=($idpoli_kontrol!=$idpoli_kontrol_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Choose one..">
										<option value="0"  <?=($idpoli_kontrol==''?'selected':'')?>>- Tidak Ada -</option>
										<?foreach($list_poli as $r){?>
										<option value="<?=$r->id?>" <?=($idpoli_kontrol==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
									
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12 ">
									<label for="example-input-normal">Dokter darisana<?=$iddokter_kontrol?></label>
									<select id="iddokter_kontrol" class="<?=($iddokter_kontrol!=$iddokter_kontrol_asal?'edited':'')?> form-control " style="width: 100%;" data-placeholder="Choose one..">
										<option value="0"  <?=($iddokter_kontrol==''?'selected':'')?>>- Tidak Ada -</option>
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"  <?=($iddokter_kontrol==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
										
									</select>
									
								</div>
								
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal fade in black-overlay" id="obat_non_racikan-modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<?
					$q="SELECT H.idtipe,T.nama_tipe from munitpelayanan_tipebarang H
					LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
					WHERE H.idunitpelayanan='1'";
					$list_tipe_barang=$this->db->query($q)->result();
				?>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Tipe Barang</label>
								<div class="col-md-8">
									<select name="tipeid" id="tipeid" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
										<?foreach ($list_tipe_barang as $r){?>
										<option value="<?=$r->idtipe?>"><?=$r->nama_tipe?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-8">
									<select name="idkat" id="idkat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang" name="btn_filter_barang" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="table-Obat" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Satuan</th>
								
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});

$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	// set_ttd_assesmen();
	// $(".js-datetimepicker").datetimepicker({
		// format: "dd-mm-yyyy HH:mm"
	// });
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		set_kecelakaan();
		set_anamnesa();
		set_riwayat_alergi();
		set_riwayat($("#st_riwayat_penyakit").val())
		load_awal_assesmen=false;
		load_alergi_igd();
		load_obat_igd();
		load_obat_infus_igd();
		load_tindakan_assesmen_igd();
		// load_alergi_igd_his();
		load_skrining_risiko_jatuh_igd();
		load_diagnosa_igd();
		list_index_history_edit();
	}
	
	// load_data_rencana_asuhan_igd(1);
});
$("#obat_id_select2").change(function() {
			// console.log('data sini masuk');
	data = $("#obat_id_select2").select2('data')[0];
	if (data){
	$("#idtipe").val(data.idtipe);
	}
	

});
$("#template_id").on("change", function(){
	let template_id=$(this).val();
	if (template_id!=template_id_last){
			swal({
			  title: 'Anda Yakain?',
			  text: "Mengganti Mode Penilaian!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Proses!'
			}).then(function(json_data) {
					$("#cover-spin").show();
					let assesmen_id=$("#assesmen_id").val();
					template_id_last=template_id;
					$.ajax({
						url: '{site_url}Tpendaftaran_poli_ttv/simpan_risiko_jatuh_template_igd', 
						dataType: "JSON",
						method: "POST",
						data : {
								assesmen_id:assesmen_id,
								template_id:template_id,
								
							},
						success: function(data) {
							
									console.log(data);
							if (data==null){
								swal({
									title: "Gagal!",
									text: "Simpan TTV.",
									type: "error",
									timer: 1500,
									showConfirmButton: false
								});

							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Ganti Mode Penilaian '});
								location.reload();			
							}
						}
					});
			}, function(dismiss) {
			  if (dismiss === 'cancel' || dismiss === 'close') {
					$("#template_id").val(template_id_last).trigger('change.select2');
			  } 
			})
		}else{
			save_all_detail(kategori_id_last);
		}
		
	
	
});
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "20%", "targets": [1] },
					 // { "width": "15%", "targets": [3] },
					 // { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_assesmen_igd', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 // $("#riwayat_penyakit").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $(".his_filter").removeAttr('disabled');
		 $(".edited").removeClass("edited").addClass("edited_final");
	}
}

function set_riwayat(st_riwayat_penyakit){
	$("#st_riwayat_penyakit").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit").removeAttr('disabled');
		$("#riwayat_penyakit_lainnya").removeAttr('disabled');
		$("#riwayat_penyakit").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit").val('').trigger('change');
		$("#riwayat_penyakit").removeAttr('required');
		$("#riwayat_penyakit").attr('disabled','disabled');
		$("#riwayat_penyakit_lainnya").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
// $("#riwayat_penyakit").change(function(){
	// generate_riwayat_penyakit();
// });

//riwayat_penyakit
function generate_riwayat_penyakit(){
	var select_button_text
	let str_penyakit='';
	if ($("#st_riwayat_penyakit").val()=='1'){
		select_button_text = $('#riwayat_penyakit option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
		// selected['Tidak Ada']
	}
	
	$("#riwayat_penyakit_lainnya").val(select_button_text);
	console.log(select_button_text);
}
function set_allo(){
	$("#st_anamnesa").val('2');
	set_anamnesa();
}
function set_auto(){
	$("#st_anamnesa").val('1');
	set_anamnesa();
}
function set_riwayat_alergi(){
	let riwayat=$("#riwayat_alergi").val();
	if (riwayat=='2'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").show();
		$(".div_input_alergi").show();
		$("#li_alergi_1").addClass('active');
		$("#li_alergi_2").removeClass('active');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else if (riwayat=='1'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").hide();
		$(".div_input_alergi").hide();
		// // $('#alergi_2 > a').tab('show');
		// $('#alergi_1 > a').tab('hide');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else{
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
		$(".div_alergi_all").hide();
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	
		
	}
	
}
function set_kecelakaan(){
	let st_kecelakaan=$("#st_kecelakaan").val();
	if (st_kecelakaan=='1'){
		$(".div_kecelakaan").show();
		
	}else{
		$(".div_kecelakaan").hide();
		$("#jenis_kecelakaan").val(0).trigger('change');
		$("#jenis_kendaraan_pasien").val(0).trigger('change');
		$("#jenis_kendaraan_lawan").val(0).trigger('change');
		$("#lokasi_kecelakaan").val(0).trigger('change');
		$("#lokasi_lainnya").val('');
	}
}
$("#st_kecelakaan").on("change", function(){
	
	set_kecelakaan();
});
function set_anamnesa(){
	if ($("#st_anamnesa").val()=='1'){
		$(".allo_anamnesa").attr('disabled','disabled');
		$(".allo_anamnesa").removeAttr('required');
		$("#nama_anamnesa").val('');
		$("#hubungan_anamnesa").val('');
	}else{
		$(".allo_anamnesa").removeAttr('disabled');
		$(".allo_anamnesa").attr('required');
		
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
	function load_tindakan_assesmen_igd(){
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$.ajax({
				url: '{site_url}thistory_tindakan/load_tindakan_assesmen_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						versi_edit:$("#versi_edit").val(),
						
					},
				success: function(data) {
					$("#index_tindakan tbody").empty();
					$("#index_tindakan tbody").append(data.tabel);
				}
			});
	}
	function load_alergi_igd(){
		$('#alergi_2 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$('#index_alergi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_alergi_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							versi_edit:versi_edit,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	function load_obat_igd(){
		
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$('#index_obat').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_obat').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_obat_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							versi_edit:versi_edit,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	function load_obat_infus_igd(){
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$.ajax({
				url: '{site_url}thistory_tindakan/load_obat_infus_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						versi_edit:$("#versi_edit").val(),
						
					},
				success: function(data) {
					$("#index_obat_infus tbody").empty();
					$("#index_obat_infus tbody").append(data.tabel);
				}
			});
	}
	function load_alergi_igd_his(){
		$('#alergi_1 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_alergi_his').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi_his').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi_his', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_alergi_igd_pasien(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_skrining_risiko_jatuh_igd(){
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$("#tabel_skrining tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}thistory_tindakan/load_skrining_risiko_jatuh_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					versi_edit:versi_edit,
					
					},
			success: function(data) {
				$("#tabel_skrining tbody").append(data.opsi);
				$("#cover-spin").hide();
			}
		});
	}
	
	function load_diagnosa_igd(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		let versi_edit=$("#versi_edit").val();
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_diagnosa_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							versi_edit:versi_edit,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa_igd();
	}
	function refresh_diagnosa_igd(){
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}thistory_tindakan/refresh_diagnosa_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					versi_edit:versi_edit,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				load_data_rencana_asuhan_igd();
			}
		});
	}
	$("#diagnosa_id_list").on("change", function(){
		load_data_rencana_asuhan_igd($(this).val());
	});
	function load_data_rencana_asuhan_igd(diagnosa_id){
		if (diagnosa_id){
			
		}else{
			diagnosa_id=$("#diagnosa_id_list").val()
		}
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}thistory_tindakan/load_data_rencana_asuhan_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_id,
					versi_edit:versi_edit,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan tbody").append(data);
			}
		});
	}
	function set_rencana_asuhan(id){
		 $("#diagnosa_id_list").val(id).trigger('change');
	}

	$(document).on('click','.data_asuhan',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/update_data_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa_igd();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
</script>