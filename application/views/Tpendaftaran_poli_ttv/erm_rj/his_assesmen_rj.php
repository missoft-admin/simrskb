<style>
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.edited{
		border-color:#d26a5c;
		background-color:#fff2f1;
	}
	.edited2{
		color:red;
		font-weight: bold;
	}
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	
	
</style>
<?if ($menu_kiri=='his_assesmen_rj'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_assesmen_rj' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
						$disabel_input='';
						$disabel_input_ttv='';
						// if ($status_ttv!='1'){
							// $disabel_input_ttv='disabled';
						// }
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_assesmen=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="template_id" value="<?=$template_id?>" >		
						<input type="hidden" id="status_ttv" value="<?=$status_ttv?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_assesmen_rj" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-xs-12" for="st_owned">Tanggal</label>
											<div class="col-xs-12">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="st_owned">Owned By Me</label>
											<div class="col-xs-12">
												<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0">TIDAK</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
											<div class="col-xs-12">
												<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
											<div class="col-xs-12">
												
												<div class="input-group">
                                                    <select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
                                                    </span>
                                                </div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_pencarian_history">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="10%">Action</th>
													<th width="20%">Versi</th>
													<th width="20%">Created</th>
													<th width="20%">Edited</th>
													<th width="25%">Alasan</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
					</div>
					<hr class="push-10-t">
					<div class="row">
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >ANAMNESIS </h5>
							</div>
						</div>
						<input class="form-control"  type="hidden" id="st_anamnesa" name="st_anamnesa" value="<?=$st_anamnesa?>" >				
								
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Keluhan Utama </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" value="1" id="auto_anamnesa" name="anamnesa" onclick="set_auto()" <?=($st_anamnesa=='1'?'checked':'')?>><span></span> Auto Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  value="1" id="allo_anamnesa" name="anamnesa" onclick="set_allo()" <?=($st_anamnesa=='2'?'checked':'')?>><span></span> Allo Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control <?=($nama_anamnesa!=$nama_anamnesa_asal?'edited':'')?> push-5-r"  type="text"  id="nama_anamnesa" name="nama_anamnesa" value="{nama_anamnesa}" placeholder="Tuliskan Nama" >
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control <?=($hubungan_anamnesa!=$hubungan_anamnesa_asal?'edited':'')?>"  type="text" id="hubungan_anamnesa" name="hubungan_anamnesa" value="{hubungan_anamnesa}" placeholder="Hubungan" >
								</div>
								<div class="col-md-12 push-10-t">
									<textarea id="keluhan_utama" class="form-control <?=($keluhan_utama!=$keluhan_utama_asal?'edited':'')?>" name="story" rows="2" style="width:100%"><?=$keluhan_utama?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit" name="st_riwayat_penyakit" value="<?=$st_riwayat_penyakit?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(0)" <?=($st_riwayat_penyakit=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" class="edited" name="opsi_riwayat_penyakit" onclick="set_riwayat(1)" <?=($st_riwayat_penyakit=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									<select id="riwayat_penyakit" name="riwayat_penyakit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach($list_riwayat_penyakit as $r){?>
										<option value="<?=$r->id?>" <?=$r->pilih?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_lainnya" class="form-control <?=($riwayat_penyakit_lainnya!=$riwayat_penyakit_lainnya_asal?'edited':'')?>" name="riwayat_penyakit_lainnya" rows="2" style="width:100%"><?=$riwayat_penyakit_lainnya?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Alergi </label>
								<div class="col-md-12">
									<select id="riwayat_alergi" name="riwayat_alergi" class="form-control  <?=($riwayat_alergi!=$riwayat_alergi_asal?'edited':'')?>" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_alergi=='0'?'selected':'')?>>Tidak Ada Alergi</option>
										<option value="1" <?=($riwayat_alergi=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_alergi=='2'?'selected':'')?>>Ada Alergi</option>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Pengobatan </label>
								<div class="col-md-12">
									<textarea id="riwayat_pengobatan" class="form-control <?=($riwayat_pengobatan!=$riwayat_pengobatan_asal?'edited':'')?>" name="riwayat_pengobatan" rows="1" style="width:100%"><?=$riwayat_pengobatan?></textarea>
								</div>
							</div>
						</div>
						
						<div class="form-group div_alergi_all">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="block">
										<ul class="nav nav-tabs nav-tabs-alt nav-justified" data-toggle="tabs">
											<li class="" id="li_alergi_1">
												<a href="#alergi_1"  onclick="load_alergi()"><i class="fa fa-pencil "></i> Data Alergi</a>
											</li>
											<li class="" id="li_alergi_2">
												<a href=" #alergi_2" onclick="load_alergi_his()"><i class="fa fa-history"></i> History Alergi</a>
											</li>
											
										</ul>
										<div class="block-content tab-content ">
											<div class="tab-pane " id="alergi_1">
												<div class="table-responsive div_data_alergi">
													<table class="table" id="index_alergi">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="tab-pane " id="alergi_2">
												<div class="table-responsive">
													<table class="table" id="index_alergi_his">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							
							</div>
							<div class="col-md-6 ">
									
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Fisik </h5>
								<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Tingkat Kesadaran </label>
									<select tabindex="8" id="tingkat_kesadaran" <?=$disabel_input_ttv?>  class="form-control <?=($tingkat_kesadaran!=$tingkat_kesadaran_asal?'edited':'')?>" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal _ttv <?=($nadi!=$nadi_asal?'edited':'')?>" type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal _ttv <?=($nafas!=$nafas_asal?'edited':'')?>" type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal _ttv <?=($td_sistole!=$td_sistole_asal?'edited':'')?>" <?=$disabel_input_ttv?> type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal _ttv <?=($td_diastole!=$td_diastole_asal?'edited':'')?>" <?=$disabel_input_ttv?> type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal _ttv <?=($suhu!=$suhu_asal?'edited':'')?>" readonly <?=$disabel_input_ttv?> type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Tinggi Badan</label>
									<div class="input-group">
										<input class="form-control decimal _ttv <?=($tinggi_badan!=$tinggi_badan_asal?'edited':'')?>" <?=$disabel_input_ttv?>  type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
										<span class="input-group-addon">Cm</span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Berat Badan</label>
									<div class="input-group">
										<input class="form-control decimal _ttv <?=($berat_badan!=$berat_badan_asal?'edited':'')?>" <?=$disabel_input_ttv?> type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Psikologis, Sosial Ekonomi, Spiritual </h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Hubungan Pasien Dengan Anggota Keluarga</label>
									<select tabindex="8" id="hubungan_anggota_keluarga" name="hubungan_anggota_keluarga" class="<?=($hubungan_anggota_keluarga!=$hubungan_anggota_keluarga_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($hubungan_anggota_keluarga == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(41) as $row){?>
										<option value="<?=$row->id?>" <?=($hubungan_anggota_keluarga == $row->id ? 'selected="selected"' : '')?> <?=($hubungan_anggota_keluarga == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Status Psikologis</label>
									<select tabindex="8" id="st_psikologis" name="st_psikologis" class="<?=($st_psikologis!=$st_psikologis_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_psikologis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(26) as $row){?>
										<option value="<?=$row->id?>" <?=($st_psikologis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_psikologis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Sosial Ekonomi</label>
									<select tabindex="8" id="st_sosial_ekonomi" name="st_sosial_ekonomi" class="<?=($st_sosial_ekonomi!=$st_sosial_ekonomi_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_sosial_ekonomi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(27) as $row){?>
										<option value="<?=$row->id?>" <?=($st_sosial_ekonomi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_sosial_ekonomi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Spiritual</label>
									<select tabindex="8" id="st_spiritual" name="st_spiritual" class="<?=($st_spiritual!=$st_spiritual_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_spiritual == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(28) as $row){?>
										<option value="<?=$row->id?>" <?=($st_spiritual == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_spiritual == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Pendidikan</label>
									<select tabindex="8" id="pendidikan" name="pendidikan" class="js-select2 form-control  select2_edit" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pendidikan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(13) as $row){?>
										<option value="<?=$row->id?>" <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Pekerjaan</label>
									<select tabindex="8" id="pekerjaan" name="pekerjaan" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pekerjaan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(6) as $row){?>
										<option value="<?=$row->id?>" <?=($pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Status Nutrisi</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Keluhan, Perubahan Nafsu Makan</label>
									<select tabindex="8" id="st_nafsu_makan" name="st_nafsu_makan" class="<?=($st_nafsu_makan!=$st_nafsu_makan_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_nafsu_makan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(29) as $row){?>
										<option value="<?=$row->id?>"  <?=($st_nafsu_makan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_nafsu_makan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penurunan Berat Badan, 3 Bln terakhir</label>
									<select tabindex="8" id="st_turun_bb" name="st_turun_bb" class="<?=($st_turun_bb!=$st_turun_bb_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_turun_bb == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(30) as $row){?>
										<option value="<?=$row->id?>" <?=($st_turun_bb == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_turun_bb == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Mual</label>
									<select tabindex="8" id="st_mual" name="st_mual" class="<?=($st_mual!=$st_mual_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_mual == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(31) as $row){?>
										<option value="<?=$row->id?>" <?=($st_mual == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_mual == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Muntah</label>
									<select tabindex="8" id="st_muntah" name="st_muntah" class="<?=($st_muntah!=$st_muntah_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_muntah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(32) as $row){?>
										<option value="<?=$row->id?>" <?=($st_muntah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_muntah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Status Fungsional</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Status Fungsional</label>
									<select tabindex="8" id="st_fungsional" name="st_fungsional" class="<?=($st_fungsional!=$st_fungsional_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_fungsional == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(33) as $row){?>
										<option value="<?=$row->id?>" <?=($st_fungsional == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_fungsional == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penggunaan Alat Bantu</label>
									<select tabindex="8" id="st_alat_bantu" name="st_alat_bantu" class="<?=($st_alat_bantu!=$st_alat_bantu_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_alat_bantu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(34) as $row){?>
										<option value="<?=$row->id?>" <?=($st_alat_bantu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_alat_bantu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Cacat Tubuh</label>
									<select tabindex="8" id="st_cacat" name="st_cacat" class="<?=($st_cacat!=$st_cacat_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_cacat == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(35) as $row){?>
										<option value="<?=$row->id?>" <?=($st_cacat == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_cacat == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Lainnya Sebutkan</label>
									<input class="form-control <?=($cacat_lain!=$cacat_lain_asal?'edited':'')?>" type="text" id="cacat_lain"  value="{cacat_lain}" name="cacat_lain" placeholder="Lainnya" >
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" ><?=$risiko_jatuh_header?></h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="tabel_skrining">
											<thead>
												<tr>
													<th width="80%">Skrining</th>
													<th width="20%">Jawaban</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Skor Pengkajian</label>
									<input class="form-control <?=($skor_pengkajian!=$skor_pengkajian_asal?'edited':'')?> " readonly type="text" id="skor_pengkajian"  value="{skor_pengkajian}" placeholder="Skor Pengkajian" >
									<input class="form-control " type="hidden" id="st_tindakan"  value="" placeholder="" >
								</div>
								<div class="col-md-9 ">
									<label for="example-input-normal">Hasil Pengkajian</label>
									<input class="form-control <?=($risiko_jatuh!=$risiko_jatuh_asal?'edited':'')?>" readonly type="text" id="risiko_jatuh"  value="{risiko_jatuh}" name="risiko_jatuh" placeholder="Hasil Pengkajian" >
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tindakan</label>
									<input class="form-control <?=($nama_tindakan!=$nama_tindakan_asal?'edited':'')?>" readonly type="text" id="nama_tindakan"  value="{nama_tindakan}" placeholder="Tindakan" >
								</div>
								
								<div class="col-md-6 ">
									<label for="example-input-normal">Apakah Tindakan Dilakukan</label>
									<select tabindex="8" id="st_tindakan_action" name="st_tindakan_action" class="<?=($st_tindakan_action!=$st_tindakan_action_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_tindakan_action == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="1" <?=($st_tindakan_action == 1 ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($st_tindakan_action == 0 ? 'selected="selected"' : '')?>>TIDAK</option>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary"><?=$risiko_jatuh_footer?></label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="h5 text-primary" >DIAGNOSA KEPERAWATAN</h5>
							</div>
							<div class="col-md-6 ">
								<label class="h5 text-primary" >RENCANA ASUHAN KEPERAWATAN</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Diagnosa Keperawatan</th>
													<th width="10%">Priority</th>
													<th width="25%">User</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa</label>
									<select tabindex="8" id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
								<div class="col-md-12 push-10-t">
									<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
										<thead></thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Edukasi</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Kesediaan Pasien / Keluarga untuk menerima informasi yang diberikan</label>
									<select tabindex="8" id="st_menerima_info" name="st_menerima_info" class="<?=($st_menerima_info!=$st_menerima_info_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_menerima_info == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(39) as $row){?>
										<option value="<?=$row->id?>" <?=($st_menerima_info == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_menerima_info == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat hambatan dalam edukasi</label>
									<select tabindex="8" id="st_hambatan_edukasi" name="st_hambatan_edukasi" class="<?=($st_hambatan_edukasi!=$st_hambatan_edukasi_asal?'edited':'')?> form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_hambatan_edukasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(40) as $row){?>
										<option value="<?=$row->id?>" <?=($st_hambatan_edukasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_hambatan_edukasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Dibutuhkan Penerjemaah ? (Jika Ya Sebutkan)</label>
									<select tabindex="8" id="st_penerjemaah" name="st_penerjemaah" class="<?=($st_penerjemaah!=$st_penerjemaah_asal?'edited':'')?>  form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_penerjemaah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(38) as $row){?>
										<option value="<?=$row->id?>" <?=($st_penerjemaah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_penerjemaah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penerjemaah Yang Dibutuhkan</label>
									<input class="form-control <?=($penerjemaah!=$penerjemaah_asal?'edited':'')?> " type="text" id="penerjemaah"  value="{penerjemaah}" name="penerjemaah" placeholder="Penerjemaah Yang Dibutuhkan" >
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Kebutuhan Edukasi</label>
									<input class="form-control <?=($edukasi_current!=$edukasi_last?'edited':'')?> " type="text" id="edukasi_current"  value="{edukasi_current}" placeholder="Kebutuhan Edukasi" >
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Catatan lain Edukasi</label>
									<input class="form-control <?=($catatan_edukasi!=$catatan_edukasi_asal?'edited':'')?> " type="text" id="catatan_edukasi"  value="{catatan_edukasi}" name="catatan_edukasi" placeholder="Catatan" >
									
								</div>
								
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		list_index_history_edit();
		set_anamnesa();
		set_riwayat_alergi();
		set_riwayat($("#st_riwayat_penyakit").val())
		load_awal_assesmen=false;
		load_alergi();
		load_alergi_his();
		load_skrining_risiko_jatuh();
		load_diagnosa();
	}
	// load_data_rencana_asuhan(1);
});
function disabel_edit(){
	// if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".his_filter").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
		 $('.edited').css('background', '#fff2f1');
		 // $(".nilai").removeAttr('disabled');
	// }
}
function set_riwayat(st_riwayat_penyakit){
	$("#st_riwayat_penyakit").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit").removeAttr('disabled');
		$("#riwayat_penyakit_lainnya").removeAttr('disabled');
		$("#riwayat_penyakit").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit").val('').trigger('change');
		$("#riwayat_penyakit").removeAttr('required');
		$("#riwayat_penyakit").attr('disabled','disabled');
		$("#riwayat_penyakit_lainnya").attr('disabled','disabled');
		
	}
	generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
// $("#riwayat_penyakit").change(function(){
	// generate_riwayat_penyakit();
// });

//riwayat_penyakit
function generate_riwayat_penyakit(){
	var select_button_text
	let str_penyakit='';
	if ($("#st_riwayat_penyakit").val()=='1'){
		select_button_text = $('#riwayat_penyakit option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
		// selected['Tidak Ada']
	}
	
	$("#riwayat_penyakit_lainnya").val(select_button_text);
	console.log(select_button_text);
}
function set_allo(){
	$("#st_anamnesa").val('2');
	set_anamnesa();
}
function set_auto(){
	$("#st_anamnesa").val('1');
	set_anamnesa();
}
function set_riwayat_alergi(){
	let riwayat=$("#riwayat_alergi").val();
	if (riwayat=='2'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").show();
		$(".div_input_alergi").show();
		$("#li_alergi_1").addClass('active');
		$("#li_alergi_2").removeClass('active');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else if (riwayat=='1'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").hide();
		$(".div_input_alergi").hide();
		// // $('#alergi_2 > a').tab('show');
		// $('#alergi_1 > a').tab('hide');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else{
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
		$(".div_alergi_all").hide();
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	
		
	}
	
}
$("#riwayat_alergi").on("change", function(){
	
	set_riwayat_alergi();
});
function set_anamnesa(){
	if ($("#st_anamnesa").val()=='1'){
		$(".allo_anamnesa").attr('disabled','disabled');
		$(".allo_anamnesa").removeAttr('required');
		$("#nama_anamnesa").val('');
		$("#hubungan_anamnesa").val('');
	}else{
		$(".allo_anamnesa").removeAttr('disabled');
		$(".allo_anamnesa").attr('required');
		
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}

	function clear_input_alergi(){
		$("#alergi_id").val('');
		$("#btn_add_alergi").html('<i class="fa fa-plus"></i> Add');
		$("#input_jenis_alergi").val('');
		$("#input_detail_alergi").val('');
		$("#input_reaksi_alergi").val('');
	}
	function load_alergi(){
		$('#alergi_2 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_alergi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	
	function load_alergi_his(){
		$('#alergi_1 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_alergi_his').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi_his').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_alergi_his', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_alergi_pasien(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_alergi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function load_skrining_risiko_jatuh(){
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$("#tabel_skrining tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Thistory_tindakan/load_skrining_risiko_jatuh', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					versi_edit:versi_edit,
					
					},
			success: function(data) {
				$("#tabel_skrining tbody").append(data.opsi);
				// $(".nilai").select2();
				// get_skor_pengkajian();
				$("#cover-spin").hide();
			}
		});
	}
	
	
	function get_skor_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#template_id").val();
		$.ajax({
			url: '{site_url}Thistory_tindakan/update_nilai_risiko_jatuh', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },
			  success: function(data) {
				 $("#st_tindakan").val(data.st_tindakan);
				 $("#nama_tindakan").val(data.nama_tindakan);
				 $("#skor_pengkajian").val(data.skor);
				 $("#risiko_jatuh").val(data.ref_nilai);
			  }
		});
		
	}
	
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function load_diagnosa(){
		
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}thistory_tindakan/load_diagnosa', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							versi_edit:versi_edit,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa();
	}
	
	function refresh_diagnosa(){
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Thistory_tindakan/refresh_diagnosa', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					versi_edit:versi_edit,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				load_data_rencana_asuhan();
			}
		});
	}
	
	$("#diagnosa_id_list").on("change", function(){
		load_data_rencana_asuhan();
	});
	function load_data_rencana_asuhan(){
		let diagnosa_id=$("#diagnosa_id_list").val();
		let assesmen_id=$("#assesmen_id").val();
		let versi_edit=$("#versi_edit").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Thistory_tindakan/load_data_rencana_asuhan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_id,
					versi_edit:versi_edit,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan tbody").append(data);
			}
		});
	}
	function set_rencana_asuhan(id){
		 $("#diagnosa_id_list").val(id).trigger('change');
	}
	
	$(document).on('click','.data_asuhan',function(){
		 event.preventDefault();
    // alert('Break');
	  
	});
	// $('input[type="checkbox"]').click(function(event) {
    // event.preventDefault();
    // alert('Break');
// });
	function list_index_history_edit(){
		let assesmen_id=$("#assesmen_id").val();
		let filter_ppa_id=$("#filter_ppa_id").val();
		let filter_profesi_id=$("#filter_profesi_id").val();
		let st_owned=$("#st_owned").val();
		let tanggal_1=$("#filter_ttv_tanggal_1").val();
		let tanggal_2=$("#filter_ttv_tanggal_2").val();
		let versi_edit=$("#versi_edit").val();
		$('#index_pencarian_history').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_pencarian_history').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					// ],
				ajax: { 
					url: '{site_url}thistory_tindakan/list_index_template', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							tanggal_1:tanggal_1,
							tanggal_2:tanggal_2,
							filter_ppa_id:filter_ppa_id,
							filter_profesi_id:filter_profesi_id,
							st_owned:st_owned,
							versi_edit:versi_edit,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}thistory_tindakan/list_history_pengkajian', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function set_loading(){
		$("#cover-spin").show();
	}
</script>