<style>
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<style>
body {
  -webkit-print-color-adjust: exact;
}


table {
	font-family: Verdana, sans-serif;
	font-size: 11px !important;
	border-collapse: collapse !important;
	width: 100% !important;
  }
  th {
	padding: 5px;
	 
  }
  td {
	padding: 13px;
	 
  }
  .content td {
	padding: 10px;
	border: 0px solid #6033FF;
  }
  .has-error2 {
	border-color: #d26a5c;
}
  .select2-selection {
	  border-color: green; /* example */
	}
  /* border-normal */
  .border-full {
	border: 1px solid #000 !important;
	
  }
  .text-header{
	font-size: 13px !important;
  }
  .text-judul{
	font-size: 14px  !important;
  }
  .border-bottom {
	border-bottom:1px solid #000 !important;
  }
   .border-bottom-left {
	border-bottom:1px solid #000 !important;
	border-left:1px solid #000 !important;
  }
   .border-bottom-right {
	border-bottom:1px solid #000 !important;
	border-right:1px solid #000 !important;
  }
  .border-bottom-top {
	border-bottom:1px solid #000 !important;
	border-top:1px solid #000 !important;
  }
  .border-bottom-top-left {
	border-bottom:1px solid #000 !important;
	border-top:1px solid #000 !important;
	border-left:1px solid #000 !important;
  }
  .border-bottom-top-right {
	border-bottom:1px solid #000 !important;
	border-top:1px solid #000 !important;
	border-right:1px solid #000 !important;
  }
  .border-left {
	border-left:1px solid #000 !important;
  }

  /* border-thick */
  .border-thick-top{
	border-top:2px solid #000 !important;
  }
  .border-thick-bottom{
	border-bottom:2px solid #000 !important;
  }

  .border-dotted{
	border-bottom:1px solid #000 !important;
	border-bottom-style: dotted;
  }

  /* text-position */
  .text-center{
	text-align: center !important;
  }
  .text-left{
	text-align: left !important;
  }
  .text-right{
	text-align: right !important;
  }

  /* text-style */
  .text-italic{
	font-style: italic;
  }
  .text-bold{
	font-weight: bold;
  }
  .text-top{
	font-size: 14px !important;
  }
  br {
	   display: block;
	   margin: 10px 0;
	}

</style>
<?if ($menu_kiri=='lihat_gc' || $menu_kiri=='his_gc'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='lihat_gc' || $menu_kiri=='his_gc' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($menu_kiri=='lihat_gc'?'GENERAL CONSENT SAAT INI':'HISTORY GENERAL CONSENT')?></a>
				</li>
				<?if ($menu_kiri=='lihat_gc'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> RIWAYAT</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_triage/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
					<div class="row">
						<div class="col-md-12">
							<?if($st_gc=='1' && $st_general=='1'){?>
								
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA GENERAL CONSENT</h4>
							<?}?>
						</div>
						<?if ($menu_kiri=='his_gc'){?>
						<div class="form-group">
							
							<div class="col-md-12 ">
								<div class="pull-right push-10-r">
									
										<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/lihat_gc" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
						<?}?>
					</div>
					<?if($st_gc=='1' && $st_general=='1'){?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 col-xs-12">
								<?if($st_gc=='1'){?>
									<div class="row">
										<div class="col-md-12">
											<table>
												<tr>
													<td width="10%"><img class="img-avatar"  id="output_img" src="{upload_path}app_setting/<?=($logo_gc?$logo_gc:'no_image.png')?>" /></td>
													<td  width="90%" class="font-w700 push-5 text-center"><h3>{judul_gc}</h3></td>
												</tr>
											</table>
											<table class="table table-bordered table-striped"  id="tabel_gc">
												<thead>
													<tr>
														<td width="2%" class="text-center text-bold text-italic text-header"></td>
														<td width="78%" class="text-center text-bold text-italic text-header">{sub_header_gc}</td>
														<td  width="20%" class="text-center text-bold text-italic text-header">{sub_header_side_gc}</td>
													</tr>
												</thead>
												<tbody></tbody>
											</table>
											
											<div class="col-md-6 push-5 push-0-l" <?=($st_general=='1'?'hidden':'')?>>
												<label class="col-md-12" for="validation-classic-city">MENGIJINKAN / TIDAK MENGIJINKAN	</label>
												<div class="col-md-12 push-5">
													<select name="jawaban_perssetujuan" <?=($st_general=='1'?'disabled':'')?> id="jawaban_perssetujuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="MENGIJINKAN / TIDAK MENGIJINKAN">
														<option value="" <?=($jawaban_perssetujuan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
														<option value="MENGIJINKAN" <?=($jawaban_perssetujuan == 'MENGIJINKAN' ? 'selected="selected"' : '')?>>MENGIJINKAN</option>
														<option value="TIDAK MENGIJINKAN" <?=($jawaban_perssetujuan == 'TIDAK MENGIJINKAN' ? 'selected="selected"' : '')?>>TIDAK MENGIJINKAN</option>
													</select>
												</div>
											</div>
											<table>
												<tr>
													<td  width="100%" class="text-left">{footer_1_gc}</td>
												</tr>
												
												<tr>
													<td  width="100%" class="text-left"><?=trim($footer_2_gc)?></td>
												</tr>
											</table>
											<table>
												<tr>
													<td  width="70%" class="text-left">NAMA PASIEN</td>
													<td  width="30%" class="text-left">JENIS KELAMIN</td>
												</tr>
												<tr>
													<td  width="70%" class="text-left">
														<input type="text" disabled class="form-control" id="namapasien" placeholder="Nama Lengkap" name="namapasien" value="{namapasien}" required>
													</td>
													<td  width="30%" class="text-left">
															<select tabindex="7" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
																<option value="">Pilih Opsi</option>
																<?foreach(list_variable_ref(1) as $row){?>
																<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
																<?}?>
															</select>
													</td>
												</tr>
												<tr>
													<td  width="70%" class="text-left">{ttd_1}</td>
													<td  width="30%" class="text-left">HUBUNGAN</td>
												</tr>
												<tr>
													<td  width="70%" class="text-left">
														<input disabled type="text" class="form-control" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}" required>
													</td>
													<td  width="30%" class="text-left">
															<select disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
																<option value="">Pilih Opsi</option>
																<?foreach(list_variable_ref(9) as $row){?>
																<option value="<?=$row->id?>" <?=($hubungan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
																<?}?>
															</select>
													</td>
												</tr>
												
											</table>
											<div class="col-md-6 push-5 push-0-l">
												<label class="col-md-12" for="validation-classic-city">Tanda Tangan</label>
												<div class="col-md-6">
													<?if ($jawaban_ttd){?>
														<img class="" style="width:100;height:80px; text-align: center;" src="<?=$jawaban_ttd?>" alt="" title="">
														
													<?}else{?>
														
													<?}?>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<input class="form-control" readonly name="jawaban_ttd" id="jawaban_ttd" value="{jawaban_ttd}" type="hidden">
										</div>
									</div>
									
								<?}?>


							</div>
						</div>
						
						<!--BATS AKRHI -->
					</div>
					<?}?>
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT GENERAL CONSENT</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<?foreach($list_poli as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="10%">Asal Pasien</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Dokter</th>
												<th width="10%">Kelompok Pasien</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	load_index_gc();
});
function load_index_gc(){
	// $("#cover-spin").show();
	var id=$("#trx_id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_poli_ttv/load_index_gc/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_gc tbody").empty();
			$("#tabel_gc tbody").append(data.tabel);
			$(".js-select2").select2();
			$("#cover-spin").hide();
		}
	});
}
function list_history_pengkajian(){
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar").val();
	let tgl_daftar_2=$("#tgl_daftar_2").val();
	let notransaksi=$("#notransaksi").val();
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	$('#index_history_kajian').DataTable().destroy();	
	// $("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_history_kajian').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
					// {  className: "text-center", targets:[1,2,3,5,6,7] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				// ],
			ajax: { 
				url: '{site_url}tpendaftaran_poli_ttv/list_history_gc', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idpasien:idpasien,
						notransaksi:notransaksi,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						idpoli:idpoli,
						iddokter:iddokter,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
</script>