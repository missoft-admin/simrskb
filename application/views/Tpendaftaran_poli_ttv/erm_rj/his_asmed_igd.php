<style>
.edited_final {
    border-color: #d26a5c;
    background-color: #fff2f1 !important;
}

.edited2 {
    background-color: #ffb9b3;
    font-weight: bold;
}

.edited_final {
    background-color: #ffb9b3;
    font-weight: bold;
}

.select2-container--default .select2-selection--single {
    background-color: #fdffe2;
}

.checkbox-asuhan {
    padding-top: 3px !important;
    margin-top: 0 !important;
    margin-bottom: 0px !important;
    vertical-align: middle !important;

}

<?
if ($versi_edit == '0') {
?>.block>.nav-tabs>li.active>a,
.block>.nav-tabs>li.active>a:hover,
.block>.nav-tabs>li.active>a:focus {
    color: #fff;
    background-color: #57c1d1;
    border-color: transparent;
}

<?
}
if ($versi_edit > 0) {
?>.block>.nav-tabs>li.active>a,
.block>.nav-tabs>li.active>a:hover,
.block>.nav-tabs>li.active>a:focus {
    color: #fff;
    background-color: #d26a5c;
    border-color: transparent;
}

<?
} ?>

</style>
<style>
#sig canvas {
    width: 100% !important;
    height: auto;
}

</style>
<? if ($menu_kiri == 'his_asmed_igd') { ?>
    <div class="col-sm-10 menu_kanan" style="display: <?= ($menu_kiri == 'his_asmed_igd' ? 'block' : 'none') ?>;">
        <!-- Music -->
        <button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
        <?
		if ($assesmen_id) {
			$tanggaldaftar = HumanDateShort($tanggal_input);
			$waktudaftar = HumanTime($tanggal_input);
		} else {
			$waktudaftar = date('H:i:s');
			$tanggaldaftar = date('d-m-Y');
		}
		if ($tanggal_kontrol) {

			$tanggal_kontrol = HumanDateShort($tanggal_kontrol);
		}
		if ($tanggal_kontrol_asal) {

			$tanggal_kontrol_asal = HumanDateShort($tanggal_kontrol_asal);
		}
		$disabel_input = '';
		$disabel_input_ttv = '';
		// if ($status_ttv!='1'){
		// $disabel_input_ttv='disabled';
		// }
		if ($st_input_asmed_igd == '0') {
			$disabel_input = 'disabled';
		}
		$disabel_cetak = '';
		if ($st_cetak_assesmen == '0') {
			$disabel_cetak = 'disabled';
		}


		?>
        <div class="block animated fadeIn push-5-t" data-category="erm_rj">
            <? if ($st_lihat_asmed_igd == '1') { ?>
                <div class="block">
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active">
                            <a href="#tab_1"><i class="si si-paper-clip"></i> <?= ($versi_edit == '0' ? 'ORIGINAL' : 'PERUBAHAN KE-' . $versi_edit) ?></a>
                        </li>

                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane fade fade-left active in" id="tab_1">

                            <?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>

                            <div class="row">

                                <input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}">
                                <input type="hidden" readonly id="assesmen_detail_id" value="">
                                <input type="hidden" readonly id="st_edited" value="{st_edited}">
                                <input type="hidden" id="status_assemen" value="<?= $status_assemen ?>">
                                <input type="hidden" id="template_id" value="<?= $template_id ?>">
                                <input type="hidden" id="trx_id" value="<?= $trx_id ?>">
                                <input type="hidden" id="jml_edit" value="<?= $jml_edit ?>">
                                <input type="hidden" id="tanggal" value="<?= $tanggal ?>">
                                <input type="hidden" id="versi_edit" value="<?= $versi_edit ?>">
                                <div class="col-md-12">
                                    <? if ($st_lihat_assesmen == '1') { ?>
                                        <h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
                                    <? } else { ?>
                                        <h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4 ">

                                    </div>
                                    <div class="col-md-8 ">
                                        <div class="pull-right push-10-r">
                                            <a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?= $pendaftaran_id ?>/erm_rj/input_asmed_igd" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="st_owned">Tanggal</label>
                                                <div class="col-xs-12">
                                                    <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                                        <input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
                                                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                                        <input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 push-5">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="st_owned">Owned By Me</label>
                                                <div class="col-xs-12">
                                                    <select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
                                                        <option value="1">YA</option>
                                                        <option value="0">TIDAK</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 push-5">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="filter_profesi_id">Profesi</label>
                                                <div class="col-xs-12">
                                                    <select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
                                                        <? foreach (list_variable_ref(21) as $r) { ?>
                                                            <option value="<?= $r->id ?>"><?= $r->nama ?></option>
                                                        <? } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 push-5">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
                                                <div class="col-xs-12">

                                                    <div class="input-group">
                                                        <select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
                                                            <? foreach (get_all('mppa', array('staktif' => 1)) as $r) { ?>
                                                                <option value="<?= $r->id ?>"><?= $r->nama ?></option>
                                                            <? } ?>
                                                        </select>
                                                        <span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table" id="index_pencarian_history">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">No</th>
                                                        <th width="10%">Action</th>
                                                        <th width="20%">Versi</th>
                                                        <th width="20%">Created</th>
                                                        <th width="20%">Edited</th>
                                                        <th width="25%">Alasan</th>

                                                    </tr>

                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="col-md-3 col-xs-6">
                                            <div class="form-material input-group date">
                                                <input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
                                                <label for="tanggaldaftar">Tanggal </label>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <div class="form-material input-group">
                                                <input tabindex="3" type="text" <?= $disabel_input ?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
                                                <label for="waktupendaftaran">Waktu</label>
                                                <span class="input-group-addon"><i class="si si-clock"></i></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-material input-group">
                                                <select id="template_assesmen_id" <?= ($status_assemen ? 'disabled' : '') ?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                                    <option value="" <?= ($template_assesmen_id == '' ? 'selected' : '') ?>>-Belum dipilih-</option>
                                                    <? foreach ($list_template_assement as $r) { ?>
                                                        <option value="<?= $r->id ?>" <?= ($template_assesmen_id == $r->id ? 'selected' : '') ?>><?= $r->nama_template ?></option>
                                                    <? } ?>

                                                </select>
                                                <label for="waktupendaftaran">Template Pengkajian </label>
                                                <span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?= ($st_input_assesmen != '1' ? 'disabled' : '') ?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group" style="margin-bottom: 10px;">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <div class="input-group">
                                                        <input class="form-control" disabled type="text" readonly value="<?= $login_nip_ppa ?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) <?= $st_input_assesmen ?></label>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <? if ($assesmen_id != '') { ?>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <label class="h5 text-primary">ANAMNESIS </h5>
                                        </div>
                                    </div>
                                    <input class="form-control" type="hidden" id="st_anamnesa" name="st_anamnesa" value="<?= $st_anamnesa ?>">

                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <label class="col-xs-12">Keluhan Utama </label>
                                            <div class="col-md-3">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" value="1" id="auto_anamnesa" name="anamnesa" onclick="set_auto()" <?= ($st_anamnesa == '1' ? 'checked' : '') ?>><span></span> Auto Anamnesis
                                                </label>

                                            </div>
                                            <div class="col-md-3">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" value="1" id="allo_anamnesa" name="anamnesa" onclick="set_allo()" <?= ($st_anamnesa == '2' ? 'checked' : '') ?>><span></span> Allo Anamnesis
                                                </label>

                                            </div>
                                            <div class="col-md-3">
                                                <input class="allo_anamnesa form-control <?= ($nama_anamnesa != $nama_anamnesa_asal ? 'edited' : '') ?> push-5-r" type="text" id="nama_anamnesa" value="{nama_anamnesa}" placeholder="Tuliskan Nama">
                                            </div>
                                            <div class="col-md-3">
                                                <input class="allo_anamnesa form-control <?= ($hubungan_anamnesa != $hubungan_anamnesa_asal ? 'edited' : '') ?>" type="text" id="hubungan_anamnesa" value="{hubungan_anamnesa}" placeholder="Hubungan">
                                            </div>
                                            <div class="col-md-12 push-10-t">
                                                <textarea id="keluhan_utama" class="form-control auto_blur" name="story" rows="2" style="width:100%"><?= $keluhan_utama ?></textarea>
                                            </div>

                                        </div>
                                        <input class="form-control" type="hidden" id="st_riwayat_penyakit" name="st_riwayat_penyakit" value="<?= $st_riwayat_penyakit ?>">
                                        <div class="col-md-6 ">
                                            <label class="col-xs-12">Riwayat Penyakit </label>
                                            <div class="col-md-3">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(0)" <?= ($st_riwayat_penyakit == '0' ? 'checked' : '') ?>><span></span> Tidak Ada
                                                </label>

                                            </div>
                                            <div class="col-md-2">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(1)" <?= ($st_riwayat_penyakit == '1' ? 'checked' : '') ?>><span></span> Ada
                                                </label>

                                            </div>
                                            <div class="col-md-7">
                                                <select id="riwayat_penyakit" name="riwayat_penyakit" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>

                                                    <? foreach ($list_riwayat_penyakit as $r) { ?>
                                                        <option value="<?= $r->id ?>" <?= $r->pilih ?>><?= $r->nama ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>

                                            <div class="col-md-12 push-10-t">
                                                <textarea id="riwayat_penyakit_lainnya" class="form-control auto_blur <?= ($riwayat_penyakit_lainnya != $riwayat_penyakit_lainnya_asal ? 'edited' : '') ?>" name="riwayat_penyakit_lainnya" rows="2" style="width:100%"><?= $riwayat_penyakit_lainnya ?></textarea>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <input class="form-control" type="hidden" id="st_riwayat_penyakit_dahulu" name="st_riwayat_penyakit_dahulu" value="<?= $st_riwayat_penyakit_dahulu ?>">
                                        <div class="col-md-6 ">
                                            <label class="col-xs-12">Riwayat Penyakit Dahulu</label>
                                            <div class="col-md-3">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" name="opsi_rp_dahulu" onclick="set_riwayat_1(0)" <?= ($st_riwayat_penyakit_dahulu == '0' ? 'checked' : '') ?>><span></span> Tidak Ada
                                                </label>

                                            </div>
                                            <div class="col-md-2">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" name="opsi_rp_dahulu" onclick="set_riwayat_1(1)" <?= ($st_riwayat_penyakit_dahulu == '1' ? 'checked' : '') ?>><span></span> Ada
                                                </label>

                                            </div>
                                            <div class="col-md-7">
                                                <select id="riwayat_penyakit_dahulu_list" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>

                                                    <? foreach ($list_riwayat_penyakit_dahulu as $r) { ?>
                                                        <option value="<?= $r->id ?>" <?= $r->pilih ?>><?= $r->nama ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>

                                            <div class="col-md-12 push-10-t">
                                                <textarea id="riwayat_penyakit_dahulu" class="form-control auto_blur <?= ($riwayat_penyakit_dahulu != $riwayat_penyakit_dahulu_asal ? 'edited' : '') ?>" name="riwayat_penyakit_dahulu" rows="2" style="width:100%"><?= $riwayat_penyakit_dahulu ?></textarea>
                                            </div>

                                        </div>
                                        <input class="form-control" type="hidden" id="st_riwayat_penyakit_keluarga" value="<?= $st_riwayat_penyakit_keluarga ?>">
                                        <div class="col-md-6 ">
                                            <label class="col-xs-12">Riwayat Penyakit Keluarga</label>
                                            <div class="col-md-3">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" name="opsi_rp_keluarga" onclick="set_riwayat_2(0)" <?= ($st_riwayat_penyakit_keluarga == '0' ? 'checked' : '') ?>><span></span> Tidak Ada
                                                </label>

                                            </div>
                                            <div class="col-md-2">
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" name="opsi_rp_keluarga" onclick="set_riwayat_2(1)" <?= ($st_riwayat_penyakit_keluarga == '1' ? 'checked' : '') ?>><span></span> Ada
                                                </label>

                                            </div>
                                            <div class="col-md-7">
                                                <select id="riwayat_penyakit_keluarga_list" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>

                                                    <? foreach ($list_riwayat_penyakit_keluarga as $r) { ?>
                                                        <option value="<?= $r->id ?>" <?= $r->pilih ?>><?= $r->nama ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>

                                            <div class="col-md-12 push-10-t">
                                                <textarea id="riwayat_penyakit_keluarga" class="form-control auto_blur <?= ($riwayat_penyakit_keluarga != $riwayat_penyakit_keluarga_asal ? 'edited' : '') ?>" rows="2" style="width:100%"><?= $riwayat_penyakit_keluarga ?></textarea>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <label class="text-primary">PEMERIKSAAN FISIK</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">

                                            <div class="col-md-12 ">
                                                <div class="table-responsive">
                                                    <table class="table table-striped" id="tabel_anatomi">
                                                        <thead>
                                                            <tr>
                                                                <th width="5%">No</th>
                                                                <th width="25%">Anatomi Tubuh</th>
                                                                <th width="40%">Deskripsi Tidak Normal</th>
                                                                <th width="15%">User</th>
                                                                <th width="15%">Perubahan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <label class=" text-primary">STATUS LOKALIS</label>
                                        </div>
                                        <div class="col-md-6 ">
                                            <label class="text-primary">KETERANGAN</label>
                                        </div>
                                    </div>
                                    <input class="form-control " readonly type="hidden" id="src_gambar" value="{upload_path}anatomi/{gambar_tubuh}" placeholder="">
                                    <input class="form-control " readonly type="hidden" id="base_url" value="{upload_path}anatomi/" placeholder="">

                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <div class="col-md-12">
                                                <canvas id="canvas" width="680" height="500"></canvas>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Template</label>
                                                <?
												$q = "
											SELECT *FROM(
											SELECT D.jml_edit as versi_edit,H.* FROM tpoliklinik_asmed_igd_lokalis_gambar H
											LEFT JOIN tpoliklinik_asmed_igd D ON D.assesmen_id=H.assesmen_id
											WHERE H.assesmen_id='$assesmen_id' AND H.gambar_final IS NOT NULL
											UNION ALL
											SELECT H.* FROM tpoliklinik_asmed_igd_his_lokalis_gambar H
											WHERE H.assesmen_id='$assesmen_id' AND H.gambar_final IS NOT NULL
											) T WHERE T.versi_edit='$versi_edit'
										";
												$list_gambar = $this->db->query($q)->result();
												?>
                                                <select id="gambar_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                                    <? foreach ($list_gambar as $r) { ?>
                                                        <option value="<?= $r->id ?>" <?= ($r->st_default == '1' ? 'selected' : '') ?>><?= $r->nama_template_lokasi ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Jenis Drawing</label>
                                                <select id="jenis_drawing" class="js-select2 form-control input-default" style="width: 100%;" data-placeholder="Choose one..">
                                                    <option value="free_draw" <?= ($jenis_drawing == 'free_draw' ? 'selected' : '') ?>>Free Draw</option>
                                                    <option value="segitiga" <?= ($jenis_drawing == 'segitiga' ? 'selected' : '') ?>>Triangle</option>
                                                    <option value="kotak" <?= ($jenis_drawing == 'kotak' ? 'selected' : '') ?>>Box</option>
                                                    <option value="bulat" <?= ($jenis_drawing == 'bulat' ? 'selected' : '') ?>>Round</option>
                                                    <option value="silang" <?= ($jenis_drawing == 'silang' ? 'selected' : '') ?>>Cross</option>
                                                    <option value="diamond" <?= ($jenis_drawing == 'diamond' ? 'selected' : '') ?>>Diamond</option>
                                                    <option value="bintang" <?= ($jenis_drawing == 'bintang' ? 'selected' : '') ?>>Star</option>
                                                    <option value="target" <?= ($jenis_drawing == 'target' ? 'selected' : '') ?>>Crosshairs</option>
                                                    <option value="hati" <?= ($jenis_drawing == 'hati' ? 'selected' : '') ?>>Heart</option>
                                                    <option value="bendera" <?= ($jenis_drawing == 'bendera' ? 'selected' : '') ?>>Flag</option>
                                                    <option value="tempe" <?= ($jenis_drawing == 'tempe' ? 'selected' : '') ?>>Bookmark</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Warna</label>
                                                <div class="js-colorpicker input-group colorpicker-element">
                                                    <input class="form-control input-default" readonly type="text" id="warna" name="warna" value="<?= $warna ?>">
                                                    <span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table" id="simbol-list">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:5%">No.</th>
                                                                <th style="width:10%">Simbol</th>
                                                                <th style="width:75%">Keterangan</th>
                                                                <th style="width:10%">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Keterangan</label>
                                                <input class="form-control auto_blur <?= ($txt_keterangan != $txt_keterangan_asal ? 'edited' : '') ?>" type="text" id="txt_keterangan" value="{txt_keterangan}" placeholder="Keterangan">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <label class="h5 text-primary"></h5>

                                        </div>
                                        <div class="col-md-6 ">
                                            <label class="h5 text-primary"></h5>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <div class="col-md-12 ">
                                                <label for="example-input-normal">Pemeriksaan Penunjang</label>
                                                <textarea id="pemeriksaan_penunjang" class="form-control auto_blur <?= ($pemeriksaan_penunjang != $pemeriksaan_penunjang_asal ? 'edited' : '') ?>" name="pemeriksaan_penunjang" rows="2" style="width:100%"><?= $pemeriksaan_penunjang ?></textarea>
                                            </div>

                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="col-md-12 ">
                                                <label for="example-input-normal">Intruksi</label>
                                                <textarea id="intruksi" class="form-control auto_blur <?= ($intruksi != $intruksi_asal ? 'edited' : '') ?>" name="intruksi" rows="2" style="width:100%"><?= $intruksi ?></textarea>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <div class="col-md-12 ">
                                                <label for="example-input-normal">Rencana Asuhan</label>
                                                <textarea id="rencana_asuhan" class="form-control auto_blur <?= ($rencana_asuhan != $rencana_asuhan_asal ? 'edited' : '') ?>" rows="2" style="width:100%"><?= $rencana_asuhan ?></textarea>
                                            </div>

                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="col-md-12 ">
                                                <label for="example-input-normal">Diagnosa Utama</label>
                                                <input class="form-control auto_blur <?= ($diagnosa_utama != $diagnosa_utama_asal ? 'edited' : '') ?>" type="text" id="diagnosa_utama" value="{diagnosa_utama}" placeholder="Diagnosa Utama">

                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <div class="col-md-12 ">
                                                <div class="table-responsive">
                                                    <table class="table" id="tabel_diagnosa">
                                                        <thead>
                                                            <tr>
                                                                <th width="5%">No</th>
                                                                <th width="45%">Diagnosa Tambahan</th>
                                                                <th width="15%">Priority</th>
                                                                <th width="15%">User</th>
                                                                <th width="10%">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <div class="col-md-12 ">
                                                <label>Keterangan</label>
                                                <input class="form-control auto_blur <?= ($txt_keterangan_diagnosa != $txt_keterangan_diagnosa_asal ? 'edited' : '') ?>" type="text" id="txt_keterangan_diagnosa" value="{txt_keterangan_diagnosa}" placeholder="Keterangan">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <label class="h5 text-primary">Edukasi</h5>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <div class="col-md-12">
                                                <label for="example-input-normal">Kebutuhan Edukasi</label>
                                                <select tabindex="8" id="medukasi_id" name="medukasi_id[]" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                                    <? foreach ($list_edukasi as $row) { ?>
                                                        <option value="<?= $row->id ?>" <?= $row->pilih ?>><?= $row->judul ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="col-md-12 ">
                                                <label for="example-input-normal">Catatan lain Edukasi</label>
                                                <input class="form-control auto_blur <?= ($catatan_edukasi != $catatan_edukasi_asal ? 'edited' : '') ?>" type="text" id="catatan_edukasi" value="{catatan_edukasi}" name="catatan_edukasi" placeholder="Catatan">

                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <label class="h5 text-primary">Ringkasan Kondisi Keluar</h5>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                <label for="example-input-normal">Keadaan Umum</label>
                                                <select tabindex="8" id="keadaan_umum" name="keadaan_umum" class="<?= ($keadaan_umum != $keadaan_umum_asal ? 'edited' : '') ?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
                                                    <? foreach (list_variable_ref(61) as $row) { ?>
                                                        <option value="<?= $row->id ?>" <?= ($keadaan_umum == $row->id ? 'selected="selected"' : '') ?> <?= ($keadaan_umum == '0' && $row->st_default == '1' ? 'selected="selected"' : '') ?>><?= $row->nama ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="example-input-normal">Tingkat Kesadaran</label>
                                                <select tabindex="8" id="tingkat_kesadaran" name="tingkat_kesadaran" class="<?= ($tingkat_kesadaran != $tingkat_kesadaran_asal ? 'edited' : '') ?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
                                                    <option value="">Pilih Opsi</option>
                                                    <? foreach (list_variable_ref(23) as $row) { ?>
                                                        <option value="<?= $row->id ?>" <?= ($tingkat_kesadaran == $row->id ? 'selected="selected"' : '') ?>><?= $row->nama ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="col-md-6 ">
                                                <label for="example-input-normal">Tekanan Darah</label>
                                                <div class="input-group">
                                                    <input class="form-control decimal <?= ($td_sistole != $td_sistole_asal ? 'edited' : '') ?> " type="text" id="td_sistole" value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
                                                    <span class="input-group-addon"><?= $satuan_td ?> / </span>
                                                    <input class="form-control decimal <?= ($td_diastole != $td_diastole_asal ? 'edited' : '') ?>" type="text" id="td_diastole" value="{td_diastole}" name="td_diastole" placeholder="Diastole" required>
                                                    <span class="input-group-addon"><?= $satuan_td ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="example-input-normal">Pernafasan</label>
                                                <div class="input-group">
                                                    <input class="form-control decimal <?= ($nafas != $nafas_asal ? 'edited' : '') ?>" type="text" id="nafas" name="nafas" value="{nafas}" placeholder="Frekuensi Nafas" required>
                                                    <span class="input-group-addon"><?= $satuan_nafas ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="example-input-normal">GDS</label>
                                                <div class="input-group">
                                                    <input class="form-control decimal <?= ($gds != $gds_asal ? 'edited' : '') ?>" type="text" id="gds" name="gds" value="{gds}" placeholder="GDS" required>
                                                    <span class="input-group-addon"><?= $satuan_gds ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <label class="h5 text-primary">Status Pulang</h5>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                <label for="example-input-normal">Status Pulang</label>
                                                <select tabindex="8" id="st_pulang" name="st_pulang" class="<?= ($st_pulang != $st_pulang_asal ? 'edited' : '') ?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
                                                    <? foreach (list_variable_ref(62) as $row) { ?>
                                                        <option value="<?= $row->id ?>" <?= ($st_pulang == $row->id ? 'selected="selected"' : '') ?> <?= ($st_pulang == '0' && $row->st_default == '1' ? 'selected="selected"' : '') ?>><?= $row->nama ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="example-input-normal">Pasien Keluar IGD (Tanggal)</label>
                                                <div class="input-group date">
                                                    <input tabindex="2" type="text" class="js-datepicker form-control <?= ($tanggal_keluar != $tanggal_keluar_asal ? 'edited' : '') ?>" data-date-format="dd/mm/yyyy" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="<?= $tanggal_keluar ?>" required>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                <label for="example-input-normal">Rencana Kontrol</label>
                                                <select tabindex="8" id="rencana_kontrol" name="rencana_kontrol" class="<?= ($rencana_kontrol != $rencana_kontrol_asal ? 'edited' : '') ?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
                                                    <option value="" <?= ($rencana_kontrol == '' ? 'selected="selected"' : '') ?>>Pilih Opsi</option>
                                                    <? foreach (list_variable_ref(43) as $row) { ?>
                                                        <option value="<?= $row->id ?>" <?= ($rencana_kontrol == '0' && $row->st_default == '1' ? 'selected="selected"' : '') ?> <?= ($rencana_kontrol == $row->id ? 'selected="selected"' : '') ?>><?= $row->nama ?></option>
                                                    <? } ?>

                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="example-input-normal">Tanggal Kontrol</label>
                                                <div class="input-group date">
                                                    <input tabindex="2" type="text" class="js-datepicker form-control <?= ($tanggal_kontrol != $tanggal_kontrol_asal ? 'edited' : '') ?>" data-date-format="dd/mm/yyyy" id="tanggal_kontrol" placeholder="dd-mm-yyyy" value="<?= $tanggal_kontrol ?>" required>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <?
										if ($idpoli_kontrol == '') {
											$idpoli_kontrol = $idpoliklinik;
										}
										if ($iddokter_kontrol == '0' || $iddokter_kontrol == '') {
											$iddokter_kontrol = $iddokter;
										}
										?>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <label for="example-input-normal">Poliklinik </label>
                                                <select id="idpoli_kontrol" name="idpoli_kontrol" class="<?= ($idpoli_kontrol != $idpoli_kontrol_asal ? 'edited' : '') ?> form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
                                                    <option value="0" <?= ($idpoli_kontrol == '' ? 'selected' : '') ?>>- Tidak Ada -</option>
                                                    <? foreach ($list_poli as $r) { ?>
                                                        <option value="<?= $r->id ?>" <?= ($idpoli_kontrol == $r->id ? 'selected' : '') ?>><?= $r->nama ?></option>
                                                    <? } ?>

                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12 ">
                                                <label for="example-input-normal">Dokter </label>
                                                <select id="iddokter_kontrol" name="iddokter_kontrol" class="<?= ($iddokter_kontrol != $iddokter_kontrol_asal ? 'edited' : '') ?>  form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
                                                    <option value="0" <?= ($iddokter_kontrol == '' ? 'selected' : '') ?>>- Tidak Ada -</option>
                                                    <? foreach ($list_dokter as $r) { ?>
                                                        <option value="<?= $r->id ?>" <?= ($iddokter_kontrol == $r->id ? 'selected' : '') ?>><?= $r->nama ?></option>
                                                    <? } ?>


                                                </select>

                                            </div>

                                        </div>
                                    </div>
                                <? } ?>
                                <!--BATS AKRHI -->
                            </div>
                            <hr class="push-5-b">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12 ">
                                        <label class="text-primary"><?= $judul_footer ?></h5>
                                    </div>
                                </div>
                            </div>

                            <?php echo form_close() ?>

                        </div>

                    </div>
                </div>
            <? } ?>
        </div>
        <!-- END Music -->
    </div>
<? } ?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen = $("#status_assemen").val();
var load_awal_assesmen = true;
var nama_template;
var idpasien = $("#idpasien").val();
var before_edit;
var sig = $('#sig').signature({
    syncField: '#signature64',
    syncFormat: 'PNG'
});
const font = '400 30px "Font Awesome 6 Free"';
const font_2 = '900 40px "Font Awesome 6 Free"';
const font_3 = '900 30px "Font Awesome 6 Free"';
const marker_type = [{
        key: 'bulat',
        content: '\uf10c',
        class_name: 'fa-circle',
        font: font
    },
    {
        key: 'segitiga',
        content: '\uf0d8',
        class_name: 'fa-caret-up',
        font: font_2
    },
    {
        key: 'kotak',
        content: '\uf096',
        class_name: 'fa-square',
        font: font
    },
    {
        key: 'silang',
        content: '\uf00d',
        class_name: 'fa-times',
        font: font_3
    },
    {
        key: 'diamond',
        content: '\uf219',
        class_name: 'fa-diamond',
        font: font_3
    },
    {
        key: 'tempe',
        content: '\uf097',
        class_name: 'fa-bookmark',
        font: font
    },
    {
        key: 'bintang',
        content: '\uf006',
        class_name: 'fa-star',
        font: font
    },
    {
        key: 'target',
        content: '\uf05b',
        class_name: 'fa-crosshairs',
        font: font_3
    },
    {
        key: 'hati',
        content: '\uf08a',
        class_name: 'fa-heart',
        font: font
    },
    {
        key: 'bendera',
        content: '\uf11d',
        class_name: 'fa-flag',
        font: font
    },
    {
        key: 'free_draw',
        content: '',
        class_name: 'fa-pencil',
        font: font_3
    },
];

var selected_marker = null;
var canvas = document.getElementById('canvas');
var context = canvas.getContext("2d");
context.lineWidth = 2;
var st_drawing = false;
var path = [];
// const change_marker = document.querySelectorAll('.change-marker');
// change_marker.forEach(el => el.addEventListener('click', event => {
// selected_marker = marker_type.find(o => o.key === el.id);

// }));
// alert('sini');
$("#jenis_drawing").on("change", function() {
    set_marker();
});


function set_marker() {
    // $("#jenis_drawing").val();
    selected_marker = marker_type.find(({
        key
    }) => key === $("#jenis_drawing").val());
    // console.log(Markers);
    // selected_marker = marker_type.find(o => o.key === el.id);
}

document.fonts.load(font).then((_) => {

    // Map sprite
    var mapSprite = new Image();
    // mapSprite.src   = "human_body.jpeg";
    mapSprite.src = $("#src_gambar").val();

    // selected_marker=$("#jenis_drawing").val();
    set_marker();
    // console.log(selected_marker);

    var Marker = function() {
        this.Width = 20;
        this.Height = 20;
        this.XPos = 0;
        this.YPos = 0;
    }

    var Markers = new Array();
    var str_key = '';
    var str_path = '';
    var selected_marker2 = null;
    <? foreach ($list_lokalis as $r) { ?>

        str_path = '<?= $r->path ?>';
        str_key = '<?= $r->key ?>';
        selected_marker2 = marker_type.find(({
            key
        }) => key === str_key);
        var marker = new Marker();
        marker.XPos = '<?= $r->XPos ?>';
        marker.YPos = '<?= $r->YPos ?>';
        marker.key = '<?= $r->key ?>';
        marker.content = selected_marker2.content;
        marker.warna = '<?= $r->warna ?>';
        if (str_path) {
            marker.path = JSON.parse("[" + str_path + "]");
        }
        // marker.path    = <?= ($r->path) ?>;

        marker.keterangan = '<?= $r->keterangan ?>';
        marker.class_name = '<?= $r->class_name ?>';
        marker.font = selected_marker2.font;
        marker.path = <?= ($r->path) ? "JSON.parse('" . $r->path . "')" : $r->path ?>;

        Markers.push(marker);
        load_data();
        console.log(Markers);
    <? } ?>
    var mouseClicked = function(mouse) {
        if (status_assemen != '2') {
            if (selected_marker != null) {
                var rect = canvas.getBoundingClientRect();
                if (selected_marker.key == 'free_draw') {
                    st_drawing = true;
                    context.beginPath();
                    context.moveTo(mouse.x - rect.left, mouse.y - rect.top);

                } else {
                    // Get corrent mouse coords

                    var mouseXPos = (mouse.x - rect.left) + 10;
                    var mouseYPos = (mouse.y - rect.top) + 30;


                    // Move the marker when placed to a better location
                    const warna = document.getElementById('warna');
                    var marker = new Marker();
                    marker.XPos = mouseXPos - (marker.Width / 2);
                    marker.YPos = mouseYPos - marker.Height;
                    marker.key = selected_marker.key;
                    marker.content = selected_marker.content;
                    marker.warna = warna.value;
                    marker.keterangan = '';
                    marker.class_name = selected_marker.class_name;
                    marker.font = selected_marker.font;
                    marker.path = '';


                    Markers.push(marker);
                }
            }
        }
    }

    // Add mouse click event listener to canvas
    canvas.addEventListener("mousedown", mouseClicked, false);

    canvas.onmousemove = draw_line;
    canvas.onmouseup = stopDrawing;

    var firstLoad = function() {
        context.font = "15px Georgia";
        context.textAlign = "center";
    }

    firstLoad();

    var main = function() {
        draw();
    };

    //\uf10c bulat
    //\uf0d8 segitiga
    //\uf096 kotak
    //\uf00d silang
    //\uf219 diamond

    var draw = function() {
        if (st_drawing == false) {

            // Clear Canvas
            context.fillStyle = "#000";
            context.fillRect(0, 0, canvas.width, canvas.height);

            // Draw map
            // Sprite, X location, Y location, Image width, Image height
            // You can leave the image height and width off, if you do it will draw the image at default size

            // console.log(Markers.length);
            context.drawImage(mapSprite, 0, 0, 680, 500);

            // Draw markers
            for (var i = 0; i < Markers.length; i++) {
                // note that I wasn't able to find \uF006 defined in the provided CSS
                // falling back to fa-bug for demo

                var tempMarker = Markers[i];
                // console.log(tempMarker);
                context.font = tempMarker.font;
                context.fillStyle = tempMarker.warna;
                // Draw marker
                //context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);
                let markerText = i + 1;
                let textMeasurements = context.measureText(markerText);


                if (tempMarker.key != 'free_draw') {
                    context.fillText(tempMarker.content, tempMarker.XPos, tempMarker.YPos);
                    // Calculate postion text

                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 50, textMeasurements.width, 20);

                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, tempMarker.XPos, tempMarker.YPos - 35);
                } else {
                    context.beginPath();
                    const begin_path = tempMarker.path[0];
                    context.moveTo(begin_path.x, begin_path.y);
                    context.strokeStyle = tempMarker.warna;
                    tempMarker.path.map((p) => {
                        context.lineTo(p.x, p.y);
                        context.stroke();
                    })

                    context.font = "15px Georgia";
                    context.fillStyle = "#666";
                    context.globalAlpha = 0.7;
                    context.fillRect(begin_path.x - (textMeasurements.width / 2), begin_path.y - 25, textMeasurements.width, 20);

                    context.globalAlpha = 1;

                    // Draw position above
                    context.fillStyle = "#fff";
                    context.fillText(markerText, begin_path.x, begin_path.y - 10);

                }
            }
        }

    };

    function draw_line(e) {
        if (st_drawing == true && selected_marker.key == 'free_draw') {
            const rect = canvas.getBoundingClientRect();
            const x = e.x - rect.left;
            const y = e.y - rect.top;
            path.push({
                x: x,
                y: y
            });
            const warna = document.getElementById('warna');
            context.strokeStyle = warna.value;
            console.log(context);
            console.log(x + ' : ' + y);
            context.lineTo(x, y);
            context.stroke();
        }
    }

    function stopDrawing() {
        if (selected_marker != null && selected_marker.key == 'free_draw') {
            if (path.length > 0) {
                const warna = document.getElementById('warna');
                const marker = new Marker();
                marker.key = selected_marker.key;
                marker.content = '';
                marker.path = path;
                marker.warna = warna.value;
                marker.keterangan = '';
                marker.class_name = selected_marker.class_name;
                Markers.push(marker);
            }
            st_drawing = false;
            path = [];
        }
        // console.log('STIP');
        load_data();
    }
    $(document).on("blur", ".input-keterangan", function() {
        let index = $(this).attr("data-id");
        Markers[index].keterangan = $(this).val();
        // $(this).closest('tr').find(".isiField").

    });
    // $(".input-keterangan").blur(function(){
    // });

    function load_data() {
        const table = document.getElementById('simbol-list');
        let table_content = '';
        table.querySelector('tbody').innerHTML = '';
        for (var i = 0; i < Markers.length; i++) {
            const row = Markers[i];
            table_content += '<tr>';
            table_content += '<td>' + (i + 1) + '</td>';
            table_content += '<td class="text-center"><i class="fa ' + row.class_name + '"></i></td>';
            table_content += '<td><input data-id="' + i + '" class="input-keterangan form-control input-sm"  type="text" value="' + row.keterangan + '"/></td>';
            table_content += '<td><button class="hapus-data btn btn-sm btn-danger" data-id="' + i + '"><i class="fa fa-trash"></i></button></td>';
            table_content += '</tr>';
        }
        table.querySelector('tbody').innerHTML = table_content;
        // console.log(Markers);\
        disabel_edit();
        const hapus_data = document.querySelectorAll('.hapus-data');
        hapus_data.forEach(el => el.addEventListener('click', event => {
            Markers.splice(el.dataset.id, 1);
            load_data();
        }));
    }

    setInterval(main, (1000 / 60)); // Refresh 60 times a second
    $("#gambar_id").on("change", function() {
        load_gambar();
    });

    function load_gambar() {
        let assesmen_id = $("#assesmen_id").val();
        let gambar_id = $("#gambar_id").val();
        let status_assemen = $("#status_assemen").val();
        let versi_edit = $("#versi_edit").val();
        let base_url = $("#base_url").val();

        var canvas = document.getElementById('canvas');
        var context = canvas.getContext("2d");
        $("#cover-spin").show();
        $.ajax({
            url: '{site_url}Thistory_tindakan/load_gambar_asmed_igd',
            dataType: "JSON",
            method: "POST",
            data: {
                assesmen_id: assesmen_id,
                gambar_id: gambar_id,
                status_assemen: status_assemen,
                versi_edit: versi_edit,

            },
            success: function(data) {
                $("#src_gambar").val(base_url + data.gambar_tubuh);

                st_drawing = false;
                // Map sprite
                mapSprite = new Image();
                // mapSprite.src   = "human_body.jpeg";
                mapSprite.src = $("#src_gambar").val();
                Markers = new Array();
                var str_key = '';
                var selected_marker2 = null;

                $.each(data.list_lokalis, function(i, r) {
                    str_key = r.key;
                    selected_marker2 = marker_type.find(({
                        key
                    }) => key === str_key);
                    var marker = new Marker();
                    marker.XPos = r.XPos;
                    marker.YPos = r.YPos;
                    marker.key = r.key;
                    marker.content = selected_marker2.content;
                    marker.warna = r.warna;
                    marker.keterangan = r.keterangan;
                    marker.class_name = r.class_name;
                    marker.font = selected_marker.font;
                    marker.path = JSON.parse(r.path);

                    Markers.push(marker);
                    // console.log(Markers);
                    // $('#nojabatan')
                    // .append('<option value="' + pegawai_item.nojabatan + '">' + pegawai_item.namajabatan + '</option>');
                });
                const table = document.getElementById('simbol-list');
                let table_content = '';
                table.querySelector('tbody').innerHTML = '';
                table.querySelector('tbody').innerHTML = table_content;
                load_data();
                $("#cover-spin").hide();
                draw();
                // alert('sini');
            }
        });
    }
})

function list_index_history_edit() {
    let assesmen_id = $("#assesmen_id").val();
    let filter_ppa_id = $("#filter_ppa_id").val();
    let filter_profesi_id = $("#filter_profesi_id").val();
    let st_owned = $("#st_owned").val();
    let tanggal_1 = $("#filter_ttv_tanggal_1").val();
    let tanggal_2 = $("#filter_ttv_tanggal_2").val();
    let versi_edit = $("#versi_edit").val();
    $('#index_pencarian_history').DataTable().destroy();
    $("#cover-spin").show();
    // alert(ruangan_id);
    table = $('#index_pencarian_history').DataTable({
        autoWidth: false,
        searching: true,
        serverSide: true,
        "processing": false,
        "order": [],
        "pageLength": 10,
        "ordering": false,
        // columnDefs: [
        // {  className: "text-right", targets:[0] },
        // { "width": "5%", "targets": [0] },
        // { "width": "20%", "targets": [1] },
        // { "width": "15%", "targets": [3] },
        // { "width": "60%", "targets": [2] }
        // ],
        ajax: {
            url: '{site_url}thistory_tindakan/list_index_his_asmed_igd',
            type: "POST",
            dataType: 'json',
            data: {
                assesmen_id: assesmen_id,
                tanggal_1: tanggal_1,
                tanggal_2: tanggal_2,
                filter_ppa_id: filter_ppa_id,
                filter_profesi_id: filter_profesi_id,
                st_owned: st_owned,
                versi_edit: versi_edit,

            }
        },
        "drawCallback": function(settings) {
            $("#cover-spin").hide();
        }
    });

}

$(document).ready(function() {

    disabel_edit();
    // set_ttd_assesmen();
    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });
    $("#tabel_rencana_asuhan tbody").empty();
    let assesmen_id = $("#assesmen_id").val();
    if (assesmen_id) {
        set_anamnesa();
        set_riwayat($("#st_riwayat_penyakit").val())
        set_riwayat_1($("#st_riwayat_penyakit_dahulu").val())
        set_riwayat_2($("#st_riwayat_penyakit_keluarga").val())
        load_anatomi();
        load_diagnosa();


        load_awal_assesmen = false;
    }
    list_index_history_edit();
});

function disabel_edit() {
    if (status_assemen == '2') {
        $("#form1 :input").prop("disabled", true);
        $("#gambar_id").removeAttr('disabled');

        $(".his_filter").removeAttr('disabled');
        $(".edited").removeClass("edited").addClass("edited_final");
        // $("#form1 :input").prop("disabled", true);
    }
}

function set_riwayat(st_riwayat_penyakit) {
    $("#st_riwayat_penyakit").val(st_riwayat_penyakit);
    if (st_riwayat_penyakit == '1') {
        $("#riwayat_penyakit").removeAttr('disabled');
        $("#riwayat_penyakit_lainnya").removeAttr('disabled');
        $("#riwayat_penyakit").attr('required');
    } else {
        // $("#riwayat_penyakit").empty();
        // $("#riwayat_penyakit option:selected").remove();
        // $("#riwayat_penyakit > option").attr("selected",false);
        $("#riwayat_penyakit").val('').trigger('change');
        $("#riwayat_penyakit").removeAttr('required');
        $("#riwayat_penyakit").attr('disabled', 'disabled');
        $("#riwayat_penyakit_lainnya").attr('disabled', 'disabled');

    }
    // generate_riwayat_penyakit();
    disabel_edit();
    if (load_awal_assesmen == false) {
        if ($("#st_edited").val() == '0') {
            simpan_assesmen();
        }

    }
}

function set_riwayat_1(st_riwayat_penyakit) {
    $("#st_riwayat_penyakit_dahulu").val(st_riwayat_penyakit);
    if (st_riwayat_penyakit == '1') {
        $("#riwayat_penyakit_dahulu").removeAttr('disabled');
        $("#riwayat_penyakit_dahulu_list").removeAttr('disabled');

        $("#riwayat_penyakit_dahulu").attr('required');
    } else {
        $("#riwayat_penyakit_dahulu_list").val('').trigger('change');
        $("#riwayat_penyakit_dahulu").removeAttr('required');
        $("#riwayat_penyakit_dahulu_list").attr('disabled', 'disabled');
        $("#riwayat_penyakit_dahulu").attr('disabled', 'disabled');

    }
    // generate_riwayat_penyakit();
    disabel_edit();
    if (load_awal_assesmen == false) {
        if ($("#st_edited").val() == '0') {
            simpan_assesmen();
        }

    }
}

function set_riwayat_2(st_riwayat_penyakit) {
    $("#st_riwayat_penyakit_keluarga").val(st_riwayat_penyakit);
    if (st_riwayat_penyakit == '1') {
        $("#riwayat_penyakit_keluarga").removeAttr('disabled');
        $("#riwayat_penyakit_keluarga_list").removeAttr('disabled');

        $("#riwayat_penyakit_dahulu").attr('required');
    } else {
        // $("#riwayat_penyakit").empty();
        // $("#riwayat_penyakit option:selected").remove();
        // $("#riwayat_penyakit > option").attr("selected",false);
        $("#riwayat_penyakit_keluarga_list").val('').trigger('change');
        $("#riwayat_penyakit_keluarga").removeAttr('required');
        $("#riwayat_penyakit_keluarga").attr('disabled', 'disabled');
        $("#riwayat_penyakit_keluarga_list").attr('disabled', 'disabled');

    }
    // generate_riwayat_penyakit();
    disabel_edit();
    if (load_awal_assesmen == false) {
        if ($("#st_edited").val() == '0') {
            simpan_assesmen();
        }

    }
}
// $("#riwayat_penyakit").change(function(){
// generate_riwayat_penyakit();
// });

function set_allo() {
    $("#st_anamnesa").val('2');
    set_anamnesa();
}

function set_auto() {
    $("#st_anamnesa").val('1');
    set_anamnesa();
}

function set_anamnesa() {
    if ($("#st_anamnesa").val() == '1') {
        $(".allo_anamnesa").attr('disabled', 'disabled');
        $(".allo_anamnesa").removeAttr('required');
        $("#nama_anamnesa").val('');
        $("#hubungan_anamnesa").val('');
    } else {
        $(".allo_anamnesa").removeAttr('disabled');
        $(".allo_anamnesa").attr('required');

    }

}


function load_anatomi() {

    let assesmen_id = $("#assesmen_id").val();
    let versi_edit = $("#versi_edit").val();
    $('#tabel_anatomi').DataTable().destroy();
    // $("#cover-spin").show();
    // alert(ruangan_id);
    table = $('#tabel_anatomi').DataTable({
        autoWidth: false,
        serverSide: true,
        "searching": false,
        "processing": false,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "order": [],
        // "pageLength": 10,
        "ordering": false,

        ajax: {
            url: '{site_url}Thistory_tindakan/load_anatomi_asmed_igd',
            type: "POST",
            dataType: 'json',
            data: {
                assesmen_id: assesmen_id,
                versi_edit: versi_edit,

            }
        },
        "drawCallback": function(settings) {
            $("#cover-spin").hide();
            disabel_edit();
        }
    });
}

function load_diagnosa() {

    let assesmen_id = $("#assesmen_id").val();
    let versi_edit = $("#versi_edit").val();
    $('#tabel_diagnosa').DataTable().destroy();
    // $("#cover-spin").show();
    // alert(ruangan_id);
    table = $('#tabel_diagnosa').DataTable({
        autoWidth: false,
        serverSide: true,
        "searching": false,
        "processing": false,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "order": [],
        // "pageLength": 10,
        "ordering": false,

        ajax: {
            url: '{site_url}Thistory_tindakan/load_diagnosa_asmed_igd',
            type: "POST",
            dataType: 'json',
            data: {
                assesmen_id: assesmen_id,
                versi_edit: versi_edit,
            }
        },
        "drawCallback": function(settings) {
            $("#cover-spin").hide();
            disabel_edit();
        }
    });
}
</script>
