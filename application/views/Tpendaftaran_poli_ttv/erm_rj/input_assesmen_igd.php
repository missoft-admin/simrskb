<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='input_assesmen_igd'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_assesmen_igd' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?	
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
					$waktutindakan=date('H:i:s');
					$waktuinfus=date('H:i:s');
					$tanggalinfus=date('d-m-Y');
					$tanggaltindakan=date('d-m-Y');
						$disabel_input='';
						
						if ($st_input_assesmen=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_assesmen=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_assesmen_igd=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian_igd()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template_igd()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_assesmen=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_assesmen_igd=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen_igd" onclick="create_assesmen_igd()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template_igd()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen_igd()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<button class="btn btn-success btn_ttd" type="button" onclick="set_ttd_assesmen()"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_assesmen_igd" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template_igd" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template_igd()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Tanggal Masuk</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tgl" data-date-format="dd/mm/yyyy" id="tanggaldatang" placeholder="HH/BB/TTTT" name="tanggaldatang" value="<?= $tanggaldatang ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Jam Masuk</label>
									<div class="input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tgl" id="waktudatang" value="<?= $waktudatang ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Surat pengantar / Rujukan</label>
									<select tabindex="8" id="surat_pengantar" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($surat_pengantar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(55) as $row){?>
										<option value="<?=$row->id?>" <?=($surat_pengantar == $row->id ? 'selected="selected"' : '')?> <?=($surat_pengantar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Sarana Transportasi</label>
									<select tabindex="8" id="sarana_transport" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sarana_transport == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(52) as $row){?>
										<option value="<?=$row->id?>" <?=($sarana_transport == $row->id ? 'selected="selected"' : '')?> <?=($sarana_transport == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
									
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Asal Rujukan</label>
									<select tabindex="32" name="idasalpasien" id="idasalpasien" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?php foreach (get_all('mpasien_asal') as $r):?>
											<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<?php if ($pendaftaran_id != '' && ($idasalpasien == '2' || $idasalpasien == '3')) {?>
								<?php
									if ($idasalpasien == 2) {
										$idtipeRS = 1;
									} elseif ($idasalpasien == 3) {
										$idtipeRS = 2;
									}
									
								}else{
									$idtipeRS='';
								}
								?>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Nama Fasilitas Kesehatan</label>
									<div class="input-group">
										<select tabindex="33" name="idrujukan" id="idrujukan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach (get_all('mrumahsakit', ['idtipe' => $idtipeRS]) as $r) {?>
												<option value="<?= $r->id?>" <?=($idrujukan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
											<?php }?>
										</select>
										
										<span class="input-group-btn">
											<button class="btn btn-warning" title="refresh" onclick="set_asal_rujukan()" type="button"><i class="fa fa-refresh"></i></button>
											<a href="{base_url}mrumahsakit/create" title="Tambah Faskes" target="_blannk" class="btn btn-success" type="button"><i class="fa fa-plus"></i></a>
											
										</span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Cara Masuk</label>
									<select tabindex="8" id="cara_masuk" name="cara_masuk" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($cara_masuk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(53) as $row){?>
										<option value="<?=$row->id?>" <?=($cara_masuk == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($cara_masuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Macam Kasus</label>
									<select tabindex="8" id="macam_kasus" name="macam_kasus" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($macam_kasus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(54) as $row){?>
										<option value="<?=$row->id?>" <?=($macam_kasus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($macam_kasus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Nama pengantar</label>
									<input tabindex="29" type="text" class="form-control auto_blur"   id="namapengantar" placeholder="Nama Pengantar" name="namapengantar" value="{namapengantar}" required>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">No Tlp Penanggung Jawab</label>
									<input tabindex="32" type="text" class="form-control auto_blur"   id="teleponpengantar" placeholder="Telepon" name="teleponpengantar" value="{teleponpengantar}" required>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >KASUS KECELAKAAN LALULINTAS </h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Kasus Kecelakaan ?</label>
									<select tabindex="8" id="st_kecelakaan" name="st_kecelakaan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="1" <?=($st_kecelakaan == '1' ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($st_kecelakaan == '0' ? 'selected="selected"' : '')?>>TIDAK</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 div_kecelakaan">
								<div class="col-md-12 ">
									<label for="example-input-normal">Jenis Kecelakaan</label>
									<select tabindex="8" id="jenis_kecelakaan" name="jenis_kecelakaan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" <?=($jenis_kecelakaan == '0' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(19) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kecelakaan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_kecelakaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group div_kecelakaan">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="jenis_kendaraan_pasien">Jenis Kendaraan Pasien</label>
									<select tabindex="8" id="jenis_kendaraan_pasien" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" <?=($jenis_kendaraan_pasien == '0' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(63) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kendaraan_pasien == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_kendaraan_pasien == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="jenis_kendaraan_lawan">Jenis Kendaraan Lawan</label>
									<select tabindex="8" id="jenis_kendaraan_lawan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" <?=($jenis_kendaraan_lawan == '0' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(64) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kendaraan_lawan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($jenis_kendaraan_lawan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group div_kecelakaan">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="lokasi_kecelakaan">Lokasi Kecelakaan</label>
									<select tabindex="8" id="lokasi_kecelakaan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0" <?=($lokasi_kecelakaan == '0' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(65) as $row){?>
										<option value="<?=$row->id?>" <?=($lokasi_kecelakaan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($lokasi_kecelakaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="lokasi_lainnya">Jika Tidak ada dalam pilihan, sebutkan</label>
									<textarea id="lokasi_lainnya" class="form-control auto_blur" name="story" rows="1" style="width:100%"><?=$lokasi_lainnya?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >ANAMNESIS </h5>
							</div>
						</div>
						<input class="form-control"  type="hidden" id="st_anamnesa" name="st_anamnesa" value="<?=$st_anamnesa?>" >				
								
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Keluhan Utama </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" value="1" id="auto_anamnesa" name="anamnesa" onclick="set_auto()" <?=($st_anamnesa=='1'?'checked':'')?>><span></span> Auto Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio"  value="1" id="allo_anamnesa" name="anamnesa" onclick="set_allo()" <?=($st_anamnesa=='2'?'checked':'')?>><span></span> Allo Anamnesis
									</label>
									
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control auto_blur push-5-r"  type="text"  id="nama_anamnesa" name="nama_anamnesa" value="{nama_anamnesa}" placeholder="Tuliskan Nama" >
								</div>
								<div class="col-md-3">
										<input class="allo_anamnesa form-control auto_blur"  type="text" id="hubungan_anamnesa" name="hubungan_anamnesa" value="{hubungan_anamnesa}" placeholder="Hubungan" >
								</div>
								<div class="col-md-12 push-10-t">
									<textarea id="keluhan_utama" class="form-control auto_blur" name="story" rows="2" style="width:100%"><?=$keluhan_utama?></textarea>
								</div>
								
							</div>
							<input class="form-control"  type="hidden" id="st_riwayat_penyakit" name="st_riwayat_penyakit" value="<?=$st_riwayat_penyakit?>" >
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Penyakit </label>
								<div class="col-md-3">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(0)" <?=($st_riwayat_penyakit=='0'?'checked':'')?>><span></span> Tidak Ada
									</label>
									
								</div>
								<div class="col-md-2">
									<label class="css-input css-radio css-radio-primary push-10-r">
										<input type="radio" name="opsi_riwayat_penyakit" onclick="set_riwayat(1)" <?=($st_riwayat_penyakit=='1'?'checked':'')?>><span></span> Ada
									</label>
									
								</div>
								<div class="col-md-7">
									<select id="riwayat_penyakit" name="riwayat_penyakit" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one.." multiple>
										
										<?foreach($list_riwayat_penyakit as $r){?>
										<option value="<?=$r->id?>" <?=$r->pilih?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								<div class="col-md-12 push-10-t">
									<textarea id="riwayat_penyakit_lainnya" class="form-control auto_blur" name="riwayat_penyakit_lainnya" rows="2" style="width:100%"><?=$riwayat_penyakit_lainnya?></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Alergi </label>
								<div class="col-md-12">
									<select id="riwayat_alergi" name="riwayat_alergi" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($riwayat_alergi=='0'?'selected':'')?>>Tidak Ada Alergi</option>
										<option value="1" <?=($riwayat_alergi=='1'?'selected':'')?>>Tidak Diketahui</option>
										<option value="2" <?=($riwayat_alergi=='2'?'selected':'')?>>Ada Alergi</option>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<label class="col-xs-12">Riwayat Pengobatan </label>
								<div class="col-md-12">
									<textarea id="riwayat_pengobatan" class="form-control auto_blur" name="riwayat_pengobatan" rows="1" style="width:100%"><?=$riwayat_pengobatan?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group div_alergi_all">
							<div class="col-md-12 div_input_alergi">
								<div class="col-md-3 ">
									<input type="hidden" readonly class="form-control input-sm" id="alergi_id" value=""> 
									<label for="example-input-normal">Jenis Alergi</label>
									<select id="input_jenis_alergi" name="input_jenis_alergi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
										<?foreach(get_all('merm_referensi',array('ref_head_id'=>24,'status'=>1)) as $r){?>
										<option value="<?=$r->nilai?>"><?=$r->ref?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4 ">
									<label for="example-input-normal">Detail Alergi</label>
									<input class="form-control" type="text" id="input_detail_alergi" name="input_detail_alergi" placeholder="Detail Alergi">
								</div>
								<div class="col-md-5 ">
									<label for="example-input-normal">Reaksi</label>
									<div class="input-group">
										<input class="form-control" type="text" id="input_reaksi_alergi" name="input_reaksi_alergi" placeholder="Reaksi Alergi">
										<span class="input-group-btn">
											<button class="btn btn-info" onclick="simpan_alergi_igd()" id="btn_add_alergi" type="button"><i class="fa fa-plus"></i> Add</button>
											<button class="btn btn-warning" onclick="clear_input_alergi()" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
									
								</div>
							</div>
							
						</div>
						<div class="form-group div_alergi_all">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="block">
										<ul class="nav nav-tabs nav-tabs-alt nav-justified" data-toggle="tabs">
											<li class="" id="li_alergi_1">
												<a href="#alergi_1"  onclick="load_alergi_igd()"><i class="fa fa-pencil "></i> Data Alergi</a>
											</li>
											<li class="" id="li_alergi_2">
												<a href=" #alergi_2" onclick="load_alergi_igd_his()"><i class="fa fa-history"></i> History Alergi</a>
											</li>
											
										</ul>
										<div class="block-content tab-content ">
											<div class="tab-pane " id="alergi_1">
												<div class="table-responsive div_data_alergi">
													<table class="table" id="index_alergi">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="tab-pane " id="alergi_2">
												<div class="table-responsive">
													<table class="table" id="index_alergi_his">
														<thead>
															<tr>
																<th width="10%">No</th>
																<th width="15%">Jenis Alergi</th>
																<th width="20%">Detail Alergi</th>
																<th width="30%">Reaksi</th>
																<th width="15%">User Input</th>
																<th width="10%">Action</th>
															   
															</tr>
															
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							
							</div>
							<div class="col-md-6 ">
									
							</div>
							
						</div>
						<div class="form-group ">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<input type="hidden" readonly class="form-control input-sm" id="obat_id" value=""> 
									<label for="example-input-normal">Obat</label>
									<input class="form-control" type="text" id="nama_obat"  placeholder="Nama Obat">
								</div>
								<div class="col-md-4 ">
									<label for="example-input-normal">Dosis</label>
									<input class="form-control" type="text" id="dosis" placeholder="Dosis">
								</div>
								<div class="col-md-5 ">
									<label for="example-input-normal">Waktu Penggunaan</label>
									<div class="input-group">
										<input class="form-control" type="text" id="waktu"  placeholder="Waktu Penggunaan">
										<span class="input-group-btn">
											<button class="btn btn-info" onclick="simpan_obat_igd()" id="btn_add_obat" type="button"><i class="fa fa-plus"></i> Add</button>
											<button class="btn btn-warning" onclick="clear_input_obat()" type="button"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
									
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="st_hamil">Status Kehamilan</label>
									<select id="st_hamil" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($st_hamil=='0'?'selected':'')?>>TIDAK</option>
										<option value="1" <?=($st_hamil=='1'?'selected':'')?>>YA</option>
										
									</select>
								</div>
								<div class="col-md-3 ">
									<label for="hamil_g">G</label>
									<div class="input-group">
										<input class="form-control auto_blur"  type="text" id="hamil_g" value="{hamil_g}"  placeholder="G" required>
										<span class="input-group-addon">G</span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="hamil_g">P</label>
									<div class="input-group">
										<input class="form-control auto_blur"  type="text" id="hamil_p" value="{hamil_p}"  placeholder="P" required>
										<span class="input-group-addon">P</span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="hamil_g">A</label>
									<div class="input-group">
										<input class="form-control auto_blur"  type="text" id="hamil_a" value="{hamil_a}"  placeholder="A" required>
										<span class="input-group-addon">A</span>
									</div>
								</div>
								
							</div>
							
						</div>
						<div class="form-group ">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="index_obat">
											<thead>
												<tr>
													<th width="30%">Obat</th>
													<th width="30%">Dosis</th>
													<th width="30%">Waktu Penggunaan</th>
													<th width="10%">Action</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="st_menyusui">Status Menyusui</label>
									<select id="st_menyusui" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($st_menyusui=='0'?'selected':'')?>>TIDAK</option>
										<option value="1" <?=($st_menyusui=='1'?'selected':'')?>>YA</option>
										
									</select>
								</div>
								<div class="col-md-9 ">
									<label for="info_menyusui">Informasi Lainnya</label>
									<input type="text" class="form-control input-sm auto_blur" id="info_menyusui" value="{info_menyusui}"> 
								</div>
								
								
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Fisik </h5>
								<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Keadaan Umum</label>
									<select tabindex="8" id="keadaan_umum"   name="keadaan_umum" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(61) as $row){?>
										<option value="<?=$row->id?>"  <?=($keadaan_umum == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Tingkat Kesadaran</label>
									<select tabindex="8" id="tingkat_kesadaran"   name="tingkat_kesadaran" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>"  <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal auto_blur"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Tinggi Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
										<span class="input-group-addon">Cm</span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Berat Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Psikologis, Sosial Ekonomi, Spiritual </h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Hubungan Pasien Dengan Anggota Keluarga</label>
									<select tabindex="8" id="hubungan_anggota_keluarga"  class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($hubungan_anggota_keluarga == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(41) as $row){?>
										<option value="<?=$row->id?>" <?=($hubungan_anggota_keluarga == $row->id ? 'selected="selected"' : '')?> <?=($hubungan_anggota_keluarga == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Status Psikologis</label>
									<select tabindex="8" id="st_psikologis" name="st_psikologis" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_psikologis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(26) as $row){?>
										<option value="<?=$row->id?>" <?=($st_psikologis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_psikologis == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Sosial Ekonomi</label>
									<select tabindex="8" id="st_sosial_ekonomi" name="st_sosial_ekonomi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_sosial_ekonomi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(27) as $row){?>
										<option value="<?=$row->id?>" <?=($st_sosial_ekonomi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_sosial_ekonomi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Spiritual</label>
									<select tabindex="8" id="st_spiritual" name="st_spiritual" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_spiritual == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(28) as $row){?>
										<option value="<?=$row->id?>" <?=($st_spiritual == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_spiritual == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Pendidikan</label>
									<select tabindex="8" id="pendidikan" name="pendidikan" class="js-select2 form-control opsi_change select2_edit" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pendidikan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(13) as $row){?>
										<option value="<?=$row->id?>" <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Pekerjaan</label>
									<select tabindex="8" id="pekerjaan" name="pekerjaan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($pekerjaan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(6) as $row){?>
										<option value="<?=$row->id?>" <?=($pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Agama</label>
									<select tabindex="8" id="agama" name="agama" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($agama == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(3) as $row){?>
										<option value="<?=$row->id?>" <?=($agama == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Status Nutrisi</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Keluhan, Perubahan Nafsu Makan</label>
									<select tabindex="8" id="st_nafsu_makan" name="st_nafsu_makan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_nafsu_makan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(29) as $row){?>
										<option value="<?=$row->id?>"  <?=($st_nafsu_makan == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_nafsu_makan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penurunan Berat Badan, 3 Bln terakhir</label>
									<select tabindex="8" id="st_turun_bb" name="st_turun_bb" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_turun_bb == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(30) as $row){?>
										<option value="<?=$row->id?>" <?=($st_turun_bb == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_turun_bb == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Mual</label>
									<select tabindex="8" id="st_mual" name="st_mual" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_mual == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(31) as $row){?>
										<option value="<?=$row->id?>" <?=($st_mual == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_mual == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Muntah</label>
									<select tabindex="8" id="st_muntah" name="st_muntah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_muntah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(32) as $row){?>
										<option value="<?=$row->id?>" <?=($st_muntah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_muntah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Faktor Pemberat (Komorbid)</label>
									<select tabindex="8" id="faktor_komorbid" name="faktor_komorbid" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($faktor_komorbid == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(66) as $row){?>
										<option value="<?=$row->id?>"  <?=($faktor_komorbid == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($faktor_komorbid == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penurunan Kapasitas Fungsi</label>
									<select tabindex="8" id="penurunan_fungsi" name="penurunan_fungsi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($penurunan_fungsi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(67) as $row){?>
										<option value="<?=$row->id?>"  <?=($penurunan_fungsi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($penurunan_fungsi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >SKRINING RISIKO CEDERA</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<?
									$q="SELECT *FROM mrisiko_jatuh WHERE idtipe='2' AND staktif='1'";
									$list_template_risiko=$this->db->query($q)->result();
									?>
									<label for="template_id">Mode Penilaian Risiko Jatuh </label>
									<select id="template_id"  name="template_id" class="js-select2 form-control opsi" style="width: 100%;" data-placeholder="Choose one..">
										<option value="" <?=($template_id=='0'?'selected':'')?>>-Belum dipilih-</option>
										<?foreach($list_template_risiko as $r){?>
										<option value="<?=$r->id?>" <?=($template_id==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Skor Pengkajian</label>
									<input class="form-control auto_blur" readonly type="text" id="skor_pengkajian"  value="" placeholder="Skor Pengkajian" >
									<input class="form-control " type="hidden" id="st_tindakan"  value="" placeholder="" >
								</div>
								<div class="col-md-9 ">
									<label for="example-input-normal">Hasil Pengkajian</label>
									<input class="form-control auto_blur" readonly type="text" id="risiko_jatuh"  value="" name="risiko_jatuh" placeholder="Hasil Pengkajian" >
								</div>
							</div>
						</div>
						<?if ($risiko_jatuh_header){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" ><?=$risiko_jatuh_header?></h5>
							</div>
						</div>
						
						<div class="form-group push-5-t">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-condensed" id="tabel_skrining">
											<thead>
												<tr>
													<th width="80%">Skrining</th>
													<th width="20%">Jawaban</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tindakan</label>
									<input class="form-control auto_blur" readonly type="text" id="nama_tindakan"  value="{nama_tindakan}" placeholder="Tindakan" >
								</div>
								
								<div class="col-md-6 ">
									<label for="example-input-normal">Apakah Tindakan Dilakukan</label>
									<select tabindex="8" id="st_tindakan_action" name="st_tindakan_action" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_tindakan_action == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="1" <?=($st_tindakan_action == 1 ? 'selected="selected"' : '')?>>YA</option>
										<option value="0" <?=($st_tindakan_action == 0 ? 'selected="selected"' : '')?>>TIDAK</option>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary"><?=$risiko_jatuh_footer?></label>
							</div>
						</div>
						<?}?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >STATUS FUNGSIONAL</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Status Fungsional</label>
									<select tabindex="8" id="st_fungsional" name="st_fungsional" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_fungsional == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(33) as $row){?>
										<option value="<?=$row->id?>" <?=($st_fungsional == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_fungsional == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penggunaan Alat Bantu</label>
									<select tabindex="8" id="st_alat_bantu" name="st_alat_bantu" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_alat_bantu == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(34) as $row){?>
										<option value="<?=$row->id?>" <?=($st_alat_bantu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_alat_bantu == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Cacat Tubuh</label>
									<select tabindex="8" id="st_cacat" name="st_cacat" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_cacat == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(35) as $row){?>
										<option value="<?=$row->id?>" <?=($st_cacat == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_cacat == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Lainnya Sebutkan</label>
									<input class="form-control auto_blur" type="text" id="cacat_lain"  value="{cacat_lain}" name="cacat_lain" placeholder="Lainnya" >
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >SKRINING DECUBITUS, P3 DAN BATUK</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Terdapat Risiko Luka Decibitus</label>
									<select tabindex="8" id="st_decubitus" name="st_decubitus" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_decubitus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(68) as $row){?>
										<option value="<?=$row->id?>" <?=($st_decubitus == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_decubitus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Perencanaan Pemulangan Pasien (P3)</label>
									<select tabindex="8" id="st_alat_bantu" name="st_p3" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_p3 == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(69) as $row){?>
										<option value="<?=$row->id?>" <?=($st_p3 == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_p3 == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-4">
									<label for="st_riwayat_demam">Riwayat Demam</label>
									<select tabindex="8" id="st_riwayat_demam" name="st_riwayat_demam" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_riwayat_demam == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(70) as $row){?>
										<option value="<?=$row->id?>" <?=($st_riwayat_demam == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_riwayat_demam == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4">
									<label for="st_riwayat_demam">Berkeringat Pada malam hari tanpa aktivitas</label>
									<select tabindex="8" id="st_berkeringat_malam" name="st_berkeringat_malam" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_berkeringat_malam == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(70) as $row){?>
										<option value="<?=$row->id?>" <?=($st_berkeringat_malam == '' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_berkeringat_malam == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-4">
									<label for="st_riwayat_demam">Riwayat Bepergian dari daerah wabah</label>
									<select tabindex="8" id="st_pergi_area_wabah" name="st_pergi_area_wabah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_pergi_area_wabah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(73) as $row){?>
						<option value="<?=$row->id?>" <?=(($st_pergi_area_wabah == '0' || $st_pergi_area_wabah == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_pergi_area_wabah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="st_riwayat_demam">Riwayat Pemakaian obat jangka panjang</label>
									<select tabindex="8" id="st_pemakaian_obat_panjang" name="st_pemakaian_obat_panjang" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_pemakaian_obat_panjang == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(71) as $row){?>
										<option value="<?=$row->id?>" <?=(($st_pemakaian_obat_panjang == '0' || $st_pemakaian_obat_panjang == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_pemakaian_obat_panjang == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="st_riwayat_demam">Riwayat Berat badan turun tanpa sebab yang diketahui</label>
									<select tabindex="8" id="st_turun_bb_tanpa_sebab" name="st_turun_bb_tanpa_sebab" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=(($st_turun_bb_tanpa_sebab == ''  || $st_turun_bb_tanpa_sebab == '0')  ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(72) as $row){?>
										<option value="<?=$row->id?>" <?=(($st_turun_bb_tanpa_sebab == '0' || $st_turun_bb_tanpa_sebab=='') && $row->st_default=='1'? 'selected="selected"' : '')?> <?=($st_turun_bb_tanpa_sebab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<label class="h5 text-primary" >DIAGNOSA KEPERAWATAN</h5>
							</div>
							<div class="col-md-6 ">
								<label class="h5 text-primary" >RENCANA ASUHAN KEPERAWATAN</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="index_header">
											<thead>
												<tr>
													<th width="55%">
														Diagnosa
													</th>
													<th width="25%">
														Priority
													</th>
													<th width="20%">
														
													</th>
												</tr>
												<tr>
													<th width="55%">
														<input class="form-control " type="hidden" id="diagnosa_id"  value="" placeholder="" >
														<select tabindex="8" id="mdiagnosa_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(get_all('mdiagnosa',array('staktif'=>1)) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama.' ('.$row->kode_diagnosa.')'?></option>
															<?}?>
														</select>
													</th>
													<th width="25%">
														<select tabindex="8" id="prioritas" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Diagnosa" required>
															
															<?for($i=1;$i<20;$i++){?>
															<option value="<?=$i?>"><?=$i?></option>
															<?}?>
														</select>
													</th>
													<th width="20%">
														<span class="input-group-btn">
															<button class="btn btn-info" onclick="simpan_diagnosa_igd()" id="btn_add_diagnosa" type="button"><i class="fa fa-plus"></i> Add</button>
															<button class="btn btn-warning" onclick="clear_input_diagnosa()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</th>
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table" id="tabel_diagnosa">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Diagnosa Keperawatan</th>
													<th width="10%">Priority</th>
													<th width="25%">User</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Diagnosa</label>
									<select tabindex="8" id="diagnosa_id_list" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										
									</select>
								</div>
								<div class="col-md-12 push-10-t">
									<table class="table-asuhan" style="border-spacing: 10px;;!important" id="tabel_rencana_asuhan">
										<thead></thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="intruksi_keperawatan" class="text-primary">Intruksi Keperawatan</label>
									<textarea class="form-control auto_blur" id="intruksi_keperawatan"  rows="2" placeholder="Intruksi Keperawatan"> <?=$intruksi_keperawatan?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >PEMBERIAN OBAT / INFUS</h5>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered" id="index_obat_infus">
											<thead>
												<tr>
													<th width="9%" class="text-center">Tanggal</th>
													<th width="7%" class="text-center">Jam</th>
													<th width="17%" class="text-center">Nama Obat / Infus</th>
													<th width="5%" class="text-center">Kuantitas</th>
													<th width="8%" class="text-center">Dosis</th>
													<th width="10%" class="text-center">Rute Pemberian</th>
													<th width="12%" class="text-center">Diperiksa Oleh</th>
													<th width="12%" class="text-center">Diberikan Oleh</th>
													<th width="8%" class="text-center">Action</th>
												</tr>
												<tr>
													<th>
														<div class="input-group date">
															<input tabindex="80" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggalinfus" placeholder="HH/BB/TTTT" value="<?= $tanggalinfus ?>" required>
														</div>
													</th>
													<th>
														<div class="input-group date">
															<input tabindex="81" type="text" class="time-datepicker form-control " id="waktuinfus" value="<?= $waktuinfus ?>" required>
														</div>
													</th>
													<th>
														<div id="" class="input-group">
															<select name="obat_id_select2" tabindex="82" id="obat_id_select2" data-placeholder="Cari Obat" style="width:100%" class="form-control  input-sm kunci"></select>
															<span class="input-group-btn">
																<button onclick="show_modal_obat()" title="Cari Obat" class="btn btn-sm btn-info kunci" type="button" id="search_obat_1"><i class="fa fa-search"></i></button>
															</span>
														</div>
													</th>
													<th>
														<input tabindex="83" class="form-control decimal "  type="text" id="kuantitas_infus"  value="" placeholder="Qty" >
													</th>
													<th>
														<input tabindex="84" class="form-control "  type="text" id="dosis_infus"  value="" placeholder="Dosis" >
													</th>
													<th>
														<select tabindex="85" id="rute"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach(list_variable_ref(74) as $row){?>
															<option value="<?=$row->id?>" ><?=$row->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th>
														<select tabindex="86" id="ppa_periksa"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>"><?=$r->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th>
														<select tabindex="87" id="ppa_pemberi"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>"><?=$r->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th>
														<input class="form-control "  type="hidden" id="obat_infus_id"  value="" placeholder="" >
														<input class="form-control " readonly type="hidden" id="idtipe"  value="" placeholder="" >
														<span class="input-group-btn">
															<button tabindex="88" class="btn btn-info" onclick="simpan_obat_infus_igd()" id="btn_add_obat_infus" type="button"><i class="fa fa-plus"></i> Add</button>
															<button tabindex="89" class="btn btn-warning" onclick="clear_input_obat_infus()" type="button"><i class="fa fa-refresh"></i></button>
														</span>
													</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >TINDAKAN</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered" id="index_tindakan">
											<thead>
												<tr>
													<th width="12%">Tanggal</th>
													<th width="10%">Jam</th>
													<th width="25%">Nama Tindakan</th>
													<th width="15%">Petugas Yang Melaksanakan</th>
													<th width="10%">Waktu Mulai</th>
													<th width="10%">Waktu Selesai</th>
													<th width="20%">Action</th>
												</tr>
												<tr>
													<th width="10%">
														<div class="input-group date">
															<input tabindex="80" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggaltindakan" placeholder="HH/BB/TTTT" value="<?= $tanggaltindakan ?>" required>
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														</div>
													</th>
													<th width="10%">
														<div class="input-group date">
															<input tabindex="81" type="text" class="time-datepicker form-control " id="waktutindakan" value="<?= $waktutindakan ?>" required>
															<span class="input-group-addon"><i class="si si-clock"></i></span>
														</div>
													</th>
													<th width="25%">
														<input class="form-control " type="hidden" id="tindakan_id"  value="" placeholder="" >
														<select tabindex="8" id="mtindakan_id" class="form-control " style="width: 100%;" data-placeholder="Cari Tindakan" required></select>
													</th>
													<th width="15%">
														<select tabindex="86" id="ppa_pelaksana"  class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="" selected>Pilih Opsi</option>
															<?foreach($list_ppa as $r){?>
															<option value="<?=$r->id?>"><?=$r->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th width="10%">
														<div class="input-group date">
															<input tabindex="81" type="text" class="time-datepicker form-control " id="waktu_mulai" value="" required>
															<span class="input-group-addon"><i class="si si-clock"></i></span>
														</div>
													</th>
													<th width="10%">
														<div class="input-group date">
															<input tabindex="81" type="text" class="time-datepicker form-control " id="waktu_selesai" value="" required>
															<span class="input-group-addon"><i class="si si-clock"></i></span>
														</div>
													</th>
													<th width="20%">
														<span class="input-group-btn">
															<button class="btn btn-info btn-sm" onclick="simpan_tindakan_igd()" id="btn_add_tindakan" type="button"><i class="fa fa-plus"></i> Add</button>
															<button class="btn btn-warning btn-sm" onclick="clear_input_tindakan()" type="button"><i class="fa fa-refresh"></i></button>
															<button class="btn btn-success btn-sm" disabled onclick="clear_input_tindakan()" type="button"> Alat Medis</button>
															<button class="btn btn-primary btn-sm" disabled onclick="clear_input_tindakan()" type="button"> BMHP</button>
														</span>
													</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >EDUKASI</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Kesediaan Pasien / Keluarga untuk menerima informasi yang diberikan</label>
									<select tabindex="8" id="st_menerima_info" name="st_menerima_info" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_menerima_info == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(39) as $row){?>
										<option value="<?=$row->id?>" <?=($st_menerima_info == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_menerima_info == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat hambatan dalam edukasi</label>
									<select tabindex="8" id="st_hambatan_edukasi" name="st_hambatan_edukasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_hambatan_edukasi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(40) as $row){?>
										<option value="<?=$row->id?>" <?=($st_hambatan_edukasi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_hambatan_edukasi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Dibutuhkan Penerjemaah ? (Jika Ya Sebutkan)</label>
									<select tabindex="8" id="st_penerjemaah" name="st_penerjemaah" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_penerjemaah == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(38) as $row){?>
										<option value="<?=$row->id?>" <?=($st_penerjemaah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_penerjemaah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Penerjemaah Yang Dibutuhkan</label>
									<input class="form-control auto_blur" type="text" id="penerjemaah"  value="{penerjemaah}" name="penerjemaah" placeholder="Penerjemaah Yang Dibutuhkan" >
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12">
									<label for="example-input-normal">Kebutuhan Edukasi</label>
									<select tabindex="8" id="medukasi_id" name="medukasi_id[]" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?foreach($list_edukasi as $row){?>
										<option value="<?=$row->id?>"  <?=$row->pilih?>><?=$row->judul?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Catatan lain Edukasi</label>
									<input class="form-control auto_blur" type="text" id="catatan_edukasi"  value="{catatan_edukasi}" name="catatan_edukasi" placeholder="Catatan" >
									
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >RINGKASAN KONDISI KELUAR</h5>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Keadaan Umum</label>
									<select tabindex="8" id="keadaan_umum_keluar" name="keadaan_umum_keluar" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($keadaan_umum_keluar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(61) as $row){?>
										<option value="<?=$row->id?>" <?=($keadaan_umum_keluar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($keadaan_umum_keluar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="tingkat_kesadaran_keluar">Tingkat Kesadaran</label>
									<select tabindex="8" id="tingkat_kesadaran_keluar" name="tingkat_kesadaran_keluar" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($tingkat_kesadaran_keluar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>" <?=($tingkat_kesadaran_keluar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($tingkat_kesadaran_keluar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"  type="text" id="td_sistole_keluar"  value="{td_sistole_keluar}" name="td_sistole_keluar" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal auto_blur"  type="text" id="td_diastole_keluar" value="{td_diastole_keluar}"  name="td_diastole_keluar" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Pernafasan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="nafas_keluar" value="{nafas_keluar}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">GDS</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur"   type="text" id="gds_keluar" value="{gds_keluar}"  placeholder="GDS" required>
										<span class="input-group-addon"><?=$satuan_gds?></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Status Pulang</label>
									<select tabindex="8" id="st_pulang" name="st_pulang" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?foreach(list_variable_ref(62) as $row){?>
										<option value="<?=$row->id?>" <?=($st_pulang == $row->id ? 'selected="selected"' : '')?> <?=($st_pulang == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Pasien Keluar IGD (Tanggal)</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="<?= $tanggal_keluar ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6">
									<label for="example-input-normal">Rencana Kontrol</label>
									<select tabindex="8" id="rencana_kontrol" name="rencana_kontrol" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($rencana_kontrol == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(43) as $row){?>
										<option value="<?=$row->id?>" <?=(($rencana_kontrol == '0' || $rencana_kontrol == '') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($rencana_kontrol == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Tanggal Kontrol</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggal_kontrol" placeholder="dd-mm-yyyy" value="<?= $tanggal_kontrol ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							
							<?
								if ($idpoli_kontrol==''){
									$idpoli_kontrol=$idpoliklinik;
								}
								if ($iddokter_kontrol=='0' || $iddokter_kontrol==''){
									$iddokter_kontrol=$iddokter;
								}
							?>
							<div class="col-md-6">
								<div class="col-md-12">
									<label for="example-input-normal">Poliklinik </label>
									<select id="idpoli_kontrol" name="idpoli_kontrol" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0"  <?=($idpoli_kontrol==''?'selected':'')?>>- Tidak Ada -</option>
										<?foreach($list_poli as $r){?>
										<option value="<?=$r->id?>" <?=($idpoli_kontrol==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
									
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12 ">
									<label for="example-input-normal">Dokter</label>
									<select id="iddokter_kontrol" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0"  <?=($iddokter_kontrol==''?'selected':'')?>>- Tidak Ada -</option>
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"  <?=($iddokter_kontrol==$r->id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
										
									</select>
									
								</div>
								
							</div>
						</div>
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<?foreach($list_poli as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian_igd()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_template_igd()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen_igd()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen_igd()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="jenis_ttd" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_ttd==''?'selected':'')?>>Silahkan Pilih</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Pasien Sendiri Sendiri</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?> >Keluarga Terdekat / Wali</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?> >Penanggung Jawab</option>
										
									</select>
									<label for="jenis_ttd">Yang Tandatangan</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							<div class="col-md-6 push-10">
								<div class="form-material">
									<select tabindex="30" id="ttd_hubungan" name="ttd_hubungan" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($ttd_hubungan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<label for="ttd_hubungan">Hubungan</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_igd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="obat_non_racikan-modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<?
					$q="SELECT H.idtipe,T.nama_tipe from munitpelayanan_tipebarang H
					LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
					WHERE H.idunitpelayanan='1'";
					$list_tipe_barang=$this->db->query($q)->result();
				?>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Tipe Barang</label>
								<div class="col-md-8">
									<select name="tipeid" id="tipeid" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
										<?foreach ($list_tipe_barang as $r){?>
										<option value="<?=$r->idtipe?>"><?=$r->nama_tipe?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-8">
									<select name="idkat" id="idkat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang" name="btn_filter_barang" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="table-Obat" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Satuan</th>
								
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var template_id_last=$("#template_id").val();
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});

$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	// set_ttd_assesmen();
	// $(".js-datetimepicker").datetimepicker({
		// format: "dd-mm-yyyy HH:mm"
	// });
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		set_kecelakaan();
		set_anamnesa();
		set_riwayat_alergi();
		set_riwayat($("#st_riwayat_penyakit").val())
		
		load_alergi_igd();
		load_obat_igd();
		load_obat_infus_igd();
		load_tindakan_assesmen_igd();
		// load_alergi_igd_his();
		load_skrining_risiko_jatuh_igd();
		load_diagnosa_igd();
		if ($("#signature64").val()){
			set_img_canvas();
		}
		
		$("#obat_id_select2").select2({
			minimumInputLength: 2,
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tpasien_penjualan/get_obat/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
				data: function(params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(item) {
							return {
								text: item.nama,
								id: item.id,
								idtipe: item.idtipe
							}
						})
					};
				}
			}
		});
		$("#mtindakan_id").select2({
			minimumInputLength: 2,
			tags: true,
			noResults: 'Tindakan Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}Tpendaftaran_poli_ttv/get_tindakan_assesmen_igd/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
							}
						})
					};
				}
			}
		});
		load_awal_assesmen=false;
	}
	
	// load_data_rencana_asuhan_igd(1);
});
$("#obat_id_select2").change(function() {
			// console.log('data sini masuk');
	data = $("#obat_id_select2").select2('data')[0];
	if (data){
	$("#idtipe").val(data.idtipe);
	}
	

});
$("#template_id").on("change", function(){
	let template_id=$(this).val();
	if (template_id!=template_id_last){
			swal({
			  title: 'Anda Yakain?',
			  text: "Mengganti Mode Penilaian!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Proses!'
			}).then(function(json_data) {
					$("#cover-spin").show();
					let assesmen_id=$("#assesmen_id").val();
					template_id_last=template_id;
					$.ajax({
						url: '{site_url}Tpendaftaran_poli_ttv/simpan_risiko_jatuh_template_igd', 
						dataType: "JSON",
						method: "POST",
						data : {
								assesmen_id:assesmen_id,
								template_id:template_id,
								
							},
						success: function(data) {
							
									console.log(data);
							if (data==null){
								swal({
									title: "Gagal!",
									text: "Simpan TTV.",
									type: "error",
									timer: 1500,
									showConfirmButton: false
								});

							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Ganti Mode Penilaian '});
								location.reload();			
							}
						}
					});
			}, function(dismiss) {
			  if (dismiss === 'cancel' || dismiss === 'close') {
					$("#template_id").val(template_id_last).trigger('change.select2');
			  } 
			})
		}else{
			save_all_detail(kategori_id_last);
		}
		
	
	
});
function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd_igd(){
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}

	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_igd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function load_ttd_igd(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/load_ttd_igd', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;
			
			// $("#sig").html('<img src="'+src+'"/>')
			// console.log(data.ttd);
		// set_img_canvas();
			$('#sig').signature('enable'). signature('draw', data.ttd);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd_igd();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
		 $(".data_asuhan").removeAttr('disabled');
	}
}
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function create_assesmen_igd(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_assesmen_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template_igd();
}
function create_with_template_igd(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template_igd(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_template_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_assesmen_igd(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_assesmen_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template_igd(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	// alert(nama_template);
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_template_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function set_riwayat(st_riwayat_penyakit){
	$("#st_riwayat_penyakit").val(st_riwayat_penyakit);
	if (st_riwayat_penyakit=='1'){
		$("#riwayat_penyakit").removeAttr('disabled');
		$("#riwayat_penyakit_lainnya").removeAttr('disabled');
		$("#riwayat_penyakit").attr('required');
	}else{
		// $("#riwayat_penyakit").empty();
		// $("#riwayat_penyakit option:selected").remove();
		// $("#riwayat_penyakit > option").attr("selected",false);
		$("#riwayat_penyakit").val('').trigger('change');
		$("#riwayat_penyakit").removeAttr('required');
		$("#riwayat_penyakit").attr('disabled','disabled');
		$("#riwayat_penyakit_lainnya").attr('disabled','disabled');
		
	}
	// generate_riwayat_penyakit();
	disabel_edit();
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
// $("#riwayat_penyakit").change(function(){
	// generate_riwayat_penyakit();
// });

//riwayat_penyakit
function generate_riwayat_penyakit(){
	if ($("#status_assemen").val()!='2'){
	var select_button_text
	let str_penyakit='';
	if ($("#st_riwayat_penyakit").val()=='1'){
		select_button_text = $('#riwayat_penyakit option:selected')
                .toArray().map(item => item.text).join();
	}else{
		 select_button_text ='Tidak Ada';
		// selected['Tidak Ada']
	}
	
	$("#riwayat_penyakit_lainnya").val(select_button_text);
	console.log(select_button_text);
	}
}
function set_allo(){
	$("#st_anamnesa").val('2');
	set_anamnesa();
}
function set_auto(){
	$("#st_anamnesa").val('1');
	set_anamnesa();
}
function set_riwayat_alergi(){
	let riwayat=$("#riwayat_alergi").val();
	if (riwayat=='2'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").show();
		$(".div_input_alergi").show();
		$("#li_alergi_1").addClass('active');
		$("#li_alergi_2").removeClass('active');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else if (riwayat=='1'){
		$(".div_alergi_all").show();
		$(".div_data_alergi").hide();
		$(".div_input_alergi").hide();
		// // $('#alergi_2 > a').tab('show');
		// $('#alergi_1 > a').tab('hide');
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
	}else{
		$("#alergi_1").addClass('active');
		$("#alergi_2").removeClass('active');
		$('#alergi_1 > a').tab('show');
		$('#alergi_2 > a').tab('hide');
		$(".div_alergi_all").hide();
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
	
		
	}
	
}
function set_kecelakaan(){
	let st_kecelakaan=$("#st_kecelakaan").val();
	if (st_kecelakaan=='1'){
		$(".div_kecelakaan").show();
		
	}else{
		$(".div_kecelakaan").hide();
		$("#jenis_kecelakaan").val(0).trigger('change');
		$("#jenis_kendaraan_pasien").val(0).trigger('change');
		$("#jenis_kendaraan_lawan").val(0).trigger('change');
		$("#lokasi_kecelakaan").val(0).trigger('change');
		$("#lokasi_lainnya").val('');
	}
}
$("#st_kecelakaan").on("change", function(){
	
	set_kecelakaan();
});
$("#riwayat_alergi").on("change", function(){
	
	set_riwayat_alergi();
});

function set_anamnesa(){
	if ($("#st_anamnesa").val()=='1'){
		$(".allo_anamnesa").attr('disabled','disabled');
		$(".allo_anamnesa").removeAttr('required');
		$("#nama_anamnesa").val('');
		$("#hubungan_anamnesa").val('');
	}else{
		$(".allo_anamnesa").removeAttr('disabled');
		$(".allo_anamnesa").attr('required');
		
	}
	if (load_awal_assesmen==false){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
		
	}
}
$("#medukasi_id").change(function(){
	if ($("#st_edited").val()=='0'){
	simpan_edukasi_igd();
	}
});
$("#riwayat_penyakit").change(function(){
	generate_riwayat_penyakit();
	if ($("#st_edited").val()=='0'){
		simpan_riwayat_penyakit_igd();
	}
});
$(".opsi_change").change(function(){
	console.log('OPSI CHANTE')
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});

$(".auto_blur").blur(function(){
	console.log('BLUR')
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
// $(".auto_blur").blur(function(){
	// if ($("#st_edited").val()=='0'){
		// $(this).removeClass('input_edited');
		// simpan_assesmen_ttv();
	// }else{
		// if ($("#st_edited").val()=='1'){
			// if ($(this).val()!=before_edit){
				// console.log('Ada Peruabahan');
				// // $(this).attr('input_edited');
				 // $(this).addClass('input_edited');
			// }
		// }
	// }
	
// });
$("#tingkat_kesadaran").change(function(){
		if ($("#st_edited").val()=='0'){
			// simpan_assesmen();
			simpan_assesmen_ttv();
		}
});

function simpan_assesmen_ttv(){
	// let assesmen_id=$("#assesmen_id").val();
	// let tingkat_kesadaran=$("#tingkat_kesadaran").val();
	// let nadi=$("#nadi").val();
	// let nafas=$("#nafas").val();
	// let td_sistole=$("#td_sistole").val();
	// let td_diastole=$("#td_diastole").val();
	// let suhu=$("#suhu").val();
	// let tinggi_badan=$("#tinggi_badan").val();
	// let berat_badan=$("#berat_badan").val();
	// console.log('SIMPAN TTV');
	// // $("#cover-spin").show();
	// $.ajax({
		// url: '{site_url}Tpendaftaran_poli_ttv/simpan_assesmen_ttv', 
		// dataType: "JSON",
		// method: "POST",
		// data : {
				// assesmen_id:assesmen_id,
				// tingkat_kesadaran:tingkat_kesadaran,
				// nadi:nadi,
				// nafas:nafas,
				// td_sistole:td_sistole,
				// td_diastole:td_diastole,
				// suhu:suhu,
				// tinggi_badan:tinggi_badan,
				// berat_badan:berat_badan,
				
			// },
		// success: function(data) {
			
					// console.log(data);
			// if (data==null){
				// swal({
					// title: "Gagal!",
					// text: "Simpan TTV.",
					// type: "error",
					// timer: 1500,
					// showConfirmButton: false
				// });

			// }else{
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan TTV Draft'});
				
			// }
		// }
	// });
}
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			// simpan_assesmen_ttv();
			simpan_edukasi_igd();
			simpan_riwayat_penyakit_igd();
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
function simpan_assesmen(){
	// alert(load_awal_assesmen);
	if (status_assemen !='2'){
		
	if (load_awal_assesmen==false){
	let assesmen_id=$("#assesmen_id").val();
	// alert($("#tanggal_keluar").val());return false;
	let tanggaldatang=$("#tanggaldatang").val();
	let waktudatang=$("#waktudatang").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	if (assesmen_id){
		console.log('SIMPAN');
		
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_assesmen_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					tanggaldatang:tanggaldatang,
					waktudatang:waktudatang,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					assesmen_id:$("#assesmen_id").val(),
					nama_template:nama_template,
					status_assemen:$("#status_assemen").val(),
					tanggal_datang:$("#tanggal_datang").val(),
					sarana_transport:$("#sarana_transport").val(),
					surat_pengantar:$("#surat_pengantar").val(),
					asal_rujukan:$("#asal_rujukan").val(),
					cara_masuk:$("#cara_masuk").val(),
					macam_kasus:$("#macam_kasus").val(),
					idasalpasien:$("#idasalpasien").val(),
					idrujukan:$("#idrujukan").val(),
					namapengantar:$("#namapengantar").val(),
					teleponpengantar:$("#teleponpengantar").val(),
					st_kecelakaan:$("#st_kecelakaan").val(),
					jenis_kecelakaan:$("#jenis_kecelakaan").val(),
					jenis_kendaraan_pasien:$("#jenis_kendaraan_pasien").val(),
					jenis_kendaraan_lawan:$("#jenis_kendaraan_lawan").val(),
					lokasi_kecelakaan:$("#lokasi_kecelakaan").val(),
					lokasi_lainnya:$("#lokasi_lainnya").val(),
					st_hamil:$("#st_hamil").val(),
					hamil_g:$("#hamil_g").val(),
					hamil_p:$("#hamil_p").val(),
					hamil_a:$("#hamil_a").val(),
					st_menyusui:$("#st_menyusui").val(),
					info_menyusui:$("#info_menyusui").val(),
					keadaan_umum:$("#keadaan_umum").val(),
					tingkat_kesadaran:$("#tingkat_kesadaran").val(),
					nadi:$("#nadi").val(),
					nafas:$("#nafas").val(),
					td_sistole:$("#td_sistole").val(),
					td_diastole:$("#td_diastole").val(),
					suhu:$("#suhu").val(),
					tinggi_badan:$("#tinggi_badan").val(),
					berat_badan:$("#berat_badan").val(),
					agama:$("#agama").val(),
					faktor_komorbid:$("#faktor_komorbid").val(),
					penurunan_fungsi:$("#penurunan_fungsi").val(),
					st_decubitus:$("#st_decubitus").val(),
					st_p3:$("#st_p3").val(),
					st_riwayat_demam:$("#st_riwayat_demam").val(),
					st_berkeringat_malam:$("#st_berkeringat_malam").val(),
					st_pergi_area_wabah:$("#st_pergi_area_wabah").val(),
					st_pemakaian_obat_panjang:$("#st_pemakaian_obat_panjang").val(),
					st_turun_bb_tanpa_sebab:$("#st_turun_bb_tanpa_sebab").val(),
					intruksi_keperawatan:$("#intruksi_keperawatan").val(),
					keadaan_umum_keluar:$("#keadaan_umum_keluar").val(),
					tingkat_kesadaran_keluar:$("#tingkat_kesadaran_keluar").val(),
					td_sistole_keluar:$("#td_sistole_keluar").val(),
					td_diastole_keluar:$("#td_diastole_keluar").val(),
					nafas_keluar:$("#nafas_keluar").val(),
					gds_keluar:$("#gds_keluar").val(),
					st_pulang:$("#st_pulang").val(),
					tanggal_keluar:$("#tanggal_keluar").val(),
					rencana_kontrol:$("#rencana_kontrol").val(),
					tanggal_kontrol:$("#tanggal_kontrol").val(),
					idpoli_kontrol:$("#idpoli_kontrol").val(),
					iddokter_kontrol:$("#iddokter_kontrol").val(),
					
					keluhan_utama:$("#keluhan_utama").val(),
					st_anamnesa:$("#st_anamnesa").val(),
					nama_anamnesa:$("#nama_anamnesa").val(),
					hubungan_anamnesa:$("#hubungan_anamnesa").val(),
					st_riwayat_penyakit:$("#st_riwayat_penyakit").val(),
					riwayat_penyakit_lainnya:$("#riwayat_penyakit_lainnya").val(),
					riwayat_alergi:$("#riwayat_alergi").val(),
					riwayat_pengobatan:$("#riwayat_pengobatan").val(),
					hubungan_anggota_keluarga:$("#hubungan_anggota_keluarga").val(),
					st_psikologis:$("#st_psikologis").val(),
					st_sosial_ekonomi:$("#st_sosial_ekonomi").val(),
					st_spiritual:$("#st_spiritual").val(),
					pendidikan:$("#pendidikan").val(),
					pekerjaan:$("#pekerjaan").val(),
					st_nafsu_makan:$("#st_nafsu_makan").val(),
					st_turun_bb:$("#st_turun_bb").val(),
					st_mual:$("#st_mual").val(),
					st_muntah:$("#st_muntah").val(),
					st_fungsional:$("#st_fungsional").val(),
					st_alat_bantu:$("#st_alat_bantu").val(),
					st_cacat:$("#st_cacat").val(),
					cacat_lain:$("#cacat_lain").val(),
					risiko_jatuh:$("#risiko_jatuh").val(),
					nama_tindakan:$("#nama_tindakan").val(),
					skor_pengkajian:$("#skor_pengkajian").val(),
					st_tindakan:$("#st_tindakan").val(),
					st_tindakan_action:$("#st_tindakan_action").val(),
					st_edukasi:$("#st_edukasi").val(),
					st_menerima_info:$("#st_menerima_info").val(),
					st_hambatan_edukasi:$("#st_hambatan_edukasi").val(),
					st_penerjemaah:$("#st_penerjemaah").val(),
					penerjemaah:$("#penerjemaah").val(),
					catatan_edukasi:$("#catatan_edukasi").val(),
					st_edited:$("#st_edited").val(),
					jml_edit:$("#jml_edit").val(),
				},
			success: function(data) {
				
						console.log(data);
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan Assesmen.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					if (data.status_assemen=='1'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}else{
						if (data.status_assemen=='3'){
							
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
						}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
							$("#cover-spin").show();
							location.reload();			
						}
						
					}
				}
			}
		});
		}
		}
	}
	}
	
	
	function simpan_alergi_igd(){// alert($("#keluhan_utama").val());
		let alergi_id=$("#alergi_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		let input_jenis_alergi=$("#input_jenis_alergi").val();
		let input_detail_alergi=$("#input_detail_alergi").val();
		let input_reaksi_alergi=$("#input_reaksi_alergi").val();
		if (input_detail_alergi==''){
			sweetAlert("Maaf...", "Isi Detail Alergi!", "error");
			return false;

		}
		if (input_reaksi_alergi==''){
			sweetAlert("Maaf...", "Isi Reaksi Alergi!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_alergi_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						alergi_id:alergi_id,
						pendaftaran_id:pendaftaran_id,
						assesmen_id:assesmen_id,
						input_jenis_alergi:input_jenis_alergi,
						input_detail_alergi:input_detail_alergi,
						input_reaksi_alergi:input_reaksi_alergi,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_alergi();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_alergi_igd();
					}
				}
			});
		}
		
	}
	function simpan_obat_igd(){// alert($("#keluhan_utama").val());
		let obat_id=$("#obat_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		let nama_obat=$("#nama_obat").val();
		let dosis=$("#dosis").val();
		let waktu=$("#waktu").val();
		if (nama_obat==''){
			sweetAlert("Maaf...", "Isi Nama Obat!", "error");
			return false;

		}
		if (dosis==''){
			sweetAlert("Maaf...", "Isi Dosis Obat!", "error");
			return false;

		}
		if (waktu==''){
			sweetAlert("Maaf...", "Isi Waktu Obat!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_obat_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						obat_id:obat_id,
						pendaftaran_id:pendaftaran_id,
						assesmen_id:assesmen_id,
						nama_obat:nama_obat,
						dosis:dosis,
						waktu:waktu,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_obat();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Alergi.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_obat_igd();
					}
				}
			});
		}
		
	}
	function simpan_obat_infus_igd(){
		let obat_infus_id=$("#obat_infus_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		
		let obat_id_select2=$("#obat_id_select2").val();
		let tanggalinfus=$("#tanggalinfus").val();
		let waktuinfus=$("#waktuinfus").val();
		let kuantitas_infus=$("#kuantitas_infus").val();
		let dosis_infus=$("#dosis_infus").val();
		let ppa_pemberi=$("#ppa_pemberi").val();
		let ppa_periksa=$("#ppa_periksa").val();
		let rute=$("#rute").val();
		let idtipe=$("#idtipe").val();
		
		if (obat_id_select2==''){
			sweetAlert("Maaf...", "Isi Nama Obat!", "error");
			return false;
		}
		if (tanggalinfus==''){
			sweetAlert("Maaf...", "Isi Tanggal!", "error");
			return false;
		}
		if (waktuinfus==''){
			sweetAlert("Maaf...", "Isi Waktu Obat!", "error");
			return false;
		}
		if (kuantitas_infus==''){
			sweetAlert("Maaf...", "Isi Kuantitas!", "error");
			return false;
		}
		if (dosis_infus==''){
			sweetAlert("Maaf...", "Isi Dosis!", "error");
			return false;
		}
		if (ppa_pemberi==''){
			sweetAlert("Maaf...", "Isi Pemberi!", "error");
			return false;
		}
		if (ppa_periksa==''){
			sweetAlert("Maaf...", "Isi Pemeriksa!", "error");
			return false;
		}
		if (rute==''){
			sweetAlert("Maaf...", "Isi Rute!", "error");
			return false;
		}
		
		
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_obat_infus_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						obat_infus_id:obat_infus_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						assesmen_id:assesmen_id,
						
						obat_id_select2:obat_id_select2,
						tanggalinfus:tanggalinfus,
						waktuinfus:waktuinfus,
						kuantitas_infus:kuantitas_infus,
						dosis_infus:dosis_infus,
						ppa_pemberi:ppa_pemberi,
						ppa_periksa:ppa_periksa,
						rute:rute,
						idtipe:idtipe,
					},
				success: function(data) {
					clear_input_obat_infus();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Pemberiann Obat.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_obat_infus_igd();
					}
				}
			});
		}
		
	}
	function clear_input_obat_infus(){
		$("#obat_infus_id").val('');
		$("#btn_add_obat_infus").html('<i class="fa fa-plus"></i> Add');
		$("#obat_id_select2").val('').trigger('change');
		$("#ppa_pemberi").val('').trigger('change');
		$("#ppa_periksa").val('').trigger('change');
		$("#kuantitas_infus").val('');
		$("#dosis_infus").val('');
	}
	function edit_obat_infus_igd(id){
		$("#obat_id").val(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_tindakan_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_obat_infus").html('<i class="fa fa-save"></i> Simpan');
				$("#obat_infus_id").val(data.id);
				let newOption = new Option(data.nama_obat, data.obat_id, true, true);
					
				$("#obat_id_select2").append(newOption);

				$("#idtipe").val(data.idtipe);
				$("#waktuinfus").val(data.waktu_infus);
				$("#tanggalinfus").val(data.tanggal_infus);
				$("#kuantitas_infus").val(data.kuantitas_infus);
				$("#rute").val(data.rute).trigger('change');
				$("#dosis_infus").val(data.dosis_infus);
				$("#ppa_pemberi").val(data.ppa_pemberi).trigger('change');
				$("#ppa_periksa").val(data.ppa_periksa).trigger('change');
			}
		});
	}
	function load_tindakan_assesmen_igd(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/load_tindakan_assesmen_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#index_tindakan tbody").empty();
					$("#index_tindakan tbody").append(data.tabel);
				}
			});
	}
	function simpan_tindakan_igd(){
		let tindakan_id=$("#tindakan_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		
		let mtindakan_id=$("#mtindakan_id").val();
		let tanggaltindakan=$("#tanggaltindakan").val();
		let waktutindakan=$("#waktutindakan").val();
		let waktu_mulai=$("#waktu_mulai").val();
		let waktu_selesai=$("#waktu_selesai").val();
		let ppa_pelaksana=$("#ppa_pelaksana").val();
		let mtindakan_nama=$("#mtindakan_id option:selected").text();	
		if (mtindakan_nama==''){
			sweetAlert("Maaf...", "Isi Tindakan!", "error");
			return false;
		}
		if (tanggaltindakan==''){
			sweetAlert("Maaf...", "Isi Tanggal Tindakan!", "error");
			return false;
		}
		if (waktutindakan==''){
			sweetAlert("Maaf...", "Isi Waktu Tindakan!", "error");
			return false;
		}
		if (waktu_mulai==''){
			sweetAlert("Maaf...", "Isi Waktu Mulai!", "error");
			return false;
		}
		if (waktu_selesai==''){
			sweetAlert("Maaf...", "Isi Waktu selesai!", "error");
			return false;
		}
		if (ppa_pelaksana==''){
			sweetAlert("Maaf...", "Isi Pemberi!", "error");
			return false;
		}
		
		
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_tindakan_assesmen_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						tindakan_id:tindakan_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						assesmen_id:assesmen_id,
						
						mtindakan_nama:mtindakan_nama,
						mtindakan_id:mtindakan_id,
						tanggaltindakan:tanggaltindakan,
						waktutindakan:waktutindakan,
						waktu_mulai:waktu_mulai,
						waktu_selesai:waktu_selesai,
						ppa_pelaksana:ppa_pelaksana,
					},
				success: function(data) {
					clear_input_tindakan();
							$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Pemberiann Obat.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_tindakan_assesmen_igd();
					}
				}
			});
		}
		
	}
	function clear_input_tindakan(){
		$("#tindakan_id").val('');
		$("#btn_add_tindakan").html('<i class="fa fa-plus"></i> Add');
		$("#mtindakan_id").val('').trigger('change');
		$("#ppa_pelaksana").val('').trigger('change');
		// $("#waktu_mulai").val('').trigger('change');
		$("#waktu_mulai").val('');
		$("#waktu_selesai").val('');
	}
	function edit_tindakan_assesmen_igd(id){
		$("#tindakan_id").val(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_tindakan_assesmen_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_tindakan").html('<i class="fa fa-save"></i> Simpan');
				$("#tindakan_id").val(data.id);
				let newOption = new Option(data.nama_tindakan, data.mtindakan_id, true, true);
					
				$("#mtindakan_id").append(newOption);

				$("#waktutindakan").val(data.waktutindakan);
				$("#tanggaltindakan").val(data.tanggaltindakan);
				$("#waktu_mulai").val(data.waktu_mulai);
				$("#waktu_selesai").val(data.waktu_selesai);
				$("#ppa_pelaksana").val(data.ppa_pelaksana).trigger('change');
			}
		});
	}
	function hapus_tindakan_assesmen_igd(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Tindakan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_tindakan_assesmen_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_tindakan_assesmen_igd();		
				}
			});
		});			
	}
	function clear_input_alergi(){
		$("#alergi_id").val('');
		$("#btn_add_alergi").html('<i class="fa fa-plus"></i> Add');
		$("#input_jenis_alergi").val('');
		$("#input_detail_alergi").val('');
		$("#input_reaksi_alergi").val('');
	}
	function clear_input_obat(){
		$("#obat_id").val('');
		$("#btn_add_obat").html('<i class="fa fa-plus"></i> Add');
		$("#nama_obat").val('');
		$("#dosis").val('');
		$("#waktu").val('');
	}
	function load_alergi_igd(){
		$('#alergi_2 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	function load_obat_igd(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#index_obat').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_obat').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_obat_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		
	}
	// function load_obat_infus_igd(){
		
		// let assesmen_id=$("#assesmen_id").val();
		// $('#index_obat_infus').DataTable().destroy();	
		// // $("#cover-spin").show();
		// // alert(ruangan_id);
		// table = $('#index_obat_infus').DataTable({
				// autoWidth: false,
				// searching: true,
				// serverSide: true,
				// "processing": false,
				// "order": [],
				// "pageLength": 10,
				// "ordering": false,
				// ajax: { 
					// url: '{site_url}tpendaftaran_poli_ttv/load_obat_infus_igd', 
					// type: "POST" ,
					// dataType: 'json',
					// data : {
							// assesmen_id:assesmen_id,
						   // }
				// },
				// columnDefs: [
					// {  className: "text-right", targets:[3] },
					// {  className: "text-center", targets:[8] },
					 // { "width": "5%", "targets": [1,3] },
					 // { "width": "10%", "targets": [0,4] },
					 // { "width": "20%", "targets": [2] },
					 // { "width": "10%", "targets": [5,6,7,8] }
				// ],
				// "drawCallback": function( settings ) {
					 // $("#cover-spin").hide();
					 // disabel_edit();
				 // }  
			// });
		
	// }
	
	function load_obat_infus_igd(){
		let assesmen_id=$("#assesmen_id").val();
		$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/load_obat_infus_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					$("#index_obat_infus tbody").empty();
					$("#index_obat_infus tbody").append(data.tabel);
				}
			});
	}
	function load_alergi_igd_his(){
		$('#alergi_1 > a').tab('hide');
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		$('#index_alergi_his').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi_his').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi_his', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function load_alergi_igd_pasien(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_alergi').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_alergi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_alergi_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function hapus_alergi_igd(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Alergi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_alergi_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_alergi_igd();		
				}
			});
		});			
	}
	function hapus_obat_igd(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Penggunaan Obat ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_obat_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_obat_igd();		
				}
			});
		});			
	}
	function hapus_obat_infus_igd(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Penggunaan Obat Infus ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_obat_infus_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_obat_infus_igd();		
				}
			});
		});			
	}
	
	function edit_alergi_igd(id){
		$("#alergi_id").val(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_alergi_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_alergi").html('<i class="fa fa-save"></i> Simpan');
				$("#input_jenis_alergi").val(data.input_jenis_alergi).trigger('change');
				$("#input_detail_alergi").val(data.input_detail_alergi);
				$("#input_reaksi_alergi").val(data.input_reaksi_alergi);
			}
		});
	}
	function edit_obat_igd(id){
		$("#obat_id").val(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_obat_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_obat").html('<i class="fa fa-save"></i> Simpan');
				$("#nama_obat").val(data.nama_obat);
				$("#dosis").val(data.dosis);
				$("#waktu").val(data.waktu);
			}
		});
	}
	function edit_obat_infus_igd(id){
		$("#obat_id").val(id);
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_obat_infus_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#btn_add_obat_infus").html('<i class="fa fa-save"></i> Simpan');
				$("#obat_infus_id").val(data.id);
				let newOption = new Option(data.nama_obat, data.obat_id, true, true);
					
				$("#obat_id_select2").append(newOption);

				$("#idtipe").val(data.idtipe);
				$("#waktuinfus").val(data.waktu_infus);
				$("#tanggalinfus").val(data.tanggal_infus);
				$("#kuantitas_infus").val(data.kuantitas_infus);
				$("#rute").val(data.rute).trigger('change');
				$("#dosis_infus").val(data.dosis_infus);
				$("#ppa_pemberi").val(data.ppa_pemberi).trigger('change');
				$("#ppa_periksa").val(data.ppa_periksa).trigger('change');
			}
		});
	}
	
	function simpan_edukasi_igd(){// alert($("#keluhan_utama").val());
		let assesmen_id=$("#assesmen_id").val();
		let medukasi_id=$("#medukasi_id").val();
		console.log('SIMPAN EDUKASI');
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_edukasi_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						medukasi_id:medukasi_id,
						assesmen_id:$("#assesmen_id").val(),
						
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}
				}
			});
		}
		
	}
	function simpan_riwayat_penyakit_igd(){// alert($("#keluhan_utama").val());
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let riwayat_penyakit=$("#riwayat_penyakit").val();
		console.log('SIMPAN RIWYAT');
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_riwayat_penyakit_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						riwayat_penyakit:riwayat_penyakit,
						assesmen_id:$("#assesmen_id").val(),
						pendaftaran_id:$("#pendaftaran_id").val(),
						idpasien:idpasien,
						
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					}
				}
			});
		}
		
	}
	function load_skrining_risiko_jatuh_igd(){
		let assesmen_id=$("#assesmen_id").val();
		$("#tabel_skrining tbody").empty();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/load_skrining_risiko_jatuh_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
					},
			success: function(data) {
				$("#tabel_skrining tbody").append(data.opsi);
				$(".nilai").select2();
				get_skor_pengkajian_igd();
				$("#cover-spin").hide();
			}
		});
	}
	
	$(document).on("change",".nilai",function(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#template_id").val();
		var tr=$(this).closest('tr');
		var risiko_nilai_id=tr.find(".risiko_nilai_id").val();
		var nilai_id=$(this).val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/update_nilai_risiko_jatuh_igd', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					risiko_nilai_id:risiko_nilai_id,
					nilai_id:nilai_id,
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },success: function(data) {
				  console.log(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
					$("#st_tindakan").val(data.st_tindakan);
					 $("#nama_tindakan").val(data.nama_tindakan);
					 $("#skor_pengkajian").val(data.skor);
					 $("#risiko_jatuh").val(data.ref_nilai);
				}
			});
		
	});
	function get_skor_pengkajian_igd(){
		let assesmen_id=$("#assesmen_id").val();
		let template_id=$("#template_id").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/update_nilai_risiko_jatuh_igd', 
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					template_id:template_id,
			  },
			  success: function(data) {
				 $("#st_tindakan").val(data.st_tindakan);
				 $("#nama_tindakan").val(data.nama_tindakan);
				 $("#skor_pengkajian").val(data.skor);
				 $("#risiko_jatuh").val(data.ref_nilai);
			  }
		});
		
	}
	function simpan_diagnosa_igd(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let mdiagnosa_id=$("#mdiagnosa_id").val();
		let diagnosa_id=$("#diagnosa_id").val();
		let prioritas=$("#prioritas").val();
		
		let idpasien=$("#idpasien").val();
		if (mdiagnosa_id==''){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (mdiagnosa_id==null){
			sweetAlert("Maaf...", "Isi Diagnosa!", "error");
			return false;

		}
		if (prioritas==''){
			sweetAlert("Maaf...", "Isi Prioritas!", "error");
			return false;

		}
		$("#cover-spin").show();
		if (assesmen_id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_diagnosa_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						diagnosa_id:diagnosa_id,
						pendaftaran_id:pendaftaran_id,
						mdiagnosa_id:mdiagnosa_id,
						prioritas:prioritas,
						assesmen_id:assesmen_id,
						idpasien:idpasien,
					},
				success: function(data) {
					clear_input_diagnosa();
					$("#cover-spin").hide();
					if (data==false){
						swal({
							title: "Gagal!",
							text: "Duplikat Diagnosa.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						load_diagnosa_igd();
					}
				}
			});
		}
		
	}
	function clear_input_diagnosa(){
		let next=parseFloat($("#prioritas").val())+1;
		$("#diagnosa_id").val('');
		$("#mdiagnosa_id").val('').trigger('change');
		$("#prioritas").val(next).trigger('change');
		$("#btn_add_diagnosa").html('<i class="fa fa-plus"></i> Add');
	}
	function load_diagnosa_igd(){
		
		let assesmen_id=$("#assesmen_id").val();
		$('#tabel_diagnosa').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#tabel_diagnosa').DataTable({
				autoWidth: false,
				serverSide: true,
				"searching": false,
				"processing": false,
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"order": [],
				// "pageLength": 10,
				"ordering": false,
			
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/load_diagnosa_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 disabel_edit();
				 }  
			});
		refresh_diagnosa_igd();
	}
	function edit_diagnosa_igd(id){
		$("#diagnosa_id").val(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_diagnosa_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				$("#btn_add_diagnosa").html('<i class="fa fa-save"></i> Simpan');
				$("#mdiagnosa_id").val(data.mdiagnosa_id).trigger('change');
				$("#prioritas").val(data.prioritas).trigger('change');
			}
		});
	}
	function refresh_diagnosa_igd(){
		let assesmen_id=$("#assesmen_id").val();
		
		$("#diagnosa_id_list").empty();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/refresh_diagnosa_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					
				},
			success: function(data) {
				// alert(data);
				$("#diagnosa_id_list").append(data);
				load_data_rencana_asuhan_igd();
			}
		});
	}
	function hapus_diagnosa_igd(id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Diagnosa ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_diagnosa_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Alergi'});
					load_diagnosa_igd();	
					$("#cover-spin").hide();						
				}
			});
		});			
	}
	$("#diagnosa_id_list").on("change", function(){
		load_data_rencana_asuhan_igd($(this).val());
	});
	function load_data_rencana_asuhan_igd(diagnosa_id){
		if (diagnosa_id){
			
		}else{
			diagnosa_id=$("#diagnosa_id_list").val()
		}
		let assesmen_id=$("#assesmen_id").val();
		let tabel='<tr><td>TABEL 1</td><td>TABEL 2</td>';
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/load_data_rencana_asuhan_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					diagnosa_id:diagnosa_id,
					
				},
			success: function(data) {
				tabel=data;
				$("#tabel_rencana_asuhan tbody").empty().html(tabel);
				$("#cover-spin").hide();
				disabel_edit();
				// console.log(data)
				// $("#tabel_rencana_asuhan tbody").append(data);
			}
		});
	}
	function set_rencana_asuhan(id){
		 $("#diagnosa_id_list").val(id).trigger('change');
	}
	// $(".data_asuhan").on("click", function(){
		// let id_det;
		 // check = $(this).is(":checked");
		// alert(check);
			// // if(check) {
				// // $(this).closest('tr').find(".isiField").removeAttr("disabled");
			// // } else {
				// // $(this).closest('tr').find(".isiField").attr('disabled', 'disabled');
			// // }
	// }); 
	$(document).on('click','.data_asuhan',function(){
		if (status_assemen!='2'){
			var isChecked = $(this).is(':checked');
			var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/update_data_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa_igd();		
				}
			});
		}else{
			event.preventDefault();
		}
	});
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template_igd").removeAttr('disabled');
		}else{
			$("#btn_create_with_template_igd").attr('disabled','disabled');
		}
	});
	function list_index_template_igd(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_index_template_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_assesmen_igd(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/edit_template_assesmen_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian_igd(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idpoli=$("#idpoli").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_history_pengkajian_igd', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							idpoli:idpoli,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen_igd(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		// alert(assesmen_detail_id);return false;
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_edit_assesmen_igd', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id_ranap=data.pendaftaran_id;
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tpendaftaran_poli_ttv/tindakan/"+pendaftaran_id_ranap+"/erm_rj/input_assesmen_igd'); ?>";
				}
			}
		});
	}
	function hapus_record_assesmen_igd(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_assesmen_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian_igd();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_assesmen_igd', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template_igd();
				}
			});
		});

	}
	$(document).on('change','#rencana_kontrol',function(){
		set_rencana_kontrol();
	});
	function set_rencana_kontrol(){
		let rencana_kontrol=$("#rencana_kontrol").val();
		let tanggal=$("#tanggal").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/set_rencana_kontrol', 
			dataType: "JSON",
			method: "POST",
			data : {
					tanggal:tanggal,
					rencana_kontrol:rencana_kontrol,
				   },
			success: function(data) {
				// alert(data);
				$("#tanggal_kontrol").val(data);
				simpan_assesmen();
			}
		});
	}
	$("#idasalpasien").change(function() {
		// set_asal_rujukan();
		load_faskes_1();
	});
	function load_faskes_1(){
		$("#cover-spin").show();
		var asal = $("#idasalpasien").val();
		$.ajax({
			url: '{site_url}tpoliklinik_pendaftaran/getRujukan',
			method: "POST",
			dataType: "json",
			data: {
				"asal": asal
			},
			success: function(data) {
				$("#idrujukan").empty();
				$("#idrujukan").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#idrujukan").append("<option value='" + data[i].id + "' >" + data[i].nama + "</option>");
				}
				$("#idrujukan").selectpicker("refresh");
				$("#cover-spin").hide();
			}
		});
	}
	function show_modal_obat(){
		$("#obat_non_racikan-modal").modal('show');
		$("#tipeid,#idkat").select2({
			dropdownParent: $("#obat_non_racikan-modal")
		  });
	}
	$("#btn_filter_barang").click(function() {
		loadBarang();
	});
	
	function loadBarang() {
		var idunit = '1';
		var idtipe = $("#tipeid").val();
		var idkategori = $("#idkat").val();
		$('#table-Obat').DataTable().destroy();
		var table = $('#table-Obat').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}Tpendaftaran_poli_ttv/get_obat_rujukan/',
				type: "POST",
				dataType: 'json',
				data: {
					idunit: idunit,
					idtipe: idtipe,
					idkategori: idkategori,
				}
			},
			columnDefs: [{
					"width": "5%",
					"targets": [0],
					"orderable": true
				},
				{
					"width": "10%",
					"targets": [1, 3],
					"orderable": true
				},
				// {
					// "width": "15%",
					// "targets": [5],
					// "orderable": true
				// },
				{
					"width": "40%",
					"targets": [2],
					"orderable": true
				},
			]
		});
	}
	$("#tipeid").change(function() {
		load_kategori();
	});
	
	function load_kategori() {
		var idtipe = $("#tipeid").val();
		$('#idkat').find('option').remove();
		$('#idkat').append('<option value="#" selected>- Silahkan Pilih -</option>');
		if (idtipe != '#') {

			$.ajax({
				url: '{site_url}tstockopname/list_kategori/' + idtipe,
				dataType: "json",
				success: function(data) {
					$.each(data, function(i, row) {

						$('#idkat')
							.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
					});
				}
			});

		}
	}
	function TreeView($level, $name) {
		// console.log($name);
		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			if ($i > 0) {
				$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$indent += '';
			}
		}

		if ($i > 0) {
			$indent += '└─';
		} else {
			$indent += '';
		}

		return $indent + ' ' + $name;
	}
	function pilih_obat(obat_id,tipe_id,nama_obat){
		// $("#obat_infus_id").val(data.id);
		let newOption = new Option(nama_obat, obat_id, true, true);
			
		$("#obat_id_select2").append(newOption);

		$("#idtipe").val(tipe_id);
		$("obat_non_racikan-modal").modal('hide');
	}
	$(document).on("click", ".selectObat", function() {
		
		let idobat = ($(this).data('idobat'));
		let idtipe = ($(this).data('idtipe'));
		// alert(idtipe);
		$("#idtipe").val(idtipe);
		$.ajax({
			url: '{site_url}tpasien_penjualan/get_obat_detail/' + idobat + '/5/' + idtipe,
			dataType: "json",
			success: function(data) {
				

				var isi = {
					id: idobat,
					text: data.nama,
					idtipe: data.idtipe
				};
				// alert(isi.idtipe);
				var newOption = new Option(isi.text, isi.id, true, true);
				$("#obat_id_select2").append(newOption);
				$("obat_non_racikan-modal").modal('hide');
			}

		});

	});
</script>