<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.auto_blur{
		background-color:#fdffe2;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
</style>
<style>
	//.kbw-signature { width: 100%;}
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_paraf canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_dokter canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_kel canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_pernyataan canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_saksi_kel canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_saksi_rs canvas{
		width: 100% !important;
		height: auto;
	}
</style>

<?if ($menu_kiri=='input_ic' || $menu_kiri=='input_ic_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_ic' || $menu_kiri=='input_ic_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_pernyataan;
					if ($assesmen_id){
						$tanggalpernyataan=HumanDateShort($tanggal_pernyataan);
						$waktupernyataan=HumanTime($tanggal_pernyataan);
						
						$tanggalinfo=HumanDateShort($tanggal_informasi);
						$waktuinfo=HumanTime($tanggal_input);
						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktupernyataan=date('H:i:s');
						$waktuinfo=date('H:i:s');
						$tanggalinfo=date('d-m-Y');
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggalpernyataan=date('d-m-Y');
						
					}
					
					$disabel_input='';
					if ($st_input_ic=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_per">
	<?if ($st_lihat_ic=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_ranap" value="<?=$st_ranap?>" >		
						<input type="hidden" id="pendaftaran_id_trx" value="<?=($st_ranap=='1'?$pendaftaran_id_ranap:$pendaftaran_id)?>" >		
						<input type="hidden" id="st_sedang_edit" value="" >		
						<div class="col-md-12">
							<?if($st_lihat_ic=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_ic=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_assesmen" onclick="close_assesmen()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
												<a href="{site_url}tpendaftaran_ranap_erm/cetak_tindakan_ic/<?=$assesmen_id?>" target="_blank" class="btn btn-info" type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_ic" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Pengkajian</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="dokter_pelaksana">{dokter_pelaksan_ina} / <i class="text-muted">{dokter_pelaksan_eng}</i></label>
										<select tabindex="8" id="dokter_pelaksana" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($dokter_pelaksana == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach($list_dokter_pelaksana as $row){?>
											<option value="<?=$row->id?>" <?=($st_setting_dokter == 1 ? 'selected="selected"' : '')?> <?=($dokter_pelaksana == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
									<label for="example-input-normal">{tanggal_tindakan_ina} / <i class="text-muted">{tanggal_tindakan_eng}</i></label>
										<div class="input-group date">
											<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tanggal" data-date-format="dd/mm/yyyy" id="tanggalinfo" placeholder="HH/BB/TTTT" name="tanggalinfo" value="<?= $tanggalinfo ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-6 ">
										<label for="example-input-normal">{waktu_tindakan_ina} / <i class="text-muted">{waktu_tindakan_eng}</i></label>
										<div class="input-group">
											<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tanggal" id="waktuinfo" value="<?= $waktuinfo ?>" required>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
									
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="pemberi_info"> {nama_pemberi_info_ina} / <i class="text-muted">{nama_pemberi_info_eng}</i> </label>
										<select tabindex="8" id="pemberi_info" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($pemberi_info == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach($list_pemberi as $row){?>
											<option value="<?=$row->id?>" <?=($st_setting_pemberi_info == 1 ? 'selected="selected"' : '')?> <?=($pemberi_info == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="pendamping">{nama_petugas_pendamping_ina} / <i class="text-muted">{nama_petugas_pendamping_eng}</i></label>
										<select tabindex="8" id="pendamping" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($pendamping == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach($list_pendamping as $row){?>
											<option value="<?=$row->id?>" <?=($st_setting_petugas_pendamping == 1 ? 'selected="selected"' : '')?> <?=($pendamping == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {nama_penerima_info_ina} / <i class="text-muted">{nama_penerima_info_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control auto_blur"   id="penerima_info" placeholder="{nama_penerima_info_ina}" name="penerima_info" value="{penerima_info}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="hubungan_id">{hubungan_ina} / <i class="text-muted">{hubungan_eng}</i></label>
										<select tabindex="8" id="hubungan_id" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($hubungan_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(80) as $row){?>
											<option value="<?=$row->id?>" <?=($hubungan_id == $row->id ? 'selected="selected"' : '')?> <?=($hubungan_id == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
											<label for="st_setting_pemberi_info"> PILIH MATERI TINDAKAN </label>
										<div class="input-group">
											<select  id="template_tindakan" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="0" <?=($template_tindakan == '0' ? 'selected="selected"' : '')?>>Pilih Template Materi Tindakan</option>
												<?foreach(get_all('mtemplate_tindakan_ic',array('staktif'=>1)) as $row){?>
												<option value="<?=$row->id?>" <?=($template_tindakan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
												<?}?>
												
											</select>
											<span class="input-group-btn">
												<button class="btn btn-success" type="button" id="btn_create_with_template" <?=($st_input_assesmen!='1'?'disabled':'')?> onclick="get_template()"><i class="fa fa-level-down"></i> Terapkan</button>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group push-5-t">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
									
											<table class="table table-bordered" id="tabel_informasi">
												<thead>
													<tr>
														<th width="5%" class="text-center">No</th>
														<th width="25%" class="text-center">{jenis_info_ina} / <i class="text-muted">{jenis_info_eng}</i></th>
														<th width="50%" class="text-center">{isi_info_ina} / <i class="text-muted">{isi_info_eng}</i></th>
														<th width="20%" class="text-center">{paraf_ina} / <i class="text-muted">{paraf_eng}</i></th>
													</tr>
												</thead>
												<tbody>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label class="text-primary">PERNYATAAN</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label> {info_pernyataan_ina} <br> <i class="text-muted">{info_pernyataan_eng}</i> </label>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {nama_pembuat_pernyataan_ina} / <i class="text-muted">{nama_pembuat_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control auto_blur"   id="nama" placeholder="{nama}" name="nama" value="{nama}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="umur">{umur_pembuat_pernyataan_ina} / <i class="text-muted">{umur_pembuat_pernyataan_eng}</i></label>
										<div class="input-group">
											<input class="form-control decimal auto_blur"   type="text" id="umur" value="{umur}"  placeholder="{umur}" required>
											<span class="input-group-addon">umur</span>
										</div>
									</div>
									<div class="col-md-6 ">
										<label for="jenis_kelamin">{jk_pembuat_pernyataan_ina} / <i class="text-muted">{jk_pembuat_pernyataan_eng}</i></label>

										<select tabindex="8" id="jenis_kelamin" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($jenis_kelamin == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(77) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?> <?=($jenis_kelamin == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {alamat_pembuat_pernyataan_ina} / <i class="text-muted">{alamat_pembuat_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control auto_blur"   id="alamat" placeholder="{alamat}" name="alamat" value="{alamat}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {nik_pembuat_pernyataan_eng} / <i class="text-muted">{nik_pembuat_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control auto_blur"   id="no_ktp" placeholder="{no_ktp}" name="no_ktp" value="{no_ktp}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {dengan_ini_memberikan_ina} / <i class="text-muted">{dengan_ini_memberikan_eng}</i> </label>
										<select tabindex="8" id="hereby" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($hereby == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(get_all('mjenis_persetujuan_ic',array('staktif'=>'1')) as $row){?>
											<option value="<?=$row->id?>" <?=($hereby == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {untuk_tindakan_ina} / <i class="text-muted">{untuk_tindakan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control auto_blur"   id="untuk_tindakan" placeholder="{untuk_tindakan}" name="untuk_tindakan" value="{untuk_tindakan}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {terhadap_ina} / <i class="text-muted">{terhadap_eng}</i> </label>
										<select tabindex="8" id="terhadap" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($terhadap == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(79) as $row){?>
											<option value="<?=$row->id?>" <?=($terhadap == $row->id ? 'selected="selected"' : '')?> <?=($terhadap == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {nama_pasien_ina} / <i class="text-muted">{nama_pasien_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control auto_blur"   id="nama_pasien" placeholder="{nama_pasien}" name="nama_pasien" value="{nama_pasien}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {tanggal_lahir_ina} / <i class="text-muted">{tanggal_lahir_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control auto_blur"   id="tanggal_lahir_pasien" placeholder="{tanggal_lahir_pasien}" name="tanggal_lahir_pasien" value="<?=HumanDateShort($tanggal_lahir_pasien)?>" required>
									</div>
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {usia_ina} / <i class="text-muted">{usia_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control auto_blur"   id="umur_pasien" placeholder="{umur_pasien}" name="umur_pasien" value="{umur_pasien}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {jenis_kelamin_ina} / <i class="text-muted">{jenis_kelamin_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control auto_blur"   id="jenis_kelamin_pasien" placeholder="{jenis_kelamin_pasien}" name="jenis_kelamin_pasien" value="{jenis_kelamin_pasien}" required>
									</div>
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {norek_ina} / <i class="text-muted">{norek_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control auto_blur"   id="no_medrec" placeholder="{no_medrec}" name="no_medrec" value="{no_medrec}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label id="ket_pernyataan_ina">  </label>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-8 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {waktu_pernyataan_ina} / <i class="text-muted">{waktu_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control auto_blur"   id="lokasi_pernyataan_ina" placeholder="{lokasi_pernyataan_ina}" name="lokasi_pernyataan_ina" value="{lokasi_pernyataan_ina}" required>
									</div>
									
									<div class="col-md-3 ">
										<label for="example-input-normal">&nbsp;</label>
										<div class="input-group date">
											<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tanggal" data-date-format="dd/mm/yyyy" id="tanggalpernyataan" placeholder="HH/BB/TTTT" value="<?= $tanggalpernyataan ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-3 ">
										<label for="example-input-normal">&nbsp;</label>
										<div class="input-group">
											<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tanggal" id="waktupernyataan" value="<?= $waktupernyataan ?>" required>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-8 ">
										
										<table class="table table-bordered">
											<tr>
												<td style="width:30%" class="text-center"><strong>{ket_gambar_paraf_menyatakan_ina} <br> <i class="text-muted">{ket_gambar_paraf_menyatakan_eng}</strong></td>
												<td style="width:30%" class="text-center"><strong>{ket_gambar_paraf_saksi_ina} <br> <i class="text-muted">{ket_gambar_paraf_saksi_eng}</strong></td>
												<td style="width:30%" class="text-center"><strong>{ket_gambar_paraf_saksi_rs_ina} <br> <i class="text-muted">{ket_gambar_paraf_saksi_rs_eng}</strong></td>
											</tr>
											<tr>
												<td style="width:30%" class="text-center">
													<?if ($ttd_menyatakan){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$ttd_menyatakan?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_pernyataan()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
												</td>
												<td style="width:30%" class="text-center">
													<?if ($ttd_saksi){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$ttd_saksi?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_saksi_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_saksi_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_saksi_kel()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
												</td>
												<td style="width:30%" class="text-center">
													<?if ($ttd_saksi_rs){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$ttd_saksi_rs?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_saksi_rs()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_saksi_rs()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_saksi_rs()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
												</td>
											</tr>
											<tr>
												<td style="width:30%" class="text-center"><label id="nama_pemberi_pernyataan"><?=$yang_menyatakan?></label></td>
												<td style="width:30%" class="text-center"><label id="nama_saksi_keluarga"><?=$saksi_keluarga?></label></td>
												<td style="width:30%" class="text-center"><label id="nama_saksi_rs"><?=$saksi_rs?></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="notransaksi">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tgl_daftar">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="mppa_id">Dokter Pelaksana</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('tipepegawai'=>'2','staktif'=>'1')) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="mppa_id">Status Persetujuan</label>
										<div class="col-md-8">
											<select tabindex="8" id="st_verifikasi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="#" selected>Pilih Opsi</option>
												<?foreach(list_variable_ref(78) as $row){?>
												<option value="<?=$row->id?>" ><?=$row->nama?></option>
												<?}?>
												
											</select>
											
										</div>
									</div>
									
								</div>
								
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idpoli">Pemberi Informasi</label>
										<div class="col-md-8">
											<select id="profesi_id_filter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>'1')) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="iddokter">Owned By Me</label>
										<div class="col-md-8">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>ALL</option>
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal_input_1">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												
												<th width="100%">Action</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_ic!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="20%">Action</th>
												<th width="70%">Template</th>
												<th width="10%">Jumlah</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
</div>
<?}?>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="jenis_ttd" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($jenis_ttd==''?'selected':'')?>>Silahkan Pilih</option>
										<option value="1" <?=($jenis_ttd=='1'?'selected':'')?>>Pasien Sendiri Sendiri</option>
										<option value="2" <?=($jenis_ttd=='2'?'selected':'')?> >Keluarga Terdekat / Wali</option>
										<option value="3" <?=($jenis_ttd=='3'?'selected':'')?> >Penanggung Jawab</option>
										
									</select>
									<label for="jenis_ttd">Yang Tandatangan</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="ttd_nama" placeholder="Nama" value="{ttd_nama}">
									<label for="jenis_ttd">Nama</label>
								</div>
							</div>
							<div class="col-md-6 push-10">
								<div class="form-material">
									<select tabindex="30" id="ttd_hubungan" name="ttd_hubungan" class="form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($ttd_hubungan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<label for="ttd_hubungan">Hubungan</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<textarea id="signature64" name="signed_2" style="display: none"><?=$ttd?></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd()"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_paraf" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Paraf Pasien</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ket_gambar_paraf_penerima_ina} / <i class="text-muted">{ket_gambar_paraf_penerima_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_paraf" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{ket_nama_paraf_penerima_ina} / <i class="text-muted">{ket_nama_paraf_penerima_eng}</i></label>
										<input tabindex="32" type="text" class="form-control " readonly  id="penerima_info_paraf" placeholder="{nama_penerima_info_ina}" name="penerima_info" value="{penerima_info}" required>
									</div>
								</div>
								<input type="hidden" readonly id="informasi_id" value="">
								<textarea id="signature64_paraf" name="signed_paraf" style="display: none"></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_paraf()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_paraf()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_dokter" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Dokter</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ket_gambar_ttd_dokter_ina} / <i class="text-muted">{ket_gambar_ttd_dokter_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_dokter" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{nama_ttd_dokter_ina} / <i class="text-muted">{nama_ttd_dokter_eng}</i></label>
										<input tabindex="32" type="text" class="form-control " readonly  id="nama_dokter_pelaksana">
									</div>
								</div>
								
								<textarea id="signature64_ttd_dokter" name="signed_ttd_dokter" style="display: none"><?=$ttd_dokter_pelaksana?></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_dokter()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_dokter()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_kel" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penerma Informasi</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ket_gambar_ttd_pasien_ina} / <i class="text-muted">{ket_gambar_ttd_pasien_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_kel" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{nama_ttd_kel_ina} / <i class="text-muted">{nama_ttd_kel_eng}</i></label>
										<input tabindex="32" type="text" class="form-control " readonly  id="nama_kel_penerima_info" value="{penerima_info}">
									</div>
								</div>
								
								<textarea id="signature64_ttd_kel" name="signed_ttd_kel" style="display: none"><?=$ttd_penerima_info?></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_kel()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_kel()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_pernyataan" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Yang Menyatakan</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ket_gambar_paraf_menyatakan_ina} / <i class="text-muted">{ket_gambar_paraf_menyatakan_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_pernyataan" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{ket_nama_paraf_menyatakan_ina} / <i class="text-muted">{ket_nama_paraf_menyatakan_eng}</i></label>
										<input tabindex="32" type="text" class="form-control " readonly  id="yang_menyatakan" value="{yang_menyatakan}">
									</div>
								</div>
								
								<textarea id="signature64_ttd_pernyataan" name="signed_ttd_pernyataan" style="display: none"><?=$ttd_menyatakan?></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_pernyataan()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_pernyataan()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_saksi_kel" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Saksi Keluarga</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ket_gambar_paraf_saksi_ina} / <i class="text-muted">{ket_gambar_paraf_saksi_eng}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_saksi_kel" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{ket_nama_paraf_saksi_ina} / <i class="text-muted">{ket_nama_paraf_saksi_eng}</i></label>
										<input tabindex="32" type="text" class="form-control "   id="saksi_keluarga" value="{saksi_keluarga}">
									</div>
								</div>
								
								<textarea id="signature64_ttd_saksi_kel" name="signed_ttd_saksi_kel" style="display: none"><?=$ttd_saksi?></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_saksi_kel()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_saksi_kel()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd_saksi_rs" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Saksi RS</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 ">
										<label for="example-input-normal">{ket_gambar_paraf_saksi_rs_ina} / <i class="text-muted">{ket_gambar_paraf_saksi_rs_ina}</i></label>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_ttd_saksi_rs" ></div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label for="example-input-normal">{ket_nama_paraf_saksi_rs_ina} / <i class="text-muted">{ket_nama_paraf_saksi_rs_ina}</i></label>
										<input tabindex="32" type="text" class="form-control " id="saksi_rs" value="{saksi_rs}">
									</div>
								</div>
								
								<textarea id="signature64_ttd_saksi_rs" name="signed_ttd_saksi_rs" style="display: none"><?=$ttd_saksi_rs?></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="clear_ttd_saksi_rs()"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_ttd_saksi_rs()" ><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig_ttd_saksi_rs = $('#sig_ttd_saksi_rs').signature({syncField: '#signature64_ttd_saksi_rs', syncFormat: 'PNG'});
var sig_ttd_saksi_kel = $('#sig_ttd_saksi_kel').signature({syncField: '#signature64_ttd_saksi_kel', syncFormat: 'PNG'});
var sig_ttd_pernyataan = $('#sig_ttd_pernyataan').signature({syncField: '#signature64_ttd_pernyataan', syncFormat: 'PNG'});
var sig_ttd_kel = $('#sig_ttd_kel').signature({syncField: '#signature64_ttd_kel', syncFormat: 'PNG'});
var sig_ttd_dokter = $('#sig_ttd_dokter').signature({syncField: '#signature64_ttd_dokter', syncFormat: 'PNG'});
var sig_paraf = $('#sig_paraf').signature({syncField: '#signature64_paraf', syncFormat: 'PNG'});
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
function modal_ttd_dokter(){
	let nama_dokter_pelaksana=$("#dokter_pelaksana option:selected").text();	
	$("#nama_dokter_pelaksana").val(nama_dokter_pelaksana);
	let ttd=$("#signature64_ttd_dokter").val();
	$("#modal_ttd_dokter").modal('show');
	$('#sig_ttd_dokter').signature('enable').signature('draw', $("#signature64_ttd_dokter").val());
}
function clear_ttd_dokter(){
	sig_ttd_dokter.signature('clear');
	$("#signature64_ttd_dokter").val('');
}
function simpan_ttd_dokter(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64_ttd_dokter").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_dokter_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				load_index_informasi();
				$("#modal_ttd_dokter").modal('hide');
			}
		});
}
function hapus_ttd_dokter(){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64_ttd_dokter").val('')
	var signature64=$("#signature64_ttd_dokter").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_dokter_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				load_index_informasi();
				$("#modal_ttd_dokter").modal('hide');
			}
		});
}
function modal_ttd_kel(){
	let ttd=$("#signature64_ttd_kel").val();
	$("#modal_ttd_kel").modal('show');
	$('#sig_ttd_kel').signature('enable').signature('draw', $("#signature64_ttd_kel").val());
}
function clear_ttd_kel(){
	sig_ttd_kel.signature('clear');
	$("#signature64_ttd_kel").val('');
}
function simpan_ttd_kel(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64_ttd_kel").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_kel_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				load_index_informasi();
				$("#modal_ttd_kel").modal('hide');
			}
		});
}
function hapus_ttd_kel(){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64_ttd_kel").val('')
	var signature64=$("#signature64_ttd_kel").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_kel_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				load_index_informasi();
				$("#modal_ttd_kel").modal('hide');
			}
		});
}

function modal_ttd_pernyataan(){
	let ttd=$("#signature64_ttd_pernyataan").val();
	$("#modal_ttd_pernyataan").modal('show');
	$('#sig_ttd_pernyataan').signature('enable').signature('draw', $("#signature64_ttd_pernyataan").val());
}
function clear_ttd_pernyataan(){
	sig_ttd_pernyataan.signature('clear');
	$("#signature64_ttd_pernyataan").val('');
}
function simpan_ttd_pernyataan(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64_ttd_pernyataan").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_pernyataan_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  location.reload();
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
function hapus_ttd_pernyataan(){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64_ttd_pernyataan").val('')
	var signature64=$("#signature64_ttd_pernyataan").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_pernyataan_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				location.reload();
			}
		});
}
function modal_ttd_saksi_kel(){
	let ttd=$("#signature64_ttd_saksi_kel").val();
	$("#modal_ttd_saksi_kel").modal('show');
	$('#sig_ttd_saksi_kel').signature('enable').signature('draw', $("#signature64_ttd_saksi_kel").val());
}
function clear_ttd_saksi_kel(){
	sig_ttd_saksi_kel.signature('clear');
	$("#signature64_ttd_saksi_kel").val('');
}
function simpan_ttd_saksi_kel(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64_ttd_saksi_kel").val();
	var saksi_keluarga=$("#saksi_keluarga").val();
	if (saksi_keluarga==''){
		sweetAlert("Maaf...", "Tentukan Nama Saksi Keluarga", "error");
		return false;
	}

	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_saksi_kel_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				saksi_keluarga:saksi_keluarga,
		  },success: function(data) {
			  $("#modal_ttd_saksi_kel").modal('hide');
			  location.reload();
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
function hapus_ttd_saksi_kel(){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64_ttd_saksi_kel").val('')
	var signature64=$("#signature64_ttd_saksi_kel").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_saksi_kel_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				location.reload();
			}
		});
}

function modal_ttd_saksi_rs(){
	let ttd=$("#signature64_ttd_saksi_rs").val();
	$("#modal_ttd_saksi_rs").modal('show');
	$('#sig_ttd_saksi_rs').signature('enable').signature('draw', $("#signature64_ttd_saksi_rs").val());
}
function clear_ttd_saksi_rs(){
	sig_ttd_saksi_rs.signature('clear');
	$("#signature64_ttd_saksi_rs").val('');
}
function simpan_ttd_saksi_rs(){
	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64_ttd_saksi_rs").val();
	var saksi_rs=$("#saksi_rs").val();
	if (saksi_rs==''){
		sweetAlert("Maaf...", "Tentukan Nama Saksi Keluarga", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_saksi_rs_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
				saksi_rs:saksi_rs,
		  },success: function(data) {
			  $("#modal_ttd_saksi_rs").modal('hide');
			  location.reload();
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
			  $("#cover-spin").show();
			}
		});
}
function hapus_ttd_saksi_rs(){
	var assesmen_id=$("#assesmen_id").val();
	$("#signature64_ttd_saksi_rs").val('')
	var signature64=$("#signature64_ttd_saksi_rs").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_saksi_rs_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				location.reload();
			}
		});
}

$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function clear_paraf(){
	// e.preventDefault();
	sig_paraf.signature('clear');
	$("#signature64_paraf").val('');
}

function create_awal_template(){
	$("#modal_default_template").modal('show');
	$("#mnyeri_id_2").select2({
		dropdownParent: $("#modal_default_template")
	  });
}
function load_index_informasi(){
	$("#cover-spin").show();
	var assesmen_id=$("#assesmen_id").val();
	var status_assemen=$("#status_assemen").val();
	var ttd_dokter_pelaksana=$("#signature64_ttd_dokter").val();
	var ttd_penerima_info=$("#signature64_ttd_kel").val();
	// alert(id);
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/load_index_informasi_ic/',
		dataType: "json",
		type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				ttd_dokter_pelaksana:ttd_dokter_pelaksana,
				ttd_penerima_info:ttd_penerima_info,
				
		  },
		success: function(data) {
			$("#tabel_informasi tbody").empty();
			$("#tabel_informasi tbody").append(data.tabel);
			$("#cover-spin").hide();
			disabel_edit();
			$('.auto_blur_tabel').summernote({
				height: 100,
			    codemirror: { // codemirror options
					theme: 'monokai'
				  },	
			  callbacks: {
				onBlur: function(contents, $editable) {
						assesmen_id=$("#assesmen_id").val();
						var tr=$(this).closest('tr');
						var informasi_id=tr.find(".informasi_id").val();
						var nourut=tr.find(".nourut").val();
						var isi=$(this).val();
						var isi_html=$(this).val();
						if (nourut=='3'){
							const strippedString = isi_html.replace(/(<([^>]+)>)/gi, "");
							$("#untuk_tindakan").val(strippedString);
						}
						$.ajax({
							url: '{site_url}Tpendaftaran_poli_ttv/update_isi_informasi_ic/',
							dataType: "json",
							type: 'POST',
							data: {
							informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
							},success: function(data) {

							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
							}
						});
				}
			  }
			});
				
		}
	});
}
$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	
	disabel_edit();
	// set_ttd_assesmen();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	// $("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	
		if (assesmen_id){
			load_index_informasi();
			set_persetujuan();
		}
	load_awal_assesmen=false;
	

});

function modal_faraf($app_id){
	$("#informasi_id").val($app_id);
	$("#modal_paraf").modal('show');
	var sig = $('#sig_paraf').signature({syncField: '#signature64_paraf', syncFormat: 'PNG'});
	
}
function set_img_canvas(){
	
	var src =$("#signature64").val();

	$('#sig').signature('enable'). signature('draw', src);
}
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
function simpan_ttd(){
	if ($("#jenis_ttd").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Jenis Tandatangan", "error");
		return false;
	}
	if ($("#ttd_nama").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Tandatangan", "error");
		return false;
	}
	if ($("#jenis_ttd").val()!=''){
		if ($("#ttd_hubungan").val()==''){
			sweetAlert("Maaf...", "Tentukan Hubungan", "error");
			return false;
		}
	}

	var assesmen_id=$("#assesmen_id").val();
	var signature64=$("#signature64").val();
	var jenis_ttd=$("#jenis_ttd").val();
	var ttd_nama=$("#ttd_nama").val();
	var ttd_hubungan=$("#ttd_hubungan").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_ttd_asmed/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				jenis_ttd:jenis_ttd,
				ttd_nama:ttd_nama,
				ttd_hubungan:ttd_hubungan,
				signature64:signature64,
		  },success: function(data) {
				location.reload();
			}
		});
}
function simpan_paraf(){
	

	var informasi_id=$("#informasi_id").val();
	var signature64=$("#signature64_paraf").val();
	
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/simpan_paraf_info_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				informasi_id:informasi_id,
				signature64:signature64,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Paraf'});
			  $("#cover-spin").hide();
				load_index_informasi();
				$("#modal_paraf").modal('hide');
			}
		});
}
function hapus_paraf(id){
	$("#informasi_id").val(id);
	$("#signature64_paraf").val('');
	var informasi_id=$("#informasi_id").val();
	var signature64=$("#signature64_paraf").val();
	$.ajax({
	  url: '{site_url}Tpendaftaran_poli_ttv/simpan_paraf_info_ic/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			informasi_id:informasi_id,
			signature64:signature64,
	  },success: function(data) {
		  $.toaster({priority : 'success', title : 'Hapus!', message : ' Paraf'});
		  $("#cover-spin").hide();
			load_index_informasi();
		}
	});
}
function set_ttd_assesmen(){
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
	  load_ttd();
}
function load_ttd(){
	assesmen_id=$("#assesmen_id").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/load_ttd_asmed', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
			   },
		success: function(data) {
			$("#jenis_ttd").val(data.jenis_ttd).trigger('change');
			$("#ttd_nama").val(data.ttd_nama);
			$("#ttd_hubungan").val(data.ttd_hubungan).trigger('change');
			$("#signature64").val(data.ttd);
			var src =data.ttd;

			$('#sig').signature('enable'). signature('draw', src);
		}
	});
}
function set_ttd_assesmen_manual(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	load_ttd();
	$("#modal_ttd").modal('show');
	$("#jenis_ttd,#ttd_hubungan").select2({
		dropdownParent: $("#modal_ttd")
	  });
}
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 // $(".btn_rencana").removeAttr('disabled');
		 // $(".btn_ttd").removeAttr('disabled');
	}
}
function simpan_template(){
	$("#modal_savae_template_assesmen").modal('show');
	
}
function get_template(){
	var assesmen_id=$("#assesmen_id").val();
	var template_tindakan=$("#template_tindakan").val();
	if ($("#template_tindakan").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Materi Tindakan", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpendaftaran_poli_ttv/get_template_tindakan_ic/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,
				template_tindakan:template_tindakan,
		  },success: function(data) {
			  $.toaster({priority : 'success', title : 'Succes!', message : ' Template Tindakan'});
			  $("#cover-spin").hide();
				load_index_informasi();
		}
	});
}
function create_assesmen(){
	
	$("#modal_default").modal('hide');
	let pendaftaran_id=$("#pendaftaran_id_trx").val();
	let st_ranap=$("#st_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_ic', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_assesmen(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	// let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id_trx").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_ic', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id_trx").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_ic', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id=$("#pendaftaran_id_trx").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let idpasien=$("#idpasien").val();
	let st_ranap=$("#st_ranap").val();
	// alert(idpasien);
	// return false;
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_template_ic', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
					st_ranap:st_ranap,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
$(".opsi_change").change(function(){
	// console.log('OPSI CHANTE')
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});
$("#hereby").change(function(){
	set_persetujuan();
});
function set_persetujuan(){
	
	let assesmen_id=$("#assesmen_id").val();
	let hereby=$("#hereby").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/get_persetujaun_ic', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				hereby:hereby,
			
			   },
		success: function(data) {
			$("#ket_pernyataan_ina").html(data);
			// // $("#cover-spin").hide();
			
			// $.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
			// location.reload();
		}
	});
}
$(".auto_blur_tanggal").change(function(){
	console.log($("#waktupernyataan").val())
	if ($("#st_edited").val()=='0'){
		simpan_assesmen();
	}
});
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_ic', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_template_ic', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
 $(document).find('.js-summernote').on('summernote.blur', function() {
   if ($("#st_edited").val()=='0'){
	 
	   
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
  });

  

$(".auto_blur").blur(function(){
	// alert('sini');
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_assesmen();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
$("#penerima_info").blur(function(){
	$("#penerima_info_paraf").val($("#penerima_info").val());
	$("#nama_kel_penerima_info").val($("#penerima_info").val());
	
});
$("#nama").blur(function(){
	$("#nama_pemberi_pernyataan").html($("#nama").val());
	$("#yang_menyatakan").val($("#nama").val());
	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_assesmen(){
	if ($("#dokter_pelaksana").val()==''){
		sweetAlert("Maaf...", "Tentukan Dokter Pelaksana", "error");
		return false;
	}
	if ($("#pemberi_info").val()==''){
		sweetAlert("Maaf...", "Tentukan Pemberi Info", "error");
		return false;
	}
	if ($("#pendamping").val()==''){
		sweetAlert("Maaf...", "Tentukan Pendamping", "error");
		return false;
	}
	if ($("#penerima_info").val()==''){
		sweetAlert("Maaf...", "Tentukan Penerima Informasi", "error");
		return false;
	}
	if ($("#hubungan_id").val()==''){
		sweetAlert("Maaf...", "Tentukan Hubungan Dengan Pasien", "error");
		return false;
	}
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			
			simpan_assesmen();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}
	$("#modal_savae_template_assesmen").modal('hide');
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_assesmen();
		});		
	
}
function simpan_assesmen(){
	if (load_awal_assesmen==false){
		if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let tanggalinformasi=$("#tanggalinfo").val();
		let waktuinformasi=$("#waktuinfo").val();
		let tanggalpernyataan=$("#tanggalpernyataan").val();
		let waktupernyataan=$("#waktupernyataan").val();
		
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/save_ic', 
				dataType: "JSON",
				method: "POST",
				data : {
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						dokter_pelaksana : $("#dokter_pelaksana").val(),
						waktuinformasi : waktuinformasi,
						tanggalinformasi : tanggalinformasi,
						pemberi_info : $("#pemberi_info").val(),
						pendamping : $("#pendamping").val(),
						penerima_info : $("#penerima_info").val(),
						hubungan_id : $("#hubungan_id").val(),
						nama : $("#nama").val(),
						umur : $("#umur").val(),
						jenis_kelamin : $("#jenis_kelamin").val(),
						alamat : $("#alamat").val(),
						no_ktp : $("#no_ktp").val(),
						hereby : $("#hereby").val(),
						untuk_tindakan : $("#untuk_tindakan").val(),
						terhadap : $("#terhadap").val(),
						nama_pasien : $("#nama_pasien").val(),
						tanggal_lahir_pasien : $("#tanggal_lahir_pasien").val(),
						umur_pasien : $("#umur_pasien").val(),
						jenis_kelamin_pasien : $("#jenis_kelamin_pasien").val(),
						no_medrec : $("#no_medrec").val(),
						tanggalpernyataan : tanggalpernyataan,
						waktupernyataan : waktupernyataan,
						yang_menyatakan : $("#yang_menyatakan").val(),
						template_tindakan : $("#template_tindakan").val(),
						// saksi_rs : $("#saksi_rs").val(),
						// saksi_keluarga : $("#saksi_keluarga").val(),

						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
		}
	}
}
	
	
	
	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "20%", "targets": [1] },
						 // { "width": "15%", "targets": [3] },
						 // { "width": "60%", "targets": [2] }
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_index_template_ic', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 // $("#index_template_assemen thead").remove();
				 }  
			});
		
	}
	
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id_trx").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/edit_template_ic', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let profesi_id_filter=$("#profesi_id_filter").val();
		let st_verifikasi=$("#st_verifikasi").val();
		let st_owned=$("#st_owned").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				// columnDefs: [
						// {  className: "text-right", targets:[0] },
						// {  className: "text-center", targets:[1,2,3,5,6,7] },
						 // { "width": "5%", "targets": [0] },
						 // { "width": "10%", "targets": [1,2,3,5] },
						 // { "width": "15%", "targets": [4] },
					// ],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_history_pengkajian_ic', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							st_owned:st_owned,
							st_verifikasi:st_verifikasi,
							pemberi_info:profesi_id_filter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
					 $("#index_history_kajian thead").remove();
				 }  
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		// $("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_edit_ic', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaran_id_ranap=data.pendaftaran_id;
					$("#cover-spin").show();
					if (data.st_ranap=='1'){
					window.location.href = "<?php echo site_url('tpendaftaran_poli_ttv/tindakan/"+pendaftaran_id_ranap+"/erm_ri/input_ic_ri'); ?>";
						
					}else{
					window.location.href = "<?php echo site_url('tpendaftaran_poli_ttv/tindakan/"+pendaftaran_id_ranap+"/erm_rj/input_ic'); ?>";
						
					}
					
				}
			}
		});
	}
	function hapus_record_assesmen(){
		$("#modal_hapus").modal('hide');
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_ic', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_ic', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	
	// $(document).on("blur",".auto_blur_tabel",function(){
		// assesmen_id=$("#assesmen_id").val();
		// var tr=$(this).closest('tr');
		// var informasi_id=tr.find(".informasi_id").val();
		// var isi=$(this).val();
		// $.ajax({
			  // url: '{site_url}Tpendaftaran_poli_ttv/update_isi_informasi_ic/',
			  // dataType: "json",
			  // type: 'POST',
			  // data: {
					// informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
			  // },success: function(data) {
				 
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
			  // }
			// });
		
	// });
</script>