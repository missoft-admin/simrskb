<style>
	.auto_blur{
		background-color:#d0f3df;
	}
	
</style>
<?if ($menu_kiri=='input_ttv'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_ttv' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($ttv_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
						$disabel_input='';
						if ($st_input_ttv=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_ttv=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_ttv=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="fa fa-thermometer-0"></i> Pemeriksaan</a>
				</li>
				<li class="">
					<a href="#tab_2" ><i class="fa fa-bar-chart"></i> Grafik</a>
				</li>
				
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_ttv/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<div class="row">
						<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
						<input type="hidden" readonly id="ttv_detail_id" value="">
						<input type="hidden" readonly id="st_sedang_edit" value="0">
						<div class="col-md-12">
							<?if($st_lihat_ttv=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
									<div class="alert alert-warning alert-dismissable" id="peringatan_ttv" style="display:<?=($status_ttv=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Belum Disimpan</a>!</p>
									</div>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right">
									<button class="btn btn-success" id="btn_simpan_ttv" <?=$disabel_input?> type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
									<?if ($ttv_id!=''){?>
									<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
									<?}?>
									<button class="btn btn-warning" type="button"><i class="fa fa-floppy-o"></i> Simpan Template</button>
								</div>
								
							</div>
						</div>
					</div>
					<hr class="push-5-b">
					<div class="row">
						
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<div class="col-md-4">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<div class="col-md-8">
									<div class="form-material">
										<select id="tingkat_kesadaran" style="background-color:#d0f3df;" name="tingkat_kesadaran" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</option>
											<?foreach(list_variable_ref(23) as $row){?>
											<option value="<?=$row->id?>" <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										<label for="tanggaldaftar">Tingkat Kesadaran </label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-material">
										<select id="suplemen_oksigen" style="background-color:#d0f3df;" name="suplemen_oksigen" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</option>
											<?foreach(list_variable_ref(421) as $row){?>
											<option value="<?=$row->id?>" <?=($suplemen_oksigen=='' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($suplemen_oksigen == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										<label for="tanggaldaftar">Suplemen Oksigen </label>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<div class="col-md-4">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control decimal auto_blur" <?=$disabel_input?>  type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
											<span class="input-group-addon"><?=$satuan_nadi?></span>
										</div>
										<label for="nadi">Frekuensi Nadi </label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control decimal auto_blur" <?=$disabel_input?>  type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
											<span class="input-group-addon"><?=$satuan_nafas?></span>
										</div>
										<label for="nafas">Frekuensi Nafas </label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control decimal auto_blur" <?=$disabel_input?>  type="text" id="spo2" name="spo2" value="{spo2}"  placeholder="SpO2" required>
											<span class="input-group-addon"><?=$satuan_spo2?></span>
										</div>
										<label for="spo2">SpO2 </label>
									</div>
								</div>
								
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<div class="col-md-12">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
											<span class="input-group-addon"><i class="fa fa-user"></i></span>
										</div>
										<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
									</div>
								</div>
								
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<div class="col-md-8">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control decimal auto_blur" <?=$disabel_input?> type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
											<span class="input-group-addon"><?=$satuan_td?> / </span>
											<input class="form-control decimal auto_blur" <?=$disabel_input?> type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
											<span class="input-group-addon"><?=$satuan_td?></span>
										</div>
										<label for="satuan_td">Tekanan Darah </label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control decimal auto_blur" <?=$disabel_input?> type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
											<span class="input-group-addon"><?=$satuan_suhu?></span>
										</div>
										<label for="suhu">Suhu Tubuh </label>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<div class="col-md-6">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control decimal auto_blur" <?=$disabel_input?>  type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
											<span class="input-group-addon">Cm</span>
											
										</div>
										<label for="satuan_td">Tinggi Badan </label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-material">
										<div class="input-group">
											<input class="form-control decimal auto_blur" <?=$disabel_input?> type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
											<span class="input-group-addon">Kg</span>
										</div>
										<label for="suhu">Berat Badan </label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<h5 class="text-primary">{judul_footer}</h5>
						</div>
					</div>
					<hr>
					<div class="block block-content-narrow" >
						<div class="row push-10-t" >
							<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
							<div class="col-md-12">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								
							</div>
						</div>
						<div class="row push-10-t">
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Tanggal</label>
											<div class="col-xs-12">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Owned By Me</label>
											<div class="col-xs-12">
												<select id="st_owned" name="st_owned" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0" selected>TIDAK</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Profesi</label>
											<div class="col-xs-12">
												<select id="filter_profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Add By Person</label>
											<div class="col-xs-12">
												
												<div class="input-group">
                                                    <select id="filter_ppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-success" type="button" onclick="load_index_ttv()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
                                                    </span>
                                                </div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="form-group ">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_ttv">
											<thead>
												<tr>
													<th width="10%"></th>
													<th width="10%"></th>
													<th width="10%"></th>
													<th width="10%"></th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
						
						
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						
						<div class="row">
							<div class="col-md-12">
								
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-th-large text-danger"></i> COMING SOON !</h4>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN TTV</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_ttv()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT TTV</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="get_edit_ttv()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_history_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">HISTORY PERUBAHAN INPUT TTV</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="table-responsive">
									<table class="table" id="index_ttv_perubahan">
										<thead>
											<tr>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#cover-spin").hide();
		// document.getElementById("nadi").style.color = "#eb0404";
		$(".time-datepicker").datetimepicker({
			format: "HH:mm:ss"
		});
		load_index_ttv();
	});
	function edit_ttv(id){
		$("#ttv_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function get_edit_ttv(){
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		let ttv_id=$("#ttv_id").val();
		if (ttv_id!=''){
			hapus_langsung(ttv_id);
		}
		let id=$("#ttv_detail_id").val();
		$("#cover-spin").show();
		
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/edit_ttv', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
						$("#cover-spin").hide();
				
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Load Data TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
						$("#ttv_id").val(data.id);
						$("#tglpendaftaran").val(data.tglpendaftaran);
						$("#waktupendaftaran").val(data.waktupendaftaran);
						$("#tingkat_kesadaran").val(data.tingkat_kesadaran).trigger('change.select2');
						$("#suplemen_oksigen").val(data.suplemen_oksigen).trigger('change.select2');
						$("#nadi").val(data.nadi);
						$("#nafas").val(data.nafas);
						$("#spo2").val(data.spo2);
						$("#td_sistole").val(data.td_sistole);
						$("#td_diastole").val(data.td_diastole);
						$("#suhu").val(data.suhu);
						$("#tinggi_badan").val(data.tinggi_badan);
						$("#berat_badan").val(data.berat_badan);
						$("#peringatan_ttv").removeClass('alert-warning').addClass('alert-success');
					if (data.status_ttv=='1'){
						$("#peringatan_ttv").show();
						$("#peringatan_ttv").html('info : Status Editing [BELUM DISAVE]');
						$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Draft'});
					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
						$("#peringatan_ttv").html('info : Status Editing [BELUM ADA PERUBAHAN]');
						$("#peringatan_ttv").show();
						// location.reload();			
					}
					$("#cover-spin").hide();
					load_index_ttv();
				}
			}
		});
	}
	function copy_ttv(id){
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/copy_ttv', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
				},
			success: function(data) {
				
						$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Load Data TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
						$("#ttv_id").val('');
						$("#tglpendaftaran").val(data.tglpendaftaran);
						$("#waktupendaftaran").val(data.waktupendaftaran);
						$("#tingkat_kesadaran").val(data.tingkat_kesadaran).trigger('change.select2');
						$("#nadi").val(data.nadi);
						$("#nafas").val(data.nafas);
						$("#td_sistole").val(data.td_sistole);
						$("#td_diastole").val(data.td_diastole);
						$("#suhu").val(data.suhu);
						$("#tinggi_badan").val(data.tinggi_badan);
						$("#berat_badan").val(data.berat_badan);
						$("#spo2").val(data.spo2);
					if (data.status_ttv=='1'){
						$("#peringatan_ttv").show();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Draft'});
					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
						$("#peringatan_ttv").removeClass('alert-warning').addClass('alert-success');
						$("#peringatan_ttv").html('info : Status Duplikasi inputan Baru');
						$("#peringatan_ttv").show();
						// location.reload();			
					}
				}
			}
		});
	}
	function load_index_ttv(){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(idpasien);
		$('#index_ttv').DataTable().destroy();	
		let st_owned=$("#st_owned").val();
		let profesi_id=$("#filter_profesi_id").val();
		let tanggal_1=$("#filter_ttv_tanggal_1").val();
		let tanggal_2=$("#filter_ttv_tanggal_1").val();
		let ppa_id=$("#filter_ppa_id").val();
		let st_sedang_edit=$("#st_sedang_edit").val();
		
		// alert(ruangan_id);
		table = $('#index_ttv').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				"columnDefs": [
						{ "width": "10%", "targets": 0},
						{ "width": "40%", "targets": 1},
						{ "width": "40%", "targets": 2},
						{ "width": "10%", "targets": 3},
						// { "width": "10%", "targets": 3},
						
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/index_TTV_history', 
					type: "POST" ,
					dataType: 'json',
					data : {
							pendaftaran_id:pendaftaran_id,
							st_owned:st_owned,
							profesi_id:profesi_id,
							tanggal_1:tanggal_1,
							tanggal_2:tanggal_2,
							ppa_id:ppa_id,
							st_sedang_edit:st_sedang_edit,
							idpasien:idpasien,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#index_ttv thead").remove();
				 }  
			});
		$("#cover-spin").hide();
	}
	$("#btn_simpan_ttv").click(function(){
		let login_ppa_id=$("#login_ppa_id").val();
		let ttv_id=$("#ttv_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let tanggaldaftar=$("#tglpendaftaran").val();
		let waktudaftar=$("#waktupendaftaran").val();
		let tingkat_kesadaran=$("#tingkat_kesadaran").val();
		let nadi=$("#nadi").val();
		let nafas=$("#nafas").val();
		let td_sistole=$("#td_sistole").val();
		let td_diastole=$("#td_diastole").val();
		let suhu=$("#suhu").val();
		let tinggi_badan=$("#tinggi_badan").val();
		let berat_badan=$("#berat_badan").val();
		
		if (tanggaldaftar==''){
			sweetAlert("Maaf...", "Tentukan Tanngal", "error");
			return false;
		}
		if (tingkat_kesadaran=='' || tingkat_kesadaran==null){
			sweetAlert("Maaf...", "Tentukan  Tingkat Kesadaran", "error");
			return false;
		}
		if (nadi=='' || nadi<=0){
			sweetAlert("Maaf...", "Tentukan  Frekuensi nadi", "error");
			return false;
		}
		if (nafas=='' || nafas<=0){
			sweetAlert("Maaf...", "Tentukan  Frekuensi nafas", "error");
			return false;
		}
		if (td_sistole=='' || td_sistole<=0){
			sweetAlert("Maaf...", "Tentukan  Tekanan Darah sistole", "error");
			return false;
		}
		if (td_diastole=='' || td_diastole<=0){
			sweetAlert("Maaf...", "Tentukan  Tekanan Darah diastole", "error");
			return false;
		}
		if (suhu=='' || suhu<=0){
			sweetAlert("Maaf...", "Tentukan Suhu Badan", "error");
			return false;
		}
		if (tinggi_badan=='' || tinggi_badan<=0){
			sweetAlert("Maaf...", "Tentukan  Tinggi Badan", "error");
			return false;
		}
		if (berat_badan==''){
			sweetAlert("Maaf...", "Tentukan Berat Badan", "error");
			return false;
		}
		$("#cover-spin").show();
		simpan_ttv(2);	
	});
	$("#tingkat_kesadaran").change(function(){
		simpan_ttv(1);
	});
	$("#suplemen_oksigen").change(function(){
		simpan_ttv(1);
	});
	
	$(".auto_blur").blur(function() {
		simpan_ttv(1);
	});
	function simpan_ttv(status_ttv){
		let login_ppa_id=$("#login_ppa_id").val();
		let ttv_id=$("#ttv_id").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let tanggaldaftar=$("#tglpendaftaran").val();
		let waktudaftar=$("#waktupendaftaran").val();
		let tingkat_kesadaran=$("#tingkat_kesadaran").val();
		let nadi=$("#nadi").val();
		let nafas=$("#nafas").val();
		let td_sistole=$("#td_sistole").val();
		let td_diastole=$("#td_diastole").val();
		let suhu=$("#suhu").val();
		let tinggi_badan=$("#tinggi_badan").val();
		let berat_badan=$("#berat_badan").val();
		let spo2=$("#spo2").val();
		let suplemen_oksigen=$("#suplemen_oksigen").val();
		
		// $("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_ttv', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					login_ppa_id:login_ppa_id,
					tanggal_input:tanggaldaftar,
					waktudaftar:waktudaftar,
					tingkat_kesadaran:tingkat_kesadaran,
					nadi:nadi,
					nafas:nafas,
					td_sistole:td_sistole,
					td_diastole:td_diastole,
					suhu:suhu,
					tinggi_badan:tinggi_badan,
					berat_badan:berat_badan,
					spo2:spo2,
					status_ttv:status_ttv,
					ttv_id:ttv_id,
					suplemen_oksigen:suplemen_oksigen,
					
				},
			success: function(data) {
				
						console.log(data);
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					if (data.status_ttv=='1'){
						$("#ttv_id").val(data.ttv_id);
						$("#peringatan_ttv").show();
						$("#peringatan_ttv").removeClass('alert-Succes').addClass('alert-warning');
						$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Draft'});
					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
						$("#cover-spin").hide();
						location.reload();			
					}
				}
			}
		});
	}
	function hapus_ttv(id){
		$("#ttv_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	
	function hapus_record_ttv(){
		let id=$("#ttv_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan TTV ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_ttv', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						$('#index_ttv').DataTable().ajax.reload( null, false );
						$("#modal_hapus").modal('hide');
						$("#st_sedang_edit").val(0);
					}
				}
			});
		});

	}
	function hapus_langsung(id){
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_ttv', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						$('#index_ttv').DataTable().ajax.reload( null, false );
					}
				}
			});

	}
	function lihat_perubahan(id){
		$("#modal_history_edit").modal('show');
		document.getElementById("modal_history_edit").style.zIndex = "1201";
		$('#index_ttv_perubahan').DataTable().destroy();	
		table = $('#index_ttv_perubahan').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				"columnDefs": [
						{ "width": "10%", "targets": 0},
						{ "width": "40%", "targets": 1},
						{ "width": "40%", "targets": 2},
						{ "width": "10%", "targets": 3},
						// { "width": "10%", "targets": 3},
						
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/index_ttv_perubahan', 
					type: "POST" ,
					dataType: 'json',
					data : {
							id:id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#index_ttv thead").remove();
				 }  
			});
		$("#cover-spin").hide();
	}
</script>