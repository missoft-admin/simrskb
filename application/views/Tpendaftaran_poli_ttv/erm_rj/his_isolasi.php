<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	.edited_final{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	.edited2{
		color:red;
		font-weight: bold;
	}
	.edited_final {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	<?
	if ($versi_edit=='0'){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #57c1d1;
			border-color: transparent;
		}
		<?
	}
	if ($versi_edit>0){
		?>
		.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
			color: #fff;
			background-color: #d26a5c;
			border-color: transparent;
		}
	<?
	}?>
	
</style>
<style>
	//.kbw-signature { width: 100%;}
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_paraf canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_dokter canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_kel canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_pernyataan canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_saksi_kel canvas{
		width: 100% !important;
		height: auto;
	}
	#sig_ttd_saksi_rs canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<?if ($menu_kiri=='his_isolasi' || $menu_kiri=='his_isolasi_ri'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_isolasi' || $menu_kiri=='his_isolasi_ri' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_pernyataan;
					if ($assesmen_id){
						$tanggalpernyataan=HumanDateShort($tanggal_pernyataan);
						$waktupernyataan=HumanTime($tanggal_pernyataan);
						
						$tanggalinfo=HumanDateShort($tanggal_informasi);
						$waktuinfo=HumanTime($tanggal_input);
						
						$tanggalinfo_asal=HumanDateShort($tanggal_informasi_asal);
						$waktuinfo_asal=HumanTime($tanggal_input_asal);
						
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktupernyataan=date('H:i:s');
						$waktuinfo=date('H:i:s');
						$tanggalinfo=date('d-m-Y');
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						$tanggalpernyataan=date('d-m-Y');
						
					}
					
					$disabel_input='';
					if ($st_input_isolasi=='0'){
						$disabel_input='disabled';
					}
					$disabel_cetak='';
					if ($st_cetak_assesmen=='0'){
						$disabel_cetak='disabled';
					}
					
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_isolasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_assesmen/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						<input type="hidden" id="st_sedang_edit" value="" >		
						<div class="col-md-12">
							<?if($st_lihat_isolasi=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">INFORMATION EDITING</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_isolasi" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="dokter_pelaksana">{dokter_pelaksan_ina} / <i>{dokter_pelaksan_eng}</i></label>
										<select tabindex="8" id="dokter_pelaksana" class="<?=($dokter_pelaksana!=$dokter_pelaksana_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($dokter_pelaksana == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach($list_dokter_pelaksana as $row){?>
											<option value="<?=$row->id?>" <?=($st_setting_dokter == 1 ? 'selected="selected"' : '')?> <?=($dokter_pelaksana == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
									<label for="example-input-normal">{tanggal_tindakan_ina} / <i>{tanggal_tindakan_eng}</i></label>
										<div class="input-group date">
											<input tabindex="2" type="text" class="<?=($tanggal_informasi!=$tanggal_informasi_asal?'edited':'')?> form-control auto_blur_tanggal" data-date-format="dd/mm/yyyy" id="tanggalinfo" placeholder="HH/BB/TTTT" name="tanggalinfo" value="<?= $tanggalinfo ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-3 ">
										<label for="example-input-normal">{waktu_tindakan_ina} / <i>{waktu_tindakan_eng}</i></label>
										<div class="input-group">
											<input tabindex="3" type="text" <?=$disabel_input?> class="<?=($tanggal_informasi!=$tanggal_informasi_asal?'edited':'')?> form-control auto_blur_tanggal" id="waktuinfo" value="<?= $waktuinfo ?>" required>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
									<div class="col-md-3 ">
										
									</div>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="pemberi_info"> {nama_pemberi_info_ina} / <i>{nama_pemberi_info_eng}</i> </label>
										<select tabindex="8" id="pemberi_info" class="<?=($pemberi_info!=$pemberi_info_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($pemberi_info == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach($list_pemberi as $row){?>
											<option value="<?=$row->id?>" <?=($st_setting_pemberi_info == 1 ? 'selected="selected"' : '')?> <?=($pemberi_info == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
										
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="pendamping">{nama_petugas_pendamping_ina} / <i>{nama_petugas_pendamping_eng}</i></label>
										<select tabindex="8" id="pendamping" class="<?=($pendamping!=$pendamping_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($pendamping == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach($list_pendamping as $row){?>
											<option value="<?=$row->id?>" <?=($st_setting_petugas_pendamping == 1 ? 'selected="selected"' : '')?> <?=($pendamping == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {nama_penerima_info_ina} / <i>{nama_penerima_info_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control <?=($penerima_info!=$penerima_info_asal?'edited':'')?>"   id="penerima_info" placeholder="{nama_penerima_info_ina}" name="penerima_info" value="{penerima_info}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="hubungan_id">{hubungan_ina} / <i>{hubungan_eng}</i></label>
										<select tabindex="8" id="hubungan_id" class="<?=($hubungan_id!=$hubungan_id_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($hubungan_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(80) as $row){?>
											<option value="<?=$row->id?>" <?=($hubungan_id == $row->id ? 'selected="selected"' : '')?> <?=($hubungan_id == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="form-group push-5-t">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
									
											<table class="table table-bordered" id="tabel_informasi">
												<thead>
													<tr>
														<th width="5%" class="text-center">No</th>
														<th width="25%" class="text-center">{jenis_info_ina} / <i>{jenis_info_eng}</i></th>
														<th width="50%" class="text-center">{isi_info_ina} / <i>{isi_info_eng}</i></th>
														<th width="20%" class="text-center">{paraf_ina} / <i>{paraf_eng}</i></th>
													</tr>
												</thead>
												<tbody>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label class="text-primary">PERNYATAAN</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label> {info_pernyataan_ina} <br> <i>{info_pernyataan_eng}</i> </label>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {nama_pembuat_pernyataan_ina} / <i>{nama_pembuat_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control <?=($nama!=$nama_asal?'edited':'')?>"   id="nama" placeholder="{nama}" name="nama" value="{nama}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="umur">{umur_pembuat_pernyataan_ina} / <i>{umur_pembuat_pernyataan_eng}</i></label>
										<div class="input-group">
											<input class="form-control decimal <?=($umur!=$umur_asal?'edited':'')?>"   type="text" id="umur" value="{umur}"  placeholder="{umur}" required>
											<span class="input-group-addon">umur</span>
										</div>
									</div>
									<div class="col-md-6 ">
										<label for="jenis_kelamin">{jk_pembuat_pernyataan_ina} / <i>{jk_pembuat_pernyataan_eng}</i></label>

										<select tabindex="8" id="jenis_kelamin" class="<?=($jenis_kelamin!=$jenis_kelamin_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($jenis_kelamin == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(77) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?> <?=($jenis_kelamin == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {alamat_pembuat_pernyataan_ina} / <i>{alamat_pembuat_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control <?=($alamat!=$alamat_asal?'edited':'')?>"   id="alamat" placeholder="{alamat}" name="alamat" value="{alamat}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {nik_pembuat_pernyataan_eng} / <i>{nik_pembuat_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control <?=($no_ktp!=$no_ktp_asal?'edited':'')?>"   id="no_ktp" placeholder="{no_ktp}" name="no_ktp" value="{no_ktp}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {dengan_ini_memberikan_ina} / <i>{dengan_ini_memberikan_eng}</i> </label>
										<select tabindex="8" id="hereby" class="<?=($hereby!=$hereby_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($hereby == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<option value="" <?=($hereby == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(get_all('mjenis_persetujuan_isolasi',array('staktif'=>'1')) as $row){?>
											<option value="<?=$row->id?>" <?=($hereby == $row->id ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="st_setting_pemberi_info"> {untuk_tindakan_ina} / <i>{untuk_tindakan_eng}</i> </label>
										<input tabindex="32" type="text" class="form-control <?=($untuk_tindakan!=$untuk_tindakan_asal?'edited':'')?>"   id="untuk_tindakan" placeholder="{untuk_tindakan}" name="untuk_tindakan" value="{untuk_tindakan}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {terhadap_ina} / <i>{terhadap_eng}</i> </label>
										<select tabindex="8" id="terhadap" class="<?=($terhadap!=$terhadap_asal?'edited':'')?> form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($terhadap == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(79) as $row){?>
											<option value="<?=$row->id?>" <?=($terhadap == $row->id ? 'selected="selected"' : '')?> <?=($terhadap == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {nama_pasien_ina} / <i>{nama_pasien_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control <?=($nama_pasien!=$nama_pasien_asal?'edited':'')?>"   id="nama_pasien" placeholder="{nama_pasien}" name="nama_pasien" value="{nama_pasien}" required>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {tanggal_lahir_ina} / <i>{tanggal_lahir_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control <?=($tanggal_lahir_pasien!=$tanggal_lahir_pasien_asal?'edited':'')?>"   id="tanggal_lahir_pasien" placeholder="{tanggal_lahir_pasien}" name="tanggal_lahir_pasien" value="<?=HumanDateShort($tanggal_lahir_pasien)?>" required>
									</div>
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {usia_ina} / <i>{usia_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control <?=($umur_pasien!=$umur_pasien_asal?'edited':'')?>"   id="umur_pasien" placeholder="{umur_pasien}" name="umur_pasien" value="{umur_pasien}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {jenis_kelamin_ina} / <i>{jenis_kelamin_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control <?=($jenis_kelamin_pasien!=$jenis_kelamin_pasien_asal?'edited':'')?>"   id="jenis_kelamin_pasien" placeholder="{jenis_kelamin_pasien}" name="jenis_kelamin_pasien" value="{jenis_kelamin_pasien}" required>
									</div>
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {norek_ina} / <i>{norek_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control <?=($no_medrec!=$no_medrec_asal?'edited':'')?>"   id="no_medrec" placeholder="{no_medrec}" name="no_medrec" value="{no_medrec}" required>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label id="ket_pernyataan_ina">  </label>
									</div>
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-6 ">
										<label for="st_setting_pemberi_info"> {waktu_pernyataan_ina} / <i>{waktu_pernyataan_eng}</i> </label>
										<input tabindex="32" type="text" disabled class="form-control"   id="lokasi_pernyataan_ina" placeholder="{lokasi_pernyataan_ina}" name="lokasi_pernyataan_ina" value="{lokasi_pernyataan_ina}" required>
									</div>
									
									<div class="col-md-3 ">
										<label for="example-input-normal">&nbsp;</label>
										<div class="input-group date">
											<input tabindex="2" type="text" class=" form-control <?=($tanggal_pernyataan!=$tanggal_pernyataan_asal?'edited':'')?>" data-date-format="dd/mm/yyyy" id="tanggalpernyataan" placeholder="HH/BB/TTTT" value="<?= $tanggalpernyataan ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-3 ">
										<label for="example-input-normal">&nbsp;</label>
										<div class="input-group">
											<input tabindex="3" type="text" <?=$disabel_input?> class="form-control <?=($tanggal_pernyataan!=$tanggal_pernyataan_asal?'edited':'')?>" id="waktupernyataan" value="<?= $waktupernyataan ?>" required>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-8 ">
										
										<table class="table table-bordered">
											<tr>
												<td style="width:30%" class="text-center"><strong>{ket_gambar_paraf_menyatakan_ina} <br> <i>{ket_gambar_paraf_menyatakan_eng}</strong></td>
												<td style="width:30%" class="text-center"><strong>{ket_gambar_paraf_saksi_ina} <br> <i>{ket_gambar_paraf_saksi_eng}</strong></td>
												<td style="width:30%" class="text-center"><strong>{ket_gambar_paraf_saksi_rs_ina} <br> <i>{ket_gambar_paraf_saksi_rs_eng}</strong></td>
											</tr>
											<tr>
												<td style="width:30%" class="text-center">
													<?if ($ttd_menyatakan){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$ttd_menyatakan?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_pernyataan()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_pernyataan()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
												</td>
												<td style="width:30%" class="text-center">
													<?if ($ttd_saksi){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$ttd_saksi?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_saksi_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_saksi_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_saksi_kel()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
												</td>
												<td style="width:30%" class="text-center">
													<?if ($ttd_saksi_rs){?>
														<div class="img-container fx-img-rotate-r">
															<img class="img-responsive" src="<?=$ttd_saksi_rs?>" alt="">
															<?if ($status_assemen!='2'){?>
															<div class="img-options">
																<div class="img-options-content">
																	<div class="btn-group btn-group-sm">
																		<a class="btn btn-default" onclick="modal_ttd_saksi_rs()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
																		<a class="btn btn-default btn-danger" onclick="hapus_ttd_saksi_rs()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
																	</div>
																</div>
															</div>
															<?}?>
														</div>
													<?}else{?>
														<button class="btn btn-sm btn-success" onclick="modal_ttd_saksi_rs()"  type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
													<?}?>
												</td>
											</tr>
											<tr>
												<td style="width:30%" class="text-center"><label id="nama_pemberi_pernyataan" class="<?=($yang_menyatakan!=$yang_menyatakan_asal?'edited2':'')?>"><?=$yang_menyatakan?></label></td>
												<td style="width:30%" class="text-center"><label id="nama_saksi_keluarga" class="<?=($saksi_keluarga!=$saksi_keluarga_asal?'edited2':'')?>"><?=$saksi_keluarga?></label></td>
												<td style="width:30%" class="text-center"><label id="nama_saksi_rs" class="<?=($saksi_rs!=$saksi_rs_asal?'edited2':'')?>"><?=$saksi_rs?></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var sig_ttd_saksi_rs = $('#sig_ttd_saksi_rs').signature({syncField: '#signature64_ttd_saksi_rs', syncFormat: 'PNG'});
var sig_ttd_saksi_kel = $('#sig_ttd_saksi_kel').signature({syncField: '#signature64_ttd_saksi_kel', syncFormat: 'PNG'});
var sig_ttd_pernyataan = $('#sig_ttd_pernyataan').signature({syncField: '#signature64_ttd_pernyataan', syncFormat: 'PNG'});
var sig_ttd_kel = $('#sig_ttd_kel').signature({syncField: '#signature64_ttd_kel', syncFormat: 'PNG'});
var sig_ttd_dokter = $('#sig_ttd_dokter').signature({syncField: '#signature64_ttd_dokter', syncFormat: 'PNG'});
var sig_paraf = $('#sig_paraf').signature({syncField: '#signature64_paraf', syncFormat: 'PNG'});
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});

function load_index_informasi(){
	$("#cover-spin").show();
	var versi_edit=$("#versi_edit").val();
	var assesmen_id=$("#assesmen_id").val();
	var status_assemen=$("#status_assemen").val();
	var ttd_dokter_pelaksana=$("#signature64_ttd_dokter").val();
	var ttd_penerima_info=$("#signature64_ttd_kel").val();
	// alert(id);
	$.ajax({
		url: '{site_url}thistory_tindakan/load_index_informasi_isolasi/',
		dataType: "json",
		type: 'POST',
		  data: {
				versi_edit:versi_edit,
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				ttd_dokter_pelaksana:ttd_dokter_pelaksana,
				ttd_penerima_info:ttd_penerima_info,
				
		  },
		success: function(data) {
			$("#tabel_informasi tbody").empty();
			$("#tabel_informasi tbody").append(data.tabel);
			$("#cover-spin").hide();
			disabel_edit();
		}
	});
}
$(document).ready(function() {
	disabel_edit();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
		if (assesmen_id){
			load_index_informasi();
			list_index_history_edit();
			set_persetujuan();
		}
	load_awal_assesmen=false;
});
function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $(".edited").removeClass("edited").addClass("edited_final");
		 $(".his_filter").removeAttr('disabled');
	}
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			// columnDefs: [
					// {  className: "text-right", targets:[0] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "20%", "targets": [1] },
					 // { "width": "15%", "targets": [3] },
					 // { "width": "60%", "targets": [2] }
				// ],
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_isolasi', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
$("#hereby").change(function(){
	set_persetujuan();
});
function set_persetujuan(){
	
	let assesmen_id=$("#assesmen_id").val();
	let hereby=$("#hereby").val();
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/get_persetujaun_isolasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				hereby:hereby,
			
			   },
		success: function(data) {
			$("#ket_pernyataan_ina").html(data);
			$(".text-muted").removeClass("text-muted").addClass("text-muted");
		}
	});
}
</script>