<style>
	.auto_blur_tgl{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.input_pemeriksaan_header_global{
		background-color:#d0f3df;
	}
	.auto_blur_ttv{
		background-color:#d0f3df;
	}
	
	.input_edited{
		color:red;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	.table-asuhan {
	  border: 2px solid;
	  border-collapse: collapse;
	   width: 100%;
	   padding-left: 30px;
	}
	.table-asuhan tr td  {
	  vertical-align: middle;
	  border: 1px solid #ddd;
	   padding-left: 15px;
	   padding-bottom: 3px;
	   padding-top: 3px;
	}
	.checkbox-asuhan{
		padding-top: 3px!important;
		margin-top: 0!important;
		margin-bottom: 0px!important;
		vertical-align: middle!important;
		
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>

<?if ($menu_kiri=='input_triage'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_triage' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
						$tanggaldatang=HumanDateShort($tanggal_datang);
						$waktudatang=HumanTime($tanggal_datang);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
						$waktudatang=date('H:i:s');
						$tanggaldatang=date('d-m-Y');
						
					}
						$disabel_input='';
						$disabel_input_ttv='';
						// if ($status_ttv!='1'){
							// $disabel_input_ttv='disabled';
						// }
						if ($st_input_triage=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_triage=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_triage=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Pengkajian Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('Tpendaftaran_poli_triage/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div class="row">
						
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="assesmen_detail_id" value="">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<div class="col-md-12">
							<?if($st_lihat_triage=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_triage" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_triage" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_triage" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_triage=='1'){?>
									<button class="btn btn-primary" id="btn_create_triage" onclick="create_triage()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_triage" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_triage" onclick="batal_triage()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_triage" onclick="close_triage()" type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<button class="btn btn-info" <?=$disabel_cetak?> type="button"><i class="fa fa-print"></i> CETAK</button>
										<?}?>
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}tpendaftaran_poli_ttv/tindakan/<?=$pendaftaran_id?>/erm_rj/input_triage" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tgl" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tgl" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
								<div class="col-md-6 col-xs-12">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_triage!='1'?'disabled':'')?> onclick="create_with_template()"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label for="satuan_td">Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >KEDATANGAN </h5>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Tanggal Masuk</label>
									<div class="input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tgl" data-date-format="dd/mm/yyyy" id="tanggaldatang" placeholder="HH/BB/TTTT" name="tanggaldatang" value="<?= $tanggaldatang ?>" required>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Jam Masuk</label>
									<div class="input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control auto_blur_tgl" id="waktudatang" value="<?= $waktudatang ?>" required>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Terdapat Surat pengantar / Rujukan</label>
									<select tabindex="8" id="surat_pengantar" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($surat_pengantar == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(55) as $row){?>
										<option value="<?=$row->id?>" <?=($surat_pengantar == $row->id ? 'selected="selected"' : '')?> <?=($surat_pengantar == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Sarana Transportasi</label>
									<select tabindex="8" id="sarana_transport" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($sarana_transport == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(52) as $row){?>
										<option value="<?=$row->id?>" <?=($sarana_transport == $row->id ? 'selected="selected"' : '')?> <?=($sarana_transport == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
									
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Asal Rujukan</label>
									<select tabindex="32" name="idasalpasien" id="idasalpasien" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?php foreach (get_all('mpasien_asal') as $r):?>
											<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<?php if ($pendaftaran_id != '' && ($idasalpasien == '2' || $idasalpasien == '3')) {?>
								<?php
									if ($idasalpasien == 2) {
										$idtipeRS = 1;
									} elseif ($idasalpasien == 3) {
										$idtipeRS = 2;
									}
									
								}else{
									$idtipeRS='';
								}
								?>
							<div class="col-md-6 ">
								<div class="col-md-12 ">
									<label for="example-input-normal">Nama Fasilitas Kesehatan</label>
									<div class="input-group">
										<select tabindex="33" name="idrujukan" id="idrujukan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach (get_all('mrumahsakit', ['idtipe' => $idtipeRS]) as $r) {?>
												<option value="<?= $r->id?>" <?=($idrujukan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
											<?php }?>
										</select>
										
										<span class="input-group-btn">
											<button class="btn btn-warning" title="refresh" onclick="set_asal_rujukan()" type="button"><i class="fa fa-refresh"></i></button>
											<a href="{base_url}mrumahsakit/create" title="Tambah Faskes" target="_blannk" class="btn btn-success" type="button"><i class="fa fa-plus"></i></a>
											
										</span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Cara Masuk</label>
									<select tabindex="8" id="cara_masuk" name="cara_masuk" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($cara_masuk == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(53) as $row){?>
										<option value="<?=$row->id?>" <?=($cara_masuk == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($cara_masuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Macam Kasus</label>
									<select tabindex="8" id="macam_kasus" name="macam_kasus" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($macam_kasus == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(54) as $row){?>
										<option value="<?=$row->id?>" <?=($macam_kasus == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($macam_kasus == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Nama pengantar</label>
									<input tabindex="29" type="text" class="form-control auto_blur"   id="namapengantar" placeholder="Nama Pengantar" name="namapengantar" value="{namapengantar}" required>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">No Tlp Penanggung Jawab</label>
									<input tabindex="32" type="text" class="form-control auto_blur"   id="teleponpengantar" placeholder="Telepon" name="teleponpengantar" value="{teleponpengantar}" required>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Tanda Tanda Vital </h5>
								<input type="hidden" readonly class="form-control input-sm" id="ttv_id" value="{ttv_id}"> 
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">GCS</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"  type="text" id="gcs_eye"  value="{gcs_eye}" placeholder="Eye" required>
										<span class="input-group-addon"><?=$satuan_gcs_eye?></span>
										<input class="form-control decimal auto_blur_ttv"  type="text" id="gcs_voice" value="{gcs_voice}"  placeholder="Voice" required>
										<span class="input-group-addon"><?=$satuan_gcs_voice?></span>
										<input class="form-control decimal auto_blur_ttv"  type="text" id="gcs_motion" value="{gcs_motion}"   placeholder="Motion" required>
										<span class="input-group-addon"><?=$satuan_gcs_motion?></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Pupil</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"  type="text" id="pupil_1"  value="{pupil_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_pupil?> / </span>
										<input class="form-control decimal auto_blur_ttv"  type="text" id="pupil_2" value="{pupil_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_pupil?></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Refleksi Cahaya</label>
									<div class="input-group">
										<input class="form-control  auto_blur_ttv"  type="text" id="reflex_cahaya_1"  value="{reflex_cahaya_1}" placeholder="Kiri" required>
										<span class="input-group-addon"><?=$satuan_cahaya?> / </span>
										<input class="form-control  auto_blur_ttv"  type="text" id="reflex_cahaya_2" value="{reflex_cahaya_2}" placeholder="Kanan" required>
										<span class="input-group-addon"><?=$satuan_cahaya?></span>
									</div>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Tekanan Darah</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"  type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal auto_blur_ttv"  type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="example-input-normal">Frekuensi Nadi</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv blur_nadi"   type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Nadi</label>
									<input class="form-control auto_blur_ttv " readonly  type="text" id="ket_nadi" value="{ket_nadi}"  placeholder="Keterangan Nadi">
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Frekuensi Nafas</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"   type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
								</div>
								<div class="col-md-6">
									<label for="example-input-normal">Suhu Tubuh</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"  type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 ">
								
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">SPO2</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"   type="text" id="spo2"  value="{spo2}"  placeholder="{satuan_spo2}" required>
										<span class="input-group-addon"><?=$satuan_spo2?></span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Akral</label>
									<div class="input-group">
										<input class="form-control auto_blur_ttv"  type="text" id="akral"  value="{akral}" placeholder="{satuan_akral}" required>
										<span class="input-group-addon"><?=$satuan_akral?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6 ">
								
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Tinggi Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"   type="text" id="tinggi_badan"  value="{tinggi_badan}" name="tinggi_badan" placeholder="cm" required>
										<span class="input-group-addon">Cm</span>
										
									</div>
								</div>
								<div class="col-md-6 push-10-t">
									<label for="example-input-normal">Berat Badan</label>
									<div class="input-group">
										<input class="form-control decimal auto_blur_ttv"  type="text" id="berat_badan"  value="{berat_badan}" name="berat_badan" placeholder="Kg" required>
										<span class="input-group-addon">Kg</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >Pemeriksaan Lain-Lain</h5>
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-6 ">
								<div class="col-md-3 ">
									<label for="example-input-normal">Kasus Alergi</label>
									<select tabindex="8" id="kasus_alergi" name="kasus_alergi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($kasus_alergi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(58) as $row){?>
										<option value="<?=$row->id?>" <?=($kasus_alergi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($kasus_alergi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Jika Ada Sebutkan</label>
									<input class="form-control auto_blur" type="text" id="alergi"  value="{alergi}" placeholder="Detail Alergi" >
								</div>
								<div class="col-md-3 ">
									<label for="example-input-normal">Status Psikologi </label>
									<select tabindex="8" id="st_psikologi" name="st_psikologi" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($st_psikologi == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(57) as $row){?>
										<option value="<?=$row->id?>" <?=($st_psikologi == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($st_psikologi == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
							</div>
							<div class="col-md-6 ">
								<div class="col-md-6 ">
									<label for="example-input-normal">Risiko Jatuh  Morse Skor</label>
									<select tabindex="8" id="risiko_jatuh_morse" name="risiko_jatuh_morse" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($risiko_jatuh_morse == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(59) as $row){?>
										<option value="<?=$row->id?>" <?=($risiko_jatuh_morse == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($risiko_jatuh_morse == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-6 ">
									<label for="example-input-normal">Risiko Jatuh  Morse Skor</label>
									<select tabindex="8" id="risiko_pasien_anak" name="risiko_pasien_anak" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($risiko_pasien_anak == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(56) as $row){?>
										<option value="<?=$row->id?>" <?=($risiko_pasien_anak == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($risiko_pasien_anak == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								
								
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="h5 text-primary" >TRIAGE</h5>
								</div>
						</div>
						<?
							$q="SELECT *FROM tpoliklinik_triage_kat H WHERE H.assesmen_id='$assesmen_id' ";
							$header=$this->db->query($q)->result();
						?>
						<div class="form-group" hidden>
							<div class="col-md-12 ">
								<div class="col-md-6 ">
								<label for="example-input-normal">Triage</label>
									<select id="kategori_id" name="kategori_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($kategori_id==''?'selected':'')?>>- Belum dipilih -</option>
										<?foreach($header as $r){?>
										<option value="<?=$r->mtriage_id?>" <?=($r->mtriage_id==$kategori_id?'selected':'')?>><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<div class="table-responsive">
										<table class="table table-bordered" id="tabel_pemeriksaan">
											
										</table>
									</div>
								</div>
							</div>
							
						</div>
						
						<?}?>
						<!--BATS AKRHI -->
					</div>
					<hr class="push-5-b">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<label class="text-primary" ><?=$judul_footer?></h5>
							</div>
						</div>
					</div>
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
										<div class="col-md-8">
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<?foreach($list_poli as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE PENGKAJIAN</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_triage!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_savae_template_triage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_triage()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_triage()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_triage=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var kategori_id_last=$("#kategori_id").val();
var kategori_id_new=$("#kategori_id").val();
$(document).ready(function() {
	// alert(status_assemen);
	// document.getElementById("nadi").style.color = "#eb0404";
	disabel_edit();
	// set_ttd_triage();
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	$("#tabel_rencana_asuhan tbody").empty();
	let assesmen_id=$("#assesmen_id").val();
	if (assesmen_id){
		load_pemeriksaan();
		
	}
	ucheck_option();
	// load_data_rencana_asuhan(1);
});
function load_pemeriksaan(){
	let kategori=$("#kategori_id").val();
	let assesmen_id=$("#assesmen_id").val();
	let status_assemen=$("#status_assemen").val();
	$("#tabel_skrining tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpendaftaran_poli_ttv/load_pemeriksaan_triage', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:assesmen_id,
				status_assemen:status_assemen,
				
				},
		success: function(data) {
			$("#tabel_pemeriksaan").append(data.tabel);
			ucheck_option();
			// $(".nilai").select2();
			// // set_kategori();
			// $(".item_all").prop("disabled", true);
			// if (kategori!='#'){
				// $(".chk_kategori_"+kategori).removeAttr('disabled');
				// $(".input_kategori_"+kategori).removeAttr('disabled');
			// }
			$("#cover-spin").hide();
			load_awal_triage=false;
		}
	});
}

function set_all_opsi_kategori(kategori){
	if (status_assemen!='2'){
		if (kategori!=kategori_id_last){
			swal({
			  title: 'Anda Yakain?',
			  text: "Mengganti Triage!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Proses!'
			}).then(function(json_data) {
				$(".input_pemeriksaan_header_global").val('');
				set_kategori();
				kategori_id_last=kategori;
				$("#kategori_id").val(kategori).trigger('change');
				ucheck_option();
				if ($("#st_edited").val()=='0'){
				simpan_triage();
				}
				$(".chk_kategori_"+kategori).prop("checked", true);
				let pemeriksaan=$(".input_pemeriksaan_header_"+kategori).val();
				$.ajax({
						url: '{site_url}Tpendaftaran_poli_ttv/simpan_triage_header', 
						dataType: "JSON",
						method: "POST",
						data : {
								assesmen_id:$("#assesmen_id").val(),
								pemeriksaan:pemeriksaan,
								kategori:kategori,
								
							},
						success: function(data) {
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}
				});
				swal({
					title: "Behasil!",
					text: "Proses.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				save_all_detail(kategori_id_last);
			}, function(dismiss) {
			  if (dismiss === 'cancel' || dismiss === 'close') {
				$(".opsi_kategori_"+kategori).prop("checked", false);
				$(".opsi_kategori_"+kategori_id_last).prop("checked", true);
				// $("#kategori_id").val(kategori).trigger('change');
				if ($("#st_edited").val()=='0'){
				simpan_triage();
				}
				swal({
					title: "Batal!",
					text: "Proses.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});

			  } 
			})
		}else{
			save_all_detail(kategori_id_last);
		}
		
	}else{
		event.preventDefault();
	}
	
	// alert(pemeriksaan);
}
function save_all_detail(kategori_id){
	$(".chk_kategori_"+kategori_id).prop("checked", true);
	$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_all_detail_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$("#assesmen_id").val(),
					kategori_id:kategori_id,
					
				},
			success: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
			}
	});
}
$("#kategori_id").change(function(){
		// kategori_id_new=$(this).val();
	// // console.log(kategori_id_last+' - '+ $(this).val());
	// if (kategori_id_new!=kategori_id_last){
		
	// }
	
	
	
});
$(document).on('click','.chck_all',function(){
	let data_id=$(this).attr("data-id");
	let data_kat=$(this).attr("data-kat");
	var isChecked = $(this).is(':checked');
	let pemeriksaan=$(".input_pemeriksaan_header_"+data_kat).val();
	// alert(data_kat);
	if (status_assemen!='2'){
		if (data_kat!=kategori_id_last){
			swal({
			  title: 'Anda Yakain?',
			  text: "Mengganti Triage!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Proses!'
			}).then(function(json_data) {
				$(".input_pemeriksaan_header_global").val('');
				kategori_id_last=data_kat;
				$("#kategori_id").val(data_kat).trigger('change');
				ucheck_option();
				if ($("#st_edited").val()=='0'){
				simpan_triage();
				}
				
				// var data_id=$(this).closest('td').find(".data_id").val();
				var pilih;
				if (isChecked){
				  pilih=1;
				  $(".opsi_kategori_"+kategori_id_last).prop("checked", true);
				  // alert('sini');
				}else{
				  pilih=0;
				}
				$.ajax({
					url: '{site_url}Tpendaftaran_poli_ttv/update_data_triage', 
					dataType: "JSON",
					method: "POST",
					data : {
							id:data_id,
							pilih:pilih,
							data_kat:data_kat,
							pemeriksaan:pemeriksaan,
							assesmen_id:$("#assesmen_id").val(),
						},
					success: function(data) {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data Satu'});
						// load_diagnosa();		
					}
				});
				swal({
					title: "Behasil!",
					text: "Proses.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
			}, function(dismiss) {
				
			  if (dismiss === 'cancel' || dismiss === 'close') {
					$(".chk_kategori_"+data_kat).prop("checked", false);
					swal({
						title: "Batal!",
						text: "Proses.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});

			  } 
			})
		}else{
			
			// var data_id=$(this).closest('td').find(".data_id").val();
			var pilih;
			if (isChecked){
			  pilih=1;
			}else{
			  pilih=0;
			  // $(".input_kategori_header").removeAttr('checked');
			}
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/update_data_triage', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:data_id,
						pilih:pilih,
						data_kat:data_kat,
						pemeriksaan:pemeriksaan,
						assesmen_id:$("#assesmen_id").val(),
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
					// load_diagnosa();		
				}
			});
		}
		// 
	}else{
		event.preventDefault();
	}
});
function ucheck_option(){
	$("#kategori_id > option").each(function() {
		let kat_id=this.value;
		if (kat_id !='#'){
			
			if (kat_id != kategori_id_last){
				$(".input_pemeriksaan_header_"+kat_id).val("");
				$(".input_pemeriksaan_header_"+kat_id).prop("disabled",true);
				// chk_kategori_
				$(".chk_kategori_"+kat_id).removeAttr("checked")
				$(".opsi_kategori_"+kat_id).removeAttr("checked")
				console.log(this.value);
			}else{
				$(".input_pemeriksaan_header_"+kat_id).removeAttr("disabled");
			}
		// alert(this.text + ' ' + this.value);
		}
	});
}
function set_kategori(){
	// let kategori_id=$("#kategori_id").val();
	// set_all_opsi(kategori_id);
}
function set_all_opsi(kategori){
	
	// // $(".item_all").prop("disabled", true);
	// $(".chck_all").prop("checked", false);
	// // $(".input_kategori_header").prop("checked", false);
	// // $(".input_pemeriksaan_header").val('');
	// // $(".chk_kategori_"+kategori).prop("checked", true);
	// if (kategori!='#'){
		// $(".chk_kategori_"+kategori).removeAttr('disabled');
		// $(".input_kategori_"+kategori).removeAttr('disabled');
	// }
	
}
function ganti_blur(kategori){
	if (status_assemen!='2'){
		let pemeriksaan=$(".input_pemeriksaan_header_"+kategori).val();
		$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/simpan_triage_pemeriksaan', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						pemeriksaan:pemeriksaan,
						mtriage_id:kategori,
						
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
				}
		});
	}
}

// $(document).on('focus','.item_all',function(){
	// // let data_id=$(this).attr("data-id");
	// // alert(index);
	// // if (status_assemen!='2'){
		// // var isChecked = $(this).is(':checked');
		// // // var data_id=$(this).closest('td').find(".data_id").val();
		// // var pilih;
		// // if (isChecked){
		  // // pilih=1;
		// // }else{
		  // // pilih=0;
		// // }
		// // $.ajax({
			// // url: '{site_url}Tpendaftaran_poli_ttv/update_data_triage', 
			// // dataType: "JSON",
			// // method: "POST",
			// // data : {
					// // id:data_id,
					// // pilih:pilih,
					
				// // },
			// // success: function(data) {
				// // $.toaster({priority : 'success', title : 'Succes!', message : ' Save Draft'});
				// // // load_diagnosa();		
			// // }
		// // });
	// // }else{
		// // event.preventDefault();
	// // }
// });

$("#nadi").on("blur", function() {
	get_ket_nadi();
});

function disabel_edit(){
	if (status_assemen=='2'){
		 $("#form1 :input").prop("disabled", true);
		 $("#diagnosa_id_list").removeAttr('disabled');
		 $("#jenis_ttd").removeAttr('disabled');
		 $(".btn_rencana").removeAttr('disabled');
		 $(".btn_ttd").removeAttr('disabled');
	}
}
function simpan_template(){
	$("#modal_savae_template_triage").modal('show');
	
}

function create_triage(){
	let idpasien=$("#idpasien").val();
	// alert(idpasien);
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					template_id:template_id,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function gunakan_template_triage(id){
	$("#template_assesmen_id").val(id).trigger('change');
	create_with_template();
}
function create_with_template(){
	let template_assesmen_id=$("#template_assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari template ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function copy_history_triage(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_with_template_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function create_template(){
	let pendaftaran_id=$("#pendaftaran_id").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Template "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/create_template_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					tglpendaftaran:tglpendaftaran,
					waktupendaftaran:waktupendaftaran,
					idpasien:idpasien,
					template_id:template_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}

function batal_triage(){
	let assesmen_id=$("#assesmen_id").val();
	let st_edited=$("#st_edited").val();
	let jml_edit=$("#jml_edit").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					st_edited:st_edited,
					jml_edit:jml_edit,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
function batal_template(){
	let assesmen_id=$("#assesmen_id").val();
	let nama_template=$("#nama_template").val();
	
	let template=$("#template_id option:selected").text();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Assesment ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/batal_template_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
					nama_template:nama_template,
				
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}

$(".opsi_change").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_triage();
	}
});
$(".auto_blur_tgl").change(function(){
	if ($("#st_edited").val()=='0'){
		simpan_triage();
	}
});
$(".auto_blur_tgl").blur(function(){
	if ($("#st_edited").val()=='0'){
		simpan_triage();
	}
});


$(".auto_blur").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_triage();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});
$(".auto_blur_ttv").blur(function(){
	if ($("#st_edited").val()=='0'){
		$(this).removeClass('input_edited');
		simpan_triage();
	}else{
		if ($("#st_edited").val()=='1'){
			if ($(this).val()!=before_edit){
				console.log('Ada Peruabahan');
				// $(this).attr('input_edited');
				 $(this).addClass('input_edited');
			}
		}
	}
	
});

$(".auto_blur").focus(function(){
	before_edit=$(this).val();
	console.log(before_edit);
});
function close_triage(){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Inputan Assesmen?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$("#status_assemen").val(2);
			nama_template='';
			simpan_triage();
		});		
	
}
function close_template(){
	if ($("#nama_template").val()==''){
		sweetAlert("Maaf...", "Tentukan Nama Template", "error");
		return false;
	}

	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#status_assemen").val(4);
			nama_template=$("#nama_template").val();
			simpan_triage();
		});		
	
}
function simpan_triage(){
	// alert($("#keluhan_utama").val());
	if (load_awal_triage==true){return false}
	// if ($("#st_edited").val()=='1'){
		// return false;
	// }
	if (status_assemen !='2'){
		
	
		let assesmen_id=$("#assesmen_id").val();
		// alert(assesmen_id);return false;
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		
		let tanggaldatang=$("#tanggaldatang").val();
		// alert(tanggaldatang);
		let waktudatang=$("#waktudatang").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		if (assesmen_id){
			console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/save_triage', 
				dataType: "JSON",
				method: "POST",
				data : {
						tanggaldatang:tanggaldatang,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						waktudatang:waktudatang,
						assesmen_id:$("#assesmen_id").val(),
						nama_template:nama_template,
						status_assemen:$("#status_assemen").val(),
						sarana_transport : $("#sarana_transport").val(),
						surat_pengantar : $("#surat_pengantar").val(),
						asal_rujukan : $("#asal_rujukan").val(),
						cara_masuk : $("#cara_masuk").val(),
						macam_kasus : $("#macam_kasus").val(),
						gcs_eye : $("#gcs_eye").val(),
						gcs_voice : $("#gcs_voice").val(),
						gcs_motion : $("#gcs_motion").val(),
						pupil_1 : $("#pupil_1").val(),
						pupil_2 : $("#pupil_2").val(),
						reflex_cahaya_1 : $("#reflex_cahaya_1").val(),
						reflex_cahaya_2 : $("#reflex_cahaya_2").val(),
						spo2 : $("#spo2").val(),
						akral : $("#akral").val(),
						nadi : $("#nadi").val(),
						ket_nadi : $("#ket_nadi").val(),
						nafas : $("#nafas").val(),
						suhu : $("#suhu").val(),
						td_sistole : $("#td_sistole").val(),
						td_diastole : $("#td_diastole").val(),
						tinggi_badan : $("#tinggi_badan").val(),
						berat_badan : $("#berat_badan").val(),
						kasus_alergi : $("#kasus_alergi").val(),
						alergi : $("#alergi").val(),
						st_psikologi : $("#st_psikologi").val(),
						risiko_jatuh_morse : $("#risiko_jatuh_morse").val(),
						risiko_pasien_anak : $("#risiko_pasien_anak").val(),
						kategori_id : $("#kategori_id").val(),
						st_edited:$("#st_edited").val(),
						jml_edit:$("#jml_edit").val(),
						idasalpasien:$("#idasalpasien").val(),
						idrujukan:$("#idrujukan").val(),
						namapengantar:$("#namapengantar").val(),
						teleponpengantar:$("#teleponpengantar").val(),
					},
				success: function(data) {
					
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='3'){
								
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
							}else{
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								location.reload();			
							}
							
						}
					}
				}
			});
			}
		}
	}
	

	$(document).on('change','#template_assesmen_id',function(){
		if ($(this).val()!=''){
			$("#btn_create_with_template").removeAttr('disabled');
		}else{
			$("#btn_create_with_template").attr('disabled','disabled');
		}
	});
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_index_template_triage', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	
	function edit_template_triage(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/edit_template_triage', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function get_ket_nadi(){
		let nadi=$("#nadi").val();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/get_ket_nadi_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					nadi:nadi,
					
				},
			success: function(data) {
				if (data){
				$("#ket_nadi").val(data);
					
				}else{
				$("#ket_nadi").val('');
					
				}
			}
		});
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idpoli=$("#idpoli").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		$('#index_history_kajian').DataTable().destroy();	
		$("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tpendaftaran_poli_ttv/list_history_pengkajian_triage', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							idpoli:idpoli,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function edit_history_assesmen(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function hapus_history_triage(id){
		$("#assesmen_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	function save_edit_triage(){
		let jml_edit=$("#jml_edit").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpendaftaran_poli_ttv/save_edit_triage', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					let pendaftaran_id_ranap=data.pendaftaran_id;
					$("#cover-spin").show();
					window.location.href = "<?php echo site_url('tpendaftaran_poli_ttv/tindakan/"+pendaftaran_id_ranap+"/erm_rj/input_triage'); ?>";
					
				}
			}
		});
	}
	function hapus_record_triage(){
		let id=$("#assesmen_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Inputan Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_triage', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						list_history_pengkajian();
						$("#modal_hapus").modal('hide');
					}
				}
			});
		});

	}
	function hapus_template_triage(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tpendaftaran_poli_ttv/hapus_record_triage', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	$("#idasalpasien").change(function() {
		// set_asal_rujukan();
		load_faskes_1();
	});
	function load_faskes_1(){
		$("#cover-spin").show();
		var asal = $("#idasalpasien").val();
		$.ajax({
			url: '{site_url}tpoliklinik_pendaftaran/getRujukan',
			method: "POST",
			dataType: "json",
			data: {
				"asal": asal
			},
			success: function(data) {
				$("#idrujukan").empty();
				$("#idrujukan").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#idrujukan").append("<option value='" + data[i].id + "' >" + data[i].nama + "</option>");
				}
				$("#idrujukan").selectpicker("refresh");
				$("#cover-spin").hide();
			}
		});
	}
</script>