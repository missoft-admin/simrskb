<div class="modal in" id="modal_email_user" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-danger">
					<h3 class="block-title">HISTORY KIRIM EMAIL</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="table-responsive">
									<table class="table" id="index_list_email">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="30%">Petugas</th>
												<th width="20%">Tanggal</th>
												<th width="35%">Email Tujuan</th>
												<th width="10%">Status</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_kirim_email" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<h3 class="block-title">Kirim E-mail Surat</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<label for="list_email">Alamat E-mail </label>
								<input class="js-tags-input form-control" type="text" id="list_email" name="list_email" value="">
							</div>
							
						</div>
					</div>
					<input type="hidden" id="email_jenis_surat" value="" >		
					<input type="hidden" id="email_assesmen_id" value="" >		
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-success" type="button" onclick="kirim_email()"><i class="si si-envelope"></i> Kirim E-mail</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
/*


*/
function show_email_history(assesmen_id,jenis_surat){
	$("#modal_email_user").modal('show');
	list_his_email(assesmen_id,jenis_surat);
}
function kirim_email(){
	if ($("#list_email").val()=='' || $("#list_email").val()==null){
		sweetAlert("Maaf...", "Tentukan E-mail tujuan", "error");
		return false;
	}
	// alert($("#email_jenis_surat").val());
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tpoliklinik_ranap/kirim_email',
		type: 'POST',
		data: {
			jenis_surat: $("#email_jenis_surat").val(),
			assesmen_id: $("#email_assesmen_id").val(),
			list_email: $("#list_email").val(),
			},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Sent Email'});
			$("#modal_kirim_email").modal('hide');
			$("#cover-spin").hide();
		}
	});
}
function list_his_email(assesmen_id,jenis_surat){
	
	$('#index_list_email').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_list_email').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[0,1,2] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}tpoliklinik_ranap/list_surat_email', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						jenis_surat:jenis_surat,
						
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
}
function show_email(assesmen_id,jenis_surat,idpasien){
	$("#email_jenis_surat").val(jenis_surat);
	$("#email_assesmen_id").val(assesmen_id);
	$("#modal_kirim_email").modal('show');
	$("#cover-spin").show();
	$('#list_email').importTags('');
	$.ajax({
		url: '{site_url}tsurat/get_email_pasien',
		type: 'POST',
		dataType: 'json',
		data: {
			idpasien: idpasien,
			},
		success: function(data) {
			$("#cover-spin").hide();
			$('#list_email').addTag(data.email);
		}
	});
	// load_data_email(assesmen_id,jenis_surat);
}

</script>