<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	
	.tabel_racikan{
		background-color:#fdffe2;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>

<?if ($menu_kiri=='input_perencanaan_bedah'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_perencanaan_bedah' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_permintaan;
	if ($assesmen_id){
		$tanggalcreated=HumanDateShort($created_date);
		$waktucreated=HumanTime($created_date);
		$tanggalpermintaan=HumanDateShort($tanggal_permintaan);
		$waktupermintaan=HumanTime($tanggal_permintaan);
		$tanggalbedah=HumanDateShort($tanggal_bedah);
		$waktubedah=HumanTime($tanggal_bedah);
		
	}else{
		$tanggalpermintaan=date('d-m-Y');
		$waktupermintaan=date('H:i:s');
		$tanggalcreated=date('d-m-Y');
		$waktucreated=date('H:i:s');
		
	}
	
	?>
<?if ($st_lihat_rencana_bedah=='1'){?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rm">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="<?=($assesmen_id!=''?'active':'')?>">
					<a href="#tab_1"><i class="si si-paper-clip"></i> Perencanaan Baru <?=($status_assemen=='2'?'&nbsp;&nbsp; <span class="badge badge-success pull-right">Readonly</span>':'')?></a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="<?=($assesmen_id==''?'active':'')?>">
					<a href="#tab_2" onclick="list_perencanaan_bedah()" ><i class="fa fa-send"></i> Perencanaan Saya</a>
				</li>
				<?php if (UserAccesForm($user_acces_form,array('1855'))){?>
				<li class="">
					<a href="#tab_3" onclick="list_perencanaan_bedah_history()"><i class="fa fa-history"></i> Riwayat Perencanaan</a>
				</li>
				<?}?>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left <?=($assesmen_id!=''?'active in':'')?>" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
					
					<div class="row">
						<input type="hidden" id="idtipe_poli" value="{idtipe_poli}"> 
						<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}"> 
						<input type="hidden" id="idpoliklinik" value="{idpoliklinik}"> 
						<input type="hidden" id="idrekanan" value="{idrekanan}"> 
						<input type="hidden" id="iddokter" value="{iddokter}"> 
						<input type="hidden" id="assesmen_id" value="{assesmen_id}"> 
					<input type="hidden" id="st_ranap" value="<?=($asalrujukan=='3'?'1':'0')?>"> 

						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="iddokter" value="<?=$iddokter?>" >		
						<input type="hidden" id="idtipe" value="<?=$idtipe?>" >		
						<input type="hidden" id="assesmen_detail_id" value="" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_edited" value="<?=$st_edited?>" >		
						<input type="hidden" id="iddokter" value="<?=$iddokter?>" >		
						
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" disabled class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggalcreated" placeholder="HH/BB/TTTT" name="tanggalcreated" value="<?= $tanggalcreated ?>" required>
										<label for="tanggalcreated">Tanggal <i class="text-muted">Created Date</i></label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" disabled  class="time-datepicker form-control " id="waktucreated" name="waktucreated" value="<?= $waktucreated ?>" required>
										<label for="waktupendaftaran">Waktu <i class="text-muted">Time</i></label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-12">
							<?if($st_lihat_eresep=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_per_ina}</h4>
								<h5 class="push-5 text-center">{judul_per_eng}</h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					
					<div class="row">
					
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' ){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Perencanaan Belum Disimpan</a>!</p>
									</div>
								<?}?>
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
									<?if ($st_input_rencana_bedah=='1'){?>
									<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
									<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-success" onclick="close_assesmen()" type="button"><i class="fa fa-send"></i> Simpan & Kirim Perencanaan</button>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<?}?>
										<?if ($assesmen_id!=''){?>
										<a href="<?=site_url()?>Tpoliklinik_ranap/cetak_rencana_bedah/<?=$assesmen_id?>" target="_blank" class="btn btn-info btn_cetak" type="button"><i class="fa fa-print"></i> CETAK</a>
										<?}?>
										
										<?if ($status_assemen=='2'){?>
											<a href="{site_url}Tpoliklinik_ranap/tindakan/<?=$pendaftaran_id?>/erm_rm/input_perencanaan_bedah" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
										<?}?>
									<?}?>
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id!=''){?>
							
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="diagnosa">{nama_pasien_ina} / <i class="text-muted">{nama_pasien_eng}</i></label>
										<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?= $nomedrec_pasien.' - '.$nama_pasien ?>" required>
									</div>
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="diagnosa">{ttl_ina} / <i class="text-muted">{ttl_eng}</i></label>
										<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?=HumanDateShort($ttl_pasien).' - '.$umur_pasien ?> Tahun" required>
									</div>
									
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-6 ">
										<div class="col-md-12 ">
											<label for="dengan_diagnosa">{diagnosa_per_ina} / <i class="text-muted">{diagnosa_per_eng}</i></label>
											<textarea class="form-control js-summernote auto_blur" id="dengan_diagnosa" width="100%"  rows="2" placeholder="Dengan Diagnosa"> <?=$dengan_diagnosa?></textarea>
										</div>
									</div>
									<div class="col-md-6 ">
										<div class="col-md-12 ">
											<label for="fisio">{tindakan_ina} / <i class="text-muted">{tindakan_eng}</i></label>
											<textarea class="form-control js-summernote auto_blur" id="rencana_tindakan"  width="100%"  rows="2" placeholder="Catatan"> <?=$rencana_tindakan?></textarea>
										</div>
									</div>
							</div>
							<div class="form-group">
								
								<div class="col-md-6 ">
									<div class="col-md-4 ">
										<label for="diagnosa">{tipe_ina} <br><i class="text-muted">{tipe_eng}</i></label>
										<select tabindex="13" id="tipe" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($tipe == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<option value="3" <?=($tipe == 3 ? 'selected="selected"' : '')?>>RAWAT INAP</option>
											<option value="4" <?=($tipe == 4 ? 'selected="selected"' : '')?>>ODS</option>
											
										</select>
										
									</div>
									<div class="col-md-3 ">
										<label for="example-input-normal">{rencana_masuk_ina}<br><i class="text-muted">{rencana_masuk_eng}</i></label>
										<div class="input-group date">
											<input tabindex="2" type="text" class="js-datepicker form-control auto_blur_tgl" data-date-format="dd/mm/yyyy" id="tanggalbedah" placeholder="HH/BB/TTTT" name="tanggalbedah" value="<?= $tanggalbedah ?>" required>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="col-md-3 ">
										<label for="example-input-normal">&nbsp;<br><i class="text-muted">&nbsp;</i></label>
										<div class="input-group">
											<input tabindex="3" type="text" class="time-datepicker form-control auto_blur_tgl" id="waktubedah" name="waktubedah" value="<?= $waktubedah ?>" required>
											<span class="input-group-addon"><i class="si si-clock"></i></span>
										</div>
									</div>
									
									
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="example-input-normal">{dokter_ina}<br><i class="text-muted">{dokter_eng}</i></label>
										<select tabindex="12" id="dpjp" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($dpjp == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
											<option value="<?=$row->id?>" <?=($dpjp==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								
							</div>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal">{catatan_ina}<br><i class="text-muted">{catatan_eng}</i></label>
										<input tabindex="17" type="text" id="catatan" class="form-control auto_blur " value="<?=$catatan?>" required>
									</div>
									
								</div>
							
							</div>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										
									</div>
									
								</div>
							
							</div>
							<br>
							<br>
							<br>
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left  <?=($assesmen_id==''?'active in':'')?>" id="tab_2">
					
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">{judul_per_ina} SAYA</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_my_order" placeholder="No Pendaftaran / No Pengajuan" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_my_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_my_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_my_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_my_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tipe</label>
											<div class="col-md-8">
												<select tabindex="13" id="tipe_my_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All</option>
													<option value="3">RAWAT INAP</option>
													<option value="4">ODS</option>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="dpjp_filter">DPJP</label>
											<div class="col-md-8">
												<select tabindex="12" id="dpjp_my_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All </option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_perencanaan_bedah()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_my_order">
											<thead>
												<tr>
													<th width="12%">Action</th>
													<th width="8%">No.Perencanaan</th>
													<th width="8%">No Pendaftaran</th>
													<th width="9%">Tipe Perencanaan</th>
													<th width="15%">Dokter Bedah</th>
													<th width="15%">Tanggal Bedah</th>
													<th width="10%">Diagnosa</th>
													<th width="10%">Keterangan</th>
													<th width="12%">Pembuat</th>
													<th width="10%">Status</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					<?php echo form_open_multipart('#', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					
					<div id="div_filter">
						<div class="row">
								<div class="form-group">
									<div class="col-md-12 ">
										<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_per_ina}</h4>
									</div>
									
								</div>
								<div class="form-group">
									<div class="col-md-6 ">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
											<div class="col-md-8">
												<input type="text" class="form-control" id="notransaksi_his_order" placeholder="No Pendaftaran / No Pengajuan" value="">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tgl_daftar_his_order" name="tgl_daftar" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tgl_daftar_2_his_order" name="tgl_daftar_2" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tanggal Pengajuan</label>
											<div class="col-md-8">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_input_1_his_order" name="tanggal_input_1" placeholder="From" value=""/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_input_2_his_order" placeholder="To" value=""/>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-6">
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="tanggal">Tipe</label>
											<div class="col-md-8">
												<select tabindex="13" id="tipe_his_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All</option>
													<option value="3">RAWAT INAP</option>
													<option value="4">ODS</option>
													
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 10px;">
											<label class="col-md-3 control-label" for="dpjp_filter">DPJP</label>
											<div class="col-md-8">
												<select tabindex="12" id="dpjp_his_order" class="js-select2 form-control  " style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>All </option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
													
												</select>
											</div>
										</div>
										
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-3 control-label" for="btn_filter_all"></label>
											<div class="col-md-8">
												<button class="btn btn-success text-uppercase" type="button" onclick="list_perencanaan_bedah_history()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_history_perencanaan">
											<thead>
												<tr>
													<th width="12%">Action</th>
													<th width="8%">No.Perencanaan</th>
													<th width="8%">No Pendaftaran</th>
													<th width="9%">Tipe Perencanaan</th>
													<th width="15%">Dokter Bedah</th>
													<th width="15%">Tanggal Bedah</th>
													<th width="12%">Diagnosa</th>
													<th width="12%">Keterangan</th>
													<th width="12%">Pembuat</th>
													<th width="10%">Status</th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<hr class="push-5-b">
						<!--BATS AKRHI -->
					
					<?php echo form_close() ?>					
				</div>
				
			</div>
		</div>
	</div>
<?}?>
</div>
<?}?>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_assesmen()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var class_obat;
$(document).ready(function() {
	$(".btn_close_left").click(); 
	$(".number").number(true,0,'.',',');
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	disable_edit();
	if ($("#assesmen_id").val()){
		$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	}else{
		list_perencanaan_bedah();
	}
	
});
function disable_edit(){
	if (status_assemen=='2'){
		$("#form_input :input").prop("disabled", true);
		$(".btn_cetak").removeAttr('disabled');
	}
}
function save_edit_assesmen(){
		let jml_edit=$("#jml_edit").val();
		let materi_edukasi=$("#materi_edukasi").val();
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		let assesmen_detail_id=$("#assesmen_detail_id").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_ranap/save_edit_assesmen_rencana_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					jml_edit:jml_edit,
					assesmen_id:assesmen_detail_id,
					alasan_id:alasan_id,
					materi_edukasi:materi_edukasi,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#cover-spin").show();
					let pendaftaranid=data.pendaftaran_id;
					window.location.href = "<?php echo site_url('Tpoliklinik_ranap/tindakan/"+pendaftaranid+"/erm_perencanaan/input_perencanaan_bedah'); ?>";
				}
			}
		});
	}
// $("#idpoli_my_order").change(function(){
		// $.ajax({
			// url: '{site_url}tkonsul/find_dokter/',
			// dataType: "json",
			// method: "POST",
			// data : {
					// idpoliklinik:$(this).val(),
				   // },
			// success: function(data) {
				// // alert(data);
				// $("#iddokter_my_order").empty();
				// $("#iddokter_my_order").append(data);
			// }
		// });

// });
function close_assesmen(){
	
	swal({
		title: "Notifikasi",
		text : "Apakah Anda Yakin telah selesai membuat Perencanaan? Data permintaan Anda akan otomatis terkirim",
		icon : "question",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#status_assemen").val(2);
		
		simpan_assesmen();
	});		

	
}
function hapus_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_hapus").modal('show');
	document.getElementById("modal_hapus").style.zIndex = "1201";
	$("#alasan_id").select2({
		dropdownParent: $("#modal_hapus")
	  });
	// document.getElementById("alasan_id").style.zIndex = "1202";
}
function hapus_record_assesmen(){
	$("#modal_hapus").modal('hide');
	let id=$("#assesmen_detail_id").val();
	// alert(id);
	let keterangan_hapus=$("#keterangan_hapus").val();
	let alasan_id=$("#alasan_id").val();
	
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Inputan Assesmen ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_ranap/hapus_record_rencana_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					keterangan_hapus:keterangan_hapus,
					alasan_id:alasan_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					list_perencanaan_bedah();
					list_perencanaan_bedah_history();
					$("#modal_hapus").modal('hide');
				}
			}
		});
	});

}
function copy_history_assesmen(id){
	let template_assesmen_id=id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Assesmen Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_ranap/create_with_template_rencana_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function edit_history_assesmen(id){
	$("#assesmen_detail_id").val(id);
	$("#modal_edit").modal('show');
	document.getElementById("modal_edit").style.zIndex = "1201";
	$("#alasan_id_edit").select2({
		dropdownParent: $("#modal_edit")
	  });
}
function create_assesmen(){
	
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe=$("#idtipe").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
	let st_ranap=$("#st_ranap").val();
	let tglpendaftaran=$("#tglpendaftaran").val();
	let waktupendaftaran=$("#waktupendaftaran").val();
	// let template_id=$("#template_id").val();
	let idpasien=$("#idpasien").val();
	let iddokter=$("#iddokter").val();
	// alert(iddokter);
	let template='Baru';
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Perencanaan "+template+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_ranap/create_assesmen_perencanaan_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id:pendaftaran_id,
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					st_ranap:st_ranap,
					tglpendaftaran:tglpendaftaran,
					idpasien:idpasien,
					iddokter:iddokter,
					idtipe:idtipe,
					idpoliklinik:idpoliklinik,
					
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});

}
function batal_assesmen(){
	let assesmen_id=$("#assesmen_id").val();
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membatalkan Pengajuan Perencanaan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_ranap/batal_assesmen_rencana_bedah', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
				
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
				location.reload();
			}
		});
	});

}
$(".opsi_change ").change(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur_tgl").change(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur_tgl").blur(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
$(".auto_blur").blur(function(){
		if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
});
 $(document).find('.js-summernote').on('summernote.blur', function() {
	if ($("#st_edited").val()=='0'){
			simpan_assesmen();
		}
  });
function simpan_assesmen(){
		if (status_assemen !='2'){
		let assesmen_id=$("#assesmen_id").val();
		// alert(konsul_id);return false;
		if (assesmen_id){
			// console.log('SIMPAN');
			
			$.ajax({
				url: '{site_url}Tpoliklinik_ranap/simpan_assesmen_rencana_bedah', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:$("#assesmen_id").val(),
						dengan_diagnosa : $("#dengan_diagnosa").val(),
						rencana_tindakan : $("#rencana_tindakan").val(),
						tipe : $("#tipe").val(),
						dpjp : $("#dpjp").val(),
						tanggalbedah : $("#tanggalbedah").val(),
						waktubedah : $("#waktubedah").val(),
						catatan : $("#catatan").val(),
						status_assemen : $("#status_assemen").val(),
						st_edited : $("#st_edited").val(),
					},
				success: function(data) {
							console.log(data);
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						if (data.status_assemen=='1'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
						}else{
							if (data.status_assemen=='2'){
								$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
								$("#cover-spin").show();
								// alert('sini');
								location.reload();			
							}
							
						}
					}
				}
			});
		}
	}
}

function list_perencanaan_bedah(){
	$("#div_history").hide();
	$("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_my_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_my_order").val();
	let dpjp=$("#dpjp_my_order").val();
	let tipe=$("#tipe_my_order").val();
	let notransaksi=$("#notransaksi_my_order").val();
	let tanggal_input_1=$("#tanggal_input_1_my_order").val();
	let tanggal_input_2=$("#tanggal_input_2_my_order").val();
	$('#index_my_order').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_my_order').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9] },
					 { "width": "10%", "targets": [0,4,,6,7] },
					 { "width": "15%", "targets": [8] },
					 { "width": "8%", "targets": [1,2,3,5,9] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpoliklinik_ranap/list_perencanaan_bedah', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						dpjp:dpjp,
						tipe:tipe,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}
function list_perencanaan_bedah_history(){
	// $("#div_history").hide();
	// $("#div_filter").show();
	let assesmen_id=$("#assesmen_id").val();
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let tgl_daftar_1=$("#tgl_daftar_his_order").val();
	let tgl_daftar_2=$("#tgl_daftar_2_his_order").val();
	let dpjp=$("#dpjp_his_order").val();
	let tipe=$("#tipe_his_order").val();
	let notransaksi=$("#notransaksi_his_order").val();
	let tanggal_input_1=$("#tanggal_input_1_his_order").val();
	let tanggal_input_2=$("#tanggal_input_2_his_order").val();
	$('#index_history_perencanaan').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#index_history_perencanaan').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			columnDefs: [
					// {  className: "text-right", targets:[0] },
					{  className: "text-center", targets:[1,2,3,6,4,0,7,5,6,7,8,9] },
					 { "width": "10%", "targets": [0,4,,6,7] },
					 { "width": "15%", "targets": [8] },
					 { "width": "8%", "targets": [1,2,3,5,9] },
					 // { "width": "10%", "targets": [1,2,3,5] },
					 // { "width": "15%", "targets": [4] },
				],
			ajax: { 
				url: '{site_url}Tpoliklinik_ranap/list_perencanaan_bedah_history', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						idpasien:idpasien,
						notransaksi:notransaksi,
						dpjp:dpjp,
						tipe:tipe,
						tgl_daftar_1:tgl_daftar_1,
						tgl_daftar_2:tgl_daftar_2,
						tanggal_input_1:tanggal_input_1,
						tanggal_input_2:tanggal_input_2,
						pendaftaran_id:pendaftaran_id,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	$("#div_history").hide();
}

function copy_order(assesmen_id){
	let template_assesmen_id=assesmen_id;
	let pendaftaran_id=$("#pendaftaran_id").val();
	let idpasien=$("#idpasien").val();
	let idpoliklinik=$("#idpoliklinik").val();
	let idtipe=$("#idtipe_poli").val();
	let idtarif_header=$("#idtarif_header").val();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Membuat Perencanaan Dari Duplikasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tpoliklinik_ranap/copy_order', 
			dataType: "JSON",
			method: "POST",
			data : {
					template_assesmen_id:template_assesmen_id,
					pendaftaran_id:pendaftaran_id,
					idpasien:idpasien,
					idpoliklinik:idpoliklinik,
					idtipe:idtipe,
					idtarif_header:idtarif_header,
				   },
			success: function(data) {
				
				// $("#cover-spin").hide();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
				location.reload();
			}
		});
	});
}
</script>