<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<style>
	
	.edited_final{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	.edited2{
		color:red;
		font-weight: bold;
	}
	.edited_final {
		background-color:#ffb9b3;
		font-weight: bold;
	}
	.auto_blur{
		background-color:#fdffe2;
	}
	.auto_input{
		background-color:#fdffe2;
	}
	.has-error {
		border-color: #d26a5c;
		
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>
<?if ($menu_kiri=='his_estimasi_biaya'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='his_estimasi_biaya' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
		// echo $tanggal_permintaan;
	if ($assesmen_id){
		$tanggalcreated=HumanDateShort($created_date);
		$waktucreated=HumanTime($created_date);
		$tanggalpermintaan=HumanDateShort($tanggal_permintaan);
		$waktupermintaan=HumanTime($tanggal_permintaan);
		
		
	}else{
		$tanggalpermintaan=date('d-m-Y');
		$waktupermintaan=date('H:i:s');
		$tanggalcreated=date('d-m-Y');
		$waktucreated=date('H:i:s');
		
	}
	$dengan_diagnosa=trim(strip_tags($dengan_diagnosa));
	$dengan_diagnosa_asal=trim(strip_tags($dengan_diagnosa_asal));
	
	$rencana_tindakan_asal=trim(strip_tags($rencana_tindakan_asal));
	$rencana_tindakan=trim(strip_tags($rencana_tindakan));
	?>
<?if ($st_lihat_estimasi_biaya=='1'){?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rm">
	
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="si si-paper-clip"></i> <?=($versi_edit=='0'?'ORIGINAL':'PERUBAHAN KE-'.$versi_edit)?></a>
				</li>
				
			</ul>
			
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
											<a href="{site_url}tpoliklinik_ranap/tindakan/<?=$pendaftaran_id?>/erm_perencanaan/input_estimasi_biaya" class="btn btn-default btn_kembali_ass" type="button"><i class="fa fa-reply"></i> Kembali</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<input type="hidden" id="idtipe_poli" value="{idtipe_poli}"> 
						<input type="hidden" id="idkelompokpasien" value="{idkelompokpasien}"> 
						<input type="hidden" id="idpoliklinik" value="{idpoliklinik}"> 
						<input type="hidden" id="idrekanan" value="{idrekanan}"> 
						<input type="hidden" id="iddokter" value="{iddokter}"> 
						<input type="hidden" id="assesmen_id" value="{assesmen_id}"> 
					

						<input type="hidden" id="status_assemen" value="<?=$status_assemen?>" >		
						<input type="hidden" id="trx_id" value="<?=$trx_id?>" >		
						<input type="hidden" id="iddokter" value="<?=$iddokter?>" >		
						<input type="hidden" id="idtipe" value="<?=$idtipe?>" >		
						<input type="hidden" id="assesmen_detail_id" value="" >		
						<input type="hidden" id="jml_edit" value="<?=$jml_edit?>" >		
						<input type="hidden" id="st_edited" value="<?=$st_edited?>" >		
						<input type="hidden" id="iddokter" value="<?=$iddokter?>" >		
						<input type="hidden" id="versi_edit" value="<?=$versi_edit?>" >		
						
						<div class="form-group">
							<div class="col-md-6 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" disabled class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tanggalcreated" placeholder="HH/BB/TTTT" name="tanggalcreated" value="<?= $tanggalcreated ?>" required>
										<label for="tanggalcreated">Tanggal <i class="text-muted">Created Date</i></label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" disabled  class="time-datepicker form-control " id="waktucreated" name="waktucreated" value="<?= $waktucreated ?>" required>
										<label for="waktupendaftaran">Waktu <i class="text-muted">Time</i></label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="col-md-12 col-xs-12">
									<div class="form-group" style="margin-bottom: 10px;">
										<div class="col-md-12">
											<div class="form-material">
												<div class="input-group">
													<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
												</div>
												<label >Nama Profesional Pemberi Asuhan (PPA) </label>
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-12">
							<?if($st_lihat_eresep=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_per_ina}</h4>
								<h5 class="push-5 text-center">{judul_per_eng}</h5>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Tanggal</label>
										<div class="col-xs-12">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_1" placeholder="From" value="">
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control  his_filter" type="text" id="filter_ttv_tanggal_2" placeholder="To" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="st_owned">Owned By Me</label>
										<div class="col-xs-12">
											<select id="st_owned" name="st_owned" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1">YA</option>
												<option value="0">TIDAK</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_profesi_id">Profesi</label>
										<div class="col-xs-12">
											<select id="filter_profesi_id" class="js-select2 form-control  his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 push-5">
									<div class="form-group">
										<label class="col-xs-12" for="filter_ppa_id">Edited By Person</label>
										<div class="col-xs-12">
											
											<div class="input-group">
												<select id="filter_ppa_id" class="js-select2 form-control his_filter" style="width: 100%;" data-placeholder="Choose one.." multiple>
												<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
												<span class="input-group-btn">
													<button class="btn btn-success his_filter" type="button" onclick="list_index_history_edit()" id="btn_filter_ttv"><i class="fa fa-search"></i> Cari Data</button>
												</span>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_pencarian_history">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="20%">Versi</th>
												<th width="20%">Created</th>
												<th width="20%">Edited</th>
												<th width="25%">Alasan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
					
						<?if ($assesmen_id!=''){?>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="diagnosa">{nama_pasien_ina} / <i class="text-muted">{nama_pasien_eng}</i></label>
										<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?= $nomedrec_pasien.' - '.$nama_pasien.' - '.$jk_pasien ?>" required>
									</div>
									
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="diagnosa">{ttl_ina} / <i class="text-muted">{ttl_eng}</i></label>
										<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?=HumanDateShort($ttl_pasien).' - '.$umur_pasien ?> Tahun" required>
									</div>
									
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-6 ">
										<div class="col-md-12 ">
											<label for="dengan_diagnosa">{diagnosa_per_ina} / <i class="text-muted">{diagnosa_per_eng}</i></label>
											<textarea class="form-control <?=($dengan_diagnosa!=$dengan_diagnosa_asal?'edited':'')?>" id="dengan_diagnosa" width="100%"  rows="4" placeholder="Dengan Diagnosa"> <?=$dengan_diagnosa?></textarea>
										</div>
									</div>
									<div class="col-md-6 ">
										<div class="col-md-12 ">
											<label for="fisio">{nama_tindakan_ina} / <i class="text-muted">{nama_tindakan_eng}</i></label>
											<textarea class="form-control <?=($rencana_tindakan!=$rencana_tindakan_asal?'edited':'')?>" id="rencana_tindakan"  width="100%"  rows="4" placeholder="Catatan"> <?=$rencana_tindakan?></textarea>
										</div>
									</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px!important">
								<div class="col-md-6 ">
									<div class="col-md-4 ">
										<label for="diagnosa">{tipe_ina} <br><i class="text-muted">{tipe_eng}</i></label>
										<select tabindex="13" id="tipe" class="<?=($tipe!=$tipe_asal?'edited':'')?> form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($tipe == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<option value="3" <?=($tipe == 3 ? 'selected="selected"' : '')?>>RAWAT INAP</option>
											<option value="4" <?=($tipe == 4 ? 'selected="selected"' : '')?>>ODS</option>
											
										</select>
										
									</div>
									<div class="col-md-4 ">
										<label for="icu">{icu_ina} <br><i class="text-muted">{icu_eng}</i></label>
										<select tabindex="8" id="icu" name="icu" class="<?=($icu!=$icu_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(122) as $row){?>
											<option value="<?=$row->id?>" <?=($icu == $row->id ? 'selected="selected"' : '')?> <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
									<div class="col-md-4 ">
										<label for="cito">{cito_ina} <br><i class="text-muted">{cito_eng}</i></label>
										<select tabindex="8" id="cito" name="cito" class="<?=($cito!=$cito_asal?'edited':'')?> form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach(list_variable_ref(123) as $row){?>
											<option value="<?=$row->id?>" <?=($icu == $row->id ? 'selected="selected"' : '')?> <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12 ">
										<label for="example-input-normal">{dokter_ina}<br><i class="text-muted">{dokter_eng}</i></label>
										<select tabindex="12" id="dpjp" class="<?=($dpjp!=$dpjp_asal?'edited':'')?> form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="" <?=($dpjp == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
											<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
											<option value="<?=$row->id?>" <?=($dpjp==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								
							</div>
							<div class="form-group"  style="margin-bottom: 25px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal">{catatan_ina}<br><i class="text-muted">{catatan_eng}</i></label>
										<input tabindex="17" type="text" id="catatan" class="form-control <?=($catatan!=$catatan_asal?'edited':'')?> " value="<?=$catatan?>" required>
									</div>
									
								</div>
							
							</div>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal">{rencana_implant_ina}<br><i class="text-muted">{rencana_implant_eng}</i></label>
									</div>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom: 15px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="table-responsive">
											<table class="table table-bordered table-striped" id="index_implant">
												<thead>
													<tr>
														<th width="5%" class="text-center">{table_no_ina}<br><i class="text-muted">{table_no_eng}</i></th>
														<th width="15%" class="text-center">{table_alat_ina}<br><i class="text-muted">{table_alat_eng}</i></th>
														<th width="8%" class="text-center">{table_ukuran_ina}<br><i class="text-muted">{table_ukuran_eng}</i></th>
														<th width="10%" class="text-center">{table_merk_ina}<br><i class="text-muted">{table_merk_eng}</i></th>
														<th width="20%" class="text-center">{table_catatan_ina}<br><i class="text-muted">{table_catatan_eng}</i></th>
														<th width="8%" class="text-center">{table_jumlah_ina}<br><i class="text-muted">{table_jumlah_eng}</i></th>
														<th width="10%" class="text-center">{table_biaya_ina}<br><i class="text-muted">{table_biaya_eng}</i></th>
														<th width="15%" class="text-center">Keterangan<br><i class="text-muted"></i></th>										   
													</tr>
													
												</thead>
												<tbody></tbody>
											</table>
										</div>	
									</div>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom: 5px!important">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<label for="example-input-normal">{table_footer_ina}<br><i class="text-muted">{table_footer_eng}</i></label>
									</div>
								</div>
							</div>
						<?}?>
					</div>	
						<!--BATS AKRHI -->
					<hr class="push-5-b">
					
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
	</div>
<?}?>
</div>
<?}?>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var status_assemen=$("#status_assemen").val();
var load_awal_assesmen=true;
var nama_template;
var idpasien=$("#idpasien").val();
var before_edit;
var class_obat;
$(document).ready(function() {
	$(".btn_close_left").click(); 
	$(".number").number(true,0,'.',',');
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	let assesmen_id=$("#assesmen_id").val();
	disable_edit();
	if ($("#assesmen_id").val()){
		$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
		load_implant();
		list_index_history_edit();
	}
	
});
function disable_edit(){
	if (status_assemen=='2'){
		$('.js-summernote').removeClass('js-summernote');
		$("#form_input :input").prop("disabled", true);
		$(".btn_kolom").prop("disabled", true);
		// $('#dengan_diagnosa').summernote('disable');
		$(".btn_cetak").removeAttr('disabled');
		$(".his_filter").removeAttr('disabled');
		$(".edited").removeClass("edited").addClass("edited_final");
	}
}
function load_implant(){
	$('#index_implant').DataTable().destroy();	
	table = $('#index_implant').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}thistory_tindakan/load_implant_estimasi_biaya', 
                type: "POST" ,
                dataType: 'json',
				data : {
						assesmen_id: $("#assesmen_id").val(),
						versi_edit: $("#versi_edit").val(),
					   }
            }
        });
}
function list_index_history_edit(){
	let assesmen_id=$("#assesmen_id").val();
	let filter_ppa_id=$("#filter_ppa_id").val();
	let filter_profesi_id=$("#filter_profesi_id").val();
	let st_owned=$("#st_owned").val();
	let tanggal_1=$("#filter_ttv_tanggal_1").val();
	let tanggal_2=$("#filter_ttv_tanggal_2").val();
	let versi_edit=$("#versi_edit").val();
	$('#index_pencarian_history').DataTable().destroy();	
	$("#cover-spin").show();
	// alert(ruangan_id);
	table = $('#index_pencarian_history').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}thistory_tindakan/list_index_his_estimasi_biaya', 
				type: "POST" ,
				dataType: 'json',
				data : {
						assesmen_id:assesmen_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						filter_ppa_id:filter_ppa_id,
						filter_profesi_id:filter_profesi_id,
						st_owned:st_owned,
						versi_edit:versi_edit,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
	
}
</script>