<style>
.auto_blur{
		background-color:#fdffe2;
	}
	.select2-container--default .select2-selection--single{
		background-color: #fdffe2;
	}
	
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	
	
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

   .text-muted {
	  color: #919191;
	  font-weight: normal;
	  
	}
	
</style>
<?if ($menu_kiri=='input_observasi'){?>
<div class="col-sm-10 menu_kanan" style="display: <?=($menu_kiri=='input_observasi' ? 'block' : 'none') ?>;">
	<!-- Music -->
	<button class="btn btn-default push-10-t push-10-l btn_close_left" type="button"><i class="fa fa-ellipsis-v"></i></button>
	<?
					if ($assesmen_id){
						$tanggaldaftar=HumanDateShort($tanggal_input);
						$waktudaftar=HumanTime($tanggal_input);
					}else{
						$waktudaftar=date('H:i:s');
						$tanggaldaftar=date('d-m-Y');
						
					}
						$disabel_input='';
						if ($st_input_observasi=='0'){
							$disabel_input='disabled';
						}
						$disabel_cetak='';
						if ($st_cetak_observasi=='0'){
							$disabel_cetak='disabled';
						}
						
						
					?>
	<div class="block animated fadeIn push-5-t" data-category="erm_rj">
	<?if ($st_lihat_observasi=='1'){?>
		<div class="block">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active">
					<a href="#tab_1"><i class="fa fa-thermometer-0"></i> Pengkajian Baru</a>
				</li>
				<?if ($status_assemen!='2'){?>
				<li class="">
					<a href="#tab_2" onclick="list_history_pengkajian()" ><i class="fa fa-history"></i> Riwayat Pengkajian</a>
				</li>
				<li class="">
					<a href="#tab_3" onclick="list_index_template()"><i class="fa fa-copy"></i> Template</a>
				</li>
				<li >
					<a href="#tab_4" onclick="list_balance()"><i class="si si-graph"></i> Balance</a>
				</li>
				<?}?>
				
			</ul>
			<div class="block-content tab-content">
				<div class="tab-pane fade fade-left active in" id="tab_1">
					
					<?php echo form_open_multipart('tobservasi/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
					<div class="row">
						<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
						<input type="hidden" readonly id="ttv_detail_id" value="">
						<input type="hidden" readonly id="st_sedang_edit" value="0">
						<input type="hidden" readonly id="st_ranap" value="{st_ranap}">
						<input type="hidden" readonly id="st_edited" value="{st_edited}">
						<div class="col-md-12">
							<?if($st_lihat_observasi=='1'){?>
								<h4 class="font-w700 push-5 text-center text-primary">{judul_header}</h4>
							<?}else{?>
								<h4 class="font-w700 push-5 text-center text-primary"><i class="fa fa-ban text-danger"></i> TIDAK ADA AKSES</h4>
							<?}?>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-4 ">
								<?if ($status_assemen=='1' && $st_edited=='0'){?>
									<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)"> Document Baru Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='1' && $st_edited=='1'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='1'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info Document <a class="alert-link" href="javascript:void(0)"> Editing Belum Disimpan</a>!</p>
									</div>
								<?}?>
								<?if ($status_assemen=='3'){?>
									<div class="alert alert-danger alert-dismissable" id="peringatan_assesmen" style="display:<?=($status_assemen=='3'?'block':'none')?>">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<p>Info <a class="alert-link" href="javascript:void(0)">Template Belum Disimpan</a>!</p>
									</div>
								<?}?>
							</div>
							<div class="col-md-8 ">
								<div class="pull-right push-10-r">
									<?if ($assesmen_id==''){?>
										<?if ($st_input_observasi=='1'){?>
										<button class="btn btn-primary" id="btn_create_assesmen" onclick="create_assesmen()" type="button"><i class="si si-doc"></i> New </button>
										<?}?>
									<?}?>
									<?if ($assesmen_id!=''){?>
										<?if ($status_assemen=='3'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_template()"  type="button"><i class="fa fa-times"></i> Batalkan Template</button>
										<?}?>
										<?if ($status_assemen=='1'){?>
										<button class="btn btn-danger" id="btn_hapus_assesmen" onclick="batal_assesmen()"  type="button"><i class="fa fa-times"></i> Batalkan</button>
										<button class="btn btn-success" id="btn_simpan_observasi"  type="button"><i class="fa fa-floppy-o"></i> SIMPAN</button>
										<?}?>
										
										<?if ($status_assemen=='3'){?>
											<button class="btn btn-warning" type="button" onclick="simpan_template()"><i class="fa fa-floppy-o"></i> Simpan Template</button>
										<?}?>
										
									<?}?>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group" style="margin-top:20px">
							<div class="col-md-8 col-xs-12">
								<div class="col-md-3 col-xs-6">
									<div class="form-material input-group date">
										<input tabindex="2" type="text" class="js-datepicker form-control " data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										<label for="tanggaldaftar">Tanggal </label>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-md-2 col-xs-6">
									<div class="form-material input-group">
										<input tabindex="3" type="text" <?=$disabel_input?> class="time-datepicker form-control " id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										<label for="waktupendaftaran">Waktu</label>
										<span class="input-group-addon"><i class="si si-clock"></i></span>
									</div>
								</div>
								<div class="col-md-7 col-xs-6">
									<div class="form-material input-group">
										<select id="template_assesmen_id" <?=($status_assemen?'disabled':'')?> name="template_assesmen_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($template_assesmen_id==''?'selected':'')?>>-Belum dipilih-</option>
											<?foreach($list_template_assement as $r){?>
											<option value="<?=$r->id?>" <?=($template_assesmen_id==$r->id?'selected':'')?>><?=$r->nama_template?></option>
											<?}?>
											
										</select>
										<label for="waktupendaftaran">Template Pengkajian </label>
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btn_create_with_template" disabled <?=($st_input_pews!='1'?'disabled':'')?> onclick="create_with_template(0)"><i class="fa fa-plus"></i> Template</button>
										</span>
									</div>
								</div>
								
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-12">
										<div class="form-material">
											<div class="input-group">
												<input class="form-control" disabled type="text" readonly value="<?=$login_nip_ppa?> - {login_nama_ppa}" id="nama_ppa" name="nama_ppa" placeholder="Nama PPA" required>
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
											</div>
											<label >Nama Profesional Pemberi Asuhan (PPA) </label>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
						<?if ($assesmen_id){?>
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-6">
									<label for="tingkat_kesadaran">Tingkat Kesadaran </label>
									<select id="tingkat_kesadaran"  name="tingkat_kesadaran" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(23) as $row){?>
										<option value="<?=$row->id?>" <?=($tingkat_kesadaran == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-3">
									<label for="tanggaldaftar">Suhu </label>
									<div class="input-group">
										<input class="form-control decimal auto_blur" <?=$disabel_input?> type="text" id="suhu" name="suhu" value="{suhu}"  placeholder="Suhu Tubuh" required>
										<span class="input-group-addon"><?=$satuan_suhu?></span>
									</div>
									
								</div>
								<div class="col-md-3">
									<label for="tanggaldaftar">Nadi </label>
									<div class="input-group">
										<input class="form-control decimal auto_blur" <?=$disabel_input?>  type="text" id="nadi" name="nadi" value="{nadi}" placeholder="Nadi" required>
										<span class="input-group-addon"><?=$satuan_nadi?></span>
									</div>
									
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-6">
									<label for="tanggaldaftar">Tekanan Darah </label>
									<div class="input-group">
										<input class="form-control decimal auto_blur" <?=$disabel_input?> type="text" id="td_sistole"  value="{td_sistole}" name="td_sistole" placeholder="Sistole" required>
										<span class="input-group-addon"><?=$satuan_td?> / </span>
										<input class="form-control decimal auto_blur" <?=$disabel_input?> type="text" id="td_diastole" value="{td_diastole}"  name="td_diastole" placeholder="Diastole" required>
										<span class="input-group-addon"><?=$satuan_td?></span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="tanggaldaftar">Pernafasan </label>
									<div class="input-group">
										<input class="form-control decimal auto_blur" <?=$disabel_input?>  type="text" id="nafas" name="nafas" value="{nafas}"  placeholder="Frekuensi Nafas" required>
										<span class="input-group-addon"><?=$satuan_nafas?></span>
									</div>
									
								</div>
								<div class="col-md-3">
									<label for="tanggaldaftar">SPO2 </label>
									<div class="input-group">
										<input class="form-control decimal auto_blur" <?=$disabel_input?>  type="text" id="spo2" name="spo2" value="{spo2}"  placeholder="SpO2" required>
										<span class="input-group-addon"><?=$satuan_spo2?></span>
									</div>
									
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-2">
									<label for="intek_jenis">Jenis Intek </label>
									<select  id="intek_jenis" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($intek_jenis == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(423) as $row){?>
										<option value="<?=$row->id?>" <?=($intek_jenis == $row->id ? 'selected="selected"' : '')?> <?=($intek_jenis == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-2">
									<label for="intek_jenis">Intek </label>
									<input class="form-control decimal auto_blur" style="width: 100%;" <?=$disabel_input?> type="text" id="intek"  value="{intek}" name="intek" placeholder="Intek" required>
								</div>
								<div class="col-md-2">
									<label for="intek">Satuan Intek </label>
									<select  id="satuan_intek" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Satuan Intek" required>
										<option value="" <?=($satuan_intek == '' ? 'selected="selected"' : '')?>>Satuan Intek</option>
										<?foreach(list_variable_ref(425) as $row){?>
										<option value="<?=$row->id?>" <?=($satuan_intek == $row->id ? 'selected="selected"' : '')?> <?=($satuan_intek == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-2">
									<label for="output">Jenis Output </label>
									<select  id="output" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="" <?=($output == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(424) as $row){?>
										<option value="<?=$row->id?>" <?=($output == $row->id ? 'selected="selected"' : '')?> <?=($output == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
								<div class="col-md-2">
									<label for="output">Output </label>
									<input class="form-control decimal auto_blur" style="width: 100%;" <?=$disabel_input?> type="text" id="jumlah_output"  value="{jumlah_output}" name="jumlah_output" placeholder="Output" required>
								</div>
								<div class="col-md-2">
									<label for="jumlah_output">Output </label>
									<select  id="satuan_output" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Satuan Output" required>
										<option value="" <?=($satuan_output == '' ? 'selected="selected"' : '')?>>Satuan Intek</option>
										<?foreach(list_variable_ref(425) as $row){?>
										<option value="<?=$row->id?>" <?=($satuan_output == $row->id ? 'selected="selected"' : '')?> <?=($satuan_output == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12">
									<label for="tanggaldaftar">Keterangan </label>
									<input class="form-control auto_blur" style="width: 100%;" <?=$disabel_input?> type="text" id="keterangan"  value="{keterangan}" name="keterangan" placeholder="Keterangan" required>
								</div>
							</div>
						</div>
						
						<div class="col-md-12">
							<span class="text-primary"><i>{judul_footer}</i></span>
						</div>
						<?}?>
					</div>
					
					<div class="block block-content-narrow" >
						<div class="row push-10-t" >
							<input type="hidden" readonly class="form-control input-sm" id="assesmen_id" value="{assesmen_id}"> 
							<div class="col-md-12">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								
							</div>
						</div>
						
						<div class="row push-10-t">
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Tanggal</label>
											<div class="col-xs-12">
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="filter_observasi_tanggal_1" placeholder="From" value="">
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="filter_observasi_tanggal_2" placeholder="To" value="">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Owned By Me</label>
											<div class="col-xs-12">
												<select id="st_owned" name="st_owned" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0" selected>TIDAK</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Profesi</label>
											<div class="col-xs-12">
												<select id="filter_profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4 push-5">
										<div class="form-group">
											<label class="col-xs-12" for="ruangan_id">Add By Person</label>
											<div class="col-xs-12">
												
												<div class="input-group">
                                                    <select id="filter_ppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
													<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
														<option value="<?=$r->id?>" ><?=$r->nama?></option>
													<?}?>
												</select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-success" type="button" onclick="load_index_observasi()" id="btn_filter_observasi"><i class="fa fa-search"></i> Cari Data</button>
                                                    </span>
                                                </div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="form-group ">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table" id="index_observasi">
											<thead>
												<tr>
													<th width="10%"></th>
													<th width="10%"></th>
													<th width="10%"></th>
													<th width="10%"></th>
												   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
						
						
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_2">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">RIWAYAT {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-6 ">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<select id="idrawat_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua -</option>
												<?if ($st_ranap=='1'){?>
												<option value="<?=$pendaftaran_id_ranap?>" >Dirawat Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('trawatinap_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id_ranap ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
												<?}else{?>
												<option value="<?=$pendaftaran_id?>" >Poliklinik Saat Ini <?=$nopendaftaran?></option>
												<?foreach(get_all('tpoliklinik_pendaftaran',array('status >' => 0,'id <> '=>$pendaftaran_id ,'idpasien'=>$idpasien)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nopendaftaran.' - '.HumanDateShort($r->tanggaldaftar)?></option>
												<?}?>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idkelas">Kelas</label>
										<div class="col-md-8">
											<select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Kelas -</option>
												<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tgl_daftar" name="tgl_daftar" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tgl_daftar_2" name="tgl_daftar_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Yang Melakukan Pengkajian</label>
										<div class="col-md-8">
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua PPA -</option>
												<?foreach($list_ppa as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
										<div class="col-md-8">
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Poliklinik -</option>
												<option value="1" >Rawat Inap</option>
												<option value="2" >ODS</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idruang">Ruang Perawatan</label>
										<div class="col-md-8">
											<select id="idruang" name="idruang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Ruangan -</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="idbed">Bed</label>
										<div class="col-md-8">
											<select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Bed -</option>
												<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Dokter</label>
										<div class="col-md-8">
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>- Semua Dokter -</option>
												<?foreach($list_dokter as $r){?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 control-label" for="tanggal">Tanggal Pengkajian</label>
										<div class="col-md-8">
											<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
												<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
												<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
												<input class="form-control" type="text" id="tanggal_input_2" placeholder="To" value=""/>
											</div>
										</div>
									</div>
									<div class="form-group" style="margin-top: 15px;">
										<label class="col-md-3 control-label" for="btn_filter_all"></label>
										<div class="col-md-8">
											<button class="btn btn-success text-uppercase" type="button" onclick="list_history_pengkajian()" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_history_kajian">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Action</th>
												<th width="10%">Tanggal Daftar</th>
												<th width="10%">No Pendaftaran</th>
												<th width="15%">Tujuan</th>
												<th width="10%">Tanggal Pengkajian</th>
												<th width="10%">Yang Melakukan Pengkajian</th>
												<th width="15%">Informasi Perubahan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_3">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">TEMPLATE {judul_header}</h4>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="pull-left">
										<button class="btn btn-info " type="button" <?=($st_input_pews!='1'?'disabled':'')?> onclick="create_template()"><i class="fa fa-plus"></i> Template</button>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_template_assemen">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Action</th>
												<th width="60%">Nama Template</th>
												<th width="15%">Jumlah Penggunaan</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
				<div class="tab-pane fade fade-left " id="tab_4">
					
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<h4 class="font-w700 push-5 text-center text-primary">BALANCE {judul_header}</h4>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 ">
									<div class="col-md-12 ">
										<div class="form-group" style="margin-bottom: 5px;">
											<div class="col-md-4 ">
												<label class="control-label" for="tanggal_balance_1">Tanggal</label>
												<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
													<input class="form-control" type="text" id="tanggal_balance_1"  placeholder="From" value="<?=date('d-m-Y')?>"/>
													<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
													<input class="form-control" type="text" id="tanggal_balance_2" placeholder="To" value="<?=date('d-m-Y')?>"/>
												</div>
											</div>
											<div class="col-md-2">
												<label class="control-label" for="st_waktu">Waktu</label>
												<select id="st_waktu" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" >DITENTUKAN</option>
													<option value="0" selected>TIDAK DITENTUKAN</option>
												</select>
											</div>
											<div class="col-md-2 div_tentukan">
												<label class="control-label" for="shift_id">Jenis</label>
												<select id="shift_id" name="shift_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="" selected>Pilih Opsi</option>
													<?foreach(list_variable_ref(295) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
												</select>
											</div>
											<div class="col-md-3 div_tentukan">
												<label class="control-label" for="tanggal">&nbsp;</label>
												<div class="input-group">
													<input  type="text" class="time-datepicker form-control " id="waktu_1" value="" required>
													<span class="input-group-addon"><i class="si si-clock"></i></span>
													<input  type="text" class="time-datepicker form-control " id="waktu_2" value="" required>
												</div>
											</div>
											<div class="col-md-1">
												<label class="control-label" for="tanggal">&nbsp;</label>
												<button class="btn btn-success text-uppercase" type="button" onclick="list_balance()" id="btn_filter_balancec" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-2">
												<label class="control-label" for="tanggal_balance_1">TOTAL INTAKE</label>
												<input class="form-control" readonly type="text" id="total_intake" value="" >
											</div>
											<div class="col-md-2">
												<label class="control-label" for="tanggal_balance_1">TOTAL OUTPUT</label>
												<input class="form-control " readonly type="text" id="total_output" value="" >
											</div>
											<div class="col-md-2">
												<label class="control-label" for="tanggal_balance_1">BALANCE</label>
												<input class="form-control " readonly type="text" id="total_balance" value="" >
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div id="div_balance"></div>
							</div>
						</div>
					<?php echo form_close() ?>
					
				</div>
			</div>
		</div>
		<?}?>
	</div>
	<!-- END Music -->
	</div>
<?}?>
<div class="modal in" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN TTV</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_observasi()" id="btn_hapus"><i class="fa fa-refresh"></i> Hapus</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">ALASAN EDIT TTV</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id_edit" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_edit',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Edit</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_edit"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_edit">Keterangan Edit</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="save_edit_observasi()" id="btn_hapus"><i class="fa fa-refresh"></i> Proses</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_history_edit" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">HISTORY PERUBAHAN INPUT TTV</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="table-responsive">
									<table class="table" id="index_observasi_perubahan">
										<thead>
											<tr>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_savae_template_assesmen" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Simpan Template Assesmen</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input class="form-control " type="text" id="nama_template" value="{nama_template}"  placeholder="Nama Template" >
									<label for="nama_template">Nama Template Assesmen</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" onclick="close_modal()" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="close_template()" id="btn_hapus"><i class="fa fa-save"></i> Simpan Template</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		$("#cover-spin").hide();
		// document.getElementById("nadi").style.color = "#eb0404";
		$(".time-datepicker").datetimepicker({
			format: "HH:mm:ss"
		});
		set_tentukan();
		load_index_observasi();
	});
	function list_balance(){
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let tgl_filter_1=$("#tanggal_balance_1").val();
		let tgl_filter_2=$("#tanggal_balance_2").val();
		let st_waktu=$("#st_waktu").val();
		let shift_id=$("#shift_id").val();
		let waktu_1=$("#waktu_1").val();
		let waktu_2=$("#waktu_2").val();
		let st_ranap=$("#st_ranap").val();
		$.ajax({
			url: '{site_url}tobservasi/list_balance', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					pendaftaran_id:pendaftaran_id,
					assesmen_id:assesmen_id,
					tanggal_1:tgl_filter_1,
					tanggal_2:tgl_filter_2,
					st_waktu:st_waktu,
					shift_id:shift_id,
					waktu_1:waktu_1,
					waktu_2:waktu_2,
					st_ranap:st_ranap,
					
					},
			success: function(data) {
				$("#cover-spin").hide();
				$("#div_balance").html(data.tabel);
				$("#total_intake").val(data.total_intek);
				$("#total_output").val(data.total_output);
				$("#total_balance").val(data.total_balance);
				$('#index_balance').DataTable({
					scrollX: true,
					// autoWidth: false,
					// searching: true,
					// serverSide: true,
					"processing": false,
					"order": [],
					"pageLength": 100,
					"ordering": false,
				
					
				});
				disabel_edit();
			}
		});
	}
	function close_template(){
		if ($("#nama_template").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Template", "error");
			return false;
		}
		$("#modal_savae_template_assesmen").modal('hide');
		swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menyimpan Template ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$("#status_assemen").val(4);
				nama_template=$("#nama_template").val();
				simpan_observasi(4);
			});		
		
	}
	function simpan_template(){
		$("#modal_savae_template_assesmen").modal('show');
		
	}
	function create_template(){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let mpews_id=$("#mpews_id_2").val();
		let idpasien=$("#idpasien").val();
		let st_ranap=$("#st_ranap").val();
		let template='Baru';
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Template "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tobservasi/create_template_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						pendaftaran_id:pendaftaran_id,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						idpasien:idpasien,
						mpews_id:mpews_id,
						st_ranap:st_ranap,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});
	}
	function list_index_template(){
		let assesmen_id=$("#assesmen_id").val();
		$('#index_template_assemen').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_template_assemen').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						 { "width": "5%", "targets": [0] },
						 { "width": "20%", "targets": [1] },
						 { "width": "15%", "targets": [3] },
						 { "width": "60%", "targets": [2] }
					],
				ajax: { 
					url: '{site_url}tobservasi/list_index_template_observasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function create_assesmen(){
	
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		let st_ranap=$("#st_ranap").val();
		let tglpendaftaran=$("#tglpendaftaran").val();
		let waktupendaftaran=$("#waktupendaftaran").val();
		let tipe_rj_ri=$("#opsi_tipe_rj_ri").val();
		// let template_id=$("#template_id").val();
		let idpasien=$("#idpasien").val();
		
		
		let template='Baru';
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen "+template+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tobservasi/create_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						pendaftaran_id:pendaftaran_id,
						tglpendaftaran:tglpendaftaran,
						waktupendaftaran:waktupendaftaran,
						idpasien:idpasien,
						st_ranap:st_ranap,
					   },
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
					
				}
			});
		});

	}
	function edit_observasi(id){
		$("#ttv_detail_id").val(id);
		$("#modal_edit").modal('show');
		document.getElementById("modal_edit").style.zIndex = "1201";
		$("#alasan_id_edit").select2({
			dropdownParent: $("#modal_edit")
		  });
	}
	function save_edit_observasi(){
		let keterangan_edit=$("#keterangan_edit").val();
		let alasan_id=$("#alasan_id_edit").val();
		$("#st_sedang_edit").val(1);
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_edit").modal('hide');
		let assesmen_id=$("#assesmen_id").val();
		let id=$("#ttv_detail_id").val();
		$("#cover-spin").show();
		
		$.ajax({
			url: '{site_url}tobservasi/save_edit_observasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
					alasan_id:alasan_id,
					keterangan_edit:keterangan_edit,
				},
			success: function(data) {
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Gagal!",
						text: "Edit.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					let pendaftaran_id=data.pendaftaran_id;
					let st_ranap=data.st_ranap;
					let pendaftaran_id_ranap=data.pendaftaran_id_ranap;
					$("#cover-spin").show();
					if (st_ranap=='1'){
					window.location.href = "<?php echo site_url('tobservasi/tindakan/"+pendaftaran_id_ranap+"/1/erm_observasi/input_observasi'); ?>";
						
					}else{
					window.location.href = "<?php echo site_url('tobservasi/tindakan/"+pendaftaran_id+"/0/erm_observasi/input_observasi'); ?>";
						
					}
					
					
				}
			}
		});
	}
	// function copy_observasi(id){
		// $("#cover-spin").show();
		// $.ajax({
			// url: '{site_url}tobservasi/copy_observasi', 
			// dataType: "JSON",
			// method: "POST",
			// data : {
					// id:id,
				// },
			// success: function(data) {
				
						// $("#cover-spin").hide();
				// if (data==null){
					// swal({
						// title: "Gagal!",
						// text: "Load Data TTV.",
						// type: "error",
						// timer: 1500,
						// showConfirmButton: false
					// });

				// }else{
						// $("#assesmen_id").val('');
						// $("#tglpendaftaran").val(data.tglpendaftaran);
						// $("#waktupendaftaran").val(data.waktupendaftaran);
						// $("#tingkat_kesadaran").val(data.tingkat_kesadaran).trigger('change.select2');
						// $("#nadi").val(data.nadi);
						// $("#nafas").val(data.nafas);
						// $("#td_sistole").val(data.td_sistole);
						// $("#td_diastole").val(data.td_diastole);
						// $("#suhu").val(data.suhu);
						// $("#intek_jenis").val(data.intek_jenis);
						// $("#intek").val(data.intek);
					// if (data.status_assemen=='1'){
						// $("#peringatan_observasi").show();
						// $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Draft'});
					// }else{
						// $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
						// $("#peringatan_observasi").removeClass('alert-warning').addClass('alert-success');
						// $("#peringatan_observasi").html('info : Status Duplikasi inputan Baru');
						// $("#peringatan_observasi").show();
						// // location.reload();			
					// }
				// }
			// }
		// });
	// }
	function load_index_observasi(){
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let pendaftaran_id=$("#pendaftaran_id").val();
		$('#index_observasi').DataTable().destroy();	
		let st_owned=$("#st_owned").val();
		let profesi_id=$("#filter_profesi_id").val();
		let tanggal_1=$("#filter_observasi_tanggal_1").val();
		let tanggal_2=$("#filter_observasi_tanggal_1").val();
		let ppa_id=$("#filter_ppa_id").val();
		let st_sedang_edit=$("#st_sedang_edit").val();
		let st_ranap=$("#st_ranap").val();
		
		// alert(ruangan_id);
		table = $('#index_observasi').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				"columnDefs": [
						{ "width": "10%", "targets": 0},
						{ "width": "40%", "targets": 1},
						{ "width": "40%", "targets": 2},
						{ "width": "10%", "targets": 3},
						// { "width": "10%", "targets": 3},
						
					],
				ajax: { 
					url: '{site_url}tobservasi/index_observasi_history', 
					type: "POST" ,
					dataType: 'json',
					data : {
							pendaftaran_id:pendaftaran_id,
							st_ranap:st_ranap,
							pendaftaran_id_ranap:pendaftaran_id_ranap,
							st_owned:st_owned,
							profesi_id:profesi_id,
							tanggal_1:tanggal_1,
							tanggal_2:tanggal_2,
							ppa_id:ppa_id,
							st_sedang_edit:st_sedang_edit,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#index_observasi thead").remove();
				 }  
			});
		$("#cover-spin").hide();
	}
	$("#btn_simpan_observasi").click(function(){
		let login_ppa_id=$("#login_ppa_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tanggaldaftar=$("#tglpendaftaran").val();
		let waktudaftar=$("#waktupendaftaran").val();
		let tingkat_kesadaran=$("#tingkat_kesadaran").val();
		let nadi=$("#nadi").val();
		let nafas=$("#nafas").val();
		let td_sistole=$("#td_sistole").val();
		let td_diastole=$("#td_diastole").val();
		let suhu=$("#suhu").val();
		let intek_jenis=$("#intek_jenis").val();
		let intek=$("#intek").val();
		
		if (tanggaldaftar==''){
			sweetAlert("Maaf...", "Tentukan Tanngal", "error");
			return false;
		}
		if (tingkat_kesadaran=='' || tingkat_kesadaran==null){
			sweetAlert("Maaf...", "Tentukan  Tingkat Kesadaran", "error");
			return false;
		}
		if (nadi==''){
			sweetAlert("Maaf...", "Tentukan  Frekuensi nadi", "error");
			return false;
		}
		if (nafas==''){
			sweetAlert("Maaf...", "Tentukan  Frekuensi nafas", "error");
			return false;
		}
		if (td_sistole==''){
			sweetAlert("Maaf...", "Tentukan  Tekanan Darah sistole", "error");
			return false;
		}
		if (td_diastole==''){
			sweetAlert("Maaf...", "Tentukan  Tekanan Darah diastole", "error");
			return false;
		}
		if (suhu==''){
			sweetAlert("Maaf...", "Tentukan Suhu Badan", "error");
			return false;
		}
		if (intek_jenis==''){
			sweetAlert("Maaf...", "Tentukan  Tinggi Badan", "error");
			return false;
		}
		if (intek==''){
			sweetAlert("Maaf...", "Tentukan Berat Badan", "error");
			return false;
		}
		$("#cover-spin").show();
		simpan_observasi(2);	
	});
	$(document).on("blur",".auto_blur",function(){
		console.log('sni');
		if ($("#st_edited").val()=='0'){
			simpan_observasi(1);
		}
	});
	$(document).on("change",".opsi_change",function(){
		console.log('sni');
		if ($("#st_edited").val()=='0'){
			simpan_observasi(1);
		}
	});

	// $(".auto_blur").blur(function() {
		
	// });
	// $(".opsi_change").blur(function() {
		// console.log('sni');
		// if ($("#st_edited").val()=='0'){
		// simpan_observasi(1);
		// }
	// });
	function simpan_observasi(status_assemen){
		let login_ppa_id=$("#login_ppa_id").val();
		let assesmen_id=$("#assesmen_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let tanggaldaftar=$("#tglpendaftaran").val();
		let waktudaftar=$("#waktupendaftaran").val();
		let tingkat_kesadaran=$("#tingkat_kesadaran").val();
		let nadi=$("#nadi").val();
		let nafas=$("#nafas").val();
		let td_sistole=$("#td_sistole").val();
		let td_diastole=$("#td_diastole").val();
		let suhu=$("#suhu").val();
		let spo2=$("#spo2").val();
		let intek_jenis=$("#intek_jenis").val();
		let intek=$("#intek").val();
		let satuan_intek=$("#satuan_intek").val();
		let output=$("#output").val();
		let jumlah_output=$("#jumlah_output").val();
		let satuan_output=$("#satuan_output").val();
		let keterangan=$("#keterangan").val();
		let st_edited=$("#st_edited").val();
		let nama_template=$("#nama_template").val();
		// alert(st_edited);
		if (status_assemen=='2'){
			$("#cover-spin").show();
			
		}
		$.ajax({
			url: '{site_url}tobservasi/save_observasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					pendaftaran_id_ranap:pendaftaran_id_ranap,
					login_ppa_id:login_ppa_id,
					tanggal_input:tanggaldaftar,
					waktudaftar:waktudaftar,
					tingkat_kesadaran:tingkat_kesadaran,
					nadi:nadi,
					nafas:nafas,
					spo2:spo2,
					td_sistole:td_sistole,
					td_diastole:td_diastole,
					suhu:suhu,
					intek_jenis:intek_jenis,
					intek:intek,
					status_assemen:status_assemen,
					assesmen_id:assesmen_id,
					satuan_intek:satuan_intek,
					output:output,
					jumlah_output:jumlah_output,
					satuan_output:satuan_output,
					keterangan:keterangan,
					st_edited:st_edited,
					nama_template:nama_template,
					
				},
			success: function(data) {
				
						console.log(data);
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Simpan TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					if (data.status_assemen=='1'){
						$("#cover-spin").hide();
						$("#peringatan_observasi").show();
						$("#peringatan_observasi").removeClass('alert-Succes').addClass('alert-warning');
						$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Draft'});
					}else{
						if (data.status_assemen=='3'){
							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Template'});
						}else{
							$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Finish'});
							$("#cover-spin").show();
							location.reload();			
						}
							
					}
				}
			}
		});
	}
	function edit_template_assesmen(assesmen_id){
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		// alert(pendaftaran_id);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Template?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tobservasi/edit_template_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						
					},
				success: function(data) {
					
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Simpan Assesmen.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
						$("#cover-spin").show();
						location.reload();	
					}
				}
			});
		});			
		
		
	}
	function list_history_pengkajian(){
		let assesmen_id=$("#assesmen_id").val();
		let idpasien=$("#idpasien").val();
		let tgl_daftar_1=$("#tgl_daftar").val();
		let tgl_daftar_2=$("#tgl_daftar_2").val();
		let mppa_id=$("#mppa_id").val();
		let iddokter=$("#iddokter").val();
		let idrawat_ranap=$("#idrawat_ranap").val();
		let notransaksi=$("#notransaksi").val();
		let tanggal_input_1=$("#tanggal_input_1").val();
		let tanggal_input_2=$("#tanggal_input_2").val();
		let idtipe=$("#idtipe").val();
		let idruang=$("#idruang").val();
		let idbed=$("#idbed").val();
		let idkelas=$("#idkelas").val();
		let st_ranap=$("#st_ranap").val();
		$('#index_history_kajian').DataTable().destroy();	
		// $("#cover-spin").show();
		// alert(ruangan_id);
		table = $('#index_history_kajian').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,5,6,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "10%", "targets": [1,2,3,5] },
						 { "width": "15%", "targets": [4] },
					],
				ajax: { 
					url: '{site_url}tobservasi/list_history_pengkajian_observasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							assesmen_id:assesmen_id,
							idpasien:idpasien,
							notransaksi:notransaksi,
							iddokter:iddokter,
							mppa_id:mppa_id,
							tgl_daftar_1:tgl_daftar_1,
							tgl_daftar_2:tgl_daftar_2,
							tanggal_input_1:tanggal_input_1,
							tanggal_input_2:tanggal_input_2,
							idrawat_ranap:idrawat_ranap,
							idtipe:idtipe,
							idruang:idruang,
							idbed:idbed,
							idkelas:idkelas,
							st_ranap:st_ranap,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		
	}
	function batal_assesmen(){
		let assesmen_id=$("#assesmen_id").val();
		let st_edited=$("#st_edited").val();
		let jml_edit=$("#jml_edit").val();
		
		let template=$("#template_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Assesment ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tobservasi/batal_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						st_edited:st_edited,
						jml_edit:jml_edit,
					
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
					location.reload();
				}
			});
		});

	}
	function batal_template(){
		let assesmen_id=$("#assesmen_id").val();
		let nama_template=$("#nama_template").val();
		
		let template=$("#template_id option:selected").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membatalkan Assesment ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tobservasi/batal_template_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						assesmen_id:assesmen_id,
						nama_template:nama_template,
					
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Canceled'});
					location.reload();
				}
			});
		});

	}
	function gunakan_template_assesmen(id){
	// $("#template_assesmen_id").val(id).trigger('change');
		copy_observasi(id);
	}
	function hapus_template_assesmen(id){
	
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Template Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tobservasi/hapus_record_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						keterangan_hapus:'HAPUS TEMPLATE',
						alasan_id:null,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_index_template();
				}
			});
		});

	}
	function hapus_observasi(id){
		$("#ttv_detail_id").val(id);
		$("#modal_hapus").modal('show');
		document.getElementById("modal_hapus").style.zIndex = "1201";
		$("#alasan_id").select2({
			dropdownParent: $("#modal_hapus")
		  });
		// document.getElementById("alasan_id").style.zIndex = "1202";
	}
	
	function hapus_record_observasi(){
		let id=$("#ttv_detail_id").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Assesmen ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tobservasi/hapus_record_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						$('#index_observasi').DataTable().ajax.reload( null, false );
						$("#modal_hapus").modal('hide');
						$("#st_sedang_edit").val(0);
					}
				}
			});
		});

	}
	function copy_observasi(id){
		let template_assesmen_id='';
		if (id=='0'){
			template_assesmen_id=$("#template_assesmen_id").val();
		}else{
			template_assesmen_id=id;
		}
		// alert(template_assesmen_id);
		// return false;
		let pendaftaran_id=$("#pendaftaran_id").val();
		let pendaftaran_id_ranap=$("#pendaftaran_id_ranap").val();
		let idpasien=$("#idpasien").val();
		let st_ranap=$("#st_ranap").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Membuat Assesmen Dari template ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tobservasi/create_with_template_obsesvasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						template_assesmen_id:template_assesmen_id,
						pendaftaran_id:pendaftaran_id,
						pendaftaran_id_ranap:pendaftaran_id_ranap,
						idpasien:idpasien,
						st_ranap:st_ranap,
					   },
				success: function(data) {
					
					// $("#cover-spin").hide();
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Created'});
					location.reload();
				}
			});
		});

	}
	function hapus_langsung(id){
			$.ajax({
				url: '{site_url}tobservasi/hapus_observasi', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						$('#index_observasi').DataTable().ajax.reload( null, false );
					}
				}
			});

	}
	function lihat_perubahan(id){
		$("#modal_history_edit").modal('show');
		document.getElementById("modal_history_edit").style.zIndex = "1201";
		$('#index_observasi_perubahan').DataTable().destroy();	
		table = $('#index_observasi_perubahan').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				"columnDefs": [
						{ "width": "10%", "targets": 0},
						{ "width": "40%", "targets": 1},
						{ "width": "40%", "targets": 2},
						{ "width": "10%", "targets": 3},
						// { "width": "10%", "targets": 3},
						
					],
				ajax: { 
					url: '{site_url}tobservasi/index_observasi_perubahan', 
					type: "POST" ,
					dataType: 'json',
					data : {
							id:id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#index_observasi thead").remove();
				 }  
			});
		$("#cover-spin").hide();
	}
	$("#st_waktu").change(function(){
		set_tentukan();
	});
	$("#shift_id").change(function(){
		let id=$("#shift_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tobservasi/find_shift/'+id,
			dataType: "json",
			success: function(data) {
				$("#cover-spin").hide();
				$("#waktu_1").val(data.waktu_1);
				$("#waktu_2").val(data.waktu_2);
			}
		});
	});
	function set_tentukan(){
		if ($("#st_waktu").val()=='1'){
			$(".div_tentukan").show();
		}else{
			$(".div_tentukan").hide();
			
		}
	}
</script>