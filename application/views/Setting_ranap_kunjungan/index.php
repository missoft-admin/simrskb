<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1919'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_kunjungan()"> PENGATURAN KUNJUNGAN</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1920'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2" onclick="load_tipe()"> LOGIC TIPE PASIEN</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1921'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3" onclick="load_form()"> LOGIC FORM</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1922'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="set_tab(4)"> LEMBAR MASUK KELUAR</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1923'))){ ?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5" onclick="set_tab(5)"> SURAT PERNYATAAN</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1932'))){ ?>
		<li class="<?=($tab=='6'?'active':'')?>">
			<a href="#tab_6" onclick="load_terima_all()"> PENERIMAAN RANAP</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1919'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('SETTING PENGATURAN KUNJUNGAN')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_kunjungan">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="25%">Asal Pasien</th>
											<th width="30%">Poliklinik</th>
											<th width="30%">Dokter</th>
											<th width="10%">Action</th>										   
										</tr>
										<?$asal_pasien='#';?>
										<tr>
											<th>#<input type="hidden" value="" id="kunjungan_id"></th>
											<th>
												<select id="asal_pasien" name="asal_pasien" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="0" <?=($asal_pasien=='0'?'selected':'')?>>-SEMUA-</option>
													<option value="1" <?=($asal_pasien=='1'?'selected':'')?>>POLIKLINIK</option>
													<option value="2" <?=($asal_pasien=='2'?'selected':'')?>>IGD</option>
												</select>
											</th>
											<th>
												<select id="idpoli" name="idpoli" style="width:100%"  class="js-select2 form-control" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="iddokter" name="iddokter" class="js-select2 form-control"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<button class="btn btn-primary btn-sm"  type="button" id="btn_tambah_kunjungan" onclick="add_kunjungan()"><i class="fa fa-plus"></i> Simpan</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1932'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='6'?'active in':'')?> " id="tab_6">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('SETTING PENERIMAAN')?></h5>
						<input type="hidden" class="form-control" id="arr_sound" placeholder="No Urut" name="arr_sound" value="">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="col-md-2">
								<label for="st_play">Play Sound Penerimaan</label>
								<select id="st_play" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="1" <?=($st_play == '1' ? 'selected="selected"' : '')?>>YA</option>
									<option value="0" <?=($st_play == '0' ? 'selected="selected"' : '')?>>MUTE</option>
									
								</select>							
							</div>
							
						</div>
					
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_sound">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Urutan</th>
											<th width="65%">Asset Sound</th>
											<th width="20%">Action</th>										   
										</tr>
										<input id="idsound" type="hidden" value="">
										<tr>
											<th>#</th>
											<th>
												<input tabindex="0" type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut">
											</th>
											<th>
												<select id="sound_id"  name="sound_id" class="js-select2 form-control" style="width: 100%;">
													<option value="#" selected>-Pilih Asset Sound-</option>
													<?foreach($list_sound as $r){?>
													  <option value="<?=$r->id?>" ><?=$r->nama_asset?> (<?=$r->file_sound?>)</option>
													<?}?>
												</select>
											
											</th>
											
											<th>
												<div class="btn-group">
													<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_sound"><i class="fa fa-save"></i></button>
													<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_sound"><i class="fa fa-refresh"></i></button>
													<button class="btn btn-danger btn-sm"  title="Play All" type="button" id="btn_play_all"><i class="fa fa-play"></i></button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('LOGIC PENERIMAAN')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_terima">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="15%">Tipe Pasien</th>
											<th width="20%">Ruangan</th>
											<th width="20%">Kelas</th>
											<th width="20%">Penerimaan Pasien</th>
											<th width="10%">Action</th>										   
										</tr>
										<tr>
											<th>#<input type="hidden" value="" id="kunjungan_terima_id"></th>
											<th>
												<select id="idtujuan_terima" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="0" selected>-SEMUA-</option>
													<option value="3" >RAWAT INAP</option>
													<option value="4" >ODS</option>
													
												</select>
											</th>
											
											<th>
												<select id="idruangan_terima" name="idruangan_terima" class="js-select2 form-control"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mruangan',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idkelas_terima" name="idkelas_terima" class="js-select2 form-control"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="st_terima_ranap" name="st_terima_ranap" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="1">Otomatis Terima</option>
													<option value="0">Manual</option>
												</select>
											</th>
											
											<th>
												<button class="btn btn-primary btn-sm"  type="button" id="btn_tambah_terima" onclick="add_terima()"><i class="fa fa-plus"></i> Simpan</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1920'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?> " id="tab_2">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('SETTING PENGATURAN LOGIC TIPE PASIEN')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="col-md-2">
								<label for="st_tipe_pasien_edit">Tipe Pasien Dapat Diedit </label>
								<select id="st_tipe_pasien_edit" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="1" <?=($st_tipe_pasien_edit == '1' ? 'selected="selected"' : '')?>>YA</option>
									<option value="2" <?=($st_tipe_pasien_edit == '2' ? 'selected="selected"' : '')?>>TIDAK</option>
									
								</select>							
							</div>
							
						</div>
					
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_tipe">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Asal Pasien</th>
											<th width="10%">Poliklinik</th>
											<th width="10%">Dokter</th>
											<th width="10%">Kelompok Pasien</th>
											<th width="10%">Tipe Rekanan</th>
											<th width="15%">Nama Perusahaan</th>
											<th width="10%">Tujuan</th>
											<th width="10%">Tipe Pasien</th>
											<th width="10%">Action</th>										   
										</tr>
										<?$asal_pasien_tipe='#';?>
										<tr>
											<th>#<input type="hidden" value="" id="kunjungan_tipe_id"></th>
											<th>
												<select id="asal_pasien_tipe" name="asal_pasien_tipe" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="0" <?=($asal_pasien_tipe=='0'?'selected':'')?>>-SEMUA-</option>
													<option value="1" <?=($asal_pasien_tipe=='1'?'selected':'')?>>POLIKLINIK</option>
													<option value="2" <?=($asal_pasien_tipe=='2'?'selected':'')?>>IGD</option>
												</select>
											</th>
											<th>
												<select id="idpoli_tipe" name="idpoli_tipe" style="width:100%"  class="js-select2 form-control" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="iddokter_tipe" name="iddokter_tipe" class="js-select2 form-control"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idkelompokpasien_tipe" name="idkelompokpasien_tipe" class="js-select2 form-control "  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="tipe_rekanan_tipe" name="tipe_rekanan_tipe" class="js-select2 form-control div_asuransi"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(list_variable_ref(117) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idrekanan_tipe" name="idrekanan_tipe" class="js-select2 form-control div_asuransi"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mrekanan',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idtujuan_tipe" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="0" selected>-SEMUA-</option>
													<option value="3" >RAWAT INAP</option>
													<option value="4" >ODS</option>
													
												</select>
											</th>
											<th>
												<select id="idtipepasien_tipe" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="0" >-PILIH-</option>
													<option value="1" >PASIEN RS</option>
													<option value="2" >PASIEN PRIBADI</option>
												</select>
											</th>
											<th>
												<button class="btn btn-primary btn-sm"  type="button" id="btn_tambah_tipe" onclick="add_tipe()"><i class="fa fa-plus"></i> Simpan</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1921'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('SETTING FORM')?></h5>
						</div>
					</div>
				</div>
				
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_form">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Tipe Pasien</th>
											<th width="10%">Dokter</th>
											<th width="10%">Lembar Masuk</th>
											<th width="10%">Lembar Pernyataan</th>
											<th width="15%">General Consent</th>
											<th width="10%">Hak & Kewajiban</th>
											<th width="10%">Action</th>										   
										</tr>
										<tr>
											<th>#<input type="hidden" value="" id="kunjungan_form_id"></th>
											<th>
												<select id="idtujuan_form" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="0" selected>-SEMUA-</option>
													<option value="3" >RAWAT INAP</option>
													<option value="4" >ODS</option>
													
												</select>
											</th>
											
											<th>
												<select id="iddokter_form" name="iddokter_form" class="js-select2 form-control"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="st_lembar_masuk_form" name="st_lembar_masuk" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0">TIDAK</option>
												</select>
											</th>
											<th>
												<select id="st_pernyataan_form" name="st_pernyataan" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0">TIDAK</option>
												</select>
											</th>
											<th>
												<select id="st_general_form" name="st_general" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0">TIDAK</option>
												</select>
											</th>
											<th>
												<select id="st_hak_kewajiban_form" name="st_hak_kewajiban" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="1">YA</option>
													<option value="0">TIDAK</option>
												</select>
											</th>
											
											<th>
												<button class="btn btn-primary btn-sm"  type="button" id="btn_tambah_form" onclick="add_form()"><i class="fa fa-plus"></i> Simpan</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1922'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?> " id="tab_4">
			<?php echo form_open_multipart('setting_ranap_kunjungan/save_label_lembar','class="js-form1 validation form-horizontal" id="form-work"') ?>
				<div class="col-md-12">
			<div class="row">
				<?if ($tab=='4'){?>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<img class="img-avatar"  id="output_img" src="{upload_path}logo_setting/{logo_form}" />
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Logo Header (100x100)</label>
								<div class="box">
									<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo_form" value="{logo_form}" />
									<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
					</div>
				<?}?>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Alamat</label>
								<input type="text" class="form-control" name="alamat_form" value="{alamat_form}">
							</div>
							<div class="col-md-6">
								<label>Telepon</label>
								<input type="text" class="form-control" name="telepone_form" value="{telepone_form}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-12">
								<label>Web / Email</label>
								<input type="text" class="form-control" name="email_form" value="{email_form}">
							</div>
							
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Judul</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="judul_ina" value="{judul_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="judul_eng" value="{judul_eng}">
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Label Header Identitas</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="header_ina" value="{header_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="header_eng" value="{header_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No Registrasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="no_register_ina" value="{no_register_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="no_register_eng" value="{no_register_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No rekam Medis</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="no_rekam_medis_ina" value="{no_rekam_medis_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="no_rekam_medis_eng" value="{no_rekam_medis_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Nama Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="nama_pasien_ina" value="{nama_pasien_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="nama_pasien_eng" value="{nama_pasien_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tanggal Lahir</label>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="ttl_ina" value="{ttl_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="ttl_eng" value="{ttl_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="umur_ina" value="{umur_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="umur_eng" value="{umur_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="jk_ina" value="{jk_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="jk_eng" value="{jk_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Pekerjaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="pekerjaan_ina" value="{pekerjaan_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="pekerjaan_eng" value="{pekerjaan_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Pendidikan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="pendidikan_ina" value="{pendidikan_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="pendidikan_eng" value="{pendidikan_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Agama</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="agama_ina" value="{agama_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="agama_eng" value="{agama_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Alamat</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="alamat_ina" value="{alamat_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="alamat_eng" value="{alamat_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Provinsi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="provinsi_ina" value="{provinsi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="provinsi_eng" value="{provinsi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kabupaten</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kab_ina" value="{kab_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kab_eng" value="{kab_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kecamatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kec_ina" value="{kec_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kec_eng" value="{kec_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kelurahan / Desa</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kel_ina" value="{kel_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kel_eng" value="{kel_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kode Pos</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kode_pos_ina" value="{kode_pos_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kode_pos_eng" value="{kode_pos_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">RT</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="rt_ina" value="{rt_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="rt_eng" value="{rt_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">RW</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="rw_ina" value="{rw_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="rw_eng" value="{rw_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Warga Negara</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="wn_ina" value="{wn_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="wn_eng" value="{wn_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Telephone</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="telephone_ina" value="{telephone_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="telephone_eng" value="{telephone_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No HP</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="nohp_ina" value="{nohp_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="nohp_eng" value="{nohp_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">NIK</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="nik_ina" value="{nik_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="nik_eng" value="{nik_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Header Identitas Penanggung Jawab</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="label_header_pj_ina" value="{label_header_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="label_header_pj_eng" value="{label_header_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Nama</label>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="nama_pj_ina" value="{nama_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="nama_pj_eng" value="{nama_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tanggal Lahir</label>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="ttl_pj_ina" value="{ttl_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="ttl_pj_eng" value="{ttl_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="umur_pj_ina" value="{umur_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="umur_pj_eng" value="{umur_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="jk_pj_ina" value="{jk_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="jk_pj_eng" value="{jk_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Pekerjaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="pekerjaan_pj_ina" value="{pekerjaan_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="pekerjaan_pj_eng" value="{pekerjaan_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Pendidikan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="pendidikan_pj_ina" value="{pendidikan_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="pendidikan_pj_eng" value="{pendidikan_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Agama</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="agama_pj_ina" value="{agama_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="agama_pj_eng" value="{agama_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Alamat</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="alamat_pj_ina" value="{alamat_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="alamat_pj_eng" value="{alamat_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Provinsi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="provinsi_pj_ina" value="{provinsi_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="provinsi_pj_eng" value="{provinsi_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kabupaten</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kab_pj_ina" value="{kab_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kab_pj_eng" value="{kab_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kecamatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kec_pj_ina" value="{kec_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kec_pj_eng" value="{kec_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kelurahan / Desa</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kel_pj_ina" value="{kel_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kel_pj_eng" value="{kel_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kode Pos</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kode_pos_pj_ina" value="{kode_pos_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kode_pos_pj_eng" value="{kode_pos_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">RT</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="rt_pj_ina" value="{rt_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="rt_pj_eng" value="{rt_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">RW</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="rw_pj_ina" value="{rw_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="rw_pj_eng" value="{rw_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Warga Negara</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="wn_pj_ina" value="{wn_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="wn_pj_eng" value="{wn_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Telephone</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="telephone_pj_ina" value="{telephone_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="telephone_pj_eng" value="{telephone_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">NIK</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="nik_pj_ina" value="{nik_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="nik_pj_eng" value="{nik_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Hubungan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="hubungan_pj_ina" value="{hubungan_pj_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="hubungan_pj_eng" value="{hubungan_pj_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Label Detail Pendaftaran</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="detail_pendaftaran_ina" value="{detail_pendaftaran_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="detail_pendaftaran_eng" value="{detail_pendaftaran_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tipe Pelayanan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="tipe_pasien_ina" value="{tipe_pasien_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="tipe_pasien_eng" value="{tipe_pasien_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Asal pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="asal_pasien_ina" value="{asal_pasien_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="asal_pasien_eng" value="{asal_pasien_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Poliklinik</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="poliklinik_ina" value="{poliklinik_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="poliklinik_eng" value="{poliklinik_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tanggal Pendaftaran</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="tgl_pendaftaran_ina" value="{tgl_pendaftaran_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="tgl_pendaftaran_eng" value="{tgl_pendaftaran_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kasus Kepolisian</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kasus_ina" value="{kasus_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kasus_eng" value="{kasus_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Dirawat Ke-</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="dirawat_ke_ina" value="{dirawat_ke_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="dirawat_ke_eng" value="{dirawat_ke_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Cara Masuk</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="cara_masuk_ina" value="{cara_masuk_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="cara_masuk_eng" value="{cara_masuk_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kelompok Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kel_pasien_ina" value="{kel_pasien_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kel_pasien_eng" value="{kel_pasien_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">DPJP Utama</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="dpjp_utama_ina" value="{dpjp_utama_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="dpjp_utama_eng" value="{dpjp_utama_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Ruangan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="ruangan_ina" value="{ruangan_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="ruangan_eng" value="{ruangan_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Kelas</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="kelas_ina" value="{kelas_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="kelas_eng" value="{kelas_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Bed</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="bed_ina" value="{bed_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="bed_eng" value="{bed_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Pernyataaan Persetujuan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="pernyataan_persetujuan_ina" value="{pernyataan_persetujuan_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="pernyataan_persetujuan_eng" value="{pernyataan_persetujuan_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Identifikasi Verifikasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="identifikasi_privasi_ina" value="{identifikasi_privasi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="identifikasi_privasi_eng" value="{identifikasi_privasi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Khsusus Pasien Asuransi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="khusus_asuransi_ina" value="{khusus_asuransi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="khusus_asuransi_eng" value="{khusus_asuransi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Lain-Lain </label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="lain_lain_ina" value="{lain_lain_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="lain_lain_eng" value="{lain_lain_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tanda Tangan Petugas Pendaftaran</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="ttd_pendaftaran_ina" value="{ttd_pendaftaran_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="ttd_pendaftaran_eng" value="{ttd_pendaftaran_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:15px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tanda Tangan Pasien / Keluarga</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="ttd_pasien_ina" value="{ttd_pasien_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="ttd_pasien_eng" value="{ttd_pasien_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:25px">
						<div class="col-md-12">
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
								</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
								<div class="col-md-12">
									<button class="btn btn-danger btn-xs" type="button" id="add_content"><i class="fa fa-plus"></i> Content</button>
								</div>
								<div class="col-md-12">
									<table class="table" id="index_content">
										<thead>
											<tr>
												<th class="text-center" style="width: 50px;">#</th>
												<th>Content</th>
												<th class="hidden-xs" style="width: 15%;">Opsi</th>
												<th class="hidden-xs" style="width: 15%;">Created</th>
												<th class="text-center" style="width: 100px;">Actions</th>
											</tr>
										</thead>
										<tbody>
											
											
										</tbody>
									</table>
								</div>
									
						</div>
					</div>
					
					
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1923'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?> " id="tab_5">
		<?php echo form_open_multipart('setting_ranap_kunjungan/save_pernyataan','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
			<div class="col-md-12">
			<?if ($tab=='5'){?>
				<div class="form-group" style="margin-bottom:5px">
					
					<div class="col-md-12">
						<div class="col-md-6">
							<img class="img-avatar"  id="output_img" src="{upload_path}logo_setting/{logo_form}" />
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Logo Header (100x100)</label>
							<div class="box">
								<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo_form" value="{logo_form}" />
								<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
							</div>
						</div>
					</div>
				</div>
			<?}?>
				<div class="form-group" style="margin-bottom:5px">
					
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Alamat</label>
							<input type="text" class="form-control" name="alamat_form" value="{alamat_form}">
						</div>
						<div class="col-md-6">
							<label>Telepon</label>
							<input type="text" class="form-control" name="telepone_form" value="{telepone_form}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Web / Email</label>
							<input type="text" class="form-control" name="email_form" value="{email_form}">
						</div>
						
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Judul</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="judul_ina" value="{judul_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="judul_eng" value="{judul_eng}">
						</div>
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraf 1</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea style="width:100%" class="form-control" id="paragraf_1_ina" name="paragraf_1_ina" rows="3" placeholder="Content.."><?=$paragraf_1_ina?></textarea>
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea style="width:100%" class="form-control" id="paragraf_1_eng" name="paragraf_1_eng" rows="3" placeholder="Content.."><?=$paragraf_1_eng?></textarea>
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Header Identitas Penanggung Jawab</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_header_pj_ina" value="{label_header_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_header_pj_eng" value="{label_header_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_pj_ina" value="{nama_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_pj_eng" value="{nama_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal Lahir</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttl_pj_ina" value="{ttl_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttl_pj_eng" value="{ttl_pj_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="umur_pj_ina" value="{umur_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="umur_pj_eng" value="{umur_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jk_pj_ina" value="{jk_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jk_pj_eng" value="{jk_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pekerjaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pekerjaan_pj_ina" value="{pekerjaan_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pekerjaan_pj_eng" value="{pekerjaan_pj_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pendidikan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pendidikan_pj_ina" value="{pendidikan_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pendidikan_pj_eng" value="{pendidikan_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Agama</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="agama_pj_ina" value="{agama_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="agama_pj_eng" value="{agama_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Alamat</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="alamat_pj_ina" value="{alamat_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="alamat_pj_eng" value="{alamat_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Provinsi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="provinsi_pj_ina" value="{provinsi_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="provinsi_pj_eng" value="{provinsi_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kabupaten</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kab_pj_ina" value="{kab_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kab_pj_eng" value="{kab_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kecamatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kec_pj_ina" value="{kec_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kec_pj_eng" value="{kec_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kelurahan / Desa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kel_pj_ina" value="{kel_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kel_pj_eng" value="{kel_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kode Pos</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kode_pos_pj_ina" value="{kode_pos_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kode_pos_pj_eng" value="{kode_pos_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">RT</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rt_pj_ina" value="{rt_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rt_pj_eng" value="{rt_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">RW</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rw_pj_ina" value="{rw_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rw_pj_eng" value="{rw_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Warga Negara</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="wn_pj_ina" value="{wn_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="wn_pj_eng" value="{wn_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Telephone</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="telephone_pj_ina" value="{telephone_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="telephone_pj_eng" value="{telephone_pj_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">NIK</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nik_pj_ina" value="{nik_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nik_pj_eng" value="{nik_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Hubungan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="hubungan_pj_ina" value="{hubungan_pj_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="hubungan_pj_eng" value="{hubungan_pj_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraf 2 </label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea style="width:100%" class="form-control" id="paragraf_2_ina" name="paragraf_2_ina" rows="3" placeholder="Content.."><?=$paragraf_2_ina?></textarea>
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea style="width:100%" class="form-control" id="paragraf_2_eng" name="paragraf_2_eng" rows="3" placeholder="Content.."><?=$paragraf_2_eng?></textarea>
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Header Identitas</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="header_ina" value="{header_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="header_eng" value="{header_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">No Registrasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="no_register_ina" value="{no_register_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="no_register_eng" value="{no_register_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">No rekam Medis</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="no_rekam_medis_ina" value="{no_rekam_medis_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="no_rekam_medis_eng" value="{no_rekam_medis_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_pasien_ina" value="{nama_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_pasien_eng" value="{nama_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal Lahir</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttl_ina" value="{ttl_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttl_eng" value="{ttl_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="umur_ina" value="{umur_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="umur_eng" value="{umur_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jk_ina" value="{jk_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jk_eng" value="{jk_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pekerjaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pekerjaan_ina" value="{pekerjaan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pekerjaan_eng" value="{pekerjaan_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pendidikan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pendidikan_ina" value="{pendidikan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pendidikan_eng" value="{pendidikan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Agama</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="agama_ina" value="{agama_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="agama_eng" value="{agama_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Alamat</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="alamat_ina" value="{alamat_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="alamat_eng" value="{alamat_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Provinsi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="provinsi_ina" value="{provinsi_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="provinsi_eng" value="{provinsi_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kabupaten</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kab_ina" value="{kab_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kab_eng" value="{kab_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kecamatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kec_ina" value="{kec_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kec_eng" value="{kec_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kelurahan / Desa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kel_ina" value="{kel_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kel_eng" value="{kel_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kode Pos</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kode_pos_ina" value="{kode_pos_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kode_pos_eng" value="{kode_pos_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">RT</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rt_ina" value="{rt_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rt_eng" value="{rt_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">RW</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rw_ina" value="{rw_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rw_eng" value="{rw_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Warga Negara</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="wn_ina" value="{wn_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="wn_eng" value="{wn_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Telephone</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="telephone_ina" value="{telephone_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="telephone_eng" value="{telephone_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">No HP</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nohp_ina" value="{nohp_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nohp_eng" value="{nohp_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">NIK</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nik_ina" value="{nik_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nik_eng" value="{nik_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Detail Pendaftaran</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="detail_pendaftaran_ina" value="{detail_pendaftaran_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="detail_pendaftaran_eng" value="{detail_pendaftaran_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tipe Pelayanan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tipe_pasien_ina" value="{tipe_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tipe_pasien_eng" value="{tipe_pasien_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kelompok Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kel_pasien_ina" value="{kel_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kel_pasien_eng" value="{kel_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">DPJP Utama</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dpjp_utama_ina" value="{dpjp_utama_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dpjp_utama_eng" value="{dpjp_utama_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Ruangan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ruangan_ina" value="{ruangan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ruangan_eng" value="{ruangan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kelas</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kelas_ina" value="{kelas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kelas_eng" value="{kelas_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Bed</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="bed_ina" value="{bed_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="bed_eng" value="{bed_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraf 3</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea style="width:100%" class="form-control" id="paragraf_3_ina" name="paragraf_3_ina" rows="3" placeholder="Content.."><?=$paragraf_3_ina?></textarea>
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea style="width:100%" class="form-control" id="paragraf_3_eng" name="paragraf_3_eng" rows="3" placeholder="Content.."><?=$paragraf_3_eng?></textarea>
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanda Tangan Pasien / Keluarga</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttd_pasien_ina" value="{ttd_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttd_pasien_eng" value="{ttd_pasien_eng}">
						</div>
						
					</div>
				</div>
				
				
				<div class="col-md-12 push-20-t push-20">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" ><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				
				
				
			</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<?}?>
	</div>
</div>
<div class="modal" id="modal_content" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Content</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
                        <input type="hidden" readonly id="header_id" name="header_id" value="">
                        <input type="hidden" readonly id="idcontent" name="idcontent" value="">

						<div class="col-md-12">                               
							<div class="form-group">
								<label class="col-md-2 control-label">No Urut</label>
								<div class="col-md-9"> 
									<input tabindex="0" type="text" class="form-control number" maxlength="3" id="no" placeholder="No Urut" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Content</label>
								<div class="col-md-9"> 
									<textarea class="form-control js-summernote footer" name="isi" id="isi"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Jenis Jawaban</label>
								<div class="col-md-9"> 
									<select tabindex="5" id="jenis_isi"   name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0"  <?=($jenis_isi == "0" ? 'selected="selected"' : '')?>>Label</option>
										<option value="1"  <?=($jenis_isi == "1" ? 'selected="selected"' : '')?>>Option</option>
										<option value="2" <?=($jenis_isi == 2 ? 'selected="selected"' : '')?>>Free Text</option>
										<option value="3" <?=($jenis_isi == 3 ? 'selected="selected"' : '')?>>Tanda Tangan</option>
										
									</select>
								</div>
							</div>
							<div class="form-group div_opsi">
								<label class="col-md-2 control-label">Value</label>
								<div class="col-md-9"> 
									<select id="ref_id" name="ref_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
																		
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" type="submit" type="button" id="btn_simpan_content"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function loadFile_img(event){
	// alert('sini');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_kunjungan();
	}
	if (tab=='2'){
		load_tipe();
	}
	if (tab=='3'){
		load_tipe();
		load_form();
	}
	if (tab=='6'){
		load_terima_all();
	}
	if (tab=='4'){
		load_content();
		$('.js-summernote').summernote({
		  height: 130,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
	}
	clear_tipe();
	set_poli();
	// selec2Barang();
})	
$("#jenis_isi").change(function() {
	let jenis_isi=$("#jenis_isi").val();
	if (jenis_isi=='1'){
		$(".div_opsi").css("display", "block");
	}else{
		
		$(".div_opsi").css("display", "none");
	}
	
});
function load_terima_all(){
	load_sound();
	load_terima();
}
//add_content
$("#btn_simpan_content").click(function() {
	if ($("#no").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if ($("#no").val()=='0'){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}

	if ($("#isi").val()==''){
		sweetAlert("Maaf...", "Isi Content ", "error");
		return false;
	}
	var idcontent=$("#idcontent").val();
	var no=$("#no").val();
	var header_id=$("#header_id").val();
	var isi=$("#isi").val();
	var jenis_isi=$("#jenis_isi").val();
	var ref_id=$("#ref_id").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/simpan_content/',
		dataType: "json",
		type: "POST",
		data: {
			idcontent:idcontent,
			header_id:header_id,
			jenis_isi:jenis_isi,
			ref_id:ref_id,
			no: no,isi:isi
		},
		success: function(data) {
			$("#modal_content").modal('hide');
			$("#cover-spin").hide();
			load_content();
		}
	});

});
$("#add_content").click(function() {
	$("#modal_content").modal('show');
	$("#header_id").val('0');
	$("#idcontent").val('');
	var rowCount = $('#index_content tr').length;
	$("#no").val(rowCount);
	$('#isi').summernote('code','');
	var id='';
	$.ajax({
			url: '{site_url}setting_ranap_kunjungan/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
});
function add_detail(id){
	$("#modal_content").modal('show');
	$("#header_id").val(id);
	$("#idcontent").val('');
	var rowCount = $('#index_content tr').length;
	$("#no").val(rowCount);
	$('#isi').summernote('code','');
	var id='';
	$.ajax({
			url: '{site_url}setting_ranap_kunjungan/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
};
function edit_content(id,header_id){
	$("#idcontent").val(id);
	$("#header_id").val(header_id);
	$("#modal_content").modal('show');
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}setting_ranap_kunjungan/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}setting_ranap_kunjungan/get_edit',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$('#isi').summernote('code',data.isi);
				// $("#isi").html(data.isi);
				$("#jenis_isi").val(data.jenis_isi).trigger('change');
				$("#no").val(data.no);
				$("#cover-spin").hide();
				
			}
	});
}
function load_content(){
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/load_content/',
		dataType: "json",
		success: function(data) {
			$("#index_content tbody").empty();
			$('#index_content tbody').append(data);
		}
	});

}
function hapus(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ranap_kunjungan/hapus',
				type: 'POST',
				data: {id: id},
				complete: function() {
					load_content();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function set_tab(tab){
	$("#cover-spin").show();
	window.location.href = "<?php echo site_url('setting_ranap_kunjungan/index/"+tab+"'); ?>";
}
$("#tipe_rekanan_tipe").change(function(){
	set_rekanan();
});
function set_rekanan(){
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/get_asuransi/',
		dataType: "json",
		 type: "POST" ,
		dataType: 'json',
		data : {
				tipe_rekanan_tipe : $("#tipe_rekanan_tipe").val(),
			   },
		success: function(data) {
			// alert(data);
			$("#idrekanan_tipe").empty();
			$("#idrekanan_tipe").append(data.detail);
			
		}
	});
}
function set_poli(){
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/get_poli/',
		dataType: "json",
		 type: "POST" ,
		dataType: 'json',
		data : {
				asal_pasien : $("#asal_pasien").val(),
			   },
		success: function(data) {
			// alert(data);
			$("#idpoli").empty();
			$("#idpoli").append(data);
		}
	});
}
$("#asal_pasien").change(function(){
	set_poli();
});
$("#st_tipe_pasien_edit,#st_play").change(function(){
	simpan_general();
});
function simpan_general(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/simpan_general', 
		dataType: "JSON",
		method: "POST",
		data : {
				st_tipe_pasien_edit : $("#st_tipe_pasien_edit").val(),
				st_play : $("#st_play").val(),
				
			},
		success: function(data) {
			$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

		}
	});
}
function add_kunjungan(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/simpan_kunjungan', 
		dataType: "JSON",
		method: "POST",
		data : {
				kunjungan_id : $("#kunjungan_id").val(),
				asal_pasien : $("#asal_pasien").val(),
				idpoli : $("#idpoli").val(),
				iddokter : $("#iddokter").val(),
				
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				clear_kunjungan();
				load_kunjungan();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function edit_kunjungan(id){
	clear_kunjungan();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/edit_kunjungan', 
		dataType: "JSON",
		method: "POST",
		data : {
				id : id,
			
			},
		success: function(data) {
			
				$("#cover-spin").hide();
			if (data){
				$("#kunjungan_id").val(id);
				$("#idpoli").val(data.idpoli).trigger('change.select2');
				$("#asal_pasien").val(data.asal_pasien).trigger('change.select2');
				$("#iddokter").val(data.iddokter).trigger('change.select2');
				
			}else{
				swal({
					title: "Data Error!",
					text: "Tidak Ditemukan.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function clear_kunjungan(){
	$("#kunjungan_id").val('');
	$("#asal_pasien").val(0).trigger('change');
	$("#idpoli").val(0).trigger('change');
	$("#iddokter").val(0).trigger('change');
}
function load_kunjungan(){
	$('#index_kunjungan').DataTable().destroy();	
	table = $('#index_kunjungan').DataTable({
            searching: true,
			autoWidth: false,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
		
            ajax: { 
                url: '{site_url}setting_ranap_kunjungan/load_kunjungan', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_kunjungan(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ranap_kunjungan/hapus_kunjungan',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_kunjungan').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

// TIPE
function set_poli_tipe(){
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/get_poli/',
		dataType: "json",
		 type: "POST" ,
		dataType: 'json',
		data : {
				asal_pasien : $("#asal_pasien_tipe").val(),
			   },
		success: function(data) {
			// alert(data);
			$("#idpoli_tipe").empty();
			$("#idpoli_tipe").append(data);
		}
	});
}
$("#asal_pasien_tipe").change(function(){
	set_poli_tipe();
});
$("#idkelompokpasien_tipe").change(function(){
	if ($(this).val()=='1'){
		$(".div_asuransi").removeAttr('disabled');
		
	}else{
		$(".div_asuransi").prop('disabled',true);
		$(".div_asuransi").val(0).trigger('change.select2');
	}
});
function add_tipe(){
	let idtipepasien_tipe=$("#idtipepasien_tipe").val();
	let asal_pasien_tipe=$("#asal_pasien_tipe").val();
	if (asal_pasien_tipe=='0'){
		sweetAlert("Maaf...", "Tentukan Asal pasien", "error");
		return false;
	}
	if (idtipepasien_tipe=='0'){
		sweetAlert("Maaf...", "Tentukan Tipe Pasien", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/simpan_tipe', 
		dataType: "JSON",
		method: "POST",
		data : {
				kunjungan_tipe_id : $("#kunjungan_tipe_id").val(),
				asal_pasien : $("#asal_pasien_tipe").val(),
				idpoli : $("#idpoli_tipe").val(),
				iddokter : $("#iddokter_tipe").val(),
				idkelompokpasien : $("#idkelompokpasien_tipe").val(),
				tipe_rekanan : $("#tipe_rekanan_tipe").val(),
				idrekanan : $("#idrekanan_tipe").val(),
				idtujuan : $("#idtujuan_tipe").val(),
				idtipepasien : $("#idtipepasien_tipe").val(),

			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				clear_tipe();
				load_tipe();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}

function edit_tipe(id){
	clear_tipe();
	$("#kunjungan_tipe_id").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/edit_tipe', 
		dataType: "JSON",
		method: "POST",
		data : {
				id : id,
			
			},
		success: function(data) {
			
				$("#cover-spin").hide();
			if (data){
				$("#idpoli_tipe").val(data.idpoli).trigger('change.select2');
				$("#asal_pasien_tipe").val(data.asal_pasien).trigger('change.select2');
				$("#iddokter_tipe").val(data.iddokter).trigger('change.select2');
				$("#idkelompokpasien_tipe").val(data.idkelompokpasien).trigger('change');
				$("#tipe_rekanan_tipe").val(data.tipe_rekanan).trigger('change.select2');
				$("#idrekanan_tipe").val(data.idrekanan).trigger('change.select2');
				$("#idtujuan_tipe").val(data.idtujuan).trigger('change.select2');
				$("#idtipepasien_tipe").val(data.idtipepasien).trigger('change.select2');
				
			}else{
				swal({
					title: "Data Error!",
					text: "Tidak Ditemukan.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function clear_tipe(){
	$("#kunjungan_tipe_id").val('');
	$("#asal_pasien_tipe").val(0).trigger('change');
	$("#idpoli_tipe").val(0).trigger('change');
	$("#idkelompokpasien_tipe").val(0).trigger('change');
	$("#iddokter_tipe").val(0).trigger('change');
	$("#tipe_rekanan_tipe").val(0).trigger('change.select2');
}
function load_tipe(){
	$('#index_tipe').DataTable().destroy();	
	table = $('#index_tipe').DataTable({
            searching: true,
			autoWidth: false,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
		
            ajax: { 
                url: '{site_url}setting_ranap_kunjungan/load_tipe', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_tipe(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ranap_kunjungan/hapus_tipe',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tipe').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
//FORM
function add_form(){
	let idtipepasien_form=$("#idtipepasien_form").val();
	if (idtipepasien_form=='0'){
		sweetAlert("Maaf...", "Tentukan Tipe Pasien", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/simpan_form', 
		dataType: "JSON",
		method: "POST",
		data : {
				kunjungan_form_id : $("#kunjungan_form_id").val(),
				idtujuan : $("#idtujuan_form").val(),
				iddokter : $("#iddokter_form").val(),
				st_lembar_masuk : $("#st_lembar_masuk_form").val(),
				st_pernyataan : $("#st_pernyataan_form").val(),
				st_general : $("#st_general_form").val(),
				st_hak_kewajiban : $("#st_hak_kewajiban_form").val(),
				st_hak_kewajiban : $("#st_hak_kewajiban_form").val(),

			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				clear_form();
				load_form();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function edit_form(id){
	clear_form();
	$("#kunjungan_form_id").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/edit_form', 
		dataType: "JSON",
		method: "POST",
		data : {
				id : id,
			
			},
		success: function(data) {
			
				$("#cover-spin").hide();
			if (data){
				$("#idtujuan_form").val(data.idtujuan).trigger('change.select2');
				$("#iddokter_form").val(data.iddokter).trigger('change.select2');
				$("#st_lembar_masuk_form").val(data.st_lembar_masuk).trigger('change.select2');
				$("#st_pernyataan_form").val(data.st_pernyataan).trigger('change.select2');
				$("#st_general_form").val(data.st_general).trigger('change.select2');
				$("#st_hak_kewajiban_form").val(data.st_hak_kewajiban).trigger('change.select2');
				
			}else{
				swal({
					title: "Data Error!",
					text: "Tidak Ditemukan.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function clear_form(){
	// $("#kunjungan_form_id").val('');
	// $("#idtujuan").val(0).trigger('change');
	// $("#st_lembar_masuk_form").val(0).trigger('change');
	// $("#st_pernyataan_form").val(0).trigger('change');
	// $("#st_general").val(0).trigger('change');
	// $("#st_hak_kewajiban").val(0).trigger('change');
	// $("#iddokter_form").val(0).trigger('change');
}
function load_form(){
	$('#index_form').DataTable().destroy();	
	table = $('#index_form').DataTable({
            searching: true,
			autoWidth: false,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
		
            ajax: { 
                url: '{site_url}setting_ranap_kunjungan/load_form', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_form(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ranap_kunjungan/hapus_form',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_form').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
//TERIMA
function add_terima(){
	let idtujuan_terima=$("#idtujuan_terima").val();
	if (idtujuan_terima=='0'){
		sweetAlert("Maaf...", "Tentukan Tipe Pasien", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/simpan_terima', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtrx : $("#kunjungan_terima_id").val(),
				idtujuan : $("#idtujuan_terima").val(),
				idruangan: $("#idruangan_terima").val(),
				idkelas : $("#idkelas_terima").val(),
				st_terima_ranap : $("#st_terima_ranap").val(),
				
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				clear_terima();
				load_terima();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function edit_terima(id){
	clear_terima();
	$("#kunjungan_terima_id").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/edit_terima', 
		dataType: "JSON",
		method: "POST",
		data : {
				id : id,
			
			},
		success: function(data) {
			
				$("#cover-spin").hide();
			if (data){
				$("#idtujuan_terima").val(data.idtujuan).trigger('change.select2');
				$("#idruangan_terima").val(data.idruangan).trigger('change.select2');
				$("#idkelas_terima").val(data.idkelas).trigger('change.select2');
				$("#st_terima_ranap").val(data.st_terima_ranap).trigger('change.select2');
				
			}else{
				swal({
					title: "Data Error!",
					text: "Tidak Ditemukan.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function clear_terima(){
	$("#kunjungan_terima_id").val('');
	// $("#idtujuan").val(0).trigger('change');
	// $("#st_lembar_masuk_terima").val(0).trigger('change');
	// $("#st_pernyataan_terima").val(0).trigger('change');
	// $("#st_general").val(0).trigger('change');
	// $("#st_hak_kewajiban").val(0).trigger('change');
	// $("#iddokter_terima").val(0).trigger('change');
}
function load_terima(){
	$('#index_terima').DataTable().destroy();	
	table = $('#index_terima').DataTable({
            searching: true,
			autoWidth: false,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
		
            ajax: { 
                url: '{site_url}setting_ranap_kunjungan/load_terima', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_terima(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ranap_kunjungan/hapus_terima',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_terima').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
$("#btn_tambah_sound").click(function() {
	let idsound=$("#idsound").val();
	let nourut=$("#nourut").val();
	let sound_id=$("#sound_id").val();
	if ($("#nourut").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if ($("#sound_id").val()==''){
		sweetAlert("Maaf...", "Tentukan Isi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/simpan_sound', 
		dataType: "JSON",
		method: "POST",
		data : {
			idsound:idsound,
			nourut:nourut,
			sound_id:sound_id,
			},
		complete: function(data) {
			// $("#idtipe").val('#').trigger('change');
			$('#index_sound').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			clear_sound();
			
			load_data_sound();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
$("#btn_refresh_sound_text").click(function() {
clear_sound();
});
$("#btn_play_all").click(function() {
	play_sound_arr();
});
function clear_sound(){
	$("#idsound").val('');
	$("#nourut").val('');
	$("#sound_id").val('#').trigger('change');
}
function load_sound(){
	var idpoli=$("#idpoli").val();
	$('#index_sound').DataTable().destroy();	
	table = $('#index_sound').DataTable({
		autoWidth: false,
		searching: false,
		serverSide: true,
		"processing": true,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		
		ajax: { 
			url: '{site_url}setting_ranap_kunjungan/load_sound', 
			type: "POST" ,
			dataType: 'json',
			data : {
					idpoli:idpoli
				   }
		}
	});
	load_data_sound();
}
function hapus_sound($id){
 var id=$id;
swal({
		title: "Anda Yakin ?",
		text : "Untuk Hapus Sound?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}setting_ranap_kunjungan/hapus_sound',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				$('#index_sound').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				load_data_sound();
			}
		});
	});
}
function edit_sound($id){
	let id=$id;
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/edit_sound', 
		dataType: "JSON",
		method: "POST",
		data : {id:id},
		success: function(data) {
			$("#nourut").val(data.nourut);
			$("#sound_id").val(data.sound_id).trigger('change');
			$("#idsound").val(data.id);
		}
	});

}
function load_data_sound(){
	$.ajax({
		url: '{site_url}setting_ranap_kunjungan/load_data_sound', 
		dataType: "JSON",
		method: "POST",
		data : {
			},
		success: function(data) {
			$("#arr_sound").val(data.file);
			
		}
	});

}
function play_sound_arr(){
	let str_file=$("#arr_sound").val();
	var myArray = str_file.split(":");
	console.log(myArray[0]);
	queue_sounds(myArray);
}
function queue_sounds(sounds){
	// $config['upload_path'] = './assets/upload/sound_antrian/';
	let path_sound='{site_url}assets/upload/sound_antrian/';
	
	var index = 0;
	function recursive_play()
	{
		let file_sound = new Audio(path_sound+sounds[index]);
	  if(index+1 === sounds.length)
	  {
		play(file_sound);
	  }
	  else
	  {
		play(file_sound,function(){
			index++; recursive_play();
			});
	  }
	}

recursive_play();   
}
 function play(audio, callback) {

	audio.play();
	if(callback)
	{
		audio.onended = callback;
	}
}
</script>