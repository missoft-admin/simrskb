<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mvariable" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mvariable/save','class="form-horizontal push-10-t"') ?>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Sumber Variable</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama Sumber Variable" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Tipe Variable</label>
				<div class="col-md-7">
					<select id="tipe_id" name="tipe_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($tipe_id=='#'?'selected':'')?> selected>- Pilih Tipe Variable -</option>
						<option value="1" <?=($tipe_id=='1'?'selected':'')?>>Pendapatan</option>
						<option value="2" <?=($tipe_id=='2'?'selected':'')?>>Potongan</option>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Jenis Variable</label>
				<div class="col-md-7">
					<select id="jenis_id" name="jenis_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($jenis_id=='#'?'selected':'')?> selected>- Pilih Tipe Variable -</option>
						<option value="0" <?=($jenis_id=='0'?'selected':'')?>>Non Sub</option>
						<option value="1" <?=($jenis_id=='1'?'selected':'')?>>Sub</option>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">No Akun.</label>
				<div class="col-md-7">
					<select id="jenis_id" name="jenis_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						
						
					</select>
				</div>
			</div>
			<div class="form-group" id="div_sub">
				<div class="col-md-12">
					
				</div>
			</div>
			
			
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mvariable" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

	
	})	
</script>