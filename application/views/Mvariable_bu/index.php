<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('129'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('130'))){ ?>
		<ul class="block-options">
       
            <a href="{base_url}mvariable/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
       
			</ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>#</th>
					<th>Variable</th>
					<th>Tipe </th>
					<th>Jenis</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ 
		// alert('sini');
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mvariable/getIndex',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "20%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true },
					{ "width": "20%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "20%", "targets": 5, "orderable": true },
				]
			});
	});
</script>
