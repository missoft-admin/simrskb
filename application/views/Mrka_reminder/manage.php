<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrka" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama RKA</label>
				<div class="col-md-7">
					<input type="hidden" class="form-control" id="id" placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
					<input type="text" class="form-control" id="nama" <?=$disabel?> placeholder="Nama RKA" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" placeholder="Nama RKA" name="disabel" value="{disabel}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Periode</label>
				<div class="col-md-7">
					<select id="periode" <?=$disabel?> name="periode" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="">- Pilih Periode -</option>
						<?
						$tahun=date('Y');
						for($i=($tahun-1);$i<($tahun+5);$i++){?>
							<option value="<?=$i?>" <?=$periode==$i?'selected':''?>><?=$i?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" <?=$disabel?> type="submit">Simpan</button>
					<a href="{base_url}mrka" <?=$disabel?> class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<?if ($id!=''){?>
<div class="block">
	<div class="block-header">	
		<h3 class="block-title">TENTUKAN PERSPEKTIF DAN PROGRAM</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Prespektif</label>
                    <div class="col-md-8">
                       <select id="idprespektif" name="idprespektif" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Prespektif -</option>
							<?foreach(get_all('mprespektif_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>	
                                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <div class="col-md-3">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter_detail" id="btn_filter_detail" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_program">
			<thead>
				<tr>
					<th hidden></th>
					<th hidden></th>
					<th style="width:10%">
						<label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
							<input type="checkbox" <?=$disabel?>  id="check-all" name="check-all" class="check_all"><span></span>
						</label>
					</th>
					<th>PROGRAM</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		if ($("#id").val()!=''){
			load_program();
		}
	})	
	$(document).on("click",".check_all",function(){	
		
		if ($(this).is(':checked')) {
			
			check_all_rka('1');
		}else{
			
			check_all_rka('0');
		}
	});
	$(document).on("click",".chek1",function(){	
		var table = $('#table_program').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		
		if ($(this).is(':checked')) {
			// alert('CHECK All'+id);
			check_all(id,'1');
		}else{
			// alert('UNCHECK All'+id);
			check_all(id,'0');
		}
	});
	$(document).on("click",".chek2",function(){	
		var table = $('#table_program').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,1).data()
		
		if ($(this).is(':checked')) {
			// alert('CHECK All'+id);
			check_program(id,'1');
		}else{
			// alert('UNCHECK All'+id);
			check_program(id,'0');
		}
	});
	function check_all($idprespektif,$status){
		var idrka=$("#id").val();
		var idprespektif=$idprespektif;
		var status=$status;
		$.ajax({
			url: '{site_url}mrka/update_status_detail',
			type: 'POST',
			dataType: 'json',
			data: {
				idrka: idrka,idprespektif: idprespektif,status: status,	
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' '});
				$('#table_program').DataTable().ajax.reload( null, false );
			}
		});
	}
	function check_program($idprogram,$status){
		var idrka=$("#id").val();
		var idprogram=$idprogram;
		var status=$status;
		$.ajax({
			url: '{site_url}mrka/update_status_program',
			type: 'POST',
			dataType: 'json',
			data: {
				idrka: idrka,idprogram: idprogram,status: status,	
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' '});
				$('#table_program').DataTable().ajax.reload( null, false );
			}
		});
	}
	function check_all_rka($status){
		var idrka=$("#id").val();
		var status=$status;
		$.ajax({
			url: '{site_url}mrka/update_status_all',
			type: 'POST',
			dataType: 'json',
			data: {
				idrka: idrka,status: status,	
			},
			complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' '});
				$('#table_program').DataTable().ajax.reload( null, false );
			}
		});
	}
	function load_program(){
		var idprespektif=$("#idprespektif").val();
		var idrka=$("#id").val();
		var disabel=$("#disabel").val();
		// alert(idrka);
			$('#table_program').DataTable().destroy();
			table=$('#table_program').DataTable({
					"autoWidth": false,
					"pageLength": 50,
					"ordering": false,
					"processing": true,
					"serverSide": true,
					"order": [],
					"ajax": {
						url: '{site_url}mrka/load_program',
						type: "POST",
						dataType: 'json',
						data : {
							idrka:idrka,
							idprespektif:idprespektif,
							disabel:disabel,
							
						   }
					},
					createdRow: function (row, data, index) {
						//
						// if the second column cell is blank apply special formatting
						//
						if (data[1] == "") {
							console.dir(row);
							$('tr', row).addClass('warning');
						}
					},
					"columnDefs": [
						{"targets": [0,1], "visible": false },
						{  className: "text-center", targets:[2] },
					],
					
				});
		
	}
	$(document).on("click","#btn_filter_detail",function(){
		load_program();
	});
</script>