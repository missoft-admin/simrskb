<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1388'))){ ?>
<div class="block">
	<div class="block-header">
	
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Periode</label>
                    <div class="col-md-8">
						<div class="input-group date">
							<select id="periode1" name="periode1" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#">- Semua Periode -</option>
								<?		
								$bulan=(int)date('m');
								for($i=1;$i<=12;$i++){?>
									<option value="<?=$i?>" <?=(1==$i?'selected':'')?>><?=MONTHFormat($i)?></option>									
								<?}?>
								
							</select>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</span>
							<select id="periode2" name="periode2" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Periode -</option>
								<?
								for($i=1;$i<=12;$i++){?>
									<option value="<?=$i?>" <?=($bulan==$i?'selected':'')?>><?=MONTHFormat($i)?></option>									
								<?}?>
								
							</select>
						</div>
                       
                    </div>
                </div>	
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Prespektif</label>
                    <div class="col-md-8">
						<select id="idprespektif" name="idprespektif" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Prespektif -</option>
							<?foreach(get_all('mprespektif_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
						</select>                       
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Program</label>
                    <div class="col-md-8">
						<select id="idprogram" name="idprogram" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Program -</option>
							<?foreach(get_all('mprogram_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>                     
                    </div>
                </div>	
                                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <div class="col-md-3">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th hidden>idrka_kegiatan</th>
					<th hidden>idrka</th>
					<th hidden>idpresprekti</th>
					<th hidden>idprogram</th>
					<th hidden>idkegiatan</th>
					<th>#</th>
					<th>RKA</th>
					<th>Kegiatan</th>
					<th>Program</th>
					<th>Prespektif</th>
					<th align="center">Target</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	function load_index(){
		var periode1=$("#periode1").val();
		var periode2=$("#periode2").val();
		var idprespektif=$("#idprespektif").val();
		var idprogram=$("#idprogram").val();
		// alert(periode);
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_reminder/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						periode1:periode1,
						periode2:periode2,
						idprespektif:idprespektif,
						idprogram:idprogram,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,3,4], "visible": false },
					{"targets": [10], "class": 'text-center' },
					// { "width": "5%", "targets": 0, "orderable": true },
					// { "width": "20%", "targets": 1, "orderable": true },
					// { "width": "10%", "targets": 2, "orderable": true },
					// { "width": "10%", "targets": 3, "orderable": true },
					// { "width": "10%", "targets": 4, "orderable": true },
					// { "width": "20%", "targets": 5, "orderable": true },
					// { "width": "20%", "targets": 6, "orderable": true },
				]
			});
	}
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	
</script>
