<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	
	<div class="block-content tab-content">
		<?php if (UserAccesForm($user_acces_form,array('2098'))){ ?>
		<div class="tab-pane fade fade-left active in" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_racikan_des">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="20%">Jenis Racikan</th>
										<th width="60%">Deskripsi</th>
										<th width="15%">Action</th>										   
									</tr>
									<tr>
										<th>#<input class="form-control" type="hidden" id="racikan_des_id" name="racikan_des_id" value="" placeholder="id"></th>
										<th>
											<select tabindex="8" id="jenis_racik_id" name="jenis_racik_id" class="js-select2 form-control js_99" style="width: 100%;">
												<option value="" <?=($jenis_racik_id==""?'selected':'')?>>Pilih Opsi</option>
												<?foreach(list_variable_ref(105) as $row){?>
													<option value="<?=$row->id?>" <?=($jenis_racik_id==$row->id?'selected':'')?>><?=$row->nama?></option>
												<?}?>
											</select>
										</th>
										<th>
											<input class="form-control" type="text" style="width:100%" id="deskripsi" name="deskripsi" placeholder="Kategori" value="" >
										</th>
										
										</th>
										<th>
											<div class="btn-group">
											<?php if (UserAccesForm($user_acces_form,array('2099'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_racikan_des" name="btn_tambah_racikan_des"><i class="fa fa-save"></i> Simpan</button>
											<?}?>
											<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_racikan_des()"><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_racikan_des();		
	
})	
function clear_racikan_des(){
	$("#jenis_racik_id").val('').trigger('change');
	$("#deskripsi").val('');
	$("#btn_tambah_racikan_des").html('<i class="fa fa-save"></i> Save');
	
}
function load_racikan_des(){
	$('#index_racikan_des').DataTable().destroy();	
	table = $('#index_racikan_des').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mracikan_des/load_racikan_des', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_racikan_des").click(function() {
	let jenis_racik_id=$("#jenis_racik_id").val();
	let deskripsi=$("#deskripsi").val();
	let racikan_des_2=$("#racikan_des_2").val();
	let kategori_racikan_des=$("#kategori_racikan_des").val();
	let warna=$("#warna").val();
	
	if (jenis_racik_id==''){
		sweetAlert("Maaf...", "Tentukan Jenis Racikan", "error");
		return false;
	}
	if (deskripsi==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mracikan_des/simpan_racikan_des', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_racik_id:jenis_racik_id,
				deskripsi:deskripsi,
				
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data){
				clear_racikan_des();
				$('#index_racikan_des').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Gagal!",
					text: "Data Duplicate.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
				
		}	
	});

});
function edit_racikan_des(racikan_des_id){
	$("#racikan_des_id").val(racikan_des_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mracikan_des/find_racikan_des',
		type: 'POST',
		dataType: "JSON",
		data: {id: racikan_des_id},
		success: function(data) {
			$("#btn_tambah_racikan_des").html('<i class="fa fa-save"></i> Edit');
			$("#racikan_des_id").val(data.id);
			$("#racikan_des_1").val(data.racikan_des_1);
			$("#racikan_des_2").val(data.racikan_des_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_racikan_des").val(data.kategori_racikan_des);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_racikan_des(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus deskripsi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mracikan_des/hapus_racikan_des',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_racikan_des').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>