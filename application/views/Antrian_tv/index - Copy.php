<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<link rel='stylesheet' href='<?=$css_path?>video/style.css' type='text/css' media='all' />
<style>
 #cover-spin {
  position: fixed;
  width: 100%;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: rgba(255, 255, 255, 0.7);
  z-index: 9999;
  display: none;
}

@-webkit-keyframes spin {
  from {
	-webkit-transform: rotate(0deg);
  }

  to {
	-webkit-transform: rotate(360deg);
  }
}

@keyframes spin {
  from {
	transform: rotate(0deg);
  }

  to {
	transform: rotate(360deg);
  }
}

#cover-spin::after {
  content: '';
  display: block;
  position: absolute;
  left: 48%;
  top: 40%;
  width: 40px;
  height: 40px;
  border-style: solid;
  border-color: black;
  border-top-color: transparent;
  border-width: 4px;
  border-radius: 50%;
  -webkit-animation: spin .8s linear infinite;
  animation: spin .8s linear infinite;
}
#main-container {
    background-color: <?=$bg_color?>;
}
.block {
    background-color: <?=$bg_color?>;
}
.font-s25 {
  font-size: 25px !important;
}
.font-s20 {
  font-size: 20px !important;
}
.font-s40 {
  font-size: 100px !important;
}
.ms-container {
    height: 400px;
    overflow: hidden;
    position:relative
}
.flexslider {
    position:static;
}
.metaslider .slides img {
    width: auto;
    min-width: 100vw!important;
    min-height: 400px;
    position: absolute;
}
 @media screen {
	 table {
	   font-size: 13px !important;
	   border-collapse: collapse !important;
	   width: 100% !important;
	   
	 }
	 td {
	   padding: 5px;
	   border: 0px solid #fff !important;
	 }
	 .content td {
	   padding: 0px;
	 }
	 .border-full {
	   border: 1px solid #000 !important;
	 }
	 .border-bottom {
	   border-bottom:1px solid #000 !important;
	 }
	 .border-right {
	   border-right:1px solid #000 !important;
	 }
	 .border-left {
	   border-left:1px solid #000 !important;
	 }
	 .border-top {
	   border-top:1px solid #000 !important;
	 }
	 .border-thick-top{
	   border-top:2px solid #000 !important;
	 }
	 .border-thick-bottom{
	   border-bottom:2px solid #000 !important;
	 }
	 .text-center{
	   text-align: center !important;
	 }
	 .text-right{
	   text-align: right !important;
	 }
	 .text-semi-bold {
	   font-size: 15px !important;
	   font-weight: 700 !important;
	 }
	 .text-bold {
	   font-size: 15px !important;
	   font-weight: 700 !important;
	 }
	 .text-footer {
	   font-size: 12px !important;
	 }
	 .text-address {
	   color: #3abad8 !important;
	 }
  }
</style>
<div class="block">
<div id="cover-spin"></div>
<div class="content content-narrow">
	<div class="row pull-t">
		<div class="col-sm-8 col-lg-8">
			<div class="block-content block-content-full">
					<table class="" style="width:100%">
						<tbody>
							<tr>
								<td style="width: 15%;" rowspan="2" class="text-center">
									<img class="img-avatar img-avatar96" src="{upload_path}antrian/<?=$header_logo?>" alt="">
								</td>
								<td class="" style="width: 85%;">
									<h3 class="text-white  text-left">{judul_header}</h3>
								</td>
							</tr>
							<tr>
								<td >
									<address class="text-white">
									<strong>{alamat}</strong><br>
									{telepone}<br>{email} | {website}
									</address>
								</td>
							</tr>
							
						</tbody>
					</table>
					
			</div>
		</div>
		
		<div class="col-sm-4 col-lg-4">
			<div class="block-content block-content-full clearfix">
				<table class="block-table text-center">
					<tbody>
						<tr>
							<td style="width: 80%;">
								<div class="text-right font-w700 text-white"><span class="font-s25" id="tanggal"><?=GetDayIndonesia(date('Y-m-d'),true,'text')?></span></div>
								<div class="text-right text-white"><h2 id="current-time-now" data-start="<?php echo time() ?>"></h2></div>
							</td>
							<td class="" style="width: 20%;">
								<a class="push-5-l" onclick="cetak_ulang()" title="Cetak Ulang"  href="javascript:void(0)">
									<i class="fa fa-print fa-3x text-white text-left"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
				
				
			</div>
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-sm-5">
			<div class="block ">
				
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full block-content-mini  bg-danger h1 text-white">
								ANTRIAN
							</div>
							<div class="block-content block-content-full bg-danger">
								<div class="font-s40 font-w700 text-white push-5-t">B10</div>
							</div>
							<div class="block-content block-content-full block-content-mini  bg-white h1">
								<i class="si si-screen-desktop"></i>&nbsp;&nbsp; COUNTER 1
							</div>
						</a>
				
			</div>
		</div>
		<div class="col-sm-7">
			<div class="block container">
					<div class="main-video-container">
					  <video src="<?=$upload_path?>/video/vid-1.mp4" loop controls class="main-video"></video>
					  <h3 class="main-vid-title">house flood animation</h3>
				   </div>
					
			</div>
		</div>
		
		
		
	</div>

	<div class="row push-15-t">
		<div class="col-sm-1">
			<div class="block">
				<div class="block-content">
					
				</div>
			</div>
		</div>
		<div class="col-sm-10">
			<div class="block" id="content_button">
			
			</div>
		</div>
		<div class="col-sm-1">
			<div class="block">
				<div class="block-content">
					
				</div>
			</div>
		</div>
		
		
	</div>
</div>
</div>
<script type="text/javascript" src="{js_path}video/script.js"></script>
<script src="{plugins_path}sparkline/jquery.sparkline.min.js"></script>
<script src="{plugins_path}slick/slick.min.js"></script>
<script src="{js_path}pages/base_ui_widgets.js"></script>
<script>
 // var freshTime = new Date(parseInt($("#current-time-now").attr("data-start"))*1000);
// //loop to tick clock every second
// var func = function myFunc() {
	// //set text of clock to show current time
	// $("#current-time-now").text(freshTime.toLocaleTimeString());
	// //add a second to freshtime var
	// freshTime.setSeconds(freshTime.getSeconds() + 1);
	// //wait for 1 second and go again
	// setTimeout(myFunc, 1000);
// };
// func();
// startWorker();
$(document).ready(function(){	
	// refres_button();
})	
function cetak_ulang(){
	alert('Ccetak Ulang');
}
function refres_button(){
	$.ajax({
		url: '{site_url}antrian_tiket/refresh_button',
		type: 'POST',
		dataType: "json",
		data: {
			
			
		},
		success: function(data) {
			$("#content_button").html(data);
			$("#cover-spin").hide();
		}
	});
}
function get_antrian(id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}antrian_tiket/get_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			antrian_id:id
			
		},
		success: function(data) {
			
			refresh_button();
		}
	});
}
function startWorker() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker("{site_url}assets/js/worker.js");
	
		}
		w.postMessage({'cmd': 'start' ,'msg': 3000});
		w.onmessage = function(event) {
			refres_button();
		};
		w.onerror = function(event) {
			window.location.reload();
		};
	} else {
		alert("Sorry, your browser does not support Autosave Mode");
		$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
	}
 }
</script>