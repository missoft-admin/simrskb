<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('148'))){ ?>
<div class="block">
    <div class="block-header">
        <?php if (UserAccesForm($user_acces_form,array('150'))){ ?>
        <ul class="block-options">
            <li>
                <a href="{base_url}mtarif_radiologi/create/{idtipe}" class="btn"><i class="fa fa-plus"></i></a>
            </li>
        </ul>
        <?}?>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php if (UserAccesForm($user_acces_form,array('149'))){ ?>
        <hr style="margin-top:0px">
        <?php echo form_open('mtarif_radiologi/filter','class="form-horizontal" id="form-work"') ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="">Tipe</label>
                    <div class="col-md-7">
                        <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Tipe">
                            <option value="0" <?=($idtipe == '0') ? "selected" : "" ?>>Semua Tipe</option>
                            <option value="1" <?=($idtipe == '1' ? 'selected="selected"':'')?>>X-Ray</option>
                            <option value="2" <?=($idtipe == '2' ? 'selected="selected"':'')?>>USG</option>
                            <option value="3" <?=($idtipe == '3' ? 'selected="selected"':'')?>>CT Scan</option>
                            <option value="4" <?=($idtipe == '4' ? 'selected="selected"':'')?>>MRI</option>
                            <option value="5" <?=($idtipe == '5' ? 'selected="selected"':'')?>>BMD</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="">Head Parent</label>
                    <div class="col-md-7">
                        <select name="idparent" id="idparent" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Head Parent">
                            <option value="0" <?=($idparent == '0') ? "selected" : "" ?>>Semua Head Parent</option>
                            <?php foreach ($list_parent as $row) { ?>
                            <option value="<?=$row->path;?>" <?=($idparent === $row->path) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama)?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="">Sub Parent</label>
                    <div class="col-md-7">
                        <select name="idsubparent[]" id="idsubparent" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Sub Parent">
                            <?php foreach ($list_subparent as $row) { ?>
                            <option value="<?=$row->path;?>" <?= in_array($row->path, explode("_", $idsubparent), true) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama)?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for=""></label>
                    <div class="col-md-7">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close() ?>
        <hr>
        <?}?>

        <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}mtarif_radiologi/getIndex/{idtipe}/{idparent}/{idsubparent}',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "10%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "70%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "20%",
                "targets": 2,
                "orderable": true
            }
        ]
    });
});

$("#idtipe").change(function() {
    if ($(this).val() != '') {
        $.ajax({
            url: '{site_url}mtarif_radiologi/find_index_parent/' + $(this).val(),
            dataType: "json",
            success: function(data) {
                $('#idparent').find('option').remove().end().append('<option value="0" selected>Semua</option>').val('0').trigger("liszt:updated");
                $('#idparent').append(data);
            }
        });
    }
});

$("#idparent").change(function() {
    if ($(this).val() != '') {
				$('#idsubparent').empty();

        $.ajax({
            url: '{site_url}mtarif_radiologi/find_index_subparent/' + $(this).val(),
            dataType: "json",
            success: function(data) {
                $('#idsubparent').append(data);
            }
        });
    }
});

</script>
