<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mtarif_radiologi/index/<?=$this->uri->segment(3)?>" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_radiologi/save','class="form-horizontal push-10-t" id="form-work"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idtipe">Tipe</label>
			<div class="col-md-7">
				<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($idtipe == 1 ? 'selected="selected"':'')?>>X-Ray</option>
					<option value="2" <?=($idtipe == 2 ? 'selected="selected"':'')?>>USG</option>
					<option value="3" <?=($idtipe == 3 ? 'selected="selected"':'')?>>CT Scan</option>
					<option value="4" <?=($idtipe == 4 ? 'selected="selected"':'')?>>MRI</option>
					<option value="5" <?=($idtipe == 5 ? 'selected="selected"':'')?>>BMD</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="level0">Head Parent</label>
			<div class="col-md-7">
				<select name="level0" id="level0" class="js-select2 form-control" style="width: 100%;"
					data-placeholder="Pilih Opsi">
					<?php foreach ($list_level0 as $index => $row) { ?>
					<? if ($index == 0) { ?>
						<option value="<?php echo $level0; ?>" <?php echo ($level0 === $path) ? 'selected' : ''; ?>>Root
						</option>
					<?php } ?>
					<option value="<?php echo $row->path; ?>" <?php echo ($level0 !== $path && $level0 === $row->path) ? 'selected' : ''; ?>><?php echo TreeView($row->level, $row->nama); ?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="headerpath">Parent</label>
			<div class="col-md-7">
				<select name="headerpath" id="headerpath" class="js-select2 form-control" style="width: 100%;"
					data-placeholder="Pilih Opsi">
					<?php foreach ($list_parent as $row) { ?>
					<option value="<?php echo $row->path; ?>" <?php echo ($headerpath !== $path && $headerpath === $row->path) ? 'selected' : ''; ?>><?php echo TreeView($row->level, $row->nama); ?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idkelompok">Jenis</label>
			<div class="col-md-7">
				<label class="radio-inline" for="kelompok">
            <input type="radio" id="kelompok" checked name="idkelompok" onclick="toggleJenis('kelompok')" <?=($idkelompok == 1 ? 'checked':'')?> value="1"> Kelompok
        </label>
				<label class="radio-inline" for="rincian">
            <input type="radio" id="rincian" name="idkelompok" onclick="toggleJenis('rincian')" <?=($idkelompok == 0 ? 'checked':'')?> value="0"> Rincian
        </label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="kode">Kode Tarif</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="kode" placeholder="Kode Tarif" name="kode" value="{kode}"
					required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama Tarif</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama Tarif" name="nama" value="{nama}"
					required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama_english">Nama Tarif (English Version)</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama_english" placeholder="Nama Tarif (English Version)" name="nama_english" value="{nama_english}"
					required="" aria-required="true">
			</div>
		</div>
		<div id="form-detail-xray" style="display:none">
			<div class="form-group" id="form-detail-xray-expose" style="display:none">
				<label class="col-md-3 control-label" for="idexpose">Expose</label>
				<div class="col-md-7">
					<select name="idexpose" id="idexpose" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach  ($list_expose as $row) { ?>
							<option value="<?=$row->id;?>" <?=($idexpose==$row->id) ? "selected" : "" ?>><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group" id="form-detail-xray-film" style="display:none">
				<label class="col-md-3 control-label" for="idfilm">Film</label>
				<div class="col-md-7">
					<select name="idfilm" id="idfilm" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach  ($list_film as $row) { ?>
							<option value="<?=$row->id;?>" <?=($idfilm==$row->id) ? "selected" : "" ?>><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="block-content" id="form-detail-xray-tarif" style="display:none">
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="totaltarif_xray">
			<thead>
				<tr>
					<th rowspan="2">Kelas</th>
					<th rowspan="2">Jasa Sarana</th>
					<th rowspan="2">BHP</th>
					<th rowspan="2">Biaya Perawatan</th>
					<th rowspan="2">Total Tarif</th>
					<th colspan="3" style="text-align:center">Jasa Pelayanan</th>
				</tr>
				<tr>
					<th>Langsung</th>
					<th>Menolak</th>
					<th>kembali</th>
				</tr>
			</thead>
			<tbody>
				<?php if($this->uri->segment(2) == 'create'){ ?>
				<tr style="background-color:#e0dfda">
					<td style="text-align:center">POLIKLINIK / IGD</td>
					<td style="display:none"><input type="hidden" class="kelaskamarXRAY" value="0"></td>
					<td><input type="text" class="form-control number jasasaranaXRAY" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaXRAY" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number bhpXRAY" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpXRAY" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanXRAY" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanXRAY" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalXRAY" placeholder="Total" value="0"></td>
					<td><input type="text" class="form-control number langsungXRAY" placeholder="Langsung" value="0"></td>
					<td><input type="text" class="form-control number menolakXRAY" placeholder="Menolak" value="0"></td>
					<td><input type="text" class="form-control number kembaliXRAY" placeholder="Kembali" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">I</td>
					<td style="display:none"><input type="hidden" class="kelaskamarXRAY" value="1"></td>
					<td><input type="text" class="form-control number jasasaranaXRAY" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaXRAY" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number bhpXRAY" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpXRAY" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanXRAY" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanXRAY" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalXRAY" placeholder="Total" value="0"></td>
					<td><input type="text" class="form-control number langsungXRAY" placeholder="Langsung" value="0"></td>
					<td><input type="text" class="form-control number menolakXRAY" placeholder="Menolak" value="0"></td>
					<td><input type="text" class="form-control number kembaliXRAY" placeholder="Kembali" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">II</td>
					<td style="display:none"><input type="hidden" class="kelaskamarXRAY" value="2"></td>
					<td><input type="text" class="form-control number jasasaranaXRAY" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaXRAY" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number bhpXRAY" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpXRAY" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanXRAY" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanXRAY" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalXRAY" placeholder="Total" value="0"></td>
					<td><input type="text" class="form-control number langsungXRAY" placeholder="Langsung" value="0"></td>
					<td><input type="text" class="form-control number menolakXRAY" placeholder="Menolak" value="0"></td>
					<td><input type="text" class="form-control number kembaliXRAY" placeholder="Kembali" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">III</td>
					<td style="display:none"><input type="hidden" class="kelaskamarXRAY" value="3"></td>
					<td><input type="text" class="form-control number jasasaranaXRAY" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaXRAY" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number bhpXRAY" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpXRAY" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanXRAY" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanXRAY" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalXRAY" placeholder="Total" value="0"></td>
					<td><input type="text" class="form-control number langsungXRAY" placeholder="Langsung" value="0"></td>
					<td><input type="text" class="form-control number menolakXRAY" placeholder="Menolak" value="0"></td>
					<td><input type="text" class="form-control number kembaliXRAY" placeholder="Kembali" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">UTAMA</td>
					<td style="display:none"><input type="hidden" class="kelaskamarXRAY" value="4"></td>
					<td><input type="text" class="form-control number jasasaranaXRAY" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaXRAY" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number bhpXRAY" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpXRAY" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanXRAY" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanXRAY" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalXRAY" placeholder="Total" value="0"></td>
					<td><input type="text" class="form-control number langsungXRAY" placeholder="Langsung" value="0"></td>
					<td><input type="text" class="form-control number menolakXRAY" placeholder="Menolak" value="0"></td>
					<td><input type="text" class="form-control number kembaliXRAY" placeholder="Kembali" value="0"></td>
				</tr>
				<?php }else{ ?>
					<?php foreach  ($list_detailtarif as $row) { ?>
					<tr style="background-color:<?=($row->kelas == '0' ? '#e0dfda' : '')?>">
						<td style="text-align:center"><?=GetKelas($row->kelas)?></td>
						<td style="display:none"><input type="hidden" class="kelaskamarXRAY" value="<?=$row->kelas?>"></td>
						<td><input type="text" class="form-control number jasasaranaXRAY" placeholder="Jasa Sarana" value="<?=$row->jasasarana?>"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasaranaXRAY" placeholder="Group Jasa Sarana" value="<?=$row->group_jasasarana?>"></td>
						<td><input type="text" class="form-control number bhpXRAY" placeholder="BHP" value="<?=$row->bhp?>"></td>
						<td style="display:none"><input type="text" class="form-control group_bhpXRAY" placeholder="Group BHP" value="<?=$row->group_jasasarana?>"></td>
						<td><input type="text" class="form-control number biayaperawatanXRAY" placeholder="Biaya Perawatan" value="<?=$row->biayaperawatan?>"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatanXRAY" placeholder="Group Biaya Perawatan" value="<?=$row->group_jasasarana?>"></td>
						<td><input type="text" readonly class="form-control number totalXRAY" placeholder="Total" value="<?=$row->total?>"></td>
						<td><input type="text" class="form-control number langsungXRAY" placeholder="Langsung" value="<?=$row->nominallangsung?>"></td>
						<td><input type="text" class="form-control number menolakXRAY" placeholder="Menolak" value="<?=$row->nominalmenolak?>"></td>
						<td><input type="text" class="form-control number kembaliXRAY" placeholder="Kembali" value="<?=$row->nominalkembali?>"></td>
					</tr>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

	<div class="block-content" id="form-detail-usg" style="display:none">
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="totaltarif_usg">
			<thead>
				<tr>
					<th>Kelas</th>
					<th>Jasa Sarana</th>
					<th>Jasa Pelayanan</th>
					<th>BHP</th>
					<th>Biaya Perawatan</th>
					<th>Total Tarif</th>
				</tr>
			</thead>
			<tbody>
				<?php if($this->uri->segment(2) == 'create'){ ?>
				<tr style="background-color:#e0dfda">
					<td style="text-align:center">POLIKLINIK / IGD</td>
					<td style="display:none"><input type="hidden" class="kelaskamarUSG" value="0"></td>
					<td><input type="text" class="form-control number jasasaranaUSG" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaUSG" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayananUSG" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayananUSG" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhpUSG" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpUSG" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanUSG" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanUSG" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalUSG" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">I</td>
					<td style="display:none"><input type="hidden" class="kelaskamarUSG" value="1"></td>
					<td><input type="text" class="form-control number jasasaranaUSG" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaUSG" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayananUSG" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayananUSG" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhpUSG" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpUSG" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanUSG" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanUSG" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalUSG" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">II</td>
					<td style="display:none"><input type="hidden" class="kelaskamarUSG" value="2"></td>
					<td><input type="text" class="form-control number jasasaranaUSG" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaUSG" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayananUSG" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayananUSG" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhpUSG" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpUSG" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanUSG" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanUSG" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalUSG" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">III</td>
					<td style="display:none"><input type="hidden" class="kelaskamarUSG" value="3"></td>
					<td><input type="text" class="form-control number jasasaranaUSG" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaUSG" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayananUSG" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayananUSG" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhpUSG" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpUSG" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanUSG" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanUSG" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalUSG" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">UTAMA</td>
					<td style="display:none"><input type="hidden" class="kelaskamarUSG" value="4"></td>
					<td><input type="text" class="form-control number jasasaranaUSG" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasaranaUSG" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayananUSG" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayananUSG" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhpUSG" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhpUSG" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatanUSG" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatanUSG" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control number totalUSG" placeholder="Total" value="0"></td>
				</tr>
				<?php }else{ ?>
					<?php foreach  ($list_detailtarif as $row) { ?>
					<tr style="background-color:<?=($row->kelas == '0' ? '#e0dfda' : '')?>">
						<td style="text-align:center"><?=GetKelas($row->kelas)?></td>
						<td style="display:none"><input type="hidden" class="kelaskamarUSG" value="<?=$row->kelas?>"></td>
						<td><input type="text" class="form-control number jasasaranaUSG" placeholder="Jasa Sarana" value="<?=$row->jasasarana?>"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasaranaUSG" placeholder="Group Jasa Sarana" value="<?=$row->group_jasasarana?>"></td>
						<td><input type="text" class="form-control number jasapelayananUSG" placeholder="Jasa Pelayanan" value="<?=$row->jasapelayanan?>"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayananUSG" placeholder="Group Jasa Pelayanan" value="<?=$row->group_jasapelayanan?>"></td>
						<td><input type="text" class="form-control number bhpUSG" placeholder="BHP" value="<?=$row->bhp?>"></td>
						<td style="display:none"><input type="text" class="form-control group_bhpUSG" placeholder="Group BHP" value="<?=$row->group_bhp?>"></td>
						<td><input type="text" class="form-control number biayaperawatanUSG" placeholder="Biaya Perawatan" value="<?=$row->biayaperawatan?>"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatanUSG" placeholder="Group Biaya Perawatan" value="<?=$row->group_biayaperawatan?>"></td>
						<td><input type="text" readonly class="form-control number totalUSG" placeholder="Total" value="<?=$row->total?>"></td>
					</tr>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

	<div class="block-content" id="form-detail-cmb" style="display:none">
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="totaltarif_cmb">
			<thead>
				<tr>
					<th>Kelas</th>
					<th>Jasa Sarana</th>
					<th>Jasa Pelayanan</th>
					<th>BHP</th>
					<th>Biaya Perawatan</th>
					<th>Total Tarif</th>
				</tr>
			</thead>
			<tbody>
				<?php if($this->uri->segment(2) == 'create'){ ?>
					<tr style="background-color:#e0dfda">
						<td style="text-align:center">POLIKLINIK / IGD</td>
						<td style="display:none"><input type="hidden" class="kelaskamar" value="0"></td>
						<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
						<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
						<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
						<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
						<td><input type="text" readonly class="form-control number total" placeholder="Total" value="0"></td>
					</tr>
					<tr>
						<td style="text-align:center">I</td>
						<td style="display:none"><input type="hidden" class="kelaskamar" value="1"></td>
						<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
						<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
						<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
						<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
						<td><input type="text" readonly class="form-control number total" placeholder="Total" value="0"></td>
					</tr>
					<tr>
						<td style="text-align:center">II</td>
						<td style="display:none"><input type="hidden" class="kelaskamar" value="2"></td>
						<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
						<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
						<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
						<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
						<td><input type="text" readonly class="form-control number total" placeholder="Total" value="0"></td>
					</tr>
					<tr>
						<td style="text-align:center">III</td>
						<td style="display:none"><input type="hidden" class="kelaskamar" value="3"></td>
						<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
						<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
						<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
						<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
						<td><input type="text" readonly class="form-control number total" placeholder="Total" value="0"></td>
					</tr>
					<tr>
						<td style="text-align:center">UTAMA</td>
						<td style="display:none"><input type="hidden" class="kelaskamar" value="4"></td>
						<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
						<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
						<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
						<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
						<td><input type="text" readonly class="form-control number total" placeholder="Total" value="0"></td>
					</tr>
				<?php }else{ ?>
					<?php foreach  ($list_detailtarif as $row) { ?>
					<tr>
						<td style="text-align:center"><?=GetKelas($row->kelas)?></td>
						<td style="display:none"><input type="hidden" class="kelaskamar" value="<?=$row->kelas?>"></td>
						<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="<?=$row->jasasarana?>"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="<?=$row->group_jasasarana?>"></td>
						<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="<?=$row->jasapelayanan?>"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="<?=$row->group_jasapelayanan?>"></td>
						<td><input type="text" class="form-control number bhp" placeholder="BHP" value="<?=$row->bhp?>"></td>
						<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value="<?=$row->group_bhp?>"></td>
						<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="<?=$row->biayaperawatan?>"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="<?=$row->group_biayaperawatan?>"></td>
						<td><input type="text" readonly class="form-control number total" placeholder="Total" value="<?=$row->total?>"></td>
					</tr>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

	<div class="row" style="margin-top:-25px">
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_radiologi/index/<?=$this->uri->segment(3)?>" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="old_headerpath" name="old_headerpath" value="{headerpath}" readonly>
	<input type="hidden" id="old_level0" name="old_level0" value="{level0}" readonly>
	<input type="hidden" id="path" name="path" value="{path}" readonly>
	<input type="hidden" id="level" name="level" value="{level}" readonly>
	<input type="hidden" id="xray_value" name="xray_value" />
	<input type="hidden" id="usg_value" name="usg_value" />
	<input type="hidden" id="cmb_value" name="cmb_value" />
	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<!-- Modal Edit Label -->
<div class="modal fade" id="modalSettingTarifTambahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="block-header bg-primary">
                <ul class="block-options">
                    <li>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                </ul>
                <h5 class="modal-title" id="modalSettingTarifTambahanLabel">Setting Tarif Tambahan</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Penambahan Tarif CITO Persentase</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="number" id="setting-penambahan-tarif-cito-persentase" class="form-control" name="setting-penambahan-tarif-cito-persentase" required="" aria-required="true" min="0" max="100" value="">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Penambahan Tarif CITO Rupiah</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="setting-penambahan-tarif-cito-rp" class="form-control number" name="setting-penambahan-tarif-cito-rp" required="" aria-required="true" min="0" max="100" value="">
                                    <span class="input-group-addon">Rp</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
								<br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Diskon Tarif Persentase</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="number" id="setting-diskon-tarif-persentase" class="form-control" name="setting-diskon-tarif-persentase" required="" aria-required="true" value="">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Diskon Tarif Rupiah</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="setting-diskon-tarif-rp" class="form-control number" name="setting-diskon-tarif-rp" required="" aria-required="true" value="">
                                    <span class="input-group-addon">Rp</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-update-setting-tarif-tambahan" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	var idtipe = <?=$idtipe?>;
	var label = <?=$idkelompok?>;

	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		if(idtipe == 1 && label == '0'){
			$("#form-detail-xray").show();
			$("#form-detail-xray-expose").show();
			$("#form-detail-xray-film").show();
			$("#form-detail-xray-tarif").show();
			$("#form-detail-usg").hide();
			$("#form-detail-cmb").hide();
		}else if(idtipe == 2 && label == '0'){
			$("#form-detail-xray").hide();
			$("#form-detail-xray-expose").hide();
			$("#form-detail-xray-film").hide();
			$("#form-detail-xray-tarif").hide();
			$("#form-detail-usg").show();
			$("#form-detail-cmb").hide();
		}else if((idtipe == 3 || idtipe == 4 || idtipe == 5) && label == '0'){
			$("#form-detail-xray").hide();
			$("#form-detail-xray-expose").hide();
			$("#form-detail-xray-film").hide();
			$("#form-detail-xray-tarif").hide();
			$("#form-detail-usg").hide();
			$("#form-detail-cmb").show();
		}else{
			$("#form-detail-xray").hide();
			$("#form-detail-xray-expose").hide();
			$("#form-detail-xray-film").hide();
			$("#form-detail-xray-tarif").hide();
			$("#form-detail-usg").hide();
			$("#form-detail-cmb").hide();
		}

		$(document).on("change", "#idtipe", function() {
			$("#form-detail-xray").hide();
			$("#form-detail-xray-expose").hide();
			$("#form-detail-xray-film").hide();
			$("#form-detail-xray-tarif").hide();
			$("#form-detail-usg").hide();
			$("#form-detail-cmb").hide();

			$("#kelompok").prop("checked", true);
			$("#idexpose").select2('val', '#');
			$("#idfilm").select2('val', '#');
			$("#harga").val('');

			$('#headerpath').find('option').remove().end().append('<option value="loading">Loading...</option>').val('loading');

			$.ajax({
				url: '{base_url}mtarif_radiologi/find_manage_parent/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					$("#level0").find("option").remove().end().append(data).val("");
				}
			});
		});

		$(document).on("change", "#level0", function() {
			$('#headerpath').find('option').remove().end().append(
				'<option value="loading">Loading...</option>').val('loading');

			// Get Subparent
			$.ajax({
				url: '{base_url}mtarif_radiologi/find_manage_subparent/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					$("#headerpath").find("option").remove().end().append(data).val("");
				}
			});

			// Get Path
			$.ajax({
				url: '{base_url}mtarif_radiologi/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					if ($('#level0 option:selected').text() == 'Root') {
						$("#path").val($('#level0 option:selected').val());
					} else {
						$("#path").val(data.path);
					}
					$("#level").val(data.level);
				}
			});
		});

		$(document).on("change", "#headerpath", function() {
			$.ajax({
				url: '{base_url}mtarif_radiologi/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					if ($('#headerpath option:selected').text() === 'Root') {
						$("#path").val($('#headerpath option:selected').val());
					} else {
						$("#path").val(data.path);
					}
					$("#level").val(data.level);
				}
			});
		});

		// tarif x-ray
		$(document).on("keyup", ".jasasaranaXRAY", function() {
			var jasasarana = parseInt($(this).val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhpXRAY").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatanXRAY").val().replace(/,/g, ''));
			var total = jasasarana + bhp + biayaperawatan;
			$(this).closest('tr').find(".totalXRAY").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".bhpXRAY", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasaranaXRAY").val().replace(/,/g, ''));
			var bhp = parseInt($(this).val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatanXRAY").val().replace(/,/g, ''));
			var total = jasasarana + bhp + biayaperawatan;
			$(this).closest('tr').find(".totalXRAY").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".biayaperawatanXRAY", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasaranaXRAY").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhpXRAY").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).val().replace(/,/g, ''));
			var total = jasasarana + bhp + biayaperawatan;
			$(this).closest('tr').find(".totalXRAY").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});

		// tarif usg
		$(document).on("keyup", ".jasasaranaUSG", function() {
			var jasasarana = parseInt($(this).val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayananUSG").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhpUSG").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatanUSG").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".totalUSG").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".jasapelayananUSG", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasaranaUSG").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhpUSG").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatanUSG").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".totalUSG").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".bhpUSG", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasaranaUSG").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayananUSG").val().replace(/,/g, ''));
			var bhp = parseInt($(this).val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatanUSG").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".totalUSG").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".biayaperawatanUSG", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasaranaUSG").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayananUSG").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhpUSG").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".totalUSG").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});

		// tarif ctscan, mri, bmd
		$(document).on("keyup", ".jasasarana", function() {
			var jasasarana = parseInt($(this).val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".jasapelayanan", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".bhp", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".biayaperawatan", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});

		$(document).on("click", ".edit-button", function() {
			if ($(this).data('label') == 'jasasarana') {
				var label = 'Jasa Sarana';
			} else if ($(this).data('label') == 'jasapelayanan') {
				var label = 'Jasa Pelayanan';
			} else if ($(this).data('label') == 'bhp') {
				var label = 'BHP';
			} else if ($(this).data('label') == 'biayaperawatan') {
				var label = 'Biaya Perawatan';
			}

			$('#modalSettingTarifTambahanLabel').html('Setting Tarif Tambahan - ' + label);
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var xray_tbl = $('table#totaltarif_xray tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function (cell) {
						if($(cell).find("input").length >= 1){
								return $(cell).find("input").val();
						}else{
								return $(cell).html();
						}
				});
			});
			var usg_tbl = $('table#totaltarif_usg tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function (cell) {
						if($(cell).find("input").length >= 1){
								return $(cell).find("input").val();
						}else{
								return $(cell).html();
						}
				});
			});
			var cmb_tbl = $('table#totaltarif_cmb tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function (cell) {
						if($(cell).find("input").length >= 1){
								return $(cell).find("input").val();
						}else{
								return $(cell).html();
						}
				});
			});

			$("#xray_value").val(JSON.stringify(xray_tbl));
			$("#usg_value").val(JSON.stringify(usg_tbl));
			$("#cmb_value").val(JSON.stringify(cmb_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	function toggleJenis(label) {
		idtipe = $('#idtipe option:selected').val();
		if(idtipe == 1 && label == 'rincian'){
			$("#form-detail-xray").show();
			$("#form-detail-xray-expose").show();
			$("#form-detail-xray-film").show();
			$("#form-detail-xray-tarif").show();
			$("#form-detail-usg").hide();
			$("#form-detail-cmb").hide();
		}else if(idtipe == 2 && label == 'rincian'){
			$("#form-detail-xray").hide();
			$("#form-detail-xray-expose").hide();
			$("#form-detail-xray-film").hide();
			$("#form-detail-xray-tarif").hide();
			$("#form-detail-usg").show();
			$("#form-detail-cmb").hide();
		}else if((idtipe == 3 || idtipe == 4 || idtipe == 5) && label == 'rincian'){
			$("#form-detail-xray").hide();
			$("#form-detail-xray-expose").hide();
			$("#form-detail-xray-film").hide();
			$("#form-detail-xray-tarif").hide();
			$("#form-detail-usg").hide();
			$("#form-detail-cmb").show();
		}else{
			$("#form-detail-xray").hide();
			$("#form-detail-xray-expose").hide();
			$("#form-detail-xray-film").hide();
			$("#form-detail-xray-tarif").hide();
			$("#form-detail-usg").hide();
			$("#form-detail-cmb").hide();
		}
	}

	$(".edit-button").on("click", function () {
			selectedLabel = $(this).data("label");
			selectedRow = $(this).closest("tr");

			let citoPersentase = selectedRow.find(`.tarif_cito_persentase_${selectedLabel}`).val();
			let citoRp = selectedRow.find(`.tarif_cito_rp_${selectedLabel}`).val();
			let diskonPersentase = selectedRow.find(`.tarif_diskon_persentase_${selectedLabel}`).val();
			let diskonRp = selectedRow.find(`.tarif_diskon_rp_${selectedLabel}`).val();

			$("#setting-penambahan-tarif-cito-persentase").val(citoPersentase);
			$("#setting-penambahan-tarif-cito-rp").val(citoRp);
			$("#setting-diskon-tarif-persentase").val(diskonPersentase);
			$("#setting-diskon-tarif-rp").val(diskonRp);
	});

	$("#btn-update-setting-tarif-tambahan").on("click", function () {
			selectedRow.find(`.tarif_cito_persentase_${selectedLabel}`).val($("#setting-penambahan-tarif-cito-persentase").val());
			selectedRow.find(`.tarif_cito_rp_${selectedLabel}`).val($("#setting-penambahan-tarif-cito-rp").val());
			selectedRow.find(`.tarif_diskon_persentase_${selectedLabel}`).val($("#setting-diskon-tarif-persentase").val());
			selectedRow.find(`.tarif_diskon_rp_${selectedLabel}`).val($("#setting-diskon-tarif-rp").val());
	});

	function gatherDataFromRow(row) {
			var data = {
					'kelas': row.find('.kelaskamar').val(),
					'jasasarana': row.find('.jasasarana').val(),
					'group_jasasarana': row.find('.group_jasasarana').val(),
					'tarif_cito_persentase_jasasarana': row.find('.tarif_cito_persentase_jasasarana').val(),
					'tarif_cito_rp_jasasarana': row.find('.tarif_cito_rp_jasasarana').val(),
					'tarif_diskon_persentase_jasasarana': row.find('.tarif_diskon_persentase_jasasarana').val(),
					'tarif_diskon_rp_jasasarana': row.find('.tarif_diskon_rp_jasasarana').val(),
					'jasapelayanan': row.find('.jasapelayanan').val(),
					'group_jasapelayanan': row.find('.group_jasapelayanan').val(),
					'tarif_cito_persentase_jasapelayanan': row.find('.tarif_cito_persentase_jasapelayanan').val(),
					'tarif_cito_rp_jasapelayanan': row.find('.tarif_cito_rp_jasapelayanan').val(),
					'tarif_diskon_persentase_jasapelayanan': row.find('.tarif_diskon_persentase_jasapelayanan').val(),
					'tarif_diskon_rp_jasapelayanan': row.find('.tarif_diskon_rp_jasapelayanan').val(),
					'bhp': row.find('.bhp').val(),
					'group_bhp': row.find('.group_bhp').val(),
					'tarif_cito_persentase_bhp': row.find('.tarif_cito_persentase_bhp').val(),
					'tarif_cito_rp_bhp': row.find('.tarif_cito_rp_bhp').val(),
					'tarif_diskon_persentase_bhp': row.find('.tarif_diskon_persentase_bhp').val(),
					'tarif_diskon_rp_bhp': row.find('.tarif_diskon_rp_bhp').val(),
					'biayaperawatan': row.find('.biayaperawatan').val(),
					'group_biayaperawatan': row.find('.group_biayaperawatan').val(),
					'tarif_cito_persentase_biayaperawatan': row.find('.tarif_cito_persentase_biayaperawatan').val(),
					'tarif_cito_rp_biayaperawatan': row.find('.tarif_cito_rp_biayaperawatan').val(),
					'tarif_diskon_persentase_biayaperawatan': row.find('.tarif_diskon_persentase_biayaperawatan').val(),
					'tarif_diskon_rp_biayaperawatan': row.find('.tarif_diskon_rp_biayaperawatan').val(),
					'total': row.find('.total').val(),
			};
			return data;
	}

	function gatherAllDataFromTable() {
			var allData = [];

			$('#tarif-pemeriksaan tbody tr').each(function () {
					var rowData = gatherDataFromRow($(this));
					allData.push(rowData);
			});

			return allData;
	}
</script>
