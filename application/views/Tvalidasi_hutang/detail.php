<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_hutang/save_detail','class="form-horizontal push-10-t"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Jurnal</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="nojurnal" placeholder="No. Jurnal" name="nojurnal" value="{nojurnal}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                    </div>
                </div>
                
                
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Distributor</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{distributor}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Cara Bayar</label>
					<div class="col-md-8">
                    <h4><?=cara_bayar($st_cara_bayar)?></h4>
                   
					</div>
                </div>
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Kontrabon</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="{notransaksi}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=HumanDateShort($tanggal_transaksi)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Posting</label>
                    <div class="col-md-8">
                       <h2 class="block-title"><?=($st_posting=='1'?text_primary('SUDAH DIPOSTING'):text_default('MENUNGGU DIPOSTING'))?></h2>
                    </div>
                </div>
               
			</div>
            
        </div>
	</div>
	<?
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		
		$q="SELECT *from tvalidasi_hutang_bayar A WHERE A.idvalidasi='$id' AND A.st_biaya_tf='1'";
		
		$list_bayar=$this->db->query($q)->result();
	?>
	<div class="block-content">
		<div class="row">
			<label class="col-md-2 control-label" for="tanggal"></label>
            <div class="col-md-10">
					<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:35%"  class="text-center">KETERANGAN </th>
								<th style="width:20%"  class="text-center">NOMINAL</th>
								<th style="width:45%"  class="text-center">NO AKUN</th>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong>AKUN PELUNASAN HUTANG</strong></td>
								<td><input type="text"  readonly class="form-control decimal" id="nominal_hutang" placeholder="No. Faktur" name="nominal_hutang" value="<?=$nominal_hutang?>"></td>
								<td>
									<select id="idakun_hutang" <?=$disabel?> name="idakun_hutang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list as $r){?>
										<option value="<?=$r->id?>" <?=($idakun_hutang==$r->id?'selected':'')?>><?=$r->noakun.' - '.$r->namaakun?></option>
										
										<?}?>
										
									</select>
								</td>
							</tr>
							<tr>
								<td><strong>AKUN BIAYA CHEQ </strong></td>
								<td><input type="text"  readonly class="form-control decimal" id="nominal_cheq" placeholder="No. Faktur" name="nominal_cheq" value="<?=$nominal_cheq?>"></td>
								<td>
									<select id="idakun_cheq"  <?=$disabel?> name="idakun_cheq" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list as $r){?>
										<option value="<?=$r->id?>" <?=($idakun_cheq==$r->id?'selected':'')?>><?=$r->noakun.' - '.$r->namaakun?></option>
										
										<?}?>
										
									</select>
								</td>
							</tr>
							<tr>
								<td><strong>AKUN BIAYA MATERAI</strong></td>
								<td><input type="text"  readonly class="form-control decimal" id="nominal_materai" placeholder="No. Faktur" name="nominal_materai" value="<?=$nominal_materai?>"></td>
								<td>
									<select id="idakun_materai"  <?=$disabel?> name="idakun_materai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list as $r){?>
										<option value="<?=$r->id?>" <?=($idakun_materai==$r->id?'selected':'')?>><?=$r->noakun.' - '.$r->namaakun?></option>
										
										<?}?>
										
									</select>
								</td>
							</tr>
							<?
								$no=1;
							foreach($list_bayar as $row){
								
								?>
							<tr>
								<td>
									<strong>AKUN BIAYA TRANSFER <?=$no?></strong>
									<input type="hidden"  readonly class="form-control" name="iddet[]" value="<?=$row->id?>">
								</td>
								<td><input type="text"  readonly class="form-control decimal" id="nominal_materai" placeholder="No. Faktur" name="nominal_materai" value="<?=$row->nominal?>"></td>
								<td>
									<select  name="idakun_tf[]"  <?=$disabel?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list as $r){?>
										<option value="<?=$r->id?>" <?=($row->idakun==$r->id?'selected':'')?>><?=$r->noakun.' - '.$r->namaakun?></option>
										
										<?}?>
										
									</select>
								</td>
							</tr>
							<?
							$no=$no+1;
							}?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_hutang" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
						<button class="btn btn-success" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Update & Posting</button>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update Akun</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   
});


</script>