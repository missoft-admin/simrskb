<div class="row">
	<div class="col-lg-4">
		<!-- Bootstrap Register -->
		<div class="block block-themed">
			<div class="block-header bg-success">
				<ul class="block-options">
					<li>
						<button id="add_menu" type="button" data-toggle="block-option" title="Add Menu Utama" ><i class="si si-plus"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Menu Utama</h3>
			</div>
			<div class="block-content">
				<form class="form-horizontal push-5-t" action="base_forms_premade.html" method="post" onsubmit="return false;">
					<table id="tabel_utama" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="3%">id</th>
								<th width="5%">No</th>
								<th width="80%">Main Menu</th>
								<th width="15%">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<!-- {{ @action }} -->
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<!-- END Bootstrap Register -->
	</div>
	<div class="col-lg-4">
		<!-- Material Register -->
		<div class="block block-themed">
			<div class="block-header bg-success">
				<ul class="block-options">
					<li>
						<button id="add_sub" type="button" data-toggle="block-option" title="Add Sub Menu" ><i class="si si-plus"></i></button>
					</li>
					
				</ul>
				<h3 class="block-title">Sub Menu</h3>
			</div>
			<div class="block-content">
				<form class="form-horizontal push-10-t push-10" action="base_forms_premade.html" method="post" onsubmit="return false;">
					<div class="form-group  has-success">
                        <label for="t_menu_utama" class="col-sm-4 control-label">Menu Utama</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control" id="t_menu_utama" placeholder="Menu Utama Selected" name="t_menu_utama" value="" required="" aria-required="true">
                            <input type="hidden" readonly class="form-control" id="t_menu_utama_id" placeholder="Menu Utama Selected" name="t_menu_utama_id" value="" required="" aria-required="true">
                        </div>
                    </div>
					
					<table id="tabel_sub" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="3%">id</th>
								<th width="5%">No</th>
								<th width="75%">Sub Menu</th>
								<th width="20%">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<!-- {{ @action }} -->
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<!-- END Material Register -->
	</div>
	<div class="col-lg-4">
		<!-- Material Register -->
		<div class="block block-themed">
			<div class="block-header bg-success">
				<ul class="block-options">
					<li>
						<button id="add_action" type="button" data-toggle="block-option" title="Add Action" ><i class="si si-plus"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Action</h3>
			</div>
			<div class="block-content">
				<form class="form-horizontal push-10-t push-10" action="base_forms_premade.html" method="post" onsubmit="return false;">
					<div class="form-group  has-success">
                        <label for="t_menu_utama" class="col-sm-4 control-label">Sub Menu</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control" id="t_sub_menu" placeholder="Sub Menu Selected" name="t_sub_menu" value="" required="" aria-required="true">
                            <input type="hidden" readonly class="form-control" id="t_sub_menu_id" placeholder="Menu Utama Selected" name="t_sub_menu_id" value="" required="" aria-required="true">
                        </div>
                    </div>
					<table id="tabel_action" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="3%">id</th>
								<th width="5%">No</th>
								<th width="75%">Action</th>
								<th width="20%">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<!-- {{ @action }} -->
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<!-- END Material Register -->
	</div>
	
</div>
<div class="modal" id="modalAdd" role="dialog" aria-hidden="true" aria-labelledby="modalAdd">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Add Menu Utama</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                       
						<div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">No Urut</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control number" id="menu_index" placeholder="No Urut" name="menu_index" value="">
									<input type="hidden" id="menu_utama_id" name="menu_utama_id" value="">
                                    
                                </div>								
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">Menu Utama</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control" id="menu_utama" placeholder="Nama Menu Utama" name="menu_utama" value="">
							
                                </div>								
                            </div>
                        </div>
						
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-success" type="button" id="simpan_menu">Simpan</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalAddSub" role="dialog" aria-hidden="true" aria-labelledby="modalAddSub">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Add Sub Menu</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                       
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">Menu Utama</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="text" class="form-control" readonly id="a_menu_utama" placeholder="a_menu_utama" name="a_menu_utama" value="">
                                    <input type="hidden" id="a_rowIndex" name="a_rowIndex" value="">
                                    <input type="hidden" id="a_menu_utama_id" name="a_menu_utama_id" value="">
                                    
                                </div>								
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">No Urut</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control number" id="a_sub_menu_index" placeholder="No Urut" name="a_sub_menu_index" value="">
							
                                    
                                </div>								
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">Sub Menu</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control" id="a_sub_menu" placeholder="Sub Menu" name="a_sub_menu" value="">
							
                                </div>								
                            </div>
                        </div>
						
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-success" type="button" id="simpan_sub">Simpan</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalAddAction" role="dialog" aria-hidden="true" aria-labelledby="modalAddAction">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Add Action</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                       
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">Sub Menu</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="text" class="form-control" readonly id="b_sub_menu" placeholder="b_sub_menu" name="b_sub_menu" value="">
                                    <input type="hidden" id="b_rowIndex" name="b_rowIndex" value="">
                                    <input type="hidden" id="b_sub_menu_id" name="b_sub_menu_id" value="">
                                    
                                </div>								
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">No Urut</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control number" id="b_action_index" placeholder="No Urut" name="b_action_index" value="">
							
                                    
                                </div>								
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-3 control-label" for="a_menu_utama">Action</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control" id="b_action" placeholder="Action" name="b_action" value="">							
                                </div>								
                            </div>
                        </div>
						
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-success" type="button" id="simpan_action">Simpan</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
  var table;
  var t_sub;
  var t_action;
 
	t_action = $('#tabel_action').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [0,4], "visible": false },
						 {"targets": [1], className: "text-center" },
						 {"targets": [2], className: "text-left" },						 
						 {"targets": [3], className: "text-center" }						 
						 ]
    })
	table = $('#tabel_utama').DataTable({
		paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
		columnDefs: [{ "targets": [0], "visible": false },
						 {"targets": [1], className: "text-center" },
						 {"targets": [2], className: "text-left" },						 
						 {"targets": [3], className: "text-center" }						 
						 ]
		 })
  $(document).ready(function () {
		$(".number").number(true,0,'.',',');
		load_menu_utama();	
	
  });
  
  function load_menu_utama(){
	  table.clear().draw(false);
		$.ajax({
			url: '{site_url}mresources/get_utama',
			dataType: "json",
			success: function(data) {
				$.each(data, function (i,row) {
					table.row.add([row.id, row.index, row.menu
					,'<button class="btn btn-xs btn-success edit_menu" type="button" data-toggle="tooltip" title="Edit Menu" ><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-info main_menu" type="button" data-toggle="tooltip" title="List Sub Menu" ><i class="fa fa-arrow-right"></i></button>']).draw(false)   ;
					// console.log(row.id2);
				})
			}
		});
		
  }
  function load_sub($id){
	  $('#tabel_sub').DataTable().destroy();
	   t_sub = $('#tabel_sub').DataTable({
        paging: true,
        info: false,
        sort: false,
        searching: true,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [0], "visible": false },
						 {"targets": [1,3], className: "text-center" },
						 {"targets": [2], className: "text-left" },						 
						 {"targets": [3], width: "20%" },						 
						 {"targets": [1], width: "10%" },						 
						 {"targets": [2], width: "70%" },						 
						 ]
    })
	  t_sub.clear().draw(false);
		$.ajax({
			url: '{site_url}mresources/get_sub/'+$id,
			dataType: "json",
			success: function(data) {
				$.each(data, function (i,row) {
					t_sub.row.add([row.id2, row.index2, row.menu2
					,'<button class="btn btn-xs btn-success edit_sub_menu" type="button" data-toggle="tooltip" title="Edit Sub Menu" ><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-info sub_menu" type="button" data-toggle="tooltip" title="List Action" data-original-title="Edit Client"><i class="fa fa-arrow-right"></i></button>']).draw(false)   ;
					// console.log(row.id2);
				})
			}
		});
  }
  function load_action($id){
	  t_action.clear().draw(false);
		$.ajax({
			url: '{site_url}mresources/get_action/'+$id,
			dataType: "json",
			success: function(data) {
				$.each(data, function (i,row) {
					t_action.row.add([row.id3, row.index3, row.action+' ('+row.id3+')'
					,'<button class="btn btn-xs btn-success edit_action" type="button" data-toggle="tooltip" title="Edit Action" ><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-danger delete_action" type="button" data-toggle="tooltip" title="List Action"><i class="fa fa-remove"></i></button>',row.action]).draw(false)   ;
					
					// console.log(row.id2);
				})
			}
		});
  }
   $(document).on('click','.main_menu', function(){
		// success
		$('#tabel_utama tbody tr').removeClass('success');
		$row = $(this).closest('tr');
		$row.addClass("success");
		var id = table.cell($(this).parents('tr'),0).data();
		$("#t_menu_utama").val(table.cell($(this).parents('tr'),2).data());
		$("#t_menu_utama_id").val(id);
		load_sub(id);

	})
	
	$(document).on('click','.sub_menu', function(){
		// success
		$('#tabel_sub tbody tr').removeClass('success');
		$row = $(this).closest('tr');
		$row.addClass("success");
		var id = t_sub.cell($(this).parents('tr'),0).data();
		$("#t_sub_menu").val(t_sub.cell($(this).parents('tr'),2).data());
		$("#t_sub_menu_id").val(id);
		load_action(id);
	})
	$(document).on('click','.edit_sub_menu', function(){
		$('#tabel_sub tbody tr').removeClass('success');
		$row = $(this).closest('tr');
		$row.addClass("success");
		var id = t_sub.cell($(this).parents('tr'),0).data();
		var nama_sub = t_sub.cell($(this).parents('tr'),2).data();
		$("#a_rowIndex").val(id);
		$("#a_menu_utama_id").val($("#t_menu_utama_id").val());
		$("#a_sub_menu_index").val(t_sub.cell($(this).parents('tr'),1).data());
		$("#a_sub_menu").val(nama_sub);
		$("#a_menu_utama").val($("#t_menu_utama").val());
		// alert(id);
		$('#modalAddSub .block-title').text('Edit Sub Menu ['+nama_sub+']');
        $('#modalAddSub').modal('toggle');
	})
	$(document).on('click','.edit_action', function(){
		$('#tabel_action tbody tr').removeClass('success');
		$row = $(this).closest('tr');
		$row.addClass("success");
		var id = t_action.cell($(this).parents('tr'),0).data();
		var nama_sub = t_action.cell($(this).parents('tr'),4).data();
		$("#b_rowIndex").val(id);
		$("#b_sub_menu_id").val($("#t_sub_menu_id").val());
		$("#b_action_index").val(t_action.cell($(this).parents('tr'),1).data());
		$("#b_action").val(nama_sub);
		// $("#b_action").val($("#t_menu_utama").val());
		// alert(id);
		$('#modalAddAction .block-title').text('Edit Action ['+nama_sub+']');
        $('#modalAddAction').modal('toggle');
	})
	$(document).on('click','.edit_menu', function(){
		$('#tabel_utama tbody tr').removeClass('success');
		$row = $(this).closest('tr');
		$row.addClass("success");
		var id = table.cell($(this).parents('tr'),0).data();
		var nama_menu = table.cell($(this).parents('tr'),2).data();
		var index = table.cell($(this).parents('tr'),1).data();
		$("#menu_utama_id").val(id);
		$("#menu_utama").val(nama_menu);
		$("#menu_index").val(index);
		$('#modalAdd .block-title').text('Edit Menu ['+nama_menu+']');
        $('#modalAdd').modal('toggle');
	})
	$(document).on('click','#add_menu', function(){
		
		$("#menu_index").val('');
		$("#menu_utama_id").val('');
		$("#menu_utama").val('');
		
		$('#modalAdd .block-title').text('Tambah Menu Utama');
        $('#modalAdd').modal('toggle');
		var rowCount = $('#tabel_utama tr').length;
		$("#menu_index").val(rowCount);
		$("#menu_index").focus();
	})
	$(document).on('click','#add_sub', function(){
		var id=$("#t_menu_utama_id").val();
		if (id==''){
			// alert('Silahkan Pilih Menu Utama');
			// return false;
			sweetAlert("Maaf...", "Menu Utama Belum dipilih!", "error");
			// $("#kuantitas").focus();
			return false;
		}
		$("#a_menu_utama").val($("#t_menu_utama").val());
		$("#a_menu_utama_id").val(id);
		$("#a_sub_menu").val('');
		$("#a_rowIndex").val('');
		
		$('#modalAddSub .block-title').text('Tambah Sub Menu ['+$("#t_menu_utama").val()+']');
        $('#modalAddSub').modal('toggle');
		var rowCount = $('#tabel_sub tr').length;
		$("#a_sub_menu_index").val(rowCount);
		$("#a_sub_menu_index").focus();
	})
	$(document).on('click','#add_action', function(){
		var id=$("#t_sub_menu_id").val();
		if (id==''){
			// alert('Silahkan Pilih Menu Utama');
			// return false;
			sweetAlert("Maaf...", "Sub Menue Belum dipilih!", "error");
			// $("#kuantitas").focus();
			return false;
		}
		$("#b_sub_menu").val($("#t_sub_menu").val());
		$("#b_sub_menu_id").val(id);
		$("#b_action_index").val('');
		$("#b_rowIndex").val('');
		$("#b_action").val('');
		
		$('#modalAddAction .block-title').text('Tambah Action ['+$("#t_sub_menu").val()+']');
        $('#modalAddAction').modal('toggle');
		var rowCount = $('#tabel_action tr').length;
		$("#b_action_index").val(rowCount);
		$("#b_action_index").focus();
	})
	$(document).on('click','#simpan_menu', function(){
		var id = $("#menu_utama_id").val();
		var index = $("#menu_index").val();
		var menu = $("#menu_utama").val();
		if (id){
			$.ajax({
					url: '{site_url}mresources/edit_menu/',
					dataType: "JSON",
					method: "POST",
					data : {id:id,index:index,menu:menu},
					success: function(data) {
						  $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil disimpan'});
						load_menu_utama();
						 $("#modalAdd").modal("hide");
						 // console.log(data);
					}
			});
		}else{
			$.ajax({
					url: '{site_url}mresources/simpan_menu/',
					dataType: "JSON",
					method: "POST",
					data : {index:index,menu:menu},
					success: function(data) {
						  $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil disimpan'});
						  load_menu_utama();
						 $("#modalAdd").modal("hide");
						 // console.log(data);
					}
			});
		}
	})
	$(document).on('click','#simpan_sub', function(){
		var id2 = $("#a_rowIndex").val();
		var id_menu = $("#a_menu_utama_id").val();
		var index2 = $("#a_sub_menu_index").val();
		var menu2 = $("#a_sub_menu").val();
		if (id2){
			$.ajax({
					url: '{site_url}mresources/edit_sub/',
					dataType: "JSON",
					method: "POST",
					data : {id2:id2,index2:index2,menu2:menu2},
					success: function(data) {
						  $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil disimpan'});
						  // $('#tabel_sub').ajax.reload( null, false );
						  // $('#tabel_sub')
						 load_sub(id_menu);
						 $("#modalAddSub").modal("hide");
						 console.log(data);
					}
			});
		}else{
			$.ajax({
					url: '{site_url}mresources/simpan_sub/',
					dataType: "JSON",
					method: "POST",
					data : {id_menu:id_menu,index2:index2,menu2:menu2},
					success: function(data) {
						  $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil disimpan'});
						 load_sub(id_menu);
						 $("#modalAddSub").modal("hide");
						 console.log(data);
					}
			});
		}
	})

	$(document).on('click','#simpan_action', function(){
		var id3 = $("#b_rowIndex").val();
		var id_menu_sub = $("#b_sub_menu_id").val();
		var index3 = $("#b_action_index").val();
		var action = $("#b_action").val();
		if (id3){
			$.ajax({
					url: '{site_url}mresources/edit_action/',
					dataType: "JSON",
					method: "POST",
					data : {id3:id3,index3:index3,action:action},
					success: function(data) {
						  $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil disimpan'});
						 load_action(id_menu_sub);
						 $("#modalAddAction").modal("hide");
						 // console.log(data);
					}
			});
		}else{
			$.ajax({
					url: '{site_url}mresources/simpan_action/',
					dataType: "JSON",
					method: "POST",
					data : {id_menu_sub:id_menu_sub,index3:index3,action:action},
					success: function(data) {
						  $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil disimpan'});
						 load_action(id_menu_sub);
						 $("#modalAddAction").modal("hide");
						 // console.log(data);
					}
			});
		}
	})
	$(document).on('click','.delete_action', function(){
		var id3 = t_action.cell($(this).parents('tr'),0).data();;
		var id_menu_sub = $("#t_sub_menu_id").val();
		
		if (id3){
			$.ajax({
					url: '{site_url}mresources/delete_action/',
					dataType: "JSON",
					method: "POST",
					data : {id3:id3},
					success: function(data) {
						  $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil Dihapus'});
						 load_action(id_menu_sub);
						 // $("#modalAddAction").modal("hide");
						 // console.log(data);
					}
			});
		}
	})

</script>