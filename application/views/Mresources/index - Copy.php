<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<?php if (button_roles('mresources/save')): ?>
			<ul class="block-options"> 
				<li> 
					<button class="btn" data-toggle="modal" data-target="#add-resource"><i class="fa fa-plus"></i></button> 
				</li> 
			</ul>
		<?php endif ?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<table id="mresource" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="3%">No</th>
					<th>Nama</th>
          <th>Tipe</th>
          <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<!-- {{ @action }} -->
			</tbody>
		</table>
	</div>
</div>

<!-- @ -->
<div class="modal in" id="add-resource" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tambah Resource</h3>
				</div>
				<div class="block-content">
          <!-- @ -->

          <div class="row">
            <div class="col-md-12">

              <form id="form-resource" class="form-horizontal push-10-t">
								<div class="col-md-12">

									<div class="form-group">
										<label for="" class="control-label col-md-3">Nama</label>
										<div class="col-md-8">
											<input id="nama-resource" class="form-control" type="text" name="nama-resource">
										</div>
									</div>

                  <div class="form-group">
                    <label for="" class="control-label col-md-3">Tipe</label>
                    <div class="col-md-8">
											<label class="radio-inline">
												<input type="radio" name="tipe-resource" value="0">Action
											</label>
											<label class="radio-inline">
												<input type="radio" name="tipe-resource" value="1">Controller
											</label>
                      <label class="radio-inline">
												<input type="radio" name="tipe-resource" value="2">Module
											</label>
                      <label class="radio-inline">
												<input type="radio" name="tipe-resource" value="3">Other
											</label>
										</div>
                  </div>

									<div class="form-group">
										<label for="" class="control-label col-md-3">Jenis</label>
										<div class="col-md-8">
											<label class="radio-inline">
												<input type="radio" name="jenis-resource" value="0">Kelompok
											</label>
											<label class="radio-inline">
												<input type="radio" name="jenis-resource" value="1">Rincian
											</label>
										</div>
									</div>

									<div class="form-group">
										<label for="" class="control-label col-md-3">Parent Id</label>
										<div class="col-md-8">
											<select id="parent-id" class="form-control js-select2" type="text" name="parent-id" style="width:100%;">
												<?= $list_parent ?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-3"></div>
										<div class="col-md-8">
											<button id="save-resource" class="btn btn-sm btn-primary" type="button" name="button">Save</button>
										</div>
									</div>

								</div>

								<input type="hidden" id="path" name="path" value="1" readonly>
								<input type="hidden" id="level" name="level" value="0" readonly>

              </form>

            </div>
          </div>

          <!-- @ -->
				</div>
				<div class="modal-footer">
					<div style="float:right;">
						<button type="button" class="btn btn-sm btn-default text-uppercase" data-dismiss="modal" style="width:100px;font-size:11px;">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- @ -->

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

  $(document).ready(function () {
		$.ajax({
			url: '{base_url}mresources/get_child_level/' + 0,
			method: 'GET',
			dataType: 'json',
			success: function(data) {
				if ($('#parent-id option:selected').text() === 'Root') {
					$("#path").val($('#parent-id option:selected').val());
				} else {
					$("#path").val(data.path);
				}
				$("#level").val(data.level);
			}
		});

		$(document).on("change", "#parent-id", function() {
			$.ajax({
				url: '{base_url}mresources/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					if ($('#parent-id option:selected').text() === 'Root') {
						$("#path").val($('#parent-id option:selected').val());
					} else {
						$("#path").val(data.path);
					}
					$("#level").val(data.level);
				}
			});
		});

		var showResource	= $('#mresource').DataTable({
			"oLanguage":{
				"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
			},
			"ajax": {
				"url":"{site_url}mresources/showResource",
			},
			"autoWidth": false,
			"pageLength": 50,
			"lengthChange": true,
			"ordering": true,
			"processing": true,
			"order": []
		});

		$(document).on("click", "#save-resource", function() {
			$.ajax({
				url: '{site_url}mresources/save',
				type: 'POST',
				data: $('#form-resource').serialize(),
				complete: function() {
					swal("Berhasil!", "Menambahkan Resource", "success").then(function() {
						window.location.assign('{site_url}mresources');
					});
					showResource.ajax.reload();
				}
			});
		});

    $(document).on('click', '.delete-resource', function() {
      var id = $(this).attr('data-value');

      swal({
        title: "Anda Yakin ?",
        text : "Ingin menghapus data ini ?",
        type : "success",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#34a263",
        cancelButtonText: "Batalkan",
      }).then(function() {
        $.ajax({
          url: '{site_url}mresources/deleteResource',
          type: 'POST',
          dataType: 'json',
          data: {id:id},
          complete: function(data) {
            showResource.ajax.reload();
          }
        });
      });
    });


  });

</script>
