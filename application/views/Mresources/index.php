<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('215'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('217'))){ ?>
			<ul class="block-options"> 
				<li> 
					<a href="{site_url}mresources/add" class="btn btn-default" >
                        <i class="fa fa-plus"></i> Tambah
                    </a>
				</li> 
			</ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:25px">
			<?php if (UserAccesForm($user_acces_form,array('216'))){ ?>
		<div class="row">
            <?php echo form_open('mresources/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="id">Menu Utama</label>
                    <div class="col-md-8">
                        <select id="id" name="id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($id == '#' ? 'selected' : ''); ?>>Semua Menu Utama</option>
                            <?php foreach ($list_menu as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?=($id == $row->id ? 'selected' : ''); ?>><?php echo $row->menu; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Sub Menu</label>
                    <div class="col-md-8">
                        <select id="id_menu_sub" name="id_menu_sub" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($id_menu_sub == '#' ? 'selected' : ''); ?>>-Semua Sub Menu-</option>
                            <?php foreach ($list_sub as $row) { ?>
                                    <option value="<?php echo $row->id2; ?>" <?=($id_menu_sub == $row->id2 ? 'selected' : ''); ?>><?php echo $row->menu2; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
				
				
                
            </div>
			<div class="col-md-6">
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Index</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($status == '#' ? 'selected' : ''); ?>>Semua</option>
                            <option value="1" <?=($status == '1' ? 'selected' : ''); ?>>Sudah Ada Action</option>
                            <option value="2" <?=($status == '2' ? 'selected' : ''); ?>>Belum Ada Action</option>
                            
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Action</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="action" placeholder="Aksi" name="action" value="{action}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
			<?}?>
	</div>
	<div class="block-content">
		<table id="tabel_index" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="3%">#</th>
					<th width="3%">No</th>
					<th style="width:20%">Menu Utama</th>
					<th style="width:20%">Form</th>
					<th style="width:20%">Action</th>
					<th style="width:20%">Index</th>
				</tr>
			</thead>
			<tbody>
				<!-- {{ @action }} -->
			</tbody>
		</table>
	</div>
</div>
<?}?>
<!-- @ -->

<!-- @ -->

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

  $(document).ready(function () {
		table = $('#tabel_index').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false }],
            ajax: { 
                url: '{site_url}mresources/get_index', 
                type: "POST" ,
                dataType: 'json'
            }
        });

  });
  $("#id").change(function(){
		console.log($(this).val());
		$('#id_menu_sub')
					.find('option')
					.remove()			
					.end()
					.append('<option value="#">- Semua Sub Menu -</option>')
					.val('').trigger("change");
		if ($(this).val()!=''){
			$.ajax({
			  url: '{site_url}mresources/get_sub/'+$(this).val(),
			  dataType: "json",
			  success: function(data) {
				  
				$.each(data, function (i,row) {
					console.log(row.menu2);
					$('#id_menu_sub')
					.append('<option value="' + row.id2 + '">' + row.menu2 + '</option>');
				});	
			  }
			});
		}
		
	});
  $(document).on('click', '.delete-resource', function() {
      var id = $(this).attr('data-value');

      swal({
        title: "Anda Yakin ?",
        text : "Ingin menghapus data ini ?",
        type : "success",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#34a263",
        cancelButtonText: "Batalkan",
      }).then(function() {
        $.ajax({
          url: '{site_url}mresources/deleteResource',
          type: 'POST',
          dataType: 'json',
          data: {id:id},
          complete: function(data) {
            showResource.ajax.reload();
          }
        });
      });
    });


</script>
