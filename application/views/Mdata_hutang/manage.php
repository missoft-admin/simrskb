<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_hutang" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdata_hutang/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="nama">Nama Hutang</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama Hutang" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nominal_default">Nominal Default</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="nominal_default" placeholder="Nominal Default" name="nominal_default" value="{nominal_default}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nominal_default">Np. Akun</label>
				<div class="col-md-7">
					<select name="idakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="#">-Pilih No Akun-</option>
						<?php foreach ($list_akun as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $idakun ? 'selected="selected"':'')?>><?=$row->noakun.'-'.$row->namaakun?></option>
						<? } ?>
					</select>
				</div>
			</div>
			
			
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mdata_hutang" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		
	})	
	
	
	function validate_final(){
		
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Hutang", "error");
			return false;
		}
		
		if ($("#nominal_default").val()=='' || $("#nominal_default").val()=='0'){
			sweetAlert("Maaf...", "Tentukan Nominal Default", "error");
			return false;
		}
		if ($("#idakun").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Akun", "error");
			return false;
		}
		return true;

		
	}
</script>