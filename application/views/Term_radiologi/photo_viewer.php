<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Preview Photo Radiologi</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/zoomist@2/zoomist.css" />
    <script src="https://cdn.jsdelivr.net/npm/zoomist@2/zoomist.umd.js"></script>
    <style type="text/css">
        body {
            display: flex;
            flex-direction: row;
            align-items: stretch;
            background-color: #000;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
        .sidebar {
            width: 200px;
            background-color: #333;
            overflow-y: auto;
            color: white;
            padding: 20px;
            height: 100vh; /* Set height to 100% of the viewport height */
        }

        .sidebar img {
            display: block;
            width: 100%;
            margin-bottom: 10px;
            cursor: pointer;
        }
        
        .sidebar::-webkit-scrollbar {
            width: 0;
        }

        .sidebar::-webkit-scrollbar-track {
            background: transparent; /* Make scrollbar track transparent */
        }
        .zoomist-container {
            width: 100%;
            max-width: 580px;
            margin-left: auto;
            margin-right: auto;
        }

        .zoomist-wrapper {
            background: #000;
        }

        .zoomist-image {
            width: 100%;
            aspect-ratio: 1;
        }

        .zoomist-image img {
            width: 100%;
            /* height: 100%; */
            object-fit: cover;
            object-position: center;
        }

        .information {
            width: 100%; /* Atur lebar sesuai kebutuhan */
            max-width: 400px; /* Tidak lebih dari 400px */
            padding: 16px;
            color: #fff;
            overflow: auto; /* Aktifkan overflow jika konten lebih panjang */
        }

        .information h1 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        table {
            width: 100%; /* Gunakan lebar penuh dari container */
        }

        table tr td {
            padding: 5px;
            word-break: break-all; /* Memastikan teks patah ke baris baru jika terlalu panjang */
        }

        .rotate-button {
            position: absolute;
            top: 20px;
            left: -80px;
            transform: translateX(-50%);
            background-color: var(--zoomist-zoomer-button-color);
            color: #000;
            border: none;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 8px;
        }
    </style>
</head>

<body>
    <div class="sidebar">
        <?php foreach ($list_photo as $row) { ?>
            <a href="<?= base_url(); ?>term_radiologi/preview_photo/<?= $row->id; ?>/<?= $tipe; ?>/<?= $row->pemeriksaan_id; ?>">
                <img src="<?= $photo_url; ?><?= $row->filename; ?>" alt="<?= $row->filename; ?>">
            </a>
        <?php } ?>
    </div>
    <div class="zoomist-container">
        <button onclick="rotateImage()" class="rotate-button">Rotate</button>
        <div class="zoomist-wrapper" id="zoomistWrapper">
            <div class="zoomist-image">
                <img id="previewImage" src="<?= $current_photo; ?>" />
            </div>
        </div>
    </div>
    <div class="information">
        <h1>Information</h1>
        <table>
            <tr>
                <td width="100px">Filename</td>
                <td>:</td>
                <td><?= $current_photo_filename; ?></td>
            </tr>
            <tr>
                <td>Uploaded at</td>
                <td>:</td>
                <td><?= $current_photo_uploaded_at; ?></td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        var currentRotation = 0; // Inisialisasi rotasi saat ini

        function rotateImage() {
            var image = document.getElementById('zoomistWrapper');
            currentRotation += 90; // Rotasi 90 derajat
            image.style.transform = 'rotate(' + currentRotation + 'deg)'; // Atur transformasi CSS
        }

        new Zoomist('.zoomist-container', {
            // Optional parameters
            maxScale: 4,
            bounds: true,
            // if you need slider
            slider: true,
            // if you need zoomer
            zoomer: true
        })
    </script>
</body>

</html>
