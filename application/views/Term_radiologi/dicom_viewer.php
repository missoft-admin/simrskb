<!DOCTYPE html>

<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link href="<?= base_url().'assets/js/dicom/cornerstone.min.css'; ?>" rel="stylesheet">
  <style>
    body,
    html {
      margin: 0;
      padding: 0;
      overflow: hidden;
    }

    #dicomImage {
      width: 100vw;
      height: 100vh;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .cornerstone-canvas {
      display: block;
      width: 100%;
      height: 100%;
    }
  </style>
</head>

<body>

    <div style="display: none;">
      <div id="loadProgress">Image Load Progress:</div>
    </div>

    <div class="row">
      <div id="dicomImage" style="width: 100vw; height: 100vh;">
        <canvas class="cornerstone-canvas" style="display: block; width: 100%; height: 100%;" width="512" height="512"></canvas>
      </div>
    </div>

  <script src="<?= base_url().'assets/js/dicom/cornerstone.min.js'; ?>"></script>
  <script src="<?= base_url().'assets/js/dicom/cornerstoneMath.min.js'; ?>"></script>
  <script src="<?= base_url().'assets/js/dicom/cornerstoneTools.min.js'; ?>"></script>

  <!-- include the dicomParser library as the WADO image loader depends on it -->
  <script src="<?= base_url().'assets/js/dicom/dicomParser.min.js'; ?>"></script>

  <!-- uids -->
  <script src="<?= base_url().'assets/js/dicom/uids.js'; ?>"></script>

  <!-- Lines ONLY required for this example to run without building the project -->
  <script>
    window.cornerstoneWADOImageLoader || document.write('<script src="https://unpkg.com/cornerstone-wado-image-loader">\x3C/script>')
  </script>
  <script src="<?= base_url().'assets/js/dicom/cornerstone-wado-image-loader'; ?>"></script>
  <script src="<?= base_url().'assets/js/dicom/initializeWebWorkers.js'; ?>"></script>

  <script>
    cornerstoneWADOImageLoader.external.cornerstone = cornerstone;

    // this function gets called once the user drops the file onto the div
    function handleFileSelect(evt) {
      evt.stopPropagation();
      evt.preventDefault();

      // Get the FileList object that contains the list of files that were dropped
      const files = evt.dataTransfer.files;

      // this UI is only built for a single file so just dump the first one
      file = files[0];
      const imageId = cornerstoneWADOImageLoader.wadouri.fileManager.add(file);
      loadAndViewImage(imageId);
    }

    function handleDragOver(evt) {
      evt.stopPropagation();
      evt.preventDefault();
      evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    // Setup the dnd listeners.
    const dropZone = document.getElementById('dicomImage');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);

    cornerstoneWADOImageLoader.configure({
      beforeSend: function (xhr) {
        // Add custom headers here (e.g. auth tokens)
        //xhr.setRequestHeader('x-auth-token', 'my auth token');
      },
    });

    let loaded = false;

    function loadAndViewImage(imageId) {
      const element = document.getElementById('dicomImage');
      const start = new Date().getTime();
      cornerstone.loadImage(imageId).then(function (image) {
        console.log(image);
        const viewport = cornerstone.getDefaultViewportForImage(element, image);
        cornerstone.displayImage(element, image, viewport);
        if (loaded == false) {
          cornerstoneTools.mouseInput.enable(element);
          cornerstoneTools.mouseWheelInput.enable(element);
          cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
          cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
          cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
          cornerstoneTools.zoomWheel.activate(element); // zoom is the default tool for middle mouse wheel

          // cornerstoneTools.imageStats.enable(element);
          loaded = true;
        }
      }, function (err) {
        console.log(err);
      });
    }

    cornerstone.events.addEventListener('cornerstoneimageloadprogress', function (event) {
      const eventData = event.detail;
      const loadProgress = document.getElementById('loadProgress');
      loadProgress.textContent = `Image Load Progress: ${eventData.percentComplete}%`;
    });

    const element = document.getElementById('dicomImage');
    cornerstone.enable(element);

    document.addEventListener('DOMContentLoaded', function () {
      const filePath = '<?= $url; ?>';

      fetch(filePath)
        .then(response => response.blob())
        .then(blob => {
          const file = new File([blob], '');
          const imageId = cornerstoneWADOImageLoader.wadouri.fileManager.add(file);
          loadAndViewImage(imageId);
        })
        .catch(error => {
          console.error('Gagal memuat file:', error);
        });
    });
  </script>

</body>

</html>