<style>
    .note-editor.required {
        border: 1px solid red;
    }
</style>

<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block">
    <div class="block-content">
        <?php echo form_open('term_radiologi_usg_expertise/save', 'id="form-work"') ?>
        <div class="row">
            <div class="col-md-3">
                <h4>EKSPERTISE</h4>
                <h6><i>EKSPERTISE</i></h6>
            </div>
            <div class="col-md-9 text-right">
                <?php if ($hasil_pemeriksaan_id) { ?>
                    <div class="btn-group">
                        <button class="btn btn-warning btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-print"></i> Cetak Hasil
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li class=""><a href="#" onclick="cetakHasilPemeriksaan('{hasil_pemeriksaan_id}', 'FORMAT_1')">Format 1</a></li>
                            <li class=""><a href="#" onclick="cetakHasilPemeriksaan('{hasil_pemeriksaan_id}', 'FORMAT_2')">Format 2</a></li>
                        </ul>
                    </div>
                <? } ?>
                <button type="submit" class="btn btn-primary" name="form_submit" value="form-submit"><i class="fa fa-save"></i> Simpan</button>
                <button type="submit" class="btn btn-success" name="form_submit" value="form-submit-and-validation"><i class="fa fa-save"></i> Simpan Final</button>
                <a href="{site_url}term_radiologi_usg_expertise" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_hidden('kelas_id', $kelas_id); ?>
        <?php echo form_hidden('dokter_radiologi', $dokter_radiologi); ?>
        <?php echo form_close(); ?>

        <hr>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_tujuan_radiologi']); ?></label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="tujuan_radiologi" class="form-control" value="{tujuan_radiologi_label}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_dokter_peminta_pemeriksaan']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                            <option value="<?= $r->id; ?>" <?= $r->id == $dokter_peminta_id ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_diagnosa']); ?></label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="diagnosa" class="form-control" value="{diagnosa}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?></label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="catatan_permintaan" class="form-control" value="{catatan_permintaan}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_waktu_pemeriksaan']); ?></label>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <input type="text" disabled class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggal_permintaan" value="{tanggal_permintaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" disabled class="time-datepicker form-control" id="waktu_permintaan" value="{waktu_permintaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_prioritas']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="prioritas" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(249) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $prioritas ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pasien_puasa']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="pasien_puasa" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(87) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $pasien_puasa ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pengiriman_hasil']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="pengiriman_hasil" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(88) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_alergi_bahan_kontras']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="alergi_bahan_kontras" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(252) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $alergi_bahan_kontras ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pasien_hamil']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="pasien_hamil" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(253) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $pasien_hamil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <hr>

            <div class="col-md-4">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Waktu Pemeriksaan</label>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <input type="text" disabled class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggal_pemeriksaan" value="{tanggal_pemeriksaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" disabled class="time-datepicker form-control" id="waktu_pemeriksaan" value="{waktu_pemeriksaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Petugas Pemeriksa</label>
                    <div class="col-xs-12">
                        <select disabled class="js-select2 form-control" id="petugas_pemeriksaan" style="width: 100%;">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Nomor Foto</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="nomor_foto" class="form-control" value="{nomor_foto}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Dokter Radiologi</label>
                    <div class="col-xs-12">
                        <select disabled class="js-select2 form-control" id="dokter_radiologi" style="width: 100%;">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Jumlah Expose</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="jumlah_expose" class="form-control" value="{jumlah_expose}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Jumlah Film</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="jumlah_film" class="form-control" value="{jumlah_film}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">QP</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="qp" class="form-control" value="{qp}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">MAS</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="mas" class="form-control" value="{mas}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Posisi</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="posisi" class="form-control" value="{posisi}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Nomor Permintaan</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="nomor_permintaan" class="form-control" value="{nomor_permintaan}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Nomor Radiologi</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="nomor_radiologi" class="form-control" value="{nomor_radiologi}">
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-3">
                <table class="table table-bordered table-striped" id="table-pemeriksaan">
                    <thead>
                        <tr>
                            <th class="text-center">Nama Pemeriksaan</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="col-md-9">
                <div id="pemeriksaan-message"></div>
                <div id="pemeriksaan-content" style="display: none;">
                    <input name="pemeriksaan_id" id="pemeriksaan_id" type="hidden" value="">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-xs-12" for="">Nama Pemeriksaan</label>
                                <div class="col-xs-12">
                                    <input type="text" disabled id="pemeriksaan_nama" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="col-xs-12" for="">Flag Kritis</label>
                                <div class="col-xs-12">
                                    <select class="js-select2 form-control" id="pemeriksaan_flag" style="width: 100%;">
                                        <option value="1">Tidak</option>
                                        <option value="2">Ya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="col-xs-12" for="">Klinis</label>
                                <div class="col-xs-12">
                                    <textarea class="form-control summernote" id="pemeriksaan_klinis" placeholder="Klinis" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="col-xs-12" for="">Kesan</label>
                                <div class="col-xs-12">
                                    <textarea class="form-control summernote" id="pemeriksaan_kesan" placeholder="Kesan" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="col-xs-12" for="">Usul</label>
                                <div class="col-xs-12">
                                    <textarea class="form-control summernote" id="pemeriksaan_usul" placeholder="Usul" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-xs-12" for="">Hasil Pemeriksaan</label>
                                <div class="col-xs-12">
                                    <textarea class="form-control summernote" id="pemeriksaan_hasil" placeholder="Hasil Pemeriksaan" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="gallery" class="gallery" itemscope itemtype="http://schema.org/ImageGallery">
                        <div id="list-foto-radiologi" class="row"></div>
                    </div>
                </div>
                <br><br><br>
            </div>
        </div>
    </div>
</div>

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- Background of PhotoSwipe. 
            It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>
    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">
        <!-- Container that holds slides. 
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        </div>
        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
            <!--  Controls are self-explanatory. Order can be changed. -->
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <!-- element will get class pswp__preloader--active when preloader is running -->
            <div class="pswp__preloader">
            <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                <div class="pswp__preloader__donut"></div>
                </div>
            </div>
            </div>
        </div>
        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
        </div>
        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
        </button>
        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
        </button>
        <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
        </div>
        </div>
    </div>
</div>

<?php $this->load->view('Term_radiologi_usg_expertise/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataDokterRadiologi('{tujuan_radiologi}', '{dokter_radiologi}');
        loadDataPetugasProsesPemeriksaan('{tujuan_radiologi}', '{petugas_pemeriksaan}');
        loadDataPemeriksaan('{id}');
        checkPemeriksaanId();

        $('.summernote').summernote({
            height: 150,
            minHeight: null,
            maxHeight: null
        });

        $(document).on('click', '.btn-open', function() {
            if (!isRequiredCheck()) {
                swal({
                    text: "Harap isi semua elemen yang diperlukan sebelum membuka lembar kerja baru.",
                    type: "error",
                })

                return;
            }

            var pemeriksaanId = $(this).data('pemeriksaan-id');
            var pemeriksaanNama = $(this).data('pemeriksaan-nama');

            $("#pemeriksaan_id").val(pemeriksaanId);
            $("#pemeriksaan_nama").val(pemeriksaanNama);

            getDetailPemeriksaan(pemeriksaanId);
            loadFotoRadiologi(pemeriksaanId);
            checkSettingExpertisePemeriksaan(pemeriksaanId);
            checkPemeriksaanId();
        });

        $(document).on('change', '#pemeriksaan_flag', function() {
            setTimeout(function() {
                updatePemeriksaanExpertise();
            }, 500);
        });

        $('.summernote').on('summernote.change', function() {
            setTimeout(function() {
                updatePemeriksaanExpertise();
            }, 500);
        });
    });

    function getDetailPemeriksaan(pemeriksaanId) {
        $.ajax({
            url: '{base_url}term_radiologi_usg_expertise/get_detail_pemeriksaan',
            method: 'POST',
            dataType: 'json',
            data: { pemeriksaan_id: pemeriksaanId },
            success: function(response) {
                $("#pemeriksaan_flag").val(response.flag_kritis).trigger('change');
                $("#pemeriksaan_klinis").summernote('code', response.klinis);
                $("#pemeriksaan_kesan").summernote('code', response.kesan);
                $("#pemeriksaan_usul").summernote('code', response.usul);
                $("#pemeriksaan_hasil").summernote('code', response.hasil);
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    }

    function checkSettingExpertisePemeriksaan(pemeriksaanId) {
        const tipeLayanan = '{tipe_layanan}';
        const asalPasien = '{asal_pasien}';
        const poliklinikKelas = '{poliklinik_kelas}';
        const dokterRadiologi = '{dokter_radiologi}';

        $.ajax({
            url: '{base_url}term_radiologi/check_setting_expertise_pemeriksaan',
            method: 'POST',
            dataType: 'json',
            data: {
                tipe_layanan: tipeLayanan,
                pemeriksaan_id: pemeriksaanId,
                asal_pasien: asalPasien,
                poliklinik_kelas: poliklinikKelas,
                dokter_radiologi: dokterRadiologi
            },
            success: function(response) {
                if (response == null) {
                    $("#pemeriksaan_klinis").closest('.form-group').hide();
                    $("#pemeriksaan_klinis").prop('required', 0);

                    $("#pemeriksaan_kesan").closest('.form-group').hide();
                    $("#pemeriksaan_kesan").prop('required', 0);

                    $("#pemeriksaan_usul").closest('.form-group').hide();
                    $("#pemeriksaan_usul").prop('required', 0);

                    $("#pemeriksaan_hasil").closest('.form-group').hide();
                    $("#pemeriksaan_hasil").prop('required', 0);
                } else {
                    $("#pemeriksaan_klinis").closest('.form-group').toggle(response.klinis != 0);
                    $("#pemeriksaan_klinis").prop('required', response.klinis == 1);

                    $("#pemeriksaan_kesan").closest('.form-group').toggle(response.kesan != 0);
                    $("#pemeriksaan_kesan").prop('required', response.kesan == 1);

                    $("#pemeriksaan_usul").closest('.form-group').toggle(response.usul != 0);
                    $("#pemeriksaan_usul").prop('required', response.usul == 1);

                    $("#pemeriksaan_hasil").closest('.form-group').toggle(response.hasil != 0);
                    $("#pemeriksaan_hasil").prop('required', response.hasil == 1);
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    }

    function loadFotoRadiologi(pemeriksaanId) {
        var element = "#list-foto-radiologi";

        $(element).empty();

        $.ajax({
            url: '{site_url}term_radiologi_usg_hasil/get_foto_radiologi_pemeriksaan/' + pemeriksaanId,
            dataType: "json",
            success: function(data) {
                $(element).append(`<div class="col-md-12">
                    <b><span class="label label-success" style="font-size:12px">FOTO RADIOLOGI</span></b>
                <div>
                <br>`);
                $(element).append(data.detail);
                photoSwipe();
            }
        });
    }

    function removeFotoRadiologi(fileId, pemeriksaanId) {
        swal({
            title: "Anda Yakin ?",
            text: "Untuk Hapus data?",
            type: "success",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
        }).then(function() {
            $.ajax({
                url: '{site_url}term_radiologi_usg_expertise/remove_foto_radiologi_pemeriksaan/' + fileId + '/' + pemeriksaanId,
                complete: function() {
                    $.toaster({
                        priority: 'success',
                        title: 'Succes!',
                        message: ' Hapus Data'
                    });

                    loadFotoRadiologi(pemeriksaanId);;
                }
            });
        });
    }

    function loadDataPemeriksaan(transaksiId) {
        $.ajax({
            url: '{site_url}term_radiologi_usg_expertise/get_data_pemeriksaan/' + transaksiId,
            type: 'GET',
            dataType: 'html',
            success: function(data) {
                $('#table-pemeriksaan tbody').html(data);
            },
            error: function(xhr, status, error) {
            }
        });
    }

    function checkPemeriksaanId() {
        var pemeriksaanId = $("#pemeriksaan_id").val();
        if (pemeriksaanId == '') {
            $("#pemeriksaan-message").show();
            $("#pemeriksaan-message").html(`<div style="text-align: center; border: 1px dashed #999; padding: 10px; width: 100%; background-color: #f9f9f9;">
            Untuk melakukan input hasil, silahkan pilih pemeriksaan terlebih dahulu.
            </div>`);
            $("#pemeriksaan-content").hide();
        } else {
            $("#pemeriksaan-message").hide();
            $("#pemeriksaan-content").show();
        }
    }

    function updatePemeriksaanExpertise() {
        var pemeriksaanId = $("#pemeriksaan_id").val();
        var flagKritis = $("#pemeriksaan_flag").val();
        var klinis = $("#pemeriksaan_klinis").val();
        var kesan = $("#pemeriksaan_kesan").val();
        var usul = $("#pemeriksaan_usul").val();
        var hasil = $("#pemeriksaan_hasil").val();

        $.ajax({
            url: '{base_url}term_radiologi_usg_expertise/update_pemeriksaan_expertise',
            method: 'POST',
            data: {
                pemeriksaan_id: pemeriksaanId,
                flag_kritis: flagKritis,
                klinis: klinis,
                kesan: kesan,
                usul: usul,
                hasil: hasil
            },
            success: function(response) {
                $.toaster({ priority: 'success', title: 'Success!', message: 'Perubahan Data berhasil disimpan' });
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    }

    function photoSwipe() {
        var container = [];

        // Loop over gallery items and push them to the container array
        $('#gallery').find('figure').each(function() {
            var $link = $(this).find('a'),
            item = {
                src: $link.attr('href'),
                w: $link.data('width'),
                h: $link.data('height'),
                title: $link.data('caption')
            };
            container.push(item);
            
            // Define click event for each anchor element within a figure
            $link.click(function(event) {
                event.preventDefault();
                var index = $(this).closest('figure').index();
                openPhotoSwipe(index);
            });
        });

        // Function to open PhotoSwipe gallery
        function openPhotoSwipe(index) {
            var pswpElement = document.querySelectorAll('.pswp')[0];
            var options = {
                index: index,
                bgOpacity: 0.85,
                showHideOpacity: true
            };
            var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, container, options);
            gallery.init();
        }
    }

    function isRequiredCheck() {
        var isRequired = true;
        $('.summernote').each(function() {
            if ($(this).prop('required') && $(this).summernote('isEmpty')) {
                $(this).siblings('.note-editor').addClass('required');
                isRequired = false;
            } else {
                $(this).siblings('.note-editor').removeClass('required');
            }
        });
        return isRequired;
    }
</script>
