<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}minvasif" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('minvasif/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="minvasif_id" placeholder="minvasif_id" name="minvasif_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			
			
			<?if ($id){?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Template</label>
				<div class="col-md-10">
					<div class="input-group">
						<select id="template_id" name="template_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($template_id==''?'selected':'')?>>-Belum dipilih-</option>
							<?foreach(get_all('mtemplate_invasif',array('staktif'=>1)) as $r){?>
							<option value="<?=$r->id?>" <?=($template_id==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
							
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" type="button" id="btn_create_with_template" onclick="create_with_template()"><i class="fa fa-level-down"></i> Terapkan</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group push-5-t">
				<label class="col-md-2 control-label" for="nama"><?=text_primary('INFORMASI TINDAKAN ')?></label>
				<div class="col-md-10 ">
					<div class="table-responsive">
				
						<table class="table table-bordered" id="tabel_informasi">
							<thead>
								<tr>
									<th width="5%" class="text-center">No</th>
									<th width="25%" class="text-center">Jenis Informasi / Type Of Information	</th>
									<th width="50%" class="text-center">Isi Informasi / Content Information</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}minvasif" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let minvasif_id=$("#minvasif_id").val();
	if (minvasif_id){
		load_index_informasi();
	}
	
})	
function load_index_informasi(){
	$("#cover-spin").show();
	let minvasif_id=$("#minvasif_id").val();
	
	$.ajax({
		url: '{site_url}minvasif/load_index_informasi_invasif/',
		dataType: "json",
		type: 'POST',
		  data: {
				id:minvasif_id,
				
		  },
		success: function(data) {
			$("#tabel_informasi tbody").empty();
			$("#tabel_informasi tbody").append(data.tabel);
			$("#cover-spin").hide();
			$('.auto_blur_tabel').summernote({
				height: 100,
			    codemirror: { // codemirror options
					theme: 'monokai'
				  },	
			  callbacks: {
				onBlur: function(contents, $editable) {
						var tr=$(this).closest('tr');
						var informasi_id=tr.find(".informasi_id").val();
						var isi=$(this).val();
						$.ajax({
							url: '{site_url}minvasif/update_isi_informasi_invasif/',
							dataType: "json",
							type: 'POST',
							data: {
							informasi_id:informasi_id,isi:isi
							},success: function(data) {

							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
							}
						});
				}
			  }
			});
				
		}
	});
}
function create_with_template(){
	$("#cover-spin").show();
	let minvasif_id=$("#minvasif_id").val();
	let template_id=$("#template_id").val();
	$.ajax({
		url: '{site_url}minvasif/create_with_template', 
		dataType: "JSON",
		method: "POST",
		data : {
				minvasif_id:minvasif_id,
				template_id:template_id,
			},
		complete: function(data) {
			load_index_informasi();
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});
}
</script>