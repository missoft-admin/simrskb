<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2377'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2379'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2381'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"  onclick="load_tab3()"><i class="fa fa-check-square-o"></i> Parameter</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2382'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4"  onclick="load_tab4()"><i class="fa fa-check-square-o"></i> Hasil</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2377'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			
			<?php echo form_open('setting_ews/save_ews','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_ina">Judul Header <span style="color:red;"> INA</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_ina" name="judul_header_ina" value="{judul_header_ina}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_ina">Judul Footer <span style="color:red;"> INA </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_ina" name="judul_footer_ina" value="{judul_footer_ina}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_eng">Judul Footer <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_eng" name="judul_footer_eng" value="{judul_footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('2378'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2379'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2380'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2381'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('SETTING PARAMETER')?></h5>
					</div>
				</div>
				</div>
				<?
					$param_1=get_parameter_ews(1);
					$param_2=get_parameter_ews(2);
					$param_3=get_parameter_ews(3);
					$param_4=get_parameter_ews(4);
					$param_5=get_parameter_ews(5);
					$param_6=get_parameter_ews(6);
					$param_7=get_parameter_ews(7);
					$param_8=get_parameter_ews(8);
					$param_9=get_parameter_ews(9);
					$param_10=get_parameter_ews(10);
				?>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_1">TINGKAT KESADARAN</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_1" name="nama_1" value="<?=$param_1->nama_ews; ?>" placeholder="<?=$param_1->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_1">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_1" name="nourut_1" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_1->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_1">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_1" name="staktif_1" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_1->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_1->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_1">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Tingkat Kesadaran</th>
												<th width="25%">Skor</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_1" name="skor_id_1" value="">
													<input class="form-control" type="hidden" id="nilai_akhir_1" name="nilai_akhir_1" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<select id="nilai_awal_1" name="nilai_awal_1" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
															<option value="" selected>Pilih Opsi</option>
															<?foreach(list_variable_ref(23) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
														</select>
													</div>
												</th>
												<th>
													<select id="skor_1" name="skor_1" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_1" name="warna_1" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_1" onclick="simpan_skor(1)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_1" onclick="clear_skor(1)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_2">NADI</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_2" name="nama_2" value="<?=$param_2->nama_ews; ?>" placeholder="<?=$param_2->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_2">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_2" name="nourut_2" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_2->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_2">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_2" name="staktif_2" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_2->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_2->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_2">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_2" name="skor_id_2" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_2" name="nilai_awal_2" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_2" name="nilai_akhir_2" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_2" name="skor_2" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_2" name="warna_2" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_2" onclick="simpan_skor(2)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_2" onclick="clear_skor(2)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_3">NAFAS</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_3" name="nama_3" value="<?=$param_3->nama_ews; ?>" placeholder="<?=$param_3->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_3">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_3" name="nourut_3" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_3->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_3">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_3" name="staktif_3" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_3->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_3->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_3">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_3" name="skor_id_3" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_3" name="nilai_awal_3" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_3" name="nilai_akhir_3" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_3" name="skor_3" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_3" name="warna_3" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_3" onclick="simpan_skor(3)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_3" onclick="clear_skor(3)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_4">SPO2</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_4" name="nama_4" value="<?=$param_4->nama_ews; ?>" placeholder="<?=$param_4->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_4">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_4" name="nourut_4" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_4->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_4">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_4" name="staktif_4" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_4->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_4->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_4">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_4" name="skor_id_4" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_4" name="nilai_awal_4" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_4" name="nilai_akhir_4" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_4" name="skor_4" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_4" name="warna_4" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_4" onclick="simpan_skor(4)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_4" onclick="clear_skor(4)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_5">TEKANAN DARAH SISTOLE</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_5" name="nama_5" value="<?=$param_5->nama_ews; ?>" placeholder="<?=$param_5->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_5">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_5" name="nourut_5" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_5->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_5">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_5" name="staktif_5" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_5->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_5->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_5">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_5" name="skor_id_5" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_5" name="nilai_awal_5" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_5" name="nilai_akhir_5" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_5" name="skor_5" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_5" name="warna_5" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_5" onclick="simpan_skor(5)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_5" onclick="clear_skor(5)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_6">TEKANAN DARAH DIASTOLE</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_6" name="nama_6" value="<?=$param_6->nama_ews; ?>" placeholder="<?=$param_6->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_6">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_6" name="nourut_6" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_6->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_6">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_6" name="staktif_6" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_6->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_6->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_6">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_6" name="skor_id_6" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_6" name="nilai_awal_6" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_6" name="nilai_akhir_6" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_6" name="skor_6" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_6" name="warna_6" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_6" onclick="simpan_skor(6)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_6" onclick="clear_skor(6)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_7">SUHU TUBUH</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_7" name="nama_7" value="<?=$param_7->nama_ews; ?>" placeholder="<?=$param_7->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_7">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_7" name="nourut_7" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_7->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_7">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_7" name="staktif_7" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_7->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_7->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_7">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_7" name="skor_id_7" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_7" name="nilai_awal_7" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_7" name="nilai_akhir_7" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_7" name="skor_7" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_7" name="warna_7" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_7" onclick="simpan_skor(7)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_7" onclick="clear_skor(7)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_8">TINGGI BADAN</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_8" name="nama_8" value="<?=$param_8->nama_ews; ?>" placeholder="<?=$param_8->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_8">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_8" name="nourut_8" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_8->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_8">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_8" name="staktif_8" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_8->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_8->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_8">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_8" name="skor_id_8" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_8" name="nilai_awal_8" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_8" name="nilai_akhir_8" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_8" name="skor_8" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_8" name="warna_8" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_8" onclick="simpan_skor(8)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_8" onclick="clear_skor(8)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_9">BERAT BADAN</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_9" name="nama_9" value="<?=$param_9->nama_ews; ?>" placeholder="<?=$param_9->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_9">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_9" name="nourut_9" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_9->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_9">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_9" name="staktif_9" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_9->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_9->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_9">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Nilai</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_9" name="skor_id_9" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<div class="input-group" style="width:100%">
															<input class="form-control decimal" type="text" id="nilai_awal_9" name="nilai_awal_9" placeholder="Skor 1" value="" >
															<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
															<input class="form-control decimal" type="text" id="nilai_akhir_9" name="nilai_akhir_9" placeholder="Skor 2" value="" >
														</div>
														
													</div>
												</th>
												<th>
													<select id="skor_9" name="skor_9" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_9" name="warna_9" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_9" onclick="simpan_skor(9)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_9" onclick="clear_skor(9)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="form-group">
								<div class="col-xs-6">
									<label class="col-xs-12 text-primary" for="nama_10">SUPLEMEN OKSIGEN</label>
									<div class="col-xs-12">
										<input class="form-control auto_blur" type="text" id="nama_10" name="nama_10" value="<?=$param_10->nama_ews; ?>" placeholder="<?=$param_10->nama_ews; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="nama_10">URUTAN TAMPIL</label>
									<div class="col-xs-12">
										<select id="nourut_10" name="nourut_10" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<?for($i=1;$i<30;$i++){?>
											<option value="<?=$i?>" <?=($param_10->nourut==$i?'selected':'')?>><?=$i?></option>
											<?}?>
											
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<label class="col-xs-12" for="staktif_10">STATUS</label>
									<div class="col-xs-12">
										<select id="staktif_10" name="staktif_10" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=($param_10->staktif=='1'?'selected':'')?>>AKTIF</option>
											<option value="0" <?=($param_10->staktif=='0'?'selected':'')?>>TIDAK AKTIF</option>
											
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_10">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Suplemen Oksigen</th>
												<th width="25%">Skor / Nilai</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#
													<input class="form-control" type="hidden" id="skor_id_10" name="skor_id_10" value="">
													<input class="form-control" type="hidden" id="nilai_akhir_10" name="nilai_akhir_10" value="">
												</th>
												<th>
													<div class="input-group" style="width:100%">
														<select id="nilai_awal_10" name="nilai_awal_10" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
															<option value="" selected>Pilih Opsi</option>
															<?foreach(list_variable_ref(421) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
														</select>
													</div>
												</th>
												<th>
													<select id="skor_10" name="skor_10" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
														<?for($i=0;$i<30;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
														<?}?>
													</select>
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna_10" name="warna_10" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
														<button class="btn btn-success btn-sm" type="button" id="btn_tambah_10" onclick="simpan_skor(10)"><i class="fa fa-save"></i> Simpan</button>
														<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_10" onclick="clear_skor(10)"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
					</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		
	</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2382'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
				<div class="row">
					
					<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('HASIL AKHIR SKOR')?></h5>
						</div>
					</div>
					</div>
					
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_hasil_skor">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="40%">Range Nilai</th>
													<th width="25%">Hasil Skor</th>
													<th width="15%">Warna</th>
													<th width="15%">Action</th>										   
												</tr>
												<tr>
													<th>#
														<input class="form-control" type="hidden" id="hasil_skor_id" name="hasil_skor_id" value="">
													</th>
													<th>
														<div class="input-group" style="width:100%">
															<div class="input-group" style="width:100%">
																<input class="form-control decimal" type="text" id="nilai_awal_skor" name="nilai_awal_skor" placeholder="Skor 1" value="" >
																<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
																<input class="form-control decimal" type="text" id="nilai_akhir_skor" name="nilai_akhir_skor" placeholder="Skor 2" value="" >
															</div>
															
														</div>
													</th>
													<th>
														<select id="hasil_skor" name="hasil_skor" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
															<option value="" selected>Pilih Opsi</option>
															<?foreach(list_variable_ref(422) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
														</select>
													</th>
													<th>
														
														<div class="js-colorpicker input-group colorpicker-element">
															<input class="form-control" type="text" id="warna_skor" name="warna_skor" value="#0000">
															<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
														</div>
													
													</th>
													<th>
														<div class="btn-group">
															<button class="btn btn-success btn-sm" type="button" id="btn_tambah_skor" onclick="simpan_hasil_skor()"><i class="fa fa-save"></i> Simpan</button>
															<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_skor" onclick="clear_ahsil_skor()"><i class="fa fa-refresh"></i> Clear</button>
														</div>
													</th>										   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>					
								</div>
							</div>
							
						</div>
				</div>
				
			</div>
			<div class="row">
					
					<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('HASIL AKHIR PARAMETER')?></h5>
						</div>
					</div>
					</div>
					
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_hasil_parameter">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="20%">Jumlah Parameter</th>
													<th width="20%">Skor Parameter</th>
													<th width="20%">Hasil Parameter</th>
													<th width="15%">Warna</th>
													<th width="20%">Action</th>										   
												</tr>
												<tr>
													<th>#
														<input class="form-control" type="hidden" id="hasil_parameter_id" name="hasil_parameter_id" value="">
													</th>
													<th>
														<select id="jumlah_parameter" name="jumlah_parameter" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
															<?for($i=0;$i<30;$i++){?>
															<option value="<?=$i?>" ><?=$i?></option>
															<?}?>
														</select>
													</th>
													<th>
														<select id="skor_parameter" name="skor_parameter" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
															<?for($i=0;$i<30;$i++){?>
															<option value="<?=$i?>" ><?=$i?></option>
															<?}?>
														</select>
													</th>
													<th>
														<select id="hasil_parameter" name="hasil_parameter" class="js-select2 form-control " style="width: 100%;" data-placeholder="Choose one..">
															<option value="" selected>Pilih Opsi</option>
															<?foreach(list_variable_ref(422) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama?></option>
															<?}?>
														</select>
													</th>
													<th>
														
														<div class="js-colorpicker input-group colorpicker-element">
															<input class="form-control" type="text" id="warna_parameter" name="warna_parameter" value="#0000">
															<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
														</div>
													
													</th>
													<th>
														<div class="btn-group">
															<button class="btn btn-success btn-sm" type="button" id="btn_tambah_parameter" onclick="simpan_hasil_parameter()"><i class="fa fa-save"></i> Simpan</button>
															<button class="btn btn-warning btn-sm" type="button" id="btn_refresh_parameter" onclick="clear_ahsil_parameter()"><i class="fa fa-refresh"></i> Clear</button>
														</div>
													</th>										   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>					
								</div>
							</div>
							
						</div>
				</div>
				
			</div>
		<?php echo form_close() ?>
		
	</div>
	<?}?>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function loadFile_img(event){
	// alert('load');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}
$(document).ready(function(){	
	$('.js-summernote').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
			load_tab2();
	}
	if (tab=='3'){
			load_tab3();
	}
	if (tab=='4'){
			load_tab4();
	}
	$('.decimal').number(true, 2);
	
})	
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
}
function load_tab3(){
	load_skor(1);	
	load_skor(2);	
	load_skor(3);	
	load_skor(4);	
	load_skor(5);	
	load_skor(6);	
	load_skor(7);	
	load_skor(8);	
	load_skor(9);	
	load_skor(10);	
}
function load_tab4(){
	load_hasil_skor();	
	load_hasil_parameter();	
	
}
// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_ews/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ews/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ews/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_ews/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$(".opsi_change").change(function(){
	simpan_parameter();
});

$(".auto_blur").blur(function(){
	simpan_parameter();
});
function simpan_parameter(){
	let nama_1=$("#nama_1").val();
	let nourut_1=$("#nourut_1").val();
	let staktif_1=$("#staktif_1").val();

	let nama_2=$("#nama_2").val();
	let nourut_2=$("#nourut_2").val();
	let staktif_2=$("#staktif_2").val();

	let nama_3=$("#nama_3").val();
	let nourut_3=$("#nourut_3").val();
	let staktif_3=$("#staktif_3").val();

	let nama_4=$("#nama_4").val();
	let nourut_4=$("#nourut_4").val();
	let staktif_4=$("#staktif_4").val();

	let nama_5=$("#nama_5").val();
	let nourut_5=$("#nourut_5").val();
	let staktif_5=$("#staktif_5").val();

	let nama_6=$("#nama_6").val();
	let nourut_6=$("#nourut_6").val();
	let staktif_6=$("#staktif_6").val();

	let nama_7=$("#nama_7").val();
	let nourut_7=$("#nourut_7").val();
	let staktif_7=$("#staktif_7").val();

	let nama_8=$("#nama_8").val();
	let nourut_8=$("#nourut_8").val();
	let staktif_8=$("#staktif_8").val();

	let nama_9=$("#nama_9").val();
	let nourut_9=$("#nourut_9").val();
	let staktif_9=$("#staktif_9").val();

	let nama_10=$("#nama_10").val();
	let nourut_10=$("#nourut_10").val();
	let staktif_10=$("#staktif_10").val();
	
	console.log(nama_4);
	
	$.ajax({
		url: '{site_url}setting_ews/simpan_parameter', 
		dataType: "JSON",
		method: "POST",
		data : {
			
			nama_1:nama_1,
			nourut_1:nourut_1,
			staktif_1:staktif_1,
			nama_2:nama_2,
			nourut_2:nourut_2,
			staktif_2:staktif_2,
			nama_3:nama_3,
			nourut_3:nourut_3,
			staktif_3:staktif_3,	
			nama_4:nama_4,
			nourut_4:nourut_4,
			staktif_4:staktif_4,	
			nama_5:nama_5,
			nourut_5:nourut_5,
			staktif_5:staktif_5,
			nama_6:nama_6,
			nourut_6:nourut_6,
			staktif_6:staktif_6,
			nama_7:nama_7,
			nourut_7:nourut_7,
			staktif_7:staktif_7,
			nama_8:nama_8,
			nourut_8:nourut_8,
			staktif_8:staktif_8,
			nama_9:nama_9,
			nourut_9:nourut_9,
			staktif_9:staktif_9,
			nama_10:nama_10,
			nourut_10:nourut_10,
			staktif_10:staktif_10,
			},
		success: function(data) {
			
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
				
		}
	});
}
function load_skor(ews_id){
	$('#index_'+ews_id).DataTable().destroy();	
	table = $('#index_'+ews_id).DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_ews/load_skor', 
                type: "POST" ,
                dataType: 'json',
				data : {
						ews_id:ews_id
					   }
            }
        });
}

$("#nilai_awal_1").change(function(){
	$("#nilai_akhir_1").val($("#nilai_awal_1").val());
});
$("#nilai_awal_10").change(function(){
	$("#nilai_akhir_10").val($("#nilai_awal_10").val());
});
function clear_skor(ews_id){
	$("#skor_id_"+ews_id).val('');
	if (ews_id=='1'){
		$("#nilai_awal_"+ews_id).val('').trigger('change');
	}else if (ews_id=='10'){
		$("#nilai_awal_"+ews_id).val('').trigger('change');
	}else{
	$("#nilai_awal_"+ews_id).val('');
		
	}
	$("#nilai_akhir_"+ews_id).val('');
	$("#skor_"+ews_id).val('');
	$("#warna_"+ews_id).val('#000');
	$("#btn_tambah_"+ews_id).html('<i class="fa fa-save"></i> Save');
	
}

function simpan_skor(ews_id){
	let skor_id=$("#skor_id_"+ews_id).val();
	let nilai_1=$("#nilai_awal_"+ews_id).val();
	let nilai_2=$("#nilai_akhir_"+ews_id).val();
	let skor=$("#skor_"+ews_id).val();
	let warna=$("#warna_"+ews_id).val();
	
	if (ews_id=='1'){
		if (nilai_1==''){
			sweetAlert("Maaf...", "Tentukan Tingkat Kesadaran", "error");
			return false;
		}
	}else if (ews_id=='1'){
		if (nilai_1==''){
			sweetAlert("Maaf...", "Tentukan Suplemen Oksigen", "error");
			return false;
		}
	
	}else{
		if (nilai_1==''){
			sweetAlert("Maaf...", "Tentukan Range 1", "error");
			return false;
		}
	
	
		if (nilai_2==''){
			sweetAlert("Maaf...", "Tentukan Range 2", "error");
			return false;
		}
		if (warna==''){
			sweetAlert("Maaf...", "Tentukan Warna", "error");
			return false;
		}
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ews/simpan_skor', 
		dataType: "JSON",
		method: "POST",
		data : {
				skor_id:skor_id,
				ews_id:ews_id,
				nilai_1:nilai_1,
				nilai_2:nilai_2,
				skor:skor,
				warna:warna,
				
			},
		complete: function(data) {
			clear_skor(ews_id);
			$('#index_'+ews_id).DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});
}
function hapus_nilai(id,ews_id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ews/hapus_nilai',
				type: 'POST',
				data: {id: id,ews_id:ews_id},
				complete: function() {
					$('#index_'+ews_id).DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function edit_nilai(skor_id,ews_id){
	$("#skor_id_"+ews_id).val(skor_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_ews/find_skor',
		type: 'POST',
		dataType: "JSON",
		data: {id: skor_id},
		success: function(data) {
			$("#btn_tambah_"+ews_id).html('<i class="fa fa-save"></i> Edit');
			if (ews_id=='1' || ews_id=='10'){
			$("#nilai_awal_"+ews_id).val(data.nilai_1).trigger('change');
				
			}else{
			$("#nilai_awal_"+ews_id).val(data.nilai_1);
				
			}
			$("#nilai_akhir_"+ews_id).val(data.nilai_2);
			$("#skor_"+ews_id).val(data.skor).trigger('change');
			$("#warna_"+ews_id).val(data.warna).trigger('change');
			$("#cover-spin").hide();
			$("#btn_tambah_"+ews_id).html('<i class="fa fa-save"></i> Update');
		}
	});
}
function edit_hasil_skor(skor_id){
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_ews/find_hasil_skor',
		type: 'POST',
		dataType: "JSON",
		data: {id: skor_id},
		success: function(data) {
			$("#btn_tambah_skor").html('<i class="fa fa-save"></i> Edit');
			$("#hasil_skor_id").val(data.id);
			$("#hasil_skor").val(data.hasil_skor).trigger('change');
			
			$("#nilai_awal_skor").val(data.nilai_1);
			$("#nilai_akhir_skor").val(data.nilai_2);
			$("#warna_skor").val(data.warna).trigger('change');
			$("#cover-spin").hide();
			$("#btn_tambah_skor").html('<i class="fa fa-save"></i> Update');
		}
	});
}
function simpan_hasil_skor(){
	let id=$("#hasil_skor_id").val();
	let hasil_skor=$("#hasil_skor").val();
	let nilai_1=$("#nilai_awal_skor").val();
	let nilai_2=$("#nilai_akhir_skor").val();
	let warna=$("#warna_skor").val();
	
	
	if (nilai_1==''){
		sweetAlert("Maaf...", "Tentukan Range 1", "error");
		return false;
	}


	if (nilai_2==''){
		sweetAlert("Maaf...", "Tentukan Range 2", "error");
		return false;
	}
	if (hasil_skor==''){
		sweetAlert("Maaf...", "Tentukan Hasil Skor", "error");
		return false;
	}
	if (warna==''){
		sweetAlert("Maaf...", "Tentukan Warna", "error");
		return false;
	}

	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ews/simpan_hasil_skor', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				hasil_skor:hasil_skor,
				nilai_1:nilai_1,
				nilai_2:nilai_2,
				warna:warna,
				
			},
		complete: function(data) {
			clear_hasil_skor();
			$('#index_hasil_skor').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

}
	function clear_hasil_skor(){
		$("#hasil_skor").val('').trigger('change');
		$("#hasil_skor_id").val('');
		$("#nilai_awal_skor").val('');
		$("#nilai_akhir_skor").val('');
		$("#hasil_skor_").val('');
		$("#warna_skor").val('#000');
		$("#btn_tambah_skor").html('<i class="fa fa-save"></i> Save');
		
	}
	function load_hasil_skor(){
	$('#index_hasil_skor').DataTable().destroy();	
	table = $('#index_hasil_skor').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_ews/load_hasil_skor', 
                type: "POST" ,
                dataType: 'json',
				data : {
						// ews_id:ews_id
					   }
            }
        });
}
function hapus_hasil_skor(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ews/hapus_hasil_skor',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hasil_skor').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function edit_hasil_parameter(parameter_id){
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}setting_ews/find_hasil_parameter',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			$("#hasil_parameter_id").val(data.id);
			$("#hasil_parameter").val(data.hasil_parameter).trigger('change');
			
			$("#jumlah_parameter").val(data.jumlah_param).trigger('change');
			$("#skor_parameter").val(data.skor_param).trigger('change');
			$("#warna_parameter").val(data.warna).trigger('change');
			$("#cover-spin").hide();
			$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Update');
		}
	});
}
function simpan_hasil_parameter(){
	let id=$("#hasil_parameter_id").val();
	let hasil_parameter=$("#hasil_parameter").val();
	let jumlah_param=$("#jumlah_parameter").val();
	let skor_param=$("#skor_parameter").val();
	let warna=$("#warna_parameter").val();
	
	
	if (jumlah_param==''){
		sweetAlert("Maaf...", "Tentukan Jumlah Parameter", "error");
		return false;
	}


	if (skor_param==''){
		sweetAlert("Maaf...", "Tentukan Skor Parameter", "error");
		return false;
	}
	if (hasil_parameter==''){
		sweetAlert("Maaf...", "Tentukan Hasil Skor", "error");
		return false;
	}
	if (warna==''){
		sweetAlert("Maaf...", "Tentukan Warna", "error");
		return false;
	}

	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_ews/simpan_hasil_parameter', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				hasil_parameter:hasil_parameter,
				jumlah_param:jumlah_param,
				skor_param:skor_param,
				warna:warna,
				
			},
		complete: function(data) {
			clear_hasil_parameter();
			$('#index_hasil_parameter').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

}
	function clear_hasil_parameter(){
		$("#hasil_parameter").val('').trigger('change');
		$("#hasil_parameter_id").val('');
		$("#skor_parameter").val('0').trigger('change');
		$("#jumlah_parameter").val('0').trigger('change');
		$("#hasil_parameter_").val('');
		$("#warna_parameter").val('#000');
		$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Save');
		
	}
	function load_hasil_parameter(){
	$('#index_hasil_parameter').DataTable().destroy();	
	table = $('#index_hasil_parameter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_ews/load_hasil_parameter', 
                type: "POST" ,
                dataType: 'json',
				data : {
						// ews_id:ews_id
					   }
            }
        });
}
function hapus_hasil_parameter(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_ews/hapus_hasil_parameter',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hasil_parameter').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>