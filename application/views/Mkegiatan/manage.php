<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mkegiatan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mkegiatan/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Kegiatan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama Kegiatan" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Klasifikasi</label>
				<div class="col-md-7">
					<select id="idklasifikasi" name="idklasifikasi" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="">- Pilih klasifikasi -</option>
						<?foreach(get_all('mklasifikasi',array('status'=>1),'nama') as $row){?>
							<option value="<?=$row->id?>" <?=$idklasifikasi==$row->id?'selected':''?>><?=$row->nama?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Status</label>
				<div class="col-md-7">
					<select id="status_kegiatan" name="status_kegiatan" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="1" <?=($status_kegiatan=='1'?'selected':'')?>>Pengulangan</option>
						<option value="2" <?=($status_kegiatan=='2'?'selected':'')?>>NON Pengulangan</option>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="lokasi">Urutan Tampil</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="urutan" placeholder="Urutan Tampil" required="" name="urutan" value="{urutan}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Deskripsi</label>
				<div class="col-md-7">
					<textarea class="form-control js-summernote" name="deskripsi" id="deskripsi"><?=$deskripsi?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mkegiatan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		$('#deskripsi').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

	})	
	function validate_final(){
		if ($('#idklasifikasi').val()=='#'){
			sweetAlert("Maaf...", "Tentukan Prespektif", "error");
			return false;

		}
	}
</script>