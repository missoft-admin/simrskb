<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Kelompok Pasien -</option>
							<?foreach($list_kelompok_pasien as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Perusahaan</label>
                    <div class="col-md-8">
                        <select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Perusahaan -</option>
							<?foreach($list_rekanan as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
                                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Jenis</label>
                    <div class="col-md-8">
						<select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>Poliklinik</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>IGD</option>
							<option value="3" <?=("3" == $tipe ? 'selected' : ''); ?>>Rawat Inap</option>
							<option value="4" <?=("4" == $tipe ? 'selected' : ''); ?>>ODS</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="0" <?=("0" == $status ? 'selected' : ''); ?>>Belum Jatuh Tempo</option>
							<option value="1" <?=("1" == $status ? 'selected' : ''); ?>>Sudah Jatuh Tempo</option>
							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx1" name="tgl_trx1" placeholder="From" value="{tgl_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value="{tgl_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggaldaftar1" name="tanggaldaftar1" placeholder="From" value="{tanggaldaftar1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggaldaftar2" name="tanggaldaftar2" placeholder="To" value="{tanggaldaftar2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
					<th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                     <th width="10%">NO</th>
                    <th width="10%">TGL KUNJUGAN / NO. REG</th>
                    <th width="10%">JENIS</th>
                    <th width="10%">MEDREC</th>
                    <th width="10%">PASIEN</th>
                    <th width="15%">KEL PASIEN</th>
                    <th width="15%">TOT TAGIHAN</th>
                    <th width="15%">TIDAK TERTAGIH</th>
                    <th width="15%">BAYAR LAINNYA</th>
                    <th width="15%">JATUH TEMPO</th>
                    <th width="15%">JENIS</th>
                    <th width="10%">UMUR</th>
                    <th width="15%">STATUS JATUH TEMPO</th>
                    <th width="15%">REMINDER</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Jatuh Tempo </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="status">Nominal</label>
								<div class="col-md-8">
										<input class="form-control input decimal" disabled type="text" id="xnominal2" name="xnominal2" value="">
									
									
								</div>
							</div>				
						</div>	
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal" value="">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
							
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Tanggal Edit</label>
								<div class="col-md-8">
									<div class="input-group">
										<input class="js-datepicker form-control input" type="text" id="xtgl_pilih" name="xtgl_pilih" value="" data-date-format="dd-mm-yyyy">

										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									
								</div>
							</div>				
						</div>	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan Ganti</label>
								<div class="col-md-8">
										<input class="form-control input" type="text" id="xalasan" name="xalasan" value="">
									
									
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_fu" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:60%">
        <div class="modal-content" >
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">PENGHAPUSAN PIUTANG </h3>
                </div>
                <div class="block-content">
					<div class="row">
						
						<div class="col-md-12" style="margin-top: 15px;">
							<label class="col-md-2 control-label" for="status">Tanggal Informasi</label>
							<div class="col-md-4">
								<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="fu_tanggal_info" name="fu_tanggal_info" placeholder="Tanggal FU" value="<?=date('d-m-Y')?>"/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<label class="col-md-2 control-label" for="status">User Penghapusan</label>
							<div class="col-md-4">
									<input class="form-control" type="text" readonly id="user_hapus" name="user_hapus" placeholder="User Hapus" value="<?=$this->session->userdata('user_name')?>"/>
								
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">	
							<label class="col-md-2 control-label" for="status">Nominal Dihapuskan</label>
								<div class="col-md-10">
										<input class="form-control decimal" readonly type="text" id="nominal_dihapus" name="nominal_dihapus" placeholder="" value=""/>
								</div>				
						</div>
						<div class="col-md-12" style="margin-top: 5px;">	
							<label class="col-md-2 control-label" for="status">Alasan</label>
								<div class="col-md-10">
										<textarea class="form-control js-summernote" id="keterangan"></textarea>
								</div>				
						</div>
						<div class="col-md-12" style="margin-top: 5px;">	
							<label class="col-md-2 control-label" for="status"></label>
								<div class="col-md-10">
									
							</div>				
						</div>
						
						
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="block block-themed " id="div_kunjungan">
								<div class="block-header bg-primary">
									<ul class="block-options">
										<li>
											<button id="button_up_kunjungan" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
										</li>
									</ul>
									<h3 class="block-title"><i class="fa fa-file-photo-o"></i>  Document</h3>
								</div>
								<div class="block-content block-content">
									<form action="{base_url}tpiutang_tt_reminder/upload_bukti_hapus" enctype="multipart/form-data" class="dropzone" id="dropzone_semua">
										<input type="hidden" class="form-control" id="upload_piutang_id" placeholder="" name="upload_piutang_id" value="">
										<div>
										  <h5>DOCUMENT</h5>
										</div>
									</form>
									<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
										<table class="table table-bordered table-striped" id="index_dokumen">
											<thead>
												<tr>
													<th hidden width="2%">ID</th>
													<th width="10%">NO</th>
													<th width="10%">FILE</th>
													<th width="10%">Size</th>
													<th width="10%">AKSI</th>											
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</form>
								</div>
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">	
							<div class="block-content">
								
							</div>				
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="fu_piutang_id" placeholder="No. PO" name="fu_piutang_id" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_tambah_fu" id="btn_tambah_fu"><i class="fa fa-save"></i> Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
var myDropzone 
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   $('.js-summernote').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
	
	Dropzone.autoDiscover = false;
	myDropzone = new Dropzone(".dropzone", { 
	   autoProcessQueue: true,
	   maxFilesize: 30,
	});
	myDropzone.on("complete", function(file) {	  
	  myDropzone.removeFile(file);
	  $('#index_dokumen').DataTable().ajax.reload( null, false );
	});
	
});
$(document).on("click", "#btn_tambah_fu", function() {
	
	var keterangan=$("#keterangan").summernote('code');
	if ($("#fu_tanggal_info").val()==''){
		sweetAlert("Maaf...", "Tanggal Informasi belum dipilih!", "error");
		return false;
	}
	if ($("#keterangan").val()==''){
		sweetAlert("Maaf...", "Alasan Hapus belum dipilih!", "error");
		return false;
	}
	var piutang_id=$("#fu_piutang_id").val();
	var tanggal_info=$("#fu_tanggal_info").val();
	var nominal_dihapus=$("#nominal_dihapus").val();
	// var tanggal_reminder=$("#fu_tanggal_reminder").val();
	// alert(piutang_id);
	swal({
		title: "Anda Yakin ?",
		text : "Akan Menghapus Tagihan ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpiutang_tt_reminder/tambah_fu_hapus',
			type: 'POST',
			data: {
				piutang_id: piutang_id,
				tanggal_info: tanggal_info,
				nominal_dihapus: nominal_dihapus,
				// tanggal_reminder: tanggal_reminder,
				keterangan: keterangan,
				},
			complete: function() {
				 $('#index_list').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Tambah Follow Up'});
				$("#modal_fu").modal('hide');
				$("#cover-spin").hide();
			}
		});
	});
	
	
});
$(document).on("click",".hapus_file",function(){	
	var table = $('#index_dokumen').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	$.ajax({
		url: '{site_url}tpiutang_tt_reminder/hapus_file',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
			$('#index_dokumen').DataTable().ajax.reload( null, false );			
		}
	});
});
function load_index(){
	var no_reg=$("#no_reg").val();
	var no_medrec=$("#no_medrec").val();
	var nama_pasien=$("#nama_pasien").val();
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	var tipe=$("#tipe").val();
	// alert(tipe);
	var status=$("#status").val();
	var tgl_trx1=$("#tgl_trx1").val();
	var tgl_trx2=$("#tgl_trx2").val();
	var tanggaldaftar1=$("#tanggaldaftar1").val();
	var tanggaldaftar2=$("#tanggaldaftar2").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,2,3,8], "visible": false },
							{ "width": "3%", "targets": [4] },
							{ "width": "6%", "targets": [5,6,10,14] },
							{ "width": "7%", "targets": [9,10,11,12,13,15,16,17] },
							// { "width": "10%", "targets": [9] },
							// { "width": "8%", "targets": [8,,9,10] },
							{ "width": "12%", "targets": [18,7] },
						 {"targets": [11,12,10], className: "text-right" },
						 {"targets": [9,5,6,7,8,10,14,15,16,18,13], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpiutang_tt_reminder/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_reg:no_reg,no_medrec:no_medrec,nama_pasien:nama_pasien,idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan,tipe:tipe,status:status,tgl_trx1:tgl_trx1,
						tgl_trx2:tgl_trx2,tanggaldaftar1:tanggaldaftar1,tanggaldaftar2:tanggaldaftar2,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

$(document).on("click", ".ganti", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,3).data()
	var tanggal=table.cell(tr,13).data()
	var nominal=table.cell(tr,11).data()
	$("#xnominal2").val(nominal);
	$("#tgl_asal").val(tanggal);
	$("#xtgl_pilih").val(tanggal);
	$("#xid").val(id);
	$("#xalasan").val('');
	$('#modal_edit').modal('show');
	
});
function load_list_tanggal(){
	var idkelompokpasien=$("#xidkelompokpasien").val();
	var idrekanan=$("#xidrekanan").val();
	var tipe=$("#xtipe").val();
	var st_multiple=$("#xst_multiple").val();
	var idpasien=$("#xidpasien").val();
	// alert(idkelompokpasien+' - '+idrekanan+' - '+tipe);
	if (idkelompokpasien && tipe){
		$.ajax({
			url: '{site_url}tklaim_rincian/list_tanggal',
			dataType: "json",
			type: 'POST',
			data: {idkelompokpasien: idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
			success: function(data) {
				$("#xtgl_pilih").empty();
				$('#xtgl_pilih').append(data.detail);
			}
		});
	}
}

$(document).on("click", "#btn_ubah", function() {
		if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		// alert($("#tgl_asal").val()+' '+$("#xtgl_pilih").val());
		if ($("#tgl_asal").val()==$("#xtgl_pilih").val()){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		if ($("#xalasan").val()=='' || $("#xalasan").val()==null){
			sweetAlert("Maaf...", "Alasan harus diisi!", "error");
			return false;
		}
		// alert();
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan Tanggal Jatuh Tempo?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal_satu();
		});
		
	});
function edit_tanggal_satu(){
		var xid=$("#xid").val();		
		var tanggal_jatuh_tempo=$("#xtgl_pilih").val();		
		var tgl_asal=$("#tgl_asal").val();		
		var xalasan=$("#xalasan").val();		
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tpiutang_tt_monitoring/edit_tanggal_satu',
			type: 'POST',
			data: {id: xid,tanggal_jatuh_tempo:tanggal_jatuh_tempo,alasan:xalasan,tgl_asal:tgl_asal},
			complete: function() {
				table.ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
				// location.reload();
				// filter_form();
				$("#modal_edit").modal('hide');
				$("#cover-spin").hide();
			}
		});
	}
	$(document).on("click", ".hapus", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,3).data()
		var nominal_dihapus=table.cell(tr,11).data()
		$("#upload_piutang_id").val(id);
		$("#fu_piutang_id").val(id);
		$("#nominal_dihapus").val(nominal_dihapus);
		// $("#fu_no_tagihan").val(table.cell(tr,3).data());
		// $("#fu_tanggal_info").val(table.cell(tr,9).data());
		$("#keterangan").summernote('code','')
		$("#tanggal_reminder").val();
		// load_informasi();
		load_upload();
		$("#modal_fu").modal('show');
	});
	function load_upload(){
		var piutang_id=$("#fu_piutang_id").val();
		
		$('#index_dokumen').DataTable().destroy();
		table = $('#index_dokumen').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 10,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [{ "targets": [0], "visible": false },
								{ "width": "5%", "targets": [1] },
								{ "width": "40%", "targets": [2] },
								{ "width": "10%", "targets": [3] },
								{ "width": "15%", "targets": [4] },
							 {"targets": [2], className: "text-left" },
							 {"targets": [1], className: "text-right" },
							 {"targets": [3,4], className: "text-center" }
							 ],
				ajax: { 
					url: '{site_url}tpiutang_tt_reminder/load_upload', 
					type: "POST" ,
					dataType: 'json',
					data : {
							piutang_id:piutang_id
						   }
				}
			});
	}
</script>