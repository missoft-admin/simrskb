<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_hutang" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_hutang/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Proses Approval')?></label>
				<input type="hidden" class="form-control" id="tmp_idakun_kredit" placeholder="0" name="tmp_idakun_kredit" value="{idakun_kredit}">
				<div class="col-md-4">
					<select id="st_auto_posting" name="st_auto_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			<?
			$rows2=get_all('msumber_kas',array(),'nama','ASC');
			$rows=get_all('mdistributor',array(),'nama','ASC');
			?>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Batas Waktu Batal')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal" placeholder="0" name="batas_batal" value="<?=$batas_batal?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('SETTING AKUN HUTANG')?></h4></label>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_hutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe</th>															
								<th style="width: 20%;">Distributor </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="tipe_distributor_hutang" id="tipe_distributor_hutang" style="width: 100%" id="step" class="js-select2 form-control input-sm tipe_distributor">										
										<option value="#" selected>- Pilih -</option>																			
										<option value="1">Pemesanan Gudang</option>																			
										<option value="3">Pengajuan</option>																			
										
									</select>										
								</td>
								<td>
									<select name="iddistributor_hutang" id="iddistributor_hutang" style="width: 100%" id="step" class="js-select2 form-control input-sm distirbutor">										
										<option value="#" selected>- All Distributor -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_hutang" id="idakun_hutang" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_hutang" placeholder="0" name="id_edit_hutang" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_hutang" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_hutang" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_primary('SETTING BIAYA CHEQ')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_cheq" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe </th>															
								<th style="width: 20%;">Distributor </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="tipe_distributor_cheq" id="tipe_distributor_cheq" style="width: 100%" id="step" class="js-select2 form-control input-sm tipe_distributor">										
										<option value="#" selected>- Pilih -</option>																			
										<option value="1">Pemesanan Gudang</option>																			
										<option value="3">Pengajuan</option>																			
										
									</select>										
								</td>
								<td>
									<select name="iddistributor_cheq" id="iddistributor_cheq" style="width: 100%" id="step" class="js-select2 form-control input-sm distirbutor">										
										<option value="#" selected>- All Distributor -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_cheq" id="idakun_cheq" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_cheq" placeholder="0" name="id_edit_cheq" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_cheq" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_cheq" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_danger('SETTING BIAYA MATERAI')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_materai" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe </th>															
								<th style="width: 20%;">Distributor </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="tipe_distributor_materai" id="tipe_distributor_materai" style="width: 100%" id="step" class="js-select2 form-control input-sm tipe_distributor">										
										<option value="#" selected>- Pilih -</option>																			
										<option value="1">Pemesanan Gudang</option>																			
										<option value="3">Pengajuan</option>																			
										
									</select>										
								</td>
								<td>
									<select name="iddistributor_materai" id="iddistributor_materai" style="width: 100%" class="js-select2 form-control input-sm distirbutor">										
										<option value="#" selected>- All Distributor -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_materai" id="idakun_materai" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_materai" placeholder="0" name="id_edit_materai" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_materai" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_materai" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_warning('SETTING AKUN BIAYA TRANSFER')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_kas" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Sumber Kas </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								
								<td>
									<select name="sumber_kas_id" id="sumber_kas_id" style="width: 100%" class="js-select2 form-control input-sm">										
										<option value="#" selected>- All Sumber Kas -</option>																			
										<?foreach($rows2 as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>																				
										<?}?>
									</select>										
								</td>								
								
								<td>
									<select name="idakun_kas" id="idakun_kas" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_kas" placeholder="0" name="id_edit_kas" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_kas" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_kas" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_akun();
		load_hutang();
		load_cheq();
		load_materai();
		load_kas();
		clear_input_hutang();
		clear_input_materai();
		clear_input_cheq();

	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting=$("#st_auto_posting").val();
		var idakun_kredit=$("#idakun_kredit").val();
		var batas_batal=$("#batas_batal").val();
		$("#tmp_idakun_kredit").val(idakun_kredit)
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/update',
			type: 'POST',
			data: {
				st_auto_posting:st_auto_posting,idakun_kredit:idakun_kredit,batas_batal:batas_batal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_kredit").val($("#tmp_idakun_kredit").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_kredit").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	$(".tipe_distributor").change(function(){
		list_distributor($(this).val());
	});

	function list_distributor($tipe_distributor){
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/list_distributor/'+$tipe_distributor,
			dataType: "json",
			success: function(data) {
				$(".distirbutor").empty();
				$('.distirbutor').append('<option value="#" selected>- All Distributor -</option>');
				$('.distirbutor').append(data.detail);
				
			}
		});		
	}
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/list_akun',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	
	
	function validate_add_hutang()
	{
		
		
		if ($("#tipe_distributor_hutang").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idakun_hutang").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_hutang(){
		$("#tipe_distributor_hutang").val('#').trigger('change');	
		$("#iddistributor_hutang").val('#').trigger('change');	
		$("#id_edit_hutang").val('');		
		$("#idakun_hutang").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_hutang",function(){
		if (validate_add_hutang()==false)return false;
		var id_edit=$("#id_edit_hutang").val();
		var tipe_distributor=$("#tipe_distributor_hutang").val();
		var iddistributor=$("#iddistributor_hutang").val();
		var idkategori=$("#idkategori_hutang").val();
		var idbarang=$("#idbarang_hutang").val();
		var idakun=$("#idakun_hutang").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/simpan_hutang',
			type: 'POST',
			data: {
				id_edit:id_edit,iddistributor:iddistributor,
				idkategori: idkategori,idbarang:idbarang,idakun:idakun,tipe_distributor:tipe_distributor,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_hutang').DataTable().ajax.reload( null, false );
					clear_input_hutang();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_hutang($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_hutang/hapus_hutang/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_hutang').DataTable().ajax.reload( null, false );
						clear_input_hutang();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_hutang",function(){
		
		clear_input_hutang();
	});
	function load_hutang(){
		var idlogic=$("#id").val();
		
		$('#tabel_hutang').DataTable().destroy();
		var table = $('#tabel_hutang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_hutang/load_hutang/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	//PPN
	
	function validate_add_cheq()
	{
		if ($("#tipe_distributor_cheq").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		
		if ($("#idakun_cheq").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function clear_input_cheq(){
		$("#iddistributor_cheq").val('#').trigger('change');	
			
		$("#id_edit_cheq").val('');		
		$("#idakun_cheq").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_cheq",function(){
		if (validate_add_cheq()==false)return false;
		var id_edit=$("#id_edit_cheq").val();
		var iddistributor=$("#iddistributor_cheq").val();
		var tipe_distributor=$("#tipe_distributor_cheq").val();
		var idakun=$("#idakun_cheq").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/simpan_cheq',
			type: 'POST',
			data: {
				id_edit:id_edit,iddistributor:iddistributor,idakun:idakun,tipe_distributor:tipe_distributor,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_cheq').DataTable().ajax.reload( null, false );
					clear_input_cheq();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_cheq($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_hutang/hapus_cheq/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_cheq').DataTable().ajax.reload( null, false );
						clear_input_cheq();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_cheq",function(){
		
		clear_input_cheq();
	});
	function load_cheq(){
		var idlogic=$("#id").val();
		
		$('#tabel_cheq').DataTable().destroy();
		var table = $('#tabel_cheq').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_hutang/load_cheq/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
//DISKON

	function validate_add_materai()
	{
		if ($("#tipe_distributor_materai").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		
		if ($("#idakun_materai").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_materai(){
		$("#iddistributor_materai").val('#').trigger('change');	
					
		$("#id_edit_materai").val('');		
		$("#idakun_materai").val('#').trigger('change');
	}
	$(document).on("click","#simpan_materai",function(){
		if (validate_add_materai()==false)return false;
		var id_edit=$("#id_edit_materai").val();
		var tipe_distributor=$("#tipe_distributor_materai").val();
		var iddistributor=$("#iddistributor_materai").val();
		var idakun=$("#idakun_materai").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/simpan_materai',
			type: 'POST',
			data: {
				id_edit:id_edit,iddistributor:iddistributor,
				idakun:idakun,tipe_distributor:tipe_distributor,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_materai').DataTable().ajax.reload( null, false );
					clear_input_materai();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_materai($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_hutang/hapus_materai/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_materai').DataTable().ajax.reload( null, false );
						clear_input_materai();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_materai",function(){
		
		clear_input_materai();
	});
	function load_materai(){
		var idlogic=$("#id").val();
		
		$('#tabel_materai').DataTable().destroy();
		var table = $('#tabel_materai').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_hutang/load_materai/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	function validate_add_kas()
	{
		
		
		if ($("#idakun_kas").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_kas(){
		$("#sumber_kas_id").val('#').trigger('change');	
					
		$("#id_edit_kas").val('');		
		$("#idakun_kas").val('#').trigger('change');
	}
	$(document).on("click","#simpan_kas",function(){
		if (validate_add_kas()==false)return false;
		var id_edit=$("#id_edit_kas").val();
		var sumber_kas_id=$("#sumber_kas_id").val();
		var idakun=$("#idakun_kas").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/simpan_kas',
			type: 'POST',
			data: {
				id_edit:id_edit,sumber_kas_id:sumber_kas_id,
				idakun:idakun,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_kas').DataTable().ajax.reload( null, false );
					clear_input_kas();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_kas($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_hutang/hapus_kas/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_kas').DataTable().ajax.reload( null, false );
						clear_input_kas();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_kas",function(){
		
		clear_input_kas();
	});
	function load_kas(){
		var idlogic=$("#id").val();
		
		$('#tabel_kas').DataTable().destroy();
		var table = $('#tabel_kas').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_hutang/load_kas/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
</script>
