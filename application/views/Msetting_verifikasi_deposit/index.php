<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-content">
		<?php echo form_open('msetting_verifikasi_deposit/save','class="form-horizontal push-10-t"') ?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive">
			<thead>
				<tr>
					<th>Metode Deposit</th>
					<th>No. Akun Debit</th>
					<th>No. Akun Kredit</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<select class="js-select2 form-control" name="idmetode" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php if(!in_array("1", $list_akun_submited) || $idmetode != '') { ?>
								<option value="1" <?=($idmetode == 1 ? 'selected':'')?>>Tunai</option>
							<?php } ?>

							<?php if(!in_array("2", $list_akun_submited) || $idmetode != '') { ?>
								<option value="2" <?=($idmetode == 2 ? 'selected':'')?>>Debit</option>
							<?php } ?>

							<?php if(!in_array("3", $list_akun_submited) || $idmetode != '') { ?>
								<option value="3" <?=($idmetode == 3 ? 'selected':'')?>>Kredit</option>
							<?php } ?>

							<?php if(!in_array("4", $list_akun_submited) || $idmetode != '') { ?>
								<option value="4" <?=($idmetode == 4 ? 'selected':'')?>>Transfer</option>
							<?php } ?>
						</select>
					</td>
					<td>
						<select class="js-select2 form-control" name="noakun_debit" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('makun_nomor') as $row) { ?>
								<option value="<?=$row->noakun?>" <?=($noakun_debit == $row->noakun ? 'selected':'')?>><?=$row->noakun?> <?=$row->namaakun?></option>
							<?php } ?>
						</select>
					</td>
					<td>
						<select class="js-select2 form-control" name="noakun_kredit" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('makun_nomor') as $row) { ?>
								<option value="<?=$row->noakun?>" <?=($noakun_kredit == $row->noakun ? 'selected':'')?>><?=$row->noakun?> <?=$row->namaakun?></option>
							<?php } ?>
						</select>
					</td>
					<td>
						<button class="btn btn-success" type="submit">Simpan</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<hr>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Metode Deposit</th>
					<th>No. Akun Debit</th>
					<th>No. Akun Kredit</th>
					<th>Last Update</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($list_setting as $index => $row) { ?>
					<tr>
						<td><?=$index+1?></td>
						<td><?=metodePembayaran($row->idmetode)?></td>
						<td><?=$row->noakun_debit?> <?=$row->namaakun_debit?></td>
						<td><?=$row->noakun_kredit?> <?=$row->namaakun_kredit?></td>
						<td><?=$row->updated_at?></td>
						<td>
							<div class="btn-group">
								<a href="<?=base_url()?>msetting_verifikasi_deposit/index/<?=$row->id?>" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								<a href="<?=base_url()?>msetting_verifikasi_deposit/delete/<?=$row->id?>" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>
</div>
