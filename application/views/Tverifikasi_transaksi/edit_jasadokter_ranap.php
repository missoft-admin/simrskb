<?= ErrorSuccess($this->session); ?>
<?php if ($error != '') {
	echo ErrorMessage($error);
} ?>

<style media="screen">
.text-bold {
    font-weight: bold;
}

.separator-header {
    margin-top: 450px;
}

.modal {
    overflow: auto !important;
}

</style>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tverifikasi_transaksi/index/rawatinap" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{nomedrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Alamat Pasien</span>
                    <input type="text" class="form-control tindakanAlamatPasien" value="{alamatpasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Handphone</span>
                    <input type="text" class="form-control tindakanNoHandphone" value="{nohp}" readonly="true">
                </div>
                <hr>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Penanggung Jawab</span>
                    <input type="text" class="form-control tindakanPenanggungJawab" value="{namapenanggungjawab}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Telephone</span>
                    <input type="text" class="form-control tindakanTelpPenanggungJawab" value="{telppenanggungjawab}" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                    <input type="text" class="form-control tindakanKelompokPasien" value="{namakelompok}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
                    <input type="text" class="form-control tindakanNamaPerusahaan" value="{namaperusahaan}" readonly="true">
                </div>
                <hr>
                <?php if ($idtipe == 1) {?>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelas Perawatan</span>
                    <input type="text" class="form-control tindakanNamaKelas" value="{namakelas}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
                    <input type="text" class="form-control tindakanNamaBed" value="{namabed}" readonly="true">
                </div>
                <hr>
                <?php } ?>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
                    <input type="text" class="form-control tindakanNamaDokter" value="{namadokterpenanggungjawab}" readonly="true">
                </div>
            </div>
        </div>

        <div class="content-verification">

            <hr>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 10px;">
                        <label class="col-md-4 control-label" style="margin-top: 5px;">Tipe Pasien</label>
                        <div class="col-md-8">
                            <select id="idtipepasien" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="1" <?=($idtipepasien == 1 ? 'selected' : '')?>>Pasien RS</option>
                                <option value="2" <?=($idtipepasien == 2 ? 'selected' : '')?>>Pasien Pribadi</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">FULL CARE</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRawatInapFullCare = 0; ?>
                    <?php $PajakRawatInapFullCare = 0; ?>
                    <?php $NettoRawatInapFullCare = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($id, 1) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trawatinap_tindakan',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRawatInapFullCare = $PotonganRawatInapFullCare + $nominal_potongan_rs; ?>
                    <?php $PajakRawatInapFullCare = $PajakRawatInapFullCare + $nominal_pajak_dokter; ?>
                    <?php $NettoRawatInapFullCare = $NettoRawatInapFullCare + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRawatInapFullCare)?></td>
                        <td class="text-bold"><?=number_format($PajakRawatInapFullCare)?></td>
                        <td class="text-bold"><?=number_format($NettoRawatInapFullCare)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">ECG</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRawatInapECG = 0; ?>
                    <?php $PajakRawatInapECG = 0; ?>
                    <?php $NettoRawatInapECG = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($id, 2) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trawatinap_tindakan',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRawatInapECG = $PotonganRawatInapECG + $nominal_potongan_rs; ?>
                    <?php $PajakRawatInapECG = $PajakRawatInapECG + $nominal_pajak_dokter; ?>
                    <?php $NettoRawatInapECG = $NettoRawatInapECG + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRawatInapECG)?></td>
                        <td class="text-bold"><?=number_format($PajakRawatInapECG)?></td>
                        <td class="text-bold"><?=number_format($NettoRawatInapECG)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">AMBULANCE</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRawatInapAmbulance = 0; ?>
                    <?php $PajakRawatInapAmbulance = 0; ?>
                    <?php $NettoRawatInapAmbulance = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($id, 5) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trawatinap_tindakan',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRawatInapAmbulance = $PotonganRawatInapAmbulance + $nominal_potongan_rs; ?>
                    <?php $PajakRawatInapAmbulance = $PajakRawatInapAmbulance + $nominal_pajak_dokter; ?>
                    <?php $NettoRawatInapAmbulance = $NettoRawatInapAmbulance + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRawatInapAmbulance)?></td>
                        <td class="text-bold"><?=number_format($PajakRawatInapAmbulance)?></td>
                        <td class="text-bold"><?=number_format($NettoRawatInapAmbulance)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">SEWA ALAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRawatInapSewaAlat = 0; ?>
                    <?php $PajakRawatInapSewaAlat = 0; ?>
                    <?php $NettoRawatInapSewaAlat = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($id, 4) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trawatinap_tindakan',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRawatInapSewaAlat = $PotonganRawatInapSewaAlat + $nominal_potongan_rs; ?>
                    <?php $PajakRawatInapSewaAlat = $PajakRawatInapSewaAlat + $nominal_pajak_dokter; ?>
                    <?php $NettoRawatInapSewaAlat = $NettoRawatInapSewaAlat + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRawatInapSewaAlat)?></td>
                        <td class="text-bold"><?=number_format($PajakRawatInapSewaAlat)?></td>
                        <td class="text-bold"><?=number_format($NettoRawatInapSewaAlat)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">LAIN LAIN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRawatInapLainLain = 0; ?>
                    <?php $PajakRawatInapLainLain = 0; ?>
                    <?php $NettoRawatInapLainLain = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($id, 6) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trawatinap_tindakan',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRawatInapLainLain = $PotonganRawatInapLainLain + $nominal_potongan_rs; ?>
                    <?php $PajakRawatInapLainLain = $PajakRawatInapLainLain + $nominal_pajak_dokter; ?>
                    <?php $NettoRawatInapLainLain = $NettoRawatInapLainLain + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRawatInapLainLain)?></td>
                        <td class="text-bold"><?=number_format($PajakRawatInapLainLain)?></td>
                        <td class="text-bold"><?=number_format($NettoRawatInapLainLain)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">UMUM</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganLaboratoriumUmum = 0; ?>
                    <?php $PajakLaboratoriumUmum = 0; ?>
                    <?php $NettoLaboratoriumUmum = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($id, 1) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_laboratorium_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idlaboratorium,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganLaboratoriumUmum = $PotonganLaboratoriumUmum + $nominal_potongan_rs; ?>
                    <?php $PajakLaboratoriumUmum = $PajakLaboratoriumUmum + $nominal_pajak_dokter; ?>
                    <?php $NettoLaboratoriumUmum = $NettoLaboratoriumUmum + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganLaboratoriumUmum)?></td>
                        <td class="text-bold"><?=number_format($PajakLaboratoriumUmum)?></td>
                        <td class="text-bold"><?=number_format($NettoLaboratoriumUmum)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">PATHOLOGI ANATOMI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganLaboratoriumPA = 0; ?>
                    <?php $PajakLaboratoriumPA = 0; ?>
                    <?php $NettoLaboratoriumPA = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($id, 2) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_laboratorium_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idlaboratorium,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganLaboratoriumPA = $PotonganLaboratoriumPA + $nominal_potongan_rs; ?>
                    <?php $PajakLaboratoriumPA = $PajakLaboratoriumPA + $nominal_pajak_dokter; ?>
                    <?php $NettoLaboratoriumPA = $NettoLaboratoriumPA + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganLaboratoriumPA)?></td>
                        <td class="text-bold"><?=number_format($PajakLaboratoriumPA)?></td>
                        <td class="text-bold"><?=number_format($NettoLaboratoriumPA)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">PMI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganLaboratoriumPMI = 0; ?>
                    <?php $PajakLaboratoriumPMI = 0; ?>
                    <?php $NettoLaboratoriumPMI = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($id, 3) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_laboratorium_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idlaboratorium,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganLaboratoriumPMI = $PotonganLaboratoriumPMI + $nominal_potongan_rs; ?>
                    <?php $PajakLaboratoriumPMI = $PajakLaboratoriumPMI + $nominal_pajak_dokter; ?>
                    <?php $NettoLaboratoriumPMI = $NettoLaboratoriumPMI + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganLaboratoriumPMI)?></td>
                        <td class="text-bold"><?=number_format($PajakLaboratoriumPMI)?></td>
                        <td class="text-bold"><?=number_format($NettoLaboratoriumPMI)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">X-RAY</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRadiologi = 0; ?>
                    <?php $PajakRadiologi = 0; ?>
                    <?php $NettoRadiologi = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($id, 1) as $row) {?>
                    <?php if ($row->jasamedis > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_radiologi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idradiologi,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => $row->status_tindakan,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRadiologi = $PotonganRadiologi + $nominal_potongan_rs; ?>
                    <?php $PajakRadiologi = $PajakRadiologi + $nominal_pajak_dokter; ?>
                    <?php $NettoRadiologi = $NettoRadiologi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRadiologi)?></td>
                        <td class="text-bold"><?=number_format($PajakRadiologi)?></td>
                        <td class="text-bold"><?=number_format($NettoRadiologi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">USG</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRadiologi = 0; ?>
                    <?php $PajakRadiologi = 0; ?>
                    <?php $NettoRadiologi = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($id, 2) as $row) {?>
                    <?php if ($row->jasamedis > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_radiologi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idradiologi,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRadiologi = $PotonganRadiologi + $nominal_potongan_rs; ?>
                    <?php $PajakRadiologi = $PajakRadiologi + $nominal_pajak_dokter; ?>
                    <?php $NettoRadiologi = $NettoRadiologi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRadiologi)?></td>
                        <td class="text-bold"><?=number_format($PajakRadiologi)?></td>
                        <td class="text-bold"><?=number_format($NettoRadiologi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">CT SCAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRadiologi = 0; ?>
                    <?php $PajakRadiologi = 0; ?>
                    <?php $NettoRadiologi = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($id, 3) as $row) {?>
                    <?php if ($row->jasamedis > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_radiologi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idradiologi,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRadiologi = $PotonganRadiologi + $nominal_potongan_rs; ?>
                    <?php $PajakRadiologi = $PajakRadiologi + $nominal_pajak_dokter; ?>
                    <?php $NettoRadiologi = $NettoRadiologi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRadiologi)?></td>
                        <td class="text-bold"><?=number_format($PajakRadiologi)?></td>
                        <td class="text-bold"><?=number_format($NettoRadiologi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">MRI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRadiologi = 0; ?>
                    <?php $PajakRadiologi = 0; ?>
                    <?php $NettoRadiologi = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($id, 4) as $row) {?>
                    <?php if ($row->jasamedis > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_radiologi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idradiologi,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRadiologi = $PotonganRadiologi + $nominal_potongan_rs; ?>
                    <?php $PajakRadiologi = $PajakRadiologi + $nominal_pajak_dokter; ?>
                    <?php $NettoRadiologi = $NettoRadiologi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRadiologi)?></td>
                        <td class="text-bold"><?=number_format($PajakRadiologi)?></td>
                        <td class="text-bold"><?=number_format($NettoRadiologi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">BMD</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRadiologi = 0; ?>
                    <?php $PajakRadiologi = 0; ?>
                    <?php $NettoRadiologi = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($id, 5) as $row) {?>
                    <?php if ($row->jasamedis > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_radiologi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idradiologi,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRadiologi = $PotonganRadiologi + $nominal_potongan_rs; ?>
                    <?php $PajakRadiologi = $PajakRadiologi + $nominal_pajak_dokter; ?>
                    <?php $NettoRadiologi = $NettoRadiologi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRadiologi)?></td>
                        <td class="text-bold"><?=number_format($PajakRadiologi)?></td>
                        <td class="text-bold"><?=number_format($NettoRadiologi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER FISIOTERAPI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganFisioterapi = 0; ?>
                    <?php $PajakFisioterapi = 0; ?>
                    <?php $NettoFisioterapi = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($id) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_fisioterapi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganFisioterapi = $PotonganFisioterapi + $nominal_potongan_rs; ?>
                    <?php $PajakFisioterapi = $PajakFisioterapi + $nominal_pajak_dokter; ?>
                    <?php $NettoFisioterapi = $NettoFisioterapi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganFisioterapi)?></td>
                        <td class="text-bold"><?=number_format($PajakFisioterapi)?></td>
                        <td class="text-bold"><?=number_format($NettoFisioterapi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER VISITE</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:5%">Ruangan</th>
                        <th style="width:5%">Tanggal</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Jasa Medis</th>
                        <th style="width:5%">Status Perujuk</th>
                        <th style="width:5%">Perujuk (%)</th>
                        <th style="width:5%">Potongan RS (%)</th>
                        <th style="width:5%">Pajak Dokter (%)</th>
                        <th style="width:10%">Perujuk (Rp)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PerujukVisite = 0; ?>
                    <?php $PotonganVisite = 0; ?>
                    <?php $PajakVisite = 0; ?>
                    <?php $NettoVisite = 0; ?>
                    <?php foreach ($FeeJasaDokterVisite as $row) { ?>
                    <?php if ($row['jasamedis'] > 0) { ?>
                    <?php $urutan_operasi = (isset($row['urutan_operasi']) ? $row['urutan_operasi'] : '0'); ?>
                    <?php $urutan_visite = (isset($row['urutan_visite']) ? $row['urutan_visite'] : '0'); ?>
                    <?php $status_perujuk = logicFeeRujukan($idtipepasien, $idasalpasien, $idpoliklinik, $idkategoriperujuk, $row['iddokter'], $urutan_operasi, $urutan_visite, $statusKasirRajal); ?>
                    <?php $persentase_perujuk = ($status_perujuk ? $row['persentaseperujuk'] : 0); ?>
                    <?php $potongan_rs = ($status_perujuk ? $row['potonganfeers'] : $row['potongan_rs']); ?>
                    <?php $jasamedis = ($row['jasamedis'] - $row['diskon']); ?>
                    <?php $nominal_perujuk = $jasamedis  * ($persentase_perujuk / 100); ?>
                    <?php $nominal_potongan_rs = $jasamedis * ($potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = ($jasamedis - $nominal_perujuk) * ($row['pajak_dokter'] / 100); ?>
                    <?php $jasamedis_netto = $jasamedis - $nominal_perujuk - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row['periode_pembayaran'] != '' ? DMYFormat($row['periode_pembayaran']) : getPembayaranHonorDokter($row['iddokter'])); ?>
                    <?php $periode_jatuhtempo = ($row['periode_jatuhtempo'] != '' ? DMYFormat($row['periode_jatuhtempo']) : getJatuhTempoHonorDokter($row['iddokter'])); ?>

                    <?php
						$dataModal = [
							'table' => 'trawatinap_visite',
							'idrawatinap' => $id,
							'iddetail' => $row['iddetail'],
							'idtarif' => $row['idtarif'],
							'namatarif' => $row['namatarif'],
							'iddokter' => $row['iddokter'],
							'namadokter' => $row['namadokter'],
							'jasamedis' => $jasamedis,
							'potongan_rs' => $potongan_rs,
							'pajak_dokter' => $row['pajak_dokter'],
							'persentase_perujuk' => $persentase_perujuk,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'nominal_perujuk' => $nominal_perujuk,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row['namatarif']?> <br><span style="color: red">Urutan Operasi : <?=$urutan_operasi?> Urutan Visite : <?=$urutan_visite?></span></td>
                        <td><?=$row['namaruangan']?></td>
                        <td><?=DMYFormat($row['tanggal'])?></td>
                        <td><?=($row['namadokter'] != null ? $row['namadokter'] : '-')?></td>
                        <td><?=number_format($jasamedis)?></td>
                        <td align="center"><?=($status_perujuk ? '<span class="label label-xl label-primary" style="font-size: 11px;"><i class="fa fa-check"></i></span>' : '<span class="label label-xl label-danger" style="font-size: 11px;"><i class="fa fa-times"></i></span>') ?></td>
                        <td><?=number_format($persentase_perujuk, 2)?></td>
                        <td><?=number_format($potongan_rs, 2)?></td>
                        <td><?=number_format($row['pajak_dokter'], 2)?></td>
                        <td><?=number_format($nominal_perujuk)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row['status_jasamedis'])?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PerujukVisite = $PerujukVisite + $nominal_perujuk; ?>
                    <?php $PotonganVisite = $PotonganVisite + $nominal_potongan_rs; ?>
                    <?php $PajakVisite = $PajakVisite + $nominal_pajak_dokter; ?>
                    <?php $NettoVisite = $NettoVisite + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="9"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PerujukVisite)?></td>
                        <td class="text-bold"><?=number_format($PotonganVisite)?></td>
                        <td class="text-bold"><?=number_format($PajakVisite)?></td>
                        <td class="text-bold"><?=number_format($NettoVisite)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER OPERATOR</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tanggal Operasi</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:5%">Status Perujuk</th>
                        <th style="width:5%">Perujuk (%)</th>
                        <th style="width:5%">Potongan RS (%)</th>
                        <th style="width:5%">Pajak Dokter (%)</th>
                        <th style="width:10%">Perujuk (Rp)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PerujukOperator = 0; ?>
                    <?php $PotonganOperator = 0; ?>
                    <?php $PajakOperator = 0; ?>
                    <?php $NettoOperator = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewFeeOperasiJasaDokterOpertor($id) as $row) {?>
                    <?php //if ($row->jasamedis > 0) { ?>
                    <?php $status_perujuk = logicFeeRujukan($idtipepasien, $idasalpasien, $idpoliklinik, $idkategoriperujuk, $row->iddokter, $row->urutan_operasi, 1, $statusKasirRajal); ?>
                    <?php $jasamedis = ($row->jasamedis - $row->diskon); ?>
                    <?php $potongan_rs = ($status_perujuk ? $row->potonganfeers : $row->potongan_rs); ?>
                    <?php $persentase_perujuk = ($status_perujuk ? $row->persentaseperujuk : 0); ?>
                    <?php $nominal_perujuk = $jasamedis * ($persentase_perujuk / 100); ?>
                    <?php $nominal_potongan_rs = $jasamedis * ($potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = ($jasamedis - $nominal_perujuk) * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $jasamedis - $nominal_perujuk - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'tkamaroperasi_jasado',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $jasamedis,
							'potongan_rs' => $potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'persentase_perujuk' => $persentase_perujuk,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'nominal_perujuk' => $nominal_perujuk,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($jasamedis)?></td>
                        <td align="center"><?=($status_perujuk ? '<span class="label label-xl label-primary" style="font-size: 11px;"><i class="fa fa-check"></i></span>' : '<span class="label label-xl label-danger" style="font-size: 11px;"><i class="fa fa-times"></i></span>') ?></td>
                        <td><?=number_format($persentase_perujuk, 2)?></td>
                        <td><?=number_format($potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_perujuk)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PerujukOperator = $PerujukOperator + $nominal_perujuk; ?>
                    <?php $PotonganOperator = $PotonganOperator + $nominal_potongan_rs; ?>
                    <?php $PajakOperator = $PajakOperator + $nominal_pajak_dokter; ?>
                    <?php $NettoOperator = $NettoOperator + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php //} ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="8"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PerujukOperator)?></td>
                        <td class="text-bold"><?=number_format($PotonganOperator)?></td>
                        <td class="text-bold"><?=number_format($PajakOperator)?></td>
                        <td class="text-bold"><?=number_format($NettoOperator)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER ANESTHESI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tanggal Operasi</th>
                        <th style="width:10%">Total Operator</th>
                        <th style="width:10%">Persentase Anesthesi</th>
                        <th style="width:10%">Total Anesthesi</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Persentase</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganAnesthesi = 0; ?>
                    <?php $PajakAnesthesi = 0; ?>
                    <?php $NettoAnesthesi = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($id) as $row) {?>
                    <?php $jasamedis = $row->grandtotal; ?>
                    <?php $nominal_potongan_rs = $jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'tkamaroperasi_jasada',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td><?=number_format($row->total_operator)?></td>
                        <td><?=number_format($row->persentase_anesthesi, 2)?></td>
                        <td><?=number_format($row->total_anesthesi)?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->persentase, 2)?></td>
                        <td><?=number_format($jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganAnesthesi = $PotonganAnesthesi + $nominal_potongan_rs; ?>
                    <?php $PajakAnesthesi = $PajakAnesthesi + $nominal_pajak_dokter; ?>
                    <?php $NettoAnesthesi = $NettoAnesthesi + $jasamedis_netto; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="9"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganAnesthesi)?></td>
                        <td class="text-bold"><?=number_format($PajakAnesthesi)?></td>
                        <td class="text-bold"><?=number_format($NettoAnesthesi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER FEE RUJUKAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Nominal Rujukan</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $idDokterFeeRujukan = ''; ?>
                    <?php $PotonganFeeRujukan = 0; ?>
                    <?php $PajakFeeRujukan = 0; ?>
                    <?php $NettoFeeRujukan = 0; ?>

                    <?php $nominal_perujuk = $PerujukVisite + $PerujukOperator; ?>
                    <?php $FeeRujukanDokter = get_all('tfeerujukan_dokter', ['idrawatinap' => $id]); ?>
                    <?php if (COUNT($FeeRujukanDokter) > 0) { ?>
                        <?php foreach ($FeeRujukanDokter as $row) { ?>
                            <?php if ($row->jasamedis > 0) { ?>
                                <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                                <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                                <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                                <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                                <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                                <?php
                                $dataModal = [
                                    'table' => 'tfeerujukan_dokter',
                                    'idrawatinap' => $id,
                                    'iddetail' => '',
                                    'idtarif' => '',
                                    'namatarif' => 'Fee Rujukan Dokter',
                                    'iddokter' => $row->iddokter,
                                    'namadokter' => $row->namadokter,
                                    'jasamedis' => $row->jasamedis,
                                    'potongan_rs' => $row->potongan_rs,
                                    'pajak_dokter' => $row->pajak_dokter,
                                    'nominal_potongan_rs' => $nominal_potongan_rs,
                                    'nominal_pajak_dokter' => $nominal_pajak_dokter,
                                    'jasamedis_netto' => $jasamedis_netto,
                                    'periode_pembayaran' => DMYFormat($periode_pembayaran),
                                    'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
                                    'status_expertise' => 0,
                                    'status_verifikasi' => $status_verifikasi,
                                ];
                                ?>

                                <?php $idDokterFeeRujukan = $row->iddokter; ?>
                                <?php $statusSelisihNominalFee = ($nominal_perujuk != $jasamedis ? 1 : 0); ?>
                                <tr style="<?=($statusSelisihNominalFee == 1 ? 'background-color: #ffc10740;' : '');?>">
                                    <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                                    <td><?=number_format($row->jasamedis)?></td>
                                    <td><?=number_format($row->potongan_rs, 2)?></td>
                                    <td><?=number_format($row->pajak_dokter, 2)?></td>
                                    <td><?=number_format($nominal_potongan_rs)?></td>
                                    <td><?=number_format($nominal_pajak_dokter)?></td>
                                    <td><?=number_format($jasamedis_netto)?></td>
                                    <td><?=$periode_pembayaran?></td>
                                    <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                                    <td>
                                        <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                                        <?php if ($statusSelisihNominalFee) { ?>
                                        <a href="{base_url}tverifikasi_transaksi/resetFeeRujukan/<?= $id; ?>" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $PotonganFeeRujukan = $PotonganFeeRujukan + $nominal_potongan_rs; ?>
                                <?php $PajakFeeRujukan = $PajakFeeRujukan + $nominal_pajak_dokter; ?>
                                <?php $NettoFeeRujukan = $NettoFeeRujukan + $jasamedis_netto; ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if ($nominal_perujuk > 0) { ?>
                            <?php $nominal_potongan_rs = $nominal_perujuk * ($potonganrs_perujuk / 100); ?>
                            <?php $nominal_pajak_dokter = $nominal_perujuk * ($pajak_perujuk / 100); ?>
                            <?php $jasamedis_netto = $nominal_perujuk - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                            <?php $namadokter_perujuk = ($namadokterperujuk != null ? $namadokterperujuk : '-'); ?>

                            <?php
                            $dataModal = [
                                'table' => 'tfeerujukan_dokter',
                                'idrawatinap' => $id,
                                'iddetail' => '',
                                'idtarif' => '',
                                'namatarif' => 'Fee Rujukan Dokter',
                                'iddokter' => $iddokterperujuk,
                                'namadokter' => $namadokter_perujuk,
                                'jasamedis' => $nominal_perujuk,
                                'potongan_rs' => $potonganrs_perujuk,
                                'pajak_dokter' => $pajak_perujuk,
                                'nominal_potongan_rs' => $nominal_potongan_rs,
                                'nominal_pajak_dokter' => $nominal_pajak_dokter,
                                'jasamedis_netto' => $jasamedis_netto,
                                'periode_pembayaran' => getPembayaranHonorDokter($iddokterperujuk),
                                'periode_jatuhtempo' => getJatuhTempoHonorDokter($iddokterperujuk),
                                'status_expertise' => 0,
                                'status_verifikasi' => $status_verifikasi,
                            ];
                            ?>

                            <?php $idDokterFeeRujukan = $iddokterperujuk; ?>

                            <tr>
                                <td><?=$namadokter_perujuk?></td>
                                <td><?=number_format($nominal_perujuk)?></td>
                                <td><?=number_format($potonganrs_perujuk, 2)?></td>
                                <td><?=number_format($pajak_perujuk, 2)?></td>
                                <td><?=number_format($nominal_potongan_rs)?></td>
                                <td><?=number_format($nominal_pajak_dokter)?></td>
                                <td><?=number_format($jasamedis_netto)?></td>
                                <td><?=getPembayaranHonorDokter($iddokterperujuk)?></td>
                                <td><?=StatusEditVerifikasi('0')?></td>
                                <td>
                                    <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                                </td>
                            </tr>
                            <?php $PotonganFeeRujukan = $PotonganFeeRujukan + $nominal_potongan_rs; ?>
                            <?php $PajakFeeRujukan = $PajakFeeRujukan + $nominal_pajak_dokter; ?>
                            <?php $NettoFeeRujukan = $NettoFeeRujukan + $jasamedis_netto; ?>
                        <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="4"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganFeeRujukan)?></td>
                        <td class="text-bold"><?=number_format($PajakFeeRujukan)?></td>
                        <td class="text-bold"><?=number_format($NettoFeeRujukan)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RAWAT JALAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganPoliklinikPelayanan = 0; ?>
                    <?php $PajakPoliklinikPelayanan = 0; ?>
                    <?php $NettoPoliklinikPelayanan = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($id) as $row) {?>
                    <?php if ($row->jasamedis > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'tpoliklinik_pelayanan',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr style="<?=($idDokterFeeRujukan == $row->iddokter ? 'background-color: #ffe3e1' : '')?>">
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganPoliklinikPelayanan = $PotonganPoliklinikPelayanan + $nominal_potongan_rs; ?>
                    <?php $PajakPoliklinikPelayanan = $PajakPoliklinikPelayanan + $nominal_pajak_dokter; ?>
                    <?php $NettoPoliklinikPelayanan = $NettoPoliklinikPelayanan + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganPoliklinikPelayanan)?></td>
                        <td class="text-bold"><?=number_format($PajakPoliklinikPelayanan)?></td>
                        <td class="text-bold"><?=number_format($NettoPoliklinikPelayanan)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <?php
				$totalPotonganRs = $PotonganPoliklinikPelayanan + $PotonganRawatInapFullCare + $PotonganRawatInapECG + $PotonganRawatInapAmbulance + $PotonganRawatInapSewaAlat + $PotonganRawatInapLainLain + $PotonganRadiologi + $PotonganLaboratoriumUmum + $PotonganLaboratoriumPA + $PotonganLaboratoriumPMI + $PotonganFisioterapi + $PotonganVisite + $PotonganOperator + $PotonganAnesthesi;
				$totalPajakDokter = $PajakPoliklinikPelayanan + $PajakRawatInapFullCare + $PajakRawatInapECG + $PajakRawatInapAmbulance + $PajakRawatInapSewaAlat + $PajakRawatInapLainLain + $PajakRadiologi + $PajakLaboratoriumUmum + $PajakLaboratoriumPA + $PajakLaboratoriumPMI + $PajakFisioterapi + $PajakVisite + $PajakOperator + $PajakAnesthesi;
			?>

            <table class="table table-bordered table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL POTONGAN RS</b></td>
                        <td align="left" class="text-bold" align="center" style="font-size:15px"><?=number_format($totalPotonganRs)?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL PAJAK DOKTER</b></td>
                        <td align="left" class="text-bold" align="center" style="font-size:15px"><?=number_format($totalPajakDokter)?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');
    $('.discount').number(true, 2, '.', ',');

    // Tipe Pasien
    $(document).on('change', '#idtipepasien', function() {
        var idpendaftaran = '<?= $id ?>';
        var idtipepasien = $(this).val();

        $.ajax({
            url: '{site_url}tverifikasi_transaksi/updateTipePasien',
            method: 'POST',
            dataType: 'json',
            data: {
                idpendaftaran: idpendaftaran,
                idtipepasien: idtipepasien,
                table: 'trawatinap_pendaftaran'
            },
            success: function(data) {
                if (data.status) {
                    swal({
                        title: 'Berhasil',
                        text: 'Tipe Pasien berhasil diubah.',
                        type: 'success',
                        timer: 1500
                    });
                }
            }
        });
    });
});

</script>

<!-- Modal Included -->
<?php $this->load->view('Tverifikasi_transaksi/modal/modal_jasa_dokter') ?>
