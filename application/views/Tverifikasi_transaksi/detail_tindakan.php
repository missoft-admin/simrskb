<?= ErrorSuccess($this->session); ?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tverifikasi_transaksi/index/rawatinap" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Tarif</span>
                    <input type="text" class="form-control" value="<?=htmlspecialchars($namatarif)?>" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Jenis Tarif</span>
                    <input type="text" class="form-control" value="<?=htmlspecialchars($jenistarif)?>" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
                    <input type="text" class="form-control" value="<?=htmlspecialchars($namadokter)?>" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">User Input</span>
                    <input type="text" class="form-control" value="<?=htmlspecialchars($userinput)?>" readonly="true">
                </div>
            </div>
        </div>

        <div class="content-verification">

            <hr>

            <div class="row">
                <div class="col-md-6">
                    <button data-toggle="modal" data-target="#ActionNoticeModal" class="btn btn-danger" type="submit">Notice Keseluruhan <i class="fa fa-arrow-right"></i></button>
                </div>
            </div>

            <hr>

            <table class="table table-bordered table-striped" id="detailTindakan" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Item Biaya</th>
                        <th style="width:10%">Harga Master</th>
                        <th style="width:10%">Harga</th>
                        <th style="width:10%">Diskon (%)</th>
                        <th style="width:10%">Diskon (Rp)</th>
                        <th style="width:10%">Total</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:10%">Group Pembayaran</th>
                        <th style="width:10%">Keputusan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($itemtindakan as $row) { ?>
                    <tr>
                        <td><?=$row['nama']?></td>
                        <td><?=number_format($row['tarif_master'])?></td>
                        <td><input class="form-control number tarif" type="text" placeholder="Harga" value="<?=$row['tarif_transaksi']?>"></td>
                        <td><input class="form-control discount diskon_persen" type="text" placeholder="Diskon (%)" min="0" max="100" value="<?=$row['diskon_persen']?>"></td>
                        <td><input class="form-control number diskon_rupiah" type="text" placeholder="Diskon (Rp)" value="<?=$row['diskon_rupiah']?>"></td>
                        <td><input class="form-control number total" type="text" placeholder="Total" readonly value="<?=$row['total']?>"></td>
                        <td><?=StatusEditVerifikasi($row['status'])?></td>
                        <td>
                            <select class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="" <?=($group->id == '' ? 'selected':'')?>>Pilih Opsi</option>
                                <?php foreach ($list_group_pembayaran as $group) { ?>
                                <option value="<?=$group->id?>" <?=($group->id == $row['group_pembayaran'] ? 'selected':'')?>><?=$group->nama?></option>
                                <? } ?>
                            </select>
                        </td>
                        <td>
                            <select class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="" <?=($row['keputusan'] == '' ? 'selected':'')?>>Pilih Opsi</option>
                                <option value="1" <?=($row['keputusan'] == 1 ? 'selected':'')?>>Masuk Other Loss</option>
                                <option value="2" <?=($row['keputusan'] == 2 ? 'selected':'')?>>Masuk Other Income</option>
                                <option value="3" <?=($row['keputusan'] == 3 ? 'selected':'')?>>Notice</option>
                                <option value="4" <?=($row['keputusan'] == 4 ? 'selected':'')?>>Ditagihkan ke piutang</option>
                            </select>
                        </td>
                    </tr>
                    <? } ?>
                </tbody>
            </table>
            <button class="btn btn-success" style="float:right" id="updateAll" type="submit">Update</button>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>

<!-- Modal Included -->
<? $this->load->view('Tverifikasi_transaksi/modal/modal_notice_keseluruhan') ?>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');
    $('.discount').number(true, 2, '.', ',');

    $(".tarif").keyup(function() {
        var diskon_persen = $(this).closest('tr').find(".diskon_persen").val();
        var diskon_rupiah = parseFloat($(this).val() * parseFloat(diskon_persen) / 100);

        $(this).closest('tr').find(".diskon_rupiah").val(diskon_rupiah);
        $(this).closest('tr').find(".total").val(parseFloat($(this).val()) - parseFloat(diskon_rupiah));
    });

    $(".diskon_persen").keyup(function() {
        var tarif = $(this).closest('tr').find(".tarif").val();
        var diskon_rupiah = parseFloat(tarif * parseFloat($(this).val()) / 100);

        if (parseFloat($(this).val()) > 100) {
            $(this).val(100);
        }

        $(this).closest('tr').find(".diskon_rupiah").val(diskon_rupiah);
        $(this).closest('tr').find(".total").val(parseFloat(tarif) - parseFloat(diskon_rupiah));
    });

    $(".diskon_rupiah").keyup(function() {
        var tarif = $(this).closest('tr').find(".tarif").val();

        if (parseFloat($(this).val()) == '') {
            $(this).val(0);
        }

        if (parseFloat($(this).val()) > tarif) {
            $(this).val(tarif);
        }

        var diskon_persen = parseFloat((parseFloat($(this).val() * 100)) / tarif);
        $(this).closest('tr').find(".diskon_persen").val(diskon_persen);
        $(this).closest('tr').find(".total").val(parseFloat(tarif) - parseFloat($(this).val()));
    });

    $("#updateAll").click(function() {
        var table = '<?=$table?>';
        var iddetail = '<?=$iddetail?>';
        var idtarif = '<?=$idtarif?>';
        var iddokter = '<?=$iddokter?>';

        var tindakan_tbl = $('table#detailTindakan tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                if ($(cell).find("input").length >= 1) {
                    return $(cell).find("input").val();
                } else if ($(cell).find("select").length >= 1) {
                    return $(cell).find("select").val();
                } else {
                    return $(cell).html();
                }
            });
        });

        $.ajax({
            method: 'POST',
            data: {
                table: table,
                iddetail: iddetail,
                idtarif: idtarif,
                iddokter: iddokter,
                data: JSON.stringify(tindakan_tbl)
            },
            url: '{site_url}tverifikasi_transaksi/noticeTindakan',
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Notice Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function() {
                    location.reload();
                }, 2500);
            }
        });
    });
});

</script>
