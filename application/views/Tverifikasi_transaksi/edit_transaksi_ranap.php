<?=ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}
?>

<style media="screen">
.text-bold {
    font-weight: bold;
}

.separator-header {
    margin-top: 450px;
}

.modal {
    overflow: auto !important;
}

</style>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tverifikasi_transaksi/index/rawatinap" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{nomedrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Alamat Pasien</span>
                    <input type="text" class="form-control tindakanAlamatPasien" value="{alamatpasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Handphone</span>
                    <input type="text" class="form-control tindakanNoHandphone" value="{nohp}" readonly="true">
                </div>
                <hr>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Penanggung Jawab</span>
                    <input type="text" class="form-control tindakanPenanggungJawab" value="{namapenanggungjawab}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Telephone</span>
                    <input type="text" class="form-control tindakanTelpPenanggungJawab" value="{telppenanggungjawab}" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                    <input type="text" class="form-control tindakanKelompokPasien" value="{namakelompok}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
                    <input type="text" class="form-control tindakanNamaPerusahaan" value="{namaperusahaan}" readonly="true">
                </div>
                <hr>
                <?php if ($idtipe == 1) {?>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelas Perawatan</span>
                    <input type="text" class="form-control tindakanNamaKelas" value="{namakelas}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
                    <input type="text" class="form-control tindakanNamaBed" value="{namabed}" readonly="true">
                </div>
                <hr>
                <?php } ?>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
                    <input type="text" class="form-control tindakanNamaDokter" value="{namadokterpenanggungjawab}" readonly="true">
                </div>
            </div>
        </div>

        <div class="content-verification">

            <hr>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 10px;">
                        <label class="col-md-4 control-label" style="margin-top: 5px;">Tipe Pasien</label>
                        <div class="col-md-8">
                            <select id="idtipepasien" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="1" <?=($idtipepasien == 1 ? 'selected' : '')?>>Pasien RS</option>
                                <option value="2" <?=($idtipepasien == 2 ? 'selected' : '')?>>Pasien Pribadi</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <?php $totalRanapRuangan = 0; ?>
            <?php if ($idtipe == 1) {?>
            <b><span class="label label-success" style="font-size:12px">RUANGAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Ruangan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Hari</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody id="tempRuanganRanap">
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapRuanganValidasi($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapRuangan($row->idtarif, $row->idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaldari)?> - <?=DMYFormat($row->tanggalsampai)?></td>
                        <td hidden><?=$row->idtarif?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->idruangan?></td>
                        <td hidden><?=$row->idkelas?></td>
                        <td hidden><?=$row->tanggaldari?></td>
                        <td hidden><?=$row->tanggalsampai?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->total)?></td>
                        <td><?=number_format($row->jumlahhari)?></td>
                        <td><?=number_format($row->total * $row->jumlahhari)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, ($row->total * $row->jumlahhari)); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiRuangRanapModal" data-idrow="<?=$row->id?>" data-table="trawatinap_ruangan" class="btn btn-sm btn-success editDataRuangRanap"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_ruangan/<?=$row->id?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_ruangan/<?=$row->id?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php $totalRanapRuangan = $totalRanapRuangan + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="3"><?=number_format($totalRanapRuangan)?></td>
                    </tr>
                </tfoot>
            </table>
            <?php } ?>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">FULL CARE</span></b>
            <table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapFullCare = 0; ?>
                    <?php $totalRanapFullCareVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapTindakan($row->idtarif, $row->idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idpelayanan?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal" data-idrow="<?=$row->iddetail?>" class="btn btn-sm btn-success editDataTindakan"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapFullCareVerif = $totalRanapFullCareVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapFullCare)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">ECG</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapECG = 0; ?>
                    <?php $totalRanapECGVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapTindakan($row->idtarif, $row->idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idpelayanan?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal" data-idrow="<?=$row->iddetail?>" class="btn btn-sm btn-success editDataTindakan"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapECG = $totalRanapECG + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapECGVerif = $totalRanapECGVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapECG)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">VISITE DOKTER</span></b>
            <table id="historyVisiteDokter" class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:25%">Dokter</th>
                        <th style="width:10%">Ruangan</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapVisite = 0; ?>
                    <?php $totalRanapVisiteVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapVisite($row->idtarif, $row->idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idpelayanan?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=$row->namadokter?></td>
                        <td hidden><?=$row->idruangan?></td>
                        <td><?=$row->namaruangan?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td hidden><?=number_format($row->subtotal)?></td>
                        <td hidden><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiVisiteModal" data-idrow="<?=$row->iddetail?>" class="btn btn-sm btn-success editDataVisiteDokter"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_visite/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_visite/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapVisiteVerif = $totalRanapVisiteVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapVisite)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">SEWA ALAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapSewaAlat = 0; ?>
                    <?php $totalRanapSewaAlatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapTindakan($row->idtarif, $row->idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idpelayanan?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal" data-idrow="<?=$row->iddetail?>" class="btn btn-sm btn-success editDataTindakan"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapSewaAlatVerif = $totalRanapSewaAlatVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapSewaAlat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">AMBULANCE</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapAmbulance = 0; ?>
                    <?php $totalRanapAmbulanceVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapTindakan($row->idtarif, $row->idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idpelayanan?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal" data-idrow="<?=$row->iddetail?>" class="btn btn-sm btn-success editDataTindakan"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapAmbulanceVerif = $totalRanapAmbulanceVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapAmbulance)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapObat = 0; ?>
                    <?php $totalRanapObatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idobat);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunit?></td>
                        <td><?=$row->namaunit?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="trawatinap_obat" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_obat/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_obat/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalRanapObat = $totalRanapObat + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapObatVerif = $totalRanapObatVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiObat = 0; ?>
                    <?php $totalFarmasiObatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idbarang);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <?php if ($row->statusracikan == 0) {?>
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
					$totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiObatVerif = $totalFarmasiObatVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiReturObat = 0; ?>
                    <?php $totalFarmasiReturObatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopengembalian?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_pengembalian_detail/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_pengembalian_detail/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                        </td>
                    </tr>
                    <?php
					$totalFarmasiReturObat = $totalFarmasiReturObat + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiReturObatVerif = $totalFarmasiReturObatVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapAlkes = 0; ?>
                    <?php $totalRanapAlkesVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idalkes);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunit?></td>
                        <td><?=$row->namaunit?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_alkes/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_alkes/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                        </td>
                    </tr>
                    <?php
					$totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapAlkesVerif = $totalRanapAlkesVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiAlkes = 0; ?>
                    <?php $totalFarmasiAlkesVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                        </td>
                    </tr>
                    <?php
					$totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiAlkesVerif = $totalFarmasiAlkesVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiReturAlkes = 0; ?>
                    <?php $totalFarmasiReturAlkesVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopengembalian?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_pengembalian_detail/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_pengembalian_detail/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                        </td>
                    </tr>
                    <?php
					$totalFarmasiReturAlkes = $totalFarmasiReturAlkes + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiReturAlkesVerif = $totalFarmasiReturAlkesVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapAlkes + $totalFarmasiAlkes + $totalFarmasiReturAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT BANTU</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapAlkesBantu = 0; ?>
                    <?php $totalRanapAlkesBantuVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idalkes);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunit?></td>
                        <td><?=$row->namaunit?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="trawatinap_alkes" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_alkes/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_alkes/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                        </td>
                    </tr>
                    <?php
					$totalRanapAlkesBantu = $totalRanapAlkesBantu + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapAlkesBantuVerif = $totalRanapAlkesBantuVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiAlkesBantu = 0; ?>
                    <?php $totalFarmasiAlkesBantuVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkesBantu($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idalkes);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                        </td>
                    </tr>
                    <?php
					$totalFarmasiAlkesBantu = $totalFarmasiAlkesBantu + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiAlkesBantuVerif = $totalFarmasiAlkesBantuVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiReturAlkesBantu = 0; ?>
                    <?php $totalFarmasiReturAlkesBantuVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idalkes);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopengembalian?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_pengembalian_detail/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_pengembalian_detail/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                        </td>
                    </tr>
                    <?php
					$totalFarmasiReturAlkesBantu = $totalFarmasiReturAlkesBantu + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiReturAlkesBantuVerif = $totalFarmasiReturAlkesBantuVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapAlkesBantu + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">LAIN-LAIN</span></b>
            <table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapLainLain = 0; ?>
                    <?php $totalRanapLainLainVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapTindakan($row->idtarif, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idpelayanan?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanRanapModal" data-idrow="<?=$row->iddetail?>" class="btn btn-sm btn-success editDataTindakan"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_tindakan/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRanapLainLainVerif = $totalRanapLainLainVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapLainLain)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoli = 0; ?>
                    <?php $totalPoliVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRajalTindakan($row->idtarif);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idpelayanan?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_pelayanan" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_pelayanan/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_pelayanan/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalPoli = $totalPoli + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalPoliVerif = $totalPoliVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalPoli)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoliObat = 0; ?>
                    <?php $totalPoliObatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran, 2) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunit?></td>
                        <td><?=$row->namaunit?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_obat" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalPoliObat = $totalPoliObat + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalPoliObatVerif = $totalPoliObatVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiObatIGD = 0; ?>
                    <?php $totalFarmasiObatIGDVerif = 0; ?>
                    <?php $dataFarmasiObatIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 3); ?>
                    <?php foreach ($dataFarmasiObatIGD as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <?php $subtotal = $row->hargajual * $row->kuantitas ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <?php if ($row->statusracikan == 0) {?>
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
					$totalFarmasiObatIGD = $totalFarmasiObatIGD + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiObatIGDVerif = $totalFarmasiObatIGDVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>



                    <?php $totalFarmasiRajalReturObat = 0; ?>
                    <?php $totalFarmasiRajalReturObatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalObat($idpendaftaran) as $row) { ?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopengembalian?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php
                        $totalFarmasiRajalReturObat = $totalFarmasiRajalReturObat + $row->totalkeseluruhan;
                        if ($row->statusverifikasi == 1) {
                            $totalFarmasiRajalReturObatVerif = $totalFarmasiRajalReturObatVerif + $row->totalkeseluruhan;
                        }
                        ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoliAlkes = 0; ?>
                    <?php $totalPoliAlkesVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunit?></td>
                        <td><?=$row->namaunit?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_alkes" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_alkes/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_alkes/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalPoliAlkesVerif = $totalPoliAlkesVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiAlkesIGD = 0; ?>
                    <?php $totalFarmasiAlkesIGDVerif = 0; ?>
                    <?php $dataFarmasiAlkesIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 1); ?>
                    <?php foreach ($dataFarmasiAlkesIGD as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <?php $subtotal = $row->hargajual * $row->kuantitas ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <?php if ($row->statusracikan == 0) {?>
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>-->
                            <!--<a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpasien_penjualan_nonracikan/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>-->
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
					$totalFarmasiAlkesIGD = $totalFarmasiAlkesIGD + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFarmasiAlkesIGDVerif = $totalFarmasiAlkesIGDVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>

                    <?php $totalFarmasiRajalReturAlkes = 0; ?>
                    <?php $totalFarmasiRajalReturAlkesVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalAlkes($idpendaftaran) as $row) { ?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopengembalian?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php
                        $totalFarmasiRajalReturAlkes = $totalFarmasiRajalReturAlkes + $row->totalkeseluruhan;
                        if ($row->statusverifikasi == 1) {
                            $totalFarmasiRajalReturAlkesVerif = $totalFarmasiRajalReturAlkesVerif + $row->totalkeseluruhan;
                        }
                        ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalPoliAlkes + $totalFarmasiAlkesIGD + $totalFarmasiRajalReturAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">UMUM</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalLab = 0; ?>
                    <?php $totalLabVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditLaboratorium($row->idlaboratorium, $row->idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idlaboratorium?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_laboratorium_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_laboratorium_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalLab = $totalLab + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalLabVerif = $totalLabVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalLab)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">PATHOLOGI ANATOMI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalLabPA = 0; ?>
                    <?php $totalLabPAVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditLaboratorium($row->idlaboratorium, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idlaboratorium?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_laboratorium_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_laboratorium_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalLabPA = $totalLabPA + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalLabPAVerif = $totalLabPAVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalLabPA)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">PMI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalLabPMI = 0; ?>
                    <?php $totalLabPMIVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditLaboratorium($row->idlaboratorium, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idlaboratorium?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_laboratorium_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_laboratorium_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalLabPMI = $totalLabPMI + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalLabPMIVerif = $totalLabPMIVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalLabPMI)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">X-RAY</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadXray = 0; ?>
                    <?php $totalRadXrayVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRadiologi($row->idradiologi, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idradiologi?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadXray = $totalRadXray + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRadXrayVerif = $totalRadXrayVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadXray)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">USG</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadUSG = 0; ?>
                    <?php $totalRadUSGVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRadiologi($row->idradiologi, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idradiologi?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadUSG = $totalRadUSG + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRadUSGVerif = $totalRadUSGVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadUSG)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">CT SCAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadCTScan = 0; ?>
                    <?php $totalRadCTScanVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRadiologi($row->idradiologi, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idradiologi?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRadCTScanVerif = $totalRadCTScanVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadCTScan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">MRI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadMRI = 0; ?>
                    <?php $totalRadMRIVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRadiologi($row->idradiologi, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idradiologi?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadMRI = $totalRadMRI + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRadMRIVerif = $totalRadMRIVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadMRI)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">BMD</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadBMD = 0; ?>
                    <?php $totalRadBMDVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRadiologi($row->idradiologi, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idradiologi?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_radiologi_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadBMD = $totalRadBMD + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalRadBMDVerif = $totalRadBMDVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadBMD)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FISIOTERAPI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFisio = 0; ?>
                    <?php $totalFisioVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditFisioterapi($row->idtarif, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idfisioterapi?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="trujukan_fisioterapi_detail" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_fisioterapi_detail/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trujukan_fisioterapi_detail/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalFisio = $totalFisio + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalFisioVerif = $totalFisioVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFisio)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">SEWA ALAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKSewaAlat = 0; ?>
                    <?php $totalOKSewaAlatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditSewaAlatOperasi($row->idtarif, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td hidden><?=$row->idtarif?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden>0</td>
                        <td>-</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_sewaalat" class="btn btn-sm btn-success editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_sewaalat/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_sewaalat/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalOKSewaAlatVerif = $totalOKSewaAlatVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKSewaAlat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKAlkes = 0; ?>
                    <?php $totalOKAlkesVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapAlkes($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_alkes" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_alkes/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_alkes/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalOKAlkesVerif = $totalOKAlkesVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKObat = 0; ?>
                    <?php $totalOKObatVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						$margin = $tarif->marginumum;
						if ($row->idkelompokpasien == 1) {
							$margin = $tarif->marginasuransi;
						} elseif ($row->idkelompokpasien == 2) {
							$margin = $tarif->marginjasaraharja;
						} elseif ($row->idkelompokpasien == 3) {
							$margin = $tarif->marginbpjskesehatan;
						} elseif ($row->idkelompokpasien == 4) {
							$margin = $tarif->marginbpjstenagakerja;
						}

						if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_obat" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_obat/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_obat/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalOKObat = $totalOKObat + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalOKObatVerif = $totalOKObatVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT NARCOSE</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKObatNarcose = 0; ?>
                    <?php $totalOKObatNarcoseVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapObat($row->idtarif);
					if ($tarif) {
						if ($tarif->hargadasar != $row->hargadasar) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_narcose" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_narcose/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_narcose/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalOKObatNarcoseVerif = $totalOKObatNarcoseVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKObatNarcose)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">IMPLAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Implan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKImplan = 0; ?>
                    <?php $totalOKImplanVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanaImplan($row->idtarif);
					if ($tarif) {
						if ($tarif->hargadasar != $row->hargadasar) {
							$statusEdit = '1';
						}
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunitpelayanan?></td>
                        <td><?=$row->unitpelayanan?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_implan" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_implan/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_implan/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalOKImplan = $totalOKImplan + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalOKImplanVerif = $totalOKImplanVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKImplan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">SEWA KAMAR</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:20%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKSewaKamar = 0; ?>
                    <?php $totalOKSewaKamarVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditOperasi($row->idtarif, $row->idkelas);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td hidden><?=$row->idtarif?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiKamarOperasiModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_ruangan" class="btn btn-sm btn-success editDataSewaKamarOperasi"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_ruangan/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_ruangan/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalOKSewaKamarVerif = $totalOKSewaKamarVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKSewaKamar)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">JASA DOKTER OPERATOR</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Nama Dokter</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKJasaDokterOperator = 0; ?>
                    <?php $totalOKJasaDokterOperatorVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterOpertor($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditOperasi($row->idtarif, $row->idkelas);
					if ($tarif) {
                        if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
                            $statusEdit = '1';
                        }
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td hidden><?=$row->idtarif?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=$row->namadokter?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiDokterOperatorModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasado" class="btn btn-sm btn-success editDataDokterOperator"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_jasado/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_jasado/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKJasaDokterOperator = $totalOKJasaDokterOperator + $row->totalkeseluruhan;
					if ($row->statusverifikasi == 1) {
						$totalOKJasaDokterOperatorVerif = $totalOKJasaDokterOperatorVerif + $row->totalkeseluruhan;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKJasaDokterOperator)?></td>
                    </tr>
                </tfoot>
            </table>
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">JASA DOKTER ANESTHESI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Nama Dokter</th>
                        <th style="width:10%">Nominal Acuan</th>
                        <th style="width:7%">Persentase (%)</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:8%">Diskon (%)</th>
                        <th style="width:10%">Total Tarif</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKJasaDokterAnesthesi = 0; ?>
                    <?php $totalOKJasaDokterAnesthesiVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran) as $row) {?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td hidden><?=$row->idtarif?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=$row->namadokter?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->persen)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->grandtotal)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiAsistenOperasiModal" data-idrow="<?=$row->iddetail?>" data-table="tkamaroperasi_jasada" class="btn btn-sm btn-success editDataAsistenOperasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_jasada/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tkamaroperasi_jasada/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalOKJasaDokterAnesthesi = $totalOKJasaDokterAnesthesi + $row->grandtotal;
					if ($row->statusverifikasi == 1) {
						$totalOKJasaDokterAnesthesiVerif = $totalOKJasaDokterAnesthesiVerif + $row->grandtotal;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="8"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKJasaDokterAnesthesi)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">JASA ASISTEN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Nama Dokter</th>
                        <th style="width:10%">Nominal Acuan</th>
                        <th style="width:7%">Persentase (%)</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:8%">Diskon (%)</th>
                        <th style="width:10%">Total Tarif</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKJasaAsisten = 0; ?>
                    <?php $totalOKJasaAsistenVerif = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran) as $row) {?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggaloperasi)?></td>
                        <td hidden><?=$row->idtarif?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->idpegawai?></td>
                        <td><?=$row->namapegawai?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->persen)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->grandtotal)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiAsistenOperasiModal" data-idrow="<?=$row->iddetail?>" data-table="<?=($row->namatarif == 'Jasa Asisten Anasthesi' ? 'tkamaroperasi_jasadaa' : 'tkamaroperasi_jasaao')?>" class="btn btn-sm btn-success editDataAsistenOperasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/<=($row->namatarif == 'Jasa Asisten Anasthesi' ? 'tkamaroperasi_jasadaa' : 'tkamaroperasi_jasaao')?>/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/<=($row->namatarif == #notice'Jasa Asisten Anasthesi' ? 'tkamaroperasi_jasadaa' : 'tkamaroperasi_jasaao')?>/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php
					$totalOKJasaAsisten = $totalOKJasaAsisten + $row->grandtotal;
					if ($row->statusverifikasi == 1) {
						$totalOKJasaAsistenVerif = $totalOKJasaAsistenVerif + $row->grandtotal;
					}
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKJasaAsisten)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
            <b><span class="label label-default" style="font-size:12px">RAWAT JALAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:10%">Diskon (%)</th>
                        <th style="width:20%">Tarif Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalAdmRajal = 0; ?>
                    <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRajalTindakan($row->idtarif);
					if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
						$statusEdit = '1';
					}
					?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td hidden><?=$row->idtarif?></td>
                        <td><?=$row->namatarif?></td>
                        <td hidden><?=$row->jasasarana?></td>
                        <td hidden><?=$row->jasasarana_disc?></td>
                        <td hidden><?=$row->jasapelayanan?></td>
                        <td hidden><?=$row->jasapelayanan_disc?></td>
                        <td hidden><?=$row->bhp?></td>
                        <td hidden><?=$row->bhp_disc?></td>
                        <td hidden><?=$row->biayaperawatan?></td>
                        <td hidden><?=$row->biayaperawatan_disc?></td>
                        <td hidden><?=number_format($row->subtotal)?></td>
                        <td hidden><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#ActionEditAdministrasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_administrasi" class="btn btn-sm btn-success editDataAdministrasi"><i class="fa fa-pencil"></i></button>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_administrasi/<?=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a>
                            <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_administrasi/<?=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a>
                        </td>
                    </tr>
                    <?php $totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalAdmRajal)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <?php
			// All
			$totalSebelumAdm = $totalRanapFullCare + $totalRanapECG + $totalRanapVisite + $totalRanapSewaAlat +
			$totalRanapAmbulance + $totalRanapAlkes + $totalRanapAlkesBantu + $totalRanapLainLain + $totalPoli +
			$totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat + $totalPoliAlkes + $totalFarmasiAlkesIGD + $totalFarmasiRajalReturAlkes + $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat + $totalFarmasiAlkes +
			$totalFarmasiReturAlkes + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu + $totalLab + $totalLabPA + $totalLabPMI +
			$totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD +
			$totalFisio + $totalOKSewaAlat + $totalOKAlkes + $totalOKObat + $totalOKObatNarcose +
			$totalOKImplan + $totalOKSewaKamar + $totalOKJasaDokterOperator + $totalOKJasaDokterAnesthesi + $totalOKJasaAsisten +
			$totalRanapRuangan + $totalAdmRajal;

			// Verif Only
			$totalSebelumAdmVerif = $totalRanapFullCareVerif + $totalRanapECGVerif + $totalRanapVisiteVerif + $totalRanapSewaAlatVerif +
			$totalRanapAmbulanceVerif + $totalRanapAlkesVerif + $totalRanapAlkesBantuVerif + $totalRanapLainLainVerif + $totalPoliVerif +
			$totalPoliObatVerif + $totalRanapObatVerif + $totalFarmasiObatIGDVerif + $totalFarmasiRajalReturObatVerif + $totalPoliAlkesVerif + $totalFarmasiAlkesIGDVerif + $totalFarmasiRajalReturAlkesVerif + $totalFarmasiObatVerif + $totalFarmasiReturObatVerif + $totalFarmasiAlkesVerif +
			$totalFarmasiReturAlkesVerif + $totalFarmasiAlkesBantuVerif + $totalFarmasiReturAlkesBantuVerif + $totalLabVerif + $totalLabPAVerif + $totalLabPMIVerif +
			$totalRadXrayVerif + $totalRadUSGVerif + $totalRadCTScanVerif + $totalRadMRIVerif + $totalRadBMDVerif +
			$totalFisioVerif + $totalOKSewaAlatVerif + $totalOKAlkesVerif + $totalOKObatVerif + $totalOKObatNarcoseVerif +
			$totalOKImplanVerif + $totalOKSewaKamarVerif + $totalOKJasaDokterOperatorVerif + $totalOKJasaDokterAnesthesiVerif + $totalOKJasaAsistenVerif +
			$totalRanapRuangan + $totalAdmRajal;
			?>

            <b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
            <b><span class="label label-default" style="font-size:12px">RAWAT INAP</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:10%">Diskon (%)</th>
                        <th style="width:20%">Tarif Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody id="tempAdministrasiRanap">
                    <?php $totalAdmRanap = 0; ?>
                    <?php foreach (get_all('trawatinap_administrasi', ['idpendaftaran' => $idpendaftaran]) as $row) {?>
                    <?php
					$statusEdit = '0';
					$tarif = $this->model->getStatusEditRanapTindakan($row->idadministrasi, $idkelas);
					if ($tarif) {
						if ($tarif->jasasarana != $row->jasasarana || $tarif->jasapelayanan != $row->jasapelayanan || $tarif->bhp != $row->bhp || $tarif->biayaperawatan != $row->biayaperawatan) {
							$statusEdit = '1';
						}
					}
					?>
                    <?php $ranap = get_by_field('id', $row->idpendaftaran, 'trawatinap_pendaftaran')?>
                    <tr>
                        <td><?=$ranap->nopendaftaran?></td>
                        <td><?=DMYFormat($ranap->tanggaldaftar)?></td>
                        <td hidden><?=$row->idadministrasi?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->tarif)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->tarif); ?></td>
                        <td><?=number_format($row->tarifsetelahdiskon)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#ActionEditAdministrasiRanapModal" data-idrow="<?=$row->id?>" data-table="trawatinap_administrasi" class="btn btn-sm btn-success editDataAdministrasiRanap"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_administrasi/<=$row->id?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/trawatinap_administrasi/<=$row->id?>#notice" target="_blank" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right"></i></a> -->
                        </td>
                    </tr>
                    <?php $totalAdmRanap = $totalAdmRanap + $row->tarifsetelahdiskon; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalAdmRanap)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <?php $totalKeseluruhan = $totalSebelumAdm + $totalAdmRanap; ?>
            <?php $totalVerifikasi = $totalSebelumAdmVerif + $totalAdmRanap; ?>
            <?php $totalHarusDibayar = $totalKeseluruhan - $totaldeposit; ?>

            <table class="table table-bordered table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL KESELURUHAN</b></td>
                        <td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px"><?=number_format($totalKeseluruhan)?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL VERIFIKASI</b></td>
                        <td align="left" class="text-bold" id="grandTotalVerifikasi" align="center" style="font-size:15px"><?=number_format($totalVerifikasi)?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL DEPOSIT</b></td>
                        <td align="left" class="text-bold" id="grandTotalDeposit" align="center" style="font-size:15px"><span id="totalDeposit"><?=number_format($totaldeposit)?></span></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL HARUS DIBAYAR</b></td>
                        <td align="left" class="text-bold" id="grandTotalPembayaran" align="center" style="font-size:15px"><?=number_format($totalHarusDibayar)?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div id="transaction-only">
    <div class="block">
        <div class="block-content">
            <h5 style="margin-bottom: 10px;">Pembayaran</h5>
            <table id="cara_pembayaran" class="table table-bordered table-striped" style="margin-top: 20px;">
                <tbody>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_all" name="gt_all" value="<?=number_format($totalKeseluruhan)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>DISKON</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input class="form-control" type="text" id="diskon_rp" name="diskon_rp" placeholder="Discount Rp" value="{diskon_rp}">
							</div>
							<div class="input-group">
								<span class="input-group-addon"> %. </span>
								<input class="form-control" type="text" id="diskon_persen" name="diskon_persen" placeholder="Discount %"  value="{diskon_persen}">
							</div>
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_rp" name="gt_rp" value="<?=number_format($totalKeseluruhan)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL DEPOSIT</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_deposit" name="gt_deposit" value="<?=number_format($totaldeposit)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL HARUS DIBAYAR</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_final" name="gt_final" value="<?=number_format($totalHarusDibayar)?>">
						</th>
					</tr>
					<tr class="bg-light dker">
						<th colspan="5"></th>
						<th colspan="2">
							<button class="btn btn-success btn_pembayaran" id="btn_pembayaran" data-toggle="modal"  data-target="#PembayaranModal"  type="button">Pilih Cara Pembayaran</button>
						</th>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>

                    <div class="alert alert-danger alert-dismissable" id="alertSelisihPembayaran" hidden>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h3 class="font-w300 push-15">Selisih Pembayaran</h3>
                        <p>Terdapat Selisih Pembayaran Rp. <span id="nominalSelisihPembayaran"></span></p>
                    </div>

					<div class="control-group">
                        <?php if ($statuspembayaran) { ?>
                        <div class="row" style="margin-top: 10px; margin-bottom: 25px;">
                            <div class="form-group">
                            <label class="col-md-2 control-label" style="margin-top: 10px;">Tanggal Pembayaran :</label>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-2">
                                            <input type="text" class="js-datepicker form-control" id="tanggal_pembayaran" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="{tanggal_pembayaran}">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="time-datepicker form-control" id="waktu_pembayaran" value="{waktu_pembayaran}">
                                        </div>
                                        <div class="col-md-8" id="message_pembayaran">
                                            <button class="btn btn-success" id="btnUpdateTanggalPembayaran" style="font-size:13px;"><i class="fa fa-filter"></i> Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
							<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 25%;">Jenis Pembayaran</th>
										<th style="width: 35%;">Keterangan</th>
										<th style="width: 10%;">Nominal</th>
										<th style="width: 10%;" class="editOnly">Aksi</th>
									</tr>
								</thead>
								<input type="hidden" id="rowindex">
								<input type="hidden" id="nomor">
								<tbody>
									<?php $no = 1; ?>
									<?php foreach ($detail_pembayaran as $row) {?>
										<tr>
											<td hidden><?=$row->idmetode?></td>
											<td><?=$no++?></td>
											<td><?=metodePembayaran($row->idmetode)?></td>
											<td><?=$row->keterangan?></td>
											<td hidden><?=$row->nominal?></td>
											<td><?=number_format($row->nominal)?></td>
											<td hidden>idkasir</td>
											<td hidden><?=$row->idmetode?></td>
											<td hidden><?=$row->idpegawai?></td>
											<td hidden><?=$row->idbank?></td>
											<td hidden><?=$row->ket_cc?></td>
											<td hidden><?=$row->idkontraktor?></td>
											<td hidden><?=$row->keterangan?></td>
											<td class="editOnly">
                                                <button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>
												&nbsp;&nbsp;
                                                <button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>
											</td>
											<td hidden><?=$row->jaminan?></td>
											<td hidden><?=$row->tracenumber?></td>
                                            <?php if ($row->idmetode == '8') { ?>
                                                <td hidden><?=$row->tipekontraktor?></td>
                                            <?php } else { ?>
                                                <td hidden>0</td>
                                            <?php } ?>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot id="foot_pembayaran">
									<tr class="bg-light dker">
										<th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
                                        <th colspan="2">
                                            <?php if ($total_pembayaran): ?>
                                            <input type="text" class="form-control input-sm " readonly id="bayar_rp" name="bayar_rp" value="<?=$total_pembayaran?>" />
                                            <?php else: ?>
                                            <input type="text" class="form-control input-sm " readonly id="bayar_rp" name="bayar_rp" value="{bayar_rp}" />
                                            <?php endif?>
                                        </th>
                                    </tr>
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Sisa Rp.</b></label></th>
                                        <th colspan="2">
                                            <input type="text" class="form-control input-sm " readonly id="sisa_rp" name="sisa_rp" value="{sisa_rp}" />
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="transaction-only-action" hidden>
            <input type="hidden" id="tempIdRawatInap" name="" value="{idpendaftaran}">
            <input type="hidden" id="pos_tabel_pembayaran" name="pos_tabel_pembayaran">
            <input type="hidden" id="idpendaftaran" value="{idpendaftaran}">
            <div class="modal-footer">
                <div class="buton">
                    <button type="submit" class="btn btn-primary" id="btn_simpan_pembayaran">Simpan Pembayaran</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');
    $('.discount').number(true, 2, '.', ',');

    var statusEdit = '<?=$stedit; ?>';
    if (statusEdit == "0") {
        $(".editOnly").hide();
        $("#btn_pembayaran").hide();
        $("#btnUpdateTanggalPembayaran").hide();
        $("input, select").prop('disabled', true);
    }

    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });

    getDokter();
	setTimeout(function () { 
        checkSelisihPembayaran();
    }, 1000);


    // Tipe Pasien
    $(document).on('change', '#idtipepasien', function() {
        var idpendaftaran = '<?= $idpendaftaran ?>';
        var idtipepasien = $(this).val();

        $.ajax({
            url: '{site_url}tverifikasi_transaksi/updateTipePasien',
            method: 'POST',
            dataType: 'json',
            data: {
                idpendaftaran: idpendaftaran,
                idtipepasien: idtipepasien,
                table: 'trawatinap_pendaftaran'
            },
            success: function(data) {
                if (data.status) {
                    swal({
                        title: 'Berhasil',
                        text: 'Tipe Pasien berhasil diubah.',
                        type: 'success',
                        timer: 1500
                    });
                }
            }
        });
    });

    // Tanggal & Waktu Pembayaran
    $(document).on('click', '#btnUpdateTanggalPembayaran', function() {
        var idpendaftaran = '<?= $idpendaftaran ?>';
        var tanggalPembayaran = $('#tanggal_pembayaran').val();
        var waktuPembayaran = $('#waktu_pembayaran').val();

        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateTanggalPembayaran',
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                tanggal_pembayaran: tanggalPembayaran,
                waktu_pembayaran: waktuPembayaran
            },
            success: function(data) {
                $("#message_pembayaran").html('<div class="alert alert-success" style="padding: 8px 10px 5px 15px;"><p><b>Berhasil!</b> Tanggal Pembayaran Telah di Ubah</p></div>');

                setTimeout(function() {
                    $("#message_pembayaran").html('<button class="btn btn-success" id="btnUpdateTanggalPembayaran" style="font-size:13px;"><i class="fa fa-filter"></i> Simpan</button>');
                }, 3000);
            }
        });
    });

    // Simpan Pembayaran
    $(document).on("click", "#btn_simpan_pembayaran", function(e) {
        e.preventDefault();

        var pos_tabel_pembayaran = $('table#manage_tabel_pembayaran tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            })
        })

        $("#pos_tabel_pembayaran").val(JSON.stringify(pos_tabel_pembayaran));

        $.ajax({
            url: '{site_url}trawatinap_tindakan/simpan_transaksi',
            method: 'POST',
            dataType: 'json',
            data: {
                idtindakan: '<?php echo $this->uri->segment(3) ?>',
                idpendaftaran: $('#idpendaftaran').val(),
                subtotal: $('#gt_all').val(),
                diskon_rp: $('#diskon_rp').val(),
                diskon_persen: $('#diskon_persen').val(),
                deposit: $('#gt_deposit').val(),
                total: $('#gt_rp').val(),
                totalharusdibayar: $('#gt_final').val(),
                pembayaran: $('#bayar_rp').val(),
                sisa: $('#sisa_rp').val(),
                pos_tabel_pembayaran: JSON.stringify(pos_tabel_pembayaran),
            },
            success: function(data) {
                if (data.status == 200) {
                    swal({
                        title: "Berhasil!",
                        text: "Pembayaran Telah Tersimpan.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    location.reload();
                } else {
                    alert('data gagal disimpan');
                }
            }
        })
    });
});

function getDokter() {
    $.ajax({
        url: "{site_url}trawatinap_tindakan/getDokterPJ",
        dataType: "json",
        success: function(data) {
            $("#tTindakanIdDokter").empty();
            $("#tTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#tTindakanIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
            }

            $("#tTindakanGlobalIdDokter").empty();
            $("#tTindakanGlobalIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#tTindakanGlobalIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
            }

            $("#tVisiteIdDokter").empty();
            $("#tVisiteIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#tVisiteIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
            }

            $("#fVisiteIdDokter").empty();
            $("#fVisiteIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#fVisiteIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
            }

            $("#fTindakanIdDokter").empty();
            $("#fTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#fTindakanIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
            }
        }
    });
}

function checkSelisihPembayaran() {
    const totalKeseluruhan = Math.ceil($("#gt_final").val() / 100) * 100;
    const nominalPembayaran = $("#bayar_rp").val();
    const nominalSelisih = totalKeseluruhan - nominalPembayaran;
    
    if (nomnialSelisih > 0) {
        $("#alertSelisihPembayaran").show();
        $("#nominalSelisihPembayaran").html($.number(nominalSelisih));
    } else {
        $("#alertSelisihPembayaran").hide();
    }
}

</script>

<!-- Modal Included -->
<?php $this->load->view('Trawatinap_tindakan/modal/modal_action_tindakan') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_action_visite') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_administrasi') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_administrasi_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_deposit') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_kamar_ok') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_operator_ok') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_asisten_ok') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_ruangan_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_tindakan_farmasi') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_tindakan_global') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_tindakan_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_visite_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_pembayaran') ?>
