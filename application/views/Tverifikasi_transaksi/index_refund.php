<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>
<div id="cover-spin"></div>
<!-- Icon Tiles -->
<h2 class="content-heading">Jenis Pasien</h2>
<div class="content-grid push-50">
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index/poliklinik">
                <div class="block-content block-content-full bg-primary-dark">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Poliklinik & IGD</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index/rawatinap">
                <div class="block-content block-content-full bg-primary-dark">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Rawat Inap & ODS</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index_deposit">
                <div class="block-content block-content-full bg-primary-dark">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Deposit Perawatan</div>
                </div>
            </a>
        </div>
		<div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index_refund">
                <div class="block-content block-content-full bg-success">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Refund Tunai</div>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- END Icon Tiles -->

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr>
        <div class="row">
            <?php echo form_open('tverifikasi_transaksi/filter_refund', 'class="form-horizontal" id="form-filter"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tanggal</label>
                    <div class="col-md-8">
                      <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
          							<input class="form-control" type="text" name="tanggal_awal" placeholder="From" value="{tanggal_awal}">
          							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
          							<input class="form-control" type="text" name="tanggal_akhir" placeholder="To" value="{tanggal_akhir}">
          						</div>
        						</div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
        					<label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
        					<div class="col-md-8">
        						<input type="text" class="form-control" name="nomedrec" value="{nomedrec}" placeholder="No. Medrec">
        					</div>
        				</div>
        				<div class="form-group" style="margin-bottom: 5px;">
        					<label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
        					<div class="col-md-8">
        						<input type="text" class="form-control" name="namapasien" value="{namapasien}" placeholder="Nama Pasien">
        					</div>
        				</div>

               
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                  <label class="col-md-4 control-label" for="idkelompokpasien">User Transaksi</label>
                  <div class="col-md-8">
                    <select name="iduserinput" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." >
                      <option value="0">Semua User</option>
                      <?php foreach(get_all('musers', array('status' => 1)) as $row) { ?>
                        <option value="<?=$row->id?>" <?=($iduserinput == $row->id ? 'selected' : '')?>><?=$row->name?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
               
                <div class="form-group" style="margin-bottom: 5px;">
                  <label class="col-md-4 control-label" for="iddokter">Tipe Refund </label>
                  <div class="col-md-8">
                    <select name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                      <option value="#" <?=($tipe == '#' ? 'selected="selected"':'')?>>Semua Tipe</option>
                      <option value="0" <?=($tipe == '0' ? 'selected="selected"':'')?>>Deposit</option>
                      <option value="1" <?=($tipe == '1' ? 'selected="selected"':'')?>>Pengembalilan Obat</option>
                      <option value="2" <?=($tipe == '2' ? 'selected="selected"':'')?>>Transaksi</option>
                    </select>
                  </div>
                </div>
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>

        <form class="" action="{base_url}tverifikasi_transaksi/updateStatusAll" method="post">
          <hr>
          <input type="hidden" name="tipetransaksi" value="refund">
          <button href="#" class="btn btn-primary" id="verifyAll" style="display:none;" type="submit"><i class="fa fa-check"></i> Proses Verifikasi</button>
          <hr>

          <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		  <div class="table-responsive">
          <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
              <thead>
              <tr>
                  <th>No</th>
					<th>Tanggal & Waktu</th>
					<th>No Refund</th>
					<th>No Transaksi</th>
					<th>Tipe</th>
					<th>No Medrec</th>
					<th>Nama Pasien</th>
					<th>Nominal</th>
					<th width="9%">Aksi</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
    </div>
        </form>
        <br>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}autoNumeric/autoNumeric.min.js"></script>
<script type="text/javascript">
// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tverifikasi_transaksi/getIndexRefund/<?=$tipetransaksi?>',
            type: "POST",
            dataType: 'json'
        },
       columnDefs: [
					// { "targets": [0,1], "visible": false },
					{ "width": "3%", "targets": [0] },
					{ "width": "7%", "targets": [1,2,3,4,5,7] },
					{ "width": "8%", "targets": [7] },
					{ "width": "12%", "targets": [6,8] },
					{"targets": [0,7], className: "text-right" },
					{"targets": [1,2,3,4,5], className: "text-center" }
					
				 ],
    });
});

$(document).ready(function () {
    $('#checkAll').click(function(event) {
      if(this.checked) {
          $('.checkboxTrx').each(function() {
              this.checked = true;
              $('#verifyAll').show();
          });
      } else {
          $('.checkboxTrx').each(function() {
              this.checked = false;
              $('#verifyAll').hide();
          });
      }
    });

    $(document).on('click', '.checkboxTrx', function () {
      if($(".checkboxTrx:checked").length > 0) {
        $('#verifyAll').show();
      } else {
        $('#verifyAll').hide();
      }
    });
	$(document).on('click', '.loading', function() {
       $("#cover-spin").show();
    });
});
</script>
