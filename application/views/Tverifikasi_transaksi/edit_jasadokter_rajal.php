<?= ErrorSuccess($this->session); ?>
<?php if ($error != '') {
	echo ErrorMessage($error);
} ?>

<style media="screen">
.text-bold {
    font-weight: bold;
}

.separator-header {
    margin-top: 450px;
}

.modal {
    overflow: auto !important;
}

</style>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tverifikasi_transaksi/index/poliklinik" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Pendaftaran</span>
                    <input type="text" class="form-control tindakanNoPendaftaran" value="{nopendaftaran}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{nomedrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tipe</span>
                    <input type="text" class="form-control tindakanTipePendaftaran" value="{tipependaftaran}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Poliklinik</span>
                    <input type="text" class="form-control tindakanPoliklinik" value="{poliklinik}" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                    <input type="text" class="form-control tindakanKelompokPasien" value="{kelompokpasien}" readonly="true">
                </div>
                <?if ($idkelompokpasien == '3') {?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">BPJS Kesehatan</span>
                    <input type="text" class="form-control tindakanBPJSKesehatan" value="{bpjs_kesehatan}" readonly="true">
                </div>
                <?}?>
                <?if ($idkelompokpasien == '4') {?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">BPJS Ketenagakerjaan</span>
                    <input type="text" class="form-control tindakanBPJSKetenagakerjaan" value="{bpjs_ketenagakerjaan}" readonly="true">
                </div>
                <?}?>
                <?if ($idkelompokpasien == '1') {?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Asuransi</span>
                    <input type="text" class="form-control tindakanAsuransi" value="{namarekanan}" readonly="true">
                </div>
                <?}?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">User Transaksi</span>
                    <input type="text" class="form-control tindakanUserTransaksi" value="{usertransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Transaksi</span>
                    <input type="text" class="form-control tindakanTanggalTransaksi" value="{tanggaltransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
                    <input type="text" class="form-control tindakanDokter" value="{namadokter}" readonly="true">
                </div>
            </div>
        </div>

        <div class="content-verification">

            <hr>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 10px;">
                        <label class="col-md-4 control-label" style="margin-top: 5px;">Tipe Pasien</label>
                        <div class="col-md-8">
                            <select id="idtipepasien" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="1" <?=($idtipepasien == 1 ? 'selected' : '')?>>Pasien RS</option>
                                <option value="2" <?=($idtipepasien == 2 ? 'selected' : '')?>>Pasien Pribadi</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RAWAT JALAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganPoliklinikPelayanan = 0; ?>
                    <?php $PajakPoliklinikPelayanan = 0; ?>
                    <?php $NettoPoliklinikPelayanan = 0; ?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalTindakan($id) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'tpoliklinik_pelayanan',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganPoliklinikPelayanan = $PotonganPoliklinikPelayanan + $nominal_potongan_rs; ?>
                    <?php $PajakPoliklinikPelayanan = $PajakPoliklinikPelayanan + $nominal_pajak_dokter; ?>
                    <?php $NettoPoliklinikPelayanan = $NettoPoliklinikPelayanan + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganPoliklinikPelayanan)?></td>
                        <td class="text-bold"><?=number_format($PajakPoliklinikPelayanan)?></td>
                        <td class="text-bold"><?=number_format($NettoPoliklinikPelayanan)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER LABORATORIUM</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganLaboratorium = 0; ?>
                    <?php $PajakLaboratorium = 0; ?>
                    <?php $NettoLaboratorium = 0; ?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianLaboratorium($id) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_laboratorium_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganLaboratorium = $PotonganLaboratorium + $nominal_potongan_rs; ?>
                    <?php $PajakLaboratorium = $PajakLaboratorium + $nominal_pajak_dokter; ?>
                    <?php $NettoLaboratorium = $NettoLaboratorium + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganLaboratorium)?></td>
                        <td class="text-bold"><?=number_format($PajakLaboratorium)?></td>
                        <td class="text-bold"><?=number_format($NettoLaboratorium)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER RADIOLOGI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganRadiologi = 0; ?>
                    <?php $PajakRadiologi = 0; ?>
                    <?php $NettoRadiologi = 0; ?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($id) as $row) {?>
                    <?php if ($row->jasamedis > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_radiologi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => $row->status_tindakan,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganRadiologi = $PotonganRadiologi + $nominal_potongan_rs; ?>
                    <?php $PajakRadiologi = $PajakRadiologi + $nominal_pajak_dokter; ?>
                    <?php $NettoRadiologi = $NettoRadiologi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganRadiologi)?></td>
                        <td class="text-bold"><?=number_format($PajakRadiologi)?></td>
                        <td class="text-bold"><?=number_format($NettoRadiologi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">JASA DOKTER FISIOTERAPI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">Tindakan</th>
                        <th style="width:10%">Dokter</th>
                        <th style="width:10%">Jasa Medis</th>
                        <th style="width:10%">Potongan RS (%)</th>
                        <th style="width:10%">Pajak Dokter (%)</th>
                        <th style="width:10%">Potongan RS (Rp)</th>
                        <th style="width:10%">Pajak Dokter (Rp)</th>
                        <th style="width:10%">Jasa Medis (Netto)</th>
                        <th style="width:5%">Periode Pembayaran</th>
                        <th style="width:5%">Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $PotonganFisioterapi = 0; ?>
                    <?php $PajakFisioterapi = 0; ?>
                    <?php $NettoFisioterapi = 0; ?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFisioterapi($id) as $row) {?>
                    <?php if ($row->jasapelayanan > 0) { ?>
                    <?php $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100); ?>
                    <?php $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100); ?>
                    <?php $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter; ?>
                    <?php $periode_pembayaran = ($row->periode_pembayaran != '' ? DMYFormat($row->periode_pembayaran) : getPembayaranHonorDokter($row->iddokter)); ?>
                    <?php $periode_jatuhtempo = ($row->periode_jatuhtempo != '' ? DMYFormat($row->periode_jatuhtempo) : getJatuhTempoHonorDokter($row->iddokter)); ?>

                    <?php
						$dataModal = [
							'table' => 'trujukan_fisioterapi_detail',
							'idrawatinap' => $id,
							'iddetail' => $row->iddetail,
							'idtarif' => $row->idtarif,
							'namatarif' => $row->namatarif,
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'jasamedis' => $row->jasamedis,
							'potongan_rs' => $row->potongan_rs,
							'pajak_dokter' => $row->pajak_dokter,
							'nominal_potongan_rs' => $nominal_potongan_rs,
							'nominal_pajak_dokter' => $nominal_pajak_dokter,
							'jasamedis_netto' => $jasamedis_netto,
							'periode_pembayaran' => DMYFormat($periode_pembayaran),
							'periode_jatuhtempo' => DMYFormat($periode_jatuhtempo),
							'status_expertise' => 0,
							'status_verifikasi' => $status_verifikasi,
						];
					?>

                    <tr>
                        <td><?=$row->namatarif?></td>
                        <td><?=($row->namadokter != null ? $row->namadokter : '-')?></td>
                        <td><?=number_format($row->jasamedis)?></td>
                        <td><?=number_format($row->potongan_rs, 2)?></td>
                        <td><?=number_format($row->pajak_dokter, 2)?></td>
                        <td><?=number_format($nominal_potongan_rs)?></td>
                        <td><?=number_format($nominal_pajak_dokter)?></td>
                        <td><?=number_format($jasamedis_netto)?></td>
                        <td><?=$periode_pembayaran?></td>
                        <td><?=StatusEditVerifikasi($row->status_jasamedis)?></td>
                        <td>
                            <button data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>" class="btn btn-sm btn-success editJasaMedis"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                    <?php $PotonganFisioterapi = $PotonganFisioterapi + $nominal_potongan_rs; ?>
                    <?php $PajakFisioterapi = $PajakFisioterapi + $nominal_pajak_dokter; ?>
                    <?php $NettoFisioterapi = $NettoFisioterapi + $jasamedis_netto; ?>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($PotonganFisioterapi)?></td>
                        <td class="text-bold"><?=number_format($PajakFisioterapi)?></td>
                        <td class="text-bold"><?=number_format($NettoFisioterapi)?></td>
                        <td class="text-bold" colspan="3"></td>
                    </tr>
                </tfoot>
            </table>

            <?php
				$totalPotonganRs = $PotonganPoliklinikPelayanan + $PotonganRadiologi + $PotonganLaboratorium + $PotonganFisioterapi;
				$totalPajakDokter = $PajakPoliklinikPelayanan + $PajakRadiologi + $PajakLaboratorium + $PajakFisioterapi;
			?>

            <hr>

            <table class="table table-bordered table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL POTONGAN RS</b></td>
                        <td align="left" class="text-bold" align="center" style="font-size:15px"><?=number_format($totalPotonganRs)?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL PAJAK DOKTER</b></td>
                        <td align="left" class="text-bold" align="center" style="font-size:15px"><?=number_format($totalPajakDokter)?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');
    $('.discount').number(true, 2, '.', ',');

    // Tipe Pasien
    $(document).on('change', '#idtipepasien', function() {
        var idpendaftaran = '<?=$id?>';
        var idtipepasien = $(this).val();

        $.ajax({
            url: '{site_url}tverifikasi_transaksi/updateTipePasien',
            method: 'POST',
            dataType: 'json',
            data: {
                idpendaftaran: idpendaftaran,
                idtipepasien: idtipepasien,
                table: 'tpoliklinik_pendaftaran'
            },
            success: function(data) {
                if (data.status) {
                    swal({
                        title: 'Berhasil',
                        text: 'Tipe Pasien berhasil diubah.',
                        type: 'success',
                        timer: 1500
                    });
                }
            }
        });
    });
});

</script>

<!-- Modal Included -->
<?php $this->load->view('Tverifikasi_transaksi/modal/modal_jasa_dokter') ?>
