<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') {
	echo ErrorMessage($error);
} ?>
<div id="cover-spin"></div>
<!-- Icon Tiles -->
<h2 class="content-heading">Jenis Pasien</h2>
<div class="content-grid push-50">
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index/poliklinik">
                <div class="block-content block-content-full <?= ($tipetransaksi == 'poliklinik' ? 'bg-success' : 'bg-primary-dark') ?>">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Poliklinik & IGD</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index/rawatinap">
                <div class="block-content block-content-full <?= ($tipetransaksi == 'rawatinap' ? 'bg-success' : 'bg-primary-dark') ?>">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Rawat Inap & ODS</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index_deposit">
                <div class="block-content block-content-full <?= ($this->uri->segment(2) == 'index_deposit' ? 'bg-success' : 'bg-primary-dark') ?>">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Deposit Perawatan</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index_refund">
                <div class="block-content block-content-full <?= ($this->uri->segment(2) == 'index_refund' ? 'bg-success' : 'bg-primary-dark') ?>">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Refund Tunai</div>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- END Icon Tiles -->

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr>
        <div class="row">
            <?php echo form_open('tverifikasi_transaksi/filter/{tipetransaksi}', 'class="form-horizontal" id="form-filter"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tgl Pendaftaran</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pendaftaran_awal" placeholder="From" value="{tanggal_pendaftaran_awal}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pendaftaran_akhir" placeholder="To" value="{tanggal_pendaftaran_akhir}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tgl Pembayaran</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pembayaran_awal" placeholder="From" value="{tanggal_pembayaran_awal}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pembayaran_akhir" placeholder="To" value="{tanggal_pembayaran_akhir}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tipe</label>
                    <div class="col-md-8">
                        <select name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Tipe</option>
                            <?php if ($tipetransaksi == 'poliklinik') { ?>
                            <option value="1" <?=($idtipe == 1 ? 'selected="selected"' : '')?>>Poliklinik</option>
                            <option value="2" <?=($idtipe == 2 ? 'selected="selected"' : '')?>>Instalasi Gawat Darurat (IGD)</option>
                            <option value="3" <?=($idtipe == 3 ? 'selected="selected"' : '')?>>Obat Bebas</option>
                            <?php } elseif ($tipetransaksi == 'rawatinap') { ?>
                            <option value="1" <?=($idtipe == 1 ? 'selected="selected"' : '')?>>Rawat Inap</option>
                            <option value="2" <?=($idtipe == 2 ? 'selected="selected"' : '')?>>One Day Surgery (ODS)</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="idpoliklinik">Poliklinik</label>
                    <div class="col-md-8">
                        <select name="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Poliklinik</option>
                            <?php foreach (get_all('mpoliklinik', ['status' => '1']) as $row) {?>
                            <option value="<?= $row->id?>" <?=($idpoliklinik == $row->id ? 'selected="selected"' : '')?>><?= $row->nama?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nomedrec" value="{nomedrec}" placeholder="No. Medrec">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}" placeholder="Nama Pasien">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <input type="hidden" class="form-control" name="tipetransaksi" value="{tipetransaksi}">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idkelompokpasien">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="0">Semua Kelompok Pasien</option>
                            <?php foreach (get_all('mpasien_kelompok', ['status' => 1]) as $row) { ?>
                            <option value="<?=$row->id?>" <?=($idkelompokpasien == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Perusahaan</label>
                    <div class="col-md-8">
                        <select name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Perusahaan</option>
                            <?php foreach (get_all('mrekanan') as $row) {?>
                            <option value="<?= $row->id?>" <?=($idrekanan == $row->id ? 'selected="selected"' : '')?>><?= $row->nama?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokter">User Transaksi</label>
                    <div class="col-md-8">
                        <select name="iduser" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua User</option>
                            <?php foreach (get_all('musers', ['st_kasir' => 1, 'status' => 1]) as $row) { ?>
                            <option value="<?= $row->id?>" <?=($iduser == $row->id ? 'selected="selected"' : '')?>><?= $row->name?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokter">Dokter</label>
                    <div class="col-md-8">
                        <select name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Dokter</option>
                            <?php foreach (get_all('mdokter', ['status' => 1]) as $row) {?>
                            <option value="<?= $row->id?>" <?=($iddokter == $row->id ? 'selected="selected"' : '')?>><?= $row->nama?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nopendaftaran">No. Pendaftaran</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nopendaftaran" value="{nopendaftaran}" placeholder="No. Pendaftaran">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nokasir">No. Kasir</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nokasir" value="{nokasir}" placeholder="No. Kasir">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <a href="#" class="btn btn-primary text-uppercase" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Print</a>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>

        <form class="" action="{base_url}tverifikasi_transaksi/updateStatusAll" method="post">
            <hr>
            <input type="hidden" name="tipetransaksi" value="{tipetransaksi}">
            <button href="#" class="btn btn-primary" id="verifyAll" style="display:none;" type="submit"><i class="fa fa-check"></i> Proses Verifikasi</button>
            <hr>
			<div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>
                            <label class="css-input css-checkbox css-checkbox-success">
                                <input type="checkbox" id="checkAll"><span></span>
                            </label>
                        </th>
                        <th>Tanggal</th>
                        <th>No. Pendaftaran</th>
                        <th>Tipe</th>
                        <th>No. Medrec</th>
                        <th>Nama Pasien</th>
                        <th>Jumlah</th>
                        <th>Status Edit</th>
                        <th>Status Verifikasi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>
        </form>
        <br>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tverifikasi_transaksi/getIndex/{uri}/{tipetransaksi}',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": false
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "20%",
                "targets": 9,
                "orderable": true
            }
        ],
    });
});

$(document).ready(function() {
    $('#checkAll').click(function(event) {
        if (this.checked) {
            $('.checkboxTrx').each(function() {
                this.checked = true;
                $('#verifyAll').show();
            });
        } else {
            $('.checkboxTrx').each(function() {
                this.checked = false;
                $('#verifyAll').hide();
            });
        }
    });

    $(document).on('click', '.loading', function() {
       $("#cover-spin").show();
    });
	$(document).on('click', '.checkboxTrx', function() {
        if ($(".checkboxTrx:checked").length > 0) {
            $('#verifyAll').show();
        } else {
            $('#verifyAll').hide();
        }
    });
});

</script>
