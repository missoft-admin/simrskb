<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>

<!-- Icon Tiles -->
<h2 class="content-heading">Jenis Pasien</h2>
<div class="content-grid push-50">
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index/poliklinik">
                <div class="block-content block-content-full bg-primary-dark">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Poliklinik & IGD</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index/rawatinap">
                <div class="block-content block-content-full bg-primary-dark">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Rawat Inap & ODS</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index_deposit">
                <div class="block-content block-content-full bg-success">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Deposit Perawatan</div>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-4 col-lg-2">
            <a class="block block-link-hover2 text-center" href="{base_url}tverifikasi_transaksi/index_refund">
                <div class="block-content block-content-full <?= ($this->uri->segment(2) == 'index_refund' ? 'bg-success' : 'bg-primary-dark') ?>">
                    <i class="si si-user fa-4x text-white"></i>
                    <div class="font-w600 text-white-op push-15-t">Refund Tunai</div>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- END Icon Tiles -->

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr>
        <div class="row">
            <?php echo form_open('tverifikasi_transaksi/filter_deposit', 'class="form-horizontal" id="form-filter"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggal_awal" placeholder="From" value="{tanggal_awal}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggal_akhir" placeholder="To" value="{tanggal_akhir}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nomedrec" value="{nomedrec}" placeholder="No. Medrec">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}" placeholder="Nama Pasien">
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idkelompokpasien">User Transaksi</label>
                    <div class="col-md-8">
                        <select name="iduserinput" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="0">Semua User</option>
                            <?php foreach(get_all('musers', array('status' => 1)) as $row) { ?>
                            <option value="<?=$row->id?>" <?=($iduserinput == $row->id ? 'selected' : '')?>><?=$row->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Metode Pembayaran</label>
                    <div class="col-md-8">
                        <select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Metode Pembayaran</option>
                            <option value="1">Tunai</option>
                            <option value="2">Debit</option>
                            <option value="3">Kartu Kredit</option>
                            <option value="4">Transfer</option>
                            <option value="5">Tagihan Karyawan (Pegawai)</option>
                            <option value="6">Tagihan Karyawan (Dokter)</option>
                            <option value="7">Tidak Tertagihkan</option>
                            <option value="8">Kontraktor</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokter">Bank</label>
                    <div class="col-md-8">
                        <select name="iduser" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Bank</option>
                            <?php foreach (get_all('mbank', array('status' => 1)) as $row){ ?>
                            <option value="<?= $row->id?>" <?=($idbank == $row->id ? 'selected="selected"':'')?>><?= $row->nama?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokter">Tipe</label>
                    <div class="col-md-8">
                        <select name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Tipe</option>
                            <option value="1" <?=($idtipe == 1 ? 'selected="selected"':'')?>>Rawat Inap</option>
                            <option value="2" <?=($idtipe == 2 ? 'selected="selected"':'')?>>One Day Surgery (ODS)</option>
                        </select>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>

        <form class="" action="{base_url}tverifikasi_transaksi/updateStatusAll" method="post">
            <hr>
            <input type="hidden" name="tipetransaksi" value="deposit">
            <button href="#" class="btn btn-primary" id="verifyAll" style="display:none;" type="submit"><i class="fa fa-check"></i> Proses Verifikasi</button>
            <hr>

            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>
                            <label class="css-input css-checkbox css-checkbox-success">
                                <input type="checkbox" id="checkAll"><span></span>
                            </label>
                        </th>
                        <th>Tanggal</th>
                        <th>No. Medrec</th>
                        <th>Nama Pasien</th>
                        <th>Tipe</th>
                        <th>Metode Pembayaran</th>
                        <th>Bank</th>
                        <th>Nominal</th>
                        <th>User Transaksi</th>
                        <th>Status Verifikasi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>
        </form>
        <br>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<? $this->load->view('Tverifikasi_transaksi/modal/modal_deposit_header')?>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tverifikasi_transaksi/getIndexDeposit/<?=$tipetransaksi?>',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 9,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 10,
                "orderable": true
            }
        ],
    });
});

$(document).ready(function() {
    $('#checkAll').click(function(event) {
        if (this.checked) {
            $('.checkboxTrx').each(function() {
                this.checked = true;
                $('#verifyAll').show();
            });
        } else {
            $('.checkboxTrx').each(function() {
                this.checked = false;
                $('#verifyAll').hide();
            });
        }
    });

    $(document).on('click', '.checkboxTrx', function() {
        if ($(".checkboxTrx:checked").length > 0) {
            $('#verifyAll').show();
        } else {
            $('#verifyAll').hide();
        }
    });
});

</script>
