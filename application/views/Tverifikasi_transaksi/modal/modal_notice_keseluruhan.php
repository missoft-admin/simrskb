<div class="modal in" id="ActionNoticeModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" style="width:60%">
        <div class="modal-content">
            <div class="block block-themed" style="margin-bottom:0">
                <div class="block-header bg-danger">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">NOTICE TRANSAKSI</h3>
                </div>
                <div class="block-content">

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="headerpath" style="margin-top: 5px">Tarif Seharusnya</label>
                        <div class="col-md-10">
                            <select id="idtarif" class="js-select2 form-control" disabled style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0"><?=htmlspecialchars($namatarif)?></option>
                            </select>
                        </div>
                    </div>

                    <br><br>

                    <table class="table table-bordered table-striped" id="detailTransaksi">
                        <thead>
                            <tr>
                                <th>Item Biaya</th>
                                <th>Harga</th>
                                <th>Nominal Real</th>
                                <th>Selisih</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_selisih = 0; ?>
                            <?php foreach ($itemtindakan as $row) { ?>
                            <?php $selisih = $row['tarif_transaksi'] - $row['tarif_master']; ?>
                            <tr>
                                <td><?=$row['nama']?></td>
                                <td><?=number_format($row['tarif_master'])?></td>
                                <td><input class="form-control number nominal" type="text" placeholder="Nominal Real" value="<?=$row['tarif_transaksi']?>"></td>
                                <td><input class="form-control number selisih" type="text" placeholder="Selisih" readonly value="<?=$selisih?>"></td>
                            </tr>
                            <?php $total_selisih += $selisih; ?>
                            <? } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3"><b>Total Kurang / Lebih</b></td>
                                <td><b id="totalselisih"><?=number_format($total_selisih)?></b></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Keputusan</b></td>
                                <td>
                                    <select id="idkeputusan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                        <option value="1">Masuk Other Loss</option>
                                        <option value="2">Masuk Other Income</option>
                                        <option value="3">Notice</option>
                                        <option value="4">Ditagihkan ke piutang</option>
                                    </select>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-success" id="saveDataNotice" type="button">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    if (window.location.hash == '#notice') {
        $("#ActionNoticeModal").modal();
    }

    $('#ActionNoticeModal').on('shown.bs.modal', function() {
        window.location.hash = '#notice';
    })

    $('#ActionNoticeModal').on('hidden.bs.modal', function() {
        window.location.hash = '';
    })

    $(".nominal").keyup(function() {
        var harga = $(this).closest('tr').find("td:eq(1)").html().replace(/,/g, '');
        var selisih = parseFloat($(this).val()) - parseFloat(harga);
        $(this).closest('tr').find(".selisih").val(selisih);

        calculateTotal();
    });

    $("#saveDataNotice").click(function() {
        var table = '<?=$table?>';
        var iddetail = '<?=$iddetail?>';
        var idtarif = '<?=$idtarif?>';
        var idkeputusan = $('#idkeputusan option:selected').val();

        var tindakan_tbl = $('table#detailTransaksi tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                if ($(cell).find("input").length >= 1) {
                    return $(cell).find("input").val();
                } else if ($(cell).find("select").length >= 1) {
                    return $(cell).find("select").val();
                } else {
                    return $(cell).html();
                }
            });
        });

        $.ajax({
            method: 'POST',
            data: {
                table: table,
                iddetail: iddetail,
                idtarif: idtarif,
                idkeputusan: idkeputusan,
                data: JSON.stringify(tindakan_tbl)
            },
            url: '{site_url}tverifikasi_transaksi/noticeAllTindakan',
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Notice Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function() {
                    location.reload();
                }, 2500);
            }
        });
    });
});

function calculateTotal() {
    var totalreal = 0;
    var totaladjusment = 0;
    $('#detailTransaksi tbody tr').each(function() {
        totalreal += parseFloat($(this).find('td:eq(2) input').val().replace(/\,/g, ""));
        totaladjusment += parseFloat($(this).find('td:eq(3) input').val().replace(/\,/g, ""));
    });

    var totalselisih = parseFloat(totalreal) - parseFloat(totaladjusment);
    $("#totalselisih").html($.number(totalselisih));
}

</script>
