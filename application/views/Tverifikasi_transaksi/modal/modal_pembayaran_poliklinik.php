<!-- Modal Transaksi Pembayaran -->
<div class="modal fade in black-overlay" id="PembayaranModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Pembayaran</h3>
                </div>
                <div class="block-content block-content-narrow">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="idmetode">Metode</label>
                            <div class="col-md-7">
                                <select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="">Pilih Opsi</option>
                                    <option value="1">Tunai</option>
                                    <option value="2">Debit</option>
                                    <option value="3">Kartu Kredit</option>
                                    <option value="4">Transfer</option>
                                    <option value="5">Tagihan Karyawan (Pegawai)</option>
                                    <option value="6">Tagihan Karyawan (Dokter)</option>
                                    <option value="7">Tidak Tertagihkan</option>
                                    <option value="8">Kontraktor</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
                            <div class="col-md-7">
                                <input readonly type="text" class="form-control" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group" id="div_bank" hidden>
                            <label class="col-md-3 control-label" for="idbank">Bank</label>
                            <div class="col-md-7">
                                <select name="idbank" id="idbank" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="">Pilih Opsi</option>
                                    <?php foreach ($list_bank as $row) {?>
                                    <option value="<?=$row->id?>"><?=$row->nama . ' (' . $row->atasnama . ')'?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="div_cc" hidden>
                            <label class="col-md-3 control-label" for="nama">Ket. CC </label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="ket_cc" placeholder="Ket. CC" name="ket_cc" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group" id="div_trace" hidden>
                            <label class="col-md-3 control-label" for="nama">Trace Number</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="trace_number" placeholder="Trace Number" name="trace_number" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group" id="div_kontraktor" hidden>
                            <label class="col-md-3 control-label" for="tipekontraktor">Tipe Kontraktor </label>
                            <div class="col-md-7">
                                <select name="tipekontraktor" id="tipekontraktor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="#">Pilih Opsi</option>
                                    <?php foreach (get_all('mpasien_kelompok', array('status' => 1, 'id !=' => 5)) as $row) {?>
                                    <option value="<?=$row->id?>"><?=$row->nama?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="div_kontraktor2" hidden>
                            <label class="col-md-3 control-label" for="idkontraktor">Kontraktor</label>
                            <div class="col-md-7">
                                <select style="width:100%" name="idkontraktor" id="idkontraktor" data-placeholder="Kontraktor" class="form-control  input-sm"></select>
                            </div>
                        </div>
                        <div class="form-group" id="div_pegawai" hidden>
                            <label class="col-md-3 control-label" for="idpegawai">Pegawai</label>
                            <div class="col-md-7">
                                <select style="width:100%" name="idpegawai" id="idpegawai" data-placeholder="Pegawai" class="form-control  input-sm"></select>
                            </div>
                        </div>
                        <div class="form-group" id="div_dokter" hidden>
                            <label class="col-md-3 control-label" for="iddokter">Dokter</label>
                            <div class="col-md-7">
                                <select style="width:100%" name="iddokter" id="iddokter" data-placeholder="Dokter" class="form-control  input-sm">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="nominal" placeholder="Nominal" name="nominal" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group" id="div_jaminan">
                            <label class="col-md-3 control-label" for="nama">Jaminan </label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="jaminan" placeholder="Jaminan" name="jaminan" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="nama">Keterangan </label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="ket" placeholder="Keterangan" name="ket" required="" aria-required="true">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Add</button>
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#diskon_rp").number(true, 0, '.', ',');
    $("#diskon_persen").number(true, 0, '.', ',');
    $("#gt_all").number(true, 0, '.', ',');
    $("#gt_rp").number(true, 0, '.', ',');
    $("#sisa_rp").number(true, 0, '.', ',');
    $("#gt_final").number(true, 0, '.', ',');

    $("#bayar_rp").number(true, 0, '.', ',');
    $("#sisa_modal").number(true, 0, '.', ',');
    $("#nominal").number(true, 0, '.', ',');

    gen_harga_diskon();
    grand_total_bayar();
    validasi_save();

    $("#idpegawai").select2({
        minimumInputLength: 2,
        noResults: 'Pegawai Tidak Ditemukan.',
        ajax: {
            url: '{site_url}tkasir/get_pegawai/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,

            data: function(params) {
                var query = {
                    search: params.term,
                }
                return query;
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });

    $("#iddokter").select2({
        minimumInputLength: 2,
        noResults: 'Dokter Tidak Ditemukan.',
        ajax: {
            url: '{site_url}tkasir/get_dokter/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,

            data: function(params) {
                var query = {
                    search: params.term,
                }
                return query;
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });
});

$("#diskon_persen").keyup(function() {
    if ($("#diskon_persen").val() == '') {
        $("#diskon_persen").val(0)
    }
    if (parseFloat($(this).val()) > 100) {
        $(this).val(100);
    }
    var harga_jadi = 0;
    harga_jadi = (parseFloat($("#gt_all").val()) * (parseFloat($("#diskon_persen").val() / 100)));
    $("#diskon_rp").val(harga_jadi);
    gen_harga_diskon();
});

$("#diskon_rp").keyup(function() {
    if ($("#diskon_rp").val() == '') {
        $("#diskon_rp").val(0)
    }
    if (parseFloat($(this).val()) > parseFloat($("#gt_all").val())) {
        $(this).val(parseFloat($("#gt_all").val()));
    }
    var harga_jadi = 0;
    harga_jadi = (parseFloat($("#diskon_rp").val()) * 100) / (parseFloat($("#gt_all").val()));
    $("#diskon_persen").val(parseFloat(harga_jadi).toFixed(2));
    gen_harga_diskon();
});

function gen_harga_diskon() {
    var total;
    total = parseFloat($("#gt_all").val().replace(/,/g, '')) - parseFloat($("#diskon_rp").val().replace(/,/g, ''));
    total = Math.ceil(total / 100) * 100;

    $("#gt_rp").val(total);

    gen_sisa();
}

function gen_sisa() {
    var total;
    total = parseFloat($("#gt_rp").val()) - parseFloat($("#bayar_rp").val());

    $("#sisa_rp").val(total);
    validasi_save();
}

$("#btn_pembayaran").click(function() {

    $("#sisa_modal").val($("#sisa_rp").val());
    $("#nominal").val($("#sisa_rp").val());

    $("#nomor").val('')
    $("#trace_number").val('')
    $("#rowindex").val('')
    $('#idmetode').val('').trigger('change');
    $('#idbank').val('').trigger('change');
    $('#idpegawai').val('').trigger('change');
    $("#div_bank").hide();
    $("#div_kontraktor").hide();
    $("#div_pegawai").hide();
    $("#div_cc").hide();
    $("#div_trace").hide();
    $("#div_jaminan").hide();

    $("#ket").val('');
});

$("#idmetode").change(function() {
    if ($("#idmetode").val() == "1") {
        //CASH
        $("#div_jaminan").hide();
        $("#div_cc").hide();
        $("#div_trace").hide();
        $("#div_bank").hide();
        $("#div_kontraktor").hide();
        $("#div_kontraktor2").hide();
        $("#div_pegawai").hide();
        $("#div_tipepegawai").hide();
        $("#div_dokter").hide();
        $("#ket").val('Tunai');
    }
    if ($("#idmetode").val() == "2" || $("#idmetode").val() == "3" || $("#idmetode").val() == "4") {
        $("#div_kontraktor").hide();
        $("#div_kontraktor2").hide();
        $("#div_jaminan").hide();
        if ($("#idmetode").val() == "3") {
            $("#div_cc").show();
        } else {
            $("#div_cc").hide();
        }
        $("#div_trace").show();
        $("#div_pegawai").hide();
        $("#div_tipepegawai").hide();
        $("#div_dokter").hide();
        $("#div_bank").show();
        $("#ket").val($("#idmetode option:selected").text() + ' : ' + $("#idbank option:selected").text());
    }
    if ($("#idmetode").val() == "5") {
        //Pegawai
        $("#div_jaminan").hide();
        $("#div_bank").hide();
        $("#div_cc").hide();
        $("#div_trace").hide();
        $("#div_kontraktor").hide();
        $("#div_kontraktor2").hide();
        $("#div_tipepegawai").show();
        $("#div_pegawai").show();
        $("#div_dokter").hide();
        $("#ket").val('Tagihan Karyawan : ' + $("#idpegawai option:selected").text());
    }
    if ($("#idmetode").val() == "6") {
        //Dokter
        $("#div_jaminan").hide();
        $("#div_bank").hide();
        $("#div_cc").hide();
        $("#div_trace").hide();
        $("#div_kontraktor").hide();
        $("#div_kontraktor2").hide();
        $("#div_tipepegawai").show();
        $("#div_pegawai").hide();
        $("#div_dokter").show();
        $("#ket").val('Tagihan Dokter : ' + $("#iddokter option:selected").text());
    }
    if ($("#idmetode").val() == "7") {
        //Tidak Tertagihkan
        $("#div_jaminan").show();
        $("#div_bank").hide();
        $("#div_trace").hide();
        $("#div_cc").hide();
        $("#div_kontraktor").hide();
        $("#div_kontraktor2").hide();
        $("#div_pegawai").hide();
        $("#div_tipepegawai").hide();
        $("#div_dokter").hide();
        $("#ket").val('Tidak Tertagihkan dg Jaminan ' + $("#jaminan").val() + ' Karena : ');
    }
    if ($("#idmetode").val() == "8") {
        //Kontraktor
        $("#div_bank").hide();
        $("#div_trace").hide();
        $("#div_jaminan").hide();
        $("#div_cc").hide();
        $("#div_kontraktor").show();
        $("#div_kontraktor2").show();
        $("#div_pegawai").hide();
        $("#div_tipepegawai").hide();
        $("#div_dokter").hide();
        $("#ket").val('Pembayaran ' + $("#tipekontraktor option:selected").text() + ' : ' + $("#idkontraktor option:selected").text());
    }
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$("#tipekontraktor").change(function() {
    if ($(this).val() != '') {
        $.ajax({
            url: '{base_url}trawatinap_tindakan/find_kontraktor/' + $(this).val(),
            dataType: "json",
            success: function(data) {
                $('#idkontraktor').empty();
                $.each(data.detail, function(i, row) {
                    $('#idkontraktor')
                        .append('<option value="' + row.id + '" selected>' + row.nama + '</option>');
                });
            }
        });
        $("#ket").val($("#tipekontraktor option:selected").text() + ' : ' + $("#idkontraktor option:selected").text());
    }
});

$("#idkontraktor").change(function() {
    $("#ket").val($("#tipekontraktor option:selected").text() + ' : ' + $("#idkontraktor option:selected").text());
});

$("#idbank").change(function() {
    $("#ket").val($("#idmetode option:selected").text() + ' : ' + $("#idbank option:selected").text());
});

$("#idpegawai").change(function() {
    $("#ket").val('Tagihan Karyawan : ' + $("#idpegawai option:selected").text());
});

$("#ket_cc").keyup(function() {
    $("#ket").val($("#idmetode option:selected").text() + ' : ' + $("#idbank option:selected").text() + ', Keterangan CC : ' + $("#ket_cc").val());
});

$("#jaminan").keyup(function() {
    $("#ket").val('Tidak Tertagihkan dg Jaminan ' + $("#jaminan").val() + ' Karena : ');
});

function validate_detail() {
    if ($("#idmetode").val() == "") {
        sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
        $("#idmetode").focus();
        return false;
    }
    if ($("#idmetode").val() == null) {
        sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
        $("#idmetode").focus();
        return false;
    }
    if ($("#idmetode").val() == '2' || $("#idmetode").val() == '3' || $("#idmetode").val() == '4') {
        if ($("#idbank option:selected").val() == '' || $("#idbank option:selected").val() == null) {
            sweetAlert("Maaf...", "Bank Harus dipilih!", "error");
            $("#idbank").focus();
            return false;
        }
    }
    if ($("#idmetode").val() == '3') {
        if ($("#ket_cc").val() == '' || $("#ket_cc").val() == null) {
            sweetAlert("Maaf...", "CC Harus dipilih!", "error");
            $("#ket_cc").focus();
            return false;
        }
    }
    if ($("#idmetode").val() == '5') {
        if ($("#idpegawai option:selected").val() == '' || $("#idpegawai option:selected").val() == null) {
            sweetAlert("Maaf...", "Pegawai Harus dipilih!", "error");
            $("#idpegawai").focus();
            return false;
        }
    }
    if ($("#idmetode").val() == '6') {
        if ($("#iddokter option:selected").val() == '' || $("#iddokter option:selected").val() == null) {
            sweetAlert("Maaf...", "Dokter Harus dipilih!", "error");
            $("#iddokter").focus();
            return false;
        }
    }
    if ($("#idmetode").val() == '7') {
        if ($("#jaminan").val() == '' || $("#jaminan").val() == null) {
            sweetAlert("Maaf...", "Jaminan Harus diisi!", "error");
            $("#jaminan").focus();
            return false;
        }
    }
    if ($("#idmetode").val() == '8') {
        if ($("#tipekontraktor option:selected").val() != '2') {
            if ($("#idkontraktor option:selected").val() == '' || $("#idkontraktor option:selected").val() == null) {
                sweetAlert("Maaf...", "Kontraktor Harus dipilih!", "error");
                $("#idpegawai").focus();
                return false;
            }
        }
    }
    if (parseFloat($("#nominal").val()) < 1 || $("#nominal").val() == '') {
        sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
        return false;
    }

    return true;
}

$("#btn_add_bayar").click(function() {
    if (!validate_detail()) return false;
    var duplicate = false;
    var content = "";

    if ($("#nomor").val() != '') {
        var no = $("#nomor").val();
    } else {
        var no = $('#manage_tabel_pembayaran tr').length - 2;
        var content = "<tr>";
    }

    if (duplicate) return false;
    content += "<td style='display:none'>" + $("#idmetode").val() + "</td>"; //0 Nama Obat
    content += "<td >" + no + "</td>"; //1 Nomor
    content += "<td >" + $("#idmetode option:selected").text() + "</td>"; //2 Nama
    content += "<td >" + $("#ket").val() + "</td>"; //3 Keterangan
    content += "<td style='display:none'>" + $("#nominal").val() + "</td>"; //4 Nominal
    content += "<td >" + formatNumber($("#nominal").val()) + "</td>"; //5 Nominal
    content += "<td  style='display:none'>" + $("#idkasir").val() + "</td>"; //6 Kasir
    content += "<td  style='display:none'>" + $("#idmetode").val() + "</td>"; //7 Metode
    content += "<td  style='display:none'>" + $("#idpegawai").val() + "</td>"; //8 Metode
    content += "<td  style='display:none'>" + $("#idbank").val() + "</td>"; //9 Metode
    content += "<td  style='display:none'>" + $("#ket_cc").val() + "</td>"; //10 Metode
    content += "<td  style='display:none'>" + $("#idkontraktor").val() + "</td>"; //11 Metode
    content += "<td  style='display:none'>" + $("#ket").val() + "</td>"; //12 Metode
    content += "<td>";
    content += "<button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;";
    content += "	<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>";
    content += "</td>";
    content += "<td  style='display:none'>" + $("#jaminan").val() + "</td>"; //14 Metode
    content += "<td  style='display:none'>" + $("#trace_number").val() + "</td>"; //15 Metode
    if ($("#idmetode").val() == '8') {
        content += "<td style='display:none'>" + $("#tipekontraktor").val() + "</td>"; //16 Tipe Kontraktor
    } else {
        content += "<td style='display:none'>0</td>"; //16 Tipe Kontraktor
    }
    if ($("#rowindex").val() != '') {
        $('#manage_tabel_pembayaran tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
    } else {
        content += "</tr>";
        $('#manage_tabel_pembayaran tbody').append(content);
    }

    grand_total_bayar();
    checkSelisihPembayaran();
});

$(document).on("click", ".hapus", function() {
    if (confirm("Hapus Data ?") == true) {

        $(this).closest('td').parent().remove();

    }
    grand_total_bayar();

});

$(document).on("click", ".edit", function() {
    $nominal = $(this).closest('tr').find("td:eq(4)").html();
    $idmetode = $(this).closest('tr').find("td:eq(0)").html();
    $idpegawai = $(this).closest('tr').find("td:eq(8)").html();
    $idbank = $(this).closest('tr').find("td:eq(9)").html();
    $ket_cc = $(this).closest('tr').find("td:eq(10)").html();
    $idkontraktor = $(this).closest('tr').find("td:eq(11)").html();
    $ket = $(this).closest('tr').find("td:eq(12)").html();
    $jaminan = $(this).closest('tr').find("td:eq(14)").html();
    $trace_number = $(this).closest('tr').find("td:eq(15)").html();

    $('#idmetode').val($idmetode).trigger('change');
    $('#idpegawai').val($idpegawai).trigger('change');
    $('#idbank').val($idbank).trigger('change');
    $('#ket_cc').val($ket_cc);
    $('#ket').val($ket);
    $('#nominal').val($nominal);
    $('#jaminan').val($jaminan);
    $('#trace_number').val($trace_number);

    $('#nominal_edit').val($nominal);
    $("#sisa_modal").val(parseFloat($("#sisa_rp").val()) + parseFloat($("#nominal").val()));
    $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
    $("#nomor").val($(this).closest('tr').find("td:eq(1)").html());
    $("#PembayaranModal").modal('show');
});

function grand_total_bayar() {
    var total_grand = 0;
    $('#manage_tabel_pembayaran tbody tr').each(function() {
        total_grand += parseFloat($(this).find('td:eq(4)').text());
    });

    $("#bayar_rp").val(total_grand);

    gen_sisa();
}

function validasi_save() {
    if (parseFloat($("#sisa_rp").val()) > 0) {
        $("#btn_save").attr('disabled', true);
    } else {
        $("#btn_save").attr('disabled', false);
    }
}

</script>
