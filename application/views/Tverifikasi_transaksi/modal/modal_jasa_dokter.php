<div class="modal fade in black-overlay" id="EditJasaMedisModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
        <div class="modal-content">
            <div class="block block-themed">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">EDIT JASA DOKTER</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <form class="form-horizontal">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Uraian Jasa</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tTindakanNamaTarif" placeholder="Uraian Jasa" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group" id="detail_xray" hidden>
                                    <label class="col-md-3 control-label">Status Expertise</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tTindakanXRayStatusExpertise" placeholder="Status Tindakan" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jasa Medis</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control number input-sm" id="tTindakanJasaMedis" placeholder="Jasa Medis" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="tVisiteIdDokter">Dokter</label>
                                    <div class="col-md-9">
                                        <select id="tTindakanIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group detail_perujuk">
                                    <label class="col-md-3 control-label">Persentase Perujuk (%)</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control discount input-sm" id="tTindakanPersentasePerujuk" disabled placeholder="Persentase Perujuk" value="">
                                    </div>
                                </div>
                                <div class="form-group detail_perujuk">
                                    <label class="col-md-3 control-label">Nominal Perujuk</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control number input-sm" id="tTindakanNominalPerujuk" disabled placeholder="Nominal Perujuk" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Potongan RS (%)</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control discount input-sm" id="tTindakanPotonganRS" placeholder="Potongan RS" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nominal Potongan RS</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control number input-sm" id="tTindakanNominalPotonganRS" placeholder="Nominal Potongan RS" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Pajak Dokter (%)</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control discount input-sm" id="tTindakanPajakDokter" placeholder="Pajak Dokter" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nominal Pajak Dokter</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control number input-sm" id="tTindakanNominalPajakDokter" placeholder="Nominal Pajak Dokter" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jasa Medis Netto</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control number input-sm" id="tTindakanJasaMedisNetto" placeholder="Jasa Medis Netto" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Periode Pembayaran</label>
                                    <div class="col-md-9">
                                        <select id="tTindakanPeriodePembayaran" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="tTindakanTable" readonly value="">
                            <input type="hidden" id="tTindakanIdRawatInap" readonly value="">
                            <input type="hidden" id="tTindakanRowId" readonly value="">
                            <input type="hidden" id="tTindakanIdTarif" readonly value="">
                            <input type="hidden" id="tTindakanPeriodeJatuhTempo" readonly value="">
                            <input type="hidden" id="tTindakanStatusVerifikasi" readonly value="">

                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" id="saveDataJasaDokter" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
                <button class="btn btn-sm btn-default" id="cancelDataJasaDokter" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');

    getDokter();

    $(document).on('click', '.editJasaMedis', function() {
        var item = JSON.parse(decodeURIComponent($(this).data('item')));

        if (item.table) {
            var table = item.table;
            var idrawatinap = item.idrawatinap;
            var iddetail = item.iddetail;
            var idtarif = item.idtarif;
            var namatarif = item.namatarif;
            var iddokter = item.iddokter;
            var namadokter = item.namadokter;
            var jasamedis = item.jasamedis;
            var persentase_perujuk = item.persentase_perujuk;
            var potongan_rs = item.potongan_rs;
            var pajak_dokter = item.pajak_dokter;
            var nominal_potongan_rs = item.nominal_potongan_rs;
            var nominal_pajak_dokter = item.nominal_pajak_dokter;
            var nominal_perujuk = item.nominal_perujuk;
            var jasamedis_netto = item.jasamedis_netto;
            var periode_pembayaran = item.periode_pembayaran;
            var periode_jatuhtempo = item.periode_jatuhtempo;
            var status_expertise = item.status_expertise;
            var status_verifikasi = item.status_verifikasi;

            loadPeriodePembayaranHonorDokter(iddokter, periode_pembayaran);

            $("#tTindakanTable").val(table);
            $("#tTindakanIdRawatInap").val(idrawatinap);
            $("#tTindakanRowId").val(iddetail);
            $("#tTindakanIdTarif").val(idtarif);
            $("#tTindakanNamaTarif").val(namatarif);
            $("#tTindakanJasaMedis").val(jasamedis);

            $("#tTindakanIdDokter").select2('destroy');
            $("#tTindakanIdDokter").val(iddokter);
            $("#tTindakanIdDokter").select2();

            $("#tTindakanPotonganRS").val(potongan_rs);
            $("#tTindakanNominalPotonganRS").val(nominal_potongan_rs);
            $("#tTindakanPajakDokter").val(pajak_dokter);
            $("#tTindakanNominalPajakDokter").val(nominal_pajak_dokter);
            $("#tTindakanJasaMedisNetto").val(jasamedis_netto);
            $("#tTindakanPeriodePembayaran").val(periode_pembayaran);
            $("#tTindakanPeriodeJatuhTempo").val(periode_jatuhtempo);
            $("#tTindakanStatusVerifikasi").val(status_verifikasi);

            if (persentase_perujuk) {
                $(".detail_perujuk").show();
                $("#tTindakanPersentasePerujuk").val(persentase_perujuk);
                $("#tTindakanNominalPerujuk").val(nominal_perujuk);
            } else {
                $("#tTindakanPersentasePerujuk").val(0);
                $("#tTindakanNominalPerujuk").val(0);
                $(".detail_perujuk").hide();
            }

            if (status_expertise == 1) {
                $("#detail_xray").show();
                $("#tTindakanXRayStatusExpertise").val('Langsung');
            } else if (status_expertise == 2) {
                $("#detail_xray").show();
                $("#tTindakanXRayStatusExpertise").val('Menolak');
            } else if (status_expertise == 3) {
                $("#detail_xray").show();
                $("#tTindakanXRayStatusExpertise").val('Kembali');
            } else {
                $("#detail_xray").hide();
            }
        }
    });

    $(document).on('change', '#tTindakanIdDokter', function() {
        const iddokter = $(this).val();
        const table = $("#tTindakanTable").val();
        const idrawatinap = $("#tTindakanIdRawatInap").val();

        loadPeriodePembayaranHonorDokter(iddokter, '');
        if (table == 'trawatinap_tindakan' || table == 'trawatinap_visite' || table == 'tkamaroperasi_jasado') {
            getPotonganDokterRanap(idrawatinap, iddokter);
        } else {
            getPotonganDokter(iddokter);
		}
    });

    $(document).on('change', '#tTindakanPeriodePembayaran', function() {
        $("#tTindakanPeriodeJatuhTempo").val($('#tTindakanPeriodePembayaran option:selected').data('jatuhtempo'));
    });

    $(document).on('keyup', '#tTindakanPotonganRS', function() {
        var jasamedis = parseFloat($('#tTindakanJasaMedis').val().replace(/,/g, ''));
        var potongan_rs = parseFloat($(this).val().replace(/,/g, ''));
        var nominal_potongan_rs = parseFloat(jasamedis) * (parseFloat(potongan_rs) / 100);

        if (potongan_rs > 100) {
            $('#tTindakanPotonganRS').val(100);
            $("#tTindakanNominalPotonganRS").val(jasamedis);
        } else {
            $("#tTindakanNominalPotonganRS").val(nominal_potongan_rs);
        }

        calculateJasaMedisNetto();
    });

    $(document).on('keyup', '#tTindakanNominalPotonganRS', function() {
        var jasamedis = parseFloat($('#tTindakanJasaMedis').val().replace(/,/g, ''));
        var nominal_potongan_rs = parseFloat($(this).val().replace(/,/g, ''));
        var potongan_rs = (parseFloat(nominal_potongan_rs) / parseFloat(jasamedis)) * 100;

        if (nominal_potongan_rs > jasamedis) {
            $('#tTindakanNominalPotonganRS').val(jasamedis);
            $("#tTindakanPotonganRS").val(100);
        } else {
            $("#tTindakanPotonganRS").val(potongan_rs);
        }

        calculateJasaMedisNetto();
    });

    $(document).on('keyup', '#tTindakanPajakDokter', function() {
        var jasamedis = parseFloat($('#tTindakanJasaMedis').val().replace(/,/g, ''));
        var nominal_perujuk = parseFloat($("#tTindakanNominalPerujuk").val().replace(/,/g, ''));
        var pajak_dokter = parseFloat($(this).val().replace(/,/g, ''));

        if (nominal_perujuk > 0) {
            var nominal_pajak_dokter = (parseFloat(jasamedis) - parseFloat(nominal_perujuk)) * (parseFloat(pajak_dokter) / 100);
        } else {
            var nominal_pajak_dokter = parseFloat(jasamedis) * (parseFloat(pajak_dokter) / 100);    
        }

        if (pajak_dokter > 100) {
            $('#tTindakanPajakDokter').val(100);
            $("#tTindakanNominalPajakDokter").val(jasamedis);
        } else {
            $("#tTindakanNominalPajakDokter").val(nominal_pajak_dokter);
        }

        calculateJasaMedisNetto();
    });

    $(document).on('keyup', '#tTindakanNominalPajakDokter', function() {
        var jasamedis = parseFloat($('#tTindakanJasaMedis').val().replace(/,/g, ''));
        var nominal_perujuk = parseFloat($("#tTindakanNominalPerujuk").val().replace(/,/g, ''));
        var nominal_pajak_dokter = parseFloat($(this).val().replace(/,/g, ''));
        var pajak_dokter = (parseFloat(nominal_pajak_dokter) / parseFloat(jasamedis)) * 100;

        if (nominal_perujuk > 0) {
            var pajak_dokter = (parseFloat(nominal_pajak_dokter) / (parseFloat(jasamedis) - parseFloat(nominal_perujuk))) * 100;
        } else {
            var pajak_dokter = (parseFloat(nominal_pajak_dokter) / parseFloat(jasamedis)) * 100;
        }

        if (nominal_pajak_dokter > jasamedis) {
            $('#tTindakanNominalPajakDokter').val(jasamedis);
            $("#tTindakanPajakDokter").val(100);
        } else {
            $("#tTindakanPajakDokter").val(pajak_dokter);
        }

        calculateJasaMedisNetto();
    });

    $(document).on('click', '#saveDataJasaDokter', function() {
        var table = $('#tTindakanTable').val();
        var idrawatinap = $('#tTindakanIdRawatInap').val();
        var iddetail = $('#tTindakanRowId').val();
        var idkategori = $('#tTindakanIdDokter option:selected').data('idkategori');
        var namakategori = $('#tTindakanIdDokter option:selected').data('namakategori');
        var iddokter = $('#tTindakanIdDokter option:selected').val();
        var namadokter = $('#tTindakanIdDokter option:selected').text();
        var jasamedis = $('#tTindakanJasaMedis').val();
        var potongan_rs = $('#tTindakanPotonganRS').val();
        var nominal_potongan_rs = $('#tTindakanNominalPotonganRS').val();
        var pajak_dokter = $('#tTindakanPajakDokter').val();
        var nominal_pajak_dokter = $('#tTindakanNominalPajakDokter').val();
        var jasamedis_netto = $('#tTindakanJasaMedisNetto').val();
        var periode_pembayaran = $('#tTindakanPeriodePembayaran').val();
        var periode_jatuhtempo = $('#tTindakanPeriodeJatuhTempo').val();
        var status_verifikasi = $('#tTindakanStatusVerifikasi').val();

        if (iddokter == '') {
            sweetAlert("Maaf...", "Dokter tidak boleh kosong !", "error").then((value) => {
                $('#tTindakanIdDokter').focus();
            });
            return false;
        }

        if (potongan_rs == '') {
            sweetAlert("Maaf...", "Potongan RS tidak boleh kosong !", "error").then((value) => {
                $("#tTindakanPotonganRS").select2('open');
            });
            return false;
        }

        if (pajak_dokter == '') {
            sweetAlert("Maaf...", "Pajak Dokter tidak boleh kosong !", "error").then((value) => {
                $("#tTindakanPajakDokter").select2('open');
            });
            return false;
        }

        if (periode_pembayaran == '') {
            sweetAlert("Maaf...", "Periode Pembayaran tidak boleh kosong !", "error").then((value) => {
                $("#tTindakanPeriodePembayaran").select2('open');
            });
            return false;
        }

        $.ajax({
            url: '{site_url}tverifikasi_transaksi/saveDataJasaDokter',
            method: 'POST',
            data: {
                table: table,
                idrawatinap: idrawatinap,
                iddetail: iddetail,
                idkategori: idkategori,
                namakategori: namakategori,
                iddokter: iddokter,
                namadokter: namadokter,
                jasamedis: jasamedis,
                potongan_rs: potongan_rs,
                pajak_dokter: pajak_dokter,
                nominal_potongan_rs: nominal_potongan_rs,
                nominal_pajak_dokter: nominal_pajak_dokter,
                jasamedis_netto: jasamedis_netto,
                periode_pembayaran: periode_pembayaran,
                periode_jatuhtempo: periode_jatuhtempo,
                status_verifikasi: status_verifikasi
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Jasa Dokter Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                location.reload();
            }
        });
    });
});

function getDokter() {
    $.ajax({
        url: "{site_url}trawatinap_tindakan/getDokterPJ",
        dataType: "json",
        success: function(data) {
            $("#tTindakanIdDokter").empty();
            $("#tTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#tTindakanIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "' data-namakategori='" + data[i].namakategori + "'>" + data[i].nama + "</option>");
            }
        }
    });
}

var tempIdDokter;

function loadPeriodePembayaranHonorDokter(iddokter, periode_pembayaran) {
    if (tempIdDokter != iddokter) {
        $.ajax({
            url: '{site_url}tverifikasi_transaksi/GetPeriodePembayaranHonorDokter/' + iddokter + '/' + periode_pembayaran,
            dataType: "json",
            success: function(data) {
                $("#tTindakanPeriodePembayaran").empty();
                $('#tTindakanPeriodePembayaran').append(data.list_option);
            }
        });
    }

    tempIdDokter = iddokter;
}

function getPotonganDokter(iddokter) {
    $.ajax({
        url: '{site_url}tverifikasi_transaksi/GetPotonganDokter/' + iddokter,
        dataType: "json",
        success: function(data) {
            $('#tTindakanPajakDokter').val(data.pajak_dokter);

            var jasamedis = parseFloat($('#tTindakanJasaMedis').val().replace(/,/g, ''));
            var nominal_perujuk = parseFloat($("#tTindakanNominalPerujuk").val().replace(/,/g, ''));

            if (nominal_perujuk > 0) {
                var potongan_rs = data.potonganfee_rs;
                var nominal_pajak_dokter = (parseFloat(jasamedis) - parseFloat(nominal_perujuk)) * (parseFloat(data.pajak_dokter) / 100);
            } else {
                var potongan_rs = data.potongan_rs;
                var nominal_pajak_dokter = parseFloat(jasamedis) * (parseFloat(data.pajak_dokter) / 100);    
            }

            var nominal_potongan_rs = parseFloat(jasamedis) * (parseFloat(potongan_rs) / 100);

            $("#tTindakanPotonganRS").val(potongan_rs);
            $("#tTindakanNominalPotonganRS").val(nominal_potongan_rs);
            $('#tTindakanNominalPajakDokter').val(nominal_pajak_dokter);
        }, complete: function (data) {
            calculateJasaMedisNetto();
        }
    });
}

function getPotonganDokterRanap(idrawatinap, iddokter) {
    $.ajax({
        url: '{site_url}tverifikasi_transaksi/getPotonganDokterRanap/' + idrawatinap + '/' + iddokter,
        dataType: "json",
        success: function(data) {
            $('#tTindakanPajakDokter').val(data.pajak_dokter);

            var jasamedis = parseFloat($('#tTindakanJasaMedis').val().replace(/,/g, ''));
            var nominal_perujuk = parseFloat($("#tTindakanNominalPerujuk").val().replace(/,/g, ''));

            if (nominal_perujuk > 0) {
                var potongan_rs = data.potonganfee_rs;
                var nominal_pajak_dokter = (parseFloat(jasamedis) - parseFloat(nominal_perujuk)) * (parseFloat(data.pajak_dokter) / 100);
            } else {
                var potongan_rs = data.potongan_rs;
                var nominal_pajak_dokter = parseFloat(jasamedis) * (parseFloat(data.pajak_dokter) / 100);    
            }

            var nominal_potongan_rs = parseFloat(jasamedis) * (parseFloat(potongan_rs) / 100);

            $("#tTindakanPotonganRS").val(potongan_rs);
            $("#tTindakanNominalPotonganRS").val(nominal_potongan_rs);
            $('#tTindakanNominalPajakDokter').val(nominal_pajak_dokter);
        }, complete: function (data) {
            calculateJasaMedisNetto();
        }
    });
}

function calculateJasaMedisNetto() {
    var jasamedis = parseFloat($("#tTindakanJasaMedis").val());
    var nominal_perujuk = parseFloat($("#tTindakanNominalPerujuk").val());
    var nominal_potongan_rs = parseFloat($("#tTindakanNominalPotonganRS").val());
    var nominal_pajak_dokter = parseFloat($("#tTindakanNominalPajakDokter").val());
    var netto = jasamedis - nominal_perujuk - nominal_potongan_rs - nominal_pajak_dokter;
    $("#tTindakanJasaMedisNetto").val(netto);
}

</script>
