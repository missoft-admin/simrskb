<?=ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}
?>

<style media="screen">
.text-bold {
    font-weight: bold;
}

.separator-header {
    margin-top: 450px;
}

.modal {
    overflow: auto !important;
}

</style>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tverifikasi_transaksi/index/poliklinik" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Transaksi</span>
                    <input type="text" class="form-control tindakanTanggalTransaksi" value="{tanggaltransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Transaksi</span>
                    <input type="text" class="form-control tindakanNoTransaksi" value="{notransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Resep</span>
                    <input type="text" class="form-control tindakanStatusResep" value="{statusresep}" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kategori</span>
                    <input type="text" class="form-control tindakanKategori" value="{kategori}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{nomedrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
            </div>
        </div>

        <div class="content-verification">

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiObat = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianOB($id, 3) as $row) {?>
                    <?php
                    $statusEdit = '0';
                    $tarif = $this->model->getStatusEditRanapObat($row->idtarif);
                    if ($tarif) {
                        $margin = $tarif->marginumum;
                        if ($row->idkelompokpasien == 1) {
                        $margin = $tarif->marginasuransi;
                        }else if ($row->idkelompokpasien == 2) {
                        $margin = $tarif->marginjasaraharja;
                        }else if ($row->idkelompokpasien == 3) {
                        $margin = $tarif->marginbpjskesehatan;
                        }else if ($row->idkelompokpasien == 4) {
                        $margin = $tarif->marginbpjstenagakerja;
                        }

                        if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
                        $statusEdit = '1';
                        }
                    }
                    ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunit?></td>
                        <td><?=$row->namaunit?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpasien_penjualan_nonracikan" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan; ?>
                    <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFarmasiObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT RACIKAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiObatRacikan = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianOBObatRacikan($id) as $row) { ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <?php $totalFarmasiObatRacikan = $totalFarmasiObatRacikan + $row->totalkeseluruhan; ?>
                    <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFarmasiObatRacikan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiAlkes = 0;?>
                    <?php $dataFarmasiAlkesIGD = $this->Tpoliklinik_verifikasi_model->viewRincianOB($id, 1);?>
                    <?php foreach ($dataFarmasiAlkesIGD as $row) {?>
                    <?php
                    $statusEdit = '0';
                    $tarif = $this->model->getStatusEditRanapObat($row->idtarif);
                    if ($tarif) {
                        $margin = $tarif->marginumum;
                        if ($row->idkelompokpasien == 1) {
                        $margin = $tarif->marginasuransi;
                        }else if ($row->idkelompokpasien == 2) {
                        $margin = $tarif->marginjasaraharja;
                        }else if ($row->idkelompokpasien == 3) {
                        $margin = $tarif->marginbpjskesehatan;
                        }else if ($row->idkelompokpasien == 4) {
                        $margin = $tarif->marginbpjstenagakerja;
                        }

                        if ($tarif->hargadasar != $row->hargadasar || $margin != $row->margin) {
                        $statusEdit = '1';
                        }
                    }
                    ?>
                    <?php $subtotal = $row->hargajual * $row->kuantitas ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpasien_penjualan_nonracikan" class="btn btn-sm btn-success editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                            <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                        </td>
                    </tr>
                    <?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan; ?>
                    <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFarmasiAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <?php $totalKeseluruhan = $totalFarmasiObat + $totalFarmasiObatRacikan + $totalFarmasiAlkes; ?>

            <hr>

            <table class="table table-bordered table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL KESELURUHAN</b></td>
                        <td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px"><?=number_format($totalKeseluruhan)?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div id="transaction-only">
    <div class="block">
        <div class="block-content">
            <h5 style="margin-bottom: 10px;">Pembayaran</h5>
            <table id="cara_pembayaran" class="table table-bordered table-striped" style="margin-top: 20px;">
                <tbody>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm" readonly type="text" id="gt_all" name="gt_all" value="<?=number_format($totalKeseluruhan)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>DISKON</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input class="form-control" type="text" id="diskon_rp" name="diskon_rp" placeholder="Discount Rp" value="{diskon_rp}">
							</div>
							<div class="input-group">
								<span class="input-group-addon"> %. </span>
								<input class="form-control" type="text" id="diskon_persen" name="diskon_persen" placeholder="Discount %"  value="{diskon_persen}">
							</div>
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm" readonly type="text" id="gt_rp" name="gt_rp" value="<?=number_format($totalKeseluruhan)?>">
						</th>
					</tr>
					<tr class="bg-light dker">
						<th colspan="5"></th>
						<th colspan="2">
							<button class="btn btn-success btn_pembayaran" id="btn_pembayaran" data-toggle="modal"  data-target="#PembayaranModal"  type="button">Pilih Cara Pembayaran</button>
						</th>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
							<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 25%;">Jenis Pembayaran</th>
										<th style="width: 35%;">Keterangan</th>
										<th style="width: 10%;">Nominal</th>
										<th style="width: 10%;" class="editOnly">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;?>
									<?php foreach ($detail_pembayaran as $row) {?>
										<tr>
											<td hidden><?=$row->idmetode?></td>
											<td><?=$no++?></td>
											<td><?=metodePembayaran($row->idmetode)?></td>
											<td><?=$row->keterangan?></td>
											<td hidden><?=$row->nominal?></td>
											<td><?=number_format($row->nominal)?></td>
											<td hidden>idkasir</td>
											<td hidden><?=$row->idmetode?></td>
											<td hidden><?=$row->idpegawai?></td>
											<td hidden><?=$row->idbank?></td>
											<td hidden><?=$row->ket_cc?></td>
											<td hidden><?=$row->idkontraktor?></td>
											<td hidden><?=$row->keterangan?></td>
											<td class="editOnly">
                                                <button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>
												&nbsp;&nbsp;
                                                <button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>
											</td>
											<td hidden><?=$row->jaminan?></td>
											<td hidden><?=$row->trace_number?></td>
										</tr>
									<?php }?>
								</tbody>
								<tfoot id="foot_pembayaran">
									<tr class="bg-light dker">
										<th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
                        <th colspan="2">
                            <input type="text" class="form-control input-sm" readonly id="bayar_rp" name="bayar_rp" value="{bayar_rp}">
                        </th>
                    </tr>
                    <tr class="bg-light dker">
                        <th colspan="3"><label class="pull-right"><b>Sisa Rp.</b></label></th>
                        <th colspan="2">
                            <input type="text" class="form-control input-sm" readonly id="sisa_rp" name="sisa_rp" value="<?=$totalKeseluruhan - $bayar_rp?>">
                        </th>
                    </tr>
                    </tfoot>
            </table>
        </div>
    </div>
</div>

<input type="hidden" id="rowindex" value="">
<input type="hidden" id="nomor" value="">
<input type="hidden" id="pos_tabel_pembayaran" name="pos_tabel_pembayaran">
<input type="hidden" id="idpendaftaran" value="{idpendaftaran}">

<div id="transaction-only-action" hidden>
    <div class="modal-footer">
        <div class="buton">
            <button type="submit" class="btn btn-primary" id="btn_simpan_pembayaran">Simpan Pembayaran</a>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');
    $('.discount').number(true, 2, '.', ',');

    var statusEdit = "<?=$stedit;?>";
    if (statusEdit == "0") {
        $(".editOnly").hide();
        $("#btn_pembayaran").hide();
        $("input, select").prop('disabled', true);
    }

    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });

    getDokter();
});

function getDokter() {
    $.ajax({
        url: "{site_url}trawatinap_tindakan/getDokterPJ",
        dataType: "json",
        success: function(data) {
            $("#tTindakanIdDokter").empty();
            $("#tTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#tTindakanIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
            }

            $("#tTindakanGlobalIdDokter").empty();
            $("#tTindakanGlobalIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#tTindakanGlobalIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
            }

            $("#fTindakanIdDokter").empty();
            $("#fTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
            for (var i = 0; i < data.length; i++) {
                $("#fTindakanIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
            }
        }
    });
}

</script>

<!-- Modal Included -->
<? $this->load->view('Trawatinap_tindakan/modal/modal_edit_tindakan_farmasi') ?>
<? $this->load->view('Tverifikasi_transaksi/modal/modal_pembayaran_poliklinik') ?>

<script type="text/javascript">
$(document).on("click", "#btn_simpan_pembayaran", function(e) {
    e.preventDefault();

    var pos_tabel_pembayaran = $('table#manage_tabel_pembayaran tbody tr').get().map(function(row) {
        return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
        })
    })

    $("#pos_tabel_pembayaran").val(JSON.stringify(pos_tabel_pembayaran));

    $.ajax({
        url: '{site_url}tverifikasi_transaksi/simpan_pembayaran_poliklinik',
        method: 'POST',
        dataType: 'json',
        data: {
            idtindakan: '<?php echo $this->uri->segment(3) ?>',
            idpendaftaran: $('#idpendaftaran').val(),
            subtotal: $('#gt_all').val(),
            diskon_rp: $('#diskon_rp').val(),
            diskon_persen: $('#diskon_persen').val(),
            deposit: $('#gt_deposit').val(),
            total: $('#gt_rp').val(),
            totalharusdibayar: $('#gt_final').val(),
            pembayaran: $('#bayar_rp').val(),
            sisa: $('#sisa_rp').val(),
            pos_tabel_pembayaran: JSON.stringify(pos_tabel_pembayaran),
        },
        success: function(data) {
            if (data.status == 200) {
                window.location = '{site_url}tverifikasi_transaksi/success/' + '<?php echo $this->uri->segment(3) ?>';
            } else {
                alert('data gagal disimpan');
            }
        }
    })
});

</script>
