<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1224'))){ ?>
<?
	$this->load->view('Trm_gabung/menu');
?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}Trefund/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<?php echo form_open('Trm_gabung_history/filter','class="form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable" id="alertInfo" hidden>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        <p></p>
		      </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px; margin-left: 0;">
						<b><span class="label label-success" style="font-size:12px">Data Pasien Lama</span></b>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">No. Medrec</label>
						<div class="col-md-10">
							<input type="text" id="nomedrec" class="form-control" name="nomedrec" value="{nomedrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Nama Pasien</label>
						<div class="col-md-10">
							<input type="text" id="namapasien" class="form-control" name="namapasien" value="{namapasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Tanggal Lahir</label>
						<div class="col-md-10">
							<input type="text" id="tanggallahir" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggallahir" value="{tanggallahir}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Jenis Kelamin</label>
						<div class="col-md-10">
							<input type="text" id="jeniskelamin" class="form-control" name="jeniskelamin" value="{jeniskelamin}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Alamat</label>
						<div class="col-md-10">
							<input type="text" id="alamatpasien" class="form-control" name="alamatpasien" value="{alamatpasien}">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px; margin-left: 0;">
						<b><span class="label label-success" style="font-size:12px">Data Pasien Baru</span></b>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">No. Medrec</label>
						<div class="col-md-10">
							<input type="text" id="nomedrec_new" class="form-control" name="nomedrec_new" value="{nomedrec_new}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Nama Pasien</label>
						<div class="col-md-10">
							<input type="text" id="namapasien_new" class="form-control" name="namapasien_new" value="{namapasien_new}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Tanggal Lahir</label>
						<div class="col-md-10">
							<input type="text" id="tanggallahir_new" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggallahir_new" value="{tanggallahir_new}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Jenis Kelamin</label>
						<div class="col-md-10">
							<input type="text" id="jeniskelamin_new" class="form-control" name="jeniskelamin_new" value="{jeniskelamin_new}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Alamat</label>
						<div class="col-md-10">
							<input type="text" id="alamatpasien_new" class="form-control" name="alamatpasien_new" value="{alamatpasien_new}">
						</div>
					</div>
			</div>
			<div class="col-md-12">
				<hr>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" style="text-align: left;">Tanggal Transaksi</label>
					<div class="col-md-10">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
              <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
              <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
              <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
            </div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label"  style="text-align: left;">User</label>
					<div class="col-md-10">
						<div class="row">
							<div class="col-md-10">
								<select name="iduser" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" <?=($iduser == '#' ? 'selected' : '')?>>Semua User</option>
									<?php foreach($list_user as $row){ ?>
										<option value="<?=$row->id?>" <?=($iduser == $row->id ? 'selected' : '')?>><?=$row->name?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2">
								<button class="btn btn-success" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>

		<hr style="margin-top:10px">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th rowspan="2" style="text-align:center; vertical-align: middle;;">Tanggal</th>
					<th colspan="4" style="text-align:center; vertical-align: middle;;">Data Lama</th>
					<th colspan="4" style="text-align:center; vertical-align: middle;;">Data Baru</th>
					<th rowspan="2" style="text-align:center; vertical-align: middle;;">Alasan</th>
					<th rowspan="2" style="text-align:center; vertical-align: middle;;">User</th>
					<th rowspan="2" style="text-align:center; vertical-align: middle;;">Aksi</th>
				</tr>
				<tr>
					<th style="text-align:center; vertical-align: middle;;">No. Medrec</th>
					<th style="text-align:center; vertical-align: middle;;">Nama Pasien</th>
					<th style="text-align:center; vertical-align: middle;;">Tanggal Lahir</th>
					<th style="text-align:center; vertical-align: middle;;">Alamat</th>
					<th style="text-align:center; vertical-align: middle;;">No. Medrec</th>
					<th style="text-align:center; vertical-align: middle;;">Nama Pasien</th>
					<th style="text-align:center; vertical-align: middle;;">Tanggal Lahir</th>
					<th style="text-align:center; vertical-align: middle;;">Alamat</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function() {
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}Trm_gabung_history/getIndex/' + '<?=$this->uri->segment(2)?>',
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "10%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 4,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 5,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 6,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 7,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 8,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 9,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 10,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 11,
					"orderable": true
				}
			]
		});
	});
</script>
