<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_retur" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_retur/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Proses Approval')?></label>
				<input type="hidden" class="form-control" id="tmp_idakun_kbo" placeholder="0" name="tmp_idakun_kbo" value="{idakun_kbo}">
				<div class="col-md-4">
					<select id="st_auto_posting" name="st_auto_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			<?
			$rows2=get_all('msumber_kas',array(),'nama','ASC');
			$rows=get_all('mdistributor',array(),'nama','ASC');
			?>
			<div class="form-group" hidden>
				<label class="col-md-2 control-label" ><?=text_primary('Akun Hutang (KBO)')?> </label>
				
				<div class="col-md-8">
					<select id="idakun_kbo" name="idakun_kbo" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
					<?foreach($list_akun as $r){?>
						<option value="<?=$r->id?>" <?=($r->id == $idakun_kbo? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
					<?}?>
					</select>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Hari Batas Waktu Batal')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal" placeholder="0" name="batas_batal" value="<?=$batas_batal?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('SETTING AKUN HUTANG')?></h4></label>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_hutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe</th>															
								<th style="width: 20%;">Distributor </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="tipe_distributor_hutang" id="tipe_distributor_hutang" style="width: 100%" id="step" class="js-select2 form-control input-sm tipe_distributor">										
										<option value="#" selected>- Pilih -</option>																			
										<option value="1">Pemesanan Gudang</option>																			
										<option value="3">Pengajuan</option>																			
										
									</select>										
								</td>
								<td>
									<select name="iddistributor_hutang" id="iddistributor_hutang" style="width: 100%" id="step" class="js-select2 form-control input-sm distirbutor">										
										<option value="#" selected>- All Distributor -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_hutang" id="idakun_hutang" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_hutang" placeholder="0" name="id_edit_hutang" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_hutang" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_hutang" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_akun();
		load_hutang();
		clear_input_hutang();

	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting=$("#st_auto_posting").val();
		var idakun_kbo=$("#idakun_kbo").val();
		var batas_batal=$("#batas_batal").val();
		$("#tmp_idakun_kbo").val(idakun_kbo)
		$.ajax({
			url: '{site_url}msetting_jurnal_retur/update',
			type: 'POST',
			data: {
				st_auto_posting:st_auto_posting,idakun_kbo:idakun_kbo,batas_batal:batas_batal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_kbo").val($("#tmp_idakun_kbo").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_kbo").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_retur/list_akun',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	
	
	$(".tipe_distributor").change(function(){
		list_distributor($(this).val());
	});

	function list_distributor($tipe_distributor){
		$.ajax({
			url: '{site_url}msetting_jurnal_hutang/list_distributor/'+$tipe_distributor,
			dataType: "json",
			success: function(data) {
				$(".distirbutor").empty();
				$('.distirbutor').append('<option value="#" selected>- All Distributor -</option>');
				$('.distirbutor').append(data.detail);
				
			}
		});		
	}
	function validate_add_hutang()
	{
		
		
		if ($("#tipe_distributor_hutang").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idakun_hutang").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_hutang(){
		$("#tipe_distributor_hutang").val('#').trigger('change');	
		$("#iddistributor_hutang").val('#').trigger('change');	
		$("#id_edit_hutang").val('');		
		$("#idakun_hutang").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_hutang",function(){
		if (validate_add_hutang()==false)return false;
		var id_edit=$("#id_edit_hutang").val();
		var tipe_distributor=$("#tipe_distributor_hutang").val();
		var iddistributor=$("#iddistributor_hutang").val();
		var idkategori=$("#idkategori_hutang").val();
		var idbarang=$("#idbarang_hutang").val();
		var idakun=$("#idakun_hutang").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_retur/simpan_hutang',
			type: 'POST',
			data: {
				id_edit:id_edit,iddistributor:iddistributor,
				idkategori: idkategori,idbarang:idbarang,idakun:idakun,tipe_distributor:tipe_distributor,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_hutang').DataTable().ajax.reload( null, false );
					clear_input_hutang();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_hutang($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_retur/hapus_hutang/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_hutang').DataTable().ajax.reload( null, false );
						clear_input_hutang();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_hutang",function(){
		
		clear_input_hutang();
	});
	function load_hutang(){
		var idlogic=$("#id").val();
		
		$('#tabel_hutang').DataTable().destroy();
		var table = $('#tabel_hutang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_retur/load_hutang/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
	
</script>
