<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tipe Tagihan</label>
                    <div class="col-md-8">
                        <select id="tipe_tagihan" name="tipe_tagihan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1">Tagihan Pegawai</option>
							<option value="2">Tagihan Dokter</option>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Karyawan  / Dokter</label>
                    <div class="col-md-8">
                        <select id="peg_id" name="peg_id" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
							
						</select>
                    </div>
                </div>
                                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID</th>
                    <th width="10%">TIPEPEG</th>
                    <th width="10%">#</th>
                    <th width="10%">TIPE</th>
                    <th width="10%">NIP</th>
                    <th width="10%">NAMA </th>
                    <th width="10%">SUMMARY</th>                    
                    <th width="15%" class="text-center">AKSI</th>                    
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
  
});
 $("#peg_id").select2({
	minimumInputLength: 2,
	noResults: 'Dokter Tidak Ditemukan.',
	ajax: {
		url: '{site_url}tpiutang_verifikasi/js_pegawai_dokter/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
	 data: function (params) {
		  var query = {
			search: params.term,tipe:$("#tipe_tagihan").val()
		  }
		  return query;
		},
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.nama ,
						id: item.id
					}
				})
			};
		}
	}
});
$("#xidpeg_dok").select2({
	minimumInputLength: 2,
	noResults: 'Dokter Tidak Ditemukan.',
	ajax: {
		url: '{site_url}tpiutang_verifikasi/js_pegawai_dokter/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
	 data: function (params) {
		  var query = {
			search: params.term,tipe:$("#xtipepegawai").val()
		  }
		  return query;
		},
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.nama ,
						id: item.id
					}
				})
			};
		}
	}
});
function load_index(){
	
	var tipe_tagihan=$("#tipe_tagihan").val();
	var peg_id=$("#peg_id").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1], "visible": false },
							{ "width": "5%", "targets": [2] },
							{ "width": "10%", "targets": [3,4,6] },
							{ "width": "15%", "targets": [5,7] },
						 {"targets": [5], className: "text-left" },
						 {"targets": [2,6], className: "text-right" },
						 {"targets": [3,4,7], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpiutang_history/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						
						tipe_tagihan:tipe_tagihan
						,peg_id:peg_id
						
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

$(document).on("change", "#xtipepegawai", function() {
	$("#xidpeg_dok").val("#").trigger('change');	
});
$(document).on("change", "#tipe_tagihan", function() {
	$("#peg_id").val(null).trigger('change');	
});

</script>