<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
       
        <div class="row">
            <div class="col-sm-6 col-lg-2">
				<div class="block block-rounded block-bordered">
					<div class="block-header bg-gray-lighter">
						<ul class="block-options">
							<li>
								<button type="button"><i class="fa fa-calendar"></i></button>
							</li>
						</ul>
						<h3 class="block-title">History Tagihan</h3>
					</div>
					<div class="block-content">
						<div class="pull-r-l pull-t push">
							<table class="block-table text-center bg-primary border-b">
								<tbody>
									<tr>
										<td class="border-r bg-success" style="width: 20%;">
											<div class="push-30 push-30-t">
												<i class="fa fa-building fa-3x text-white-op"></i>
											</div>
										</td>
										<td>
											<div class="h5 text-left text-white  font-w500"><?=$nama?></div>
											<div class="text-left text-white  font-w200">NIP :<?=$nip?></div>
											<input class="form-control" type="hidden" name="id_peg" id="id_peg" value="{id}" >
											<input class="form-control" type="hidden" name="tipepegawai" id="tipepegawai" value="{tipepegawai}" >
										</td>
									</tr>
									<tr>
										<td colspan="2" class="border-r bg-danger">
											<select id="tahun" name="tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua Tahun -</option>
												<?for($x =2020; $x <=  date('Y'); $x++){?>
													<option value="<?=$x?>" <?=($x==date('Y')?'selected':'')?>><?=$x?></option>									
												<?}?>
											
											</select>
										</td>
									<tr>
								</tbody>
							</table>
							
						</div>
						
					</div>
					<div class="block-content">
						
						<div class="pull-r-l pull-t push">
							
							<table class="table block-table text-center" id="index_left">
								<tbody>
												
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-lg-10">
				<div class="block block-rounded block-bordered">
					<div class="block-header bg-gray-lighter">
						<ul class="block-options">
							<li>
								<button type="button"><i class="fa fa-history"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Detail Tagihan</h3>
					</div>
					<div class="block-content">
						<div class="row">
							<input type="hidden" class="form-control detail" id="ym" name="ym" value="{ym}">
							<div class="col-md-6">
								<h3 id="text_header"></h3>
							</div>
							<div class="col-md-6">
								<button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;float:right;"><i class="fa fa-print"></i> Cetak History</button>
							</div>
							<hr>
							<div class="col-md-12">
								<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_list">
									<thead>
										<tr>
											<th>#</th>
											<th>#</th>
											<th>NO PENAGIHAN</th>
											<th>NO REG</th>
											<th>LAYANAN</th>
											<th>NOMINAL</th>
											<th>STATUS TAGIHAN</th>
											<th>STATUS</th>
											<th>STATUS CICILAN</th>
											<th>STATUS BAYAR</th>
											<th>AKSI </th>
											
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
	
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){	
   load_tahun();
   clear_data();
});
$("#tahun").change(function() {
	clear_data();
	load_tahun();
});
function load_index(){
	var tipepegawai=$("#tipepegawai").val();
	var id_peg=$("#id_peg").val();
	var ym=$("#ym").val();
	
	// alert(ym);
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [
							 { targets: [0,1], visible: false},
							 {  className: "text-center", targets:[1,2,3,4,6,8,9] },
							 {  className: "text-right", targets:[5] },
							 { "width": "3%", "targets": [1] },
							 { "width": "8%", "targets": [3] },
						 ],
            ajax: { 
                url: '{site_url}tpiutang_history/load_detail_trx', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tipepegawai:tipepegawai,ym:ym,id_peg:id_peg,
					   }
            }
        });
}
function clear_data(){
	$("#text_header").html('');
		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable( {
			"searching": false,
			columnDefs: [
				 { targets: [0,1], visible: false},
				 {  className: "text-center", targets:[2,3,5,6,7,8,9] },
				 {  className: "text-right", targets:[1,4] },
				 { "width": "3%", "targets": [1] },
				 { "width": "8%", "targets": [3] },
				 // { "width": "10%", "targets": [11] }
			]
		} );
	}
function load_tahun(){
	var tahun=$("#tahun").val();
	var tipepegawai=$("#tipepegawai").val();
	var id_peg=$("#id_peg").val();
	$('#index_left tbody').empty();
	$.ajax({
		url: '{site_url}tpiutang_history/load_left',
		type: 'POST',
		dataType: "json",
		data: {
			id_peg: id_peg,
			tipepegawai: tipepegawai,
			tahun: tahun,
			
		},
		success: function(data) {
			var content='';
			$.each(data, function (i,row) {
				content='';
				content +='<tr>';										
				content +='<td><button class="btn btn-block btn-primary btn_bulan" type="button" value="'+row.ym+'"><i class="fa fa-calendar pull-left"></i>'+get_nama_bulan(row.m)+' '+row.y+'</button></td>';
				content +='</tr>';
				$('#index_left tbody').append(content);
			console.log(row.ym);
					
			});
		}
	});

	// alert(tahun);
}
$(document).on("click",".btn_bulan",function(){	
	var ym=$(this).val();
	$("#ym").val(ym);
	generate_bulan(ym);
	load_index();
});
function generate_bulan($ym){
	var str=$ym;
	$bulan=get_nama_bulan(str.substring(str.length - 2, str.length));
	$tahun=str.substring(0,4);
	$("#text_header").html('<span class="label label-default">'+$bulan+' '+$tahun+'</span>');
	
}
$(document).on("click",".serahkan",function(){	
	var table = $('#index_list').DataTable();
	tr = table.row( $(this).parents('tr') ).index()
	var id=table.cell(tr,0).data();
	// alert(id);
	load_file_serahkan(id);		
});
function load_file_serahkan($id){
		var id=$id;
		 
		$('#modal_serahkan').modal('show');
		$('#list_file tbody').empty();
		$.ajax({
			url: '{site_url}tkontrabon/get_data_serahkan/'+id,
			dataType: "json",
			success: function(data) {
				$("#penerima").val(data.header.nama_penerima);
				$("#tgl_serahkan").val(data.header.tanggal_penyerahan);
				if (data.detail!=''){
					$("#list_file tbody").append(data.detail);
					$("#box_file2").attr("hidden",false);
				}else{
					$("#box_file2").attr("hidden",true);
					
				}
				// console.log(data);
				
			}
		});
	}
function get_nama_bulan($bulan){
	var text;
	// var bulan = "0";
	switch ($bulan) {
	  case '01':
		text = "JANUARI";
		break;
	  case '02':
		text = "FEBRUARI";
		break;
	  case '03':
		text = "MARET";
		break;
	  case '04':
		text = "APRIL";
		break;
	  case '05':
		text = "MEI";
		break;
	  case '06':
		text = "JUNI";
		break;
	   case '07':
		text = "JULI";
		break;
	  case '08':
		text = "AGUSTUS";
		break;
	  case '09':
		text = "SEPTEMBER";
		break;
	  case '10':
		text = "OKTOBER";
		break;
	  case '11':
		text = "NOVEMBER";
		break;
	  case '12':
		text = "DESEMBER";
		break;
	  default:
		text = " ";
	}
	return text;
}
</script>