<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1987'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1989'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_label" ><i class="si si-note"></i> Label</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1988'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1987'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_pindah_dpjp/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header" name="judul_header" value="{judul_header}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="footer_ina">Footer<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="footer_ina" name="footer_ina" value="{footer_ina}" placeholder="Footer INA">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="footer_eng">Footer <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="footer_eng" name="footer_eng" value="{footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('1759'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1989'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_label">
			
			<?php echo form_open('setting_pindah_dpjp/save_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Paragraf 1</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_1_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paragraf_1_ina" name="paragraf_1_ina" value="{paragraf_1_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_1_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paragraf_1_eng" name="paragraf_1_eng" value="{paragraf_1_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Pilih Yang Bertandatangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="pilih_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="pilih_ttd_ina" name="pilih_ttd_ina" value="{pilih_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="pilih_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="pilih_ttd_eng" name="pilih_ttd_eng" value="{pilih_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_ttd_ina" name="nama_ttd_ina" value="{nama_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_ttd_eng" name="nama_ttd_eng" value="{nama_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanggal Lahir</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttl_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttl_ttd_ina" name="ttl_ttd_ina" value="{ttl_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttl_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttl_ttd_eng" name="ttl_ttd_eng" value="{ttl_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Umur</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_ttd_ina" name="umur_ttd_ina" value="{umur_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_ttd_eng" name="umur_ttd_eng" value="{umur_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Alamat</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="alamat_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="alamat_ttd_ina" name="alamat_ttd_ina" value="{alamat_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="alamat_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="alamat_ttd_eng" name="alamat_ttd_eng" value="{alamat_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Provinsi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="provinsi_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="provinsi_ttd_ina" name="provinsi_ttd_ina" value="{provinsi_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="provinsi_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="provinsi_ttd_eng" name="provinsi_ttd_eng" value="{provinsi_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Kabupaten / Kota</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="kab_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="kab_ttd_ina" name="kab_ttd_ina" value="{kab_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="kab_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="kab_ttd_eng" name="kab_ttd_eng" value="{kab_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Kecamatan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="kec_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="kec_ttd_ina" name="kec_ttd_ina" value="{kec_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="kec_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="kec_ttd_eng" name="kec_ttd_eng" value="{kec_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Kelurahan / Desa</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="desa_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="desa_ttd_ina" name="desa_ttd_ina" value="{desa_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="desa_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="desa_ttd_eng" name="desa_ttd_eng" value="{desa_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Kode Pos</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="kodepos_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="kodepos_ttd_ina" name="kodepos_ttd_ina" value="{kodepos_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="kodepos_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="kodepos_ttd_eng" name="kodepos_ttd_eng" value="{kodepos_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">RT</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="rt_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="rt_ttd_ina" name="rt_ttd_ina" value="{rt_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="rt_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="rt_ttd_eng" name="rt_ttd_eng" value="{rt_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Rukun Warga</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="rw_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="rw_ttd_ina" name="rw_ttd_ina" value="{rw_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="rw_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="rw_ttd_eng" name="rw_ttd_eng" value="{rw_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Rukun Tetangga</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="rt_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="rt_ttd_ina" name="rt_ttd_ina" value="{rt_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="rt_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="rt_ttd_eng" name="rt_ttd_eng" value="{rt_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Hubungan Dengan Pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="rt_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="hubungan_ina" name="hubungan_ina" value="{hubungan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="rt_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="hubungan_eng" name="hubungan_eng" value="{hubungan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Paragraf 2</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_2_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " name="paragraf_2_ina"  value="<?=$paragraf_2_ina?>" placeholder="paragraf_2_ina"> 
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_2_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " name="paragraf_2_eng"  value="<?=$paragraf_2_eng?>" placeholder="paragraf_2_eng">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Dari Dokter</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="dari_dokter_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="dari_dokter_ina" name="dari_dokter_ina" value="{dari_dokter_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="dari_dokter_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="dari_dokter_eng" name="dari_dokter_eng" value="{dari_dokter_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Menjadi Dokter</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="jadi_dokter_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " name="jadi_dokter_ina"  value="<?=$jadi_dokter_ina?>" placeholder="jadi_dokter_ina"> 
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="jadi_dokter_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " name="jadi_dokter_eng"  value="<?=$jadi_dokter_eng?>" placeholder="jadi_dokter_eng"> 
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Paragraf 3</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_3_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="paragraf_3_ina" name="paragraf_3_ina" value="{paragraf_3_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_3_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paragraf_3_eng" name="paragraf_3_eng" value="{paragraf_3_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nomor Register</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="noreg_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " name="noreg_ina"  value="<?=$noreg_ina?>" placeholder="noreg_ina"> 
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="noreg_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " name="noreg_eng"  value="<?=$noreg_eng?>" placeholder="noreg_eng"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nomor Rekam Medis</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nomedrec_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nomedrec_ina" name="nomedrec_ina" value="{nomedrec_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nomedrec_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nomedrec_eng" name="nomedrec_eng" value="{nomedrec_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pasien_ina" name="nama_pasien_ina" value="{nama_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pasien_eng" name="nama_pasien_eng" value="{nama_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanggal Lahir</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttl_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttl_pasien_ina" name="ttl_pasien_ina" value="{ttl_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttl_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttl_pasien_eng" name="ttl_pasien_eng" value="{ttl_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Umur</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_pasien_ina" name="umur_pasien_ina" value="{umur_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_pasien_eng" name="umur_pasien_eng" value="{umur_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Jenis Kelamin</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="jk_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jk_ina" name="jk_ina" value="{jk_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="jk_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jk_eng" name="jk_eng" value="{jk_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Ruangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ruangan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ruangan_ina" name="ruangan_ina" value="{ruangan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ruangan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ruangan_eng" name="ruangan_eng" value="{ruangan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Kelas</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="kelas_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="kelas_ina" name="kelas_ina" value="{kelas_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="kelas_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="kelas_eng" name="kelas_eng" value="{kelas_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Bed</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="bed_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="bed_ina" name="bed_ina" value="{bed_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="bed_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="bed_eng" name="bed_eng" value="{bed_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Paragraf 4</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_4_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paragraf_4_ina" name="paragraf_4_ina" value="{paragraf_4_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_4_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paragraf_4_eng" name="paragraf_4_eng" value="{paragraf_4_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Yang Membuat Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="yg_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="yg_pernyataan_ina" name="yg_pernyataan_ina" value="{yg_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="yg_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="yg_pernyataan_eng" name="yg_pernyataan_eng" value="{yg_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Saksi Rumah Sakit</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="saksi_rs_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="saksi_rs_ina" name="saksi_rs_ina" value="{saksi_rs_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="saksi_rs_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="saksi_rs_eng" name="saksi_rs_eng" value="{saksi_rs_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanda Tangan DPJP Lama</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_dpjp_lama_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_dpjp_lama_ina" name="ttd_dpjp_lama_ina" value="{ttd_dpjp_lama_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_dpjp_lama_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_dpjp_lama_eng" name="ttd_dpjp_lama_eng" value="{ttd_dpjp_lama_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanda Tangan DPJP Baru</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_dpjp_baru_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_dpjp_baru_ina" name="ttd_dpjp_baru_ina" value="{ttd_dpjp_baru_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_dpjp_baru_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_dpjp_baru_eng" name="ttd_dpjp_baru_eng" value="{ttd_dpjp_baru_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
				<div class="col-md-12">
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="btn_save_label"></label>
						<div class="col-md-10">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1988'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('1761'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1770'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('DOKTER PELAKSANA TINDAKAN')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
							<label for="st_setting_dokter">Pengaturan Dokter Pelaksana</label>
							<select id="st_setting_dokter" name="st_setting_dokter" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_dokter=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_dokter=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_dokter=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12" id="div_dokter">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_dokter">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_dokter" name="profesi_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_dokter" name="spesialisasi_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_dokter" name="mppa_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_dokter()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_success('SETTING PEMBERI INFORMASI')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						
						<div class="col-md-12">
							<label for="st_setting_pemberi_info">Pengaturan Pemberi Informasi</label>
							<select id="st_setting_pemberi_info" name="st_setting_pemberi_info" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_pemberi_info=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_pemberi_info=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_pemberi_info=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12"  id="div_pemberi">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_pemberi">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_pemberi" name="profesi_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_pemberi" name="spesialisasi_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_pemberi" name="mppa_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_pemberi()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_danger('SETTING PETUGAS YANG MENDAMPINGI')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						
						
						<div class="col-md-12">
							<label for="st_setting_petugas_pendamping">Pengaturan Petugas yang Mendampingi</label>
							<select id="st_setting_petugas_pendamping" name="st_setting_petugas_pendamping" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_petugas_pendamping=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_petugas_pendamping=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_petugas_pendamping=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12" id="div_pendamping">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_pendamping">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_pendamping" name="profesi_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_pendamping" name="spesialisasi_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_pendamping" name="mppa_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_pendamping()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
			
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

})	

$(".auto_blur").change(function(){
	set_user();
	simpan_user_head();
	
});
function simpan_user_head(){
	let st_setting_dokter=$("#st_setting_dokter").val();
	let st_setting_pemberi_info=$("#st_setting_pemberi_info").val();
	let st_setting_petugas_pendamping=$("#st_setting_petugas_pendamping").val();
	$.ajax({
		url: '{site_url}setting_pindah_dpjp/simpan_user_head', 
		dataType: "JSON",
		method: "POST",
		data : {
				st_setting_dokter:st_setting_dokter,
				st_setting_pemberi_info:st_setting_pemberi_info,
				st_setting_petugas_pendamping:st_setting_petugas_pendamping,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==true){
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
$("#profesi_id_dokter").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_pindah_dpjp/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_dokter").empty();
				$("#mppa_id_dokter").append(data);
			}
		});
	}else{
		
				$("#mppa_id_dokter").empty();
	}

});
$("#profesi_id_pemberi").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_pindah_dpjp/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pemberi").empty();
				$("#mppa_id_pemberi").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pemberi").empty();
	}

});
$("#profesi_id_pendamping").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_pindah_dpjp/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pendamping").empty();
				$("#mppa_id_pendamping").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pendamping").empty();
	}

});
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
	// load_formulir();	
}

// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_pindah_dpjp/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_pindah_dpjp/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_pindah_dpjp/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_default(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_pindah_dpjp/hapus_default',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil_default').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_pindah_dpjp/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$("#operand_tahun").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_bulan").val($(this).val()).trigger('change');
		$("#operand_hari").val($(this).val()).trigger('change');
		$("#umur_bulan").val($(this).val()).trigger('change');
		$("#umur_hari").val($(this).val()).trigger('change');
		$(".bulan").attr('disabled','disabled');
		$(".hari").attr('disabled','disabled');
	}else{
		$(".bulan").removeAttr('disabled');
		// $(".hari").removeAttr('disabled');
	}

});
$("#operand_bulan").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_hari").val($(this).val()).trigger('change');
		$(".hari").attr('disabled','disabled');
		$("#umur_hari").val($(this).val()).trigger('change');
	}else{
		$(".hari").removeAttr('disabled');
	}

});
$("#st_spesifik_pindah_dpjp").change(function(){
	if ($(this).val()=='1'){
		$(".setting_tidak").show();
	}else{
		$(".setting_tidak").hide();
	}

});
$("#btn_tambah_formulir").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let pertemuan_id=$("#pertemuan_id").val();
	let st_tujuan_terakhir=$("#st_tujuan_terakhir").val();
	let operand=$("#operand").val();
	// alert(operand);
	let lama_terakhir_tujan=$("#lama_terakhir_tujan").val();
	let st_formulir=$("#st_formulir").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tujuan", "error");
		return false;
	}
	
	if (st_formulir=='0'){
		sweetAlert("Maaf...", "Tentukan Status Formulir", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_pindah_dpjp/simpan_formulir', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe:idtipe,
				idpoli:idpoli,
				statuspasienbaru:statuspasienbaru,
				pertemuan_id:pertemuan_id,
				st_tujuan_terakhir:st_tujuan_terakhir,
				operand:operand,
				lama_terakhir_tujan:lama_terakhir_tujan,
				st_formulir:st_formulir,

			
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				$("#idtipe").val('#').trigger('change');
				$("#idpoli").val('#').trigger('change');
				$("#statuspasienbaru").val('#').trigger('change');
				$("#pertemuan_id").val('#').trigger('change');
				$("#operand").val('0').trigger('change');
				$("#st_tujuan_terakhir").val('0').trigger('change');
				$("#lama_terakhir_tujan").val('0');
				$("#st_formulir").val('0').trigger('change');
				$('#index_tampil').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});

});
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}setting_pindah_dpjp/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
$("#idtipe_default").change(function(){
		$.ajax({
			url: '{site_url}setting_pindah_dpjp/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli_default").empty();
				$("#idpoli_default").append(data);
			}
		});

});
function load_formulir(){
	$('#index_tampil').DataTable().destroy();	
	table = $('#index_tampil').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [3,2,4,5,6] },
					// { "width": "15%", "targets": [1]},
					// { "width": "8%", "targets": [8]},
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}setting_pindah_dpjp/load_formulir', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_formulir(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_pindah_dpjp/hapus_formulir',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>