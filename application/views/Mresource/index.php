<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
        <ul class="block-options">
            <li>
                <a href="javascript:add_resource()" class="btn"><i class="fa fa-plus"></i></a>
            </li>
        </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_table">
			<thead>
				<tr>
					<th>#</th>
					<th>Resource</th>
					<th>Tipe</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>


<div class="modal in" id="add_resource" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="javascript:save_resource()" id="form1">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Resource</h3>
                </div>
                <div class="block-content">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Resource</label>
                        <div class="col-md-7">
                            <input class="form-control" type="text" name="resource" placeholder="Resource..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tipe</label>
                        <div class="col-md-7">
                            <select class="form-control" name="tipe" style="width:100%"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis</label>
                        <div class="col-md-7">
                            <select class="form-control" name="jenis" style="width:100%"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Parent ID</label>
                        <div class="col-md-7">
                            <select class="form-control" name="path" style="width:100%"></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Back</button>
                <button class="btn btn-sm btn-success" type="submit">Simpan</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
		index_table();

        $('select[name=tipe]').select2({
            placeholder: 'Select an Option',
            data: [
                {id: '1', text: 'Module'},
                {id: '2', text: 'Controller'},
                {id: '3', text: 'Action'},
                {id: '0', text: 'Other'},
            ]
        }).val(null).trigger('change')

        $('select[name=jenis]').select2({
            placeholder: 'Select an Option',
            data: [
                {id: '1', text: 'Kelompok'},
                {id: '2', text: 'Rincian'},
            ]
        }).val(null).trigger('change')

        $('select[name=path]').select2({
            placeholder: 'Select an Option',
            allowClear: true,
            ajax: {
                url: '{site_url}mresource/select_path/',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })
	})

    function save_resource() {
        var form = $('#form1')[0];
        var formData = new FormData(form);
        $.ajax({
            url: '{site_url}mresource/save_resource',
            method: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(res) {
                if(res.status == 'success') {
                    swal('Berhasil',res.msg,'success');
                    $('#add_resource').modal('hide')
                    index_table()
                } else {
                    swal('Error',res.msg,'error');
                    $('#add_resource').modal('hide')
                    index_table()
                }
            }
        })
    }


    function index_table() {

        table = $('#index_table').DataTable({
            searching: true,
            pageLength: 50,
            autoWidth: false,
            scrollX: true,
            destroy: true,
            ajax: {
                url: '{site_url}mresource/list_resource',
                type: 'POST',
            },
            columns: [
                {data: 'path', visible: false, orderable: true},
                {
                	data: 'resource',
                	render: function(data,type,row) {
                        var link;
                        if(row.tipe !== '1') link = '#';
                        else link = '{site_url}'+data;
                        var tipe = '---';
                        var resource = tipe.repeat(row.tipe) + data;
                		return '<a href="'+link+'">'+resource+'</a>';
                	}
            	},
                {
                	data: 'tipe',
                	render: function(data) {
                		if(data == 1) return 'Module';
                        else if(data == 2) return 'Controller';
                        else if(data == 3) return 'Action';
                		else return 'Other';
                	}
                },
                {
                	className: 'text-right',
                    render: function(data) {
                        var aksi = '<div class="btn-group">';
                        aksi += '<a href="javascript:delete_resource()" title="Hapus"><i class="fa fa-trash-o"></i></a>';
                        aksi += '</div>';
                        return aksi;
                    }
                },
            ]
        });
    }

    function add_resource() {
        $('#add_resource').modal('show');
        $('#form1 select').val(null).trigger('change')
        $('#form1 input').val(null)
    }

    function delete_resource() {
        alert('Jangan dihapus!')
    }
</script>
