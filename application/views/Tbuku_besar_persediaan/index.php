<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">		
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('Tbuku_besar_persediaan/export','class="form-horizontal" id="form-work" target="_blank"') ?>
           
			<div class="col-md-10">				
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Periode</label>
                    <div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>					
					</div>
                </div>
				
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-2 control-label" for="tanggal">User</label>
                    <div class="col-md-4">
						<select name="iduser" id="iduser" class="js-select2 form-control" style="width: 100%;">
							<option value="#" selected>- Pilih User -</option>
						<?foreach(get_all('musers') as $row){?>
							<option value="<?=$row->id?>"><?=$row->name?></option>
						
						<?}?>
					</select>
					</div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for=""></label>
                    <div class="col-md-2">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="width:100%"><i class="fa fa-filter"></i> Filter</button>
					</div>
					<div class="col-md-2">
						<a href="{base_url}tbuku_besar_persediaan/create"  class="btn btn-danger" type="submit" name="btn_export" style="width:100%" ><i class="fa fa-plus"></i> Create</a>
					</div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>No</th>
					<th>No</th>
					<th>Periode</th>
					<th>Nilai Persediaan</th>
					<th>User</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	load_detail();
})
function hapus($id){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Persediaan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Tbuku_besar_persediaan/hapus/'+$id,
				dataType: "json",
				success: function(data) {
					$("#cover-spin").hide();
					swal("Berhasil!", "Hapus Data Transaksi!", "success");
					$('#index_list').DataTable().ajax.reload( null, false );
				}
			});
		});
		

}
$(document).on("click","#btn_filter",function(){	
	load_detail();		
});
function load_detail() {
	// alert('sini');
	var iduser=$("#iduser").val();
	var tanggal_trx1=$("#tanggal_trx1").val();
	var tanggal_trx2=$("#tanggal_trx2").val();
	
	$('#index_list').DataTable().destroy();
	var table = $('#index_list').DataTable({
	"pageLength": 100,
	"ordering": false,
	"processing": true,
	"serverSide": true,
	"autoWidth": false,
	"fixedHeader": true,
	"searching": false,
	"order": [],
	"ajax": {
		url: '{site_url}Tbuku_besar_persediaan/getIndex/',
		type: "POST",
		dataType: 'json',
		data: {
			tanggal_trx1: tanggal_trx1,
			tanggal_trx2: tanggal_trx2,
			iduser: iduser,
		}
	},
	columnDefs: [
				 {"targets": [0], "visible": false },
				 {  className: "text-right", targets:[1,3] },
				 {  className: "text-center", targets:[2,4,5] },
				 { "width": "5%", "targets": [1] },
				 { "width": "15%", "targets": [2,3] },
				 { "width": "25%", "targets": [4,5] },
				 // { "width": "15%", "targets": [2,13] }

				]
	});
}

</script>
