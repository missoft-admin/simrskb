<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$judul?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
            margin-top: 1,8em;
            margin-left: 2,3em;
            margin-right: 2em;
            margin-bottom: 2.5em;
        }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
		
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }


  </style>
</head>

<body>
	<table class="content">
		<tr>
			<td rowspan="3" width="30%" class="text-center"><img src="<?=$logo1_rs?>" alt="" width="60" height="60"></td>
			<td rowspan="2" colspan="3" width="50%" class="text-center text-bold text-judul"><u><?=$judul?></u></td>
			<td rowspan="2" width="20%"></td>
		</tr>
		<tr></tr>
		<tr>
			<td width="20%" class="text-left text-bold text-top"></td>
			<td width="80%" colspan="" class="text-top"></td>			
		</tr>
	</table>
	<table class="content">
		
		<tr>
			<td width="20%" class="text-left text-bold text-top">TIPE</td>
			<td width="80%" colspan="" class="text-top">: <?=$nama_tipe?></td>			
		</tr>
		<tr>
			<td width="20%" class="text-left text-bold text-top">KATEGORI</td>
			<td width="80%" colspan="" class="text-top">: <?=$nama_kategori?></td>			
		</tr>
		<tr>
			<td width="20%" class="text-left text-bold text-top">TANGGAL</td>
			<td width="80%" colspan="" class="text-top">: <?=$tanggal?></td>			
		</tr>
		
		
	</table>

	<br>

	<table class="content-2">
		
		<tr>
			<th class="border-full text-center" width="10%">UNIT</th>
			<th class="border-full text-center"  width="8%">TIPE</th>
			<th class="border-full text-center" width="8%">KATEGORI</th>
			<th class="border-full text-center" width="20%">BARANG</th>
			<th class="border-full text-center" width="5%">SATUAN</th>
			<th class="border-full text-center" width="8%">STOK</th>
			<th class="border-full text-center" width="10%">HPP</th>
			<th class="border-full text-center" width="10%">NILAI PERSEDIAAN</th>
		</tr>
		<?php 
		$no=0;
		
		foreach ($detail as $row) { 
		$no ++;
		?>
		
		<tr>
			<td class="border-full text-center"><?=($row->nama_unit);?></td>
			<td class="border-full text-left"><?=$row->nama_tipe?></td>
			<td class="border-full text-left"><?=$row->namakategori?></td>
			<td class="border-full text-left"><?=$row->nama_barang?></td>
			<td class="border-full text-left"><?=$row->namasatuan?></td>
			<td class="border-full text-right"><?=number_format($row->stok,2)?></td>
			<td class="border-full text-right"><?=number_format($row->hpp,2)?></td>
			<td class="border-full text-right"><?=number_format($row->nilai,2)?></td>
		</tr>
		<?php } ?>
		
	</table>
</body>

</html>
