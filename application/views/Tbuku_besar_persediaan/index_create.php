<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">		
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('Tbuku_besar_persediaan/export','class="form-horizontal" id="form-work" target="_blank"') ?>
			<input type="hidden" class="form-control" id="tanggal" placeholder="0" name="tanggal" value="<?=date('d-m-Y')?>">
			<input type="hidden" class="form-control" id="id" name="id" placeholder="0" name="id" value="<?=$id?>">
			<div class="col-md-10">	
				<?if ($id!=''){?>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-3">
						<div class="input-group date">
							<input class="js-datepicker form-control input" disabled type="text" id="tanggal_pengajuan" name="tanggal_pengajuan" value="<?=HumanDateShort($tanggal)?>" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>				
					</div>
					<label class="col-md-2 control-label" for="tanggal">Nilai Persediaan</label>
					<div class="col-md-3">
						<input type="text" class="form-control decimal" readonly id="nilai_persediaan" placeholder="0" name="nilai_persediaan" value="{nilai_persediaan}">	
					</div>
                </div>
				<?}?>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Tipe</label>
                    <div class="col-md-10">
						<select name="idtipe[]" id="idtipe" class="js-select2 form-control" style="width: 100%;" multiple>
							<?php foreach  (get_all('mdata_tipebarang') as $row) { ?>
							<option value="'<?=$row->id?>'"><?=$row->nama_tipe?></option>
							<?php } ?>						
						</select>					
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-2 control-label" for="tanggal">Kategori</label>
                    <div class="col-md-10">
						<select name="idkategori[]" id="idkategori" class="js-select2 form-control" style="width: 100%;" multiple>
							<?php foreach  (get_all('mdata_kategori',array('status'=>1)) as $row) { ?>
							<option value="'<?=$row->id?>'"><?=$row->nama?></option>
							<?php } ?>						
						</select>			
					</div>
                </div>
				
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for=""></label>
                    <div class="col-md-10">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
						<?if($id!=''){?>
						<button  class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
						<button  class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
						<button  class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
						<?}?>
					</div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="row">
			<div class="col-md-12">	
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-responsive" id="index_list">
						<thead>
							<tr>
								<th></th>
								<th>Kode Barang</th>
								<th>TIPE BARANG</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>HPP</th>
								<th>Nilai Persediaan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">	
			<div style="margin-top:50px;margin-bottom:20px;" class="text-right">
                <a href="{base_url}tbuku_besar_persediaan" class="btn btn-default btn-md"><i class="fa fa-reply"></i> Kembali</a>
				<?if($id==''){?>
                <button type="button" class="btn btn-success btn-md" id="btn_simpan"><i class="fa fa-save"></i> Generate Persediaan</button>
				<?}?>
            </div>
			</div>
		</div>
		
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_barang" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Rincian Barang</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">Tipe</label>
									<div class="col-md-9">
										<input type="text" class="form-control" readonly id="tipe_nama" name="tipe_nama" value="" />
										
									</div>
								</div>
															
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">Kategori</label>
									<div class="col-md-9">
										<input type="text" class="form-control" readonly id="kategori_nama" name="kategori_nama" value="" />
									</div>
								</div>
								
							</div>
						</form>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
										<div class="table-responsive">
											<table class="table table-bordered table-striped table-responsive" id="index_detail">
												<thead>
													<tr>
														<th></th>
														<th>Unit</th>
														<th>Nama Barang</th>
														<th>Satuan</th>
														<th>Stok</th>
														<th>HPP</th>
														<th>Nilai Persediaan</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
					</div>
					</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	getIndex_create();
	
})
 $('#index_list tbody').on('click', 'td.details-control', function () {
		var id=$("#id").val();
        var tr = $(this).closest('tr');
        var row = table.row(tr);
		var idtipe=table.cell(tr,0).data();
		// alert(idtipe);
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
			$("#cover-spin").show();
            // Open this row
			$.ajax({
			url: '{site_url}Tbuku_besar_persediaan/load_detail_kategori/'+idtipe+'/'+id,
			dataType: "json",
			success: function(data) {
				// alert(data);
				$('.tabel-kategori').DataTable().destroy();
				 row.child(data.detail).show();
				tr.addClass('shown');
				$("#cover-spin").hide();
				$('.tabel-kategori').DataTable({
					"pageLength": 10,
					"ordering": false,
					"processing": true,
					"serverSide": false,
					"autoWidth": false,
					"fixedHeader": true,
					"searching": false,
					columnDefs: [
						 // {"targets": [0], "visible": false },
							{  className: "text-right", targets:[2] },
							{ "width": "30%", "targets": [1,2] },
							{ "width": "5%", "targets": [0] },
							{ "width": "10%", "targets": [3] },
							 {  className: "text-center", targets:[3] },
						]
					}
				);
			}
		});


           
        }
} );

$(document).on("click","#btn_filter",function(){	
	getIndex_create();		
});

$(document).on("click","#btn_simpan",function(){	
	var tanggal=$("#tanggal").val();
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Generate Persediaan Per Tanggal "+tanggal+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			simpan();
		});
	
});
function simpan(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tbuku_besar_persediaan/simpan/',
		dataType: "json",
		success: function(data) {
			$("#cover-spin").hide();
			swal("Berhasil!", "Input Data Transaksi!", "success");
			window.location.href = "<?php echo site_url('tbuku_besar_persediaan')?>";
		}
	});

	// $("#cover-spin").hide();
	
}
function getIndex_create() {
	
	// alert('sini');
	var idtipe=$("#idtipe").val();
	var idkategori=$("#idkategori").val();
	var id=$("#id").val();
	
	$('#index_detail').DataTable().destroy();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
	"paging": false,
	"ordering": false,
	"processing": true,
	"serverSide": true,
	"autoWidth": false,
	"fixedHeader": true,
	"searching": false,
	"order": [],
	"ajax": {
		url: '{site_url}Tbuku_besar_persediaan/getIndex_create/',
		type: "POST",
		dataType: 'json',
		data: {
			id: id,
			idtipe: idtipe,
			idkategori: idkategori,
		}
	},
	columnDefs: [
				 {"targets": [0,1,3,4,5], "visible": false },
				 {  className: "text-right", targets:[1,4,5,6] },
				 {  className: "text-center", targets:[3] },
				 { "width": "5%", "targets": [1,7] },
				 { "width": "15%", "targets": [2] },
				 { "width": "8%", "targets": [3,4] },
				 { "width": "10%", "targets": [5,6] },
				 {  className: "details-control", targets:[7] },
				 // { "width": "15%", "targets": [2,13] }

				]
	});
}
function get_detail($idtipe,$tipe_nama,$idkategori,$kategori_nama){
	// alert($nama);
	
	$("#tipe_nama").val($tipe_nama);
	$("#kategori_nama").val($kategori_nama);
	$('#modal_barang').modal('show');
	getIndex_detail($idtipe,$idkategori);
}
function getIndex_detail($idtipe,$idkategori) {
	// alert('sini');
	var id=$("#id").val();
	var idtipe=$idtipe;
	var idkategori=$idkategori;
	
	$('#index_detail').DataTable().destroy();
	var table = $('#index_detail').DataTable({
	
	"pageLength": 10,
	"ordering": false,
	"processing": true,
	"serverSide": true,
	"autoWidth": false,
	"fixedHeader": true,
	"searching": true,
	"order": [],
	"ajax": {
		url: '{site_url}Tbuku_besar_persediaan/getIndex_detail/',
		type: "POST",
		dataType: 'json',
		data: {
			id: id,
			idtipe: idtipe,
			idkategori: idkategori,
		}
	},
	columnDefs: [
				 {"targets": [0], "visible": false },
				 {  className: "text-right", targets:[4,5,6] },
				 {  className: "text-center", targets:[3,7] },
				 { "width": "10%", "targets": [1] },
				 { "width": "15%", "targets": [2] },
				 { "width": "8%", "targets": [3,4] },
				 { "width": "10%", "targets": [5,6,7] },
				 // { "width": "15%", "targets": [2,13] }

				]
	});
}

</script>
