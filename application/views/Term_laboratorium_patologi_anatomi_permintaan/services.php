<script type="text/javascript">
function buatDraftPermintaan(pendaftaranId, pasienId, asalRujukan) {
    <?php
        $jenisPemeriksaan = list_variable_ref(86);
        $selected = array_search(1, array_column($jenisPemeriksaan, 'st_default'));
    ?>

    let pendaftaran_id = pendaftaranId;
    let pasien_id = pasienId;
    let asal_rujukan = asalRujukan;
    let jenis_pemeriksaan = '<?= $jenisPemeriksaan[$selected]->id; ?>';
    let rencana_pemeriksaan = '<?= date("Y-m-d"); ?>';

    swal({
        title: "Apakah Anda Yakin?",
        text: "Membuat Draft Permintaan?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#34a263",
        cancelButtonText: "Batalkan",
    }).then(function() {
        $("#cover-spin").show();
        $.ajax({
            url: '{site_url}term_laboratorium_patologi_anatomi/buat_draft_permintaan/2/1',
            dataType: "JSON",
            method: "POST",
            data: {
                pendaftaran_id: pendaftaran_id,
                pasien_id: pasien_id,
                asal_rujukan: asal_rujukan,
                jenis_pemeriksaan: jenis_pemeriksaan,
                rencana_pemeriksaan: rencana_pemeriksaan,
            },
            success: function(response) {
                let transaksiId = response.data;
                let asalRujukanStatus = (asalRujukan == 1 || asalRujukan == 2 ? 'rawat_jalan' : 'rawat_inap'); 
                window.location = '{base_url}term_laboratorium_patologi_anatomi/tindakan/' + asalRujukanStatus + '/' + pendaftaranId + '/erm_lab/patologi_anatomi_permintaan/' + transaksiId + '/input_permintaan';
            }
        });
    });
}

function lihatDataPermintaan(transaksiId) {
    $('#action-data-permintaan').html('<a href="#" class="btn btn-primary" onclick="cetakBuktiPermintaan(' + transaksiId + ')"><i class="fa fa-print"></i> Cetak Dokumen</a>');

    $.ajax({
        url: '{base_url}term_laboratorium_patologi_anatomi/daftar_pemeriksaan/' + transaksiId,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if (response.status == 'success') {
                var tableBody = $('#table-data-permintaan tbody');
                tableBody.empty();

                var totalHarga = 0;

                $.each(response.data, function(index, item) {
                    var row = '<tr>' +
                        '<td class="text-center">' + (index + 1) + '</td>' +
                        '<td class="text-center">' + item.nama_pemeriksaan + '</td>' +
                        '<td class="text-right">' + $.number(item.harga) + '</td>' +
                        '<td class="text-center">' + $.number(item.kuantitas) + '</td>' +
                        '<td class="text-center">' + $.number(item.diskon) + '</td>' +
                        '<td class="text-right">' + $.number(item.total_keseluruhan) + '</td>' +
                        '</tr>';
                    tableBody.append(row);

                    // Accumulate total harga
                    totalHarga += parseFloat(item.total_keseluruhan);
                });

                $('#table-data-permintaan tfoot td:eq(1)').text(totalHarga.toLocaleString());
            } else {
                alert('Error fetching data');
            }
        },
        error: function(xhr, status, error) {
            console.error(error);
            alert('Error fetching data');
        }
    });
}

function batalTransaksi(transaksiId) {
    swal({
        title: 'Batalkan Permintaan',
        text: 'Apakah Anda yakin ingin membatalkan permintaan ini? Jika dilakukan pembatalan, permintaan Anda tidak akan terkirim ke laboratorium tujuan. Tekan "Ya" untuk membatalkan.',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.ajax({
                url: '{site_url}term_laboratorium_patologi_anatomi/batal_draft_permintaan/' + transaksiId,
                success: function(result) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: 'Data berhasil dihapus.'
                    });

                    // Re-load Datatable
                    loadDataTableTransaksiPermintaan();
                },
                error: function(error) {
                    console.error('Error deleting order:', error);
                    alert('Error deleting order. Please try again.');
                }
            });
        }
    });
}

function prosesTransaksi(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_lab/patologi_anatomi_pemeriksaan/' + transaksiId + '/input_pemeriksaan', '_blank');
}

function editPermintaan(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_lab/patologi_anatomi_pemeriksaan/' + transaksiId + '/edit_pemeriksaan', '_blank');
}

function planOrder(pendaftaranId, pasienId, asalRujukan) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/plan_order/' + pendaftaranId + '/' + pasienId + '/' + asalRujukan, '_blank');
}

function switchOrder(pendaftaranId, pasienId, transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/switch_order/' + pendaftaranId + '/' + pasienId + '/' + transaksiId, '_blank');
}

function cetakBuktiPermintaan(transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/cetak_bukti_permintaan/' + transaksiId, '_blank');
}

function cetakBuktiPengambilanPemeriksaan(transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/cetak_bukti_pengambilan_pemeriksaan/' + transaksiId, '_blank');
}

function cetakBuktiPengambilanPemeriksaanSmall(transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/cetak_bukti_pengambilan_pemeriksaan_small/' + transaksiId, '_blank');
}

function lihatPermintaan(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_lab/patologi_anatomi_permintaan/' + transaksiId + '/lihat_permintaan', '_blank');
}

function stikerIdentitas(transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/stiker_identitas/' + transaksiId, '_blank');
}

function labelTabungDarah(transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/label_tabung_darah/' + transaksiId, '_blank');
}                    

function inputBMHPTagihan(pendaftaranId) {
    window.open('{base_url}tpoliklinik_trx/tindakan/' + pendaftaranId + '/erm_trx/input_bmhp', '_blank');
}                    

function inputBMHPNonTagihan(pendaftaranId) {
    window.open('{base_url}tpoliklinik_trx/tindakan/' + pendaftaranId + '/erm_trx/input_bmhp', '_blank');
}          

function riwayatLaboratorium(pendaftaranId, pasienId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/riwayat_laboratorium/' + pendaftaranId + '/' + pasienId, '_blank');
}          

function splitPermintaan(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_lab/patologi_anatomi_split/' + transaksiId, '_blank');
}          

function gabungPermintaan(pendaftaranId, transaksiId, pasienId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/gabung_permintaan/' + pendaftaranId + '/' + transaksiId + '/' + pasienId, '_blank');
}          

function dataLaboratorium(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_laboratorium_patologi_anatomi/data_laboratorium/' + asalRujukan + '/' + pendaftaranId + '/' + transaksiId, '_blank');
}
</script>
