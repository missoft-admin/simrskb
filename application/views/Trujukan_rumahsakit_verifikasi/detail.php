<?= ErrorSuccess($this->session); ?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>

<style media="screen">
.text-bold {
    font-weight: bold;
}

.separator-header {
    margin-top: 450px;
}

.modal {
    overflow: auto !important;
}

</style>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}trujukan_rumahsakit_verifikasi/index" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Pendaftaran</span>
                    <input type="text" class="form-control tindakanNoPendaftaran" value="{nopendaftaran}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{nomedrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Poliklinik</span>
                    <input type="text" class="form-control tindakanPoliklinik" value="{tipependaftaran} [{poliklinik}]" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                    <input type="text" class="form-control tindakanKelompokPasien" value="{kelompokpasien}" readonly="true">
                </div>
                <?if ($idkelompokpasien=='3') {?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">BPJS Kesehatan</span>
                    <input type="text" class="form-control tindakanBPJSKesehatan" value="{bpjs_kesehatan}" readonly="true">
                </div>
                <?}?>
                <?if ($idkelompokpasien=='4') {?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">BPJS Ketenagakerjaan</span>
                    <input type="text" class="form-control tindakanBPJSKetenagakerjaan" value="{bpjs_ketenagakerjaan}" readonly="true">
                </div>
                <?}?>
                <?if ($idkelompokpasien=='1') {?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Asuransi</span>
                    <input type="text" class="form-control tindakanAsuransi" value="{namarekanan}" readonly="true">
                </div>
                <?}?>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">User Transaksi</span>
                    <input type="text" class="form-control tindakanUserTransaksi" value="{usertransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Transaksi</span>
                    <input type="text" class="form-control tindakanTanggalTransaksi" value="{tanggaltransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
                    <input type="text" class="form-control tindakanDokter" value="{namadokter}" readonly="true">
                </div>
            </div>
        </div>

        <hr>

        <div class="content-verification">
            <b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalAdministrasi = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianAdministrasi($id) as $row) {?>
                    <?php $statusRujukan = '0'; ?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalAdministrasi = $totalAdministrasi + $row->totalkeseluruhan;?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalAdministrasi)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN POLIKLINIK / IGD</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoliTindakan = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalTindakan($id) as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $row->status_rujukan;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalPoliTindakan = $totalPoliTindakan + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalPoliTindakan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN POLIKLINIK / IGD</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoliObat = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalObat($id) as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $obat_poli;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalPoliObat = $totalPoliObat + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalPoliObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN POLIKLINIK / IGD</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoliAlkes = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalAlkes($id) as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $alkes_poli;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalPoliAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalLaboratorium = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianLaboratorium($id) as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $row->status_rujukan;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalLaboratorium = $totalLaboratorium + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalLaboratorium)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadiologi = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($id) as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $row->status_rujukan;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalRadiologi = $totalRadiologi + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalRadiologi)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FISIOTERAPI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFisioterapi = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFisioterapi($id) as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $row->status_rujukan;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->norujukan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalFisioterapi = $totalFisioterapi + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalFisioterapi)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiObat = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($id, 3) as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $obat_farmasi;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalFarmasiObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT RACIKAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiObatRacikan = 0;?>
                    <?php foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasiObatRacikan($id) as $row) { ?>
                    <?php $statusRujukan = '0'; ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalFarmasiObatRacikan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Nominal Transaksi</th>
                        <th style="width:5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiAlkes = 0;?>
                    <?php $dataFarmasiAlkesIGD = $this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($id, 1);?>
                    <?php foreach ($dataFarmasiAlkesIGD as $row) {?>
                    <?php 
                        if ($idkategori == 1) {
                            if ($tentukantarif == 0) {
                                $statusRujukan = '1';
                            } elseif ($tentukantarif == 1) {
                                $statusRujukan = $alkes_farmasi;
                            }
                        } else {
                            $statusRujukan = '0';
                        }
                    ?>
                    <tr>
                        <td><?=$row->nopenjualan?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="text-center"><?=StatusRujukanRS($statusRujukan)?></td>
                    </tr>
                    <?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="3"><b>TOTAL</b></td>
                        <td class="text-bold"><?=number_format($totalFarmasiAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <?php $totalKeseluruhan = $totalAdministrasi + $totalPoliTindakan + $totalPoliObat + $totalPoliAlkes + $totalLaboratorium + $totalRadiologi + $totalFisioterapi + $totalFarmasiObat + $totalFarmasiObatRacikan + $totalFarmasiAlkes; ?>
            <?php $totalFeeRujukan = $this->model->GetNominalRujukanRS(json_decode(json_encode([
                'idpendaftaran' => $idpendaftaran,
                'idkategori' => $idkategori,
                'jenis_berekanan' => $jenis_berekanan,
                'jumlah' => $jumlah,
                'persentase' => $persentase,
                'ratetarif' => $ratetarif,
                'tentukantarif' => $tentukantarif,
                'alkes_poli' => $alkes_poli,
                'obat_poli' => $obat_poli,
                'alkes_farmasi' => $alkes_farmasi,
                'obat_farmasi' => $obat_farmasi,
            ]), FALSE)); ?>

            <hr>

            <table class="table table-bordered table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL TRANSAKSI</b></td>
                        <td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px"><?=number_format($totalKeseluruhan)?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-size:15px"><b>NOMINAL FEE RUJUKAN</b></td>
                        <td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px"><?=number_format($totalFeeRujukan)?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>