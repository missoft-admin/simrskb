<div class="modal fade in black-overlay" id="EditTanggalPembayaranModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
        <div class="modal-content">
            <form class="form-horizontal" action="{base_url}trujukan_rumahsakit_verifikasi/updateData" method="POST">
                <div class="block block-themed">
                    <div class="block-header bg-primary">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">EDIT TANGGAL PEMBAYARAN</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nama Pasien</label>
                                    <div class="col-md-9">
                                        <input id="namapasien" class="form-control" readonly style="width: 100%;" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Asal Pasien</label>
                                    <div class="col-md-9">
                                        <select id="midasalpasien" name="idasalpasien"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="1">Datang Sendiri</option>
                                            <option value="2">RFKTP</option>
                                            <option value="3">RFKRTL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Rujukan</label>
                                    <div class="col-md-9">
                                        <select id="midrujukan" name="idrujukan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Pembayaran</label>
                                    <div class="col-md-9">
                                        <select id="tanggalPembayaranPerubahan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-sm btn-primary" name="submitWithVerifikasi" value="Simpan Verifikasi">
                    <input type="submit" class="btn btn-sm btn-success" name="submitOnly" value="Simpan">
                    <button class="btn btn-sm btn-default" id="cancelTanggalPembayaran" type="button" data-dismiss="modal">Batal</button>
                </div>

                <input type="hidden" id="idkasir" name="idkasir" readonly value="">
                <input type="hidden" id="idtransaksi" name="idtransaksi" readonly value="">
                <input type="hidden" id="periodePembayaran" name="periode_pembayaran" value="">
                <input type="hidden" id="periodeJatuhTempo" name="periode_jatuhtempo" value="">

            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$(document).on('change', '#midasalpasien', function() {
        const idasalpasien = $(this).val();
        const formRujukan = $("#midrujukan");

        if (idasalpasien == "0" || idasalpasien == "1") {
            formRujukan.empty();
        } else {
			getRujukan(idasalpasien, formRujukan);
		}
    });

	$(document).on('change', '#midrujukan', function() {
        const idasalpasien = $('#midasalpasien option:selected').val();
        const idrujukan = $('#midrujukan option:selected').val();
        getPeriodePembayaran(idasalpasien, idrujukan);
    });

    $(document).on('click', '.editTanggalPembayaran', function() {
        const formRujukan = $("#midrujukan");
        const idkasir = $(this).data('idkasir');
        const idtransaksi = $(this).data('idpendaftaran');
        const nomedrec = $(this).data('nomedrec');
        const namapasien = $(this).data('namapasien');
        const idasalpasien = $(this).data('idasalpasien');
        const idrujukan = $(this).data('idrujukan');
        const tanggalpembayaran = $(this).data('tanggalpembayaran');

        $("#idkasir").val(idkasir);
        $("#idtransaksi").val(idtransaksi);
        $("#namapasien").val(nomedrec + " - " + namapasien);
        $("#tanggalpembayaran").val(tanggalpembayaran);
        
        $("#midasalpasien").select2("destroy");
        $("#midasalpasien").val(idasalpasien);
        $("#midasalpasien").select2();
        
        if (idasalpasien == "0" || idasalpasien == "1") {
            formRujukan.empty();
        } else {
			getRujukan(idasalpasien, formRujukan, idrujukan);
		}
        
        getPeriodePembayaran(idasalpasien, idrujukan, tanggalpembayaran);
    });
});

function getPeriodePembayaran(idasalpasien, idrujukan, periodePembayaran = '') {
    $("#tanggalPembayaranPerubahan").empty();

    $.ajax({
        url: '{base_url}trujukan_rumahsakit_verifikasi/GetPeriodePembayaranFeeRujukanRS/' + idasalpasien + '/' + idrujukan + '/' + periodePembayaran,
        dataType: "json",
        success: function (data) {
            $('#tanggalPembayaranPerubahan').append(data.list_option);
        },
        complete: function (data) {
            setPeriodeHonor();
        }
    });
}

function setPeriodeHonor() {
    var periodePembayaran = $('#tanggalPembayaranPerubahan option:selected').val();
    var periodeJatuhTempo = $('#tanggalPembayaranPerubahan option:selected').data('jatuhtempo');
    $("#periodePembayaran").val(periodePembayaran);
    $("#periodeJatuhTempo").val(periodeJatuhTempo);
}

</script>
