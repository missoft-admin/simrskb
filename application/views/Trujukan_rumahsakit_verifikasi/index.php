<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') {
	echo ErrorMessage($error);
} ?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr>
        <div class="row">
            <?php echo form_open('Trujukan_rumahsakit_verifikasi/filter', 'class="form-horizontal" id="form-filter"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tanggal Pendaftaran</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pendaftaran_awal" placeholder="From" value="{tanggal_pendaftaran_awal}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pendaftaran_akhir" placeholder="To" value="{tanggal_pendaftaran_akhir}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nomedrec" value="{nomedrec}" placeholder="No. Medrec">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}" placeholder="Nama Pasien">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <input type="hidden" class="form-control" name="tipetransaksi" value="{tipetransaksi}">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idlayanan">Layanan</label>
                    <div class="col-md-8">
                        <select name="idlayanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Layanan</option>
                            <option value="1" <?=($idlayanan == 1 ? 'selected' : '')?>>Rawat Jalan</option>
                            <option value="2" <?=($idlayanan == 2 ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idasalpasien">Asal Pasien</label>
                    <div class="col-md-8">
                        <select name="idasalpasien" id="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Asal Pasien</option>
                            <option value="1" <?=($idasalpasien == 1 ? 'selected' : '')?>>Datang Sendiri</option>
                            <option value="2" <?=($idasalpasien == 2 ? 'selected' : '')?>>RFKTP</option>
                            <option value="3" <?=($idasalpasien == 3 ? 'selected' : '')?>>RFKRTL</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idrujukan">Rujukan</label>
                    <div class="col-md-8">
                        <select name="idrujukan" id="idrujukan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Rujukan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tanggal Pembayaran</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pembayaran_awal" placeholder="From" value="{tanggal_pembayaran_awal}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" autocomplete="off" name="tanggal_pembayaran_akhir" placeholder="To" value="{tanggal_pembayaran_akhir}">
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>

        <form class="" action="{base_url}Trujukan_rumahsakit_verifikasi/updateStatusAll" method="post">
            <hr>
            <input type="hidden" name="tipetransaksi" value="{tipetransaksi}">
            <button href="#" class="btn btn-primary" id="verifyAll" style="display:none;" type="submit"><i class="fa fa-check"></i> Proses Verifikasi</button>
            <hr>
			<div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>
                            <label class="css-input css-checkbox css-checkbox-success">
                                <input type="checkbox" id="checkAll"><span></span>
                            </label>
                        </th>
                        <th>Tanggal</th>
                        <th>No. Medrec</th>
                        <th>Nama Pasien</th>
                        <th>Layanan</th>
                        <th>Asal Pasien</th>
                        <th>Rujukan</th>
                        <th>Status Rekanan</th>
                        <th>Tanggal Pembayaran</th>
                        <th>Total Transaksi</th>
                        <th>Nominal Rujukan</th>
                        <th>Status Verifikasi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>
        </form>
        <br>
    </div>
</div>

<style>
    .highlightRow {
        background-color: #c6eb9c !important;
    }
</style>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}Trujukan_rumahsakit_verifikasi/getIndex/{uri}',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": false
            },
            {
                "width": "5%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "8%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "8%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 6,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 8,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "8%",
                "targets": 9,
                "orderable": true
            },
            {
                "width": "8%",
                "targets": 10,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 11,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 12,
                "orderable": true
            }
        ],
    });
});

$(document).ready(function() {
    $('#checkAll').click(function(event) {
        if (this.checked) {
            $('.checkboxTrx').each(function() {
                this.checked = true;
                $('#verifyAll').show();
            });
        } else {
            $('.checkboxTrx').each(function() {
                this.checked = false;
                $('#verifyAll').hide();
            });
        }
    });

    $(document).on('click', '.loading', function() {
        $("#cover-spin").show();
    });

	$(document).on('click', '.checkboxTrx', function() {
        if ($(".checkboxTrx:checked").length > 0) {
            $('#verifyAll').show();
        } else {
            $('#verifyAll').hide();
        }
    });

	$(document).on('change', '#idasalpasien', function() {
        const asalpasien = $(this).val();
        const formRujukan = $("#idrujukan");

        if (asalpasien == "0" || asalpasien == "1") {
            formRujukan.empty();
        } else {
			getRujukan(asalpasien, formRujukan);
		}
    });
});

function getRujukan(asalpasien, formRujukan, formSelected = '') {
    formRujukan.empty();
    $.ajax({
        url: '{site_url}tpoliklinik_pendaftaran/getRujukan',
        method: "POST",
        dataType: "json",
        data: {
            "asal": asalpasien
        },
        success: function(data) {
            formRujukan.select2("destroy");
            formRujukan.empty();
            formRujukan.append("<option value=''>Pilih Opsi</option>");
            data.map((item) => {
                formRujukan.append(`<option value="${item.id}" ${(formSelected == item.id ? 'selected' : '')}>${item.nama}</option>`);
            });
            formRujukan.select2();
        }
    });
}

</script>

<!-- Modal Included -->
<?php $this->load->view('Trujukan_rumahsakit_verifikasi/modal/modal_edit') ?>