<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block push-10">
    <div class="block-content bg-primary" style="border-radius: 10px;">
        <div class="form-horizontal">
            <div class="row pull-10">
                <div class="col-md-2 push-10">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tujuan Laboratorium</label>
                        <div class="col-xs-12">
                            <select id="tujuan_laboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach ($tujuan_laboratorium as $r) { ?>
                                <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 push-10">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Asal Pasien</label>
                        <div class="col-xs-12">
                            <select id="asal_pasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <option value="1">Poliklinik</option>
                                <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                <option value="3">Rawat Inap</option>
                                <option value="4">One Day Surgery (ODS)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 push-10">
                    <div class="form-group">
                        <label class="col-xs-12" for="dokter_peminta">Dokter Peminta</label>
                        <div class="col-xs-12">
                            <select id="dokter_peminta" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 push-10">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tanggal</label>
                        <div class="col-xs-12">
                            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_dari" value="{tanggal}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_sampai" value="{tanggal}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 push-10">
                    <div class="form-group">
                        <label class="col-xs-12" for="">&nbsp;&nbsp;</label>
                        <div class="col-xs-12">
                            <button class="btn btn-success" id="btn-search" type="button" title="Cari"><i class="fa fa-filter"></i> Cari</button>
                            <button class="btn btn-warning" type="button" title="Filter" data-toggle="modal" data-target="#modal-filter"><i class="fa fa-expand"></i></button>
                            <button class="btn btn-danger" id="btn-toggle-sound" type="button" title="Unmute"><i class="fa fa-volume-up"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block">
    <div class="block-content">
        <!-- Tabs -->
        <ul class="nav nav-pills" id="section-tabs">
            <li class="section-tab" data-tab="1">
                <a href="javascript:void(0)"><i class="si si-list"></i> Antrian Permintaan</a>
            </li>
            <li class="section-tab" data-tab="2">
                <a href="javascript:void(0)"><i class="si si-plus"></i> Tambah Baru</a>
            </li>
            <li class="section-tab" data-tab="3">
                <a href="javascript:void(0)"><i class="fa fa-bookmark"></i> Daftar Rencana</a>
            </li>
            <li class="section-tab" data-tab="4">
                <a href="javascript:void(0)"><i class="si si-plus"></i> Tambah Rawat Inap</a>
            </li>
        </ul>

        <!-- Section Tab 1 Content -->
        <div class="section-content" data-tab="1">
            <hr>

            <div class="form-horizontal">
                <div class="row pull-10">
                    <div class="col-md-3">
                        <div class="form-group" style="margin-bottom: 10px;">
                            <label class="col-md-4 control-label" for="">No Medrec</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="nomor_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 10px;">
                            <label class="col-md-4 control-label" for="">Nama Pasien</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="namapasien" value="">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 10px;">
                            <label class="col-md-4 control-label" for="">Status</label>
                            <div class="col-md-8">
                                <select id="status_antrian" name="status_antrian" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="#" selected>Semua</option>
                                    <option value="0">Menunggu Dipanggil</option>
                                    <option value="1">Sedang Dilayani</option>
                                    <option value="2">Dilewati</option>
                                    <option value="3">Telah Dilayani</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-4 control-label" for=""></label>
                            <div class="col-md-8">
                                <button class="btn btn-success text-uppercase" type="button" id="btn-filter" style="font-size:13px; width:100%; float:right;"><i class="fa fa-filter"></i> Filter</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 push-10">
                        <div class="col-md-12">
                            <div class="form-group push-5 div_counter">
                                <div style="background-color: #EEEFFF; border-radius: 10px; width: 100%; text-align: center;">
                                    <div class="div" style="padding: 50px;">
                                        <i class="fa fa-spinner fa-spin" style="font-size:24px; padding: 10px;"></i><br>
                                        <label for="">Loading</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            
            <ul class="nav nav-pills">
                <li id="tab-semua" class="<?= ($status_pemeriksaan == '#' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)" onclick="setTabStatusPemeriksaan('#')"><i class="si si-list"></i> Semua</a>
                </li>
                <li id="tab-menunggu" class="<?= ($status_pemeriksaan == '0' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)"  onclick="setTabStatusPemeriksaan(0)"><i class="si si-pin"></i> Menunggu Dipanggil </a>
                </li>
                <li id="tab-sedang-dilayani" class="<?= ($status_pemeriksaan == '1' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)" onclick="setTabStatusPemeriksaan(1)"><i class="fa fa-check-square-o"></i> Sedang Dilayani</a>
                </li>
                <li id="tab-telah-dilayani" class="<?= ($status_pemeriksaan == '2' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)"  onclick="setTabStatusPemeriksaan(2)"><i class="fa fa-times"></i> Dilewati</a>
                </li>
                <li id="tab-dilewati" class="<?= ($status_pemeriksaan == '3' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)"  onclick="setTabStatusPemeriksaan(3)"><i class="fa fa-flag"></i> Telah Dilayani</a>
                </li>
            </ul>

            <br>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="transaksi_antrian_permintaan">
                            <thead>
                                <tr>
                                    <th width="10%">No</th>
                                    <th width="10%">Asal Pasien</th>
                                    <th width="10%">Nomor Pendaftaran</th>
                                    <th width="10%">Nomor Permintaan</th>
                                    <th width="10%">Tanggal Permintaan</th>
                                    <th width="10%">Urutan Antrian</th>
                                    <th width="10%">Antrian Daftar</th>
                                    <th width="10%">No. Medrec</th>
                                    <th width="10%">Nama Pasien</th>
                                    <th width="10%">Kelompok Pasien</th>
                                    <th width="10%">Status Antrian</th>
                                    <th width="10%">Status Tindakan</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section Tab Content for Tab 2 -->
        <div class="section-content" data-tab="2">
            <?php $this->load->view('Term_laboratorium_umum_permintaan/tab/tab_tambah_baru'); ?>
        </div>
        
        <!-- Section Tab 3 Content -->
        <div class="section-content" data-tab="3">
            <?php $this->load->view('Term_laboratorium_umum_permintaan/tab/tab_daftar_rencana'); ?>
        </div>

        <!-- Section Tab Content for Tab 4 -->
        <div class="section-content" data-tab="4">
            <?php $this->load->view('Term_laboratorium_umum_permintaan/tab/tab_tambah_rawat_inap'); ?>
        </div>
    </div>
</div>

<input type="hidden" id="tujuan_pelayanan" value="{tujuan_pelayanan}">
<input type="hidden" id="tujuan_laboratorium_array" value="{tujuan_laboratorium_array}">
<input type="hidden" id="status_pemeriksaan" name="status_pemeriksaan" value="{status_pemeriksaan}">

<?php $this->load->view('Term_laboratorium_umum_permintaan/modal/modal_filter'); ?>
<?php $this->load->view('Term_laboratorium_umum_permintaan/modal/modal_call_antrian'); ?>
<?php $this->load->view('Term_laboratorium_umum_permintaan/modal/modal_data_permintaan'); ?>
<?php $this->load->view('Term_laboratorium_umum_permintaan/modal/modal_ubah_tujuan_laboratorium'); ?>
<?php $this->load->view('Term_laboratorium_umum_permintaan/modal/modal_ubah_kelas_tarif'); ?>
<?php $this->load->view('Term_laboratorium_umum_permintaan/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    var transaksiId;
    var activeTab = 1;
    // var activeTab = localStorage.getItem('activeTab') || 1;

    $(document).ready(function() {
        setSectionTab(activeTab);
        setTabStatusPemeriksaan($("#status_pemeriksaan").val());

        $(document).on("click", ".section-tab", function() {
            var tabNumber = $(this).data('tab');
            setSectionTab(tabNumber);
        });

        
        $(document).on("click", "#btn-toggle-sound", function() {
            var iconElement = $(this).find('i');

            if (iconElement.hasClass('fa-volume-up')) {
                iconElement.removeClass('fa-volume-up').addClass('fa-volume-off');
                $(this).attr('title', 'Unmute');
            } else {
                iconElement.removeClass('fa-volume-off').addClass('fa-volume-up');
                $(this).attr('title', 'Mute');
            }
        });

        $(document).on("click", "#btn-filter, #btn-filter-advance, #btn-search", function() {
            $("#cover-spin").show();

            loadDataTableTransaksiPermintaan();
        });

        refreshCounter();

        setTimeout(function() {
            startWorker();
        }, 10000);
    });

    function callAntrianManual(antrianId) {
        let tujuanPelayanan = $("#tujuan_pelayanan").val();

        getDataAntrianId(antrianId);

        $.ajax({
            url: '{site_url}Term_laboratorium_umum_permintaan/callAntrian',
            type: 'POST',
            dataType: "json",
            data: {
                tujuan_pelayanan: tujuanPelayanan,
                antrian_id: antrianId,
            },
            success: function(data) {
                if (data == false){
                    swal({
                        title: "Maaf!",
                        text: "No Antrian Tidak tersedia",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    });
                } else {
                    refreshCounter();
                    loadDataTableTransaksiPermintaan();
                }
            }
        });
    }

    function getDataAntrianId(antrianId) {
        let tujuan_pelayanan = $("#tujuan_pelayanan").val();
	    let tujuan_laboratorium = $("#tujuan_laboratorium_array").val();
        
        $("#cover-spin").show();

        $.ajax({
            url: '{site_url}Term_laboratorium_umum_permintaan/getInformasiAntrian',
            type: 'POST',
            dataType: "json",
            data: {
                antrian_id: antrianId,
                tujuan_pelayanan: tujuan_pelayanan,
                tujuan_laboratorium: tujuan_laboratorium,
            },
            success: function(result) {
                $("#cover-spin").hide();

                let data = result.data;
                let antrian_id_next = result.antrian_id_next;

                if (data){
                    $("#antrian-nomor").html(`NOMOR ANTRIAN : ${data.kode_antrian}`);
                    $("#antrian-pasien").html(`${data.nama_pasien} - ${data.nomor_medrec}`);
                    $("#antrian-nomor-transaksi").html(`${data.nomor_pendaftaran} - ${data.nomor_laboratorium}`);
                    $("#antrian-identitas").html(`${data.jenis_kelamin}, ${data.tanggal_lahir} - ${data.umur_tahun} Tahun ${data.umur_bulan} Bulan ${data.umur_hari} Hari`);
                    $("#antrian-aksi").html(`
                        <button class="btn btn-sm btn-success" type="button" onclick="prosesTransaksi('${data.asal_rujukan}', '${data.pendaftaran_id}', '${data.antrian_id}')" data-dismiss="modal"><i class="fa fa-check-circle"></i> PROSES TRANSAKSI</button>
                        <button class="btn btn-sm btn-danger" type="button" onclick="batalTransaksi('${data.antrian_id}')" data-dismiss="modal"><i class="fa fa-trash"></i> BATAL TRANASAKSI</button>
                        <button class="btn btn-sm btn-info" type="button" onclick="recallManual('${data.antrian_id}')" data-dismiss="modal"><i class="fa fa-refresh"></i> PANGGIL ULANG</button>
                        <button class="btn btn-sm btn-primary" type="button" onclick="callLewati('${data.antrian_id}', '${antrian_id_next}')" data-dismiss="modal"><i class="fa fa-forward"></i> LEWATI ANTRIAN</button>
                    `);
                }
            }
        });
    }
    
    function callLewati(antrianId, antrianIdNext) {
        let tujuanPelayanan = $("#tujuan_pelayanan").val();

        $("#cover-spin").show();

        $.ajax({
            url: '{site_url}Term_laboratorium_umum_permintaan/callLewati',
            type: 'POST',
            dataType: "json",
            data: {
                tujuan_pelayanan: tujuanPelayanan,
                antrian_id: antrianId,
                antrian_id_next: antrianIdNext,
            
            },
            success: function(data) {
                $("#cover-spin").hide();

                if (data == false){
                    swal({
                        title: "Maaf!",
                        text: "No Antrian Tidak tersedia",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    });
                } else {
                    loadDataTableTransaksiPermintaan();
                }
            }
        });
    }

    function recallManual(antrianId) {
        let tujuanPelayanan = $("#tujuan_pelayanan").val();

        $("#cover-spin").show();

        $.ajax({
            url: '{site_url}Term_laboratorium_umum_permintaan/callAntrian',
            type: 'POST',
            dataType: "json",
            data: {
                tujuan_pelayanan: tujuanPelayanan,
                antrian_id: antrianId,
            
            },
            success: function(data) {
                $("#cover-spin").hide();

                if (data == false){
                    swal({
                        title: "Maaf!",
                        text: "No Antrian Tidak tersedia",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    });
                } else {
                    loadDataTableTransaksiPermintaan();
                }
            }
        });
    }

    function refreshCounter() {
        let tujuan_pelayanan = $("#tujuan_pelayanan").val();
        let tujuan_laboratorium = $("#tujuan_laboratorium_array").val();
        
        $.ajax({
            url: '{site_url}Term_laboratorium_umum_permintaan/refreshDivCounter',
            type: 'POST',
            dataType: "json",
            data: {
                tujuan_pelayanan: tujuan_pelayanan,
                tujuan_laboratorium: tujuan_laboratorium,
            },
            success: function(data) {
                $(".div_counter").html(data);
                $("#cover-spin").hide();
            }
        });
    }

    function loadDataTableTransaksiPermintaan() {
        let tujuan_laboratorium = $("#selectTujuanLab option:selected").val();
        let asal_pasien = $("#selectAsalPasien option:selected").val();
        let tanggal_dari = $("#inputTanggalPermintaanDari").val();
        let tanggal_sampai = $("#inputTanggalPermintaanSampai").val();
        let dokter_peminta = $("#dokter_peminta option:selected").val();
        let kelompok_pasien = $("#selectKelompokPasien option:selected").val();
        let nomor_medrec = $("#inputNomorMedrec").val();
        let nama_pasien = $("#inputNamaPasien").val();
        let nomor_pendaftaran = $("#inputNomorPendaftaran").val();
        let nomor_permintaan = $("#inputNomorPermintaan").val();
        let status_antrian = $("#selectStatusAntrian").val();
        let status_tindakan = $("#selectStatusTindakan").val();

        $('#transaksi_antrian_permintaan').DataTable().destroy();
        $('#transaksi_antrian_permintaan').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "5%",
                    "targets": [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11],
                    "className": "text-center"
                },
                {
                    "width": "5%",
                    "targets": [8],
                    "className": "text-left"
                },
                {
                    "width": "10%",
                    "targets": [12],
                    "className": "text-center"
                },
            ],
            ajax: {
                url: '{site_url}term_laboratorium_umum_permintaan/getIndex',
                type: "POST",
                dataType: 'json',
                data: {
                    tujuan_laboratorium: tujuan_laboratorium,
                    asal_pasien: asal_pasien,
                    tanggal_dari: tanggal_dari,
                    tanggal_sampai: tanggal_sampai,
                    dokter_peminta: dokter_peminta,
                    kelompok_pasien: kelompok_pasien,
                    nomor_medrec: nomor_medrec,
                    nama_pasien: nama_pasien,
                    nomor_pendaftaran: nomor_pendaftaran,
                    nomor_permintaan: nomor_permintaan,
                    status_antrian: status_antrian,
                    status_tindakan: status_tindakan,
                }
            }
        });

        $("#cover-spin").hide();
    }

    function startWorker() {
        if (typeof Worker !== "undefined") {
            if (typeof workerInstance == "undefined") {
                workerInstance = new Worker("{site_url}assets/js/worker.js");
            }

            workerInstance.postMessage({ 'cmd': 'start', 'msg': 30000 });
            workerInstance.onmessage = function (event) {
                loadDataPlay();
                refreshCounter();
            };
            workerInstance.onerror = function (event) {
                window.location.reload();
            };
        } else {
            alert("Sorry, your browser does not support Autosave Mode");
        }
    }

    function loadDataPlay() {
        let tujuanLaboratorium = $("#tujuan_laboratorium_array").val();
        
        $.ajax({
            url: '{site_url}term_laboratorium_umum_permintaan/getNotif',
            dataType: "JSON",
            method: "POST",
            data: {
                tujuan_laboratorium: tujuanLaboratorium
            },
            success: function (data) {
                if (data.sound_play != null) {
                    playSoundArray(data.sound_play);
                    updateNotification(data.assesmen_id);
					$('#transaksi_antrian_permintaan').DataTable().ajax.reload( null, false );
                }
            }
        });
    }

    function updateNotification(antrianId) {
        $.ajax({
            url: '{site_url}term_laboratorium_umum_permintaan/updateNotif',
            dataType: "JSON",
            method: "POST",
            data: { antrian_id: antrianId },
            success: function (data) {
                // Additional logic can be added here if needed
            }
        });
    }

    function playSoundArray(soundFiles) {
        let pathSound = '{site_url}assets/upload/sound_antrian/';
        let soundArray = soundFiles.split(":");

        function recursivePlay(index) {
            let fileSound = new Audio(pathSound + soundArray[index]);
            play(fileSound, function () {
                if (index + 1 < soundArray.length) {
                    recursivePlay(index + 1);
                }
            });
        }

        recursivePlay(0);
    }

    function play(audio, callback) {
        audio.play();
        if (callback) {
            audio.onended = callback;
        }
    }

    function setSectionTab(tabNumber) {
        // Hide all section contents
        $('.section-content').hide();

        // Show the selected section content
        $(`.section-content[data-tab="${tabNumber}"]`).show();

        // Toggle 'active' class for the tabs
        $('.section-tab').removeClass('active');
        $(`.section-tab[data-tab="${tabNumber}"]`).addClass('active');

        // Store the active tab in localStorage
        localStorage.setItem('activeTab', tabNumber);

        if (tabNumber == 1) {
            loadDataTableTransaksiPermintaan();
        } else if (tabNumber == 2) {
            loadDataTableTransaksiTambahBaru();
        } else if (tabNumber == 3) {
            loadDataTableTransaksiDaftarRencana();
        } else if (tabNumber == 4) {
            loadDataTableTransaksiTambahRanap();
        }
    }

    function setTabStatusPemeriksaan(tab) {
        $("#cover-spin").show();

        document.getElementById("tab-semua").classList.remove("active");
        document.getElementById("tab-menunggu").classList.remove("active");
        document.getElementById("tab-sedang-dilayani").classList.remove("active");
        document.getElementById("tab-telah-dilayani").classList.remove("active");
        document.getElementById("tab-dilewati").classList.remove("active");
        
        $("#status_antrian").removeAttr("disabled");
        $("#status_pemeriksaan").removeAttr("disabled");

        $("#status_antrian").val(tab).trigger('change');

        if (tab == '#') {
            document.getElementById("tab-semua").classList.add("active");
            $("#status_antrian").attr('disabled', 'disabled');
        }
        
        if (tab == '0') {
            document.getElementById("tab-menunggu").classList.add("active");
            $("#status_antrian").attr('disabled', 'disabled');
        }
        
        if (tab == '1') {
            document.getElementById("tab-sedang-dilayani").classList.add("active");
            $("#status_antrian").attr('disabled', 'disabled');
        }
        
        if (tab == '2') {
            document.getElementById("tab-telah-dilayani").classList.add("active");
            $("#status_antrian").attr('disabled', 'disabled');
        }

        if (tab == '3') {
            document.getElementById("tab-dilewati").classList.add("active");
            $("#status_antrian").attr('disabled', 'disabled');
        }

        loadDataTableTransaksiPermintaan();
    }
</script>
