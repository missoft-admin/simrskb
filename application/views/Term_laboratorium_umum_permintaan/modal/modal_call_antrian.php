<!-- Modal Call Antrian -->
<div class="modal fade in" id="modal-call-antrian" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 10px; width: 620px;">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content">
                    <div class="text-center" style="margin-bottom: 10px;">
                        <div class="h2 font-w700 text-center text-primary" id="antrian-nomor">NOMOR ANTRIAN : -</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="http://localhost/halmahera.simrs/assets/upload/avatars/default.jpg" id="antrian-photo" alt="" class="img-fluid" style="width: 100%; height: 90px; object-fit:cover;border-radius: 10px;">
                        </div>
                        <div class="col-md-9">
                            <div class="h4 font-w600" style="margin-bottom: 10px;" id="antrian-pasien">-</div>
                            <div style="margin-bottom: 10px;" class="h4" id="antrian-nomor-transaksi">-</div>
                            <div style="margin-bottom: 10px;" class="h4" id="antrian-identitas">-</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-center" style="padding: 20px 0;" id="antrian-aksi"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>