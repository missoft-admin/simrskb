<hr>

<div class="form-horizontal">
    <div class="row pull-10">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="jenis">Jenis</label>
                <div class="col-md-8">
                    <select id="jenis_tab4" name="jenis_tab4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Jenis">
                        <option value="0">Semua</option>
                        <option value="1">Direct</option>
                        <option value="2">Non Direct</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="asal_pasien_tab4">Asal Pasien</label>
                <div class="col-md-8">
                    <select id="asal_pasien_tab4" name="asal_pasien_tab4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Tipe">
                        <option value="0">Semua</option>
                        <option value="3">Rawat Inap</option>
                        <option value="4">One Day Surgery (ODS)</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="dokter_tab4">Dokter</label>
                <div class="col-md-8">
                    <select id="dokter_tab4" name="dokter_tab4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Dokter">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mdokter') as $dokter) { ?>
                            <option value="<?=$dokter->id?>"><?=$dokter->nama?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="medrec_tab4">No. Medrec</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="medrec_tab4" placeholder="No. Medrec" name="medrec_tab4" value="">
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="tanggal_tab4">Tanggal</label>
                <div class="col-md-8">
                    <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                        <input class="form-control" type="text" id="tanggal_dari_tab4" placeholder="Tanggal Dari" value="">
                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                        <input class="form-control" type="text" id="tanggal_sampai_tab4" placeholder="Tanggal Sampai" value="">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="status_permintaan_tab4">Status Permintaan</label>
                <div class="col-md-8">
                    <select id="status_permintaan_tab4" name="status_permintaan_tab4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Status Permintaan">
                        <option value="0">Semua</option>
                        <option value="1">Memiliki Permintaan</option>
                        <option value="2">Tidak Memiliki Permintaan</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="klinik_kelas_tab4">Klinik / Kelas</label>
                <div class="col-md-8">
                    <select id="klinik_kelas_tab4" name="klinik_kelas_tab4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Klinik / Kelas">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mpoliklinik', ['status' => 1]) as $poliklinik) { ?>
                            <option value="<?=$poliklinik->id?>"><?=$poliklinik->nama?></option>
                        <?php } ?>
                        <?php foreach (get_all('mkelas') as $kelas) { ?>
                            <option value="<?=$kelas->id?>"><?=$kelas->nama?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="kelompok_pasien_tab4">Kelompok Pasien</label>
                <div class="col-md-8">
                    <select id="kelompok_pasien_tab4" name="kelompok_pasien_tab4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Kelompok Pasien">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mpasien_kelompok') as $kelompok) { ?>
                            <option value="<?=$kelompok->id?>"><?=$kelompok->nama?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" for="nama_pasien_tab4">Nama Pasien</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="nama_pasien_tab4" placeholder="Nama Pasien" name="nama_pasien_tab4" value="">
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 5px;">
                <label class="col-md-4 control-label" for=""></label>
                <div class="col-md-8">
                    <button class="btn btn-success text-uppercase" type="button" id="btn-filter-transaksi-tambah-rawat-inap" style="font-size:13px; width:100%; float:right;"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="transaksi-tambah-rawat-inap">
                <thead>
                    <tr>
                        <th width="5%">Action</th>
                        <th width="5%">Status Permintaan</th>
                        <th width="5%">Jenis</th>
                        <th width="10%">Tanggal Daftar</th>
                        <th width="10%">Asal Pasien</th>
                        <th width="10%">Nomor Pendaftaran</th>
                        <th width="10%">Nomor Medrec</th>
                        <th width="10%">Nama Pasien</th>
                        <th width="10%">Kelompok Pasien</th>
                        <th width="10%">Detail</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", "#btn-filter-transaksi-tambah-rawat-inap", function() {
        $("#cover-spin").show();
        
        loadDataTableTransaksiTambahRanap();
    });
});

function loadDataTableTransaksiTambahRanap() {
    let jenis = $("#jenis_tab4 option:selected").val();
    let asal_pasien = $("#asal_pasien_tab4 option:selected").val();
    let dokter = $("#dokter_tab4 option:selected").val();
    let medrec = $("#medrec_tab4").val();
    let tanggal_dari = $("#tanggal_dari_tab4").val();
    let tanggal_sampai = $("#tanggal_sampai_tab4").val();
    let status_permintaan = $("#status_permintaan_tab4 option:selected").val();
    let klinik_kelas = $("#klinik_kelas_tab4 option:selected").val();
    let kelompok_pasien = $("#kelompok_pasien_tab4 option:selected").val();
    let nama_pasien = $("#nama_pasien_tab4").val();

    $('#transaksi-tambah-rawat-inap').DataTable().destroy();
    $('#transaksi-tambah-rawat-inap').DataTable({
        "autoWidth": false,
        "searching": true,
        "pageLength": 100,
        "serverSide": true,
        "processing": false,
        "order": [],
        "ordering": false,
        "columnDefs": [
            {
                "width": "5%",
                "targets": [0, 1, 2, 3, 4, 5, 6, 8, 9],
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": [7],
                "className": "text-left"
            },
        ],
        ajax: {
            url: '{site_url}term_laboratorium_umum_permintaan/getIndexTambahRanap',
            type: "POST",
            dataType: 'json',
            data: {
                jenis: jenis,
                asal_pasien: asal_pasien,
                dokter: dokter,
                medrec: medrec,
                tanggal_dari: tanggal_dari,
                tanggal_sampai: tanggal_sampai,
                status_permintaan: status_permintaan,
                klinik_kelas: klinik_kelas,
                kelompok_pasien: kelompok_pasien,
                nama_pasien: nama_pasien,
            }
        }
    });

    $("#cover-spin").hide();
}
</script>
