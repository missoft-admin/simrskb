<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content ">
		<?php echo form_open_multipart('msetting_honor_dokter_email/update') ?>
		<div class="form-horizontal push-10-t" style="margin-bottom: 10px;">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">
									<img class="" height="150px" width="160px" id="output_img_logo" src="{upload_path}setting_printout_honor_dokter/<?=($img_logo?$img_logo:'no_image.png')?>" />
							</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="">Gambar Header</label>
						<div class="col-md-8">
								<div class="box">
										<input type="file" id="img_logo" class="inputfile inputfile-3" onchange="loadImgLogo(event)" style="display:none;" name="img_logo" value="{img_logo}" accept="image/png, image/gif, image/jpg, image/jpeg" />
										<label for="img_logo"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Nama Tanda Tangan</label>
							<div class="col-md-8">
									<div class="box">
											<input type="text" id="nama_ttd" class="form-control" name="nama_ttd" value="{nama_ttd}" />
									</div>
							</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for=""></label>
							<div class="col-md-8">
									<img class="" height="150px" width="160px" id="output_img_ttd" src="{upload_path}setting_printout_honor_dokter/<?=($img_ttd?$img_ttd:'no_image.png')?>" />
							</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Tanda Tangan</label>
							<div class="col-md-8">
									<div class="box">
											<input type="file" id="img_ttd" class="inputfile inputfile-3" onchange="loadImgSignature(event)" style="display:none;" name="img_ttd" value="{img_ttd}" accept="image/png, image/gif, image/jpg, image/jpeg" />
											<label for="img_ttd"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
									</div>
							</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Jabatan</label>
							<div class="col-md-8">
									<div class="box">
											<input type="text" id="nama_jabatan" class="form-control" name="nama_jabatan" value="{nama_jabatan}" />
									</div>
							</div>
					</div>
				</div>
			</div>
		</div>
		
		<b><span class="label label-success" style="font-size:12px">CATATAN FOOTER</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_catatan_footer">
			<thead>
				<tr>
					<th width="20%">Keterangan</th>
					<th width="20%">Urutan Tampil</th>
					<th width="20%">Rekap Pendapatan</th>
					<th width="20%">Rekap Pendapatan Tunda</th>
					<th width="20%">Detail Rawat Jalan</th>
					<th width="20%">Detail Radiologi</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<input type="text" class="form-control" id="catatan_keterangan" value="">
					</th>
					<th>
						<input type="number" class="form-control" id="catatan_urutan_tampil" value="">
					</th>
					<th>
						<select class="js-select2 form-control" id="catatan_rekap_pendapatan" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Tampil</option>
							<option value="0">Tidak Tampil</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="catatan_rekap_pendapatan_tunda" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Tampil</option>
							<option value="0">Tidak Tampil</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="catatan_detail_rajal" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Tampil</option>
							<option value="0">Tidak Tampil</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="catatan_detail_radiologi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Tampil</option>
							<option value="0">Tidak Tampil</option>
						</select>
					</th>
					<th>
						<button type="button" class="btn btn-primary" id="tambah_catatan_footer">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($setting_catatan_footer as $row) { ?>
					<tr>
						<td hidden><?=$row->id?></td>
						<td><?=$row->keterangan?></td>
						<td><?=$row->urutan?></td>
						<td hidden><?=$row->rekap_pendapatan?></td>
						<td><?=$row->rekap_pendapatan?></td>
						<td hidden><?=$row->rekap_pendapatan_tunda?></td>
						<td><?=$row->rekap_pendapatan_tunda?></td>
						<td hidden><?=$row->detail_rajal?></td>
						<td><?=$row->detail_rajal?></td>
						<td hidden><?=$row->detail_radiologi?></td>
						<td><?=$row->detail_radiologi?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">SETTING KIRIM EMAIL</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_kirim_email">
			<thead>
				<tr>
					<th width="20%">Kategori Dokter</th>
					<th width="20%">Nama Dokter</th>
					<th width="20%">Rekap Pendapatan</th>
					<th width="20%">Rekap Pendapatan Tunda</th>
					<th width="20%">Detail Rawat Jalan</th>
					<th width="20%">Detail Radiologi</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select class="js-select2 form-control" id="kirim_email_kategori_dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('mdokter_kategori') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->nama?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="kirim_email_dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="kirim_email_pendapatan" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Ya</option>
							<option value="0">Tidak</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="kirim_email_pendapatan_tunda" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Ya</option>
							<option value="0">Tidak</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="kirim_email_detail_rajal" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Ya</option>
							<option value="0">Tidak</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="kirim_email_detail_radiologi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" selected>Ya</option>
							<option value="0">Tidak</option>
						</select>
					</th>
					<th>
						<button type="button" class="btn btn-primary" id="tambah_kirim_email">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($setting_kirim_email as $row) { ?>
					<tr>
						<td hidden><?=$row->id?></td>
						<td hidden><?=$row->idkategori?></td>
						<td><?=$row->namakategori?></td>
						<td hidden><?=$row->iddokter?></td>
						<td><?=$row->namadokter?></td>
						<td hidden><?=$row->rekap_pendapatan?></td>
						<td><?=$row->rekap_pendapatan?></td>
						<td hidden><?=$row->rekap_pendapatan_tunda?></td>
						<td><?=$row->rekap_pendapatan_tunda?></td>
						<td hidden><?=$row->detail_rajal?></td>
						<td><?=$row->detail_rajal?></td>
						<td hidden><?=$row->detail_radiologi?></td>
						<td><?=$row->detail_radiologi?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<hr>
		<div class="text-right bg-light lter">
			<button class="btn btn-info" type="submit">Simpan Perubahan</button>
		</div>
		<br>
		
		<input type="hidden" class="form-control" id="tbl_catatan_footer" name="catatan_footer">
		<input type="hidden" class="form-control" id="tbl_kirim_email" name="kirim_email">

		<?php echo form_close(); ?>
</div>

<input type="hidden" class="form-control" id="rowindex">

<script type="text/javascript">
	$(document).on("change","#kirim_email_kategori_dokter",function(){
		const id = $(this).val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_honor/list_dokter/' + id,
			dataType: "json",
			success: function(data) {
				$("#kirim_email_dokter").empty();
				$('#kirim_email_dokter').append('<option value="0" selected>Semua Dokter</option>');
				$('#kirim_email_dokter').append(data.detail);
			}
		});
	});
	
	$(document).on("click", "#tambah_catatan_footer", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_catatan_footer tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#catatan_keterangan").val() && $cells.eq(1).text() === $("#catatan_urutan_tampil").val()) {
					sweetAlert("Maaf...", $("#catatan_keterangan").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td width='10%'>" + $("#catatan_keterangan").val(); + "</td>";
			content += "<td width='10%'>" + $("#catatan_urutan_tampil").val(); + "</td>";
			content += "<td style='display: none'>" + $("#catatan_rekap_pendapatan option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#catatan_rekap_pendapatan option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#catatan_rekap_pendapatan_tunda option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#catatan_rekap_pendapatan_tunda option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#catatan_detail_rajal option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#catatan_detail_rajal option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#catatan_detail_radiologi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#catatan_detail_radiologi option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_catatan_footer tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_catatan_footer tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#tambah_kirim_email", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_kirim_email tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#kirim_email_kategori_dokter option:selected").val() && $cells.eq(2).text() === $("#kirim_email_dokter option:selected").val()) {
					sweetAlert("Maaf...", "Kategori Dokter '" + $("#kirim_email_kategori_dokter option:selected").text() + ", Dokter '" + $("#kirim_email_dokter option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#kirim_email_kategori_dokter option:selected").val(); + "</td>";
			content += "<td>" + $("#kirim_email_kategori_dokter option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#kirim_email_dokter option:selected").val(); + "</td>";
			content += "<td>" + $("#kirim_email_dokter option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#kirim_email_pendapatan option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#kirim_email_pendapatan option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#kirim_email_pendapatan_tunda option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#kirim_email_pendapatan_tunda option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#kirim_email_detail_rajal option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#kirim_email_detail_rajal option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#kirim_email_detail_radiologi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#kirim_email_detail_radiologi option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_kirim_email tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_kirim_email tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", ".delete", function() {
		var row = $(this).closest('td').parent();
		swal({
			title: '',
			text: "Apakah anda yakin akan menghapus data ini?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya!",
			cancelButtonText: "Batalkan!",
			closeOnConfirm: false,
			closeOnCancel: true
		}).then(function() {
			row.remove();

			return submitForm();
		})
		return false;
	});

	function submitForm() {
		var catatan_footer_tbl = $('table#table_catatan_footer tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(8))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var kirim_email_tbl = $('table#table_kirim_email tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(16))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		$("#tbl_catatan_footer").val(JSON.stringify(catatan_footer_tbl));
		$("#tbl_kirim_email").val(JSON.stringify(kirim_email_tbl));
	}

	function loadImgLogo(event) {
		var output_img = document.getElementById('output_img_logo');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}

	function loadImgSignature(event) {
		var output_img = document.getElementById('output_img_ttd');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
</script>
